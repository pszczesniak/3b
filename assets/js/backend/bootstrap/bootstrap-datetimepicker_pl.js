(function($){
$.fn.datetimepicker.dates['pl'] = {
        days: ["Niedziela", "Poniedzia�ek", "Wtorek", "�roda", "Czwartek", "Pi�tek", "Sobota", "Niedziela"],
        daysShort: ["Nie", "Pn", "Wt", "�r", "Czw", "Pt", "So", "Nie"],
        daysMin: ["N", "Pn", "Wt", "�r", "Cz", "Pt", "So", "N"],
        months: ["Stycze�", "Luty", "Marzec", "Kwiecie�", "Maj", "Czerwiec", "Lipiec", "Sierpie�", "Wrzesie�", "Pa�dziernik", "Listopad", "Grudzie�"],
        monthsShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze", "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
        today: "Dzisiaj",
		suffix: [],
		meridiem: [],
        weekStart: 1
};
}(jQuery));