'use strict';

var KAPI = {
    'config' : {
        'container' : $('#myFeature'),
        'itemNavSelector' : 'h3',
		'left_side_width' : 220,
        'itemNavProcessor' : function($selection) {
            return 'Preview of ' +
                $selection.eq(0).text();
        }
    },

    'init' : function(config) {
		KAPI.sendFormData();
		KAPI.menuBuilder();
    },

    'sendFormData' : function() {
        $('form.ajaxform').submit(function() {
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $('form.ajaxform').serialize(),
				url: $(this).attr('action'),
				beforeSend: function() { 
					$("#validation-errors").hide().empty(); 
				},
				success: function(data) {
					if(data.success == false)
					{
						var arr = data.errors;
						$.each(arr, function(index, value)
						{
							if (value.length != 0)
							{
								$("#validation-errors").append('<strong>'+ value +'</strong><br/>');
							}
						});
						$("#validation-errors").show();
					} else {
						/*location.reload();*/alert('OK');
					}
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					alert('Something went to wrong.Please Try again later...');
				}
			});
			return false;
		});
    },
	
	'menuBuilder' : function() {
		$('.dd').nestable({ maxDepth: 2 });
		$( "#menu-items-list " ).delegate( ".item-action-edit", "click", function() {
			var idItem = $(this).attr('id');
			var arr = idItem.split('-');
			$('#item-edit-'+arr[1]).css('display','block');
			$('#item-edit-'+arr[1]).append(arr[1]);
		});
		
		$('.selecctall').click(function(event) {  //on click
			if(this.checked) { // check select status
				$('.'+$(this).attr('id')).each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkbox1"              
				});
			}else{
				$('.'+$(this).attr('id')).each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkbox1"                      
				});        
			}
		});
		
		$('#menu-selected-page').click(function(event){
			$('.checkbox_page').each(function() { //loop through each checkbox
				if($(this).is(':checked') ) {
					//console.log($(this));
					//$("#menu-items-list").append('<li class="dd-item" data-id="'+$(this).attr('value')+'"> <div class="dd-handle">'+$(this).parent().text()+'<a title="Edytuj wpis" href="#" class="item-action-edit" id="item-'+$(this).attr('value')+'">[strona]</a> </div><div class="item-edit" id="item-edit-'+$(this).attr('value')+'">test</div></li>');
					$.ajax({
						method: "POST",
						url: "/cms/cmsmenu/icreate",
						dataType : 'json',
						data: { menu_id: 1, item_type: 1, element_id: $(this).attr('value') , element_name: $(this).parent().text() }
					}).done(function( result ) {
						if(result.success) {
							var liContent = '<li class="dd-item dd3-item" data-id="'+$(this).attr('value')+'">';
							liContent += '<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>';
							liContent += '<div class="dd3-content">'+$(this).parent().text()+'<a title="Edytuj wpis" href="#" class="item-action-edit label label-primary" id="item-'+$(this).attr('value')+'">strona<span class="glyphicon glyphicon-triangle-bottom"></span></a></div>';
							liContent += '<div class="item-edit" id="item-edit-'+$(this).attr('value')+'">test</div>';
							liContent += '</li>';
							$("#menu-items-list").append(liContent);
						} else { console.log(result); }
					});
				}               
			});
		});
		
		$('#menu-selected-category').click(function(event){
			$('.checkbox_category').each(function() { //loop through each checkbox
				if($(this).is(':checked') ) {
					//console.log($(this));
					//$("#menu-items-list").append('<li class="dd-item" data-id="'+$(this).attr('value')+'"> <div class="dd-handle">'+$(this).parent().text()+'<a title="Edytuj wpis" href="#" class="item-action-edit" id="item-'+$(this).attr('value')+'">[strona]</a> </div><div class="item-edit" id="item-edit-'+$(this).attr('value')+'">test</div></li>');
					$.ajax({
						method: "POST",
						url: "/cms/cmsmenu/icreate",
						dataType : 'json',
						data: { menu_id: 1, item_type: 2, element_id: $(this).attr('value') , element_name: $(this).parent().text() }
					}).done(function( result ) {
						if(result.success) {
							var liContent = '<li class="dd-item dd3-item" data-id="'+$(this).attr('value')+'">';
							liContent += '<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>';
							liContent += '<div class="dd3-content">'+$(this).parent().text()+'<a title="Edytuj wpis" href="#" class="item-action-edit label label-warning" id="item-'+$(this).attr('value')+'">kategoria<span class="glyphicon glyphicon-triangle-bottom"></span></a></div>';
							liContent += '<div class="item-edit" id="item-edit-'+$(this).attr('value')+'">test</div>';
							liContent += '</li>';
					$("#menu-items-list").append(liContent);
							$("#menu-items-list").append(liContent);
						} else { console.log(result); }
					});

				}               
			});
		});
		
		$('#menu-selected-link').click(function(event){
		
			//console.log($(this));
			//$("#menu-items-list").append('<li class="dd-item" data-id="'+$(this).attr('value')+'"> <div class="dd-handle">'+$(this).parent().text()+'<a title="Edytuj wpis" href="#" class="item-action-edit" id="item-'+$(this).attr('value')+'">[strona]</a> </div><div class="item-edit" id="item-edit-'+$(this).attr('value')+'">test</div></li>');
			var liContent = '<li class="dd-item dd3-item" data-id="'+$(this).attr('value')+'">';
				liContent += '<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>';
				liContent += '<div class="dd3-content">'+$("#element-name").val()+'<a title="Edytuj wpis" href="#" class="item-action-edit label label-warning" id="item-'+$("#element-name").val()+'">link<span class="glyphicon glyphicon-triangle-bottom"></span></a></div>';
				liContent += '<div class="item-edit" id="item-edit-'+$("#element-name").val()+'">test</div>';
				liContent += '</li>';
			$("#menu-items-list").append(liContent);
			return false;

		});
		
		/*$('.item-action-edit').click(function(event){alert('test');
			$('.item-edit').css('display','block');
			return false;
		});*/
	},

    /*'buildItemNav' : function($items) {

        $items.each(function() {
            var $item = $(this);

            // use the selector and processor
            // from the config
            // to get the text for each item nav
            var myText = myFeature.config.itemNavProcessor(
                $item.find(myFeature.config.itemNavSelector)
            );

            $('<li/>')
            // use the new variable
            // as the text for the nav item
              .text(myText)
              .appendTo(myFeature.$item_nav)
              .data('item', $item)
              .click(myFeature.showContentItem);
      });
  },*/

	'showSection' : function() {
		// stays the same
	  },

	  'showContentItem' : function() {
		// stays the same
	  }

};




$(document).ready(function() {
	KAPI.init();
	/* AJAX FORM */
	$("#validation-errors").hide().empty(); 
	$(".alertHide").hide();
    
	
	$('.nav-tabs li.disabled').click(function() {
		return false;
	});
	
});
	
var left_side_width = 220; //Sidebar width in pixels

$(function() {
    "use strict";
    
	
	/*tinymce.init({
		selector: "textarea.tiny",
		theme: "modern",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor colorpicker textpattern"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		toolbar2: "print preview media | forecolor backcolor emoticons",
		image_advtab: true,
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		],
		file_browser_callback : function filebrowserr(field_name, url, type, win) {
     
				var fileBrowserURL = "/pdw_file_browser/index.php?editor=tinymce&filter=" + type;
				   
				tinyMCE.activeEditor.windowManager.open({
					title: "PDW File Browser",
					url: fileBrowserURL,
					width: 950,
					height: 650,
					inline: 0,
					maximizable: 1,
					close_previous: 0
				  },{
					window : win,
					input : field_name				  }
				);   
			}

	});*/


    //Enable sidebar toggle
    $("[data-toggle='offcanvas']").click(function(e) {
        e.preventDefault();

        //If window is small enough, enable sidebar push menu
        if ($(window).width() <= 992) {
            $('.row-offcanvas').toggleClass('active');
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
            $('.row-offcanvas').toggleClass("relative");
        } else {
            //Else, enable content streching
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        }
    });

    //Add hover support for touch devices
    $('.btn').bind('touchstart', function() {
        $(this).addClass('hover');
    }).bind('touchend', function() {
        $(this).removeClass('hover');
    });

    //Activate tooltips
    $("[data-toggle='tooltip']").tooltip();

    /*     
     * Add collapse and remove events to boxes
     */
    $("[data-widget='collapse']").click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!box.hasClass("collapsed-box")) {
            box.addClass("collapsed-box");
            //Convert minus into plus
            $(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            bf.slideUp();
        } else {
            box.removeClass("collapsed-box");
            //Convert plus into minus
            $(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            bf.slideDown();
        }
    });


    /*
     * INITIALIZE BUTTON TOGGLE
     * ------------------------
     */
    $('.btn-group[data-toggle="btn-toggle"]').each(function() {
        var group = $(this);
        $(this).find(".btn").click(function(e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });

    $("[data-widget='remove']").click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        box.slideUp();
    });

    /* Sidebar tree view */
    $(".sidebar .treeview").tree();

    /* 
     * Make sure that the sidebar is streched full height
     * ---------------------------------------------
     * We are gonna assign a min-height value every time the
     * wrapper gets resized and upon page load. We will use
     * Ben Alman's method for detecting the resize event.
     * 
     **/
    function _fix() {
        //Get window height and the wrapper height
        var height = $(window).height() - $("body > .header").height() - ($("body > .footer").outerHeight() || 0);
        $(".wrapper").css("min-height", height + "px");
        var content = $(".wrapper").height();
        //If the wrapper height is greater than the window
        if (content > height)
            //then set sidebar height to the wrapper
            $(".left-side, html, body").css("min-height", content + "px");
        else {
            //Otherwise, set the sidebar to the height of the window
            $(".left-side, html, body").css("min-height", height + "px");
        }
    }
    //Fire upon load
    _fix();
    //Fire when wrapper is resized
    $(".wrapper").resize(function() {
        _fix();
        fix_sidebar();
    });

    //Fix the fixed layout sidebar scroll bug
    fix_sidebar();
	
	/* gallery */
	/*var url =  '/admin/upload_handler/post';
		$('#fileupload').fileupload({
			url: url,
			dataType: 'json',
			done: function (e, data) {
				$.each(data.result.files, function (index, file) {//alert('test');
					//$('<p/>').text(file.name).appendTo('#files');
					var res = '<div id="' + 1 + '-' + 1 + '" class="photo">' + '<div class="image-preview"><img src="' + file.thumbnailUrl + '"/></div><div class="caption">';
					//if (opts.hasName)res += '<h5>' + name + '</h5>';
					//if (opts.hasDesc)res += '<p>' + description + '</p>';
					res += '</div><input type="hidden" name="order[' + 1 + ']" value="' + 1 + '"/><div class="actions">' +
                        '<span data-photo-id="' + 1 + '" class="btn btn-xs btn-danger glyphicon glyphicon-trash"></span>' +
						'</div><input type="checkbox" class="photo-select"/></div>';
							$('.images').append(res);
				});
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('#progress .progress-bar').css(
					'width',
					progress + '%'
				);
			},
			error: function(jqXHR, data, textStatus, errorThrown)  {
				// Handle errors here
				console.log('ERRORS: ' + textStatus + ', errorThrown: ' + errorThrown);
				// STOP LOADING SPINNER
			},
			fail: function (ev, data) {//alert(data);
				if (data.jqXHR) {
					console.log('Server-error:\n\n' + data.jqXHR.responseText); 
				}
			},
		}).prop('disabled', !$.support.fileInput)
		  .parent().addClass($.support.fileInput ? undefined : 'disabled');*/
 

});
function fix_sidebar() {
    //Make sure the body tag has the .fixed class
    if (!$("body").hasClass("fixed")) {
        return;
    }

    //Add slimscroll
    $(".sidebar").slimscroll({
        height: ($(window).height() - $(".header").height()) + "px",
        color: "rgba(0,0,0,0.2)"
    });
}



/*
 * SIDEBAR MENU
 * ------------
 * This is a custom plugin for the sidebar menu. It provides a tree view.
 * 
 * Usage:
 * $(".sidebar).tree();
 * 
 * Note: This plugin does not accept any options. Instead, it only requires a class
 *       added to the element that contains a sub-menu.
 *       
 * When used with the sidebar, for example, it would look something like this:
 * <ul class='sidebar-menu'>
 *      <li class="treeview active">
 *          <a href="#>Menu</a>
 *          <ul class='treeview-menu'>
 *              <li class='active'><a href=#>Level 1</a></li>
 *          </ul>
 *      </li>
 * </ul>
 * 
 * Add .active class to <li> elements if you want the menu to be open automatically
 * on page load. See above for an example.
 */
(function($) {
    "use strict";

    $.fn.tree = function() {

        return this.each(function() {
            var btn = $(this).children("a").first();
            var menu = $(this).children(".treeview-menu").first();
            var isActive = $(this).hasClass('active');

            //initialize already active menus
            if (isActive) {
                menu.show();
                btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
            }
            //Slide open or close the menu on link click
            btn.click(function(e) {
                e.preventDefault();
                if (isActive) {
                    //Slide up to close menu
                    menu.slideUp();
                    isActive = false;
                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
                    btn.parent("li").removeClass("active");
                } else {
                    //Slide down to open menu
                    menu.slideDown();
                    isActive = true;
                    btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
                    btn.parent("li").addClass("active");
                }
            });

            /* Add margins to submenu elements to give it a tree look */
            menu.find("li > a").each(function() {
                var pad = parseInt($(this).css("margin-left")) + 10;

                $(this).css({"margin-left": pad + "px"});
            });

        });

    };


}(jQuery));



/* CENTER ELEMENTS */
(function($) {
    "use strict";
    jQuery.fn.center = function(parent) {
        if (parent) {
            parent = this.parent();
        } else {
            parent = window;
        }
        this.css({
            "position": "absolute",
            "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
            "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
        });
        return this;
    }
}(jQuery));

