'use strict';

var KAPI = {
    utils: {},
    burger: {},
    sidebar: {}
};


KAPI = {
  
    'preloader': function() {
        setTimeout(function(){
            $('body').addClass('preload--off');
        }, 400);
        setTimeout(function(){
            $('body').removeClass('preload').removeClass('preload--off');
        }, 1000);
    },
    
    'customWindow': function() {
        $('.collapse-window').click(function (e){
            /*var chevState = $(e.target).siblings("i.collapse-window-icon").toggleClass('glyphicon-menu-down glyphicon-menu-up');
            $("i.collapse-window-icon").not(chevState).removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");*/
             $(this).find('i.collapse-window-icon').toggleClass('glyphicon-menu-down glyphicon-menu-up');
        });
    },
    
    'galleryManager': function() {
        var $galleryId = $("#gallery-manager-container").attr('data-id');
        var $galleryUrl = $("#gallery-manager-container").attr('data-url');
        var $galleryBehavior = $("#gallery-manager-container").attr('data-behavior');
        $.ajax({
            type: "POST",
            //url: "/files/galleryimages?id="+$galleryId,
            url: $galleryUrl,
            success: function (response) {
                $('.gallery-manager').galleryManager({
                    "hasName":true,
                    "hasDesc":true,
                    "uploadUrl":$galleryBehavior+"?type=cmsgallery&behaviorName=galleryBehavior&galleryId="+$galleryId+"&action=ajaxUpload",
                    "deleteUrl":$galleryBehavior+"?type=cmsgallery&behaviorName=galleryBehavior&galleryId="+$galleryId+"&action=delete",
                    "updateUrl":$galleryBehavior+"?type=cmsgallery&behaviorName=galleryBehavior&galleryId="+$galleryId+"&action=changeData",
                    "arrangeUrl":$galleryBehavior+"?type=cmsgallery&behaviorName=galleryBehavior&galleryId="+$galleryId+"&action=order",
                    "nameLabel":"Name",
                    "descriptionLabel":"Description",
                    "photos": response
                    //"photos":[{"id":"32","rank":"32","name":"","description":"","preview":"/panel/../uploads/gallery/4/32/preview.jpg?_=1463329037"}]
                });
                
            }
        });
    },

    'imgAttachment': function() {//console.log($('#imgAttachment').data('apiurl'));
        $('#imgAttachment').imageAttachment({
            "hasImage":$('#imgAttachment').data('hasimage'),
            "previewUrl":$('#imgAttachment').data('previewurl'),
            "previewWidth":300,
            "previewHeight":200,
            "apiUrl":$('#imgAttachment').data('apiurl')
        });
    },
    
    calendar: function() {
        $('#schedule').fullCalendar({
			defaultView: 'month', 
            timezone: "Europe/Warsaw",
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2015-02-12',
			businessHours: false, // display business hours
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			buttonText: {
				today:    'Dzisiaj',
				month:    'Miesiąc',
				week:     'Tydzień',
				day:      'Dzień'
			},
			firstDay: 1,
			minTime: "07:00:00",
			maxTime: "20:00:00",
			editable: true, 
            disableResizing: true,
            disableDragging: true,
			allDayDefault: false,
			url: true,
			agenda: 'HH:mm', 
			//events: '/task/event/scheduledata',
            events: function(start, end, timezone, callback) {
                var $ids = $('.checkbox_employee:checked').map(function() {return this.value;}).get().join(',');
                var $colors = $('.checkbox_employee:checked').map(function() { return $('label[for='+this.id+']').css('color');  }).get().join(';'); 
                $.ajax({
                    url: '/task/calendar/scheduledata',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix(),
                        typeEvent: $("#caltasksearch-type_fk").val(),
						statusEvent: $("#caltasksearch-id_dict_task_status_fk").val(),
						department: $("#caltasksearch-id_department_fk").val(),
						employee: $("#caltasksearch-id_employee_fk").val(),
                        branch: $("#caltasksearch-id_company_branch_fk").val(),
                        employees:  $ids,
                        colors: $colors
                    },
                    success: function(doc) {
                        var events = [];
                        $(doc).each(function() {
                            events.push({
                                id: $(this).attr('id'),
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
								backgroundColor: $(this).attr('backgroundColor'),
								borderColor: $(this).attr('backgroundColor'),
								eventColor: $(this).attr('backgroundColor'),
								eventBorderColor: $(this).attr('backgroundColor'),
                                allDay :  $(this).attr('allDay'),
                                icon: $(this).attr('icon')
                            });
                        });
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element) {
                //if(event.icon){          
                    element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i>");
                //}
                 //eventElement.attr('title', 'test');
                element.tooltip({title: event.title, container: "body", html: true});  
                /*element.tooltip({   
                    //selector: "[data-toggle='tooltip']",
                    //container: "body",
                    title: event.description,
                    html: true
                });*/
            },
			/*eventMouseover: function(calEvent, jsEvent) { 
				var tooltip = '<div class="tooltipevent"> ' + calEvent.title + '</div>'; 
				var $tool = $(tooltip).appendTo('body');
                $(this).mouseover(function(e) {
                    $(this).css('z-index', 10000);
                            $tool.fadeIn('500');
                            $tool.fadeTo('10', 1.9);
                }).mousemove(function(e) {
                    $tool.css('top', e.pageY + 10);
                    $tool.css('left', e.pageX + 20);
                });
                },
            eventMouseout: function(calEvent, jsEvent) {
                $(this).css('z-index', 8);
                $('.tooltipevent').remove();
            },*/
			weekends: true,
			allDaySlot : true,
			selectable: true,
			selectHelper: true,
			slotDuration: "00:30:00",
			disableDragging: true,
			disableResizing: true,
			monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
			monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
			dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek','Sobota'],
			dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
			select: function(start, end, allDay) {
				$target = $("#modal-grid-item");
                var $action = '/task/event/new/0?start='+start.unix()+'&end='+end.unix();
                /*$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zdarzenie');
                $("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/createajax/0?start='+start.unix()+'&end='+end.unix());*/
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowy');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
		
				return false;
			},
			eventClick: function(calEvent, jsEvent, view) { 
				//console.log(calEvent.id);	
				//var idArr = calEvent.id.split('-');
				$target = $("#modal-grid-item");
				$target.find(".modal-title").html('<i class="fa fa-eye"></i>Podgląd');
				$target.find(".modalContent").addClass('preload');
                /*$("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/showajax/'+calEvent.id);*/
			    $target.find(".modalContent")
                    .load('/task/event/showajax/'+calEvent.id, 
                        function(response, status, xhr){ 
                            $target.modal('show'); 

                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 300);
                   });   
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
				
				$('#myModal').on('hidden.bs.modal', function(){
					$(this).removeData('bs.modal');
				});
				return false;
									  
			}
		});
    },
	'cropieInit': function() {
        if($("#upload-demo").length) {

            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 100,
                    height: 100,
                    type: 'squere'
                },
                boundary: {
                    width: 500,
                    height: 360
                },
          
            });

            $('#upload').on('change', function () { 
                readFile(this); 
            });
                                      
            $('.upload-result').on('click', function (e) { 
                e.preventDefault();
                e.stopPropagation();
                
                var $urlPath = $(this).attr("data-path");
                
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (resp) {
                    /*popupResult({
                        src: resp
                    });*/
                    //console.log(resp);
                    if($("#upload-demo").data('avtr') == 1) {
                        $(".stat-people").attr('src',resp);
                        $(".avtr").attr('src',resp);
                    }
                    
                    var img = {avatar: resp };
                    $.ajax({
                        type: 'post',
                        url: $urlPath,
                        data:  {avatar: resp },
                        dataType: "json",
                        async: false,
                        cache: false,
                        /*contentType: false,
                        processData: false,*/
                        success: function (data) {
                            console.log(data);
                        },
                    });
                    
                });
                return false;
            });
            
            $('.change-view').on('click', function (e) { 
                e.preventDefault();
                e.stopPropagation();
                
                $uploadCrop.croppie({
                    enableExif: true,
                    viewport: {
                        width: 200,
                        height: 260,
                        type: 'squere'
                    },
                    boundary: {
                        width: 500,
                        height: 360
                    },
              
                });
            
                return false;
            }); 
            //console.log($("#upload-demo-id").attr('data-id'));
            if($("#upload-demo-id").attr('data-id')*1 != 0) {
                 $uploadCrop.croppie('bind', {
                    url: '/uploads/users/avatars/avatar-'+$("#upload-demo-id").attr('data-id')+'.jpg'
                });
            }
            
        }
    },
    'tinyMceFull': function() {
        tinymce.init({
            selector: '.mce-full',
            height: 300,
            language_url: '/js/langs/pl.js',
            convert_urls: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code',
                'textcolor colorpicker'
            ],
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
                //'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                // '//www.tinymce.com/css/codepen.min.css'
            ],
            file_browser_callback: function myFileBrowser(field, url, type, win) {
                //if(type == "image") {
                    tinyMCE.activeEditor.windowManager.open({
                        file: "/filebrowser/browse.php?opener=tinymce4&field=" + field + "&type=" + type,
                        title: "Media ",
                        width: 700,
                        height: 500,
                        inline: true,
                        close_previous: false
                    }, {
                        window: win,
                        input: field
                    }); 
                //}
                
                return false;			
            },
            /*init_instance_callback: function(editor) {
                alert('tinymce init: '+editor.id);
            }*/
        });
    },
	
	'tinyMceBasic': function() {
        tinymce.init({
            selector: '.mce-basic',
            height: 150,
            language_url: '/js/langs/pl.js',
            theme_url: '/js/themes/modern/theme.min.js',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code',
                'textcolor colorpicker'
          ],
          toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
          content_css: [
            //'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            //'//www.tinymce.com/css/codepen.min.css'
          ],
          file_browser_callback: function myFileBrowser(field, url, type, win) {
                            if(type == "image") {
                                tinyMCE.activeEditor.windowManager.open({
                                    file: "/filebrowser/browse.php?opener=tinymce4&field=" + field + "&type=" + type,
                                    title: "Media ",
                                    width: 700,
                                    height: 500,
                                    inline: true,
                                    close_previous: false
                                }, {
                                    window: win,
                                    input: field
                                }); 
                            }
                        return false;			
                        }
        });
    },
    
    'sendFormData' : function() {
        $(document).on("submit", "form.ajaxform", function(event) {
			event.stopImmediatePropagation();
            event.preventDefault();
            
            var $formClass = 'ajaxform', $table = false;
            if( $(this).data('form') ) $formClass = $(this).data('form');
			if( $(this).data('table') ) $table = $(this).data('table');

            $.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $(this).serialize(),
				url: $(this).attr('action'),
				beforeSend: function() { 
					$(".alert").remove(); 
				},
				success: function(data) {
					if(data.success == false) {
						var arr = data.errors; var txtError = '';
                        
                        txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        $.each(arr, function(index, value)  { if (value.length != 0)   { txtError += '<strong>'+ value +'</strong><br/>';  }  });
                        var unix = Math.round(+new Date()/1000);
                        $("."+$formClass).prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 100000);
					} else {
						if(data.alert) {
                            var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        } else	{
                            var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        }
                        var unix = Math.round(+new Date()/1000);
                        $("."+$formClass).prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
                        if(data.action) {
                            window.location.replace(data.action);
                        }
						
						if($table) {							
							$($table).bootstrapTable('refresh');
						}
               
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
					}
				},
				error: function(xhr, textStatus, thrownError) {
					alert('Something went to wrong.Please Try again later...');
				}
			});
			return false;
		});
    },
	
};
