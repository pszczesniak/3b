'use strict';

KAPI.recipe = function() {
};



//$(document).on("click", ".modal-grid-btn", function (event) {
$(document).on("submit", ".modal-grid-field", function (event) {
	var $form = $(this); //$($(this).attr('data-form'));
	var $target = $($form.attr("data-target"));
	var $table = $($(this).attr('data-table'));
	var $rowsCounter = $("#table-fields tbody tr").length;/* $table.bootstrapTable('getOptions').totalRows;*/
	
    $.ajax({
		type: $form.attr("method"),
		url: $form.attr("action")/*+"/"+($rowsCounter-1)*/,
		data: $form.serialize(),

		success: function(data, status) {
            if(data.result) {
				$table.bootstrapTable('refresh');
				$target.modal("hide");
					/*$row = '<tr><td>test'+$rowsCounter+'</td><td>test</td><td>test</td><td>test</td><td><button class="gridViewModalMaterialDelete" value="delete">delete</button></td></tr>';
					$table.append($row);*/
		
			} else { 
				$target.find(".modalContent").html(data.html);
                $target.modal("hide");
			}
		}
	});
    
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});


$(document).ready( function() {
    $('#modal-grid-item').on('shown.bs.modal', function () {
        $( "#shopcategoryfactor-value_type" ).change( function() { 
            if( $(this).val() == 3 || $(this).val() == 4) {
                $("#shopcategoryfactor-options").removeClass('hide').addClass('display-block');
            } else {
                $("#shopcategoryfactor-options").addClass('hide');
            }
        });
    });
    
});


window.actionEvents = {
	/*'click .gridViewModalFieldDelete': function (e, value, row, index) {
		//alert('You click like icon, row: ' + JSON.stringify(row));
		//$("#table-materials tbody tr").eq(index).remove();
		//document.getElementById("table-materials").deleteRow(index);
	
		
		$("#table-fields").bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});
		$weight = $("#lsddrecipe-recipe_weight").val()*1; $("#lsddrecipe-recipe_weight").val($weight-row.amount*1);
		$cost = $("#lsddrecipe-recipe_cost").val()*1; $("#lsddrecipe-recipe_cost").val($cost-row.cost*1);
		$cost_2 = $("#lsddrecipe-recipe_cost_2").val()*1; $("#lsddrecipe-recipe_cost_2").val($cost_2-row.cost_2*1);
		$("#recipe-matarial-info").removeClass().addClass('alert alert-warning').html('Zapisz zmiany');
		if($weight-row.amount*1 == 1000) $("#save-recipe-material").attr("disabled",false); else $("#save-recipe-material").attr("disabled","disabled");
		
	},*/
	/*'click .gridViewModalField': function (e, value, row, index) {

		var $target = $(e.target);
		var $modal = $($target.attr('data-target'));

		var ShopCategoryFactor = {id_category_fk: row.categoryId, name: row.name, value_type: row.value_type, isUpdate: 1, id: row.id };
		$.ajax({
			type: 'post',
			url: $target.attr("data-href"),
            data: { ShopCategoryFactor },
			success: function(data, status) {
				if(data.success) {

					$modal.find(".modalContent").html(data.html);
					$modal.modal("show");
					
				} else { 
					$modal.find(".modalContent").html(data.html);
					$modal.modal("show");
				}
			}
		});
	},*/
	'click .up': function (e, value, row, index) {console.log('up');
        var source = JSON.stringify($('#table-fields').bootstrapTable('getData')[index]);
        var target = JSON.stringify($('#table-fields').bootstrapTable('getData')[index - 1]);
        $('#table-fields').bootstrapTable('updateRow', {'index':index - 1, 'row': JSON.parse(source)});
        $('#table-fields').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
	},
	'click .down': function (e, value, row, index) {
        var source = JSON.stringify($('#table-materials').bootstrapTable('getData')[index]);
        var target = JSON.stringify($('#table-materials').bootstrapTable('getData')[index + 1]);
        $('#table-fields').bootstrapTable('updateRow', {'index':index + 1, 'row': JSON.parse(source)});
        $('#table-fields').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
	}
};