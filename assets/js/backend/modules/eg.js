'use strict';

KAPI.calendar = function() {

	$('#calendar').fullCalendar({
		defaultView: 'agendaWeek', 
		theme: false,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		//defaultDate: '2015-02-12',
		businessHours: false, // display business hours
		timeFormat: 'HH:mm',
		axisFormat: 'HH:mm',
		buttonText: {
			today:    'Dzisiaj',
			month:    'Miesiąc',
			week:     'Tydzień',
			day:      'Dzień'
		},
		firstDay: 1,
		minTime: "06:00:00",
		maxTime: "22:00:00",
		editable: true, 
		allDayDefault: false,
		url: true,
		agenda: 'HH:mm', 
		events: '/panel/jx/eg/egvisit/scheduledata',
		/*events: function(start, end, timezone, callback) {
			$.ajax({
				url: '/eg/egvisit/scheduledata',
				dataType: 'json',
				data: {
					// our hypothetical feed requires UNIX timestamps
					start: start.unix(),
					end: end.unix()
				},
				success: function(doc) {
					var events = [];
					$(doc).find('event').each(function() {
						events.push({
							title: $(this).attr('title'),
							start: $(this).attr('start') // will be parsed
						});
					});
					callback(events);
				}
			});
		},*/
		data: {
			start:  moment( $("#calendar").fullCalendar("getView").start ).format("YYYY-MM-DD"),
			end:    moment( $("#calendar").fullCalendar("getView").end ).format("YYYY-MM-DD")
		},
		weekends: false,
		allDaySlot : false,
		selectable: true,
		selectHelper: true,
		slotDuration: "00:15:00",
		disableDragging: true,
		disableResizing: true,
		monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
		monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
		dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwratek', 'Piatek','Sobota'],
		dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
		eventRender: function(event, eventElement) {
			if (event.imageurl) {
				eventElement.find("div.fc-time").append("<img class='event-icon-edit' src='" + event.imageurl +"' width='12' height='12'>");
			}
			if (event.confirmClass) {
				eventElement.find("div.fc-content").addClass(event.confirmClass);
			}
		},
		select: function(start, end, allDay) {
			$('#myModal').modal({
				keyboard: false,
				backdrop: true,
				remote: '/panel/jx/eg/egvisit/createvisitform/1/'+moment(start ).format("YYYY-MM-DD_HH:mm")
				//moment( $("#calendar").fullCalendar("getView").start ).format("YYYY-MM-DD HH:mm") 
			});
			
			$('#myModal').on('shown.bs.modal', function (event) {
				var action;
				$(".number-spinner button").mousedown(function () { console.log('up');
					btn = $(this);
					input = btn.closest('.number-spinner').find('input');
					//input = $("input#egvisit-visit_duration");
					btn.closest('.number-spinner').find('button').prop("disabled", false);

					if (btn.attr('data-dir') == 'up') { 
						action = setInterval(function(){
							if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(360) ) {
								input.val(parseInt(input.val())+15);
							}else{
								btn.prop("disabled", true);
								clearInterval(action);
							}
						}, 70);
					} else {
						action = setInterval(function(){
							if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(15) ) {
								input.val(parseInt(input.val())-15);
							}else{
								btn.prop("disabled", true);
								clearInterval(action);
							}
						}, 70);
					}
				}).mouseup(function(){
					clearInterval(action);
				});
			});
			
			$('#myModal').on('hidden.bs.modal', function(){
				$(this).removeData('bs.modal');
			});
			
			
			/*$('#myModal').on('show.bs.modal', function (event) {
			  var button = $(event.relatedTarget) // Button that triggered the modal
			  var recipient = button.data('whatever') // Extract info from data-* attributes
			  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			  var modal = $(this)
			  //modal.find('.modal-title').text('New message to ' + recipient)
			  //modal.find('.modal-body input').val(recipient)
			})*/
			 
				return false;
		},
		/*dayClick: function(date, allDay, jsEvent, view) {alert('dayClick');
			if (allDay) {
				var url = "/eg/visit/createVisit/term/"+$.fullCalendar.formatDate(date,'yyyy-MM-dd H:mm');
			}else{
				var url = "/eg/visit/createVisit/term/"+$.fullCalendar.formatDate(date,'yyyy-MM-dd H:mm');
			}
			return false;
		},*/

		eventClick: function(calEvent, jsEvent, view) { 
			//console.log(calEvent.id);	
			var idArr = calEvent.id.split('-');
			if(idArr[0] == "fc") {
				$('#myModal').modal({
					keyboard: false,
					backdrop: true,
					remote: '/panel/jx/eg/egvisit/visitform/1'
					//moment( $("#calendar").fullCalendar("getView").start ).format("YYYY-MM-DD HH:mm") 
				});
				$('#myModal').find('.modal-title').text('Rezerwacja wolnego terminu');
			} else {
				$('#myModal').modal({
					keyboard: false,
					backdrop: true,
					remote: '/panel/jx/eg/egvisit/visitform/1'
					//moment( $("#calendar").fullCalendar("getView").start ).format("YYYY-MM-DD HH:mm") 
				});
				$('#myModal').find('.modal-title').text('Nowa wizyta');
			}
			/*$('#myModal').on('shown.bs.modal', function (event) {
				var action;
				$(".number-spinner button").mousedown(function () {
					btn = $(this);
					input = btn.closest('.number-spinner').find('input');
					//input = $("input#egvisit-visit_duration");
					btn.closest('.number-spinner').find('button').prop("disabled", false);

					if (btn.attr('data-dir') == 'up') {
						action = setInterval(function(){
							if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(360) ) {
								input.val(parseInt(input.val())+15);
							}else{
								btn.prop("disabled", true);
								clearInterval(action);
							}
						}, 70);
					} else {
						action = setInterval(function(){
							if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(15) ) {
								input.val(parseInt(input.val())-15);
							}else{
								btn.prop("disabled", true);
								clearInterval(action);
							}
						}, 70);
					}
				}).mouseup(function(){
					clearInterval(action);
				});
			});*/
			
			$('#myModal').on('hidden.bs.modal', function(){
				$(this).removeData('bs.modal');
			});
			return false;
								  
		}
	});
	
};
	
KAPI.schedule = function() {
	
	/*$('#schedule').contextmenu({
		delegate: ".hasmenu",
		preventContextMenuForPopup: true,
		preventSelect: true,
		menu: [
				{title: "Cut", cmd: "cut", uiIcon: "ui-icon-scissors"},
				{title: "Copy", cmd: "copy", uiIcon: "ui-icon-copy"},
				{title: "Paste", cmd: "paste", uiIcon: "ui-icon-clipboard", disabled: true },
			],
		select: function(event, ui) {
				
			// Logic for handing the selected option
		},
		beforeOpen: function(event, ui) {
			$.proxy(this.doStuff, this); console.log('contextmenu');
			// Things to happen right before the menu pops up
		}
	});*/
	
	$("#schedule").mousedown(function (e) {
		if (e.button === 2) {
			
			console.log("down: "+$(e.target).parents(".fc-event").length);
			if($(e.target).parents(".fc-event").length > 0) return;

			var newEvent = $.extend($.Event("mousedown"), {
				which: 1,
				clientX: e.clientX,
				clientY: e.clientY,
				pageX: e.pageX,
				pageY: e.pageY,
				screenX: e.screenX,
				screenY: e.screenY

			});
			$(e.target).trigger(newEvent);

		}else if(e.button===1){
		$('#context-menu').removeClass('open')
		}
		if(e.button===0){

		$('#context-menu').removeClass('open');
		}
	});

	$("#schedule").mouseup(function (e) {
		if(e.button===0){

		$('#context-menu').removeClass('open');
		}
		
		if (e.button === 2) {
			console.log("up");
			if(!$(e.target).parents(".fc-event").length > 0) return;

			var newEvent = $.extend($.Event("click"), {
				which: 1,
				clientX: e.clientX,
				clientY: e.clientY,
				pageX: e.pageX,
				pageY: e.pageY,
				screenX: e.screenX,
				screenY: e.screenY

			});
			$(e.target).trigger(newEvent);

		}
	   
	});
	
	$('#schedule').fullCalendar({
		defaultView: 'agendaWeek', 
		theme: false,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'agendaWeek'
		},
		//defaultDate: '2015-02-12',
		//businessHours: true, // display business hours
		timeFormat: 'HH:mm',
		axisFormat: 'HH:mm',
		buttonText: {
			today:    'Dzisiaj',
			month:    'Miesiąc',
			week:     'Tydzień',
			day:      'Dzień'
		},
		firstDay: 1,
		minTime: "06:00:00",
		maxTime: "22:00:00",
		editable: true, 
		allDayDefault: false,
		url: true,
		agenda: 'HH:mm', 
		//events: '/eg/egvisit/scheduledata',
		weekends: false,
		allDaySlot : false,
		selectable: true,
		selectHelper: false,
		slotDuration: "00:30:00",
		slotEventOverlap: true,
		disableDragging: true,
		disableResizing: true,
		monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
		monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
		dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwratek', 'Piatek','Sobota'],
		dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
		next: function(){console.log('next');
			$('#schedule').load();
		},
		events: function(start, end, timezone, callback) {
			$.ajax({
				url: '/eg/egvisitterm/scheduleclear',
				dataType: 'xml',
				data: {
					// our hypothetical feed requires UNIX timestamps
					start: start.unix(),
					end: end.unix()
				},
				success: function(doc) {
					var events = [];
					$(doc).find('event').each(function() {
						events.push({
							title: $(this).attr('title'),
							start: $(this).attr('start') // will be parsed
						});
					});
					callback(events);
				}
			});
		},
		 eventResize: function(event, delta, revertFunc) {

			//if (!confirm("is this okay?")) {
			if(event.end.format().split('T')[0] != event.start.format().split('T')[0]) {
				revertFunc();
			}

		},
		eventRender: function(event, eventElement) {
			var originalClass = eventElement[0].className;
			eventElement[0].className = originalClass + ' hasmenu';
			
			//if (event.imageurl) {
				eventElement.find("div.fc-time").append("<img class='event-icon-delete' src='/img/trash-light.png' width='12' height='12' title='Usuń element' >");
				eventElement.find(".event-icon-delete").click(function() {
				   $('#schedule').fullCalendar('removeEvents',event._id);
				});
			//}
			/*if (event.confirmClass) {
				eventElement.find("div.fc-content").addClass(event.confirmClass);
			}*/
		},
		dayRender: function (day, cell) {
			var originalClass = cell[0].className;
			cell[0].className = originalClass + ' hasmenu';
		},
		select: function(start, end, allDay) {
			/*$('#visitDialog').html('<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/blue_loader.gif" alt="loading"/> </div>').load('/eg/visit/createVisit/type/1/day/'+$.fullCalendar.formatDate(start,'yyyy-MM-dd')+'/time/'+$.fullCalendar.formatDate(start,'HH:mm')).dialog({
					autoOpen: false,
					width: 600,
					height: 460,
					modal: true,
					complate : function(r) {
						$("#loading").css('display','none');
						$("#modal-body").css('display','block');
					},
				});

				$('#visitDialog').dialog('open');
			 *///alert('select');
				return false;
		},
		dayClick: function(date, allDay, jsEvent, view) {
			/*var a=	document.getElementById('location');
				a.value=date.toDateString();

			if(view.name!='month'){	
				a.value+=" " + date.toLocaleTimeString();
			}
			var e = document.getElementById("Place");
			var strUser = e.options[e.selectedIndex].value;
			
			a.value+=" at "+strUser;*/
			//alert('day click');
			return false;
		},

		eventClick: function(event, jsEvent, view) { 
			
				/*$('#visitDialog').load('/eg/visit/viewVisit/id/'+calEvent.id).dialog({
							autoOpen: false,
							width: 600,
							height: 500,
							modal: true,
							title: 'Szczegóły wizyty',
							complete : function(r) {
								$("#loading").css('display','none');
								$("#modal-body").css('display','block');
							},
						});

						$('#visitDialog').dialog('open');*/
				//alert('eventClick');
						return false;
								  
		},
		droppable: true, // this allows things to be dropped onto the calendar !!!
		drop: function(date) { // this function is called when something is dropped
			// retrieve the dropped element's stored Event Object
			var originalEventObject = $(this).data('eventObject');
			// we need to copy it, so that multiple events don't have a reference to the same object
			var copiedEventObject = $.extend({}, originalEventObject);
			// assign it the date that was reported
			copiedEventObject.start = date;
			//copiedEventObject.backgroundColor ="#0000FF";
			
			// render the event on the calendar
			// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
			$('#schedule').fullCalendar('renderEvent', copiedEventObject, true);
			// is the "remove after drop" checkbox checked?
			if ($('#drop-remove').is(':checked')) {
			// if so, remove the element from the "Draggable Events" list
				$(this).remove();
			}
		}
	});
	
	 /*$('.fc-next-button').click(function(){
		 var date1 = $('#schedule').fullCalendar('next');
		 console.log(date1);
		 return false;
	  });*/
	
	$('#external-events div.external-event').each(function() {
		// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		// it doesn't need to have a start or end
		var eventObject = {
			title: $.trim($(this).text()),
			backgroundColor: $.trim($(this).attr("alt")),
			borderColor: $.trim($(this).attr("alt")),
			className: $.trim($(this).attr("id"))
		};
		// store the Event Object in the DOM element so we can get to it later
		$(this).data('eventObject', eventObject);
		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true, // will cause the event to go back to its
			revertDuration: 0 // original position after the drag
		});
	});
	
	$('.load-working-hours').click(function() {
		var momentStart = $('#schedule').fullCalendar('getView').start;
		$( "div.external-event" ).each(function( index, element ) {
			// element == this
			var id = $( element ).attr('id').split('-')[1];
			var eventObject = {
				title: $.trim($( element ).text()),
				backgroundColor: $.trim($( element ).attr("alt")),
				borderColor: $.trim($( element ).attr("alt")),
				className: $.trim($( element ).attr("id"))
			};
			
			//var dataCalendar = $( element ).data('calendar');
			//console.log(dataCalendar);
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: {_csrf : $('meta[name="csrf-token"]').attr("content")},
				//url: 'eg/egstaff/saveworkinghours?id='+$(this).attr('id').split('-')[1],
				'url': '/eg/egstaff/getworkinghours?id='+id,
				/*beforeSend: function() { 
					$("#validation-errors").hide().empty(); 
				},*/
				success: function(data) {
											//var momentEnd = $('#schedule').fullCalendar('getView').end;
					//console.log("The current date of the calendar is " + momentStart.format()+', ' + momentEnd.format());
					if(data.result != false) {
						var doctorSchedule = JSON.parse(data.result);
						for(var i=0; i<doctorSchedule.length;++i) {
							if(doctorSchedule[i].state == true) {
								var dayDate = momentStart.add(i,'days');
								eventObject.start = dayDate.format()+'T'+doctorSchedule[i].start+':00'; 
								eventObject.end = dayDate.format()+'T'+doctorSchedule[i].end+':00'; 
								$('#schedule').fullCalendar('renderEvent', eventObject, true);
								console.log(i+': '+eventObject.title+' => '+eventObject.start);
								var dayDate = momentStart.add(-1*i,'days');
							}
						} 
					}
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					alert('Something went to wrong.Please Try again later...');
				}
			});
		});
	});

	$('.generate-free-time').click(function() {
		var allEvents = $('#schedule').fullCalendar( 'clientEvents' ); var li = allEvents.length;
		
		var allEventsArr = [];
		
		for (var i = 0; i < li; i++) {
			//console.log(allEvents[i].end);
			var o = {};
			o.doctorId = allEvents[i].className[0].split('-')[1];
			o.start = allEvents[i].start;
			o.end = allEvents[i].end;
			allEventsArr.push(o);
		}
		$.ajax({
			type: 'post',
			cache: false,
			dataType: 'json',
			data: {_csrf : $('meta[name="csrf-token"]').attr("content"), events: JSON.stringify(allEventsArr)},
			//url: 'eg/egstaff/saveworkinghours?id='+$(this).attr('id').split('-')[1],
			'url': '/eg/egvisitterm/generatefreetime',
			/*beforeSend: function() { 
				$("#validation-errors").hide().empty(); 
			},*/
			success: function(data) {
										//var momentEnd = $('#schedule').fullCalendar('getView').end;
				//console.log("The current date of the calendar is " + momentStart.format()+', ' + momentEnd.format());
				console.log('Terminy wizyt zostały wygenerowane');
			},
			error: function(xhr, textStatus, thrownError) {
				console.log(xhr);
				alert('Something went to wrong.Please Try again later...');
			}
		});
	});
};

KAPI.staffWorkingHours = function() {
	//$('input[type="checkbox"]').change(function () {
	$('.onoffswitch-checkbox').change(function () {
		var id = $(this).attr('id').split('_');
		var check = $(this).prop('checked');
		/*console.log("Change: " + id[2] + " to " + check);*/
		if(check) {
			$("li#wh-"+id[2]).addClass('active');
			$('select#hour-start-'+id[2]).prop('disabled', false);
			$('select#hour-end-'+id[2]).prop('disabled', false);
		} else {
			$("li#wh-"+id[2]).removeClass('active');
			$('select#hour-start-'+id[2]).prop('disabled', true);
			$('select#hour-end-'+id[2]).prop('disabled', true);
		}
		
	});
	$('.btn-working-hours').click(function() {
		var jsonArr = [];

		for (var i = 0; i < 7; i++) {
			jsonArr.push({
				name: $("li#wh-"+i+" > label > span").text(),
				state: $('#wh_day_'+i).prop('checked'),
				start: $('select#hour-start-'+i).val(),
				end: $('select#hour-end-'+i).val()
			});
		}
		//console.log(jsonArr);
		$.ajax({
			type: 'post',
			cache: false,
			dataType: 'json',
			data: {'wh': JSON.stringify(jsonArr), _csrf : $('meta[name="csrf-token"]').attr("content")},
			//url: 'eg/egstaff/saveworkinghours?id='+$(this).attr('id').split('-')[1],
			'url': $(this).attr('data-url'),
			/*beforeSend: function() { 
				$("#validation-errors").hide().empty(); 
			},*/
			success: function(data) {
				if(data.success == false)
				{
					alert('error');
				} else {
					alert('ok');
				}
			},
			error: function(xhr, textStatus, thrownError) {
				console.log(xhr);
				alert('Something went to wrong.Please Try again later...');
			}
		});
	});
};
	
	
KAPI.showSection = function() {
	// stays the same
  };

KAPI.showContentItem = function() {
	// stays the same
  };


