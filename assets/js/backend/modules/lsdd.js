'use strict';

KAPI.recipe = function() {
};





//$(document).on("click", ".modal-grid-btn", function (event) {
$(document).on("submit", ".modal-grid-material", function (event) {
	var $form = $(this); //$($(this).attr('data-form'));
	var $target = $($form.attr("data-target"));
	var $table = $($(this).attr('data-table'));
	var $rowsCounter = $("#table-materials tbody tr").length;/* $table.bootstrapTable('getOptions').totalRows;*/
	
    $.ajax({
		type: $form.attr("method"),
		url: $form.attr("action")/*+"/"+($rowsCounter-1)*/,
		data: $form.serialize(),

		success: function(data, status) {
            if(data.result) {
				/*$table.bootstrapTable('refresh');*/
				var $rowObj = {
							name: data.data.name,
							amount: data.data.amount,
							cost: data.data.cost,
							action: data.data.action,
							move: data.data.move,
							id: data.data.id,
							material: data.data.material,
							cost_2: data.data.cost_alternative
						};
				if(data.index == -1) {
					
					$table.bootstrapTable('insertRow', {
						index: $rowsCounter,
						row: $rowObj
					});
										
					$weight = $("#lsddrecipe-recipe_weight").val()*1; $("#lsddrecipe-recipe_weight").val($weight+data.data.amount*1);
					$cost = $("#lsddrecipe-recipe_cost").val()*1; $("#lsddrecipe-recipe_cost").val($cost+data.data.cost*1);
					$cost_2 = $("#lsddrecipe-recipe_cost_2").val()*1; $("#lsddrecipe-recipe_cost_2").val($cost_2+data.data.cost_alternative*1);
					$("#recipe-matarial-info").removeClass('alert-info').addClass('alert-warning').html('Zapisz zmiany');
					if($weight+data.data.amount*1 == 1000) $("#save-recipe-material").attr("disabled",false); else $("#save-recipe-material").attr("disabled","disabled");
					/*$row = '<tr><td>test'+$rowsCounter+'</td><td>test</td><td>test</td><td>test</td><td><button class="gridViewModalMaterialDelete" value="delete">delete</button></td></tr>';
					$table.append($row);*/
				} else {
					//$editRow = $table.bootstrapTable('getRowByUniqueId', data.data.id); console.log(data.data.id);console.log($editRow);
					var $editRow = $table.bootstrapTable('getRowByUniqueId', data.data.id);
					$weight = $("#lsddrecipe-recipe_weight").val()*1-$editRow.amount*1; 
					$cost = $("#lsddrecipe-recipe_cost").val()*1-$editRow.cost*1; 
					$cost_2 = $("#lsddrecipe-recipe_cost_2").val()*1-$editRow.cost_2*1; 
					
					$table.bootstrapTable('updateRow', {
						index: data.index,
						row: $rowObj
					});
					$("#lsddrecipe-recipe_weight").val($weight+data.data.amount*1);
					$("#lsddrecipe-recipe_cost").val($cost+data.data.cost*1);
					$("#lsddrecipe-recipe_cost_2").val($cost_2+data.data.cost_alternative*1);
					$("#recipe-matarial-info").removeClass('alert-info').addClass('alert-warning').html('Zapisz zmiany');
					if($weight*1+data.data.amount*1 == 1000) $("#save-recipe-material").attr("disabled",false); else $("#save-recipe-material").attr("disabled","disabled");
				}
				$target.modal("hide");
				$rowObj = {};
			} else { 
				$target.find(".modalContent").html(data.html);
			}
		}
	});
    
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});

$(document).on("click", ".gridViewModalMaterialDeletee", function (event) {
	event.stopImmediatePropagation();
	event.preventDefault();
	var $table = $($(this).attr('data-table'));

	var $killrow = $(this).closest( 'tr');
		$killrow.addClass("bg-danger");
	$killrow.fadeOut(1000, function(){
		$(this).closest( 'tr').remove();
	});

	return false;
});
	


$(document).ready(function() {

	/* production */
	$('#table-production-recipies').on('check.bs.table', function (e, row, $el) {
    	/*alert('check index: ' + $el.closest('tr').data('index'));*/
		var $row =  $el.closest('tr');   
		var $recipe = '<li id="'+$row.data('uniqueid')+'" class="list-group-item">'+$row.find('td:nth-child(2)').text()+'<input type="text" name="Items[item-'+$row.data('uniqueid')+']" value="0"></li>';
		$("#items-counter").val($("#items-counter").val()*1+1);
		$("#list-recipies").append($recipe);
    });
    $('#table-production-recipies').on('uncheck.bs.table', function (e, row, $el) {
    	//alert('uncheck index: ' + $el.closest('tr').data('index'));
		var $row =  $el.closest('tr');
		$("#items-counter").val($("#items-counter").val()*1-1);
		$("#list-recipies li#"+$row.data('uniqueid')).remove();
    });
	$.fn.editable.defaults.mode = 'inline';
	$.fn.editable.defaults.showbuttons = false;
	$.fn.editable.defaults.url = '/post';
	$.fn.editable.defaults.type = 'text';
	//$(".production-edit-amount").editable();
	/*$("#production-status").on('click', function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		
		$(".production-edit-amount").editable('option', 'disabled', true);
		return false;
	})*/
	$('#production-status').click(function(event) {
       $('.editable').editable('toggleDisabled');
	   
	    event.stopImmediatePropagation();
		event.preventDefault();
		return false;
   }); 
	/* end production*/
	
	/* multiselect structure of zone */
	$("#zone-structure").treeMultiselect({
		// Sections have checkboxes which when checked, check everything within them
		allowBatchSelection: false,
		// Selected options can be sorted by dragging 
		// Requires jQuery UI
		sortable: false,
		// Adds collapsibility to sections
		collapsible: true,
		// Disables selection/deselection of options; aka display-only
		freeze: false,
		// Hide the right panel showing all the selected items
		hideSidePanel: false,
		// Only sections can be checked, not individual items
		onlyBatchSelection: false,
		// Separator between sections in the select option data-section attribute
		sectionDelimiter: '/',
		// Show section name on the selected items
		showSectionOnSelected: true,
		// Activated only if collapsible is true; sections are collapsed initially
		startCollapsed: false,
	  // Callback
	  onChange: null	  
	});
    
	$("#lsddzone-alternative_currency_list").chosen({disable_search_threshold:10});
	/* materials price */
	//$("#material-price-change").click(function(event) {
	//$( "#toolbar-material" ).delegate( "#material-price-change", "click", function(event) {
	$( "#material-price-change" ).on( "click", function(event) {
		var self = this;
		var $current = $(self).attr("data-current");
		
		if($current == "actual") {
			//console.log('actual => '+$current+": "+$(self).data("forecast-label"));
			$(self).html('<i class="glyphicon glyphicon-resize-horizontal"></i>'+$(self).attr("data-actual-label"));
			$("#price-option").text($(self).attr("data-forecast-label"));
			$(self).attr('data-current', "forecast");
		} else {
			//console.log('forecast => '+$current+": "+$(self).data("actual-label"));
			$(self).html('<i class="glyphicon glyphicon-resize-horizontal"></i>'+$(self).attr("data-forecast-label"));
			$("#price-option").text($(self).attr("data-actual-label"));
			$(self).attr('data-current', "actual");
		}
		
		/*event.stopImmediatePropagation();
		event.preventDefault();*/
		return false;
	});
	
	//$( "#company-recipe" ).on( "change", function(event) {
	$( 'body' ).delegate( "#company-recipe", "change", function() {
		$.post( "/panel/pl/lsdd/lsddcompanyrecipe/listpackages?id="+$(this).val(), function( data ) {
		  $( "select#company-recipe-package" ).html( data );
		});
	});
	
	$( 'body' ).delegate( "#config-zone", "change", function() {
		$.post( "/panel/pl/lsdd/lsddzone/listcurrency?id="+$(this).val(), function( data ) {
		  $( "select#config-currency" ).html( data );
		});
	});
	
	/*$("#table-materials tbody").sortable({ helper: fixHelperModified  }).disableSelection();*/
	
	$("#save-recipe-material").on( "click", function(event) {
		//console.log(JSON.stringify($("#table-materials").bootstrapTable('getData')));
		/*var tableObj = [];
		$("#table-materials tbody tr").each(function() {
			var $self = $(this); console.log($self);
			var materialName = $self.find("td").eq(0).find('span:first').attr('class');
			//var materialId = materialName.first().attr('class');
			var materialAmount = $self.find("td").eq(1).html();

			item = {};
			item[materialName] = materialAmount;

			tableObj.push(item);

		});*/
		/*console.log(tableObj);*/
		//console.log($("#table-materials tbody").sortable('serialize'));
		var items = [];
		var $tableData = /*JSON.stringify(*/$("#table-materials").bootstrapTable('getData')/*)*/;
		$.each($tableData, function( index, value ) {
			console.log( value.material + ": " + value.amount );
			var item = {};
			item['material'] = value.material;
			item['amount'] = value.amount;
			items.push(item);
		});
		$.post({
			url: $(this).attr("data-url"),
			//type: 'post',
			data: { items },
			success: function(data, status) {
				if(data.result) {
					$("#table-materials").bootstrapTable('refresh');
					$("#recipe-matarial-info").removeClass('alert-info').removeClass().addClass('alert alert-success').html(data.alert);
				} else {
					$("#recipe-matarial-info").removeClass().addClass('alert alert-danger').html(data.alert);
				}
			}
		});
		return false;
	});
	
	$("#table-recipies").on('expand-row.bs.table', function (e, index, row, $detail) {console.log($detail);
		$detail.html('Loading from ajax request...');
		$.get(row['single'], function (res) {
			$detail.html(res);
		});
	});
	
});

function actionFormatter(value, row, index) {
	return [
		'<a class="like" href="javascript:void(0)" title="Like">',
		'<i class="glyphicon glyphicon-heart"></i>',
		'</a>',
		'<a class="edit ml10" href="javascript:void(0)" title="Edit">',
		'<i class="glyphicon glyphicon-edit"></i>',
		'</a>',
		'<a class="remove ml10" href="javascript:void(0)" title="Remove">',
		'<i class="glyphicon glyphicon-remove"></i>',
		'</a>'
	].join('');
};
/*$(document).on("click", ".gridViewModalMaterial", function (event) {
   var tagname = $(this)[0].tagName;    
   $($(this).attr('data-target')).find(".modal-title").html($(this).data("title"));
   $($(this).attr('data-target')).modal("show")
			  .find(".modalContent")
			  .load($(this).attr("href"));
	$("input#lsddrecipematerial-amount").val('111');
   return false;   
});*/

function detailFormatter(index, row) {
	var html = [];
	/*$.each(row, function (key, value) {
		html.push('<p><b>' + key + ':</b> ' + value + '</p>');
	});
	return html.join('');*/
	$.post({
		url: row['single'],
		success: function(data, status) {
			$("#"+index).html(data);
		}
	});
	return html;
}




window.actionEvents = {
	'click .gridViewModalMaterialDelete': function (e, value, row, index) {
		//alert('You click like icon, row: ' + JSON.stringify(row));
		//$("#table-materials tbody tr").eq(index).remove();
		//document.getElementById("table-materials").deleteRow(index);
		/*console.info(value);
		console.log(row);
		console.info(index);*/
		
		$("#table-materials").bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});
		$weight = $("#lsddrecipe-recipe_weight").val()*1; $("#lsddrecipe-recipe_weight").val($weight-row.amount*1);
		$cost = $("#lsddrecipe-recipe_cost").val()*1; $("#lsddrecipe-recipe_cost").val($cost-row.cost*1);
		$cost_2 = $("#lsddrecipe-recipe_cost_2").val()*1; $("#lsddrecipe-recipe_cost_2").val($cost_2-row.cost_2*1);
		$("#recipe-matarial-info").removeClass().addClass('alert alert-warning').html('Zapisz zmiany');
		if($weight-row.amount*1 == 1000) $("#save-recipe-material").attr("disabled",false); else $("#save-recipe-material").attr("disabled","disabled");
		
	},
	'click .gridViewModalMaterial': function (e, value, row, index) {

		var $target = $(e.target);
		var $modal = $($target.attr('data-target'));

		var LsddRecipeMaterial = {id_material_fk: row.material, amount: row.amount, index: index, isUpdate: 1, id: row.id };
		$.ajax({
			type: 'post',
			url: $target.attr("data-href"),
            data: { LsddRecipeMaterial },
			success: function(data, status) {
				if(data.success) {

					$modal.find(".modalContent").html(data.html);
					$modal.modal("show");
					
				} else { 
					$modal.find(".modalContent").html(data.html);
					$modal.modal("show");
				}
			}
		});
	},
	'click .up': function (e, value, row, index) {console.log('up');
        var source = JSON.stringify($('#table-materials').bootstrapTable('getData')[index]);
        var target = JSON.stringify($('#table-materials').bootstrapTable('getData')[index - 1]);
        $('#table-materials').bootstrapTable('updateRow', {'index':index - 1, 'row': JSON.parse(source)});
        $('#table-materials').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
	},
	'click .down': function (e, value, row, index) {
        var source = JSON.stringify($('#table-materials').bootstrapTable('getData')[index]);
        var target = JSON.stringify($('#table-materials').bootstrapTable('getData')[index + 1]);
        $('#table-materials').bootstrapTable('updateRow', {'index':index + 1, 'row': JSON.parse(source)});
        $('#table-materials').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
	}
};

/*function fmtMoveUp(value) {return '<a class="action up" href="javascript:void(0)" title="Move up">+</a>';};
function fmtMoveDown(value) {return '<a class="action down" href="javascript:void(0)" title="Move down">-</a>';};*/