'use strict';

KAPI.menuBuilder = function() { 
	$('.dd').nestable({ maxDepth: 3, collapsedClass:'dd-collapsed',/*noDragClass: "dd-nodrag"*/ })
		.on('change', function() {
			/*if ($('.dd-actions div.alert').length){
				$('.dd-actions div.alert').removeClass('alert-danger').removeClass('alert-success').addClass('alert-warning').text($(this).data("alert"));
			} else {
				$('.dd-actions').append('<div class="alert alert-warning" role="alert">'+$(this).data("alert")+'</div>');
			}*/
		}).nestable('collapseAll');

	$( "#menu-items-list" ).delegate( ".item-action-edit", "click", function() {
		var idItem = $(this).attr('id');
		var arr = idItem.split('-');
		$("#item-edit-"+arr[1]).toggle();
		$(this).toggleClass('active');
		/*$('#item-edit-'+arr[1]).css('display','block');
		$('#item-edit-'+arr[1]).append(arr[1]);*/
	});
    
    /*$( "#structure-items" ).delegate( ".item-action-edit", "click", function() {
        var idItem = $(this).attr('id');
        var arr = idItem.split('-');
        $("#item-edit-"+arr[1]).toggle();
        $(this).toggleClass('active');
    });*/
	
	$("#save-items").click(function() {
		var saveUrl = $(this).attr('href');
	//	var updateId = $(this).attr('id').split('-')[1]; console.log($("input#uitem-"+updateId));
		$.ajax({
			method: "POST",
			url: saveUrl,
			dataType : 'json',
			data: {items: JSON.stringify($('.dd').nestable('serialize')) }
		}).done(function( result ) {
			if(result.success) {
				if ($('.dd-actions div.alert').length){
					$('.dd-actions div.alert').removeClass('alert-danger').removeClass('alert-warning').addClass('alert-success').text(result.alert);
				} else {
					$('.dd-actions').append('<div class="alert alert-success" role="alert">'+result.alert+'</div>');
				}
			} else {
				if ($('.dd-actions div.alert').length){
					$('.dd-actions div.alert').removeClass('alert-success').removeClass('alert-warning').addClass('alert-danger').text(result.alert);
				} else {
					$('.dd-actions').append('<div class="alert alert-danger" role="alert">'+result.alert+'</div>');
				}
			}
		});
		return false;
	});
	
	//$(".menu-item-delete").click(function() {
	$( "#menu-items-list" ).delegate( ".menu-item-delete", "click", function() {
		var deleteUrl = $(this).attr('href');
		var deleteId = $(this).attr('id').split('-')[1];
		$.ajax({
			method: "POST",
			url: deleteUrl,
			dataType : 'json'
		}).done(function( result ) {
			$('li.dd-item[data-id='+deleteId+']').remove();
		});
		return false;
	});
	
	//$(".menu-item-update").click(function() { 
	/*$( "#menu-items-list" ).delegate( ".menu-item-update", "click", function() {
		var updateUrl = $(this).attr('href');
		var updateId = $(this).attr('id').split('-')[1];  
		$.ajax({
			method: "POST",
			url: updateUrl,
			dataType : 'json',
			data: {element_name: $("input#uitem-"+updateId).val() }
		}).done(function( result ) {
			$('li.dd-item[data-id='+updateId+'] > .dd3-content > span.dd3-content-title').text($("input#uitem-"+updateId).val());
		});
		return false;
	});*/
    
    $( "#menu-items-list" ).delegate( ".menu-item-update", "submit", function() {
		var updateUrl = $(this).attr('action');
		var updateId = $(this).attr('id').split('-')[1];  
		$.ajax({
			method: "POST",
			url: updateUrl,
			dataType : 'json',
			data: $(this).serialize()
		}).done(function( result ) {
			if(result.success) {
                $('li.dd-item[data-id='+updateId+'] > .dd3-content > span.dd3-content-title').text($("input#uitem-"+updateId).val());
                $(".help-block-"+updateId).removeClass('alert-danger').addClass('alert alert-success').text('Zmiany zostały zapisane');
            } else {
                $(".help-block-"+updateId).removeClass('alert-success').addClass('alert alert-danger').text(result.error);
            }
		});
		return false;
	});
	
	$('.selecctall').click(function(event) {  //on click
		if(this.checked) { // check select status
			$('.'+$(this).attr('id')).each(function() { //loop through each checkbox
				this.checked = true;  //select all checkboxes with class "checkbox1"              
			});
		}else{
			$('.'+$(this).attr('id')).each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "checkbox1"                      
			});        
		}
	});
	
	$('.chk-selected').click(function(event){
		var $menuId = $(this).data('id'), $actionUrl = $(this).data('url'), $type = $(this).data('type'), $target = $(this).data('target');
		$($target).each(function() { //loop through each checkbox
			if($(this).is(':checked') ) {
				$.ajax({
					method: "POST",
					url: $actionUrl,
					dataType : 'json',
					data: { menu_id: $menuId, item_type: $type, element_id: $(this).attr('value') , element_name: $(this).parent().text() }
				}).done(function( result ) {
					if(result.success) {
						//var liContent = itemPattern.format({_id: result.id,_text: result.text, _style: 'primary', _type : 'strona'});
						$("#menu-items-list").append(result.html);
					} else { console.log(result); }
				});
			}               
		});
	});
		
	$('#menu-selected-link').click(function(event){
		var $menuId = $(this).data('id'), $actionUrl = $(this).data('url'), $type = $(this).data('type'), $target = $(this).data('target');
		$.ajax({
			method: "POST",
			url: $actionUrl,
			dataType : 'json',
			data: { menu_id: $menuId, item_type: $type, link_href: $("input#element-url").val() , element_name: $("input#element-name").val() }
		}).done(function( result ) {
			if(result.success) {
				$("#menu-items-list").append(result.html);
			} else { 
				$("#link-error").append(result.error);
				$("#link-error").parent().addClass('has-error');
			}
		});
		return false;
	});
	
	$('#menu-selected-separator').click(function(event){
		event.stopImmediatePropagation();
	    event.preventDefault();
		
		var $menuId = $(this).attr('data-id'), $actionUrl = $(this).data('url'), $type = $(this).data('type'), $target = $(this).data('target');
		$.ajax({
			method: "POST",
			url: $actionUrl,
			dataType : 'json',
			data: { menu_id: $menuId, item_type: $type,  element_name: $("input#element-name-separator").val() }
		}).done(function( result ) {
			if(result.success) {
				$("#menu-items-list").append(result.html);
			} else { 
				$("#separator-error").append(result.error);
				$("#separator-error").parent().addClass('has-error');
			}
		});

		return false;
	});
    
    $(document).on("submit", ".structure-form-edit", function (event) { 
        var $form = $(this);//$("#modal-grid-view-form-dict");
        $.ajax({
            type: $form.attr("method"),
            url: $form.attr("action"),
            data: $form.serialize(),

            success: function(data, status) {
                $('li.dd-item[data-id='+data.id+'] > .dd3-content > span.dd3-content-title').text(data.name);
                $('li.dd-item[data-id='+data.id+']  > .dd3-content > .item-action-edit > .click-text > span.type-name').text(data.type);
            }
        });
        
        event.stopImmediatePropagation();
        event.preventDefault();
        return false;
    });
    
    $(document).on("submit", ".modal-structure-create", function (event) {
        event.stopImmediatePropagation();
        event.stopImmediatePropagation();
        event.preventDefault();
        
        var $form = $(this); //$($(this).attr('data-form'));
        var $target = $($form.attr("data-target")); 
            console.log($target);
        var $list = $($(this).attr('data-list'));

        $.ajax({
            type: $form.attr("method"),
            url: $form.attr("action"),
            data: $form.serialize(),

            success: function(data, status) {
                if(data.success) {
                    if(!data.parent || data.parent == 0) {
                        $("#menu-items-list").append(data.html);
                    } else {
                        //console.log($("#structure-items li").find('[data-id="'+data.parent+'"]'));
                        var $parent = $("#menu-items-list").find('.dd-item[data-id="'+data.parent+'"]');
                        var $parentChildList = $("#menu-items-list").find('.dd-item[data-id="'+data.parent+'"] > .dd-list');
                        /*console.log(data.parent);
                        console.info($parent);
                        console.info($parentChildList.length);*/
                        
                        //if($parent.find('ol.dd-list').lenght) {
                            //$parent.find('ol.dd-list').append(data.html);
                            
                        if($parentChildList.length) {
                            $parentChildList.append(data.html);
                        } else {
                            //$parent.append('<ol class="dd-list">'+data.html+'</ol>');
                            $('#menu-items-list li').each(function(index) {
                                if ( $(this).attr('data-id') ==  data.parent ) {
                                    $(this).append('<ol class="dd-list">'+data.html+'</ol>');
                                    //console.info( $(this) );
                                }
                            });
                        }                
                    }
                    $target.modal("hide");
                    
                } else { 
                    $target.find(".modalContent").html(data.html);
                }
            }
        });
    });
    
    $("#save-structure").click(function() {
        var saveUrl = $(this).attr('href');
        //	var updateId = $(this).attr('id').split('-')[1]; console.log($("input#uitem-"+updateId));
        $.ajax({
            method: "POST",
            url: saveUrl,
            dataType : 'json',
            data: {items: JSON.stringify($('.dd').nestable('serialize')) }
        }).done(function( result ) {
            if(result.success) {
                if ($('.dd-actions div.alert').length){
                    $('.dd-actions div.alert').removeClass('alert-danger').removeClass('alert-warning').addClass('alert-success').text(result.alert);
                } else {
                    $('.dd-actions').append('<div class="alert alert-success" role="alert">'+result.alert+'</div>');
                }
            } else {
                if ($('.dd-actions div.alert').length){
                    $('.dd-actions div.alert').removeClass('alert-success').removeClass('alert-warning').addClass('alert-danger').text(result.alert);
                } else {
                    $('.dd-actions').append('<div class="alert alert-danger" role="alert">'+result.alert+'</div>');
                }
            }
        });
        return false;
    });
};
