function folderBrowser() {

	var filemanager = $('.filemanager'),
		breadcrumbs = $('.breadcrumbs'),
		fileList = filemanager.find('.data');
        
    fileList.on('click', 'li.folders', function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var nextDir = $(this).find('a.folders').attr('href');

        $.ajax({
            type: "POST",
            //dataType: 'json',
            //data: formData,
            async: false,
            url: nextDir,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {   },
            success: function(data) {
                render(data);
                $(".breadcrumbs").html(data.breadcrumbs);
            },
            error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
               // $target.find(".modalContent").removeClass('preload');
                alert('Something went to wrong.Please Try again later...');
            }
        });
    });
    
    fileList.on('dblclick', 'li.folders-check', function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var nextDir = $(this).find('a.folders-check').attr('href');

        $.ajax({
            type: "POST",
            //dataType: 'json',
            //data: formData,
            async: false,
            url: nextDir,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {   },
            success: function(data) {
                render(data);
                $(".breadcrumbs").html(data.breadcrumbs);
            },
            error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
               // $target.find(".modalContent").removeClass('preload');
                alert('Something went to wrong.Please Try again later...');
            }
        });
    });
    
    fileList.on('click', 'li.folders-check', function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var fileId = $(this).data('id');

        $("#selectedFolder").prepend('<option value="'+$(this).data("id")+'" selected>'+$(this).text()+'</option>');
        $("#selectedFolder").trigger("chosen:updated");
    });
    
    fileList.on('click', 'li.files', function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var fileId = $(this).data('id');

        $("#selectedFile").prepend('<option value="'+$(this).data("id")+'" selected>'+$(this).text()+'</option>');
        $("#selectedFile").trigger("chosen:updated");
    });
    
    $(".breadcrumbs").on('click', '.folderUp', function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var nextDir = $(this).attr('href');

        $.ajax({
            type: "POST",
            //dataType: 'json',
            //data: formData,
            async: false,
            url: $(this).attr("href"),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {   },
            success: function(data) {
                render(data);
                $(".breadcrumbs").html(data.breadcrumbs);
            },
            error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
              //  $target.find(".modalContent").removeClass('preload');
                alert('Something went to wrong.Please Try again later...');
            }
        });
    });

    function render(data) {
            
        var scannedFolders = [],
            scannedFiles = [];

        if(Array.isArray(data)) {

            data.forEach(function (d) {

                if (d.type === 'folder') {
                    scannedFolders.push(d);
                }
                else if (d.type === 'file') {
                    scannedFiles.push(d);
                }

            });

        }
        else if(typeof data === 'object') {
//console.log(data);
            scannedFolders = data.folders;
            scannedFiles = data.files;
            
           /* $.each(data, function(i, d) { console.log(d);
                if (d.type === 'folder') {
                    scannedFolders.push(d);
                }
                else if (d.type === 'file') {
                    scannedFiles.push(d);
                }
            });*/
        }

        // Empty the old result and make the new one

        fileList.empty().hide();

        if(!scannedFolders.length && !scannedFiles.length) {
            filemanager.find('.nothingfound').show();
        }
        else {
            filemanager.find('.nothingfound').hide();
        }

        if(scannedFolders.length && scannedFolders != 'undefind') {

            scannedFolders.forEach(function(f) {

                var itemsLength = f.length,
                    name = /*escapeHTML(*/f.name/*)*/,
                    icon = '<i class="icon-folder fa fa-folder fa-3x text--yellow"></i>',
                    className = (f.className) ? f.className : "folders";

                /*if(itemsLength) {
                    icon = '<span class="icon folder full"></span>';
                }

                if(itemsLength == 1) {
                    itemsLength += ' item';
                }
                else if(itemsLength > 1) {
                    itemsLength += ' items';
                }
                else {
                    itemsLength = 'Empty';
                }*/
      
                var folder = $('<li class="'+className+'"><a href="'+ f.url +'" title="'+ f.name +'" class="'+className+'">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');
                folder.appendTo(fileList);
            });

        }

        if(scannedFiles.length && scannedFiles != 'undefind') {

            scannedFiles.forEach(function(f) {

                var fileSize = bytesToSize(f.size),
                    name = escapeHTML(f.name),
                    fileType = name.split('.'),
                    icon = '<span class="icon file"></span>';

                fileType = fileType[fileType.length-1];

                //icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span><br /><span class="details">'+fileSize+'</span>';
                icon = '<div class="details"><i class="icon-folder fa fa-file fa-3x text--teal"></i><br /><span>'+fileSize+'</span></div>';
                var file = $('<li class="files" data-id="'+f.id+'">'+icon+'<a href="'+ f.url+'" class="name ellipsis" title="'+ name +'"><span class="text-concat">'+ name +' </span></a></li>');
                file.appendTo(fileList);
            });

        }


        // Generate the breadcrumbs

        var url = '';

       /* if(filemanager.hasClass('searching')){
            url = '<span>Search results: </span>';
            fileList.removeClass('animated');
        }
        else {
            fileList.addClass('animated');
            breadcrumbsUrls.forEach(function (u, i) {
                var name = u.split('/');
                if (i !== breadcrumbsUrls.length - 1) {
                    url += '<a href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
                }
                else {
                    url += '<span class="folderName">' + name[name.length-1] + '</span>';
                }
            });
        }
        breadcrumbs.text('').append(url);*/

        // Show the generated elements

        //fileList.animate({'display':'inline-block'});
        $("ul.data").show();

    }
    
    function escapeHTML(text) {
        return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
    }


    // Convert file sizes from bytes to human readable units

    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Bytes';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
}

$(document).on("click", ".filemanager-open > a", function (event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $target = $($(this).attr('data-target'));  
    var $model = $(this).data("model");
    var $action = $(this).attr("href")+"?modelName=" + $model;
    
    $target.find(".modalContent").addClass('preload');
    $target.find(".modal-title").html($(this).data("title"));
  
    $target.find(".modalContent")
			  .load($action, 
                    function(response, status, xhr){ 
                        $target.modal('show'); 
                        
                        if ( status == "error" ) {
                            var msg = "Sorry but there was an error: ";
                            $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                        } 
                        //$target.find(".modalContent").removeClass('preload');
                        //setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
               });          
    $target.on('shown.bs.modal', function() {
        folderBrowser();
        $("select.chosen-select").chosen({disable_search_threshold:10});
        $target.find(".modalContent").removeClass('preload');
        
	});
	
   return false;   
});

$(document).on("submit", ".modalBrowser", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $target = $($(this).data('target'));
    
    var xhr = new XMLHttpRequest();
    xhr.open($(this).attr('method'), $(this).attr('action'), true);
    //xhr.setRequestHeader('Authorization', 'Bearer ' + '5cd56bcf');
    
    xhr.onreadystatechange=function()  {
        if (xhr.readyState==4 && xhr.status==200)  {
            var result = JSON.parse(xhr.responseText);
            $("#selectedFilesFromSystem > ul").prepend(result.list);
            $target.modal("hide");
        }
        
        if (xhr.readyState==4 && xhr.status==500)  {
            alert('error');          
        }
        
        if (xhr.readyState==4 && xhr.status==422)  {
            var result = JSON.parse(xhr.responseText);
            
            $("#selectedFilesFromSystem").html(element.message);
        }
    }
    
    var form = new FormData(this);
    
    xhr.send(form);
    
    
    return false;
});

