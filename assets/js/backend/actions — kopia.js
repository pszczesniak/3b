$(document).ready( function() {
    KAPI.misc();
    
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    
    $("body").tooltip({   
        selector: "[data-toggle='tooltip']",
        container: "body"
    })
    .popover({
        selector: "[data-toggle='popover']",
        container: "body",
        html: true
    });
    
    $('html').on('click', function(e) {
        if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
            $('[data-original-title]').popover('hide');
        }
    });


	$('.select2').select2({dropdownAutoWidth : true,  width: '100%', allowClear: true, placeholder: '- wybierz -'});
    
    $("select.chosen-select").chosen({disable_search_threshold:10});
    
    $('fieldset.collapsible > legend').append(' (<span style="font-family: monospace;">+</span>)');
	
	$('.inlinesparkline').sparkline(); 
    
    $('.ms-resource').multiselect({includeSelectAllOption: true, selectAllValue: 0, maxHeight: 220, nonSelectedText: 'wybierz opcje'});
    
    $('.ms-options').multiselect({includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        selectAllText: 'Zaznacz wszystko',
        nonSelectedText: 'wybierz opcje'
    });
    
    $('.ms-select').multiselect({
        includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        selectAllText: 'Zaznacz wszystko',
        nonSelectedText: 'wybierz opcje',
        onDeselectAll: function() {
            var ids = new Array(); var employees = new Array();  ids.push( 0 ); 
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
        onSelectAll: function() {
            var values = $('.ms-select option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
        onChange: function (option, checked) {
            var values = $('.ms-select option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
    });
    
    $('.sidebar-add').click(function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        window.location.replace( $(this).data("href") );
    });
    
    $('.selecctall').click(function(event) {  
        var isCalendar = $(this).hasClass('calendar-employees');
        if(this.checked) { 
			$('.'+$(this).attr('id')).each(function() { 
				this.checked = true;    
                if(isCalendar) {
                    var color ='#'+Math.random().toString(16).substr(2,6);
                    $('label[for='+$(this).attr('id')+']').css('color', color);
                }           
			});
		}else{
			$('.'+$(this).attr('id')).each(function() { 
				this.checked = false;   
                if(isCalendar) {
                    var color ='#000000';
                    $('label[for='+$(this).attr('id')+']').css('color', color);
                }                    
			});        
		}
	});
	
	$(".filtering-options").on('change', function(e) {
        $('#calendar').fullCalendar('refetchEvents');
    } );
    
    $(".schedule-filtering-options").on('change', function(e) {
        if( $('#schedule').length > 0 )
			$('#schedule').fullCalendar('refetchEvents');
        if( $('#personal').length > 0 )
			$('#personal').fullCalendar('refetchEvents');
        if( $('#resources').length > 0 )
			$('#resources').fullCalendar('refetchEvents');
    } );
	
	/*swal({
	  title: 'Error!',
	  text: 'Do you want to continue',
	  type: 'error',
	  confirmButtonText: 'Cool'
	});*/

});

var $btns = $('.btn-alphabet').click(function() {
        if (this.id == 'all') {
            $('#parent > div').fadeIn(450);
        } else {
            var $el = $('.' + this.id).fadeIn(450);
            $('#parent > div').not($el).hide();
        }
        $btns.removeClass('active');
        $(this).addClass('active');
    });
    
$(document).on("click", ".modalConfirm", function (e) {
    e.preventDefault();

    var id = $(this).data('id'); var table = $(this).data('table');
    $('#modalConfirm').data('id', id).data('table', table).modal('show');
    $("#btnYesConfirm").attr("href", $(this).attr("href"));
});

$(document).on("click", ".deleteConfirm", function (e) {
    e.preventDefault();

    var id = $(this).data('id'); var table = $(this).data('table');
	if($(this).hasClass('done')) {
		$target = $("#modal-grid-item");
		$target.find(".modal-title").html('<i class="fa fa-info-circle text--red"></i>Alert');
		//$target.find(".modalContent").addClass('preload');
		/*$("#modal-grid-item").modal("show")
			.find(".modalContent")
			.load('/task/event/showajax/'+calEvent.id);*/
		$target.modal("show").find(".modalContent").html('<div class="modal-body"><div class="alert alert-danger">Zadanie zostało już zamknięte</div></div>');   
	} else {
		$('#modalConfirmDelete').data('id', id).data('table', table).modal('show');
       
		$("#btnYesDelete").attr("href", $(this).attr("href"));
		if( $(this).attr('data-label') ) {
			$("#btnYesDelete").html($(this).data('label'));
		}
		if( $(this).attr('data-info') ) {
			$(".modal-body > p").html($(this).data('info'));
		}
        if( $(this).attr('data-id') ) {
			$("#btnYesDelete").attr('data-id', $(this).attr('data-id'));
		}
    }
});

$(document).on("click", ".deleteConfirmWithComment", function (e) {
    e.preventDefault();
   
    var id = $(this).data('id'); var table = $(this).data('table');
    $('#modalConfirmDeleteWithComment').data('id', id).data('table', table).modal('show');
    $("#confirm-comment").val('')
    $("#btnYesDeleteWithComment").attr("href", $(this).attr("href"));
    if( $(this).attr('data-label') ) {
        $("#btnYesDeleteWithComment").html($(this).data('label'));
    }
});

$(document).on("click", "#btnYesDelete", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    
  	var id = $('#modalConfirmDelete').data('id');
    var $row = $(this).data('row')*1;

  	$('#modalConfirmDelete').modal('hide');
    
    $.ajax({
        type: 'post',
        cache: false,
        data: {_csrf : csrfToken},
        dataType: 'json',
        url: $(this).attr('href'),
        beforeSend: function() { 
            $("#validation-form").hide().empty(); 
        },
        success: function(data) {
            $idRow = data.id; 
			if(data.table) 
				var $table = $(data.table);
			else
				var $table = $("#table-data");
                
            if(data.result == true && data.action == 'fileDelete') { 
                var $fileId = data.id;
                $('li#file-'+$fileId).prepend('<div class="file-alert alert alert-success">Dokument został usunięty</div>').delay(1000).fadeOut(600, function(){  $(this).remove(); });
            } else if(data.result == false && data.action == 'fileDelete') { 
                var $fileId = data.id
                $('li#file-'+$fileId).prepend('<div class="file-alert alert alert-danger">'+data.error+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
            }
            
            if(data.success == true && data.action == 'suborderDelete') { 
                var $id = data.id;
                $('li#oitem-'+$id).delay(1000).fadeOut(600, function(){  $(this).remove(); });
            }  
                
            if($table.length == 0)
                $table = $("#table-events");
			if(data.success == false) {
				var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                
               /* var arr = data.errors; 
                var txtError = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                $.each(arr, function(index, value) {
                    if (value.length != 0) {
                       // $("#validation-form").removeClass('none').removeClass('alert-success').addClass('alert-danger').append('<strong>'+ value +'</strong><br/>');
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });*/
                
                var unix = Math.round(+new Date()/1000);
                
                if($table.length > 0 ) {
                    $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                }
                
                KAPI.resources.afterSaveFalse(txtError, unix, data);
                KAPI.tasks.afterSaveFalse(txtError, unix, data);
                
            } else { 
                /*$table.bootstrapTable('remove', {
                    field: 'id',
                    values: [$idRow]
                });*/
                //$table.bootstrapTable('refresh');
                
                if(data.refresh == 'inline') {
                    $table.bootstrapTable('updateRow', {index: data.index, row: data.row });
                    //$('#table-inbox').bootstrapTable('updateRow', {index: data.index, row: { star:data.star   }});
                } else {
                var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $table.bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o
                    });
                }
                
                if(data.alert) {
                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    
                    if( $table.length > 0 ) {
                        $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    }
                }
                if(data.buttons) { 
                    //$('tr[data-index='+data.index+']').find('.edit-btn-group').html(data.buttons);
                    $('#table-data').bootstrapTable('updateRow', {index: data.index, row: { actions:data.buttons   }});
                }
             
                if ( $("#db-event-"+data.id).length > 0 && data.action == 'delete') {
                    $("#db-event-"+data.id).remove();
                    $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                }
                
                if(data.action == 'fastAction') { 
                    $(".todo-hours_per_day").html(data.stats.hours_per_day);
					$(".todo-hours_per_month").html(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);                  
                } else {
                    KAPI.tasks.afterSaveTrue(txtError, unix, data);
                    KAPI.resources.afterSaveTrue(txtError, unix, data);
                    KAPI.chat.afterSaveTrue(txtError, unix, data);
                }
            }
            
            if(data.action != 'deleteTodoSet') {
                KAPI.tasks.afterSaveTrue(txtError, unix, data);
            }
            if(data.action != 'fastAction' && data.action != 'deleteRate' && data.action != 'genInvoice' && data.action != 'delSide') {
                if ( $("#modal-grid-event").length > 0 ) {
                    $("#modal-grid-event").modal("hide");
                }
                
                if ( $("#modal-grid-item").length > 0 ) {
                    $("#modal-grid-item").modal("hide");
                }
            }
			
			if(data.action == 'genInvoice') {
				$('#table-periods').bootstrapTable('refresh');
                $('#table-periods').bootstrapTable('resetView');

                $.post({
                    url: '/accounting/period/stats?ajax=true',
                   /* data: { id: row.id },*/

                    success: function(result) {
                        var chartData = {
                            labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                            datasets: [{
                                type: 'line',
                                label: 'Średnia stawka',
                                borderColor: window.chartColors.blue,
                                borderWidth: 2,
                                fill: false,
                                data: result.rates
                            }, {
                                type: 'bar',
                                label: 'Wartość z faktur [w tys]',
                                backgroundColor: window.chartColors.red,
                                data:  result.profits,
                                borderColor: 'white',
                                borderWidth: 2
                            }]

                        };
                        
                        var ctx = document.getElementById("canvas").getContext("2d");
                        window.myMixedChart = new Chart(ctx, {
                            type: 'bar',
                            data: chartData,
                            options: {
                                responsive: true,
                                title: {
                                    display: true,
                                    text: 'Statystyki zysku i średniej aaa'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: true
                                }
                            }
                        }); 
                    }
                });
			}
            
            if(data.action == "dpaccount") {
                $(".crm-accounts-info-peronals").html(data.info);
            }
            
            if(data.action == "daccount") {
                $(".crm-accounts-info-status").html(data.info);
            }
            
            if(data.action == "debtLock") {  
                $("#crm-debt-info").html(data.info);
                $("#debt-block > legend").replaceWith(data.legend);
            }
            
            if(data.action == "showClient" && $("#btn-show-client").length) {  
                $("#btn-show-client").html(data.info);
            }
        },
        error: function(xhr, textStatus, thrownError) {
            alert('Something went to wrong.Please Try again later...');
        }
    });
    
});

$(document).on("click", "#btnYesDeleteWithComment", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    
  	var id = $('#modalConfirmDeleteWithComment').data('id');
    var $row = $(this).data('row')*1;
    var $comment = $("#confirm-comment").val();
    
    if($comment) {
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, comment: $comment},
            dataType: 'json',
            url: $(this).attr('href'),
           
            success: function(data) {
                $idRow = data.id; 
                if(data.table) 
                    var $table = $(data.table);
                else
                    var $table = $("#table-data");
                    
                if($table.length == 0)
                    $table = $("#table-events");
                if(data.success == false) {
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                    $(".field-confirm-comment > .help-block").text(data.alert);
                   /* if($table.length > 0 ) {
                        $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    }*/
                    
                   /* KAPI.resources.afterSaveFalse(txtError, unix, data);
                    KAPI.tasks.afterSaveFalse(txtError, unix, data);*/
                    
                } else { 
                    $('#modalConfirmDeleteWithComment').modal('hide');
                    //$table.bootstrapTable('refresh');
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $table.bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o
                    });
                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    
                    if( $table.length > 0 ) {
                        $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    }
                    if(data.buttons) { 
                        //$('tr[data-index='+data.index+']').find('.edit-btn-group').html(data.buttons);
                        $('#table-data').bootstrapTable('updateRow', {index: data.index, row: { actions:data.buttons   }});
                    }
                 
                    if ( $("#db-event-"+data.id).length > 0 && data.action == 'delete') {
                        $("#db-event-"+data.id).remove();
                        $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    }
                    
                    KAPI.tasks.afterSaveTrue(txtError, unix, data);
                    KAPI.resources.afterSaveTrue(txtError, unix, data);
                    KAPI.chat.afterSaveTrue(txtError, unix, data);
                    
                    if ( $("#modal-grid-event").length > 0 ) {
                        $("#modal-grid-event").modal("hide");
                    }
                    
                    if ( $("#modal-grid-item").length > 0 ) {
                        $("#modal-grid-item").modal("hide");
                    }
                }
                
                
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    } else {
        $("#confirm-comment").addClass('bg-red2');
    }
    
});

$(document).on("click", "#user-comment-send", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    if($("#user-comment").val() != '') {
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, Note: { note: $("#user-comment").val()}, AccNote: { note: $("#user-comment").val()}},
            dataType: 'json',
            url: $(this).data('action'),
            beforeSend: function() { 
                $("#validation-form").hide().empty(); 
            },
            success: function(data) {
               
                if(data.success == false) {
                    
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                   
                    $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                } else {
                    $("#user-comment").val('');

                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    
                   $(".task-comments-content").prepend(data.html);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    } else {
        var txtError = 'Wpisz treść wiadomości<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
        var unix = Math.round(+new Date()/1000);
       
        $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        
        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
    }
    
});

$(document).on("click", "#user-comment-send-public", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    if($("#user-comment-public").val() != '') {
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, Note: { note: $("#user-comment-public").val()}},
            dataType: 'json',
            url: $(this).data('action'),
            beforeSend: function() { 
                $("#validation-form").hide().empty(); 
            },
            success: function(data) {
               
                if(data.success == false) {
                    
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                   
                    $(".task-comments-public").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                } else {
                    $("#user-comment-public").val('');

                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    $(".task-comments-public").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    
                   $(".task-comments-content-public").prepend(data.html);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    } else {
        var txtError = 'Wpisz treść wiadomości<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
        var unix = Math.round(+new Date()/1000);
       
        $(".task-comments-public").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        
        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
    }
    
});

$(document).on("click", ".gridViewModal", function (event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $target = $($(this).attr('data-target'));  
    var $tbuild = $(this).attr('data-tbuild'); 
    $target.find(".modalContent").addClass('preload');
    if( $("#dictionarysearch-id").length > 0 ) {
        var valueId = $("#dictionarysearch-id").val(); 
        if( valueId != 0 && valueId != '' )
            var $action = $(this).attr("href")+'/'+valueId;
        else
            var $action = $(this).attr("href")+'/0';
    } else {
        var $action = $(this).attr("href");
    }
    
    if( $(".cal-desc-date").length > 0 ) {
        var calendarDate = $(".cal-desc-date").text();
        var calendarDateArr = calendarDate.split('-');
        var calendarDateUTC = Date.UTC(calendarDateArr[0], (calendarDateArr[1]*1-1), calendarDateArr[2])/1000;
        $action = $action + '?start='+calendarDateUTC;
    }
    
    if( $(this).attr('data-msg-id') ) {
        if($("#table-inbox").length)
            $("#table-inbox").find("[data-uniqueid='" + $(this).attr('data-msg-id') + "']").removeClass('bold');
        
        if($("#cme-"+$(this).attr('data-msg-id')).length) {
            $("#cme-"+$(this).attr('data-msg-id')).remove();
            $("#msg-counter").text($("#msg-counter").text()*1-1);
        }
    }
    /*if ($target.data('bs.modal').isShown) {
        $target.modal("hide");
    }*/
    var caseList = ( $(this).data('case') ) ? $($(this).data('case')) : false;
    if( caseList ) {
        if( caseList.length ) {
            var selectedArr = [];
            $($(this).data('case')+' option:selected').each(function() {
                selectedArr.push($(this).val());
            });
            if( selectedArr.length == 0 ) {
                $target.find(".modal-title").html($(this).data("title"));
  
                $target.find(".modalContent")
                    .load($action, 
                        function(response, status, xhr){ 
                            $target.modal('show'); 
             
                            var msg = "Proszę wybrać sprawę!";
                            $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+ msg +'</div></div>' );

                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
                   });   
                return false;
            } else {
                $action = $action + '?cids='+selectedArr.join()+'&docs=false';
            }
        }
    }
    
    var customerList = ( $(this).data('customer') ) ? $($(this).data('customer')) : false;
    if( customerList ) {
        if( customerList.length ) {
            if( !customerList.val() || customerList.val() == 0 ) {
                $target.find(".modal-title").html($(this).data("title"));
  
                $target.find(".modalContent")
                    .load($action, 
                        function(response, status, xhr){ 
                            $target.modal('show'); 
             
                            var msg = "Proszę wybrać klienta!";
                            $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+ msg +'</div></div>' );

                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
                   });   
                return false;
            } else {
                $action = $action + '?cid='+customerList.val();
            }
        }
    }
  
    $target.find(".modal-title").html($(this).data("title"));
  
    $target.find(".modalContent")
			  .load($action, 
                    function(response, status, xhr){ 
                        if($tbuild) { 
                            //$($tbuild).bootstrapTable({method: 'get'});
                            $($tbuild).bootstrapTable();
                            $($tbuild).on('editable-save.bs.table', function(field, row, oldValue, $el){
                                $.ajax({
                                    type: 'POST',
                                    url: '/accounting/discount/save',
                                    data: {id: oldValue['id'], period: oldValue['period'], value: oldValue[row], field: row},
                                    success: function(data, status) {
                                        var unix = Math.round(+new Date()/1000);
                                        //$('#table-settled').bootstrapTable('resetView');
                                        if(data.success) {
                                            $(".div-table-settled").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
                                            ///if( data.table ) { 
                                            ///    $('#table-settled').bootstrapTable('refresh'); 
                                            ///} 
                                        } else {
                                            var arr = data.errors; 
                                            var txtError = '';//'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                                            $.each(arr, function(index, value) {
                                                if (value.length != 0) {
                                                   // $("#validation-form").removeClass('none').removeClass('alert-success').addClass('alert-danger').append('<strong>'+ value +'</strong><br/>');
                                                    txtError += '<strong>'+ value +'</strong><br/>';
                                                }
                                            });
                                            $(".div-table-settled").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
                                        }
                                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                                    }
                                });
                            });
                        }
                        
                        $target.modal('show'); 
                        
                        if ( status == "error" ) {
                            var msg = "Sorry but there was an error: ";
                            $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                        } 
                        //$target.find(".modalContent").removeClass('preload');
                        setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
               });          
    $target.on('shown.bs.modal', function() {
        relationship();
        $target.find(".modalContent").removeClass('preload');
        
	});
	
   return false;   
});

$(document).on("click", '.insertInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $(this).attr('data-target'); 
    var $input = $(this).attr('data-input'); 
    //$target.load($(this).attr("href"));
    $($target).load($(this).attr("href"), 
                function(response, status, xhr){                     
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } else {
                        $(".saveInline").attr('data-target', $target);
                        $(".saveInline").attr('data-input', $input);
                        $($target).removeClass('none');
                    }
                        
            });  
 
    return false;
});

$(document).on("click", '.saveInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $($(this).attr('data-target')); 
    var $input = $($(this).attr('data-input')); 
    
    if(!$("#dictValue").val()) {
        $("#dictValue").addClass('bg-red2');
        $("#dictValue").attr('placeholder', 'Proszę wpisać wartość słownika');
    } else {
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {value: $("#dictValue").val()},
            url: $(this).attr('href'),
            success: function(data) {              
                if(data.success) {
                    if($input.length) {
                        var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
                        var values = [];
                        $input.prepend(opt);
                       
                        values.push(data.id);
                        $input.val(values);
                        
                        $input.trigger('change');
                        
                        if($input.hasClass('ms-options')) {
                            $('#correspondence-addresses_list').multiselect('rebuild');
                            $('#correspondence-addresses_list').multiselect('refresh');
                        }
                        $target.addClass('none');
                    }
                } else {    
                    $("#dictValueHint").text(data.alert);
                }
            },
            /*error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                //$target.find(".modalContent").removeClass('preload');
                //alert('Something went to wrong.Please Try again later...');
                CheckForSession();
            }*/
        });
    }
    
    //var $target = $($(this).attr('data-target')); 
    //$target.load($(this).attr("href"));
    //$target.removeClass('none');
    
    return false;
});

/*$(document).on("submit", '.saveInlineForm', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $($(this).attr('data-target')); 
    var $input = $($(this).attr('data-input')); 

	$.ajax({
		type: 'post',
		cache: false,
		dataType: 'json',
		data: {value: $(this).serialize()},
		url: $(this).attr('href'),
		success: function(data) {              
			if(data.success) {
				if($input.length) {
					var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
					var values = [];
					$input.prepend(opt);
				   
					values.push(data.id);
					$input.val(values);
					
					if($input.hasClass('ms-options')) {
						$('#correspondence-addresses_list').multiselect('rebuild');
						$('#correspondence-addresses_list').multiselect('refresh');
					}
					$target.addClass('none');
				}
			} else {    
				$("#dictValueHint").text(data.alert);
			}
		},

	});
    
    return false;
});*/

$(document).on("click", '.closeInline', function(event) { 
    $( event.target ).parent().parent().addClass('none');
});

$(document).on("click", '.closeInlineForm', function(event) { 
    $( event.target ).parent().parent().parent().addClass('none');
});

$(document).on("click", ".viewEditModal", function (event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $target = $($(this).attr('data-target')); 
    $target.find(".modalContent").addClass('preload');
    if($("#dictionarysearch-id").length > 0)
        var $action = $(this).attr("href")+'/'+$("#dictionarysearch-id").val();
    else
        var $action = $(this).attr("href");
    
    var $changeView = false;
	$target.modal("hide");

    $target.find(".modal-title").html($(this).data("title"));

    $target.on('hidden.bs.modal', function () {
        $target.find(".modalContent")
            .load($action, 
                function(response, status, xhr){ 
                    $target.unbind('hidden.bs.modal');
                    $target.modal('show'); 
                    
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } else {
                    
                        $target.on('shown.bs.modal', function() {
                            relationship();
                            $target.find(".modalContent").removeClass('preload');
                        });
                }
                        
            });          
    });    
	
   return false;   
});

$(document).on("submit", ".modal-grid", function (event) {
	var $form = $(this); //$($(this).attr('data-form'));
	var $target = $($form.attr("data-target"));
	var $table = $($(this).attr('data-table'));
    var $input = $($(this).attr('data-input'));

	$.ajax({
		type: $form.attr("method"),
		url: $form.attr("action"),
		data: $form.serialize(),

		success: function(data, status) {
            if(data.success) {
				if($table.length) {
                    $table.bootstrapTable('refresh');
                    $target.modal("hide");
                } else {
                    if( $("#table-events").length > 0 ) {
                        $table = $("#table-events");
                        $table.bootstrapTable('refresh');
                        $target.modal("hide");
                    }
                }
                if($input.length) {
                    var fld = document.getElementById('correspondence-events_list');
                    var values = [];
                    for (var i = 0; i < fld.options.length; i++) {
                        if (fld.options[i].selected) {
                            values.push(fld.options[i].value);
                        }
                    }
                    
                    var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
  
					$input.prepend(opt);
                    values.push(data.id);
					$input.val(values);
					
					$target.modal("hide");
   
                    $('#correspondence-events_list').multiselect('rebuild');
                    $('#correspondence-events_list').multiselect('refresh');
                }
                
                if(data.action == 'fastAction') {
                    $(".todo-hours_per_day").html(data.stats.hours_per_day);
					$(".todo-hours_per_month").html(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);
                    
                    $target.modal("hide");
                }
			} else { 
				$target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                $.each(arr, function(index, value) {
                    if (value.length != 0) {
                       // $("#validation-form").removeClass('none').removeClass('alert-success').addClass('alert-danger').append('<strong>'+ value +'</strong><br/>');
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
               // $("#validation-form").show();
                $target.find(".modal-body").prepend('<div class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                /*if($('fieldset.collapsible > legend > span').length == 0)
                    $('fieldset.collapsible > legend').append(' (<span style="font-family: monospace;">+</span>)');
                $('fieldset.collapsible > legend').click(function () {
                    var $divs = $(this).siblings();
                    $divs.toggle();

                    $(this).find('span').text(function () {
                        return ($divs.is(':visible')) ? '-' : '+';
                    });
                });*/
			}
		}
	});
    
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});

$(document).on("submit", ".modalAjaxForm", function (event) { 
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target")); 
    var $table = $($form.attr("data-table"));
	var $input = $($form.attr("data-input"));
    $target.find(".modalContent").addClass('preload');
    $.ajax({
        type: 'post',
        cache: false,
        dataType: 'json',
        data: $(this).serialize(),
        url: $(this).attr('action'),
        beforeSend: function() { 
            $("#validation-form").hide().empty(); 
        },
        success: function(data) {
            $target.find(".modalContent").removeClass('preload');
			if(data.success == false) { 
               
                $target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '';
                $.each(arr, function(index, value)  {
                    if (value.length != 0)  {
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
   
                txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                $target.find(".modal-body").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                
                if($('#table-persons').length > 0) {
                    //$('#table-persons').bootstrapTable('refresh',  {url: '../json/data1.json'});
                    $('#table-persons').bootstrapTable({
                        url: $(this).data("url"),
                        idField: 'id',
                        uniqueId: 'id',
                        columns: [
                                    { field: 'id',title: 'ID', visible: false},
                                    { field: 'firstname',title: 'Imię'},
                                    { field: 'lastname', title: 'Nazwisko'}, 
                                    { field: 'position',title: 'Stanowisko'},
                                    { field: 'contact',title: 'Kontakt'},
                                    { field: 'actions',title: '', 'events': 'actionEvents'},
                                ]
                    });
                }
                
                $('.selecctall').click(function(event) {  //on click
                    //console.log('.'+$(this).attr('id'));
                    if(this.checked) { // check select status
                        $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                            this.checked = true;  //select all checkboxes with class "checkbox1"              
                        });
                    }else{
                        $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                            this.checked = false; //deselect all checkboxes with class "checkbox1"                      
                        });        
                    }
                });
                
                 relationship();
                 //KAPI.meeting.afterSaveFalse(txtError, unix, data);
            } else { 
                /*location.reload();*/
                KAPI.resources.afterSaveTrue(txtError, unix, data);
                KAPI.tasks.afterSaveTrue(txtError, unix, data);
                KAPI.chat.afterSaveTrue(txtError, unix, data);
                KAPI.meeting.afterSaveTrue(txtError, unix, data);
                //console.log('ok');
                if($target.length > 0 ) {
                    if(data.close == false) {
                        $target.find(".modalContent").html(data.html);
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
						var unix = Math.round(+new Date()/1000);
						$(".action-form").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else {
                        $target.modal("hide");
                    }
                }
                if($("#calendar").length > 0) {
                    $("#calendar").fullCalendar( 'refetchEvents' );     
                    if(data.alert) {
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					} else	{
						var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					}
					var unix = Math.round(+new Date()/1000);
                    $("#calendar").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					 setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                }
      
                if( $table.length == 0 && $("#table-events").length > 0 ) {   $table = $("#table-events");   }
                
                var unix = Math.round(+new Date()/1000);
                $($form).prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                
                if( $(".todo-list").length > 0 ) {
                    if(data.show) { 
                        $(".todo-list").prepend(data.listElement);
                    }
                    var unix = Math.round(+new Date()/1000);
                    $(".todo-list").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
					setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                }
                
                if($table.length > 0 ) {
                    
                    if(data.refresh == 'inline') {
                        $table.bootstrapTable('updateRow', {index: data.index, row: data.row });
                        $('tr[data-index='+data.index+']').addClass('success');
                        setTimeout(function(){
                            $('tr[data-index='+data.index+']').removeClass('success', 1000, "easeInBack");
                        }, 1000);
                    } else {
                    
                        var o = {};
                        //console.log($('#form-data-search').serializeArray());
                        $.each($('#form-data-search').serializeArray(), function() {
                            if (o[this.name] !== undefined) {
                                if (!o[this.name].push) {
                                    o[this.name] = [o[this.name]];
                                }
                                o[this.name].push(this.value || '');
                            } else {
                                o[this.name] = this.value || '';
                            }
                        });
                        $table.bootstrapTable('refresh', { url: $('#table-data').data("url"), method: 'get',  query: o  });
                        //$table.find('tbody tr:eq(0)').addClass('success');
                    }
				
					if(data.alert) {
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					} else	{
						var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					}
					var unix = Math.round(+new Date()/1000);
					$table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					 setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
				
				
					if(data.action =="insertRow") {
						//console.log('insert');
						$table.bootstrapTable('selectPage', 1);
						$('tr[data-index='+data.index+']').addClass('success');
					}
			    }
             
				if($input.length) {
                    var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
                    var values = [];
					$input.prepend(opt);
                   
                    values.push(data.id);
					$input.val(values).trigger('change');
					                    
                    if($input.hasClass('ms-options')) {
                        $('#correspondence-addresses_list').multiselect('rebuild');
                        $('#correspondence-addresses_list').multiselect('refresh');
                    }
                  
					$target.modal("hide");
                }
                
                if(data.action == "caccount") { 
                    $(".crm-account-create").remove();
                    $(".crm-accounts-info").html(data.info);
                }
                
                if(data.action == "cpaccount") {
                    $(".crm-accounts-info-peronals").html(data.info);
                }
                
                if(data.action == "climits") { 
                    $("#crm-debt-limit_credit").html(data.limits.credit);
                }
                
                if(data.action == "debtLock") {  
                    $("#crm-debt-info").html(data.info);
                }
                
                if(data.action == 'suborderAdd') {
                    $("ul#suborders").append(data.item);
                }
				
				if(data.action == "matterStatus") {
					if($("#matter-update-status").length)
						$("#matter-update-status").html(data.html);
				}
				
				if(data.action == 'invItemAdd') {
                    $("#inv-items > body").prepend(data.item);
                }
				
				if(data.action == 'invItemEdit') {
                    $("tr#item"+data.id).html(data.item);
                }
				
				if(data.action == 'invItemDel') {
                    $("tr#item"+data.id).remove();
                }
				
				if(data.action == 'alertDone') {
					$("#alertWidget").html(data.alertWidget);
					$("#alertWidget").addClass('open');
					
					if($("#table-history").length) {
						$("#table-history").bootstrapTable('refresh');
					}
					
					if($("#table-timeline").length) {
						$("#table-timeline").bootstrapTable('refresh');
					}
				}
                
                if(data.action == 'generate') {
                    if($("#generate-tasks").length) {
                        $("#generate-tasks").append(data.build_list);
                    }
                }
                
                if(data.action == 'invItem') {
                    if($("#inv-items").length) {
                        $("#item-" + data.id+" td[data-column='2']").html(data.row.name);
                    }
                }
				
				relationship();
            }
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
			$target.find(".modalContent").removeClass('preload');
            //alert('Something went to wrong.Please Try again later...');
            CheckForSession();
        }
    });
    return false;
});

$(document).on("submit", ".modalAjaxFormWithAttach", function (event) { 
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target")); 
    var $table = $($form.attr("data-table"));
	var $input = $($form.attr("data-input"));
    var $model = $form.attr("data-model");
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    var formData = new FormData();
    
    var other_data = $(this).serializeArray();
    $.each(other_data,function(key,input){
        formData.append(input.name,input.value);
    });
    //console.log(formData);
    //formData.append( 'Files[files][]', $(".ufile")[0].files[0] );
    /*$.each( $(".attachs"), function(i, file) {
        //ajaxData.append('photo['+i+']', file);
        formData.append( 'Files[files]['+i+']', file[0] );
    } );*/
    var file_data,arr; var c=0;
    $('input[type="file"]').each(function(){
        file_data = $('input[type="file"]')[c].files; // get multiple files from input file
        //console.log(file_data);
        for(var i = 0;i < file_data.length;i++){ 
            formData.append($model+'[attachs]['+c+']', file_data[i]); // we can put more than 1 image file
        }
        c++;
    }); 
    //formData.append( 'Files[title_file]', $(".ufile-name-"+$typeId).val());
    //formData.append( 'Files[id_dict_type_file_fk]', $(".ufile-type-"+$typeId).val());
    //formData.append( 'Files[id_type_file_fk]', $(this).data("type"));
    //formData.append( 'Files[id_fk]', $(this).data("id"));
    
    $target.find(".modalContent").addClass('preload');
    $.ajax({
        type: "POST",
        //dataType: 'json',
        data: formData,
        async: false,
        url: $(this).attr('action'),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {   },
        success: function(data) {
            $target.find(".modalContent").removeClass('preload');
			if(data.success == false) { 
               
                $target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '';
                $.each(arr, function(index, value)  {
                    if (value.length != 0)  {
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
   
                txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                $target.find(".modal-body").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                relationship();
            } else {
                if($target.length > 0 ) {
                    if(data.close == false) {
                        $target.find(".modalContent").html(data.html);
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
						var unix = Math.round(+new Date()/1000);
						$(".action-form").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else {
                        $target.modal("hide");
                    }
                }
                
                if( $table.length == 0 && $("#table-events").length > 0 ) {   $table = $("#table-events");   }
                
                if($table.length > 0 && data.refresh == 'yes') {
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    $table.bootstrapTable('refresh', { url: $table.data("url"), method: 'get',  query: o  });
                    
					/*if(data.alert) {
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					} else	{
						var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					}
					var unix = Math.round(+new Date()/1000);
					$table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);*/
			    }

                if(data.action == 'add_note') {
                    showNotification('success', data.alert);
                    if(data.parent == 0) {
                        //$(".chat-box-history").prepend(data.html);
                        $("#chat-all").prepend(data.html);
                        $("#empty-0").remove();
                        $("#chat-"+data.type).prepend(data.html);
                        $("#empty-"+data.type).remove();
                    } else {
                        $(".chat-box-history #comment-"+data.parent).append(data.html);
                    }
                }
                
                if(data.action == 'add_msg') {
                    showNotification('success', data.title, data.alert); 
                }
            }
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
			$target.find(".modalContent").removeClass('preload');
            alert('Something went to wrong.Please Try again later...');
        }
    });
    return false;
});

$(document).on("submit", ".modalAjaxFormFile", function (event) { 
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target")); 
    var $table = $($form.attr("data-table"));
	var $input = $($form.attr("data-input"));
    $target.find(".modalContent").addClass('preload');
    
    var file_data = $('#import_file')[0].files[0]; 
    var formData = new FormData(); 
    formData.append( 'DebtReport[company_fk]', $("#import_file-company").val());
    if(file_data)
        formData.append("DebtReport[file]", file_data);  
   
    $.ajax({
        type: "POST",
		url: $form.attr("action"),
        data:  formData,
        //async: false,
        cache: false,
        contentType: false,
        processData: false,
        /*beforeSend: function() { 
            //$("#validation-form").hide().empty(); 
        },*/
        success: function(data) {
            $target.find(".modalContent").removeClass('preload');
			if(data.success == false) { 
               
                $target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '';
                $.each(arr, function(index, value)  {
                    if (value.length != 0)  {
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
   
                txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                $target.find(".modal-body").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
            } else { 
                if($target.length > 0 ) {
                    $target.modal("hide");
                }

                if( $table.length == 0 && $("#table-events").length > 0 ) {   $table = $("#table-events");   }
                
                var unix = Math.round(+new Date()/1000);
                $($form).prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
				
				$("#company-"+data.company).text(data.load_date);
                
                if($table.length > 0 ) {
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    $table.bootstrapTable('refresh', { url: $('#table-data').data("url"), method: 'get',  query: o  });
                    //$table.find('tbody tr:eq(0)').addClass('success');
                    $('tr[data-index='+data.index+']').addClass('success');
                    setTimeout(function(){
                        $('tr[data-index='+data.index+']').removeClass('success', 1000, "easeInBack");
                    }, 1000);
				
					if(data.alert) {
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					} else	{
						var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					}
					var unix = Math.round(+new Date()/1000);
					$table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
			    }
            }
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
			$target.find(".modalContent").removeClass('preload');
            //alert('Something went to wrong.Please Try again later...');
            CheckForSession();
        }
    });
    return false;
});

$(document).on("submit", ".correspondenceSend", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    //$('#corresppndence-send').prop('disabled', true);
    $('#corresppndence-send').attr('disabled','disabled'); 
    var $sendPath = $(this).attr("action");
    var $target = $($(this).attr("data-target"));
    $table = $("#table-box");
    $target.modal("hide");
    
    $table.parent().parent().parent().prepend('<div id="alert-sending" class="alert alert-info alert-dismissible" style="margin-bottom:5px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>Trwa rozsyłanie poczty....</div>');
    
   // window.location.replace($exportPath+'?'+$(this).serialize());
    $.ajax({
		type: "POST",
       // data: $("#form-data-search").serialize(),
		url: $sendPath,
        //dataType: 'binary',
        //headers:{'Content-Type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','X-Requested-With':'XMLHttpRequest'},
		success: function(data, status) {
           /* console.log($exportPath+'?'+$("#form-data-search").serialize());
            window.location.replace($exportPath+'?'+$("#form-data-search").serialize());*/
             $("#alert-sending").remove();
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
               
               // $('#corresppndence-send').prop('disabled', false);
               $('#corresppndence-send').removeAttr('disabled'); 
            });
            
            var o = {};
            //console.log($('#form-data-search').serializeArray());
            $.each($('#ffilter-box-search').serializeArray(), function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            
            $('#table-box')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                url: $('#table-box').data("url"), method: 'get',
                query: o
                
                //queryParams:function(p){
                //    return  { main_cat_id : 1 } /*$('#form-data-search').serialize()*/
                //}
            });
		},
        error: function(data) {
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
            });
        }
	});
    
    return false;
});

$(document).on("submit", ".correspondenceSendByPosting", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    //$('#corresppndence-send').prop('disabled', true);
    $('#corresppndence-send').attr('disabled','disabled'); 
    $('.dropdown-toggle').prop('disabled', true);
    var $sendPath = $(this).attr("action");
    var $target = $($(this).attr("data-target"));
    $table = $("#table-box");
    $target.modal("hide");
    
    $table.parent().parent().parent().prepend('<div id="alert-sending" class="alert alert-info alert-dismissible" style="margin-bottom:5px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>Trwa rozsyłanie poczty...</div>');
    
   // window.location.replace($exportPath+'?'+$(this).serialize());
    $.ajax({
		type: "POST",
        data: $(this).serialize(),
		url: $sendPath,
        //dataType: 'binary',
        //headers:{'Content-Type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','X-Requested-With':'XMLHttpRequest'},
		success: function(data, status) {
           /* console.log($exportPath+'?'+$("#form-data-search").serialize());
            window.location.replace($exportPath+'?'+$("#form-data-search").serialize());*/
             $("#alert-sending").remove();
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
               
               // $('#corresppndence-send').prop('disabled', false);
               $('#corresppndence-send').removeAttr('disabled'); 
                $('.dropdown-toggle').prop('disabled', false);
               if(data.success) {
                    var $idItem = $('input[name=postingDate]').val();// console.log($idItem);
                    $("li#posting-"+$idItem).remove(); //console.log($("#postingDates-list li").length);
                    if($("#postingDates-list li").length == 0) {
                        $("#postingDates-btn").remove();
                    }
               }
            });
            
            var o = {};
            //console.log($('#form-data-search').serializeArray());
            $.each($('#filter-box-search').serializeArray(), function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            
            $('#table-box')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                url: $('#table-box').data("url"), method: 'get',
                query: o
                
                //queryParams:function(p){
                //    return  { main_cat_id : 1 } /*$('#form-data-search').serialize()*/
                //}
            });
		},
        error: function(data) {
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
            });
        }
	});
    
    return false;
});

$(document).on("submit", ".periodSend", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    //$('#corresppndence-send').prop('disabled', true);
    //$('#corresppndence-send').attr('disabled','disabled'); 
    var $sendPath = $(this).attr("action");
    var $target = $($(this).attr("data-target"));
    $table = $($(this).attr("data-table"));
    $target.modal("hide");
    if($(this).attr("data-type") == 'accept') {
		$table.parent().parent().parent().prepend('<div id="alert-sending" class="alert alert-info alert-dismissible" style="margin-bottom:5px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>Trwa wysyłanie informacji do działu księgowości....</div>');	
		$("#period-accept-"+$(this).attr("data-id")).remove();	
	} else {
		$table.parent().parent().parent().prepend('<div id="alert-sending" class="alert alert-info alert-dismissible" style="margin-bottom:5px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>Trwa rozsyłanie informacji do kierowników....</div>');
    }
   // window.location.replace($exportPath+'?'+$(this).serialize());
    $.ajax({
		type: "POST",
        data: $(this).serialize(),
		url: $sendPath,
		success: function(data, status) {
            $("#alert-sending").remove();
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
            
            $table.bootstrapTable('refresh');	
		},
        error: function(data) {
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
            });
        }
	});
    
    return false;
});

$(document).on("submit", ".periodGen", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    //$('#corresppndence-send').prop('disabled', true);
    //$('#corresppndence-send').attr('disabled','disabled'); 
    var $sendPath = $(this).attr("action");
    var $target = $($(this).attr("data-target"));
    $table = $($(this).attr("data-table"));
    $target.modal("hide");

	$table.parent().parent().parent().prepend('<div id="alert-sending" class="alert alert-info alert-dismissible" style="margin-bottom:5px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>Trwa wysyłanie żądania wygenerowania faktur....</div>');
    
   // window.location.replace($exportPath+'?'+$(this).serialize());
    $.ajax({
		type: "POST",
        data: $(this).serialize(),
		url: $sendPath,
		success: function(data, status) {
            $("#alert-sending").remove();
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
            
            $table.bootstrapTable('refresh');	
		},
        error: function(data) {
            $table.parent().parent().parent().prepend('<div id="alert-send" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.info +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
            });
        }
	});
    
    return false;
});

$(document).on("click", ".google-data", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    //$('#corresppndence-send').prop('disabled', true);
    var $sendPath = $(this).attr("href");
    var $target = $($(this).attr("data-target"));

   // window.location.replace($exportPath+'?'+$(this).serialize());
    $.ajax({
		type: "POST",
        data: $(this).serialize(),
		url: $sendPath,
        //dataType: 'binary',
        //headers:{'Content-Type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','X-Requested-With':'XMLHttpRequest'},
		success: function(data, status) {
           $target.modal("hide");
           if( $('#personal').length > 0 )
			$('#personal').fullCalendar('refetchEvents');
		},
        error: function(data) {
            $('#alert-send').delay(2000).fadeOut(600, function(){
                $(this).remove();
            });
        }
	});
    
    return false;
});

$(document).on("change", "#calendar-all-day", function(event) {
    //$("#task_toTime").datepicker("option", "disabled", true);
    if(event.target.checked) {
        $("#task_toTime > input").prop('disabled', true).val('');
        $("#task_fromTime > input").prop('disabled', true).val('');
    } else {
        $("#task_toTime > input").prop('disabled', false);
        $("#task_fromTime > input").prop('disabled', false);
    }
});


$(document).on("click", "#invoice-generate", function(event) {
    //JSON.stringify($table.bootstrapTable('getData'))
    if($('#table-settled').bootstrapTable('getSelections').length > 5) {
        swal({
                title: 'Uwaga!',
                text: 'Aby skorzytsać z tej funkcjonalności nie możesz wybrać więcej niż 5 pozycji',
                type: 'error',
                confirmButtonText: 'Rozumiem'
            });
    } else {
        $.ajax({
            type: 'POST',
            url: '/accounting/invoice/gen',
            data: {items: JSON.stringify($('#table-settled').bootstrapTable('getSelections'))},
            success: function(data, status) {
                $('#table-settled').bootstrapTable('refresh');
                $('#table-periods').bootstrapTable('refresh');
                var unix = Math.round(+new Date()/1000);
                //$('#table-settled').bootstrapTable('resetView');
                if(data.success) {
                    $(".div-table-settled").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
                    if($("#chart-main").length) {
                        $("#table-crmstats").bootstrapTable('refresh');
                              
                        window.myMixedChart.data.datasets[0].data = data.stats.rates;
                        window.myMixedChart.data.datasets[1].data = data.stats.profits;
                        window.myMixedChart.update(); 
                    }
                } else {
                    var arr = data.errors; 
                    var txtError = '';//'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    $.each(arr, function(index, value) {
                        if (value.length != 0) {
                           // $("#validation-form").removeClass('none').removeClass('alert-success').addClass('alert-danger').append('<strong>'+ value +'</strong><br/>');
                            txtError += '<strong>'+ value +'</strong><br/>';
                        }
                    });
                    $(".div-table-settled").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
                }
                //setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
            }, error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                //alert('Something went to wrong.Please Try again later...');
            }
        });
    }
});

function relationship() {
    
    $("input.number").keyup(function(e){
        var key = e.which ? e.which : event.keyCode;
        if(key == 110 || key == 188){
          e.preventDefault();
          var value = $(this).val();         
          $(this).val(value.replace(",","."));
        }   
    });
    
    if($('#table-persons').length > 0) {
        //$('#table-persons').bootstrapTable('refresh',  {url: '../json/data1.json'});
        $('#table-persons').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'firstname',title: 'Imię'},
                        { field: 'lastname', title: 'Nazwisko'}, 
                        { field: 'position',title: 'Stanowisko'},
                        { field: 'contact',title: 'Kontakt'},
                        { field: 'image',title: ''},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if($('#table-events').length > 0) {
        //console.log('events');
        //$('#table-persons').bootstrapTable('refresh',  {url: '../json/data1.json'});
        $('#table-events').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'name',title: 'Tytuł'},
                        { field: 'date_start', title: 'Start'}, 
                        { field: 'date_end',title: 'Koniec'},
                        { field: 'employees',title: 'Pracownicy'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if( $("#table-rates").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-rates').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'type',title: 'Typ pracownika'},
                        { field: 'employee',title: 'Pracownik'},
                        { field: 'rate',title: 'Stawka'},
                        { field: 'date_from', title: 'Obowiązuje od'}, 
                        { field: 'date_to',title: 'Obowiązuje do'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if( $("#table-discounts").length > 0 ) {  
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-discounts').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'period',title: 'Okres'},
                        { field: 'amount',title: 'Rabat [%]'},
                        { field: 'name',title: 'Nazwa'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
   
    if( $("#table-sides-e").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-sides-e').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'role',title: 'Rola'},
                        { field: 'name',title: 'Nazwa'},
                        { field: 'contact',title: 'Kontakt'},
                        { field: 'note',title: '<i class="fa fa-comment"></i>'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    /*if( $("#table-settlements").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-settlements').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'customer',title: 'Klient'},
                        { field: 'order',title: 'Zlecenie'},
                        { field: 'discount',title: 'Rabat [%]'},
                    ]
        });
    }*/
    
    /*if($("#table-settled").length > 0) { 
        //$("#table-settled").bootstrapTable('resetView');
        $("#table-settled").bootstrapTable('refresh');
    }*/
    
    if($('fieldset.collapsible > legend > span').length == 0)
        $('fieldset.collapsible > legend').append(' (<span style="font-family: monospace;">+</span>)');
        
    $('fieldset.collapsible > legend').off('click').click(function () {
        //console.log('fieldset collapsible click');
        var $divs = $(this).siblings();
        $divs.toggle();

        $(this).find('span').text(function () {
            return ($divs.is(':visible')) ? '-' : '+';
        });
    });
    $('.selecctall').click(function(event) {  //on click
        //console.log('.'+$(this).attr('id'));
        if(this.checked) { // check select status
            $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1" 
            });
        }else{
            $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
    
    $('#imgAttachmentPerson').imageAttachment({
        "hasImage":$('#imgAttachmentPerson').data('hasimage'),
        "previewUrl":$('#imgAttachmentPerson').data('previewurl'),
        "previewWidth":300,
        "previewHeight":200,
        "apiUrl":$('#imgAttachmentPerson').data('apiurl')
    });
    
    $('.select2').select2({dropdownAutoWidth : true,  width: '100%'});
    $("select.chosen-select").chosen({disable_search_threshold:10});
    $('.ms-resource').multiselect({includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        nonSelectedText: 'wybierz opcje'
    });
    $('.ms-select-ajax').multiselect({
        includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        nonSelectedText: 'wybierz opcje',
        onChange: function (option, checked) {
            //get all of the selected tiems
            var values = $('.ms-select-ajax option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            //update hidden field with comma delimited string for state persistance
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
                        
            if($("#menu-list-employee-task").length > 0 ){
                type = $('.ms-select-ajax').data('type');
                if( $("#calcase-type_fk").length ) {
                    if($("#calcase-type_fk").val() == 1) type = 'matter'; else type = 'project';
                }
                var box = ( $('.ms-select-ajax').data('box') ) ? $('.ms-select-ajax').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+type+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(dataResult, status) {
                        if(dataResult.dropdown) {
                            $("#menu-list-employee-task").html(dataResult.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        } 
                    }
                });
            }
         
            if($("#groupaction-employees_list").length > 0 ){ 
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type=group&box=0',
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(dataResult, status) {
                        if(dataResult.list) {
                            $("#groupaction-employees_list").html(dataResult.dropdown);
                        } else {
                            $("#groupaction-employees_list").html('');
                        }
                        $("#groupaction-employees_list").trigger("chosen:updated");
                    }
                });
            }
        },
    });
	
    
   /* $("#caltask-id_case_fk").change(function() {
    });*/
}


window.actionEvents = {
	'click .up': function (e, value, row, index) {
         //console.log('up');
        if(index > 0) {
            var source = JSON.stringify($('#table-actions').bootstrapTable('getData')[index]);
            var target = JSON.stringify($('#table-actions').bootstrapTable('getData')[index - 1]);
            $('#table-actions').bootstrapTable('updateRow', {'index':index - 1, 'row': JSON.parse(source)});
            $('#table-actions').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(".info-message").addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-actions-move").removeClass("none");
        }
	},
	'click .down': function (e, value, row, index) { 
        //console.log( ($('#table-items tr').length-2) + ' > ' + (index));
        if(($('#table-actions tr').length-2) > (index)) {
            var source = JSON.stringify($('#table-actions').bootstrapTable('getData')[index]);
            var target = JSON.stringify($('#table-actions').bootstrapTable('getData')[index + 1]);
            $('#table-actions').bootstrapTable('updateRow', {'index':index + 1, 'row': JSON.parse(source)});
            $('#table-actions').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(".info-message").addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-actions-move").removeClass("none");
        }
	},
	'click .update': function(event, value, row, index) {
		$target = $(this).attr('data-target');
		$($target).find(".modalContent").addClass('preload');
		event.stopImmediatePropagation();
		event.preventDefault();
	    var tagname = $(this)[0].tagName;   
		if($("#dictionarysearch-id").length > 0)
			var $action = $(this).attr("href")+'/'+$("#dictionarysearch-id").val()+'?index='+index;
		else
			var $action = $(this).attr("href")+'?index='+index;
		$($(this).attr('data-target')).find(".modal-title").html($(this).data("title"));
        $($(this).attr('data-target')).find(".alert").remove();
	    $($(this).attr('data-target')).modal("show")
				  .find(".modalContent")
				  .load($action, function( response, status, xhr ) {
									if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
                                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
									}
									$($target).find(".modalContent").removeClass('preload');
								}
                    );
                  
        $( "#success" ).load( "/not-here.php", function( response, status, xhr ) {
          if ( status == "error" ) {
            var msg = "Sorry but there was an error: ";
            $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
          }
		  $($target).find(".modalContent").removeClass('preload');
        });          
                  
		$($(this).attr('data-target')).on('shown.bs.modal', function() {		   
		    relationship();
			$($target).find(".modalContent").removeClass('preload');
		});
		
	   return false;   
	},
    'click .remove': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $('#modalConfirmDelete').find(".modal-title i").addClass('fa-trash').removeClass('fa-lock');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Usuń');
        if( $(this).attr('data-label') ) {
			$("#btnYesDelete").html($(this).data('label'));
		}
        
        var o = {};
        //console.log($('#form-data-search').serializeArray());
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'get',
            query: o
            
            //queryParams:function(p){
            //    return  { main_cat_id : 1 } /*$('#form-data-search').serialize()*/
            //}
        });
	},
    'click .lock': function (e, value, row, index) { 
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Zablokuj');
        
       
	},
    'click .unlock': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Odblokuj');
	},
    'click .inlineConfirm': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
                   
        var id = $(this).data('id'), 
        table = $(this).data('table'); 
        
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass($(this).data('icon'));
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Potwierdź');
        if( $(this).attr('data-label') ) {
			$("#btnYesDelete").html($(this).data('label'));
		}
        
        $(table).find('.dropdown-menu').hide();
        /*$(table).on('hide.bs.dropdown', function (e) {
            $(e.target).append(dropdownMenu.detach());
        });*/
	},
     'click .inlineEdit': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
        
        $target = $($(this).attr('data-target'));
		$($target).find(".modalContent").addClass('preload');
		
	    var tagname = $(this)[0].tagName;   
		$target.find(".modal-title").html($(this).data("title"));
        var $action = $(this).attr("href")+'?index='+index;
  
        $target.find(".modalContent")
              .load($action, 
                    function(response, status, xhr){ 

                        $target.modal('show'); 
                        
                        if ( status == "error" ) {
                            var msg = "Sorry but there was an error: ";
                            $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                        } 
                        //$target.find(".modalContent").removeClass('preload');
                        setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
               });          
        $target.on('shown.bs.modal', function() {
            relationship();
            $target.find(".modalContent").removeClass('preload');
            
        });
		
	   return false;  
	},
    'click .starred': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
                   
        var id = $(this).data('id'), 
        table = $(this).data('table'); 
        
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass('fa-star');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Oznacz');
        $("#btnYesDelete").html('Usuń');
        if( $(this).attr('data-label') ) {
			$("#btnYesDelete").html($(this).data('label'));
		}
	}
};

