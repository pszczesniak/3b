'use strict';

/**
 *
 * Universal function for animation sections while scrolling
 * Waypoints dependency needed
 *
 */

var UTILS = UTILS || {};

UTILS._animationedSections = function _animationedSections() {
    var animation = {
            runAnimation: function(section, animClassName) {
                const splittedAnimClassName = animClassName.split('-');

                setTimeout(function() {
                    section.classList.remove(animClassName);
                }, 1200);
                section.classList.add('animated');
                section.classList.add(splittedAnimClassName[1]);
                section.classList.add('active');

                setTimeout(function() {
                    if (splittedAnimClassName[1] !== 'easeOutBounceMagicWidthLong') {
                        section.classList.remove('animated');
                        section.classList.remove(splittedAnimClassName[1]);
                        section.classList.remove('active');
                    }
                }, 1200);
            }
        },
        offSetSlider = '85%',
        items,
        section,
        animationClassTable = ['animation-fadeInUp', 'animation-fadeInDown', 'animation-easeOutBounceSkewLeftMd', 'animation-easeOutBounceSkewRightMd', 'animation-easeOutBounceRotateLeftMd', 'animation-easeOutBounceRotateRightMd', 'animation-easeOutBounceRotateRightZero', 'animation-bounceIn',      'animation-easeOutBounce'],
        animationClassTableNoWaypoint = ['animation-easeOutBounceMagicWidth', 'animation-easeOutBounceMagicWidthLong'],
        i;

    for (i = 0; i < animationClassTable.length; i++) {
        let dotClassName = `.${animationClassTable[i]}`,
            className = animationClassTable[i];

        if (document.querySelector(dotClassName)) {
            items = document.querySelectorAll(dotClassName);

            [].forEach.call(items, function(item) {
                section = new Waypoint({
                    element: item,
                    handler: function(direction) {
                        if (direction === 'down') {
                            animation.runAnimation( item, className );
                            this.destroy();
                        }
                    },
                    offset: offSetSlider
                });
            });
        }
    }



    // without waypoints
    for (i = 0; i < animationClassTableNoWaypoint.length; i++) {
        let dotClassName = `.${animationClassTableNoWaypoint[i]}`,
            className = animationClassTableNoWaypoint[i];

        if (document.querySelector(dotClassName)) {
            items = document.querySelectorAll(dotClassName);

            [].forEach.call(items, function(item) {
                setTimeout(function() {
                    animation.runAnimation( item, className );
                }, 200);
            });
        }
    }
};
