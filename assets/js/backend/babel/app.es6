'use strict';

let KAPI = KAPI || {};
let UTILS = UTILS || {};
let static_folder = '/images';

KAPI.utils = UTILS || {};

KAPI.metaTags = {};

KAPI.position = $(window).scrollTop();
KAPI.utils.navbarPosition = function() {
    let scroll;

    if ( document.querySelector('.l-navbar') && !document.querySelector('body').classList.contains('stop-scrolling') ) {
        scroll = window.scrollY || window.pageYOffset  || document.documentElement.scrollTop;

        if (scroll > 20) {
            document.querySelector('.l-navbar').classList.add('l-navbar--white');
        } else {
            document.querySelector('.l-navbar').classList.remove('l-navbar--white');
        }

        KAPI.position = scroll;
    }
};

KAPI.utils.inputLabels = function inputLabels() {
    function placeholderPolyfill(el) {
      this.classList[this.value ? 'add' : 'remove']('filled');
    }

    if ( document.querySelector('input') ) {
        var allInputs = document.querySelectorAll('input');
        var allTextareas = document.querySelectorAll('textarea');

        [].forEach.call(allInputs, function(singleInput) {
            singleInput.classList[singleInput.value ? 'add' : 'remove']('filled');
            singleInput.addEventListener('change', placeholderPolyfill);
            singleInput.addEventListener('keyup', placeholderPolyfill);
        });

        [].forEach.call(allTextareas, function(singleTextarea) {
            singleTextarea.classList[singleTextarea.value ? 'add' : 'remove']('filled');
            singleTextarea.addEventListener('change', placeholderPolyfill);
            singleTextarea.addEventListener('keyup', placeholderPolyfill);
        });
    }
};

KAPI.utils.waypointCallback = function() {
    KAPI.utils._animationedSections();
};


KAPI.utils.formPopup = function formPopup(popupType, message, borderInfo) {
    let borderInfoHtml = '';

    if (borderInfo.length) {
        borderInfoHtml = `<p class="popup-modal__border-info">${borderInfo}</p>`;
    }

    $.magnificPopup.open({
        items: {
            src: `<div class="popup-modal">
                    <img class="popup-modal__icon" src="/images/icons/${popupType}.svg" alt="">
                    <h2 class="popup-modal__headline">${message}</h2>
                    ${borderInfoHtml}
                </div>`,
            type: 'inline'
        },
      markup: '<div class="mfp-figure">'+
                    '<div class="mfp-img"></div>'+
                    '<div class="mfp-bottom-bar">'+
                    '<div class="mfp-title"></div>'+
                    '<div class="mfp-counter"></div>'+
                    '<p class="mfp-close"></p>'+
                    '</div>'+
                    '<p class="mfp-close"></p>'+
                '</div>',

    callbacks: {
        open: function() {
            $('.l-navbar, header, .l-main, aside, .l-footer').addClass('blurry');
            $('.l-navbar, header, .l-main, aside, .l-footer').addClass('blurry--trans');
        },
        close: function() {
            $('.l-navbar, header, .l-main, aside, .l-footer').removeClass('blurry');

            setTimeout(function() {
                $('.l-navbar, header, .l-main, aside, .l-footer').addClass('blurry--trans');
            }, 500);
        }
    }
    });

    return false;
}

KAPI.mainMenu = {
    burger: function() {
        if ( document.querySelector('.js-burger') ) {
            var burgerButton = document.querySelector('.js-burger'),
                burgerTarget = document.querySelector('.js-burger-target');

            burgerButton.addEventListener("click", function( event ) {
                event.preventDefault();
                burgerTarget.classList.contains('open') ? burgerTarget.classList.remove('open') : burgerTarget.classList.add('open');
                burgerButton.classList.contains('burger--open') ? burgerButton.classList.remove('burger--open') : burgerButton.classList.add('burger--open');
                burgerButton.classList.contains('burger--open') ? document.body.classList.add('burger--open') : document.body.classList.remove('burger--open');
                burgerButton.classList.contains('burger--open') ? document.documentElement.classList.add('burger--open') : document.documentElement.classList.remove('burger--open');

            }, false);
        }
    },

    init: function() {
        this.burger();
    }
};

KAPI.carousel = {
    logos: function() {
        const carouselSettings = {
            loop: true,
            margin: 100,
            nav: true,
            dots: false,
            autoplay: true,
            autoplayTimeout: 3000,
            responsive: {
                0: {
                    items: 1
                },
                568: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1140: {
                    items: 5
                }
            }
        };

        if ($('.js-logos').length) {
            $('.js-logos').owlCarousel(carouselSettings);
        }
    },
    init: function() {
        this.logos();
    }
};


KAPI.forms =  {
    fileUploadForm: function() {
        if ( document.querySelector('.js-file-form') ) {

            Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

                // The configuration we've talked about above
                autoProcessQueue: false,
                uploadMultiple: false,
                maxFilesize: 10, // MB
                clickable: '.dropzone-icon-zone',
                acceptedFiles: '.png, .jpg, .jpeg, .pdf, .eps, .ai',

                // addRemoveLinks: true,



                parallelUploads: 100,
                maxFiles: 2,
                // previewTemplate: document.querySelector('#filetpl').innerHTML,
                url: document.querySelector('.js-file-form').getAttribute('data-endpoint'),

                // The setting up of the dropzone
                init: function() {
                  var myDropzone = this;
                  var prevFile;
                  var fileForm = document.querySelector('.js-file-form');
                  var submitType = fileForm.getAttribute('data-submit-type');

                  // First change the button to actually tell Dropzone to process the queue.
                //   fileForm.querySelector("button[type=submit]").addEventListener("click", function(e) {
                  this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                    // Make sure that the form isn't actually being sent.
                    
                    // e.preventDefault();
                    // e.stopPropagation();

                    // myDropzone.processQueue();

                    if (myDropzone.files.length) {
                        // fileForm.setAttribute('data-submit-type', 'FILE');
                        // myDropzone.processQueue(); // upload files and submit the form

                        if(! $(fileForm)[0].checkValidity() ) {
                            e.preventDefault();
                            e.stopPropagation();
                        } else {
                            fileForm.setAttribute('data-submit-type', 'FILE');
                            myDropzone.processQueue(); // upload files and submit the form
                        }
                    } else {
                        $(fileForm).find('.form__response').remove();

                        if(! $(fileForm)[0].checkValidity() ) {
                            // e.preventDefault();
                            e.stopPropagation();
                        } else {
                            fileForm.setAttribute('data-submit-type', 'NORMAL');

                            // myDropzone.querySelector("button[type=submit]").trigger( "click" );

                            // $(fileForm).submit(); // submit the form
                            $(fileForm).find('.loading').fadeIn();
                        }


                        // $( e.target ).trigger( "click" );
                    }

                  });

                  // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
                  // of the sending event because uploadMultiple is set to true.
                  this.on("sendingmultiple", function(data, xhr, formData) {

                    // Gets triggered when the form is actually being sent.
                    // Hide the success button or the complete form.
                  });
                  this.on("successmultiple", function(files, response) {
                    // Gets triggered when the files have successfully been sent.
                    // Redirect user or notify of success.
                  });
                  this.on("errormultiple", function(files, response) {
                    // Gets triggered when there was an error sending the files.
                    // Maybe show form again, and notify user of error
                  });

                  this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                  });

                  this.on('addedfile', function(file) {

                    // replace dragged file with new one
                    if (prevFile) {
                        this.removeFile(prevFile);
                    }
                    prevFile = file;
                    document.querySelector('.js-dropzone-filename').innerHTML = file.name;
                  });

                //   this.on('drop', function(file, responseText) {
                //     prevFile = file;
                //     console.log('success');
                //     console.log(file);
                //   });

                  this.on("success", function(file, response) {
                    let message = 'File uploaded successfully',
                        borderInfo = '';

                    $(fileForm).find('.loading').fadeOut();
                    // KAPI.utils.formPopup('ok', message, borderInfo);


                    // response success information
                    if ( $(fileForm).find('.form__response--ok').length === 0 ) {
                        $(fileForm).prepend( '<div class="form__response form__response--ok"><p>'+response.message+'</p></div>' );
                    } else {
                        $('.form__response--ok p').html(response.message);
                    }

                    $(fileForm)[0].reset();

                  });

                  this.on("error", function(file, errorMessage) {
                    let sizeTooBig = 'File is too big',
                        wrongType = 'You can\'t upload files of this type',
                        borderInfo = '';


                    if ( errorMessage.indexOf(sizeTooBig) !== -1 ) {
                        errorMessage = 'Sorry but your file is too big';
                        borderInfo = 'File size limit: 10MB';
                    } else if ( errorMessage.indexOf(wrongType) !== -1 ) {
                        errorMessage = 'Sorry but your file is not supported';
                        borderInfo = 'Allowed files: JPG, PNG, PDF, EPS, AI';
                    }

                    KAPI.utils.formPopup('error', errorMessage, borderInfo);
                    this.removeFile(file);
                    document.querySelector('.js-dropzone-filename').innerHTML = '';

                    $(fileForm).find('.loading').fadeOut();
                  });
                }

              }
        }
    },

    init: function() {
        this.fileUploadForm();
    }
};



KAPI.refinement = {
    groupRevealed: null,
    getCurrentCategorySlug: function() {
        return $('[data-category]').attr('data-category');
    },
    getCategoryName: function(slug) {
        var name = $('[data-param="' + slug + '"]').html();

        if (name) {
            name = name.trim();
        }

        return name;
    },
    getLabelName: function(slug) {
        var name = $('input[value="' + slug + '"]').parent().find('.js-label-name').html();

        if (name) {
            name = name.trim();
        }

        return name;
    },
    getCurrentLabelSlug: function() {
        return $('input[name="labelSlug"]:checked').val();
    },
    filterBy: function(type, value, withoutTopicRefinement) {
        var categorySlug = KAPI.refinement.getCurrentCategorySlug();
        var labelSlug = KAPI.refinement.getCurrentLabelSlug();
        var url;

        if (type === 'category') {
            if (withoutTopicRefinement) {
                url = value;
                // KAPI.refinement.trackEvent(value);
            } else {
                url = value + (labelSlug ? '/' + labelSlug : '');
                // KAPI.refinement.trackEvent(value, labelSlug);
            }

        } else if (type === 'label') {
            url = categorySlug + '/' + value;
            // KAPI.refinement.trackEvent(categorySlug, value);

        } else {
            url = categorySlug;
            // KAPI.refinement.trackEvent(categorySlug);
        }

        history.pushState({url: url}, null, url);
        KAPI.pushStateCounter++;
        KAPI.refinement.requestContent(url);
    },
    // trackEvent: function(categorySlug, labelSlug) {
    //     var ga = window.ga || null;
    //     if (!ga) {
    //         return true;
    //     }

    //     var categoryName = KAPI.refinement.getCategoryName(categorySlug);
    //     var labelName = KAPI.refinement.getLabelName(labelSlug);
    //     var data = {
    //         'hitType': 'event',
    //         'eventCategory': 'Resources: ' + categoryName,
    //         'eventAction': labelSlug ? 'refined with' : 'selected',
    //         'eventLabel': labelSlug ? labelSlug : ''
    //     };

    //     ga('send', data);
    // },
    findParams: function(value) {
        if (value.indexOf('?') >= 0) {
            value += '/';
            return value;
        } else {
            value += '/?';
            return value;
        }
    },
    requestContent: function(url) {
        $('.loading').fadeIn();

        if (KAPI.infiniteElement) {
            KAPI.infiniteElement.destroy();
        }

        if ( url.slice(-1) === '/' ) {
            url = url.slice(0, -1);
        }

        url += (KAPI.refinement.groupRevealed ? '?groupRevealed=' + KAPI.refinement.groupRevealed : '');
        var newUrl = KAPI.refinement.findParams(url);

        if ( newUrl.slice(-1) === '/' ) {
            newUrl = newUrl.slice(0, -1);
        }

        setTimeout(function() {
            $.ajax({
                url: newUrl + Math.random(),
                dataType: 'html',
                success: function(responseText) {

                    $('#js-content').replaceWith(responseText);

                    var codes = $('#js-content').find("script");
                    for (var i=0; i<codes.length; i++) {
                        eval(codes[i].text);
                    }

                    document.title = KAPI.metaTags.title;
                    $('meta[name=description]').attr('content', KAPI.metaTags.description);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    Waypoint.destroyAll();
                    KAPI.utils._animationedSections();
                    KAPI.utils._ajaxPopup();
                    KAPI.refinement.init();
                    // KAPI.utils._ajaxPopupAttribute();
                    // imageLazyLoading();

                    setTimeout(function() {
                        // KAPI.utils.infiniteScroll(KAPI.utils.waypointCallback);
                        Waypoint.refreshAll();
                    }, 1000);

                    setTimeout(function() {
                        $('.loading').fadeOut();
                    }, 200);
                },

                error: function(responseText) {
                    setTimeout(function() {
                        $('.loading').fadeOut();
                    }, 200);
                }
            });
        }, 300);

        return false;
    },
    initPopstate: function() {
        window.addEventListener('popstate', function(e) {
            var character = e.state;

            if (character === null && KAPI.pushStateCounter) {
                location.reload();
            } else {
                KAPI.refinement.requestContent(character.url);
            }
        });
    },

    selectEvent: function() {

        $('.js-filter-ajax').on('change', function(e) {
            var self = this;
            var val = $(this).val();

            if (val !== '-1') {

                var filterType = self.getAttribute('data-filter-type'),
                    selectedOption = $(self).find(":selected"),
                    slug = val,
                    withoutTopicRefinement = selectedOption.data('no-topics') || false;

                KAPI.refinement.filterBy(filterType, slug, withoutTopicRefinement);
            }
        });

    },

    init: function() {
        this.selectEvent();
    }
};



KAPI.contact = {
    loadScript: function() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3&key= AIzaSyDKrMm3mlITZvtKAO-5vkiBVQvcB5AjnGE&' +
                'callback=KAPI.contact.map';
        document.body.appendChild(script);

    },

    map: function() {
        function init() {
                var draggable;
                var isMobile = function() {
                try{ document.createEvent("TouchEvent"); return true; }
                    catch(e){ return false; }
                };
                if(isMobile()) {
                    draggable = false;
                } else {
                    draggable = true;
                }

                var maps = document.querySelectorAll('.js-map');

                [].forEach.call(maps, function(mapObj) {

                    var myLatLngX = mapObj.getAttribute('data-latitude');
                    var myLatLngY = mapObj.getAttribute('data-longitude');

                    var mapOptions = {
                        zoom: 16,
                        center: new google.maps.LatLng(myLatLngX,myLatLngY),
                        scrollwheel: false,
                        draggable: draggable,

                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.LARGE,
                            position: google.maps.ControlPosition.RIGHT_TOP
                        }
                    };

                    var mapElement = mapObj;
                    var map = new google.maps.Map(mapElement, mapOptions);
                    var image = static_folder+'/map-marker.png';
                    var marker = new google.maps.Marker({
                        position: map.getCenter(),
                        icon: image,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        url: 'http://maps.google.com/?q='+myLatLngX+','+myLatLngY
                    });

                    google.maps.event.addDomListener(window, 'resize', function() {
                        var center = map.getCenter();
                        google.maps.event.trigger(map, 'resize');
                        map.setCenter(center);
                    });
                });
        }
        init();
    }
};





$(document).ready(function() {
    KAPI.utils.hashAnchorClick();
    KAPI.utils._animationedSections();
    // KAPI.utils._openPopupOnClick();
    // KAPI.utils._magnific();
    KAPI.utils.inputLabels();
    KAPI.mainMenu.init();
    KAPI.carousel.init();
    // KAPI.forms.init();
    // KAPI.utils.infiniteScroll(KAPI.utils.waypointCallback);

    if ($('#js-content').length) {
        KAPI.refinement.initPopstate();
    }

    KAPI.refinement.init();
});

KAPI.forms.init();

window.addEventListener('scroll', function() {
    KAPI.utils.navbarPosition();
}, false);

if ($('.js-map').length) {
    const mapWaypoint = new Waypoint({
        element: $('.js-map')[0],
        handler: function(direction) {
            if (direction === 'down') {
                KAPI.contact.loadScript();
            }
        },
        offset: '80%'
    });
}

