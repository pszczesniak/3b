$.fn.modal.Constructor.prototype.enforceFocus=function(){};

$(document).ready( function() { 
    KAPI.misc();
    
    /*$('#modal-grid-item').on('show.bs.modal', function() {
        if($('.selectAutoCompleteModal').length)     
            $('.selectAutoCompleteModal').select2({
                placeholder: 'wyszukaj',
                width: '100%',
                ajax: {
                    url: $('.selectAutoCompleteModal').data('url'),
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
    });*/
      
    $('#modal-grid-item').on('hidden.bs.modal', function() {
        //$('.selectAutoCompleteModal').select2('destroy');
        tinymce.remove(); 
    });
    
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if($('.table-widget').length)
            $('.table-widget').bootstrapTable('resetView');
    });
    
    $("body").tooltip({   
        selector: "[data-toggle='tooltip']",
        container: "body"
    })
    .popover({
        selector: "[data-toggle='popover']",
        container: "body",
        html: true
    });
	
	$("input.number").keyup(function(e){
        var key = e.which ? e.which : event.keyCode;
        if(key == 110 || key == 188){
          e.preventDefault();
          var value = $(this).val();         
          $(this).val(value.replace(",","."));
        }   
    });
    
    $('html').on('click', function(e) {
        if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
            $('[data-original-title]').popover('hide');
        }
    });
	
	$(function () {
		$('#colorPicker').colorpicker();
        $('.choseColor').colorpicker();
	});
    
    /*if($('.breadcrumb').length)
        $('.breadcrumb').asBreadcrumbs({
            namespace: 'breadcrumb'
        });*/
        
    $("#save-table-items-move").click(function() {
        var saveUrl = $(this).attr('href');
    //	var updateId = $(this).attr('id').split('-')[1]; console.log($("input#uitem-"+updateId));
        $.ajax({
            method: "POST",
            url: saveUrl,
            dataType : 'json',
            data: {items: $("#table-items").bootstrapTable('getData') }
        }).done(function( result ) {
            if(result.success) {
                $('.info-message').removeClass('alert-danger').removeClass('alert-info').addClass('alert alert-success').text(result.alert);
                $("#save-table-items-move").addClass("none");
            } else {
                $('.info-message').removeClass('alert-success').removeClass('alert-info').addClass('alert alert-danger').text(result.alert);
            }
        });
        return false;
    });
    
    if($('.chosen-select').length)
        $("select.chosen-select").chosen({disable_search_threshold:10});
	    
    if($('.select2').length)
        $('.select2').select2({dropdownAutoWidth : true,  width: '100%', allowClear: true, placeholder: '- wybierz -'});
	
	$('.selectAutoComplete').select2({
        placeholder: 'wyszukaj',
		width: '100%',
        allowClear: true,
        ajax: {
            url: $('.selectAutoComplete').data('url'),
            dataType: 'json',
            delay: 250,
           /* data: function(params) {
                return {
                    q: ((params.term) ? params.term : $('.selectAutoComplete').data('default')),
                    page: params.page
                };
            },*/
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        /*initSelection: function(element, callback) {
            return $.getJSON($('.selectAutoComplete').data('url') + '?id='+$('.selectAutoComplete').data('id'), null, function(data) {
                return callback(data);
            });
        }*/
        /*initSelection : function (element, callback) {
            var data = [];
            $(element.val().split(",")).each(function () {
                data.push({id: this, text: this});
            });
            callback(data);
        }*/
	});
	
	$('.ms-select').multiselect({
        includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        selectAllText: 'Zaznacz wszystko',
        nonSelectedText: 'wybierz opcje',
        onDeselectAll: function() {
            var ids = new Array(); var employees = new Array();  ids.push( 0 ); 
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
        onSelectAll: function() {
            var values = $('.ms-select option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
        onChange: function (option, checked) {
            var values = $('.ms-select option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
            if($("#menu-list-employee").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee").html(data.list);
                        } else {
                            $("#menu-list-employee").html('<li>brak danych</li>');
                        }
                        
                        if( $("#calcase-id_employee_leading_fk").length > 0 ) {
                            $("#calcase-id_employee_leading_fk").html(data.dropdown);
                        }
                    }
                });
            }
            
            if($("#menu-list-employee-task").length > 0 ){
                var box = ( $('.ms-select').data('box') ) ? $('.ms-select').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+$('.ms-select').data('type')+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
    });
    
    $('.ms-options').multiselect({includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        selectAllText: 'Zaznacz wszystko',
        nonSelectedText: 'wybierz opcje'
    });
    
    $(".schedule-filtering-options").on('change', function(e) {
        if( $('#schedule').length > 0 )
			$('#schedule').fullCalendar('refetchEvents');
        if( $('#personal').length > 0 )
			$('#personal').fullCalendar('refetchEvents');
        if( $('#timetable').length > 0 )
			$('#timetable').fullCalendar('refetchEvents');
    } );
});

$(document).on("click", ".viewModal", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $target = $($(this).attr('data-target')), $title = $(this).data("title"), $icon = $(this).data("icon"); 
    var $action = $(this).attr("href"); 
    if( $("#dictionarysearch-id").length > 0 ) {
        var valueId = $("#dictionarysearch-id").val(); 
        if( valueId != 0 && valueId != '' )
            var $action = $(this).attr("href")+'/'+valueId;
        else
            var $action = $(this).attr("href")+'/0';
    } else {
        var $action = $(this).attr("href");
    }

    $target.find(".modalContent").addClass('preload');

    $target.find(".modal-title").html('<i class="fa fa-'+$icon+' fa-2x"></i><span>'+$title+'</span>');
  
    $target.find(".modalContent")
           .load($action, 
                function(response, status, xhr){ 
                    $target.modal('show'); 
                    
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } 
                    //$target.find(".modalContent").removeClass('preload');
                    setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
                });   
       
    $target.on('shown.bs.modal', function() {
        relationship();
        $target.find(".modalContent").removeClass('preload');
    });
    
    $target.on('hide.bs.modal', function () {
       // $target.find(".modalContent").html('');
        tinymce.remove();  
        /*if($('.selectAutoCompleteModal').length) 
            $('.selectAutoCompleteModal').select2('destroy');*/

    });
  
    return false;   
});

$(document).on("submit", ".restForm", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var formId = $(this).attr('id');
    var formPrefix = $(this).data('prefix');
    var $target = $($(this).data('target'));  
    
    $("#"+formId+" > .help-block").html("");
    //console.log($(this).attr('action'));
    var xhr = new XMLHttpRequest();
    xhr.open($(this).attr('method'), $(this).attr('action'), true);
    xhr.setRequestHeader('Authorization', 'Bearer ' + '5cd56bcf');
    //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //xhr.setRequestHeader('X-CSRF-Token', _csrf);
    //xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    
    xhr.onreadystatechange=function()  {
        if (xhr.readyState==4 && xhr.status==200)  {
            var result = JSON.parse(xhr.responseText);
            $target.modal("hide");
           // setCookie('username', result.email, 30);           
        }
        
        if (xhr.readyState==4 && xhr.status==500)  {
            alert('error');          
        }
        
        if (xhr.readyState==4 && xhr.status==422)  {
            var result = JSON.parse(xhr.responseText);
            //console.log(result);
            
            result.forEach(function (element, index, array) {
                //console.log("[" + element.field + "] jest " + element.message);
                $(".field-"+formPrefix+"-"+element.field+" > .help-block").html(element.message);
            });
        }
    }
    //xhr.send(new FormData(document.getElementById(formId)));
    //xhr.send($(this).serialize());
    //var _postdata = 'firstname=' + 'Kamila' + '&lastname=' + 'Bajdowska';
    //xhr.send(encodeURI(_postdata));
    
    var form = new FormData(this);
    var formSet = document.getElementById(formId).elements;
    for(i = 0; i < formSet.length; i++) {
        /*var Name = Form[I].getAttribute('name');
        var Value = Form[I].value;
        alert(Name + ' : ' + Value);*/
        console.log(formSet[i].getAttribute('name')+" => "+formSet[i].value);
        //form.append(formSet[i].getAttribute('name'), formSet[i].value);
    }
    //form.append("blobbie", blob);
    xhr.send(form);
    
    return false;
});
	
$(document).on("submit", ".modalAjaxForm", function (event) { 
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target")); 
    var $table = $($form.attr("data-table"));
	var $input = $($form.attr("data-input"));
    var $model = $form.attr("data-model");
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    var formData = new FormData();
    
    var other_data = $(this).serializeArray();
    $.each(other_data,function(key,input){
        formData.append(input.name,input.value);
    });

    var file_data,arr; var c=0;
    $('input[type="file"]').each(function(){
        file_data = $('input[type="file"]')[c].files; // get multiple files from input file
        //console.log(file_data);
        for(var i = 0; i < file_data.length; i++){ 
            formData.append($model+'[attachs]['+c+']', file_data[i]); // we can put more than 1 image file
        }
        c++;
    }); 
    
    $target.find(".modalContent").addClass('preload');
    $.ajax({
        type: "POST",
        //dataType: 'json',
        data: formData,
        async: false,
        url: $(this).attr('action'),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {   },
        success: function(data) {
            $target.find(".modalContent").removeClass('preload');
			if(data.success == false) { 
               
                $target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '';
                $.each(arr, function(index, value)  {
                    if (value.length != 0)  {
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
                relationship();
                txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                $target.find(".modal-body").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);

            } else {
                if($target.length > 0 ) {
                    if(data.close == false) {
                        $target.find(".modalContent").html(data.html);
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
						var unix = Math.round(+new Date()/1000);
						$(".action-form").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else {
                        $target.modal("hide");
                    }
                }
                                
                if($table.length > 0 && data.refresh == 'yes') {
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
          
                    $table.bootstrapTable('refresh');
                    
                    if(data.id && data.append == 'element') {
                        $('#'+data.prefix+'-'+data.id).html(data.value); 
                    }
			    }
                
                if($table.length > 0 && data.refresh == 'inline') {
                    $table.bootstrapTable('updateRow', {index: data.index, row: data.row });
                    $('tr[data-index='+data.index+']').addClass('success');
                    setTimeout(function(){
                        $('tr[data-index='+data.index+']').removeClass('success', 1000, "easeInBack");
                    }, 1000);
                    
                    if(data.id && data.append == 'element') {
                        $('#'+data.prefix+'-'+data.id).html(data.value); 
                    }
                }
                
                if(data.refresh == 'element') {
                    //$('#'+data.prefix+'-'+data.id).html(data.value); 
                    data.elements.forEach(function (element, index, array) {
                        $('#'+element.prefix+'-'+element.id).html(element.value);
                    });
			    }
                
                if(data.calendar) {
                    if($(data.calendar).length) {
                        $(data.calendar).fullCalendar( 'refetchEvents' );
                    }
                }
                
                if($input.length) {
                    var opt = "<option value='" + data.id + "' selected>" + data.name + "</option>";
                    var values = [];
                    $input.prepend(opt);
                }

                if(data.action == 'addFolderNote') {
                    showNotification('success', data.alert);

                    $(".notices").prepend(data.html);
                } 
                
                if(data.action == 'addWorkflowNote') {
                    showNotification('success', data.alert);

                    $(".notices").prepend(data.html);
                    $(".notes-count").text( $(".notes-count").text() * 1 + 1);
                }
                
                if(data.action == 'add_msg') {
                    showNotification('success', data.title, data.alert); 
                }
                
                if(data.action == 'toothEdit') {
                    $('.tooth-img[data-tooth="'+data.id+'"]').html(data.toothInfo);
                }
                
                if(data.action == 'logoChange') {
                    $('.logo').html(data.newLogo);
                }
                
                if(data.action == 'sendMsg') {
                    alert('Wiadomo�� zosta�� wys�ana');
                }
            }
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
			$target.find(".modalContent").removeClass('preload');
            alert('Something went to wrong.Please Try again later...');
        }
    });
    return false;
});

$(document).on("click", '.insertInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $(this).attr('data-target'); 
    var $input = $(this).attr('data-input'); 
    //$target.load($(this).attr("href"));
    $($target).load($(this).attr("href"), 
                function(response, status, xhr){                     
                    if (status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } else {
                        $(".saveInline").attr('data-target', $target);
                        $(".saveInline").attr('data-input', $input);
                        $($target).removeClass('none');
                    }
                        
            });  
 
    return false;
});

$(document).on("click", '.saveInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $($(this).attr('data-target')); 
    var $input = $($(this).attr('data-input')); 
    
    if(!$("#dictValue").val()) {
        $("#dictValue").addClass('bg-red2');
        $("#dictValue").attr('placeholder', 'Prosz� wpisa� warto�� s�ownika');
    } else {
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {value: $("#dictValue").val()},
            url: $(this).attr('href'),
            success: function(data) {              
                if(data.success) {
                    if($input.length) {
                        var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
                        var values = [];
                        $input.prepend(opt);
                       
                        values.push(data.id);
                        $input.val(values);
                        
                        $input.trigger('change');
                        
                        if($input.hasClass('ms-options')) {
                            $('#correspondence-addresses_list').multiselect('rebuild');
                            $('#correspondence-addresses_list').multiselect('refresh');
                        }
                        $target.addClass('none');
                    }
                } else {    
                    $("#dictValueHint").text(data.alert);
                }
            },
            /*error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                //$target.find(".modalContent").removeClass('preload');
                //alert('Something went to wrong.Please Try again later...');
                CheckForSession();
            }*/
        });
    }
    
    return false;
});

$(document).on("click", '.closeInline', function(event) { 
    $( event.target ).parent().parent().addClass('none');
});

$(document).on("click", '.closeInlineForm', function(event) { 
    $( event.target ).parent().parent().parent().addClass('none');
});

$(document).on("click", "#user-comment-send", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    if($("#user-comment").val() != '') {
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, Note: { note: $("#user-comment").val()}, AccNote: { note: $("#user-comment").val()}},
            dataType: 'json',
            url: $(this).data('action'),
            beforeSend: function() { 
                $("#validation-form").hide().empty(); 
            },
            success: function(data) {
               
                if(data.success == false) {
                    
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zosta�y usuni�te.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                   
                    $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                } else {
                    $("#user-comment").val('');

                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    
                    if(data.refresh == 'inline' && data.table) {
                        $(data.table).bootstrapTable('updateRow', {index: data.index, row: data.row });
                        $('tr[data-index='+data.index+']').addClass('success');
                        setTimeout(function(){
                            $('tr[data-index='+data.index+']').removeClass('success', 1000, "easeInBack");
                        }, 1000);
                    }
                    
                   $(".task-comments-content").prepend(data.html);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    } else {
        var txtError = 'Wpisz tre�� wiadomo�ci<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
        var unix = Math.round(+new Date()/1000);
       
        $(".task-comments").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        
        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
    }
    
});


function relationship() {
    
    $("input.number").keyup(function(e){
        var key = e.which ? e.which : event.keyCode;
        if(key == 110 || key == 188){
          e.preventDefault();
          var value = $(this).val();         
          $(this).val(value.replace(",","."));
        }   
    });
    
    if($('fieldset.collapsible > legend > span').length == 0)
        $('fieldset.collapsible > legend').append(' (<span style="font-family: monospace;">+</span>)');
        
    $('fieldset.collapsible > legend').off('click').click(function () {
        //console.log('fieldset collapsible click');
        var $divs = $(this).siblings();
        $divs.toggle();

        $(this).find('span').text(function () {
            return ($divs.is(':visible')) ? '-' : '+';
        });
    });
    
    if( $("#table-services").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-services').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'service',title: 'Us�ugi'},
                        //{ field: 'tooth',title: 'Z�b'},
                        { field: 'payment',title: 'Cena'},
                        { field: 'paid',title: 'Zap�acono'},
                        { field: 'discount',title: 'Rabat'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if($('#imgAttachmentPerson').length)
        $('#imgAttachmentPerson').imageAttachment({
            "hasImage":$('#imgAttachmentPerson').data('hasimage'),
            "previewUrl":$('#imgAttachmentPerson').data('previewurl'),
            "previewWidth":300,
            "previewHeight":200,
            "apiUrl":$('#imgAttachmentPerson').data('apiurl')
        });
  
    
    if($('.chosen-select').length)
        $("select.chosen-select").chosen({disable_search_threshold:10});
		
	if($(".mce-basic").length)
        KAPI.tinyMceBasic();
    
    if($('.ms-select-ajax').length) 
        $('.ms-select-ajax').multiselect({
            includeSelectAllOption: true,
            selectAllValue: 0,
            maxHeight: 220,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            nonSelectedText: 'wybierz opcje',
            onChange: function (option, checked) {
                //get all of the selected tiems
                var values = $('.ms-select-ajax option:selected');
                var selected = "";
                var ids = new Array(); var employees = new Array();
                $(values).each(function (index, value) {
                    selected += $(value).prop('value') + ",";
                    ids.push($(this).val()); 
                });
                selected = selected.substring(0, selected.length - 1);
                //update hidden field with comma delimited string for state persistance
                
                $("input[name='employees[]']:checked").each(function(){
                    employees.push($(this).val()); 
                });
            },
        });
        
    if($('.select2Modal').length)
        $('.select2Modal').select2({dropdownAutoWidth : true,  allowClear: true, width: '100%'});
        
    if($('#selectAutoCompleteModal').length)     
        $('#selectAutoCompleteModal').select2({
            placeholder: 'wyszukaj',
            allowClear: true, 
            width: '100%',
            ajax: {
                url: $('#selectAutoCompleteModal').data('url'),
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
}
