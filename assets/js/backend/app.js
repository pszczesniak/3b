/*jslint browser: true */
/*global window */
/*jshint unused:false*/

'use strict';

//var KAPI;

var busyDays = [];

KAPI.misc = function(config) {
    KAPI.preloader();
    KAPI.customWindow();
    
    KAPI.sendFormData();
		
    if($('#imgAttachment').length)
        KAPI.imgAttachment();
    
    if($('.gallery-manager').length) 
        KAPI.galleryManager();
        
    /*if($('#schedule').length)	
		KAPI.calendar();*/
		
    if($("#upload-demo").length)     
        KAPI.cropieInit();
        
    if($(".mce-full").length)
        KAPI.tinyMceFull();
        
	if($(".mce-basic").length)
        KAPI.tinyMceBasic();
        
    if($('#personal').length) { 
		KAPI.tasks.personal();
        KAPI.tasks.externalEvent();
    }
    
    if($('#schedule').length) { 
		KAPI.tasks.calendar();
    }
	
	if($('#timetable').length) { 
		KAPI.tasks.timetable();
    }
    
    if($("#external-events").length) 
        KAPI.tasks.externalEvent();
        
    if($('.dd').length) 
		KAPI.menuBuilder();
        
    //KAPI.translation();
};

KAPI.utils = {

    _screenSize: function() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth|| e.clientWidth|| g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight,
            sizes = {x: x, y: y};
        return sizes;
    }
};

KAPI.burger = {

    _closeNav: function() {
        var burgerButton = document.querySelector('.js-burger'),
            asideNav = document.querySelector('.js-nav'),
            mainContent = document.querySelector('.js-main');
            
        asideNav.classList.add('l-navbar--hide');
        mainContent.classList.add('l-main--full');
        burgerButton.classList.remove('is-active');
        setTimeout(function(){ $(".table-hover").bootstrapTable('resetView'); }, 300);
    },
    
    _openNav: function() {
        var burgerButton = document.querySelector('.js-burger'),
            asideNav = document.querySelector('.js-nav'),
            mainContent = document.querySelector('.js-main');
            
        burgerButton.classList.add('is-active');
        asideNav.classList.remove('l-navbar--hide');
        mainContent.classList.remove('l-main--full');
        setTimeout(function(){ $(".table-hover").bootstrapTable('resetView'); }, 300);
    },
    
    showHideNav: function showHideNav() {
        var burgerButton = document.querySelector('.js-burger'),
            asideNav = document.querySelector('.js-nav'),
            mainContent = document.querySelector('.js-main');

        burgerButton.addEventListener('click', function(event) {
            event.preventDefault();
            if (asideNav.classList.contains('l-navbar--hide')) {
                this.classList.add('is-active');
                asideNav.classList.remove('l-navbar--hide');
                mainContent.classList.remove('l-main--full');
            } else {
                asideNav.classList.add('l-navbar--hide');
                mainContent.classList.add('l-main--full');
                this.classList.remove('is-active');
            }
            setTimeout(function(){ $(".table-hover").bootstrapTable('resetView'); }, 300);
        });
    },

    showNavSubmenu: function showNavSubmenu() {
        var submenuButtons = document.querySelectorAll('.js-submenu');

        [].forEach.call(submenuButtons, function(submenuButton) {
            submenuButton.addEventListener('click', function(event) {
                event.preventDefault();
                var nextSibling = this.nextSibling;

                while(nextSibling && nextSibling.nodeType !== 1  ) {
                    nextSibling = nextSibling.nextSibling;
                }

                if (this.classList.contains('open')) {
                    this.classList.remove('open');
                    nextSibling.classList.add('l-navbar__sub-menu--close');
                } else {
                    this.classList.add('open');
                    nextSibling.classList.remove('l-navbar__sub-menu--close');
                }
            });
        });
    },

    init: function() {
        this.showHideNav();
        this.showNavSubmenu();
    }
    
};

KAPI.sidebar = {
    showHideSidenav: function showHideSidenav() {
        var burgerButton = document.querySelector('.js-aside'),
            asideNav = document.querySelector('.js-aside-nav');
		
		if (burgerButton) {
			burgerButton.addEventListener('click', function(event) {
				event.preventDefault();
				if (asideNav.classList.contains('l-aside--hide')) {
					asideNav.classList.remove('l-aside--hide');
				} else {
					asideNav.classList.add('l-aside--hide');
				}
			});
		}
    },

    init: function() {
        this.showHideSidenav();
    }
};

KAPI.initial = {
    burgerOnStart: function burgerOnStart() {
        if ( document.querySelector('.js-burger') ) {
            var x = KAPI.utils._screenSize().x;
            if (x < 1025 || $('#acc-actions-index').length) {
                KAPI.burger._closeNav();
            } else {
                KAPI.burger._openNav();
            }
        }
    },

    init: function() {
        this.burgerOnStart();
    }
};

KAPI.burger.init();
KAPI.sidebar.init();
KAPI.initial.init();

document.onreadystatechange = function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    if (document.readyState === 'complete') {
        $(function () {
			document.querySelector('body').classList.remove('preload');
           /* if (document.querySelector('.table-hover')) {
                setTimeout(function(){ 
                    $(".table-hover").bootstrapTable('refresh'); 
                }, 100);
            }*/
            if( $("#table-data").length ) {
                $("#table-data").on('load-error.bs.table', function (status, res) {
                    //console.log('load-success');
                    CheckForSession();
                });
            }
            
            if( $(".table-widget").length ) {
                $(".table-widget").on('load-error.bs.table', function (status, res) {
                    //console.log('load-success');
                    CheckForSession();
                });
            }
            
            if (document.querySelector('#datetimepicker_start')) {
                $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD HH:mm',  });
                $('#datetimepicker_start').on("dp.change", function (e) {
                                        /*console.log(e.date);
                                        console.log(e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm'));*/
                                        minDate = e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm');
                                       // $('#datetimepicker_end').data("DateTimePicker").minDate(minDate);
                                        $('#caltask-date_to').val(minDate);
                                    });
            }
            if (document.querySelector('#datetimepicker_end')) {
                $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD HH:mm',  });
            } 
            
            if (document.querySelector('#datetimepicker_tasks')) {
                $.ajax({
                    type: 'post',
                    cache: false,
                    dataType: 'json',
                    data: $(this).serialize(),
                    url: '/task/default/busydays',
            
                    success: function(data) {
                        console.log('busydays start');
                        busyDays = data.days;
                    },
                    error: function(xhr, textStatus, thrownError) {
                        console.log(xhr);
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
                
                setTimeout(function(){ 
                    $('#datetimepicker_tasks').datepicker({ 
                        language: "pl",
                        format: 'YYYY-MM-DD', 
                        weekStart: 1,
                        beforeShowDay:  function (date) {
                            dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
                            //console.log(busyDays);
                            if ($.inArray(dmy, busyDays) < 0) {
                                //console.log('free');
                                return {classes:'text--red', tooltip:'Na ten dzień nie ustalono żadnych czynności kancelaryjnych'}
                            } else {
                                //console.log('not free');
                                return {enabled:true,classes:'bg-purple2 text--purple',tooltip:'Kliknij i zobacz czynności kancelaryjne na ten dzień'}
                               // return [false,"","Booked Out"];
                            }
                        },
                        todayHighlight: true
                            //var curr_date = Date.toJSON().substring(0,10);
                            //if (forbidden.indexOf(curr_date)>-1) return false;     
                        /*inline: true, beforeShowDay:
                        function(dt)  { 
                            return [dt.getDay() == 0 || dt.getDay() == 6 || available(dt), "" ];
                            }*/
                    } );                  
                }, 500);
                
                $('#datetimepicker_tasks').on("changeMonth", function (e) {
                    var month = (new Date(e.date).getMonth() + 1);
                    var year = String(e.date).split(" ")[3]; //(new Date(e.date).getYear());
                    $.post({
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: '/task/default/busydays?month='+month+'&year='+year,
                
                        success: function(data) {
                            busyDays = [];
                            console.log('busydays start');
                            busyDays = data.days;
                            //$('#datetimepicker_tasks').datepicker('update', new Date(2016, 11, 1));
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                            alert('Something went to wrong.Please Try again later...');
                        }
                    });
                });
                
                $/*('#datetimepicker_tasks').on('show', function(e) {
 
                    //this is the magic here.. forces the datepicker to run beforeShowDay()
                    //each time the calendar is displayed.
                    $(this).datepicker('update');
                 
                 }); */
           
                $('#datetimepicker_tasks').on("changeDate", function (e) {

                    $("#datetimepicker_tasks-cases").parent().addClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().addClass('overlay');

                    //console.log(e.timeStamp);
                    var changeDate = new Date(e.date).getTime()/1000;
                    //var changeDate = e.timeStamp;
                   // console.log(changeDate);
                    var that = this,
                    value = $(this).val();
                    
                    var eventLink = $('#datetimepicker_tasks-date');
                    var eventArr = eventLink.attr("href").split("=");
                    eventLink.attr("href", eventArr[0]+'='+changeDate)
                   // startDate.setDate(startDate.getDate(new Date(e.date.valueOf()))); console.log(startDate);
                   // var month = currentDate.getMonth();
                    $(".cal-desc-date").text(moment(new Date(e.date).getTime()).format('YYYY-MM-DD'));

                    $.ajax({
                        type: 'post',
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: $("#datetimepicker_tasks").data('action')+'?time='+changeDate,
                
                        success: function(data) {
                            console.log('test');
                            $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                            $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');

                            $("#datetimepicker_tasks-cases").html(data.cases);
							$("#datetimepicker_tasks-tasks").html(data.tasks);
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                            alert('Something went to wrong.Please Try again later...');
                            $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                            $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');
                        }
                    });
                    return false;
                });
           
            }  
            
            if (document.querySelector('#datetimepicker_personal')) {
                $.ajax({
                    type: 'post',
                    cache: false,
                    dataType: 'json',
                    data: $(this).serialize(),
                    url: '/task/personal/busydays?id'+$("#datetimepicker_personal").data('employee'),
            
                    success: function(data) {
                        busyDays = data.days;
                        $('#datetimepicker_personal').datepicker({ 
                            language: "pl",
                            format: 'YYYY-MM-DD', 
                            weekStart: 1,
                            beforeShowDay:  function (date) {
                                dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
                                if ($.inArray(dmy, busyDays) < 0) {
                                    return {classes:'text--red', tooltip:'Na ten dzień nie ustalono żadnych zadań'}
                                } else {
                                    return {enabled:true,classes:'bg-purple2 text--purple',tooltip:'Kliknij i zobacz zadania zaplanowane na ten dzień'}
                                }
                            },
                            todayHighlight: true
         
                        } );      
                    },
                    error: function(xhr, textStatus, thrownError) {
                        console.log(xhr);
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
                
                //setTimeout(function(){ 
                                
                //}, 500);
                
                $('#datetimepicker_personal').on("changeMonth", function (e) {
                    var month = (new Date(e.date).getMonth() + 1);
                    var year = String(e.date).split(" ")[3]; //(new Date(e.date).getYear());
                    $.post({
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: '/task/personal/busydays/'+$("#datetimepicker_personal").data('employee')+'?month='+month+'&year='+year,
                
                        success: function(data) {
                            //busyDays = [];
                            busyDays = data.days;
                            //$('#datetimepicker_tasks').datepicker('update', new Date(2016, 11, 1));
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                            alert('Something went to wrong.Please Try again later...');
                        }
                    });
                });
                
                /*$('#datetimepicker_personal').on('show', function(e) {
 
                    //this is the magic here.. forces the datepicker to run beforeShowDay()
                    //each time the calendar is displayed.
                    $(this).datepicker('update');
                 
                 }); */
           
                $('#datetimepicker_personal').on("changeDate", function (e) {

                    $("#datetimepicker_personal-tasks").parent().addClass('overlay');
                    $("#datetimepicker_personal-events").parent().addClass('overlay');
                    var changeDate = new Date(e.date).getTime()/1000;
                    //var changeDate = e.timeStamp;
                   // console.log(changeDate);
                    var that = this,
                    value = $(this).val();
                    
                   /* var eventLink = $('#datetimepicker_tasks-date');
                    var eventArr = eventLink.attr("href").split("=");
                    eventLink.attr("href", eventArr[0]+'='+changeDate)
                   // startDate.setDate(startDate.getDate(new Date(e.date.valueOf()))); console.log(startDate);
                   // var month = currentDate.getMonth();
                    $(".cal-desc-date").text(moment(new Date(e.date).getTime()).format('YYYY-MM-DD'));*/

                    $.ajax({
                        type: 'post',
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: $("#datetimepicker_personal").data('events')+'?time='+changeDate,
                
                        success: function(data) {
                            $("#datetimepicker_personal-tasks").parent().removeClass('overlay');
                            $("#datetimepicker_personal-events").parent().removeClass('overlay');

							$("#datetimepicker_personal-tasks").html(data.tasks);
                            $("#datetimepicker_personal-events").html(data.events);
                            $(".todo-hours_per_day").text(data.stats.hours_per_day);
                            $(".todo-hours_per_month").text(data.stats.hours_per_month);
                            $(".todo-date").text(data.checkDay);
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                            alert('Something went to wrong.Please Try again later...');
                            $("#datetimepicker_personal-tasks").parent().removeClass('overlay');
                            $("#datetimepicker_personal-events").parent().removeClass('overlay');
                        }
                    });
                    return false;
                });
           
            }  
            
            if (document.querySelector('#datetimepicker_resources')) {
                $.ajax({
                    type: 'post',
                    cache: false,
                    dataType: 'json',
                    data: $(this).serialize(),
                    url: '/company/resources/busy/'+$('#datetimepicker_resources').data('id'),
            
                    success: function(data) {
                        console.log('busydays start');
                        busyDays = data.days;
                    },
                    error: function(xhr, textStatus, thrownError) {
                        console.log(xhr);
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
                
                setTimeout(function(){ 
                    $('#datetimepicker_resources').datepicker({ 
                        language: "pl",
                        format: 'YYYY-MM-DD', 
                        weekStart: 1,
                        beforeShowDay:  function (date) {
                            dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
                            if ($.inArray(dmy, busyDays) < 0) {
                                return {classes:'text--red', tooltip:'Na ten dzień nie ma żadnych rezerwacji'}
                            } else {
                                return {enabled:true,classes:'bg-purple2 text--purple',tooltip:'Kliknij i zobacz rezerwacje na ten dzień'}
                            }
                        },
                        todayHighlight: true
                    } );                  
                }, 500);
                
                $('#datetimepicker_resources').on("changeMonth", function (e) {
                    var month = (new Date(e.date).getMonth() + 1);
                    var year = String(e.date).split(" ")[3]; //(new Date(e.date).getYear());
                    $.post({
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: '/company/resources/busy?id='+$('#datetimepicker_resources').data('id')+'&month='+month+'&year='+year,
                
                        success: function(data) {
                            busyDays = [];
                           // console.log('busydays start');
                            busyDays = data.days;
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                            alert('Something went to wrong.Please Try again later...');
                        }
                    });
                });
                
                /*$('#datetimepicker_resources').on('show', function(e) {
                    $(this).datepicker('update');               
                }); */
           
                $('#datetimepicker_resources').on("changeDate", function (e) {

                    $("#datetimepicker_tasks-cases").parent().addClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().addClass('overlay');

                    //console.log(e.timeStamp);
                    var changeDate = new Date(e.date).getTime()/1000;

                    var that = this,
                    value = $(this).val();
                    
                   /* var eventLink = $('#datetimepicker_resources-date');
                    console.log(eventLink.attr("href").split("="));
                    var eventArr = eventLink.attr("href").split("=");
                    eventLink.attr("href", eventArr[0]+'='+changeDate)*/
                   // startDate.setDate(startDate.getDate(new Date(e.date.valueOf()))); console.log(startDate);
                   // var month = currentDate.getMonth();
                    $(".cal-desc-date").text(moment(new Date(e.date).getTime()).format('YYYY-MM-DD'));

                    $.ajax({
                        type: 'post',
                        cache: false,
                        dataType: 'json',
                        data: $(this).serialize(),
                        url: $("#datetimepicker_resources").data('action')+'?time='+changeDate,
                
                        success: function(data) {
                            console.log('test');
                            $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                            $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');

                            $("#datetimepicker_tasks-cases").html(data.cases);
							$("#datetimepicker_tasks-tasks").html(data.tasks);
                        },
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr);
                           // alert('Something went to wrong.Please Try again later...');
                            $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                            $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');
                        }
                    });
                    return false;
                });
           
            }  
            
            if (document.querySelector('#datetimepicker_delivery')) {
                $('#datetimepicker_delivery').datetimepicker({ format: 'YYYY-MM-DD'  });
            }  
            if (document.querySelector('#datetimepicker_posting')) {
                $('#datetimepicker_posting').datetimepicker({ format: 'YYYY-MM-DD' });
            }  
			if (document.querySelector('#datetimepicker_hstart')) {
                $('#datetimepicker_hstart').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true  });
            }
            if (document.querySelector('#datetimepicker_hend')) {
                $('#datetimepicker_hend').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true  });
            }  
            if (document.querySelector('#datetimepicker_hstart_full')) {
                $('#datetimepicker_hstart_full').datetimepicker({ format: 'YYYY-MM-DD HH', showClear: true, showClose: true  });
            }
            if (document.querySelector('#datetimepicker_hend_full')) {
                $('#datetimepicker_hend_full').datetimepicker({ format: 'YYYY-MM-DD HH', showClear: true, showClose: true  });
            }  
            
            if (document.querySelector('#datetimepicker_delivery_search')) {
                $('#datetimepicker_delivery_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_delivery_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('.table-hover')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('.table-hover').data("url"), method: 'get',
                        query: o

                    });
                });
            }
            
            if (document.querySelector('#datetimepicker_posting_search')) {
                $('#datetimepicker_posting_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_posting_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('.table-hover')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('.table-hover').data("url"), method: 'get',
                        query: o

                    });
                });
            }
            
            if (document.querySelector('#datetimepicker_date_from')) {
                $('#datetimepicker_date_from').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true  });
                $('#datetimepicker_date_from').on("dp.change", function (e) {
                    
                    var that = this,
                    value = $(this).val();
                    
                    if( $("#structure").length > 0 )
                        var o = { 'Files[id_dict_type_file_fk]': $('#structure').jstree('get_selected')[0]};
                    else    
                        var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    var $table = $('#datetimepicker_date_from').data("table");
                    if( $table) {
                        //$($table).bootstrapTable('refresh');
                        if($($table).data('side-pagination') == 'client') {
                            $($table).bootstrapTable('refresh');
                        } else {
                            var options = $($table).bootstrapTable('getOptions');
                            var pageNumber = options.pageNumber;
                            if(pageNumber == 1)
                                $($table).bootstrapTable('refresh');
                            else
                                $($table).bootstrapTable('selectPage', 1);
                        }
                    } else {
                        $('.table-hover')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                            url: $('.table-hover').data("url"), method: 'get',
                            query: o

                        });
                    }
                });
            }
            if (document.querySelector('#datetimepicker_date_to')) {
                $('#datetimepicker_date_to').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true  });
                $('#datetimepicker_date_to').on("dp.change", function (e) {
                    var that = this,
                    value = $(this).val();
                    if( $("#structure").length > 0 )
                        var o = { 'Files[id_dict_type_file_fk]': $('#structure').jstree('get_selected')[0]};
                    else    
                        var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    var $table = $('#datetimepicker_date_to').data("table");
                    if( $table) {
                        //$($table).bootstrapTable('refresh');
                        if($($table).data('side-pagination') == 'client') {
                            $($table).bootstrapTable('refresh');
                        } else {
                            var options = $($table).bootstrapTable('getOptions');
                            var pageNumber = options.pageNumber;
                            if(pageNumber == 1)
                                $($table).bootstrapTable('refresh');
                            else
                                $($table).bootstrapTable('selectPage', 1);
                        }
                    } else {
                        $('.table-hover')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                            url: $('.table-hover').data("url"), method: 'get',
                            query: o

                        });
                    }
                });
            }  
            if (document.querySelector('#datetimepicker_period')) {
                $('#datetimepicker_period').datetimepicker({ format: 'YYYY-MM', showClear: true, showClose: true  });
                $('#datetimepicker_period').on("dp.change", function (e) {
                    var that = this,
                    value = $(this).val();
                    if( $("#structure").length > 0 )
                        var o = { 'Files[id_dict_type_file_fk]': $('#structure').jstree('get_selected')[0]};
                    else    
                        var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    var $table = $('#datetimepicker_period').data("table");
                    if( $table) {
                        //$($table).bootstrapTable('refresh');
                        if($($table).data('side-pagination') == 'client') {
                            $($table).bootstrapTable('refresh');
                        } else {
                            var options = $($table).bootstrapTable('getOptions');
                            var pageNumber = options.pageNumber;
                            if(pageNumber == 1)
                                $($table).bootstrapTable('refresh');
                            else
                                $($table).bootstrapTable('selectPage', 1);
                        }
                    } else {
                        $('.table-hover')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                            url: $('.table-hover').data("url"), method: 'get',
                            query: o

                        });
                    }
                    
                    if($(".btn-calculate").length) {
                        var calculateBtn = $(".btn-calculate");
                        var url = new URL(calculateBtn.attr('href'));
                        var searchParams = new URLSearchParams(url.search);
                        //console.log(searchParams.get('period')); 
                        if($("#salary-period").val()) {
                            //searchParams.set('period', $("#salary-period").val());
                            var newUrl = calculateBtn.attr('href').replace(searchParams.get('period'), $("#salary-period").val());
                            calculateBtn.attr('href', newUrl);
                            calculateBtn.find('span.period').text($("#salary-period").val());
                        } else { 
                            calculateBtn.attr('disabled', true);
                        }
                    }
                    
                });
            }  
            
            if (document.querySelector('#datetimepicker_period_from')) {
                $('#datetimepicker_period_from').datetimepicker({ format: 'YYYY-MM', showClear: true, showClose: true  });
                $('#datetimepicker_period_from').on("dp.change", function (e) {
                    var $table = $('#datetimepicker_period_from').data("table");
                    if( $table) {
                        //$($table).bootstrapTable('refresh');
                        if($($table).data('side-pagination') == 'client') {
                            $($table).bootstrapTable('refresh');
                        } else {
                            var options = $($table).bootstrapTable('getOptions');
                            var pageNumber = options.pageNumber;
                            if(pageNumber == 1)
                                $($table).bootstrapTable('refresh');
                            else
                                $($table).bootstrapTable('selectPage', 1);
                        }
                    }
                });
            }
            
            if (document.querySelector('#datetimepicker_period_to')) {
                $('#datetimepicker_period_to').datetimepicker({ format: 'YYYY-MM', showClear: true, showClose: true  });
                $('#datetimepicker_period_to').on("dp.change", function (e) {
                    var $table = $('#datetimepicker_period_to').data("table");
                    if( $table) {
                        //$($table).bootstrapTable('refresh');
                        if($($table).data('side-pagination') == 'client') {
                            $($table).bootstrapTable('refresh');
                        } else {
                            var options = $($table).bootstrapTable('getOptions');
                            var pageNumber = options.pageNumber;
                            if(pageNumber == 1)
                                $($table).bootstrapTable('refresh');
                            else
                                $($table).bootstrapTable('selectPage', 1);
                        }
                    }
                });
            }
            
            if (document.querySelector('#task_date')) {
                $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
                
                $('#task_date').on("dp.change", function (e) {
                                    minDate = e.date.format('YYYY-MM-DD');
                                    if( $('#task_fromDate').length > 0 ) {
                                        $('#task_fromDate').data("DateTimePicker").minDate(minDate);
                                        $('#task_fromDate input').val(minDate);
                                    }
                                    minDeadline = e.date.format('YYYY-MM-DD 16:00');
                                    if( $('#task_deadline').length > 0 ) {
                                        $('#task_deadline').data("DateTimePicker").minDate(minDeadline);
                                        $('#task_deadline input').val(minDeadline);
                                    }
                                });
            }
            
            if (document.querySelector('#task_time')) {
                $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
                
                $('#task_time').on("dp.change", function (e) {
                                    if(!e.date) {
                                        
                                        if( $('#task_fromTime').length > 0 ) {
                                            $("#task_toTime > input").prop('disabled', true).val('');
                                            $("#task_fromTime > input").prop('disabled', true).val('');
                                        }
                                    
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', true);
                                        }
                                    } else {
                                        minDate = e.date.format('HH:mm');
                                        if( $('#task_fromTime').length > 0 ) {
                                            $('#task_fromTime').data("DateTimePicker").minDate(minDate);
                                            $('#task_fromTime input').val(minDate);
                                            
                                            $("#task_toTime > input").prop('disabled', false);
                                            $("#task_fromTime > input").prop('disabled', false);
                                        }
                                        
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', false);
                                        }
                                    }
                                });
                
            }
            
            if (document.querySelector('#task_deadline')) {
                 $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
            }
            
            if (document.querySelector('#task_fromDate')) {
                $('#task_fromDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
                $('#task_fromDate').on("dp.change", function (e) {
                
                                    //minDate = e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm');
                                    minDate = e.date.format('YYYY-MM-DD HH:mm');
                                    $('#task_toDate').data("DateTimePicker").minDate(minDate);
                                    $('#task_toDate input').val(minDate);
                                });
            }
            
            if (document.querySelector('#task_fromTime')) {
                $('#task_fromTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });
            }
            
            if (document.querySelector('#task_toDate')) {
                $('#task_toDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
            }
            
            if (document.querySelector('#task_toTime')) {
                $('#task_toTime').datetimepicker({  format: 'LT', showClear: true, showClose: true, ignoreReadonly: false });
            }
               
        });
    }
};




//
// Reesize event listener
//

(function() {
    window.addEventListener("resize", resizeThrottler, false);
    
   /* window.onbeforeunload = function (event) {
        var message = 'Important: Please click on \'Save\' button to leave this page.';
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    };*/

    var resizeTimeout;
    function resizeThrottler() {
        // ignore resize events as long as an actualResizeHandler execution is in the queue
        if ( !resizeTimeout ) {
            resizeTimeout = setTimeout(function() {
            resizeTimeout = null;
            actualResizeHandler();

            // The actualResizeHandler will execute at a rate of 15fps
            }, 66);
        }
    }

    function actualResizeHandler() {
        KAPI.initial.burgerOnStart();
    }

}());


function formatErrorMessage(jqXHR, exception) {

    if (jqXHR.status === 0) {
        return ('Not connected.\nPlease verify your network connection.');
    } else if (jqXHR.status == 404) {
        return ('The requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        return ('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        return ('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        return ('Time out error.');
    } else if (exception === 'abort') {
        return ('Ajax request aborted.');
    } else {
        return ('Uncaught Error.\n' + jqXHR.responseText);
    }
}

$(function () {
    //setup ajax error handling
    $.ajaxSetup({
        error: function (x, status, error) {
           CheckForSession();
           /* if (x.status == 403) {
                alert("Sorry, your session has expired. Please login again to continue");
                window.location.href ="/login";
            }
            else {
                alert("An error occurred: " + status + "nError: " + error);
            }*/
        }
    });
});

var check_session;
function CheckForSession() {
		/*jQuery.ajax({
				type: "POST",
				url: "/check-session",
				cache: false,
				success: function(data){
					if(data.check == 0) {
                        //$("#modal-session").modal('show');
                        window.location.href ="/login?check=out";
					}
				}
		});*/
}

    $.fn.datepicker.dates['pl'] = {
        days: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"],
        daysShort: ["Nie", "Sob", "Wto", "Śro", "Czw", "Pią", "Sob", "Nie"],
        daysMin: ["Ni", "Po", "Wt", "Śr", "Cz", "Pi", "So", "Ni"],
        months: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        monthsShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"]
    };
/*check_session = setInterval(CheckForSession, 500);*/

function readFile(input) {
    //console.log($(input));
    var $path = $(input).data("path"); console.log($path);
    if (input.files && input.files[0]) {
        var reader = new FileReader();          
        reader.onload = function (e) {
            var allFiles = $("#upload")[0].files;
            var file=allFiles[0];
            var data = new FormData();
            //for(var i = 0; i < files.length; i++) data.append('file'+i, files[i]);
             data.append('Avatar[avatar]', $("#upload")[0].files[0]);
            $.ajax({
                type: 'post',
                url: $path,
                data:  data,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                        //url: data.file
                    });
                    $('.upload-demo').addClass('ready');
                },
                dataType: "json",
            });

        }           
        reader.readAsDataURL(input.files[0]);
    }
    //return false;
}