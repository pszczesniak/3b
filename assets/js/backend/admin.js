
$(".data-employee-grant").on("change", function(event) {
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    $.ajax({
        type: 'POST',
        //url: "/company/admin/treestructure/"+this.value,
        url: "/company/admin/grants/"+this.value,
        data: {_csrf: csrfToken},
        success: function(data, status) {
           /* $("#grants-structure").html(data);
            $(".tree-multiselect").remove();
            $("#grants-structure").treeMultiselect({ });*/
            $(".panel-grants").html(data);
        }
    });
});

$(".data-group-grant").on("change", function(event) {
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    $.ajax({
        type: 'POST',
        //url: "/company/admin/treestructure/"+this.value,
        url: "/company/admin/ggrants",
        data: {_csrf: csrfToken, role: this.value},
        success: function(data, status) {
           /* $("#grants-structure").html(data);
            $(".tree-multiselect").remove();
            $("#grants-structure").treeMultiselect({ });*/
            $(".panel-group-grants").html(data);
        }
    });
});

$(document).on("submit", "#admin-grants", function (event) {
	var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target"));

	$.ajax({
		type: $form.attr("method"),
		url: $form.attr("action"),
		data: $form.serialize(),

		success: function(data, status) {
			if(data.result) {
				if ($('#grands div.alert').length){
					$('#grands div.alert').removeClass('alert-danger').addClass('alert-success').text(data.success);
				} else {
					$('#grands').prepend('<div class="alert alert-success" role="alert">'+data.success+'</div>');
				}
			} else {
				if ($('#grands div.alert').length){
					$('#grands div.alert').removeClass('alert-success').addClass('alert-danger').text(data.error);
				} else {
					$('#grands').prepend('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
				}
			}
		}
	});
    
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});

