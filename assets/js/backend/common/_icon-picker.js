;(function($){
  $('.ct-iconpicker .ct-ip-holder .ct-ip-icon').on('click', function(){
    var iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');
    
    iconPickerPopup.slideToggle();
  });
  $('.ct-iconpicker .ct-ip-popup').on('click', 'a', function (e) {
    e.preventDefault();
    var iconClass = $(this).data('icon'),
        inputField = $('.ct-icon-value'),
        iconHolder = $('.ct-ip-icon i');
    iconHolder.attr('class', '');
    iconHolder.addClass(iconClass);
    inputField.val(iconClass);
  });
  
  $('.ct-iconpicker .ct-ip-popup').on('change keyup paste', 'input.ct-ip-search-input', function (e) {
    var iconPickerPopup = $('.ct-iconpicker .ct-ip-popup ul'),
        searchVal = $(this).val();
    
    if( _.isEmpty(searchVal) ){
      iconPickerPopup.find('li').removeClass('hidden');
    } else {
      iconPickerPopup.find('li').addClass('hidden');
    
      var found = iconPickerPopup.find('li a[data-icon*="'+searchVal+'"]');
      found.parent('li').removeClass('hidden');
    }
    
    
  });
  
  $(document).mouseup(function (e){
    var iconPicker = $('.ct-iconpicker'),
        iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');

      if ( ( ! iconPicker.is(e.target) && iconPicker.has(e.target).length === 0 ) ){
          iconPickerPopup.slideUp();
      }
  });
  

})(jQuery)