'use strict';


KAPI.chat = {
    talk: function() {
        $("#chat-comment-send").click( function(event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            if( $("#chat-comment-text").val() == '' ) {
                var unix = Math.round(+new Date()/1000);
                $(".chat-room-talk").append('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible">Proszę wpisać komentarz</div>');
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
            } else { 
                var csrfToken = $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    type: 'post',
                    cache: false,
                    data: {_csrf : csrfToken, ComPost: { note: $("#chat-comment-text").val()}},
                    dataType: 'json',
                    url: $(this).data('action'),
                    success: function(data) {                      
                        if(data.success == false) {
                            var unix = Math.round(+new Date()/1000);
                            $(".chat-room-talk").append('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible">Twój komentarz nie został opublikowany</div>');
                            setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                        } else {
                            $("#chat-comment-text").val('');
                            $(".chat-room-talk").prepend(data.html);
                        }                        
                    },
                    error: function(xhr, textStatus, thrownError) {
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
            }
            
            return false;
        });
    },
    
    refreshTalk: function() {
        $("#btn-refresh-talk").click( function(event) {
            event.stopImmediatePropagation();
            event.preventDefault();
           // $(".chat-room-talk").addClass('preload');
            $.ajax({
                type: 'post',
                cache: false,
               // data: {_csrf : csrfToken, ComPost: { note: $("#chat-comment-text").val()}},
                dataType: 'json',
                url: $(this).attr('href'),
                success: function(data) {                      
                    $(".chat-room-talk").html(data.html);    
                   // $(".chat-room-talk").removeClass('preload');                        
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
                        
            return false;
        });
    },
    
    afterSaveTrue: function(txtError, unix, data) {
        if(data.action == 'createGroup') {
            $(".chat-available-user").prepend(data.tmpItem);        
            $("#chat-groups-empty").addClass("disabled");
        }
        if(data.action == 'updateGroup') {
            $(".chat-available-user > li#chat-group-"+data.id).html(data.tmpItem);        
            $("#chat-groups-empty").addClass("disabled");
        }
        if(data.action == 'createTopic') {
            $(".chat-list").prepend(data.tmpItem);        
            $("#chat-topics-empty").addClass("disabled");
            if(data.tmpPost) {
                $(".last-posts").prepend(data.tmpPost); 
            }
        }
        if(data.action == 'deleteMember') { 
            $("li#group-member-"+data.id).remove();        
            //$("#chat-topics-empty").addClass("disabled");
        }
        if(data.action == 'addMember') {
            $(".chat-available-user").prepend(data.tmpItem);       
            //$("#chat-topics-empty").addClass("disabled");
        }
    },
};