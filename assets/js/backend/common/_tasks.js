'use strict';


KAPI.tasks = {
	timetable: function() {
        $('#timetable').fullCalendar({
			defaultView: 'agendaWeek', 
            timezone: "Europe/Warsaw",
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2015-02-12',
			businessHours: false, // display business hours
            slotLabelFormat: 'HH:mm', 
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			buttonText: {
				today:    'Dzisiaj',
				month:    'Miesiąc',
				week:     'Tydzień',
				day:      'Dzień'
			},
			//hiddenDays: [6,0],
			firstDay: 1,
			//minTime: "09:00:00",
			//maxTime: "24:00:00",
			editable: true, 
            disableResizing: true,
            disableDragging: true,
			allDayDefault: false,
			url: true,
			agenda: 'HH:mm', 
			//events: '/task/event/scheduledata',
            events: function(start, end, timezone, callback) {
                //var $ids = $('.checkbox_employee:checked').map(function() {return this.value;}).get().join(',');
                //var $colors = $('.checkbox_employee:checked').map(function() { return $('label[for='+this.id+']').css('color');  }).get().join(';'); 
                $.ajax({
                    url: '/eg/calendar/scheduledata',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix(),
                        confirmVisit: $("#visit-is_confirm").val(),
						statusVisit: $("#visit-id_dict_eg_visit_status_fk").val(), 
						doctor: $("#visit-id_doctor_fk").val(),
						patient: $("#visit-id_patient_fk").val(),
                    },
                    success: function(doc) {
                        var events = [];
                        $(doc).each(function() {
                            events.push({
                                id: $(this).attr('id'),
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
								backgroundColor: $(this).attr('backgroundColor'),
								borderColor: $(this).attr('borderColor'),
								eventColor: $(this).attr('backgroundColor'),
								eventBorderColor: $(this).attr('backgroundColor'),
                                textColor: $(this).attr('textColor'),
                                allDay :  $(this).attr('allDay'),
                                icon: $(this).attr('icon'),
                                className: $(this).attr('className')
                            });
                        });
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element) {
                //if(event.icon){          
                    element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i>");
                //}

                element.tooltip({title: event.title, container: "body", html: true});  
            },

			weekends: true,
			allDaySlot : true,
			selectable: true,
			selectHelper: true,
			slotDuration: "00:15:00",
			disableDragging: true,
			disableResizing: true,
			monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
			monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
			dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek','Sobota'],
			dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
			select: function(start, end, allDay) {
				$target = $("#modal-grid-item");
                var $action = '/eg/visit/new/0?start='+start.unix()+'&end='+end.unix();
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowy');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
		
				return false;
			},
			eventClick: function(calEvent, jsEvent, view) { 
				//console.log(calEvent.id);	
				//var idArr = calEvent.id.split('-');
				$target = $("#modal-grid-item");
				$target.find(".modal-title").html('<i class="fa fa-eye"></i>Szczegóły wizyty');
				$target.find(".modalContent").addClass('preload');
    
			    $target.find(".modalContent")
                    .load('/eg/visit/showajax/'+calEvent.id, 
                        function(response, status, xhr){ 
                            $target.modal('show'); 

                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 300);
                   });   
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
				
				/*$('#myModal').on('hidden.bs.modal', function(){
					$(this).removeData('bs.modal');
				});*/
				return false;
									  
			},
            droppable: true,
            drop: function(date) { // this function is called when something is dropped
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				// assign it the date that was reported
				copiedEventObject.start = date;
				//copiedEventObject.backgroundColor ="#0000FF";
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				//$('#timetable').fullCalendar('renderEvent', copiedEventObject, true);
                $target = $("#modal-grid-item");
                var $action = '/eg/visit/new/0?start='+date.unix()+'&doctor='+copiedEventObject.className;
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowy');
                  
                $target.find(".modalContent")
                        .load($action, 
                            function(response, status, xhr){ 
                                $target.modal('show'); 
                                
                                if ( status == "error" ) {
                                    var msg = "Sorry but there was an error: ";
                                    if(xhr.status == 302) {
                                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                        CheckForSession();
                                    } else {
                                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                                    }
                                } 
                                $target.find(".modalContent").removeClass('preload');
                        });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
				// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			}
		});
    },
    calendar: function() {
        $('#schedule').fullCalendar({
			defaultView: 'month', 
            timezone: "Europe/Warsaw",
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2015-02-12',
			businessHours: false, // display business hours
            slotLabelFormat: 'HH:mm', 
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			buttonText: {
				today:    'Dzisiaj',
				month:    'Miesiąc',
				week:     'Tydzień',
				day:      'Dzień'
			},
			firstDay: 1,
			minTime: "07:00:00",
			maxTime: "20:00:00",
			editable: true, 
            disableResizing: true,
            disableDragging: true,
			allDayDefault: false,
			url: true,
			agenda: 'HH:mm', 
			//events: '/task/event/scheduledata',
            events: function(start, end, timezone, callback) {
                var $ids = $('.checkbox_employee:checked').map(function() {return this.value;}).get().join(',');
                var $colors = $('.checkbox_employee:checked').map(function() { return $('label[for='+this.id+']').css('color');  }).get().join(';'); 
                $.ajax({
                    url: '/task/calendar/scheduledata',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix(),
                        typeEvent: $("#caltasksearch-type_fk").val(),
						statusEvent: $("#caltasksearch-id_dict_task_status_fk").val(),
						department: $("#caltasksearch-id_department_fk").val(),
						employee: $("#caltasksearch-id_employee_fk").val(),
                        branch: $("#caltasksearch-id_company_branch_fk").val(),
                        employees:  $ids,
                        colors: $colors
                    },
                    success: function(doc) {
                        var events = [];
                        $(doc).each(function() {
                            events.push({
                                id: $(this).attr('id'),
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
								backgroundColor: $(this).attr('backgroundColor'),
								borderColor: $(this).attr('backgroundColor'),
								eventColor: $(this).attr('backgroundColor'),
								eventBorderColor: $(this).attr('backgroundColor'),
                                allDay :  $(this).attr('allDay'),
                                icon: $(this).attr('icon')
                            });
                        });
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element) {
                //if(event.icon){          
                    element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i>");
                //}
                 //eventElement.attr('title', 'test');
                element.tooltip({title: event.title, container: "body", html: true});  
                /*element.tooltip({   
                    //selector: "[data-toggle='tooltip']",
                    //container: "body",
                    title: event.description,
                    html: true
                });*/
            },
			/*eventMouseover: function(calEvent, jsEvent) { 
				var tooltip = '<div class="tooltipevent"> ' + calEvent.title + '</div>'; 
				var $tool = $(tooltip).appendTo('body');
                $(this).mouseover(function(e) {
                    $(this).css('z-index', 10000);
                            $tool.fadeIn('500');
                            $tool.fadeTo('10', 1.9);
                }).mousemove(function(e) {
                    $tool.css('top', e.pageY + 10);
                    $tool.css('left', e.pageX + 20);
                });
                },
            eventMouseout: function(calEvent, jsEvent) {
                $(this).css('z-index', 8);
                $('.tooltipevent').remove();
            },*/
			weekends: true,
			allDaySlot : true,
			selectable: true,
			selectHelper: true,
			slotDuration: "00:30:00",
			disableDragging: true,
			disableResizing: true,
			monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
			monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
			dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek','Sobota'],
			dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
			select: function(start, end, allDay) {
				$target = $("#modal-grid-item");
                var $action = '/task/event/new/0?start='+start.unix()+'&end='+end.unix();
                /*$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zdarzenie');
                $("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/createajax/0?start='+start.unix()+'&end='+end.unix());*/
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowy');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
		
				return false;
			},
			eventClick: function(calEvent, jsEvent, view) { 
				//console.log(calEvent.id);	
				//var idArr = calEvent.id.split('-');
				$target = $("#modal-grid-item");
				$target.find(".modal-title").html('<i class="fa fa-eye"></i>Podgląd');
				$target.find(".modalContent").addClass('preload');
                /*$("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/showajax/'+calEvent.id);*/
			    $target.find(".modalContent")
                    .load('/task/event/showajax/'+calEvent.id, 
                        function(response, status, xhr){ 
                            $target.modal('show'); 

                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 300);
                   });   
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
				
				$('#myModal').on('hidden.bs.modal', function(){
					$(this).removeData('bs.modal');
				});
				return false;
									  
			}
		});
    },
    
    personal: function() {
        $('#personal').fullCalendar({
			defaultView: 'agendaWeek', 
            timezone: "Europe/Warsaw",
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2015-02-12',
			businessHours: false, // display business hours
            slotLabelFormat: 'HH:mm', 
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			buttonText: {
				today:    'Dzisiaj',
				month:    'Miesiąc',
				week:     'Tydzień',
				day:      'Dzień'
			},
			firstDay: 1,
			minTime: "07:00:00",
			maxTime: "20:00:00",
			editable: true, 
            disableResizing: true,
            disableDragging: true,
			allDayDefault: false,
			url: true,
			agenda: 'HH:mm', 
			//events: '/task/event/scheduledata',
            googleCalendarApiKey: 'AIzaSyBbTvU77Fk6aULzj_yYx3ItkPRnFBecdOE',
            eventSources: [
                {
                    googleCalendarId: 'kontakt.kapisoft@gmail.com'
                },
                {
                    googleCalendarId: 'kontakt.kapisoft@gmail.com',
                    className: 'nice-event'
                }
            ],
            events: function(start, end, timezone, callback) {
                var $ids = $('.checkbox_employee:checked').map(function() {return this.value;}).get().join(',');
                var $colors = $('.checkbox_employee:checked').map(function() { return $('label[for='+this.id+']').css('color');  }).get().join(';'); 
                $.ajax({
                    url: '/task/personal/scheduledata',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix(),
                        typeEvent: $("#caltasksearch-type_fk").val(),
						statusEvent: $("#caltasksearch-id_dict_task_status_fk").val(),
						department: $("#caltasksearch-id_department_fk").val(),
						employee: $("#caltasksearch-id_employee_fk").val(),
                        employees:  $ids,
                        colors: $colors
                    },
                    success: function(doc) {
                        var events = [];
                        $(doc).each(function() {
                            events.push({
                                id: $(this).attr('id'),
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
								backgroundColor: $(this).attr('backgroundColor'),
								borderColor: $(this).attr('backgroundColor'),
								eventColor: $(this).attr('backgroundColor'),
								eventBorderColor: $(this).attr('backgroundColor'),
                                allDay :  $(this).attr('allDay'),
								icon: $(this).attr('icon')
                            });
                        });
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element) {
                //if(event.icon){          
                    //element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i>");
                    element.find(".fc-title").prepend("<i class='"+event.icon+"'></i>");
                //}
                element.tooltip({title: event.title, container: "body", html: true});  
            },
			weekends: true,
			allDaySlot : true,
			selectable: true,
			selectHelper: true,
			slotDuration: "00:30:00",
			disableDragging: true,
			disableResizing: true,
            droppable: true,
			monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
			monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
			dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek','Sobota'],
			dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
			select: function(start, end, allDay) {
				$target = $("#modal-grid-item");
                var $action = '/task/personal/createajax/0?start='+start.unix()+'&end='+end.unix();
                /*$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zdarzenie');
                $("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/createajax/0?start='+start.unix()+'&end='+end.unix());*/
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zadanie osobiste');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
				return false;
			},
			eventClick: function(calEvent, jsEvent, view) { 
				//console.log(calEvent.id);	
				//var idArr = calEvent.id.split('-');
				$("#modal-grid-item").find(".modalContent").addClass('preload');
                if(calEvent.icon == 'fa fa-tasks' || calEvent.icon == 'fa fa-comment-alt') {
					$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-eye"></i>Zdarzenie kancelaryjne');
					$("#modal-grid-item").modal("show")
						.find(".modalContent")
						.load('/task/event/showajax/'+calEvent.id+'?personal=1');
                } else if(calEvent.icon == 'fa fa-handshake') { 
                    $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-handshake-o"></i>Spotkanie');
					$("#modal-grid-item").modal("show")
						.find(".modalContent")
						.load('/community/meeting/update/'+calEvent.id);
                    $("#modal-grid-item").on('shown.bs.modal', function() {
                        relationship();
                        $("#modal-grid-item").find(".modalContent").removeClass('preload');
                    });
                } else {
					$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-pencil"></i>Zadanie osobiste');
					$("#modal-grid-item").modal("show")
						.find(".modalContent")
						.load('/task/personal/updateajax/'+calEvent.id);
                    $("#modal-grid-item").on('shown.bs.modal', function() {
                        relationship();
                        $("#modal-grid-item").find(".modalContent").removeClass('preload');
                    });
				}
                
                $("#modal-grid-item").find(".modalContent").removeClass('preload');
				
				$('#myModal').on('hidden.bs.modal', function(){
					$(this).removeData('bs.modal');
				});
				return false;
									  
			},
            drop: function(date, event) {
                //console.log(date.unix());
                $target = $("#modal-grid-item");
                var $action = '/task/personal/createajax/0?start='+date.unix()+'&end='+date.unix();
               
                $target.find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zadanie osobiste');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
		
				return false;

            }
		});
    },
    
    afterSaveTrue: function(txtError, unix, data) {
        /*if($("#schedule").length > 0) {
            $("#schedule").fullCalendar( 'refetchEvents' );
            $("#schedule").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        }
        
        if($("#personal").length > 0) {
            $("#personal").fullCalendar( 'refetchEvents' );
            $("#personal").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        }*/

        if($("#datetimepicker_tasks").length > 0 && data.action == 'close') {
            $("#db-event-"+data.id).find('a.event-close').addClass('done');
            $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        }
        
        if( $('#datetimepicker_tasks').length > 0 ) {
            var calendarDate = $(".cal-desc-date").text();
            var calendarDateArr = calendarDate.split('-');
            var calendarDateUTC = Date.UTC(calendarDateArr[0], (calendarDateArr[1]*1-1), calendarDateArr[2])/1000;
            
            if(data.alert) {
                var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                //$("#validation-form").removeClass('none').removeClass('alert-danger').addClass('alert-success').html(data.alert);
             } else	{
                var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                //$("#validation-form").removeClass('none').removeClass('alert-danger').addClass('alert-success').html('Zmiany zostały zapisane');
                // $("#validation-form").show();
            }
            var unix = Math.round(+new Date()/1000);
            $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
             setTimeout(function(){ $("#alert-"+unix).remove(); }, 300);
            
            $.ajax({
                type: 'post',
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: $("#datetimepicker_tasks").data('action')+'?time='+calendarDateUTC,
        
                success: function(data) {
                    console.log('test');
                    $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');

                    $("#datetimepicker_tasks-cases").html(data.cases);
                    $("#datetimepicker_tasks-tasks").html(data.tasks);
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                    alert('Something went to wrong.Please Try again later...');
                    $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');
                }
            });
        }
        
        if($("#schedule").length > 0) {
            $("#schedule").fullCalendar( 'refetchEvents' );
            if(data.alert) {
                var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
             } else	{
                var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            }
            $("#schedule").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
             setTimeout(function(){ $("#alert-"+unix).remove(); }, 300);
        }
        
        if($("#personal").length > 0) {
            $("#personal").fullCalendar( 'refetchEvents' );
            if(data.alert) {
                var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            } else	{
                var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            }
            $("#personal").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
             setTimeout(function(){ $("#alert-"+unix).remove(); }, 300);
        }
        
        if($("#datetimepicker_personal-tasks").length && (data.action == 'create' ) ) {
            $("#datetimepicker_personal-tasks").parent().addClass('overlay');
            $("#datetimepicker_personal-events").parent().addClass('overlay');
            var changeDate = new Date( $('.todo-date').text() ).getTime()/1000;
            //var changeDate = e.timeStamp;
           // console.log(changeDate);

            
           /* var eventLink = $('#datetimepicker_tasks-date');
            var eventArr = eventLink.attr("href").split("=");
            eventLink.attr("href", eventArr[0]+'='+changeDate)
           // startDate.setDate(startDate.getDate(new Date(e.date.valueOf()))); console.log(startDate);
           // var month = currentDate.getMonth();
            $(".cal-desc-date").text(moment(new Date(e.date).getTime()).format('YYYY-MM-DD'));*/

            $.ajax({
                type: 'post',
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: $("#datetimepicker_personal").data('events')+'?time='+changeDate,
        
                success: function(data) {
                    $("#datetimepicker_personal-tasks").parent().removeClass('overlay');
                    $("#datetimepicker_personal-events").parent().removeClass('overlay');

                    $("#datetimepicker_personal-tasks").html(data.tasks);
                    $("#datetimepicker_personal-events").html(data.events);
                    $(".todo-hours_per_day").text(data.stats.hours_per_day);
                    $(".todo-hours_per_month").text(data.stats.hours_per_month);
                    $(".todo-date").text(data.checkDay);
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                    alert('Something went to wrong.Please Try again later...');
                    $("#datetimepicker_personal-tasks").parent().removeClass('overlay');
                    $("#datetimepicker_personal-events").parent().removeClass('overlay');
                }
            });
                   
        }
        
        if($("#datetimepicker_personal-events").length && (data.action == 'close' || data.action == 'update') ) {
            if(data.action == 'close')
                $("#eid-"+data.id).html(data.eventItem).addClass('bg-green2');
            if(data.action == 'update')
                $("#eid-"+data.id).html(data.eventItem);
        }
  
        if($("#datetimepicker_personal-events").length && data.action == 'delete') {
            $("#eid-"+data.id).remove();
            $.post({
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: '/task/personal/busydays?month='+month+'&year='+year,
        
                success: function(data) {
                    busyDays = [];
                    busyDays = data.days; console.log(moment(new Date(currentDate).getTime()).format('YYYY-MM-DD'));
                    $('#datetimepicker_personal').datepicker('update', currentDate /*moment(new Date(currentDate).getTime()).format('YYYY-MM-DD')*/ );
                    //$('#datetimepicker_personal').datepicker({'date': moment(new Date(currentDate).getTime()).format('YYYY-MM-DD') });
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }
        
        if($("#datetimepicker_personal-tasks").length && (data.action == 'close' || data.action == 'update') ) {
            if(data.action == 'close')
                $("#tid-"+data.id).html(data.eventItem).addClass('bg-green2');
            if(data.action == 'update')
                $("#tid-"+data.id).html(data.eventItem);
        }
  
        if($("#datetimepicker_personal-tasks").length && data.action == 'delete') {
            $("#tid-"+data.id).remove();
            var currentDate = $("#datetimepicker_personal").datepicker('getDate');
            //$("#datetimepicker_personal").datepicker('update', '');
            var month = (new Date(currentDate).getMonth() + 1);
            var year = String(currentDate).split(" ")[3]; //(new Date(e.date).getYear());
            $.post({
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: '/task/personal/busydays?month='+month+'&year='+year,
        
                success: function(data) {
                    busyDays = [];
                    busyDays = data.days; console.log(moment(new Date(currentDate).getTime()).format('YYYY-MM-DD'));
                    $('#datetimepicker_personal').datepicker('update', currentDate /*moment(new Date(currentDate).getTime()).format('YYYY-MM-DD')*/ );
                    //$('#datetimepicker_personal').datepicker({'date': moment(new Date(currentDate).getTime()).format('YYYY-MM-DD') });
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }
        
        if( $(".dashboard-stats").length ) {
            $.ajax({
                type: 'post',
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: $(".dashboard-stats").data('action'),
        
                success: function(data) {
                    console.log('stats');
                    $(".stats-actions").text(data.actions_per_month + ' h');
                    $(".stats-todos").text(data.todos_per_month + ' h');
                    /*$("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');

                    $("#datetimepicker_tasks-cases").html(data.cases);
                    $("#datetimepicker_tasks-tasks").html(data.tasks);*/
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                   /* alert('Something went to wrong.Please Try again later...');
                    $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');*/
                }
            });
        }
        
        if(data.action == 'boxCase') {
            var fids = document.getElementById('correspondence-cases_list');
            var ids = [];
            for (var i = 0; i < fids.options.length; i++) {
                if (fids.options[i].selected) {
                    ids.push(fids.options[i].value);
                }
            }
            ids.push(data.id); 
            $("#correspondence-cases_list").prepend("<option value='" + data.id + "'>" + data.name + "</option>");
            $("#correspondence-cases_list").val(ids);
            var eids = document.getElementById('correspondence-events_list');
            var evids = []; evids.push(0);
            for (var i = 0; i < eids.options.length; i++) {
                if (eids.options[i].selected) {
                    evids.push(eids.options[i].value);
                }
            }
           
            $.ajax({
                type: 'POST',
                url: "/task/default/mevents?ids="+ids+"&without=1&box=1&events="+evids.join(),
                //data: {ids: ids.join(), employees: employees.join()},

                success: function(result, status) {
                    $('#correspondence-cases_list').multiselect('rebuild');
					$('#correspondence-cases_list').multiselect('refresh');
					if($("#menu-list-employee").length) {
						document.getElementById('menu-list-employee').innerHTML = result.employees;
                    }
					if($("#correspondence-departments_list").length) {
						var x = document.getElementById('correspondence-departments_list');
						for (i = 0; i < x.length; i++) {
							if(result.departments.indexOf(x.options[i].value*1) >= 0) {
								x.options[i].selected = true;
							} else {
								x.options[i].selected = false;
							}
						}
						$('#correspondence-departments_list').multiselect('rebuild');
						$('#correspondence-departments_list').multiselect('refresh');
					}
                }
            });
        }
        
        if(data.action == 'generate') {
            var fids = document.getElementById('correspondence-cases_list');
            var ids = [];
            for (var i = 0; i < fids.options.length; i++) {
                if (fids.options[i].selected) {
                    ids.push(fids.options[i].value);
                }
            }
            var eids = document.getElementById('correspondence-events_list');
            var evids = []; evids.push(0);
            for (var i = 0; i < eids.options.length; i++) {
                if (eids.options[i].selected) {
                    evids.push(eids.options[i].value);
                }
            }
            
            $.ajax({
                type: 'POST',
                url: "/task/default/mevents?ids="+ids+"&without=1&box=1&events="+evids.join()+","+data.ids,
                //data: {ids: ids.join(), employees: employees.join()},

                success: function(result, status) {
                    document.getElementById('correspondence-events_list').innerHTML = result.events;
                    $('#correspondence-events_list').multiselect('rebuild');
                    $('#correspondence-events_list').multiselect('refresh');
                }
            });
        }
        
        if(data.action == 'detailsLimit') {
            $("#employees-limit").html(data.html);
            
            var chartData =  {
                labels: data.chartLabel,
                datasets: [
                    {
                        data: data.chartData,
                        backgroundColor: data.chartColor,
                        hoverBackgroundColor: data.chartColor
                    }]
            };
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myMixedChart = new Chart(ctx, {
                type: 'doughnut',
                data: chartData,
                options: {
                    legend: { display: false },
                   /* tooltips: { enabled: false }*/
                }
            });
        }
        
        if(data.detailsStats) {
            var arr = data.detailsStats; 
            $.each(arr, function(index, value) {
                if (value.length) {
                   $(".details_"+index).html(value);
                } else {
                    $(".details_"+index).html('');
                }
            });
        }
        
        if( $("#project-details-percent").length && data.projectDetailsPercent) {
            $("#project-details-percent").attr('class', 'c100 p'+data.projectDetailsPercent+' small green center');
            $("#project-details-percent > span").html(data.projectDetailsPercent+' %');
            $("#project-details-employees").html(data.projectDetailsEmployees);
        }
		
		if(data.action == 'detailsCases') {
            $("#details-cases").html(data.html);
        }
    },
    
    afterSaveFalse: function(txtError, unix, data) {
    },
    
    externalEvent: function() {
        $(document).ready(function() {

            $('#external-events div.external-event').each(function() {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: 'Nowa wizyta lekarza: '+$.trim($(this).text()),
                    backgroundColor: '#f2f2f2',//$.trim($(this).attr("alt")),
                    borderColor: $.trim($(this).attr("alt")),
                    textColor: $.trim($(this).attr("alt")),
                    className: $.trim($(this).attr("id")).split('-')[1],
                };
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 // original position after the drag
                });
            });
            /*$('#external-events .fc-event').each(function() {
                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });


            $('#timetable').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                drop: function() {
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                }
            });*/

        });

    }

}