'use strict';


KAPI.meeting = {

    afterSaveTrue: function(txtError, unix, data) {
        /*if($("#resources").length > 0) {
            $("#resources").fullCalendar( 'refetchEvents' );
            if(data.alert) {
                txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
             } else	{
                txtError = 'Zmiany zosta�y zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            }
            $("#resources").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
             setTimeout(function(){ $("#alert-"+unix).remove(); }, 300);
        }*/
        
        $(".project-body").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
        var arr = data.changes; 
        $.each(arr, function(index, value) {
            if (value.length) {
               $("#meeting-"+index).html(value);
            } else {
                $("#meeting-"+index).html('');
            }
        });
    },
    afterSaveFalse: function(txtError, unix, data) {

        $(".modal-body").parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible" style="margin-bottom:5px;">'+ data.alert +'</div>');
        /*setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);*/
       
    }
};