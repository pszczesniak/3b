(function(){

	"use strict";

	var capitals = {};

	/**
	 * @global
	 */
	var ByteArray = this.Uint8Array || Array;

	/**
	 * Extensions to the built-in JavaScript string prototype
	 *
	 * @class String
	 * @category util/extensions
	 */

	/**
	 * Returns capitalized string. (example becomes Example)
	 *
	 * @returns {string}
	 */
	String.prototype.capitalize = function(){

		return !this.length ? this : (capitals[this] || (capitals[this] = this[0].toUpperCase() + this.slice(1)));

	};

	var FORMAT_RE = /{[^}]*}/g;
	/**
	 * Python-like string format.
	 * Takes an input string as its first parameter, and arguments to augment this string as additional parameters.
	 *
	 * @returns {string}
	 * @example
	 * 'Some {} {}'.format('value', 'here'); // 'Some value here'
	 */
	String.prototype.format = function(){

		var args = arguments,
		    alen = args.length;

		// no arguments given
		if (!alen) return this;

		// second argument is either variable or object with named vars
		else {

			FORMAT_RE.lastIndex = 0;

			var str = this,
			    matches = str.match(FORMAT_RE),
			    mlen = matches ? matches.length : 0,
			    i = 0,
			    name, val;

			// for object with named variables
			if (typeof(args[0]) === 'object'){

				while (i < mlen){

					name = matches[i].replace(/{/, '').replace(/}/, '');
					val = args[0][name];
					var rs = isNaN(parseInt(name)) ? "{" + name + "}" : "{[" + i + "]}";

					var r = new RegExp(rs, "g");
					str = str.replace(r, args[0][name]);

					i++;

				}

				return str;

			} else { // for variables as arguments

				if (!matches || matches.length !== alen)

					throw new RangeError("String#format: wrong number of arguments, " + str);

				while (i < alen){

					// replace {} {} {} ... {}
					if (matches[i].length == 2) str = str.replace('{}', args[i]);

					// replace {0} {1} {2} ... {n}
					else {

						var r = new RegExp("{[" + i + "]}", "g");
						str = str.replace(r, args[i]);

					}

					i++;

				}
				return str;
			}
		}
	};

	/**
	 * Returns byte array containing the contents of the string
	 *
	 * @returns {ByteArray}
	 */
	String.prototype.toByteArray = function(){

		for (var i = 0, length = this.length, array = new ByteArray(length); i < length; i++)

			array[i] = this.charCodeAt(i) & 0xff;

		return array;

	};

	/**
	 * Check whether parameter is a string.
	 *
	 * @param  {*} elem
	 * @return {Boolean}
	 */
	String.isString = function(elem){

		return typeof elem === 'string';

	};

}).call(this);


