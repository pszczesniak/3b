'use strict';


KAPI.resources = {

    calendar: function() {
        $('#resources').fullCalendar({
			defaultView: 'month', 
            timezone: "Europe/Warsaw",
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			//defaultDate: '2015-02-12',
			businessHours: false, // display business hours
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			buttonText: {
				today:    'Dzisiaj',
				month:    'Miesiąc',
				week:     'Tydzień',
				day:      'Dzień'
			},
			firstDay: 1,
			minTime: "07:00:00",
			maxTime: "20:00:00",
			editable: true, 
            disableResizing: true,
            disableDragging: true,
			allDayDefault: false,
			url: true,
			agenda: 'HH:mm', 
			//events: '/task/event/scheduledata',
            events: function(start, end, timezone, callback) {
                var $ids = $('.checkbox_employee:checked').map(function() {return this.value;}).get().join(',');
                var $colors = $('.checkbox_employee:checked').map(function() { return $('label[for='+this.id+']').css('color');  }).get().join(';'); 
                $.ajax({
                    url: '/company/resources/scheduledata',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix(),
                        typeEvent: $("#companyresourcesschedule-type_fk").val(),
						statusEvent: $("#companyresourcesschedule-status").val(),  
						branch: $("#companyresourcesschedule-id_company_branch_fk").val(),
						employee: $("#companyresourcesschedule-id_employee_fk").val(),
                        resource: $("#companyresourcesschedule-id_resource_fk").val(),
                        //employees:  $ids,
                       // colors: $colors
                    },
                    success: function(doc) {
                        var events = [];
                        $(doc).each(function() {
                            events.push({
                                id: $(this).attr('id'),
                                title: $(this).attr('title'),
                                start: $(this).attr('start'),
                                end: $(this).attr('end'),
								backgroundColor: $(this).attr('backgroundColor'),
								borderColor: $(this).attr('backgroundColor'),
								eventColor: $(this).attr('backgroundColor'),
								eventBorderColor: $(this).attr('backgroundColor'),
                               // allDay :  $(this).attr('allDay'),
								icon: $(this).attr('icon')
                            });
                        });
                        callback(events);
                    }
                });
            },
            eventRender: function(event, element) {
                //if(event.icon){          
                    element.find(".fc-title").prepend("<i class='"+event.icon+"'></i>");
                //}
				element.tooltip({title: event.title, container: "body", html: true}); 
            },
			weekends: true,
			allDaySlot : true,
			selectable: true,
			selectHelper: true,
			slotDuration: "00:30:00",
			disableDragging: true,
			disableResizing: true,
			monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
			monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
			dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek','Sobota'],
			dayNamesShort: ['Nie','Pon','Wto','Śro','Czw', 'Pią', 'Sob'],
			select: function(start, end, allDay) {
				$target = $("#modal-grid-item");
                var $action = '/company/resources/bookingajax/0?start='+start.unix()+'&end='+end.unix();
                /*$("#modal-grid-item").find(".modal-title").html('<i class="fa fa-plus"></i>Nowe zdarzenie');
                $("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/task/event/createajax/0?start='+start.unix()+'&end='+end.unix());*/
                $target.find(".modal-title").html('<i class="fa fa-hand-pointer-o"></i>Rezerwacja');
                  
                $target.find(".modalContent")
                          .load($action, 
                                function(response, status, xhr){ 
                                    $target.modal('show'); 
                                    
                                    if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
										if(xhr.status == 302) {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">Zostałeś wylogowany z systemu.</div></div>' );
                                            CheckForSession();
										} else {
											$( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
										}
                                    } 
                                    $target.find(".modalContent").removeClass('preload');
                           });          
                $target.on('shown.bs.modal', function() {
                    relationship();
                    $target.find(".modalContent").removeClass('preload');
                });
		
				return false;
			},
			eventClick: function(calEvent, jsEvent, view) { 
				//console.log(calEvent.id);	
				//var idArr = calEvent.id.split('-');
                $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-eye"></i>Rezerwacja');
                $("#modal-grid-item").modal("show")
                    .find(".modalContent")
                    .load('/company/resources/bookingupdate/'+calEvent.id);
               
				
				$('#myModal').on('hidden.bs.modal', function(){
					$(this).removeData('bs.modal');
				});
				return false;
									  
			}
		});
    }, 
    afterSaveTrue: function(txtError, unix, data) {
       
        if($("#resources").length > 0) {
            $("#resources").fullCalendar( 'refetchEvents' );
            if(data.alert) {
                txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
             } else	{
                txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            }
            $("#resources").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
             setTimeout(function(){ $("#alert-"+unix).remove(); }, 300);
        }
        
        if($("#datetimepicker_resources").length > 0 /*&& data.action == 'accept'*/) {
            //$(".cal-desc-date").text(moment(new Date(e.date).getTime()).format('YYYY-MM-DD'));
            
            var checkedDate = $(".cal-desc-date").text().trim(); 
            //console.log(checkedDate);
            var calendarDateArr=checkedDate.split("-");
            var changeDate = Date.UTC(calendarDateArr[0], (calendarDateArr[1]*1-1), calendarDateArr[2])/1000;

            $.ajax({
                type: 'post',
                cache: false,
                dataType: 'json',
                data: $(this).serialize(),
                url: $("#datetimepicker_resources").data('action')+'?time='+changeDate,
        
                success: function(data) {
                    console.log('test');
                    $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');

                    $("#datetimepicker_tasks-cases").html(data.cases);
                    $("#datetimepicker_tasks-tasks").html(data.tasks);
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                   // alert('Something went to wrong.Please Try again later...');
                    $("#datetimepicker_tasks-cases").parent().removeClass('overlay');
                    $("#datetimepicker_tasks-tasks").parent().removeClass('overlay');
                }
            });
        }
    },
    afterSaveFalse: function(txtError, unix, data) {
        if($("#resources").length > 0) {
            //$("#resources").fullCalendar( 'refetchEvents' );
            $("#resources").parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        }
        
        if($("#datetimepicker_resources").length > 0 /*&& data.action == 'accept'*/) {
            //$("#db-event-"+data.id).find('a.event-close').addClass('done');
            $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
        }
    }
};