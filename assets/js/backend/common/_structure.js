$(function () {
    $(".files-search-text").keyup(function () {
        var that = this,
        value = $(this).val();
       // console.log($('#structure').jstree('get_selected'));
        var o = { 'Files[id_dict_type_file_fk]': $('#structure').jstree('get_selected')[0]};
        
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-files')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
            url: $('#table-files').data("url"), method: 'get',
            query: o
        });
    });
    
    $('#structure')
        .jstree({
            'core': {
                "themes": { "theme": "classic", "dots": false, "icons": false, 'responsive' : true, 'variant' : 'small', 'stripes' : true},
                'html_titles': true,
                'load_open': true,
                'data': {
                    'url': '/document/types/folders',
                    'data': function (node) {
                        return { 'id': node.id === '#' ? '0_0' : node.id, 'noCache': Math.random() };
                    }
                },
                'check_callback': function () { return true; }
            },
            'force_text' : true,
            //'plugins' : ['state','dnd','contextmenu','wholerow'],
            'unique' : {
                'duplicate' : function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins' : ['state','dnd','sort','types','contextmenu','unique','wholerow'],
            'sort' : function(a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu' : {
                'items' : function(node) {
                    var tmp = $.jstree.defaults.contextmenu.items();
                   // delete tmp.create.action;
                    delete tmp.ccp; console.log($("#structure").attr["data-new"]);
                    if( $("#structure").attr("data-new") == "no") {
                        delete tmp.create;
                    } else {
                        tmp.create.label = "Nowy";
                    }
                    if( $("#structure").attr("data-edit") == "no") {
                        delete tmp.rename;
                    } else {
                        tmp.rename.label = "Zmień nazwę";
                    }
                    if( $("#structure").attr("data-remove") == "no") {
                        delete tmp.remove;
                    } else {
                        tmp.remove.label = "Usuń";
                    }

                    if(this.get_type(node) === "file") {
                        delete tmp.create;
                        delete tmp.rename;
                        delete tmp.remove;
                    }
                    console.log(this.get_type(node));
                    if(this.get_type(node) === "root") {
                        delete tmp.rename;
                        delete tmp.remove;
                    }
                    
                   /* if(node.li_attr['data-new'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.create;
                    }
                    if(node.li_attr['data-edit'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.rename;
                    }
                    if(node.li_attr['data-remove'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.remove;
                    }*/
                    return tmp;
                }
            },
            /*'types' : {
                'default' : { 'icon' : 'folder' },
                'file' : { 'valid_children' : [], 'icon' : 'file' }
            },*/
            "types" : {
                "default" : {
                    "icon" : "glyphicon glyphicon-folder-close"
                },
                "root" : {
                    "icon" : "glyphicon glyphicon-tree-deciduous"
                },
                "folder" : {
                    "icon" : "glyphicon glyphicon-folder-close"
                },
                "file" : {
                    'icon' : 'glyphicon glyphicon-file'
                }
            },
        })
        .on('delete_node.jstree', function (e, data) {
            $.get('/document/types/delete', { 'id' : data.node.id })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('create_node.jstree', function (e, data) {
            $.get('/document/types/create', {'type' : data.node.type, 'id' : data.node.parent, 'position' : data.position, 'text' : data.node.text })
                .done(function (d) {
                    data.instance.set_id(data.node, d.id);
                })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('rename_node.jstree', function (e, data) {
            $.get('/document/types/rename', { 'id' : data.node.id, 'text' : data.text })
                .done(function (d) {
                    $.get('/document/types/nodes?id=' + data.node.id, function (d) {
                        $('#data .default').html(d.content).show();
                    });
                })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('changed.jstree', function (e, data) {
            if(data && data.selected && data.selected.length) {
               // if(data.node.type == "file") {
                    var $downloadPath = data.node.a_attr.href;
                    var $table = $("#table-files");
                    var o = { 'Files[id_dict_type_file_fk]': data.selected[0]};
                   // o[Files[id_dict_type_file_fk]].push(data.selected.join(':'));
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $table.bootstrapTable('refresh', {
                        url: $('#table-files').data("url"), method: 'get',
                        query: o
                    });
                   /* $.ajax({
                        type: "GET",
                        url: $downloadPath,
                        success: function(data, status) {
                            console.log($downloadPath);
                            window.location.replace($downloadPath);
                        },
                        error: function(data) {
                            $('.files-container').parent().prepend('<div class="file-alert alert alert-danger">'+data.responseText+'</div>');
                            $('.file-alert').delay(2000).fadeOut(600, function(){
                                $(this).remove();
                            });
                        }
                    });*/
                //    document.location.href = $downloadPath;
                //} else {
                    /*$.get('/document/types/nodes?id=' + data.selected.join(':'), function (d) {
                        $('#data .default').html(d.content).show();
                       
                    });*/
                //}
            }
            else { 
                $('#data .content').hide();
                $('#data .default').text('Select a folder from the tree.').show();
            }
        })
        .on('open_node.jstree', function(e, data){
           $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                .removeClass('glyphicon-folder-close').addClass('glyphicon-folder-open');
        })
        .on('close_node.jstree', function(e, data){
           $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                .removeClass('glyphicon-folder-open').addClass('glyphicon-folder-close');
        })
        .on('select_node.jstree', function (e, data) {
            // console.log(data.selected[0]);
            
            /*if(data.node.type == "file") {
                var href = data.node.a_attr.href;
                document.location.href = href;
            }*/
            
            var $table = $("#table-files");
            var o = { "Files[id_dict_type_file_fk]": data.selected[0]};
           // o[Files[id_dict_type_file_fk]].push(data.selected.join(':'));
            $.each($('#form-files-search').serializeArray(), function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
           
            $table.bootstrapTable({
                url: $(this).data("url"),
                query: o,
                idField: 'id',
                uniqueId: 'id',
                columns: [
                            { field: 'id',title: 'ID', visible: false},
                            { field: 'name',title: 'Nazwa'},
                            { field: 'created', title: 'Data'}, 
                            { field: 'size',title: 'Rozmiar [kB]'},
                            { field: 'ext',title: ''},
                            { field: 'actions',title: '', 'events': 'actionEvents'},
                        ]
            });
        });

});

