// Prevent bootstrap dialog from blocking focusin
$(document).on('focusin', function(e) {
    if ($(e.target).closest(".mce-window").length) {
		e.stopImmediatePropagation();
	}
});

$(document).ready(function() {
	
    $('.select2').select2({dropdownAutoWidth : true,  width: '100%'});
    
    $("select.ms-select").chosen({disable_search_threshold:10, placeholder_text_multiple: 'wybierz',  width: '100%'});
    $("select.province_additional").chosen({max_selected_options: $("select.province_additional").data('amount'), placeholder_text_multiple: 'wybierz dodatkowe województwa'});
    
    $('.ms-options').multiselect({includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        selectAllText: 'Zaznacz wszystko',
        nonSelectedText: 'wybierz opcje'
    });
   
	//$('.nav-pills, .nav-tabs').tabdrop();

	$(".gridViewDictModal").click(function(){
	    var tagname = $(this)[0].tagName; 
    
        if( $("#dictionarysearch-id").length > 0 ) {
            var valueId = $("#dictionarysearch-id").val(); 
            if( valueId != 0 && valueId != '' )
                var $action = $(this).attr("href")+'/'+valueId;
            else
                var $action = $(this).attr("href")+'/0';
        } else {
            var $action = $(this).attr("href");
        }     
        
	   $("#modal-grid-view-dict").find(".modal-title").html($(this).data("title"));
	   $("#modal-grid-view-dict").modal("show")
				  .find(".modalContent")
				  .load($action);
	   return false;   
    });
       
    $('#imageGallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:9,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }   
    });
  
	if( $("#playlist").length > 0 ) {
		$("#playlist li").on("click", function() {
			$("#videoarea").attr({
				"src": $(this).attr("movieurl"),
				"poster": "",
				"autoplay": "autoplay"
			})
		})
	}
	
	if( $("#videoarea").length > 0 ) {
		$("#videoarea").attr({
			"src": $("#playlist li").eq(0).attr("movieurl"),
			"poster": $("#playlist li").eq(0).attr("moviesposter")
		})
	}
 
    $("#save-table-items-move").click(function() {
		var saveUrl = $(this).attr('href');
        var tableId = $(this).attr('data-table'); 
		$.ajax({
			method: "POST",
			url: saveUrl,
			dataType : 'json',
			data: {items: $(tableId).bootstrapTable('getData') }
		}).done(function( result ) {
			if(result.success) {
				$(tableId+'-info-message').removeClass('alert-danger').removeClass('alert-info').addClass('alert alert-success').text(result.alert);
	
			} else {
				$(tableId+'-info-message').removeClass('alert-success').removeClass('alert-info').addClass('alert alert-danger').text(result.alert);
			}
		});
		return false;
	});
    
    $(".fb-publish").on("click", function() {
        var $urlPath = $(this).attr('data-action');
        $(".fb-publish-result").html('<div class="alert alert-info">Trwa łączenie z FB...</div>');
        $.ajax({
            type: 'post',
            url: $urlPath,
            async: false,
            success: function (data) {
                console.log(data.result);
                $(".fb-publish-result").html(data.alert);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });	
    
});

$(document).on("submit", ".modalAjaxForm", function (event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    
    var $form = $(this);//$("#modal-grid-view-form-dict");
	var $target = $($form.attr("data-target")); 
    var $table = $($form.attr("data-table"));
	var $input = $($form.attr("data-input"));
    $target.find(".modalContent").addClass('preload');
    $.ajax({
        type: 'post',
        cache: false,
        dataType: 'json',
        data: $(this).serialize(),
        url: $(this).attr('action'),
        //beforeSend: function() {  },
        success: function(data) {
            $target.find(".modalContent").removeClass('preload');
			if(data.success == false) { 
               
                $target.find(".modalContent").html(data.html);
                
                var arr = data.errors; var txtError = '';
                $.each(arr, function(index, value)  {
                    if (value.length != 0)  {
                        txtError += '<strong>'+ value +'</strong><br/>';
                    }
                });
   
                txtError += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                $target.find(".modal-body").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
            } else {
                //console.log('ok');
                if($target.length > 0 ) {
                    if(data.close == false) {
                        $target.find(".modalContent").html(data.html);
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
						var unix = Math.round(+new Date()/1000);
						$(".action-form").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else {
                        $target.modal("hide");
                    }
                }
                
                if( $table.length == 0 && $("#table-events").length > 0 ) {   $table = $("#table-events");   }
                
                if($table.length > 0 ) {
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    $table.bootstrapTable('refresh', { url: $('#table-data').data("url"), method: 'get',  query: o  });
                    //$table.find('tbody tr:eq(0)').addClass('success');
                    $('tr[data-index='+data.index+']').addClass('success');
                    setTimeout(function(){
                        $('tr[data-index='+data.index+']').removeClass('success', 1000, "easeInBack");
                    }, 1000);
				
					if(data.alert) {
						var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					} else	{
						var txtError = 'Zmiany zostały zapisane.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
					}
					var unix = Math.round(+new Date()/1000);
					$table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible" style="margin-bottom:5px;">'+ txtError +'</div>');
					 setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
				
				
					if(data.action =="insertRow") {
						//console.log('insert');
						$table.bootstrapTable('selectPage', 1);
						$('tr[data-index='+data.index+']').addClass('success');
					}
			    }
             
				/*if($input.length) {
                    var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
                    var values = [];
					$input.prepend(opt);
                    values.push(data.id);
					$input.val(values);
					
					$target.modal("hide");
                }*/
				if(data.action == 'createByInput') {
					var fids = document.getElementById(data.input);
					var ids = [];
					for (var i = 0; i < fids.options.length; i++) {
						if (fids.options[i].selected) {
							ids.push(fids.options[i].value);
						}
					}
					ids.push(data.id);
					$("#"+data.input).prepend("<option value='" + data.id + "'>" + data.name + "</option>");
					$("#"+data.input).val(ids);				
				    $("#"+data.input).multiselect('rebuild');
					$("#"+data.input).multiselect('refresh');
				}
                
                if(data.action == 'add_note') {
                    if(data.parent == 0)
                        $(".chat-box-history").prepend(data.html);
                    else
                         $(".chat-box-history #comment-"+data.parent).append(data.html);
                }
            }
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
			$target.find(".modalContent").removeClass('preload');
            alert('Something went to wrong.Please Try again later...');
        }
    });
    return false;
});

$(document).on("submit", ".modal-grid", function (event) {
	var $form = $(this); 
	var $target = $($form.attr("data-target"));
	var $table = $($(this).attr('data-table'));

	$.ajax({
		type: $form.attr("method"),
		url: $form.attr("action"),
		data: $form.serialize(),

		success: function(data, status) {
            if(data.result) {
				$table.bootstrapTable('refresh');
				$target.modal("hide");
			} else { 
				$target.find(".modalContent").html(data.html);
			}
		}
	});
    
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});

$(document).on("click", ".gridViewModal", function (event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $target = $($(this).attr('data-target'));  
    var $editor = $(this).data('editor');
    $target.find(".modalContent").addClass('preload');
    if( $("#dictionarysearch-id").length > 0 ) {
        var valueId = $("#dictionarysearch-id").val(); 
        if( valueId != 0 && valueId != '' )
            var $action = $(this).attr("href")+'/'+valueId;
        else
            var $action = $(this).attr("href")+'/0';
    } else {
        var $action = $(this).attr("href");
    }
    
    /*if ($target.data('bs.modal').isShown) {
        $target.modal("hide");
    }*/
    
    $target.find(".modal-title").html($(this).data("title"));
   
    $target.find(".modalContent")
			  .load($action, 
                    function(response, status, xhr){ 
                        $target.modal('show'); 
                        
                        if ( status == "error" ) {
                            var msg = "Sorry but there was an error: ";
                            $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                        } 
                        //$target.find(".modalContent").removeClass('preload');
                        setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
               });          
    $target.on('shown.bs.modal', function() {
        relationship();
        if($("#item-form-field").length) { 
            //alert('reset: '+$("#sitestepsform-describe").val());
            document.getElementById("item-form-field").reset();
            document.getElementById("sitestepsform-describe").value = '';
            //$("#sitestepsform-describe").val(null);
        }
        //tinymce.remove("content");
        //tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'content');
        /*if (tinymce.getInstanceById("content"))
            tinymce.EditorManager.execCommand('mceRemoveControl', true, "content");*/
        if($editor)
            KAPI.tinyMceFull();
        $target.find(".modalContent").removeClass('preload');
	});
    
    $target.on('hide.bs.modal', function() {
        /*relationship();*/
        if($editor)
            tinymce.remove("#content");
	});
	
   return false;   
});

$(window).load(function() {
	
});

function relationship() {
    
    $("input.number").keyup(function(e){
        var key = e.which ? e.which : event.keyCode;
        if(key == 110 || key == 188){
          e.preventDefault();
          var value = $(this).val();         
          $(this).val(value.replace(",","."));
        }   
    });
    
    if($('#table-persons').length > 0) {
        //$('#table-persons').bootstrapTable('refresh',  {url: '../json/data1.json'});
        $('#table-persons').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'firstname',title: 'Imię'},
                        { field: 'lastname', title: 'Nazwisko'}, 
                        { field: 'position',title: 'Stanowisko'},
                        { field: 'contact',title: 'Kontakt'},
                        { field: 'image',title: ''},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if($('#table-events').length > 0) {
        //console.log('events');
        //$('#table-persons').bootstrapTable('refresh',  {url: '../json/data1.json'});
        $('#table-events').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'name',title: 'Tytuł'},
                        { field: 'date_start', title: 'Start'}, 
                        { field: 'date_end',title: 'Koniec'},
                        { field: 'employees',title: 'Pracownicy'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if( $("#table-rates").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-rates').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'type',title: 'Typ pracownika'},
                        { field: 'employee',title: 'Pracownik'},
                        { field: 'rate',title: 'Stawka'},
                        { field: 'date_from', title: 'Obowiązuje od'}, 
                        { field: 'date_to',title: 'Obowiązuje do'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    
    if( $("#table-discounts").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-discounts').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'period',title: 'Okres'},
                        { field: 'amount',title: 'Rabat [%]'},
                        { field: 'name',title: 'Nazwa'},
                        { field: 'actions',title: '', 'events': 'actionEvents'},
                    ]
        });
    }
    /*if( $("#table-settlements").length > 0 ) { 
        //$("#table-rates").bootstrapTable('resetView');
        $('#table-settlements').bootstrapTable({
            url: $(this).data("url"),
            idField: 'id',
            uniqueId: 'id',
            columns: [
                        { field: 'id',title: 'ID', visible: false},
                        { field: 'customer',title: 'Klient'},
                        { field: 'order',title: 'Zlecenie'},
                        { field: 'discount',title: 'Rabat [%]'},
                    ]
        });
    }*/
    
    if($("#table-settled").length > 0) { 
        //$("#table-settled").bootstrapTable('resetView');
        $("#table-settled").bootstrapTable('refresh');
    }
    
    if($('fieldset.collapsible > legend > span').length == 0)
        $('fieldset.collapsible > legend').append(' (<span style="font-family: monospace;">+</span>)');
        
    $('fieldset.collapsible > legend').off('click').click(function () {
        //console.log('fieldset collapsible click');
        var $divs = $(this).siblings();
        $divs.toggle();

        $(this).find('span').text(function () {
            return ($divs.is(':visible')) ? '-' : '+';
        });
    });
    $('.selecctall').click(function(event) {  //on click
        //console.log('.'+$(this).attr('id'));
        if(this.checked) { // check select status
            $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1" 
            });
        }else{
            $('.'+$(this).attr('id')).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
    
    $('#imgAttachmentPerson').imageAttachment({
        "hasImage":$('#imgAttachmentPerson').data('hasimage'),
        "previewUrl":$('#imgAttachmentPerson').data('previewurl'),
        "previewWidth":300,
        "previewHeight":200,
        "apiUrl":$('#imgAttachmentPerson').data('apiurl')
    });
    
    $('.select2').select2({dropdownAutoWidth : true,  width: '100%'});
    $("select.chosen-select").chosen({disable_search_threshold:10});
    $('.ms-resource').multiselect({includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        nonSelectedText: 'wybierz opcje'
    });
    $('.ms-select-ajax').multiselect({
        includeSelectAllOption: true,
        selectAllValue: 0,
        maxHeight: 220,
        nonSelectedText: 'wybierz opcje',
        onChange: function (option, checked) {
            //get all of the selected tiems
            var values = $('.ms-select-ajax option:selected');
            var selected = "";
            var ids = new Array(); var employees = new Array();
            $(values).each(function (index, value) {
                selected += $(value).prop('value') + ",";
                ids.push($(this).val()); 
            });
            selected = selected.substring(0, selected.length - 1);
            //update hidden field with comma delimited string for state persistance
            
            $("input[name='employees[]']:checked").each(function(){
                employees.push($(this).val()); 
            });
            
                        
            if($("#menu-list-employee-task").length > 0 ){
                type = $('.ms-select-ajax').data('type');
                if( $("#calcase-type_fk").length ) {
                    if($("#calcase-type_fk").val() == 1) type = 'matter'; else type = 'project';
                }
                var box = ( $('.ms-select-ajax').data('box') ) ? $('.ms-select-ajax').data('box') : 0;
                $.ajax({
                    type: 'POST',
                    url: '/company/department/employees?type='+type+'&box='+box,
                    data: {ids: ids.join(), employees: employees.join()},

                    success: function(data, status) {
                        if(data.list) {
                            $("#menu-list-employee-task").html(data.list);
                        } else {
                            $("#menu-list-employee-task").html('<li>brak danych</li>');
                        }
             
                    }
                });
            }
        },
    });
    
   /* $("#caltask-id_case_fk").change(function() {
    });*/
}

$(document).on("submit", "#item-form-field", function (event) {
	var $form = $(this); //$($(this).attr('data-form'));
	var $target = $($form.attr("data-target"));
	var $table = $($(this).attr('data-table'));

	$.ajax({
		type: $form.attr("method"),
		url: $form.attr("action"),
		data: $form.serialize(),

		success: function(data, status) {
            if(data.result) {
				//$table.bootstrapTable('refresh');
				if(data.type == 2) {
					$table.bootstrapTable('updateRow', {
						index: data.index,
						row: data.row /*{
							id: randomId,
							name: 'Item ' + randomId,
							price: '$' + randomId
						}*/
					});
				} else {
					$table.bootstrapTable('insertRow', {
						index: data.index,
						row: data.row /*{
							id: randomId,
							name: 'Item ' + randomId,
							price: '$' + randomId
						}*/
					});
				}
				$target.modal("hide");
			} else { 
				$target.find(".modalContent").html(data.html);
			}
		}
	});
    $("#save-table-items-move").removeClass("none");
	event.stopImmediatePropagation();
	event.preventDefault();
	return false;
});

$(document).on("click", '.insertInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $(this).attr('data-target'); 
    var $input = $(this).attr('data-input'); 
    //$target.load($(this).attr("href"));
    $($target).load($(this).attr("href"), 
                function(response, status, xhr){                     
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } else {
                        $(".saveInline").attr('data-target', $target);
                        $(".saveInline").attr('data-input', $input);
                        $($target).removeClass('none');
                    }
                        
            });  
 
    return false;
});

$(document).on("click", '.saveInline', function(event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    
    var $target = $($(this).attr('data-target')); 
    var $input = $($(this).attr('data-input')); 
    
    if(!$("#dictValue").val()) {
        $("#dictValue").addClass('bg-red2');
        $("#dictValue").attr('placeholder', 'Proszę wpisać wartość słownika');
    } else {
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {value: $("#dictValue").val()},
            url: $(this).attr('href'),
            success: function(data) {              
                if(data.success) {
                    if($input.length) {
                        var opt = "<option value='" + data.id + "'>" + data.name + "</option>";
                        var values = [];
                        $input.prepend(opt);
                       
                        values.push(data.id);
                        $input.val(values);
                        
                        if($input.hasClass('ms-options')) {
                            $('#correspondence-addresses_list').multiselect('rebuild');
                            $('#correspondence-addresses_list').multiselect('refresh');
                        }
                        $target.addClass('none');
                    }
                } else {    
                    $("#dictValueHint").text(data.alert);
                }
            },
            /*error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                //$target.find(".modalContent").removeClass('preload');
                //alert('Something went to wrong.Please Try again later...');
                CheckForSession();
            }*/
        });
    }
    
    //var $target = $($(this).attr('data-target')); 
    //$target.load($(this).attr("href"));
    //$target.removeClass('none');
    
    return false;
});

$(document).on("click", '.closeInline', function(event) { 
    $( event.target ).parent().parent().addClass('none');
});