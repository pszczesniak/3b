$(document).ready( function() {
	$('#toolbar-data').on('click', "#btn-refresh", function (e) {
        var o = {};
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data').bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'get',
            query: o
        });
    });
    
    $(".data-search-text").keyup(function () {
        var that = this,
        value = $(this).val();
        
       // if (value.length >= 3 ) {
            var o = {};
            $.each($('#form-data-search').serializeArray(), function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            
            $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                url: $('#table-data').data("url"), method: 'get',
                query: o
            });
    });
    

    $(".data-search-list").change(function () {
        //$("#table-data").bootstrapTable('refresh');
        var o = {};
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'post',
            query: o
        });

    });
    
    /* table widgets */
    
    $('.table-widget').bootstrapTable({
        method: 'get',
        queryParams: function(params) {
            //console.log(this['searchForm']);
            //var o = {};
            $.each($(this['searchForm']).serializeArray(), function() {
                if (params[this.name] !== undefined) {
                    if (!params[this.name].push) {
                        params[this.name] = [params[this.name]];
                    }
                    params[this.name].push(this.value || '');
                } else {
                    params[this.name] = this.value || '';
                }
            });
            //console.log(params);
            return params;
        }
    });
    
    $('.toolbar-table-widget').on('click', ".btn-refresh-table", function (e) {
       /* var o = {};
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });*/
        var $table = $(this).data("table"); 
        //console.log($table);
        $($table).bootstrapTable('refresh'/*, {
            url: $($table).data("url"), method: 'get',
            query: o
        }*/);
    });
    
    $(".widget-search-text").keyup(function () {
        var that = this,
        value = $(this).val();
        var $table = $(this).data("table");
        var $form = $(this).data("form");
       // if (value.length >= 3 ) {
            var o = {};
           
            $($table).bootstrapTable('refresh'/*, {
                url: $('#table-data').data("url"), method: 'get',
                query: o
            }*/);
    });
    

    $(".widget-search-list").change(function () {
        //$("#table-data").bootstrapTable('refresh');
        var $table = $(this).data("table");
        var $form = $(this).data("form");
     
        $($table).bootstrapTable('refresh' /*{
            url: $($table).data("url"), method: 'post',
            query: o
        }*/);

    });
    var containerForm = document.getElementById("merge-matters-form");
    /*$("#table-matters").on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {*/
    $("#table-matters").on('check.bs.table', function (e, row, $element) {    
        $("#merge").prop('disabled', !$("#table-matters").bootstrapTable('getSelections').length);
		var input = document.createElement("input");
		input.type = "checkbox";
		input.name = "ids[]";
		input.classList.add('none');
		input.checked = true;
		input.value = row['nid'];
		input.id = "m-" + row['nid'];
		containerForm.appendChild(input);
        //selections = getIdSelections(); 
    });
    
    $("#table-matters").on('uncheck.bs.table', function (e, row, $element) {
        $("#merge-").prop('disabled', !$("#table-matters").bootstrapTable('getSelections').length);
		var child = document.getElementById("m" + row['nid']);
		containerForm.removeChild(child);
        //selections = getIdSelections(); 
    });
} );

function getIdSelections() {
    return $.map($("#table-matters").bootstrapTable('getSelections'), function (row) {
        return row.nid
    });
}

window.actionEvents = {
	'click .up': function (e, value, row, index) {
        //console.log('up');
        var tableId = $(this).attr('data-table'); 
        if(index > 0) {
            var source = JSON.stringify($(tableId).bootstrapTable('getData')[index]);
            var target = JSON.stringify($(tableId).bootstrapTable('getData')[index - 1]);
            $(tableId).bootstrapTable('updateRow', {'index':index - 1, 'row': JSON.parse(source)});
            $(tableId).bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(tableId+'-info-message').addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-items-move").removeClass("none");
        }
	},
	'click .down': function (e, value, row, index) { 
        var tableId = $(this).attr('data-table'); 
        //console.log( ($('#table-items tr').length-2) + ' > ' + (index));
        if(($(tableId+' tr').length-2) > (index)) {
            var source = JSON.stringify($(tableId).bootstrapTable('getData')[index]);
            var target = JSON.stringify($(tableId).bootstrapTable('getData')[index + 1]);
            $(tableId).bootstrapTable('updateRow', {'index':index + 1, 'row': JSON.parse(source)});
            $(tableId).bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(tableId+'-info-message').addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-items-move").removeClass("none");
        }
	},
    'click .update': function(event, value, row, index) {
		event.stopImmediatePropagation();
		event.preventDefault();
	    var tagname = $(this)[0].tagName;   
		if($("#dictionarysearch-id").length > 0)
			var $action = $(this).attr("href")+'/'+$("#dictionarysearch-id").val()+'?index='+index;
		else
			var $action = $(this).attr("href")+'?index='+index;
		$($(this).attr('data-target')).find(".modal-title").html($(this).data("title"));
        $($(this).attr('data-target')).find(".alert").remove();
	    $($(this).attr('data-target')).modal("show")
				  .find(".modalContent")
				  .load($action, function( response, status, xhr ) {
                                      if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
                                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                                      }
                                      $($(this).attr('data-target')).find(".modalContent").removeClass('preload');
                                    }
                    );
                  
        $( "#success" ).load( "/not-here.php", function( response, status, xhr ) {
          if ( status == "error" ) {
            var msg = "Sorry but there was an error: ";
            $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
          }
        });          
                  
		/*$($(this).attr('data-target')).on('shown.bs.modal', function() {		   
		    relationship();
		});*/
		
	   return false;   
	},
    'click .remove': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $('#modalConfirmDelete').find(".modal-title i").addClass('fa-trash').removeClass('fa-lock');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Usu�');
        
        /*var o = {};
        //console.log($('#form-data-search').serializeArray());
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data').bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'get',
            query: o
            
            //queryParams:function(p){
            //    return  { main_cat_id : 1 } 
            //}
        });*/
	},
    'click .lock': function (e, value, row, index) { 
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Zablokuj');
        
       
	},
    'click .unlock': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirmDelete').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirmDelete').data('id', id).modal('show');
        $("#btnYesDelete").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesDelete").attr("data-row", row.id);
		$("#btnYesDelete").attr("data-table", table);
        $("#btnYesDelete").html('Odblokuj');
	},
    'click .edit': function (e, value, row, index) {
		//alert('You click like action, row: ' + JSON.stringify(row));
		var tableId = $(this).attr('data-table'); 
		e.stopImmediatePropagation();
		e.preventDefault();
								
	    var tagname = $(this)[0].tagName;    
	    $($(this).attr('data-target')).find(".modal-title").html($(this).data("title"));
	    $($(this).attr('data-target')).modal("show")
				  .find(".modalContent")
				  .load($(this).attr("href"));
		//console.log(row);		  
	    $($(this).attr('data-target')).on('shown.bs.modal', function() {
			$('#item-form-field input#sitestepsform-index').val(index);
			$('#item-form-field input#sitestepsform-describe').val(row.describe);
		});
		$(tableId+'-info-message').addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
		$("#save-table-items-move").removeClass("none");
			  
	   return false;   
	},
    'click .item_remove': function (e, value, row, index) {
		var tableId = $(this).attr('data-table'); 
        $(tableId).bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});
		$(tableId+'-info-message').addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
		$("#save-table-items-move").removeClass("none");
	}
};

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    /*if ( (row.delay*1) > 0 && (row.status != 'Zako�czone') ) {
        return {
            classes: 'danger'
        };
    }*/
    if ( row.className )
        return {
            classes: row.className
        };
    if ( row.empStatus == -1 ) {
        return {
            classes: 'danger'
        };
    }
    return {};
}