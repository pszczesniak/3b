$(function () {
    $(window).resize(function () {
        var h = Math.max($(window).height() - 0, 420);
        $('#container, #data, #tree, #structure').height(h).filter('.default').css('lineHeight', h + 'px');
    }).resize();

    $('#tree')
        .jstree({
            /*'core' : {
                'data' : {
                    'cache':false,
                    'url' : '/document/default/folders',
                    'data' : function (node) {console.log(node);
                        return { 'id' : node.id };
                    }
                },
               
                'check_callback' : true,
                'force_text' : true,
                'themes' : {
                    'responsive' : true,
                    'variant' : 'small',
                    'stripes' : true
                }
            },*/
            'core': {
                "themes": { "theme": "classic", "dots": false, "icons": false, 'responsive' : true, 'variant' : 'small', 'stripes' : true},
                'html_titles': true,
                'load_open': true,
                'data': {
                    'url': '/document/default/folders',
                    'data': function (node) {
                        return { 'id': node.id === '#' ? '0_0' : node.id, 'noCache': Math.random() };
                    }
                },
                'check_callback': function () { return true; }
            },
            'force_text' : true,
            //'plugins' : ['state','dnd','contextmenu','wholerow'],
            'unique' : {
                'duplicate' : function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins' : ['state','dnd','sort','types','contextmenu','unique','wholerow'],
            'sort' : function(a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu' : {
                'items' : function(node) {
                    var tmp = $.jstree.defaults.contextmenu.items();
                   // delete tmp.create.action;
                    delete tmp.ccp; console.log($("#tree").attr["data-new"]);
                    if( $("#tree").attr["data-new"] == "no") {
                        delete tmp.create;
                    } else {
                        tmp.create.label = "Nowy";
                    }
                    if( $("#tree").attr["data-edit"] == "no") {
                        delete tmp.rename;
                    } else {
                        tmp.rename.label = "Zmień nazwę";
                    }
                    if( $("#tree").attr["data-remove"] == "no") {
                        delete tmp.remove;
                    } else {
                        tmp.remove.label = "Usuń";
                    }
                    
                    
                    
                   /* tmp.create.submenu = {
                        "create_folder" : {
                            "separator_after"	: true,
                            "label"				: "Folder",
                            "action"			: function (data) {
                                var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                inst.create_node(obj, { type : "default" }, "last", function (new_node) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                });
                            }
                        },
                        "create_file" : {
                            "label"				: "Plik",
                            "action"			: function (data) {
                                var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                inst.create_node(obj, { type : "file" }, "last", function (new_node) {
                                    setTimeout(function () { inst.edit(new_node); },0);
                                });
                            }
                        }
                    };*/
                    
                    if(this.get_type(node) === "file") {
                        delete tmp.create;
                        delete tmp.rename;
                        delete tmp.remove;
                    }
                    console.log(this.get_type(node));
                    if(this.get_type(node) === "root") {
                        delete tmp.rename;
                        delete tmp.remove;
                    }
                    
                   /* if(node.li_attr['data-new'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.create;
                    }
                    if(node.li_attr['data-edit'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.rename;
                    }
                    if(node.li_attr['data-remove'] == "no") {
                        // Delete the "delete" menu item
                        delete tmp.remove;
                    }*/
                    return tmp;
                }
            },
            /*'types' : {
                'default' : { 'icon' : 'folder' },
                'file' : { 'valid_children' : [], 'icon' : 'file' }
            },*/
            "types" : {
                "default" : {
                    "icon" : "glyphicon glyphicon-folder-close"
                },
                "root" : {
                    "icon" : "glyphicon glyphicon-tree-deciduous"
                },
                "folder" : {
                    "icon" : "glyphicon glyphicon-folder-close"
                },
                "file" : {
                    'icon' : 'glyphicon glyphicon-file'
                }
            },
        })
        .on('delete_node.jstree', function (e, data) {
            $.get('/document/default/delete', { 'id' : data.node.id })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('create_node.jstree', function (e, data) {
            $.get('/document/default/create', {'type' : data.node.type, 'id' : data.node.parent, 'position' : data.position, 'text' : data.node.text })
                .done(function (d) {
                    data.instance.set_id(data.node, d.id);
                })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('rename_node.jstree', function (e, data) {
            $.get('/document/default/rename', { 'id' : data.node.id, 'text' : data.text })
                .done(function (d) {
                    $.get('/document/default/nodes?id=' + data.node.id, function (d) {
                        $('#data .default').html(d.content).show();
                    });
                })
                .fail(function () {
                    data.instance.refresh();
                });
        })
        .on('changed.jstree', function (e, data) {
            if(data && data.selected && data.selected.length) {
               // if(data.node.type == "file") {
                    var $downloadPath = data.node.a_attr.href;
                   /* $.ajax({
                        type: "GET",
                        url: $downloadPath,
                        success: function(data, status) {
                            console.log($downloadPath);
                            window.location.replace($downloadPath);
                        },
                        error: function(data) {
                            $('.files-container').parent().prepend('<div class="file-alert alert alert-danger">'+data.responseText+'</div>');
                            $('.file-alert').delay(2000).fadeOut(600, function(){
                                $(this).remove();
                            });
                        }
                    });*/
                //    document.location.href = $downloadPath;
                //} else {
                    $.get('/document/default/nodes?id=' + data.selected.join(':'), function (d) {
                        $('#data .default').html(d.content).show();
                       /* if(data.node.type == "file") {
                            $.ajax({
                                type: "GET",
                                url: $downloadPath,
                                success: function(data, status) {
                                    console.log($downloadPath);
                                    //window.location.replace($downloadPath);
                                    document.location.href = $downloadPath;
                                },
                                error: function(data) {
                                    $('.files-container').parent().prepend('<div class="file-alert alert alert-danger">'+data.responseText+'</div>');
                                    $('.file-alert').delay(2000).fadeOut(600, function(){
                                        $(this).remove();
                                    });
                                }
                            });
                            //document.location.href = $downloadPath;
                        }*/
                    });
                //}
            }
            else { 
                $('#data .content').hide();
                $('#data .default').text('Select a folder from the tree.').show();
            }
        })
        .on('open_node.jstree', function(e, data){
           $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                .removeClass('glyphicon-folder-close').addClass('glyphicon-folder-open');
        })
        .on('close_node.jstree', function(e, data){
           $('#' + data.node.id).find('i.jstree-icon.jstree-themeicon').first()
                .removeClass('glyphicon-folder-open').addClass('glyphicon-folder-close');
        })
        .on('select_node.jstree', function (e, data) {
            // console.log(data);
            
            /*if(data.node.type == "file") {
                var href = data.node.a_attr.href;
                document.location.href = href;
            }*/
        });

});


$(document).on("click", ".btn-file-tree", function (event) {
    var $form = $(this);
	var $tree = $('#tree').jstree({});
	var $label = $form.data("label");
	var $inputFile = $form.attr("data-inputfile");
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    //var formData = new FormData($(this)[0]); //console.log(formData);
    var formData = new FormData();
    //formData.append( 'Files[files][]', $(".ufile")[0].files[0] );
    $.each( $(".ufile")[0].files, function(i, file) {
        //ajaxData.append('photo['+i+']', file);
        formData.append( 'Files[files]['+i+']', file );
     } ); 
    formData.append( 'Files[title_file]', $(".ufile-name").val());
    formData.append( 'Files[id_type_file_fk]', $(this).data("type"));
    formData.append( 'Files[id_fk]', $(this).data("id"));
    //formData.append( '_csrf', csrfToken );
        //alert($form.data("action"));
    $.ajax({
        type: "POST",
		url: $form.data("action"),
        data:  formData,
        async: false,
        success: function (data) {
			if(data.result) {
				$form.closest('form').find('input[type=file], input[type=text]').val(''); 
				//console.log('$inputFile: '+$inputFile+', $label: '+$label);
				$("label[for='"+$inputFile+"']").text($label);
				//$table.bootstrapTable('refresh');
                $(".files-set").prepend(data.new);
                
				$('.file-upload-alert').prepend("<div class='alert alert-success message-remove'>"+data.success+"</div>");
                setTimeout(function() {
                  $('.message-remove').remove();
                }, 2000);
                
                //$('#tree').jstree(true).create_node(data.id, data.node, 0);
                $('#tree').jstree(true).refresh_node(data.node);
                $('#tree').jstree(true).open_node(data.node);
                $('#tree').jstree(true).select_node(data.node);
               /* $selectedNode = $('#tree').jstree(true).get_selected(); console.info($selectedNode);
                $tree.set_id(data.node, data.id);*/
                $(".pattern-details tbody").append(data.row);

			} else {
				$('.file-upload-alert').prepend("<div class='alert alert-danger message-remove'>"+data.error+"</div>");
                setTimeout(function() {
                  $('.message-remove').remove();
                }, 2000);
			}
        },
        cache: false,
        contentType: false,
        processData: false
    });
    event.stopImmediatePropagation();
	event.preventDefault();
   // return false;
});


$(document).on("click", ".pattern-action", function (event) {
    event.stopImmediatePropagation();
	event.preventDefault();
    var tagname = $(this)[0].tagName; 
    var $table = $($(this).attr('pattern-details'));  
    var $action = $(this).attr("href");

    $.ajax({
        type: 'post',
        cache: false,
        dataType: 'json',
        //data: $(this).serialize(),
        url: $action,
        beforeSend: function() { 
           // $("#validation-form").hide().empty(); 
        },
        success: function(data) {
            if(data.success == false) {
                var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                $("#data").prepend('<div class="alert alert-danger alert-dismissible">'+txtError+'</div>');	
            } else {
				var txtError = data.alert + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
				$("#data").prepend('<div class="alert alert-success alert-dismissible">'+txtError+'</div>');	
				$('.patterns').html(data.html);
                $('#tree').jstree(true).refresh_node(data.node);
                $('#tree').jstree(true).open_node(data.node);
                $('#tree').jstree(true).select_node(data.node);
            }
        },
        error: function(xhr, textStatus, thrownError) {
            console.log(xhr);
            alert('Something went to wrong.Please Try again later...');
        }
    });
	
   return false;   
});