var selections = [];

//$.fn.editable.defaults.mode = 'inline';
//$.fn.editable.defaults.container = 'body';
/*$.fn.editable.defaults.display = function(value, sourceData) {
    var displayFormatterFunction = $(this).data('display-formatter');
    if (displayFormatterFunction) {
        scope[displayFormatterFunction].call(this, value, sourceData);
    } else {
        $(this).html(value);
    }
};*/
$(document).ready( function() {
    
    $(".data-search-list").change(function () {
        //$("#table-data").bootstrapTable('refresh');
        var o = {};
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'post',
            query: o
        });

    });
    
    $('.table-widget').bootstrapTable({
        method: 'get',
        queryParams: function(params) {
            //console.log(this['searchForm']);
            //var o = {};
            $.each($(this['searchForm']).serializeArray(), function() {
                if (params[this.name] !== undefined) {
                    if (!params[this.name].push) {
                        params[this.name] = [params[this.name]];
                    }
                    params[this.name].push(this.value || '');
                } else {
                    params[this.name] = this.value || '';
                }
            });
            //console.log(params);
            return params;
        },
    });
    
    $('.toolbar-table-widget').on('click', ".btn-refresh-table", function (e) {

        var $table = $(this).data("table"); 
        //console.log($table);
        $($table).bootstrapTable('refresh'/*, {
            url: $($table).data("url"), method: 'get',
            query: o
        }*/);
    });
    
    $(document).on('click', ".btn-reset-filter", function (e) {
        var $table = $(this).data("table"); 
        var $form = $(this).data("form");
        $($form+' input, '+$form+' select').each(function(key, element) {
            //this.value='';
            if(this.value && $(this).hasClass('select2')) {
                //$(this).val( ($(this).data('default')) ? $(this).data('default') : '').trigger('change');
                $(this).select2('destroy').val(($(this).data('default')) ? $(this).data('default') : '').select2({dropdownAutoWidth : true,  width: '100%', allowClear: true, placeholder: '- wybierz -'});
            } else {
                $(this).val(($(this).data('default')) ? $(this).data('default') : '');
            }
        });
        //console.log($table);
        $($table).bootstrapTable('refresh');
    });
    
    $(".widget-search-text").keyup(function () {
        var that = this,
        value = $(this).val();
        var $table = $(this).data("table");
        var $form = $(this).data("form");
       // if (value.length >= 3 ) {
		var o = {};
	   
	    // $($table).bootstrapTable('refresh'/*, {  url: $('#table-data').data("url"), method: 'get',  query: o }*/);
	    if($($table).data('side-pagination') == 'client') {
			$($table).bootstrapTable('refresh');
		} else {
			var options = $($table).bootstrapTable('getOptions');
            var pageNumber = options.pageNumber;
            if(pageNumber == 1)
                $($table).bootstrapTable('refresh');
            else
                $($table).bootstrapTable('selectPage', 1);
        }
    });
    
    $(".widget-search-list").change(function () {
        //$("#table-data").bootstrapTable('refresh');
        var $table = $(this).data("table"); 
        var $form = $(this).data("form");
     
        //$($table).bootstrapTable('refresh', {query: {offset: 0}, pageNumber: 0});
		if($($table).data('side-pagination') == 'client') {
			$($table).bootstrapTable('refresh');
		} else {
			var options = $($table).bootstrapTable('getOptions');
            var pageNumber = options.pageNumber;
            if(pageNumber == 1)
                $($table).bootstrapTable('refresh');
            else
                $($table).bootstrapTable('selectPage', 1);
        }
        //queryParams: function(params) { alert(params['offset']);params['offset'] = 0; return params;}

    });
    
} );

 $(function () {
    
    $(".table-multi-action").on('check.bs.table', function (e, rows, $element) {    
		if($("#group").length) {  
			$("#group").prop('disabled', false);
			$("#group-counter").html( ($("#group-counter").text()*1+1) );
			selections.push(rows['nid']);
        }
    });
    
    $(".table-multi-action").on('uncheck.bs.table', function (e, rows, $element) {
		if($("#group").length) { 
			$mergeCounter = $("#group-counter").text()*1-1;
			$("#group").prop('disabled', !$mergeCounter);
			$("#group-counter").html( $mergeCounter );
			
			var index = selections.indexOf(rows['nid']);
			if(index != -1)
				selections.splice( index, 1 );
			
		}
    });
	
	$(".table-multi-action").on('refresh.bs.table', function (params) {        
        $("#group").prop('disabled', false);
        $("#group-counter").html( '0' );
        selections = [];
    }); 
       
	$(".group-actions > a").click(function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        
        var $target = $($(this).attr('data-target'));  
        var $action = $(this).attr("href");
        $target.find(".modalContent").addClass('preload');
        
        $target.find(".modal-title").html($(this).data("title"));
        
        $target.find(".modalContent")
                  .load($action+"?ids="+selections.join(), 
                        function(response, status, xhr){ 
                            $target.modal('show'); 
                            
                            if ( status == "error" ) {
                                var msg = "Sorry but there was an error: ";
                                $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                            } 
                            //$target.find(".modalContent").removeClass('preload');
                            setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
                   }); 
                
        $target.on('shown.bs.modal', function() {
            //relationship();
            $("select.chosen-select").chosen({disable_search_threshold:10});
            folderBrowser();
			$target.find(".modalContent").removeClass('preload');
        });
        
       return false;   
    });
	
    $("#sorting").click(function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        
        var $action = $(this).attr("href");
        
        var orderId = false; var $period = false;
        if($("#accactionssearch-id_order_fk").length) {
            orderId = $("#accactionssearch-id_order_fk").val();
        }
        if($("#accactionssearch-acc_period").length) {
            period = $("#accactionssearch-acc_period").val();
        }
        
        if(!orderId || !period) {
            swal({
                title: 'Uwaga!',
                text: 'Aby skorzytsać z tej funkcjonalności należy wybrać zlecenie i okres rozliczeniowy',
                type: 'error',
                confirmButtonText: 'Rozumiem'
            });
            return;
        } else {
            var tableId = "#table-actions"; 
            $.ajax({
                method: "POST",
                url: $action,
                dataType : 'json',
                data: {items: $(tableId).bootstrapTable('getData') }
            }).done(function( result ) {
                if(result.success) {
                    //$(tableId+'-info-message').removeClass('alert-danger').removeClass('alert-info').addClass('alert alert-success').text(result.alert);
                    $(tableId).parent().parent().parent().prepend('<div class="file-alert alert alert-success">'+result.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
                } else {
                    //$(tableId+'-info-message').removeClass('alert-success').removeClass('alert-info').addClass('alert alert-danger').text(result.alert);
                    $(tableId).parent().parent().parent().prepend('<div class="file-alert alert alert-danger">'+result.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>'+'</div>');
                }
            });
        }
        
       return false;   
    });

 });

function getIdSelections() {
    return $.map($("#table-matters").bootstrapTable('getSelections'), function (row) {
        return row.nid
    });
}

function responseHandler(res) {
    /*$.each(res.rows, function (i, row) {
        row.state = $.inArray(row.id, selections) !== -1;
    });*/
    
    $("input[name='ids[]']").each( function () {
       //alert( $(this).val() );
       selections.push($(this).val());
    });
    
    $.each(res.rows, function (i, row) {
        row.state = $.inArray(row.nid, selections) !== -1;
    });
    return res;
}

function responseHandlerChecked(res) {
    $.each(res.rows, function (i, row) {
        row.state = $.inArray(row.id, selections) !== -1;
    });
    return res;
}

function stateFormatter(value, row, index) { 
    if (row.confirm*1 == 1) {
        return {
            disabled: true,
            checked: true
        };
    }
	if(row.disabled == 1) {
		return {
            disabled: true,
            checked: false
        };
	}
    return value;
}

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    /*if ( (row.delay*1) > 0 && (row.status != 'Zakończone') ) {
        return {
            classes: 'danger'
        };
    }*/
    if ( row.className )
        return {
            classes: row.className
        };
    if ( row.empStatus == -1 ) {
        return {
            classes: 'danger'
        };
    }
    return {};
}

function sumFormatter(data) {
    field = this.field, sumTotal = 0; 
    if(field == 'unit_time') field = 'unit_time_h';
    var total_sum = data.reduce(function(sum, row) {
        //sumTotal = row['summary'];
        return (sum*1) + (row[field]*1 || 0);
    }, 0);
    total_sum = total_sum * 1;
    return total_sum.toFixed(2);// + '<br/><span class="text--blue">' + sumTotal + '</span>';
}

$(".table-widget").on('expand-row.bs.table', function (e, index, row, $detail) {  
    /*$detail.html('Loading from ajax request...');
    $.get('LICENSE', function (res) {
        $detail.html(res.replace(/\n/g, '<br>'));
    });*/
    
    $detail.html('Ładowanie danych...');
    if(row.typeFormatter == 'send')
        $.get('/correspondence/send/items/'+row.id, function(data, status) {
                $detail.html(data.html);
        });
    if(row.typeFormatter == 'logs') {
        $.post({
            url: '/logs/items/'+row.id,
            data: { date_from: $("#logs-date_from").val(), date_to: $("#logs-date_to").val() },

            success: function(data, status) {
                $detail.html(data.html);
            }
        });
        /*$.post('/logs/items/'+row.id, function(data, status) {
                $detail.html(data.html);
        });*/
    }
	
	if(row.typeFormatter == 'debt_report') {
        $.post({
            url: '/debt/import/items/'+row.id+'/'+row.nip,
            //data: { date_from: $("#logs-date_from").val(), date_to: $("#logs-date_to").val() },

            success: function(data, status) {
                $detail.html(data.html);
            }
        });
        /*$.post('/logs/items/'+row.id, function(data, status) {
                $detail.html(data.html);
        });*/
    }
});

(function () {
    var dropdownMenu;
    $(".table-widget").on('show.bs.dropdown', function (e) {
        dropdownMenu = $(e.target).find('.dropdown-menu');
        $('body').append(dropdownMenu.detach());
        var eOffset = $(e.target).offset();
        dropdownMenu.css({
            'display': 'block',
            'top': eOffset.top + $(e.target).outerHeight(),
            //'left': eOffset.left
        });
    });
    $("#table-actions, #table-matters, #table-customers, #table-orders, #table-events, #table-projects, #table-offers").on('hide.bs.dropdown', function (e) {
        $(e.target).append(dropdownMenu.detach());
        dropdownMenu.hide();
    });
})();

/*function timeFormatter(value, row) {alert(value);
    // var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
    // return '<i class="glyphicon ' + icon + '"></i> ' + value;
    //var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
    return value; 
}*/

function timeFormatter(value) {
    /*if(!value) {
        $(this).empty();
        return; 
    }
    var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
    $(this).html(html); */
     $(this).text(value + '$');
}

$("#table-folder").on('toggle.bs.table', function (e, index, row, $detail) {
    $("#table-folder > tbody").toggleClass('ui-folders');
});

$("#table-inbox").on('click-row.bs.table', function (e, row, $element) {
    e.stopImmediatePropagation();
	e.preventDefault();
    
    $element.attr('class', '');
    
    var $target = $('#modal-grid-item'), $title = 'Wiadomość', $icon = 'envelope-open'; 
    var $action = row.action; 

    $target.find(".modalContent").addClass('preload');

    $target.find(".modal-title").html('<i class="fa fa-'+$icon+' fa-2x"></i><span>'+$title+'</span>');
  
    $target.find(".modalContent")
           .load($action, 
                function(response, status, xhr){ 
                    $target.modal('show'); 
                    
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                    } 
                    //$target.find(".modalContent").removeClass('preload');
                    setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
                });   
       
    $target.on('shown.bs.modal', function() {
        relationship();
        $target.find(".modalContent").removeClass('preload');
        
        if($("#cme-"+row['nid']).length) {
            $("#cme-"+row['nid']).remove();
            $("#msg-counter").text($("#msg-counter").text()*1-1);
        }
        
	});
	
    return false;
});

window.actionEvents = {
	'click .up': function (e, value, row, index) {
        if(index > 0) {
            var source = JSON.stringify($('#table-items').bootstrapTable('getData')[index]);
            var target = JSON.stringify($('#table-items').bootstrapTable('getData')[index - 1]);
            $('#table-items').bootstrapTable('updateRow', {'index':index - 1, 'row': JSON.parse(source)});
            $('#table-items').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(".info-message").addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-items-move").removeClass("none");
        }
	},
	'click .down': function (e, value, row, index) { 
        if(($('#table-items tr').length-2) > (index)) {
            var source = JSON.stringify($('#table-items').bootstrapTable('getData')[index]);
            var target = JSON.stringify($('#table-items').bootstrapTable('getData')[index + 1]);
            $('#table-items').bootstrapTable('updateRow', {'index':index + 1, 'row': JSON.parse(source)});
            $('#table-items').bootstrapTable('updateRow', {'index':index, 'row': JSON.parse(target)});
            
            $(".info-message").addClass("alert alert-info").removeClass("alert-danger").removeClass("alert-success").text('Przed opuszczeniem strony, zapisz zmiany');
            $("#save-table-items-move").removeClass("none");
        }
	},
	'click .update': function(event, value, row, index) {
		event.stopImmediatePropagation();
		event.preventDefault();
	    var tagname = $(this)[0].tagName;   
		if($("#dictionarysearch-id").length > 0)
			var $action = $(this).attr("href")+'/'+$("#dictionarysearch-id").val()+'?index='+index;
		else
			var $action = $(this).attr("href")+'?index='+index;
		$($(this).attr('data-target')).find(".modal-title").html($(this).data("title"));
        $($(this).attr('data-target')).find(".alert").remove();
	    $($(this).attr('data-target')).modal("show")
				  .find(".modalContent")
				  .load($action, function( response, status, xhr ) {
                                      if ( status == "error" ) {
                                        var msg = "Sorry but there was an error: ";
                                        $( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                                      }
                                      $($(this).attr('data-target')).find(".modalContent").removeClass('preload');
                                    }
                    );
                  
        $( "#success" ).load( "/not-here.php", function( response, status, xhr ) {
          if ( status == "error" ) {
            var msg = "Sorry but there was an error: ";
            $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
          }
        });       

       /* $($(this).attr('data-target')).on('hide.bs.modal', function () {
            tinymce.remove();         
        });*/
                  
		$($(this).attr('data-target')).on('shown.bs.modal', function() {		   
		    relationship();
		});
		
	   return false;   
	},
    'click .remove': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
       /* $('#table-items').bootstrapTable('remove', {
			field: 'id',
			values: [row.id]
		});*/
		
		/*$("#save-table-items-move").removeClass("none");*/
        //$('tr[data-index='+index+']').addClass('danger');
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirm').data('id', id).modal('show');
        $('#modalConfirm').find(".modal-title i").addClass('fa-trash').removeClass('fa-lock');
        $("#btnYesConfirm").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesConfirm").attr("data-row", row.id);
		$("#btnYesConfirm").attr("data-table", table);
        $("#btnYesConfirm").html('Usuń');
        if( $(this).attr('data-label') ) {
			$("#btnYesConfirm").html($(this).data('label'));
		}
        
        var o = {};
        //console.log($('#form-data-search').serializeArray());
        $.each($('#form-data-search').serializeArray(), function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        
        $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
            url: $('#table-data').data("url"), method: 'get',
            query: o
        });
	},
    'click .lock': function (e, value, row, index) { 
		e.stopImmediatePropagation();
        e.preventDefault();
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirm').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirm').data('id', id).modal('show');
        $("#btnYesConfirm").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesConfirm").attr("data-row", row.id);
		$("#btnYesConfirm").attr("data-table", table);
        $("#btnYesConfirm").html('Zablokuj');
        
       
	},
    'click .unlock': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
                   
        var id = $(this).data('id'), table = $(this).data('table');
        $('#modalConfirm').find(".modal-title i").removeClass('fa-trash').addClass('fa-lock');
        $('#modalConfirm').data('id', id).modal('show');
        $("#btnYesConfirm").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesConfirm").attr("data-row", row.id);
		$("#btnYesConfirm").attr("data-table", table);
        $("#btnYesConfirm").html('Odblokuj');
	},
    'click .starred': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
                   
        var id = $(this).data('id'), 
        table = $(this).data('table'); 
        
        $('#modalConfirm').find(".modal-title i").removeClass('fa-trash').addClass('far-star');
        $('#modalConfirm').data('id', id).modal('show');
        $("#btnYesConfirm").attr("href", $(this).attr("href")+'?index='+index);
        $("#btnYesConfirm").attr("data-row", row.id);
		$("#btnYesConfirm").attr("data-table", table);
        $("#btnYesConfirm").html('Oznacz');
        $("#btnYesConfirm").html('Usuń');
        if( $(this).attr('data-label') ) {
			$("#btnYesConfirm").html($(this).data('label'));
		}
	},
    'click .inlineEdit': function (e, value, row, index) {
		e.stopImmediatePropagation();
        e.preventDefault();
        
        $target = $($(this).attr('data-target'));
		$($target).find(".modalContent").addClass('preload');
		
	    var tagname = $(this)[0].tagName;   
		$target.find(".modal-title").html($(this).data("title"));
        var $action = $(this).attr("href")+'?index='+index;
  
        $target.find(".modalContent")
              .load($action, 
                    function(response, status, xhr){ 

                        $target.modal('show'); 
                        
                        if ( status == "error" ) {
                            var msg = "Sorry but there was an error: ";
                            $target.find( ".modalContent" ).html( '<div class="modal-body"><div class="alert alert-danger">'+msg + xhr.status + " " + xhr.statusText+'</div></div>' );
                        } 
                        //$target.find(".modalContent").removeClass('preload');
                        setTimeout(function(){ $target.find(".modalContent").removeClass('preload') }, 5000);
               });          
        $target.on('shown.bs.modal', function() {
            relationship();
            $target.find(".modalContent").removeClass('preload');
            
        });
		
	   return false;  
	},
};