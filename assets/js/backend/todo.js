$(document).ready(function(){
    var listnum = 1;
    /*$( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();*/
    $("ul.todo-list").on('click', '.close', function (e) {
        e.preventDefault();
        $(this).parent().fadeOut();
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken},
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(data) {
                $("#todoWidget").html(data.todoWidget);
                return false;
            },
            error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
        return false;
    });
    $.fn.additem = function(){
        var addtext = $.trim($(".add-text").val());
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, CalTodo: { name: addtext}},
            dataType: 'json',
            url: $(".add-todo-short").data('action'),
           /* beforeSend: function() { 
                $("#validation-form").hide().empty(); 
            },*/
            success: function(data) {
               
                if(data.success == false) {
                    
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                   
                    $(".task-comments-public").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                } else {
                    $(".add-text").val('');
                    //$("#validation-form").removeClass('none').addClass('alert-danger').removeClass('alert-success').html(data.alert);
                    //$("#validation-form").show();
                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    $(".todo-tasks").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    
                   $("ul.todo-list").prepend(data.listItem);
                   $("#todoWidget").html(data.todoWidget);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
        
        /*var listitem = $("<li><div><input type='checkbox' id='checkbox" + listnum + "' value='None' name='check'><label class='checkbox' for='checkbox" + listnum + "''></div><p>" + addtext + "</p><a class='close'><i class='fa fa-close'></i></a></li>");
        if(addtext.length>0) {
            $("ul.todo-list").prepend(listitem);
            document.getElementById("add-text").value = "";
            listnum++;
        }*/
    }
    $(".add-todo-short").click(function(){
        var addtext = $.trim($(".add-text").val());
        if(!addtext || addtext == '') {
            var txtError = 'Proszę podać nazwę zadania.'+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
            var unix = Math.round(+new Date()/1000);
           
            $(".todo-tasks").prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
            
            setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
        } else {
            $.fn.additem();
        }
    });
    $(".add-text").keypress(function( event ) {
        if ( event.which == 13 ) {
            $.fn.additem();
        }
    });
    $("ul.todo-list").on('click', '.checkbox', function () {
        var id = $(this).data('value');      
        
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken},
            dataType: 'json',
            url: $(this).data('action'),
            success: function(data) {
               $(".todo-"+id).toggleClass("completed");
               $("#todoWidget").html(data.todoWidget);
            },
            error: function(xhr, textStatus, thrownError) {
                //console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
        
        /*$(this).parents("li").toggleClass("completed");
        if ( $(this).parents("li").hasClass("completed") ) {
            $(this).parents("li").appendTo("ul.todo-list");
        } else {
            $(this).parents("li").prependTo("ul.todo-list");
        }*/
    });
});
