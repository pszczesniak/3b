$(document).on("click", ".modalConfirm", function (e) {
    e.preventDefault();

    var id = $(this).data('id'); var table = $(this).data('table');
	if($(this).hasClass('done')) {
		$target = $("#modal-grid-item");
		$target.find(".modal-title").html('<i class="fa fa-info-circle text--red"></i>Alert');
		//$target.find(".modalContent").addClass('preload');
		/*$("#modal-grid-item").modal("show")
			.find(".modalContent")
			.load('/task/event/showajax/'+calEvent.id);*/
		$target.modal("show").find(".modalContent").html('<div class="modal-body"><div class="alert alert-danger">Zadanie zostało już zamknięte</div></div>');   
	} else {
		$('#modalConfirm').data('id', id).data('table', table).modal('show');
		$("#btnYesConfirm").attr("href", $(this).attr("href"));
		if( $(this).attr('data-label') ) {
			$("#btnYesConfirm").html($(this).data('label'));
		}
    }
});

$(document).on("click", ".deleteConfirmWithComment", function (e) {
    e.preventDefault();
   
    var id = $(this).data('id'); var table = $(this).data('table');
    $('#modalConfirmDeleteWithComment').data('id', id).data('table', table).modal('show');
    $("#confirm-comment").val('')
    $("#btnYesConfirmWithComment").attr("href", $(this).attr("href"));
    if( $(this).attr('data-label') ) {
        $("#btnYesConfirmWithComment").html($(this).data('label'));
    }
});

$(document).on("click", "#btnYesConfirm", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    
  	var id = $('#modalConfirm').data('id');
    var $row = $(this).data('row')*1;

  	$('#modalConfirm').modal('hide');
    
    $.ajax({
        type: 'post',
        cache: false,
        data: {_csrf : csrfToken},
        dataType: 'json',
        url: $(this).attr('href'),
        beforeSend: function() { 
        },
        success: function(data) {
            $idRow = data.id; 
			if(data.table) 
				var $table = $(data.table);
			else
				var $table = $("#table-data");
      
			if(data.success == false) {
				var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                var unix = Math.round(+new Date()/1000);
                
                if($table.length > 0 ) {
                    $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                
                    setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                }
                
                //KAPI.resources.afterSaveFalse(txtError, unix, data);
                //KAPI.tasks.afterSaveFalse(txtError, unix, data);
                
            } else { 
                if(data.refresh == 'inline') {
                    if(data.action == 'removeUser') {
                        $("tr#row-"+data.id).remove();
                        var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        var unix = Math.round(+new Date()/1000);
                        $("#table-members").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else if(data.action == 'removeUserWithGroup') {
                        $("tr#row-"+data.id).addClass('bg-yellow2');
                        $("tr#row-"+data.id+" > td.delRow").html('');
                        var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        var unix = Math.round(+new Date()/1000);
                        $("#table-members").parent().prepend('<div id="alert-'+unix+'" class="alert alert-warning alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else if(data.action == 'removeGroup') {
                        $("tr#grow-"+data.id).remove();
                        $.each(data.users, function(index, value)  {
                            if (value.length != 0)  {
                                $("tr#row-"+value).remove();
                            }
                        });
                        
                        var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                        var unix = Math.round(+new Date()/1000);
                        $("#table-members").parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    } else {
                        $table.bootstrapTable('updateRow', {index: data.index, row: data.row });
                    }
                    //$table.bootstrapTable('updateRow', {index: data.index, row: { star:data.star   }});
                    //$('#table-inbox').bootstrapTable('updateRow', {index: data.index, row: { star:data.star   }});
                } else {
                    $table.bootstrapTable('refresh');
                }
                
                if(data.calendar) {
                    if($(data.calendar).length) {
                        $(data.calendar).fullCalendar( 'refetchEvents' );
                        $('#modal-grid-item').modal('hide');
                    }
                }
                
                if(data.structure) { 
                    $('li.dd-item[data-id='+data.id+']').remove();
                }
            }

        },
        error: function(xhr, textStatus, thrownError) {
            alert('Something went to wrong.Please Try again later...');
        }
    });
    
});

$(document).on("click", "#btnYesConfirmWithComment", function (e) {

    e.stopImmediatePropagation();
    e.preventDefault();
    
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    
  	var id = $('#modalConfirmDeleteWithComment').data('id');
    var $row = $(this).data('row')*1;
    var $comment = $("#confirm-comment").val();
    
    if($comment) {
        $.ajax({
            type: 'post',
            cache: false,
            data: {_csrf : csrfToken, comment: $comment},
            dataType: 'json',
            url: $(this).attr('href'),
           
            success: function(data) {
                $idRow = data.id; 
                if(data.table) 
                    var $table = $(data.table);
                else
                    var $table = $("#table-data");
                    
                if($table.length == 0)
                    $table = $("#table-events");
                if(data.success == false) {
                    var txtError = ((data.alert) ? data.alert : 'Dane nie zostały usunięte.')+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    var unix = Math.round(+new Date()/1000);
                    $(".field-confirm-comment > .help-block").text(data.alert);
                   /* if($table.length > 0 ) {
                        $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-danger alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    }*/
                    
                   /* KAPI.resources.afterSaveFalse(txtError, unix, data);
                    KAPI.tasks.afterSaveFalse(txtError, unix, data);*/
                    
                } else { 
                    $('#modalConfirmDeleteWithComment').modal('hide');
                    //$table.bootstrapTable('refresh');
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $table.bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o
                    });
                    var unix = Math.round(+new Date()/1000);
                    var txtError = data.alert+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';
                    
                    if( $table.length > 0 ) {
                        $table.parent().parent().parent().prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                        setTimeout(function(){ $("#alert-"+unix).remove(); }, 5000);
                    }
                    if(data.buttons) { 
                        //$('tr[data-index='+data.index+']').find('.edit-btn-group').html(data.buttons);
                        $('#table-data').bootstrapTable('updateRow', {index: data.index, row: { actions:data.buttons   }});
                    }
                 
                    if ( $("#db-event-"+data.id).length > 0 && data.action == 'delete') {
                        $("#db-event-"+data.id).remove();
                        $(".calendar-block").prepend('<div id="alert-'+unix+'" class="alert alert-success alert-dismissible"  style="margin-bottom:5px;">'+ txtError +'</div>');
                    }
                    
                    ///KAPI.tasks.afterSaveTrue(txtError, unix, data);
                    ///KAPI.resources.afterSaveTrue(txtError, unix, data);
                    ///KAPI.chat.afterSaveTrue(txtError, unix, data);
                    
                    if ( $("#modal-grid-event").length > 0 ) {
                        $("#modal-grid-event").modal("hide");
                    }
                    
                    if ( $("#modal-grid-item").length > 0 ) {
                        $("#modal-grid-item").modal("hide");
                    }
                }
                
                
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    } else {
        $("#confirm-comment").addClass('bg-red2');
    }
    
});