
'use strict';

let TNS = {
    utils: {},
    shoppingBag: {},
    shop: {},
    slider: {},
    product: {},
    geo: {},
    form: {}
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

TNS.utils = {

    _screenSize () {
        let w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth|| e.clientWidth|| g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight,
            sizes = {x: x, y: y};
        return sizes;
    },

    openSidebar () {
        if ( document.querySelector('.js-open-nav') ) {
            let burgerButton = document.querySelector('.js-open-nav'),
                overlay = document.querySelector('.overlay'),
                sidebar = document.querySelector('.js-sidebar');

            burgerButton.addEventListener("click", (event) => {
                event.preventDefault();
                overlay.classList.add('overlay--show');
                sidebar.classList.add('l-sidebar--open');
                return false;
            });
        }
    },

    closeSidebar () {
        if ( document.querySelector('.js-close-nav') ) {
            let burgerCloseButton = document.querySelector('.js-close-nav');

            burgerCloseButton.addEventListener("click", (event) => {
                event.preventDefault();
                TNS.utils.__closeSidebar();
                return false;
            });
        }
    },

    // closePopup () {
    //     if ( document.querySelector('.js-close-popup') ) {
    //         let popupCloseButton = document.querySelector('.js-close-popup');

    //         popupCloseButton.addEventListener("click", (event) => {
    //             event.preventDefault();
    //             document.querySelector('.js-popup').classList.remove('popup-wrapper--show');
    //             return false;
    //         });
    //     }
    // },

    closePopup (obj, sessionKey) {
        let popupWrapper = obj.parentElement.parentElement;

        popupWrapper.classList.remove('popup-wrapper--show');
        popupWrapper.classList.add('popup-wrapper--was-shown');

        if ( sessionKey ===  'popupPromoCode') {
            sessionKey = sessionKey + '-' + document.querySelector('.js-popup-promocode').getAttribute('data-id');
        }

        if (sessionKey) {
            sessionStorage.setItem(sessionKey, 1);
        }

        // open popup with promocode if exists and wasnt shown already
        if ( document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {
            // if ( TNS.cookie.showCookie('promoCodeConfirmed') !== document.querySelector('.js-popup-promocode').getAttribute('data-id') ) {
            //     if ( !sessionStorage.getItem('popupPromoCode') ) {
            //         setTimeout(function(){
            //             document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
            //         }, 2000);
            //     }
            // }

            if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                    setTimeout(function(){
                        document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
                    }, 2000);
                }
            }
        }

        return false;

        // if ( document.querySelector('.js-close-popup') ) {
        //     let popupCloseButton = document.querySelector('.js-close-popup');

        //     popupCloseButton.addEventListener("click", (event) => {
        //         event.preventDefault();
        //         document.querySelector('.js-popup').classList.remove('popup-wrapper--show');
        //         return false;
        //     });
        // }
    },

    closeSidebarByHitEsc () {
        document.addEventListener('keyup', function( event ) {
            if (event.keyCode === 27) {
                TNS.utils.__closeSidebar();
            }
        });
    },

    closeSidebarByClickOnOverlay () {
        if ( document.querySelector('.overlay') ) {
            let overlay = document.querySelector('.overlay');

            overlay.addEventListener("click", (event) => {
                event.preventDefault();
                TNS.utils.__closeSidebar();
                return false;
            });
        }
    },

    __closeSidebar () {
        let overlay = document.querySelector('.overlay'),
            sidebar = document.querySelector('.js-sidebar');
        overlay.classList.remove('overlay--show');
        sidebar.classList.remove('l-sidebar--open');
    },

    showMore () {
        if ( document.querySelector('.js-show-more') ) {
            var showMoreButtons = document.querySelectorAll('.js-show-more');

            [].forEach.call(showMoreButtons, function(showMoreButton) {
                showMoreButton.addEventListener("click", function( event ) {
                    event.preventDefault();
                    let targetId = event.currentTarget.getAttribute('data-target-id');
                    document.querySelector('#' + targetId).style.display = 'block';
                    this.parentNode.removeChild(this);
                }, false);
            });
        }
    },

    showDeliveryOptions () {
        if ( document.querySelector('.js-delivery-options') ) {
            var deliveryOptionsButtons = document.querySelectorAll('.js-delivery-options');

            [].forEach.call(deliveryOptionsButtons, function(deliveryOptionsButton) {
                deliveryOptionsButton.addEventListener("click", function( event ) {
                    event.preventDefault();
                    let popup;
                    if (this.nextElementSibling.classList.contains('js-delivery-popup')) {
                        popup = this.nextElementSibling;

                        popup.classList.contains('product-delivery__popup--open') ? popup.classList.remove('product-delivery__popup--open') : popup.classList.add('product-delivery__popup--open');

                    }

                }, false);
            });
        }
    },

    closeDeliveryOptions () {
        if ( document.querySelector('.js-delivery-options-close') ) {
            var deliveryOptionsCloseButtons = document.querySelectorAll('.js-delivery-options-close');

            [].forEach.call(deliveryOptionsCloseButtons, function(deliveryOptionsCloseButton) {
                deliveryOptionsCloseButton.addEventListener("click", function( event ) {
                    event.preventDefault();
                    this.parentElement.classList.remove('product-delivery__popup--open');
                }, false);
            });
        }
    },

    showAsidePage () {
        if ( document.querySelector('.js-aside-page') ) {
            var showAsidePageButtons = document.querySelectorAll('.js-aside-page');

            [].forEach.call(showAsidePageButtons, function(showAsidePageButton) {
                showAsidePageButton.addEventListener("click", function( event ) {
                    event.preventDefault();

                    document.body.classList.contains('move-left') ? document.body.classList.remove('move-left') : document.body.classList.add('move-left');

                }, false);
            });
        }
    },

    showAsidePageMobile () {
        if ( document.querySelector('.js-aside-page-mobile') ) {
            var showAsidePageButtons = document.querySelectorAll('.js-aside-page-mobile');

            [].forEach.call(showAsidePageButtons, function(showAsidePageButton) {
                showAsidePageButton.addEventListener("click", function( event ) {
                    event.preventDefault();
                    let asidePage = document.querySelector('.aside-page');
                    asidePage.classList.contains('aside-page--show') ? asidePage.classList.remove('aside-page--show') : asidePage.classList.add('aside-page--show');

                }, false);
            });
        }
    },

    giftWrapPopup () {
        if ( document.querySelector('.js-tooltip') ) {
            var giftWrapPopupButtons = document.querySelectorAll('.js-tooltip');

            [].forEach.call(giftWrapPopupButtons, function(giftWrapPopupButton) {
                giftWrapPopupButton.addEventListener("click", function( event ) {
                    event.preventDefault();

                    let popup = this.parentElement.parentElement.querySelector('.shopping-bag__gift-wrap-popup'),
                        popupShowClassName = 'shopping-bag__gift-wrap-popup--show';

                    popup.classList.contains(popupShowClassName) ? popup.classList.remove(popupShowClassName) : popup.classList.add(popupShowClassName);

                }, false);
            });
        }
    },

    sharePopup () {
        if ( document.querySelector('.js-share-popup') ) {
            var sharePopupButtons = document.querySelectorAll('.js-share-popup');

            [].forEach.call(sharePopupButtons, function(sharePopupButton) {
                sharePopupButton.addEventListener("click", function( event ) {
                    event.preventDefault();

                    let popup = sharePopupButton.parentElement,
                        popupShowClassName = 'share--open';

                    popup.classList.remove('share--off');

                    popup.classList.contains(popupShowClassName) ? popup.classList.remove(popupShowClassName) : popup.classList.add(popupShowClassName);

                }, false);
            });
        }
    },

    magnific () {

      $('a.js-popup, a.popup, a.js-video').on('click', function() {
          var href = $(this).attr('href'),
              type = href.substring(href.length-4, href.length),
              title = $(this).attr('title');

          if(type === '.jpg' || type === '.gif' || type === '.png' || type === '.jpeg')
          {
            type = 'image';
          }
          else {
            type = 'iframe';
          }

          $.magnificPopup.open({
            items: {
              src: href
            },
            type: type,

            image: {
              markup: '<div class="mfp-figure">'+
                        '<div class="mfp-img"></div>'+
                        '<div class="mfp-bottom-bar">'+
                          '<div class="mfp-title"></div>'+
                          '<div class="mfp-counter"></div>'+
                          '<p class="mfp-close"></p>'+
                        '</div>'+
                        '<p class="mfp-close"></p>'+
                      '</div>',

              cursor: 'mfp-zoom-out-cur',
              titleSrc: title,

              verticalFit: true, // Fits image in area vertically

              tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
            },

            iframe: {
              patterns: {
                youtube: {
                  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                  id: 'v=', // String that splits URL in a two parts, second part should be %id%
                  src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0' // URL that will be set as a source for iframe.
                }
              }
            },
            //closeMarkup: '<a class="mfp-close btn btn--gold">CLOSE</a>',

            mainClass: 'mfp-fade',

            titleSrc: title,
            gallery: {
              enabled: true,
              navigateByImgClick: true
            }
          });

          return false;
      });
    },

    ajaxPopup () {
        $('.js-ajax-popup').on('click', function (e) {
            var self = this;

            $.magnificPopup.open({
                type: 'ajax',
                items: {
                    src: self.getAttribute('data-href')
                }
            }, 0);

            return false;
        });
    },

    scrollToID (id, context = 'html,body') {
        var path = window.location.pathname;
        var offSet = 80;
        var x = TNS.utils._screenSize().x;

        if (x < 768) {
            offSet = 60;
        }
        var targetOffset = $(id).offset().top - offSet;

        $(context).animate({scrollTop:targetOffset}, 1000);
    },

    hashAnchorClick () {
        $('a[href*="#"]:not(.js-scroll-to)').on('click', function (e) {

            var target = this.hash,
                hashValue = target.substr(target.indexOf("#"));

            if (hashValue.length) {
                TNS.utils.scrollToID( hashValue );
            }

            return false;
        });
    },

    fixedContentAnchorClick () {
        $('a.js-scroll-to[href*="#"]').on('click', function (e) {

            var target = this.hash,
                hashValue = target.substr(target.indexOf("#"));

            if (hashValue.length) {
                TNS.utils.scrollToID( hashValue, '.js-scroll-to-wrapper' );
            }

            return false;
        });
    },

    openPopupById: function(settings) {
        if (!settings.id) {
            return;
        }

        let shippingPopup = document.querySelector('#' + settings.id);

        if (shippingPopup) {
            shippingPopup.classList.add('popup-wrapper--show');
        }
    },

    openPopupOnClick () {
        if ( document.querySelector('.js-open-popup') ) {
            var popupButtons = document.querySelectorAll('.js-open-popup');

            [].forEach.call(popupButtons, function(popupButton) {
                popupButton.addEventListener("click", function( event ) {
                    event.preventDefault();

                    if (event.currentTarget.href) {
                        window.open(
                            event.currentTarget.href,
                            '_blank',
                            'toolbar=no, scrollbars=yes, resizable=yes, width=500, height=400'
                        );
                    }

                }, false);
            });
        }
    },

    infiniteScroll () {
        var infinite;

        if ($('#js-infinity-wrapper').length) {

            infinite = new Waypoint.Infinite({
                element: $('#js-infinity-wrapper')[0],
                context: document.querySelector('.l-main')
            });
        }
    },

    cardNumberValidation () {
        if ( document.querySelector('#card-number') ) {
            let numberCardInput = document.querySelector('#card-number');

            numberCardInput.addEventListener("input", (event) => {
                event.preventDefault();

                var target = event.target, position = target.selectionEnd, length = target.value.length;

                target.value = target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);

                target.value = target.value.substr(0, 19);
            });
        }
    },

    homepageAnim () {
        if ( document.querySelector('.l-header__product-img') ) {
            let products = document.querySelectorAll('.l-header__product-img'),
                timeoutAnimIn = 500,
                timeoutShow = 1750;

            [].forEach.call( products, function(product) {
                setTimeout(function(){
                    product.classList.add('anim-in');
                    product.parentElement.querySelector('.l-header__product-img-shadow').classList.add('anim-in');
                }, timeoutAnimIn);

                timeoutAnimIn = timeoutAnimIn + 300;

                setTimeout(function(){
                    product.classList.add('show');
                    product.classList.remove('anim-in');

                    product.parentElement.querySelector('.l-header__product-img-shadow').classList.add('show');
                    product.parentElement.querySelector('.l-header__product-img-shadow').classList.remove('anim-in');
                }, timeoutShow);

                timeoutShow = timeoutShow + 300;
            });
        }
    },

    hoverTimstampStart: 0,

    counter: 0,
    myInterval: null,
    interval: 30,

    hoverAnimation() {

        $(".js-homepage-anim").hover(function(e){
            let self = this;
            $(self).addClass('animate');

            TNS.utils.counter = 0;
            TNS.utils.myInterval = setInterval(function () {
                ++TNS.utils.counter;
            }, TNS.utils.interval);
        },function(e){
            let self = this,
                animDuration = self.getAttribute('data-anim-duration')*1,
                different = (parseFloat(TNS.utils.counter * TNS.utils.interval)/1000) % parseFloat((animDuration/1000) );

            clearInterval(TNS.utils.myInterval);
            TNS.utils.counter = 0;

            setTimeout(function() {
                $(self).removeClass('animate');
            }, (animDuration - (different*1000) ) );
            
        });
    },

    init () {
        this.openSidebar();
        // this.closePopup();
        this.closeSidebar();
        this.closeSidebarByHitEsc();
        this.closeSidebarByClickOnOverlay();
        this.showMore();
        this.showDeliveryOptions();
        this.closeDeliveryOptions();
        this.showAsidePage();
        this.showAsidePageMobile();
        this.giftWrapPopup();
        this.magnific();
        this.ajaxPopup();
        this.sharePopup();
        this.hashAnchorClick();
        this.infiniteScroll();
        this.openPopupOnClick();
        this.fixedContentAnchorClick();

        this.cardNumberValidation();
        this.homepageAnim();
        this.hoverAnimation();
    }
};

TNS.trackByFacebookPixel = function(event, details = null) {
    if (window['fbq'] !== undefined && event !== '') {
        fbq('track', event, details);
    }
};

TNS.equalHeights = function() {
    $('.js-equal-height').matchHeight();
};

TNS.shoppingBag = {
    clickEvent () {
        if ( document.querySelector('.js-shopping-bag-button') ) {
            let shoppingBagButton = document.querySelector('.js-shopping-bag-button'),
                shoppingBag = document.querySelector('.js-shopping-bag');

            shoppingBagButton.addEventListener("click", (event) => {
                event.preventDefault();

                TNS.shoppingBag.toggleShoppingBag()
                return false;
            });
        }
    },

    closeShoppingBag () {
        if ( document.querySelector('.js-close-shopping-bag') ) {
            let shoppingBagButton = document.querySelector('.js-close-shopping-bag'),
                shoppingBag = document.querySelector('.js-shopping-bag');

            shoppingBagButton.addEventListener("click", (event) => {
                event.preventDefault();
                shoppingBag.classList.remove('shopping-bag--show');
                return false;
            });
        }
    },

    toggleShoppingBag () {
        if ( document.querySelector('.js-shopping-bag') ) {
            let shoppingBag = document.querySelector('.js-shopping-bag');

            if (shoppingBag.classList.contains('shopping-bag--show')) {
                shoppingBag.classList.remove('shopping-bag--show');
            } else {
                shoppingBag.classList.add('shopping-bag--show');
            }
        }
    },

    __closeOnClickNotOnShoppingBag (event) {
        var current = event.target,
            parent = current.parentNode,
            isShoppingBagChild = false,
            x = TNS.utils._screenSize().x;

        while ( parent.tagName !== 'HTML') {
            if(current.classList) {
                if (current.classList.contains('shopping-bag')) {
                    isShoppingBagChild = true;
                    break;
                } else {
                    current = current.parentNode;
                    parent = parent.parentNode;
                }
            }
        }

        isShoppingBagChild ? false : document.querySelector('.js-shopping-bag').classList.remove('shopping-bag--show');
    },

    closeShoppingBagByClick () {
        if (document.querySelector('.js-shopping-bag')) {
            document.addEventListener('click', TNS.shoppingBag.__closeOnClickNotOnShoppingBag, false);
        }
    },

    init () {
        this.clickEvent();
        this.closeShoppingBag();
        this.closeShoppingBagByClick();
    }
};

TNS.shop = {
    bagSummaryElement: $('#js-bag-summary'),
    bagSummaryLoadingElement: $('.bag-summary .loading'),
    bagPopupElement: $('.shopping-bag-popup'),
    bagPopupDynamicElement: $('#js-bag-popup'),
    bagPopupLoadingElement: $('.shopping-bag-popup .loading'),

    bagCheckoutForm: $('.js-form-shopping-bag'),
    bagCheckoutFormLoading: $('.js-form-shopping-bag .loading'),

    addToBag (productId, form) {
        if (!productId) {
            return false;
        }

        var quantity = form ? form.querySelector("input[name='quantity']").value : $('#js-quantity').val();
        $('.l-main .loading').fadeIn();

        return callApi({
            url: '/api/shop/addToBag',
            method: 'POST',
            data: 'product_id=' + productId + '&quantity=' + quantity,
            onSuccess: TNS.shop.addToBagCallback
        });
    },

    addToBagCallback (response) {
        document.querySelector('.l-navbar__basket-items').innerHTML = response.counter + ' item' + (response.counter === 1 ? '' : 's' );
        $('.l-main .loading').fadeOut();
        TNS.shop.openBagPopup();

        TNS.trackByFacebookPixel('AddToCart', response.details ? response.details : null);
    },

    removeProduct (productId, popup) {
        if (!productId) {
            return false;
        }

        return callApi({
            isPopup: popup,
            url: '/api/shop/removeProduct',
            method: 'DELETE',
            data: 'product_id=' + productId,
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    changeQuantity (productId, increment, popup) {
        increment = increment || false;

        if (!productId) {
            return false;
        }

        return callApi({
            isPopup: popup,
            url: '/api/shop/changeQuantity',
            method: 'POST',
            data: 'product_id=' + productId + '&increment=' + increment,
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    changeManualQuantity (productId, startValue, newValue, popup) {
        var increment = (newValue*1 > startValue*1);

        if ( (newValue*1) % 1 !== 0 ) {
            return false;
        }

        if (!productId) {
            return false;
        }

        return callApi({
            isPopup: popup,
            url: '/api/shop/changeQuantity',
            method: 'POST',
            data: 'product_id=' + productId + '&increment=' + increment + '&amount=' + Math.abs( (startValue*1) - (newValue*1) ),
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    applyDiscountCheckout (promoCode, step) {
        if (!promoCode) {
            return false;
        }

        step = step * 1;

        return callApi({
            isPopup: false,
            url: '/api/shop/applyDiscount',
            method: 'POST',
            data: 'promo_code=' + promoCode + '&checkout=true&step=' + step,
            onSuccess: step === 3 ? reload : checkoutFormSuccessCallback,
            errorCallback: promoCodeFailed
        });
    },

    removeDiscountCheckout (step) {
        step = step * 1;

        return callApi({
            isPopup: false,
            url: '/api/shop/removeDiscount?checkout=true&step=' + step,
            method: 'DELETE',
            onSuccess: step === 3 ? reload : checkoutFormSuccessCallback,
        });
    },

    applyDiscount (promoCode, popup) {
        if (!promoCode) {
            return false;
        }

        return callApi({
            isPopup: popup,
            url: '/api/shop/applyDiscount',
            method: 'POST',
            data: 'promo_code=' + promoCode,
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback,
            errorCallback: promoCodeFailed
        });
    },

    removeDiscount (popup) {
        return callApi({
            isPopup: popup,
            url: '/api/shop/removeDiscount',
            method: 'DELETE',
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    changeDeliveryOption (deliveryId, popup) {
        if (!deliveryId) {
            return false;
        }

        return callApi({
            isPopup: popup,
            url: '/api/shop/changeDeliveryOption',
            method: 'POST',
            data: 'delivery_id=' + deliveryId,
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    restoreAutomaticDiscountCheckout (step) {
        step = step * 1;

        return callApi({
            isPopup: false,
            url: '/api/shop/restoreDiscount?checkout=true&step=' + step,
            method: 'POST',
            onSuccess: step === 3 ? reload : checkoutFormSuccessCallback,
        });
    },

    restoreAutomaticDiscount (popup) {

        return callApi({
            isPopup: popup,
            url: '/api/shop/restoreDiscount',
            method: 'POST',
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    openBagPopup () {
        return callApi({
            url: '/api/shop/bag?popup=1',
            onSuccess: fetchPopupBagContent
        });
    },

    checkCard (button, amount, currency) {
        function created3DSecure(status, result) {
            if (status !== 200) {
                var message = result.error.message;
                alert("Unexpected 3D Secure response status: " + status + ". Error: " + message);
                $('#js-checkout-step-2 .loading').fadeOut();
                return;
            }

            var msg;
            var tdsToken = result.id;
            var redirectURL = result.redirect_url;
            var redirectStatus = result.status;

            if (redirectStatus === 'succeeded' || redirectStatus !== 'redirect_pending') {
                $('#js-checkout-step-2').submit();
                return;
            }

            // we're able to continue with 3D Secure.
            // insert the iframe, and register our callback
            var iframeId = 'stripe-iframe-container';
            var container = document.getElementById(iframeId);
            Stripe.threeDSecure.createIframe(redirectURL, container, function(response) {
                // hide the modal dialog again
                $.magnificPopup.close();
                $('#js-checkout-step-2 .loading').fadeOut();

                if (response.status === 'succeeded') {
                    document.getElementById('source').value = tdsToken;
                    $('#js-checkout-step-2').submit();
                } else {
                    msg = '3D Secure authentication failed: ' + response.error_message;
                    alert(msg);
                    button.disabled = true;
                }
            });

            // open the modal dialog
            $.magnificPopup.open({
                items: {
                    src: '#' + iframeId,
                    type: 'inline'
                }
            });
        }

        //
        // show loading layer over the form
        // rest need to be in callback cause in case when alert will show it stops executing fadein loading layer
        //

        $('#js-checkout-step-2 .loading').fadeIn(410, function() {

            var cardParams = {
                number: document.getElementById('card-number').value,
                exp_month: document.getElementById('expiry_date_month').value,
                exp_year: document.getElementById('expiry_date_year').value,
                cvc: document.getElementById('cvc').value
            };

            Stripe.token.create({
                'card': cardParams
            }, function(status, result) {

                if (status !== 200) {
                    var message = result.error.message;
                    alert(message);
                    $('#js-checkout-step-2 .loading').fadeOut();

                    button.disabled = false;
                    return;
                }

                if (result.card && result.card.brand) {
                    document.getElementById('card_brand').value = result.card.brand;
                }

                Stripe.threeDSecure.create({
                    card: result.id,
                    amount: amount,
                    currency: currency
                }, created3DSecure);
            });
        });

    },

    changeBillingAddressOption (isChecked) {
        var billingAddressLayer = $('.js-billing-address');

        if (isChecked) {
            billingAddressLayer.fadeOut();

            $(billingAddressLayer).each(function() {
                $(this).find(':input:not(.btn)').removeAttr('required');
            });

        } else {
            billingAddressLayer.fadeIn();

            $(billingAddressLayer).each(function() {
                $(this).find(':input:not(.btn)').attr('required');
            });
        }
    },

    changeGiftWrapSetting (isChecked, productId, popup) {
        return callApi({
            isPopup: popup,
            url: '/api/shop/giftWrapping',
            method: 'POST',
            data: 'is_wrapped=' + isChecked + '&product_id=' + productId,
            onSuccess: popup ? bagPopupSuccessCallback : bagSummarySuccessCallback
        });
    },

    submit ( formId ) {
        var form = $('#' + formId);
        form.find('.loading').fadeIn();
        document.getElementById(formId).submit();
    }
};

//
//
// Common Shop Functions
//
//

function fetchPopupBagContent(response) {
    TNS.shop.bagPopupDynamicElement.html(response);
    TNS.shoppingBag.toggleShoppingBag();
    hideBagSummaryLoading();

    return false;
}

function callApi(options) {
    if (!options.url) {
        return false;
    }

    var ajaxDetails = {
        url: options.url + (options.isPopup ? '?popup=1' : ''),
        method: options.method || 'GET'
    };

    if (options.data) {
        ajaxDetails.data = options.data;
    }

    showBagSummaryLoading();
    $.ajax(ajaxDetails)
        .then(function(response) {
            if (options.onSuccess) {
                options.onSuccess(response);
            }
        }, function(response) {
            if (options.errorCallback) {
                options.errorCallback(response);
            }
            hideBagSummaryLoading();
        });
    return false;
}

function bagSummarySuccessCallback(response) {
    TNS.shop.bagSummaryElement.html(response);
    hideBagSummaryLoading();
}

function bagPopupSuccessCallback(response) {
    TNS.shop.bagPopupDynamicElement.html(response);
    hideBagSummaryLoading();
}

function checkoutFormSuccessCallback(response) {
    TNS.shop.bagCheckoutForm.html(response);
    hideBagSummaryLoading();
}

function showBagSummaryLoading() {
    TNS.shop.bagPopupLoadingElement.fadeIn();
    TNS.shop.bagSummaryLoadingElement.fadeIn();
    TNS.shop.bagCheckoutFormLoading.fadeIn();
}

function hideBagSummaryLoading() {
    TNS.shop.bagPopupLoadingElement.fadeOut();
    TNS.shop.bagSummaryLoadingElement.fadeOut();
    TNS.shop.bagCheckoutFormLoading.fadeOut();
}

function promoCodeFailed(response) {
    if (response.responseJSON.error === true) {
        if ( $('.promo-code__error').length === 0 ) {
            $('.promo-code').addClass('promo-code--failed');
            $('.promo-code').append( '<span class="promo-code__error">'+response.responseJSON.message+'</span>' );
        } else {
             $('.promo-code__error').html(response.responseJSON.message);
        }
    }
}

function reload() {
    window.location.reload(true);
}

function accordion() {
    var accordionBtn = document.querySelectorAll('.js-accordion-btn'); 
    var accordionContent = document.querySelectorAll('.js-accordion-content');
    var accordionIcon = document.querySelectorAll('.js-accordion-content');
    
    [].forEach.call( accordionBtn, function(item, i, accordionBtn){
        item.addEventListener('click', function(e){
            if (!(this.classList.contains('active'))) {
                [].forEach.call( accordionBtn, function(item) {
                // accordionBtn.forEach(function(item){
                    item.classList.remove('active');
                    item.classList.remove('accordion__btn--active');
                });

                [].forEach.call( accordionContent, function(item) {
                // accordionContent.forEach(function(item){
                    item.classList.remove('accordion__content--show');
                });
    
                this.classList.add('active');
                accordionContent[i].classList.add('accordion__content--show');
                accordionBtn[i].classList.add('accordion__btn--active');
            }
        });
    });
}

accordion();


//
// End of Common functions
//


TNS.slider = {
    bgSlider () {
        if ( document.querySelector('.js-bg-slider') ) {
            let bgSliders = document.querySelectorAll('.js-bg-slider'),
                imageCollection = [];

            [].forEach.call(bgSliders, function(bgSlider) {
                let slidesCount = bgSlider.getAttribute('data-bg-slider-count')*1,
                    i,
                    interval = Math.round(100/slidesCount);
                for (i = 1; i <= slidesCount; i++) {
                    imageCollection.push( bgSlider.getAttribute('data-bg-slide-url-' + i) );
                }

                var imgCount = 1;
                var intervalCounter = window.setInterval(function() {

                    bgSlider.style.backgroundImage = "url('" + imageCollection[imgCount] + "')";
                    imgCount = ( (slidesCount - 1) > imgCount) ? imgCount + 1 : 0;
                }, 5000);
            });
        }
    },

    init () {
        this.bgSlider();
    }
};

TNS.product = {
    increase (inputId, productPrice = 'product-price') {
        let targetInput = document.querySelector('#' + inputId);

        targetInput.value = (targetInput.value * 1) + 1;
        TNS.product.calculatePrice(targetInput.value, productPrice);
    },

    decrease (inputId, productPrice = 'product-price') {
        let targetInput = document.querySelector('#' + inputId),
            targetInputValue = targetInput.value*1;

        if ( targetInputValue > 1) {
            targetInput.value = (targetInput.value * 1) - 1;
            TNS.product.calculatePrice(targetInput.value, productPrice);
        }
    },

    calculatePrice (quantity, productPrice = 'product-price') {
        let priceEl = document.querySelector('#' + productPrice),
            basePrice = priceEl.getAttribute('data-base-price')*1,
            calculatedPrice = 0.00;

        calculatedPrice = quantity * basePrice;
        priceEl.innerHTML = calculatedPrice.toFixed(2);
    },

    priceInputOnChangeEvent () {
        if (document.querySelector('#js-quantity')) {
            let priceEl = document.querySelector('#js-quantity');

            priceEl.addEventListener("input", (event) => {
                if(event.target.value < 0) {
                    event.target.value = 1;
                }
                TNS.product.calculatePrice(event.target.value);
            }, false);
        }
    },

    init () {
        this.priceInputOnChangeEvent();
    }
}

TNS.geo = {
    placeSearch: '',
    autocomplete: '',
    componentForm: {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        postal_town: 'long_name',
        postal_code: 'short_name',
        administrative_area_level_1: 'long_name'
    },

    loadScript: function(map) {
        var protocol = location.protocol,
            urlApi = '';

        if (protocol === 'http:') {
            urlApi = 'maps.googleapis.com';
        } else {
            urlApi = 'maps-api-ssl.google.com';
        }

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.defer = true;
        script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBX_bYm4TkKnnV0BomV_NPrduEQP8H9gzg&language=en&libraries=places&callback=TNS.geo.initAutocomplete';

        document.body.appendChild(script);
    },

    initAutocomplete () {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        TNS.geo.autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {
                types: ['geocode'],
                componentRestrictions: {country: "uk"}
            }
        );

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        TNS.geo.autocomplete.addListener('place_changed', TNS.geo.fillInAddress);

        google.maps.event.addDomListener(document.getElementById('autocomplete'), 'keydown', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
            }
        });
    },

    fillInAddress () {
        // Get the place details from the autocomplete object.
        var place = TNS.geo.autocomplete.getPlace();

        if ( document.querySelector('.js-show-more') ) {
            document.querySelector('#more-form').style.display = 'block';
            document.querySelector('.js-show-more').parentNode.removeChild(document.querySelector('.js-show-more'));
        }

        for (var component in TNS.geo.componentForm) {
            if (TNS.geo.componentForm.hasOwnProperty(component)) {
                if (document.getElementById(component)) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }
            }
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (TNS.geo.componentForm[addressType]) {
                var val = place.address_components[i][TNS.geo.componentForm[addressType]];

                if ( document.getElementById(addressType) ) {
                    document.getElementById(addressType).value = val;
                }

                if (addressType === 'postal_town') {
                    document.getElementById('locality').value = val;
                }

                /*
                if (addressType === 'administrative_area_level_1') {
                    document.getElementById('country').value = val;
                }*/
            }
        }
    },

    geolocate () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
                });
                TNS.geo.autocomplete.setBounds(circle.getBounds());
            });
        }

    }
};

TNS.cookie = {
    setCookie: function(name, val, days) {
        var expires;
        if (days) {
            var data = new Date();
            data.setTime(data.getTime() + (days * 24*60*60*1000));
            expires = "; expires="+data.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + val + expires + "; path=/";
    },

    deleteCookie: function(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    },

    showCookie: function(name) {
        if (document.cookie!=='') {
            var cookies=document.cookie.split("; ");
            for (var i=0; i<cookies.length; i++) {
                var cookieName=cookies[i].split("=")[0];
                var cookieVal=cookies[i].split("=")[1];
                if (cookieName===name) {
                    return decodeURI(cookieVal);
                }
            }
        }
    }
};

TNS.openSocialLoginPopup = function(options) {
    if (!options.authUrl) {
        return false;
    }

    const signinWindow = window.open(options.authUrl, "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);
};

TNS.purchasingOutsideUk = {
    continue: function() {
        TNS.cookie.setCookie('outsideUKConfirmed', 'noPurchaseConfirmed', 9999);
        document.querySelector('.js-popup-purchase-outside-uk').classList.remove('popup-wrapper--show');

        // open popup with promocode if exists and wasnt shown already
        if ( document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {
            if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                    setTimeout(function(){
                        document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
                    }, 2000);
                }
            }
        }

    },

    enable: function() {
        $('.js-popup-purchase-outside-uk .loading').fadeIn();
        $.ajax({
            url: '/ajax/enablePurchasingOutsideUk',
            method: 'POST',
            dataType: 'json'
        })
        .then(function() {
            TNS.purchasingOutsideUk.continue();
            location.reload();
        }, function() {
            TNS.purchasingOutsideUk.continue();
        });
    }
};

TNS.purchasingOutsideUs = {
    continue: function() {
        TNS.cookie.setCookie('outsideUSConfirmed', 'noPurchaseConfirmed', 9999);
        document.querySelector('.js-popup-purchase-outside-us').classList.remove('popup-wrapper--show');

        // open popup with promocode if exists and wasnt shown already
        if ( document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {
            // if ( TNS.cookie.showCookie('promoCodeConfirmed') !== document.querySelector('.js-popup-promocode').getAttribute('data-id') ) {
            //     if ( !sessionStorage.getItem('popupPromoCode') ) {
            //         setTimeout(function(){
            //             document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
            //         }, 2000);
            //     }
            // }

            if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                    setTimeout(function(){
                        document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
                    }, 2000);
                }
            }
        }

    },

    enable: function() {
        $('.js-popup-purchase-outside-us .loading').fadeIn();
        $.ajax({
            url: '/ajax/enablePurchasingOutsideUs',
            method: 'POST',
            dataType: 'json'
        })
        .then(function() {
            TNS.purchasingOutsideUs.continue();
            location.reload();
        }, function() {
            TNS.purchasingOutsideUs.continue();
        });
    }
};


TNS.purchasingOutside = {
    continue: function(countrySuffix) {
        let uppercaseSuffix = countrySuffix.toUpperCase();

        TNS.cookie.setCookie(`outside${uppercaseSuffix}Confirmed`, 'noPurchaseConfirmed', 9999);

        document.querySelector('.js-popup-purchase-outside').classList.remove('popup-wrapper--show');

        // open popup with promocode if exists and wasnt shown already
        if ( document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {
            if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
                    setTimeout(function(){
                        document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
                    }, 2000);
                }
            }
        }

    },

    enable: function(countrySuffix) {
        $('.js-popup-purchase-outside .loading').fadeIn();
        $.ajax({
            url: `/ajax/enablePurchasingOutside${countrySuffix}`,
            method: 'POST',
            dataType: 'json'
        })
        .then(function() {
            TNS.purchasingOutside.continue(countrySuffix);
            location.reload();
        }, function() {
            TNS.purchasingOutside.continue(countrySuffix);
        });
    }
};

TNS.wineDirect = {
    loadBundle: function() {
        $('.js-wine-direct-load-bundle').on('change', function() {
            let productCode = $(this).val();

            if (!productCode) {
                return false;
            }

            var loadingLayer = $('.wine-direct__add-to-cart .loading');
            loadingLayer.fadeIn();
            $('#js-winedirect-add-to-card [v65remotejs]').remove();
            $('#js-winedirect-add-to-card').prepend('<div v65remotejs="addToCartForm" productSKU="' + productCode + '"></div>');
            vin65remote.product.addToCartForm(website, function() {
                loadingLayer.fadeOut();
            });

            return false;
        });
    },
    init: function() {
        this.loadBundle();
    }
};

TNS.promocode = {
    continue: function(PromoCodeId, url) {
        // TNS.cookie.setCookie('promoCodeConfirmed', PromoCodeId, 9999);
        TNS.cookie.setCookie('promoCodeConfirmed-' + PromoCodeId, PromoCodeId, 9999);
        document.querySelector('.js-popup-promocode').classList.remove('popup-wrapper--show');
        TNS.trackByFacebookPixel('Lead');
        window.location = url;
    }
};

TNS.utils.svgFit = function svgFit() {
    if ( document.querySelector('.js-svgAnimation') ) {
        var svg = document.querySelector('.js-svgAnimation'),
            originH = svg.getAttribute('data-nominal-height')*1,
            originW = svg.getAttribute('data-nominal-width')*1,
            actualWidth =  TNS.utils._screenSize().x,
            actualHeight =  TNS.utils._screenSize().y,
            screenRatio,
            svgRatio,
            newSvgHeight,
            newSvgWidth;

        screenRatio = actualWidth / actualHeight;
        svgRatio = originW / originH;

        newSvgHeight = svgRatio * actualHeight;
        newSvgWidth = svgRatio * actualWidth;


        if (screenRatio > svgRatio) {
            newSvgWidth = actualWidth;
            newSvgHeight = (originH * newSvgWidth) / originW;
        } else {
            newSvgHeight = actualHeight;
            newSvgWidth = (newSvgHeight * originW) / originH;
        }

        svg.setAttribute('width', newSvgWidth );
        svg.setAttribute('height', newSvgHeight );
    }

}

TNS.form = {
    contact (form) {
        let data = $(form).serialize(),
            loading = true,
            url;

        url = '/ajax/contact';

        $('#js-contact-form .loading').fadeIn();
        $(form).find('button[type="submit"]').text('Sending...');

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            dataType: 'json'
        })
        .then(function(response) {
            $(form).find('button[type="submit"]').text('Submit');
            $('#js-contact-form .loading').fadeOut();

            if (response.url) {
                window.location.href = response.url;
            } else {
                $('#js-contact-form .loading').fadeOut();

                if ( $(form).find('.form__response--ok').length === 0 ) {
                    $(form).prepend( '<div class="form__response form__response--ok"><p>'+response.message+'</p></div>' );
                } else {
                    $('.form__response--ok p').html(response.message);
                }
            }

            // remove error response if exists
            $(form).find('.form__response--error').remove();
            $(form)[0].reset();

        }, function(response) {
            let errorMessage = '';

            if (response.status == 422) {
                for (var field in response.responseJSON) {
                    if (response.responseJSON.hasOwnProperty(field)) {
                        errorMessage += response.responseJSON[ field ].join('<br>') + '<br>';
                    }
                }
            } else if (response.responseJSON.message) {
                errorMessage = response.responseJSON.message;
            }

            if (errorMessage) {
                if ( $(form).find('.form__response--error').length === 0 ) {
                    $(form).prepend( '<div class="form__response form__response--error"><p>'+errorMessage+'</p></div>' );
                } else {
                    $('.form__response--error p').html(errorMessage);
                }
            }

            // remove success response if exists
            $(form).find('.form__response--ok').remove();

            $(form).find('button[type="submit"]').text('Submit');
            $('#js-contact-form .loading').fadeOut();
        });

        return false;
    },

    newsletter (form) {
        let data = $(form).serialize(),
            loading = true,
            url;

        url = '/ajax/newsletter';

        $('#js-newsletter-form .loading').fadeIn();
        $(form).find('button[type="submit"]').text('Sending...');

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            dataType: 'json'
        })
        .then(function(response) {
            $(form).find('button[type="submit"]').text('Submit');
            $('.loading').fadeOut();
            TNS.trackByFacebookPixel('Lead');

            if (response.url) {
                window.location.href = response.url;
            } else {
                $('#js-newsletter-form .loading').fadeOut();
                $(form).prepend( '<div class="form__response form__response--ok"><p>'+response.message+'</p></div>' );
            }

            // remove error response if exists
            $(form).find('.form__response--error').remove();

            $(form)[0].reset();

        }, function(response) {
            let errorMessage = '';

            if (response.status == 422) {
                for (var field in response.responseJSON) {
                    if (response.responseJSON.hasOwnProperty(field)) {
                        errorMessage += response.responseJSON[ field ].join('<br>') + '<br>';
                    }
                }
            } else if (response.responseJSON.message) {
                errorMessage = response.responseJSON.message;
            }

            if (errorMessage) {
                if ( $(form).find('.form__response--error').length === 0 ) {
                    $(form).prepend( '<div class="form__response form__response--error"><p>'+errorMessage+'</p></div>' );
                } else {
                    $('.form__response--error p').html(errorMessage);
                }
            }

            // remove success response if exists
            $(form).find('.form__response--ok').remove();

            $(form).find('button[type="submit"]').text('Submit');
            $('#js-newsletter-form .loading').fadeOut();
        });

        return false;
    },

    newsletterPromo (form) {
        let data = $(form).serialize(),
            loading = true,
            url;

        url = '/ajax/newsletterpromo';

        $('#js-newsletter-promo-form .loading').fadeIn();
        $(form).find('button[type="submit"]').text('Sending...');

        // remove error response if exists
        $(form).find('.form-response').remove();

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            dataType: 'json'
        })
        .then(function(response) {
            TNS.trackByFacebookPixel('Lead');
            
            $(form).find('button[type="submit"] span').text('Submit');
            $('#js-newsletter-promo-form .loading').fadeOut();
            $('#js-newsletter-promo-form-row').fadeOut();

            if (response.url) {
                window.location.href = response.url;
            } else {
                $('#js-newsletter-promo-form .loading').fadeOut();
                $(form).append( '<div class="form-response"><p class="font-18 color-blue">'+response.message+'</p></div>' );
            }

            // $(form)[0].reset();

        }, function(response) {
            let errorMessage = '';

            if (response.status == 422) {
                for (var field in response.responseJSON) {
                    if (response.responseJSON.hasOwnProperty(field)) {
                        errorMessage += response.responseJSON[ field ].join('<br>') + '<br>';
                    }
                }
            } else if (response.responseJSON.message) {
                errorMessage = response.responseJSON.message;
            }

            if (errorMessage) {
                $(form).append( '<div class="form-response"><p class="font-18 color-paleRed">'+response.message+'</p></div>' );
            }

            $(form).find('button[type="submit"] span').text('Sign Up');
            $('#js-newsletter-promo-form .loading').fadeOut();
        });

        return false;
    }
};

TNS.calculate100vh = function() {
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
};

TNS.whiteNavbarForFullscreenHeader = function() {
    const fullscreenHeader = document.querySelector('.l-header--fullscreen-image');
    const navbar = document.querySelector('.l-navbar');

    if (!fullscreenHeader) {
        return;
    }

    navbar.classList.add('l-navbar--white-text');
};

TNS.whiteNavbarForFullscreenHeader();
TNS.utils.init();
TNS.equalHeights();
TNS.shoppingBag.init();
TNS.slider.init();
TNS.product.init();
TNS.utils.svgFit();
TNS.wineDirect.init();
TNS.calculate100vh();


if ( document.getElementById('autocomplete') ) {
    TNS.geo.loadScript();
}

if (isMobile.any()) {
    document.body.classList.add('mobile');
}

// if (isMobile.iOS()) {
//     if( document.querySelector('.l-header--banner') && (TNS.utils._screenSize().y > 500) ) {
//         window.scrollTo(0,document.body.scrollHeight - TNS.utils._screenSize().y - 40);
//     }
// }

function iOSVersion() {
  if(window.MSStream){
    // There is some iOS in Windows Phone...
    // https://msdn.microsoft.com/en-us/library/hh869301(v=vs.85).aspx
    return false;
  }
  var match = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/),
      version;

  if (match !== undefined && match !== null) {
    version = [
      parseInt(match[1], 10),
      parseInt(match[2], 10),
      parseInt(match[3] || 0, 10)
    ];
    return parseFloat(version.join('.'));
  }

  return false;
}

if ( iOSVersion() && iOSVersion()<10) {
    document.body.classList.add('mobile--ios-less-10');
}

// // if user outside the UK - open the popup
// if ( document.querySelector('.js-popup-purchase-outside-uk') && !TNS.cookie.showCookie('outsideUKConfirmed') ) {
//     if ( !sessionStorage.getItem('popupPurchaseOutsideUk') ) {
//         setTimeout(function(){
//             document.querySelector('.js-popup-purchase-outside-uk').classList.add('popup-wrapper--show');
//         }, 2000);
//     }
// }


// if user outside the country list - open the popup
let countrySuffixBody = document.querySelector('body').getAttribute('data-country-suffix');
let uppercaseCountrySuffix = String(countrySuffixBody).toUpperCase();

if ( document.querySelector('.js-popup-purchase-outside') && !TNS.cookie.showCookie(`outside${uppercaseCountrySuffix}Confirmed`) ) {
    if ( !sessionStorage.getItem('popupPurchaseOutside') ) {
        setTimeout(function(){
            document.querySelector('.js-popup-purchase-outside').classList.add('popup-wrapper--show');
        }, 2000);
    }
}

// if ( !document.querySelector('.js-popup-purchase-outside-uk') && document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {

//     // if ( TNS.cookie.showCookie('promoCodeConfirmed') !== document.querySelector('.js-popup-promocode').getAttribute('data-id') ) {
//     //     if ( !sessionStorage.getItem('popupPromoCode') ) {
//     //         setTimeout(function(){
//     //             document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
//     //         }, 2000);
//     //     }
//     // }

//     if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
//         if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
//             setTimeout(function(){
//                 document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
//             }, 2000);
//         }
//     }
// }



if ( !document.querySelector('.js-popup-purchase-outside') && document.querySelector('.js-popup-promocode:not(.popup-wrapper--was-shown)') ) {

    if ( !TNS.cookie.showCookie('promoCodeConfirmed-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
        if ( !sessionStorage.getItem('popupPromoCode-' + document.querySelector('.js-popup-promocode').getAttribute('data-id')) ) {
            setTimeout(function(){
                document.querySelector('.js-popup-promocode').classList.add('popup-wrapper--show');
            }, 2000);
        }
    }
}

window.addEventListener('resize', function() {
    setTimeout(function(){
        TNS.utils.svgFit();
    }, 80);
}, false);

$('body').bind('touchstart', function() {});