import React from 'react';

/* const Home = React.createClass({
  render: () =>
    <div className="jumbotron">
      <h1>Hello, world!</h1>
    </div>,
});
*/

class Home extends React.Component {
  constructor() {
    super();
    this._handleClick = this._handleClick.bind(this);
  }
  
  _handleClick() {
    console.log(this);
  }

  render() {
    return <div onClick={this._handleClick}>Hello, world.</div>;
  }
}

module.exports = Home;

