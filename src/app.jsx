import React from 'react';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { browserHistory, Router } from 'react-router';
import routes from './routes.js';

// remove tap delay, essential for MaterialUI to work properly
injectTapEventPlugin();

ReactDom.render((
  <MuiThemeProvider muiTheme={getMuiTheme()}>
    <Router history={browserHistory} routes={routes} />
  </MuiThemeProvider>), document.getElementById('root'));


/*import React from 'react';
import ReactDOM from 'react-dom';
import Home from './hello';
import HomePage from './components/HomePage.jsx';

// const ReactDOM = require('react-dom');

ReactDOM.render(
  // <h1>Hello, world!</h1>,
  <HomePage />,
  document.getElementById('root'),
);*/
