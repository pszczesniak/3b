<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Imię i Nazwisko',
            'subject' => 'Temat',
            'verifyCode' => 'Kod weryfikacyjny',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        $body = 'Email: '.$this->email
                .'<br />Imię i Nazwisko: '.$this->name
                .'<br />Treść: '.$this->body;
        try {
            /*$message = new \yii\swiftmailer\Message();

            $message->setReturnPath($this->email);
            $message->setReplyTo([$this->email => $this->name]);
            $message->setFrom([$this->email => $this->name]);
            $message->setTo('kamila_bajdowska@onet.eu');
            $message->setSubject($this->subject);
            $message->setHtmlBody($body);

            $result = Yii::$app->mailer->send($message);*/
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setBcc('kamila_bajdowska@onet.eu')
                ->setFrom([$this->email => $this->name])
                ->setReplyTo([$this->email => $this->name])
                ->setReturnPath($this->email)
                ->setSubject('Formularz kontaktowy: '.$this->subject)
                ->setHtmlBody($body)
                ->send();
            return true;
        } catch (\Swift_TransportException $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
    }
}
