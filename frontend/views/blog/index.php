<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use frontend\widgets\blog\Search;
/* @var $this yii\web\View */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/web-container.php',array('model' => false, 'class' => 'page', 'title'=>Html::encode($this->title), 'back' => false)) ?>
    <div class="row">
        <?php if(!empty($_GET['tag'])): ?>
            <h4 class="search-results">Wpisy oznaczone tagiem <i>#<?= Html::encode($_GET['tag']); ?></i></h4>
        
        <?php endif; ?>

        <?php if(!empty($_GET['keyword'])): ?>
            <h4>[<i><?= Html::encode($_GET['keyword']); ?></i>]</h4>
        <?php endif; ?>

        <?php
        foreach($posts as $post)  {
            echo $this->render('_blog_item', [
                'data' => $post,
            ]);
        }
        ?>
        <?php if( count($posts) == 0) echo '<div class="alert">brak wyników</div>'; ?>

        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
<?php $this->endContent(); ?>
