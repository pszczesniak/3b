<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
?>
<!-- https://codepen.io/prasannaFE/pen/MKZXpg
    https://codepen.io/decorator/pen/qZJKyB -->
<?php $avatar = (is_file("uploads/avatars/thumb/avatar-".$comment->created_by.".png"))?"/uploads/avatars/thumb/avatar-".$comment->created_by.".png":"/images/avatar.png"; ?>

    <span class="comment-author-avatar">
        <img alt="" src="<?= $avatar ?>" class="avatar" height="40" width="40">
    </span>
    <div class="comment-info clearfix">
        <ul class="comment-meta clearfix">
            <li class="comment-meta-author"><?= $comment->authorLink ?></li>
            <li class="comment-meta-date"> <?= Yii::$app->formatter->asDatetime($comment->created_at) ?></li>
        </ul>
        <div class="clear"></div>
    </div>
    <div class="comment-content">
        <?= nl2br(Html::encode($comment->content)) ?>
    </div>



