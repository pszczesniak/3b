<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $description['firstname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['lastname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['pesel'] = 'To Twój unikatowy, jedenastocyfrowy numer identyfikacyjny. Wpisz go bez spacji.';
    $description['evidence_number'] = 'To unikatowy, dziewięciocyfrowy numer Twojego dowodu osobistego. Wprowadź go bez spacji.';
    $description['marital'] = 'Wybierz jedną opcję z rozwijanej listy, która opisuje Twój stan cywilny.';
    $description['members'] = 'To osoby, z którymi mieszkasz i które są od Ciebie finansowo zależne (np. dzieci).';  
    $description['car'] = 'Wypełnij aby zwiększyć swoje szanse na pozytywną decyzję kredytową';   
    $description['car_type'] = 'Wpisz markę Swojego samochodu'; 
    $description['car_year'] = 'Wpisz rok produkcji Swojego samochodu'; 
    $description['car_insurance'] = 'Wpisz datę w której kończy się ubezpieczenie Twojego samochodu'; 
    $description['employment_status'] = 'Wybierz rodzaj zatrudnienia z rozwijanej listy. Pamiętaj, że nie udzielamy pożyczek osobom bezrobotnym ani pozbawionym źródeł regularnego dochodu.';
    $description['employment_period'] = 'Wybierz datę rozpoczęcia pracy.';
    $description['employer_name'] = 'Nazwy Twojego pracodawcy potrzebujemy do przeprowadzenia procesu udzielenia pożyczki.';
    $description['email'] = 'Podaj adres e-mail, do którego masz stały i łatwy dostęp. To będzie Twoja nazwa użytkownika, której nie będziesz mógł już zmienić.';
    $description['email_repeat'] = 'Wpisz ponownie swój adres email. Upewnij się, że jest taki sam, jak ten, który podałeś wcześniej.';
    $description['password'] = 'Wprowadź hasło, które stworzyłeś podczas rejestracji. Powinno ono zawierać co najmniej 8 znaków, w tym wielkie i małe litery, minimum jedną cyfrę oraz nie posiadać znaków specjalnych, np. !%$?. Pamiętaj, że tę samą literę możesz użyć maksymalnie 3 razy z rzędu.';
    
    $templateInput = '<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="bottom" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';
?>

<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/client/basic'])]); ?>
    <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <fieldset><legend>Dane osobowe</legend>
		<?= $form->field($model, 'firstname', [/*'template' => sprintf($templateInput, 'envelope', $description['email'])*/])
                     //->hint($description['email'])
                     ->input('firstname', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Imię']) ?>
                     
        <?= $form->field($model, 'lastname', [/*'template' => sprintf($templateInput, 'envelope', $description['email'])*/])
                 //->hint($description['email'])
                 ->input('lastname', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Nazwisko']) ?>
                 
        <?= $form->field($model, 'email', [/*'template' => sprintf($templateInput, 'envelope', $description['email'])*/])
                 //->hint($description['email'])
                 ->input('email', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'E-mail']) ?>
        
        <?= $form->field($model, 'phone', [/*'template' => sprintf($templateInput, 'envelope', $description['email'])*/])
                 //->hint($description['email'])
                 ->input('phone', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Telefon']) ?>
	</fieldset>
	
    <div class="push text-center">
         <?= Html::submitButton('Zapisz zmiany', ['id' => 'send-email', 'class' => 'btn btn--fill btn--fill-gold', 'name' => 'contact-button']) ?>
    </div>
   
<?php ActiveForm::end(); ?>
<fieldset><legend>Zdjęcie profilowe</legend>
    <div id="upload-demo"></div>
    <div class="imageAttachment" id="upload-demo-id" data-id="<?= (is_file("uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? $model->id_user_fk : 0 ?>">
        <div class="btn-toolbar actions-bar">
            <span class="btn btn-success btn-file">
                <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                <span data-replace-text="Zamień..." data-upload-text="Prześlij" class="file_label">Pobierz zdjęcie</span>
                <input type="file" id="upload" name="Files[avatar]" value="Choose a file" data-path="<?= Url::to(['/client/upload','id' => $model->id_user_fk]) ?>">
            </span>
            <button class="btn btn-success upload-result" data-path="<?= Url::to(['/client/saveavatar','id' => $model->id_user_fk]) ?>">Zapisz avatar</button>
            <!--<button class="btn btn-success change-view">Ustaw miniaturę</button>-->
        </div>  
    </div>
</fieldset>
<script type="text/javascript">
    
    
</script>
