<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\visit\models\Calvisit */
/* @var $form yii\widgets\ActiveForm */
?>

    
<div class="grid grid--0">
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label class="control-label" for="visit_fromDate">Start</label>
            <input type='text' class="form-control form__input" id="visit_fromDate" name="Visit[fromDate]" value="<?= $model->fromDate ?>"/> 
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label class="control-label" for="visit-toDate">Koniec</label>
            <input type='text' class="form-control form__input" id="visit_toDate" name="Visit[toDate]" value="<?= $model->toDate ?>"/>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.datetimepicker.setLocale('pl');
    $('#visit_fromDate').datetimepicker({format: "Y-m-d H:i"});
    $('#visit_toDate').datetimepicker({format: 'Y-m-d H:i'});
   
</script>