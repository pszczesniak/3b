<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-container", 'data-table' => "#table-visits"],
]); ?>
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Wpisz tytuł...', 'maxlength' => true, 'class' => 'form-control form__input']) ?>
    <?= $form->field($model, 'message')->textarea(['rows' => 3, 'placeholder' => 'Wpisz wiadomość...', 'class' => 'form-control form__textarea'])->label(false) ?>
 
    <div>
        <?= Html::submitButton('Wyślij', ['class' => 'btn btn--fill btn--fill-mauve btn--lighttext']) ?>
        <button class="btn btn-sm btn-default modalCloseBtn" type="button">Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

</script>