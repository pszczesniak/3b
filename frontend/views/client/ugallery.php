<?php
/* @var $this yii\web\View */
use frontend\widgets\cms\MainWidgets;
use frontend\widgets\Calculator;
use frontend\widgets\cms\GalleryWidget;

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->additional_info;
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = 'Twoje galerie';

$this->params['breadcrumbs'][] = $this->title;

$img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/ugallery/cover/".$model->id."/medium.jpg")) ? "/uploads/ugallery/cover/".$model->id."/medium.jpg" :  "/uploads/company/logo.png";

?>
<?php $this->beginContent('@app/views/layouts/client-container.php', array('header' => false, 'model' => $model, 'class' => 'client-account', 'title' => Html::encode($this->title), 'back' => true, 'logout' => true)) ?>	
    <div style="text-align: right"><a href="<?= Url::to(['/client/download', 'id' => $model->id]) ?>" class="btn btn--fill btn--fill-gold">Pobierz wszystkie zdjęcia</a></div>
    <?= GalleryWidget::widget(['id' => $model->id_gallery_fk, 'edit' => true]);  ?>
<?php $this->endContent(); ?>



