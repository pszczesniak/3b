<?php
/* @var $this yii\web\View */
use frontend\widgets\cms\MainWidgets;
use frontend\widgets\Calculator;
use frontend\widgets\cms\GalleryWidget;

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->additional_info;
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = ['label' => 'Galerie klientów', 'url' => Url::to(['/site/galleries'])];

$this->params['breadcrumbs'][] = $this->title;

$img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/ugallery/cover/".$model->id."/medium.jpg")) ? "/uploads/ugallery/cover/".$model->id."/medium.jpg" :  "/uploads/company/logo.png";

?>
<?php $this->beginContent('@app/views/layouts/page-container.php',array('class'=>'page', 'title'=>Html::encode($this->title))) ?>
    <h4>Galeria została usunięta</h4>
          
<?php $this->endContent(); ?>



