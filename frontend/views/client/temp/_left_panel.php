<?php
	
    use yii\helpers\Url;
    use frontend\widgets\svc\CategoryBlock;
    use frontend\widgets\newsletter\NewsletterWidget;
?>

<aside>
    <?php if($model->id > 0) { ?>
    <div class="special">
        <div class="ribbon">
           <h4>Ustawienia</h4>
        </div>
        <div class="user-menu">
            <ul >
                <li <?= (Yii::$app->controller->action->id=='update')?'class="active"':'' ?> ><a href="<?= Url::to(['/client/update', 'id' => $model->id]) ?>"><i class="fa fa-user"></i>Twój profil</a></li>
                <li <?= (Yii::$app->controller->action->id=='observed')?'class="active"':'' ?> ><a href="<?= Url::to(['/client/observed', 'id' => $model->id]) ?>"><i class="fa fa-star"></i>Obserwowane</a></li>
                <li <?= (Yii::$app->controller->action->id=='changepassword')?'class="active"':'' ?> ><a href="<?= Url::to(['/client/changepassword']) ?>"><i class="fa fa-lock"></i>Hasło</a></li>
                <li <?= (Yii::$app->controller->action->id=='socialmedia')?'class="active"':'' ?> ><a href="<?= Url::to(['/client/socialmedia', 'id' => $model->id]) ?>"><i class="fa fa-globe"></i>Media społecznościowe</a></li>
                <li <?= (Yii::$app->controller->action->id=='newsletter')?'class="active"':'' ?> ><a href="<?= Url::to(['/client/newsletter', 'id' => $model->id]) ?>"><i class="fa fa-envelope"></i>Biuletyn</a></li>
            </ul>
        </div>
    </div>
    <div class="special">
        <div class="ribbon">
           <h4>Poczta</h4>
        </div>
        <div class="user-menu">
            <?php $amountMsg = $model->amountmsg; $amountCmt = $model->amountcmt; $amountCmtToAccept = $model->amountcmttoaccept; ?>
            <ul>
                <li <?= (Yii::$app->controller->action->id=='inbox' && isset($_GET['type']) && $_GET['type'] == "all" )?'class="active"':'' ?> ><span class="badge"><?= ($amountMsg + $amountCmt + $amountCmtToAccept) ?></span><a href="<?= Url::to(['/client/inbox', 'id' => $model->id, 'type' => 'all']) ?>">Wszystko</a></li>
                <li <?= (Yii::$app->controller->action->id=='inbox' && isset($_GET['type']) && $_GET['type'] == "cmt" )?'class="active"':'' ?> ><span class="badge"><?= $amountCmt ?></span><a href="<?= Url::to(['/client/inbox', 'id' => $model->id, 'type' => 'cmt']) ?>">Opinie</a></li>
                <li <?= (Yii::$app->controller->action->id=='inbox' && isset($_GET['type']) && $_GET['type'] == "msg" )?'class="active"':'' ?> ><span class="badge"><?= $amountMsg ?></span><a href="<?= Url::to(['/client/inbox', 'id' => $model->id, 'type' => 'msg']) ?>">Pytania i odpowiedzi</a></li>

            </ul>
        </div>
    </div>
    <?php } ?>
    <div class="special">
        <div class="ribbon">
           <h4>Reklama</h4>
        </div>
        <div class="special-offert">
            <!--<div id="banP21">
                <div class="banLabel">REKLAMA</div>
                <div id="b616054" class="banB21">
                    <a href="http://ad.adview.pl/ad/reloadwww?atex=616054&amp;url=http%3A%2F%2Fwyborcza.biz%2Fbiznes%2F1%2C147767%2C19614323%2Cpomoz-swoim-rodzicom-nowa-akcja-spoleczna-fundacji-agory.html%3Futm_source%3Dwyborcza.pl%26utm_medium%3DAutopromoINT%26utm_campaign%3Da_Mad_Marcin_fundacja_agory_1proc_dziadek_120216%26utm_content%3D300x90&amp;dx=35276&amp;jsp=30&amp;ts=1457432701147" target="_blank">
                        <img height="90" width="300" class="banBIM21" alt="" src="http://adv.adview.pl/ads/308/OMVPFRXAQJOMYVEWSLYESLJRROWSNC_616054.gif">
                    </a>
                </div>
            </div>-->
        </div>
    </div>
	<!--<div class="special">
		<div class="ribbon">
		   <h4>Biuletyn</h4>
		</div>
		<div class="special-offert">
			<?= NewsletterWidget::widget(['id' => 1]) ?>
		</div>
	</div>-->
</aside>
