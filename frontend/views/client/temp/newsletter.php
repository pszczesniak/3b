<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

$this->title = 'Biuletyn';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'client-newsletter', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-envelope"></i>Preferencje dotyczące biuletynów</h2> 
                <?= Alert::widget() ?>
                <form id="newsletter-groups" method="POST" >
                    <?php
                        foreach($groups as $key => $group) {
                            if(in_array($group->id, $subscribes)) $checked = 'checked="checked"'; else $checked = '';
                            echo '<div class="row">'
                                    .'<input type="checkbox" value="'.$group->id.'" name="Newsletter[groups][]" id="newsletter-'.$group->id.'" class="form__checkbox" '.$checked.'>'
                                    .'<label for="newsletter-'.$group->id.'" class="form__checkbox-label">'.$group->name.'</label>'
                                    .'<p class="desc">'.$group->describe.'</p>'
                                .'</div>';
                        }
                    ?>
                    <div class="form-group align-right">
                        <button class="btn btn-success" type="submit">Subskrybuj</button>               
                    </div>
                </form>
                
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>
