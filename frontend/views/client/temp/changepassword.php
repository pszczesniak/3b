<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = Yii::t('lsdd', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $modeluser, 'class'=>'user-password-change', 'title'=>Html::encode($this->title))) ?>	
   <?= Alert::widget() ?>
   <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $modeluser]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-lock"></i>Zmiana hasła</h2> 
                <div class="col-md-12 col-lg-8">
                    <?php $form = ActiveForm::begin([
                            'id'=>'changepassword-form',
                            'options'=>['class'=>'form-horizontal'],
                            /*'fieldConfig'=>[
                                'template'=>"{label}\n<div class=\"col-lg-3\">
                                            {input}</div>\n<div class=\"col-lg-5\">
                                            {error}</div>",
                                'labelOptions'=>['class'=>'col-lg-2 control-label'],
                            ],*/
                        ]); ?>
                        <?= $form->field($model,'oldpass',['inputOptions'=>[
                            'placeholder' => Yii::t('lsdd', 'Old Password')
                        ]])->passwordInput() ?>
                       
                        <?= $form->field($model,'newpass',['inputOptions'=>[
                            'placeholder' => Yii::t('lsdd', 'New Password')
                        ]])->passwordInput() ?>
                       
                        <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
                            'placeholder' => Yii::t('lsdd', 'Repeat New Password')
                        ]])->passwordInput() ?>
                       
                        <div class="form-group align-right">
                            <?= Html::submitButton(Yii::t('lsdd', 'Change password'),[
                                'class'=>'btn more_btn2'
                            ]) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
           </div>
            
        </div>
    </div>

<?php $this->endContent(); ?>