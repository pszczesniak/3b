<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
	<?php if(!$model->isNewRecord) { ?>
    <div class="form__row">
		<div class="form__row__left">
			<label for="country" class="form__label">Logo firmy / avatar :</label>
		</div>
		<div class="form__row__right">
            <label class="text-center">Wrzuć swoje logo/zdjęcie, pojawi się wówczas kreator który pomoże Ci dopasować avatar do strony. Załadowane zdjęcie będzie jednocześnie głównym zdjęciem ogłoszenia.</label> 
            <div id="upload-demo"></div>
            <div class="imageAttachment" id="upload-demo-id" data-id="<?= (is_file("uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? $model->id_user_fk : 0 ?>">
                <div class="btn-toolbar actions-bar">
                    <span class="btn btn-success btn-file">
                        <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                        <span data-replace-text="Zamień..." data-upload-text="Prześlij" class="file_label">Pobierz zdjęcie</span>
                        <input type="file" id="upload" name="Files[avatar]" value="Choose a file" data-path="<?= Url::to(['/client/upload','id' => $model->id_user_fk]) ?>">
                    </span>
                    <button class="btn btn-success upload-result" data-path="<?= Url::to(['/client/saveavatar','id' => $model->id_user_fk]) ?>">Zapisz avatar</button>
                    <!--<button class="btn btn-success change-view">Ustaw miniaturę</button>-->
                </div>  
            </div>
            <p class="text-brand"><i class="fa fa-info-circle"></i> Pierwsza rzeczą którą zobaczą inni użytkownicy będzie zdjęcie Twojej Firmy.</p>
		</div>
	</div>
    <?php } ?>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="firstname" class="form__label">Imię:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
			<?= $form->field($model, 'firstname', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Imię']) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="lastname" class="form__label">Nazwisko:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
			<?= $form->field($model, 'lastname', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Nazwisko']) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="sex" class="form__label">Płeć:</label>
		</div>
		<div class="form__row__right">
            <?= $form->field($model, 'sex', ['template' => '{input}',  'options' => ['class' => 'form__select']])->dropdownList(['F' => 'Kobieta', 'M' => 'Mężczyzna'], ['class' => 'form__input', 'prompt' => 'Płeć']); ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="province" class="form__label">Województwo:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
            <?= $form->field($model, 'id_province_fk', ['template' => '{input}',  'options' => ['class' => 'form__select']])->dropdownList(ArrayHelper::map( backend\Modules\Loc\models\LocProvince::find()->orderby('name')->all(), 'id', 'name'), ['class' => 'form__input', 'prompt' => 'Region']); ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="city" class="form__label">Miejscowość:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
			<?= $form->field($model, 'city', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Miejscowość']) ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="postalcode" class="form__label">Kod pocztowy:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="text" required="" placeholder="Kod pocztowy" name="name-title" class="form__input form__input--title">	 -->
			<?= $form->field($model, 'postal_code', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Kod pocztowy']) ?>
		</div>
	</div>
	
	<div class="form__row">
		<div class="form__row__left">
			<label for="address" class="form__label">Ulica i numer lokalu:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="text" required="" placeholder="Kod pocztowy" name="name-title" class="form__input form__input--title">	 -->
			<?= $form->field($model, 'address', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Ulica i numer lokalu']) ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="phone" class="form__label">Telefon kontaktowy:</label>
		</div>
		<div class="form__row__right">
			<!--<input type="text" required="" id="postcode" name="postcode" class="form__input form__input--narrow-60">-->
			<?= $form->field($model, 'phone', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Telefon kontaktowy']) ?>

		</div>
	</div>
	
	<div class="form__row">
		<div class="form__row__left">
			<label for="email" class="form__label">Email:</label>
		</div>
		<div class="form__row__right">
			<?= $form->field($model, 'email', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Email']) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="birthday" class="form__label">Data urodzenia:</label>
		</div>
		<div class="form__row__right">
            <div class="form-group">
                <div class='input-group date' id='svcclient-birthday-calendar'>
                    <input type="text" class="form-control" name="SvcClient[birthday]" id="svcclient-birthday" value="<?= $model->birthday ?>" />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar">
                        </span>
                    </span>
                </div>
            </div>
            
            
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="funpage" class="form__label">Funpage:</label>
		</div>
		<div class="form__row__right">
			<?= $form->field($model, 'facebook_url', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Funpage']) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="profession" class="form__label">Zawód:</label>
		</div>
		<div class="form__row__right">
			<?= $form->field($model, 'profession', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Zawód']) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="trade" class="form__label">Branża:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
            <?= $form->field($model, 'id_trade_fk', ['template' => '{input}',  'options' => ['class' => 'form__select']])->dropdownList(ArrayHelper::map( backend\Modules\Svc\models\SvcTrade::find()->orderby('name')->all(), 'id', 'name'), ['class' => 'form__input', 'prompt' => 'Branża']); ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="education" class="form__label">Wykształcenie:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
            <?= $form->field($model, 'id_education_fk', ['template' => '{input}',  'options' => ['class' => 'form__select']])->dropdownList(ArrayHelper::map( backend\Modules\Svc\models\SvcEducation::find()->orderby('name')->all(), 'id', 'name'), ['class' => 'form__input', 'prompt' => 'Wykształcenie']); ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="describe" class="form__label">Opis:</label>
		</div>
		<div class="form__row__right">
			<!--<textarea required="" id="message" name="message" class="form__textarea"></textarea>-->
			<?= $form->field($model, 'note')->textarea(['rows' => 6, 'class' => 'form__textarea'])->label(false) ?>
		</div>
	</div>
    <?php if($model->isNewRecord) { ?>
	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right">
			<input type="checkbox" name="SvcOffer[rules]" id="agree" class="form__checkbox"><label class="form__checkbox-label" for="agree">Akceptuję regulamin</label>
		</div>
	</div>
    <?php } ?>

	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right text-center">
			<input type="text" id="contact-mail" name="contact-mail">
			<button title="Send message" type="submit" class="btn more_btn2"><?= ($model->isNewRecord) ? 'Utwórz ofertę' : 'Aktualizuj ofertę' ?> </button>
		</div>
	</div>
<?php ActiveForm::end(); ?>

