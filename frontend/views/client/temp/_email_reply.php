<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Wiadomości';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'offer-email-view', 'title'=>Html::encode($this->title))) ?>	
    <?= Alert::widget() ?>
    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-envelope"></i>Wiadomość</h2> 
                
            </div>
            
            <?= $this->render('_email_reply_form', ['model' => $replyEmail]) ?>
            <?= $this->render('_msg', ['model' => $email, 'showIcons' => false]) ?>
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>