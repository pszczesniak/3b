<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */


$this->title = 'Pakiet';
?>

<?php $this->beginContent('@app/views/layouts/offer-window.php',array('model' => $model, 'class'=>'offer-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <h2><i class="fa fa-level-up"></i>Zmień swój pakiet na lepszy</h2>
            <div class="user-testimonial">
                <h3>2017-04-30 </h3><h4>do tego dnia ważny jest Twój pakiet</h4>
                <p><a class="btn more_btn2" href="#"><i class="fa fa-cc-visa "></i> Przedłuż ważność pakietu</a></p>
                <p>
                    <hr></hr>
                    <h3>Już teraz możesz zmienić swój pakiet na lepszy</h3>
                </p>
                <div class="row">
                    <div class="col-xs-6">
                        <p><a class="btn more_btn2" href="#"><i class="fa fa-hand-o-right"></i> Pakiet 'Aktywny'</a></p>
                    </div>
                    <div class="col-xs-6">
                        <p><a class="btn more_btn2" href="#"><i class="fa fa-hand-o-right"></i> Pakiet "VIP"</a></p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>

