<?php
	use frontend\widgets\svc\EventsBlock;
?>
<section class="bg--folder">
	<div class="container">
		<div class="row-g-20">
			<h2 class="headline-3">KREATOR IDEALNEJ UROCZYSTOŚCI</h3>
			<!--<p class="margin4 textStyle12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse molestie metus ut auctor ornare. Phasellus sagittis tincidunt odio non blandit.  Duis consequat mauris aliquam leo gravida, eget tempus nulla fermentum.</p>-->
			<?= Eventsblock::widget() ?>
		</div>
	</div>
</section>