<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */
use yii\authclient\widgets\AuthChoice;

$this->title = 'Media społecznościowe';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'client-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-globe"></i>Media społecznościowe</h2> 
               		<?php
                        $response = false;
                        $appId = '241429362869663';
                        $appSecret = '6266a632e5e405501a7ff76f8796897b';
                        $accessToken = \Yii::$app->session->get('fb.token');
                        
                        if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
                            $fb = new \Facebook\Facebook([
                                'app_id' => $appId,
                                'app_secret' => $appSecret,
                                'default_graph_version' => 'v2.5',
                                'default_access_token' => \Yii::$app->session->get('fb.token'),
                                'cookie' => true
                            ]);
                            $linkData = [
                              'link' => 'http://twojeprzyjecia.dev',
                              'message' => 'User provided message',
                              ];

                               
                            try {
                                //$response = $fb->get('/me?fields=id,name,albums.limit(5){name, photos.limit(2){name, picture, tags.limit(2)}},posts.limit(5)');//var_dump($response); echo '<br />';
                                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                                $user = $response->getGraphUser();
                              //  echo 'Name: ' . $user->getName().'<br />';
                               // var_dump($user);
                                $res = $fb->get('/me/picture?type=large&redirect=false',  $accessToken);
                              //  echo '<br /><br />';
                                $picture = $res->getGraphObject();
 
                               // var_dump( $picture );
                                
                                echo '<div class="facebook">'
                                    .'<ul>'
                                        .'<li><img class="avatar" src="'.$picture['url'].'" alt="FB_IMG"></img></li>'
                                        .'<li>Witaj '.$user->getName().'</li>'
                                        .'<li><a href="'.$user['link'].'" target="_blank"">Twój fanpage</a></li>'
                                        //.'<li><a href="#">Find Friends</a></li>'
                                        /*.'<li id="noti_Container">'
                                            .'<div id="noti_Counter"></div>'
                                            .'<div id="noti_Button"></div>'    
                                            .'<div id="notifications">'
                                                .'<h3>Notifications</h3>'
                                                .'<div style="height:300px;"></div>'
                                                .'<div class="seeAll"><a href="#">See All</a></div>'
                                            .'</div>'
                                        .'</li>'*/
                                        
                                    .'</ul><div class="clear"></div>'
                                .'</div>';
             
                            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                                echo 'Graph returned an error: ' . $e->getMessage();
                            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                            }
                            echo '<div id="fb-content"><div class="alert">Twój profil został połączony z kontem na facebooku. Nie będziemy umieszczać, żadnych postów bez Twojej zgody.</div></div>';
                            echo '<div class="align-right"><a href="/client/fbdisconnet" class="btn btn-primary btn-icon"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Rozłącz</a></div>';
                        
                            /*$helper = $fb->getRedirectLoginHelper();
                            $permissions = ['email', 'user_likes'];
                            $loginUrl = $helper->getLoginUrl('http://facebook.devils-heaven.com/login-callback.php', $permissions);
                            echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';*/
                        
                        } else {
                        
                            /*echo yii\authclient\widgets\AuthChoice::widget([
                                 'baseAuthUrl' => ['offer/auth']
                            ]);*/
                            
                            $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['client/auth'], 'autoRender' => false, 'popupMode' => false]); 
                                echo '<ul>';
                                foreach ($authAuthChoice->getClients() as $client) { 
                                    echo '<li>'.Html::a( '<i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Połącz się z '. $client->title, ['client/auth', 'authclient'=> $client->name, ], ['class' => "btn btn-primary btn-icon $client->name "]).'</li>';
                                } 
                                echo '</ul>';
                            AuthChoice::end(); 
                        }

                        
                
                    ?>   
                <!--
                Twój profil został połączony z kontem na facebooku.
                Nie będziemy umieszczać, żadnych postów bez Twojej zgody.-->
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>
