<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Logowanie';
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/client-container.php',array('class'=>'login', 'title'=>Html::encode($this->title), 'back' => false, 'logout' => true)) ?>

<div class="login-page">
    <div class="panel-bg">
        <?= Alert::widget() ?>
        <div class="panel">
        <div class="panel-heading"> <span class="panel-title"> <span class="fa fa-lock text-purple2"></span> Logowanie </span> <span class="panel-header-menu pull-right">Pomoc</span> </div>
            <div class="panel-body">
                <div class="login-avatar"> <img src="/images/avatar.png"  alt="avatar"> </div>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['autocomplete' => 'off']]); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'placeholder' => 'login', 'autocomplete' => 'off'])->label(false) ?>

                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => false, 'placeholder' => 'hasło', 'autocomplete' => 'off'])->label(false) ?>


                    <div class="form-group align-right">
                        <?= Html::submitButton('Zaloguj się', ['class' => 'btn btn--teal btn--full-width', 'name' => 'login-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="panel-footer"> Jeśli zapomniałeś hasła <?= Html::a('zresetuj je', ['/site/request-password-reset']) ?>.
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>
