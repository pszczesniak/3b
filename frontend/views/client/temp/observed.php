<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Searchformwidget;
use yii\widgets\LinkPager;

$this->title = 'Twojeprzyjecia.pl';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'observed', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <h2><i class="fa fa-star"></i>Obserwowane oferty</h4> 
            <div class="page-search-result">
                <?php $iter = 1;
                    foreach ($models as $key => $offer) {
                        if($iter == 1) {
                            echo '<div class="row">'; 
                        }
                            echo '<div class="col-xs-12 col-sm-6 padding10">'.$this->render('_search_item_large', ['model' => $offer]).'</div>';
                        if($iter == 2) {
                            echo '</div>';
                            $iter = 1;
                        } else {
                            $iter = 2;
                        }
                            
                    } 
                ?>
               
                <?php echo LinkPager::widget([ 'pagination' => $pages, ]); ?>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>


<?= $this->render('_events') ?>

