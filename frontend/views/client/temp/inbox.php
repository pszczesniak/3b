<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

$this->title = 'Media społecznościowe';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'client-inbox', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-globe"></i>Wiadomości</h2> 
                <?= $this->render('_inbox', [
                    'model' => $model, 'type' => $type
                ]) ?>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>