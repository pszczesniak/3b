<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="ajax-table-items">	 
	<div id="toolbar-messages" class="btn-group">
		<?php /* Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New')), Url::to(['/lsdd/lsddsupplierrecipies/create', 'id' => $model->id]) , 
					['class' => 'btn btn-success btn-icon gridViewModal', 
					 'data-toggle' => "modal", 
					 'data-target' => "#modal-grid-recipies", 
					 'data-form' => "recipies-form", 
					 'data-table' => "table-recipies",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('lsdd', 'New') 
					])*/ ?>
		<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
	</div>
	<table  class="table table-striped table-hover"  id="table-messages"
		    data-toolbar="#toolbar-messages" 
			data-toggle="table" 
			data-show-refresh="true"
			data-show-toggle="false" 
			data-show-export="false" 
            data-search="true"
			data-filter-control="true"
			data-unique-id="id"
            data-show-pagination-switch="false"
            data-page-list="false"
            data-pagination="true"
            data-id-field="id"
            data-show-footer="false"
			data-url=<?= Url::to(['/client/messages', 'id' => $model->id, 'type' => $type]) ?> >
		<thead>
			<tr>
				<!--<th data-field="state" data-checkbox="true"></th>-->
				<th data-field="offer" class="td-email" >Oferta</th>
                <th data-field="content" >Treść</th>
                <th data-field="type" class="td-type">Typ</th>
                <th data-field="avatar" class="td-avatar-img"></th>
                <th data-field="date" class="td-date">Data</th>
				<th data-field="action" class="td-actions"></th>
			</tr>
		</thead>
		<tbody class="ui-sortable">
			
		</tbody>

	</table>


	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-recipies', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';

		yii\bootstrap\Modal::end();
	?> 
</div>