<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Uploaderwidget;


$this->title = 'Galeria';
?>

<?php $this->beginContent('@app/views/layouts/offer-window.php',array('model' => $model, 'class'=>'offer-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-file-image-o"></i>Galeria zdjęć</h2>
                <?= Uploaderwidget::widget(['typeId' => 1, 'parentId' => $model->id]) ?>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>

<script>
function initMap() { 
	var latlng = new google.maps.LatLng(<?= $model->pos_lat ?>, <?= $model->pos_lng ?>);
	
	map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: <?= $model->pos_lat ?>, lng: <?= $model->pos_lng ?>}, 
		  zoom: 15
	});
	
	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		title: 'Opis',
		draggable: true
	});

	google.maps.event.addListener(marker, 'dragend', function (event) {
		document.getElementById("latbox").value = <?= $model->pos_lat ?>;
		document.getElementById("lngbox").value = <?= $model->pos_lng ?>;
	});
	
	var infoWindow = new google.maps.InfoWindow({map: map});
	
	google.maps.event.addListener(map,'click',function(event) {
		document.getElementById('svcoffer-pos_lat').value = event.latLng.lat(); 
		document.getElementById('svcoffer-pos_lng').value = event.latLng.lng(); 
	}); 
	
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
	searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
         /* markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];*/

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            /*markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));*/

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });


	// Try HTML5 geolocation.
	if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(function(position) {
		var pos = {
		  lat: position.coords.latitude,
		  lng: position.coords.longitude
		};

		infoWindow.setPosition(pos);
		infoWindow.setContent('Location found.');
		map.setCenter(pos);
		
		geocoder = new google.maps.Geocoder();
		
		var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		geocoder.geocode({'latLng': latlng}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			  /* console.log(results) */
				if (results[1]) {
				//alert(results[0].formatted_address)
				document.getElementById('svcoffer-address').value = results[0].formatted_address;
				for (var i=0; i<results[0].address_components.length; i++) {
					for (var b=0;b<results[0].address_components[i].types.length;b++) {

						if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
							//this is the object you are looking for
							city= results[0].address_components[i];
							break;
						}
					}
				}
				//city data
				//alert(city.short_name + " " + city.long_name)
                document.getElementById('svcoffer-city').value = city.short_name;

				} else {
				  alert("No results found");
				}
			} else {
				alert("Geocoder failed due to: " + status);
			}
		});
	  }, function() {
		handleLocationError(true, infoWindow, map.getCenter());
	  });
	} else {
	  // Browser doesn't support Geolocation
	  handleLocationError(false, infoWindow, map.getCenter());
	}
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
infoWindow.setPosition(pos);
infoWindow.setContent(browserHasGeolocation ?
					  'Error: The Geolocation service failed.' :
					  'Error: Your browser doesn\'t support geolocation.');
}
</script>

