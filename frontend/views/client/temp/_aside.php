<div class="apl-calculator-aside">
    <h5>Chcę pożyczyć <b>10 000 zł</b></h5>
    <table>
        <tr><td>Wysokość raty:</td><th>389,00 zł</th></tr>
        <tr><td>Kwota pożyczki:</td><th>1000,00 zł</th></tr>
        <tr><td>Okres kredytowania:</td><th>17.06.2017 r.</th></tr>
        <tr><td>Liczba rat:</td><th>12 m-ce</th></tr>
        <tr><td>Opłaty:</td><th>189,00 zł</th></tr>
        <tr><td>RRSO:</td><th>137 %</th></tr>
    </table>
</div>
<div class="apl-info">
    <ul>
        <li>
            <h3>Do czego potrzebne są moje dane osobowe?</h3>
            <p>Twoje dane potrzebne są nam do rozpatrzenia Twojego wniosku o pożyczkę, a także do zidentyfikowania Ciebie jako właściciela konta, które nam podałeś. Dzięki temu unikniemy problemów z identyfikacją.</p>
        </li>
        <li>
            <h3>Czy moje dane są bezpieczne?</h3>
            <p>Całkowicie! Zgodnie z ustawą o Ochronie Danych Osobowych, w gtw-finanse.pl zapewniamy całkowitą poufność Twoich danych. W tym celu wprowadziliśmy niezbędne środki bezpieczeństwa uniemożliwiające wszelką modyfikację, zagubieniu lub nieuprawnione użycie Twoich danych. Ponadto, posiadamy certyfikat zaufania w Internecie.</p>
        </li>
        <li>
            <h3>Potrzebujesz pomocy?</h3>
            <p>Zadzwoń! Tel. 500 000 000 godz. Pracy: Pn-Pt od 8:00  do 16:00</p>
        </li>
    </ul>

</div>