<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use common\widgets\Alert;
    /* @var $this yii\web\View */

    use frontend\widgets\Calculator;
    use backend\Modules\Eg\models\Patient;
    use backend\Modules\Eg\models\Visit;
?>
<table class="custom-table" id="table-visits">
    <thead><tr><th>#</th><th>Termin</th><th>Typ imprezy</th><th>Status</th><th></th></tr></thead>
    <tbody>
        <?php $states = Visit::getStates(); ?>
        <?php foreach(Patient::getVisits($id) as $key => $visit) {
            echo '<tr>'
                    .'<td data-column="#">'.($key + 1).'</td>'
                    .'<td data-column="Termin">'.$visit['visit_day'].'</td>'
                    //.'<td data-column="Zakończenie">'.date('Y-m-d H:i', strtotime($visit['end'])).'</td>'
                    .'<td data-column="Typ imprezy">'.$visit['type_name'].'</td>'
                    .'<td data-column="Status">'.(isset($states['id_dict_eg_visit_status_fk']) ? $states['id_dict_eg_visit_status_fk'] : 'nowy').'</td>'
                    .'<td data-column="Operacje" class="center">'
                        .'<a href="'.Url::to(['/client/ubooking', 'id' => $visit['id']]).'" class="viewModal" data-id="show">edytuj</a>'
                        . (($visit['id_gallery_fk']) ? '&nbsp;|&nbsp;<a href="'.Url::to(['/client/ugallery', 'gid' => $visit['id']]).'">zdjęcia</a>' : '')
                    .'</td>'
                .'</tr>';
        } ?>
    </tbody>
    
</table>