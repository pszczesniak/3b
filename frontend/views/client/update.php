<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use frontend\widgets\Calculator;
/* @var $this yii\web\View */

$this->title = 'Twój profil';

$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = ['label' => 'Twoje konto', 'url' => ['account']];
$this->params['breadcrumbs'][] = 'Aktualizacja';
?>

<?php $this->beginContent('@app/views/layouts/account-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false)) ?>	

    <?= Alert::widget([]) ?>

    <?= $this->render('_formBasic', ['model' => $model]) ?>

<?php $this->endContent(); ?>

