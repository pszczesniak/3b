<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use frontend\widgets\cms\BlockWidget;
use backend\Modules\Company\models\Company;

$this->title = Yii::$app->name;
//$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = $this->title;

$template = '<div class="form__row">{label}{input}{error}</div>';

?>
<?php $this->beginContent('@app/views/layouts/client-container.php', array('header' => false, 'class' => 'login', 'title' => Html::encode($this->title), 'back' => false, 'logout' => false)) ?>

<section class="block block--narrow block--middle order-sm-2">
	<h2 class="headline-11 color-gold text-center">Logowanie</h2>

	<div class="login-grid login-grid--narrow">
		<div class="login-grid__top row">
			<?= Alert::widget() ?>

			<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['autocomplete' => 'off']]); ?>

				<?= $form->field($model, 'username', ['template' => $template])->textInput(['autofocus' => false, 'autocomplete' => 'off', 'class' => 'form-control form__input'])->label('Login', ['class' => 'form__label form__label--thin']) ?>

				<?= $form->field($model, 'password', ['template' => $template])->passwordInput(['autofocus' => false, 'autocomplete' => 'off', 'class' => 'form-control form__input'])->label('Hasło', ['class' => 'form__label form__label--thin']) ?>

				
				<div class="form__row mt--5em">
					<?= Html::submitButton('Zaloguj się', ['class' => 'btn btn--borderr btn--full btn--low-thin btn--solid-gold', 'name' => 'login-button']) ?>
				</div>

			<?php ActiveForm::end(); ?>

			<a class="block small-blue-link text-center mt--5em" href="<?= Url::to(['/site/request-password-reset']) ?>" title="Zapomniałaś(eś) hasła?">Zamponiałeś hasła?</a>
			<a class="block small-blue-link text-center mt--5em" href="<?= Url::to(['/apl/index']) ?>" title="Nie masz konta??">Nie masz konta?</a>
		</div>
		
		<div class="login-grid__middle">lub</div>
		
		<div class="login-grid__top">
			<label class="form__label form__label--large color-gold text-center">Galerie klientów</label>
			<a class="image-button" href="/site/galleries" title="Galerie klientów"><img src="/images/3b/fotobudka_135.jpg" /></a>
		</div>
	</div>
	
</section>

<?php $this->endContent(); ?>
