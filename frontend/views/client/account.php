<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Calculator;
use backend\Modules\Eg\models\Patient;
use backend\Modules\Eg\models\Visit;

$this->title = 'Twoje konto';

$this->params['breadcrumbs'][] = 'Strefa klienta';
?>

<?php $this->beginContent('@app/views/layouts/client-container.php', array('header' => false, 'model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false, 'logout' => true)) ?>	
    <?= Alert::widget([]) ?>
    <div class="grid">
        <!--<div class="col-lg-3 col-md-4 col-xs-12"> </div>-->
        <!--<div class="col-lg-9 col-md-8 col-xs-12">-->
        <div class="col-xs-12">    
            <fieldset><legend>Zarządzanie kontem</legend>
                <div class="grid flex flex--between">
                    <div class="col-sm-4 col-xs-12"><a href="<?= Url::to(['/client/basic']) ?>" class="social-button social-button--soc-fb">Profil</a></div>
                    <div class="col-sm-4 col-xs-12"><a href="<?= Url::to(['/client/booking']) ?>" class="social-button social-button--soc-insta viewModal" data-id="show">Rezerwacja</a></div>
                    <div class="col-sm-4 col-xs-12"><a href="<?= Url::to(['/client/message']) ?>" class="social-button social-button--soc-twit viewModal" data-id="show">Napisz do Nas</a></div>

                    <div class="col-sm-12 col-xs-12 text-center"><a href="<?= Url::to(['/client/changepassword']) ?>" class="btn btn--underline btn--underline-gold btn--medium ">Resetowanie hasła</a></div>
                    <!--<div class="col-sm-6 col-xs-12"><a href="<?= Url::to(['/client/testimonials']) ?>" class="btn btn--fill btn--full-width">Twoje komentarze</a></div>-->
                </div>
            </fieldset>
            <div id="client-terms"><?= $this->render('_terms', ['id' => $model->id]) ?></div>
        </div>
    </div>
<?php $this->endContent(); ?>

