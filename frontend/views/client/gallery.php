<?php
/* @var $this yii\web\View */
use frontend\widgets\cms\MainWidgets;
use frontend\widgets\Calculator;
use frontend\widgets\cms\GalleryWidget;

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'GALERIA: '.$model->additional_info;
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = ['label' => 'Galerie klientów', 'url' => Url::to(['/site/galleries'])];

$this->params['breadcrumbs'][] = $this->title;

$img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/ugallery/cover/".$model->id."/medium.jpg")) ? "/uploads/ugallery/cover/".$model->id."/medium.jpg" :  "/images/3b/logo_fotobudka.png";

?>
<?php $this->beginContent('@app/views/layouts/client-container.php', array('img' => $img, 'header' => (!$access) ? true : false, 'class' => 'page', 'title' => Html::encode($this->title))) ?>
		<?php if($access) {
			echo GalleryWidget::widget(['id' => $model->id_gallery_fk]); 
		} else { ?>
			<div class="product-name order-sm-1">
				<h2 class="headline-14 text-upper color-gold">Weryfikacja dostępu</h2>
				<h1 class="headline-2 color-dark-gray"><?= $model->additional_info ?></h1>
			</div>
			<div class="product-action order-sm-2">Galeria użytkownika zabezpieczona jest hasłem.<br />Aby odblokować zawartość galerii prosimy o podanie kodu i zatwierdzenie go.</div>
			<form method="POST" class="enter__form enter__form--no-margin">
				<?php if($passError) { ?><div class="alert alert-danger">Podano błędny kod dostępu.</div><?php } ?>
				<p class="text-left">
					<label for="accessCode" class="form__label">Kod</label>
					<input type="accessCode" name="accessCode" id="accessCode" class="form__input" placeholder="wpisz kod" required>
				</p>

				<p class="text-center mt--20em">
					<button class="btn btn--underline btn--underline-red btn--medium" type="submit">
						<span class="btn__text color-black">Zatwierdź</span>
					</button>
				</p>
			</form>
				
		<?php } ?>
          
<?php $this->endContent(); ?>



