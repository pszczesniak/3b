<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use frontend\widgets\Calculator;
/* @var $this yii\web\View */

$this->title = 'Twoje komentarze';

$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = ['label' => 'Twoje konto', 'url' => ['account']];
$this->params['breadcrumbs'][] = 'Komentarze';
?>

<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => true, 'logout' => true)) ?>	
    <?= Alert::widget([]) ?>
    <div class="grid">
		<div class="col-md-3 col-sm-4 col-xs-12">
            <div class="calculator"><?= Calculator::widget(['action' => Url::to(['/apl/index'])]) ?></div>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <fieldset><legend>Nowy komentarz</legend>
                 <?php $form = ActiveForm::begin([
                        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                       // 'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-comments"],
                    ]); ?>
                    <?= $form->field($model, 'comment')->textarea(['rows' => 4, 'class' => 'form__textarea'])->label(false) ?>
                    <div class="align-right">
                        <button title="Send message" type="submit" class="btn btn--teal">Wyślij komentarz</button>
                    </div>
            <?php ActiveForm::end(); ?>
            </fieldset><br />
            <?php if(count($testimonials) > 0) { ?>
            <h4>Wysłane komentarze</h4>
            <section role="complementary" class="quotes">
                <?php
                    foreach($testimonials as $key => $testimonial) { 
                        echo '<blockquote class="quote-card '.$testimonial->statusColor.'-card">'
                                .'<p>'.$testimonial->comment.'</p>'
                                .'<cite>'.$testimonial->statusName.'</cite>'
                            .'</blockquote>';
                    }
                ?>    
            </section>
            <?php } ?>
        </div>
    </div>
<?php $this->endContent(); ?>

