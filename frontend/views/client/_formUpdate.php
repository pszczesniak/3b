<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([ 'options' => ['class' => 'modal-form', 'data-target' => '#modal-view'] ]); ?>
    <div class="modal-body">
        <div class="tab-block">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Profil</a></li>
                <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Kontakt</a></li>
                <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Finanse</a></li>
            </ul>

              <!-- Tab panes -->
           <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab1">
                    <div class="grid">
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            
                            <fieldset><legend>Dane osobowe</legend>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput( ['maxlength' => 255] )  ?></div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput( ['maxlength' => 255] )  ?> </div>
                                </div>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'pesel')->textInput( ['maxlength' => 255] )  ?></div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'evidence_number')->textInput( ['maxlength' => 255] )  ?> </div>
                                </div>
                            </fieldset>
                            <fieldset><legend>Zatrudnienie</legend>
                                <?= $form->field($model, 'employer_name')->textInput( ['maxlength' => 255] )  ?> 
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12">                    
                                        <?= $form->field($model, 'employment_status', [])->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusEmployment(), [ 'prompt' => '-- wybierz --']); ?>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="aplcustomer-employment_period" class="control-label">Data wydarzenia</label>
                                            <input type='text' class="form-control employment_period" id="aplcustomer-employment_period" name="AplCustomer[employment_period]" value="<?= $model->employment_period ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <fieldset><legend>Dodatkowe</legend>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12">                    
                                        <?= $form->field($model, 'id_marital_status_fk', [])->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusMarital(), [ 'prompt' => '-- wybierz --']); ?>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <?= $form->field($model, 'number_of_members')->textInput( ['maxlength' => 255] )  ?>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset><legend>Samochód</legend>
                                <?= $form->field($model, 'own_car')->checkBox([ 'uncheck' => 0, 'checked' => 1])  ?>
                                <div id="car-info" class="car-info <?= ($model->own_car == 1) ? '' : 'no-display'?>" >
                                    <?= $form->field($model, 'car_type')->textInput( ['maxlength' => 255 ] )  ?>
                                    <div class="grid">
                                        <div class="col-sm-6 col-xs-12">                    
                                            <?= $form->field($model, 'car_year')->textInput( ['maxlength' => 255] )  ?>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="aplcustomer-car_insurance" class="control-label">Data wydarzenia</label>
                                                    <input type='text' class="form-control car_insurance" id="aplcustomer-car_insurance" name="AplCustomer[car_insurance]" value="<?= $model->car_insurance ?>"  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab2">
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12">
                            <fieldset><legend>Telefon</legend>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone_mobile')->textInput( ['maxlength' => 255] )  ?></div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput( ['maxlength' => 255] )  ?> </div>
                                </div>
                            </fieldset>
                            <fieldset><legend>Adres zameldowania</legend>
                                <?= $form->field($model, 'address_status')->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusAddress(), ['prompt' => '-- wybierz --']); ?>
                                <div class="grid">
                                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'address_postalcode')->textInput( ['maxlength' => 255] )  ?></div>
                                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'address_city')->textInput( ['maxlength' => 255] )  ?> </div>
                                </div>
                                <div class="grid">
                                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'address_street')->textInput( ['maxlength' => 255] )  ?></div>
                                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'address_street_number')->textInput( ['maxlength' => 255] )  ?> </div>
                                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'address_local_number')->textInput( ['maxlength' => 255] )  ?> </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?= $form->field($model, 'c_address')->checkBox([ 'uncheck' => null, 'checked' => true])  ?>
                            <fieldset><legend>Adres korespondencyjny</legend>
                                <div class="grid">
                                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'c_address_postalcode')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false ) ] )  ?></div>
                                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'c_address_city')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                                </div>
                                <div class="grid">
                                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'c_address_street')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?></div>
                                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'c_address_street_number')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'c_address_local_number')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab3">
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12">
                            <fieldset><legend>Finanse</legend>
                                <?= $form->field($model, 'monthly_income')->textInput( ['maxlength' => 255] )  ?>
                                <?= $form->field($model, 'monthly_expenses')->textInput( ['maxlength' => 255] )  ?> 
                                <?= $form->field($model, 'monthly_other_loans')->textInput( ['maxlength' => 255] )  ?> 
                            </fieldset> 
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <fieldset><legend>Bank</legend>
                                <?= $form->field($model, 'bank_name')->textInput( ['maxlength' => 255] )  ?>
                                <?= $form->field($model, 'bank_account_number')->textInput( ['maxlength' => 255] )  ?>
                                <?= $form->field($model, 'bank_how_long_use')->dropdownList(\backend\Modules\Apl\models\AplCustomer::periodBank(), ['prompt' => '-- wybierz --']); ?>
                            </fieldset>
                        </div>
                    </div>    
                    <fieldset><legend>Dodatkowe informacje</legend>                    
                        <div class="grid">
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'use_credit_card')->radioList([1 => 'TAK', 0 => 'NIE'], 
                                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                                    'item' => function ($index, $label, $name, $checked, $value) {
                                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                        },
                                                ]) ?>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'frequency_payment')->radioList([1 => 'Tygodniowo', 2 => 'Miesięcznie'], 
                                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                                    'item' => function ($index, $label, $name, $checked, $value) {
                                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                        },
                                                ]) ?>
                            </div>
                        </div>          
                    </fieldset> 
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer"> 
        <div class="form-group align-right">
            <?= Html::submitButton('Zapisz zmiany', ['class' => 'btn btn-info']) ?>
            <button aria-hidden="true" data-dismiss="modal" class="btn" type="button" >Wyjdź</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    
    $('.car_insurance').datetimepicker({
        format: 'YYYY-MM-DD',
        inline: false,
        minDate: 'now'
    });
    
    $('.employment_period').datetimepicker({
        format: 'YYYY-MM',
        inline: false,
    });
    
    document.getElementById('aplcustomer-own_car').onchange = function(event) {
        if(!event.target.checked) document.getElementById('car-info').classList.add('no-display');
        if(event.target.checked) document.getElementById('car-info').classList.remove('no-display');
    }
    
    document.getElementById('aplcustomer-c_address').onchange = function(event) {
        if(!event.target.checked) {
            document.getElementById('aplcustomer-c_address_city').disabled = true;
            document.getElementById('aplcustomer-c_address_postalcode').disabled = true;
            document.getElementById('aplcustomer-c_address_street').disabled = true;
            document.getElementById('aplcustomer-c_address_street_number').disabled = true;
            document.getElementById('aplcustomer-c_address_local_number').disabled = true;
        } 
        if(event.target.checked) {
            document.getElementById('aplcustomer-c_address_city').disabled = false;
            document.getElementById('aplcustomer-c_address_postalcode').disabled = false;
            document.getElementById('aplcustomer-c_address_street').disabled = false;
            document.getElementById('aplcustomer-c_address_street_number').disabled = false;
            document.getElementById('aplcustomer-c_address_local_number').disabled = false;
        }
    }
 </script>