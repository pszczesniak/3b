<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use common\components\CustomHelpers;
use frontend\widgets\company\EmployeesCheck;
use frontend\widgets\crm\SidesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
$templateInput = '<div class="form__row">{label}{input}{error}</div>';
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-container", 'data-table' => "#table-visits"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

    <!--<div class="row row-vertical-middle">
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="visit_fromDate">Start</label>
                <input type='text' class="form-control form__input" id="visit_fromDate" name="Visit[fromDate]" value="<?= $model->fromDate ?>"/> 
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="visit-toDate">Koniec</label>
                <input type='text' class="form-control form__input" id="visit_toDate" name="Visit[toDate]" value="<?= $model->toDate ?>"/>
            </div>
        </div>
    </div>-->
    <div class="row row-vertical-middle">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'visit_day', ['template' => $templateInput])->textInput(['id' => 'datePickerModal', 'maxlength' => true, 'class' => 'form-control form__input'])->label('Data imprezy', ['class' => 'form__label form__label--thin']) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_package_fk', ['template' => $templateInput])->dropDownList(ArrayHelper::map(\backend\Modules\Svc\models\SvcPackage::find()->where(['status' => 1])->orderby('id')->all(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'type-new select2Modal',] )->label('Usługa', ['class' => 'form__label form__label--thin']) ?></div>
    </div>
    <div class="row row-vertical-middle">
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'id_visit_type_fk', ['template' => $templateInput])->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Type::find()->where(['status' => 1, 'is_active' => 1])->orderby('type_name')->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'type-new select2Modal',] )->label('Typ', ['class' => 'form__label form__label--thin']) ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="row row-vertical-middle">
                <div class="col-xs-4"><?= $form->field($model, 'place_postalcode', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Kod', ['class' => 'form__label form__label--thin']) ?></div>
                <div class="col-xs-8"><?= $form->field($model, 'place_city', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Miejscowość', ['class' => 'form__label form__label--thin']) ?></div>
            </div>
        </div>
    
    </div>
    <?= $form->field($model, 'place_address', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Adres', ['class' => 'form__label form__label--thin']) ?>
    <?= $form->field($model, 'note', ['template' => $templateInput])->textArea(['rows' => 3, 'maxlength' => true, 'class' => 'form-control form__textarea'])->label('Opis', ['class' => 'form__label form__label--thin']) ?>
    <div>
        <?= Html::submitButton('Wyślij', ['class' => 'btn btn--fill btn--fill-gold']) ?>
        <button class="btn btn-sm btn-default modalCloseBtn" type="button">Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $.datetimepicker.setLocale('pl');
    $('#datePickerModal').datetimepicker({format: "Y-m-d", timepicker: false});
    /*$('#visit_toDate').datetimepicker({format: 'Y-m-d H:i'});*/
</script>

