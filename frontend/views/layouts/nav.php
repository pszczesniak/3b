<?php


use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\cms\MainMenu;

$galleries = \backend\Modules\Cms\models\CmsPage::find()->where(['status' => 1, 'fk_category_id' => 1])->all();

?>
<a class="ts-logo ts-logo--mob ts-logo--product" href="/" title="Thomson and Scott">
	<svg class="logo-black"><use xlink:href="images/icons.svg#icon-mob-logo-black"/></svg>
	<svg class="logo-white"><use xlink:href="images/icons.svg#icon-story-logo-white"/></svg>
</a>

<nav class="l-navbar l-navbar--white l-navbar--white-strip  ">
	<div class="l-navbar__right">
		<a class="l-navbar__basket js-shopping-bag-button" href="#" title="">
			<!--<svg width="28px" height="25px"><use xlink:href="images/icons.svg#icon-shopping-bag"/></svg>-->
			<span class="l-navbar__basket-items">Galerie</span>
		</a>

		<div class="shopping-bag js-shopping-bag">
			<div class="shopping-bag__inner-scroll">
				<h3 class="shopping-bag__headline">
					<!--<svg class="shopping-bag__icon" width="58px" height="50px"><use xlink:href="images/icons.svg#icon-shopping-bag"/></svg>-->
					<span>GALERIE</span>
				</h3>

				<a class="shopping-bag__close js-close-shopping-bag" href="#" title="Close Shopping Bag">
					<svg width="29px" height="29px"><use xlink:href="/images/icons.svg#icon-cross-main"/></svg>
				</a>

				<ul class="shopping-bag__list">
					<?php foreach($galleries as $key => $gallery) { $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$gallery->id."/medium.jpg")) ? "/uploads/pages/cover/".$gallery->id."/medium.jpg" : "/images/logo.png"; ?>
					<li class="shopping-bag__item">
						<figure class="shopping-bag__item-img">
							<img src="<?= $img ?>" alt="<?= $gallery->title ?>">
						</figure>
						
						<div class="shopping-bag__item-desc">
							<a class="shopping-bag__item-name" href="<?= Url::to(['site/page', 'pid' => $gallery->id, 'slug' => $gallery->title_slug]) ?>" title="TITLE"><?= $gallery->title ?></a>
						</div>
					</li>
					<?php } ?>
				</ul>

			</div>
		</div>

		<a class="burger js-open-nav" href="#" title="Menu">
			<svg class="burger__icon" width="35px" height="26px"><use xlink:href="/images/icons.svg#icon-burger-menu"/></svg>
		</a>
	</div>
</nav>
<nav class="l-sidebar js-sidebar">
	<a class="l-sidebar__close js-close-nav" href="#" title="Close menu">
		<svg width="29px" height="29px"><use xlink:href="/images/icons.svg#icon-cross-main"/></svg>
	</a>
	<?= MainMenu::widget(['id' => 1, 'options' => ['class' => 'l-sidebar__nav', 'id' => 'l-sidebar__nav--id' ], 'itemOptions' => ['class' => 'l-sidebar__item'], 'linkClass' => 'l-sidebar__link' ]) ?>  
<!--
	<ul class="l-sidebar__nav">
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Shop now">Shop now</a>
		</li>
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Join our club">Join our club</a>
		</li>
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Special offers">Special offers</a>
		</li>
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Our story">Our story</a>
		</li>
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Blog">Blog</a>
		</li>
		<li class="l-sidebar__item">
			<a class="l-sidebar__link" href="#" title="Contact">Contact</a>
		</li>
	</ul>-->
	<div class="l-sidebar__spacer"></div>

	<div class="l-sidebar__footer">
		<?php if(!\Yii::$app->session->get('user.client') || \Yii::$app->user->isGuest) { ?>
		<a class="l-sidebar__login" title="Zaloguj si� lub za�� konto" href="<?= Url::to(['/client/login']) ?>">Strefa klienta</a>
		<?php } else { ?>
			<a class="l-sidebar__login" title="Twoje konto" href="<?= Url::to(['/client/account']) ?>">Twoje konto</a>
		<?php } ?>
		<!--<a class="l-sidebar__small-link" href="" title="Therms and conditions">Therms and conditions</a>
		<a class="l-sidebar__small-link" href="" title="Privacy Policy">Privacy Policy</a>-->
	</div>
</nav>

<nav class="nav-left">
	<div class="nav-left__main nav-left__main--bg-top nav-left__main--padded"> 
		<div class="nav-left__main-logo">   
			<a href="/" class="nav-left__logo-link">               
				<img class="nav-left__logo" src="/images/3b/logo_fotovideo.png" alt="3b-artstudio logo">
			</a>   
			<!--<h2 class="headline-14 color-black mt--10em">CHAMPAGNE</h2>-->
		</div>
	</div>
	<a href="<?= Url::to(['site/page', 'pid' => 11, 'slug' => 'uslugi']) ?>" class="nav-left__sort">
		<div class="nav-left__sort-bg" style="background-image: url(/images/3b/IMG_4751.jpg);"></div>
		<h3 class="headline-19 color-darkGray nav-left__sort-headline"><img class="l-header__logo" src="/images/3b/logo_fotovideo.png" alt="3b-artstudio"></h3>
	</a>
	<a href="<?= Url::to(['site/page', 'pid' => 2, 'slug' => 'fotobudka']) ?>" class="nav-left__sort" >
		<div class="nav-left__sort-bg" style="background-image: url(/images/3b/fotobudka_135.jpg);"></div>
		<h3 class="headline-19 color-darkGray nav-left__sort-headline"><img class="l-header__logo" src="/images/3b/logo_fotobudka.png" alt="3b-artstudio"></h3>
	</a>
	<!--<a href="#" class="nav-left__sort">
		<div class="nav-left__sort-bg bg-third"></div>
		<h3 class="headline-19 nav-left__sort-headline">GALERIA</h3>
	</a>
	<a href="#" class="nav-left__sort">
		<div class="nav-left__sort-bg bg-secondary"></div>
		<h3 class="headline-19 nav-left__sort-headline">KONTAKT</h3>
	</a>-->
	<a href="<?= Url::to(['/client/login']) ?>" class="nav-left__sort bg-primary">
		<div class="nav-left__sort-bg"></div>
		<h3 class="headline-19 nav-left__sort-headline">STREFA KLIENTA</h3>
	</a>
</nav>
