<div id="footer-posts">
	<div class="wrapper">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="blog-post-s5">
                    <div class="blog-post-s5-thumb"></div>
                        <a href="#">  <img width="272" height="160" src="/images/fotolia_182024172.jpg" class="attachment-the-essence-s5 " alt=""></a>
                    </div>
                    <div class="blog-post-s5-title">
                        <h2 ><a href="#">Republicans Say They’re Embarrassed by Trump but Voting for Him Anyway</a></h2>
                    </div>
                    <div class="blog-post-s5-meta">
                        <div class="blog-post-s5-meta-author" >
                            <a href="http://meridianthemes-demo.net/the-essence/author/0176wpczar/" title="Posts by Carley Keener" rel="author">Carley Keener</a>	
                    	</div>
                        <div class="blog-post-s5-meta-date">
                            2 years ago		
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>	
</div>
<footer id="footer" class="site-footer container">		
	<div id="footer-widgets">
		<div class="grid">
			<div class="footer-widgets-1 col-sm-4">
				<section id="text-2" class="widget widget_text">			
                    <div class="textwidget">
                        <p><img src="/images/logo.png" alt="logo"></p>
                        <h4>nazwa-domeny</h4>
                        <p>Tortor. Curabitur sed semper neque. Integer lectus arcu, ornare eu nisi sit amet, vehicula finibus odio. Nulla facilisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div>
                </section>
                <section id="tag_cloud-2" class="widget widget_tag_cloud">
                    <h2 class="widget-title" >Popularne tagi</h2>
                    <div class="tagcloud">
                        <a href="#" class="tag-cloud-link">pożyczka</a>
                    </div>
                </section>				
            </div>
			<div class="footer-widgets-2 col-sm-4">
                <section id="the_essence_subscribe_widget-2" class="widget widget_the_essence_subscribe_widget">
                    <h2 class="widget-title">My Newsletter</h2>
                    <div class="subscribe-widget" style="background-image:url(/images/fotolia_169233286.jpg);">
                        <div class="subscribe-widget-inner">
                            <h4>Receive My News Straight Into Your Inbox!</h4>
                            <div class="widget_wysija_cont shortcode_wysija"><div id="msg-form-wysija-shortcode5a57834d427a4-1" class="wysija-msg ajax"></div>
                                <form method="post" action="#wysija" class="widget_wysija shortcode_wysija">
                                    <p class="wysija-paragraph">
                                        <input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Your Email Address" placeholder="Your Email Address" value="">
                                        <span class="abs-req">
                                            <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="">
                                        </span>
                                    </p>
                                    <input class="wysija-submit wysija-submit-field" type="submit" value="Send">

                                    <input type="hidden" name="form_id" value="1">
                                    <input type="hidden" name="action" value="save">
                                    <input type="hidden" name="controller" value="subscribers">
                                    <input type="hidden" value="1" name="wysija-page">

                                    <input type="hidden" name="wysija[user_list][list_ids]" value="1">
    
                                </form>
                            </div>
                            <h5 >Don’t Worry, I Don't Spam.</h5>
                        </div>
                    </div>
                </section>				
            </div>
			<div class="footer-widgets-3 col-sm-4">
                <section id="the_essence_posts_list_widget-2" class="widget widget_the_essence_posts_list_widget">
                    <h2 class="widget-title" >Recent Posts</h2>
                    <div class="posts-list-widget clearfix">
                        <div class="blog-post-s6 clearfix col col-12  post-39 post type-post status-publish format-standard has-post-thumbnail hentry category-travel tag-beauty tag-carousel tag-hair tag-style">
                            <div class="blog-post-s6-thumb">
                                <a href="#">
                                    <img width="85" height="85" src="/images/fotolia_165901499.jpg" class="attachment-the-essence-small size-the-essence-small wp-post-image"></a>
                            </div>
                            <div class="blog-post-s6-main">

                                <div class="blog-post-s6-title" data-mtst-selector=".blog-post-s6-title" data-mtst-label="Blog Post S6 - Title" data-mtst-no-support="background,border"><a href="http://meridianthemes-demo.net/the-essence/2016/04/07/why-did-reed-krakoff-walk-away-from-his-brand/">Why Did Reed Krakoff Walk Away from His Brand?</a></div>
                                <div class="blog-post-s6-comments" data-mtst-selector=".blog-post-s6-comments" data-mtst-label="Blog Post S6 - Title" data-mtst-no-support="background,border"><span class="fa fa-commenting-o"></span>3 comments</div>
                            </div><!-- .blog-post-s6-main -->
                        </div><!-- .blog-post-s6-post -->						
                    </div><!-- .posts-list-widget -->
                </section>				
            </div>
		</div><!-- .wrapper -->
	</div><!-- #footer-widgets -->
		
	<div id="footer-bottom">
		<div class="wrapper clearfix">
			<div id="footer-copyright">
				Designed &amp; Developed by <a href="#" rel="nofollow">KAPI Soft</a>			
            </div><!-- #footer-copyright -->
		</div><!-- .wrapper -->
	</div><!-- #footer-bottom -->
</footer>