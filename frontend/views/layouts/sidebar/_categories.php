<?php
	use yii\helpers\Url;
    use yii\helpers\Html;
    
    $categories = \backend\Modules\Blog\models\BlogCatalog::find()->where(['status' => 1, 'is_nav' => 1])->orderby('title')->all();

?>
<section class="widget">
    <h2 class="widget-title" >Kategorie</h2>
    <div class="posts-list-alt-widget clearfix">
        <ul class="blog-catalogs-list">
            <?php
                foreach($categories as $key => $value) {
                    echo '<li><a href="'.Url::to(['/blog/catalog', 'id' => $value->id, 'surname' => $value->title]).'">'.$value->title.'</a></li>';
               }
            ?>
        </ul>
    </div>
</section>