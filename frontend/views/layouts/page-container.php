<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\cms\MainMenu;

/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<?= $this->render('nav') ?>
<main class="l-main l-main--product">
	<section class="block block--large block--column">
		<div class="product-name">
			<!--<h2 class="headline-14 text-upper color-gold">3b-artstudio</h2>-->
			<h1 class="headline-2 color-dark-gray"><?= $title ?></h1>
			<h3 class="headline-14 color-gold">3b-artstudio</h3>
		</div>
		<?= $content ?>
	</section>
</main>
<!--
<aside class="aside-page aside-page--dark-bg bg-dark-gray">
	<a class="aside-page__btn js-aside-page" href="#" title="Find out more">
		<span class="aside-page__btn-show" >Find out more</span>
		<span class="aside-page__btn-hide color-white" >Hide</span>
	</a>

	<a class="aside-page__close js-aside-page-mobile" href="#" title="Close">
		<svg width="29px" height="29px"><use xlink:href="images/icons.svg#icon-cross-main"/></svg>
	</a>

	<div class="block block--overflow block--narrow">
		<h2 class="headline-4 mb--20em">Skinny Champagne Grand Cru Brut</h2>
		<h1 class="headline-1 mb--10em color-gold">Description</h1>

		<div class="content">
			<p>Thomson & Scott Skinny Champagne Grand Cru Brut is the flagship wine from Amanda Thomson's portfolio of top quality Champagne and sparkling wine. It contains up to 0.1g of sugar per litre.</p>
			<p>The base wines used in this blend come from the chalky slopes of the Grand Cru village of Verzy, famous for producing expressive Pinot Noir, and come mainly from the 2010 harvest.</p>
			<p>Reserve wines from older years make up 30-40%, adding complexity and richness. The proportion of Pinot Noir in the blend is 70%, with the remainder being Chardonnay, also from Grand Cru vineyards.</p>
			<p>This outstanding Champagne has a light amber colour with intense aromas of brioche and nuts and an appealing touch of spice and white pepper.</p>
			<p>Rich, silky and balanced on the tongue, Thomson & Scott Skinny Champagne Grand Cru Brut has a remarkably long finish, combining the purity of up to 0.1g of sugar per litre with the complexity gained from long ageing on its lees.</p>
		</div>
	</div>
</aside>

-->
