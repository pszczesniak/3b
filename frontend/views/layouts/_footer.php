<footer class="l-footer">
    <div class="section section-md bg-bright-gray">
        <div class="shell text-left">
            <div class="grid">
                <div class="col-sm-6 col-lg-3 text-center text-sm-left">
                    <div class="brand">
                        <a href="/l">
                            <img class="img-responsive" src="/images/logo.png" alt="" >
                        </a>
                    </div>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum.</p>-->
                    <div class="group group-xl">
                        <span class="small text-italic">Znajdziesz Nas:</span>
                        <ul class="inline-list">
                            <li> <a href="#" class="fa fa-facebook"></a> </li>
                            <li> <a href="#" class="fa fa-twitter"></a> </li>
                            <li> <a href="#" class="fa fa-google-plus"></a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <h4>Przydatne linki</h4>
                    <hr class="bg-shuttle-gray">
                    <ul class="footer-navigation">
                        <li> <a href="#">O Nas</a> </li>
                        <li> <a href="#">Blog</a> </li>
                        <li> <a href="#">Link</a> </li>
                        <li> <a href="#">Link</a> </li>
                        <li> <a href="#">Link</a> </li>
                        <li> <a href="#">Link</a> </li>
                        <li> <a href="#">Link</a> </li>
                        <li> <a href="#">Link</a> </li>
                    </ul>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <h4>Kontakt</h4>
                    <hr class="bg-shuttle-gray">
                    <address class="contact-info">
                        <ul class="list">
                            <li>
                                <span class="fa fa-phone"></span> <a href="callto:+1234567890">(123) 45678910</a>
                            </li>
                            <li>
                                <span class="fa fa-envelope-o text-picton-blue"></span> <a href="mailto:email@email.pl">email@email.pl</a>
                            </li>
                            <li>
                                <span class="fa fa-clock-o text-picton-blue"></span><span>Pon - Sob: 9:00 - 18:00</span>
                            </li>
                            <li>
                                <span class="fa fa-map-marker text-picton-blue"></span>
                                <a href="#">
                                    <span class="text-bold reveal-block">xxx</span>
                                    <span> 42-200 Częstochowa</span>
                                </a>
                            </li>
                        </ul>
                    </address>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <h4>Subskrybuj</h4>
                    <hr class="bg-shuttle-gray">
                    <p>Bądź na bieżąco.</p>
                    <form data-form-output="form-output-global" data-form-type="subscribe" method="post" action="#" class="rd-mailform rd-mailform-inline" novalidate="novalidate">
                        <div class="form-froup">
                            <input id="footer-contact-email" name="email" data-constraints="@Required @Email" class="form-input form-control-has-validation form-control-last-child" placehoder="E-mail" type="email"><span class="form-validation"></span>
                        </div>
                        <button type="submit" class="button button-xs button-primary-filled">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="shell text-center rights">
        <p>©&nbsp;
            <span id="copyright-year">2017</span>&nbsp;KAPI Soft.
                <a href="privacy-policy.html">Privacy policy</a>
                <!-- {%FOOTER_LINK}-->
            </p>
    </div>
</footer>