<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<header class="l-header l-header--banner">
	<div class="l-header__flex-container">
		<a href="<?= Url::to(['site/page', 'pid' => 11, 'slug' => 'uslugi']) ?>" class="l-header__block text-center">
			<div class="l-header__block-wrapper" style="background-image: url(/images/3b/IMG_4751.jpg);"></div> 
			<div class="l-header__label l-header__label--bg-white">
				<img class="l-header__logo" src="images/3b/logo_fotovideo.png" alt="3b-artstudio">
				<!--<h2 class="headline-14 color-darkBlueSecond">FOTO-WIDEO</h2>-->
			</div>
		</a>
		<div class="l-header__block l-header__block--column text-center" >
			<!--<div class="l-header__block-wrapper" style="background-image: url(images/backgrounds/video.jpeg);"></div>
			<div class="l-header__label l-header__label--bg-white">
				<img class="l-header__logo" src="images/icons/logo-black.svg" alt="3b-artstudio">
				<h2 class="headline-14 color-black">WIDEO</h2>
			</div>-->
			<a href="<?= Url::to(['/site/category','cid' => 1, 'cslug' => 'galerie']) ?>" class="l-header__block-color1" title="">GALERIA</a>
			<a href="<?= Url::to(['/site/page', 'pid' => 6, 'slug' => 'kontakt']) ?>" class="l-header__block-color2" title="">KONTAKT</a>
			<a href="<?= Url::to(['/client/login']) ?>" class="l-header__block-color3" title="">STREFA KLIENTA</a>
		</div>
		<a href="<?= Url::to(['site/page', 'pid' => 2, 'slug' => 'fotobudka']) ?>" class="l-header__block text-center" >
			<div class="l-header__block-wrapper" style="background-image: url(images/3b/fotobudka_135.jpg);"></div>
			<div class="l-header__label l-header__label--bg-white">
				<img class="l-header__logo" src="images/3b/logo_fotobudka.png" alt="3b-artstudio">
				<!--<h2 class="headline-14 color-darkBlueSecond">FOTOBUDKA</h2>-->
			</div>
		</a>
	</div>
</header>