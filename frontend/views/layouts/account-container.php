<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<?php /*$this->beginContent($controller->module->mainLayout)*/ ?>
<div class="account bg-smoke">
    <div class="container ">
        <div class="grid">
            <div class="col-lg-3">
                <div class="u-block-hover g-pos-rel">
                    <figure>  <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/images/avatar.png" alt="Image Description"> </figure>
                    
                    <span class="g-pos-abs user-info">
                        <a class="bg-blue" href="#!"><?= Yii::$app->user->identity['fullname'] ?></a> <small class="d-block bg-primary"><?= Yii::$app->user->identity['email'] ?></small>
                    </span>
                </div>

                <div class="list-group list-group-border-0 g-mb-40">
                    <a href="<?= Url::to(['/client/account']) ?>" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'account') ? 'active' : '' ?>">
                        <span><i class="fa fa-home"></i> Twój panel</span>
                    </a>
                    <a href="<?= Url::to(['/client/profile']) ?>" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'profile') ? 'active' : '' ?>">
                        <span><i class="fa fa-user"></i> Profil</span>
                    </a>
                    <a href="<?= Url::to(['/client/edit']) ?>" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'edit') ? 'active' : '' ?>">
                        <span><i class="fa fa-cog"></i> Ustawienia konta</span>
                    </a>
                    <a href="<?= Url::to(['/client/loans']) ?>" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'loans') ? 'active' : '' ?>">
                        <span><i class="fa fa-credit-card"></i> Pożyczki</span>
                        <span class="badge badge-info">9</span>
                    </a>
                    <a href="<?= Url::to(['/client/investments']) ?>" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'investments') ? 'active' : '' ?>">
                        <span><i class="fa fa-chart-line"></i> Inwestycje</span>
                        <span class="badge badge-info">9</span>
                    </a>
                    <a href="box" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'comments') ? 'active' : '' ?>">
                        <span><i class="fa fa-envelope"></i> Skrzynka kontaktowa</span>
                        <span class="badge badge-info">9</span>
                    </a>
                    <a href="comments" class="list-group-item list-group-item-action justify-content-between <?= (Yii::$app->controller->action->id == 'comments') ? 'active' : '' ?>">
                        <span><i class="fa fa-comments"></i> Komenatrze</span>
                        <span class="badge badge-info">9</span>
                    </a>
                    <a href="<?= Url::to(['/site/logout']) ?>" class="list-group-item list-group-item-action justify-content-between">
                        <span><i class="fa fa-lock"></i> Wyloguj</span>
                    </a>
                </div>
              </div>

              <div class="col-lg-9">
                <?= $content ?>
            </div>
         </div>
     </div>
 </div>    
<script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/admin.css?v=<?php echo date ("ymdHis", filemtime("./css/admin.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>