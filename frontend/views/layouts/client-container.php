<?php


use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\cms\MainMenu;

/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<?php /*$this->beginContent($controller->module->mainLayout)*/ ?>

<?= $this->render('nav') ?>
<?php if($header) { ?>
<header class="l-header l-header--fixed l-header--product two-third bg-light-gold-gradient" style=" background-repeat: no-repeat; background-image: url(<?= $img ?>); background-size: cover; opacity: 0.6;">
	<!--<div class="bottle">
		<div class="bottle__product bottle__product--no-animation" style="background-image: url(<?= $img ?>)">
			<img src="/images/3b/logo_fotobudka.png" alt="ratio">
		</div>
	</div>-->
</header>
<?php } ?>
<main class="l-main  l-main--product <?= ($header) ? ' l-main--full-height one-third' : '' ?>">
	<section class="<?= (!$header) ? 'block block--large block--column' : 'block block--narrow block--middle block--column' ?>">
		<?php if(!($header)) { ?>
		<div class="product-name">
			<!--<h2 class="headline-14 text-upper color-gold">3b-artstudio</h2>-->
			<h1 class="headline-2 color-dark-gray"><?= $title ?></h1>
			<h3 class="headline-14 color-gold">
				STREFA KLIENTA <?php if(isset($logout)) { ?><span class="panel-actions">
				<a href="<?= Url::to('/client/account') ?>"><svg width="20px" height="20px" class="svg-fill-blue"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons.svg#icon-home"></use> </svg></a>
				<a href="<?= Url::to('/site/logout') ?>" class="btn btn--border">wyloguj się</a></span><?php } ?>
			</h3>
		</div>
		<?php } ?>
		<?= $content ?>
	</section>
</main>
