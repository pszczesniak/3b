<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
?>

<div  class="chat">
    <ul class="chat-list" id="chat-list-<?= $new->id_parent_fk ?>">
        <?php 
            foreach($comments as $item){
                echo '<li>'.$this->render('_comment_single',array( 'comment' => $item, 'post'=>$post, 'new' => $new ))/\.'</li>';
            }
        ?>

    </ul>
    <div class="clear"></div>
</div>
