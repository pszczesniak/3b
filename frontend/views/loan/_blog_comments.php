<section id="comments" class="post-comments">
	<h2 id="comments-title" class="comments-title">
		Dołącz do dyskusji		<small><?= 'Liczba komentarzy: '.$post->commentsCount  ?></small>
	</h2>
	<ol class="commentlist" id="commentlist">
        <?php foreach($comments as $item){ echo '<li class="comment">'.$this->render('comment/_comment_single',array( 'comment' => $item, 'post' => $post )).'</li>'; } ?>
	</ol>
    
    <div class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title">Zostaw swój komentarz  </h3>
        <?= $this->render('comment/_form_comment', [
            'model' => $comment, 'post' => $post->id, 'formId' => 0
        ]);?>
    </div>
</section>
