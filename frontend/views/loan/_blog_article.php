<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Artykuł';
?>

<div id="tagline">

    <div class="wrapper">

        <h1><?= $data->title ?></h1>

        <div class="blog-post-meta clearfix">
           <div class="blog-post-meta-author clearfix">
                <div class="blog-post-meta-author-avatar">
                    <img alt="" src="/images/avatar.png" class="avatar" height="40" width="40">
                </div>
                <div class="blog-post-meta-author-main">
                    <div class="blog-post-meta-author-name"> <a href="#">Admin Admin</a></div>
                    <div class="blog-post-meta-author-date"><?= date('Y/m/d', strtotime($data->created_at)) ?></div>
                </div>
            </div>
            <div class="blog-post-meta-separator"></div>

            <div class="blog-post-meta-cats">
                <a href="#" rel="category tag"><?= $data->catalog['title'] ?></a>					
            </div>

            <div class="blog-post-meta-separator"></div>
        
            <div class="blog-post-meta-comments-count">
                <span class="fa fa-commenting-o"></span>
                <span><a href="#comments">komentarze: <?= count($data->comments) ?></a></span>
            </div>

            <div class="blog-post-meta-separator"></div>
                 <div class="blog-post-meta-share-count">
                    <span class="blog-post-meta-share-count-num">108</span>
                    <span class="blog-post-meta-share-count-text">shares</span>
                </div>
        
            </div>

        </div>

    </div>
<div class="blog-post clearfix">

    <div class="blog-post-thumb" style="background: url(/uploads/blog/<?= $data->id ?>/original.jpg)">  </div>
    <div class="blog-post-excerpt">
        <?= $data->brief ?>
    </div>
    <div class="blog-post-content">
        <?= $data->content ?>
    </div>
    <?php if($data->tags) { ?>
    <div class="blog-post-tags">
		Tagi: <?php foreach($data->ftags as $key => $tag) echo '<a href="'.Url::to(['/blog/index']).'?tag='.$tag.'">#'.$tag.'</a>&nbsp;&nbsp;'; ?>
	</div>
    <?php } ?>
</div>