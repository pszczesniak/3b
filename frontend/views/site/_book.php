<?php
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\widgets\cms\MainWidgets;
use frontend\widgets\cms\GalleryWidget;

$this->title = $model->title;

$this->metaTags[] = Html::tag('meta', '', ['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->metaTags[] = Html::tag('meta', '', ['name' => 'description', 'content' => $model->meta_description]);
/* @var $this yii\web\View */
if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category['title'],  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' =>  $model->category['title_slug']] ];
}
$this->params['breadcrumbs'][] = $model->title;

?>

<!--<header class="l-header l-header--page">
    <h1 class="site-title">BLOG</h1>
</header>-->

<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$model->id."/medium.jpg")) ? "/uploads/pages/cover/".$model->id."/medium.jpg" : false; ?>

<?php $this->beginContent('@app/views/layouts/page-container.php', array('model' => $model, 'class' => 'page', 'title' => Html::encode($this->title), 'back' => false)) ?>
    <div class="product-name order-sm-1">
		<!--<h2 class="headline-14 text-upper color-gold">3b-artstudio</h2>-->
		<h1 class="headline-2 color-dark-gray"><?= $model->title ?></h1>
		<h3 class="headline-14 color-gold">3b-artstudio</h3>
	</div>
    <?= MainWidgets::widget(['id' => $model->id, 'position' => 2]) ?> 
    <?= $model->content; ?>
	<?php foreach($books as $key => $book) { ?>
		<?= $this->render('gallery/_book', ['book' => $book]) ?>
	<?php } ?>
    <?= MainWidgets::widget(['id' => $model->id, 'position' => 1]) ?> 
<?php $this->endContent(); ?>