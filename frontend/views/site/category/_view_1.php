<div class="category_view_1">
    <?php $iter = 1; ?>
    <?php foreach ($models as $pmodel) { ?>
        <?= $this->render('item/_view_'.$model->list_view, ['pmodel' => $pmodel, 'mod' => ($iter % 2)  ]) ?>
        <?php ++$iter; ?>
    <?php } ?>
</div>