<?php
    use yii\helpers\Url;
?>
<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$pmodel->id."/medium.jpg")) ? "/uploads/pages/cover/".$pmodel->id."/medium.jpg" : "/images/logo.png"; ?>

<div class="tile_1">
    <?= ($mod) ? '<img src="'.$img.'" alt="" class="img1">' : ''; ?>
    <div class="header_txt_section">
        <p class="txt1"><?= $pmodel->title ?></p>
        <p class="txt2"><?= $pmodel->short_content ?></p>
        <p class="txt3"><a href="<?= Url::to(['site/cpage', 'pid' => $pmodel->id, 'pslug' => $pmodel->title_slug, 'cslug' => $pmodel->category['title_slug']]) ?>" class="link1">czytaj więcej</a></p>
    </div>
    <?= (!$mod) ? '<img src="'.$img.'" alt="" class="img1">' : ''; ?>
</div>