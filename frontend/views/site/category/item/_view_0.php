<?php
    use yii\helpers\Url;
?>
<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$pmodel->id."/medium.jpg")) ? "/uploads/pages/cover/".$pmodel->id."/medium.jpg" : "/images/logo.png"; ?>

 <a href="<?= Url::to(['site/page', 'pid' => $pmodel->id, 'slug' => $pmodel->title_slug]) ?>" class="tile" style="background-image: url('<?= $img ?>');">
    <div class="textWrapper"><h2><?= $pmodel->title ?></h2>
		<div class="content"><?= $pmodel->short_content ?></div>
    </div>
</a>
  