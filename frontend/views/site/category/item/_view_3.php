<?php
    use yii\helpers\Url;
?>
<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$pmodel->id."/medium.jpg")) ? "/uploads/pages/cover/".$pmodel->id."/medium.jpg" : "/images/logo.png"; ?>

<div class="grid grid--0">
    <!-- <?= ($mod) ? '<div class="col-sm-6 category-img  col-xs-first col-sm-last" style="background-image: url('.$img.')"></div>' : ''; ?> -->
    <div class="col-sm-6 category-img  col-xs-first  <?= ($mod) ? 'col-sm-last':''; ?>" style="background-image: url('<?= $img; ?>')"></div>
    <div class="col-sm-6 col-xs-12 category-text">
        <h2><?= $pmodel->title ?></h2>
        <p class="txt1"><?= $pmodel->short_content ?></p>
        <p class="txt2"><a href="<?= Url::to(['site/cpage', 'pid' => $pmodel->id, 'pslug' => $pmodel->title_slug, 'cslug' => $pmodel->category['title_slug']]) ?>" class="link">czytaj więcej</a></p>
    </div>
    <!-- <?= (!$mod) ? '<div class="col-sm-6 category-img  col-xs-first col-sm-last" style="background-image: url('.$img.')"></div>' : ''; ?> -->
</div>