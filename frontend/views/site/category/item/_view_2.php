<?php
    use yii\helpers\Url;
?>
<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$pmodel->id."/medium.jpg")) ? "/uploads/pages/cover/".$pmodel->id."/medium.jpg" : "/images/default-box.jpg"; ?>

<div class="grid grid--0 grid--center no--pad">
    <div class="col-md-4 bg-image" style="background-image: url(<?= $img ?>)">
        <div class="bg-image-text">  </div>
    </div>
    <div class="col-md-8">
        <div class="block">
            <h2><?= $pmodel->title ?></h2>
            <p class="text-bold container--page"><?= $pmodel->short_content ?></p>
        </div>
        <?php
            if($pmodel->details_config) {
                $details = \yii\helpers\Json::decode($pmodel->details_config);
                $pmodel->map_latitude = (isset($details['latitude'])) ? $details['latitude'] : 50.811435;
                $pmodel->map_longitude = (isset($details['longitude'])) ? $details['longitude'] : 19.125092;
            }
        ?>
        <div class="js-map map" data-latitude="<?= $pmodel->map_latitude ?>" data-longitude="<?= $pmodel->map_longitude ?>"></div>

        <a class="link-page" href="<?= Url::to(['site/cpage', 'pid' => $pmodel->id, 'pslug' => $pmodel->title_slug, 'cslug' => $pmodel->category['title_slug']]) ?>">
            <span class="text-bold link"><?= $pmodel->title ?></span>
        </a>
    </div>
</div>
