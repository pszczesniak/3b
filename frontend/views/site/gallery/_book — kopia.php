
<div id="bcontainer" class="bcontainer">

	<div class="bb-custom-wrapper">
		<div id="bb-bookblock" class="bb-bookblock">
			<?php foreach($book->items as $key => $photo) { ?>
				<div class="bb-item">
				<div class="bcontent">
					<div class="bcontent-item" style="background-image: url(<?= Yii::getAlias('@imageurl') . '/gallery/'.$book->id.'/'.$photo->id.'/original.jpg'; ?>);">
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		
		<nav>
			<span id="bb-nav-prev">&larr;</span>
			<span id="bb-nav-next">&rarr;</span>
		</nav>

	</div>
		
</div><!-- /container -->