<?php
/* @var $this yii\web\View */
use frontend\widgets\cms\MainWidgets;
use frontend\widgets\Calculator;
use frontend\widgets\cms\GalleryWidget;

use yii\helpers\Html;
use yii\helpers\Url;

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/
$this->title = 'Galerie klientów';
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@app/views/layouts/page-container.php', array('header' => false, 'class' => 'page', 'title' => Html::encode($this->title))) ?>
    <div class="imageGrid">
    <?php foreach($galleries as $key => $item) {
        $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/ugallery/cover/".$item->id."/medium.jpg")) ? "/uploads/ugallery/cover/".$item->id."/medium.jpg" :  "/uploads/company/logo.png";
        echo '<div class="tile tile--hover" >'
                    .'<div class="tile__abs-img" style="background-image: url('.$img.');"></div>'
                    .'<h2 class="tile__title">'.$item->additional_info.'</h2>'
                    .'<div class="tile__abs-button">'
                    .'<a href="'.Url::to(['/client/gallery', 'gid' => $item->id, 'gslug' => preg_replace('~[^-\w]+~', '', $item->additional_info)]).'" class="btn btn--borderr btn--full btn--low-thin btn--solid-gold">Przejdź do galerii</a>'
                    .'</div>'
             .'</div>';
    } ?>
    </div>
          
<?php $this->endContent(); ?>

