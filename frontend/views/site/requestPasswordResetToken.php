<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;


$template = '<div class="form__row">{label}{input}</div>';

?>
<?php $this->beginContent('@app/views/layouts/client-container.php', array('header' => false, 'class' => 'login', 'title' => Html::encode($this->title), 'back' => false, 'logout' => false)) ?>
    
    
	<section class="block block--narrow block--middle order-sm-2">
		<h2 class="headline-11 color-gold text-center">Zmiana hasła</h2>

		<div class="login-grid login-grid--narrow">
			<div class="login-grid__top">
				<?= Alert::widget() ?>
				<?php $form = ActiveForm::begin(['method' => 'post', 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['autocomplete' => 'off', 'class' => 'form'] ]); ?>
					<p class="txt1">Please fill out your email. A link to reset password will be sent there.</p>
					<?= $form->field($model, 'email', ['template' => $template])
											 ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz email...', 'title' => 'Email', 'autocomplete' => 'off'])  ?>

					<?= Alert::widget() ?>
					<div class="form__row mt--5em">
						<button class="btn btn--border btn--full btn--low-thin btn--solid-gold"> Wyślij </button>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
			
			<div class="login-grid__middle">
				
			</div>
			
			<div class="login-grid__top">
				
			</div>
		</div>
		
	</section>
	
<?php $this->endContent(); ?>
