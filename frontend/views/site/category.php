<?php
	/* @var $this yii\web\View */
	use frontend\widgets\Mainwidgets;
	use yii\widgets\LinkPager;
	use yii\helpers\Url;
    use yii\helpers\Html;
    
    $this->params['breadcrumbs'][] = $model->title;
    
    $this->title = $model->title;
    //$this->metaTags[] = Html::tag('meta', '', ['title' => 'keywords', 'content' => $model->meta_title]);
    $this->metaTags[] = Html::tag('meta', '', ['name' => 'keywords', 'content' => $model->meta_keywords]);
    $this->metaTags[] = Html::tag('meta', '', ['name' => 'description', 'content' => $model->meta_description]);
?>

<?php $this->beginContent('@app/views/layouts/page-container.php', array('title' => $model->title, 'model' => $model, 'class' => 'page', 'title' => Html::encode($this->title), 'back' => false)) ?>
	<div class="category-list" >
        <?= $this->render('category/_view_'.$model->list_view, ['model' => $model, 'models' => $models, 'books' => $books]) ?>
    </div>
<?php $this->endContent(); ?>

