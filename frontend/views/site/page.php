<?php
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\widgets\cms\MainWidgets;
use frontend\widgets\cms\GalleryWidget;

$this->title = $model->title;

$this->metaTags[] = Html::tag('meta', '', ['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->metaTags[] = Html::tag('meta', '', ['name' => 'description', 'content' => $model->meta_description]);
/* @var $this yii\web\View */
if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category['title'],  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' =>  $model->category['title_slug']] ];
}
$this->params['breadcrumbs'][] = $model->title;

?>

<!--<header class="l-header l-header--page">
    <h1 class="site-title">BLOG</h1>
</header>-->

<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$model->id."/medium.jpg")) ? "/uploads/pages/cover/".$model->id."/medium.jpg" : false; ?>

<?php $this->beginContent('@app/views/layouts/page-container.php', array('title' => $model->title, 'model' => $model, 'class' => 'page', 'title' => Html::encode($this->title), 'back' => false)) ?>
	<?php if($img && $model->fk_category_id) { ?>
        <?php if($gallery) { ?>
            <br /><div class="page-leading-slider"><?php echo GalleryWidget::widget(['id' => $gallery->fk_gallery_id]); ?></div>
        <?php } /*else {*/ ?>
            <!--<div class="page-leading-image"><img src="<?= $img ?>" title="<?= $model->title ?>" /><div class="page-leading-image-overly"></div></div>-->
        <?php /*}*/ ?>
    <?php } ?>
    <?= MainWidgets::widget(['id' => $model->id, 'position' => 2]) ?> 
    <?= $model->content; ?>
    <?= MainWidgets::widget(['id' => $model->id, 'position' => 1]) ?> 
<?php $this->endContent(); ?>