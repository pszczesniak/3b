<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Wyszukiwarka';
/* @var $this yii\web\View */

$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/web-container.php',array('model' => false, 'class' => 'page', 'title'=>Html::encode($this->title), 'back' => false)) ?>

    <h4 class="search-results">Wyniki wyszukiwania frazy <i><?= isset($_GET['phrase']) ? Html::encode($_GET['phrase']) : ''; ?></i></h4>
    <ul class="search-listing">
        <?php foreach($results as $key => $item) { ?>
        <li>
            <h3><a href="<?= Url::to(['/site/page', 'pid' => $item['id'], 'slug' => $item['title_slug']]) ?>"><?= $item['title'] ?></a></h3>
            <a href="<?= Url::to(['/site/page', 'pid' => $item['id'], 'slug' => $item['title_slug']]) ?>" class="search-links"><?= Url::to(['/site/page', 'pid' => $item['id'], 'slug' => $item['title_slug']], true) ?></a>
            <p><?= mb_strimwidth($item['content'], 0, 200, ' ...') ?></p>
        </li>
        <?php }?>
    </ul>
            
    
<?php $this->endContent(); ?>