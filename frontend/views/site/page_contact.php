<?php
/* @var $this yii\web\View */
$this->title = $model->title;
use frontend\widgets\cms\MainWidgets;

use backend\Modules\Company\models\Company;

use yii\helpers\Html;
use yii\helpers\Url;

$company = Company::findOne(1);

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/
if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category['title'],  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' =>  $model->category['title_slug']] ];
}
$this->params['breadcrumbs'][] = $model->title;

?>
<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$model->id."/medium.jpg")) ? "/uploads/pages/cover/".$model->id."/medium.jpg" : false; ?>

<?php $this->beginContent('@app/views/layouts/page-container.php', array('class' => 'page', 'title' => Html::encode($this->title))) ?>

	<?= $model->content ?>
	<div class="grid">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<?=  \frontend\widgets\cms\ContactWidget::widget(['view' => 'widget']);/* MainWidgets::widget(['id' => $model->id]) */ ?>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12">
			<div class="bg-white-shadow" style="margin-left:50px;">
				<ul class="list-clean">  
					<li> 
						<h4 class="headline-4">E-mail</h4> 
						<span class="ico-label__icon"> <svg width="20px" height="20px" class="svg-fill-gold"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons.svg#icon-mail"></use> </svg> </span> 
						<span class="ico-label__txt"> <a href="mailto:<?= $company->email ?>"><?= $company->email ?></a> </span> 
					</li>   
					<li> 
						<h4 class="headline-4">Telefon</h4> 
						<?php $phones = explode('|', $company->phone); ?>
						<?php foreach($phones as $i => $phone) { if($i > 0) echo '<br />'; ?>
						<span class="ico-label__icon"> <svg width="20px" height="20px" class="svg-fill-gold"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons.svg#icon-phone"></use> </svg> </span> 
						<span class="ico-label__txt"> <a href="tel:<?= $phone ?>"><?= $phone ?></a> </span> 
						<?php } ?>
					</li>  
					<li> 
						<h4 class="headline-4">Adres</h4> 
						<span class="ico-label__icon"> <svg width="20px" height="20px" class="svg-fill-gold"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons.svg#icon-location"></use> </svg> </span> 
						<span class="ico-label__txt"> <a href="#"><?= nl2br($company->address) ?></a></span> 
					</li>
				</ul>
				<br />
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2949.3239535615216!2d19.033646315967907!3d50.73526327468219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4710c965bb9416f9%3A0x15aa58af530a850b!2sKijas+11%2C+42-274+Wygoda%2C+Polska!5e1!3m2!1spl!2sus!4v1523860630976" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	
       
<?php $this->endContent(); ?>



