<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Wysłanie wniosku';

$this->params['breadcrumbs'][] = ['label' => 'Twoje wnioski',  'url' => Url::to(['/client/account']) ];
$this->params['breadcrumbs'][] = '#'.$model->id;
?>
<?php
    $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
    $loanData = \yii\helpers\Json::decode($site->custom_data);
    $minLoan = $loanData['amountFrom']; $maxLoan = $loanData['amountTo']; $minPeriod = $loanData['periodFrom']; $maxPeriod = $loanData['periodTo'];
	
	$description['bank_name'] = 'Wpisz całą nazwę banku, w którym masz rachunek bankowy';
    $description['bank_account_number'] = 'To jest Twój unikalny 26-cyfrowy numer konta bankowego. Wpisz numer bez żadnych przerw.';
    $description['bank_how_long_use'] = '';
    $description['frequency_payment'] = '';
    $description['use_credit_card'] = '';
    $description['monthly_income'] = 'To wysokość Twojego całkowitego dochodu pozyskiwanego z różnych źródeł, po odliczeniu należnych podatków.';
    $description['monthly_expenses'] = 'To wysokość Twoich comiesięcznych wydatków uwzględniających np. wynajem mieszkania, zakup żywności czy rachunki za media (inne ewentualne pożyczki nie są tu brane pod uwagę).';
    $description['monthly_other_loans'] = 'To wysokość comiesięcznych obciążeń związanych ze spłatą innych pożyczek.';
    $description['loan_purpose'] = 'Wybierz jedną opcję z rozwijanej listy.';
    $description['loan_day_of_payment'] = 'Wybierz jedną opcję z rozwijanej listy.';

    $templateInput = '<div class="input-icon-wrap"><span class="input-icon"><span class="fa fa-%s"></span></span>{input}<span class="input-info"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" data-original-title="%s"></i></div>{error}{hint}';
?>
<?php $this->beginContent('@app/views/layouts/page-container.php',array('class'=>'page', 'title'=>Html::encode($this->title))) ?>
    <?php $form = ActiveForm::begin(['method' => 'post']); ?>
        <div class="grid">
            <div class="col-sm-6 col-xs-12">
                <fieldset><legend>Regulamin</legend>
                    <?php
                        if($accepts) { $items = []; 
                            foreach($accepts as $key => $accept) {
                            $items[$accept->id] = $accept->title.'|'.$accept->content;
                                /*echo '<div class="form__row">'
                                        .'<div class="form__row__left"></div>'
                                        .'<div class="form__row__right">'
                                            .$form->field($model, 'accepts_1[accept-'.$accept->id.']', ['template' => '{input}{label}{hint}{error}'])
                                                ->hint("Prosimy o wyrażenie zgody")
                                                ->checkbox(['maxlength' => true, 'class' => 'form__checkbox', 'label' => null])->label($accept->title, ['class' => 'form__checkbox-label'])
                                        .'</div>'
                                    .'</div>';*/
                            }
                            echo $form->field($model, 'accepts', ['template' => '{input}{error}'])
                                        //->hint("Prosimy o wyrażenie zgody")
                                        ->checkboxlist($items, 
                                                        ['item' => function ($index, $label, $name, $checked, $value) {
                                                                        $labelArr = explode('|', $label);
                                                                        return '<textarea class="form__textarea">'.$labelArr[1].'</textarea><br/>'.Html::checkbox($name, $checked, ['value' => $value, 'class' => 'form__checkbox', 'id' => ('accept-'.$value), 'label' => null]) . '<label for="'.('accept-'.$value).'" class="form__checkbox-label ' . $checked . '">' .$labelArr[0].'</label>' ;
                                                                    }, 
                                                        'separator' => '<br /><br/>']
                                                      //['itemOptions' => ['class' => 'form__checkbox', 'label' => null]]
                                           )
                                        ->label(false);
                                
                        }
                    ?>
                    
                </fieldset>
                
               
                <div class="form__row">
                    <div class="form__row__left"></div>
                    <div class="form__row__right text-center">
                        <input type="text" id="contact-mail" name="contact-mail">
                        <button title="Send message" type="submit" class="btn btn--teal">Wyślij wniosek</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="calculator">
                    <table class="info-tab">
                        <tr><td colspan="2" style="text-align:center; border-bottom: 1px solid #fff;"><i>Szczegóły wniosku gotowe do wysłania</i></td></tr>
                        <tr><td>Wysokość raty</td><th><span id="calc_rate">389,00</span> zł</th></tr>
                        <tr><td>Kwota pożyczki</td><th><span id="calc_amount"> <?= $model->l_amount; ?></span> zł</th></tr>
                        <tr><td>Okres kredytowania</td><th><span id="calc_deadline">17.06.2017 </span></th></tr>
                        <tr><td>Liczba rat</td><th><span id="calc_period"><?= $model->l_period; ?></span> m-ce</th></tr>
                        <tr><td>Opłaty</td><th><span id="calc_charges">189,00</span> zł</th></tr>
                        <tr><td>RRSO</td><th><span id="calc_rrso">137 </span> %</th></tr>
                        <tr><td colspan="2" style="text-align:center;border-bottom: 1px solid #fff;padding-top:20px;"><i>Dodatkowe informacje</i></td></tr>
                        <tr><td>Cel pożyczki</td><th><span id="calc_rrso"> <?= $model->purpose?> </span> </th></tr>
                        <tr><th colspan="2" style="text-align:center;">Płatność w <?= $model->l_day_of_payment ?> dzień miesiąca</th></tr>
                    </table>
                </div>
            </div>
        </div>
        
        
    <?php ActiveForm::end(); ?>
<?php $this->endContent(); ?>
