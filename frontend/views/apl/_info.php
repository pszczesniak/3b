<?php
    $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
    $loanData = \yii\helpers\Json::decode($site->custom_data);
    $minLoan = $loanData['amountFrom']; $maxLoan = $loanData['amountTo']; $minPeriod = $loanData['periodFrom']; $maxPeriod = $loanData['periodTo']; 
?>
<div class="calculator">
     <label>Jaką kwotę potrzebujesz?</label>
	<div class="calculator__range">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', true);">
            <i class="calculator__minus"></i>
        </button>
        <input class="calculator__range-input" type="range" min="<?= $minLoan ?>" max="<?= $maxLoan ?>" step="100" value="<?= $model->l_amount ?>" name="AplRequest[l_amount]" id="l_amount">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', false);">
            <i class="calculator__plus"></i>
        </button>
    </div>
	<label>Na jak długo?</label>
    <div class="calculator__range">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_period', true);">
            <i class="calculator__minus"></i>
        </button>
        <input class="calculator__range-input" type="range" min="<?= $minPeriod ?>" max="<?= $maxPeriod ?>" step="1" value="<?= $model->l_period ?>" name="AplRequest[l_period]" id="l_period">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_period', false);">
            <i class="calculator__plus"></i>
        </button>
    </div>
   
    <h5>Chcę pożyczyć <b><?= $model->l_amount ?> zł</b></h5>
    <table class="info-tab"> 
        <tr><td>Wysokość raty</td><th><span id="calc_rate"><?= $model->calculation['rate'] ?></span> zł</th></tr>
        <tr><td>Kwota pożyczki</td><th><span id="calc_amount"><?= $model->l_amount ?> </span> zł</th></tr>
        <tr><td>Okres kredytowania</td><th><span id="calc_deadline"><?= $model->calculation['deadline'] ?> </span></th></tr>
        <tr><td>Liczba rat</td><th><span id="calc_period"><?= $model->l_period ?></span> m-ce</th></tr>
        <tr><td>Opłaty</td><th><span id="calc_charges"><?= $model->calculation['charges'] ?></span> zł</th></tr>
        <tr><td>RRSO</td><th><span id="calc_rrso"><?= $model->calculation['l_rrso'] ?> </span> %</th></tr>
    </table>
</div>
<div class="apl-info">
    <?php $items = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 5])->all(); ?>
    <?php
        if($items) {
            echo '<ul>';
                foreach($items as $key => $item) {
                    echo '<li>'
                            .'<h3>'.$item->title.'</h3>'
                            .'<p>'.$item->content.'</p>'
                        .'</li>';
                }
            echo '</ul>';
        }
    ?>

</div>

<script type="text/javascript">
    var amountSlider = document.getElementById('l_amount');
    var periodSlider = document.getElementById('l_period');

    
    amountSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
    };

    periodSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
	
	function rangeChange(targetId, decrease) {
        var amountSlider = document.getElementById('l_amount');
        var periodSlider = document.getElementById('l_period');

        var targetEl = document.getElementById(targetId);
        var step = parseInt(targetEl.getAttribute('step'));
        var min = parseInt(targetEl.getAttribute('min'));
        var max = parseInt(targetEl.getAttribute('max'));

        if ( (targetEl.value >= max  && !decrease) ||(targetEl.value <= min && decrease) ) {
            return false;
        } else {
            targetEl.value = ( decrease ? parseInt(targetEl.value) - step : parseInt(targetEl.value) + step);
        }

        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);

        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
</script>