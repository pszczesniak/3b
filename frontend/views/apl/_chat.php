<?php
    use yii\helpers\Url;
?>
<div class="chat-box">
    <div class="align-right"><a href="<?= Url::to(['/client/note']) ?>?id=<?= $id ?>&type=<?= $type ?>" class="btn btn--teal viewModal" data-target="#modal-view" data-title="Nowa wiadomość" data-icon="envelope">Nowa wiadomość</a></div>
    <div class="chat-box-history">
        <?php
            if($model->notes) {
                foreach($model->notes as $key => $note) {
                    echo $this->render('_note', ['model' => $note]);
                }
            }
        ?>
    </div>
    <!--<div class="comment">
        <img src="/images/default-user.png" alt="" class="comment-avatar">
        <div class="comment-body">
            <div class="comment-text">
                <div class="comment-header">
                    <a href="#" title="">Małgorzata Gruca</a><span>2017-08-06 11:22:13</span>
                </div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.   
            </div>
            <div class="comment-footer">
                <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                <a href="#">Odpowiedz</a>
            </div>
        </div>

        <div class="comment">
            <img src="/images/default-user.png" alt="" class="comment-avatar">
            <div class="comment-body">
                <div class="comment-text">
                    <div class="comment-header">
                        <a href="#" title="">Piotr Szczęśniak</a><span>2017-08-06 11:22:13</span>
                    </div>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
                <div class="comment-footer">
                    <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                    <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                    <a href="#">Odpowiedz</a>
                </div>
            </div>
        </div>
    </div>
    <div class="comment">
        <img src="/images/default-user.png" alt="" class="comment-avatar">
        <div class="comment-body">
            <div class="comment-text">
                <div class="comment-header">
                    <a href="#" title="">Piotr Szczęśniak</a><span>2017-08-06 11:22:13</span>
                </div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </div>
            <div class="comment-footer">
                <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                <a href="#">Odpowiedz</a>
            </div>
        </div>
    </div>-->
    
    <!--<div class="m-t-30 text-center">
        <a href="" class="btn btn-default waves-effect waves-light btn-sm">Wczytaj więcej...</a>
    </div>-->
</div>