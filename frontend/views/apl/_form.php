<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $description['firstname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['lastname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['pesel'] = 'To Twój unikatowy, jedenastocyfrowy numer identyfikacyjny. Wpisz go bez spacji.';
    $description['evidence_number'] = 'To unikatowy, dziewięciocyfrowy numer Twojego dowodu osobistego. Wprowadź go bez spacji.';
    $description['marital'] = 'Wybierz jedną opcję z rozwijanej listy, która opisuje Twój stan cywilny.';
    $description['members'] = 'To osoby, z którymi mieszkasz i które są od Ciebie finansowo zależne (np. dzieci).';  
    $description['car'] = 'Wypełnij aby zwiększyć swoje szanse na pozytywną decyzję kredytową';   
    $description['car_type'] = 'Wpisz markę Swojego samochodu'; 
    $description['car_year'] = 'Wpisz rok produkcji Swojego samochodu'; 
    $description['car_insurance'] = 'Wpisz datę w której kończy się ubezpieczenie Twojego samochodu'; 
    $description['employment_status'] = 'Wybierz rodzaj zatrudnienia z rozwijanej listy. Pamiętaj, że nie udzielamy pożyczek osobom bezrobotnym ani pozbawionym źródeł regularnego dochodu.';
    $description['employment_period'] = 'Wybierz datę rozpoczęcia pracy.';
    $description['employer_name'] = 'Nazwy Twojego pracodawcy potrzebujemy do przeprowadzenia procesu udzielenia pożyczki.';
    $description['email'] = 'Podaj adres e-mail, do którego masz stały i łatwy dostęp. To będzie Twoja nazwa użytkownika, której nie będziesz mógł już zmienić.';
    $description['email_repeat'] = 'Wpisz ponownie swój adres email. Upewnij się, że jest taki sam, jak ten, który podałeś wcześniej.';
    $description['password'] = 'Wprowadź hasło, które stworzyłeś podczas rejestracji. Powinno ono zawierać co najmniej 8 znaków, w tym wielkie i małe litery, minimum jedną cyfrę oraz nie posiadać znaków specjalnych [np. !%$?] i polskich liter. Pamiętaj, że tę samą literę możesz użyć maksymalnie 3 razy z rzędu.';
    
    $templateInput = '<div class="form__row">{label}{input}{error}</div>';

?>

<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/apl/register', 'rid' => $model->term_id]), 'enableClientValidation' => true, 'options' => ['class' => 'widget-contact-form'] ]); ?>
    <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <div class="row row-g-20">
        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
            <fieldset><legend>Rejestracja konta</legend>
				<div class="row row-vertical-middle">
					<div class="col-sm-6"><?= $form->field($model, 'firstname', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Imię', ['class' => 'form__label form__label--thin']) ?></div>
					<div class="col-sm-6"><?= $form->field($model, 'lastname', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Nazwisko', ['class' => 'form__label form__label--thin']) ?></div>
				</div>
				
				<?= $form->field($model, 'phone', ['template' => $templateInput])
						 //->hint($description['email'])
						 ->input('phone', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Telefon'])->label('Telefon', ['class' => 'form__label form__label--thin']) ?>
						 
				<?= $form->field($model, 'email', ['template' => $templateInput])
						 //->hint($description['email'])
						 ->input('email', ['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Adres e-mail'])->label('E-mail', ['class' => 'form__label form__label--thin']) ?>
			   
				<?= $form->field($model, 'repeat_email', ['template' => $templateInput])
						 //->hint("Nieprawidłowe powtórzenie adresu e-mail")
						 ->textInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Powtórz adres e-mail']) ->label('Powtórz e-mail', ['class' => 'form__label form__label--thin'])?>
		   
				<?= $form->field($model, 'password', ['template' => $templateInput])
						//->hint($description['password'])
						->passwordInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Hasło'/*, 'pattern' => "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"*/])->label('Hasło', ['class' => 'form__label form__label--thin']) ?>

				<?= $form->field($model, 'repeat_password', ['template' => $templateInput])
                    // ->hint("Nieprawidłowe powtórzenie hasła")
                     ->passwordInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Powtórz hasło'])->label('Powtórz hasło', ['class' => 'form__label form__label--thin']) ?>
            
				<?php
					if($rules) {
						foreach($rules as $key => $rule) {
							echo '<div class="form__row">'
									.'<div class="form__row__left"></div>'
									.'<div class="form__row__right">'
										.'<textarea class="form__textarea">'.$rule->content.'</textarea>'
									.'</div>'
								.'</div>';
						}
					}
				?>
				<?php /*$form->field($model, 'rules', ['template' => '{input}{error}'])->hiddenInput()->label(false)*/ ?> 
				<?php
					if($accepts) { $items = [];
						foreach($accepts as $key => $accept) {
						$items[$accept->id] = $accept->title;
							/*echo '<div class="form__row">'
									.'<div class="form__row__left"></div>'
									.'<div class="form__row__right">'
										.$form->field($model, 'accepts_1[accept-'.$accept->id.']', ['template' => '{input}{label}{hint}{error}'])
											->hint("Prosimy o wyrażenie zgody")
											->checkbox(['maxlength' => true, 'class' => 'form__checkbox', 'label' => null])->label($accept->title, ['class' => 'form__checkbox-label'])
									.'</div>'
								.'</div>';*/
						}
						echo '<div class="form__row">'
								.'<div class="form__row__left"></div>'
								.'<div class="form__row__right">'
									.$form->field($model, 'accepts_1', ['template' => '{input}{error}'])
										//->hint("Prosimy o wyrażenie zgody")
										->checkboxlist($items, 
														['item' => function ($index, $label, $name, $checked, $value) {
																		return Html::checkbox($name, $checked, ['value' => $value, 'class' => 'form__checkbox', 'id' => ('accept-'.$value), 'label' => null]) . '<label for="'.('accept-'.$value).'" class="form__checkbox-label ' . $checked . '">' .$label.'</label>' ;
																	}, 'separator' => '<br />'
														]
													  //['itemOptions' => ['class' => 'form__checkbox', 'label' => null]]
										   )
										->label(false)//->label($accept->title, ['class' => 'form__checkbox-label'])
								.'</div>'
							.'</div>';
							
					}
				?>
			</fieldset>
        </div>
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
			<fieldset><legend>Zapytanie o termin</legend>
				
                <?= $form->field($model, 'term_date', ['template' => $templateInput])->textInput(['id' => 'datePicker', 'maxlength' => true, 'class' => 'form-control form__input'])->label('Data imprezy [YYYY-DD-MM]', ['class' => 'form__label form__label--thin']) ?>

				<div class="row row-vertical-middle">
					<div class="col-xs-4"><?= $form->field($model, 'place_postalcode', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('K. pocztowy', ['class' => 'form__label form__label--thin']) ?></div>
					<div class="col-xs-8"><?= $form->field($model, 'place_city', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Miejscowość', ['class' => 'form__label form__label--thin']) ?></div>
				</div>
				<?= $form->field($model, 'place_address', ['template' => $templateInput])->textInput(['maxlength' => true, 'class' => 'form-control form__input'])->label('Adres', ['class' => 'form__label form__label--thin']) ?>
			
				<?= $form->field($model, 'id_visit_type_fk', ['template' => $templateInput])->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Type::find()->where(['status' => 1, 'is_active' => 1])->orderby('type_name')->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'type-new select2Modal',] )->label('Typ imprezy', ['class' => 'form__label form__label--thin']) ?>
				<?= $form->field($model, 'id_package_fk', ['template' => $templateInput])->dropDownList(ArrayHelper::map(\backend\Modules\Svc\models\SvcPackage::find()->where(['status' => 1])->orderby('id')->all(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'type-new select2Modal',] )->label('Usługa', ['class' => 'form__label form__label--thin']) ?>
					
				<?= $form->field($model, 'term_note', ['template' => $templateInput])->textArea(['rows' => 3, 'maxlength' => true, 'class' => 'form-control form__textarea'])->label('Opis', ['class' => 'form__label form__label--thin']) ?>
			</fieldset>
		</div>
    </div>
	<div class="push text-center">
		<?= Html::submitButton('Wyślij', ['id' => 'send-email', 'class' => 'btn btn--white', 'name' => 'contact-button']) ?>
	</div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
  /*  var emailInput = document.getElementById('aplcustomer-email');
		emailInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	emailInput.onchange = validateEmail;
	var emailRepeatInput = document.getElementById('aplcustomer-repeat_email');
		emailRepeatInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	emailRepeatInput.onkeyup = validateEmail;
	
	function validateEmail(){
		if(emailInput.value != emailRepeatInput.value && emailInput.value != '') {
			//emailRepeatInput.setCustomValidity("Email Don't Match");
			emailRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
			emailRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            emailRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
			emailRepeatInput.classList.remove('validate-ok');
            emailRepeatInput.classList.add('validate-no');
		} else if(emailInput.value == emailRepeatInput.value) {
			//emailRepeatInput.setCustomValidity('');
			emailRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
			emailRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
            emailRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			emailRepeatInput.classList.add('validate-ok');
            emailRepeatInput.classList.remove('validate-no');
		} else {
			emailRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
			emailRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            emailRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			emailRepeatInput.classList.remove('validate-ok');
            emailRepeatInput.classList.remove('validate-no');
		}
	}
	
	var passInput = document.getElementById('aplcustomer-password');
		passInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	passInput.onchange = validatePassword;
	var passRepeatInput = document.getElementById('aplcustomer-repeat_password');
		passRepeatInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	passRepeatInput.onkeyup = validatePassword;
	
	function validatePassword(){
		$repeatChar = 1; $charTemp = ''; $strPass = passInput.value; $passValid = true;
		for (var i = 0, len = $strPass.length; i < len; i++) {
		    if($charTemp == $strPass[i] && $repeatChar < 4 ) { 
				$repeatChar = $repeatChar + 1;
			} else if ( $repeatChar != 4 ) {
				$repeatChar = 1;
			}
			$charTemp = $strPass[i];
		}

		if($repeatChar >= 4) { $passValid = false;
			passInput.setCustomValidity("Ten sam znak możesz użyć maksymalnie 3 razy z rzędu.");		
		} else if(passInput.value.length < 8) { $passValid = false;
			passInput.setCustomValidity("Hasło nie może mieć mniej niż 8 znaków.");
		} else if( !passInput.value.match(/[a-z]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną małą literę.");
		} else if( !passInput.value.match(/[A-Z]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną dużą literę.");
		} else if( !passInput.value.match(/[0-9]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną cyfrę.");
		} else {
		   passInput.setCustomValidity("");
		}
		
		if(!$passValid) {
			passInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
			passInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
			passInput.classList.remove('validate-ok');
            passInput.classList.add('validate-no');
		} else {
			passInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
			passInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
            passInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passInput.classList.add('validate-ok');
            passInput.classList.remove('validate-no');
		}

		if(passInput.value != passRepeatInput.value && passInput.value != '')  {
			//passRepeatInput.setCustomValidity("Passwords Don't Match");
			passRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
			passRepeatInput.classList.remove('validate-ok');
            passRepeatInput.classList.add('validate-no');
		} else if(passInput.value == passInput.value) {
			//passRepeatInput.setCustomValidity('');
			passRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passRepeatInput.classList.add('validate-ok');
            passRepeatInput.classList.remove('validate-no');
		} else {
			passRepeatInput.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passRepeatInput.classList.remove('validate-ok');
            passRepeatInput.classList.remove('validate-no');
		}
	}
	
	/*var input = document.getElementById('aplcustomer-firstname'); input.oninvalid = function(event) { event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john'); }*/
    
    var selectLists = document.querySelectorAll('select.form-control'); 
    for (var s = 0; s < selectLists.length; s++) {
        selectLists[s].onchange = function(event) {
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            } else {
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.classList.add('validate-ok');
            }
        }
    }
    
    var inputLists = document.querySelectorAll('input.form-control');
    for (var i = 0; i < inputLists.length; i++) {
        /*inputLists[i].addEventListener('input', function () {
            console.log(this.value);
        });*/
        inputLists[i].addEventListener('change', function(event) {
            if (event.target.validity.valid) {
                event.target.classList.add('validate-ok');
				event.target.classList.remove('validate-no');
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
                event.target.parentElement.parentElement.querySelector('div.help-block').style.display = "none";
            } else {
                //Field contains invalid data.
                event.target.classList.remove('validate-ok');
				event.target.classList.add('validate-no');
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            }
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            }
        }, false);
    }
</script>

<script type="text/javascript">
   /* document.getElementById('aplcustomer-own_car').onchange = function(event) {
        if(event.target.value == 0) document.getElementById('car-info').classList.add('no-display');
        if(event.target.value == 1) document.getElementById('car-info').classList.remove('no-display');
    }*/
 </script>