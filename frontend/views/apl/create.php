<?php
/* @var $this yii\web\View */
$this->title = 'Nowy wniosek';
use frontend\widgets\cms\MainWidgets;
use common\widgets\Alert;

use yii\helpers\Html;
use yii\helpers\Url;

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/

$this->params['breadcrumbs'][] = 'Nowy wniosek';

?>

<?php $this->beginContent('@app/views/layouts/page-container.php',array('class'=>'page', 'title'=>Html::encode($this->title), 'back' => true)) ?>
    <?= Alert::widget() ?>
    <div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
		<i class="fa fa-exclamation-triangle fa-5x pull-left"></i> <br ><strong>Uwaga:</strong> Zanim złożysz nowy wniosek <a href="<?= Url::to(['/client/update']) ?>?id=<?= $model->id_customer_fk ?>" class="btn btn--cream viewModal" data-target="#modal-view" data-title="Aktualizacja konta" data-icon="user">sprawdź tutaj</a> czy wszystkie Twoje dane są aktualne.
    </div>
    <?= $this->render('_formCreate', ['model' => $model]) ?>
    
<?php $this->endContent(); ?>



