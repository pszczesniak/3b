<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
    $loanData = \yii\helpers\Json::decode($site->custom_data);
    $minLoan = $loanData['amountFrom']; $maxLoan = $loanData['amountTo']; $minPeriod = $loanData['periodFrom']; $maxPeriod = $loanData['periodTo'];
	
	$description['bank_name'] = 'Wpisz całą nazwę banku, w którym masz rachunek bankowy';
    $description['bank_account_number'] = 'To jest Twój unikalny 26-cyfrowy numer konta bankowego. Wpisz numer bez żadnych przerw.';
    $description['bank_how_long_use'] = '';
    $description['frequency_payment'] = '';
    $description['use_credit_card'] = '';
    $description['monthly_income'] = 'To wysokość Twojego całkowitego dochodu pozyskiwanego z różnych źródeł, po odliczeniu należnych podatków.';
    $description['monthly_expenses'] = 'To wysokość Twoich comiesięcznych wydatków uwzględniających np. wynajem mieszkania, zakup żywności czy rachunki za media (inne ewentualne pożyczki nie są tu brane pod uwagę).';
    $description['monthly_other_loans'] = 'To wysokość comiesięcznych obciążeń związanych ze spłatą innych pożyczek.';
    $description['loan_purpose'] = 'Wybierz jedną opcję z rozwijanej listy.';
    $description['loan_day_of_payment'] = 'Wybierz jedną opcję z rozwijanej listy.';

    $templateInput = '<div class="input-icon-wrap"><span class="input-icon"><span class="fa fa-%s"></span></span>{input}<span class="input-info"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" data-original-title="%s"></i></div>{error}{hint}';
?>
<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/apl/create', 'rid' => $model->id_request_fk])]); ?>
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
            <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
            <fieldset><legend>Twoja pożyczka</legend>
                <label>Jaką kwotę potrzebujesz?</label>
				<div class="calculator__range">
					<button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', true);">
						<i class="calculator__minus"></i>
					</button>
					<input class="calculator__range-input" type="range" min="<?= $minLoan ?>" max="<?= $maxLoan ?>" step="100" value="<?= $model->l_amount ?>" name="AplLoan[l_amount]" id="l_amount">
					<button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', false);">
						<i class="calculator__plus"></i>
					</button>
				</div>
				<label>Na jak długo?</label>
				<div class="calculator__range">
					<button class="calculator__range-button" type="button" onclick="rangeChange('l_period', true);">
						<i class="calculator__minus"></i>
					</button>
					<input class="calculator__range-input" type="range" min="<?= $minPeriod ?>" max="<?= $maxPeriod ?>" step="1" value="<?= $model->l_period ?>" name="AplLoan[l_period]" id="l_period">
					<button class="calculator__range-button" type="button" onclick="rangeChange('l_period', false);">
						<i class="calculator__plus"></i>
					</button>
				</div>
                 
                 <div class="form__row">
                    <div class="form__row__left">
                        <label for="l_purpose" class="form__label">Cel pożyczki:</label>
                    </div>
                    <div class="form__row__right">
                        <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                            <?= $form->field($model, 'id_purpose_fk', ['template' => sprintf($templateInput, 'tag', $description['loan_purpose']),  'options' => ['class' => '']])->dropdownList(\backend\Modules\Apl\models\AplLoan::listPurposes(), ['class' => 'form__input', 'prompt' => '-- wybierz --']); ?>
                    </div>
                </div>
                <div class="form__row">
                    <div class="form__row__left">
                        <label for="l_day_of_payment" class="form__label">Dzień spłaty:</label>
                    </div>
                    <div class="form__row__right">
                        <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                            <?= $form->field($model, 'l_day_of_payment', ['template' => sprintf($templateInput, 'calendar', $description['loan_day_of_payment']),  'options' => ['class' => '']])->dropdownList(\backend\Modules\Apl\models\AplLoan::listDayPayments(), ['class' => 'form__input'/*, 'prompt' => '-- wybierz --'*/]); ?>
                    </div>
                </div>
                
            </fieldset>
            
           
            <div class="form__row">
                <div class="form__row__left"></div>
                <div class="form__row__right text-center">
                    <input type="text" id="contact-mail" name="contact-mail">
                    <button title="Send message" type="submit" class="btn btn--teal"> <?= ( ($model->isNewRecord) ? 'Złóż wniosek' : 'Zapisz zmiany') ?></button>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="calculator">
                <table class="info-tab">
                    <tr><td colspan="2" style="text-align:center; border-bottom: 1px solid #fff;"><i>Szczegóły wniosku gotowe do wysłania</i></td></tr>
                    <tr><td>Wysokość raty</td><th><span id="calc_rate"><?= number_format($model->l_monthly_rate, 2, '.', ' '); ?></span> zł</th></tr>
                    <tr><td>Kwota pożyczki</td><th><span id="calc_amount"> <?= number_format($model->l_amount, 2, '.', ' '); ?></span> zł</th></tr>
                    <tr><td>Okres kredytowania</td><th><span id="calc_deadline"><?= date('d.m.Y', strtotime($model->repayment_term)); ?> </span></th></tr>
                    <tr><td>Liczba rat</td><th><span id="calc_period"><?= $model->l_period; ?></span> m-ce</th></tr>
                    <tr><td>Opłaty</td><th><span id="calc_charges"><?= number_format($model->l_provision_amount, 2, '.', ' '); ?></span> zł</th></tr>
                    <tr><td>RRSO</td><th><span id="calc_rrso"><?= $model->l_rrso; ?> </span> %</th></tr>
                    <tr><td colspan="2" style="text-align:center;border-bottom: 1px solid #fff;padding-top:20px;"><i>Dodatkowe informacje</i></td></tr>
                    <tr><td>Cel pożyczki</td><th><span id="calc_rrso"> <?= $model->purpose?> </span> </th></tr>
                    <tr><th colspan="2" style="text-align:center;">Płatność w <?= $model->l_day_of_payment ?> dzień miesiąca</th></tr>
                </table>
            </div>
        </div>
    </div>
    
    
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var amountSlider = document.getElementById('l_amount');
    var periodSlider = document.getElementById('l_period');

    
    amountSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
    };

    periodSlider.onchange = function () {
        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };

    function rangeChange(targetId, decrease) {
        var amountSlider = document.getElementById('l_amount');
        var periodSlider = document.getElementById('l_period');

        var targetEl = document.getElementById(targetId);
        var step = parseInt(targetEl.getAttribute('step'));
        var min = parseInt(targetEl.getAttribute('min'));
        var max = parseInt(targetEl.getAttribute('max'));

        if ( (targetEl.value >= max  && !decrease) ||(targetEl.value <= min && decrease) ) {
            return false;
        } else {
            targetEl.value = ( decrease ? parseInt(targetEl.value) - step : parseInt(targetEl.value) + step);
        }

        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);

        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
</script>