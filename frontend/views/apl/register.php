<?php
/* @var $this yii\web\View */
$this->title = 'Formularz rejestracyjny';
use frontend\widgets\cms\MainWidgets;

use yii\helpers\Html;

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/

$this->params['breadcrumbs'][] = 'Złożenie wniosku';

?>

<?php $this->beginContent('@app/views/layouts/page-container.php',array('class'=>'page', 'title'=>Html::encode($this->title))) ?>  
    <div class="grid">
        <div class="col-md-9 col-sm-7 col-xs-12">
            <div class="alert alert-success">Pierwszy krok został wykonany. Prosimy o sprawdzenie skrzynki pocztowej i postępowanie wg instrukcji w e-mailu.</div>
        </div>
        <div class="col-md-3 col-sm-5 col-xs-12"></div>
    </div>
<?php $this->endContent(); ?>



