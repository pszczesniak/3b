<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $description['phone_mobile'] = 'Nr telefonu komórkowego w formacie XXXXXXXXX (9 cyfr)';
    $description['phone'] = 'Nr telefonu domowego w formacie (XX) XXXXXXX (9 cyfr)';
    $description['address_postalcode'] = 'Kod pocztowy w formacie XX-XXX (5 cyfr).';
    $description['address_city'] = 'Wpisz miejscowość zamieszkania';
    $description['address_street'] = 'Wpisz nazwę ulicy, min. 3 znaki';
    $description['address_street_number'] = 'Wpisz numer Twojego domu';
    $description['address_local_number'] = 'Jeżeli Twój adres nie zawiera numeru mieszkania, pozostaw to pole puste.';
    $description['address_status'] = 'Wybierz rodzaj nieruchomości z rozwijanej listy.';

    $templateInput = '<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="left" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';
?>
<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/apl/contact'])]); ?>
    <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <fieldset><legend>Numer telefonu</legend>
        <div class="form__row">
            <div class="form__row__left">
                <label for="phone_mobile" class="form__label">Komórka:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'phone_mobile', ['template' => sprintf($templateInput, 'mobile', $description['phone_mobile'])])
                         ->hint($description['phone_mobile'])
                         ->textInput(['minlength' => 9, 'maxlength' => 9, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Telefon komórkowy', 'pattern' => "[0-9]{9}"]) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="phone" class="form__label">Stacjonarny:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'phone', ['template' => sprintf($templateInput, 'phone', $description['phone'])])
                         ->hint($description['phone'])
                         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Telefon stacjonarny', 'pattern' => "[(][0-9]{2}[)][0-9]{7}"]) ?>
            </div>
        </div>

    </fieldset>
    
    <fieldset><legend>Adres zameldowania</legend>
         <div class="form__row">
            <div class="form__row__left">
                <label for="address_postalcode" class="form__label">Kod pocztowy:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'address_postalcode', ['template' => sprintf($templateInput, 'home', $description['address_postalcode'])])
                         ->hint($description['address_postalcode'])
                         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Kod pocztowy', 'pattern' => "[0-9]{2}-[0-9]{3}"]) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="address_city" class="form__label">Miejscowość:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'address_city', ['template' => sprintf($templateInput, 'home', $description['address_city'])])
                         ->hint($description['address_city'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Miejscowość']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="address_street" class="form__label">Ulica:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'address_street', ['template' => sprintf($templateInput, 'home', $description['address_street'])])
                         ->hint($description['address_street'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Ulica']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="address_street_number" class="form__label">Nr domu:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'address_street_number', ['template' => sprintf($templateInput, 'home', $description['address_street_number'])])
                         ->hint($description['address_street_number'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Nr domu']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="address_local_number" class="form__label">Nr mieszkania:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'address_local_number', ['template' => sprintf($templateInput, 'home', $description['address_local_number'])])
                        ->hint($description['address_local_number'])
                        ->textInput(['minlength' => 1, 'maxlength' => 50, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Nr mieszkania']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="address_status" class="form__label">Status nieruchomości:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                    <?= $form->field($model, 'address_status', ['template' => sprintf($templateInput, 'home', $description['address_status']),  'options' => ['class' => '']])
                             ->hint($description['address_status'])
                             ->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusAddress(), ['class' => 'form__input form-control', 'prompt' => '-- wybierz --']); ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="own_car" class="form__label">Osobny adres korespondencyjny:</label>
            </div>
            <div class="form__row__right switch-field">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?php /*$form->field($model, 'own_car')->radioList([1 => 'TAK', 0 => 'NIE'])->label(false);*/ ?>  
                <?= $form->field($model, 'c_address')->radioList([1 => 'TAK', 0 => 'NIE'], 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ])->label(false) ?>
            </div>
        </div>
        
    </fieldset>
    
    <fieldset id="correspondence-info" class="correspondence-info <?= ($model->c_address == 1) ? '' : 'no-display'?>"><legend>Adres korespondencyjny</legend>
         <div class="form__row">
            <div class="form__row__left">
                <label for="c_address_postalcode" class="form__label">Kod pocztowy:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'c_address_postalcode', ['template' => sprintf($templateInput, 'home', $description['address_postalcode'])])
                         ->hint($description['address_postalcode'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Kod pocztowy']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="c_address_city" class="form__label">Miejscowość:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'c_address_city', ['template' => sprintf($templateInput, 'home', $description['address_city'])])
                         ->hint($description['address_city'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Miejscowość']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="c_address_street" class="form__label">Ulica:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'c_address_street', ['template' => sprintf($templateInput, 'home', $description['address_street'])])
                         ->hint($description['address_street'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Ulica']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="c_address_street_number" class="form__label">Nr domu:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'c_address_street_number', ['template' => sprintf($templateInput, 'home', $description['address_street_number'])])
                         ->hint($description['address_street_number'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Nr domu']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="c_address_local_number" class="form__label">Nr mieszkania:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'c_address_local_number', ['template' => sprintf($templateInput, 'home', $description['address_local_number'])])
                         ->hint($description['address_local_number'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Nr mieszkania']) ?>
            </div>
        </div>
  
    </fieldset>
    
   	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right text-center">
			<input type="text" id="contact-mail" name="contact-mail">
			<button title="Send message" type="submit" class="btn btn--teal">Przejdź do danych finansowych</button>
		</div>
	</div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    document.getElementById('aplcustomer-c_address').onchange = function(event) {
        if(event.target.value == 0) document.getElementById('correspondence-info').classList.add('no-display');
        if(event.target.value == 1) document.getElementById('correspondence-info').classList.remove('no-display');
    }
    
    var selectLists = document.querySelectorAll('select.form-control'); 
    for (var s = 0; s < selectLists.length; s++) {
        selectLists[s].onchange = function(event) {
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            } else {
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.classList.add('validate-ok');
            }
        }
    }
    
    var inputLists = document.querySelectorAll('input.form-control');
    for (var i = 0; i < inputLists.length; i++) {
        /*inputLists[i].addEventListener('input', function () {
            console.log(this.value);
        });*/
        inputLists[i].addEventListener('change', function(event) {
            if (event.target.validity.valid) {
                event.target.classList.add('validate-ok');
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
                event.target.parentElement.parentElement.querySelector('div.help-block').style.display = "none";
            } else {
                //Field contains invalid data.
                event.target.classList.remove('validate-ok');
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            }
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            }
        }, false);
    }
</script>