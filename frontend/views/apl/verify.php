<?php
/* @var $this yii\web\View */
$this->title = 'Formularz rejestracyjny';
use frontend\widgets\cms\MainWidgets;

use yii\helpers\Html;

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/

$this->params['breadcrumbs'][] = 'Złożenie wniosku';

?>
 <?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class' => 'homepage', 'title'=>Html::encode($this->title), 'back' => false)) ?> 
    <?= $this->render('_verify', ['model' => $model,]) ?>
 <?php $this->endContent(); ?>



