<?php
/* @var $this yii\web\View */
$this->title = 'Szczegóły wniosku';
use frontend\widgets\cms\MainWidgets;
use common\widgets\Alert;

use yii\helpers\Html;
use yii\helpers\Url;

/*if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category->title,  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' => $model->category->title_slug] ];
}*/

$this->params['breadcrumbs'][] = ['label' => 'Twoje wnioski',  'url' => Url::to(['/client/account']) ];
$this->params['breadcrumbs'][] = '#'.$model->id;

?>

<?php $this->beginContent('@app/views/layouts/page-container.php',array('class'=>'page', 'title'=>Html::encode($this->title), 'back' => true)) ?>

    <?= Alert::widget() ?>

    <?php if($model->status == 0) { ?>
        <?= $this->render('_formCreate', ['model' => $model]) ?>
        <div class="form__row">
            <div class="form__row__left"></div>
            <div class="form__row__right text-center">
                <a href="<?= Url::to(['/apl/send', 'lid' => $model->id]) ?>" title="Wyślij wniosek" class="btn btn--green btn--full-width">Wyślij wniosek</a><br />
                <a href="<?= Url::to(['/apl/discard', 'lid' => $model->id]) ?>" title="Odrzuć wniosek" class="btn btn--red btn--full-width">Odrzuć wniosek</a><br />
            </div>
        </div>
    <?php } else { ?>
        <?= $this->render('_details', ['model' => $model]) ?>
    <?php } ?>
    
<?php $this->endContent(); ?>



