<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    use frontend\widgets\files\FilesBlock;
    
    $status = \backend\Modules\Apl\models\AplLoan::statusList();
?>

<div class="grid">
    <div class="col-md-5 col-sm-5 col-xs-12">
        <div class="card-profile card-profile__<?= $status['colors_f'][$model->status] ?>">
            <div class="profile-header">
                <h4><?= $status['names'][$model->status] ?></h4>
                <span class="profile-header__avatar"><i class="fa fa-<?= $status['icons'][$model->status] ?>"></i></span>
            </div>
            <div class="profile-body">
                <!--<div class="card-block">
                    <?= Html::a('<i class="fa fa-user"></i>Edytuj', Url::to(['/apl/aplcustomer/update', 'id' => $model->id]), ['class' => 'btn btn-icon btn-primary']) ?>
                    <?= Html::a('<i class="fa fa-lock"></i>'.'Zablokuj', [Url::to(['/apl/aplcustomer/delete', 'id' => $model->id]), 'id' => $model->id], ['class' => 'btn btn-icon btn-danger']) ?>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.  </p>-->
                <ul class="profile-contact">
                    <li>
                        <div class="media small-teaser">
                            <div class="media-left media-middle">
                                <div class="teaser_icon round">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                            <div class="media-body media-middle">
                                <strong>Kwota kredytu: </strong><?= $model->l_amount ?>
                            </div>
                        </div>
                        <div class="media small-teaser">
                            <div class="media-left media-middle">
                                <div class="teaser_icon round">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            <div class="media-body media-middle">
                                <strong>Liczba rat: </strong><?= $model->l_period ?>
                            </div>
                        </div>
                        <div class="media small-teaser">
                            <div class="media-left media-middle">
                                <div class="teaser_icon round">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                            <div class="media-body media-middle">
                                <strong>Rata: </strong><?= $model->l_monthly_rate ?>
                            </div>
                        </div>
                        <div class="media small-teaser">
                            <div class="media-left media-middle">
                                <div class="teaser_icon round">
                                    <i class="fa fa-percent"></i>
                                </div>
                            </div>
                            <div class="media-body media-middle">
                                <strong>Oprocentowanie: </strong><?= $model->l_interest ?>
                            </div>
                        </div>
                        <div class="media small-teaser">
                            <div class="media-left media-middle">
                                <div class="teaser_icon round">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                            <div class="media-body media-middle">
                                <strong>Koszty: </strong><?= $model->l_provision_amount ?>
                            </div>
                        </div>
                    </li>
                    
                </ul>
                <!--<a href="" class="btn btn-primary btn-sm btn-block m-t-10">Follow</a>-->
            </div>
        </div>
        <!--<div class="panel panel-default">
            <div class="panel-heading"> Dokumenty
                <div class="pull-right"><a class="collapse-link collapse-window" href="#details-docs" data-perform="panel-collapse" data-toggle="collapse"><i class="fa fa-window-minimize"></i></a> </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true" id="details-docs">
                <div class="panel-body">
                </div>
                <div class="panel-footer align-right"> <small>Liczba dokumentów: <b><?= count( $model->files )  ?></b></small> </div>
            </div>
        </div>-->
    </div>
    <div class="col-md-7 col-sm-7 col-xs-12">
        <div class="tab-block">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Wiadomości</a></li>
                <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Dokumenty</a></li>
            </ul>

              <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab1">
                    <?= $this->render('_chat', ['model' => $model, 'id' => $model->id, 'type' => 2]) ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab2">
                    <?= FilesBlock::widget(['files' => $model->cfiles, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => false, 'showType' => false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(isset($_GET['info'])) { ?>
<script type="text/javascript">
document.onreadystatechange = function(){
    if (document.readyState === 'complete') {
        $("#modal-view").find(".modal-title").html('<i class="fa fa-envelope"></i>Wiadomość');
	    $("#modal-view").modal("show")
				  .find(".modalContent")
				  .load("<?= \yii\helpers\Url::to(['/client/note']) ?>?parent=<?= $_GET['info'] ?>");
    }
};
</script>
<?php } ?>
