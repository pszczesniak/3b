<form action="<?= $action ?>" method="post">
    <p>
        <label for="loan-value" class="form__label">Ile chcesz pożyczyć? <span class="form__asterisk">*</span></label>
        <div class="flex flex--nowrap">
            <input type="range" min="100" max="10000" step="100" value="2000" name="loan-value" id="l_amount" data-value-target-class="loan-value-amount" required >
            <input type="text" min="100" max="10000" step="100" value="2000" name="AplRequest[l_amount]" id="l_amount_text" class="ml--10em form__input form__input--sm loan-value-amount" required onchange="rangeChange('l_amount', true);">
        </div>
    </p>

    <p>
        <label for="loan-range" class="form__label">Na jaki okres chcesz pożyczyć? <span class="form__asterisk">*</span></label>
        <div class="flex flex--nowrap">
            <input type="range" min="6" max="60" step="1" value="6" name="loan-range" id="l_period" data-value-target-class="loan-range-month" required >
            <input type="text" min="6" max="60" step="1" value="6" name="AplRequest[l_period]" id="l_period_text" class="ml--10em form__input form__input--sm loan-range-month"  id="l_period" required onchange="rangeChange('l_period', true);">
        </div>
    </p>
    <button type="submit" class="btn">Wyślij zgłoszenie</button>
</form>
<script type="text/javascript">
    var amountSlider = document.getElementById('l_amount');
    var periodSlider = document.getElementById('l_period');

    
    amountSlider.onchange = function () {
        var amountVal = parseInt(amountSlider.value); document.getElementById('l_amount_text').value = amountVal;
        var periodVal = parseInt(periodSlider.value); document.getElementById('l_period_text').value = periodVal;
    };

    periodSlider.onchange = function () {
        var amountVal = parseInt(amountSlider.value); document.getElementById('l_amount_text').value = amountVal;
        var periodVal = parseInt(periodSlider.value); document.getElementById('l_period_text').value = periodVal;
    };

    function rangeChange(targetId, decrease) {
        
        var amountSlider = document.getElementById('l_amount');
        var periodSlider = document.getElementById('l_period');

        var targetEl = document.getElementById(targetId);
        var step = parseInt(targetEl.getAttribute('step'));
        var min = parseInt(targetEl.getAttribute('min'));
        var max = parseInt(targetEl.getAttribute('max'));
        
        if ( (targetEl.value >= max  && !decrease) ||(targetEl.value <= min && decrease) ) {
            return false;
        } else {
            /*targetEl.value = ( decrease ? parseInt(targetEl.value) - step : parseInt(targetEl.value) + step);*/
            targetEl.value = this.value;
        }

        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);
    };
</script>