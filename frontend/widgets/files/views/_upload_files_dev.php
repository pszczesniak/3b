<?php

use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if(!$view) { ?>
<div class="file-upload-alert file-upload-alert-<?= $type ?>"></div>
<div class="upload-container upload-<?= $type ?>">
    <div class="progress-wrp none"><!--<div class="progress-bar"></div ><div class="status">0%</div>--></div>
    
	<div class="input-group <?= ($showType) ? 'docs-group' : ''?>">
		<span class="input-group-btn">
			<span class="btn bg-purple btn-file">
				<span class="fa fa-hand-pointer-o"></span>
				<input type="file" multiple="" data-type="<?= $type ?>" class="ufile">
			</span>
		</span>
        <?php if($showType) { ?>
		<span class="input-group-addon">
			<input type="checkbox" title="Pokaż klientowi" class="ufile-show-<?= $type ?>" name="ufile-show-<?= $type ?>">
		</span>
        <?php } ?>
		<input type="text" class="form-control ufile-name ufile-name-<?= $type ?>">
        <?php if($showType) { ?>
		<select id="lunch" class="selectpicker form-control ufile-type ufile-type-<?= $type ?> docs-type" data-live-search="true" title="Wybierz kategorię">
			<option value="0">- wybierz -</option>
            <?php
				foreach($types as $key => $item) {
					echo '<option '. ( ($model->id_dict_type_file_fk == $item->id) ? ' selected ' : '') .' value="'.$item->id.'">'.$item->name.'</option>';
				}
			?>
		</select> 
		<span class="input-group-addon bg-green">
			<?= Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/1' , 
				['class' => 'insertInline text--white', 
				 'data-target' => "#docs-type-insert", 
				 'data-input' => ".docs-type"
				]) ?>
		</span>
        <?php } ?>
		<span class="input-group-btn">
			<button class="btn bg-teal btn-file-send" data-table="#table-files-<?= $type ?>" data-action="<?= Url::to(['/files/upload']) ?>" data-type="<?= $type ?>" data-id="<?= $id ?>" data-original-title="Upload" title="Pobierz"><i class="fa fa-cloud-upload" title="Pobierz"></i></button>		
		</span>
    </div>
    <div id="docs-type-insert" class="insert-inline bg-purple2 none"> </div>
</div>

<?php } ?>	
