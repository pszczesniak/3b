<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Apl\models\AplRequest;

class Calculator extends Widget{
	public $id;
	public $viewType;
    public $request = false;
    public $action;
    public $model = false;
    public $calc;
    
    private $config = ['minLoan' => 0, 'maxLoan' => 0, 'minPeriod' => 0, 'maxPeriod' => 0, 'note' => false];
	
	public function init(){
		parent::init();
		
       /* $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
        $loanData = \yii\helpers\Json::decode($site->custom_data);
        $this->config['minLoan'] = $loanData['amountFrom']; 
        $this->config['maxLoan'] = $loanData['amountTo']; 
        $this->config['minPeriod'] = $loanData['periodFrom']; 
        $this->config['maxPeriod'] = $loanData['periodTo']; 
        $this->config['defaultAmount'] = $loanData['defaultAmount'];
        $this->config['defaultPeriod'] = $loanData['defaultPeriod'];
        $this->config['note'] = (isset($loanData['loanNote'])) ? $loanData['loanNote'] : false;
        
        $this->calc = \frontend\controllers\AplController::calculation($loanData['defaultAmount'], $loanData['defaultPeriod']);*/
		//$this->category = CmsCategory::findOne($this->id);
	}
	
	public function run(){
		return $this->render('calculator', [
            'request' => $this->request, 'model' => $this->model, 'action' => $this->action, 'config' => $this->config, 'view' => $this->viewType, 'calc' => $this->calc
        ]);
	}
}
?>
