<?php
namespace frontend\widgets\blog;

use yii\base\Widget;

class Search extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('search');
    }
}