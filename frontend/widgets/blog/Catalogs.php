<?php
namespace frontend\widgets\blog;

use backend\Modules\Blog\models\BlogCatalog;
use backend\Modules\Blog\models\Status;
use yii\base\Widget;
use yii\helpers\Html;

class Catalogs extends Widget
{
    public $title;
    public $listClass = 'list-circle';

    public function init()
    {
        parent::init();

        if ($this->title === null) {
            $this->title = 'title';
        }
    }

    public function run()
    {
        $catalogs = BlogCatalog::find()->where(['status' => Status::STATUS_ACTIVE, 'is_nav' => 1])->orderBy(['title' => SORT_ASC])->all();

        return $this->render('catalogs', [
            'title' => $this->title,
            'catalogs' => $catalogs,
            'listClass' => $this->listClass
        ]);
    }
}