<?php
use yii\helpers\Html;
?>
<?php if($title) { ?>
<div class="portlet-blog">
    <div class="portlet-blog-decoration">
        <div class="portlet-blog-title"><?= $title ?></div>
    </div>
    <div class="portlet-blog-content">
<?php } ?>
        <ul>
            <?php foreach($comments as $comment) { ?>
                <li><strong><?php echo $comment->authorLink; ?></strong> 
                    <?php echo Html::a(Html::encode($comment->post->title), $comment->getUrl()); ?>
                </li>
            <?php } ?>
            <?php if( count($comments) == 0 ) echo '<div class="alert">brak komentarzy</div>'; ?>
        </ul>
<?php if($title) { ?>
    </div>
</div>
<?php } ?>