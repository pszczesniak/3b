<?php

use yii\helpers\Html;
?>

<?php if($title) { ?>
<div class="portlet">
    <div class="portlet-decoration">
        <div class="portlet-title"><?= $title ?></div>
    </div>
<?php } ?>
    <div class="portlet-content">
        <ul class="<?= $listClass ?>">
            <?php foreach($catalogs as $catalog): ?>
                <li>
                    <?= '<a href="'. Yii::$app->getUrlManager()->createUrl(['/blog/catalog/', 'id'=>$catalog->id, 'surname'=>$catalog->title]) .'">' . $catalog->title . '</a>' ?><span class="marker1"></span>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php if($title) { ?>
</div>
<?php } ?>