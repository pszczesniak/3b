<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if($title) { ?>
<div class="portlet">
    <div class="portlet-decoration">
        <div class="portlet-title"><?= $title ?></div>
    </div>
<?php } ?>
    <div class="portlet-content">
        <ul>
            <?php foreach($posts as $post): ?>
                <li>
                    <?php echo Html::a(Html::encode($post->title), Url::to(['blog/view', 'id' => $post->id, 'title' => $post->title]) ); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php if($title) { ?>
</div>
<?php } ?>

<a class="btn more_btn3" href="<?= Url::to(['blog/create']) ?>" >Zaproponuj swój wpis</a>