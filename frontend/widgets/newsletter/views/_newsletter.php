<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>


<div class="newsletter-widget-form">
    <div id="newsletter-form-<?= $id ?>-response" class="no-display alert alert-success"></div>
    <?php /*yii\widgets\Pjax::begin(['id' => 'pjax-grid-view-form-dicts'])*/ ?>
		<?php $form = ActiveForm::begin(['method' => 'POST', 
                                    'action' => Url::to(['site/subscribe']),
                                    'id' => 'newsletter-form-'.$id,
                                    'enableClientValidation' => false,
                                    'enableAjaxValidation' => false,
                                    'validateOnSubmit' => false,]); ?>

            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Nazwa'])->label(false) ?>
            <ul class="newsletter-widget-form-groups">
                <?php foreach($groups as $key=>$value) { 
                    echo '<input type="checkbox" value="'.$value->id.'" name="NewsletterSubscribe[groups][]" id="group-'.$value->id.'" class="form__checkbox" checked="checked">'
                        .'<label for="group-'.$value->id.'" class="form__checkbox-label">'.$value->name.'</label>';
                 } ?>
            </ul>
            <div class="form-group">
                <?= Html::submitButton('Subskrybuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <button id="newsletter-delete-<?= $id ?>" class="btn btn-danger" data-href="<?= Url::to(['site/subscribedelete'] ) ?>">Zrezygnuj</button>
            </div>

        <?php ActiveForm::end(); ?>
    <?php /*yii\widgets\Pjax::end()*/ ?>
</div>

<script>
	var formNewsletter<?= $id ?> = document.getElementById("newsletter-form-<?= $id ?>");
	formNewsletter<?= $id ?>.onsubmit = function() {
		var formDataComment<?= $id ?> = new FormData(formNewsletter<?= $id ?>);
		var xhr = new XMLHttpRequest();
		xhr.open('POST', formNewsletter<?= $id ?>.getAttribute('action'), true);
		xhr.onreadystatechange=function()  {
			if (xhr.readyState==4 && xhr.status==200)  {
				var a = JSON.parse(xhr.responseText);
				if(a.success) {
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("alert-error");
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("alert-success");
					document.getElementById("newsletter-form-<?= $id ?>-response").innerHTML = a.responseInfo;

					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("no-display");
					//document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("no-display");
                    
                    formNewsletter<?= $id ?>.reset();
				} else {
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("no-display", "alert-success");
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("alert-error");
					document.getElementById("newsletter-form-<?= $id ?>-response").innerHTML = a.error;
				}
			}
		}
		xhr.send(formDataComment<?= $id ?>);
		return false;
	}
    
    var deleteLink<?= $id ?> = document.getElementById("newsletter-delete-<?= $id ?>");
    deleteLink<?= $id ?>.onclick = function() {
		var formDataComment<?= $id ?> = new FormData(formNewsletter<?= $id ?>);
		var xhr = new XMLHttpRequest();
        xhr.open('POST', deleteLink<?= $id ?>.getAttribute('data-href'), true);
		xhr.onreadystatechange=function()  {
			if (xhr.readyState==4 && xhr.status==200)  {
				var a = JSON.parse(xhr.responseText);
				if(a.success) {
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("alert-error");
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("alert-success");
					document.getElementById("newsletter-form-<?= $id ?>-response").innerHTML = a.responseInfo;

					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("no-display");
					//document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("no-display");
                    
                    formNewsletter<?= $id ?>.reset();
				} else {
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.remove("no-display", "alert-success");
					document.getElementById("newsletter-form-<?= $id ?>-response").classList.add("alert-error");
					document.getElementById("newsletter-form-<?= $id ?>-response").innerHTML = a.error;
				}
			}
		}
		xhr.send(formDataComment<?= $id ?>);
		return false;
    }
    
    
</script>
