<?php
namespace frontend\widgets\newsletter;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Newsletter\models\NewsletterSubscribe;
use backend\Modules\Newsletter\models\NewsletterGroup;

class NewsletterWidget extends Widget{
	
    private $groups;
    private $model;
    public $id = 1;
	
	public function init(){
		parent::init();
		
		$this->model = new NewsletterSubscribe();
        $this->groups = NewsletterGroup::find()->orderby('name')->all();
	}
	
	public function run(){
		//return Html::encode($this->structure);
		//$this->structure = [['label' => 'Gabinet', 'url' => ['#']], ['label' => 'Porady', 'url' => ['#']]];
		return $this->render('_newsletter', [
            'model' => $this->model, 'groups' => $this->groups, 'id' => $this->id
        ]);
	}
}
?>
