<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcPackage;
use backend\Modules\Svc\models\SvcPackageOption;

class PackagesWidget extends Widget{
	public $id;
    public $view = 1;
    private $packages = false;
    private $packagesData = false;
	
	public function init(){
		parent::init();
		
		$packages = SvcPackage::find()->where(['status' => 1])->orderby('rank')->all();
        
        $packageOptions = SvcPackageOption::find()->where(['status' => 1])->orderby('rank')->all();
		foreach($packageOptions as $key=>$option) {
			$packagesData[$option->id]['label'] = $option->name;
			foreach($packages as $i=>$package) {
				$packagesData[$option->id]['value-'.$package->id] = '-';
				$packagesData[$option->id]['package'] = $package->id;
			}
		}
		//print_r($packagesData); exit;
		
		foreach($packages as $i => $package) {
			$configPackage = \yii\helpers\Json::decode($package->options);
			foreach($configPackage as $key => $option) {
				if(isset($packagesData[$key]['value-'.$package->id]))
					$packagesData[$key]['value-'.$package->id] = $option;
			}
            $packagesButtons[$package->id]['label'] = $package->price_label;
            $packagesButtons[$package->id]['promoClass'] = '';
            $packagesButtons[$package->id]['promoLabel'] = '';
            if($package->is_sale) {
                $packagesButtons[$package->id]['promoClass'] = 'promo';
                $packagesButtons[$package->id]['promoLabel'] = '<span class="percentage">%</span>';
                $packagesButtons[$package->id]['label'] = '<span class="old">'.$package->price.'</span>'.$packagesButtons[$package->id]['label'];
            }
            /*if($package->price == 0 || ($package->is_sale && $package->price_sale == 0) )
                $packagesButtons[$package->id]['button'] = '<a class= "btn more_btn2" href="'. Url::to(['offer/create', 'package' => $package->id]) .'">wybierz</a>';
            else {
                if($package->is_sale)  $package->price = $package->price_sale;
                $ParametersArray = array (
                    "api_version"  => "dev",
                    "amount"  => ($package->price),
                    "currency"  => "PLN",
                    "description"  => "P�atno�� za wykup pakietu ".$package->name,
                    "url"  => Url::to(['/offer/pay', 'package' => $package->id, 'user' => \Yii::$app->user->id ], true),
                    "type"  => "0",
                    "buttontext"  => "Wr�� do serwisu",
                    "urlc"  => Url::to(['/offer/pay', 'package' => $package->id, 'user' => \Yii::$app->user->id ], true),
                    "control"  => "MXdvR1MzaUdLQWRk",
                    //"firstname"  => "Jan",
                    //"lastname"  => "Nowak",
                    //"email"  => $model->email,
                   // "street"  => "Warszawska",
                    //"street_n1"  => "1",
                    //"city"  => $model->city,
                    //"postcode"  => $model->postal_code,
                    //"phone"  => $model->phone,
                    "country"  => "POL"
                );

                $packageButton = \Yii::$app->runAction('dotpay/generate', ["DotpayId" => 734364, "DotpayPin" => "OS7frE70p6WN6iFn2omiTMFovjUlK2xw", "Environment" => "test", "RedirectionMethod" => "POST", "ParametersArray" => $ParametersArray, 'labelButton' => "wybierz", 'package' => $package->id]);
        
                
                $packagesButtons[$package->id]['button'] = $packageButton;
            }*/
		}
        
        $this->packages = $packages;
        $this->packagesData = $packagesData;
	}
	
	public function run(){
		return $this->render('_package/_view_'.$this->view, [
            'packages' => $this->packages,'packagesData' => $this->packagesData, 'packagesButtons' => false
        ]);
	}
}
?>
