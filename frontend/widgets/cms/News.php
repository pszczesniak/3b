<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\Modules\Cms\models\CmsPage;

class News extends Widget{
	public $id = 1;
	public $viewType = 'news';
	private $pages;
	
	public function init(){
		parent::init();
		
		$this->pages = CmsPage::find()->where(['fk_category_id' => $this->id, 'fk_status_id' => 1])->/*andWhere('event_date >= "2015-09-01"')->*/orderby('event_date desc')->all();
		//$this->viewType = $this->gallery->viewType;
	}
	
	public function run(){
		return $this->render('news', [
            'pages' => $this->pages, 'viewType' => $this->viewType
        ]);
	}
}
?>
