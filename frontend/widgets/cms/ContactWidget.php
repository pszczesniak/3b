<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use frontend\models\ContactForm;

class ContactWidget extends Widget{
	
	private $contactId;
    public $view = 1;
	
	public function init(){
		parent::init();
		
		$this->contactId = new ContactForm();
        if(!\Yii::$app->user->isGuest) {
            $this->contactId->name = \Yii::$app->user->identity['fullname'];
            $this->contactId->email = \Yii::$app->user->identity['email'];
        }
	}
	
	public function run(){
		return $this->render('contact/contactform_'.$this->view, [
            'model' => $this->contactId, 'view' => $this->view
        ]);
	}
}
?>
