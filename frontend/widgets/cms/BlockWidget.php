<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsWidgetBox;

class BlockWidget extends Widget{
	public $id;
    public $view = 1;
    private $object = false;
	
	public function init(){
		parent::init();
		
		$this->object = CmsWidgetBox::findOne($this->id);
	}
	
	public function run(){
		return $this->render('_block/_view_'.$this->view, [
            'object' => $this->object, 
        ]);
	}
}
?>
