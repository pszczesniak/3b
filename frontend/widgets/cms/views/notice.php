<?php
    use yii\helpers\Url;
    $langs = ['Jan' => 'Sty', 'Feb' => 'Lut', 'Mar' => 'Mar', 'Apr' => 'KWI',  'May' => 'Maj', 'Jun' => 'Cze', 'Jul' => 'Lip', 'Aug' => 'Sie', 'Sep' => 'Wrz', 'Oct' => 'Paź', 'Nov' => 'Lis', 'Dec' => 'Gru'];
?>
<?php if( count($pages) > 0 ) { ?>
<ul class="list-leaf">
    <?php
        foreach($pages as $key => $event) {
        $event->title_slug = ($event->title_slug) ? $event->title_slug : 'news';
        echo '<li>'
                .'<a href="'.Url::to(['site/cpage', 'pid' => $event->id, 'pslug' => $event->title_slug, 'cslug' => 'ogloszenia']).'">'
                    .'<i>'. $event->title .'</i>'
                .'</a>'
            .'</li>';
        }
    ?>
</ul>
<?php } else { echo '<div class="alert alert-warning"><center>brak aktualnych '.$title.'</center></div>'; } ?>