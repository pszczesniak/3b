<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/widgets/box/cover/".$object->id."/medium.jpg")) ? "/uploads/widgets/box/cover/".$object->id."/medium.jpg" : false; ?>
<div class="block-1">
    <?php if($img) { ?> <img src="<?= $img ?>" alt=""> <?php } ?>
    <div class="caption">
        <?= $object->describe ?>
    </div>
</div>
