
<script>
	var form = document.getElementById('<?= $id ?>');
	form.onsubmit = function() {
		form.classList.add('preload');
        var formData = new FormData(form);
		var xhr = new XMLHttpRequest();
		xhr.open('POST', form.getAttribute('action'), true);
		xhr.onreadystatechange=function()  {
			if (xhr.readyState==4 && xhr.status==200)  {
				var a = JSON.parse(xhr.responseText);
				if(a.success) {
					document.getElementById("<?= $id ?>-response").classList.remove("alert-danger", "alert-warning");
					document.getElementById("<?= $id ?>-response").classList.add("alert-success");
					document.getElementById("<?= $id ?>-response").innerHTML = 'Email został wysłany';

					document.getElementById("<?= $id ?>-response").classList.remove("no-display");
					document.getElementById("<?= $id ?>").classList.add("no-display");
				} else {
					document.getElementById("<?= $id ?>-response").classList.remove("alert-success", "alert-warning", "no-display");
					document.getElementById("<?= $id ?>-response").classList.add("alert-danger");
					document.getElementById("<?= $id ?>-response").innerHTML = a.error;
				}
                form.classList.remove('preload');
                $.ajax({
                    type: 'GET',
                    url: '/site/captcha',
                    data: {refresh: 1},
                    success: function (data) {
                        $("img[id$='contactform-verifycode-image']").attr('src', data.url);
                        $("input#contactform-verifycode").val('');
                    }
                });
			}
		}
		xhr.send(formData);
		return false;
	}

	function resetContactForm() {
		document.getElementById("<?= $id ?>-response").classList.remove("alert-success", "alert-danger");
		document.getElementById("<?= $id ?>-response").classList.add("alert-wait");
		document.getElementById("<?= $id ?>-response").innerHTML = 'Czekamy na wiadomość od Ciebie...';
		document.getElementById("<?= $id ?>-response").classList.add("no-display");
		document.getElementById("<?= $id ?>").classList.remove("no-display");
	}
</script>