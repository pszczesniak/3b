<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

//$this->title = 'Contact';
//$this->params['breadcrumbs'][] = $this->title;
$template = '<div class="form__row">{label}{input}</div>';
?>

<div id="contact" class="site-contact">
	
    <div class="contact-contain">
		<div id="contact-reponse" class="alert alert-warning no-display"></div>
        <div id="send-response"></div>
		<?php $form = ActiveForm::begin([
			'id' => 'widget-contact-form',
			'action' => ['fun/contact'],
			'ajaxDataType' => 'json',
			'ajaxParam' => 'ajax',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'options' => ['class' => 'widget-contact-form'] ]); ?>

                <?= $form->field($model, 'name', ['template' => $template])->textInput([/*'placeholder' => 'Wpisz imię i nazwisko',*/ 'class' => 'form__input'])->label('Imię i Nazwisko', ['class' => 'form__label form__label--thin']) ?>

                <?= $form->field($model, 'subject', ['template' => $template])->textInput([/*'placeholder' => 'Wpisz temat'*/ 'class' => 'form__input'])->label('Temat', ['class' => 'form__label form__label--thin']) ?>

                <?= $form->field($model, 'email', ['template' => $template])->textInput([/*'placeholder' => 'Wpisz adres email'*/ 'class' => 'form__input'])->label('E-mail', ['class' => 'form__label form__label--thin']) ?>
                
				<?= $form->field($model, 'body', ['template' => $template ])->textArea(['rows' => 6, /*'placeholder' => 'Wpisz treść wiadomości'*/ 'class' => 'form__textarea'])->label('Wiadomość', ['class' => 'form__label form__label--thin']) ?>
				
                <?php /* $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) */ ?>
				<div class="form__row mt--20em push">
					 <?= Html::submitButton('Wyślij wiadomość', ['id' => 'send-email', 'class' => 'btn btn--border btn--solid-gold', 'name' => 'contact-button']) ?>
				</div>
			</ul>

		<?php ActiveForm::end(); ?>
		<div id="widget-contact-form-response" class="no-display"><p>Dziękujemy za kontakt.</p><p>Odezwiemy się w ciągu 48h.</p> </div>
	</div>
</div>

<script>
	var form = document.getElementById('widget-contact-form');
	form.onsubmit = function() {
	    form.classList.add('preload');
        var formData = new FormData(form);
		var xhr = new XMLHttpRequest();
		xhr.open('POST', form.getAttribute('action'), true);
		xhr.onreadystatechange=function()  {
			if (xhr.readyState==4 && xhr.status==200)  {
				var a = JSON.parse(xhr.responseText);
				if(a.success == true) {
					document.getElementById("contact-reponse").classList.remove("alert-danger", "alert-warning");
					document.getElementById("contact-reponse").classList.add("alert-success");
					document.getElementById("contact-reponse").innerHTML = 'Email został wysłany';

					document.getElementById("widget-contact-form-response").classList.remove("no-display");
					document.getElementById("widget-contact-form").classList.add("no-display");
                    
                    form.classList.remove('preload');
				} else {
					document.getElementById("contact-reponse").classList.remove("alert-success", "alert-warning", "no-display");
					document.getElementById("contact-reponse").classList.add("alert-danger");
					document.getElementById("contact-reponse").innerHTML = a.error;
                    
                    form.classList.remove('preload');
				}
                
                /*$.ajax({
                    type: 'GET',
                    url: '/site/captcha',
                    data: {refresh: 1},
                    success: function (data) {
                        $("img[id$='contactform-verifycode-image']").attr('src', data.url);
                        $("input#contactform-verifycode").val('');
                    }
                });*/
			} else {
                document.getElementById("contact-reponse").classList.remove("alert-success", "alert-warning");
                document.getElementById("contact-reponse").classList.add("alert-danger");
                document.getElementById("contact-reponse").innerHTML = a.error;
                
                form.classList.remove('preload');
            }
		}
		xhr.send(formData);
		return false;
	}

	function resetContactForm() {
		document.getElementById("contact-reponse").classList.remove("alert-success", "alert-danger");
		document.getElementById("contact-reponse").classList.add("alert-wait");
		document.getElementById("contact-reponse").innerHTML = 'Czekamy na wiadomość od Ciebie...';
		document.getElementById("widget-contact-form-response").classList.add("no-display");
		document.getElementById("widget-contact-form").classList.remove("no-display");
	}
</script>