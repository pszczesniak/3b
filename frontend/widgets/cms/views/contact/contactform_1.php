<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<div class="widget-contact-form">
    <div id="contact-reponse" class="alert alert-warning">Czekamy na wiadomość od Ciebie...</div>
    <div id="send-response"></div>
    <?php $formContact = ActiveForm::begin([
			'id' => 'contact-form',
			'action' => ['fun/contact'],
			'ajaxDataType' => 'json',
			'ajaxParam' => 'ajax',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'options' => ['class' => 'contact-form'] ]); ?>

       
                <?= $formContact->field($model, 'name', ['template' => '{input}'])->textInput(['placeholder' => 'Wpisz imię i nazwisko']) ?>
                <?= $formContact->field($model, 'subject', ['template' => '{input}'])->textInput(['placeholder' => 'Wpisz temat']) ?>
                <?= $formContact->field($model, 'email', ['template' => '{input}'])->textInput(['placeholder' => 'Wpisz adres email']) ?>
                <?= $formContact->field($model, 'body', ['template' => ' {input}', 'options' => [] ])->textArea(['rows' => 5, 'placeholder' => 'Wpisz treść wiadomości']) ?>

        <?= $formContact->field($model, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>

        <div class="form-group-buttons">
            <?= Html::submitButton('Wyślij wiadomość', ['id' => 'send-email', 'class' => 'btn btn--white', 'name' => 'contact-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    <div id="contact-form-response" class="no-display"><p>Dziękujemy za kontakt.</p><p>Odezwiemy się w ciągu 48h.</p> </div>
</div>
