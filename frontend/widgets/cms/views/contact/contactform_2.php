<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<div class="site-contact">

        <div id="contact-form-section-response" class="alert alert-warning no-display"></div>
        <?php $form = ActiveForm::begin([
            'id' => 'contact-form-section', 
            'action' => ['fun/contact'],
            'ajaxDataType' => 'json', 
            'ajaxParam' => 'ajax', 
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,

            'options' => ['class' => 'contact-form'] ]); ?>
            <div class="grid grid--20">
                <div class="col-sm-6 col-xs-12">
                    <?= $form->field($model, 'name', ['template' => ' {input}{error}{hint}'])->textInput(['placeholder' => 'Imię i Nazwisko'])->label(false) ?>

                    <?= $form->field($model, 'email', ['template' => ' {input}{error}{hint}'])->textInput(['placeholder' => 'E-mail'])->label(false)  ?>

                    <?= $form->field($model, 'subject', ['template' => ' {input}{error}{hint}'])->textInput(['placeholder' => 'Temat'])->label(false) ?>
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="grid grid--0 grid--start"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?= $form->field($model, 'body', ['template' => ' {input}', 'options' => [] ])->textArea(['rows' => 9, 'placeholder' => 'Treść'])->label(false) ?>
                    <div class="form-group-buttons">
                        <?= Html::submitButton('Wyślij wiadomość', ['class' => 'btn', 'name' => 'contact-button']) ?>
                    </div>
                </div>
            </div>
    
        <?php ActiveForm::end(); ?>
</div>
<?= $this->render('contactform_script', ['id' => 'contact-form-section']); ?>