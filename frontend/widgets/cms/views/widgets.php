<?php
	use frontend\widgets\cms\GalleryWidget;
    use frontend\widgets\cms\TabsWidget;
	use frontend\widgets\cms\BlockWidget;
    use frontend\widgets\cms\FilesWidget;
    use frontend\widgets\cms\PackagesWidget;
    
    if(is_array($structure)) {
        for($i = 0; $i < count($structure); ++$i) {
            if($structure[$i]['type'] == 'gallery')
                echo GalleryWidget::widget(['id' => $structure[$i]['id']]);
                
            if($structure[$i]['type'] == 'tabs')
                echo TabsWidget::widget(['id' => $structure[$i]['id']]);
                
            if($structure[$i]['type'] == 'block')
                echo BlockWidget::widget(['id' => $structure[$i]['id']]);
                
            if($structure[$i]['type'] == 'files')
                echo FilesWidget::widget(['id' => $structure[$i]['id']]);
                
            if($structure[$i]['type'] == 'contact') 
                echo \frontend\widgets\cms\ContactWidget::widget(['view' => 'widget']);
                
            if($structure[$i]['type'] == 'packages') 
                echo \frontend\widgets\cms\PackagesWidget::widget(['view' => '1']);
                //echo \frontend\widgets\MapsBranch::widget(['view' => 'widget']);
        }
    }
?>