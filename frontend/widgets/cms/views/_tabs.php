<!--<div class="tab">
    <input id="tab-one" type="checkbox" name="tabs">
    <label for="tab-one">Label One</label>
    <div class="tab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum? Saepe, itaque.</p>
    </div>
</div>
<div class="tab">
    <input id="tab-two" type="checkbox" name="tabs">
    <label for="tab-two">Label Two</label>
    <div class="tab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum? Saepe, itaque.</p>
    </div>
</div>-->

<?php
    if(!$quantity) {
        foreach($object->items as $key => $item) {
            echo '<div class="tab">'
                    .'<input id="tab-'.$item->id.'" type="checkbox" name="tabs" '.(($key==0) ? 'checked' : '').'>'
                    .'<label for="tab-'.$item->id.'">'.$item->name.'</label>'
                    .'<div class="tab-content">'
                        .'<p>'.$item->describe.'</p>'
                    .'</div>'
                .'</div>';
        }
    } else {
        $quantity = ((count($object->items))>$quantity) ? $quantity : count($object->items);
        for($i=0; $i<$quantity;++$i) {
            $item = $object->items[$i];
            echo '<div class="tab">'
                    .'<input id="tab-'.$item->id.'" type="checkbox" name="tabs" '.(($i==0) ? 'checked' : '').'>'
                    .'<label for="tab-'.$item->id.'">'.$item->name.'</label>'
                    .'<div class="tab-content">'
                        .'<p>'.$item->describe.'</p>'
                    .'</div>'
                .'</div>';
        }
    }
?>