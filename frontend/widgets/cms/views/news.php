<?php
    use yii\helpers\Url;
    $langs = ['Jan' => 'Sty', 'Feb' => 'Lut', 'Mar' => 'Mar', 'Apr' => 'KWI',  'May' => 'Maj', 'Jun' => 'Cze', 'Jul' => 'Lip', 'Aug' => 'Sie', 'Sep' => 'Wrz', 'Oct' => 'Paź', 'Nov' => 'Lis', 'Dec' => 'Gru'];
?>
<?php if(count($pages)) { ?>
<ul class="list-news" id="list-news-<?= $pages[0]->fk_category_id ?>">
    <?php
        foreach($pages as $key => $event) {
        $event->title_slug = ($event->title_slug) ? $event->title_slug : 'news';
        echo '<li>'
                .'<div class="event nearest">'
                    .'<figure class="date">'
                        .'<div class="month">'.$langs[date("M", strtotime($event->event_date))].'</div>'
                        .'<div class="day">'.date("d", strtotime($event->event_date)).'</div>'
                    .'</figure>'
                    .'<div class="aside">'
                        .'<header>'
                            .'<h4><a href="'. Url::to(['/site/cpage', 'pid' => $event->id, 'pslug' => $event->title_slug, 'cslug' => 'aktualnosci']).'">'. $event->title .'</a></h4>'
                        .'</header>'
                        .'<div class="additional-info">'. $event->short_content .'</div>'
                    .'</div>'
                    .'<div class="clear"></div>'
                .'</div>'
                .'<div class="clear"></div>'
            .'</li>';
        }
    ?>
</ul>
<?php } else { echo '<div class="alert alert-warning"><center>brak aktualności</center></div>'; } ?>