<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

?>

     <div class="price-table">
        <div class="price-table__row">
            <div class="price-table__cell price-table__desc"></div>
            <?php
                foreach($packages as $key => $package) {
                    echo '<div class="price-table__cell">'.$package->name.'</div>';
                }
            ?>
            <!--<div class="price-table__cell">Podstawowy</div>
            <div class="price-table__cell">Aktywny</div>
            <div class="price-table__cell">VIP</div>-->
        </div>
        <div class="price-table__row">
            <div class="price-table__cell price-table__desc">Cena</div>
            <?php
                foreach($packages as $key => $package) {
                    echo '<div class="price-table__cell">'.trim((($package->price == 0) ? '' : $package->price).' '.$package->price_label).'</div>';
                }
            ?>
            <!--<div class="price-table__cell">Podstawowy</div>
            <div class="price-table__cell">Aktywny</div>
            <div class="price-table__cell">VIP</div>-->
        </div>
        <?php foreach($packagesData as $key => $value) { ?>
            <div class="price-table__row">
            <div class="price-table__cell"><?= $value['label'] ?></div>
            <?php
                foreach($packages as $key => $package) {
                    echo '<div class="price-table__cell">'.$value['value-'.$package->id].'</div>';
                }
            ?>
            <!--<div class="price-table__cell"><?= $value['value-1'] ?></div>
            <div class="price-table__cell"><?= $value['value-2'] ?></div>
            <div class="price-table__cell"><?= $value['value-3'] ?></div>-->
        </div>
        <?php } ?>
        <div class="price-table__summary">
            <!--<div class="price-table__cell">*Raz w miesiącu na zaprzyjaźnionych stronach, które mają minimum 500 polubień lub 200 znajomych</div>
            <div class="price-table__cell">darmowy <br /> <a class= "btn more_btn2" href="<?= Url::to(['offer/create', 'package' => 1]) ?>">wybierz</a></div>
            <div class="price-table__cell">35 PLN/rok <br /> <a class= "btn more_btn2" href="<?= Url::to(['offer/create', 'package' => 2]) ?>">wybierz</a></div>
            <div class="price-table__cell">80 PLN/rok <br /> <a class= "btn more_btn2" href="<?= Url::to(['offer/create', 'package' => 3]) ?>">wybierz</a></div>-->
            <?php
                if($packagesButtons) {
                    foreach($packagesButtons as $key => $button) {
                        echo '<div class="price-table__cell '.$button['promoClass'].'"> '.$button['label'].' <br /> '.$button['promoLabel'].$button['button'].'</div>';
                    }
                }
            ?>
        </div>
    </div>

    <?php foreach($packages as $key => $package) { ?>
        <div class="price-table-mob">
            <a class="price-table-mob__title js-toggle" title="Podstawowy" href="#"><?= $package->name ?></a>
            <div class="price-table-mob__content">
                <div class="price-table-mob__row">
                    <div class="price-table-mob__type">Cena</div>
                    <div class="price-table-mob__value"><?= trim((($package->price == 0) ? '' : $package->price).' '.$package->price_label) ?></div>
                </div>
                <?php foreach($packagesData as $i => $option) { ?>
                <div class="price-table-mob__row">
                    <div class="price-table-mob__type"><?= $option['label'] ?></div>
                    <div class="price-table-mob__value"><?= $option['value-'.$package->id] ?></div>
                </div>
                <?php } ?>
            </div>
            <?php if($packagesButtons) { ?>
            <div class="price-table-mob__summary">
                <?php $button = $packagesButtons[$package->id]; echo '<div class="price-table__cell '.$button['promoClass'].'"> '.$button['label'].' <br /> '.$button['promoLabel'].$button['button'].'</div>'; ?>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
