<?php
    use yii\helpers\Url;
?>

<div class="galleryBox" id="gallery-smile"> 
	<!--<div id="gallery-loading" class="loader">Loading images...</div> -->
	<ul class="gallery products-grid example-6 gallery-style-<?php echo $gallery->id; ?> ">
	<?php foreach($photos as $photo) { ?>
		<?php  $file_name = $photo->id;  ?>		
		<li class="item" <?= (!$photo->is_show) ? ' style="border: 2px solid red;"': '' ?>>
            <a href="<?= Url::to(['/client/ephoto', 'id' => $photo->id]) ?>" class="viewModal" data-id="show"><img src="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$photo->id.'/preview.jpg'; ?>" /></a>
		</li>
	<?php } ?>
   </ul>
</div>  