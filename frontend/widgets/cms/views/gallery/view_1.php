<div id="owl-demo" class="owl-carousel owl-theme camera_wrap flex-viewport">
    <?php foreach($photos as $photo) { $file_name = $photo->id; ?>
        <div class="item">
			<!--<figure class="ratio-container unknown-ratio-container">
				<img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-sizes="auto" data-srcset="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/medium.jpg'; ?> 320w, <?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/medium.jpg'; ?> 640w, <?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/large.jpg'; ?> 1024w" alt="">
			</figure>-->
            <figure style="background-image:url(/uploads/gallery/<?= $gallery->id ?>/<?= $photo->id ?>/large.jpg)" data-src="<?php echo '/uploads/gallery/'.$gallery->id.'/'.$photo->id.'/large.jpg'; ?>"></figure>
            <div class='slide-content'>
                <div class='overlay'></div>
                <h3><?= $photo->name ?></h3>
                <h2><?= $photo->description ?></h2>
                <!--p>erant aperiri sapientem senserit quo et. Sea aliquid interpretaris te, in his erant aperiri sapientem</p>-->
            </div>
		</div>
    <?php } ?>
</div>

<!--<div class="owl-carousel">
    <figure style="background-image:url(loading.gif)" data-src="image-real.jpg"></figure>
    <figure style="background-image:url(loading.gif)" data-src="image-real-2.jpg"></figure>
    <figure style="background-image:url(loading.gif)" data-src="image-real-3.jpg"></figure>
    <figure style="background-image:url(loading.gif)" data-src="image-real-4.jpg"></figure>
</div>-->