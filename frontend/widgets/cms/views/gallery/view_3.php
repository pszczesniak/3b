
<div class="media-slider">
    <div class="media-slider__wrapper">
        <?php foreach($photos as $key => $photo) { $file_name = $photo->id; ?>
            <img class="media-slider__img <?= ($key == 0) ? 'active' : '' ?>" src="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.jpg'; ?>" alt="slide">
        <?php } ?>
    </div>
</div>