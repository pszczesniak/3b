
<div class="galleryBox" id="gallery-smile"> 
	<!--<div id="gallery-loading" class="loader">Loading images...</div> -->
	<ul class="gallery products-grid example-6 gallery-style-<?php echo $gallery->id; ?> ">
	<?php foreach($photos as $photo) { ?>
		<?php  $file_name = $photo->id;  ?>
		<?php /*if(Yii::app()->language != 'pl') {$modelLang = GalleryPhotoLang::model()->findByAttributes(array('photo_id'=>$photo->id, 'lang'=>Yii::app()->language )); }*/ ?>
		
		<li class="item">
			<!--<div style="width:100%;float: left; ">
				<div style="width:100%;">-->
					<a href="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.jpg'; ?>" rel="lightbox[_myGallery]" data-title="<?= $photo->file_name ?>" data-lightbox="roadtrip" title="<?php if ($photo->name) echo $photo->name; else echo 'image'; ?>">
						<img src="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$photo->id.'/preview.jpg'; ?>" />
					</a>
				<!--</div>
			</div>-->
		</li>
	<?php } ?>
   </ul>
</div>  
<?php if(count($movies) > 0) { $firstLink = ''; $firstType = '';
	echo '<figure id="video_player">';
            echo '<figcaption>';
                echo '<ul id="playlist">';
                        foreach($movies as $key => $video) {
                            if($video->extension_file == 'youtube') { if($key == 0) {  $firstType = 'youtube';  $firstLink = $video->name_file; }
                                echo '<li '. ( ($key == 0) ? 'class="active"' : '') .' movieurl="'.$video->name_file.'" movietype="youtube">'.$video->title_file.'</li>';
                            } else {  if($key == 0) { $firstType = 'video'; $firstLink ='/uploads/'.$video->systemname_file.'.'.$video->extension_file; }
                                echo '<li '. ( ($key == 0) ? 'class="active"' : '') .' movieurl="/uploads/'.$video->systemname_file.'.'.$video->extension_file.'" movietype="'.$video->mime_file.'">'.$video->title_file.'</li>';
                            }
                        }
                echo '</ul>';
            echo '</figcaption>';
            echo '<video id="videoarea" controls="controls" autoplay="" preload="none" poster="" src="'.$firstLink.'" '.( ($firstType == 'youtube') ? 'class="displayNone"' : '').'></video>';
            echo '<embed id="embedarea" src="'.$firstLink.'" '.( ($firstType == 'video') ? 'class="displayNone"' : '').'></embed>';
        echo '</figure>';
} ?>
<?php if(count($movies) > 0) { ?>
<script>
    var video_player = document.getElementById("video_player");
    if(video_player) {
        var links = video_player.getElementsByTagName('a');
        for (var i=0; i<links.length; i++) {
            links[i].onclick = vhandler;
        }
        video_player.volume = 0.2;
    }
    function vhandler(e) {
        e.preventDefault();
        videotarget = this.getAttribute("href");
        filename = videotarget.substr(0, videotarget.lastIndexOf('.')) || videotarget;
        video = document.querySelector("#video_player video");
       // video.removeAttribute("controls");
        video.removeAttribute("poster");
        source = document.querySelectorAll("#video_player video source");
        source[0].src = filename + ".mp4";
        source[1].src = filename + ".mp4";
        video.volume = 0.2;
        video.load();
        video.play();    
    }
</script>
<?php } ?>