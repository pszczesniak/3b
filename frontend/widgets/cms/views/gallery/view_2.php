<div id="owl-demo" class="owl-carousel owl-theme">
	<?php foreach($photos as $photo) { $file_name = $photo->id; ?>
		<div class="item">
			<!--<img src="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.png'; ?>" alt="slide">-->
			
			<figure class="ratio-container unknown-ratio-container"><img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-sizes="(min-width: 40em) 80vw, 100vw" data-srcset="<?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.jpg'; ?> 320w, <?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.jpg'; ?> 640w, <?php echo Yii::getAlias('@imageurl') . '/gallery/'.$gallery->id.'/'.$file_name.'/original.jpg'; ?> 1024w" alt=""></figure>
		</div>
	<?php } ?>
</div>