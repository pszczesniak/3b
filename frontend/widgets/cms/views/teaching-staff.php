<?php
    use yii\helpers\Url;
?>
    <link rel="stylesheet" type="text/css" href="/css/kadra.css" />
    <table class="tabKadra" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="5%"></col>
            <col width="33%"></col>
            <col width="33%"></col>
            <col width="29%%"></col>
        </colgroup>
        <thead>
            <tr>
                <th id="first">Lp</th>
                <th>Imię i nazwisko</th>
                <th>Funkcja</th>
                <th>Przedmioty nauczania</th>
            </tr>
        </thead>
        <tbody>
            <!--<tr>
                <td class="right">1</td>
                <td>mgr Ireneusz Gudowicz</td>
                <td>dyrektor szkoły</td>
                <td>matematyka<br />przysposobienie obronne</td>
            </tr>-->
            <?php
                foreach($data1 as $key => $value) {
                    echo '<tr>'
                            .'<td class="right">'.($key + 1).'</td>'
                            .'<td>'.$value['name_employee'].'</td>'
                            .'<td>'.$value['position'].'</td>'
                            .'<td>'.$value['subject'].'</td>'
                        .'</tr>';
                }
            ?>
        </tbody>
    </table>
<br />
<h1>Pracownicy administracji</h1>
    <table class="tabKadra" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="5%"></col>
            <col width="47%"></col>
            <col width="48%"></col>
        </colgroup>
        <thead>
            <tr>
                <th id="first">Lp</th>
                <th>Imię i nazwisko</th>
                <th>Stanowisko</th>
            </tr>
        </thead>
        <tbody>
            <!--<tr>
                <td class="right">1</td>
                <td>Karina Broja</td>
                <td>główna księgowa</td>
            </tr>-->
            <?php
                foreach($data2 as $key => $value) {
                    echo '<tr>'
                            .'<td class="right">'.($key + 1).'</td>'
                            .'<td>'.$value['name_employee'].'</td>'
                            .'<td>'.$value['position'].'</td>'
                        .'</tr>';
                }
            ?>

        </tbody>
    </table>
<br />
<h1>Pracownicy obsługi</h1>
    <table class="tabKadra" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="5%"></col>
            <col width="95%"></col>
        </colgroup>
        <thead>
            <tr>
                <th id="first">Lp</th>
                <th>Imię i nazwisko</th>
            </tr>
        </thead>
        <tbody>

            <?php
                foreach($data3 as $key => $value) {
                    echo '<tr>'
                            .'<td class="right">'.($key + 1).'</td>'
                            .'<td>'.$value['name_employee'].'</td>'
                        .'</tr>';
                }
            ?>
            
        </tbody>
    </table>


