<?php
namespace frontend\widgets\cms;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\Modules\Cms\models\CmsPage;

class TeachingStaff extends Widget{
	public $id = 1;
	public $viewType = 'news';
	private $data1;
    private $data2;
    private $data3;
	
	public function init(){
		parent::init();
		$sqlQuery = "select * from {{school_employee}} where group_id=1 order by no_employee";
		$this->data1 = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        $sqlQuery = "select * from {{school_employee}} where group_id=2 order by no_employee";
		$this->data2 = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        $sqlQuery = "select * from {{school_employee}} where group_id=3 order by name_employee";
		$this->data3 = Yii::$app->db->createCommand($sqlQuery)->queryAll();
		//$this->viewType = $this->gallery->viewType;
	}
	
	public function run(){
		return $this->render('teaching-staff', [
            'data1' => $this->data1, 'data2' => $this->data2, 'data3' => $this->data3
        ]);
	}
}
?>
