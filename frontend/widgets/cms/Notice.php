<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\Modules\Cms\models\CmsPage;

class Notice extends Widget{
	public $id = 1;
	public $viewType = 'news';
	private $pages;
    public $title = 'og�osze�';
	
	public function init(){
		parent::init();
		
		$this->pages = CmsPage::find()->where(['fk_category_id' => $this->id, 'fk_status_id' => 1])->andWhere('event_date >= "'.date('Y-m-d').'"')->orderby('event_date desc')->all();
		//$this->viewType = $this->gallery->viewType;
	}
	
	public function run(){
		return $this->render('notice', [
            'pages' => $this->pages, 'title' => $this->title
        ]);
	}
}
?>
