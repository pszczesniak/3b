<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsGallery;
use backend\Modules\Cms\models\CmsGalleryPhoto;

class GalleryWidget extends Widget{
	public $id;
    public $edit = false;
	private $viewType;
	private $gallery;
	private $photos;
	private $movies;
	
	public function init(){
		parent::init();
		
		$this->gallery = CmsGallery::findOne($this->id);
        if(!$this->edit) {
            $this->viewType = $this->gallery->view_type;
            $this->photos = $this->gallery->items;
			$this->movies = $this->gallery->videos;
        } else {
            $this->viewType = 'edit';
            $this->photos = $this->gallery->itemsall;
			$this->movies = $this->gallery->videos;
        }
	}
	
	public function run(){
		return $this->render('gallery/view_'.$this->viewType, [
            'gallery' => $this->gallery, 'photos' => $this->photos, 'movies' => $this->movies
        ]);
	}
}
?>
