<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsWidgetAccordion;
use backend\Modules\Cms\models\CmsWidgetAccordionItem;

class TabsWidget extends Widget{
	public $id;
    public $quantity = false;
    private $object = false;
	
	public function init(){
		parent::init();
		
		$this->object = CmsWidgetAccordion::findOne($this->id);
	}
	
	public function run(){
		return $this->render('_tabs', [
            'object' => $this->object, 'quantity' => $this->quantity
        ]);
	}
}
?>
