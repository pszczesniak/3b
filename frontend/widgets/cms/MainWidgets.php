<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsPageWidget;
use backend\Modules\Cms\models\CmsPageGallery;


class MainWidgets extends Widget{
	public $id;
    public $position = 1;
	private $structure;
	
	public function init(){
		parent::init();
		
        if($this->position == 1) {
            $galleries = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $this->id, 'status' => '1'])->andWhere('fk_gallery_id not in (select id from {{%cms_gallery}} where view_type = 3)')->orderBy('rank')->asArray()->all();
            for($i=0; $i<count($galleries);++$i) {
                $this->structure[] = ['type' => 'gallery', 'id' => $galleries[$i]['fk_gallery_id']];
            }
        }
   
        $widgets = \backend\Modules\Cms\models\CmsPageWidget::find()->where(['fk_id' => $this->id, 'status' => '1', 'widget_position' => $this->position])->orderBy('rank')->all();
		foreach($widgets as $key => $value) {
			$this->structure[] = ['type' => $value->widget['symbol_widget'], 'id' => $value->fk_cms_widget_id];
		}
	}
	
	public function run(){
		return $this->render('widgets', [
            'structure' => $this->structure,
        ]);
	}
}
?>
