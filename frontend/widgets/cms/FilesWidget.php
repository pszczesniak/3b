<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsWidgetFilesSet;
use backend\Modules\Cms\models\CmsWidgetFilesSetItem;

class FilesWidget extends Widget{
	public $id;
    public $quantity = false;
    private $object = false;
	
	public function init(){
		parent::init();
		
		$this->object = CmsWidgetFilesSet::findOne($this->id);
	}
	
	public function run(){
		return $this->render('_files', [
            'object' => $this->object, 'quantity' => $this->quantity
        ]);
	}
}
?>
