<?php
namespace frontend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsMenu;
use backend\Modules\Cms\models\CmsMenuItem;

class MainMenu extends Widget{
	public $id;
	public $options = ['class' => 'l-navbar__menu', 'id' => 'nav'];
    public $itemOptions = []; //['class'=>'top l-navbar__menu-item'];
    public $linkClass = '';
	private $structure = [];
    private $title = false;
	
	public function init(){
		parent::init();
		
		$menu = CmsMenu::findOne($this->id);
		if($menu) { 
			if($menu->show_title) $this->title = '<h5 class="widget-title">'.$menu->title.'</h5>';
            foreach($menu->items as $key=>$value) {
				if(is_array($value->children) ) {
                    if(count($value->children) > 0) {
                        foreach($value->children as $keyItem=>$valueItem) {
                            
                            if(count($valueItem->children) > 0 ) {
                                foreach($valueItem->children as $keyItem2=>$valueItem2) {
                                    $itemsChild2[] = ['label' => $valueItem2->name, 'url' => $valueItem2->getUrl()];
                                }
                                $itemsChild[] = ['label' => '<a class="fly" href="'.$value->getUrl().'">'.$valueItem->name.'</a>', 'encode'=>false,  
                                'items' => $itemsChild2, 'submenuTemplate' => "\n<ul class='l-navbar--desktop__third-level'>\n{items}\n</ul>\n" ];
                                $itemsChild2 = [];
                            } else {
                                $itemsChild[] = ['label' => $valueItem->name, 'url' => $valueItem->getUrl()];
                            }
                        }
                        $this->structure[] = ['label' => '<a class="'.$this->linkClass.' dropdown-toggle" data-toggle="dropdown" href="'.$value->getUrl().'"><span class="down">'.$value->name.'</span></a>',
                                             /* 'url' => $value->getUrl(), */
                                              'encode'=>false,
                                              'items' => $itemsChild, 'submenuTemplate' => "\n<ul class='l-navbar--desktop__second-level'>\n{items}\n</ul>\n",
                                              //'linkTemplate' => '<a class="top_link" href="{url}"><span class="down">{label}</span></a>', 
                                              'options'=>['class'=>'more arrow-down top'] ];
                        $itemsChild = [];
                    } 
				} else {
					if($this->id == 1)
						$this->structure[] = ['label' => '<a class="'.$this->linkClass.'" href="'.$value->getUrl().'"><span>'.$value->name.'</span></a>', 
											  /*'url' => $value->getUrl(), */
											  'encode'=>false,
											  'options'=>$this->itemOptions];
					else
						$this->structure[] = ['label' => '<a class="'.$this->linkClass.'" href="'.$value->getUrl().'">'.$value->name.'</a>', 
											  /*'url' => $value->getUrl(), */
											  'encode'=>false,
											  'options'=>$this->itemOptions];
				}
			}
		} 
	}
	
	public function run(){
		//return Html::encode($this->structure);
		//$this->structure = [['label' => 'Gabinet', 'url' => ['#']], ['label' => 'Porady', 'url' => ['#']]];
		return ( ($this->title) ? $this->title : '') . 
            \yii\widgets\Menu::widget([
				'options' => $this->options,
				'items' => $this->structure
			]);
	}
}
?>
