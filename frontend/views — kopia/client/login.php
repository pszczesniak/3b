<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Logowanie';
$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = $this->title;


$templateInput = '<div class="wrap-input validate-input %s" data-validate="Name is required">'
                       .'<span class="label-input">{label}</span><div class="wrap-input-error">{error}</div>'
                        .'{input}'
                        .'<span class="focus-input"></span>'
                    .'</div>';

?>
<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => false, 'class' => 'page', 'title'=>Html::encode($this->title), 'back' => false)) ?>

    <?php $form = ActiveForm::begin(['method' => 'post', 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['autocomplete' => 'off', 'class' => 'form'] ]); ?>
        <span class="form-title"> Logowanie  </span>

        <?= $form->field($model, 'username', ['template' => sprintf($templateInput, (isset($model->getErrors()['username']) ? 'alert-validate' : ''))])
                                 ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz email...', 'title' => 'Email', 'autocomplete' => 'off'])  ?>

        <?= $form->field($model, 'password', ['template' => sprintf($templateInput, (isset($model->getErrors()['password']) ? 'alert-validate' : ''))])
                                 ->passwordInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => '***********', 'title' => 'Imię'])  ?>

        <div class="flex-m w-full">
            <span class="txt1"> Jeśli zapomniałeś hasła <?= Html::a('zresetuj je', ['/site/request-password-reset']) ?>  </span>
        </div>

        <div class="container-form-btn">
            <div class="wrap-form-btn">
                <div class="form-btn-bgbtn"></div>
                <button class="form-btn"> Zaloguj się  </button>
            </div>

            <a href="<?= Url::to(['/apl/index']) ?>" class="dis-block txt3">  Załóż konto <i class="fas fa-long-arrow-right m-l-5"></i>   </a>
        </div>
    <?php ActiveForm::end(); ?>
<?php $this->endContent(); ?>
