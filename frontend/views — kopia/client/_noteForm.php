<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([ 'options' => ['class' => 'modal-form', 'data-target' => '#modal-view'] ]); ?>
    <div class="modal-body">

        <?= $form->field($model, 'info_content')->textarea(['rows' => 3, 'placeholder' => 'Wpisz wiadomość...'])->label(false) ?>
    </div>
    <?php
        if($model->parent) {
            echo '<div class="chat-box">'.$this->render('_note', ['model' => $model->parent]).'</div>';
        }
    ?>
    
    <div class="modal-footer"> 
        <div class="form-group align-right">
            <?= Html::submitButton('Wyślij', ['class' => 'btn btn-info']) ?>
            <button aria-hidden="true" data-dismiss="modal" class="btn" type="button" >Odrzuć</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>

