<div class="card border-0">
    <div class="card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15">
        <h3 class="h6 mb-0">  <i class="icon-directions g-pos-rel g-top-1 g-mr-5"></i> Nowe zdarzenia  </h3>
    </div>

    <div class="card-block g-pa-0">
        <div class="table-responsive">
            <table class="table table-bordered u-table--v2">
                <thead class="text-uppercase g-letter-spacing-1">
                    <tr>
                        <th class="g-font-weight-300 g-color-black">Zaistniało</th>
                        <th class="g-font-weight-300 g-color-black g-min-width-200">Opis</th>
                        <th class="g-font-weight-300 g-color-black g-min-width-130">Status</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="align-middle text-nowrap text-center">
                            <img class="g-brd-around g-brd-gray-light-v4 g-pa-2 g-width-50 g-height-50 rounded-circle" src="/images/avatar.png" data-toggle="tooltip" data-placement="top" data-original-title="Pixeel Ltd" alt="Image Description">
                            <br /><?= date('Y-m-d H:i:s') ?>
                        </td>
                        <td class="align-middle">Nulla ipsum dolor sit amet, consectetur adipiscing elitut eleifend nisl.</td>
                        <td class="align-middle">
                            <span class="badge badge-info">Oczekuje</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle text-nowrap text-center">
                            <img class="g-brd-around g-brd-gray-light-v4 g-pa-2 g-width-50 g-height-50 rounded-circle" src="/images/avatar.png" data-toggle="tooltip" data-placement="top" data-original-title="Pixeel Ltd" alt="Image Description">
                            <br /><?= date('Y-m-d H:i:s') ?>
                        </td>
                        <td class="align-middle">Nulla ipsum dolor sit amet, consectetur adipiscing elitut eleifend nisl.</td>
                        <td class="align-middle">
                            <span class="badge badge-info">Oczekuje</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle text-nowrap text-center">
                            <img class="g-brd-around g-brd-gray-light-v4 g-pa-2 g-width-50 g-height-50 rounded-circle" src="/images/avatar.png" data-toggle="tooltip" data-placement="top" data-original-title="Pixeel Ltd" alt="Image Description">
                            <br /><?= date('Y-m-d H:i:s') ?>
                        </td>
                        <td class="align-middle">Nulla ipsum dolor sit amet, consectetur adipiscing elitut eleifend nisl.</td>
                        <td class="align-middle">
                            <span class="badge badge-info">Oczekuje</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>