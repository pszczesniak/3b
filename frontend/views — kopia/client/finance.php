<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use frontend\widgets\Calculator;
/* @var $this yii\web\View */

$this->title = 'Dane finansowe';

$this->params['breadcrumbs'][] = 'Strefa klienta';
$this->params['breadcrumbs'][] = ['label' => 'Twoje konto', 'url' => ['account']];
$this->params['breadcrumbs'][] = 'Aktualizacja';
?>

<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => true)) ?>	
    <?= Alert::widget([]) ?>
    <div class="grid">
		<div class="col-md-3 col-sm-4 col-xs-12">
            <div class="calculator"><?= Calculator::widget(['action' => Url::to(['/apl/index'])]) ?></div>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <?= $this->render('_formFinance', ['model' => $model]) ?>
        </div>
    </div>
<?php $this->endContent(); ?>

