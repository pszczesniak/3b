<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Calculator;

$this->title = 'Twoje po�yczki';

$this->params['breadcrumbs'][] = 'Strefa klienta';
?>

<?php $this->beginContent('@app/views/layouts/account-container.php',array('model' => false, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false)) ?>	
    <?= Alert::widget([]) ?>
    
    <div class="card-block g-pa-0">
                <!-- Product Table -->
                <div class="table-responsive">
                  <table class="table table-bordered u-table--v2">
                    <thead class="text-uppercase g-letter-spacing-1">
                      <tr>
                        <th class="g-font-weight-300 g-color-black">Product Name</th>
                        <th class="g-font-weight-300 g-color-black g-min-width-200">Locations</th>
                        <th class="g-font-weight-300 g-color-black">Status</th>
                        <th class="g-font-weight-300 g-color-black">Contacts</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td class="align-middle text-nowrap">
                          <h4 class="h6 g-mb-2">Lenovo Group</h4>
                          <div class="js-rating g-font-size-12 g-color-primary" data-rating="3.5"><div class="g-rating" style="display: inline-block; position: relative; z-index: 1; white-space: nowrap; margin-left: -2px; margin-right: -2px;"><div class="g-rating-forward" style="position: absolute; left: 0px; top: 0px; height: 100%; overflow: hidden; width: 70%;"><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i></div><div class="g-rating-backward" style="position: relative; z-index: 1;"><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i></div></div></div>
                        </td>
                        <td class="align-middle">
                          <div class="d-flex">
                            <i class="icon-location-pin g-font-size-18 g-color-gray-dark-v5 g-pos-rel g-top-5 g-mr-7"></i>
                            <span>389ZA2 DeClaudine, CA, USA</span>
                          </div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-block u-btn-primary g-rounded-50 g-py-5" href="#!">
                            <i class="fa fa-arrows-v g-mr-5"></i> Middle
                          </a>
                        </td>
                        <td class="align-middle text-nowrap">
                          <span class="d-block g-mb-5">
                            <i class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> +1 4768 97655
                          </span>
                          <span class="d-block">
                            <i class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> contact@lenovo.com
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td class="align-middle text-nowrap">
                          <h4 class="h6 g-mb-2">Samsung Electronics</h4>
                          <div class="js-rating g-font-size-12 g-color-primary" data-rating="4.5"><div class="g-rating" style="display: inline-block; position: relative; z-index: 1; white-space: nowrap; margin-left: -2px; margin-right: -2px;"><div class="g-rating-forward" style="position: absolute; left: 0px; top: 0px; height: 100%; overflow: hidden; width: 90%;"><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i></div><div class="g-rating-backward" style="position: relative; z-index: 1;"><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i></div></div></div>
                        </td>
                        <td class="align-middle">
                          <div class="d-flex">
                            <i class="icon-location-pin g-font-size-18 g-color-gray-dark-v5 g-pos-rel g-top-5 g-mr-7"></i>
                            <span>738AD Lorena Spur, London, UK</span>
                          </div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-block u-btn-pink g-rounded-50 g-py-5" href="#!">
                            <i class="fa fa-level-up g-mr-5"></i> High
                          </a>
                        </td>
                        <td class="align-middle text-nowrap">
                          <span class="d-block g-mb-5">
                            <i class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> +44 7689 7655
                          </span>
                          <span class="d-block">
                            <i class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> users@samsung.com
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td class="align-middle text-nowrap">
                          <h4 class="h6 g-mb-2">Sony Corp.</h4>
                          <div class="js-rating g-font-size-12 g-color-primary" data-rating="2"><div class="g-rating" style="display: inline-block; position: relative; z-index: 1; white-space: nowrap; margin-left: -2px; margin-right: -2px;"><div class="g-rating-forward" style="position: absolute; left: 0px; top: 0px; height: 100%; overflow: hidden; width: 40%;"><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i></div><div class="g-rating-backward" style="position: relative; z-index: 1;"><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i></div></div></div>
                        </td>
                        <td class="align-middle">
                          <div class="d-flex">
                            <i class="icon-location-pin g-font-size-18 g-color-gray-dark-v5 g-pos-rel g-top-5 g-mr-7"></i>
                            <span>044C1 Port Dickson, BC, Canada</span>
                          </div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-block u-btn-cyan g-rounded-50 g-py-5" href="#!">
                            <i class="fa fa-sort-amount-desc g-mr-5"></i> Deep
                          </a>
                        </td>
                        <td class="align-middle text-nowrap">
                          <span class="d-block g-mb-5">
                            <i class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> +1 0739 3644
                          </span>
                          <span class="d-block">
                            <i class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> clients@sony.com
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td class="align-middle text-nowrap">
                          <h4 class="h6 g-mb-2">Apple Inc.</h4>
                          <div class="js-rating g-font-size-12 g-color-primary" data-rating="5"><div class="g-rating" style="display: inline-block; position: relative; z-index: 1; white-space: nowrap; margin-left: -2px; margin-right: -2px;"><div class="g-rating-forward" style="position: absolute; left: 0px; top: 0px; height: 100%; overflow: hidden; width: 100%;"><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i></div><div class="g-rating-backward" style="position: relative; z-index: 1;"><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i></div></div></div>
                        </td>
                        <td class="align-middle">
                          <div class="d-flex">
                            <i class="icon-location-pin g-font-size-18 g-color-gray-dark-v5 g-pos-rel g-top-5 g-mr-7"></i>
                            <span>07W2 Donell Lodge, NY, USA</span>
                          </div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-block u-btn-purple g-rounded-50 g-py-5" href="#!">
                            <i class="fa fa-level-down g-mr-5"></i> Down
                          </a>
                        </td>
                        <td class="align-middle text-nowrap">
                          <span class="d-block g-mb-5">
                            <i class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> +1 6589-96451
                          </span>
                          <span class="d-block">
                            <i class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> mail@appple.com
                          </span>
                        </td>
                      </tr>

                      <tr>
                        <td class="align-middle text-nowrap">
                          <h4 class="h6 g-mb-2">Dell Corporation</h4>
                          <div class="js-rating g-font-size-12 g-color-primary" data-rating="4"><div class="g-rating" style="display: inline-block; position: relative; z-index: 1; white-space: nowrap; margin-left: -2px; margin-right: -2px;"><div class="g-rating-forward" style="position: absolute; left: 0px; top: 0px; height: 100%; overflow: hidden; width: 80%;"><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star" style="margin-left: 2px; margin-right: 2px;"></i></div><div class="g-rating-backward" style="position: relative; z-index: 1;"><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i><i class="fa fa-star-o" style="margin-left: 2px; margin-right: 2px;"></i></div></div></div>
                        </td>
                        <td class="align-middle">
                          <div class="d-flex">
                            <i class="icon-location-pin g-font-size-18 g-color-gray-dark-v5 g-pos-rel g-top-5 g-mr-7"></i>
                            <span>1A9WA4 Wanderben, Berlin, Germany</span>
                          </div>
                        </td>
                        <td class="align-middle">
                          <a class="btn btn-block u-btn-deeporange g-rounded-50 g-py-5" href="#!">
                            <i class="fa fa-bolt g-mr-5"></i> Stabile
                          </a>
                        </td>
                        <td class="align-middle text-nowrap">
                          <span class="d-block g-mb-5">
                            <i class="icon-phone g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> +49 3868 4792
                          </span>
                          <span class="d-block">
                            <i class="icon-envelope g-font-size-16 g-color-gray-dark-v5 g-pos-rel g-top-2 g-mr-5"></i> clients@dell.com
                          </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- End Product Table -->
              </div>
    
<?php $this->endContent(); ?>

