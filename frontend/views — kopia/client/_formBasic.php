<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $description['firstname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['lastname'] = 'Wpisz tylko pierwsze imię. Pamiętaj o używaniu polskich znaków podczas wprowadzania danych.';
    $description['pesel'] = 'To Twój unikatowy, jedenastocyfrowy numer identyfikacyjny. Wpisz go bez spacji.';
    $description['evidence_number'] = 'To unikatowy, dziewięciocyfrowy numer Twojego dowodu osobistego. Wprowadź go bez spacji.';
    $description['marital'] = 'Wybierz jedną opcję z rozwijanej listy, która opisuje Twój stan cywilny.';
    $description['members'] = 'To osoby, z którymi mieszkasz i które są od Ciebie finansowo zależne (np. dzieci).';  
    $description['car'] = 'Wypełnij aby zwiększyć swoje szanse na pozytywną decyzję kredytową';   
    $description['car_type'] = 'Wpisz markę Swojego samochodu'; 
    $description['car_year'] = 'Wpisz rok produkcji Swojego samochodu'; 
    $description['car_insurance'] = 'Wpisz datę w której kończy się ubezpieczenie Twojego samochodu'; 
    $description['employment_status'] = 'Wybierz rodzaj zatrudnienia z rozwijanej listy. Pamiętaj, że nie udzielamy pożyczek osobom bezrobotnym ani pozbawionym źródeł regularnego dochodu.';
    $description['employment_period'] = 'Wybierz datę rozpoczęcia pracy.';
    $description['employer_name'] = 'Nazwy Twojego pracodawcy potrzebujemy do przeprowadzenia procesu udzielenia pożyczki.';
    $description['email'] = 'Podaj adres e-mail, do którego masz stały i łatwy dostęp. To będzie Twoja nazwa użytkownika, której nie będziesz mógł już zmienić.';
    $description['email_repeat'] = 'Wpisz ponownie swój adres email. Upewnij się, że jest taki sam, jak ten, który podałeś wcześniej.';
    $description['password'] = 'Wprowadź hasło, które stworzyłeś podczas rejestracji. Powinno ono zawierać co najmniej 8 znaków, w tym wielkie i małe litery, minimum jedną cyfrę oraz nie posiadać znaków specjalnych, np. !%$?. Pamiętaj, że tę samą literę możesz użyć maksymalnie 3 razy z rzędu.';
    
    $templateInput = '<div class="form-group grid g-mb-25">'
                    .'<label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">{label}</label>'
                    .'<div class="col-sm-9">'
                        .'<div class="input-group g-brd-primary--focus">'
                            .'{input}'
                            .'<div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">'
                                .'<i class="icon-lock"></i>'
                            .'</div>'
                        .'</div>'
                    .'</div>{error}{hint}</div>';
                    
?>

<?php $form = ActiveForm::begin(['method' => 'post', 'fieldConfig' => ['template' => "{input}", 'options' => ['tag' => false]  ]]); ?>
    <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    
    <ul class="nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20" role="tablist">
          <li class="nav-item ">
            <a class="nav-link g-py-10 active" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--1" role="tab" aria-selected="false">Dane <br />osobowe</a>
          </li>
          <li class="nav-item">
            <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--2" role="tab" aria-selected="true">Dane <br />kontaktowe</a>
          </li>
          <li class="nav-item">
            <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--3" role="tab" aria-selected="false">Dane <br />finansowe</a>
          </li>
          <li class="nav-item">
            <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--3" role="tab" aria-selected="false">Media <br />społecznościowe</a>
          </li>
    </ul>
    <div id="nav-1-1-default-hor-left-underline" class="tab-content">
        <div class="tab-pane fade active show" id="nav-1-1-default-hor-left-underline--1" role="tabpanel">
            <h2 class="h4 g-font-weight-300">Twoje dane identyfikacyjne</h2>
            <p>Aktualizacja Twoich danych personalnych, które będą prezentowane w systemie</p>
            <?= $form->field($model, 'firstname', ['template' => sprintf($templateInput, 'user', $description['firstname'])])
                 //->hint($description['firstname'])
                 ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, /*'class' => 'form__input form__input--narrow form-control',*/ 'placeholder' => 'Imię', 'title' => $description['firstname']]) ?>

            <?= $form->field($model, 'lastname', ['template' => sprintf($templateInput, 'user', $description['lastname'])])
                 //->hint($description['firstname'])
                 ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, /*'class' => 'form__input form__input--narrow form-control',*/ 'placeholder' => 'Nazwisko', 'title' => $description['firstname']]) ?>
                
                
          
            <div class="form__row">
                <div class="form__row__left"></div>
                <div class="form__row__right text-center">
                    <input type="text" id="contact-mail" name="contact-mail">
                    <button title="Send message" type="submit" class="btn btn--teal">Zapisz zmiany</button>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--2" role="tabpanel">
            <h2 class="h4 g-font-weight-300">Twoje dane kontaktowe</h2>
            <p>Aktualizacja Twoich danych kontaktowych, które ułatwią kontakt z Tobą</p>
        </div>
        <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--3" role="tabpanel">
            <h2 class="h4 g-font-weight-300">Informacje o sposobie rozliczenia</h2>
            <p>Konfiguracja sposobu płatności</p>
        </div>
    </div>

<?php ActiveForm::end(); ?>
