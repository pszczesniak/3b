<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use frontend\widgets\Calculator;

$this->title = 'Zmiana hasła';
$this->params['breadcrumbs'][] = $this->title;

	$description['password'] = 'Hasło powinno zawierać co najmniej 8 znaków, w tym wielkie i małe litery, minimum jedną cyfrę oraz nie posiadać znaków specjalnych [np. !%$?] i polskich liter. Pamiętaj, że tę samą literę możesz użyć maksymalnie 3 razy z rzędu.';
    
    $templateInput = '<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="bottom" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';
?>

<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => true)) ?>	
   <?= Alert::widget() ?>
   <div class="grid">
		<div class="col-lg-3 col-md-4 col-xs-12">
            <div class="calculator"><?= Calculator::widget(['action' => Url::to(['/apl/index'])]) ?></div>
        </div>
        <div class="col-lg-9 col-md-8 col-xs-12">
			<?php $form = ActiveForm::begin(['method' => 'post',  'enableClientValidation' => true ]); ?>

				<fieldset><legend>Ustaw nowe hasło</legend>
					<div class="form__row">
						<div class="form__row__left">
							<label for="number_of_member" class="form__label">Stare hasło:</label>
						</div>
						<div class="form__row__right">
							<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
							<?= $form->field($model, 'oldpass', ['template' => sprintf($templateInput, 'key', 'Wpisz hasło, które obecnie służy Ci do autoryzacji w systemie')])
									 //->hint("Nieprawidłowe powtórzenie hasła")
									 ->passwordInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Wpisz hasło']) ?>
						</div>
					</div>
					<div class="form__row">
						<div class="form__row__left">
							<label for="number_of_member" class="form__label">Nowe hasło:</label>
						</div>
						<div class="form__row__right">
							<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
							<?= $form->field($model, 'newpass', ['template' => sprintf($templateInput, 'key', $description['password']), 'hintOptions' => ['class' => 'hint-block'] ])
									 ->hint($description['password'])
									 ->passwordInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Wpisz nowe hasło']) ?>
						</div>
					</div>
					
					<div class="form__row">
						<div class="form__row__left">
							<label for="number_of_member" class="form__label">Powtórz nowe hasło:</label>
						</div>
						<div class="form__row__right">
							<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
							<?= $form->field($model, 'repeatnewpass', ['template' => sprintf($templateInput, 'key', $description['password']), 'hintOptions' => ['class' => 'hint-block'] ])
									 ->hint("Nieprawidłowe powtórzenie hasła")
									 ->passwordInput(['maxlength' => true, 'autocomplete' => "off", 'class' => 'form__input form__input--narrow', 'placeholder' => 'Powtórz nowe hasło']) ?>
						</div>
					</div>
				</fieldset>

				<div class="form__row">
					<div class="form__row__left"></div>
					<div class="form__row__right text-center">
						<input type="text" id="contact-mail" name="contact-mail">
						<button title="Send message" type="submit" class="btn btn--teal">Zapisz nowe hasło</button>
					</div>
				</div>
			
			<?php ActiveForm::end(); ?>
            
        </div>
    </div>

<?php $this->endContent(); ?>

<script type="text/javascript">

	var passInput = document.getElementById('passwordform-newpass');
		passInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	passInput.onchange = validatePassword;
	var passRepeatInput = document.getElementById('passwordform-repeatnewpass');
		passRepeatInput.onpaste = function(e) { 
	    e.preventDefault();
	}
	passRepeatInput.onkeyup = validatePassword;
	
	function validatePassword(){
		$repeatChar = 1; $charTemp = ''; $strPass = passInput.value; $passValid = true;
		for (var i = 0, len = $strPass.length; i < len; i++) {
		    if($charTemp == $strPass[i] && $repeatChar < 4 ) { 
				$repeatChar = $repeatChar + 1;
			} else if ( $repeatChar != 4 ) {
				$repeatChar = 1;
			}
			$charTemp = $strPass[i];
		}

		if($repeatChar >= 4) { $passValid = false;
			passInput.setCustomValidity("Ten sam znak możesz użyć maksymalnie 3 razy z rzędu.");		
		} else if(passInput.value.length < 8) { $passValid = false;
			passInput.setCustomValidity("Hasło nie może mieć mniej niż 8 znaków.");
		} else if( !passInput.value.match(/[a-z]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną małą literę.");
		} else if( !passInput.value.match(/[A-Z]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną dużą literę.");
		} else if( !passInput.value.match(/[0-9]/) ) { $passValid = false;
		   passInput.setCustomValidity("Hasło musi posiadać przynajmniej jedną cyfrę.");
		} else {
		   passInput.setCustomValidity("");
		}
		
		if(!$passValid) {
			passInput.parentElement.parentElement.querySelector('p.hint-block').style.display = "block";
			passInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
			passInput.classList.remove('validate-ok');
            passInput.classList.add('validate-no');
		} else {
			passInput.parentElement.parentElement.querySelector('p.hint-block').style.display = "none";
			passInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
            passInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passInput.classList.add('validate-ok');
            passInput.classList.remove('validate-no');
		}

		if(passInput.value != passRepeatInput.value && passInput.value != '')  {
			//passRepeatInput.setCustomValidity("Passwords Don't Match");
			passRepeatInput.parentElement.parentElement.querySelector('p.hint-block').style.display = "block";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
			passRepeatInput.classList.remove('validate-ok');
            passRepeatInput.classList.add('validate-no');
		} else if(passInput.value == passInput.value) {
			//passRepeatInput.setCustomValidity('');
			passRepeatInput.parentElement.parentElement.querySelector('p.hint-block').style.display = "none";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passRepeatInput.classList.add('validate-ok');
            passRepeatInput.classList.remove('validate-no');
		} else {
			passRepeatInput.parentElement.parentElement.querySelector('p.hint-block').style.display = "none";
			passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            passRepeatInput.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
			passRepeatInput.classList.remove('validate-ok');
            passRepeatInput.classList.remove('validate-no');
		}
	}
</script>