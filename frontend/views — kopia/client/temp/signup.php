<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Rejestracja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
    /*var_dump($model->getErrors());*/
?>
<section class="section">
	<div class="container">
		<div class="site-login">
            <div class="ribbon">
               <h4><?= Html::encode($this->title) ?></h4>
            </div>
			<?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'login']]); ?>
                <h5>Strefa klienta</h5>
				<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

				<?= $form->field($model, 'email') ?>

				<?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                
                <?= $form->field($model, 'rule')->checkbox() ?>
                <?= $form->field($model, 'agree')->checkbox() ?>


				<div class="form-group">
					<?= Html::submitButton('Utwórz konto', ['class' => 'btn btn-lg more_btn1 margin3', 'name' => 'signup-button']) ?>
				</div>
                <a href="<?= Url::to(['/site/page', 'pid' => 2, 'slug' => 'regulamin']) ?>" class="btn more_btn3" ><i class="fa fa-certificate"></i>&nbsp;&nbsp;Przeczytaj regulamin</a>
			<?php ActiveForm::end(); ?>
		    
		</div>
	</div>
</section>
