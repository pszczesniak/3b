<?php
    use yii\helpers\Url;
?>

<div class="offer-post offer-large package<?= $model->id_package_fk ?>">
	<article>
		<header class="entry-header">
			<div class="entry-thumbnail">
                <img alt="avatar" src="<?= (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg':'/uploads/avatars/no-image-big.jpg' ?>" class="img-responsive">
                    <span class="post-format post-format-video"><i class="glyphicon glyphicon-star"></i></span>
                    <div class="entry-price">od <?= ($model->price_from) ? $model->price_from : 0 ?> PLN</div>
                </div>
			<div class="entry-title"><a  href="<?= Url::to(['/site/view', 'id' => $model->id, 'slug' => $model->slug]) ?>"><?= ($model->companyname)?($model->companyname):'brak nazwy' ?></a></div>
			<h4 class="entry-address"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $model->postal_code ?> <?= $model->city ?>, <?= $model->province['name'] ?></h4>
            <h5 class="entry-address"><i class="fa fa-home" aria-hidden="true"></i><?= $model->address ?></h5>
		</header>
<!--
		<div class="entry-content">
            <p><?= $model->note ?></p>
			<div class="align-right"><a class="btn more_btn1" href="<?= Url::to(['/site/view', 'id' => $model->id, 'slug' => $model->slug]) ?>">Czytaj więcej</a></div>
		</div>
-->
		<div class="entry-meta align-right">
            <!--<span class="entry-author"><i class="glyphicon glyphicon-paperclip"></i> <a href="#">Wizytówka</a></span>
            <span class="entry-category"><i class="glyphicon glyphicon-calendar"></i> <a href="#">Terminarz</a></span>
            <span class="entry-comments"><i class="glyphicon glyphicon-comment"></i> <a href="#">15</a></span>-->
            <a href="#" class="btn btn-burgund btn-show-title" data-title="<?= ($model->phone) ? $model->phone : 'brak danych' ?>"><i class="glyphicon glyphicon-earphone"></i><span class="clickToShow" data-icon="earphone" data-showData="<?= ($model->phone) ? $model->phone : 'brak danych' ?>">Wyświetl numer</span></a>
			<a href="#" class="btn btn-burgund btn-show-title" data-title="<?= ($model->email) ? $model->email : 'brak danych' ?>"><i class="glyphicon glyphicon-envelope"></i><span class="clickToShow" data-icon="envelope" data-showData="<?= ($model->email) ? $model->email : 'brak danych' ?>">Kontakt</span></a>
            <span class="btn btn-burgund click-observed" data-action="<?= Url::to(['/site/observed', 'id' => $model->id]) ?>"><i class="glyphicon glyphicon-star<?= ( \Yii::$app->session->get('user.observed') && in_array($model->id, \Yii::$app->session->get('user.observed'))) ? "" : "-empty" ?>"></i><span>Obserwuj</span></span>
        </div>
	</article>
</div>