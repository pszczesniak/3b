<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

$this->title = 'Twojeprzyjecia.pl';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'offer-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <h2><i class="fa fa-user"></i>Twój profil</h4> 
            <h4 class="text-brand">Poniższa informacja będzie wyświetlana przy Twoich opiniach.</h3>
            <div class="row">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>

