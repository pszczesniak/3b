<?php

/* @var $this yii\web\View */

$this->title = 'Twojeprzyjecia.pl';
?>
<section class="bg--admin">
    <div class="status-grid">
        <div class="status-grid-head">
            <div class="status-people-pic">
                <img title="people-name" src="/images/default-user.png" class="stat-people">
                <h2><?= Yii::$app->user->identity->username ?></h2>
                <!--<span>Lorem ipsum lorem ipsum</span>-->
            </div>
        </div>
        
        <div class="status-corrent">
            <ul>
                <li><a href="#" class="testimonial">Opinie</a></li>
                <li><a href="#" class="client">Klienci</a></li>
                <li><a href="#" class="config">Ustawienia</a></li>
                <div class="clear"> </div>
            </ul>
        </div>
        <div class="clear"> </div>
    </div>
</section>
<section class="section section--nopad-top js-aside-height">
	<div class="container">
        <div class="row-g-20">
			<div class="col-xs-2 col-sm-2 col-md-4">
				<div class="page-search-sidebar">
                    <div class="header-1 header-1--arrow header-1--left">
                        <h4> Twój profil </h4>
                        <div class="toggle-wrap">
                            <span class="toggle-bar"></span>
                         </div>
                    </div>
				</div>
			</div>
			<div class="col-xs-10 col-sm-10 col-md-8">
				<div class="page-search-result">

					<div class="header-1 header-1--right">
						<!--<div class="filter-item">
							<label class="filter-item__label">Sortuj: </label>
							<select name="order">
								<option value="price">Cena</option>
								<option value="date">Data</option>
							</select>
						</div>-->

						<div class="filter-item right">
                            <a href="#"><i class="fa fa-map-marker"></i></a>
							<a href="#"><i class="fa fa-envelope"></i> </a>
						</div>

					</div>				
				</div>
			</div>
		</div>
        <div class="row-g-20">
			<div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
				<?= $this->render('_left_panel', ['id' => 0]) ?>
				
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="page-search-result">
					<div class="row">
                        <?= $this->render('_form', ['model' => $model, 'eventsChecked' => $eventsChecked, 'categoriesChecked' => $categoriesChecked, 'events' => $events, 'categories' => $categories]) ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>


<?= $this->render('_events') ?>

<script>
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: 52.173931692568, lng: 18.8525390625}, 
		  zoom: 6
	});
	var infoWindow = new google.maps.InfoWindow({map: map});
	
	google.maps.event.addListener(map,'click',function(event) {
		document.getElementById('svcoffer-pos_lat').value = event.latLng.lat(); 
		document.getElementById('svcoffer-pos_lng').value = event.latLng.lng(); 
	}); 
	
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
	searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
         /* markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];*/

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            /*markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));*/

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });


	// Try HTML5 geolocation.
	if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(function(position) {
		var pos = {
		  lat: position.coords.latitude,
		  lng: position.coords.longitude
		};

		infoWindow.setPosition(pos);
		infoWindow.setContent('Location found.');
		map.setCenter(pos);
        map.setZoom(16);
		
		geocoder = new google.maps.Geocoder();
		
		var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		geocoder.geocode({'latLng': latlng}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			   console.log(results)
				if (results[1]) {
				//alert(results[0].formatted_address)
				document.getElementById('svcoffer-address').value = results[0].formatted_address;
				for (var i=0; i<results[0].address_components.length; i++) {
					for (var b=0;b<results[0].address_components[i].types.length;b++) {

						if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
							//this is the object you are looking for
							city= results[0].address_components[i];
							break;
						}
					}
				}
				//city data
				//alert(city.short_name + " " + city.long_name)
                document.getElementById('svcoffer-city').value = city.short_name;

				} else {
				  alert("No results found");
				}
			} else {
				alert("Geocoder failed due to: " + status);
			}
		});
	  }, function() {
		handleLocationError(true, infoWindow, map.getCenter());
	  });
	} else {
	  // Browser doesn't support Geolocation
	  handleLocationError(false, infoWindow, map.getCenter());
	}
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
infoWindow.setPosition(pos);
infoWindow.setContent(browserHasGeolocation ?
					  'Error: The Geolocation service failed.' :
					  'Error: Your browser doesn\'t support geolocation.');
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrkRGgiRSasfjloZfpmWmIVG8DYJdLkEo&libraries=places&callback=initMap"  async defer></script>
