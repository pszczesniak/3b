<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Svc Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Svc User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_user_fk',
            'id_package_fk',
            'id_country_fk',
            'id_province_fk',
            // 'city',
            // 'postal_code',
            // 'address',
            // 'firstname',
            // 'lastname',
            // 'phone',
            // 'phone_additional:ntext',
            // 'note:ntext',
            // 'first_logged_date',
            // 'last_logged_date',
            // 'status',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'deleted_at',
            // 'deleted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
