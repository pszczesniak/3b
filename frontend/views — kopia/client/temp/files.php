<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */
use frontend\widgets\Uploaderwidget;


$this->title = 'Materiały';
?>

<?php $this->beginContent('@app/views/layouts/offer-window.php',array('model' => $model, 'class'=>'offer-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-file-<?= $type ?>-o"></i>Pliki <?= $type ?></h2>
                <?= Uploaderwidget::widget(['typeId' => ($type=='video')?2:3, 'parentId' => $model->id, 'accept' => "".$type."/*"]) ?>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>

