<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
	
	<div class="form__row">
		<div class="form__row__left">
			<label for="email" class="form__label">Zaznacz na mapie:</label>
		</div>
		<div class="form__row__right google-map">
			<input id="pac-input" class="controls" type="text" placeholder="Search Box">
			<div id="map"></div>
			<?= $form->field($model, 'pos_lat', ['template' => '{input}'])->hiddenInput()->label(false) ?> <?= $form->field($model, 'pos_lng', ['template' => '{input}'])->hiddenInput()->label(false) ?>
		</div>
	</div>
    
    <div class="form__row">
		<div class="form__row__left">
			<label for="telephone" class="form__label">Województwo:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
            <?= $form->field($model, 'id_province_fk', ['template' => '{input}'])->dropdownList(ArrayHelper::map( backend\Modules\Loc\models\LocProvince::find()->orderby('name')->all(), 'id', 'name'), ['class' => 'form__input', 'prompt' => 'Region']); ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="telephone" class="form__label">Miejscowość:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
			<?= $form->field($model, 'city', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Miejscowość']) ?>
		</div>
	</div>

	<div class="form__row">
		<div class="form__row__left">
			<label for="country" class="form__label">Kod pocztowy:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="text" required="" placeholder="Kod pocztowy" name="name-title" class="form__input form__input--title">	 -->
			<?= $form->field($model, 'postal_code', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Kod pocztowy']) ?>
		</div>
	</div>
	
	<div class="form__row">
		<div class="form__row__left">
			<label for="address" class="form__label">Ulica i numer lokalu:</label>
		</div>
		<div class="form__row__right">
			<!-- <input type="text" required="" placeholder="Kod pocztowy" name="name-title" class="form__input form__input--title">	 -->
			<?= $form->field($model, 'address', ['template' => '{input}'])->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Ulica i numer lokalu']) ?>
		</div>
	</div>


	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right text-center">
			<input type="text" id="contact-mail" name="contact-mail">
			<button title="Send message" type="submit" class="btn more_btn2"><?= ($model->isNewRecord) ? 'Utwórz ofertę' : 'Aktualizuj lokalizację' ?> </button>
		</div>
	</div>
<?php ActiveForm::end(); ?>

<script>
window.onload=function(){
   /* $("#container_image").PictureCut({
        InputOfImageDirectory       : "image",
        PluginFolderOnServer        : "/pictures/", 
        FolderOnServer              : "/frontend/web/uploads/",
        EnableCrop                  : true,
        EnableResize                : false,
        CropWindowStyle             : "Bootstrap",
        CropOrientation             : false,
        MinimumWidthToResize        : 1920,
        MinimumHeightToResize       : 1200,
        MaximumSize                 : 4096,
    });*/
    var $uploadCrop;
    
    function dataURLToBlob(dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = parts[1];
            
            return new Blob([raw], {type: contentType});
        }
        else {
            var parts = dataURL.split(BASE64_MARKER);
            var contentType = parts[0].split(':')[1];
            var raw = window.atob(parts[1]);
            var rawLength = raw.length;
            
            var uInt8Array = new Uint8Array(rawLength);
            
            for (var i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }
            
            return new Blob([uInt8Array], {type: contentType});
        }
    }

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();          
            reader.onload = function (e) {
                var allFiles = $("#upload")[0].files;
				var file=allFiles[0];
				var data = new FormData();
				//for(var i = 0; i < files.length; i++) data.append('file'+i, files[i]);
                 data.append('Files[avatar]', $("#upload")[0].files[0]);
				$.ajax({
					type: 'post',
					url: "<?= Url::to(['/offer/upload','id' => $model->id]) ?>",
					data:  data,
					async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
					success: function (data) {
						console.log(data);
                        
                        $uploadCrop.croppie('bind', {
                            url: e.target.result
                            //url: data.file
                        });
                        $('.upload-demo').addClass('ready');
					},
					dataType: "json",
				});

            }           
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 100,
            height: 100,
            type: 'squere'
        },
        boundary: {
            width: 500,
            height: 360
        },
  
    });

    $('#upload').on('change', function () { 
		readFile(this); 
	});
    
    
      
    $('.upload-result').on('click', function (e) { 
        e.preventDefault();
        e.stopPropagation();
        
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            /*popupResult({
                src: resp
            });*/
            //console.log(resp);
            $(".stat-people").attr('src',resp);
            $(".avatar").attr('src',resp);
            
            var img = {avatar: resp };
            $.ajax({
                type: 'post',
                url: "<?= Url::to(['/offer/saveavatar','id' => $model->id]) ?>",
                data:  {avatar: resp },
                dataType: "json",
                async: false,
                cache: false,
                /*contentType: false,
                processData: false,*/
                success: function (data) {
                    console.log(data);
                },
            });
            
        });
        return false;
    });
    
    $('.change-view').on('click', function (e) { 
        e.preventDefault();
        e.stopPropagation();
        
       $uploadCrop.croppie('bind', {
            viewport: {
                width: 100,
                height: 100,
                type: 'squere'
            },
  
      
        });
        return false;
    });


     $uploadCrop.croppie('bind', {
                    url: '/uploads/avatars/avatar-6.jpg'
                });
    
   
}

</script>