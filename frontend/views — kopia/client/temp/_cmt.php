<?php
    use yii\helpers\Url;
?>

<div id="message">
    <?php $status = [0 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'];  $colors = [0 => 'danger', 1 => 'info', 2 => 'success']; ?>
    <ul class="message-container">
        <li class="received">
            <div class="details">
                <div class="left"><?= $model->name ?>
                    <div class="arrow orange"></div><?= '<span class="label label-'.$colors[$model->status].'">'.$status[$model->status].'</span>' ?>
                </div>
                <div class="right"><?= $model->created_at ?></div>
            </div>
            <div class="message">
                <?php for($i=1; $i<=$model->rank;++$i) echo '<i class="fa fa-star"></i>'; ?> <br />
                <?= $model->comment ?>
                <?php if($model->status == 0) {
                        echo  '<hr /><b>Powód odrzucenia:</b><span class="text-danger"> '.$model->deleted_comment .'  [ '.$model->deleted_at.' ]</span>'; 
                    }
                ?>
            </div>
            <?php if($showIcons) { ?>
            
            <form id="comment-form" method="POST"> 
                <div class="form-group field-svccomment-deleted-comment pull-right">
                    <textarea rows="3" name="SvcComment[deleted_comment]" placeholder="Powód odrzucenia"></textarea>
                    <div class="help-block"></div>
                    
                </div>
                <div class="clear"></div>
                <!--<div class="align-right"><button type="submit" class="btn">Odrzuć komentarz</button></div>-->
            
                <div class="tool-box">
                    <a class="circle-icon small fa fa-check" href="<?= Url::to(['/offer/commentaccept', 'id' => $model->id]) ?>" alt="Zaakceptuj" title="Zaakceptuj"></a>
                    <button class="circle-icon small red-hover glyphicon glyphicon-remove" type="submit"  alt="Odrzuć" title="Odrzuć"></a>
                    <!--<a class="circle-icon small red-hover glyphicon glyphicon-flag" href="#"></a>-->
                </div>
            </form>
            <?php } ?>
        </li>

    </ul>

</div>