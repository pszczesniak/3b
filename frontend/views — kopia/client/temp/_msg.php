<?php
    use yii\helpers\Url;
    use yii\widgets\DetailView;

?>

<div id="message">

    <ul class="message-container">
        <li class="received">
            <div class="details">
                <?php $parser = $model->parser; ?>
                <div class="left"> <?= $parser['from'] ?> <div class="arrow orange"></div> <?= $parser['to'] ?>  </div>
                <div class="right"><?= $model->created_at ?></div>
            </div>
            <div class="message">
                
                <?php if($model->query) {
                        $query = $model->query;
                        echo '<h5>Zapytanie z integratora</h5>';
                        echo DetailView::widget([
                            'model' => $model->query,
                            'attributes' => [
                                //'details:html',
                                [                      
                                    'attribute' => 'details:html',
                                    'format'=>'raw',
                                    'label' => 'Szczegóły',
                                    'value' => $query->details,
                                ],
                                'email:email',
                                'message:ntext',
                                'created_at',
                                [                      
                                    'label' => 'Utworzony przez',
                                    'value' => $query->author,
                                ],
                            ],
                        ]);
                    } else {
                        echo $model->message;
                    }
                ?>
                
            </div>
            <?php if($showIcons) { ?>
            <div class="tool-box">
                <a class="circle-icon small fa fa-reply" href="<?= Url::to(['/offer/emailreply', 'id' => $model->id]) ?>" alt="Odpowiedz" title="Odpowiedz"></a>
                <!--<a class="circle-icon small red-hover glyphicon glyphicon-remove" href="#"></a>
                <a class="circle-icon small red-hover glyphicon glyphicon-flag" href="#"></a>-->
            </div>
            <?php } ?>
        </li>
    </ul>

</div>