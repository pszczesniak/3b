<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */

    $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
    $loanData = \yii\helpers\Json::decode($site->custom_data);
    $minLoan = $loanData['amountFrom']; $maxLoan = $loanData['amountTo']; $minPeriod = $loanData['periodFrom']; $maxPeriod = $loanData['periodTo']; $loanNote = (isset($loanData['loanNote'])) ? $loanData['loanNote'] : false;
?>
<form action="<?= Url::to(['/apl/index']) ?>" method="post">
    <?= Alert::widget() ?>
    <label>Jaką kwotę potrzebujesz?</label>
    
    <div class="calculator__range">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', true);">
            <i class="calculator__minus"></i>
        </button>
        <input class="calculator__range-input" type="range" min="<?= $minLoan ?>" max="<?= $maxLoan ?>" step="100" value="2000" name="AplRequest[l_amount]" id="l_amount">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_amount', false);">
            <i class="calculator__plus"></i>
        </button>
    </div>

    <label>Na jak długo?</label>
    <div class="calculator__range">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_period', true);">
            <i class="calculator__minus"></i>
        </button>
        <input class="calculator__range-input" type="range" min="<?= $minPeriod ?>" max="<?= $maxPeriod ?>" step="1" value="24" name="AplRequest[l_period]" id="l_period">
        <button class="calculator__range-button" type="button" onclick="rangeChange('l_period', false);">
            <i class="calculator__plus"></i>
        </button>
    </div>
    <table class="info-tab">
        <tr><td>Wysokość raty</td><th><span id="calc_rate">389,00</span> zł</th></tr>
        <tr><td>Kwota pożyczki</td><th><span id="calc_amount">2 000,00 </span> zł</th></tr>
        <tr><td>Okres kredytowania</td><th><span id="calc_deadline">17.06.2017 </span></th></tr>
        <tr><td>Liczba rat</td><th><span id="calc_period">24</span> m-ce</th></tr>
        <tr><td>Opłaty</td><th><span id="calc_charges">189,00</span> zł</th></tr>
        <tr><td>RRSO</td><th><span id="calc_rrso">137 </span> %</th></tr>
    </table>

    <button  class="btn btn--teal" type="submit" value="Złóż wniosek"> Złóż wniosek</button>
    <div class="info">
        <ul>
            <li><h4>Pożyczki</h4><span>od <?= $minLoan ?> zł do <?= $maxLoan ?> zł</span></li>
            <li><h4>Okres spłaty</h4><span>od <?= $minPeriod ?>  do <?= $maxPeriod ?> miesięcy</span></li>
            <?= ($loanNote) ? '<li><h4>'.$loanNote.'</h4></li>' : '' ?>
        </ul>
    </div>
</form>

<script type="text/javascript">
    var amountSlider = document.getElementById('l_amount');
    var periodSlider = document.getElementById('l_period');

    
    amountSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
    };

    periodSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
	
	function rangeChange(targetId, decrease) {
        var amountSlider = document.getElementById('l_amount');
        var periodSlider = document.getElementById('l_period');

        var targetEl = document.getElementById(targetId);
        var step = parseInt(targetEl.getAttribute('step'));
        var min = parseInt(targetEl.getAttribute('min'));
        var max = parseInt(targetEl.getAttribute('max'));

        if ( (targetEl.value >= max  && !decrease) ||(targetEl.value <= min && decrease) ) {
            return false;
        } else {
            targetEl.value = ( decrease ? parseInt(targetEl.value) - step : parseInt(targetEl.value) + step);
        }

        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);

        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
</script>