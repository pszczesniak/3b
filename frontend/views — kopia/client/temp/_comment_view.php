<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Opinie';


?>
<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'offer-email-view', 'title'=>Html::encode($this->title))) ?>	
    
    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="row">
                <h2><i class="fa fa-envelope"></i>Opinie</h2> 
                <?= Alert::widget() ?>
                <?= $this->render('_cmt', ['model' => $comment, 'showIcons' => false]) ?>
            </div>

        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>