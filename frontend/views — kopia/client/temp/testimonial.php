<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */


$this->title = 'Opinie';
?>

<?php $this->beginContent('@app/views/layouts/client-window.php',array('model' => $model, 'class'=>'client-update', 'title'=>Html::encode($this->title))) ?>	

    <div class="col-xs-0 col-md-4 bg--beige bg--beige--left">
        <?= $this->render('_left_panel', ['model' => $model]) ?>
        
    </div>
    <div class="col-xs-12 col-md-8">
        <div class="page-search-result">
            <div class="user-testimonial">
                <h3><?= $model->companyname ?> </h3><h4>witaj na swojej osi czasu</h4>
                <p>
                    Ktoś skorzystał z Twoich usług? <br /> Dodaj opinię i napisz jak Ci się współpracowało. <br /> Twoje komentarze pojawią się tutaj.
                </p>
                <div class="row">
                    <div class="col-xs-6">
                        <p><a class="user-testimonial-action" href="#"><i class="fa fa-hand-pointer-o fa-5x"></i></a></p>
                        <p>Zapoznaj się ze wskazówkami jak napisać pomocną opinię!</p>
                    </div>
                    <div class="col-xs-6">
                        <p><a class="user-testimonial-action" href="#"><i class="fa fa-check-square-o fa-5x"></i></a></p>
                        <p>Sprawdź jak weryfikujemy opinie!</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<?php $this->endContent(); ?>



<?= $this->render('_events') ?>

