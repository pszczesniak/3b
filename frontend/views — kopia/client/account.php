<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Calculator;

$this->title = 'Twoje konto';

$this->params['breadcrumbs'][] = 'Strefa klienta';
?>

<?php $this->beginContent('@app/views/layouts/account-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false)) ?>	
    <?= Alert::widget([]) ?>
    
    <div class="grid g-mb-40">
      <div class="col-md-6 g-mb-30 g-mb-0--md">
            <div class="bg-primary block-stats">
                <header class="d-flex text-uppercase g-mb-40">
                    <i class="fa fa-credit-card"></i>

                    <div class="g-line-height-1">
                        <h4 class="h5">Kwota pożyczek</h4>
                        <div class="js-counter g-font-size-30" data-comma-separated="true">52,147</div>
                    </div>
                </header>
                <small class="g-font-size-12">11% less than last month</small>
            </div>
        </div>

        <div class="col-md-6">
            <div class="bg-secondary block-stats">
                <header class="d-flex text-uppercase g-mb-40">
                    <i class="fa fa-chart-line"></i>

                    <div class="g-line-height-1">
                        <h4 class="h5">Kwota inwestycji</h4>
                        <div class="js-counter g-font-size-30" data-comma-separated="true">324,056</div>
                    </div>
                </header>
                <small class="g-font-size-12">16% higher than last month</small>
            </div>
        </div>
    </div>  
    
<?php $this->endContent(); ?>

