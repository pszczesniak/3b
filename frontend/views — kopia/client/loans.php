<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Calculator;

$this->title = 'Twoje pożyczki';

$this->params['breadcrumbs'][] = 'Strefa klienta';
?>

<?php $this->beginContent('@app/views/layouts/account-container.php',array('model' => false, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false)) ?>	
    <?= Alert::widget([]) ?>
    
    <div class="card-block g-pa-0">
    <!-- Product Table -->
    <div class="table-responsive">
      <table class="table table-bordered u-table--v2">
        <thead class="text-uppercase g-letter-spacing-1">
          <tr>
            <th>Kwota</th>
            <th>Okres</th>
            <th>Całkowity koszt</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody>
            <tr>
                <td class="align-middle text-nowrap">  2 000 </td>
                <td class="align-middle">  5 miesięcy </td>
                <td class="align-middle">  2 300 </td>
                <td class="align-middle text-nowrap" align="center">
                    <div class="js-pie g-color-purple g-mb-5" data-circles-value="54" data-circles-max-value="100" data-circles-bg-color="#d3b6c6" data-circles-fg-color="#9b6bcc" data-circles-radius="30" data-circles-stroke-width="3" data-circles-additional-text="%" data-circles-duration="2000" data-circles-scroll-animate="true" data-circles-font-size="14" id="hs-pie-1">
                        <div class="circles-wrp" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60">
                            <path fill="transparent" stroke="#d3b6c6" stroke-width="3" d="M 29.994195313694686 1.5000005911295347 A 28.5 28.5 0 1 1 29.96041407176496 1.5000274920433334 Z" class="circles-maxValueStroke"></path>
                            <path fill="transparent" stroke="#9b6bcc" stroke-width="3" d="M 29.994195313694686 1.5000005911295347 A 28.5 28.5 0 1 1 22.943363074828525 57.61256734362646 " class="circles-valueStroke"></path></svg>
                            <div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 14px; height: 60px; line-height: 60px;">54%</div>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="align-middle text-nowrap">  2 000 </td>
                <td class="align-middle">  5 miesięcy </td>
                <td class="align-middle">  2 300 </td>
                <td class="align-middle text-nowrap" align="center">
                    <div class="js-pie g-color-purple g-mb-5" data-circles-value="54" data-circles-max-value="100" data-circles-bg-color="#d3b6c6" data-circles-fg-color="#9b6bcc" data-circles-radius="30" data-circles-stroke-width="3" data-circles-additional-text="%" data-circles-duration="2000" data-circles-scroll-animate="true" data-circles-font-size="14" id="hs-pie-1">
                        <div class="circles-wrp" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60">
                            <path fill="transparent" stroke="#d3b6c6" stroke-width="3" d="M 29.994195313694686 1.5000005911295347 A 28.5 28.5 0 1 1 29.96041407176496 1.5000274920433334 Z" class="circles-maxValueStroke"></path>
                            <path fill="transparent" stroke="#9b6bcc" stroke-width="3" d="M 29.994195313694686 1.5000005911295347 A 28.5 28.5 0 1 1 22.943363074828525 57.61256734362646 " class="circles-valueStroke"></path></svg>
                            <div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 14px; height: 60px; line-height: 60px;">54%</div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
      </table>
    </div>
    <!-- End Product Table -->
  </div>
    
<?php $this->endContent(); ?>

