<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $description['bank_name'] = 'Wpisz całą nazwę banku, w którym masz rachunek bankowy';
    $description['bank_account_number'] = 'To jest Twój unikalny 26-cyfrowy numer konta bankowego. Wpisz numer bez żadnych przerw.';
    $description['bank_how_long_use'] = '';
    $description['frequency_payment'] = '';
    $description['use_credit_card'] = '';
    $description['monthly_income'] = 'To wysokość Twojego całkowitego dochodu pozyskiwanego z różnych źródeł, po odliczeniu należnych podatków.';
    $description['monthly_expenses'] = 'To wysokość Twoich comiesięcznych wydatków uwzględniających np. wynajem mieszkania, zakup żywności czy rachunki za media (inne ewentualne pożyczki nie są tu brane pod uwagę).';
    $description['monthly_other_loans'] = 'To wysokość comiesięcznych obciążeń związanych ze spłatą innych pożyczek.';
    $description['loan_purpose'] = 'Wybierz jedną opcję z rozwijanej listy.';
    $description['loan_day_of_payment'] = 'Wybierz jedną opcję z rozwijanej listy.';

    $templateInput = '<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="bottom" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';
?>
<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/client/finance'])]); ?>
    <?= ( $model->getErrors() ) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <fieldset><legend>Twoje finanse</legend>
        <div class="form__row">
            <div class="form__row__left">
                <label for="bank_name" class="form__label">Nazwa banku:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'bank_name', ['template' => sprintf($templateInput, 'institution', $description['bank_name'])])
                         ->hint($description['bank_name'])
                         ->textInput(['minlength' => 3, 'maxlength' => 50, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Nazwa banku']) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="bank_account_number" class="form__label">Numer konta:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'bank_account_number', ['template' => sprintf($templateInput, 'credit-card', $description['bank_account_number'])])
                        ->hint($description['bank_account_number'])
                        ->textInput(['minlength' => 26, 'maxlength' => 26, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Numer konta', 'pattern' => "[0-9]{26}"]) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="bank_how_long_use" class="form__label">Od jak dawna jesteś klientem banku:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                    <?= $form->field($model, 'bank_how_long_use', ['template' => sprintf($templateInput, 'calendar', $description['bank_how_long_use']),  'options' => ['class' => '']])
                             ->hint($description['bank_how_long_use'])
                             ->dropdownList(\backend\Modules\Apl\models\AplCustomer::periodBank(), ['class' => 'form__input', 'prompt' => '-- wybierz --']); ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="use_credit_card" class="form__label">Czy posiadasz kartę kredytową:</label>
            </div>
            <div class="form__row__right switch-field">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?php /*$form->field($model, 'use_credit_card')->radioList([1 => 'TAK', 0 => 'NIE'])->label(false);*/ ?>  
                <?= $form->field($model, 'use_credit_card')->radioList([1 => 'TAK', 0 => 'NIE'], 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ])->label(false) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="frequency_payment" class="form__label">Częstotliowść wynagrodzenia:</label>
            </div>
            <div class="form__row__right switch-field">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?php /*$form->field($model, 'frequency_payment')->radioList([1 => 'TAK', 0 => 'NIE'])->label(false);*/ ?>  
                <?= $form->field($model, 'frequency_payment')->radioList([1 => 'tygodniowo', 2 => 'miesięcznie'], 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ])->label(false) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="monthly_income" class="form__label">Całkowity miesięczny dochód netto:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'monthly_income', ['template' => sprintf($templateInput, 'usd', $description['monthly_income'])])
                         ->hint($description['monthly_income'])
                         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Podaj kwotę', 'pattern' => "[0-9]+(\.[0-9][0-9]?)?"]) ?>
            </div>
        </div>
        
        <div class="form__row">
            <div class="form__row__left">
                <label for="monthly_expenses" class="form__label">Całkowity miesięczne wydatki:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'monthly_expenses', ['template' => sprintf($templateInput, 'usd', $description['monthly_expenses'])])
                         ->hint($description['monthly_expenses'])
                         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Podaj kwotę', 'pattern' => "[0-9]+(\.[0-9][0-9]?)?"]) ?>
            </div>
        </div>

        <div class="form__row">
            <div class="form__row__left">
                <label for="monthly_other_loans" class="form__label">Miesięczne obciążenia z tytułu innych pożyczek:</label>
            </div>
            <div class="form__row__right">
                <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                <?= $form->field($model, 'monthly_other_loans', ['template' => sprintf($templateInput, 'usd', $description['monthly_other_loans'])])
                         ->hint($description['monthly_other_loans'])
                         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow form-control', 'placeholder' => 'Podaj kwotę', 'pattern' => "[0-9]+(\.[0-9][0-9]?)?"]) ?>
            </div>
        </div>        

    </fieldset>
   
	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right text-center">
			<input type="text" id="contact-mail" name="contact-mail">
			<button title="Send message" type="submit" class="btn btn--teal">Zapisz zmiany</button>
		</div>
	</div>
<?php ActiveForm::end(); ?>
<script type="text/javascript">    
    document.getElementById('aplcustomer-monthly_income').onkeyup = function(e){
        var key = e.which ? e.which : event.keyCode;
        if(key == 110 || key == 188){
          e.preventDefault();
          var value = e.target.value;         
          this.value = value.replace(",",".");
        }   
    };
    
    var selectLists = document.querySelectorAll('select.form-control'); 
    for (var s = 0; s < selectLists.length; s++) {
        selectLists[s].onchange = function(event) {
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            } else {
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.classList.add('validate-ok');
            }
        }
    }
    
    var inputLists = document.querySelectorAll('input.form-control');
    for (var i = 0; i < inputLists.length; i++) {
        /*inputLists[i].addEventListener('input', function () {
            console.log(this.value);
        });*/
        inputLists[i].addEventListener('change', function(event) {
            if (event.target.validity.valid) {
                event.target.classList.add('validate-ok');
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "none";
                event.target.parentElement.parentElement.querySelector('div.help-block').style.display = "none";
            } else {
                //Field contains invalid data.
                event.target.classList.remove('validate-ok');
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
            }
            if(event.target.value == '') {
                event.target.parentElement.querySelector('span.input-icon').classList.remove("input-icon--green");
                event.target.parentElement.querySelector('span.input-icon').classList.add("input-icon--red");
                event.target.parentElement.parentElement.querySelector('div.hint-block').style.display = "block";
                event.target.classList.remove('validate-ok');
                event.target.classList.add('validate-no');
            }
        }, false);
    }
</script>
