<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */

use frontend\widgets\Calculator;

$this->title = 'Twoje konto';

$this->params['breadcrumbs'][] = 'Strefa klienta';
?>

<?php $this->beginContent('@app/views/layouts/account-container.php',array('model' => $model, 'class'=>'client-account', 'title'=>Html::encode($this->title), 'back' => false)) ?>	
    <?= Alert::widget([]) ?>
    <div class="g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-40">
        <div class="grid">
            <div class="col-lg-4 g-mb-40 g-mb-0--lg">
                <div class="g-mb-20"> <img class="img-fluid w-100" src="/images/avatar.png" alt="Image Description">  </div>
                <a class="btn btn-icon btn-block" href="#!">  <i class="fa fa-edit"></i> Edytuj dane  </a>
            </div>
            <div class="col-lg-8">
                <div class="d-flex align-items-center justify-content-sm-between g-mb-5">
                    <h2 class="g-font-weight-300 g-mr-10"><?= $model->fullname ?></h2>
                    <div class="social-container">
                        <ul class="social-icons">
                            <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-codepen"></i></a></li>
                        </ul>
                    </div>
                </div>

                <ul class="list-inline g-font-weight-300">
                    <li class="list-inline-item g-mr-20">
                      <i class="fa fa-map-marker"></i> Katowice, Polska
                    </li>
                    <li class="list-inline-item g-mr-20">
                        <i class="fa fa-check"></i> zweryfikowany
                    </li>

                </ul>

                <hr class="g-brd-gray-light-v4 g-my-20">

                <p class="lead g-line-height-1_8">Nulla ipsum dolor sit amet, consectetur adipiscing elitut eleifend nisl.</p>

   
            </div>
        </div>
    </div>

    <?= $this->render('_actions') ?>       
<?php $this->endContent(); ?>

