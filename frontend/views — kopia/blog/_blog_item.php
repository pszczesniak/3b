<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Artykuł';
?>
<div class="blog-post clearfix">
	<div class="blog-post-cats" >
		<a href="#" rel="category tag"><?= $data->catalog['title'] ?></a>		
    </div>
	
	<div class="blog-post-title">
		<h2><a href="<?= Url::to(['/blog/view', 'id' => $data->id, 'title' => $data->title]) ?>">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></h2>
	</div><!-- .blog-post-title -->			


    <div class="blog-post-thumb-small" style="background: url(/uploads/blog/<?= $data->id ?>/original.jpg)">  </div>
    <div class="blog-post-excerpt">
        <?= $data->brief ?>
    </div>
    <div class="blog-post-read-more">
        <a href="<?= Url::to(['/blog/view', 'id' => $data->id, 'title' => $data->title]) ?>">CZYTAJ DALEJ ...</a>
    </div>
    
</div>