<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use frontend\widgets\cms\MainMenu;
use frontend\widgets\Contactwidget;

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <title>Investor</title>
    <link rel="stylesheet" href="/css/critical.css">

    <meta name="robots" content="noindex, nofollow">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="application-name" content="Web Starter Kit">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#2F3BA2">


    <meta property="og:locale" content="en_GB">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Traditional Boilerplate - Bigger Picture">
    <meta property="og:description" content="Description here...">
    <meta property="og:url" content="http://www.biggerpicture.agency">
    <meta property="og:site_name" content="Application name">
    <meta property="og:image" content="/img/touch/chrome-touch-icon-192x192.png">

    <!-- Color the status bar on mobile devices -->
    <meta name="theme-color" content="#2F3BA2">
</head>

<body>
    <div class="client">
        <div class="client-login">
            <div class="client-login-more" style="background-image: url('/images/hp_banner.jpg');"></div>
            <!-- fotolia_56933731 -->
            <div class="client-login-wrap">
                <?= $content; ?>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,600,700|Signika:300,400&amp;subset=latin-ext" media="none" onload="if(media!='all')media='all'">
    <script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/all.css?v=<?php echo date ("ymdHis", filemtime("./css/all.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>
    <script type="text/javascript" async src="/js/all.js?v=<?php echo date ("ymdHis", filemtime("js/all.js"));?>"></script>

</body>

</html>
