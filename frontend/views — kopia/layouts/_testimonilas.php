<section class="section bg-beige">
    <div class="container">
        <div class="grid grid--40">
            <div class="col-md-6 col-xs-12">
                <h2 class="headline-1 headline-underline color-dark4">Trochę o Nas...</h2>
                <div class="content color-smoke">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias iure facere dolorum adipisci eum.</p>
                </div>
            </div>

            <div class="col-md-6 col-xs-12">
                <div class="testimonial testimonial--white">
                    <figure class="testimonial__avatar">
                        <img class="js-lazy" data-src="/images/avatar.png" alt="">
                    </figure>
                    <div class="testimonial__content">
                        <blockquote class="blockquote">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, architecto, explicabo perferendis nostrum, maxime impedit atque odit sunt pariatur illo obcaecati soluta molestias.</p>
                            <footer class="blockquote__footer">
                                Jan Kowalski
                            </footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>