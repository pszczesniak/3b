<div id="blog-sidebar">
    <div id="blog-sidebar-inner">
        <section class="widget">
            <h2 class="widget-title" >Wyszukaj...</h2>
            <div class="posts-list-alt-widget clearfix">
                <form class="form">
                    <h5>Kwota</h5>
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12">
                            <label class="input__label input__label--juro"> od</label>
                            <input class="input__field input-gradient" type="text">
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <label class="input__label input__label--juro"> od</label>
                            <input class="input__field input-gradient" type="text">
                        </div>
                    </div>
                    <h5>Okres</h5>
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12">
                            <label class="input__label input__label--juro"> od</label>
                            <input class="input__field input-gradient" type="text">
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <label class="input__label input__label--juro"> od</label>
                            <input class="input__field input-gradient" type="text">
                        </div>
                    </div>
                    <div class="container-form-btn align-right">
                        <button class="btn btn-purple">  Wyszukaj </button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>