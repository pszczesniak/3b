<footer class="l-footer">
    <div class="container l-footer__wrapper">
        <div class="l-footer__logo">
            <a href="" title="" aria-label="Strona głowna link stopki">
                <img class="js-lazy" data-src="/images/logo.png" alt="">
            </a>
        </div>

        <div class="l-footer__right">
            <div class="grid">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4 class="l-footer__label">Przydatne linki</h4>
                    <ul class="l-footer__2cols">
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                        <li>
                            <a class="l-footer__link" href="">Lorem ipsum</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4 class="l-footer__label">Kontakt</h4>

                    <address class="contact-info">
                        <ul class="list">
                            <li>
                                <a href="callto:+1234567890">(123) 45678910</a>
                            </li>
                            <li>
                                <a href="mailto:email@email.pl">email@email.pl</a>
                            </li>
                            <li>
                                <span>Pon - Sob: 9:00 - 18:00</span>
                            </li>
                            <li>
                                <a href="#LINK_DO_MAPY">
                                    xxx<br>
                                    42-200 Częstochowa
                                </a>
                            </li>
                        </ul>
                    </address>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4 class="l-footer__label">Subskrybuj</h4>
                    <form class="form">
                        <label for="subscription-email">Email:</label>
                        <input type="email" id="subscription-email" name="subscription-email" class="form__input" placeholder="example@email.com">
                        <p>
                            <button type="submit" class="btn">Wsyślij</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>


        <div class="l-footer__social">
            <span>Znajdziesz Nas:</span>
            <ul class="l-footer__social-list">
                <li>
                    <a href="">F</a>
                </li>
                <li>
                    <a href="">T</a>
                </li>
                <li>
                    <a href="">G</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="l-footer__bottom">
        <div class="container">
            <p>
                © <?= date('Y') ?> 3b-artstudio | <a href="">Privacy policy</a>
            </p>
        </div>

    </div>
</footer>