<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use frontend\widgets\cms\MainMenu;
use frontend\widgets\Contactwidget;

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <title>3b-artstudio</title>
    <link rel="stylesheet" href="/css/critical.css">

    <meta name="robots" content="noindex, nofollow">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="application-name" content="Web Starter Kit">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#2F3BA2">


    <meta property="og:locale" content="en_GB">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Traditional Boilerplate - Bigger Picture">
    <meta property="og:description" content="Description here...">
    <meta property="og:url" content="http://www.biggerpicture.agency">
    <meta property="og:site_name" content="Application name">
    <meta property="og:image" content="/img/touch/chrome-touch-icon-192x192.png">

    <!-- Color the status bar on mobile devices -->
    <meta name="theme-color" content="#2F3BA2">
</head>

<body>
    <nav class="l-navbar">
        <!-- <a class="l-navbar__logo" href="/" title="SimplyHealth">
            <svg width="250px" height="88px" class="fill-white"><use xlink:href="/images/icons.svg#icon-simplyhealth"/></svg>
        </a> -->

        <div class="l-navbar__right">
            <button class="l-navbar__button js-search" type="button" aria-label="Otwórz panel szukania"><span class="icon-search"></span></button>
            <button class="l-burger js-burger" type="button" aria-label="Pokaż menu"><span class="l-burger__line"></span></button>
            <a href="<?= Url::to(['/client/login']) ?>" class="btn btn-blue">Moje<br />konto</a>
        </div>
    </nav>

    <div class="js-megamenu megamenu">
        <button class="megamenu__close js-burger-close" type="button" aria-label="Zamknij menu"><span class="megamenu__close-line"></span></button>
        <div class="container">
			<div class="grid">
				<div class="col-sm-6 col-xs-12">
					<nav class="megamenu__nav">
						<!-- <button class="megamenu__close js-burger-close" type="button"><span class="megamenu__close-line"></span></button> -->

						<ul class="megamenu__list">
							<li class="megamenu__item">
								<a class="megamenu__link" href="/" title="">Foto &amp; Video</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="/blog" title="">Blog</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 1</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 2</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 3</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 4</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 5</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="col-sm-6 col-xs-12">
					<nav class="megamenu__nav">
						<!-- <button class="megamenu__close js-burger-close" type="button"><span class="megamenu__close-line"></span></button> -->

						<ul class="megamenu__list">
							<li class="megamenu__item">
								<a class="megamenu__link" href="/" title="">Fotobudka</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="/blog" title="">Blog</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 1</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 2</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 3</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 4</a>
							</li>
							<li class="megamenu__item">
								<a class="megamenu__link" href="" title="">Link do strony 5</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
        </div>
    </div>

    <!-- SEARCH LAYER -->
    <div class="search js-search-layer">
        <button class="search__close js-search-close" type="button" aria-label="Zamknij panel szukania"><span class="search__close-line"></span></button>
        <div class="container">
            <form method="get" action="/search">
                <label class="search__headline" for="phrase">Zacznij pisać a potem naciśniej enter</label>
                <input type="text" id="phrase" name="phrase" class="search__input js-search-input" autofocus>

                <button class="btn" type="submit">Szukaj</button>
            </form>
        </div>
    </div>
    <!-- END SEARCH LAYER -->
    <?= $content ?>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,600,700|Signika:300,400&amp;subset=latin-ext" media="none" onload="if(media!='all')media='all'">
    <script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/all.css?v=<?php echo date ("ymdHis", filemtime("./css/all.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>
    <script type="text/javascript" async src="/js/all.js?v=<?php echo date ("ymdHis", filemtime("js/all.js"));?>"></script>

    <!--<script src="//localhost:35729/livereload.js"></script>-->
</body>

</html>
