<div id="blog-sidebar">
    <div id="blog-sidebar-inner">
        <section>
			<div class="about-author-widget">
				<div class="about-author-signature-image"><img src="/images/nologo.png" alt="Logo"></div>
                <div class="about-author-widget-info">
                    <h2 class="about-author-widget-name" >Lorem ipsum dolor sit amet</h2>
                    <div class="about-author-widget-text" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum est purus, sit amet posuere tortor finibus non. Sed pretium dui sit amet imperdiet mollis. </div>
                </div>
            </div>
		</section>
        <?= $this->render('sidebar/_recents_posts') ?>
        <?= $this->render('sidebar/_categories') ?>
        <?= $this->render('sidebar/_tags') ?>
    </div>
</div>

		