<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<?php /*$this->beginContent($controller->module->mainLayout)*/ ?>
<div class="client">
    <div class="client-login">
        <div class="client-login-more" style="background-image: url('/images/hp_banner.jpg');"></div>
        <!-- fotolia_56933731 -->
        <div class="client-login-wrap">
            <?= $content; ?>
        </div>
    </div>
</div>
<script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/admin.css?v=<?php echo date ("ymdHis", filemtime("./css/admin.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>