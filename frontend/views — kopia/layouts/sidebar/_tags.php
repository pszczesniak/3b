<?php
	use yii\helpers\Url;
    use yii\helpers\Html;
    
    $tags = \backend\Modules\Blog\models\BlogTag::find()->where(['status' => 1])->orderby('name')->all();

?>
<section class="widget">
    <h2 class="widget-title" >Tagi</h2>
    <div class="posts-list-alt-widget clearfix">
        <ul class="blog-tags-cloud">
            <?php
                foreach($tags as $key => $tag) {
                    echo '<li><a href="'.Url::to(['/blog/index']).'?tag='.$tag->name.'">'.$tag->name.'</a></li>';
                }
            ?>
        </ul>
    </div>
</section>