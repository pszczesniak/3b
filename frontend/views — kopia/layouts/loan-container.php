<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>

<header class="l-header l-header--page">
    <div class="l-header--page_title">
        <h4 class="page-title"><?= $title ?></h4>
         <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => [
                        'label' => '<i class="fa fa-home"></i>'/* . Yii::t('yii', 'Home')*/,
                        'url' => Yii::$app->homeUrl,
                        'encode' => false// Requested feature
                    ],
            ]); ?>
        
		</div>
    </div>
</header>

<main>
    <?php if($class == 'homepage') { ?>
        <?= $content ?>
    <?php } else { ?>
        <section class="section">
            <div class="container">
                <div class="grid">
                    <div class="col-md-8 col-sm-7 col-xs-12"> 
                        <?= $content ?>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                         <?= $this->render('_search') ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>

<?= $this->render('_testimonilas') ?>
</main>
<?= $this->render('_footer_new') ?>
<script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/admin.css?v=<?php echo date ("ymdHis", filemtime("./css/admin.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>