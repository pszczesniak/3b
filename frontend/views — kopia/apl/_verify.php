<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */

$templateInput = '<div class="wrap-input validate-input %s" data-validate="Name is required">'
                       .'<span class="label-input">{label}</span><div class="wrap-input-error">{error}</div>'
                        .'{input}'
                        .'<span class="focus-input"></span>'
                    .'</div>';
    /*'<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="bottom" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';*/
?>

<?php $form = ActiveForm::begin(['method' => 'post',  
                                 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['class' => 'form'] ]); ?>
    <span class="form-title"> Weryfikacja  </span>

    <?= $form->field($model, 'v_code_check', ['template' => sprintf($templateInput, (isset($model->getErrors()['v_code_check']) ? 'alert-validate' : ''))])
                             ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz kod weryfikacyjny...', 'title' => 'Kod weryfikacyjny'])  ?>

    <div class="flex-m w-full">
        <div class="form-checkbox">
            <input class="input-checkbox" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox" for="ckb1">
                <span class="txt1"> Wyrażam zgodę na  <a href="#" class="txt2 hov1"> Warunki użytkownika  </a> </span>
            </label>
        </div>
    </div>

    <div class="container-form-btn">
        <div class="wrap-form-btn">
            <div class="form-btn-bgbtn"></div>
            <button class="form-btn">  Potwierdź  </button>
        </div>

    </div>
<?php ActiveForm::end(); ?>
