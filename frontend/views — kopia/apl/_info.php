<?php
    $site = \backend\Modules\Cms\models\CmsSite::findOne(1);
    $loanData = \yii\helpers\Json::decode($site->custom_data);
    $minLoan = $loanData['amountFrom']; $maxLoan = $loanData['amountTo']; $minPeriod = $loanData['periodFrom']; $maxPeriod = $loanData['periodTo']; 
?>
<div class="calculator">

</div>
<div class="apl-info">
    <?php $items = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 5])->all(); ?>
    <?php
        if($items) {
            echo '<ul>';
                foreach($items as $key => $item) {
                    echo '<li>'
                            .'<h3>'.$item->title.'</h3>'
                            .'<p>'.$item->content.'</p>'
                        .'</li>';
                }
            echo '</ul>';
        }
    ?>

</div>

<script type="text/javascript">
    var amountSlider = document.getElementById('l_amount');
    var periodSlider = document.getElementById('l_period');

    
    amountSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });
    };

    periodSlider.onchange = function () {
        amountVal = parseInt(amountSlider.value);
        periodVal = parseInt(periodSlider.value);
        
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
	
	function rangeChange(targetId, decrease) {
        var amountSlider = document.getElementById('l_amount');
        var periodSlider = document.getElementById('l_period');

        var targetEl = document.getElementById(targetId);
        var step = parseInt(targetEl.getAttribute('step'));
        var min = parseInt(targetEl.getAttribute('min'));
        var max = parseInt(targetEl.getAttribute('max'));

        if ( (targetEl.value >= max  && !decrease) ||(targetEl.value <= min && decrease) ) {
            return false;
        } else {
            targetEl.value = ( decrease ? parseInt(targetEl.value) - step : parseInt(targetEl.value) + step);
        }

        var amountVal = parseInt(amountSlider.value);
        var periodVal = parseInt(periodSlider.value);

        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {amount: amountVal, period: periodVal},
            url: "/apl/calc",
    
            success: function(result) {
                $.each(result,function(index, value){
                    //console.log('My array has at position ' + index + ', this value: ' + value);
                    document.getElementById('calc_'+index).innerHTML = value;
                });
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                alert('Something went to wrong.Please Try again later...');
            }
        });

    };
</script>