<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $templateInput = '<div class="input-icon-wrap"><span class="input-icon"><span class="fa fa-user"></span></span>{input}<span class="input-info"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" data-original-title="this is a left tooltip"></i></div>';
?>
<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/apl/accept', 'cid' => 1])]); ?>
    <fieldset><legend>Twój wniosek o pożyczkę - podsumowanie</legend>
        <div class="grid">
            <div class="col-sm-6 col-xs-12">
                <div class="form__row">
                    <div class="form__row__left">
                        <label for="l_amount" class="form__label">Kwota pożyczki:</label>
                    </div>
                    <div class="form__row__right">
                        <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                        <?= $form->field($model, 'l_amount', ['template' => $templateInput])
								 ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Kwota pożyczki']) ?>
                    </div>
                </div>
                <div class="form__row">
                    <div class="form__row__left">
                        <label for="l_period" class="form__label">Okres spłaty:</label>
                    </div>
                    <div class="form__row__right">
                        <!-- <input type="tel" required="" id="telephone" placeholder="Miejscowość" name="telephone" class="form__input form__input--narrow"> -->
                        <?= $form->field($model, 'l_period', ['template' => $templateInput])
						         ->textInput(['maxlength' => true, 'class' => 'form__input form__input--narrow', 'placeholder' => 'Kwota pożyczki']) ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
            </div>
        </div>
    </fieldset>


	<div class="form__row">
		<div class="form__row__left"></div>
		<div class="form__row__right text-center">
			<a href="<?= Url::to(['/apl/send', 'lid' => $model->id]) ?>" title="Wyślij wniosek" class="btn btn--green btn--full-width">Wyślij wniosek</a><br />
            <a href="<?= Url::to(['/apl/discard', 'lid' => $model->id]) ?>" title="Odrzuć wniosek" class="btn btn--red btn--full-width">Odrzuć wniosek</a><br />
            <a href="<?= Url::to(['/client/account']) ?>" title="Zapisz wniosek" class="btn btn--blue btn--full-width">Zapisz wniosek</a>
		</div>
	</div>
<?php ActiveForm::end(); ?>

