<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */
/* @var $form yii\widgets\ActiveForm */

$templateInput = '<div class="wrap-input validate-input %s" data-validate="Name is required">'
                       .'<span class="label-input">{label}</span><div class="wrap-input-error">{error}</div>'
                        .'{input}'
                        .'<span class="focus-input"></span>'
                    .'</div>';
    /*'<div class="input-icon-wrap">'
                        .'<span class="input-icon">'
                            .'<span class="fa fa-%s"></span>'
                        .'</span>{input}'
                        .'<span class="input-info" data-toggle="popover" data-placement="bottom" data-original-title="Pomoc" data-content="%s">'
                            .'<i class="fa fa-info-circle"></i>' 
                        .'</span>'
                     .'</div>{error}{hint}';*/
?>

<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/apl/register', 'rid' => $model->id_request_fk]), 
                                 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['class' => 'form'] ]); ?>
    <span class="form-title"> Załóż konto  </span>

    <?= $form->field($model, 'firstname', ['template' => sprintf($templateInput, (isset($model->getErrors()['firstname']) ? 'alert-validate' : ''))])
                             ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz imię...', 'title' => 'Imię'])  ?>
    
    <?= $form->field($model, 'lastname', ['template' => sprintf($templateInput, (isset($model->getErrors()['lastname']) ? 'alert-validate' : ''))])
                             ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz nazwisko...', 'title' => 'Imię'])  ?>

    <?= $form->field($model, 'email', ['template' => sprintf($templateInput, (isset($model->getErrors()['email']) ? 'alert-validate' : ''))])
                             ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz adres e-mail...', 'title' => 'Imię', 'data-validate' => "Valid email is required: ex@abc.xyz"])  ?>   
    <!--<div class="wrap-input validate-input alert-validate" data-validate="Valid email is required: ex@abc.xyz">
        <span class="label-input">Email</span>
        <input class="input" type="text" name="email" placeholder="wpisz adres e-mail...">
        <span class="focus-input"></span>
    </div>-->
    <?= $form->field($model, 'password', ['template' => sprintf($templateInput, (isset($model->getErrors()['password']) ? 'alert-validate' : ''))])
                             ->passwordInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => '***********', 'title' => 'Imię'])  ?>
                             
    <?= $form->field($model, 'repeat_password', ['template' => sprintf($templateInput, (isset($model->getErrors()['repeat_password']) ? 'alert-validate' : ''))])
                             ->passwordInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => '***********', 'title' => 'Imię'])  ?>

    <div class="flex-m w-full">
        <div class="form-checkbox">
            <input class="input-checkbox" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox" for="ckb1">
                <span class="txt1"> Wyrażam zgodę na  <a href="#" class="txt2 hov1"> Warunki użytkownika  </a> </span>
            </label>
        </div>
    </div>

    <div class="container-form-btn">
        <div class="wrap-form-btn">
            <div class="form-btn-bgbtn"></div>
            <button class="form-btn">  Przejdź do weryfikacji  </button>
        </div>

        <a href="<?= Url::to(['/client/login']) ?>" class="dis-block txt3">  Zaloguj się <i class="fas fa-long-arrow-right m-l-5"></i>   </a>
    </div>
<?php ActiveForm::end(); ?>
