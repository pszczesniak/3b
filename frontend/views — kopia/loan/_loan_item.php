<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Artykuł';
?>

<div class="loan-item" >
    <div class="item-content-wrap">
        <div class="item-thumbnail">
            <a href="#">
                <div class="item-thumbnail-wrap" style="background-image: url('/images/avatar.png')"></div>
            </a>
        </div>

        <div class="item-content">
            <div class="item-title">
                <a href="#"><h3><?= number_format($data->l_amount, 2, '.', ' ').' / '.$data->l_period.' mc' ?></h3> <span class="subtitle"><?= $data->customer['fullname'] ?></span></a>
            </div>
            <div class="item-text">
                <div class="item-excerpt txtrows-3"><p><?= mb_strimwidth($data->l_purpose, 0, 100, "...") ?></p></div>
            </div>
            <div class="item-location"><p><?= ($data->customer['address_city']) ? $data->customer['address_city'] : 'brak danych' ?></p></div>
        </div>
    </div>
    <div class="item-info-wrap">
        <div class="review-stars-container">
            <div class="content">
				<span class="review-stars" data-score="4.6" title="" style="font-size: 16px;"><i class="fa fa-star" title=""></i>&nbsp;<i class="fa fa-star" title=""></i>&nbsp;<i class="fa fa-star" title=""></i>&nbsp;<i class="fa fa-star" title=""></i>&nbsp;<i class="fa fa-star-half-o" title=""></i><input type="hidden" name="score" value="4.6" readonly="readonly"></span>
            </div>
        </div>
        <div class="item-web icon-label">
            <i class="fa fa-phone"></i> <a href="#"><?= ($data->customer['phone']) ? $data->customer['phone'] : 'brak danych' ?> </a>
        </div>

        <div class="item-mail icon-label">
            <i class="fa fa-envelope"></i> <a href="#" target="_top"><?= ($data->customer['email']) ? $data->customer['email'] : 'brak danych' ?></a>
        </div>
        
        <div class="item-actions">
            <a href="" class="btn btn--white">Inwestuj</a>
        </div>
    </div>
</div>
