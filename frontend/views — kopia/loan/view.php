<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use frontend\widgets\blog\Search;
/* @var $this yii\web\View */

$this->title = 'Blog';
?>

<?php $this->beginContent('@app/views/layouts/web-container.php',array('model' => false, 'class' => 'page', 'title'=>Html::encode($this->title), 'back' => false)) ?>
    <?= $this->render('_blog_article', [  'data' => $post,  ]); ?>
    <?= $this->render('_blog_comments', ['post' => $post,'comments' => $comments, 'comment' => $comment,]); ?>
<?php $this->endContent(); ?>
