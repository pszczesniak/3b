<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
?>
<div class="comment-form">

    <?php $form = ActiveForm::begin([
        'id' => 'blog-comment-form-'.$formId,
        'action' => Url::to(['blog/addcomment', 'id' => $post]),
        'options' => ['class' => 'blog-comment-form'],
        'fieldConfig' => [
            'template' => "{input}{label}{error}",
        ],
    ]); ?>
    <div class="grid">
        <div class="col-sm-6">
           <?= $form->field($model, 'content')->textarea(['rows' => 4, 'placeholder' => 'Treść komentarza'])->label(false); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'author')->textInput((['maxlength' => 32, 'placeholder' => 'autor' ]))->label(false); ?>
            <?= $form->field($model, 'email')->textInput((['maxlength' => 32, 'placeholder' => 'email']))->label(false); ?>
            <?= $form->field($model, 'id_parent_fk', ['template' => '{input}'])->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'discussion_label', ['template' => '{input}'])->hiddenInput()->label(false); ?>
            <div class="align-right"><button type="submit" class="btn">Dodaj komentarz</button></div>
        </div>
       
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- form -->

<script type="text/javascript">
    document.getElementById("blog-comment-form-0").onsubmit = function() {
		var data = new FormData(this);
		var http = new XMLHttpRequest();
		http.open('POST', '<?= Url::to(['blog/addcomment', 'id' => $post]) ?>',true);
 
		http.onreadystatechange=function()  {
			if (http.readyState==4 && http.status == 200) {
				var result = JSON.parse(http.responseText);
				if(result.success) { 
					commentsList = document.getElementById('commentlist');
					commentsList.insertAdjacentHTML('afterbegin', result.item);
                } else {
					document.getElementById("errorValueHint").innerHTML = result.alert;
				}
			} else {
				document.getElementById("errorValueHint").innerHTML = 'Wystąpił błąd. '+http.status;
			}
		};
 
		http.send(data); 
		return false;
	}    
</script>
