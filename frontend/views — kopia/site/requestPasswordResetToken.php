<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;

$templateInput = '<div class="wrap-input validate-input %s" data-validate="Name is required">'
                       .'<span class="label-input">{label}</span>'
                        .'{input}'
                        .'<span class="focus-input"></span>'
                    .'</div>';

?>
<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class' => 'homepage', 'title'=>Html::encode($this->title), 'back' => false)) ?> 
    
    <?php $form = ActiveForm::begin(['method' => 'post', 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['autocomplete' => 'off', 'class' => 'form'] ]); ?>
        <span class="form-title"> Żądanie resetu hasła  </span>
        <p class="txt1">Please fill out your email. A link to reset password will be sent there.</p>
        <?= $form->field($model, 'email', ['template' => sprintf($templateInput, (isset($model->getErrors()['email']) ? 'alert-validate' : ''))])
                                 ->textInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz email...', 'title' => 'Email', 'autocomplete' => 'off'])  ?>

        <?= Alert::widget() ?>
        <div class="container-form-btn">
            <div class="wrap-form-btn">
                <div class="form-btn-bgbtn"></div>
                <button class="form-btn"> Wyślij </button>
            </div>

        </div>
    <?php ActiveForm::end(); ?>
<?php $this->endContent(); ?>
