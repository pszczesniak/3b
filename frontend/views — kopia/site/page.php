<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
/* @var $this yii\web\View */
if($model->fk_category_id) {    
    $this->params['breadcrumbs'][] = ['label' => $model->category['title'],  'url' => ['category', 'cid' => $model->fk_category_id, 'cslug' =>  $model->category['title_slug']] ];
}
$this->params['breadcrumbs'][] = $model->title;

?>

<!--<header class="l-header l-header--page">
    <h1 class="site-title">BLOG</h1>
</header>-->

<?php $img = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/pages/cover/".$model->id."/medium.jpg")) ? "/uploads/pages/cover/".$model->id."/medium.jpg" : false; ?>

<?php $this->beginContent('@app/views/layouts/web-container.php',array('model' => $model, 'class' => 'page', 'title'=>Html::encode($this->title), 'back' => false)) ?>
    <?php if($img && $model->fk_category_id) { ?>
        <div class="page-header">
            <div class="page-header-image" style="background-image: url(<?= $img ?>)"></div>
            <div class="page-header-title"><?= $model->title; ?></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    <?php } ?>
    <div class="page-brief"><?= $model->short_content; ?></div>
    <div class="page-content"><?= $model->content ?></div>
<?php $this->endContent(); ?>