<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Investor';
use frontend\widgets\Calculator;
use frontend\widgets\Browser;


?>
<?php $this->beginContent('@app/views/layouts/web-container.php',array('model' => $model, 'class' => 'homepage', 'title'=>Html::encode($this->title), 'back' => false)) ?>

<section class="section">
    <div class="container">
        <div class="grid">
			<div class="col-sm-6 col-xs-12 align-center text--primary">
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
			</div>
			<div class="col-sm-6 col-xs-12  align-center text--secondary">
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
				Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam. Masagni dolores eoquie voluptatequnt. Niquerro quisquam estqui dolorem sumquia dolor sitamet conetase adipisci unumquam eius. Basmodi temporaunt, ut laboreas dolore magnam aliquam.
			</div>
		</div>
	</div>
</section>


<?php $this->endContent(); ?>