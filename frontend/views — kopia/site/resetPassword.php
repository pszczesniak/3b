<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;

$templateInput = '<div class="wrap-input validate-input %s" data-validate="Name is required">'
                       .'<span class="label-input">{label}</span>'
                        .'{input}'
                        .'<span class="focus-input"></span>'
                    .'</div>';

?>

<?php $this->beginContent('@app/views/layouts/client-container.php',array('model' => $model, 'class' => 'homepage', 'title'=>Html::encode($this->title), 'back' => false)) ?> 

    <?php $form = ActiveForm::begin(['method' => 'post', 'fieldConfig' => ['template' => "{input}", 'options' => [ 'tag' => false]  ], 'enableClientValidation' => true, 'options' => ['autocomplete' => 'off', 'class' => 'form'] ]); ?>
        <span class="form-title"> <?= $this->title ?>  </span>
        <?= $form->field($model, 'password', ['template' => sprintf($templateInput, (isset($model->getErrors()['password']) ? 'alert-validate' : ''))])
                                 ->passwordInput(['minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'class' => 'input', 'placeholder' => 'wpisz nowe hasło', 'title' => 'Hasło', 'autocomplete' => 'off'])  ?>


        <div class="container-form-btn">
            <div class="wrap-form-btn">
                <div class="form-btn-bgbtn"></div>
                <button class="form-btn"> Zapisz nowe hasło </button>
            </div>

        </div>
    <?php ActiveForm::end(); ?>
<?php $this->endContent(); ?>

