<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\Modules\Cms\models\CmsPage;
use backend\Modules\Cms\models\CmsGallery;

use yii\web\Response;
use yii\data\Pagination;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup', 'noaccess'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
           /* 'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = CmsPage::findOne(1);
        $model->user_action = 'hit'; $model->hits = $model->hits + 1; $model->save();
		return $this->render('index', [
                'model' => $model,
            ]);
    }
    
    public function actionIndex2()
    {
        $this->layout = '\\main_dark';
        $model = CmsPage::findOne(1);
		return $this->render('index_dark', [
                'model' => $model,
            ]);
    }
    
    public function actionPage($pid, $slug)  {
        //$this->layout = '\\main2';
        $company = \backend\Modules\Company\models\Company::findOne(1);
        $model = CmsPage::findOne($pid); 
        $model->user_action = 'hit'; $model->hits = $model->hits + 1; $model->save();
        if($model->details_config) {
            $details = \yii\helpers\Json::decode($model->details_config);
            $model->map_latitude = (isset($details['latitude'])) ? $details['latitude'] : 50.811435;
            $model->map_longitude = (isset($details['longitude'])) ? $details['longitude'] : 19.125092;
        }
		$books = false;
		if($model->template_view == '_books')
			$books = \backend\Modules\Cms\models\CmsGallery::find()->where(['status' => 1, 'view_type' => 2])->all();
		return $this->render('page'.$model->template_view, [
                'model' => $model, 'company' => $company, 'gallery' => false, 'books' => $books
            ]);
    }
    
    public function actionCategory($cid, $cslug)  {
        //$this->layout = '\\main2';
        $model = \backend\Modules\Cms\models\CmsCategory::findOne($cid); 
        //$model->user_action = 'hit'; $model->hits = $model->hits + 1; $model->save();
		
		$query = \backend\Modules\Cms\models\CmsPage::find()->where(['fk_category_id' => $cid, 'fk_status_id' => 1, 'status' => 1])->orderby('id desc');
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 10, 'pageSizeParam' => false, 'forcePageParam' => false]);
		$models = $query->offset($pages->offset)
			->limit($pages->limit)
			->all();
			
		$books = \backend\Modules\Cms\models\CmsPage::find()->where(['status' => 1, 'fk_category_id' => 1, 'fk_status_id' => 1])->one();

		return $this->render('category', [
                'model' => $model, 'models' => $models, 'pages' => $pages, 'books' => $books
            ]);
    }
    
    public function actionCpage($pid, $pslug, $cslug ) {
		$model = \backend\Modules\Cms\models\CmsPage::findOne($pid); 
        $model->user_action = 'hit'; $model->hits = $model->hits + 1; $model->save();
        
        if($model->details_config) {
            $details = \yii\helpers\Json::decode($model->details_config);
            $model->map_latitude = (isset($details['latitude'])) ? $details['latitude'] : '';
            $model->map_longitude = (isset($details['longitude'])) ? $details['longitude'] : '';
        }
        
        $gallery = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $model->id, 'status' => 1])->andWhere('fk_gallery_id in (select id from {{%cms_gallery}} where view_type = 3)')->one(); 
        
		return $this->render('page', [
                'model' => $model, 'gallery' => $gallery
            ]);
	}
    
    public function actionGalleries()  {
        //$this->layout = '\\main2';
        
        $galleries = \backend\Modules\Eg\models\Visit::find()->where(['status' => 1])->andWhere('id_gallery_fk is not null')->orderby('id_gallery_fk desc')->all();
        
		return $this->render('galleries', [
            'galleries' => $galleries
        ]);
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
   /* public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->session->destroy();
        Yii::$app->user->logout();
        
        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        $send = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $send = true;
                //Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                Yii::$app->session->setFlash('success', 'Sprawdź swój e-mail. Wysłaliśmy Ci wiadomość z dalszą instruckją zmiany hasła. Jeśli nie znajdziesz go w swojej skrzynce odbiorczej, sprawdź folder SPAM.');

                //return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model, 'send' => $send
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        $send = false;
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                Yii::$app->session->setFlash('success', 'New password saved.');

               // return $this->goHome();
            }
        }
        return $this->render('resetPassword', [
            'model' => $model, 'send' => $send
        ]);
    }
    
    public function actionNoaccess() {
        return $this->render('noaccess');
    }
    
    public function actionTestimonials() {
        $testimonials = \backend\Modules\Mix\models\MixComment::find()->where(['status' => 2])->orderby('id desc')->all();  
        return $this->render('testimonials', ['testimonials' => $testimonials]);
    }
	
	public function actionBook($id) {
		$book = CmsGallery::findOne($id);
		
		return $this->renderPartial('gallery/_book', ['book' => $book]);
	}
}
