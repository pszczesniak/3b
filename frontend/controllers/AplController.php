<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\Modules\Apl\models\AplRequest;
use backend\Modules\Apl\models\AplCustomer;
use backend\Modules\Apl\models\AplLoan;

use backend\Modules\Eg\models\Patient;

use yii\web\Response;
use yii\helpers\Url;
use yii\data\Pagination;

/**
 * Apl controller
 */
class AplController extends Controller
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
           /* [
                'class' => 'yii\filters\AccessControl',
                //'only' => ['contact'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function(){
                            return (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role->name == 'client');
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                   return $action->controller->redirect('/site/noaccess');

                   // return \Yii::$app->getUser()->loginRequired();
                },
            ],*/
            [
                'class' => 'yii\filters\AccessControl',
                'only' => ['contact', 'finance', 'accept'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return \Yii::$app->getUser()->loginRequired();
                },
            ],
            //...

        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /*if(!\Yii::$app->user->isGuest) {
            $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            if($customer->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($customer->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }
        } else {
        }*/
        
        $requestModel = new AplRequest();
        
        $request = Yii::$app->request;
        $requestModel->request_remote_addr = $request->getUserIP();
        $requestModel->request_user_agent = $request->getUserAgent(); 
        $requestModel->request_content_type = $request->getContentType(); 
            
        $requestModel->load(Yii::$app->request->post());
        $requestModel->save();
        /*if(!$requestModel->save()) {
            //Yii::$app->session->setFlash('error', 'Proszę wybrać parametry pożyczki.');
            //return $this->redirect(Url::to(['/site/index']));
            $requestModel->l_amount = 2000;
            $requestModel->l_period = 24;
            $requestModel->save();
        }*/
        
        if(!\Yii::$app->session->get('user.client') || \Yii::$app->user->isGuest) {
            $model = new Patient();
            $model->scenario = 'register';
            $model->term_id = $requestModel->id;
            $model->id_status_fk = 1;
            
            $rules = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 2])->all();
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 1])->all();
            
            return $this->render('index', [
                    'model' => $model, 'request' => $requestModel, 'rules' => $rules, 'accepts' => $accepts
                ]);
        } else { 
            return $this->redirect('/client/account');
        }
    }
    
    public function actionRegister($rid)  {
        if(!Yii::$app->request->ispost) {
            return $this->redirect(Url::to(['/apl/index']));
        }
        
        $requestModel = AplRequest::findOne($rid);
        
        $request = Yii::$app->request;
        $requestModel->request_remote_addr = $request->getUserIP();
        $requestModel->request_user_agent = $request->getUserAgent(); 
        $requestModel->request_content_type = $request->getContentType(); 
            
        $requestModel->load(Yii::$app->request->post());
        $requestModel->save();
       
        $model = new Patient();
        $model->scenario = 'register';
        $model->term_id = $requestModel->id;
        $model->load(Yii::$app->request->post());
        
        if($model->validate() && $model->save()) {
            return $this->render('register', [
                'model' => $model, 'request' => $requestModel
            ]);
        } else {
            $rules = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 2])->all();
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 1])->all();
            
            return $this->render('index', [
                'model' => $model, 'request' => $requestModel, 'rules' => $rules, 'accepts' => $accepts
            ]);
        }
    }
    
    public function actionContact()  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Proszę wybrać parametry pożyczki.');
            return $this->redirect(Url::to(['/site/index']));
        }
        
        $model->scenario = 'contact';
        $model->user_action = 'register_contact';
		$model->c_address = 0;
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 2;
            //$model->address = \yii\helpers\Json::encode(['postalcode' => $model->address_postalcode, 'city' => $model->address_city, 'street' => $model->address_street, 's_number' => $model->address_street_number, 'l_number' => $model->address_local_number, 'status' => $model->address_status, 'correspondence' => 'yes']);
            if($model->validate() && $model->save()) {
                return $this->redirect(Url::to(['/apl/finance']));
            } 
        } 
        
        return $this->render('contact', [
            'model' => $model, 'request' => $request
        ]);
    }
    
    public function actionFinance() {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($model->status == 1) {
            Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
            return $this->redirect(Url::to(['/apl/contact']));
        }
        
        $model->scenario = 'finance';
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        $site = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($site->custom_data);
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 3;
            if($model->validate() && $model->save()) {
                $loan = new AplLoan();
                $loan->id_customer_fk = $model->id;
                $loan->id_request_fk = $request->id;
                $loan->status = 0;
                $loan->l_amount = $request->l_amount;
                $loan->l_period = $request->l_period;
                $loan->l_monthly_rate = 100;
                $loan->l_amount_total = $request->l_period;
                $loan->l_rrso = 133;
                $loan->id_purpose_fk = $model->loan_purpose;
                $loan->l_day_of_payment = $model->loan_day_of_payment;
                $loan->l_provision = $provision;
                $loan->l_interest = $interest;
                $loan->l_provision_amount = round($loan->l_amount*$provision/100, 2);
                $loan->l_interest_amount = 0;;
                $loan->save();
                
                return $this->redirect(Url::to(['/apl/loan', 'lid' => $loan->id]));
            } 
        } 
        $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 3])->all();
		return $this->render('finance', [
                'model' => $model, 'request' => $request, 'accepts' => $accepts
            ]);
    }
    
    public function actionAccept($lid) {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        $model->scenario = 'accept';
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
                
        return $this->render('accept', [
            'model' => $model, 'request' => $request, 
        ]);
    }
    
    public function actionCalc() {
        Yii::$app->response->format = Response::FORMAT_JSON; 
        
        $model = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($model->custom_data);
        
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        $calcArray['amount']   = $_POST['amount'];
        $calcArray['period']   = $_POST['period'];
        $rate                  = self::pmt($interest, $_POST['period'], $_POST['amount']);
        $calcArray['rate']     = number_format($rate, 2);
        $calcArray['deadline'] = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$_POST['period']." months"));
        $charges = $_POST['amount']*$provision/100;
        $calcArray['charges']  = number_format($charges, 2, '.', ' ');
        //$values = [-1020, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.57];
        //$dates = ['08/01/2017', '09/01/2017', '10/01/2017', '11/01/2017', '12/01/2017', '01/01/2018', '02/01/2018', '03/01/2018', '04/01/2018', '05/01/2018', '06/01/2018', '07/01/2018', '08/01/2018', '09/01/2018'];
        $values = []; $dates = [];
        //$values = [0 => ($charges-$calcArray['amount'])]; $dates = [0 => date('m/d/y')];
        $values = [0 => ($charges-$_POST['amount'])]; $dates = [0 => date('Y-m-d')];
        for($i=1; $i <= $calcArray['period']; ++$i) {
            array_push($dates, date('Y-m-d', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$i." months")));
            /*if($i == $calcArray['period']) {
                $diff = 0.05; //$rate*$calcArray['period']-round($rate,2)*$calcArray['period'];
                array_push($values, ($rate+$diff));
            } else {*/
                array_push($values, round($rate,2));
            //}
        } 
		
		$cashflow = [];
		array_push($cashflow, ['period' => 0, 'amount' => ($charges-$_POST['amount'])]);
		for($i = 1; $i <= $calcArray['period']; ++$i) {
			array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $calcArray['rate']]);
		}

        $calcArray['rrso'] = self::rrso ($cashflow);
        
        return $calcArray;
    }
    
    public static function calculation($amount, $period) {
        $model = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($model->custom_data);
        
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        $calcArray['amount']   = $amount;
        $calcArray['period']   = $period;
        $rate                  = self::pmt($interest, $period, $amount);
        $calcArray['rate']     = number_format($rate, 2);
        $calcArray['deadline'] = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$period." months"));
        $charges = $amount*$provision/100;
        $calcArray['charges']  = number_format($charges, 2, '.', ' ');

        $cashflow = [];
		array_push($cashflow, ['period' => 0, 'amount' => ($charges-$amount)]);
		for($i = 1; $i <= $calcArray['period']; ++$i) {
			array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $calcArray['rate']]);
		}

        $calcArray['rrso'] = self::rrso ($cashflow);
        
        return $calcArray;
    }
    
    public static function pmt($interest, $months, $loan) {
       $months = $months;
       $interest = $interest / 1200;
       $amount = $interest * -$loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
       return $amount;
    }
    
    public static function rrso($cashflow) {
        $bestGuess = -1;
		$currentNPV;
		for ($rate = 0; $rate <= 1000; $rate += 0.01) {
			$npv = 0;
			for ($i = 0; $i < count($cashflow); ++$i) { 
				$npv += ($cashflow[$i]['amount'] / pow((1 + $rate/100), $cashflow[$i]['period']));
				//echo $npv.'<br />'; 
			} //echo 'all: '.$npv; exit;
			$currentNPV = round($npv * 100) / 100; 
			if ($currentNPV <= 0) {
				$bestGuess = $rate;
				if ($bestGuess == -1 || $bestGuess <= 0) {
					return 'błąd zakresu';
				}
				return round($bestGuess * 100) / 100;
			}
		}
		if ($bestGuess == -1 || $bestGuess <= 0) {
			return 'błąd zakresu';
		}
		return round($bestGuess * 100) / 100;
    }

    public function actionSend($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        $model->scenario = 'accept';
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        //$model->scenario = 'send';
       // $model->status = 1;
        $client = AplCustomer::findOne($model->id_customer_fk);
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            //$model->status = 1;
            if($model->validate() && $model->save()) {
                \Yii::$app->mailer->compose(['html' => 'loanRegistration-html', 'text' => 'loanRegistration-text'], ['loan' => $model, 'client' => $client ])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($client->email)
                        ->setBcc(\Yii::$app->params['bccEmail'])
                        ->setSubject('Potwierdzenie złożenia wniosku pożyczkowego w serwisie GTW Finanse')
                        ->send();
                Yii::$app->session->setFlash('success', 'Na podany przy rejestracji adres e-mail wysłaliśmy prośbę o potwierdzenie wysłania wniosku. Aby dopełnić procedury wysłania wniosku prosimy o odebranie wiadomości e-mail i kliknięcie w link');
                return $this->redirect(Url::to(['/client/account']));              
            }
        }
        $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 4])->all();       
        return $this->render('accept_form', [
            'model' => $model, 'accepts' => $accepts
        ]);
    }
    
    public function actionDiscard($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        //$model->scenario = 'discard';
        $model->status = -1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Wniosek został usunięty.');
        return $this->redirect(Url::to(['/client/account']));
        
    }
    
    public function actionVerification($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        $model->status = 1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Weryfikacja przebiegła pomyślnie. Wniosek został wysłany do rozpatrzenia.');
        return $this->redirect(Url::to(['/client/account']));
    }
    
    public function actionCreate($rid)   {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if(!$customer) {
            //Yii::$app->session->setFlash('error', 'Dostępne tylko dla zalogowanych.');
            return $this->redirect(Url::to(['/site/login']));
        }
               
        $request = AplRequest::findOne($rid);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        $site = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($site->custom_data);
    
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;

        $model = AplLoan::find()->where(['id_request_fk' => $rid])->one();
        if(!$model) {
            $model = new AplLoan();
            $model->id_customer_fk = $customer->id;
            $model->id_request_fk = $request->id;
            $model->l_amount = $request->l_amount;
            $model->l_period = $request->l_period;
            $model->l_provision = $provision;
            $model->l_interest = $interest;
        }
      
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 0;
            
            $model->l_monthly_rate = $this->pmt($interest, $model->l_period, $model->l_amount);
            $model->repayment_term = date('Y-m-d', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$model->l_period." months"));
            $model->l_provision_amount = round($model->l_amount*$provision/100, 2);
            $model->l_interest_amount = round($model->l_amount*$interest/100, 2);
            
            $charges = round($model->l_amount*$provision/100, 2);
            
            $cashflow = [];
            array_push($cashflow, ['period' => 0, 'amount' => ($charges-$model->l_amount)]);
            for($i = 1; $i <= $model->l_period; ++$i) {
                array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $model->l_monthly_rate]);
            }

            $model->l_rrso = self::rrso ($cashflow);
            if($model->validate() && $model->save()) {
                return $this->redirect(Url::to(['/apl/loan', 'lid' => $model->id]));
            }/* else {
                var_dump($model->getErrors()); exit;
            }*/
        } else {
            $model->l_monthly_rate = $this->pmt($interest, $model->l_period, $model->l_amount);
            $model->repayment_term = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$model->l_period." months"));
            $model->l_provision_amount = round($model->l_amount*$provision/100, 2);
            $model->l_interest_amount = round($model->l_amount*$interest/100, 2);
            
            $charges = round($model->l_amount*$provision/100, 2);
            
            $cashflow = [];
            array_push($cashflow, ['period' => 0, 'amount' => ($charges-$model->l_amount)]);
            for($i = 1; $i <= $model->l_period; ++$i) {
                array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $model->l_monthly_rate]);
            }

            $model->l_rrso = self::rrso ($cashflow);
        }
        
        return $this->render('create', [
            'model' => $model, 'request' => $request
        ]);
    }
    
    public function actionLoan($lid) {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        
        $model = AplLoan::findOne($lid);
        
        return $this->render('loan', [
            'model' => $model,
        ]);
    }
}
