<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use backend\Modules\Blog\models\BlogCatalog;
use backend\Modules\Blog\models\BlogPost;
use backend\Modules\Blog\models\BlogComment;
use backend\Modules\Blog\models\Status;
use backend\Modules\Blog\models\BlogTag;
use backend\Modules\Cms\models\CmsGallery;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\components\CustomHelpers;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use backend\extensions\kapi\gallery\GalleryManagerAction;

class BlogController extends Controller
{
    public $mainMenu = [];
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'galleryApi' => [
			   'class' => GalleryManagerAction::className(),
			   // mappings between type names and model classes (should be the same as in behaviour)
			   'types' => [
				   'cmsgallery' => cmsgallery::className()
			   ]
		   ],
           'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'BlogPost' => BlogPost::className()
				]
			],
        ];
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],*/
        ];
    }


   /* public function beforeAction($action)
    {
        $this->enableCsrfValidation = ($action->id !== "addcomment"); 
        if (parent::beforeAction($action)) {

            //menu
            $id = isset($_GET['id']) ? $_GET['id'] : 0;
            $rootId = ($id>0) ? BlogCatalog::getRootCatalogId($id, BlogCatalog::find()->all()) : 0;
            $allCatalog = BlogCatalog::findAll([
                'id_parent_fk' => 0
            ]);
            foreach($allCatalog as $catalog)
            {
                $item = ['label'=>$catalog->title, 'active'=>($catalog->id == $rootId)];
                if($catalog->redirect_url)
                {// redirect to other site
                    $item['url'] = $catalog->redirect_url;
                }
                else
                {
                    $item['url'] = Yii::$app->getUrlManager()->createUrl(['/blog/default/catalog/','id'=>$catalog->id, 'surname'=>$catalog->surname]);
                }

                if(!empty($item))
                    array_push($this->mainMenu, $item);
            }
            Yii::$app->params['mainMenu'] = $this->mainMenu;

            return true;  // or false if needed
        } else {
            return false;
        }
    }*/
     public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "offermessage" && $action->id !== "offercomment"); 
        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        $query = BlogPost::find();
        $query->where([
            'status' => Status::STATUS_ACTIVE,
        ]);

        if(Yii::$app->request->get('tag'))
            $query->andFilterWhere([
                'like', 'tags', Yii::$app->request->get('tag'),
            ]);

        if(Yii::$app->request->get('keyword'))
        {
            $keyword = strtr(Yii::$app->request->get('keyword'), array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\'));
            $keyword = Yii::$app->formatter->asText($keyword);

            $query->andFilterWhere([
                'or', ['like', 'title', $keyword], ['like', 'content', $keyword]
            ]);
        }

        $pagination = new Pagination([
            'defaultPageSize' => Yii::$app->params['blogPostPageCount'],
            'totalCount' => $query->count(),
        ]);

        $posts = $query->orderBy('created_at desc')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'posts' => $posts,
            'pagination' => $pagination,
        ]);
    }

    public function actionCatalog($id, $surname)
    {
     
        $query = BlogPost::find();
        $query->where([
            'status' => Status::STATUS_ACTIVE,
            'id_catalog_fk' => CustomHelpers::decode($id),
        ]);
   

        if(Yii::$app->request->get('tag'))
            $query->andFilterWhere([
                'tags' => Yii::$app->request->get('tag'),
            ]);

        if(Yii::$app->request->get('keyword'))
        {
            //$keyword = '%'.strtr(Yii::$app->request->get('keyword'), array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%';
            $keyword = Yii::$app->request->get('keyword');

            $query->andFilterWhere([
                'title' => $keyword,
            ]);
        }

        $pagination = new Pagination([
            'defaultPageSize' => Yii::$app->params['blogPostPageCount'],
            'totalCount' => $query->count(),
        ]);

        $posts = $query->orderBy('created_at desc')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'posts' => $posts,
            'pagination' => $pagination,
        ]);
    }

    public function actionView($id, $title)
    {
        $id = CustomHelpers::decode($id);
        //if(Yii::$app->request->get('id') && Yii::$app->request->get('id') > 0)
        //{
            $post = BlogPost::findOne($id);
            $post->updateCounters(['click' => 1]);
            $comments = BlogComment::find()->where(['id_post_fk' => $post->id, 'id_parent_fk' => 0, 'status' => Status::STATUS_ACTIVE])->orderBy(['created_at' => SORT_DESC])->all();
            //$comment = $this->newComment($post);
            $comment = new BlogComment(); $comment->id_parent_fk = 0; $comment->discussion_label = 0;
            if(!\Yii::$app->user->isGuest) {
                $comment->email = \Yii::$app->user->identity->email;
                $comment->author = \Yii::$app->user->identity->username;
            }
        //}
        //else
        //    $this->redirect(['blog']);

        //var_dump($post->comments);
        return $this->render('view', [
            'post' => $post,
            'comments' => $comments,
            'comment' => $comment,
        ]);
    }
    
    public function actionAddcomment($id)
    {   
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON; 
		$model = new BlogComment();
        $model->id_post_fk = $id;
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if(!$model->id_parent_fk) {
                $model->id_parent_fk = 0;
                $model->discussion_label = 0;
            }
			if ($model->save()) {
                //$res = ['success' => true];
                $model->created_at = date('Y-m-d H:i:s');
				$res = ['success' => true, 'parentId' => $model->id_parent_fk, 'item' => $this->renderPartial('_comment_single', ['comment' => $model, 'post'=>$id, 'new' => false ]) ];
            } else {
				$errors = $model->getErrors();
                $res = array('success' => false, 'error' => current($errors));
            }
        } else {
			$errors = $model->getErrors();
            $res = array('success' => false, 'error' => current($errors));
        }
		
		return $res;
    }
    

    protected function newComment($post)
    {
        $comment = new BlogComment;
        if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
        {
            echo ActiveForm::validate($comment);
            Yii::app()->end();
        }

        if(Yii::$app->request->post('BlogComment'))
        {
            $comment->load(Yii::$app->request->post());
            if($post->addComment($comment))
            {
                if($comment->status == Status::STATUS_INACTIVE)
                    echo Yii::$app->formatter->asText('success');
            }
            else
            {
                echo 'failed';
            }
            die();
        }
        return $comment;
    }
    
    public function actionCreate()
    {
        //if(!Yii::$app->user->can('createPost')) throw new HttpException(401, 'No Auth');

        $model = new BlogPost();
        
        if(!\Yii::$app->user->isGuest) {
            $model->author_email = \Yii::$app->user->identity->email;
            $model->author_name = \Yii::$app->user->identity->username;
        }
        
        $model->loadDefaultValues();
        $model->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
        //$model->created_by = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
          //  $model->banner = UploadedFile::getInstance($model, 'banner');
            $model->status = 0;
            $tags_tmp = '';
            if(is_array($model->tags)) {
                foreach($model->tags as $key => $tag) {
                    $tags_tmp .= $tag .',';
                }
            }
            $model->tags = substr($tags_tmp, 0, -1);
            if ($model->validate()) {
                /*if ($model->banner) {
                    $bannerName = Yii::$app->params['blogUploadPath'] . date('Ymdhis') . rand(1000, 9999) . '.' . $model->banner->extension;
                    $model->banner->saveAs(Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . $bannerName);
                    $model->banner = $bannerName;
                }*/
                $model->save(false);
                Yii::$app->session->setFlash('success', 'Dziękujemy za propozycję artykułu, administartor zweryfikuje Twoją prośbę, odpowiedź zostanie przesłana na podany email.');
                
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        //if(!Yii::$app->user->can('updatePost')) throw new HttpException(401, 'No Auth');
        $id = CustomHelpers::decode($id);
        $model = BlogPost::findOne($id);
        //$oldBanner = $model->banner;
        if (Yii::$app->request->isPost ) {
            if ($model->load(Yii::$app->request->post())) {
                //$model->banner = UploadedFile::getInstance($model, 'banner');
                if ($model->validate()) {
                    $tags_tmp = '';
                    foreach($model->tags as $key => $tag) {
                        $tags_tmp .= $tag .',';
                    }
                    $model->tags = substr($tags_tmp, 0, -1);
                   /* if($model->banner){
                        $bannerName = Yii::$app->params['blogUploadPath'] . date('Ymdhis') . rand(1000, 9999) . '.' . $model->banner->extension;
                        $model->banner->saveAs(Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . $bannerName);
                        $model->banner = $bannerName;
                    } else {
                        $model->banner = $oldBanner;
                    }*/
                    $model->save(false);
                    return $this->redirect(['update', 'id' => $model->id]);
                } /*else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }*/
            }
        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionMedia($id) {
        $id = CustomHelpers::decode($id);
        $model = BlogPost::findOne($id);
        
        return $this->render('media', [
            'model' => $model,
        ]);
    }
    
    public function actionGallery($id) {
        $id = CustomHelpers::decode($id);
        $model = BlogPost::findOne($id);
        
        if(!$model->id_gallery_fk) {
            $gallery = new \backend\Modules\Cms\models\CmsGallery();
            $gallery->title = 'blog#'.$id;
            if( $gallery->save() ) $model->id_gallery_fk = $gallery->id; $model->save();
        } else {
            $gallery = \backend\Modules\Cms\models\CmsGallery::findOne($model->id_gallery_fk);
        }
        
        return $this->render('gallery', [
            'model' => $model, 'gallery' => $gallery
        ]);
    }

}
