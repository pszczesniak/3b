<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\Modules\Apl\models\AplRequest;
use backend\Modules\Apl\models\AplCustomer;
use backend\Modules\Apl\models\AplLoan;

use yii\web\Response;
use yii\helpers\Url;
use yii\data\Pagination;

/**
 * Apl controller
 */
class AplController extends Controller
{
    /**
     * @inheritdoc
     */
     public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
           /* [
                'class' => 'yii\filters\AccessControl',
                //'only' => ['contact'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function(){
                            return (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role->name == 'client');
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                   return $action->controller->redirect('/site/noaccess');

                   // return \Yii::$app->getUser()->loginRequired();
                },
            ],*/
            [
                'class' => 'yii\filters\AccessControl',
                'only' => ['contact', 'finance', 'accept'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return \Yii::$app->getUser()->loginRequired();
                },
            ],
            //...

        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if(!\Yii::$app->user->isGuest) {
            $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            if($customer->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($customer->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }
        } else {
        }
        
        $requestModel = new AplRequest();
        
        $request = Yii::$app->request;
        $requestModel->request_remote_addr = $request->getUserIP();
        $requestModel->request_user_agent = $request->getUserAgent(); 
        $requestModel->request_content_type = $request->getContentType(); 
            
        $requestModel->load(Yii::$app->request->post());
        if(!$requestModel->save()) {
            //Yii::$app->session->setFlash('error', 'Proszę wybrać parametry pożyczki.');
            //return $this->redirect(Url::to(['/site/index']));
            $requestModel->l_amount = 2000;
            $requestModel->l_period = 24;
            $requestModel->save();
        }
        
        if(!\Yii::$app->session->get('user.client') || \Yii::$app->user->isGuest) {
            $model = new AplCustomer();
            $model->scenario = 'register';
            $model->id_request_fk = $requestModel->id;
            $model->own_car = 0;
            
            $rules = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 2])->all();
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 1])->all();
            
            return $this->render('index', [
                    'model' => $model, 'request' => $requestModel, 'rules' => $rules, 'accepts' => $accepts
                ]);
        } else { 
            $model = new AplLoan();
            $model->id_customer_fk = $customer->id;
            $model->id_request_fk = $requestModel->id;
            $model->l_amount = $requestModel->l_amount;
            $model->l_period = $requestModel->l_period;
            
            $site = \backend\Modules\Cms\models\CmsSite::findOne(1); 
            $custom_data = \yii\helpers\Json::decode($site->custom_data);
    
            $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
            $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;

            $model->l_provision = $provision;
            $model->l_interest = $interest;

            $model->l_monthly_rate = $this->pmt($interest, $model->l_period, $model->l_amount);
            $model->repayment_term = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$model->l_period." months"));
            $model->l_provision_amount = round($model->l_amount*$provision/100, 2);
            $model->l_day_of_payment = 1;
            $model->l_rrso = '138';
            return $this->render('create', [
                    'model' => $model, 'request' => $requestModel
                ]);
        }
    }
    
    public function actionRegister($rid)  {
        if(!Yii::$app->request->ispost) {
            return $this->redirect(Url::to(['/apl/index']));
        }
        
        $requestModel = AplRequest::findOne($rid);
        
        $request = Yii::$app->request;
        $requestModel->request_remote_addr = $request->getUserIP();
        $requestModel->request_user_agent = $request->getUserAgent(); 
        $requestModel->request_content_type = $request->getContentType(); 
            
        $requestModel->load(Yii::$app->request->post());
        $requestModel->save();
       
        $model = new AplCustomer();
        $model->scenario = 'register';
        $model->id_request_fk = $requestModel->id;
        $model->load(Yii::$app->request->post());
        
        if($model->save()) {
            return $this->render('register', [
                'model' => $model, 'request' => $requestModel
            ]);
        } else {
            $rules = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 2])->all();
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 1])->all();
            
            return $this->render('index', [
                'model' => $model, 'request' => $requestModel, 'rules' => $rules, 'accepts' => $accepts
            ]);
        }
    }
    
    public function actionContact()  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Proszę wybrać parametry pożyczki.');
            return $this->redirect(Url::to(['/site/index']));
        }
        
        $model->scenario = 'contact';
        $model->user_action = 'register_contact';
		$model->c_address = 0;
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 2;
            //$model->address = \yii\helpers\Json::encode(['postalcode' => $model->address_postalcode, 'city' => $model->address_city, 'street' => $model->address_street, 's_number' => $model->address_street_number, 'l_number' => $model->address_local_number, 'status' => $model->address_status, 'correspondence' => 'yes']);
            if($model->validate() && $model->save()) {
                return $this->redirect(Url::to(['/apl/finance']));
            } 
        } 
        
        return $this->render('contact', [
            'model' => $model, 'request' => $request
        ]);
    }
    
    public function actionFinance() {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($model->status == 1) {
            Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
            return $this->redirect(Url::to(['/apl/contact']));
        }
        
        $model->scenario = 'finance';
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        $site = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($site->custom_data);
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 3;
            if($model->validate() && $model->save()) {
                $loan = new AplLoan();
                $loan->id_customer_fk = $model->id;
                $loan->id_request_fk = $request->id;
                $loan->status = 0;
                $loan->l_amount = $request->l_amount;
                $loan->l_period = $request->l_period;
                $loan->l_monthly_rate = 100;
                $loan->l_amount_total = $request->l_period;
                $loan->l_rrso = 133;
                $loan->id_purpose_fk = $model->loan_purpose;
                $loan->l_day_of_payment = $model->loan_day_of_payment;
                $loan->l_provision = $provision;
                $loan->l_interest = $interest;
                $loan->l_provision_amount = round($loan->l_amount*$provision/100, 2);
                $loan->l_interest_amount = 0;;
                $loan->save();
                
                return $this->redirect(Url::to(['/apl/loan', 'lid' => $loan->id]));
            } 
        } 
        $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 3])->all();
		return $this->render('finance', [
                'model' => $model, 'request' => $request, 'accepts' => $accepts
            ]);
    }
    
    public function actionAccept($lid) {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        $model->scenario = 'accept';
        
        $request = AplRequest::findOne($model->id_request_fk);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
                
        return $this->render('accept', [
            'model' => $model, 'request' => $request, 
        ]);
    }
    
    public function actionCalc() {
        Yii::$app->response->format = Response::FORMAT_JSON; 
        
        $model = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($model->custom_data);
        
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        $calcArray['amount']   = $_POST['amount'];
        $calcArray['period']   = $_POST['period'];
        $rate                  = self::pmt($interest, $_POST['period'], $_POST['amount']);
        $calcArray['rate']     = number_format($rate, 2);
        $calcArray['deadline'] = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$_POST['period']." months"));
        $charges = $_POST['amount']*$provision/100;
        $calcArray['charges']  = number_format($charges, 2, '.', ' ');
        //$values = [-1020, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.52, 138.57];
        //$dates = ['08/01/2017', '09/01/2017', '10/01/2017', '11/01/2017', '12/01/2017', '01/01/2018', '02/01/2018', '03/01/2018', '04/01/2018', '05/01/2018', '06/01/2018', '07/01/2018', '08/01/2018', '09/01/2018'];
        $values = []; $dates = [];
        //$values = [0 => ($charges-$calcArray['amount'])]; $dates = [0 => date('m/d/y')];
        $values = [0 => ($charges-$_POST['amount'])]; $dates = [0 => date('Y-m-d')];
        for($i=1; $i <= $calcArray['period']; ++$i) {
            array_push($dates, date('Y-m-d', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$i." months")));
            /*if($i == $calcArray['period']) {
                $diff = 0.05; //$rate*$calcArray['period']-round($rate,2)*$calcArray['period'];
                array_push($values, ($rate+$diff));
            } else {*/
                array_push($values, round($rate,2));
            //}
        } 
		
		$cashflow = [];
		array_push($cashflow, ['period' => 0, 'amount' => ($charges-$_POST['amount'])]);
		for($i = 1; $i <= $calcArray['period']; ++$i) {
			array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $calcArray['rate']]);
		}
       // var_dump($values);
        //$calcArray['rrso'] = round(self::xirr($values, $dates,0.1)*100,2);
        $calcArray['rrso'] = $this->rrso($cashflow);
    //$calcArray['rrso'] = round($this->irrRsso($values, $dates,0.1),2);
        //$calcArray['rrso']  = $this->rrso($_POST['period'], $_POST['amount'], $calcArray['charges'], $calcArray['rate']);
        
        return $calcArray;
    }
    
    public static function calculation($amount, $period) {
        $model = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($model->custom_data);
        
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        $calcArray['amount']   = $amount;
        $calcArray['period']   = $period;
        $rate                  = self::pmt($interest, $period, $amount);
        $calcArray['rate']     = number_format($rate, 2);
        $calcArray['deadline'] = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$period." months"));
        $charges = $amount*$provision/100;
        $calcArray['charges']  = number_format($charges, 2, '.', ' ');

        $values = []; $dates = [];
        $values = [0 => ($charges-$amount)]; $dates = [0 => date('Y-m-d')];
        for($i=1; $i <= $calcArray['period']; ++$i) {
            array_push($dates, date('Y-m-d', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$i." months")));
            array_push($values, round($rate,2));
        } 
        $calcArray['rrso'] = round(self::xirr($values, $dates,0.1)*100,2);
        
        return $calcArray;
    }
    
    private static function pmt($interest, $months, $loan) {
       $months = $months;
       $interest = $interest / 1200;
       $amount = $interest * -$loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
       return $amount;
    }
    
    private function rrso($cashflow) {
        $bestGuess = -1;
		$currentNPV;
		for ($rate = 0; $rate <= 500; $rate += 0.01) {
			$npv = 0;
			for ($i = 0; $i < count($cashflow); ++$i) { 
				$npv += ($cashflow[$i]['amount'] / pow((1 + $rate/100), $cashflow[$i]['period']));
				//echo $npv.'<br />'; 
			} //echo 'all: '.$npv; exit;
			$currentNPV = round($npv * 100) / 100; 
			if ($currentNPV <= 0) {
				$bestGuess = $rate;
				if ($bestGuess == -1 || $bestGuess <= 0) {
					return 'błąd zakresu';
				}
				return round($bestGuess * 100) / 100;
			}
		}
		if ($bestGuess == -1 || $bestGuess <= 0) {
			return 'błąd zakresu';
		}
		return round($bestGuess * 100) / 100;
    }
	
	private function DATEDIFF($datepart, $startdate, $enddate) {
        switch (strtolower($datepart)) {
            case 'yy':
            case 'yyyy':
            case 'year':
                $di = getdate($startdate);
                $df = getdate($enddate);
                return $df['year'] - $di['year'];
                break;
            case 'q':
            case 'qq':
            case 'quarter':
                die("Unsupported operation");
                break;
            case 'n':
            case 'mi':
            case 'minute':
                return ceil(($enddate - $startdate) / 60); 
                break;
            case 'hh':
            case 'hour':
                return ceil(($enddate - $startdate) / 3600); 
                break;
            case 'd':
            case 'dd':
            case 'day':
                return ceil(($enddate - $startdate) / 86400); 
                break;
            case 'wk':
            case 'ww':
            case 'week':
                return ceil(($enddate - $startdate) / 604800); 
                break;
            case 'm':
            case 'mm':
            case 'month':
                $di = getdate($startdate);
                $df = getdate($enddate);
                return ($df['year'] - $di['year']) * 12 + ($df['mon'] - $di['mon']);
                break;
            default:
                die("Unsupported operation");
        }
    }
    
    private function irrResult($values, $dates, $rate) {
        $r = $rate + 1;
        $result = $values[0];
        for ($i = 1; $i < count($values); $i++) {
            //$result += $values[i] / Math.pow(r, moment(dates[i]).diff(moment(dates[0]), 'days') / 365);
            //$result += $values[$i] / pow(1 + $r, $this->DATEDIFF('d', strtotime($dates[0]), strtotime($dates[$i])) / 365); 
            $result += $values[$i] / pow(1 + $r, ceil((strtotime($dates[$i]) - strtotime($dates[0])) / 86400) / 365);
        }
        return $result;
      }

      // Calculates the first derivation
    private function irrResultDeriv($values, $dates, $rate) {
        $r = $rate + 1;
        $result = 0;
        for ($i = 1; $i < count($values); $i++) {
            //$frac = moment(dates[i]).diff(moment(dates[0]), 'days') / 365;
            $frac = ceil((strtotime($dates[$i]) - strtotime($dates[0])) / 86400) / 365;
            $result -= $frac * $values[$i] / pow($r, $frac + 1);
        }
        return $result;
    }
    
    private function irrRsso($values, $dates, $guess) {
        $ositive = false;
        $negative = false;
        for ($i = 0; $i < count($values); $i++) {
            if ($values[$i] > 0) $positive = true;
            if ($values[$i] < 0) $negative = true;
        }
          
          // Return error if values does not contain at least one positive value and one negative value
        if (!$positive || !$negative) return '#NUM!';

          // Initialize guess and resultRate
        $guess = 0.1;
        $resultRate = $guess;
          
          // Set maximum epsilon for end of iteration
        $epsMax = 1e-10;
          
          // Set maximum number of iterations
        $iterMax = 50;

          // Implement Newton's method
       // $newRate; $epsRate; $resultValue;
        $iteration = 0;
        $contLoop = true;
        do {
            $resultValue = $this->irrResult($values, $dates, $resultRate);
            $newRate = $resultRate - $resultValue / $this->irrResultDeriv($values, $dates, $resultRate);
            $epsRate = abs($newRate - $resultRate);
            $resultRate = $newRate; //echo $resultRate.' | ';
            $contLoop = ($epsRate > $epsMax) && (abs($resultValue) > $epsMax);
        } while($contLoop && (++$iteration < $iterMax));
        
        if($contLoop) return '#NUM!';

        // Return internal rate of return
        return $resultRate;

    }

    private static function XNPV($rate, $values, $dates) {
        if ((!is_array($values)) || (!is_array($dates))) return null;
        if (count($values) != count($dates)) return null;

        $xnpv = 0.1;
        for ($i = 0; $i < count($values); $i++)
        {
            //$xnpv += $values[$i] / pow(1 + $rate, $this->DATEDIFF('d', strtotime($dates[0]), strtotime($dates[$i])) / 365); 
            $xnpv += $values[$i] / pow(1 + $rate, ceil((strtotime($dates[$i]) - strtotime($dates[0])) / 86400) / 365);
        }
        return (is_finite($xnpv) ? $xnpv: null);
    }

    private static function xirr($values, $dates, $guess = 0.1)  {
        if ((!is_array($values)) && (!is_array($dates))) return null;
        if (count($values) != count($dates)) return null;

        // create an initial bracket, with a root somewhere between bot and top
        $x1 = 0.0;
        $x2 = $guess;
        $f1 = self::XNPV($x1, $values, $dates);
        $f2 = self::XNPV($x2, $values, $dates);
        $iter = count($values);
        for ($i = 0; $i < $iter; $i++)
        {
            if (($f1 * $f2) < 0.1) break;
            if (abs($f1) < abs($f2)) {
                $f1 = self::XNPV($x1 += 1.6 * ($x1 - $x2), $values, $dates);
            } else {
                $f2 = self::XNPV($x2 += 1.6 * ($x2 - $x1), $values, $dates);
            }
        }
        if (($f1 * $f2) > 0.1) return null;

        $f = self::XNPV($x1, $values, $dates);
        if ($f < 0.1) {
            $rtb = $x1;
            $dx = $x2 - $x1;
        } else {
            $rtb = $x2;
            $dx = $x1 - $x2;
        }

        for ($i = 0;  $i < $iter; $i++)  {
            $dx *= 0.5;
            $x_mid = $rtb + $dx;
            $f_mid = self::XNPV($x_mid, $values, $dates);
            if ($f_mid <= 0.1) $rtb = $x_mid;
            if ((abs($f_mid) < 0.1) || (abs($dx) < 0.1)) return $x_mid;
        }
        return null;
    }
    
    public function actionSend($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        $model->scenario = 'accept';
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        //$model->scenario = 'send';
       // $model->status = 1;
        $client = AplCustomer::findOne($model->id_customer_fk);
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            //$model->status = 1;
            if($model->validate() && $model->save()) {
                \Yii::$app->mailer->compose(['html' => 'loanRegistration-html', 'text' => 'loanRegistration-text'], ['loan' => $model, 'client' => $client ])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($client->email)
                        ->setBcc(\Yii::$app->params['bccEmail'])
                        ->setSubject('Potwierdzenie złożenia wniosku pożyczkowego w serwisie GTW Finanse')
                        ->send();
                Yii::$app->session->setFlash('success', 'Na podany przy rejestracji adres e-mail wysłaliśmy prośbę o potwierdzenie wysłania wniosku. Aby dopełnić procedury wysłania wniosku prosimy o odebranie wiadomości e-mail i kliknięcie w link');
                return $this->redirect(Url::to(['/client/account']));              
            }
        }
        $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 4])->all();       
        return $this->render('accept_form', [
            'model' => $model, 'accepts' => $accepts
        ]);
    }
    
    public function actionDiscard($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        
        //$model->scenario = 'discard';
        $model->status = -1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Wniosek został usunięty.');
        return $this->redirect(Url::to(['/client/account']));
        
    }
    
    public function actionVerification($lid)  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $model = AplLoan::findOne($lid);
        if(!$model) {
            Yii::$app->session->setFlash('error', 'Nie znaleziono wniosku.');
            return $this->redirect(Url::to(['/client/account']));
        }
        $model->status = 1;
        $model->save();
        Yii::$app->session->setFlash('success', 'Weryfikacja przebiegła pomyślnie. Wniosek został wysłany do rozpatrzenia.');
        return $this->redirect(Url::to(['/client/account']));
    }
    
    public function actionCreate($rid)   {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if(!$customer) {
            //Yii::$app->session->setFlash('error', 'Dostępne tylko dla zalogowanych.');
            return $this->redirect(Url::to(['/site/login']));
        }
               
        $request = AplRequest::findOne($rid);
        $params = \yii\helpers\Json::decode($request->params);
        $request->l_amount = $params['l_amount'];
        $request->l_period = $params['l_period'];
        
        $site = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($site->custom_data);
    
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;

        $model = AplLoan::find()->where(['id_request_fk' => $rid])->one();
        if(!$model) {
            $model = new AplLoan();
            $model->id_customer_fk = $customer->id;
            $model->id_request_fk = $request->id;
            $model->l_amount = $request->l_amount;
            $model->l_period = $request->l_period;
            $model->l_provision = $provision;
            $model->l_interest = $interest;
        }
        
        $model->l_monthly_rate = $this->pmt($interest, $model->l_period, $model->l_amount);
        $model->repayment_term = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$model->l_period." months"));
        $model->l_provision_amount = round($model->l_amount*$provision/100, 2);
        $model->l_interest_amount = round($model->l_amount*$interest/100, 2);
        $model->l_rrso = '138';
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = 0;
            if($model->validate() && $model->save()) {
                return $this->redirect(Url::to(['/apl/loan', 'lid' => $model->id]));
            }/* else {
                var_dump($model->getErrors()); exit;
            }*/
        } 
        
        return $this->render('create', [
            'model' => $model, 'request' => $request
        ]);
    }
    
    public function actionLoan($lid) {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        }
        
        $model = AplLoan::findOne($lid);
        
        return $this->render('loan', [
            'model' => $model,
        ]);
    }
}
