<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use yii\web\Response;

/**
 * Fun controller
 */
class FunController extends Controller
{
   
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    /**
     * Form Widget Function.
     *
     * @return mixed
     */    
    public function actionContact()  {   
        error_reporting(0);

		Yii::$app->response->format = Response::FORMAT_JSON; 
		$model = new ContactForm();
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {			
			if ($model->sendEmail('artstudio.3b@op.pl')) {
				$res = array('success' => true);
            } else {
				$res = array('success' => false, 'error' => 'Przepraszamy. Wystąpiły problemy techniczne i nie można teraz wysłać wiadmości.');
            }
        } else {
			$errors = $model->getErrors();
            $res = array('success' => false, 'error' => current($errors));
        }
		
		return $res;
    }
    
    public function actionSubscribe()  {   
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$model = new NewsletterSubscribe();
        
        if(Yii::$app->request->post('NewsletterSubscribe')['email']) {
            if($subscribeExist = NewsletterSubscribe::find()->where(['email' => Yii::$app->request->post('NewsletterSubscribe')['email']])->one())
                $model = $subscribeExist;
		}
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			if ($model->save()) {
                //$result = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::find()->where(['id_subscribe_fk', $model->id])->delete();
                $result = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::deleteAll('id_subscribe_fk = :id', [':id' => $model->id]);
                foreach (Yii::$app->request->post('NewsletterSubscribe')['groups'] as $key => $value) {
                    $ngs = new \backend\Modules\Newsletter\models\NewsletterGroupSubscribe();
                    $ngs->id_group_fk = $value;
                    $ngs->id_subscribe_fk = $model->id;
                    $ngs->email = $model->email;
                    $ngs->save();
                }
				$res = array('success' => true, 'responseInfo' => (!$subscribeExist) ? 'Twoje zgłoszenie zostało zapisane' : 'Twoje zgłoszenie zostało zaktualizowane');
            } else {
				$res = array('success' => false, 'error' => 'There was an error sending email.');
            }
        } else {
			$errors = $model->getErrors();
            $res = array('success' => false, 'error' => current($errors));
        }
		
		return $res;
    }
    
    public function actionSubscribedelete()  {   
		Yii::$app->response->format = Response::FORMAT_JSON; 
        
        if(Yii::$app->request->post('NewsletterSubscribe')['email']) {
            if($model = NewsletterSubscribe::find()->where(['email' => Yii::$app->request->post('NewsletterSubscribe')['email']])->one()) {
                $result = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::deleteAll('id_subscribe_fk = :id', [':id' => $model->id]);
                $model->delete();
                $res = array('success' => true, 'responseInfo' => 'Twoje zgłoszenie zostało usunięte');
            }
		} else {
            $res = array('success' => false, 'error' => 'Proszę podać adres e-mail');
        }
		
		return $res;
    } 

    public function actionQuestion() {
        $model = new \backend\Modules\Cms\models\CmsWidgetAccordionItem();
        $model->user_action = 'question-faq';
        $model->fk_id = 1;
        $model->status = 0;
        
        if(Yii::$app->request->ispost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            if($model->validate() && $model->save()) {
                return ['success' => true, 'title' => 'Dziękujemy za zgłoszenie', 'alert' => 'Twoje pytanie zostało wysłane'];
            } else {
                return ['success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formQuestionFaq', ['model' => $model]) ];
            }
        }
        return $this->renderPartial('_formQuestionFaq', ['model' => $model]);
    } 
}
