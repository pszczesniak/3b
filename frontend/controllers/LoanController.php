<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use backend\Modules\Apl\models\AplLoan;
use backend\Modules\Apl\models\AplCustomer;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\components\CustomHelpers;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use backend\extensions\kapi\gallery\GalleryManagerAction;

class LoanController extends Controller
{
    public $mainMenu = [];
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],*/
        ];
    }

     public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "offermessage" && $action->id !== "offercomment"); 
        return parent::beforeAction($action);
    }


    public function actionIndex()   {
        $query = AplLoan::find();

        if(Yii::$app->request->get('tag'))
            $query->andFilterWhere([
                'like', 'tags', Yii::$app->request->get('tag'),
            ]);

        if(Yii::$app->request->get('keyword'))
        {
            $keyword = strtr(Yii::$app->request->get('keyword'), array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\'));
            $keyword = Yii::$app->formatter->asText($keyword);

            $query->andFilterWhere([
                'or', ['like', 'title', $keyword], ['like', 'content', $keyword]
            ]);
        }

        $pagination = new Pagination([
            'defaultPageSize' => Yii::$app->params['blogPostPageCount'],
            'totalCount' => $query->count(),
        ]);

        $posts = $query->orderBy('created_at desc')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'posts' => $posts,
            'pagination' => $pagination,
        ]);
    }

     public function actionView($id, $title)  {
   
          $model = AplLoan::findOne($id);

        //var_dump($post->comments);
        return $this->render('view', [
            'post' => $model,
        ]);
    }
}
