<?php

namespace frontend\controllers;

use Yii;
use common\models\Files;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

class FilesController extends Controller
{
	
	public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "delete"); 
        return parent::beforeAction($action);
    }
    
    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new Files(['scenario' => Files::SCENARIO_UPLOAD]);
        
        /*if( in_array(Yii::$app->request->post('Files')['id_type_file_fk'], [1,2,3]) ) {
            $offer = \backend\Modules\Svc\models\SvcOffer::findOne(Yii::$app->request->post('Files')['id_fk']);
            if($offer->id_package_fk == 1) return ['result' => false, 'error' => 'Przekroczyłeś limit'];
            
            if(Yii::$app->request->post('Files')['id_type_file_fk'] == 1) {
                if($offer->id_package_fk == 2 && count($offer->images) >= 15 && Yii::$app->request->post('Files')['id_type_file_fk'] == 1) return ['result' => false, 'error' => 'Przekroczyłeś limit zdjęć'];
                if($offer->id_package_fk == 3 && count($offer->images) >= 30 && Yii::$app->request->post('Files')['id_type_file_fk'] == 1) return ['result' => false, 'error' => 'Przekroczyłeś limit zdjęć'];
            }
            
            if(Yii::$app->request->post('Files')['id_type_file_fk'] == 2) {
                if($offer->id_package_fk == 2 && count($offer->videos) >= 5 && Yii::$app->request->post('Files')['id_type_file_fk'] == 2) return ['result' => false, 'error' => 'Przekroczyłeś limit plików video'];
                if($offer->id_package_fk == 3 && count($offer->videos) >= 10 && Yii::$app->request->post('Files')['id_type_file_fk'] == 2) return ['result' => false, 'error' => 'Przekroczyłeś limit plików video'];
            }
            
            if(Yii::$app->request->post('Files')['id_type_file_fk'] == 3) {
                if($offer->id_package_fk == 2 && count($offer->audios) >= 10 && Yii::$app->request->post('Files')['id_type_file_fk'] == 3) return ['result' => false, 'error' => 'Przekroczyłeś limit plików mp3'];
                if($offer->id_package_fk == 3 && count($offer->audios) >= 20 && Yii::$app->request->post('Files')['id_type_file_fk'] == 3) return ['result' => false, 'error' => 'Przekroczyłeś limit plików mp3'];
            }
            
        }*/
		$model->load(Yii::$app->request->post());
        $model->show_client = 1;
       // $postData = Yii::$app->request->post('Files');
       // if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstances($model, 'files');
			//$model->name_file = $postData['name_file'];
            if ($model->upload()) {
                // file is uploaded successfully
                $new = $model->files_list_new;
                return ['result' => true, 'success' => 'Pliki zostały dodane', 'new' => $new];
            } else {
				$errors = $model->getErrors();
				foreach($errors as $key=>$value) {
					$error = $value;
				}
				return ['result' => false, 'error' => $value];
			}
       // }

        /*$this->render('upload', ['model' => $model])*/;
    }
    
    public function actionYoutube()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new Files(['scenario' => Files::SCENARIO_YOUTUBE]);
        
        if( in_array(Yii::$app->request->post('Files')['id_type_file_fk'], [1,2,3]) ) {
            $offer = \backend\Modules\Svc\models\SvcOffer::findOne(Yii::$app->request->post('Files')['id_fk']);
            if($offer->id_package_fk == 1) return ['result' => false, 'error' => 'Przekroczyłeś limit'];
 
            if(Yii::$app->request->post('Files')['id_type_file_fk'] == 2) {
                if($offer->id_package_fk == 2 && count($offer->videos) >= 5 && Yii::$app->request->post('Files')['id_type_file_fk'] == 2) return ['result' => false, 'error' => 'Przekroczyłeś limit plików video'];
                if($offer->id_package_fk == 3 && count($offer->videos) >= 10 && Yii::$app->request->post('Files')['id_type_file_fk'] == 2) return ['result' => false, 'error' => 'Przekroczyłeś limit plików video'];
            }            
        }
		$model->load(Yii::$app->request->post());
        $model->mime_file = 'video/youtube';
        $model->extension_file = 'youtube';
        $model->name_file = $model->title_file;
        $model->title_file = 'YOUTUBE';
        
        if ($model->save()) {
            // file is uploaded successfully
            return ['result' => true, 'success' => 'Pliki zostały dodane'];
        } else {
            $errors = $model->getErrors();
            foreach($errors as $key=>$value) {
                $error = $value;
            }
            return ['result' => false, 'error' => $value];
        }

        /*$this->render('upload', ['model' => $model])*/;
    }
	
	public function actionIndex($id, $type) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$files = [];
		$tmp = [];
		
		/*$model = $this->findModel($id);
		$filesData = $model->recipies;*/
		$filesData = Files::find()->where('status = 1')->andwhere('id_fk = '.$id)->andwhere('id_type_file_fk = '.$type)->all();
		
        
		$actionColumn = '<div class="edit-btn-group">';
		//$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
		$actionColumn .= '<a href="/files/update/%d" class="btn btn-xs btn-success btn-icon gridViewModal" data-table="#table-files-'.$type.'" data-target="#modal-grid-item">'.Yii::t('app', 'Edit').'</a>';

		$actionColumn .= '<a href="/files/delete/%d" class="btn btn-xs btn-danger deleteConfirm" data-confirm="Czy na pewno usunąć ten plik?" data-table="#table-files-'.$type.'">'.Yii::t('app', 'Delete').'</a>';
		$actionColumn .= '</div>';
		
		foreach($filesData as $key=>$value) {
			if($type == 1) {
                $iconColumn = '<a href="/files/getfile/'.$value->id.'"><img width="80" height="80" src="/uploads/thumbs/'.$value->extension_file.'"></a>';
            } else {
                if($value->extension_file == 'youtube') {
                    $iconColumn = '<a href="/files/getfile/'.$value->id.'"><span class="fa fa-youtube fa-2x"></span></a>';
                } else {
                    $iconColumn = '<a href="/files/getfile/'.$value->id.'"><span class="file-icon file-icon-sm" data-type="'.$value->extension_file.'"></span></a>';
                }
            }
            
            array_push($tmp, $value->title_file);        
            if(!in_array($value->extension_file, ['jpg', 'png', 'jpeg']))
                array_push($tmp, $iconColumn);
            else
                array_push($tmp, $iconColumn);
			array_push($tmp, sprintf($actionColumn, $value->id, $value->id, $value->id));
			//array_push($tmp, '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>');
			
			array_push($files, $tmp); $tmp = [];
		}
		
		return $files;
	}
	
	public function actionGetfile($id) {
		$model = $this->findModel($id);
		$file = 'uploads/' . $model->systemname_file. '.' .$model->extension_file;
		if (file_exists($file)) {
			header('Content-Description: File Transfer');
			//header('Content-Type: application/octet-stream');
			header('Content-Type: '.$model->mime_file);
			header('Content-Disposition: attachment; filename="'.basename($model->title_file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}
	
	/**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $model = $this->findModel($id); //->delete();
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -1;
        $model->user_action = 'delete';
        $files = 0;

        if($model->validate() && $model->save()) {
            return array('result' => true, 'alert' => 'Dokument zostały usunięty', 'action' => 'fileDelete', 'id' => $model->id);	
        } else {
            //var_dump($model->getErrors());
            return array('result' => false, 'error' => 'Do poczty musi być dołączony dokument', 'action' => 'fileDelete', 'id' => $model->id );	
        }
        
        //return $this->redirect(['index']);
    }
	
	/**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Files::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGalleryimages($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $images = array();
        
        $model = \backend\Modules\Cms\models\CmsGallery::findOne($id);
        
        foreach ($model->getImages() as $image) {
            $images[] = array(
                'id' => $image->id,
                'rank' => $image->rank,
                'name' => (string)$image->name,
                'link_href' => (string)$image->link_href,
                'description' => (string)$image->description,
                'preview' => $image->getUrl('preview'),
            );
        }
        
        return $images;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = "change";
        
        $types = [];
        //$typesData = Structure::find()->where(['type_fk' => 1, 'status' => 1, 'id_parent_fk' => 1])->all();
        /*foreach($typesData as $key => $item) {
            array_push($types, $item);
        }*/
	    
        if ($model->load(Yii::$app->request->post()) ) {

			Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('success' => true, 'id' => $model->id, 'new_title' => $model->title_file);	
		 	} else {
				return array('success' => false, 'html' => $this->renderPartial('_formEdit', [  'model' => $model, 'types' => $types]), 'errors' => $model->getErrors() );	
		    }
        } else {
            return $this->renderPartial('_formEdit', [
                'model' => $model, 'types' => $types
            ]);
        }

    }
	
}
