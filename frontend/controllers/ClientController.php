<?php

namespace frontend\controllers;

use Yii;
use common\models\Files;
use common\models\LoginForm;
use frontend\models\SignupForm;
use yii\web\UploadedFile;

use backend\Modules\Apl\models\AplCustomer;
use backend\Modules\Eg\models\Patient;
use backend\Modules\Eg\models\Visit;

use backend\Modules\Newsletter\models\NewsletterSubscribe;
use backend\Modules\Newsletter\models\NewsletterGroup;
use backend\Modules\Newsletter\models\NewsletterGroupSubscribe;
use common\models\User;
use common\models\PasswordForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use common\components\CustomHelpers;

/**
 * ClientController implements the CRUD actions for SvcClient model.
 */
class ClientController extends Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false; //($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    public function behaviors()
    {
        return [
           /* [
                'class' => 'yii\filters\AccessControl',
                'only' => ['create', 'update', 'delete', 'gallery', 'files', 'newsletter', 'socialmedia', 'inbox', 'location', 'requestPasswordReset'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function(){
                            return (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role->name == 'client');
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                   return $action->controller->redirect('/site/noaccess');

                   // return \Yii::$app->getUser()->loginRequired();
                },
            ],*/
            [
                'class' => 'yii\filters\AccessControl',
                'only' => ['data', 'account', 'index', 'account', 'basic', 'contact', 'finance', 'upload', 'saveavatar', 'changepassword', 'download'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return \Yii::$app->getUser()->loginRequired();
                },
            ],
            //...

        ];
    }

    public function actions() {
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'AplCustomer' => AplCustomer::className()
				]
			],
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
		];
	}
    
    public function onAuthSuccess($client) {
  
        $reponse = $client->getUserAttributes();

        $session = \Yii::$app->session;
        //echo $client->accessToken->params['access_token']; exit;
        $token = $client->accessToken->params['access_token'];

        $session->set('fb.token' ,$token);
        $id = ArrayHelper::getValue($reponse , 'id');
        $session->set('fb.id', $id);
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['socialmedia', 'id' =>  \Yii::$app->session->get('user.client')]);
       // }
    
    }
    
    public function actionFbdisconnet() {
        
        $session = \Yii::$app->session;
        
        $session->remove('fb.token');
        $session->remove('fb.id');
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['socialmedia', 'id' => \Yii::$app->session->get('user.client')]);
       // }
    
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();

        $post = $_GET; $where = [];
        if(isset($_GET['AplLoan'])) {
            $params = $_GET['AplLoan'];
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
				array_push($where, " (lower(lastname) like '%".strtolower( addslashes($params['lastname']) )."%' or lower(lastname) like '%".strtolower( addslashes($params['lastname']) )."%')");
            }
            if(isset($params['firstname']) && !empty($params['firstname']) ) {
				array_push($where, " (lower(firstname) like '%".strtolower( addslashes($params['firstname']) )."%' or lower(lastname) like '%".strtolower( addslashes($params['firstname']) )."%')");
            }
            if(isset($params['bank_name']) && !empty($params['bank_name']) ) {
				array_push($where, "lower(bank_name) like '%".strtolower( addslashes($params['bank_name']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "status = ".$params['status']);
            }
        } 
        
        $sortColumn = 'l.id';
        if( isset($post['sort']) && $post['sort'] == 'firstname' ) $sortColumn = 'firstname';
        if( isset($post['sort']) && $post['sort'] == 'lastname' ) $sortColumn = 'lastname';
        if( isset($post['sort']) && $post['sort'] == 'bank_name' ) $sortColumn = 'bank_name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'status';
        
		$query = (new \yii\db\Query())
            ->select(['l.id as id', 'l.created_at as created_at', 'l.status as status', 'l_amount', 'l_period'])
            ->from('{{%apl_loan}} as l')
            ->where( ['l.id_customer_fk' => $customer->id] )->andWhere('status != -1');
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            
        $rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-2' => 'danger', '0' => 'warning', 1 => 'info', 2 => 'success'];
        $status = ['-2' => 'odrzucony', '0' => 'do wysłania', 1 => 'wysłany', 2 => 'przyjęty'];
  
		foreach($rows as $key => $value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/wniosek/szczegoly/%d" class="btn-icon"><i class="fa fa-eye"></i></a>';
            //$actionColumn .= '<a href="/panel/pl/apl/aplcustomer/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            //$actionColumn .= ( ($value['status'] == 1 ) ? '<a href="/panel/pl/apl/aplcustomer/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['l_amount'] = $value['l_amount'];
            $tmp['l_period'] = $value['l_period'];
            $tmp['l_number'] = '#'.$value['id'];
            $tmp['created_at'] = $value['created_at'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-teal btn--small" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Lists all SvcOffer models.
     * @return mixed
     */
    public function actionIndex()  {
        $searchModel = new SvcOfferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single SvcOffer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Updates an existing SvcOffer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAccount() {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        } else {
            $model = Patient::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            /*if($model->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($model->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }*/
        }
        //$model = $this->findModel($id);
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['account']);
        } else {
            return $this->render('account', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionBasic() {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        } /*else {
            $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            $model->scenario = "basic";
            if($model->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($model->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }
        }*/
        
      
        //$model = $this->findModel($id);
        $model = Patient::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
		$model->scenario = "basic";
        $model->user_action = "basic";
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', 'Twój profil został zaktualizowany.');
            return $this->redirect(['account']);
        } else {
            return $this->render('basic', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionContact()  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        } else {
            $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            
            $addresses = $model->addresses;
            $model->address_city = $addresses['normal']['address_city'];
            $model->address_postalcode = $addresses['normal']['address_postalcode'];
            $model->address_street = $addresses['normal']['address_street'];
            $model->address_street_number = $addresses['normal']['address_street_number'];
            $model->address_local_number = $addresses['normal']['address_local_number'];
            $model->address_status = $addresses['normal']['address_status'];
            $model->c_address = $addresses['normal']['correspondence'];
            
            //if(!$model->address_for_correspondence) $model->c_address = 0; else $model->c_address = 1;
            if($model->c_address) {
                $model->c_address_city = $addresses['correspondence']['address_city'];
                $model->c_address_postalcode = $addresses['correspondence']['address_postalcode'];
                $model->c_address_street = $addresses['correspondence']['address_street'];
                $model->c_address_street_number = $addresses['correspondence']['address_street_number'];
                $model->c_address_local_number = $addresses['correspondence']['address_local_number'];
            }
            
            if($model->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($model->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }
        }
        //$model = $this->findModel($id);
		$model->scenario = "contact";
        $model->user_action = "contact";
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', 'Twoje dane kontaktowe zostały zaktualizowane.');
            return $this->redirect(['account']);
        } else {
            return $this->render('contact', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionFinance()  {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/client/login']));
        } else {
            $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
            if($model->status == 1) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane kontaktowe.');
                return $this->redirect(Url::to(['/apl/contact']));
            }
            
            if($model->status == 2) {
                Yii::$app->session->setFlash('error', 'Proszę uzupełnić dane finansowe.');
                return $this->redirect(Url::to(['/apl/finance']));
            }
        }
        //$model = $this->findModel($id);
		$model->scenario = "finance";
        $model->user_action = "finance";
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', 'Twoje dane finansowe zostały zaktualizowane.');
            return $this->redirect(['account']);
        } else {
            return $this->render('finance', [
                'model' => $model, 
            ]);
        }
    }

    /**
     * Deletes an existing SvcOffer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SvcClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
       // $id = CustomHelpers::decode($id);
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
      
    public function actionFiles($id, $type)  {
        return $this->render('files', [
            'model' => $this->findModel($id), 'type' => $type
        ]);
    }
    
    public function actionNewsletter($id)  {
        $model = $this->findModel($id);
        
        $subscribeExist = NewsletterSubscribe::find()->where(['email' => $model->email, 'status' => 1])->one();
        if(!$subscribeExist) { 
            $subscribeExist = new NewsletterSubscribe();
            $subscribeExist->email = $model->email;
            $subscribeExist->name = ($model->firstname || $model->lastname) ? $model->firstname.' '.$model->lastname : $model->email;
            if(!$subscribeExist->save()) {
                Yii::$app->session->setFlash('error', 'Nie masz ustawionego adresu email. Wybierz opcje "Twój profil" i uzupełnij brakujące dane.');
            }
        }
        
        /* save begin */
	
        if (Yii::$app->request->post('Newsletter')['groups']) {
            //$result = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::find()->where(['id_subscribe_fk', $model->id])->delete();
            $result = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::deleteAll('id_subscribe_fk = :id', [':id' => $subscribeExist->id]);
            foreach (Yii::$app->request->post('Newsletter')['groups'] as $key => $value) {
                $ngs = new \backend\Modules\Newsletter\models\NewsletterGroupSubscribe();
                $ngs->id_group_fk = $value;
                $ngs->id_subscribe_fk = $subscribeExist->id;
                $ngs->email = $subscribeExist->email;
                $ngs->save();
            }
           // $res = array('success' => true, 'responseInfo' => (!$subscribeExist) ? 'Twoje zgłoszenie zostało zapisane' : 'Twoje zgłoszenie zostało zaktualizowane');
        } 

        /* save end */
        
        
        $groups = NewsletterGroup::find()->orderby('name')->all();
       
        // $checked = NewsletterGroupSubscribe::find()->where(['email' => $model->email])->all();
        $checked = $subscribeExist->subscribegroups;
        $subscribes = []; 
        if($subscribeExist) {
            $checked = $subscribeExist->subscribegroups; 
            // $checked = \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::findOne($subscribeExist->id); 
            if($checked) {
                foreach($checked as $key => $value) {
                    array_push($subscribes, $value->id_group_fk);
                }
            }
        }
       
        return $this->render('newsletter', [
            'model' => $model, 'groups' => $groups, 'subscribes' => $subscribes
        ]);
    }
 
    public function actionUpload($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        //$post = Yii::$app->request->post(); var_dump($post);
		$model = new Files(['scenario' => Files::SCENARIO_AVATAR]);//var_dump($_FILES);
		//$model->load(Yii::$app->request->post());
        //$model->files = $post['upload'];
       // $postData = Yii::$app->request->post('Files');
        if (Yii::$app->request->isPost) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
			//$model->name_file = 'avatar';
            if ($file = $model->uploadAvatar('avatar-'.$id)) {
                // file is uploaded successfusesslly
                return ['result' => true, 'success' => 'Pliki zostały dodane', 'file' => $file];
            } else {
				$errors = $model->getErrors();
				foreach($errors as $key=>$value) {
					$error = $value;
				}
				return ['result' => false, 'error' => $error];
			}
        }
	   return ['result' => 'ok'];

        /*$this->render('upload', ['model' => $model])*/;
    }
    
    public function actionSaveavatar($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        //var_dump($_POST);
        //$id = CustomHelpers::decode($id);
        if(isset($_POST['avatar'])) {
            $avatar = $_POST['avatar'];
            $avatar = str_replace('data:image/png;base64,', '', $avatar);
            $avatar = str_replace(' ', '+', $avatar);
            $data = base64_decode($avatar);
            $file = 'uploads/avatars/thumb/avatar-'.$id.'.png';
            $success = file_put_contents($file, $data);
            
            return ['result' => 'ok'];
        } else {
            return ['result' => 'error'];
        } 
    }
    
    public function actionChangepassword(){
        $model = new PasswordForm;
        $modeluser = User::findOne(Yii::$app->user->identity->id);
        $modeluser->scenario = User::SCENARIO_PASSWORD;
        
        $modelclient = AplCustomer::find()->where(['id_user_fk' => Yii::$app->user->identity->id])->one();
     
        if( $model->load(Yii::$app->request->post()) && $model->validate() ) {
            try{
                //$modeluser->password = $_POST['PasswordForm']['newpass'];
                $modeluser->setPassword($model->newpass); 
				$modeluser->password = $model->newpass;
                if($modeluser->save()){
                    Yii::$app->getSession()->setFlash(
                        'success', Yii::t('app', 'Password changed')
                    );
                   // return $this->redirect(['index']);
                }else{ //var_dump($modeluser->getErrors()); exit;
                    Yii::$app->getSession()->setFlash(
                        'error', Yii::t('app', 'Password not changed')
                    );
                  //  return $this->redirect(['index']);
                }
                return $this->render('changepassword',[
                    'model'=>$model, 'modeluser' => $modelclient
                ]);
            } catch(Exception $e){
                Yii::$app->getSession()->setFlash(
                    'error',"{$e->getMessage()}"
                );
                return $this->render('changepassword',[
                    'model'=>$model, 'modeluser' => $modelclient
                ]);
            }
        } else{
            return $this->render('changepassword',[
                'model'=>$model, 'modeluser' => $modelclient
            ]);
        }
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()   {
        /*if (!\Yii::$app->user->isGuest) {
           //echo Yii::$app->user->identity->role->name; exit;
            if(Yii::$app->user->identity->role->name || !Yii::$app->user->can('client')) {
                Yii::$app->getSession()->setFlash(
                    'error', 'Próbowałeś się zalogować w serwisie jako klient. W ramach polityki bezpieczeństwa taka operacja jest niemożliwa do momentu aż nie wylogujesz się z panelu admina'
                );
                return $this->redirect(Url::to(['/panel']));
            } else {
                return $this->goHome();
            }
        }*/
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
		
			$client = \backend\Modules\Eg\models\Patient::find()->where(['id_user_fk' => Yii::$app->user->id])->one(); 
            
            if(!$client) {
               // $role = Yii::$app->user->identity->role->name;
                Yii::$app->user->logout();
                
                Yii::$app->getSession()->setFlash( 'error', 'Klient o takim loginie nie został zidentyfikowany.' );
                
                return $this->redirect(Url::to(['/client/login']));

            } else {
                \Yii::$app->session->set('user.client', ($client)?$client->id:0);
               /* if($client->status == 1) {
                    return $this->redirect(Url::to(['/apl/contact'])); 
                } else	{*/	     
                    return $this->redirect(Url::to(['/client/account']));               
               // }
            }
				
            //return $this->goBack();
           
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
 
    public function actionNote() {
        
        //$model = $this->findModel($id);
        $note = new \backend\Modules\Apl\models\AplInfo();
        $note->type_fk = 1;
        
        if( isset($_GET['parent']) ){
            $parent = \backend\Modules\Apl\models\AplInfo::findOne($_GET['parent']);
            $note->id_parent_fk = $parent->id;
            $note->id_loan_fk = $parent->id_loan_fk;
            $note->id_customer_fk = $parent->id_customer_fk;
        } else {
            if($_GET['type'] == 1) {
                $note->id_customer_fk = $_GET['id'];
                $note->id_loan_fk = 0;
            } else {
                $loan = \backend\Modules\Apl\models\AplLoan::findOne($_GET['id']);
                $note->id_loan_fk = $_GET['id'];
                $note->id_customer_fk = $loan->id_customer_fk;
            }
            $note->id_parent_fk = 0;
        }
        $note->status = 0;
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $note->load(Yii::$app->request->post());
            $note->status = 1;
            if($note->save()) {
                //$note->created_at = 'dodany teraz';
                try {
                    \Yii::$app->mailer->compose(['html' => 'clientAction-html', 'text' => 'clientAction-text'], ['model' => $note->loan, 'info' => 'Do systemu wpłynęła nowa wiadomość od klienta', 'id' => $note->id ])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                    //->setTo($customer->email)
                    ->setTo(\Yii::$app->params['bccEmail'])
                    ->setSubject('Nowe zdarzenie w serwisie GTW Finanse')
                    ->send();
                } catch (\Swift_TransportException $e) {
                    $note->status = 1;
                }
                
                $html = '<div class="comment" id="comment-'.$note->id.'">'
                            .'<img src="/images/default-user.png" alt="" class="comment-avatar">'
                            .'<div class="comment-body">'
                                .'<div class="comment-text">'
                                    .'<div class="comment-header">'
                                        .'<a href="#" title="">Twoja wiadomość</a><span>'.date('Y-m-d H:i:s').'</span>'
                                    .'</div>'
                                    .$note->info_content
                                .'</div>'
                                .'<div class="comment-footer">'
                                    //.'<a href="#"><i class="fa fa-thumbs-o-up"></i></a>'
                                    //.'<a href="#"><i class="fa fa-thumbs-o-down"></i></a>'
                                    .'<a href="'.Url::to(['/client/note', 'parent' => $note->id]).'" class="viewModal" data-target="#modal-view" data-title="Odpowiedź na wiadomość" data-icon="envelope">Odpowiedz</a>'
                                .'</div>'
                            .'</div>'
                        .'</div>';
                
                /*$modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                if(!$modelArch->save()) var_dump($modelArch->getErrors());*/
                
                return array('success' => true,  'action' => 'add_note', 'title' => 'Dziękujemy za kontakt', 'alert' => 'Wiadomność została wysłana', 'parent' => $note->id_parent_fk, 'html' => $html );
            } else {
                return array('success' => false, 'errors' => $note->getErrors(), 'html' => $this->renderPartial('_noteForm', [ 'model' =>$note ]), );
            }
        }
        return $this->renderPartial('_noteForm', ['model' => $note]);
    }
    
    public function actionTestimonials() {
        $customer = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        
        $model = new \backend\Modules\Mix\models\MixComment();
        $model->id_fk = 0;
        $model->id_user_fk = \Yii::$app->user->id;
        $model->id_client_fk = $customer->id;
        $model->name = $customer->fullname;
        $model->email = $customer->email;
        $model->status = 0;
        
        if (Yii::$app->request->isPost ) {
            if ($model->load(Yii::$app->request->post())) { 
                if($model->save()) {
                    Yii::$app->session->setFlash('success', 'Twój komentarz został wysłany do weryfikacji.');	
                } else {
                    Yii::$app->session->setFlash('error', 'Twój komentarz nie został wysłany do weryfikacji. Prosimy spróbować ponownie');
                    //var_dump($model->getErrors());exit;
                }
            } 
        } 
        $testimonials = \backend\Modules\Mix\models\MixComment::find()->where(['id_client_fk' => $customer->id])->orderby('id desc')->all();  
        return $this->render('testimonials', ['model' => $model, 'testimonials' => $testimonials]);
    }
    
    public function actionUpdate() {
        $model = AplCustomer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
        $model->scenario = "update";
        
        if($model->own_car) {
            $carData = $model->car;
            $model->car_type = $carData['type'];
            $model->car_year = $carData['year'];
            $model->car_insurance = $carData['insurance'];
        }
		
		$addresses = $model->addresses;
		
		if($model->address) {
			$model->address_city = $addresses['normal']['address_city'];
			$model->address_postalcode = $addresses['normal']['address_postalcode'];
			$model->address_street = $addresses['normal']['address_street'];
			$model->address_street_number = $addresses['normal']['address_street_number'];
			$model->address_local_number = $addresses['normal']['address_local_number'];
			$model->address_status = $addresses['normal']['address_status'];
		}
        
        if($model->c_address) {
			$model->c_address_city = $addresses['correspondence']['address_city'];
			$model->c_address_postalcode = $addresses['correspondence']['address_postalcode'];
			$model->c_address_street = $addresses['correspondence']['address_street'];
			$model->c_address_street_number = $addresses['correspondence']['address_street_number'];
			$model->c_address_local_number = $addresses['correspondence']['address_local_number'];
		}
                
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            if($model->save()) {
               
                return array('success' => true,  'action' => 'client_update', 'title' => 'Dane zostały zaktualizowane', 'alert' => 'Dane zostały zaktualizowane' );
            } else {
                return array('success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formUpdate', [ 'model' => $model ]), );
            }
        }
        return $this->renderPartial('_formUpdate', ['model' => $model]);
    }
    
    public function actionBooking() {
        $model = new Visit();
        $model->user_action = 'booking';
        $model->status = 1;
        $patient = \backend\Modules\Eg\models\Patient::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        $model->id_patient_fk = $patient->id;
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
           // $model->start = $model->fromDate . ':00';
           // $model->end = $model->toDate . ':00' ;
           $row = [];
           $row['term'] = $model->visit_day;
            if($model->save()) {
                return array('success' => true,  'terms' => $this->renderPartial('_terms', ['id' => $patient->id]), 'action' => 'termBooking', 'title' => 'Rezerwacja', 'alert' => 'Zgłoszenie zostało wysłane' );
            } else {
                return array('success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formBooking', ['model' => $model]), );
            }
        }
        
        return $this->renderPartial('_formBooking', ['model' => $model]);
    }
    
    public function actionUbooking($id) {
        $model = Visit::findOne($id);
        $model->user_action = 'ubooking';
        $model->status = 1;
        $model->fromDate = date('Y-m-d H:i', strtotime($model->start));
        $model->toDate = date('Y-m-d H:i', strtotime($model->end));
        $patient = \backend\Modules\Eg\models\Patient::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        $model->id_patient_fk = $patient->id;
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            $model->start = $model->fromDate . ':00';
            $model->end = $model->toDate . ':00' ;
            if($model->save()) {
                return array('success' => true,  'action' => 'client_update', 'title' => 'Aktualizacja terminu', 'alert' => 'Dane zostały zaktualizowane' );
            } else {
                return array('success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formBooking', ['model' => $model]), );
            }
        }
        
        return $this->renderPartial('_formBooking', ['model' => $model]);
    }
    
    public function actionGallery($gid, $gslug) {   
        $access = false; $passError = false;
      
        $model = \backend\Modules\Eg\models\Visit::findOne($gid);
        if($model->status == -1) {
            return $this->render('deleted', ['model' => $model]);
        }
        if(isset($_POST['accessCode']) && trim($_POST['accessCode']) == trim($model->access_code))  {
            $access = true;
        } else {
			if(isset($_POST['accessCode'])){
				//echo trim($_POST['accessCode']) .' => '. trim($model->access_code);exit;
				$passError = true;
			}
		}
        return $this->render('gallery', ['model' => $model, 'access' => $access, 'passError' => $passError]);
    }
    
    public function actionUgallery($gid) {         
        $model = \backend\Modules\Eg\models\Visit::findOne($gid);

        return $this->render('ugallery', ['model' => $model]);
    }
    
    public function actionEphoto($id) {
        $model = \backend\Modules\Cms\models\CmsGalleryPhoto::findOne($id);
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                return array('success' => true,  'action' => 'edit_photo', 'title' => 'Edycja zdjęcia', 'alert' => 'Zmiany zostały zapisane.');
            } else {
                return array('success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formPhoto', ['model' => $model]), );
            }
        }
        
        return $this->renderPartial('_formPhoto', ['model' => $model]);
    }
    
    public function actionMessage() {
        $model = new \backend\Modules\Mix\models\MixMessage();
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $model->email = $user->email;
       
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            if($model->save() && $model->send('kamila_bajdowska@onet.eu')) {
                return array('success' => true,  'action' => 'send_msg', 'title' => 'Formularz kontaktowy', 'alert' => 'Wiadomość została wysłana');
            } else {
                return array('success' => false, 'errors' => $model->getErrors(), 'html' => $this->renderPartial('_formMsg', ['model' => $model]), );
            }
        }
        
        return $this->renderPartial('_formMsg', ['model' => $model]);
    }
    
    public function actionDownload($id) {
        $model = \backend\Modules\Eg\models\Visit::findOne($id);
        $gallery = \backend\Modules\Cms\models\CmsGallery::findOne($model->id_gallery_fk);
        $zip = new \ZipArchive();
		//$zipname = 'uploads/matters/docs_'.$id.'.zip';
        $tempPath = 'uploads/zip/';
        $zipname = 'galeria_'.$model->id_gallery_fk.'_'.time().'.zip';
		$res = $zip->open($tempPath.$zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE); 
        $i = 0;
       // if ($res === TRUE) {
            foreach($gallery->itemsall as $key => $photo) { 
                $file = 'uploads/gallery/'.$gallery->id.'/'.$photo->id.'/original.jpg';
                $zip->addFile($file, 'photo_'.$photo->id.'.jpg');
                
                ++$i;
            }	

            if(!$gallery->itemsall || $i == 0) {
                $file = "uploads/brak_zdjec.txt";  
                $zip->addFile($file, 'brak zdjec');
            }
			
         $zip->close();
				header('Content-Type: application/zip');
				header('Content-disposition: attachment; filename='.$zipname);
				//header('Content-Length: ' . filesize($tempPath.$zipname));
				
				//if(!$ids) {
					//readfile('https://forfunatrakcje.pl/uploads/zip/galeria_42_1565715116.zip');
					readfile('https://forfunatrakcje.pl/uploads/zip/'.$zipname);
					exit;
				/**} else {
					Yii::$app->response->format = Response::FORMAT_JSON;
					return ['zip' => '/'.$tempPath.$zipname];
				}*/
			
			/*} else {
				return $this->redirect(['ugallery', 'gid' => $model->id_gallery_fk]);
			}*/
    }
    
    function actionDownloadold($id) {
        error_reporting(E_ALL);
        ini_set("display_errors", "On");
        //clearstatcache();
        //if the zip file already exists and overwrite is false, return false
        //chdir( sys_get_temp_dir() );
        $zip = new \ZipArchive();
        $zipname = 'test.zip';
        $filename = "uploads/zip/test.zip";
        //$tmp_zipname = uniqid(); // Generate a temp UID for the file on disk.
        //$filename = 'package_name.zip';
        //$filename = tempnam(sys_get_temp_dir(), 'zip');

        if ($zip->open($filename, \ZIPARCHIVE::CREATE | \ZipArchive::OVERWRITE )!==TRUE) {      
            exit("cannot open <$filename>\n");
        } 
        $zip->addFile("uploads/zip/test.txt", 'test.txt');

        //$thisdir = "$_SERVER[DOCUMENT_ROOT]/zip";
        //$zip->addFile($filename . "./uploads/company/logo.png");
        //echo "numfiles: " . $zip->numFiles . "\n";
       // echo "status:" . $zip->status . "\n";
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zipname);
        header('Content-Length: ' . filesize($filename));
            readfile( $filename );exit;
            //unlink( $filename );
        
        //echo 'Archive created!'; 
        //check to make sure the file exists
        //echo file_exists($tempPath); exit;
       /* header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=test.zip"); 
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile($tempPath);
        exit;*/

    }
} 
