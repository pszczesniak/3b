<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'pl',
    'name' => '3b-artstudio',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation'=>true,
			'baseUrl'=>'',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'logout' => 'site/logout',
                'strefa-klienta/login' => 'client/login',
                'site/noaccess' => 'site/noaccess',
				'search' => 'site/search',
                'opinie-klientow' => 'site/testimonials', 
                'strefa-klienta' => 'apl/index',
                'strefa-klienta/rejestracja/<rid:\d+>' => 'apl/register', 
                'strefa-klienta/dane-kontaktowe' => 'apl/contact', 
                'strefa-klienta/dane-finansowe' => 'apl/finance', 
                'strefa-klienta/akceptacja/<lid:\d+>' => 'apl/accept', 
                'strefa-klienta/send/<lid:\d+>' => 'apl/send', 
                'strefa-klienta/discard/<lid:\d+>' => 'apl/discard', 
                'strefa-klienta/save/<lid:\d+>' => 'apl/save', 
                'strefa-klienta/weryfikacja/<lid:\d+>' => 'apl/verification', 
                'strefa-klienta/szczegoly/<lid:\d+>' => 'apl/loan', 
                'strefa-klienta/nowy/<rid:\d+>' => 'apl/create', 
                'strefa-klienta/calc' => 'apl/calc',
                'strefa-klienta/galerie-uzytkownikow' => 'site/galleries',
                'strefa-klienta/galerie-uzytkownikow/<gid>/<gslug>' => 'client/gallery',
                'strefa-klienta/edycja-galerii/<gid>' => 'client/ugallery',
                'strefa-klienta/download/<id>' => 'client/download',
                '<pid:\d+>-<slug:[A-Za-z0-9 -_.]+>' => 'site/page',
				'c-<cid:\d+>-<cslug:[-a-zA-Z]+>' => 'site/category',
				'g-<gid:\d+>' => 'site/gallery',
				'<cslug>/p-<pid:\d+>-<pslug>' => 'site/cpage',
                'files/upload' => 'files/upload',
                'files/getfile/<id>' => 'files/getfile',
                'files/delete/<id>' => 'files/delete',
                'files/update/<id>' => 'files/update',
				'blog' => 'blog/index',
                'blog/catalog/<id>-<surname>' => 'blog/catalog',
                'blog/post/<id>-<title>' => 'blog/view',
                'strefa-klienta/twoje-konto' => 'client/account',
                'strefa-klienta/edycja-profilu' => 'client/edit', 
                '<controller>/<action>' => '<controller>/<action>',
            ],
        ],
        
    ],
    'params' => $params,
];
