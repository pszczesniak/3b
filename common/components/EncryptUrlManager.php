<?php 
namespace common\components;

use common\components\CustomHelpers;
use yii\web\UrlManager;
use yii\web\UrlRule;
use yii\web\UrlRuleInterface;
use Yii;

class EncryptUrlManager extends UrlManager
{
    public function createUrl($params)
    {

		$key = 'szyfr';
		if( isset($params['id']) ) {
            if( isset($params[0]) && $params[0] != 'blog/imgAttachApi' )
                $params['id'] = CustomHelpers::encode($params['id']);
	    }
        return parent::createUrl($params);
    }
}