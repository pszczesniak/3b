<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$veryfityLink = \Yii::$app->urlManager->createAbsoluteUrl(['apl/verification', 'id' => $loan->id]);
?>
<div class="password-reset">
    <h1>Kliknij aby potwierdzić złożenie wniosku #<?= $loan->id ?></h1>
    <p>Witaj <?= $client->fullname ?></p>
    <p>Chcielibyśmy się upewnić, że Twój adres e-mail podany we wniosku o Pożyczkę jest poprawny Dodatkowo kliknięcie w poniższy  rzycisk będzie dla nas również potwierdzeniem, że to właśnie Ty jesteś osobą, która wypełniła wniosek o pożyczkę. Aby potwierdzić adres e-mail, kliknij "Tak, to ja". </p>

    <p style="text-align:center;"><?= Html::a('Tak to ja', $veryfityLink, ['class' => 'button']) ?></p>
    <br/><br />
    <i>
        Jeżeli ta wiadomość dotarła do Ciebie przez pomyłkę – przekaż ją proszę na adres biuro@gtw-finanse.pl,  dzięki czemu będziemy mogli łatwiej ustalić przyczynę. Bardzo dziękujemy za Twoją pomoc!
    </i>
</div>
