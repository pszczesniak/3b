<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
Witaj,

W systemie zostało utworzone konto powiązane z tym adresem e-mail. Aby się zalogować kliknij w poniższy link i skorzystaj z danych autoryzacyjnych: login: <?= $login ?> / hasło: <?= $password ?>

<?= $resetLink ?>
