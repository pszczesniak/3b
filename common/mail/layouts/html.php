<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        body { font-size:11px; font-family:arial; margin:0px; background: #fff; }
        .btnLink { display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #1F398E; border-radius: 60px; text-decoration:none; }
        .bgRed {background-color: #fc3c07;}
        .bgGreen {background-color: #2bdb43;}
        .bgBlue {background-color: #04b2e8;}
        .bgTeal {background-color: #00c0c8;}
        .bgPurple {background-color: #a287d4;}
        
        .textPurple {olor: #a287d4;}

        .mainContent { text-align: center; background: #fff; padding: 0px 0px; font-size:11px; width: 100%; color: #514d6a;}
        .systemInfo {font-size:11px; }
        #bodyTable {  border: 1px solid #E8E8E8; font-size:11px; }
        #bodyTable td {  border: 1px solid #E8E8E8;  padding:5px;}
		ol { padding: 17px; }
    </style>
</head>
<body>
    <?php $this->beginBody() ?>
    <div width="100%" class="content">
        <div style="padding:20px 0;  margin: 0px auto; font-size: 11px">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
                <tbody>
                    <tr>
                        <td style="vertical-align: top; padding-bottom:30px;" align="center">
                            <div style="text-align: center; font-size: 10px; color: #b2b2b5;">Wiadomość wygenegrowana automatycznie przez system <?= Yii::$app->name ?></div><br />
                            <img src="<?= $message->embed(\backend\Modules\Company\models\Company::getLogoUrl()); ?>" alt="Logo" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="padding: 10px; background: #fff;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td class="mainContent">
                                <?= $content ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                <p> <?= Yii::$app->name ?> <br>
                <a href="javascript: void(0);" style="color: #b2b2b5; text-decoration: underline;"><?= date('Y') ?></a> </p>
            </div>
        </div>
    </div>

    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
