<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = \Yii::$app->urlManagerFrontEnd->createUrl('/'.\Yii::$app->params['frontendNativeLang'].'strefa-klienta/login');
?>
<div class="password-reset">
    <h1>Witaj <?= $username ?></h1>
    <p>Dziękujemy za rejestracje konta w serwisie <?= \Yii::$app->name ?>. Poniżej dane do Twojego konta - zapamiętaj je, ponieważ będą potrzebne przy każdorazowym logowaniu na portalu.</p>
    <p>Aby się zalogować kliknij w poniższy link i skorzystaj z danych autoryzacyjnych: <br />login: <?= $login ?> / hasło: <?= $password ?></p>

    <p style="text-align:center;"><?= Html::a('Zaloguj się', $resetLink, ['class' => 'btnLink']) ?></p>
</div>
