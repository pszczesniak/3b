<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = \Yii::$app->urlManagerFrontEnd->createUrl('login');
?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <td style="background:#fb9678; padding:20px; color:#fff; text-align:center;"> Warning: You're approaching your limit. Please upgrade. </td>
        </tr>
    </tbody>
</table>
<div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td><p>Your Trial Expire from <b>19th sep 2017</b></p>
                    <p>Add your credit card now to upgrade your account to a premium plan to ensure you don't miss out on any reports.</p>
                    <center>
                        <a href="javascript: void(0);" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #00c0c8; border-radius: 60px; text-decoration:none;">Upgrade My Account</a>
                    </center>
                    <b>- Thanks (Elite team)</b> 
                </td>
            </tr>
        </tbody>
    </table>
</div>
