<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$worflowLink = \Yii::$app->urlManagerFrontEnd->createUrl('login');
?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <?php if($model->user_action == 'approved') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgGreen"> Akceptacja </td>
            <?php } else if ($model->user_action == 'rejected') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgRed"> Odrzucenie </td>
            <?php } else if ($model->user_action == 'feedback') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgPurple"> Informacja zwrotna </td>
            <?php } ?>
        </tr>
    </tbody>
</table>
<div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td><p><b><?= $model->workflow['name'] ?></b></p>
                    <p><?= $model->describe ?></p>
                    <center>
                        <a href="<?= $worflowLink ?>" class="btnLink">Obieg dokumentów</a>
                    </center>
                    <b>- <?= $model->user['fullname'] ?> (wiadomość wygenerowana automatycznie przez system) </b> 
                </td>
            </tr>
        </tbody>
    </table>
</div>
