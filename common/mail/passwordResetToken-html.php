<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Witaj <?= Html::encode($user->username) ?>,<br /></p>

    <p>Kliknij poniższy link, aby zresetować hasło:</p>

    <p style="text-align:center;"><a href="<?= $resetLink ?>" class="btnLink">Resetuj hasło</a></p>
</div>
