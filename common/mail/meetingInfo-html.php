<?php
use yii\helpers\Html;
use \backend\Modules\Task\models\CalCase;

/* @var $this yii\web\View */
/* @var $user common\models\User */
 ?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" style="height: 50px;" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <?php if($alert == 'add') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgGreen"> Pracownik <?= $model->creator ?> zaprasza na spotkanie </td>
            <?php } ?>
            <?php if($alert == 'remove') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgRed">Pracownik <?= $model->meeting['removing'] ?> odwołał spotkanie</td>
            <?php } ?>
            <?php if($alert == 'accept') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgGreen">Pracownik <?= $model->employee['fullname'] ?> przyjął zaproszenie na spotkanie</td>
            <?php } ?>
            <?php if($alert == 'discard') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgRed">Pracownik <?= $model->employee['fullname'] ?> odrzucił zaproszenie na spotkanie<br />Powód: <?= $model->comment ?></td>
            <?php } ?>
            <?php if($alert == 'note') { ?>
                <td style="padding:20px; color:#fff; text-align:center;" class="bgPurple">Pracownik <?= $note->user ?> dodał komentarz: "<?= $note->note ?>"</td>
            <?php } ?>
        </tr>
    </tbody>
</table>


<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr><td style="background-color:#eee; width:50%;">Początek</td><td style="background-color:#eee; width:50%;">Koniec</td></tr>
    <tr><td><b><?= $model->meeting['date_from'] ?></b></td><td><b><?= $model->meeting['date_to'] ?></b></td></tr> 
    <tr><td style="background-color:#eee;" colspan="2">Tytuł</td></tr>
    <tr><td colspan="2"><a href="<?= \Yii::$app->urlManager->createAbsoluteUrl(['/community/meeting/view','id' => $model->id_meeting_fk]) ?>"><?=  $model->meeting['title'] ?></a></td></tr>
    <tr><td style="background-color:#eee;" colspan="2">Powód</td></tr>
    <tr><td colspan="2"><?= ($model->meeting['purpose']) ? $model->meeting['purpose'] : 'brak dodatkowego opisu' ?></td></tr>
    <?php if($alert == 'add' || $alert == 'change-term') { ?>
    <tr>
        <td style="background-color:green;color:#fff;font-size:18px;text-align:center;" ><a style="color: #fff;" href="<?= $model->accepturl ?>">AKCEPTUJ</a></td>
        <td style="background-color:red;color:#fff;font-size:18px;text-align:center;" ><a style="color: #fff;" href="<?= $model->discardurl ?>">ODRZUĆ</a></td>
    </tr>
    <?php } ?>
</table>



