<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$loanLink = \Yii::$app->urlManagerBackEnd->createUrl('/apl/aplloan/'.$model->id).'?info='.$id;
?>
<div class="password-reset">
    <h1>Wniosek #<?= $model->id ?></h1>
    <p>Klient <?= $model->customer['fullname'] ?></p>
    <p><?= $info ?></p>

    <p style="text-align:center;"><?= Html::a('Karta wniosku', $loanLink, ['class' => 'button']) ?></p>
    <br/><br />

</div>
