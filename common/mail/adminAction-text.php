<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$loanLink = \Yii::$app->urlManagerFrontEnd->createUrl('/wniosek/szczegoly/'.$model->id);
?>
<div class="password-reset">
    <h1>Wniosek #<?= $model->id ?></h1>
    <p>Witaj <?= $model->customer['fullname'] ?></p>
    <p><?= $info ?></p>

    <p style="text-align:center;"><?= Html::a('Karta wniosku', $loanLink, ['class' => 'button']) ?></p>
    <br/><br />
    <i>
        Jeżeli ta wiadomość dotarła do Ciebie przez pomyłkę – przekaż ją proszę na adres biuro@gtw-finanse.pl,  dzięki czemu będziemy mogli łatwiej ustalić przyczynę. Bardzo dziękujemy za Twoją pomoc!
    </i>
</div>
