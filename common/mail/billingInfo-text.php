<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = \Yii::$app->urlManagerFrontEnd->createUrl('login');
?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <td style="background:#fb9678; padding:20px; color:#fff; text-align:center;"> Warning: You're approaching your limit. Please upgrade. </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <td style="background:#00c0c8; padding:20px; color:#fff; text-align:center;"> Thank you very much for using Eliteadmin </td>
        </tr>
    </tbody>
</table>
<div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td><b>John Doe</b>
                    <p style="margin-top:0px;">Invoice #52342</p>
                </td>
                <td align="right" width="100"> 
                    20th Aug 2017 
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:20px 0; border-top:1px solid #f6f6f6;">
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;">Service 1</td>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;"  align="right">$ 20.00</td>
                                </tr>
                                <tr>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0;">Service 2</td>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0;" align="right">$ 30.00</td>
                                </tr>
                                <tr>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0;">Service 3</td>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0;" align="right">$ 10.00</td>
                                </tr>
                                <tr class="total">
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0; font-weight:bold;" width="80%">Total</td>
                                  <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0; font-weight:bold;" align="right">$ 60.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center><a href="javascript: void(0);" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #fb9678; border-radius: 60px; text-decoration:none;">Pay with paypal</a>   </center>
                    <b>- Thanks (Elite team)</b> 
                </td>
            </tr>
        </tbody>
    </table>
</div>
