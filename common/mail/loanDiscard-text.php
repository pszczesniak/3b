<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$veryfityLink = \Yii::$app->urlManager->createAbsoluteUrl(['apl/verification', 'id' => $loan->id]);
?>
<div class="password-reset">
    Twój wniosek o pożyczkę został odrzucony.
    <p style="color:red;">Powód odrzucenia: <b><?= $model->l_purpose ?></b></p>
</div>
