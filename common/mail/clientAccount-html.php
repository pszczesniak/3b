<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$resetLink = \Yii::$app->urlManager->createAbsoluteUrl(['client/login']);
$loginLink = \Yii::$app->urlManagerFrontEnd->createUrl('/strefa-klienta/login');
?>
<div class="password-reset">
    <h1>Konto klienta w systemie GTW</h1>
    <p>Witaj <?= $username ?></p>
    <p>Poniżej dane do Twojego konta - zapamiętaj je, ponieważ będą potrzebne przy każdorazowym logowaniu na portalu www.gtw-finanse.pl</p>
    <p>Aby się zalogować kliknij w poniższy link i skorzystaj z danych autoryzacyjnych: <br />login: <?= $login ?> / hasło: <?= $password ?></p>

    <p style="text-align:center;"><?= Html::a('Zaloguj się', $loginLink, ['class' => 'button']) ?></p>
</div>
