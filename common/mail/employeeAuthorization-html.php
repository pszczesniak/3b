<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = \Yii::$app->urlManagerFrontEnd->createUrl('login');
?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
<div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <div class="password-reset">
                        Witaj,

                        W systemie zostało utworzone konto powiązane z tym adresem e-mail. Aby się zalogować kliknij w poniższy link i skorzystaj z danych autoryzacyjnych: login: <?= $login ?> / hasło: <?= $password ?>


                        <p><a href="<?= $resetLink ?>" class="btnLink">Zaloguj się</a></p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

