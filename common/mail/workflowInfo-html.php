<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$worflowLink = \Yii::$app->urlManagerFrontEnd->createUrl('/document/workflow/'.$model->id);
?>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
        <tr>
            <td style="vertical-align: top; padding-bottom:30px;" align="center">
                <a href="#"><img src="<?= $message->embed($logoImage); ?>" alt="Lawfirm System" /> </a> 
            </td>
        </tr>
    </tbody>
</table>
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <td style="padding:20px; color:#fff; text-align:center;" class="bg<?= (($model->type_fk == 1) ? 'Teal' : 'Blue') ?>"> Prośba o <?= (($model->type_fk == 1) ? 'akceptację' : 'komentarz') ?> </td>
        </tr>
    </tbody>
</table>
<div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td><p><b><?= $model->name ?></b></p>
                    <p style="text-align: justify;"><?= $model->describe ?></p>
                    <center>
                        <a href="<?= $worflowLink ?>" class="btnLink">Obieg dokumentów</a>
                    </center>
                    <b>- <?= $model->creator['fullname'] ?> (wiadomość wygenerowana automatycznie przez system) </b> 
                </td>
            </tr>
        </tbody>
    </table>
</div>
