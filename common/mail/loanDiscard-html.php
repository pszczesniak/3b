<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$veryfityLink = \Yii::$app->urlManager->createAbsoluteUrl(['apl/verification', 'lid' => $loan->id]);
?>
<div class="password-reset">
    <h1>Twój wniosek #<?= $model->id ?> o pożyczkę został odrzucony.</h1>
    <p style="color:red;">Powód odrzucenia: <b><?= $model->l_purpose ?></b></p>
</div>
