<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = \Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    Witaj,

    W systemie zostało utworzone konto powiązane z tym adresem e-mail. Aby się zalogować kliknij w poniższy link i skorzystaj z danych autoryzacyjnych: login: <?= $login ?> / hasło: <?= $password ?>


    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
