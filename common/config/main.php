<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => '3b-artstudio',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager'=>[
			/*'class' => 'yii\rbac\PhpManager',
			'itemFile' => '@app/rbac/items.php', //Default path to items.php | NEW CONFIGURATIONS
			'assignmentFile' => '@app/rbac/assignments.php', //Default path to assignments.php | NEW CONFIGURATIONS
			'ruleFile' => '@app/rbac/rules.php', //Default path to rules.php | NEW CONFIGURATIONS*/
			'class'=>'yii\rbac\DbManager', // Database driven Yii-Auth Manager
            //'defaultRoles' => ['guest', 'user'],
            'cache' => 'yii\caching\FileCache'
           
		],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing' => true
        ],
        'formatter' => [
           'dateFormat' => 'php:Y-m-d',
           'datetimeFormat' => 'php:Y-m-d H:i:s',
           'timeFormat' => 'H:i:s',

           'locale' => 'pl-PL', //your language locale
           'defaultTimeZone' => 'UTC',
           'timeZone' => 'Europe/Warsaw', // time zone
      ],
    ],
];
