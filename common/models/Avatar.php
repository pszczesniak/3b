<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use yii\imagine\Image;

class Avatar extends Model {
    
	const SCENARIO_SAVE = 'save';
    const SCENARIO_UPLOAD = 'upload';
    const SCENARIO_CHANGE = 'change';
	const SCENARIO_UPDATE = 'update';
    const SCENARIO_AVATAR = 'avatar';
    const SCENARIO_YOUTUBE = 'youtube';
	
	public $files;
    public $avatar;
	
	/**
     * @scenarios
     */
	public function scenarios()
    {
        return [
            self::SCENARIO_SAVE => ['id_fk', 'id_type_file_fk', 'title_file', 'name_file', 'systemname_file', 'extension_file', 'size_file'],
            self::SCENARIO_UPLOAD => ['id_fk', 'id_type_file_fk', 'title_file', 'files'],
			self::SCENARIO_UPDATE => ['version_fk'],
            self::SCENARIO_AVATAR => ['avatar'],
            self::SCENARIO_YOUTUBE => ['id_fk', 'id_type_file_fk', 'title_file'],
            self::SCENARIO_CHANGE => ['title_file', 'describe_file', 'id_dict_type_file_fk'],
			'default' => array('id'),
        ];
    }

   
   	public function uploadAvatar($name)  {

        if( $this->avatar->saveAs('uploads/users/avatars/' . $name.'.jpg')) {
            Image::thumbnail('@webroot/uploads/users/avatars/' . $name . '.jpg', 500, 360)->save(Yii::getAlias('@webroot/uploads/users/avatars/' . $name . '-big.jpg'), ['quality' => 80]);
           /* $imagine = new Imagine();    
            $imagine->open('@webroot/uploads/avatars/' . $name . '.jpg')->resize(new Box(500, 360))->save(Yii::getAlias('@webroot/uploads/avatars/' . $name . '-big.jpg'), ['quality' => 80]);*/
            /*$dstSize = $img->getSize();
            $dstSize = $dstSize->widen(500);
            $dstSize = $dstSize->heighten(360);
            return $img->copy()->resize($dstSize);*/
            Image::thumbnail('@webroot/uploads/users/avatars/' . $name . '.jpg', 200, 260)
                    ->save(Yii::getAlias('@webroot/uploads/users/avatars/' . $name . '-small.jpg'), ['quality' => 80]);
                    
            return '/uploads/avatars/' . $name.'.jpg';
        } else {
            return false;
        }
    }
    
    public static function getAvatar($id) {
        $avatarUrl = false;
        if(is_file(Yii::getAlias('@webroot') . "/uploads/users/thumb/avatar-".$id.".png")) 
            $avatarUrl = "/uploads/users/thumb/avatar-".$id.".png";
        
        if(!$avatarUrl && is_file(Yii::getAlias('@webroot') . "/uploads/users/avatars/avatar-".$id."-big.jpg"))
            $avatarUrl = "/uploads/users/avatars/avatar-".$id."-big.jpg";
            
        if(!$avatarUrl)
            $avatarUrl = "/images/default-user.png";
            
        return $avatarUrl;
    }
}
