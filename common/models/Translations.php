<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%translations}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $fk_id
 * @property string $lang
 * @property string $attr_data
 * @property string $attr_slug
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
 /*
    type_id: 1 - cms page
    type_id: 2 - cms category
    type_id: 3 - shop product
    type_id: 4 - shop category
    type_id: 5 - cms widget box
    type_id: 6 - cms widget accordion
    type_id: 7 - cms menu item
    
 */
class Translations extends \yii\db\ActiveRecord
{
    public $attributes = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%translations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'fk_id', 'created_by', 'updated_by'], 'integer'],
            [['attr_data'], 'string'],
            [['lang'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['lang', 'attr_slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'lang' => Yii::t('app', 'Lang'),
            'attr_data' => Yii::t('app', 'Attr Data'),
            'attr_slug' => Yii::t('app', 'Attr Slug'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
				$this->updated_at = new Expression('NOW()');               
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->fk_status_id = 4;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    static public function firstLang() {
        $first = 'pl';
        
        foreach(\Yii::$app->params['languages'] as $key => $lang) {
            $first = $key;
            break;
        }
        
        return $first;
    }
}
