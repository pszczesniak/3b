<?php
    namespace common\models;
   
    use Yii;
    use yii\base\Model;
    use common\models\User;
   /* $2y$13$Dv4TXDAOf8xJlhlPtb9cfuVLdlTS/zY8G0wim3rYUaDy5myvW8H0i */
    class PasswordForm extends Model{
        public $oldpass;
        public $newpass;
        public $repeatnewpass;
       
        public function rules(){
            return [
                [['oldpass','newpass','repeatnewpass'],'required'],
                ['oldpass','findPasswords'],
                ['repeatnewpass','compare','compareAttribute'=>'newpass'],
            ];
        }
       
        public function findPasswords($attribute, $params){
            $user = User::findOne(Yii::$app->user->identity->id);
           // $password = $user->password_hash;
            if(!$user->validatePassword($this->oldpass)/*$password!=$this->oldpass*/)
                $this->addError($attribute,  Yii::t('app', 'Old password is incorrect'));
        }
       
        public function attributeLabels(){
            return [
                'oldpass'=> Yii::t('app', 'Old Password'),
                'newpass'=> Yii::t('app', 'New Password'),
                'repeatnewpass'=> Yii::t('app', 'Repeat New Password'),
            ];
        }
    } 