<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%logs}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $table_parent_id
 * @property integer $table_arch_id
 * @property string $table_name
 * @property string $action_name
 * @property string $action_date
 * @property string $action_describe
 * @property string $request_remote_addr
 * @property string $request_user_agent
 * @property string $request_content_type
 * @property string $request_url
 */
class Logs extends \yii\db\ActiveRecord
{
    public $date_from;
    public $date_to;
    public $sessionCount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%logs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'table_parent_id', 'table_arch_id'], 'integer'],
            [['request_user_agent', 'request_content_type', 'request_url'], 'required'],
            [['table_name'], 'string', 'max' => 100],
            [['action_name'], 'string', 'max' => 30],
            [['action_date','date_from', 'date_to'], 'string', 'max' => 300],
            [['action_describe'], 'string', 'max' => 2000],
            [['request_remote_addr', 'request_user_agent', 'request_content_type', 'request_url'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'table_parent_id' => Yii::t('app', 'Table Parent ID'),
            'table_arch_id' => Yii::t('app', 'Table Arch ID'),
            'table_name' => Yii::t('app', 'Table Name'),
            'action_name' => Yii::t('app', 'Action Name'),
            'action_date' => Yii::t('app', 'Action Date'),
            'action_describe' => Yii::t('app', 'Action Describe'),
            'request_remote_addr' => Yii::t('app', 'Request Remote Addr'),
            'request_user_agent' => Yii::t('app', 'Request User Agent'),
            'request_content_type' => Yii::t('app', 'Request Content Type'),
            'request_url' => Yii::t('app', 'Request Url'),
        ];
    }
    
    public function getUser() {
		$user = \common\models\User::findOne($this->id_user_fk); 
        return $user;
	}
	
	public static function dictStyle() {
        $dict = [
            'login' => ['icon' => 'lock-open', 'color' => 'green', 'name' => 'Logowanie do systemu'],
            'logout' => ['icon' => 'lock', 'color' => 'orange', 'name' => 'Wylogowanie z systemu'],
            'folder_create' => ['icon' => 'folder', 'color' => 'teal', 'name' => 'Utworzenie folderu'],
            'folder_move' => ['icon' => 'arrows-alt', 'color' => 'purple', 'Przeniesienie folderu'],
            'folder_delete' => ['icon' => 'trash', 'color' => 'red'],
            'file_upload' => ['icon' => 'upload', 'color' => 'teal'],
            'file_download' => ['icon' => 'download', 'color' => 'blue'],
            'file_delete' => ['icon' => 'trash', 'color' => 'red'],

        ];
        
        return $dict;
    }
    
    public static function getHistory($id) {
        return self::find()->where(['id_user_fk' => $id])->orderby('id desc')->all();
    }
}
