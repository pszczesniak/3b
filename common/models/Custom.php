<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%custom}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $system_name
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
 
 /*
    type_fk = 1: zgody przy rejestracji konta
    type_fk = 2: teksty w ramkach przy rejestracji konta
    type_fk = 3: teksty w ramkach przy złożeniu wniosku [ostatni krok rejestracji]
    type_fk = 4: zgody przy wysłaniu wniosku
    type_fk = 5: panel informacyjny w procesie rejestracji
 */

class Custom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%custom}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk'], 'required'],
            [['type_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'rank'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['system_name', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Typ'),
            'system_name' => Yii::t('app', 'Nazwa'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'rank' => 'Kolejność'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {			
			if($this->isNewRecord ) {
                $this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
			
			return false;
		}
		return false;
	}

	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
