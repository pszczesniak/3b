<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use yii\imagine\Image;

/**
 * This is the model class for table "{{%_files}}".
 *
 * @property integer $id
 * @property integer $id_type_file_fk: 1-customer; 2-loan; 3-message; 4-do pobrania
 * @property integer $id_fk
 * @property integer $version_fk
 * @property integer $type_fk
 * @property string $title_file
 * @property string $name_file
 * @property resource $content_file
 * @property string $extension_file
 * @property string $size_file
 * @property string $describe_file
 * @property string $mod_reason
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 *
 * type_fk = 1: gallery
 */

class Files extends \yii\db\ActiveRecord
{
    
	const SCENARIO_SAVE = 'save';
    const SCENARIO_UPLOAD = 'upload';
    const SCENARIO_CHANGE = 'change';
	const SCENARIO_UPDATE = 'update';
    const SCENARIO_AVATAR = 'avatar';
    const SCENARIO_YOUTUBE = 'youtube';
    
    public $user_action;
	
	public $files;
    public $files_ids = [];
    public $files_list;
    public $files_list_static;
    public $files_list_new;
    public $files_list_new_static;
    
    public $date_from;
    public $date_to;
    public $id_customer_fk;
    
    public $avatar;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }
	
	/**
     * @scenarios
     */
	public function scenarios()
    {
        return [
            self::SCENARIO_SAVE => ['id_fk', 'id_type_file_fk', 'title_file', 'name_file', 'systemname_file', 'extension_file', 'size_file', 'type_fk', 'id_dict_type_file_fk'],
            self::SCENARIO_UPLOAD => ['id_fk', 'id_type_file_fk', 'title_file', 'files', 'type_fk', 'show_client', 'id_dict_type_file_fk'],
			self::SCENARIO_UPDATE => ['version_fk', 'id_dict_type_file_fk'],
            self::SCENARIO_CHANGE => ['title_file', 'describe_file', 'id_dict_type_file_fk', 'show_client'],
            self::SCENARIO_AVATAR => ['avatar'],
			self::SCENARIO_YOUTUBE => ['id_fk', 'id_type_file_fk', 'title_file'],
			'default' => array('id'),
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_type_file_fk', 'id_fk', 'version_fk', 'type_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_dict_type_file_fk', 'show_client'], 'integer'],
            [['content_file'], 'string'],
            [['size_file'], 'number'],
            [['created_at', 'updated_at', 'deleted_at', 'describe_file', 'type_fk', 'user_action', 'id_dict_type_file_fk'], 'safe'],
            [['title_file', 'systemname_file'], 'string', 'max' => 2000],
            [['name_file'], 'string', 'max' => 1000],
            [['extension_file', 'mime_file'], 'string', 'max' => 45],
            [['mod_reason'], 'string', 'max' => 255],
			[['files', 'title_file'], 'required'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()  {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_type_file_fk' => Yii::t('app', 'Typ'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'version_fk' => Yii::t('app', 'Version Fk'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'title_file' => Yii::t('app', 'Tytuł'),
            'name_file' => Yii::t('app', 'Name File'),
            'content_file' => Yii::t('app', 'Content File'),
            'extension_file' => Yii::t('app', 'Extension File'),
            'size_file' => Yii::t('app', 'Size File'),
            'describe_file' => Yii::t('app', 'Opis'),
            'mod_reason' => Yii::t('app', 'Mod Reason'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_customer_fk' => 'Klient',
            'show_client' => 'Pokaż klientowi',
            'id_dict_type_file_fk' => 'Typ'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
				/*$modelArch = new appArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function upload()
    {
        if ($this->validate()) { 
			        
            foreach ($this->files as $file) {
                $modelFile = new Files(['scenario' => self::SCENARIO_SAVE]);
				$modelFile->extension_file = $file->extension;
				$modelFile->size_file = $file->size;
				$modelFile->mime_file = $file->type;
				$modelFile->name_file = $file->baseName;
				$modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
				$modelFile->title_file = (count($this->files) == 1) ? $this->title_file : $file->baseName.'.'.$file->extension;
				$modelFile->id_fk = $this->id_fk;
				$modelFile->id_type_file_fk = $this->id_type_file_fk;
                if($this->id_dict_type_file_fk && $this->id_dict_type_file_fk != 'undefined')
                    $modelFile->id_dict_type_file_fk = $this->id_dict_type_file_fk;
                else
                    $modelFile->id_dict_type_file_fk = 0;
                $modelFile->type_fk = $this->type_fk;
                $modelFile->show_client = $this->show_client;
                
				if($modelFile->save()) {
					/*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
					try {
					    $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
						//$this->files_list .= '<li><a href="/files/getfile/'.$modelFile->id.'">'.$modelFile->title_file .'</a> [nowy] </li>';
						array_push($this->files_ids,$modelFile->id);
						$this->files_list .= '<li id="file-'.$modelFile->id.'" class="document-normal">'
												.'<div class="desc">'
													.'<span class="attachment"><a class="file-download" href="/files/getfile/'.$modelFile->id.'">'.$modelFile->title_file .'</a></span>'
													//.'<span class="attachment"><a class="file-download" href="'.\Yii::$app->urlManager->createAbsoluteUrl(['/files/getfile','id' => $modelFile->id]).'">'.$modelFile->title_file .'</a></span>'
													.'<p>'.$modelFile->user.' <i class="fa fa-clock"></i>'.date('Y-m-d H:i:s').'</p>'
												.'</div>'
												.'<a class="file-delete" href="/files/delete/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-trash"></i></a>'
												.'<a class="file-update '.((Yii::$app->id == 'app-frontend') ? 'viewModal' : 'gridViewModal').'" data-target="#'.((Yii::$app->id == 'app-frontend') ? 'modal-view' : 'modal-grid-file').'" href="/files/update/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-pencil"></i></a>'
											.'</li>'; 
						$this->files_list_static .= '<li id="file-'.$modelFile->id.'" class="document-normal">'
												.'<div class="desc">'
													.'<span class="attachment">'.$modelFile->title_file .'</span>'
													//.'<span class="attachment"><a class="file-download" href="'.\Yii::$app->urlManager->createAbsoluteUrl(['/files/getfile','id' => $modelFile->id]).'">'.$modelFile->title_file .'</a></span>'
													.'<p>'.$modelFile->user.' <i class="fa fa-clock"></i>'.date('Y-m-d H:i:s').'</p>'
												.'</div>'
												.'<a class="file-delete" href="/files/delete/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-trash"></i></a>'
												.'<a class="file-update '.((Yii::$app->id == 'app-frontend') ? 'viewModal' : 'gridViewModal').'" data-target="#'.((Yii::$app->id == 'app-frontend') ? 'modal-view' : 'modal-grid-file').'" href="/files/update/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-pencil"></i></a>'
											.'</li>';
											
			  
						$this->files_list_new .= '<li id="file-'.$modelFile->id.'" class="document-normal">'
													.'<div class="attachment">'
														.'<div class="attachment-thumb">'
															.'<a href="#"><i class="fa fa-paperclip"></i></a>'
														.'</div>'
														.'<div class="attachment-info">'
															.'<div class="attachment-file-name">'
																 .'<a class="file-download" href="/files/getfile/'.$modelFile->id.'">'. $modelFile->title_file .'</a>'
															.'</div>'
															.'<div class="attachment-file-creator">'
																 .'<p>'.$modelFile->user.' <i class="fa fa-clock"></i>'.date('Y-m-d H:i:s').'</p>'
															.'</div>'
                                                            .'<div class="attachment-file-category">'
                                                                 .'<p> <i class="fa fa-tag"></i><span>'.( ($modelFile->category) ? $modelFile->category['name'] : 'nieprzypisany').'</span></p>'
                                                            .'</div>'
															.'<div class="attachment-action-bar">'
															   .'<span class="list-file-download"><a href="/files/getfile/'.$modelFile->id.'"><i class="fa fa-download"></i> Pobierz</a></span>'
															   .'<span class="list-file-edit"><a class="file-update '.((Yii::$app->id == 'app-frontend') ? 'viewModal' : 'gridViewModal').'" data-target="#'.((Yii::$app->id == 'app-frontend') ? 'modal-view' : 'modal-grid-file').'" href="/files/update/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-pencil"></i>Edytuj</a></span>'
															   .'<span class="list-file-del"><a class="file-delete" href="/files/delete/'.$modelFile->id.'" data-id="'.$modelFile->id.'"><i class="fa fa-trash"></i>Usuń</a></span>'
															.'</div>'
														.'</div>'
													.'</div>'
												.'</li>';
						$this->id = $modelFile->id;
					} catch (\Exception $e) {
						var_dump($e); exit;
					}
				} else {
					var_dump($modelFile->getErrors());
				}
				
				
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function uploadAvatar($name)  {

        if( $this->avatar->saveAs('uploads/avatars/' . $name.'.jpg')) {
            Image::thumbnail('@webroot/uploads/avatars/' . $name . '.jpg', 500, 360)->save(Yii::getAlias('@webroot/uploads/avatars/' . $name . '-big.jpg'), ['quality' => 80]);
           /* $imagine = new Imagine();    
            $imagine->open('@webroot/uploads/avatars/' . $name . '.jpg')->resize(new Box(500, 360))->save(Yii::getAlias('@webroot/uploads/avatars/' . $name . '-big.jpg'), ['quality' => 80]);*/
            /*$dstSize = $img->getSize();
            $dstSize = $dstSize->widen(500);
            $dstSize = $dstSize->heighten(360);
            return $img->copy()->resize($dstSize);*/
            Image::thumbnail('@webroot/uploads/avatars/' . $name . '.jpg', 200, 260)
                    ->save(Yii::getAlias('@webroot/uploads/avatars/' . $name . '-small.jpg'), ['quality' => 80]);
                    
            return '/uploads/avatars/' . $name.'.jpg';
        } else {
            return false;
        }
    }
	
	public function getUser() {
        return \common\models\User::findOne($this->created_by)->fullname;
    }
    
    public function getCategory() {
        //$category = \common\models\Structure::findOne($this->id_dict_type_file_fk);
        $category = \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_type_file_fk);
        return $category;
    }
}
