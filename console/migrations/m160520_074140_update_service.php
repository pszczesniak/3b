<?php

use yii\db\Schema;
use yii\db\Migration;

class m160520_074140_update_service extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_offer_changes', [
				'id' => 'pk',
				'id_offer_fk' => Schema::TYPE_INTEGER,
				'id_old_package_fk' => Schema::TYPE_INTEGER,
				'id_new_package_fk' => Schema::TYPE_INTEGER,
				'date_of_change' => Schema::TYPE_DATE,
                'expiration_date' => Schema::TYPE_DATE,
                'payment' => Schema::TYPE_DOUBLE,
				'is_changed' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'is_resigantion' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
                'is_payment' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_search', [
				'id' => Schema::TYPE_PK,
				'query' => Schema::TYPE_TEXT ,
                'offers' => Schema::TYPE_TEXT ,
                'order_by' => Schema::TYPE_STRING, 
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                'created_by' => Schema::TYPE_INTEGER ,
                'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER,
                'request_remote_addr' => Schema::TYPE_STRING . '(1000)',
                'request_user_agent' => Schema::TYPE_STRING . '(1000) NOT NULL',
                'request_content_type' => Schema::TYPE_STRING . '(1000) NOT NULL'
			], $tableOptions);
			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}
    }
    
    

    public function down()
    {
        echo "m160520_074140_update_service cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
