<?php

use yii\db\Migration;
use yii\db\Schema;


class m160509_154019_update_service extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_trade', [
				'id' => 'pk',
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
                'rank' => Schema::TYPE_INTEGER,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_education', [
				'id' => 'pk',
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
                'rank' => Schema::TYPE_INTEGER,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_client', [
				'id' => Schema::TYPE_PK,
				'id_user_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'id_country_fk' => Schema::TYPE_INTEGER ,
				'id_province_fk' => Schema::TYPE_INTEGER,
				'city' => Schema::TYPE_STRING,
				'postal_code' => Schema::TYPE_STRING,
				'address' => Schema::TYPE_STRING,
				'pos_lat' => Schema::TYPE_DOUBLE,
				'pos_lng' => Schema::TYPE_DOUBLE,
				'firstname' => Schema::TYPE_STRING,
				'lastname' => Schema::TYPE_STRING,
				'email' => Schema::TYPE_STRING,
				'phone' => Schema::TYPE_STRING,
				'phone_additional' => Schema::TYPE_TEXT,
				'website_url' => Schema::TYPE_STRING,
				'facebook_url' => Schema::TYPE_STRING,
                'facebook_config' => Schema::TYPE_TEXT,
                'avatar_img' => Schema::TYPE_STRING,
                'sex' => Schema::TYPE_STRING,
                'birthday' => Schema::TYPE_DATE,
                'profession' => Schema::TYPE_STRING,
                'id_trade_fk' => Schema::TYPE_INTEGER,
                'id_education_fk' => Schema::TYPE_INTEGER,
				
				'note' => Schema::TYPE_TEXT,
				'first_logged_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'last_logged_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'confirm' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_client_observed', [
				'id' => 'pk',
				'id_user_fk' => Schema::TYPE_INTEGER,
                'id_client_fk' => Schema::TYPE_INTEGER,
                'id_other_offer_fk' => Schema::TYPE_INTEGER,
				'id_offer_fk' => Schema::TYPE_INTEGER,
				'note' => Schema::TYPE_TEXT,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
            $transaction->commit();
            
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}

    }

    public function down()
    {
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
