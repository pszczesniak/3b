<?php

use yii\db\Schema;
use yii\db\Migration;

class m160315_074540_init_location extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'loc_country', [
				'id' => 'pk',
				'name' => Schema::TYPE_STRING,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'loc_province', [
				'id' => 'pk',
				'id_country_fk' => Schema::TYPE_INTEGER ,
				'name' => Schema::TYPE_STRING,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}
    }

    public function down()
    {
        echo "m160315_074540_init_location cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
