<?php

use yii\db\Schema;
use yii\db\Migration;

class m170705_173418_applicant extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            
            // table request
            $this->createTable(
                '{{%apl_request}}',
                [
                    'id' => Schema::TYPE_PK,
					'id_user_fk' => Schema::TYPE_INTEGER,
					'type_fk' => Schema::TYPE_INTEGER,
					'params' => Schema::TYPE_TEXT,
                    'v_code' => Schema::TYPE_STRING . '(15) ',

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
			
			// table customer 
            $this->createTable(
                '{{%apl_customer}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_user_fk' => Schema::TYPE_INTEGER ,
					'id_request_fk' => Schema::TYPE_INTEGER ,
                    'lastname' => Schema::TYPE_STRING . '(300) NOT NULL',
                    'firstname' => Schema::TYPE_STRING . '(300) NOT NULL',
					'pesel' => Schema::TYPE_STRING . '(20)',
					'evidence_number' => Schema::TYPE_STRING . '(20)',
					'address' => Schema::TYPE_TEXT,
                    'address_postalcode' => Schema::TYPE_STRING . '(7) NOT NULL',
                    'address_city' => Schema::TYPE_STRING . '(300) NOT NULL',
					'address_for_correspondence' => Schema::TYPE_TEXT,
					'phone' => Schema::TYPE_STRING . '(20)',
					'phone_mobile' => Schema::TYPE_STRING . '(20)',
					'email' => Schema::TYPE_STRING . '(100) NOT NULL',
					'id_marital_status_fk' => Schema::TYPE_INTEGER ,
					'number_of_members' => Schema::TYPE_INTEGER ,
					'own_car' => Schema::TYPE_SMALLINT ,
                    'info_car' => Schema::TYPE_TEXT ,
					'employment_status' => Schema::TYPE_INTEGER ,
					'employment_period' => Schema::TYPE_STRING . '(10)',
					'employer_name' => Schema::TYPE_STRING . '(1000)',
					'employer_custom' => Schema::TYPE_TEXT,
					
					'bank_name' => Schema::TYPE_STRING . '(1000)',
					'bank_account_number' => Schema::TYPE_STRING . '(1000)',
					'bank_how_long_use' => Schema::TYPE_INTEGER ,
					
					'frequency_payment' => Schema::TYPE_INTEGER ,
					'use_credit_card' => Schema::TYPE_SMALLINT ,
					'monthly_income' => Schema::TYPE_FLOAT,
					'monthly_expenses' => Schema::TYPE_FLOAT,
					'monthly_other_loans' => Schema::TYPE_FLOAT,
					
					'position' => Schema::TYPE_STRING ,
					'description' => Schema::TYPE_TEXT,
					'custom_data' => Schema::TYPE_TEXT,

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
			
			$this->createTable(
                '{{%apl_customer_arch}}',
                [
                    'id' => Schema::TYPE_PK,
					'table_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_root_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
					'user_action' => Schema::TYPE_STRING . '(100) ',
					'data_arch' => Schema::TYPE_TEXT,
                    'data_change' => Schema::TYPE_TEXT,

                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                ],
                $tableOptions
            );
			
			$this->createTable(
                '{{%apl_loan}}',
                [
                    'id' => Schema::TYPE_PK,
					'id_request_fk' => Schema::TYPE_INTEGER ,
					'id_customer_fk' => Schema::TYPE_INTEGER ,
                    'l_amount' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_amount_total' => Schema::TYPE_FLOAT,
                    'l_period' => Schema::TYPE_INTEGER . ' NOT NULL',
					'l_monthly_rate' => Schema::TYPE_FLOAT . ' NOT NULL',
					'l_amount_total' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_provision' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_provision_custom' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_provision_amount' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_interest' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_interest_custom' => Schema::TYPE_FLOAT . ' NOT NULL',
                    'l_interest_amount' => Schema::TYPE_FLOAT . ' NOT NULL',
					'l_rrso' => Schema::TYPE_FLOAT . ' NOT NULL',
					'id_purpose_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
					'l_purpose' => Schema::TYPE_TEXT,
                    'l_day_of_payment' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'repayment_term' => Schema::TYPE_DATE,
                    'shift_months' => Schema::TYPE_DATE,
					'shift_term' => Schema::TYPE_DATE,
					
					'custom_data' => Schema::TYPE_TEXT,

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
			
			// table customer info
            $this->createTable(
                '{{%apl_info}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_parent_fk' => Schema::TYPE_INTEGER ,
                    'type_fk' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'id_customer_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
					'id_loan_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'info_content' => Schema::TYPE_TEXT,
                    'data_arch' => Schema::TYPE_TEXT,

                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}	
    }

    public function down()
    {
        echo "m170705_173418_applicant cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
