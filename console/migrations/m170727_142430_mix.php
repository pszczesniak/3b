<?php

use yii\db\Schema;
use yii\db\Migration;

class m170727_142430_mix extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            $this->createTable(Yii::$app->getDb()->tablePrefix.'mix_comment', [
				'id' => Schema::TYPE_PK,
                'type_fk' =>  Schema::TYPE_INTEGER,
                'id_comment_fk' =>  Schema::TYPE_INTEGER,
                'id_root_fk' => Schema::TYPE_INTEGER,
                'id_fk' => Schema::TYPE_INTEGER,
                'id_client_fk' => Schema::TYPE_INTEGER,
                'id_user_fk' => Schema::TYPE_INTEGER,
                'is_owner' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'name' => Schema::TYPE_STRING . ' NOT NULL',
                'user_info' => Schema::TYPE_STRING . '(255)',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
				'comment' => Schema::TYPE_TEXT,
                'comment_arch' => Schema::TYPE_TEXT,
				'rank' => Schema::TYPE_DOUBLE,
                'click_ok' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                'click_no' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER,
                'deleted_comment' => Schema::TYPE_TEXT,
			], $tableOptions);
                        
            $this->createTable(Yii::$app->getDb()->tablePrefix.'mix_message', [
				'id' => Schema::TYPE_PK,
                'type_fk' =>  Schema::TYPE_INTEGER,
                'id_message_fk' => Schema::TYPE_INTEGER,
                'id_root_fk' => Schema::TYPE_INTEGER,
                'id_fk' => Schema::TYPE_INTEGER,
                'id_client_fk' => Schema::TYPE_INTEGER,
                'id_query_fk' => Schema::TYPE_INTEGER,
                'type' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'message' => Schema::TYPE_TEXT,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_offer_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_offer_by' => Schema::TYPE_INTEGER,
                'status_offer' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'deleted_client_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_slient_by' => Schema::TYPE_INTEGER,
                'status_client' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'cron_status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0'
			], $tableOptions);
            
            // table company arch
            $this->createTable(Yii::$app->getDb()->tablePrefix.'mix_arch', [
                    'id' => Schema::TYPE_PK,
					'table_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_root_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
					'user_action' => Schema::TYPE_STRING . '(100) ',
					'data_arch' => Schema::TYPE_TEXT,

                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER 
                ],
                $tableOptions );
			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}
    }

    public function safeDown()
    {
        echo "m170727_142430_comments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170727_142430_comments cannot be reverted.\n";

        return false;
    }
    */
}
