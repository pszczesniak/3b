<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_105625_newsletter extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            
            // table newsletter_group
            $this->createTable(
                '{{%newsletter_group}}',
                [
                    'id' => Schema::TYPE_PK,
                    'name' => Schema::TYPE_STRING . '(255) NOT NULL',
                    'describe' => Schema::TYPE_TEXT . ' NOT NULL',

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
            
            // table newsletter_template
            $this->createTable(
                '{{%newsletter_template}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_group_fk' => Schema::TYPE_INTEGER ,
                    'title' => Schema::TYPE_STRING . '(255) NOT NULL',
                    'content' => Schema::TYPE_TEXT . ' NOT NULL',
                    'template' => Schema::TYPE_STRING,
                    'arch' => Schema::TYPE_TEXT,
                    'ids' => Schema::TYPE_TEXT,
                    'is_sending' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'date_sending_start' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'date_sending_end' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            // table newsletter_subscribes
            $this->createTable(
                '{{%newsletter_subscribe}}',
                [
                    'id' => Schema::TYPE_PK,
                    'email' => Schema::TYPE_STRING . ' NOT NULL',
                    'name' => Schema::TYPE_STRING . ' NOT NULL',
					
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            $this->createTable(
                '{{%newsletter_group_subscribe}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_group_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_subscribe_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'email' => Schema::TYPE_STRING . ' NOT NULL',
                    'email' => Schema::TYPE_STRING . ' NOT NULL',
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            $this->createTable(
                '{{%newsletter_group_template}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_group_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_template_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            // table newsletter_send
            $this->createTable(
                '{{%newsletter_send}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_template_fk' => Schema::TYPE_STRING . '(255) NOT NULL',                  
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'send_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'send_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            $this->createTable(
                '{{%newsletter_send_subscribe}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_send_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_subscribe_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'send_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'send_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            ); 
            
            $this->createTable('newsletter_event', [
                'id' => Schema::TYPE_PK,
                'event' => Schema::TYPE_STRING . ' NOT NULL',
            ]);
            
            $this->createTable('newsletter_event_template', [
                'id' => Schema::TYPE_PK,
                'id_event_fk' => Schema::TYPE_BIGINT . ' NOT NULL',
                'id_template_fk' => Schema::TYPE_BIGINT . ' NOT NULL',
            ]);
            
            $transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}	
    }

    public function down()
    {
        echo "m160426_105625_newsletter cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
