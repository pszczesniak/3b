<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_105608_blog extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            
            // table blog_catalog
            $this->createTable(
                '{{%blog_catalog}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_parent_fk' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                    'title' => Schema::TYPE_STRING . '(255) NOT NULL',
                    'surname' => Schema::TYPE_STRING . '(128) NOT NULL',
                    'brief' => Schema::TYPE_TEXT,
                    'content' => Schema::TYPE_TEXT ,
                    'banner' => Schema::TYPE_STRING . '(255) ',
                    'is_nav' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
                    'sort_order' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 50',
                    'page_size' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 10',
                    'template' => Schema::TYPE_STRING . '(255) NOT NULL DEFAULT "post"',
                    'redirect_url' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );

            // Indexes
            $this->createIndex('is_nav', '{{%blog_catalog}}', 'is_nav');
            $this->createIndex('sort_order', '{{%blog_catalog}}', 'sort_order');
            $this->createIndex('status', '{{%blog_catalog}}', 'status');
            $this->createIndex('created_at', '{{%blog_catalog}}', 'created_at');


            // table blog_post
            $this->createTable(
                '{{%blog_post}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_catalog_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'title' => Schema::TYPE_STRING . '(255) NOT NULL',
                    'surname' => Schema::TYPE_STRING . '(128) NOT NULL',
                    'brief' => Schema::TYPE_TEXT,
                    'content' => Schema::TYPE_TEXT . ' NOT NULL',
                    'tags' => Schema::TYPE_STRING . '(255)',
                    'banner' => Schema::TYPE_STRING . '(255) ',
                    'click' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                    
                    'author_name' => Schema::TYPE_STRING . '(255) NOT NULL',
                    'author_email' => Schema::TYPE_STRING . '(255) NOT NULL',
                    
                    'id_gallery_fk' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER,
                    'deleted_comment' => Schema::TYPE_STRING 
                ],
                $tableOptions
            );

            // Indexes
            $this->createIndex('id_catalog_fk', '{{%blog_post}}', 'id_catalog_fk');
            $this->createIndex('status', '{{%blog_post}}', 'status');
            $this->createIndex('created_at', '{{%blog_post}}', 'created_at');

            // Foreign Keys
            $this->addForeignKey('FK_post_catalog', '{{%blog_post}}', 'id_catalog_fk', '{{%blog_catalog}}', 'id', 'CASCADE', 'CASCADE');
           // $this->addForeignKey('FK_post_user', '{{%blog_post}}', 'created_at', '{{%user}}', 'id', 'CASCADE', 'CASCADE');


            // table blog_comment
            $this->createTable(
                '{{%blog_comment}}',
                [
                    'id' => Schema::TYPE_PK,
                    'id_parent_fk' => Schema::TYPE_INTEGER,
                    'discussion_label' => Schema::TYPE_INTEGER,
                    'id_post_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'content' => Schema::TYPE_TEXT . ' NOT NULL',
                    'email' => Schema::TYPE_STRING . '(128) NOT NULL',
                    'author' => Schema::TYPE_STRING ,
                    'id_author_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'url' => Schema::TYPE_STRING . '(128) NULL',
                    'rank' => Schema::TYPE_DOUBLE,
                    'click_ok' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                    'click_no' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER,
                    'deleted_comment' => Schema::TYPE_STRING 
                ],
                $tableOptions
            );

            // Indexes
            $this->createIndex('id_post_fk', '{{%blog_comment}}', 'id_post_fk');
            $this->createIndex('status', '{{%blog_comment}}', 'status');
            $this->createIndex('created_at', '{{%blog_comment}}', 'created_at');

            // Foreign Keys
            $this->addForeignKey('FK_comment_post', '{{%blog_comment}}', 'id_post_fk', '{{%blog_post}}', 'id', 'CASCADE', 'CASCADE');


            // table blog_tag
            $this->createTable(
                '{{%blog_tag}}',
                [
                    'id' => Schema::TYPE_PK,
                    'name' => Schema::TYPE_STRING . '(128) NOT NULL',
                    'frequency' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
                    'custom_options' => Schema::TYPE_TEXT,
                    
                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );

            // Indexes
            $this->createIndex('frequency', '{{%blog_tag}}', 'frequency');
            
            $transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}	
    }

    public function down()
    {
        echo "m160426_105608_blog cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
