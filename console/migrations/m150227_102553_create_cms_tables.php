<?php

use yii\db\Schema;
use yii\db\Migration;

class m150227_102553_create_cms_tables extends Migration
{
    public function up()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_site', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "create"',
            'native_lang' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "pl"',
            'phone' =>  Schema::TYPE_STRING,
            'email' =>  Schema::TYPE_STRING,
            'address' =>  Schema::TYPE_STRING,
            'nip' =>  Schema::TYPE_STRING,
            'regon' =>  Schema::TYPE_STRING,
            'custom_data' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'          
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_site_part', [
            'id' => 'pk',
            'fk_site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'rank' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'fk_home_page_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "create"',
            'title_langs' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "pl"',
            'part_styles' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'          
        ], $tableOptions);
               
       
        /*
         *  Table structure for table prexif_`cms_category`
         */

		$this->createTable(Yii::$app->getDb()->tablePrefix.'cms_category', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'template_view' => Schema::TYPE_STRING . ' NOT NULL DEFAULT ""',
            'list_view' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "0"',
            'title_slug' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_slug' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'short_content' => Schema::TYPE_TEXT,
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING,
            'meta_description' => Schema::TYPE_TEXT,
            'hits' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'details_config' => Schema::TYPE_TEXT,
            'is_calendar' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'editable' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_category_lang', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'lang' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "en"',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_slug' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'short_content' => Schema::TYPE_TEXT,
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING,
            'meta_description' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER . ' NOT NULL'           
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_category_arch', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'user_action' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "create"',
            'version_data' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'          
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_status', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_lang' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_TEXT,
            'describe_lang' => Schema::TYPE_TEXT      
        ], $tableOptions);
        
        /*
         *  Table structure for table prexif_`cms_page`
         */
		$this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page', [
            'id' => 'pk',
            'template_view' => Schema::TYPE_STRING . ' NOT NULL DEFAULT ""',
            'fk_status_id' => Schema::TYPE_INTEGER,
            'fk_part_id' => Schema::TYPE_INTEGER,
            'fk_category_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_slug' => Schema::TYPE_STRING,
            'content' => Schema::TYPE_TEXT,
            'short_content' => Schema::TYPE_TEXT,
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING,
            'meta_description' => Schema::TYPE_TEXT,
            'hits' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'is_active' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'editable' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'featured' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'event_date' => Schema::TYPE_DATE,
            'is_print' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'details_config' => Schema::TYPE_TEXT,
            
            'status' => Schema::TYPE_SMALLINT . ' DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_lang', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'lang' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "en"',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_slug' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'short_content' => Schema::TYPE_TEXT,
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING,
            'meta_description' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER . ' NOT NULL'           
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_arch', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'user_action' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "create"',
            'version_data' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'          
        ], $tableOptions);
        
        /*
         *  Table structure for table prexif_`cms_page`
         */
		$this->createTable(Yii::$app->getDb()->tablePrefix.'cms_gallery', [
            'id' => 'pk',
            'type_fk' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'versions_data' => Schema::TYPE_TEXT,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_slug' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'short_content' => Schema::TYPE_TEXT,
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING,
            'meta_description' => Schema::TYPE_TEXT,
            'hits' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'view_type' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER ,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_gallery_photo', [
            'id' => 'pk',
            'fk_gallery_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING ,
            'description' => Schema::TYPE_TEXT,
			'type' => Schema::TYPE_STRING,
			'owner_id' => Schema::TYPE_STRING,
            'rank' => Schema::TYPE_INTEGER,
            'file_name' => Schema::TYPE_STRING,
            'file_ext' => Schema::TYPE_STRING,
            'file_type' => Schema::TYPE_STRING,
            'is_short' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'is_link' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'link_type' => Schema::TYPE_INTEGER,
            'link_fk_id' => Schema::TYPE_INTEGER,
            'link_href' => Schema::TYPE_STRING,
            
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'click' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER
        ], $tableOptions);
        
        /* COMMENT type '0-page,1-category' */
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_gallery', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_INTEGER,
            'fk_element_id' => Schema::TYPE_INTEGER,
            'fk_gallery_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'gallery_mode' => Schema::TYPE_INTEGER,
            'gallery_label' => Schema::TYPE_STRING,
            'gallery_label_langs' => Schema::TYPE_TEXT,
            'rank' => Schema::TYPE_INTEGER,
            'rank_url' => Schema::TYPE_STRING,      
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_menu_type', [
            'id' => 'pk',
            'type_element' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_lang' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_TEXT,
            'describe_lang' => Schema::TYPE_TEXT      
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_menu', [
            'id' => 'pk',
            'fk_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_langs' => Schema::TYPE_TEXT,
            'rank' => Schema::TYPE_INTEGER,
            'show_title' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',    
            'show_all_pages' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',  
            'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',  
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_menu_item', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'fk_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_menu_id' =>  Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_cms_element_id' =>  Schema::TYPE_INTEGER . ' NOT NULL',
            'link_href' => Schema::TYPE_STRING,  
            'menu_level' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_langs' => Schema::TYPE_TEXT,
            'rank' => Schema::TYPE_INTEGER,
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_menu', [
            'id' => 'pk',
            'fk_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_cms_element_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_menu_id' => Schema::TYPE_INTEGER . ' NOT NULL',            
            'rank' => Schema::TYPE_INTEGER,
            'rank_url' => Schema::TYPE_STRING, 
			'menu_label' => Schema::TYPE_STRING,
            'menu_label_langs' => Schema::TYPE_TEXT,      
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        /* widgets */
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget', [
            'id' => 'pk',
            'type_widget' => Schema::TYPE_INTEGER . ' NOT NULL',
            'symbol_widget' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_langs' => Schema::TYPE_TEXT,
			'fk_element_id' => Schema::TYPE_INTEGER,
            'is_deleted' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',   
			'is_system' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',      
			'init_command' => Schema::TYPE_TEXT,
            'details_config' => Schema::TYPE_TEXT
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_arch', [
            'id' => 'pk',
            'fk_widget_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_cms_widget_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_action' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "create"',
            'version_data' => Schema::TYPE_TEXT,
                  
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'                 
        ], $tableOptions);
        
        /* widget_position: '1-before;2-after' */
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_page_widget', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'fk_widget_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'fk_cms_widget_id' => Schema::TYPE_INTEGER ,
            'fk_cms_element_id' => Schema::TYPE_INTEGER ,
            'widget_position' => Schema::TYPE_INTEGER ,
            'rank' => Schema::TYPE_INTEGER,
            'rank_url' => Schema::TYPE_STRING,     
			'widget_label' => Schema::TYPE_STRING,
            'widget_label_langs' => Schema::TYPE_TEXT, 
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP ,
            'created_by' => Schema::TYPE_INTEGER ,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_accordion', [
            'id' => 'pk',
            'fk_cms_widget_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_langs' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_TEXT,
            'describe_langs' => Schema::TYPE_TEXT,
			'show_title' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',    
            'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',  
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_accordion_item', [
            'id' => 'pk',
            'fk_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_langs' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_TEXT,
            'describe_langs' => Schema::TYPE_TEXT,
            'rank' => Schema::TYPE_INTEGER,
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_box', [
            'id' => 'pk',
			'fk_cms_widget_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . '(1000) NOT NULL',
            'name_langs' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_STRING ,
            'describe_langs' => Schema::TYPE_TEXT,
            'show_title' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',    
            'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0', 
            'is_link' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',  
            'link_type' => Schema::TYPE_INTEGER,
            'link_href' => Schema::TYPE_STRING,
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);  
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_files_set', [
            'id' => 'pk',
			'fk_cms_widget_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'name_langs' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_TEXT,
            'describe_langs' => Schema::TYPE_TEXT,
            'showTitle' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',    
            'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0', 
            'is_player' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',  
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);  
        
        $this->createTable(Yii::$app->getDb()->tablePrefix.'cms_widget_files_set_item', [
            'id' => 'pk',
            'fk_set_id' => Schema::TYPE_INTEGER,
            'fk_type_file_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'title_langs' => Schema::TYPE_TEXT,
            'describe' => Schema::TYPE_STRING . ' NOT NULL',
            'describe_langs' => Schema::TYPE_TEXT,
            'file_name' => Schema::TYPE_STRING . ' NOT NULL',
            'file_extension' => Schema::TYPE_STRING . ' NOT NULL', 
            'file_sie' => Schema::TYPE_DECIMAL,
            'mv_type' => Schema::TYPE_INTEGER,
            'html_options' => Schema::TYPE_TEXT,  
                  
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);
          
    }
    public function down()
    {
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_site');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_site_part');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_category');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_category_lang');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_category_arch');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_status');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_lang');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_arch');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_gallery');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_menu');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_page_widget');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_gallery');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_gallery_photo');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_menu_type');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_menu');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_menu_item');
        
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_arch');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_accordion');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_accordion_item');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_box');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_files_set');
        $this->dropTable(Yii::$app->getDb()->tablePrefix.'cms_widget_files_set_item');
    }
}
