<?php

use yii\db\Migration;
use yii\db\Schema;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //$this->createTable('{{%user}}', [
        $this->createTable(Yii::$app->getDb()->tablePrefix.'user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'firstname' => $this->string(255),
            'lastname' => $this->string(255),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        /*
         *  Table structure for table prexif_`translations`
         */
         
        $this->createTable(Yii::$app->getDb()->tablePrefix.'translations', [
            'id' => 'pk',
            'type_id' => Schema::TYPE_INTEGER,
            'fk_id' => Schema::TYPE_INTEGER,
            'lang' => Schema::TYPE_STRING . ' NOT NULL',
            'attr_data' => Schema::TYPE_TEXT,
            'attr_slug' => Schema::TYPE_STRING,
            
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        /*
         *  Table structure for table prexif_`lang`
         */
         
        $this->createTable(Yii::$app->getDb()->tablePrefix.'lang', [
            'id' => 'pk',
            'lang' => Schema::TYPE_STRING . ' NOT NULL',
            'lang_name' => Schema::TYPE_STRING . ' NOT NULL',
            'is_main' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'is_active' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
         $this->createTable('{{%files}}', [
            
            'id' => Schema::TYPE_PK,
            'id_type_file_fk' => Schema::TYPE_INTEGER ,
            'id_fk' => Schema::TYPE_INTEGER ,
            'id_dict_type_file_fk' => Schema::TYPE_INTEGER ,
            'version_fk' => Schema::TYPE_STRING . '(2000)',
            'type_fk' => Schema::TYPE_INTEGER ,
            'title_file' => Schema::TYPE_STRING . '(2000)',
            'name_file' => Schema::TYPE_STRING . '(2000)',
            'systemname_file' => Schema::TYPE_STRING . '(300)',
            'content_file' => Schema::TYPE_BINARY,
            'extension_file' => Schema::TYPE_STRING . '(300)',
            'mime_file' => Schema::TYPE_STRING . '(300)',
            'size_file' => Schema::TYPE_DECIMAL,
            'describe_file' => Schema::TYPE_TEXT,
            'mod_reason' => Schema::TYPE_STRING . '(300)',
            
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP,
            'created_by' => Schema::TYPE_INTEGER ,
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER,
            'show_client' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0'

        ], $tableOptions);
               
        $this->createTable(Yii::$app->getDb()->tablePrefix.'custom', [
            'id' => 'pk',
            'type_fk' => Schema::TYPE_INTEGER,
            'system_name' => Schema::TYPE_STRING . ' NOT NULL', 
            'title' => Schema::TYPE_STRING . ' NOT NULL', 
            'content' => Schema::TYPE_TEXT. ' NOT NULL',   
            'rank' => Schema::TYPE_INTEGER. ' NOT NULL DEFAULT 1',
              
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'updated_by' => Schema::TYPE_INTEGER,
            'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
            'deleted_by' => Schema::TYPE_INTEGER         
        ], $tableOptions);       
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
