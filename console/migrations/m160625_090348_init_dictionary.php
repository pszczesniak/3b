<?php

use yii\db\Schema;
use yii\db\Migration;

class m160625_090348_init_dictionary extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
            
            // table dictionary
            $this->createTable(
                '{{%dictionary}}',
                [
                    'id' => Schema::TYPE_PK,
                    'name' => Schema::TYPE_STRING . '(300) NOT NULL',
                    'name_langs' => Schema::TYPE_TEXT,
					'describe' => Schema::TYPE_TEXT,
                    'describe_langs' => Schema::TYPE_TEXT,
					'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
					'custom_data' => Schema::TYPE_TEXT,

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP,
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );
			
			// table dictionary value
            $this->createTable(
                '{{%dictionary_value}}',
                [
                    'id' => Schema::TYPE_PK,
					'id_dictionary_fk' => Schema::TYPE_INTEGER,
                    'name' => Schema::TYPE_STRING . '(300) NOT NULL',
                    'name_langs' => Schema::TYPE_TEXT,
					'describe' => Schema::TYPE_TEXT,
                    'describe_langs' => Schema::TYPE_TEXT,
					'is_system' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
					'in_use' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
					'custom_data' => Schema::TYPE_TEXT,
					'data_arch' => Schema::TYPE_TEXT,

                    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                    'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'updated_by' => Schema::TYPE_INTEGER,
                    'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'deleted_by' => Schema::TYPE_INTEGER
                ],
                $tableOptions
            );			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}	
    }

    public function down()
    {
        echo "m160625_090348_init_dictionary cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
