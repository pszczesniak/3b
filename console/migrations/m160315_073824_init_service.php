<?php

use yii\db\Schema;
use yii\db\Migration;

class m160315_073824_init_service extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
		
		$transaction = \Yii::$app->db->beginTransaction();
		try {
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_event', [
				'id' => 'pk',
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_category', [
				'id' => 'pk',
				'id_parent_fk' => Schema::TYPE_INTEGER. ' NOT NULL DEFAULT 0',
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'name_lang' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_service', [
				'id' => 'pk',
				'id_user_fk' => Schema::TYPE_INTEGER . "(11) NOT NULL",
				'id_category_fk' => Schema::TYPE_INTEGER,
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'price' => Schema::TYPE_DOUBLE,
                'price_lang' => Schema::TYPE_TEXT,
				'is_sale' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'price_sale' => Schema::TYPE_DOUBLE,
                'price_sale_lang' => Schema::TYPE_TEXT,
				
				'is_new' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'is_recommended' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				
				'id_country_fk' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
				'id_province_fk' => Schema::TYPE_INTEGER,
				'city' => Schema::TYPE_STRING,
				'postal_code' => Schema::TYPE_STRING,
				'address' => Schema::TYPE_STRING,
				
				'params' => Schema::TYPE_TEXT,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
				'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_service_category', [
				'id' => Schema::TYPE_PK,
				'id_category_fk' => Schema::TYPE_INTEGER . "(11) NOT NULL",
				'id_service_fk' => Schema::TYPE_INTEGER . "(11) NOT NULL",
				'ordering' => Schema::TYPE_INTEGER . "(11) NULL",
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
				'created_by' => Schema::TYPE_INTEGER . ' NOT NULL',
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_factor', [
				'id' => 'pk',
				'id_category_fk' =>  Schema::TYPE_INTEGER,
				'name' => Schema::TYPE_STRING,
				'name_lang' => Schema::TYPE_TEXT,
				'symbol' => Schema::TYPE_STRING,
				'symbol_lang' => Schema::TYPE_TEXT,
				'describe' => Schema::TYPE_TEXT,
				'describe_lang' => Schema::TYPE_TEXT,
				'config' => Schema::TYPE_STRING,
				'is_require' => Schema::TYPE_SMALLINT,
				'value_type' => Schema::TYPE_INTEGER, // 1-input, 2-select, 3-checkbox, 4-radio
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_offer', [
				'id' => Schema::TYPE_PK,
				'id_user_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'id_package_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'id_country_fk' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
                'province_additional' => Schema::TYPE_STRING,
				'id_province_fk' => Schema::TYPE_INTEGER,
				'city' => Schema::TYPE_STRING,
				'postal_code' => Schema::TYPE_STRING,
				'address' => Schema::TYPE_STRING,
				'pos_lat' => Schema::TYPE_DOUBLE,
				'pos_lng' => Schema::TYPE_DOUBLE,
				'companyname' => Schema::TYPE_STRING,
				'firstname' => Schema::TYPE_STRING,
				'lastname' => Schema::TYPE_STRING,
				'email' => Schema::TYPE_STRING,
				'phone' => Schema::TYPE_STRING,
				'phone_additional' => Schema::TYPE_TEXT,
				'website_url' => Schema::TYPE_STRING,
				'facebook_url' => Schema::TYPE_STRING,
                'avatar_img' => Schema::TYPE_STRING,
                'price_from' => Schema::TYPE_DOUBLE,
                'price_to' => Schema::TYPE_DOUBLE,
                'price_per_person' => Schema::TYPE_SMALLINT,
                'guests_from' => Schema::TYPE_INTEGER,
                'guests_to' => Schema::TYPE_INTEGER,
				'facebook_config' => Schema::TYPE_TEXT,
				'note' => Schema::TYPE_TEXT,
                'brief' => Schema::TYPE_TEXT,
				'first_logged_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'last_logged_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'confirm' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'confirm_payment' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                'date_payment' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
                'is_promotion' => Schema::TYPE_SMALLINT,
                'value_promotion' => Schema::TYPE_DOUBLE,
                'text_promotion' => Schema::TYPE_STRING,
                'stats_click' => Schema::TYPE_INTEGER ,
                'stats_click_www' => Schema::TYPE_INTEGER ,
                'stats_click_fb' => Schema::TYPE_INTEGER ,
                
                'calendar_busy_days' => Schema::TYPE_TEXT,
                
                'expiration_date' => Schema::TYPE_DATE,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_offer_event', [
				'id' => Schema::TYPE_PK,
				'id_offer_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'id_event_fk' => Schema::TYPE_INTEGER . ' NOT NULL',

				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_offer_category', [
				'id' => Schema::TYPE_PK,
				'id_offer_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'id_category_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
				'level' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',

				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_package', [
				'id' => Schema::TYPE_PK,
				'name' => Schema::TYPE_STRING . ' NOT NULL',
				'note' => Schema::TYPE_TEXT,
				
				'price' => Schema::TYPE_DOUBLE,
				'price_label' => Schema::TYPE_STRING,
                'price_lang' => Schema::TYPE_TEXT,
				'is_sale' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'price_sale' => Schema::TYPE_DOUBLE,
				'price_sale_label' => Schema::TYPE_STRING,
                'price_sale_lang' => Schema::TYPE_TEXT,
				
				'warning' => Schema::TYPE_TEXT,
				'options' => Schema::TYPE_TEXT,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
			
			$this->createTable(Yii::$app->getDb()->tablePrefix.'svc_package_option', [
				'id' => Schema::TYPE_PK,
				'id_package_fk' => Schema::TYPE_INTEGER,
				'name' => Schema::TYPE_STRING . ' NOT NULL',
				'note' => Schema::TYPE_TEXT,
				
				'price' => Schema::TYPE_DOUBLE,
                'price_lang' => Schema::TYPE_TEXT,
				'is_sale' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'price_sale' => Schema::TYPE_DOUBLE,
                'price_sale_lang' => Schema::TYPE_TEXT,
				
				'rank' => Schema::TYPE_INTEGER ,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_comment', [
				'id' => Schema::TYPE_PK,
                'id_comment_fk' =>  Schema::TYPE_INTEGER,
                'id_root_fk' => Schema::TYPE_INTEGER,
                'id_offer_fk' => Schema::TYPE_INTEGER,
                'id_client_fk' => Schema::TYPE_INTEGER,
                'id_user_fk' => Schema::TYPE_INTEGER,
                'is_owner' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				'name' => Schema::TYPE_STRING . ' NOT NULL',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
				'comment' => Schema::TYPE_TEXT,
                'comment_arch' => Schema::TYPE_TEXT,
				'rank' => Schema::TYPE_DOUBLE,
                'click_ok' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
                'click_no' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_by' => Schema::TYPE_INTEGER,
                'deleted_comment' => Schema::TYPE_TEXT,
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_query', [
				'id' => Schema::TYPE_PK,
				'query' => Schema::TYPE_TEXT .' NOT NULL',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'message' => Schema::TYPE_TEXT,
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                'created_by' => Schema::TYPE_INTEGER ,
                'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
                'request_remote_addr' => Schema::TYPE_STRING . '(1000)',
                'request_user_agent' => Schema::TYPE_STRING . '(1000) NOT NULL',
                'request_content_type' => Schema::TYPE_STRING . '(1000) NOT NULL'
			], $tableOptions);
            
            $this->createTable(Yii::$app->getDb()->tablePrefix.'svc_message', [
				'id' => Schema::TYPE_PK,
                'id_message_fk' => Schema::TYPE_INTEGER,
                'id_root_fk' => Schema::TYPE_INTEGER,
                'id_offer_fk' => Schema::TYPE_INTEGER,
                'id_client_fk' => Schema::TYPE_INTEGER,
                'id_query_fk' => Schema::TYPE_INTEGER,
                'type' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "in"',
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'message' => Schema::TYPE_TEXT,
				
				'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
				'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'created_by' => Schema::TYPE_INTEGER ,
				'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'updated_by' => Schema::TYPE_INTEGER,
				'deleted_offer_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_offer_by' => Schema::TYPE_INTEGER,
                'status_offer' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'deleted_client_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
				'deleted_slient_by' => Schema::TYPE_INTEGER,
                'status_client' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
                'cron_status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
			], $tableOptions);
            
            // table company arch
            $this->createTable(
                '{{%svc_arch}}',
                [
                    'id' => Schema::TYPE_PK,
					'table_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
                    'id_root_fk' => Schema::TYPE_INTEGER . ' NOT NULL',
					'user_action' => Schema::TYPE_STRING . '(100) ',
					'data_arch' => Schema::TYPE_TEXT,

                    'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL',
                    'created_by' => Schema::TYPE_INTEGER ,
                ],
                $tableOptions
            );
			
			$transaction->commit();
			echo 'OK'; 
		} catch (Exception $e) {echo $e;
			$transaction->rollBack();
		}
    }

    public function down()
    {
        echo "m160315_073824_init_service cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
