<?php
namespace backend\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            //throw new InvalidParamException('Password reset token cannot be blank.');
			Yii::$app->session->setFlash('error', 'Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            //throw new InvalidParamException('Wrong password reset token.');
			Yii::$app->session->setFlash('error', 'Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            //['password', 'string', 'min' => 10],
			['password','strengthPassword'],
        ];
    }
	
	public function strengthPassword($attribute, $params) {
		if (strlen($this->password) < 10) {
			$this->addError($attribute, Yii::t('app', 'Hasło musi mieć przynajmniej 10 znaków!'));
		}

		if (!preg_match("#[0-9]+#", $this->password)) {
			$this->addError($attribute, Yii::t('app', 'Hasło musi zawierać przynajmniej jedną cyfrę!'));				
		}
		
		if (!preg_match("#[a-z]+#", $this->password)) {
			$this->addError($attribute, Yii::t('app', 'Hasło musi zawierać przynajmniej jedną małą literę!'));				
		}
		
		if (!preg_match("#[A-Z]+#", $this->password)) {
			$this->addError($attribute, Yii::t('app', 'Hasło musi zawierać przynajmniej jedną dużą literę!'));				
		}  
		
		if (!preg_match("#\W+#", $this->password)) {
			$this->addError($attribute, Yii::t('app', 'Hasło musi zawierać przynajmniej jeden znak specjalny!'));				
		}  
	}

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->password_reset_time = date('Y-m-d H:i:s');
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
