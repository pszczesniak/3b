<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SiteStepsForm extends Model
{
    public $describe;
    public $index;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['describe'], 'required'],
            // email has to be a valid email address
           // ['email', 'email'],
			['index', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'describe' => 'Opis kroku',
        ];
    }

    
}
