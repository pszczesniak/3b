<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SiteContactForm extends Model
{
    public $name;
    public $email;
    public $position;
    public $phone;
    public $index;
    public $translate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'position', 'translate'], 'required'],
            // email has to be a valid email address
           // ['email', 'email'],
            [['email', 'phone'], 'string', 'max' => 255],
			['index', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Podpis',
			'position' => 'Stanowisko',
			'phone' => 'Telefon',
            'translate' => 'Stanowisko [tłumaczenie]'
        ];
    }

    
}
