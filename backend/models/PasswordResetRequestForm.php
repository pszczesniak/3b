<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        $user->scenario = User::SCENARIO_PASSWORD_RESET;

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) { 
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
                /*$from = \Yii::$app->params['supportEmail'];
                $to = 'kamila_bajdowska@onet.eu';
                $subject = 'My Subject';
                $body = 'My Body';

                $mailer = Yii::$app->mailer;
                $return = $mailer->compose()
                        ->setFrom($from)
                        ->setTo($to)
                        ->setHtmlBody($body)
                        ->setSubject($subject)
                        ->send();

                $logger = $mailer->getLogger(); 
                var_dump($logger->dump()); exit; // could save it in a file, db, etc
                
                return false;*/
            } else {
                var_dump($user->getErrors()); exit;
            }
        }

        return false;
    }
}
