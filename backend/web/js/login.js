window.onload = function() {
    var form = document.getElementById("loginForm");
    form.onsubmit = submitted.bind(form);
}

function submitted(event) {
    event.preventDefault();
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://api.vdr.dev/auth/user/login", false);
    xhr.onreadystatechange=function()  {
        if (xhr.readyState==4 && xhr.status==200)  {
            var result = JSON.parse(xhr.responseText);
            setCookie('username', result.email, 30);           
        }
    }
    xhr.send(new FormData (document.getElementById("loginForm")));
    
   // window.location.replace("http://vdr.dev");
    //console.log(xhr.status);
    //console.log(xhr.statusText);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}