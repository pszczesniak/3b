<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddSupplier */

$this->title = Yii::t('lsdd', 'Create Lsdd Supplier');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-supplier-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
