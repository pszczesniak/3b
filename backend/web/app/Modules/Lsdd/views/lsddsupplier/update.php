<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddSupplier */

$this->title = Yii::t('lsdd', 'Update {modelClass}: ', [
    'modelClass' => 'Lsdd Supplier',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('lsdd', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-supplier-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>