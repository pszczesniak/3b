<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterialSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-material-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_zone_fk') ?>

    <?= $form->field($model, 'material_name') ?>

    <?= $form->field($model, 'material_symbol') ?>

    <?= $form->field($model, 'laboratory_index') ?>

    <?php // echo $form->field($model, 'id_material_action_fk') ?>

    <?php // echo $form->field($model, 'id_material_function_fk') ?>

    <?php // echo $form->field($model, 'material_note') ?>

    <?php // echo $form->field($model, 'currency_native') ?>

    <?php // echo $form->field($model, 'price_native') ?>

    <?php // echo $form->field($model, 'prices_converted') ?>

    <?php // echo $form->field($model, 'price_forecast_native') ?>

    <?php // echo $form->field($model, 'prices_forecast_converted') ?>

    <?php // echo $form->field($model, 'transport_cost') ?>

    <?php // echo $form->field($model, 'transport_forecast_cost') ?>

    <?php // echo $form->field($model, 'weight_main') ?>

    <?php // echo $form->field($model, 'weight_pvc') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
