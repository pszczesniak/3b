<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_zone_fk')->textInput() ?>

    <?= $form->field($model, 'material_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'material_symbol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laboratory_index')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_material_action_fk')->textInput() ?>

    <?= $form->field($model, 'id_material_function_fk')->textInput() ?>

    <?= $form->field($model, 'material_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'currency_native')->textInput() ?>

    <?= $form->field($model, 'price_native')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prices_converted')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price_forecast_native')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prices_forecast_converted')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'transport_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transport_forecast_cost')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'weight_main')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight_pvc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
