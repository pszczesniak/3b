<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMailAddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-mail-address-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_zone_fk') ?>

    <?= $form->field($model, 'address_name') ?>

    <?= $form->field($model, 'address_email') ?>

    <?= $form->field($model, 'add_date') ?>

    <?php // echo $form->field($model, 'add_user') ?>

    <?php // echo $form->field($model, 'is_accept') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
