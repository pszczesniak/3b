<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMailAddress */

$this->title = Yii::t('lsdd', 'Update {modelClass}: ', [
    'modelClass' => 'Lsdd Mail Address',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Mail Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('lsdd', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-mail-address-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>