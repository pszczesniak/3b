<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMailAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-mail-address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_zone_fk')->textInput() ?>

    <?= $form->field($model, 'address_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'add_date')->textInput() ?>

    <?= $form->field($model, 'add_user')->textInput() ?>

    <?= $form->field($model, 'is_accept')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
