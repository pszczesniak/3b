<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddRecipeType */

$this->title = Yii::t('lsdd', 'Create Lsdd Recipe Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Recipe Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-recipe-type-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
