<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddExchangeRatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-exchange-rates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_currency_fk') ?>

    <?= $form->field($model, 'rate_date') ?>

    <?= $form->field($model, 'rate_value') ?>

    <?= $form->field($model, 'rate_converter') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'add_date') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
