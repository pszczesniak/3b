<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddExchangeRates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-exchange-rates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_currency_fk')->textInput() ?>

    <?= $form->field($model, 'rate_date')->textInput() ?>

    <?= $form->field($model, 'rate_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_converter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'add_date')->textInput() ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
