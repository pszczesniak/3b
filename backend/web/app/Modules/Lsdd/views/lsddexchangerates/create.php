<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddExchangeRates */

$this->title = Yii::t('lsdd', 'Create Lsdd Exchange Rates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Exchange Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-exchange-rates-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
