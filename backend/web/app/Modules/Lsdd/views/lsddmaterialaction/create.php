<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterialAction */

$this->title = Yii::t('lsdd', 'Create Lsdd Material Action');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Material Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-material-action-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
