<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterialFunction */

$this->title = Yii::t('lsdd', 'Create Lsdd Material Function');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Material Functions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-material-function-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
