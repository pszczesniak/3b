<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterialFunction */

$this->title = Yii::t('lsdd', 'Update {modelClass}: ', [
    'modelClass' => 'Lsdd Material Function',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Material Functions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('lsdd', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-material-function-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>