<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddProject */

$this->title = Yii::t('lsdd', 'Update {modelClass}: ', [
    'modelClass' => 'Lsdd Project',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('lsdd', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-project-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>