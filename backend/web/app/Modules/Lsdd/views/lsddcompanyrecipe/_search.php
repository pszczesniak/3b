<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyRecipeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-company-recipe-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_recipe_fk') ?>

    <?= $form->field($model, 'id_company_fk') ?>

    <?= $form->field($model, 'id_package_system_fk') ?>

    <?= $form->field($model, 'bind_name') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
