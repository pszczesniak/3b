<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyRecipe */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Company Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-company-recipe-view", 'title'=>Html::encode($this->title))) ?>

    <p>
        <?= Html::a(Yii::t('lsdd', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('lsdd', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('lsdd', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_recipe_fk',
            'id_company_fk',
            'id_package_system_fk',
            'bind_name',
        ],
    ]) ?>

<?= $this->endContent(); ?>