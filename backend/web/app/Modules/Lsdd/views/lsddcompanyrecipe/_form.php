<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyRecipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-company-recipe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_recipe_fk')->textInput() ?>

    <?= $form->field($model, 'id_company_fk')->textInput() ?>

    <?= $form->field($model, 'id_package_system_fk')->textInput() ?>

    <?= $form->field($model, 'bind_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
