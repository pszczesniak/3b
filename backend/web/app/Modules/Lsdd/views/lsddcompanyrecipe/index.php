<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Lsdd\models\LsddCompanyRecipeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('lsdd', 'Lsdd Company Recipes');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-company-recipe-index", 'title'=>Html::encode($this->title))) ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('lsdd', 'Create Lsdd Company Recipe'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'summary' => "<div class='summary'>". Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.').Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'Create Lsdd Company Recipe'), ['modelClass' => Yii::t('lsdd', 'Lsdd Company Recipe'), ]), ['create'], ['class' => 'btn btn-s btn-success btn-icon'])."</div>" ,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_recipe_fk',
            'id_company_fk',
            'id_package_system_fk',
            'bind_name',

            [
				'class' => 'yii\grid\ActionColumn',
				'contentOptions' => ['class' => 'table-actions'],
				'template' => '{view}{update}{delete}',
				'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					},
					'update' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-pencil"></i>', Url::to(['/cms/cmspage/update', 'id'=>$model->id]), [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					},
					'delete' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					}
				],
			],
        ],
    ]); ?>

<?= $this->endContent(); ?>
