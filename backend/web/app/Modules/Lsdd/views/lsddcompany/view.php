<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompany */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-company-view", 'title'=>Html::encode($this->title))) ?>

    <p>
        <?= Html::a(Yii::t('lsdd', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('lsdd', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('lsdd', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_zone_fk',
            'id_country_fk',
            'company_name',
            'company_note:ntext',
            'company_lang',
            'company_logo',
            'city_name',
            'postal_code',
            'address',
            'address_home_number',
            'address_apartment_number',
            'active',
            'email:email',
            'phone',
            'status',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_at',
            'deleted_by',
        ],
    ]) ?>

<?= $this->endContent(); ?>