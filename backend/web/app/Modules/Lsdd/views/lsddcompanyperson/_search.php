<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-company-person-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_company_fk') ?>

    <?= $form->field($model, 'person_firstname') ?>

    <?= $form->field($model, 'person_lastname') ?>

    <?= $form->field($model, 'person_email') ?>

    <?php // echo $form->field($model, 'person_office') ?>

    <?php // echo $form->field($model, 'person_phone_office') ?>

    <?php // echo $form->field($model, 'person_phone_mobile') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
