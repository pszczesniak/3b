<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-company-person-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_company_fk')->textInput() ?>

    <?= $form->field($model, 'person_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_office')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_phone_office')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_phone_mobile')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
