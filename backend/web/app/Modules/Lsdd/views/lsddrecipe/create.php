<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddRecipe */

$this->title = Yii::t('lsdd', 'Create Lsdd Recipe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-recipe-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>
