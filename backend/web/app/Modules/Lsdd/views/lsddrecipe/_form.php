<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddRecipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-recipe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_zone_fk')->textInput() ?>

    <?= $form->field($model, 'root_id')->textInput() ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'recipe_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipe_code_mark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipe_version_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipe_version_format')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_recipe_group_fk')->textInput() ?>

    <?= $form->field($model, 'id_recipe_type_fk')->textInput() ?>

    <?= $form->field($model, 'recipe_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_project_fk')->textInput() ?>

    <?= $form->field($model, 'is_color')->textInput() ?>

    <?= $form->field($model, 'is_accept')->textInput() ?>

    <?= $form->field($model, 'is_production')->textInput() ?>

    <?= $form->field($model, 'is_tmp')->textInput() ?>

    <?= $form->field($model, 'date_accept')->textInput() ?>

    <?= $form->field($model, 'user_accept')->textInput() ?>

    <?= $form->field($model, 'date_production')->textInput() ?>

    <?= $form->field($model, 'user_production')->textInput() ?>

    <?= $form->field($model, 'recipe_cw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'auto_rate_nbp')->textInput() ?>

    <?= $form->field($model, 'rate_nbp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('lsdd', 'Create') : Yii::t('lsdd', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
