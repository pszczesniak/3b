<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddRecipeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-recipe-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_zone_fk') ?>

    <?= $form->field($model, 'root_id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'recipe_name') ?>

    <?php // echo $form->field($model, 'recipe_code_mark') ?>

    <?php // echo $form->field($model, 'recipe_version_no') ?>

    <?php // echo $form->field($model, 'recipe_version_format') ?>

    <?php // echo $form->field($model, 'id_recipe_group_fk') ?>

    <?php // echo $form->field($model, 'id_recipe_type_fk') ?>

    <?php // echo $form->field($model, 'recipe_note') ?>

    <?php // echo $form->field($model, 'id_project_fk') ?>

    <?php // echo $form->field($model, 'is_color') ?>

    <?php // echo $form->field($model, 'is_accept') ?>

    <?php // echo $form->field($model, 'is_production') ?>

    <?php // echo $form->field($model, 'is_tmp') ?>

    <?php // echo $form->field($model, 'date_accept') ?>

    <?php // echo $form->field($model, 'user_accept') ?>

    <?php // echo $form->field($model, 'date_production') ?>

    <?php // echo $form->field($model, 'user_production') ?>

    <?php // echo $form->field($model, 'recipe_cw') ?>

    <?php // echo $form->field($model, 'auto_rate_nbp') ?>

    <?php // echo $form->field($model, 'rate_nbp') ?>

    <?php // echo $form->field($model, 'rate_user') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('lsdd', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('lsdd', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
