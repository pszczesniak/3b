<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddRecipe */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Recipes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-recipe-view", 'title'=>Html::encode($this->title))) ?>

    <p>
        <?= Html::a(Yii::t('lsdd', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('lsdd', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('lsdd', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_zone_fk',
            'root_id',
            'parent_id',
            'recipe_name',
            'recipe_code_mark',
            'recipe_version_no',
            'recipe_version_format',
            'id_recipe_group_fk',
            'id_recipe_type_fk',
            'recipe_note:ntext',
            'id_project_fk',
            'is_color',
            'is_accept',
            'is_production',
            'is_tmp',
            'date_accept',
            'user_accept',
            'date_production',
            'user_production',
            'recipe_cw',
            'auto_rate_nbp',
            'rate_nbp',
            'rate_user',
            'status',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_at',
            'deleted_by',
        ],
    ]) ?>

<?= $this->endContent(); ?>