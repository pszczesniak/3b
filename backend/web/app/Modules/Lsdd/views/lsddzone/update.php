<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddZone */

$this->title = Yii::t('lsdd', 'Update {modelClass}: ', [
    'modelClass' => 'Lsdd Zone',
]) . ' ' . $model->id_zone_pk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('lsdd', 'Lsdd Zones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_zone_pk, 'url' => ['view', 'id' => $model->id_zone_pk]];
$this->params['breadcrumbs'][] = Yii::t('lsdd', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-zone-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?=  $this->endContent(); ?>