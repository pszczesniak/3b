<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use api\modules\company\models\Company;

$this->title = 'Logowanie';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <figure class="logo">
        <img src="<?= '/images/logo.svg'/*Company::getLogo()*/ ?>" alt="logo" style="max-height:65px;">
    </figure>
    <h1 class="align-center"><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill out the following fields to login') ?>:</p>

    <?php $form = ActiveForm::begin([/*'id' => 'loginForm', 'method' => 'POST', 'action' => 'http://api.vdr.dev/auth/user/login',*/
                                     'enableAjaxValidation' => false, 'enableClientValidation' => false, 'enableClientScript' => false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'loginForm']]); ?>

        <?= $form->field($model, 'username')
                         //->hint('Proszę podać adres e-mail')
                         ->textInput(['autofocus' => true, 'minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'placeholder' => 'nazwa użytkownika'/*, 'name' => 'email'*/]) ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'minlength' => 3, 'maxlength' => 50, 'aria-required' => true, 'placeholder' => 'hasło'/*, 'name' => 'password'*/]) ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <div style="color:#999;margin:1em 0">
            <?= Yii::t('app', 'If you forgot your password you can') ?> <?= Html::a(Yii::t('app', 'reset it'), ['site/request-password-reset']) ?>.
        </div>

        <div class="form-group align-right">
            <?= Html::submitButton('Zaloguj się', ['class' => 'btn bg-teal', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
