<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Twój panel');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-index', 'title'=>Html::encode('Nowe zdarzenia'))) ?>
    <?= $this->render('workflow') ?>
<?php $this->endContent(); ?>
<div class="grid">
    <div class="col-sm-6 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-index', 'title'=>Html::encode('Ulubione'))) ?>
            <div class="ui-folders">
                <a href="javascript:void(0)" class="card-views">
                    <i class="fas fa-folder fa-3x text--yellow"></i>
                </a>
            </div>
        <?php $this->endContent(); ?>
    </div>
    <div class="col-sm-6 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-index', 'title'=>Html::encode('Ostatnio pobierane'))) ?>
            <ul class="list-download">
                <li><a download  href="/files/getfile/1" data-filesize="1.2" class="download-button">Nazwa pliku</a></li>
                <li><a download  href="/files/getfile/1" data-filesize="1.2" class="download-button">Nazwa pliku</a></li>
            </ul>
        <?php $this->endContent(); ?>
    </div>
</div>
