<?php
	use yii\helpers\Url;
    use yii\helpers\Html;
    
    use backend\widgets\Shortcutwidget;
    use backend\widgets\gtw\Panelstatswidget;
    use backend\widgets\Panelpercentwidget;
    use backend\widgets\Lastofferwidget;
    use backend\widgets\mix\LastComments;
    use backend\widgets\LastLoans;
    use common\widgets\Alert;
    
    $this->title = 'Witamy '.Yii::$app->user->identity->username;
    $this->params['breadcrumbs'][] = 'Info';
?>
<div class="cms-default-index  ">
    <?= Alert::widget() ?>
	<?php if (\Yii::$app->user->isGuest) { ?>
		<div class="text-center">
            <h1>Witamy w panelu admina</h1>
            <a href="<?= Url::to(['/site/login']) ?>" class="btn btn-lg btn-success">Zaloguj się do systemu</a>
        </div>
	<?php } else {  ?>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-config", 'title'=>Html::encode('Konfiguracja pożyczki'))) ?>
                    <?= $this->render('_form', array('model'=>$model)) ?>
                <?= $this->endContent(); ?>
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-config", 'title'=>Html::encode('Kroki pożyczki'))) ?>
                    <?= $this->render('_steps') ?>
                <?= $this->endContent(); ?>
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-custom-1", 'title'=>Html::encode('Panel informacyjny w procesie rejestracji'))) ?>
                    <?= $this->render('_custom', ['type' => 5]) ?>
                <?= $this->endContent(); ?>
            </div>
            <div class="col-sm-6 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-custom-1", 'title'=>Html::encode('Teksty w ramkach przy rejestracji konta'))) ?>
                    <?= $this->render('_custom', ['type' => 2]) ?>
                <?= $this->endContent(); ?>
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-custom-1", 'title'=>Html::encode('Zgody przy rejestracji konta'))) ?>
                    <?= $this->render('_custom', ['type' => 1]) ?>
                <?= $this->endContent(); ?>
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-custom-1", 'title'=>Html::encode('Teksty w ramkach przy złożeniu wniosku'))) ?>
                    <?= $this->render('_custom', ['type' => 3]) ?>
                <?= $this->endContent(); ?>
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"loan-custom-1", 'title'=>Html::encode('Zgody przy wysłaniu wniosku'))) ?>
                    <?= $this->render('_custom', ['type' => 4]) ?>
                <?= $this->endContent(); ?>
            </div>
        </div>
		
		<!--<div class="info-message"></div>
		<?php /*$this->render('_fields')*/ ?>-->
	<?php } ?>
</div>



