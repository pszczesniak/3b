<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\Modules\Company\models\Company;

$this->title = Yii::t('app', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <figure class="logo">
        <img src="<?= Company::getLogo() ?>" alt="logo" style="max-height:65px;">
    </figure>
    <h1 class="align-center"><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please choose your new password') ?>:</p>


    <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'options' => ['class'=> 'loginForm']]); ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

        <div class="form-group align-right">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
