<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Help');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php', array('class'=>'cal-task-index', 'title' => Html::encode('Help'))) ?>
    <ol>
        <li><b>Logowanie</b><br /><p>Proszę zalogować się na swoje konto. W systmie aktywne są konta, które zostały wskazane w strukturze firmy</p>    
            <ul style="list-style-type: circle !important;">
                <li style="list-style-type: circle !important;">e.zeligowska [e.zeligowska@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">e.debska [e.debska@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">k.wende [k.wende@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">s.banach [s.banach@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">k.kozlowski [k.kozlowski@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">a.pawlowski [a.pawlowski@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">s.wzietal [s.wzietal@optolith.pl]</li>
                <li style="list-style-type: circle !important;">m.szostak [m.szostak@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">k.rudnik [k.rudnik@hufgard.pl]</li>
                <li style="list-style-type: circle !important;">k.iwanek [k.iwanek@hufgard.pl]</li>
            </ul>
            <p><i>Jeśli kogoś z zainteresowanych nie ma na tej liście to proszę się zgłosić do mnie [kapisoft@onet.eu] lub Sebastiana Wziętala z taką informacją i konto zostanie utworzone. </i> </p>
            <p><i>Jeśli komuś z zainteresowanych ustawiono błędny adres e-mail to proszę się zgłosić do mnie [kapisoft@onet.eu] lub Sebastiana Wziętala z taką informacją i konto zostaną poprawione. </i> </p>
            <p><b class="text--purple">Aby się zalogować, proszę skorzystać z funkcji resetu hasła w oknie logowania</b></p>
        </li>
        <li><a href="/images/wprowadzone_rozwiazania.docx" title="Pobierz dokument"><b class="text--pink">Pobierz ten dokument</b></a>, aby zapoznać się z ostatecznymi założeniami i zasadami działania programu</li>
        <li>Po zapoznaniu się z zawartością dokumentu i skonfrontowaniu tego z aplikacją proszę o przesłanie wszelkich sugestii zmian. Aby wszystko przebiegło sprawnie prosze o zachowanie poniżych zasad:
            <ul>
                <li>- wszelkie sugestie mają być adnotacjami w załączonym pliku do konkretnych punktów</li>
                <li>- adnotacje mają być umieszczone bezpośrednio pod punktem, którego dotyczą w dowolnym kolorze [ale oczywiście innym niż czarny]</li>
                <li>- sugestie/uwagi prosze rozróżnić na takie, które są konieczne przed uruchomieniem systemu i takie, które nie są niezbędne na już ale dobrze by był gdybyzostały zrealizowane</li>
            </ul>
        </li>
        <li>Po otrzymaniu odpowiedzi na wyszczególnie pytania będę potrzebowała około 5 dni na dopracowanie całego systemu [głównie chodzi o wprowadzenie zmian związanych z doprecyzowaniem uprawnień dostępu do danych + wyeliminowanie uciążliwych bugów]</li>
    </ol>
<?php $this->endContent(); ?>
