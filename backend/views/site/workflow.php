<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use common\widgets\Alert;
  
?>

<!-- Panel heading -->
<div class="panel-heading">

	<!-- Panel title -->
	<div class="panel-title">Fast Actions</div>
	<!-- /Panel title -->

	<!-- Panel controls -->
	<div class="panel-controls">
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="">
				<a href="#fast-actions-pending" aria-controls="fast-actions-pending" role="tab" data-toggle="tab" aria-expanded="false">Pending</a>
			</li>
			<li role="presentation" class="active">
				<a href="#fast-actions-completed" aria-controls="fast-actions-completed" role="tab" data-toggle="tab" aria-expanded="true">Completed</a>
			</li>
		</ul>
	</div>
	<!-- Panel controls -->

</div>
<!-- /Panel heading -->

<!-- Panel body -->
<div class="panel-body">

	<div class="tab-content">

		<!-- TAB PANE Products table -->
		<div id="fast-actions-pending" role="tabpanel" class="tab-pane fade">

			<!-- TABLE: Pending -->
			<div class="table-responsive">
				<table class="table no-mb">

					<!-- Action Row -->
					<tbody><tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Benjamin Jacobs">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Benjamin Jacobs</div>
							<p class="no-mb">
								There are many variations of passages.
							</p>
						</td>
						<td class="text-center">
							09:03&nbsp;AM
							<div class="small text-muted">TUE,&nbsp;06/12/2017</div>
						</td>
						<td>
							<div class="btn-group btn-group-nowrap" role="group">
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-success">Approve</button>
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-danger">Reject</button>
							</div>
						</td>
					</tr>
					<!-- /Action Row -->

					<!-- Action Row -->
					<tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Ashley Warren">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Ashley Warren</div>
							<p class="no-mb">
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.
							</p>
						</td>
						<td class="text-center">
							09:45&nbsp;PM
							<div class="small text-muted">MON,&nbsp;05/12/2017</div>
						</td>
						<td>
							<div class="btn-group btn-group-nowrap" role="group">
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-success">Approve</button>
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-danger">Reject</button>
							</div>
						</td>
					</tr>
					<!-- /Action Row -->

					<!-- Action Row -->
					<tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Benjamin Jacobs">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Benjamin Jacobs</div>
							<p class="no-mb">
								Many desktop publishing packages and web page editors now use.
							</p>
						</td>
						<td class="text-center">
							07:24&nbsp;PM
							<div class="small text-muted">MON,&nbsp;05/12/2017</div>
						</td>
						<td>
							<div class="btn-group btn-group-nowrap" role="group">
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-success">Approve</button>
								<button type="button" class="btn btn-sm btn-o btn-rounded btn-danger">Reject</button>
							</div>
						</td>
					</tr>
					<!-- /Action Row -->

				</tbody></table>
			</div>
			<!-- /TABLE: Pending -->

		</div>
		<!-- /TAB PANE: Products table -->

		<!-- TAB PANE Products chart -->

		<div id="fast-actions-completed" role="tabpanel" class="tab-pane fade active in">

			<!-- TABLE: Completed -->
			<div class="table-responsive">
				<table class="table table-responsive no-mb">

					<!-- Action Row -->
					<tbody><tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Benjamin Jacobs">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Benjamin Jacobs</div>
							<p class="no-mb">
								There are many variations of passages.
							</p>
						</td>
						<td class="text-center">
							09:03&nbsp;AM
							<div class="small text-muted">TUE,&nbsp;06/12/2017</div>
						</td>
						<td class="col-minimal">
							<label class="badge badge-success"><i class="icon fa fa-check"></i> Approved</label>
						</td>
					</tr>
					<!-- /Action Row -->

					<!-- Action Row -->
					<tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Ashley Warren">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Ashley Warren</div>
							<p class="no-mb">
								Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.
							</p>
						</td>
						<td class="text-center">
							09:45&nbsp;PM
							<div class="small text-muted">MON,&nbsp;05/12/2017</div>
						</td>
						<td class="col-minimal">
							<label class="badge badge-danger"><i class="icon fa fa-times"></i> Rejected</label>
						</td>
					</tr>
					<!-- /Action Row -->

					<!-- Action Row -->
					<tr>
						<td class="col-minimal">
							<div class="avatar avatar-lg">
								<img src="/images/default-user.png" alt="Benjamin Jacobs">
							</div>
						</td>
						<td>
							<div class="strong text-dark">Benjamin Jacobs</div>
							<p class="no-mb">
								Many desktop publishing packages and web page editors now use.
							</p>
						</td>
						<td class="text-center">
							07:24&nbsp;PM
							<div class="small text-muted">MON,&nbsp;05/12/2017</div>
						</td>
						<td class="col-minimal">
							<label class="badge badge-success"><i class="icon fa fa-check"></i> Approved</label>
						</td>
					</tr>
					<!-- /Action Row -->

				</tbody></table>
			</div>
			<!-- /TABLE: Completed -->

		</div>
		<!-- /TAB PANE: Products chart -->

	</div>

</div>
<!-- Panel body -->
