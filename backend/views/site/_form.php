<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-site-form">

	<?php $form = ActiveForm::begin(['action' => Url::to(['/cms/default/save']), 'id' => 'page-basic', 'options' => ['class' => 'ajaxform'] ]); ?>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <fieldset><legend>Kwota pożyczki</legend>
                    <div class="row">
                        <div class="col-sm-6"><?= $form->field($model, 'amountFrom')->textInput(['maxlength' => 255])->label('od') ?></div>
                        <div class="col-sm-6"><?= $form->field($model, 'amountTo')->textInput(['maxlength' => 255])->label('do') ?></div>
                    </div>
                </fieldset>
                <fieldset><legend>Okres kredytowania</legend>
                    <div class="row">
                        <div class="col-sm-6"><?= $form->field($model, 'periodFrom')->textInput(['maxlength' => 255])->label('od') ?></div>
                        <div class="col-sm-6"><?= $form->field($model, 'periodTo')->textInput(['maxlength' => 255])->label('do') ?></div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-6 col-xs-12">
                <fieldset><legend>Prozwizja</legend>
                    <?= $form->field($model, 'loanProvision', ['template' => '{label} <div class="input-group">{input}<span class="input-group-addon"><span class="currencyName">%</span></span></div> {error}{hint}'])->textInput()->label('bieżąca stawka') ?>
                </fieldset>
                <fieldset><legend>Oprocentowanie</legend>
                    <?= $form->field($model, 'loanInterest', ['template' => '{label} <div class="input-group">{input}<span class="input-group-addon"><span class="currencyName">%</span></span></div> {error}{hint}'])->textInput()->label('bieżąca stawka') ?>
                </fieldset>
            </div>
            <div class="col-xs-12">
                <fieldset><legend>Wartości opcjonalne</legend>
                    <div class="row">
                        <div class="col-sm-6"><?= $form->field($model, 'defaultAmount')->textInput(['maxlength' => 255]) ?></div>
                        <div class="col-sm-6"><?= $form->field($model, 'defaultPeriod')->textInput(['maxlength' => 255]) ?></div>
                    </div>
                </fieldset>
                <fieldset><legend>Dodatkowy opis</legend>
                    <?= $form->field($model, 'loanNote')->textarea(['rows' => 6, 'placeholder' => 'Wprowadź dodatkowy opis ...'])->label(false) ?>
                </fieldset>
            </div>
        </div>
		<div class="form-group align-right">
			<?= Html::submitButton('Zapisz zmiany', ['class' => 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end(); ?>
   
</div>
