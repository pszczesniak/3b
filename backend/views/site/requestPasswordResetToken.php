<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use backend\Modules\Company\models\Company;

$this->title = Yii::t('app', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <?= Alert::widget() ?>
    <figure class="logo">
        <img src="<?= Company::getLogo() ?>" alt="logo" style="max-height:65px;">
    </figure>
    <h1 class="align-center"><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill out your email. A link to reset password will be sent there.') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class'=> 'loginForm']]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'podaj adres e-mail']) ?>

            <div class="form-group align-right">
                <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

</div>
