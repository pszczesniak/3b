<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-factor-form">

    <?php $form = ActiveForm::begin(['id' => 'item-form-field', 
									'action' => Url::to(['/cms/default/createajax', 'id' => $type]),
									'options' => [ 'data-pjax' => true, 'enableAjaxValidation'=>true, 'enableClientValidation'=>true, 'data-table' => '#table-items', 'data-target' => "#modal-grid-item"],
								 ]); ?>

		<?= $form->field($model, 'index')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'translate')->textInput(['maxlength' => true]) ?>
        <div class="row">
            <div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>          
        </div>


        <div class="form-group align-right">
            <?= Html::submitButton('Potwiedź zmiany', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
