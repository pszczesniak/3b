<?php

use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\Relateditemswidget;

?>

<div class="ajax-table-items">	
   
    <?= Relateditemswidget::widget(['createUrl' => Url::to(['/cms/default/createajax', 'id' => 1]),
                            'createLabel' => 'Dodaj kontakt',
                            'createIcon' => 'plus',
                            'createStyle' => 'success',
                            //'buttonId' => 'production-status',
                            'dataUrl' =>  Url::to(['/cms/default/fields']),
                            'tableId' => "table-items",
                            'modalId' => "modal-grid-item",
                            'gridViewModal' => 'gridViewModal',
                            'tableOptions' => 'data-toggle="table" data-show-refresh="true" data-show-toggle="true"  data-show-columns="true" data-show-export="true"  data-filter-control="true" ',
                            'otherButtons' => '<a class="btn btn-primary btn-icon none" id="save-table-items-move" href="'.Url::to(['/cms/default/save','id' => 0]).'"><i class="glyphicon glyphicon-floppy-disk"></i>Zapisz zmiany</a>',
                            'columns' => [
                                0 => ['name' => Yii::t('lsdd', 'ID'), 'data' => 'data-field="id" data-visible="false"' ],
                                1 => ['name' => 'Stanowisko', 'data' => 'data-field="position"' ],
								2 => ['name' => 'Podpis', 'data' => 'data-field="name"' ],
								3 => ['name' => 'E-mail', 'data' => 'data-field="email"' ],
								4 => ['name' => 'Telefon', 'data' => 'data-field="phone"' ],
                                5 => ['name' => '', 'data' => 'data-field="action" data-align="center "data-events="actionEvents"' ],
                                6 => ['name' => '', 'data' => 'data-field="move" data-align="center" data-events="actionEvents"' ],
                                7 => ['name' => '', 'data' => 'data-field="translate" data-visible="false"' ],
                            ]   
                    ]) ?>
  
</div>
