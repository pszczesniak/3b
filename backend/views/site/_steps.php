<?php

use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\Relateditemswidget;

?>

<div class="ajax-table-items">	
   
    <?= Relateditemswidget::widget(['createUrl' => Url::to(['/cms/default/createajax', 'id' => 1]),
                            'createLabel' => 'Dodaj krok',
                            'createIcon' => 'plus',
                            'createStyle' => 'success',
                            //'buttonId' => 'production-status',
                            'dataUrl' =>  Url::to(['/cms/default/steps']),
                            'tableId' => "table-steps",
                            'modalId' => "modal-grid-item",
                            'gridViewModal' => 'gridViewModal',
                            'tableOptions' => 'data-toggle="table" data-show-refresh="true" data-show-toggle="false"  data-show-columns="false" data-show-export="false"  data-filter-control="false" ',
                            'otherButtons' => '<a class="btn btn-primary btn-icon none" id="save-table-items-move" href="'.Url::to(['/cms/default/savesteps']).'" data-table="#table-steps"><i class="glyphicon glyphicon-floppy-disk"></i>Zapisz zmiany</a>',
                            'columns' => [
                                0 => ['name' => Yii::t('lsdd', 'ID'), 'data' => 'data-field="id" data-visible="false"' ],
                                1 => ['name' => 'Opis kroku', 'data' => 'data-field="describe"' ],
                                2 => ['name' => '', 'data' => 'data-field="action" data-align="center "data-events="actionEvents"' ],
                                3 => ['name' => '', 'data' => 'data-field="move" data-align="center" data-events="actionEvents"' ],
                            ]   
                    ]) ?>
  
</div>
