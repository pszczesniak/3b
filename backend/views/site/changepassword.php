<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php /*if( $days >= 30 ) { ?><div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i>&nbsp;Od ostatniej zmiany hasła minęło 30 dni. Proszę o wprowadzenie nowego hasła.</div> <?php } */ ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'change-password', 'title'=>Html::encode($this->title))) ?>
   <?= Alert::widget() ?>

    <div class="row">
        <div class="col-md-12 col-lg-8">
            <?php $form = ActiveForm::begin([
                    'id'=>'changepassword-form',
                    'options'=>['class'=>'form-horizontal'],
                    /*'fieldConfig'=>[
                        'template'=>"{label}\n<div class=\"col-lg-3\">
                                    {input}</div>\n<div class=\"col-lg-5\">
                                    {error}</div>",
                        'labelOptions'=>['class'=>'col-lg-2 control-label'],
                    ],*/
                ]); ?>
                <?= $form->field($model,'oldpass',['inputOptions'=>['placeholder' => Yii::t('app', 'Old Password')]])->passwordInput() ?>
               
                <?= $form->field($model,'newpass',['inputOptions'=>['placeholder' => Yii::t('app', 'New Password')]])->passwordInput() ?>
               
                <?= $form->field($model,'repeatnewpass',['inputOptions'=>[ 'placeholder' => Yii::t('app', 'Repeat New Password')]])->passwordInput() ?>
               
                <div class="form-group align-right">
                    <?= Html::submitButton(Yii::t('app', 'Change password'),[
                        'class'=>'btn more_btn2'
                    ]) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
   </div>
        
<?php $this->endContent(); ?>