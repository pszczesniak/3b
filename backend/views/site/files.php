<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */

$this->title = 'Zarządzanie plikami';
$this->params['breadcrumbs'][] = ['label' => 'Zarządzanie plikami'];
/*$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');*/
?>	

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-files', 'title'=>Html::encode($this->title))) ?>
	<div id="kc_div" style="height: 687px;">
		<iframe width="100%" height="100%" frameborder="0" style="padding: 0px; margin-top: 8px; border-radius: 4px; border: 1px solid rgb(107, 107, 107); box-shadow: 0px 0px 4px rgb(107, 107, 107); height: 687px;" 
				src="/filebrowser/browse.php?lng=pl&amp;theme=default" name="kcfinder_alone" id="kc_frame">
		</iframe>
	</div>
<?php $this->endContent(); ?>
