<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lsdd-company-file-form">
    <?php $form = ActiveForm::begin(['id' => 'file-form', 
									'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
									/*'action' => Url::to(['/lsdd/lsddcompanyperson/create']),*/ 
									'options' => [ 'data-pjax' => true, 'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#files-container','data-target' => "#modal-grid-file"],
								 ]); ?>
    <?= $form->field($model, 'show_client')->checkBox()  ?>
    <?= $form->field($model, 'title_file')->textInput(['maxlength' => false]) ?>
    <div class="form-group field-files-title_file required">
        <label class="control-label">Typ</label>
        <select name="Files[id_dict_type_file_fk]" class="form-control">
            <option value="0">- wybierz -</option>
            <?php
                foreach($types as $key => $item) {
                    echo '<option value="'.$item->id.'" '. ( ($model->id_dict_type_file_fk == $item->id) ? ' selected' : '' ).'>'.$item->name.'</option>';
                    /*if($item->children) {
                        echo Yii::$app->runAction('common/structure', ['parent' => $item, 'type' => 1, 'selected' => $model->id_dict_type_file_fk]);
                    } else {
                        echo '<option value="'.$item->id.'" '. ( ($model->id_dict_type_file_fk == $item->id) ? ' selected' : '' ).'>'.$item->item_name.'</option>';
                    }*/
                }
            ?>
        </select>
    </div>

    <?= $form->field($model, 'describe_file')->textArea(['row' => 2, 'maxlength' => false]) ?>

    <div class="form-group align-right">
        <?= Html::submitButton('Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data-form' => '#file-form']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
