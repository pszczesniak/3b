<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
/* @var $this yii\web\View */
use yii\authclient\widgets\AuthChoice;

$this->title = 'Media społecznościowe';
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'facebook-index-index', 'title'=>Html::encode($this->title))) ?>

               		<?php
                        $response = false;
                        $appId = '682422408630872';
                        $appSecret = '91a618f4fd16b6a08e511d54f33ff7b4';
                        $accessToken = \Yii::$app->session->get('fb.token');
                        
                        if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
                            $fb = new \Facebook\Facebook([
                                'app_id' => $appId,
                                'app_secret' => $appSecret,
                                'default_graph_version' => 'v2.5',
                                'default_access_token' => \Yii::$app->session->get('fb.token'),
                                'cookie' => true
                            ]);
                            $linkData = [
                              'link' => 'http://twojeprzyjecia.dev',
                              'message' => 'User provided message',
                              ];

                          /*  try {
                              // Returns a `Facebook\FacebookResponse` object
                              $response = $fb->post('/'.\Yii::$app->session->get('fb.id').'/feed', $linkData, \Yii::$app->session->get('fb.token'));
                            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                              echo 'Graph returned an error: ' . $e->getMessage();
                              exit;
                            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                              echo 'Facebook SDK returned an error: ' . $e->getMessage();
                              exit;
                            }

                            $graphNode = $response->getGraphNode();

                            echo 'Posted with id: ' . $graphNode['id'];*/
                            
                            try {
                                //$response = $fb->get('/me?fields=id,name,albums.limit(5){name, photos.limit(2){name, picture, tags.limit(2)}},posts.limit(5)');//var_dump($response); echo '<br />';
                                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                                $user = $response->getGraphUser();
                              //  echo 'Name: ' . $user->getName().'<br />';
                               // var_dump($user);
                                $res = $fb->get('/me/picture?type=large&redirect=false',  $accessToken);
                              //  echo '<br /><br />';
                                $picture = $res->getGraphObject();
 
                               // var_dump( $picture );
                                
                                echo '<div class="facebook">'
                                    .'<ul>'
                                        .'<li><img class="avatar" src="'.$picture['url'].'" alt="FB_IMG"></img></li>'
                                        .'<li>Witaj '.$user->getName().'</li>'
                                        .'<li><a href="'.$user['link'].'" target="_blank"">Twój fanpage</a></li>'
                                        //.'<li><a href="#">Find Friends</a></li>'
                                        /*.'<li id="noti_Container">'
                                            .'<div id="noti_Counter"></div>'
                                            .'<div id="noti_Button"></div>'    
                                            .'<div id="notifications">'
                                                .'<h3>Notifications</h3>'
                                                .'<div style="height:300px;"></div>'
                                                .'<div class="seeAll"><a href="#">See All</a></div>'
                                            .'</div>'
                                        .'</li>'*/
                                        
                                    .'</ul><div class="clear"></div>'
                                .'</div>';
             
                            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                                echo 'Graph returned an error: ' . $e->getMessage();
                            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                            }
                            echo '<div id="fb-content"><div class="alert">Twój profil został połączony z kontem na facebooku. Nie będziemy umieszczać, żadnych postów bez Twojej zgody.</div></div>';
                            echo '<div class="align-right"><a href="'.Url::to(['/facebook/fbdisconnet']).'" class="btn btn-primary btn-icon"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Rozłącz</a></div>';
                        
                        } else {
                        
                            /*echo yii\authclient\widgets\AuthChoice::widget([
                                 'baseAuthUrl' => ['offer/auth']
                            ]);*/
                            
                            $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['facebook/auth'], 'autoRender' => false, 'popupMode' => false]); 
                                echo '<ul>';
                                foreach ($authAuthChoice->getClients() as $client) { 
                                    echo '<li>'.Html::a( '<i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Połącz się z '. $client->title, ['facebook/auth', 'authclient'=> $client->name, ], ['class' => "btn btn-primary btn-icon $client->name "]).'</li>';
                                } 
                                echo '</ul>';
                            AuthChoice::end(); 
                        }

                    ?>   
             
<?php $this->endContent(); ?>



