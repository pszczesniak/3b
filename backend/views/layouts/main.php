<?php
/*http://webapplayers.com/inspinia_admin-v2.4/calendar.html#*/
/*http://wrapbootstrap.com/preview/WB09JXK43*/
/*http://wrapbootstrap.com/preview/WB0048JF7*/
/*https://genesisui.com/demo/clever/bootstrap4-static/index.html*/

use frontend\assets\AppAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use api\modules\company\models\Company;
use backend\widgets\ModuleMenuWidget;
/* @var $this \yii\web\View */
/* @var $content string */

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <style> .preload:after { content: ' '; position: fixed; top: 0; left: 0; z-index: 2000; display: block; width: 100%; height: 100%; opacity: 1; background: url(/images/loading.gif) no-repeat center center rgba(255, 255, 255, 0.95); transition: opacity 0.3s linear; } .preload--off:after { opacity: 0; } .preload * { transition: none !important; }    </style>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="preload">
    <div id="notification-container"></div>
	<div class="main-grid">
        <?= $this->render('_header') ?>

        <figure class="logo">
            <img src="<?= '/images/logo.svg' /*Company::getLogo()*/ ?>" alt="logo" style="max-height:65px; border: none;">
        </figure>

        <?= ModuleMenuWidget::widget(['module' => 'basic' ]) ?>

        <aside class="l-aside l-aside--hide js-aside-nav">
            <?= '' /*$this->render('_right_aside')*/ ?>
        </aside>

         <main class="l-main js-main">
            <?= \yii\widgets\Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'homeLink' => [
                                    'label' => '<i class="fas fa-home"></i>'/* . Yii::t('yii', 'Home')*/,
                                    'url' => Yii::$app->homeUrl,
                                    'encode' => false// Requested feature
                                ],
                           ]); ?>
            <?= $content ?>
         </main>
    </div>
    
    <div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-item" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-pencil"></i>Edytuj</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
    
    <div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-ajax" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>Nowy użytkownik</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
    
    <div tabindex="-1" role="dialog" class="fade modal out" id="modal-browser" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="fa fa-folder-open"></i>Zasoby systemowe</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
    
    <?php
        yii\bootstrap\Modal::begin([
          'header' => '<h4 class="modal-title btn-icon"><i class="fa fa-hand-o-up"></i>'.Yii::t('app', 'Confirm').'</h4>',
          //'toggleButton' => ['label' => 'click me'],
          'id' => 'modalConfirm', // <-- insert this modal's ID
        ]);
     
        echo ' <div class="modal-body modalContent"><p>Czy na pewno chcesz wykonać tą operację?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesConfirm" data-method="post" class="btn btn-danger">Potwierdź</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

        yii\bootstrap\Modal::end();
    ?> 
   
    <script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/all.css?v=<?php echo date ("ymdHis", filemtime("./css/all.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>
	<script type="text/javascript" src="/js/all.js?v=<?php echo date ("ymdHis", filemtime("./js/all.js"));?>"></script>
</body>
</html>
