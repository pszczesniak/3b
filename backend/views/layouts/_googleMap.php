<input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map"></div>
<script>
function initMap() { 
	var latlng = new google.maps.LatLng(<?= $model->pos_lat ?>, <?= $model->pos_lng ?>);
	
	map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: <?= $model->pos_lat ?>, lng: <?= $model->pos_lng ?>}, 
		  zoom: 15
	});
	
	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		title: 'Opis',
		draggable: true
	});

	google.maps.event.addListener(marker, 'dragend', function (event) {
		document.getElementById("latbox").value = <?= $model->pos_lat ?>;
		document.getElementById("lngbox").value = <?= $model->pos_lng ?>;
	});
	
	var infoWindow = new google.maps.InfoWindow({map: map});
	
	google.maps.event.addListener(map,'click',function(event) {
		document.getElementById('svcoffer-pos_lat').value = event.latLng.lat(); 
		document.getElementById('svcoffer-pos_lng').value = event.latLng.lng(); 
  
        var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
        fromGeoToFields(latlng);
	}); 
	
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
	searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds(); 
           
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            
            var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()); 
            fromGeoToFields(latlng);
        });
        map.fitBounds(bounds);
    });


	// Try HTML5 geolocation.
	if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);

            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            fromGeoToFields(latlng);
            
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
	} else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
	}
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
}

function fromGeoToFields(latlng) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                document.getElementById('svcoffer-address').value = results[0].formatted_address;
                for (var i=0; i<results[0].address_components.length; i++) {
                    for (var b=0;b<results[0].address_components[i].types.length;b++) {

                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            //this is the object you are looking for
                            province = results[0].address_components[i];
                            break;
                        }
                        
                        if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
                            //this is the object you are looking for
                            city = results[0].address_components[i];
                            break;
                        }
                        
                        if (results[0].address_components[i].types[b] == "postal_code") {
                            //this is the object you are looking for
                            postal_code = results[0].address_components[i];
                            break;
                        }
                        
                        if (results[0].address_components[i].types[b] == "route") {
                            //this is the object you are looking for
                            address = results[0].address_components[i];
                            break;
                        }
                    }
                }
                //city data
                //alert(city.short_name + " " + city.long_name)
                document.getElementById('svcoffer-city').value = city.short_name;
                document.getElementById('svcoffer-postal_code').value = postal_code.short_name;
                document.getElementById('svcoffer-address').value = address.short_name;

                $('select#svcoffer-id_province_fk').find('option').each(function() {
                    //console.log($(this).text());
                    if( $(this).text().toLowerCase() == province.short_name ) {
                        $('select#svcoffer-id_province_fk').val($(this).val());
                        
                    }
                });

            } else {
                alert("No results found");
            }
        } else {
            alert("Geocoder failed due to: " + status);
        }
    });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrkRGgiRSasfjloZfpmWmIVG8DYJdLkEo&libraries=places&callback=initMap"  async defer></script>