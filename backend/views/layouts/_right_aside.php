<?php
    
?>
<div class="right-aside">
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                
                <?php if((Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'dashboard') || (Yii::$app->controller->id == 'calendar' && Yii::$app->controller->action->id == 'personal')  ) {   ?>
                    <li class="active"><a data-toggle="tab" href="#aside-tab2"><i class="fa fa-comments"></i></a></li>
                <?php } else { ?>  
                    <li class="active"><a data-toggle="tab" href="#aside-tab1"><i class="fa fa-tasks"></i></a></li>
                    <li><a data-toggle="tab" href="#aside-tab2"><i class="fa fa-comments"></i></a></li>
                <?php }  ?>    
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <?php if( (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'dashboard') || (Yii::$app->controller->id == 'calendar' && Yii::$app->controller->action->id == 'personal') ) {  ?>
                    <div class="tab-pane active" id="aside-tab2">
                        <?= Chat::widget([]) ?>
                    </div>
                <?php } else { ?>
                    <div class="tab-pane active" id="aside-tab1">
                        <div class="view-date">
                            <!--<div class="md-subheader">  
                                <div class="md-subheader-inner">    
                                    <div class="md-subheader-content"><span>TODAY</span></div> 
                                </div>
                            </div>-->
                            <div class="view-date-item">   
                                <div class="secondary-text">
                                    <div><?= Yii::t('app', date('l') ) ?></div>
                                    <div><span><?= date('d') ?></span> <span><?= Yii::t('app', date('F') ) ?></span></div>
                                </div>
                                <div class="md-secondary-container"></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="tab-pane" id="aside-tab2">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>