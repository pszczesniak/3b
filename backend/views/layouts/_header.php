<?php
    use yii\helpers\Url;
?>
<header class="l-header">
    <button class="hamburger hamburger--arrow js-burger is-active" type="button">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </button>
    <div class="main-grid-spacer"></div>
    <?php if(!Yii::$app->user->isGuest) { ?>
        <div class="dropdown">
            <a class="l-header__avatar" href="#" data-toggle="dropdown" aria-expanded="false">
				<img src="<?= \common\models\Avatar::getAvatar(Yii::$app->user->id) ?>" alt="avatar" class="avtr">
            </a>
            <ul class="dropdown-menu pull-right list-default">
                <!--<li><a href="<?= Url::to(['/admin/profile/index']) ?>" title="Twój profil"><i class="item-icon fa fa-address-card-o"></i>Profil</a></li>-->
                <li><a href="<?= Url::to(['/site/changepassword']) ?>" title="Zmień hasło"><i class="item-icon fa fa-exchange"></i>Zmień hasło</a></li>
                <li><hr></li>
                <!--<li><a href="#" title="Pomoc">Pomoc</a></li>-->
                <li><a href="<?= Url::to(['/site/logout']) ?>" title="Wyloguj"><i class="item-icon fa fa-key"></i>Wyloguj</a></li>
            </ul>

        </div>

        <!--<a class="l-header__settings js-aside" href="#"><i class="fa fa-cog"></i></a>-->
    <?php } else { ?>
        <a class="l-header__settings" href="<?= \yii\helpers\Url::to(['/site/login']) ?>">Zaloguj się</a>
    <?php } ?>
</header>