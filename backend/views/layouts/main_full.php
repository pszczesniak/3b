<?php
/*http://webapplayers.com/inspinia_admin-v2.4/calendar.html#*/


use frontend\assets\AppAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use frontend\widgets\ModuleMenuWidget;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php /*$this->beginPage()*/ ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="preload">
    <div class="main-grid">
        <?= $this->render('_header') ?>

        <figure class="logo">
            <img src="/images/logo.jpg" alt="logo" style="max-height:65px; border: none;">
        </figure>

        <?= ModuleMenuWidget::widget(['module' => 'basic' ]) ?>

        <aside class="l-aside l-aside--hide js-aside-nav">
            <?= $this->render('_right_aside') ?>
        </aside>

         <main class="l-main l-main--0 js-main">
            <?= $content ?>
         </main>
    </div>
    
    <div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-event" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>Nowe zdarzenie</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
    
    <!-- Render modal form -->
	<div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-item" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-pencil"></i>Edytuj</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
    
    <!-- Render modal form -->
	<div tabindex="-1" role="dialog" class="fade modal out" id="modal-session" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title btn-icon"><i class="fa fa-exclamation-traingle"></i>Wygaśniecie sesji</h4>
                </div>
                <div class="modalContent"> Twoja sesja. Proszę się ponownie zalogować. </div>
                
            </div>
        </div>
    </div>
    
    <?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-person', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';

		yii\bootstrap\Modal::end();
	?> 

    <?php
        yii\bootstrap\Modal::begin([
          'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Update').'</h4>',
          //'toggleButton' => ['label' => 'click me'],
          'id' => 'modal-grid-file', // <-- insert this modal's ID
          'class' => 'modal-grid'
        ]);
     
        echo '<div class="modalContent"></div>';

        yii\bootstrap\Modal::end();
    ?>

    <?php
        yii\bootstrap\Modal::begin([
          'header' => '<h4 class="modal-title btn-icon"><i class="fa fa-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
          //'toggleButton' => ['label' => 'click me'],
          'id' => 'modalConfirmDelete', // <-- insert this modal's ID
        ]);
     
        echo ' <div class="modal-body"><p>Czy na pewno chcesz wykonać tą operację?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" data-method="post" class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

        yii\bootstrap\Modal::end();
    ?> 
    
    <?php
        yii\bootstrap\Modal::begin([
          'header' => '<h4 class="modal-title btn-icon"><i class="fa fa-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
          //'toggleButton' => ['label' => 'click me'],
          'id' => 'modalConfirm', // <-- insert this modal's ID
        ]);
     
        echo ' <div class="modal-body"><p>Czy na pewno chcesz wykonać tą operację?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesConfirm" data-method="post" class="btn btn-danger">Potwierdź</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

        yii\bootstrap\Modal::end();
    ?> 

    <script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/all.css?v=<?php echo date ("ymdHis", filemtime("./css/all.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>
	<script type="text/javascript" src="/js/all.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>
</body>
</html>
<?php /*$this->endPage()*/ ?>
