<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/*
$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;*/
?>
<?php /*$this->beginContent($controller->module->mainLayout)*/ ?>
<div class="padding-md">
    <div class="view-window" >
        <div class="view-window-title">
            <h5><?= ($title) ?></h5>
            <div class="view-window-tools">
               
                <a class="collapse-link collapse-window" data-toggle="collapse" href="#<?= $class ?>" aria-expanded="true" aria-controls="<?= $class ?>">
                    <i class="collapse-window-icon fa fa-angle-up"></i>
                </a>
                
            </div>
        </div>
        <div class="view-window-content collapse in" id="<?= $class ?>" aria-expanded="true">
            <?= $content ?>
        </div>
    </div>
    <?php /*$this->endContent();*/ ?>
</div>