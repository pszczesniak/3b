<?php
/*http://webapplayers.com/inspinia_admin-v2.4/calendar.html#*/
/*http://wrapbootstrap.com/preview/WB09JXK43*/
/*http://wrapbootstrap.com/preview/WB0048JF7*/
/*https://genesisui.com/demo/clever/bootstrap4-static/index.html*/

use frontend\assets\AppAsset;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use frontend\widgets\ModuleMenuWidget;
/* @var $this \yii\web\View */
/* @var $content string */

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <style> .preload:after { content: ' '; position: fixed; top: 0; left: 0; z-index: 2000; display: block; width: 100%; height: 100%; opacity: 1; background: url(/img/loading.gif) no-repeat center center rgba(255, 255, 255, 0.95); transition: opacity 0.3s linear; } .preload--off:after { opacity: 0; } .preload * { transition: none !important; }    </style>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="blank-page">
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <?= $content ?>
            </div>
        </div>
    </section>
   
    <script >var ms = document.createElement('link');ms.rel = 'stylesheet';ms.href = '/css/all.css?v=<?php echo date ("ymdHis", filemtime("./css/all.css"));?>';document.getElementsByTagName("head")[0].appendChild(ms);</script>
	<script type="text/javascript" src="/js/login.js?v=<?php echo date ("ymdHis", filemtime("./js/login.js"));?>"></script>
</body>
</html>
