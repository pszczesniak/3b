<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */

$this->title = 'Aktualizacja treści';
$this->params['breadcrumbs'][] = ['label' => 'Konfiguracja', 'url' => ['index', ]];
$this->params['breadcrumbs'][] = $this->title;
/*$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');*/
?>
	
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-update-1', 'title'=>Html::encode($this->title))) ?>
	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
<?php $this->endContent(); ?>

<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>