<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Cms\CmsPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfiguracja';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"cms-page-index", 'title'=>Html::encode($this->title))) ?>

	<div id="toolbar" class="btn-group">
        
    </div>
    <?= GridView::widget([
        'id' => 'grid-view-dicts',
        'dataProvider' => $dataProvider,
        'tableOptions'=>Yii::$app->params['tableOptions'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			/*'fk_part_id',*/
			[
                'attribute' => 'system_name',
                'headerOptions' => ['data-searchable' => 'true', 'data-field' => "system_name", 'data-searchable' => "true", 'data-filter-control' => "input", /*'data-sortable' => 'true'*/],
            ],
			[
                'attribute' => 'type_fk',
                'headerOptions' => ['data-searchable' => 'true', 'data-field' => "type_fk", 'data-searchable' => "true", 'data-filter-control' => "input", /*'data-sortable' => 'true'*/],
                'value' => function ($data) {
                        $label = [1 => 'Treść na stronie', 2 => 'Treść emaila', '3' => 'Program partnerski'];
                        return $label[$data->type_fk];
                  },
            ],
			[
				'class' => 'yii\grid\ActionColumn',
				'contentOptions' => ['class' => 'table-actions'],
				'template' => '{update}',
				'buttons' => [
					
					'update' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
								'title' => Yii::t('app', 'Update'), 'class' => 'btn btn-default btn-sm'
						]);
					},
					
				],
			 ],
		],
	]); ?>
<?php $this->endContent(); ?>
