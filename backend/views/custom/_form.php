<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-page-form">
    <div id="validation-form" class="alert"></div>
	<?php $form = ActiveForm::begin([ 'id' => 'page-content', 'options' => ['class' => 'ajaxform'] ]); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 6, 'class'=> ($model->type_fk==3) ? 'textarea-400' : 'mce-full']) ;  ?>
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic']) ?>
        </div>

    <?php ActiveForm::end(); ?>
   
</div>
