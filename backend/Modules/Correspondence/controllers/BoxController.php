<?php

namespace app\Modules\Correspondence\controllers;

use Yii;
use backend\Modules\Correspondence\models\Correspondence;
use backend\Modules\Correspondence\models\CorrespondenceSearch;
use backend\Modules\Correspondence\models\CorrespondenceDepartment;
use backend\Modules\Correspondence\models\CorrespondenceEmployee;
use backend\Modules\Correspondence\models\CorrespondenceTask;
use backend\Modules\Correspondence\models\CorrespondenceAddresses;
use backend\Modules\Correspondence\models\CorrespondenceSend;
use backend\Modules\Correspondence\models\CorrespondenceSendItem;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * BoxController implements the CRUD actions for Correspondence model.
 */
class BoxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Correspondence models.
     * @return mixed
     */
   
    public function actionIndex()  {
        if( count(array_intersect(["correspondencePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        //$replacement = \backend\Modules\Company\models\CompanyReplacement::find()->where(['status' => 1, 'id_employee_fk' => $this->module->params['employeeId']])->andWhere("'".date('Y-m-d')."' between date_from and date_to")->one();
        //var_dump($replacement);exit;
        $postingDates = Correspondence::find()->where(['status' => 1, 'type_fk' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ]);
        $postingDates = $postingDates->andWhere('send_at is null')->orderby('date_posting');
        $postingDates = $postingDates->select(['date_posting'])->distinct()->all();
        
        $searchModel = new CorrespondenceSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.box'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        } else {
            $queryParams["CorrespondenceSearch"]["date_delivery"] = date('Y-m-d');
            $queryParams["CorrespondenceSearch"]["id_company_branch_fk"] = $this->module->params['employeeBranch'];
        }
        $dataProvider = $searchModel->search($queryParams);
        
        $id = false;
        if(isset($_GET['id'])) { $model = Correspondence::findOne($_GET['id']); $id = $model->id; $searchModel->name = $model->name; }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'id' => $id,
            'postingDates' => ($postingDates) ? $postingDates : [],
            'setting' => $setting
        ]);
    }
	
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $post = $_GET;
        $where = []; $fields = []; $tmp = []; $file = false; $address = false;

        if(isset($_GET['CorrespondenceSearch'])) {
            $params = $_GET['CorrespondenceSearch'];
            \Yii::$app->session->set('search.box', ['params' => $params, 'post' => $post]);
            if(isset($params['id']) && !empty($params['id']) ) {
                array_push($where, "id = " . $params['id']);
            }
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(title_file) like '%".strtolower($params['name'])."%'"); $file = true;
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "id_customer_fk = ".$params['id_customer_fk']."");
            }
            if(isset($params['date_delivery']) && !empty($params['date_delivery']) ) {
                array_push($where, "DATE_FORMAT(date_delivery, '%Y-%m-%d') = '".$params['date_delivery']."'");
            }
            if(isset($params['date_posting']) && !empty($params['date_posting']) ) {
                array_push($where, "DATE_FORMAT(date_posting, '%Y-%m-%d') = '".$params['date_posting']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "b.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                array_push($where, "b.id_company_branch_fk = ".$params['id_company_branch_fk']."");
            }
            if(isset($params['id_sender_fk']) && !empty($params['id_sender_fk']) ) {
                //array_push($where, " (id_sender_fk = ".$params['id_sender_fk']." or id_recipient_fk = ".$params['id_sender_fk']." )  ");
     
                array_push($where, "ca.id = ".$params['id_sender_fk']);
            }
            if(isset($params['address_type_fk']) && !empty($params['address_type_fk']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(email) like '%".strtolower($params['email'])."%' ");
				array_push($where, " id_dict_address_type_fk = ".$params['address_type_fk']);
            }
        } else {
            array_push($where, "DATE_FORMAT(date_delivery, '%Y-%m-%d') = '".date('Y-m-d')."'");
            if($this->module->params['employeeBranch']) {
                array_push($where, "b.id_company_branch_fk = ".$this->module->params['employeeBranch']."");
            }
        }
        
		/*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin']) {  }  */
	
		$sortColumn = 'b.id';
        if( isset($post['sort']) && $post['sort'] == 'author' ) $sortColumn = 'ca.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'place' ) $sortColumn = 'cb.name';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'b.type_fk';
        if( isset($post['sort']) && $post['sort'] == 'delivery' ) $sortColumn = 'date_delivery';
        if( isset($post['sort']) && $post['sort'] == 'posting' ) $sortColumn = 'date_posting';
		
		$query = (new \yii\db\Query())
            ->select(['b.id as id', 'b.type_fk as type_fk', 'send_at', 'send_by', 'date_delivery', 'date_posting', 'id_customer_fk', 'c.name as cname', 'c.id as cid', 'cb.name as cbname', 'GROUP_CONCAT(fulladdress(ca.id)) as caname'
                      /* '(select concat_ws(",", ca.name) from {{%correspondence_address}} ca join {{%correspondence_addresses}} cas on cas.id_address_fk = ca.id where cas.id_correspondence_fk = c.id) as caname',*/])
            ->from('{{%correspondence}} as b')
            ->join(' JOIN', '{{%company_branch}} as cb', 'cb.id = b.id_company_branch_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = b.id_customer_fk')
            ->join(' JOIN', '{{%correspondence_addresses}} as cas', 'b.id = cas.id_correspondence_fk')
            ->join(' JOIN', '{{%correspondence_address}} as ca', 'ca.id = cas.id_address_fk')
           // ->join(' JOIN', '{{%correspondence_address}} as ca', '( (b.id_recipient_fk=ca.id and b.type_fk=2) or (b.id_sender_fk=ca.id and b.type_fk=1) )')
            ->where( ['b.status' => 1] )
            ->groupBy(['b.id', 'b.type_fk', 'send_at', 'send_by', 'date_delivery', 'date_posting', 'id_customer_fk', 'c.name', 'c.id', 'cb.name']);
	    if($file)
            $query = $query->join(' JOIN', '{{%files}} as f', 'b.id = f.id_fk and f.status=1 and f.id_type_file_fk = 5');
        if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin']) {
			$query = $query->join(' JOIN', '{{%correspondence_employee}} as ce', 'b.id = ce.id_correspondence_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );

        $rows = $query->all();
		foreach($rows as $key=>$value) {
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['cid']]).'" title="Kartka klienta" >'.$value['cname'].'</a>';
            $tmp['author'] = $value['caname'];
            $tmp['type'] = ($value['type_fk'] == 1) ? '<span class="text--blue"><i class="fa fa-envelope"></i></span>' : '<span class="text--orange"><i class="fa fa-send"></i></span>';
			$tmp['delivery'] = date('Y-m-d', strtotime($value['date_delivery']));
            $tmp['posting'] = date('Y-m-d', strtotime($value['date_posting']));
			$tmp['place'] = ($value['cbname']);
			$tmp['actions'] = '<div class="edit-btn-group">'
								. '<a href="'.Url::to(['/correspondence/box/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"><i class="fa fa-eye"></i></a>'
								. ( ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 && (!$value['send_by'] || $value['type_fk']==2) ) ?  '<a href="'.Url::to(['/correspondence/box/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default  " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '')
								. ( ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 && (!$value['send_by'] || $value['type_fk']==2) ) ?  '<a href="'.Url::to(['/correspondence/box/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="fa fa-trash"></i></a>' : '')
							. '</div>';
            if($value['type_fk'] == 1)
                $tmp['send'] = ($value['send_at']) ? '<i class="fa fa-check-circle text--green"></i>' : '<i class="fa fa-exclamation-circle text--red"></i>';
            else
                $tmp['send'] = '<i class="fa fa-check-circle text--purple"></i>';

            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single Correspondence model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        if( count(array_intersect(["correspondencePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        $grants = $this->module->params['grants'];
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        if(!$this->module->params['isAdmin'] && count(array_intersect(["correspondencePanel", "grantAll"], $this->module->params['grants'])) != 0){
            $checked = \backend\Modules\Correspondence\models\CorrespondenceEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_correspondence_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego wpisu.');
            }
        }
               
        return $this->render('view', [
            'model' => $model, 'grants' => $grants, 'isAdmin' => $this->module->params['isAdmin'], 'isSpecial' => $this->module->params['isSpecial']
        ]);
    }
    
    public function actionViewajax($id)  {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        $employeesList = '';
        
        $grants = $this->module->params['grants'];
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->employees as $key => $item) {
            $employeesList .= '<li>'.$item->employee['fullname']  .'</li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';

        
        return $this->renderAjax('_viewAjax', [
            'model' => $model,  'edit' => isset($grants['correspondenceEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }

    /**
     * Creates a new Correspondence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        if( count(array_intersect(["correspondenceAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
		$grants = $this->module->params['grants']; $departments = []; $employees = []; $ids = [0 => 0];
        $lists = ['departments' => [], 'employees' => [], 'contacts' => []];
        
		if ( CustomHelpers::decode($id) == 0 ) {
            $model = new Correspondence();
            $model->type_fk = 1;
            $model->date_delivery = date("Y-m-d");  $model->date_posting = date("Y-m-d");
            $model->status = 0;
            $model->id_company_branch_fk = $this->module->params['employeeBranch'];
            $model->save();
        } else {
			$model = $this->findModel($id);
        }     
        $model->user_action = 'create';
        //$model->addresses_list = [];
        //$model->cases_list = [];
 
		if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            $model->status = 1;
          
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->employees_list = [];
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                    array_push($model->employees_list, $value);
                }
            }  
            
            if( isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) && count($_POST['Correspondence']['events_list']) == 1) {
                $model->id_event_fk = $_POST['Correspondence']['events_list'][0];
            }
            if( isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) && count($_POST['Correspondence']['cases_list']) == 1) {
                $model->id_case_fk = $_POST['Correspondence']['cases_list'][0];
            } else {
                $model->id_case_fk = count($_POST['Correspondence']['cases_list']);
            }
            
            if(is_array($_POST['Correspondence']['addresses_list'])) {
                if( isset($_POST['Correspondence']['addresses_list']) && !empty($_POST['Correspondence']['addresses_list']) && count($_POST['Correspondence']['addresses_list']) == 1) {
                    if($model->type_fk == 1)
                        $model->id_sender_fk = $_POST['Correspondence']['addresses_list'][0];
                    else
                        $model->id_recipient_fk = $_POST['Correspondence']['addresses_list'][0];
                } else {
                    if($model->type_fk == 1)
                        $model->id_sender_fk = count($_POST['Correspondence']['addresses_list']);
                    else
                        $model->id_recipient_fk = count($_POST['Correspondence']['addresses_list']);
                }
            } else {
                if($model->type_fk == 1)
                    $model->id_sender_fk = 0;
                else
                    $model->id_recipient_fk = 0;
            }
            //echo var_dump($_POST['Correspondence']['addresses_list']); exit;
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if($model->validate() && $model->save()) {
                    if(isset($_POST['Correspondence']['departments_list']) && !empty($_POST['Correspondence']['departments_list']) ) {
                        CorrespondenceDepartment::deleteAll('id_correspondence_fk = :correspondence', [ ':correspondence' => $model->id ]);
                        foreach($_POST['Correspondence']['departments_list'] as $key => $value) {
                            $model_cd = new CorrespondenceDepartment();
                            $model_cd->id_correspondence_fk = $model->id;
                            $model_cd->id_department_fk = $value;
                            $model_cd->save();
                        }
                    } 
                    
                    if(isset($_POST['employees']) ) { 
                        CorrespondenceEmployee::deleteAll('id_correspondence_fk = :correspondence', [ ':correspondence' => $model->id ]);
                        foreach(Yii::$app->request->post('employees') as $key => $value) {
                            $model_ce = new CorrespondenceEmployee();
                            $model_ce->id_correspondence_fk = $model->id;
                            $model_ce->id_employee_fk = $value;
                            if(!$model_ce->save()) var_dump($model_ce->getErrors());
                        }
                    } 
                    
                    if(isset($_POST['Correspondence']['addresses_list']) && !empty($_POST['Correspondence']['addresses_list']) ) {
                        CorrespondenceAddresses::deleteAll('id_correspondence_fk = :correspondence', [ ':correspondence' => $model->id ]);
                        foreach($_POST['Correspondence']['addresses_list'] as $key => $value) {
                            $model_a = new CorrespondenceAddresses();
                            $model_a->id_correspondence_fk = $model->id;
                            $model_a->id_address_fk = $value;
                            $model_a->save();
                        }
                    } 
                    
                    if(isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) ) {
                        CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence and id_task_fk = -1', [ ':correspondence' => $model->id ]);
                        foreach($_POST['Correspondence']['cases_list'] as $key => $value) {
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $model->id;
                            if(Yii::$app->params['env'] == 'dev') {
                                $idsArr = explode('|', $value);
                                $model_cd->id_case_fk = $idsArr[0];
                                $model_cd->id_case_instance_fk = $idsArr[1];
                            } else {
                                $model_cd->id_case_fk = $value;
                            }
                            $model_cd->id_task_fk = -1;
                            $model_cd->save();
                        }
                    } 
                    
                    if(isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) ) {
                        CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence and id_task_fk != -1', [ ':correspondence' => $model->id ]);
                        foreach($_POST['Correspondence']['events_list'] as $key => $value) {
                            $task = \backend\Modules\Task\models\CalTask::findOne($value);
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $model->id;
                            $model_cd->id_case_fk = $task->id_case_fk;
                            $model_cd->id_task_fk = $value;
                            $model_cd->save();
                            
                            if($model->type_fk == 2) {  
                                $task->scenario = CalTask::SCENARIO_CLOSE;
                                $task->id_dict_task_status_fk = 2;
                                $task->user_action = 'close';
                                $task->save();
                            }
                        }
                    } 
                    
                    /*$company = \backend\Modules\Company\models\Company::findOne(1);
                    $customData = \yii\helpers\Json::decode($company->custom_data);
                    
                    if( isset($customData['box']) ) {
                        $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                        $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
                    } else {
                        $company->box_special_emails = '';
                        $company->box_special_employees = [ 0 => 0 ];
                    }
        
                    $specialEmployees = [];
                    $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where(' id_employee_fk not in ('.implode(',', $company->box_special_employees).') and id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
                    foreach($specialEmployeesSql as $key => $value) {
                        $model_ce = new CorrespondenceEmployee();
                        $model_ce->id_correspondence_fk = $model->id;
                        $model_ce->id_employee_fk = $value->id_employee_fk;
                        $model_ce->is_show = 0;
                        $model_ce->save();
                    }*/
   
                    $transaction->commit();
  
                    Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Korespondencja została zapisana')  );
                    return $this->redirect(['view', 'id' => $model->id]);	
                } else {
                    $model->status = 0;
                    $transaction->rollBack();
                    $lists = $this->lists($model->id_customer_fk, $model->departments_list);               
                    return  $this->render((Yii::$app->params['env'] == 'dev')?'create_new':'create', [ 'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]);		
                }

            } catch (Exception $e) {
                $transaction->rollBack();
                $model->status = 0;
                $transaction->rollBack();
                return  $this->render((Yii::$app->params['env'] == 'dev')?'create_new':'create', [ 'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]);	              
            }	
		} else {
			return  $this->render((Yii::$app->params['env'] == 'dev')?'create_new':'create', [ 'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]);	
		}
	}

    /**
     * Updates an existing Correspondence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	    if( count(array_intersect(["correspondenceEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
        $model = $this->findModel($id);
        $model->user_action = 'update';
        $departments = []; $employees = [];
        $grants = $this->module->params['grants'];
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
		
        if(!$this->module->params['isAdmin'] && count(array_intersect(["correspondenceEdit", "grantAll"], $this->module->params['grants'])) != 0){
            $checked = \backend\Modules\Correspondence\models\CorrespondenceEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_correspondence_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego wpisu.');
            }
        }
		
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
			
        }
		$model->departments_list = $departments;
        $lists = $this->lists($model->id_customer_fk, $model->departments_list);
   
        foreach($model->employees as $key => $item) {
            array_push($employees, $item['id_employee_fk']);
        }
        $model->events_list = [];
        foreach($model->events as $key => $item) {
            array_push($model->events_list, $item->id_task_fk);	
        }
        $model->cases_list = [];
        foreach($model->cases as $key => $item) {
            array_push($model->cases_list, $item->id_case_fk);	
        }
        foreach($model->addresses as $key => $item) {
            array_push($model->addresses_list, $item->id_address_fk);	
        }
 
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            
            if($model->send_at && $model->type_fk == 1) {
                return array('success' => false,  'html' => 'Korespondencja została już wysłana i nie może zostać zmodyfikowana' ,'errors' => [ 0 => 'Korespondencja została już wysłana i nie może zostać zmodyfikowana']);
            }
			$model->load(Yii::$app->request->post());
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_case_fk = isset($_POST['Correspondence']['cases_list']) ? count($_POST['Correspondence']['cases_list']) : '';
            $model->id_department_fk = isset($_POST['Correspondence']['departments_list']) ? count($_POST['Correspondence']['departments_list']) : '';
            if(isset($_POST['Correspondence']['departments_list']) && !empty($_POST['Correspondence']['departments_list']) ) {
                foreach($_POST['Correspondence']['departments_list'] as $key=>$value) {
                    array_push($model->departments_rel, $value);
                    array_push($departments, $value);
                }
            } 
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);
            
            if( isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) && count($_POST['Correspondence']['events_list']) == 1) {
                $model->id_event_fk = $_POST['Correspondence']['events_list'][0];
            }
            
            if( isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) && count($_POST['Correspondence']['cases_list']) == 1) {
                $model->id_case_fk = $_POST['Correspondence']['cases_list'][0];
            }
            
            if($model->status == 0) $model->status = 1;
			if($model->validate() && $model->save()) {
                if(isset($_POST['Correspondence']['addresses_list']) && !empty($_POST['Correspondence']['addresses_list']) ) {
                    //CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence and id_task_fk = -1', [ ':correspondence' => $model->id ]);
                    CorrespondenceAddresses::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'id_correspondence_fk = '.$model->id.' and status = 1 and id_address_fk not in ('.implode(',', $_POST['Correspondence']['addresses_list']).')');
                    foreach($_POST['Correspondence']['addresses_list'] as $key => $value) {
                        $exist = CorrespondenceAddresses::find()->where(['id_correspondence_fk' => $model->id, 'id_address_fk' => $value, 'status' => 1])->count();
                        if(!$exist) {
                            $model_a = new CorrespondenceAddresses();
                            $model_a->id_correspondence_fk = $model->id;
                            $model_a->id_address_fk = $value;
                            $model_a->save();
                        }
                    }
                } 
                
                if(isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) ) {
                    //CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence and id_task_fk = -1', [ ':correspondence' => $model->id ]);
                    CorrespondenceTask::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'id_correspondence_fk = '.$model->id.' and status = 1 and id_task_fk = -1 and id_case_fk not in ('.implode(',', $_POST['Correspondence']['cases_list']).')');
                    foreach($_POST['Correspondence']['cases_list'] as $key => $value) {
                        $exist = CorrespondenceTask::find()->where(['id_correspondence_fk' => $model->id, 'id_case_fk' => $value, 'status' => 1, 'id_task_fk' => -1])->count();
                        if(!$exist) {
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $model->id;
                            $model_cd->id_case_fk = $value;
                            $model_cd->id_task_fk = -1;
                            $model_cd->save();
                        }
                    }
                } 
                
                if(isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) ) {
                    //CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence and id_task_fk != -1', [ ':correspondence' => $model->id ]);
                    CorrespondenceTask::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'id_correspondence_fk = '.$model->id.' and status = 1 and id_task_fk != -1 and id_task_fk not in ('.implode(',', $_POST['Correspondence']['events_list']).')');                    
                    foreach($_POST['Correspondence']['events_list'] as $key => $value) {
                        $task = \backend\Modules\Task\models\CalTask::findOne($value);
                        $exist = CorrespondenceTask::find()->where(['id_task_fk' => $value, 'status' => 1, 'id_correspondence_fk' => $model->id])->count();
                        if(!$exist) {
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $model->id;
                            $model_cd->id_case_fk = $task->id_case_fk;
                            $model_cd->id_task_fk = $value;
                            $model_cd->save();
                        }
                        
                        if($model->type_fk == 2) {
                            $task->scenario = CalTask::SCENARIO_CLOSE;
                            $task->id_dict_task_status_fk = 2;
                            $task->user_action = 'close';
                            $task->save();
                        }
                    }
                } /*else {
                    CorrespondenceTask::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'status = 1 and id_task_fk != -1');                    
                }*/
       
				return array('success' => true,  'alert' => 'Korespondencja <b>'.$model->name.'</b> została zaktualizowana' );	
			} else {
                $lists = $this->lists($model->id_customer_fk, $model->departments_list);
				return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]) ;	
		}
    }

	private function lists($id, $departmentsList) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
                       
        $model = \backend\Modules\Crm\models\Customer::findOne($id);
        if(empty($departmentsList)) $departmentsList = [];
        $departments = []; $ids = $departmentsList;   $employees = []; $contacts = [];
        $checked = 'checked';
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
    
        $specialDepartmens = [];
        $specialDepartmensSql = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartmensSql as $key => $value) {
            array_push($specialDepartmens, $value->id);
        }
        
        foreach( $departmentsList as $key => $value ) {
            if( in_array($value, $specialDepartmens) ) {
                unset($ids[$key]);
            }
        }
        $ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
        
        $company = \backend\Modules\Company\models\Company::findOne(1);
        $customData = \yii\helpers\Json::decode($company->custom_data);
        
        if( isset($customData['box']) ) {
            $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
            $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
        } else {
            $company->box_special_emails = '';
            $company->box_special_employees = [ 0 => 0 ];
        }
    
        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')))')->orderby('lastname')->all();        
       
        foreach($employeesData as $key => $item) {
            $employees[$item->id] = $item->fullname;                    
        }
                    
        return ['contacts' => $contacts, 'departments' => $departments, 'employees' => $employees];
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
        $departments = []; $employees = [];
        $grants = $this->module->params['grants'];
		
		$lists = $this->lists($model->id_customer_fk);
        
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
        }
        foreach($model->employees as $key => $item) {
            array_push($employees, $item->id_employee_fk);
        }

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_department_fk = isset($_POST['departments']) ? count($_POST['departments']) : '';
            if(isset($_POST['departments']) ) {
                foreach(Yii::$app->request->post('departments') as $key=>$value) {
                    array_push($model->departments_rel, $value);
                    array_push($departments, $value);
                }
            } 
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            $lists = $this->lists($model->id_customer_fk);
            if($model->status == 0) $model->status = 1;
			if($model->validate() && $model->save()) {
				
				/*CorrespondenceDepartment::deleteAll('id_correspondence_fk = :correspondence', [':correspondence' => $id]);
				if(isset($_POST['departments']) ) {
					//
                    foreach(Yii::$app->request->post('departments') as $key=>$value) {
                        $model_cd = new CorrespondenceDepartment();
                        $model_cd->id_correspondence_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} 
				CorrespondenceEmployee::deleteAll('id_correspondence_fk = :correspondence', [':correspondence' => $id]);
				if(isset($_POST['employees']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('employees') as $key=>$value) {
                        $model_ce = new CorrespondenceEmployee();
                        $model_ce->id_correspondence_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
				} */
				
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="/correspondence/box/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
				$actionColumn .= ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/box/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>' : '';
				$actionColumn .= ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/box/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data">U</a>' : '';
				$actionColumn .= '</div>';
				
                $data['client'] = $model->customer['name'];
				//$tmp['name'] = '<a href="/task/case/viewajax/'.$value->id.'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value->name.'</a>';
				$data['type'] = ($model->type_fk == 1) ? 'Przychodząca' : 'Wychodząca';
				$data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
				$data['delivery'] = date('Y-m-y', strtotime($model->date_delivery));
				$data['place'] = ($model->branch) ? $model->branch['name'] : 'brak';
				$data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
				$data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
				$data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Korespondencja <b>'.$model->name.'</b> została zaktualizowana' );	
			} else {
                $lists = $this->lists($model->id_customer_fk);
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]) ;	
		}
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $grants = $this->module->params['grants'];
        $model = $this->findModel($id);
        if( ( count(array_intersect(["grantAll"], $grants)) == 0 ) && ($model->send_by && $model->type_fk==1) ) {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Uprawnienia do usunięcia tej korespondencji ma tylko administrator')  );
            $id = CustomHelpers::decode($id);
            return $this->redirect(['view', 'id' => $id]);
        }
        
        if($model->delete()) {
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Korespondencja została usunięta')  );	
        } else {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Korespondencja nie mogła zostać usunięta')  );
        }
        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-box');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-box');	
        }
    }

    /**
     * Finds the Correspondence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Correspondence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = Correspondence::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionExportform() {
        return  $this->renderAjax('_formExport', [ ]) ;	
    }
	
	public function actionExport() {
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
        $objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Korespondencja');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Zakres dat: '.( (isset($_GET['Correspondence']['date_from']) && !empty($_GET['Correspondence']['date_from']) ) ? $_GET['Correspondence']['date_from'] : 'brak daty początkowej' ).' - '.( (isset($_GET['Correspondence']['date_to']) && !empty($_GET['Correspondence']['date_to']) ) ? $_GET['Correspondence']['date_to'] : 'brak daty końcowej' ));
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->getStartColor()->setARGB('ffff00');
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(false);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(11);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A4', 'Miejsce')
			->setCellValue('B4', 'Data')
            ->setCellValue('C4', 'Klient')
			->setCellValue('D4', 'Sprawa')
			->setCellValue('E4', 'Nadawca')
			->setCellValue('F4', 'Dokument')
            ->setCellValue('G4', 'Typ');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()->getStartColor()->setARGB('D9D9D9');
 
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('C4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('C4')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('D4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('D4')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        //$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('E4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('E4')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        //$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('F4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('F4')->getFill()->getStartColor()->setARGB('D9D9D9');
        
       // $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('G4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('G4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('G4')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=5; 
		$data = [];

        $fieldsDataQuery = Correspondence::find()->where(['status' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch']]);
        
        if(isset($_GET['Correspondence'])) {
            $params = $_GET['Correspondence'];
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_posting, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_posting, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
        }

        /*if(count(array_intersect(["grantAll"], $this->module->params['grants'])) == 0) {
            $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_correspondence_fk from {{%correspondence_employee}} where id_employee_fk = ('. $this->module->params['employeeId'].') )');
        }*/
            
        $data = $fieldsDataQuery->all();
		$files = '';	
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->branch['name'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, date('Y-m-d', strtotime($record->date_posting))); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->customer['name'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->case['name']); 
                $addresses = [];
                foreach($record->addresses as $key => $address) {
                    array_push($addresses, $address->address['name']);
                }
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, implode(', ', $addresses) /*$record->author*/); 
				
				//$objPHPExcel->getActiveSheet()->setCellValue('I'. $i, sizeof($record->files)); 
				
				$files = '';
				
				if ( sizeof($record->files) == 1 ) {
					$files = $record->files[0]->title_file;
				} else {
					foreach($record->files as $key => $file) {
						$files .= $file->title_file."\r";
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $files); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
                //$files = '';
                //foreach($record->files as $key => $file) {
                //    $files .= $file->title_file."\r";
                //} //"Hello".PHP_EOL." World"
                //$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $files); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, ($record->type_fk == 1) ? 'Przychodząca' : 'Wychodząca');
                
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setAutoSize(true);*/
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		
		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$i)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A4:G'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Korespondencja');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Korespondencja'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionSendform() {
        $grants = $this->module->params['grants'];
        
        $branch = \backend\Modules\Company\models\CompanyBranch::findOne($this->module->params['employeeBranch']);

		$fieldsDataQuery = $fieldsData = Correspondence::find()->where(['status' => 1, 'type_fk' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ]);
        $fieldsDataQuery = $fieldsDataQuery->andWhere('send_at is null');
        $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_delivery, '%Y-%m-%d') = '".date('Y-m-d')."'");
        
        $counter = $fieldsDataQuery->count();
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT SUM(size_file)/1024/1024 AS total_size FROM {{%files}}
            WHERE status = 1 and id_type_file_fk = 5 and id_fk in (select id from {{%correspondence}} where status = 1 and send_at is null and type_fk = 1 and id_company_branch_fk = :branchId and DATE_FORMAT(date_delivery, \'%Y-%m-%d\') = :dateDelivery)',
            [':branchId' => $this->module->params['employeeBranch'], 'dateDelivery' => date('Y-m-d')]);

        $total_size = $command->queryAll();
        
        return  $this->renderAjax('_formSend', [ 'counter' => $counter, 'branch' => $branch, 'total_size' => round($total_size[0]['total_size'], 2) ]) ;	
    }
    
    public function actionSendformbyposting() {
        $grants = $this->module->params['grants'];
        $postingDate = (isset($_GET['postingDate']) && !empty($_GET['postingDate'])) ? date('Y-m-d', $_GET['postingDate']) : date('Y-m-d');
        
        $branch = \backend\Modules\Company\models\CompanyBranch::findOne($this->module->params['employeeBranch']);

		$fieldsDataQuery = $fieldsData = Correspondence::find()->where(['status' => 1, 'type_fk' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ]);
        $fieldsDataQuery = $fieldsDataQuery->andWhere('send_at is null');
        $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_posting, '%Y-%m-%d') = '".$postingDate."'");
        
        $counter = $fieldsDataQuery->count();
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT SUM(size_file)/1024/1024 AS total_size FROM {{%files}}
            WHERE status = 1 and id_type_file_fk = 5 and id_fk in (select id from {{%correspondence}} where status = 1 and send_at is null and type_fk = 1 and id_company_branch_fk = :branchId and DATE_FORMAT(date_posting, \'%Y-%m-%d\') = :date_posting)',
            [':branchId' => $this->module->params['employeeBranch'], 'date_posting' => $postingDate]);

        $total_size = $command->queryAll();
        
        return  $this->renderAjax('_formSendPosting', [ 'counter' => $counter, 'branch' => $branch, 'postingDate' => $postingDate, 'total_size' => round($total_size[0]['total_size'], 2) ]) ;	
    }
    
    public function actionSend() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $branch = \backend\Modules\Company\models\CompanyBranch::findOne($this->module->params['employeeBranch']);
		$place = ($branch) ? $branch->name : 'brak informacji';
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT SUM(size_file)/1024/1024 AS total_size FROM {{%files}}
            WHERE status = 1 and id_type_file_fk = 5 and id_fk in (select id from {{%correspondence}} where status = 1 and send_at is null and type_fk = 1 and id_company_branch_fk = :branchId and DATE_FORMAT(date_delivery, \'%Y-%m-%d\') = :date_delivery)',
            [':branchId' => $this->module->params['employeeBranch'], 'date_delivery' => date('Y-m-d')]);

        $total_size = $command->queryAll();
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = $fieldsData = Correspondence::find()->where(['status' => 1, 'type_fk' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ]);
        $fieldsDataQuery = $fieldsDataQuery->andWhere('send_at is null');
        $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_delivery, '%Y-%m-%d') = '".date('Y-m-d')."'")->orderby("date_posting");
        
        $data = $fieldsDataQuery->all();
      
        if( count($data) == 0) {
            return ['success' => false, 'info' => 'Nie było żadnej korespondencji do wysłania'];
        } else {
            $this->shareBox($data, 1, date('Y-m-d'), $place, $total_size);
            return ['success' => true, 'info' => 'Korespondencja zarejestrowana w dniu dzisiejszym została zgłoszona do wysłania'];
        }
    }
    
    public function actionSendbyposting() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $branch = \backend\Modules\Company\models\CompanyBranch::findOne($this->module->params['employeeBranch']);
		$place = ($branch) ? $branch->name : 'brak informacji';
        $postingDate = ( isset($_POST['postingDate']) && !empty($_POST['postingDate']) ) ? $_POST['postingDate'] : date('Y-m-d');
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT SUM(size_file)/1024/1024 AS total_size FROM {{%files}}
            WHERE status = 1 and id_type_file_fk = 5 and id_fk in (select id from {{%correspondence}} where status = 1 and send_at is null and type_fk = 1 and id_company_branch_fk = :branchId and DATE_FORMAT(date_posting, \'%Y-%m-%d\') = :date_posting)',
            [':branchId' => $this->module->params['employeeBranch'], 'date_posting' => $postingDate]);

        $total_size = $command->queryAll();
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = $fieldsData = Correspondence::find()->where(['status' => 1, 'type_fk' => 1, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ]);
        $fieldsDataQuery = $fieldsDataQuery->andWhere('send_at is null');
        $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_posting, '%Y-%m-%d') = '".$postingDate."'");
        
        $data = $fieldsDataQuery->all();
         
        if( count($data) == 0) {
            return ['success' => false, 'info' => 'Nie było żadnej korespondencji do wysłania'];
        } else {
            $this->shareBox($data, 2, $postingDate, $place, $total_size);
            return ['success' => true, 'info' => 'Korespondencja z dniem odbioru '.$postingDate.' została zgłoszona do wysłania'];
        }
    }
    
    private function shareBox($data, $type, $date, $place, $total_size) {
        $sendingArr = []; $sendingArr[0] = [];  $files = []; $arch = []; $sendingArrBig = []; 
        
        $company = \backend\Modules\Company\models\Company::findOne(1);
        $customData = \yii\helpers\Json::decode($company->custom_data);
     
        if( isset($customData['box']) ) {
            $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
            $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
        } else {
            $company->box_special_emails = '';
            $company->box_special_employees = [ 0 => 0 ];
        }
        $allInfoEmails = [];
        foreach( explode(',', $company->box_special_emails) as $ii => $val) {
            array_push($allInfoEmails, $val);
        }
        $specialEmployees = \backend\Modules\Company\models\CompanyEmployee::find()->where('id in ('.implode(',', $company->box_special_employees).')')->all();
        if($specialEmployees && !empty($specialEmployees)) {
            foreach( $specialEmployees as $ii => $val) {
                array_push($allInfoEmails, $val->email);
            }
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $sendLog = new CorrespondenceSend();
            $sendLog->type_fk = $type;
            $sendLog->id_company_branch_fk = $this->module->params['employeeBranch'];
            $sendLog->created_at = date('Y-m-d H:i:s');
            $sendLog->created_by = \Yii::$app->user->id;
            $sendLog->email_title = 'Poczta przychodząca z dnia '.$date.' wysłana z '.$place;
            $sendLog->posting_date = $date;
            $sendLog->total_size = round($total_size[0]['total_size'], 2);
            $sendLog->save();
            
            foreach($data as $key => $item) {
                $item->user_action = 'send';
                $item->send_at = date('Y-m-d H:i:s');
                $item->send_by = Yii::$app->user->id;
                $item->id_send_fk = $sendLog->id;
                $item->id_employee_fk = count($item->employees);
                $item->save();
                $files = []; $employees = '<ol>'; $events = '<ol>'; $cases = '<ol>'; $addresses = '<ol>'; $filesBig = []; $file10 = 0; $file40 = 0;
                
                foreach($item->employees as $i => $employee) {
                    $employees .= '<li>'.$employee['fullname'].'</li>';
                }
                foreach($item->cases as $i => $case) {
                    $cases .= '<li><a href="'.\Yii::$app->urlManager->createAbsoluteUrl(['/task/'.( ($case->case['type_fk'] == 1) ? 'matter' : 'project' ).'/view','id' => $case->id_case_fk]).'">'.$case->case['name'].'</a></li>';
                    $arch[$case->id_case_fk] = 'Poczta przychodząca z dnia '.$date.' wysłana z '.$place.':<br />';
                    foreach($item->files as $j => $file) {
                        //$files[$file->systemname_file.'.'.$file->extension_file] = $file->title_file;
                        $files[$file->id]['name'] = $file->systemname_file.'.'.$file->extension_file;
                        $files[$file->id]['title'] = $file->title_file;
                        $files[$file->id]['size'] = round($file->size_file/1024/1024, 2);
                        $arch[$case->id_case_fk] .= $file->title_file.' ['.round($file->size_file/1024/1024, 2).' MB]<br />';
                        if( ($file->size_file/1024/1024) >= 10 && ($file->size_file/1024/1024) < 40 ) {
                            ++$file10;
                            $filesBig[$file->id]['name'] = $file->systemname_file.'.'.$file->extension_file;
                            $filesBig[$file->id]['title'] = $file->title_file;
                            $filesBig[$file->id]['size'] = round($file->size_file/1024/1024, 2);
                        }
                        if( ($file->size_file/1024/1024) > 40 ) ++$file40;
                    }
                }
                foreach($item->events as $i => $task) {
                    $events .= '<li><a href="'.\Yii::$app->urlManager->createAbsoluteUrl(['/task/'.( ($task->task['type_fk'] == 1) ? 'case' : 'event' ).'/view','id' => $task->id_task_fk]).'">'.$task->task['name'].'</a><span style="color: red;"><br />Termin: <b>'.$task->task['event_date'].(($task->task['event_time']) ? ' '.$task->task['event_time'] : '' ).'</b></span></li>';
                }
                foreach($item->addresses as $i => $address) {
                    $addresses .= '<li>'.$address->address['fullname'].'</li>';
                }
                if( count($item->events) == 0 ) $events = false;
                
                $temp = [   
                            'id' => $item->id,
                            'author' => $addresses.'</ol>',//$item->author,
                            'customer' => $item->customer['name'], 
                            'note' => $item['note'], 
                            'date' => date('Y-m-d', strtotime($item['date_posting'])), 
                            /*'caseId' => $item->id_case_fk, 
                            'caseName' => $item->case['name'],*/
                            'cases' => $cases,
                            'caseType' => ($item->type_fk == 1) ? 'matter' : 'project',
                            'events' =>  (!$events) ? 'brak powiązanych czynności' : $events.'</ol>',
                            'employees' => $employees.'</ol>',
                            'files' => $files     
                        ]; 
                foreach($item->employees as $i => $rel) { 
                    if( isset($sendingArr[$rel['id_employee_fk']]) ) {
                        array_push( $sendingArr[$rel['id_employee_fk']], $temp );
                    } else {
                        $sendingArr[$rel['id_employee_fk']] = [];
                        array_push( $sendingArr[$rel['id_employee_fk']], $temp );
                    }
                    
                    if( count($filesBig) > 0 &&  !in_array($rel['id_employee_fk'], $company->box_special_employees) ) {
                        foreach($filesBig as $ii => $bigFile) {
                            $tempBig = [   
                                'id' => $item->id, 'author' => $item->author, 'customer' => $item->customer['name'],  'note' => $item['note'],  'date' => date('Y-m-d', strtotime($item['date_posting'])), 
                                'cases' => $cases, 'caseType' => ($item->type_fk == 1) ? 'matter' : 'project', 'events' =>  (!$events) ? 'brak powiązanych czynności' : $events.'</ol>',
                                'employees' => $employees.'</ol>', 'files' => [0=>$bigFile]
                            ];
                            $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($rel['id_employee_fk']);
                            array_push($sendingArrBig, ['employeeEmail' => $employee->email,'data' => $tempBig]);
                        }
                    }
                }
                if( count($filesBig) > 0 ) {
                    foreach($filesBig as $ii => $bigFile) {
                        $tempBig = [   
                            'id' => $item->id, 'author' => $item->author, 'customer' => $item->customer['name'],  'note' => $item['note'],  'date' => date('Y-m-d', strtotime($item['date_posting'])), 
                            'cases' => $cases, 'caseType' => ($item->type_fk == 1) ? 'matter' : 'project', 'events' =>  (!$events) ? 'brak powiązanych czynności' : $events.'</ol>',
                            'employees' => $employees.'</ol>', 'files' => [ 0 => $bigFile ]
                        ];
                        foreach($allInfoEmails as $i => $sEmployee) {
                            array_push($sendingArrBig, ['employeeEmail' => $sEmployee, 'data' => $tempBig]);
                        }
                    }
                }
                
                array_push($sendingArr[0], $temp);
                $temp = [];
            }
            
            foreach($sendingArr as $key => $value) {
                if($key == 0) { 
                    if(!empty($sendingArr[0]) && count($data) > 0 ) {
                        $branchAdministration = [];
                        $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                                        ->where(['status' => 1, 'id_dict_employee_kind_fk' => 50, 'id_company_branch_fk' => $this->module->params['employeeBranch'] ])
                                        ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                                        ->andWhere(' id not in ('.implode(',', $company->box_special_employees).')')
                                        ->all();
                        foreach($branchAdministrationSql as $ii => $item) {
                            array_push($branchAdministration, $item['email']);
                        }
                        $infoEmployeeManagerSql = \backend\Modules\Company\models\CompanyEmployee::find()
                                        ->where('id in (select id_employee_manager_fk from {{%company_department}} where all_employee = 1 )')
                                        ->all();
                        foreach($infoEmployeeManagerSql as $ii => $item) {
                            array_push($branchAdministration, $item['email']);
                        }
           
                        $totalSize = 0; $file10Temp = 0; $file40Temp = 0;
                        foreach($value as $ii => $itemData) {
                            foreach($itemData['files'] as $jj => $fileData) {
                                $totalSize += $fileData['size'];
                                if( $fileData['size'] >= 10 ) ++$file10Temp;
                                if( $fileData['size'] >= 40 ) ++$file40Temp;
                            }
                        }
                        if($totalSize <= 10) {
                            $sendItemLog = new CorrespondenceSendItem();
                            $sendItemLog->id_send_fk = $sendLog->id;
                            $sendItemLog->id_employee_fk = 0;
                            $sendItemLog->email_to = implode(',', $allInfoEmails);
                            $sendItemLog->email_cc = implode(',', $branchAdministration); 
                            $sendItemLog->email_temp = \yii\helpers\Json::encode( $value );
                            $sendItemLog->total_size = $totalSize;
                            $sendItemLog->files_night = $file10Temp;
                            $sendItemLog->files_big = $file40Temp;
                            $sendItemLog->save();
                        } else {
                            $totalSize = 0; $file10Temp = 0; $file40Temp = 0;
                            $valueTemp = []; 
                            foreach($value as $ii => $itemData) {
                                $itemDataTemp = $itemData;
                                $itemDataTemp['files'] = []; 
                                //$filesTemp = [];
                                
                                foreach($itemData['files'] as $jj => $fileData) {
                                    if($fileData['size'] < 10) { $totalSize += $fileData['size']; } else { if($fileData['size'] < 40) ++$file10Temp; else ++$file40Temp; }
                                    if($totalSize <= 10) {
                                        array_push($itemDataTemp['files'], $fileData);
                                        //$file10Temp = $file10Temp+$itemData['file10']; $file40Temp = $file40Temp+$itemData['file40'];
                                        //array_push($valueTemp, $itemData);
                                    } else {
                                        array_push($valueTemp, $itemDataTemp);
                                        $sendItemLog = new CorrespondenceSendItem();
                                        $sendItemLog->id_send_fk = $sendLog->id;
                                        $sendItemLog->id_employee_fk = 0; 
                                        $sendItemLog->email_to = implode(',', $allInfoEmails);
                                        $sendItemLog->email_cc = implode(',', $branchAdministration); 
                                        $sendItemLog->email_temp = \yii\helpers\Json::encode( $valueTemp );
                                        $sendItemLog->total_size = ($totalSize-$fileData['size']);
                                        $sendItemLog->files_night = $file10Temp;
                                        $sendItemLog->files_big = $file40Temp;
                                        $sendItemLog->save();
                                        $itemDataTemp['files'] = [];
                                        array_push($itemDataTemp['files'], $fileData);
                                        $totalSize = 0;
                                        if($fileData['size'] < 10) $totalSize += $fileData['size'];
                                        $valueTemp = []; $file10Temp = 0; $file40Temp = 0;
                                    } 
                                }
                                array_push($valueTemp, $itemDataTemp);
                            }
                            //array_push($valueTemp, $itemDataTemp);
                            $sendItemLog = new CorrespondenceSendItem();
                            $sendItemLog->id_send_fk = $sendLog->id;
                            $sendItemLog->id_employee_fk = 0; 
                            $sendItemLog->email_to = implode(',', $allInfoEmails);
                            $sendItemLog->email_cc = implode(',', $branchAdministration); 
                            $sendItemLog->email_temp = \yii\helpers\Json::encode( $valueTemp );
                            $sendItemLog->total_size = $totalSize;
                            $sendItemLog->files_night = $file10Temp;
                            $sendItemLog->files_big = $file40Temp;
                            $sendItemLog->save();  
                        }
                    }
                } else {
                    $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($key);
                    if( !in_array($employee->id, $company->box_special_employees) ) {
                        $totalSize = 0; $file10Temp = 0; $file40Temp = 0;
                        foreach($value as $ii => $itemData) {
                            foreach($itemData['files'] as $jj => $fileData) {
                                $totalSize += $fileData['size'];
                                if( $fileData['size'] >= 10 ) ++$file10Temp;
                                if( $fileData['size'] >= 40 ) ++$file40Temp;
                            }
                        }
                        if($totalSize <= 10) {
                            $sendItemLog = new CorrespondenceSendItem();
                            $sendItemLog->id_send_fk = $sendLog->id;
                            $sendItemLog->id_employee_fk = $key;
                            $sendItemLog->email_to = $employee->email;
                            $sendItemLog->email_cc = 'it@siw.pl';
                            $sendItemLog->email_temp = \yii\helpers\Json::encode( $value );
                            $sendItemLog->total_size = $totalSize;
                            $sendItemLog->files_night = $file10Temp;
                            $sendItemLog->files_big = $file40Temp;
                            $sendItemLog->save();
                        } else {
                            $totalSize = 0;
                            $valueTemp = [];
                            foreach($value as $ii => $itemData) {
                                $itemDataTemp = $itemData;
                                $itemDataTemp['files'] = []; $file10Temp = 0; $file40Temp = 0;
                                //$filesTemp = [];
                                
                                foreach($itemData['files'] as $jj => $fileData) {
                                    if($fileData['size'] < 10) { $totalSize += $fileData['size']; } else { if($fileData['size'] < 40) ++$file10Temp; else ++$file40Temp; }
                                    if($totalSize <= 10) {
                                        array_push($itemDataTemp['files'], $fileData);
                                        //array_push($valueTemp, $itemData);
                                    } else {
                                        array_push($valueTemp, $itemDataTemp);
                                        $sendItemLog = new CorrespondenceSendItem();
                                        $sendItemLog->id_send_fk = $sendLog->id;
                                        $sendItemLog->id_employee_fk = $key;
                                        $sendItemLog->email_to = $employee->email;
                                        $sendItemLog->email_cc = 'it@siw.pl';
                                        $sendItemLog->email_temp = \yii\helpers\Json::encode( $valueTemp );
                                        $sendItemLog->total_size = ($totalSize-$fileData['size']);
                                        $sendItemLog->files_night = $file10Temp;
                                        $sendItemLog->files_big = $file40Temp;
                                        $sendItemLog->save();
                                        $itemDataTemp['files'] = [];
                                        array_push($itemDataTemp['files'], $fileData);
                                        $totalSize = 0;
                                        if($fileData['size'] < 10) $totalSize += $fileData['size'];
                                        $valueTemp = []; $file10Temp = 0; $file40Temp = 0;
                                    } 
                                }
                                array_push($valueTemp, $itemDataTemp);
                            }
                            //array_push($valueTemp, $itemDataTemp);
                            $sendItemLog = new CorrespondenceSendItem();
                            $sendItemLog->id_send_fk = $sendLog->id;
                            $sendItemLog->id_employee_fk = $key;
                            $sendItemLog->email_to = $employee->email;
                            $sendItemLog->email_cc = 'it@siw.pl';
                            $sendItemLog->email_temp = \yii\helpers\Json::encode( $valueTemp );
                            $sendItemLog->total_size = $totalSize;
                            $sendItemLog->files_night = $file10Temp;
                            $sendItemLog->files_big = $file40Temp;
                            $sendItemLog->save();  
                        }
                    }
                }
                //echo '<br />';
            }
            $valueTemp = [];
            foreach($sendingArrBig as $key => $value) {
                array_push($valueTemp, $value['data']);
                $sendItemLogBig = new CorrespondenceSendItem();
                $sendItemLogBig->id_send_fk = $sendLog->id;
                $sendItemLogBig->id_employee_fk = -2;
                $sendItemLogBig->email_to = $value['employeeEmail'];
                $sendItemLogBig->email_temp = \yii\helpers\Json::encode( $valueTemp );
                $sendItemLogBig->total_size = $value['data']['files'][0]['size'];
                $sendItemLogBig->status = -1;
                $sendItemLogBig->save();  
                $valueTemp = [];
            }
            
            foreach($arch as $key => $case) {
                $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $key;
                $modelArch->user_action = 'correspondence';
                $modelArch->data_arch = \yii\helpers\Json::encode(['files' => $case]);
                $modelArch->data_change = '';
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                $modelArch->save();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
        }
    }
    
    public function actionNote($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
		$model = $this->findModel($id); 
        $model->id_employee_fk = count($model->employees);
        $model->user_action = 'change-note';        
        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            if( $model->validate() && $model->save()) {	
                     
                $changes = [
                    'note' => $model->note
                ]; 
                Yii::$app->response->format = Response::FORMAT_JSON;   
                return array('success' => true,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id, 'changes' => $changes, 'alert' => 'Treść notatki została zaktualizowana'  );
                        //return Yii::$app->getResponse()->redirect(['view', 'id' => $model->id]);	
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false, 'html' => $this->renderAjax('_changeNote', [ 'model' => $model]), 'errors' => $model->getErrors() );	
            }
		} else {
            return  $this->renderAjax('_changeNote', [ 'model' => $model]);	
        }
	}
    
    public function actionCase($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = $this->module->params['grants'];
		$id = CustomHelpers::decode($id);
        $model = \backend\Modules\Correspondence\models\CorrespondenceTask::findOne($id);
        $correspondence = \backend\Modules\Correspondence\models\Correspondence::findOne($model->id_correspondence_fk); 
        $model->id_customer_fk = $correspondence->id_customer_fk;
        $tempCase = $model->id_case_fk;
        //$model->id_employee_fk = count($model->employees);
        //$model->user_action = 'change-case'; 
        $events = 0;       
        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            
            foreach($correspondence->cases as $key => $item) {
                if($item->id_case_fk == $model->id_case_fk) {
                    Yii::$app->response->format = Response::FORMAT_JSON;   
                    return array('success' => false, 'html' => $this->renderAjax('_changeCase', [ 'model' => $model]), 'errors' => ['id_case_fk' => 'To postępowanie jest juz przypisane do tej korespondencji'] );	
                }
            }
            $transaction = \Yii::$app->db->beginTransaction();
            try {
				/* zapis do archiwum */
				$modelArch = new \backend\Modules\Correspondence\models\CorrespondenceArch();
				$modelArch->id_root_fk = $correspondence->id;
				$modelArch->user_action = 'change-case';
				$modelArch->data_arch = \yii\helpers\Json::encode( ['cases' => $correspondence->cases ] );
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = date('Y-m-d H:i:s');
				$modelArch->save();
				if( $model->validate() && $model->save()) {	 			
					if($correspondence->events) {				   
						foreach($correspondence->events as $key => $item) {
							if($item->id_case_fk == $tempCase) {
								$task = \backend\Modules\Task\models\CalTask::findOne($item->id_task_fk);
								$task->id_case_fk = $model->id_case_fk;
								$task->user_action = 'correspondence-case-change';
								$task->save();
								++$events;
							}
						}
						\backend\Modules\Correspondence\models\CorrespondenceTask::updateAll(['id_case_fk' => $model->id_case_fk], 'status = 1 and id_task_fk != -1 and id_case_fk = '.$tempCase.'');
					}
                    
                    if($model->id_customer_fk != $correspondence->id_customer_fk) {
                        $correspondence->id_customer_fk = $model->id_customer_fk;
                        $correspondence->user_action = 'change-case';
                        if(!$correspondence->save()) { var_dump($correspondence->getErrors()); exit;}
                    }
		            $transaction->commit();
					$changes = [
						'case-'.$model->id => '<a href="'.Url::to(['/task/'. ( ($model->case['type_fk'] == 1) ? 'matter' : 'project' ).'/view', 'id' => $model->id_case_fk]) .'">'.$model->case['name'].'</a>',
                        'customer' => '<a href="'.Url::to(['/crm/customer/view', 'id' => $correspondence->id_customer_fk]) .'">'.$correspondence->customer['name'].'</a>'
					]; 
					Yii::$app->response->format = Response::FORMAT_JSON;   
					return array('success' => true,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id, 'changes' => $changes, 'alert' => 'Korespondencja została przypisana do sprawy '.$model->case['name']  );
							//return Yii::$app->getResponse()->redirect(['view', 'id' => $model->id]);	
				} else {
					Yii::$app->response->format = Response::FORMAT_JSON;
					return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_changeCase_new':'_changeCase'), [ 'model' => $model, 'grants' => $grants, 'casesCounter' => count($correspondence->cases)]), 'errors' => $model->getErrors() );	
				}
			} catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_changeCase_new':'_changeCase'), [ 'model' => $model, 'grants' => $grants, 'casesCounter' => count($correspondence->cases)]), 'errors' => $model->getErrors() );	              
            }	
		} else {
            return  $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_changeCase_new':'_changeCase'), [ 'model' => $model, 'grants' => $grants, 'casesCounter' => count($correspondence->cases)]);	
        }
	}
	
	public function actionTransfer($id) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
		//return array('success' => true,  'alert' => 'Wersja prezentacyjna. Żadna akcja nie została wykonana.' );
		
        if( count(array_intersect(["correspondenceEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
        $model = $this->findModel($id);
        $transfer = new Correspondence();
        $transfer->user_action = 'transfer';
        $transfer->type_fk = 1;
        $transfer->status = 1;
        $transfer->id_send_fk = $model->id_send_fk;
        $transfer->send_at = $model->send_at;
        $transfer->send_by = $model->send_by;
        $transfer->id_company_branch_fk = $model->id_company_branch_fk;
        $transfer->id_transfer_fk = $model->id;
        $transfer->date_delivery = $model->date_delivery;
        $grants = $this->module->params['grants'];
        $transfer->docs_list = [];
        
        $departments = []; $employees = [];
        
        if($model->status == -1) {
            return  $this->render('delete', [ 'model' => $model, ]) ;	
        }
		
        if(!$this->module->params['isAdmin'] && count(array_intersect(["correspondenceEdit", "grantAll"], $this->module->params['grants'])) != 0){
            $checked = \backend\Modules\Correspondence\models\CorrespondenceEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_correspondence_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego wpisu.');
            }
        }
		
        if (Yii::$app->request->isPost ) {
			$transfer->load(Yii::$app->request->post());
         
            if( isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) && count($_POST['Correspondence']['events_list']) == 1) {
                $transfer->id_event_fk = $_POST['Correspondence']['events_list'][0];
            }
            if( isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) && count($_POST['Correspondence']['cases_list']) == 1) {
                $transfer->id_case_fk = $_POST['Correspondence']['cases_list'][0];
                
                foreach($_POST['Correspondence']['cases_list'] as $key => $value) {
                    $case = \backend\Modules\Task\models\CalCase::findOne($value);
                    foreach($case->departments as $i => $item) {
                        if(!in_array($item->id_department_fk, $departments)) array_push($departments, $item->id_department_fk);
                    }
                    
                    foreach($case->employees as $j => $emp) {
                        if(!in_array($emp['id_employee_fk'], $employees)) array_push($employees, $emp['id_employee_fk']);
                    }
                }

            } /*else {
                $transfer->id_case_fk = count($_POST['Correspondence']['cases_list']);
            }*/
         
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if( count($employees) > 0) $transfer->id_employee_fk = count($employees);
                if($transfer->validate() && $transfer->save()) {

                    foreach($departments as $key => $value) {
                        $model_cd = new CorrespondenceDepartment();
                        $model_cd->id_correspondence_fk = $transfer->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
                    
                    $company = \backend\Modules\Company\models\Company::findOne(1);
                    $customData = \yii\helpers\Json::decode($company->custom_data);
                    
                    if( isset($customData['box']) ) {
                        $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                        $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
                    } else {
                        $company->box_special_emails = '';
                        $company->box_special_employees = [ 0 => 0 ];
                    }
                     
                    foreach($company->box_special_employees as $key => $value) {
                        if(!in_array($value, $employees)) array_push($employees, $value);
                    }
    
                    foreach($employees as $key => $value) {
                        $model_ce = new CorrespondenceEmployee();
                        $model_ce->id_correspondence_fk = $transfer->id;
                        $model_ce->id_employee_fk = $value;
                        if(!$model_ce->save()) var_dump($model_ce->getErrors());
                    }
                  
                    if(isset($_POST['Correspondence']['cases_list']) && !empty($_POST['Correspondence']['cases_list']) ) {
                        foreach($_POST['Correspondence']['cases_list'] as $key => $value) {
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $transfer->id;
                            $model_cd->id_case_fk = $value;
                            $model_cd->id_task_fk = -1;
                            $model_cd->save();
                        }
                    } 
                    
                    if(isset($_POST['Correspondence']['events_list']) && !empty($_POST['Correspondence']['events_list']) ) {
                        foreach($_POST['Correspondence']['events_list'] as $key => $value) {
                            $model_cd = new CorrespondenceTask();
                            $model_cd->id_correspondence_fk = $transfer->id;
                            $model_cd->id_case_fk = \backend\Modules\Task\models\CalTask::findOne($value)->id_case_fk;
                            $model_cd->id_task_fk = $value;
                            $model_cd->save();
                        }
                    } 
                    
                    $sql = "update {{%files}} set id_fk = ".$transfer->id
                            ." where status = 1 and  id_type_file_fk = 5 and id_fk = ".$model->id ." and id in (".implode(',', $transfer->docs_list).")";
                    $data = \Yii::$app->db->createCommand($sql)->execute(); 
   
                    $transaction->commit();
                    
                    /* tylko informacji o zmianie w sprawie, że do sprawy x została przypisana poczta przychodząca z dnia i tu nazwa pliku + link. */	
                    $info = "Poczta przychodząca z dnia ".$transfer->date_posting." została przypisana do postępowań <br /><ul>";
                    foreach($transfer->cases as $key => $case) {
                        $info .= '<li><a href="'.\Yii::$app->urlManager->createAbsoluteUrl(['/task/'.(($case->case['type_fk'])?'matter':'project').'/view','id' => $case->id_case_fk]).'">'.$case->case['name'].'</a></li>';
                    }
                    $info .= '</ul><br />Przeniesione pliki to:<br /><ol>';
                    foreach($transfer->files as $key => $file) {
                        $info .= '<li>'.$file->title_file.'</li>';
                    }
                    $info .= '</ol>';
                    try {
                        \Yii::$app->mailer->compose(['html' => 'info-html', 'text' => 'info-text'], ['info' => $info ])
                                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                            //->setTo(\Yii::$app->params['testEmail'])
                                            //->setTo($employee->email)
                                            ->setTo(['kamila_bajdowska@onet.eu'])
                                            ->setSubject('Informacja systemu nt. przeniesienia poczty') 
                                            ->send();
                    } catch (\Swift_TransportException $e) {
                        $request = Yii::$app->request;
                        $log = new \common\models\Logs();
                        $log->id_user_fk = \Yii::$app->user->id;
                        $log->action_date = date('Y-m-d H:i:s');
                        $log->action_name = 'box-transfer-'.$transfer->id;
                        $log->action_describe = $e->getMessage();
                        $log->request_remote_addr = $request->getUserIP();
                        $log->request_user_agent = $request->getUserAgent(); 
                        $log->request_content_type = $request->getContentType(); 
                        $log->request_url = 'Informacja systemu nt. przeniesienia poczty';
                        $log->save();
                    }
                
                    Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Wskazane dokumenty zostały przeniesione do nowej korespondencji')  );
                    //return $this->redirect(['view', 'id' => $model->id]);	
                    return array('success' => true, 'html' => $this->renderAjax('_transfer', [ 'model' => $transfer]), 
                                                                                    'transfer' => $transfer->docs_list,
                                                                                    'alert' => 'Wskazane dokumenty zostały przeniesione do nowe korespondencji [<a href="'.Url::to(['/correspondence/box/view', 'id' => $transfer->id]).'">przejdź do utworzonego wpisu</a>]' );
                } else {
                    $model->status = 0;
                    $transaction->rollBack();
                    $lists = $this->lists($model->id_customer_fk, $model->departments_list);               
                    return array('success' => false, 'html' => $this->renderAjax('_transfer', [ 'model' => $transfer]), 'errors' => $transfer->getErrors() );	
                }

            } catch (Exception $e) {
                $transaction->rollBack();
                $model->status = 0;
                $transaction->rollBack();
                //return  $this->render('_transfer', [ 'model' => $transfer, 'errors' => $transfer->getErrors() ]);		
                return array('success' => false, 'html' => $this->renderAjax('_transfer', [ 'model' => $transfer]), 'errors' => $transfer->getErrors() );
            }	
		} else {
			return  $this->render('_transfer', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees]) ;	
		}	
    }

}
