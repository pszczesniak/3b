<?php

namespace app\Modules\Correspondence\controllers;

use Yii;
use backend\Modules\Correspondence\models\Correspondence;

use backend\Modules\Correspondence\models\CorrespondenceDepartment;
use backend\Modules\Correspondence\models\CorrespondenceEmployee;
use backend\Modules\Correspondence\models\CorrespondenceTask;
use backend\Modules\Correspondence\models\CorrespondenceSend;
use backend\Modules\Correspondence\models\CorrespondenceSendSearch;
use backend\Modules\Correspondence\models\CorrespondenceSendItem;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * SendController implements the CRUD actions for Correspondence model.
 */
class SendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Correspondence models.
     * @return mixed
     */
   
    public function actionIndex()
    {
        if( count(array_intersect(["correspondencePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $postingDates = CorrespondenceSend::find()->all();
        
        $searchModel = new CorrespondenceSendSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["CorrespondenceSendSearch"]["posting_date"] = date('Y-m-d');
        $queryParams["CorrespondenceSendSearch"]["id_company_branch_fk"] = $this->module->params['employeeBranch'];
        $dataProvider = $searchModel->search($queryParams);
        
        $id = false;
        if(isset($_GET['id'])) { $model = Correspondence::findOne($_GET['id']); $id = $model->id; $searchModel->name = $model->name; }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'id' => $id,
            'postingDates' => ($postingDates) ? $postingDates : []
        ]);
    }
	
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = $fieldsData = CorrespondenceSend::find();
        
        if(isset($_GET['CorrespondenceSendSearch'])) {
            $params = $_GET['CorrespondenceSendSearch'];
            if(isset($params['id']) && !empty($params['id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id = " . $params['id']);
            }
            if(isset($params['posting_date']) && !empty($params['posting_date']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(created_at, '%Y-%m-%d') = '".$params['posting_date']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$params['id_company_branch_fk']."");
            }
            if(isset($params['id_sender_fk']) && !empty($params['id_sender_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere(" (id_sender_fk = ".$params['id_sender_fk']." or id_recipient_fk = ".$params['id_sender_fk']." )  ");
            }
        } else {
            //if(isset($_GET['id'])) $fieldsDataQuery = $fieldsDataQuery->andWhere("id = ".$_GET['id']);
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(created_at, '%Y-%m-%d') = '".date('Y-m-d')."'");
            if($this->module->params['employeeBranch'])
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$this->module->params['employeeBranch']."");
        }
        
        /*if(count(array_intersect(["grantAll"], $grants)) == 0) {
            if($this->module->params['employeeKind'] >= 70)
                $fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
            else 
                $fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }   */
        $fieldsData = $fieldsDataQuery->orderby('id desc');
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
		      
        /*$actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/new/correspondence/box/view/%d" class="btn btn-sm btn-default " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"><i class="fa fa-eye"></i></a>';
        $actionColumn .= ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="/new/correspondence/box/update/%d" class="btn btn-sm btn-default  " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '';
        $actionColumn .= ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="/new/correspondence/box/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="fa fa-trash"></i></a>' : '';
        $actionColumn .= '</div>';*/
        
        /*$actionColumnSend = '<div class="edit-btn-group">';
        $actionColumnSend .= '<a href="/new/correspondence/box/view/%d" class="btn btn-sm btn-default " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"><i class="fa fa-eye"></i></a>';
        $actionColumnSend .= '</div>';*/
		foreach($fieldsData as $key=>$value) {
			
			$tmp['type'] = ($value->type_fk == 1) ? '<span class="text--blue">po dacie rejestracji</span>' : '<span class="text--teal">po dacie odbioru</span>';
           // $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['posting_date'] = $value->posting_date;
			$tmp['place'] = ($value->branch) ? $value->branch['name'] : 'brak';
            $tmp['created_by'] = $value->creator;
            $tmp['created_at'] = $value->created_at;
            $tmp['total_size'] = $value->total_size;
            $tmp['typeFormatter'] = 'send';
            //$tmp['actions'] = (!$value->send_at) ? sprintf($actionColumn, $value->id, $value->id, $value->id) : sprintf($actionColumnSend, $value->id);
			$tmp['actions'] = '<div class="edit-btn-group">'
								. '<a href="'.Url::to(['/correspondence/box/view', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"><i class="fa fa-eye"></i></a>'
							. '</div>';
           /* if($value->type_fk == 1)
                $tmp['send'] = ($value->send_at) ? '<i class="fa fa-check-circle text--green"></i>' : '<i class="fa fa-exclamation-circle text--red"></i>';
            else
                $tmp['send'] = '<i class="fa fa-check-circle text--purple"></i>';*/

            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    public function actionItems($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $html = '<table class="table-logs">';
        $html .= '<thead><tr><th>#</th><th>Status</th><th>Wysłano</th><th>Adresat</th><th>Do wiadomości</th><th>Dokumenty</th><th>Rozmiar paczki</th></tr><thead><tbody>';
        
        $sendLog = CorrespondenceSend::findOne($id);
        
        foreach($sendLog->items as $key => $item) {
            $docs = [];
            $data = \yii\helpers\Json::decode( $item->email_temp );
            foreach($data as $i => $row) {
                foreach($row['files'] as $j => $file) {
                    //if(file_exists('../../frontend/web/uploads/'.$j) ) { //echo $j.'<br />';
                        if( isset($file['size']) )
                            array_push($docs, ( ($file['size']<=40) ? ( ($file['size']<10) ? $file['title']:'<span class="text--navy">'.$file['title'].'</span>') : '<span class="text--red">'.$file['title'].'</span>').' [ '.$file['size'].' MB ]' );
                        else
                            array_push($docs,  '<span>'.$file.'</span>' );
                        //$mail->attach('../../frontend/web/uploads/'.$j,  ['fileName' => $file]);
                    //}
                }
            }
            $html .= '<tr><td>'.($key+1).'<td><i class="fa '. ( ($item->status == 1) ? 'fa-check-circle text--green' : ( ($item->status == 0) ? 'fa-exclamation text--red' : 'fa-moon-o text--navy') ) .'"></i></td><td>'.$item->send_at.'</td>'
                    .'<td>'.str_replace(',', '<br>', $item->email_to).'</td>'
                    .'<td>'.str_replace(',', '<br>', $item->email_cc).'</td>'
                    .'<td>'.implode('<br>',$docs).'</td>'
                    .'<td>'.(($item->total_size)?$item->total_size.' MB':'').'</td></tr>';
        }
        
        $html .= '</tbody></table>';
               
        return [ 'html' => $html ];
    }
    
}
