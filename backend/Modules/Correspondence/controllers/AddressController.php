<?php

namespace app\Modules\Correspondence\controllers;

use Yii;
use backend\Modules\Correspondence\models\CorrespondenceAddress;
use backend\Modules\Correspondence\models\CorrespondenceAddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * AddressController implements the CRUD actions for CorrespondenceAddress model.
 */
class AddressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CorrespondenceAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CorrespondenceAddressSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		//$queryParams["CorrespondenceAddressSearch"]["name"] = '';
        $dataProvider = $searchModel->search($queryParams);
        
        $id = false;
        if(isset($_GET['id'])) { $model = CorrespondenceAddress::findOne($_GET['id']); $id = $model->id; $searchModel->name = $model->name; }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'id' => $id
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

		/*$fieldsDataQuery = $fieldsData = CorrespondenceAddress::find()->where(['status' => 1]);*/
        $post = $_GET;
        if(isset($_GET['CorrespondenceAddressSearch'])) {
            $params = $_GET['CorrespondenceAddressSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(name) like '%".strtolower($params['name'])."%' ");
				//if(Yii::$app->params['env'] == 'dev') {
                array_push($where, " lower(fulladdress(c.id)) collate utf8_general_ci like '%".strtolower(trim($params['name']))."%' ");
                //array_push($where, " lower(name) like '%".strtolower(trim($params['name']))."%' ");
            }
            if(isset($params['address']) && !empty($params['address']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(address) like '%".strtolower($params['address'])."%' ");
				array_push($where, " lower(address) like '%".strtolower($params['address'])."%' ");
            }
            if(isset($params['email']) && !empty($params['email']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(email) like '%".strtolower($params['email'])."%' ");
				array_push($where, " lower(email) like '%".strtolower($params['email'])."%' ");
            }
			if(isset($params['phone']) && !empty($params['phone']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(email) like '%".strtolower($params['email'])."%' ");
				array_push($where, " lower(phone) like '%".strtolower($params['phone'])."%' ");
            }
			if(isset($params['id_dict_address_type_fk']) && !empty($params['id_dict_address_type_fk']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(email) like '%".strtolower($params['email'])."%' ");
				array_push($where, " id_dict_address_type_fk = ".$params['id_dict_address_type_fk']);
            }
        }   
        
		$sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query = (new \yii\db\Query())
            ->select(['id', 'fulladdress(c.id) as name', 'type_fk', 'id_dict_address_type_fk', 'address', 'email', 'phone', 'note', 'symbol'])
            ->from('{{%correspondence_address}} c')
            ->where( ['c.status' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ', $where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
			
		$fields = [];
		$tmp = [];
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/correspondence/address/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="Podgląd"><i class="fa fa-eye"></i></a>';
        $actionColumn .= ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '';
        $actionColumn .= ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="fa fa-trash" title="Usuń"></i></a>' : '';
        $actionColumn .= '</div>';
		
		$types = \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes();
		foreach($rows as $key=>$value) {
			
			$tmp['name'] = '<a href="/correspondence/address/viewajax/'.$value['id'].'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
            $tmp['name'] .= (($value['symbol']) ? '<br /><small class="text--purple">'.$value['symbol'].'</small>' : '' );
			$tmp['address'] = $value['address'];
			$tmp['email'] = $value['email'];
			$tmp['type'] = (isset($types[$value['id_dict_address_type_fk']])) ? $types[$value['id_dict_address_type_fk']] : '';
			$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    /**
     * Displays a single CorrespondenceAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewajax($id)
    {
        $model = $this->findModel($id);
        
        $grants = $this->module->params['grants'];

        return $this->renderAjax('_viewAjax', [
            'model' => $model,  'edit' => isset($grants['correspondenceEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }


    /**
     * Creates a new CorrespondenceAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CorrespondenceAddress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
        $model = new CorrespondenceAddress();
   
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
           
                   
            if($model->validate() && $model->save()) {
                                   
                $actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/correspondence/address/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="Podgląd"><i class="fa fa-eye"></i></a>';
                $actionColumn .= ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '';
                $actionColumn .= ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="fa fa-trash" title="Usuń"></i></a>' : '';
                $actionColumn .= '</div>';
                
                $data['name'] = '<a href="/correspondence/address/viewajax/'.$model->id.'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$model->name.'</a>';
                $data['sname'] = $model->name;
                $data['address'] = $model->address;
                $data['email'] = $model->email;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
                
  
                return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formAjaxNew' : '_formAjax'), [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing CorrespondenceAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
        $grants = $this->module->params['grants'];
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
			
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/correspondence/address/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="Podgląd"><i class="fa fa-eye"></i></a>';
                $actionColumn .= ( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '';
                $actionColumn .= ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="/correspondence/address/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="fa fa-trash" title="Usuń"></i></a>' : '';
                $actionColumn .= '</div>';
                
                $data['name'] = '<a href="/correspondence/address/viewajax/'.$model->id.'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$model->name.'</a>';
                $data['sname'] = $model->name;
                $data['address'] = $model->address;
                $data['email'] = $model->email;
				$data['contact'] = '<div class="btn-icon"><i class="glyphicon glyphicon-phone"></i>'.(($model->phone) ? $model->phone : 'brak danych').'</div><div class="btn-icon"><i class="glyphicon glyphicon-envelope"></i>'.(($model->email) ? $model->email : 'brak danych').'</div>';
				$data['type'] = $model->type;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Wpis <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formAjaxNew' : '_formAjax'), [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id);	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id);	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CorrespondenceAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CorrespondenceAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CorrespondenceAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionFilter() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $query = CorrespondenceAddress::find()->where(['status' => 1]);
        if(isset($_GET['ids']) && !empty($_GET['ids']))
            $query = $query->andWhere('id_dict_address_type_fk in ('.$_GET['ids'].')');
        $data = $query->orderby('name')->all();
        $addresses = ( isset($_GET['prompt']) && $_GET['prompt'] == 1 ) ? '<option value=""> -- wybierz -- </option>' : '';
        foreach($data as $key => $value) {
            $addresses .= '<option value="'.$value->id.'">'.$value->fullname.'</option>';
        }
        
        return ['addresses' => $addresses];
    }
    
    public function actionInfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = CorrespondenceAddress::findOne($id);
        
        return ['model' => $model];
    }
    
    public function actionCreateinline() {
		
		$model = new CorrespondenceAddress();
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Wartość została dodana', 'id' => $model->id, 'name' => $model->fullname );
			} else {
                $alert = "Ten słownik juz posiada taką wartość";
                foreach($model->getErrors() as $key => $value){
                    $alert = $value;
                }
                return array('success' => false, 'alert' => $alert );	
			}		
		} else {
	
			return  $this->renderAjax('_formInline', [  'model' => $model,]) ;	
		}
	}
}
