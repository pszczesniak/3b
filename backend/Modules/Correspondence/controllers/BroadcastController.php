<?php

namespace app\Modules\Correspondence\controllers;

use Yii;
use backend\Modules\Correspondence\models\CorrespondenceBroadcast;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * BroadcastController implements the CRUD actions for Correspondence model.
 */
class BroadcastController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Correspondence models.
     * @return mixed
     */
   
    public function actionIndex()  {
        if( count(array_intersect(["broadcastPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
      
        $searchModel = new CorrespondenceBroadcast();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.broadcast'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        } else {
            $queryParams["CorrespondenceBroadcast"]["date_broadcasting"] = date('Y-m-d');
            $queryParams["CorrespondenceBroadcast"]["id_company_branch_fk"] = $this->module->params['employeeBranch'];
            $searchModel->date_broadcasting = date('Y-m-d');
            $searchModel->id_company_branch_fk = $this->module->params['employeeBranch'];
        }
        
        $id = false;
        if(isset($_GET['id'])) { $model = CorrespondenceBroadcast::findOne($_GET['id']); $id = $model->id; $searchModel->name = $model->name; }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'id' => $id,
            'setting' => $setting
        ]);
    }
	
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $post = $_GET;
        $where = []; $fields = []; $tmp = []; $file = false;

        if(isset($_GET['CorrespondenceBroadcast'])) {
            $params = $_GET['CorrespondenceBroadcast'];
            \Yii::$app->session->set('search.broadcast', ['params' => $params, 'post' => $post]);
            if(isset($params['id']) && !empty($params['id']) ) {
                array_push($where, "id = " . $params['id']);
            }
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(title_file) like '%".strtolower($params['name'])."%'"); $file = true;
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_broadcasting, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_broadcasting, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "b.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                array_push($where, "b.id_company_branch_fk = ".$params['id_company_branch_fk']."");
            }
        } /*else {
            array_push($where, "DATE_FORMAT(date_broadcasting, '%Y-%m') = '".date('Y-m')."'");
        }*/
        
		/*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin']) {  }  */
	
		$sortColumn = 'b.id';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'b.type';
        if( isset($post['sort']) && $post['sort'] == 'broadcasting' ) $sortColumn = 'b.date_broadcasting';
        if( isset($post['sort']) && $post['sort'] == 'place' ) $sortColumn = 'cb.name';
		
		$query = (new \yii\db\Query())
            ->select(['b.id as id', 'b.type_fk as type_fk', 'date_broadcasting', 'cb.name as cbname'])
            ->from('{{%correspondence_broadcast}} as b')
            ->join(' JOIN', '{{%company_branch}} as cb', 'cb.id = b.id_company_branch_fk')
            ->where( ['b.status' => 1] );
	    /*if($file)
            $query = $query->join(' JOIN', '{{%files}} as f', 'b.id = f.id_fk and f.status=1 and f.id_type_file_fk = 5');
        if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin']) {
			$query = $query->join(' JOIN', '{{%correspondence_employee}} as ce', 'b.id = ce.id_correspondence_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
        $names = CorrespondenceBroadcast::getTypes();
        $rows = $query->all();
		foreach($rows as $key=>$value) {
            $tmp['name'] = $names[$value['type_fk']];
			$tmp['broadcasting'] = date('Y-m-d', strtotime($value['date_broadcasting']));
			$tmp['place'] = ($value['cbname']);
			$tmp['actions'] = '<div class="edit-btn-group">'
								. '<a href="'.Url::to(['/correspondence/broadcast/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-broadcast" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"><i class="fa fa-eye"></i></a>'
								.  ( count(array_intersect(["broadcastManage", "grantAll"], $grants)) ? '<a href="'.Url::to(['/correspondence/broadcast/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default  " data-table="#table-broadcast" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>' : '')
								.  ( count(array_intersect(["broadcastManage", "grantAll"], $grants)) ?'<a href="'.Url::to(['/correspondence/broadcast/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-broadcast"><i class="fa fa-trash"></i></a>' : '')
							. '</div>';
           
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single Correspondence model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        if( count(array_intersect(["broadcastPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        $grants = $this->module->params['grants'];
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        /*if(!$this->module->params['isAdmin'] && count(array_intersect(["correspondencePanel", "grantAll"], $this->module->params['grants'])) != 0){
			throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego wpisu.');
        }*/
               
        return $this->render('view', [
            'model' => $model, 'grants' => $grants
        ]);
    }

    /**
     * Creates a new Correspondence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        if( count(array_intersect(["broadcastManage", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
		$grants = $this->module->params['grants']; $departments = []; $employees = []; $ids = [0 => 0];
        
		if ( CustomHelpers::decode($id) == 0 ) {
            $model = new CorrespondenceBroadcast();
            $model->type_fk = 1;
            $model->date_broadcasting = date("Y-m-d"); 
            $model->status = 0;
            $model->id_company_branch_fk = $this->module->params['employeeBranch'];
            $model->save();
        } else {
			$model = $this->findModel($id);
        }     
        $model->user_action = 'create';
 
		if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            $model->status = 1;
          
			if($model->validate() && $model->save()) {       
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Wpis do książki nadawczej został zarejestrowany')  );
				return $this->redirect(['view', 'id' => $model->id]);	
			} else {
				$model->status = 0;
				return  $this->render('create', [ 'model' => $model]);		
			}
		} else {
			return  $this->render('create', [ 'model' => $model]);	
		}
	}

    /**
     * Updates an existing Correspondence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	    if( count(array_intersect(["broadcastManage", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
        $model = $this->findModel($id);
        $model->user_action = 'update';
        $grants = $this->module->params['grants'];
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
		
        if(!$this->module->params['isAdmin'] && count(array_intersect(["broadcastManage", "grantAll"], $this->module->params['grants'])) != 0){
            /*$checked = \backend\Modules\Correspondence\models\CorrespondenceEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_correspondence_fk' => $model->id])->count();
		
            if($checked == 0) {*/
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego wpisu.');
            //}
        }

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            
			$model->load(Yii::$app->request->post());
           
            if($model->status == 0) $model->status = 1;
			if($model->validate() && $model->save()) {
              
				return array('success' => true,  'alert' => 'Wpis <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model]) ;	
		}
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        if($this->findModel($id)->delete()) {
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Wpis został usunięty')  );	
        } else {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Wpis nie może zostać usunięty')  );
        }
        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-broadcast');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunięte', 'id' => $id, 'table' => '#table-broadcast');	
        }
    }

    /**
     * Finds the Correspondence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Correspondence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CorrespondenceBroadcast::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
