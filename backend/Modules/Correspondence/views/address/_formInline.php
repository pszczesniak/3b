<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'saveInlineFormSide',
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'saveInlineForm', 'data-target' => "#modal-grid-item", 'data-input' => '.matter-instance'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
	<div class="grid">
        <div class="col-sm-4 col-xs-12">
        <?= $form->field($model, 'id_dict_address_type_fk', ['template' => '
                          {label}
                           <div class="input-group ">
                                {input}
                                <span class="input-group-addon bg-green">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/10' , 
                                        ['class' => 'insertInline text--white', 
                                         'data-target' => "#address-type-insert", 
                                         'data-input' => ".address-type"
                                        ])
                                .'</span>
                           </div>
                           {error}{hint}
                       '])->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['class' => 'form-control address-type'] ) ?>
            <div id="address-type-insert" class="insert-inline bg-purple2 none"> </div>
        </div>
        <div class="col-sm-8 col-xs-8"><?= $form->field($model, 'id_parent_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '-wybierz', 'class' => 'form-control select2'] ) ?> </div>
        <div class="col-sm-4 col-xs-4"><?= $form->field($model, 'symbol')->textInput(  ) ?></div>
        <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'name')->textInput(  ) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(  ) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(  ) ?></div>
        <div class="col-md-2 col-sm-4 col-xs-4"><?= $form->field($model, 'postalcode')->textInput(  ) ?></div>
        <div class="col-md-4 col-sm-8 col-xs-8"><?= $form->field($model, 'city')->textInput(  ) ?></div>
        <div class="col-md-6 col-xs-12 col-xs-12"><?= $form->field($model, 'address')->textInput(  ) ?></div>
    </div>
	<div class="text--red" id="errorValueHint"></div>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<button class="btn btn-sm btn-default closeInlineForm" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">

	document.getElementById("saveInlineFormSide").onsubmit = function() {
		var data = new FormData(this);
		var http = new XMLHttpRequest();
		http.open('POST', '<?= Url::to(['/crm/side/createinline']) ?>',true);
 
		http.onreadystatechange=function()  {
			if (http.readyState==4 && http.status == 200) {
				var result = JSON.parse(http.responseText);
				if(result.success) { 
					document.getElementById('side-insert').classList.add('none');
					sideSelect = document.getElementById('caseside-id_side_fk');
					sideSelect.innerHTML += "<option value='"+result.id+"'>"+result.name+"</option>";
					sideSelect.value = result.id;
					//alert("Form Submitted");
                } else {
					document.getElementById("errorValueHint").innerHTML = result.alert;
				}
			} else {
				document.getElementById("errorValueHint").innerHTML = 'Wystąpił błąd.';
			}
		};
 
		http.send(data); 
		return false;
	}    

</script>


