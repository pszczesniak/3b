<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => "#table-data", 'data-input' => '.correspondence-address'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="grid">
			<div class="col-sm-4 col-xs-12">
            <?= $form->field($model, 'id_dict_address_type_fk', ['template' => '
                              {label}
                               <div class="input-group ">
                                    {input}
                                    <span class="input-group-addon bg-green">'.
                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/10' , 
                                            ['class' => 'insertInline text--white', 
                                             'data-target' => "#address-type-insert", 
                                             'data-input' => ".address-type"
                                            ])
                                    .'</span>
                               </div>
                               {error}{hint}
                           '])->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['class' => 'form-control address-type'] ) ?>
                <div id="address-type-insert" class="insert-inline bg-purple2 none"> </div>
            </div>
            <div class="col-sm-8 col-xs-8"><?= $form->field($model, 'id_parent_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '-wybierz', 'class' => 'form-control select2'] ) ?> </div>
			<div class="col-sm-4 col-xs-4"><?= $form->field($model, 'symbol')->textInput(  ) ?></div>
            <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'name')->textInput(  ) ?></div>
			<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(  ) ?></div>
			<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(  ) ?></div>
            <div class="col-md-2 col-sm-4 col-xs-4"><?= $form->field($model, 'postalcode')->textInput(  ) ?></div>
            <div class="col-md-4 col-sm-8 col-xs-8"><?= $form->field($model, 'city')->textInput(  ) ?></div>
			<div class="col-md-6 col-xs-12 col-xs-12"><?= $form->field($model, 'address')->textInput(  ) ?></div>
		</div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
	document.getElementById('correspondenceaddress-id_parent_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/correspondence/address/info']) ?>/"+((this.value)?this.value:0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				if(result.model) {
                    document.getElementById('correspondenceaddress-address').value = result.model.address;
                    document.getElementById('correspondenceaddress-phone').value = result.model.phone;
                    document.getElementById('correspondenceaddress-email').value = result.model.email;
                    document.getElementById('correspondenceaddress-city').value = result.model.city;
                    document.getElementById('correspondenceaddress-postalcode').value = result.model.postalcode;
                }
            }
        }
       xhr.send();
       
       return false;
    }
</script>