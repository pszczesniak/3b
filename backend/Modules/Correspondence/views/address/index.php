<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Książka adresów');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'address-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 
	<div id="toolbar-data" class="btn-group toolbar-table-widget">
		<?= ( ( count(array_intersect(["correspondenceAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/correspondence/address/createajax']) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ]) : '' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-data"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-box-address-search"
                data-method="get"
                data-url=<?= Url::to(['/correspondence/address/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
					<th data-field="name"  data-sortable="true" data-events="actionEvents" data-sort-name="sname">Nazwa</th>
					<?php if(Yii::$app->params['env'] == 'dev') { ?>
					<th data-field="type"  data-sortable="true">Typ</th>
					<th data-field="contact"  data-sortable="true">Kontakt</th>    
					<?php } else { ?>                  
                    <th data-field="email"  data-sortable="true">Email</th>
					<?php } ?>                    
                    <th data-field="address"  data-sortable="true">Adres</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
       
    </div>

	<!-- Render modal form -->
	<div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-item" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-pencil"></i>Edytuj</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>

<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Update').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-file', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';

		yii\bootstrap\Modal::end();
	?>

<?php if($id) { ?>
<script type="text/javascript">
document.onreadystatechange = function(){
    if (document.readyState === 'complete') {
        $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-eye-open"></i>Podgląd');
	    $("#modal-grid-item").modal("show")
				  .find(".modalContent")
				  .load("<?= Url::to(['/correspondence/box/viewajax', 'id' => $id]) ?>");
    }
};
</script>
<?php } ?>

