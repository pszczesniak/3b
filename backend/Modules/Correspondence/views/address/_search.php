<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-box-address-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-data']
    ]); ?>
    
    <div class="grid">
		<?php if(Yii::$app->params['env'] == 'dev') { ?>
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'id_dict_address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-data', 'data-form' => '#filter-box-address-search'] ) ?></div>
		<?php } ?>
		<div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-data', 'data-form' => '#filter-box-address-search' ]) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'address')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-data', 'data-form' => '#filter-box-address-search' ]) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'email')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-data', 'data-form' => '#filter-box-address-search' ]) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
	document.onreadystatechange = function(){
		if (document.readyState === 'complete') {
			$(function () {
                $('#datetimepicker_delivery_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_delivery_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o

                    });
                });
  
			});
		}
	};
   
	
</script>
