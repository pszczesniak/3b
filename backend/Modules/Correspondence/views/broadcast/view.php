<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\files\FilesBlock;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = '#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka nadawcza'), 'url' => Url::to(['/correspondence/broadcast/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/correspondence/broadcast/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["correspondenceAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/correspondence/broadcast/create', 'id' => 0]) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowy wpis</a>
		    <?php } ?>
            
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'broadcast-view', 'title'=>Html::encode($this->title))) ?>
    
    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading <?= ($model->type_fk == 1) ? ' bg-blue' : 'bg-orange' ?>">
                    <strong><?= \backend\Modules\Correspondence\models\CorrespondenceBroadcast::getTypes()[$model->type_fk] ?></strong>
                    <?php if( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 ) { ?> <a href="<?= Url::to(['/correspondence/broadcast/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-flat pull-right" style="margin-top:-8px"><span class="fa fa-pencil"></span> Edytuj</a><?php } ?>
                </div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p><span class="bold"> Miejsce </span>: <?= ($model->branch) ? $model->branch['name'] : 'brak danych' ?> </p>
                        </div>
                        
                        <div class="row-details">
                            <p>
                                <span class="bold">Notatka</span>:     
                                <span id="meeting-note"><?= $model->note ?></span>
                           </p>
                        </div>
                        
                    </div>
                </div><!--/project-body-->
            </section>
            
            
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            
            <div class="task-body-content">
                <div class="task-content-top">
                    <div class="task-meta clearfix">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="task-assignee">
                                    <div>
                                        <div class="task-recipient">
                                            <span><span>Utworzono: </span> <?= $model->creator ?> [<?= $model->created_at ?>]</span><br />
                                            <span><span>Aktualizowano: </span> <?= $model->updating ?> [<?= $model->updated_at ?>]</span><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="task-date">
                                    <div class="task-date">
                                        <div class="task-due-date">
                                            <i class="fa fa-calendar-plus-o"></i>Data rejestracji: <?= $model->created_at ?>
                                        </div>
                                        <div class="task-complete-date text--blue">
                                            <i class="fa fa-calendar"></i>Data dokumentu : <?= date('Y-m-d', strtotime($model->date_broadcasting)) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 8, 'parentId' => $model->id, 'onlyShow' => true]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>

