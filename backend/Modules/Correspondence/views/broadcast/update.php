<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\Correspondence */

$this->title = '#'.$model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka nadawcza'), 'url' => Url::to(['/correspondence/broadcast/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => '#'.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-update', 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model, 
    ]) ?>

<?php $this->endContent(); ?>
