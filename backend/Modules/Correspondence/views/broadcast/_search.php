<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use \backend\Modules\Correspondence\models\CorrespondenceBroadcast;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-broadcast-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-broadcast']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( CorrespondenceBroadcast::getTypes(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-broadcast', 'data-form' => '#filter-broadcast-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-correspondencebroadcast-date_from">
				<label for="correspondencebroadcast-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="correspondencebroadcast-date_from" name="CorrespondenceBroadcast[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>		
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-correspondencebroadcast-date_to">
				<label for="correspondencebroadcast-date_to" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="correspondencebroadcast-date_to" name="CorrespondenceBroadcast[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>		
		</div>
		<div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list ', 'data-table' => '#table-broadcast', 'data-form' => '#filter-broadcast-search' ] ) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
	document.onreadystatechange = function(){
		if (document.readyState === 'complete') {
			$(function () {
                $('#datetimepicker_delivery_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_delivery_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o

                    });
                });
  
			});
		}
	};
   
	
</script>
