<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\Correspondence */

$this->title = Yii::t('app', 'Nowy wpis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka korespondencji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-create', 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', ['model' => $model]) ?>

<?php $this->endContent(); ?>
