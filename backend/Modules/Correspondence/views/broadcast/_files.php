<?php
    use frontend\widgets\files\FilesBlock;
?>

<?php if(!$model->isNewRecord) { ?>
    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 8, 'parentId' => $model->id, 'onlyShow' => false]) ?>

<?php } else {
        echo '<div class="alert alert-warning">Aby dodać pliki musisz utworzyć wpis.</div>';
    } 
?>