<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/correspondence/box/export']),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxFormExport', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data-export"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
       <div class="grid">
            <div class='col-sm-6'>
                <div class="form-group">
                    <label for="correspondence-date_from" class="control-label">od</label>
                    <div class='input-group date' id='datetimepicker_date_from'>
                        <input type='text' class="form-control" id="correspondence-date_from" name="Correspondence[date_from]" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker_date_from').datetimepicker({ format: 'YYYY-MM-DD' });
                });
            </script>
            <div class='col-sm-6'>
                <div class="form-group">
                    <label for="correspondence-date_to" class="control-label">do</label>
                    <div class='input-group date' id='datetimepicker_date_to'>
                        <input type='text' class="form-control" id="correspondence-date_to" name="Correspondence[date_to]" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker_date_to').datetimepicker({ format: 'YYYY-MM-DD' });
                });
            </script>
        </div>
    </div>
            
		
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('Eksportuj', ['class' =>'btn btn-default']) ?>
        <?php
           /* Html::a('Eksportuj', Url::to(['/correspondence/box/export']) , 
                        ['class' => 'btn btn-default btn-export-form', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ])*/
        ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

