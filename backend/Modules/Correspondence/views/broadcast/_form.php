<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\FilesWidget;

/* @var $this yii\web\View */
/* @var $model app\Modules\Correspondence\models\CorrespondenceBroadcast */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form">
    <?php $form = ActiveForm::begin(['action' => ($model->status == 0) ? Url::to(['/correspondence/broadcast/create', 'id' => $model->id]) : Url::to(['/correspondence/broadcast/update', 'id' => $model->id]),
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => ($model->status != 0) ? 'ajaxform' : '',  'data-table' => "#table-data"],
        'fieldConfig' => [ ],
    ]); ?>
        <?= ($model->status == 0 && $model->getErrors()) ? Html::decode('<div class="alert alert-danger">'.$form->errorSummary($model).'</div>') : ''; ?>
        
        <div class="grid">
            <div class="col-md-6 col-sm-6">
                <fieldset><legend>Podstawowe</legend>
					<?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceBroadcast::getTypes(), [/*'prompt' => '- wybierz -'*/] ) ?>

                    <div class="grid">
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <label for="correspondence-broadcasting" class="control-label">Data </label>
                                <div class='input-group date clsDatePicker' id='datetimepicker_delivery'>
                                    <input type='text' class="form-control" id="correspondence-broadcasting" name="CorrespondenceBroadcast[date_broadcasting]" value="<?= $model->date_broadcasting ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?>
                </fieldset>
                <fieldset><legend>Notatka</legend><?= $form->field($model, 'note')->textArea( )->label(false) ?> </fieldset>
            </div>
            <div class="col-md-6 col-sm-6">               
                <fieldset><legend>Dokument</legend>
                    <?= $this->render('_files', ['model' => $model, 'type' => 5, 'onlyShow' => false]) ?>
                </fieldset>                
            </div>          
        </div>

        <div class="form-group align-right">
            <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
            <?= Html::submitButton(($model->status == 0) ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
