<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Książka nadawcza');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 <?= Alert::widget() ?> 
	<div id="toolbar-broadcast" class="btn-group toolbar-table-widget">
        <?= ( ( count(array_intersect(["broadcastManage", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/correspondence/broadcast/create', 'id' => 0]) , 
                    ['class' => 'btn btn-success btn-icon', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) : '' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-broadcast"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table emails">
        <table  class="table table-striped table-hover table-curved email-table header-fixed table-widget"  id="table-broadcast"
                data-toolbar="#toolbar-broadcast" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-broadcast-search"
                data-method="get"
                data-sort-order="desc"
                data-url=<?= Url::to(['/correspondence/broadcast/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true" >Nazwa</th>
                    <th data-field="place"  data-sortable="true"  >Oddział</th>
                    <th data-field="broadcasting"  data-sortable="true" data-align="center">Data</th> 
                    <th data-field="actions" data-events="actionEvents" data-align="right"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	
</div>
<?php $this->endContent(); ?>



<?php if($id) { ?>
<script type="text/javascript">
document.onreadystatechange = function(){
    if (document.readyState === 'complete') {
        $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-eye-open"></i>Podgląd');
	    $("#modal-grid-item").modal("show")
				  .find(".modalContent")
				  .load("<?= Url::to(['/task/case/viewajax', 'id' => 103]) ?>");
    }
};
</script>
<?php } ?>

