<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'form-data-search'
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( [1 => 'po dacie rejestracji', 2 => 'po dacie odbioru'], ['prompt' => ' - wybierz -', 'class' => 'form-control data-search-list' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control data-search-list select2' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-correspondencesearch-date_delivery">
				<label for="correspondencesearch-date_delivery" class="control-label">Data wysyłki</label>
				<div class='input-group date' id='datetimepicker_delivery_search'>
					<input type='text' class="form-control data-search-text" id="correspondencesendsearch-posting_date" name="CorrespondenceSendSearch[posting_date]" value="<?= $model->posting_date ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
	document.onreadystatechange = function(){
		if (document.readyState === 'complete') {
			$(function () {
                $('#datetimepicker_delivery_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_delivery_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o

                    });
                });
  
			});
		}
	};
   
	
</script>
