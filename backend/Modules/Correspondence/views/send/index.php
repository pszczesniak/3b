<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Żądania wysyłki korespondencji');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 <?= Alert::widget() ?> 
	<div id="toolbar-data" class="btn-group">
       
        <button class="btn btn-default" type="button" id="btn-refresh" title="Odśwież"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table emails">
        <table  class="table table-striped table-hover table-curved email-table header-fixed"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-detail-view="true"
                data-detail-formatter="boxFormatter"
                data-method="get"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>" 
                data-url=<?= ($id) ? Url::to(['/correspondence/box/data', 'id' => $id]) : Url::to(['/correspondence/send/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="typeFormatter" data-visible="false">formatter</th>
                    <th data-field="place"  data-sortable="true"  >Miejsce</th>
                    <th data-field="type"  data-sortable="true" >Tryb</th>
                    <th data-field="total_size"  data-sortable="true" >Rozmiar [MB]</th>
                    <th data-field="posting_date"  data-sortable="true" >Poczta z dnia</th>
                    <th data-field="created_at"  data-sortable="true" >Data</th>
                    <th data-field="created_by"  data-sortable="true" >Wysyłający</th>
                    <!--<th data-field="state"  data-sortable="false" data-align="right"></th> 
                    <th data-field="actions" data-events="actionEvents" data-align="right"></th>-->
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	
</div>
<?php $this->endContent(); ?>



