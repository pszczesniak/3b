<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form">
    <?php $form = ActiveForm::begin(['action' => ($model->status == 0) ? Url::to(['/correspondence/box/create', 'id' => $model->id]) : Url::to(['/correspondence/box/update', 'id' => $model->id]),
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => ($model->status != 0) ? 'ajaxform' : '',  'data-table' => "#table-data"],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
        ],
    ]); ?>
        <?= ($model->status == 0 && $model->getErrors()) ? Html::decode('<div class="alert alert-danger">'.$form->errorSummary($model).'</div>') : ''; ?>
        
        <div class="grid">
            <div class="col-md-6 col-sm-6">
                <fieldset><legend>Podstawowe</legend>
                    <?= $form->field($model, 'type_fk')->radioList([1 => 'Przychodząca', 2 => 'Wychodząca'], 
                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                        },
                                ]) ?>
                    <div class="grid">
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <label for="correspondence-date_from" class="control-label">Data rejestracji</label>
                                <div class='input-group date clsDatePicker' id='datetimepicker_delivery'>
                                    <input type='text' class="form-control" id="correspondence-date_from" name="Correspondence[date_delivery]" value="<?= $model->date_delivery ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
     
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <label for="correspondence-date_to" class="control-label">Data <span id="label-in" class="label-date <?= ($model->type_fk==1) ? '' : ' none' ?>">odbioru</span><span id="label-out" class="label-date  <?= ($model->type_fk==2) ? '' : ' none' ?>">nadania</span></label>
                                <div class='input-group date' id='datetimepicker_posting'>
                                    <input type='text' class="form-control" id="correspondence-date_to" name="Correspondence[date_posting]" value="<?= $model->date_posting ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
          
                    </div>
                    
                    <div id="sender-block"  <?= ($model->type_fk==1) ? '' : ' class="none"' ?>>
						<?php /*$form->field($model, 'id_sender_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -'] )*/  ?>
						<?= $form->field($model, 'id_sender_fk', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax']) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'sender-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-address",
											 'data-title' => "Dodaj"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
					</div>
                    <div id="recipient-block" <?= ($model->type_fk==2) ? '' : ' class="none"' ?>>
						<?php /*$form->field($model, 'id_recipient_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -'] ) */ ?>
					<?= $form->field($model, 'id_recipient_fk', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'recipient-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-address",
											 'data-title' => "Dodaj"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
					</div>
          
                    <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?>
                    
                    <?= $form->field($model, 'id_customer_fk', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/customer/createajax']) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'customer-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-customer",
											 'data-title' => "Nowy klient"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-customer select2'] ) 
                    ?> 
                    
                    <?php /*$form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['class' => 'form-control select2', 'prompt' => '- wybierz -'] )*/ ?> 
                    
                    <?= $form->field($model, 'id_case_fk', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/task/matter/createajax', 'id' => 0]) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'case-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form",
                                             'data-customer' => '#correspondence-id_customer_fk', 
											 'data-input' => ".correspondence-case",
											 'data-title' => "Nowa sprawa/projekt"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList( ($model->id_customer_fk) ? $model->id_customer_fk : 0 ), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control  select2'] ) 
                    ?> 

                    <?php /*$form->field($model, 'id_case_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList( ($model->id_customer_fk) ? $model->id_customer_fk : 0 ), 'id', 'name'), ['class' => 'form-control select2'] )->label('Sprawa/Projekt')*/ ?> 
                    <?php 
						//if($model->status == 1) {
							echo $form->field($model, 'events_list', ['template' => '
                                  {label}
                                   <div class="input-group ">
                                        {input}
                                        <span class="input-group-addon bg-green">'.
                                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/task/event/new', 'id' => 0]) , 
                                                ['class' => 'gridViewModal text--white', 
                                                    'data-target' => "#modal-grid-item", 
                                                    'data-form' => "event-form", 
                                                    'data-table' => "table-events",
                                                    'data-input' => ".correspondence-event",
                                                    'data-case' => '#correspondence-id_case_fk',
                                                    'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                                                ])
                                  
                                        .'</span>
                                   </div>
                                   {error}{hint}
                               '])->dropDownList( \backend\Modules\Task\models\CalTask::getGroups( ($model->id_case_fk) ? $model->id_case_fk : 0 ), [/*'prompt' => '- wybierz -',*/ "multiple" => true, 'class' => 'form-control correspondence-event ms-select', 'data-type' => 'matter','data-box' => 1,] )->label('Zdarzenia');
						//}
					?>
                    
                </fieldset>
                <fieldset><legend>Opis</legend><?= $form->field($model, 'note')->textArea( )->label(false) ?> </fieldset>
            </div>
            <div class="col-md-6 col-sm-6">               
                <fieldset><legend>Dokument</legend>
                    <?= $this->render('_files', ['model' => $model, 'type' => 5, 'onlyShow' => false]) ?>
                </fieldset>                
                <fieldset><legend>Personalizacja</legend>
                    <label>Działy</label>
                    <?= $form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'matter', 'data-box' => 1, 'multiple' => 'multiple', ] ); ?>
                    <?= $this->render('_employeesList', ['employees' => $lists['employees'], 'employees_ch' => $employees]) ?>
                </fieldset>
            </div>
           
        </div>

        <div class="form-group align-right">
            <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
            <?= Html::submitButton(($model->status == 0) ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
	
	document.getElementById('correspondence-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/lists']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('correspondence-id_case_fk').innerHTML = result.cases; 
                /*document.getElementById('correspondence-departments_list').innerHTML = result.departments; 
                $('#correspondence-departments_list').multiselect('rebuild');
                $('#correspondence-departments_list').multiselect('refresh');
                document.getElementById('menu-list-employee').innerHTML = result.employees; */
            }
        }
       xhr.send();
       
       return false;
    }
    document.getElementById('correspondence-id_case_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        var caseId = 0;
        if(this.value) caseId = this.value;
        xhr.open('POST', "<?= Url::to(['/task/default/events']) ?>/"+caseId+'?without=1&box=1', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('correspondence-events_list').innerHTML = result.events;
                $('#correspondence-events_list').multiselect('rebuild');
                $('#correspondence-events_list').multiselect('refresh');
                //console.log(result.departments);
                //document.getElementById('correspondence-departments_list').value = new Array("4","12","6");
                var x = document.getElementById('correspondence-departments_list');
                for (i = 0; i < x.length; i++) {
                    //console.log(x.options[i].value+ ' - '+result.departments.indexOf(x.options[i].value));
                    if(result.departments.indexOf(x.options[i].value*1) >= 0) {
                        x.options[i].selected = true;
                    } else {
                        x.options[i].selected = false;
                    }
                }
                $('#correspondence-departments_list').multiselect('rebuild');
                $('#correspondence-departments_list').multiselect('refresh');
                document.getElementById('menu-list-employee').innerHTML = result.employees;
            }
        }
       xhr.send();
       
       return false;
    }
	
	document.getElementById('correspondence-type_fk').onchange = function(event) {
        //if(event.target.value == 1) {
            document.getElementById('sender-block').classList.toggle("none");
            document.getElementById('recipient-block').classList.toggle("none");
            document.getElementById('label-in').classList.toggle("none");
            document.getElementById('label-out').classList.toggle("none");
        //}
    }
    
	if(document.getElementById('correspondence-createtask') != null) {
		document.getElementById('correspondence-createtask').onchange = function(event) {
			document.getElementById('new-task').classList.toggle("none"); 
			//console.log(this.checked);
			if(this.checked == true) {
				document.getElementById('correspondence-id_event_fk').setAttribute('disabled',true);
			} else {
				document.getElementById('correspondence-id_event_fk').removeAttribute("disabled");
			}
		}
    }
</script>