<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\Correspondence */

$this->title = '#'.$model->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka korespondencji'), 'url' => Url::to(['/correspondence/box/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => '#'.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-update', 'title'=>Html::encode($this->title))) ?>

    <?php if($model->send_at && $model->type_fk == 1) { ?>
        <div class="alert alert-warning">Korespondencja została ju wysłana i nie może zostać zmodyfikowana</div>
    <?php } else { ?>
    <?= $this->render('_form', [
        'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees
    ]) ?>
    <?php } ?>

<?php $this->endContent(); ?>
