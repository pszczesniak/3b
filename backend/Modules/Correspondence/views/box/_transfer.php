<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\FilesWidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form">
    <?php $form = ActiveForm::begin(['action' => Url::to(['/correspondence/box/transfer', 'id' => $model->id]),
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => ($model->status != 0) ? 'ajaxform' : '',  'data-table' => "#table-data"],
        'fieldConfig' => [ ],
    ]); ?>
        <?= ($model->status == 0 && $model->getErrors()) ? Html::decode('<div class="alert alert-danger">'.$form->errorSummary($model).'</div>') : ''; ?>
            <?php
				echo $form->field($model, 'docs_list')->dropDownList( ArrayHelper::map($model->files, 'id', 'title_file'), ["multiple" => true, 'class' => 'form-control correspondence-docs ms-options', 'data-type' => 'matter','data-box' => 1, 'data-placeholder' => 'zaznacz dokumenty'] )->label('Dokumenty');
			?>
			
			<div class="grid">
				<div class='col-sm-4 col-xs-6'>
					<div class="form-group">
						<label for="correspondence-date_to" class="control-label">Data <span id="label-in" class="label-date <?= ($model->type_fk==1) ? '' : ' none' ?>">odbioru</span><span id="label-out" class="label-date  <?= ($model->type_fk==2) ? '' : ' none' ?>">nadania</span></label>
						<div class='input-group date' id='datetimepicker_posting'>
							<input type='text' class="form-control" id="correspondence-date_to" name="Correspondence[date_posting]" value="<?= $model->date_posting ?>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				
				<div class='col-sm-8 col-xs-6'>
					<?= $form->field($model, 'id_sender_fk', ['template' => '
					  {label}
					   <div class="input-group ">
							{input}
							<span class="input-group-addon bg-green">'.
								Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax']) , 
									['class' => 'gridViewModal text--white', 
									 'id' => 'sender-create',
									 //'data-toggle' => ($gridViewModal)?"modal":"none", 
									 'data-target' => "#modal-grid-item", 
									 'data-form' => "item-form", 
									 'data-input' => ".correspondence-address",
									 'data-title' => "Dodaj"
									])
							.'</span>
					   </div>
					   {error}{hint}
				   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
				</div>
			</div>
			<div class="grid">
				<div class='col-sm-6 col-xs-12'>           
					<?= $form->field($model, 'id_customer_fk', ['template' => '
							  {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/customer/createajax']) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'customer-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-customer",
											 'data-title' => "Nowy klient"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-customer select2'] ) 
					?> 
				</div>
				<div class='col-sm-6 col-xs-12'>           
					<?= $form->field($model, 'cases_list', ['template' => '
							  {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/task/matter/createajax', 'id' => 0]) , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'case-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form",
											 'data-customer' => '#correspondence-id_customer_fk', 
											 'data-input' => ".correspondence-case",
											 'data-title' => "Nowe postępowanie"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList( ($model->id_customer_fk) ? $model->id_customer_fk : 0 ), 'id', 'name'), ["multiple" => true, 'class' => 'form-control correspondence-case ms-options', 'data-type' => 'matter','data-box' => 1] ) 
					?> 
				</div>
				<div class='col-sm-12 col-xs-12'>           
					<?php 
						echo $form->field($model, 'events_list', ['template' => '
							  {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/task/event/generate']) , 
											['class' => 'gridViewModal text--white', 
												'data-target' => "#modal-grid-item", 
												'data-form' => "event-form", 
												'data-table' => "table-events",
												'data-input' => ".correspondence-event",
												'data-case' => '#correspondence-cases_list',
												'data-title' => "<i class='glyphicon glyphicon-plus'></i>Generator zdarzeń"
											])
							  
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList( \backend\Modules\Task\models\CalTask::getForbox( ($model->cases_list) ? $model->cases_list : [0] ), [/*'prompt' => '- wybierz -',*/ "multiple" => true, 'class' => 'form-control correspondence-event ms-options', 'data-type' => 'matter','data-box' => 1,] )->label('Zdarzenia');
					?>
				</div>
			</div>
			<fieldset><legend>Opis</legend><?= $form->field($model, 'note')->textArea( )->label(false) ?> </fieldset>
		</div>

        <div class="form-group align-right">
            <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
            <?= Html::submitButton('Przenieś dokumenty', ['class' => 'btn bg-purple']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
	
	document.getElementById('correspondence-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/mlists']) ?>/"+( (this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('correspondence-cases_list').innerHTML = result.cases; 
                $('#correspondence-cases_list').multiselect('rebuild');
                $('#correspondence-cases_list').multiselect('refresh');
            }
        }
       xhr.send();
       
       return false;
    }
    document.getElementById('correspondence-cases_list').onchange = function() {
        var xhr = new XMLHttpRequest();
        var caseId = 0;
        //if(this.value) caseId = this.value;
        var fids = document.getElementById('correspondence-cases_list');
        var ids = [];
        for (var i = 0; i < fids.options.length; i++) {
            if (fids.options[i].selected) {
                ids.push(fids.options[i].value);
            }
        }
        xhr.open('POST', "<?= Url::to(['/task/default/mevents']) ?>?ids="+ids+'&without=1&box=1', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('correspondence-events_list').innerHTML = result.events;
                $('#correspondence-events_list').multiselect('rebuild');
                $('#correspondence-events_list').multiselect('refresh');
            }
        }
       xhr.send();
       
       return false;
    }
	
	if(document.getElementById('correspondence-createtask') != null) {
		document.getElementById('correspondence-createtask').onchange = function(event) {
			document.getElementById('new-task').classList.toggle("none"); 
			//console.log(this.checked);
			if(this.checked == true) {
				document.getElementById('correspondence-id_event_fk').setAttribute('disabled',true);
			} else {
				document.getElementById('correspondence-id_event_fk').removeAttribute("disabled");
			}
		}
    }
</script>