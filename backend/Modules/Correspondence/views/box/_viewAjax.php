<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

    <div class="modal-body">
        <h3>Korespondencja <?= ($model->type_fk == 1) ? 'przychodząca' : 'wychodząca' ?></h3>
        <?php 
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [                      
                        'attribute' => 'date_posting',
                        'format'=>'raw',
                        'value' => date('Y-m-s', strtotime($model->date_posting))
                    ], 
                    [                      
                        'attribute' => 'date_delivery',
                        'format'=>'raw',
						'value' => date('Y-m-s', strtotime($model->date_delivery))
                    ],   
                    [                      
                        'attribute' => 'id_customer_fk',
                        'format'=>'raw',
                        'value' => $model->customer['name'],
                    ],
					[                      
                        'attribute' => 'id_case_fk',
                        'format'=>'raw',
                        'value' => $model->case['name'],
                    ],
                    'sender', 
                    [                      
                        'attribute' => 'departments_list:html',
                        'format'=>'raw',
                        'label' => 'Działy',
                        'value' => $model->departments_list,
                    ], 
                    [                      
                        'attribute' => 'files_list:html',
                        'format'=>'raw',
                        'label' => 'Dokumenty',
                        'value' =>$this->render('_files', ['model' => $model, 'type' => 3, 'onlyShow' => true])
                    ],  
                         
                ],
            ]) 
        ?>
    </div>
        
            
    <div class="modal-footer"> 
		<?php if(!$edit) { ?><a href="/correspondence/box/updateajax/<?= $model->id ?>?index=<?= $index ?>" class="btn btn-sm btn-primary viewEditModal" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class='glyphicon glyphicon-pencil'></i>Edycja">Edytuj</a> <?php } ?>
		<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>