<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Podstawowe</a> </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body" style="overflow: inherit !important;">
                        <?= $form->field($model, 'type_fk')->radioList([1 => 'Przychodząca', 2 => 'Wychodząca'], 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-info' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ]) ?>
                        <div id="sender-block"  <?= ($model->type_fk==1) ? '' : ' class="none"' ?>><?= $form->field($model, 'sender')->textInput(  ) ?></div>
                        <div id="recipient-block" <?= ($model->type_fk==2) ? '' : ' class="none"' ?>><?= $form->field($model, 'recipient')->textInput(  ) ?></div>
                                    
                        <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?>
                        
                        <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?> 

                        <?= $form->field($model, 'id_case_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList( ($model->id_customer_fk) ? $model->id_customer_fk : 0 ), 'id', 'name'), [] ) ?> 

                       <div class="row">
                            <div class='col-sm-6'>
                                <div class="form-group">
                                    <label for="correspondence-date_from" class="control-label">Data</label>
                                    <div class='input-group date clsDatePicker' id='datetimepicker_delivery'>
                                        <input type='text' class="form-control" id="correspondence-date_from" name="Correspondence[date_delivery]" value="<?= $model->date_delivery ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_delivery').datetimepicker({ format: 'YYYY-MM-DD',  });
                                });
                            </script>
                            <div class='col-sm-6'>
                                <div class="form-group">
                                    <label for="correspondence-date_to" class="control-label">Data nadania</label>
                                    <div class='input-group date' id='datetimepicker_end'>
                                        <input type='text' class="form-control" id="correspondence-date_to" name="Correspondence[date_posting]" value="<?= $model->date_posting ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
                                });
                            </script>
                        </div>
                    </div>

				</div>
			</div>
            
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">  Dokumenty</a> </h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body">
						<?= $this->render('_files', ['model' => $model, 'type' => 5, 'onlyShow' => false]) ?>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">  Działy</a> </h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="panel-body-max">
                            <?= $this->render('_departmentsList', ['departments' => $lists['departments'], 'departments_ch' => $departments]) ?>
                        </div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">  Pracownicy</a> </h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="panel-body-max">
                            <?= $this->render('_employeesList', ['employees' => $lists['employees'], 'employees_ch' => $employees]) ?>
                        </div>
					</div>
				</div>
			</div>
            <?php if($model->status == 0) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">  Zdarzenie</a> </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-body" style="overflow: inherit !important;">
                                <?= $form->field($model, 'createTask', ['template' => '{input} {label}'])->checkbox() ?>
                                <?= $this->render('_createTask', ['task' => $task, 'form' => $form]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
		</div> 
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
	
	document.getElementById('correspondence-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/lists']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('correspondence-id_case_fk').innerHTML = result.cases; 
                document.getElementById('menu-list-department').innerHTML = result.departments; 
                document.getElementById('menu-list-employee').innerHTML = result.employees; 
            }
        }
       xhr.send();
       
       return false;
    }
	
	document.getElementById('correspondence-type_fk').onchange = function(event) {
        //if(event.target.value == 1) {
            document.getElementById('sender-block').classList.toggle("none");
            document.getElementById('recipient-block').classList.toggle("none");
        //}
    }
</script>