<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/correspondence/box/send']),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'correspondenceSend', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data-export"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
       <?php
            if($branch) {
                echo '<div class="alert alert-info">Liczba wpisów dla oddziału <b>'.$branch->name.'</b> oczekująca na wysłanie: '. $counter .' [ załączniki o wadze '.$total_size.' MB ] </div>';
            } else {
                echo '<div class="alert alert-danger">Nie jesteś przypisany do żadnego działu więc nie jesteś upoważniony do rozsyłania korespondencji.</div>';
            }
        ?>
    </div>
            
		
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?php echo ($branch) ? Html::submitButton('Roześlij [oddział <b>'.$branch->name.'</b>]', ['class' =>'btn btn-default correspondenceSendBtn']) : '' ?>
        <?php
           /* Html::a('Eksportuj', Url::to(['/correspondence/box/export']) , 
                        ['class' => 'btn btn-default btn-export-form', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ])*/
        ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

