<?php
    use yii\helpers\Url;
?>
<div class="content-department-all-select custom-inputs"><input type="checkbox" class="selecctall" id="checkbox_department"/> <label for="checkbox_department">Zaznacz wszystkie działy</label></div>
<div class="content-department-select custom-inputs">
	<ul id="menu-list-department" class="department-container ">
		<?php
			$editableDepartments = [];
            foreach($departments as $key=>$value){
				//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
				//array_push($editableDepartments, $key);
                $checked = '';
                if(in_array($key, $departments_ch)) $checked = 'checked="checked"';
				echo '<li><input class="checkbox_department" id="d'.$key.'" type="checkbox" name="departments[]" value="'.$key.'" '.$checked.'><label for="d'.$key.'" >'.$value.'</label></li>';
			}
            
            /*foreach($all as $key=>$value){
				//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
                $checked = 'checked="checked"';
                if(!in_array($value->id_department_fk, $editableDepartments)) 
                    echo '<li><input class="checkbox_department" id="d'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.' disabled><label for="d'.$value->id_department_fk.'" >'.$value->department['name'].'</label></li>';
			}*/
		?>
	</ul> 
</div>
