<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-box-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-box']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-box', 'data-form' => '#filter-box-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( [1 => 'Przychodząca', 2 => 'Wychodząca'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-box', 'data-form' => '#filter-box-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-correspondencesearch-date_delivery">
				<label for="correspondencesearch-date_delivery" class="control-label">Data rejestracji</label>
				<div class='input-group date' id='datetimepicker_delivery_search'>
					<input type='text' class="form-control" id="correspondencesearch-date_delivery" name="CorrespondenceSearch[date_delivery]" value="<?= $model->date_delivery ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-correspondencesearch-date_posting">
				<label for="correspondencesearch-date_posting" class="control-label">Data odbioru/nadania</label>
				<div class='input-group date' id='datetimepicker_posting_search'>
					<input type='text' class="form-control" id="correspondencesearch-date_posting" name="CorrespondenceSearch[date_posting]" value="<?= $model->date_posting ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-box', 'data-form' => '#filter-box-search' ]) 
                                                            ->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list ', 'data-table' => '#table-box', 'data-form' => '#filter-box-search' ] ) ?></div>
        <?php if(Yii::$app->params['env'] == 'dev') { ?>
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-box', 'data-form' => '#filter-box-address-search'] )->label('Typ podmiotu') ?></div>
		<?php } ?>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_sender_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::find()->where(['status'=>1])->orderby('name')->all(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-box', 'data-form' => '#filter-box-search' ] )->label('Nadawca/Odbiorca') ?></div>
        
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
	document.onreadystatechange = function(){
		if (document.readyState === 'complete') {
			$(function () {
                $('#datetimepicker_delivery_search').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
                $('#datetimepicker_delivery_search').on("dp.change", function (e) {
                    //console.log(e.date.format('Y-MM-DD'));
                    var that = this,
                    value = $(this).val();
                    
                    var o = {};
                    //console.log($('#form-data-search').serializeArray());
                    $.each($('#form-data-search').serializeArray(), function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    
                    $('#table-data')./*bootstrapTable('refresh').*/bootstrapTable('refresh', {
                        url: $('#table-data').data("url"), method: 'get',
                        query: o

                    });
                });
  
			});
		}
	};
    <?php if(Yii::$app->params['env'] == 'dev') { ?>
     document.getElementById('correspondencesearch-address_type_fk').onchange = function() {
       var opts = [], opt;
        var sel = document.getElementById('correspondencesearch-address_type_fk');
        for (var i=0, len=sel.options.length; i<len; i++) {
            opt = sel.options[i];
            if ( opt.selected ) {
                opts.push(opt.value);
                /*if (fn) {  fn(opt);  }*/
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/correspondence/address/filter']) ?>?ids="+opts.join()+"&prompt=1", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('correspondencesearch-id_sender_fk').innerHTML = result.addresses; 
                //$('#correspondencesearch-id_sender_fk').multiselect('rebuild');
                //$('#correspondencesearch-id_sender_fk').multiselect('refresh');
            }
        }
       xhr.send();
       
       return false;
    }
    <?php } ?>
	
</script>
