<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Książka korespondencji');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-index', 'title'=>Html::encode($this->title))) ?>
    <?php if( isset($_GET['alert']) && $_GET['alert'] == 'noSend' ) { ?>
	<div class="alert alert-danger" role="alert">
		<i class="fa fa-exclamation-triangle fa-5x pull-left"></i> <br ><strong>Uwaga:</strong> System wykrył niewysłaną korespondencję przychodzącą dla Twojeo oddziału. Jeśli to celowe działanie to wyloguj się korzystać z przycisku poniżej.
		<br > <br > <br >
		<p><a href="<?= Url::to(['/site/logout']) ?>" class="btn btn-lg btn-danger">Wyloguj się</a></p>
	  </div>
	  <?php } ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
   
	<?= Alert::widget() ?> 
	<div id="toolbar-box" class="btn-group toolbar-table-widget">
        <?= ( ( count(array_intersect(["correspondenceAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/correspondence/box/create', 'id' => 0]) , 
                    ['class' => 'btn btn-success btn-icon', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) : '' ?>
        <?= ( ( count(array_intersect(["correspondenceExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/correspondence/box/exportform']) , 
                    ['class' => 'btn btn-info btn-icon gridViewModal', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Eksport danych"
                    ]) : '' ?>
        <?= ( ( count(array_intersect(["correspondenceSend", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-send"></i>Roześlij', Url::to(['/correspondence/box/sendform']) , 
                        ['class' => 'btn bg-purple btn-icon gridViewModal', 
                         'id' => 'corresppndence-send',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Rozesłanie korespondencji"
                        ]) : '' ?>
         <?php if( ( count(array_intersect(["correspondenceSend", "grantAll"], $grants)) > 0 ) && count($postingDates) >= 1 ) { ?>
             <div class="btn-group" id="postingDates-btn">
                <button type="button" class="btn bg-pink">Roześlij po dacie odbioru</button>
                <button type="button" class="btn bg-pink dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" id="postingDates-list">
                    <?php
                        foreach($postingDates as $key => $date) { 
                            $datePosting = strtotime($date['date_posting']);
                            echo '<li id="posting-'.date('Y-m-d', $datePosting).'">'
                                   . '<a  href="'.Url::to(['/correspondence/box/sendformbyposting']).'?postingDate='.$datePosting.'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-items" data-title="Rozesłanie korespondencji po dacie odbioru">'.date('Y-m-d',$datePosting).'</a>'
                                .'</li>';
                        }
                    ?>
                    <!--<li role="separator" class="divider"></li>-->
                </ul>
            </div>
         <?php } ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-box"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table emails">
        <table  class="table table-striped table-hover table-curved email-table header-fixed table-widget"  id="table-box"
                data-toolbar="#toolbar-box" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-box-search"
                data-method="get"
                data-url=<?= ($id) ? Url::to(['/correspondence/box/data', 'id' => $id]) : Url::to(['/correspondence/box/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="author"  data-sortable="true" >Nadawca / Odbiorca</th>
                    <th data-field="client"  data-sortable="true" >Klient</th>
					<th data-field="type"  data-sortable="true" data-width="25px">Typ</th>
                    <th data-field="place"  data-sortable="true"  >Miejsce</th>
                    <th data-field="delivery"  data-sortable="true" data-align="right">Data rejestracji</th> 
                    <th data-field="posting"  data-sortable="true" data-align="right">Odbiór/Nadanie</th> 
                    <th data-field="send"  data-sortable="false" data-align="right"></th> 
                    <th data-field="actions" data-events="actionEvents" data-align="right"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	
</div>
<?php $this->endContent(); ?>



<?php if($id) { ?>
<script type="text/javascript">
document.onreadystatechange = function(){
    if (document.readyState === 'complete') {
        $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-eye-open"></i>Podgląd');
	    $("#modal-grid-item").modal("show")
				  .find(".modalContent")
				  .load("<?= Url::to(['/task/case/viewajax', 'id' => 103]) ?>");
    }
};
</script>
<?php } ?>

