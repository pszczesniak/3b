<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\files\FilesBlock;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = '#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka korespondencji'), 'url' => Url::to(['/correspondence/box/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["correspondenceDelete", "grantAll"], $grants)) > 0 ) && (!$model->send_by || $model->type_fk==2) ) { ?>
				<a href="<?= Url::to(['/correspondence/box/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["grantAll"], $grants)) > 0 ) && ($model->send_by && $model->type_fk==1) ) { ?>
				<a href="<?= Url::to(['/correspondence/box/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["correspondenceAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/correspondence/box/create', 'id' => 0]) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa korespondencja</a>
		    <?php } ?>
            
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-view', 'title'=>Html::encode($this->title))) ?>

    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading <?= ($model->type_fk == 1) ? ' bg-blue' : 'bg-orange' ?>">
                    <strong><?= ($model->type_fk == 1) ? 'przychodząca' : 'wychodząca' ?></strong>
                    <?php if( count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 && (!$model->send_by || $model->type_fk==2) ) { ?> <a href="<?= Url::to(['/correspondence/box/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-flat pull-right" style="margin-top:-8px"><span class="fa fa-pencil"></span> Edytuj</a><?php } ?>
                </div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p>
                                <span class="bold"> <?= ($model->type_fk == 1) ? ' Nadawcy' : 'Odbiorcy' ?> </span>: 
                                <ol>
                                    <?php foreach($model->addresses as $key => $address) { ?>
                                        <li><?= $address->address['fullname'] ?></li>                               
                                    <?php } ?>
                                </ol>
                            </p>
                        </div>
                        
                        <div class="row-details">
                            <p><span class="bold"> Miejsce </span>: <?= ($model->branch) ? $model->branch['name'] : 'brak danych' ?> </p>
                        </div>
                        <div class="row-details">
                                <span class="bold"> Postępowania</span>: <ol>
                                <?php foreach($model->cases as $key => $case) { ?>
                                    <li><?php if(count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 && ($model->send_by && $model->type_fk==1)) { ?> <a href="<?= Url::to(['/correspondence/box/case', 'id' => $case->id]) ?>" data-target="#modal-grid-item" data-form="item-form" title="Zmień sprawę" class="btn btn-xs bg-purple gridViewModal"><i class="fa fa-pencil"></i></a><?php } ?>                                
                                    <span id="meeting-case-<?= $case->id ?>"><a href="<?= Url::to(['/task/'. ( ($case->case['type_fk'] == 1) ? 'matter' : 'project' ).'/view', 'id' => $case->id_case_fk]) ?>"><?= $case->case['name'] ?></a></span></li>
                                <?php } ?>
                                </ol>
                        </div>
                        <div class="row-details">
                            <div class="grid profile">
                                <div class="col-md-4">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file("uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="profile-name">
                                        <h5><span id="meeting-customer"><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></span></h5>
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['statusname'] ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-details">
                            <p>
                                <span class="bold">Opis</span>:     
                                <?php if(count(array_intersect(["correspondenceEdit", "grantAll"], $grants)) > 0 && ($model->send_by && $model->type_fk==1)) { ?> <a href="<?= Url::to(['/correspondence/box/note', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" title="Edytuj notatkę" class="btn btn-xs bg-purple gridViewModal"><i class="fa fa-pencil"></i></a><?php } ?>
                                <span id="meeting-note"><?= $model->note ?></span>
                           </p>
                        </div>
                        
                    </div>
                </div><!--/project-body-->
            </section>
            
            
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            
            <div class="task-body-content">
                <div class="task-content-top">
                    <div class="task-meta clearfix">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="task-assignee">
                                    <div>
                                        <!--<div class="task-thumb">
                                            <img src="/images/avatar.png" alt="user">
                                        </div>-->
                                        <div class="task-recipient">
                                            <span><span>Utworzono: </span> <?= $model->creator ?> [<?= $model->created_at ?>]</span><br />
                                            <span><span>Aktualizowano: </span> <?= $model->updating ?> [<?= $model->updated_at ?>]</span><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="task-date">
                                    <div class="task-date">
                                        <div class="task-due-date">
                                            <i class="fa fa-calendar-plus-o"></i>Data rejestracji: <?= date('Y-m-d', strtotime($model->date_delivery)) ?>
                                        </div>
                                        <div class="task-complete-date text--blue">
                                            <i class="fa fa-calendar"></i>Data <?= ($model->type_fk == 1) ? 'odebrania' : 'nadania' ?> : <?= date('Y-m-d', strtotime($model->date_posting)) ?>
                                        </div>
										<?php if( $model->type_fk == 1  && !empty($model->send_at) ) { ?>
										<div class="task-reminder-date text--orange">
                                            <i class="fa fa-send"></i>Data wysyłki: <?= date('Y-m-d', strtotime($model->send_at)) ?>
                                        </div>
										<?php } ?>
                                        <?php if( $model->id_transfer_fk) { ?>
										<div class="task-reminder-date text--purple">
                                            <i class="fa fa-exchange"></i><a href="<?= Url::to(['/correspondence/box/view', 'id' => $model->id_transfer_fk]) ?>">Transfer pliku z wysłanej poczty</a>
                                        </div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br />
                    <fieldset ><legend>Powiązane zdarzenia</legend>
                        <div class="list-group" id="datetimepicker_tasks-cases">  
                            <?php
                                foreach($model->events as $key => $item) {
                                    echo '<a href="'.Url::to(['/task/'.( ($item->task['type_fk'] == 1) ? 'case' : 'event' ).'/view', 'id' => $item->id_task_fk]).'" class="list-group-item text-ellipsis">'
                                            .'<span class="badge bg-'.( ($item->task['type_fk'] == 1) ? 'pink' : 'orange' ).'">'.( ($item->task['type_fk'] == 1) ? '<i class="fa fa-gavel"></i>' : '<i class="fa fa-tasks"></i>' ).'</span> '.$item->task['name'].' </a>';
                                }
                                if( count($model->events) == 0 )
                                    echo '<span class="list-group-item text-ellipsis"> brak powiązanych czynności </span>';
                            ?>
                        </div>
                    </fieldset>
					
                </div>
            </div>
        
            <div class="panel with-nav-tabs panel-default">
                <?php $files = $model->files; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="<?= (!isset($_GET['type'])) ? "active" : "" ?>"><a data-toggle="tab" href="#tab1"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span></a></li>
                        <li class="<?= (isset($_GET['type']) && $_GET['type'] == 'docs') ? "active" : "" ?>"><a data-toggle="tab" href="#tab2"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                        <?php if( $model->type_fk == 1 && $model->id_send_fk > 0 && count($files) > 1) { ?>
						<li><a data-toggle="tab" href="#tab3"><i class="fa fa-exchange"></i><span class="panel-tabs--text">Zmiana</span> </a></li>
						<?php } ?>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane <?= (!isset($_GET['type'])) ? "active" : "" ?>" id="tab1">
                            <?php /* $this->render('_employees', ['employees' => $model->employees]) */ ?>
                            <?= EmployeesBlock::widget(['employees' => $model->employees]) ?>
						</div>
						<div class="tab-pane <?= (isset($_GET['type']) && $_GET['type'] == 'docs') ? "active" : "" ?>" id="tab2">
                            <?= FilesBlock::widget(['files' => $files, 'isNew' => $model->isNewRecord, 'typeId' => 5, 'parentId' => $model->id, 'onlyShow' => true, 'actionsShow' => ( ($isAdmin || $isSpecial) ? true : false)]) ?>
						</div>
						<?php if( $model->type_fk == 1 && $model->id_send_fk > 0 && count($files) > 1) { ?>
                        <div class="tab-pane" id="tab3">
                            <?= $this->render('_transfer', ['model' => $model]) ?>
						</div>
						<?php } ?>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

<?php $this->endContent(); ?>

