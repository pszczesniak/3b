<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\Correspondence */

$this->title = Yii::t('app', 'Nowa korespondencja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka korespondencji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-create', 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form_new', ['model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees ]) ?>

<?php $this->endContent(); ?>
