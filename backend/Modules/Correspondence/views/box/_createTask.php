<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $task app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
    
    <?= $form->field($task, 'type_fk')->radioList([1 => 'Rozprawa', 2 => 'Zadanie'], 
                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                        },
                                ]) ?>
    
    <?= $form->field($task, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($task, 'id_dict_task_type_fk')->radioList(\backend\Modules\Task\models\CalTask::listTypes(), 
                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                            return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                        },
                                ]) ?>

    <?= $form->field($task, 'place')->textInput(['maxlength' => true]) ?>
    <?= $form->field($task, 'description')->textarea(['rows' => 2]) ?>
    <div class="grid">
        <div class='col-md-6'>
            <div class="form-group">
                <label for="caltask-date_from" class="control-label">Start</label>
                <div class='input-group date' id='datetimepicker_start'>
                    <input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $task->date_from ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class='col-sm-6'>
            <div class="form-group">
                <label for="caltask-date_to" class="control-label">Koniec</label>
                <div class='input-group date' id='datetimepicker_end'>
                    <input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $task->date_to ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>

    </div>


    <?= $form->field($task, 'id_dict_task_category_fk')->radioList(\backend\Modules\Task\models\CalTask::listCategory(), 
                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                    'item' => function ($index, $label, $name, $checked, $value) {
                            return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                        },
                ]) ?>
    <?= $form->field($task, 'id_dict_task_status_fk')->radioList(\backend\Modules\Task\models\CalTask::listStatus(), 
                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                    'item' => function ($index, $label, $name, $checked, $value) {
                            return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                        },
                ]) ?>

	

