<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php
                if(count(array_intersect(["grantAll"], $grants)) == 1 && $casesCounter == 1 ) {
                    echo $form->field($model, 'id_customer_fk', [])->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] );
                }
            ?> 

            <?= $form->field($model, 'id_case_fk', [])->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList( ($model->id_customer_fk) ? $model->id_customer_fk : 0 ), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-case select2'] ) ?> 
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Zapisz zmiany'), ['class' => 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<?php if(count(array_intersect(["grantAll"], $grants)) == 1 && $casesCounter == 1 ) { ?>
<script type="text/javascript">
    document.getElementById('correspondencetask-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+( (this.value) ? this.value : 0 ), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('correspondencetask-id_case_fk').innerHTML = result.list;   
            }
        }
       xhr.send();
        return false;
    }
</script>
<?php } ?>