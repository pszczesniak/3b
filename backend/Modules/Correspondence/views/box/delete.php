<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
$this->title = Yii::t('app', 'Edycja korespondencji');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Książka korespondencji'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Alert');
?>

<div class="alert alert-danger">
    Korespondencja <b><?= ($model->type_fk == 1) ? 'przychodząca' : 'wychodząca' ?></b> została usunięta <br /><br />
    Dokumenty powiązane, które zostały usunięte razem z tym wpisem: 
    <ul>
    <?php
        foreach($model->dfiles as $key => $file) {
            echo '<li>'.$file->title_file.'</li>';
        }
    ?>
    </ul>
    Data usunięcia: <?= $model->deleted_at ?><br />
    Osoba usuwająca: <?= \common\models\User::findOne($model->deleted_by)->fullname ?>
</div>