<?php
    $managers = [];
    $managersSql = \backend\Modules\Company\models\CompanyDepartment::find()->all();
    foreach($managersSql as $key => $value) {
        array_push($managers, $value->id_employee_manager_fk);
    }
    
    $company = \backend\Modules\Company\models\Company::findOne(1);
    $customData = \yii\helpers\Json::decode($company->custom_data);
    
    if( isset($customData['box']) ) {
        $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
        $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
    } else {
        $company->box_special_emails = '';
        $company->box_special_employees = [ 0 => 0 ];
    }
?>
<div class="content-employee-all-select custom-inputs"><input type="checkbox" class="selecctall" id="checkbox_employee"/> <label for="checkbox_employee">Zaznacz wszystkich</label></div>
<div class="content-employee-select custom-inputs">
	<ul id="menu-list-employee" class="employee-container ">
		<?php
			/*$employeesList = \backend\Modules\Company\models\CompanyEmployee::getListByDepartments($ids);
            $editableEmployees = [];
            if($employeesList) {*/
				foreach($employees as $key=>$value){
				//foreach(\backend\Modules\Company\models\CompanyEmployee::getList() as $key=>$value){
					//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
					//array_push($editableEmployees, $key);
					$checked = '';
					if(in_array($key, $employees_ch)) $checked = 'checked="checked"';
                    if(in_array($key, $managers)) $checked = 'checked="checked"';
                    if(in_array($key, $company->box_special_employees)) $checked = 'checked="checked"';
					echo '<li><input  id="d'.$key.'" type="checkbox" name="employees[]" value="'.$key.'" '.$checked.'>'
                        .'<label for="d'.$key.'" class="'. ( (in_array($key, $managers)) ? "text--blue" : ( (in_array($key, $company->box_special_employees)) ? "text--pink" : "text--black") ) .'">'.$value.'</label></li>';
                    
				}
			//}
            
            /*foreach($all as $key=>$value){
				//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
                $checked = 'checked="checked"';
                if(!in_array($value->id_employee_fk, $editableEmployees)) 
					echo '<li><input class="checkbox_employee" id="d'.$value->id_employee_fk.'" type="checkbox" name="employees[]" value="'.$value->id_employee_fk.'" '.$checked.' disabled><label for="d'.$value->id_employee_fk.'">'.$value->employee['fullname'].'</label></li>';
			}*/
		?>
	</ul> 
</div>
