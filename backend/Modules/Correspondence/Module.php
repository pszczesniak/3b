<?php

namespace app\modules\Correspondence;

use Yii;
use yii\helpers\Url;
/**
 * Correspondence module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\Modules\Correspondence\controllers';

    
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        if(!Yii::$app->user->isGuest) {
			if(!in_array(Yii::$app->controller->id, [ 'site' ]) ){
				/*if(!$this->params['departments'] ) {
					throw new \yii\web\HttpException(405, 'Parametry sesji nie zosta�y ustawione lub wygas�y. Aby rozpocz�� prac� z systemem nale�y wybra� opcj� konfiguracji sesji i ustawi� wymagane parametry..');
				}*/
                if(Yii::$app->runAction('/site/mustresetpassword', []) >= 30 && \Yii::$app->user->identity->username != 'superadmin')
                    return Yii::$app->getResponse()->redirect(Url::to(['/site/changepassword']));
			}
        } else {
			//return $this->redirect(Url::to(['/site/login']));
			return Yii::$app->getResponse()->redirect(Url::to(['/site/login']));
		}

        return true; // or false to not run the action
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        //$this->params['zoneId'] = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):false; 
       
        $grants = [];
        $departments = [];
		$this->params['isAdmin'] = 0;
        $session = \Yii::$app->session;
        if(!Yii::$app->user->isGuest) {
            if(\Yii::$app->user->identity->username != 'superadmin') {
            
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $this->params['isAdmin'] = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
                $this->params['isSpecial'] = $session->get('user.isSpecial'); 
                $this->params['employeeId'] = $employee->id;
                $this->params['employeeKind'] = $employee->id_dict_employee_kind_fk;
                $this->params['employeeBranch'] = ($employee->id_company_branch_fk) ? $employee->id_company_branch_fk : 0;
                
                if(!$employee->is_admin) {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    }
                    
                    if(!Yii::$app->user->isGuest /*&& $this->params['structureId']*/) {
                        $auth = Yii::$app->authManager;
                        $roles = $auth->getRolesByUser(Yii::$app->user->identity->id);//var_dump($roles); exit;
                       // $departments = \backend\Modules\Company\models\EmployeeDepartment::find()->where(['id_zone_structure_fk' => $this->params['structureId'], 'id_user_fk' => Yii::$app->user->identity->id])->one();
                        foreach($roles as $key=>$value) {

                            $grantsRole = $auth->getPermissionsByRole($value->name); 
                            foreach($grantsRole as $key1=>$value1) {
                                array_push($grants, $key1);
                            }
                           
                            array_push($grants, $key);
                        }
                        $grantsUser = $auth->getPermissionsByUser(Yii::$app->user->identity->id); 
                    }   

                    
                } else {
                    $this->params['isAdmin'] = 1;
					$this->params['employeeKind'] = 100;
                    array_push($grants, 'grantAll');
                }
            } else {
                $this->params['isAdmin'] = 1;
				$this->params['employeeBranch'] = 0;
                $this->params['employeeId'] = 0;
                $this->params['employeeKind'] = 100;
                array_push($grants, 'grantAll');
            }
        }
        $this->params['grants'] = $grants;
        $this->params['departments'] = $departments;
		
        /*replacement begin*/
        $replacement = \backend\Modules\Company\models\CompanyReplacement::find()->where(['status' => 1, 'id_employee_fk' => ( isset($this->params['employeeId']) ? $this->params['employeeId'] : 0) ])->andWhere("'".date('Y-m-d')."' between date_from and date_to")->one();
        if($replacement && $replacement->id_substitute_branch_fk) $this->params['employeeBranch'] = $replacement->id_substitute_branch_fk;
        /*replacement end*/
        
        //var_dump($grants); exit;
	    $app = Yii::$app;
	    $app->session->set('user.module','task');
	   
		parent::init();

        // custom initialization code goes here
    }
}
