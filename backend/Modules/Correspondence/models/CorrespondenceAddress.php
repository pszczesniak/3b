<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%correspondence_address}}".
 *
 * @property integer $id
 * @property integer $id_company_branch_fk
 * @property string $name
 * @property integer $type_fk
 * @property string $address
 * @property string $email
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CorrespondenceAddress extends \yii\db\ActiveRecord
{
    public $is_ignore = 0;
    public $user_action;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['id_parent_fk', 'id_dict_address_type_fk', 'id_company_branch_fk', 'type_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['note', 'symbol'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'postalcode', 'city','country'], 'safe'],
            [['name', 'address'], 'string', 'max' => 1000],
            [['email',  'phone', 'city','country'], 'string', 'max' => 255],
			[['email'], 'email'],
            ['name', 'unique', 'when' => function($model) { return $model->is_ignore == 0; }, 'message' => 'Wpis o podanej nazwie już występuje w systemie.', 'filter' => ['status' => 1]],
            ['name', 'compareName', 'when' => function($model) { return $model->is_ignore == 0; } ],
        ];
    }
    
    public function compareName($attribute, $params)
    {
        if($this->isNewRecord)
			$names = \backend\Modules\Correspondence\models\CorrespondenceAddress::find()->where(['status' => 1])->andWhere("lower(replace(name, ' ', '')) = '".str_replace(' ', '', $this->name)."'")->all();
        else
			$names = \backend\Modules\Correspondence\models\CorrespondenceAddress::find()->where(['status' => 1])->andWhere("id != ".$this->id." and lower(replace(name, ' ', '')) = '".str_replace(' ', '', $this->name)."'")->all();

		$ok = true;
        
        if(count($names) > 0) {
            $this->addError($attribute, 'W systemie istnieje kontakt o podobnej nazwie. Jeśli '.$names[0]->name.''
                                        .' to zupełnie kontakt i chcesz wprowadzić wpis o podanej nazwie zaznacz tutaj <input type="checkbox" value="1" name="CorrespondenceAddress[is_ignore]" id="correspondenceaddress-is_ignore"> i ponownie zapisz zmiany.');
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_branch_fk' => Yii::t('app', 'Id Company Branch Fk'),
            'name' => Yii::t('app', 'Name'),
            'type_fk' => Yii::t('app', 'Type'),
            'id_dict_address_type_fk' => Yii::t('app', 'Type'),
            'address' => Yii::t('app', 'Address'),
            'postalcode' => 'Kod pocztowy',
            'city' => 'Miasto',
            'email' => Yii::t('app', 'Email'),
			'phone' => Yii::t('app', 'Telefon'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_parent_fk' => 'Podmiot nadrzędny'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else {
                $this->updated_by = \Yii::$app->user->id;
            }
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->is_ignore = 1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public static function getList() {
        if(Yii::$app->params['env'] == 'dev')
            return CorrespondenceAddress::find()->select(['id', 'fulladdress(id) as name'])->where(['status' => 1])->orderby('name')->all(); 
        else
            return CorrespondenceAddress::find()->where(['status' => 1])->orderby('name')->all(); 
    }
	
	public static function getTypes() {
        return [1 => 'Sądy', 2 => 'Prokuratury', 3 => 'Urzędy', 4 => 'Kancelarie/Prawnicy'];
    }
	
	public function getType() {
        return $this->getTypes()[$this->type_fk];
    }
    
    public static function listTypes() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 10])->orderby('name')->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public function getFullname() {
        return Yii::$app->db->createCommand("select fulladdress(".$this->id.") as fullname")->queryOne()['fullname'];
    }
}
