<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Correspondence\models\CorrespondenceSend;

/**
 * CorrespondenceSearch represents the model behind the search form about `backend\Modules\Correspondence\models\Correspondence`.
 */
class CorrespondenceSendSearch extends CorrespondenceSend
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_company_branch_fk'], 'integer'],
            [['posting_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CorrespondenceSend::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company_branch_fk' => $this->id_company_branch_fk,
            'posting_date' => $this->posting_date,  
        ]);

       /* $query->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'type_fk', $this->type_fk])
            ->andFilterWhere(['like', 'sender', $this->sender])
            ->andFilterWhere(['like', 'recipient', $this->recipient]);*/

        return $dataProvider;
    }
}
