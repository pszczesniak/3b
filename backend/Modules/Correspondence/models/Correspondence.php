<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Correspondence\models\CorrespondenceArch;
use backend\Modules\Correspondence\models\CorrespondenceEmployee;
use backend\Modules\Correspondence\models\CorrespondenceTask;
/**
 * This is the model class for table "{{%correspondence}}".
 *
 * @property integer $id
 * @property integer $id_company_branch_fk
 * @property string $place
 * @property string $type_fk
 * @property string $date_delivery
 * @property string $date_posting
 * @property string $sender
 * @property string $recipient
 * @property integer $id_customer_fk
 * @property integer $id_case_fk
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Correspondence extends \yii\db\ActiveRecord
{
    public $name;
    public $user_action;
    
    public $departments_list;
    public $files_list;
    public $employees_list;
    public $events_list;
    public $cases_list = [];
	public $docs_list;
	public $id_employee_fk;
	public $id_department_fk;
	public $employees_rel = [];
    public $departments_rel = [];
    public $addresses_list = [];
    
    public $createTask;
    public $isFile;
    public $address_type_fk;
    
    public $is_ignore = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_branch_fk', 'id_customer_fk', 'id_case_fk', 'id_case_instance_fk', 'id_event_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'send_by', 'type_fk','createTask', 'id_sender_fk', 'id_recipient_fk','isFile','id_send_fk', 'is_ignore', 'id_transfer_fk'], 'integer'],
            [['date_delivery', 'date_posting', 'created_at', 'updated_at', 'deleted_at', 'send_at', 'note', 'departments_list', 'events_list', 'cases_list', 'employees_list', 'docs_list', 'addresses_list', 'address_type_fk'], 'safe'],
            [['type_fk', 'id_company_branch_fk', 'id_customer_fk', 'id_case_fk', 'date_delivery'], 'required', 'when' => function($model) { return ($model->status == 1 && $model->user_action != 'change-note' && $model->user_action != 'change-case');  }],
            [['note'], 'required', 'when' => function($model) { return ($model->status == 1 );  }],
            [['place'], 'string', 'max' => 1000],
            [['sender', 'recipient'], 'string', 'max' => 100],
            ['isFile', 'required', 'when' => function($model) { return (($model->user_action == 'transfer') ? ( (is_array($model->docs_list) && count($model->docs_list) == 0) || !$model->docs_list ) : (\common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 5, 'id_fk' => $model->id])->count() == 0 && $model->status == 1)); } ],
            ['id_case_fk', 'required', 'when' => function($model) { return ($model->id_case_fk == 0 && $model->status == 1); } ],
			['addresses_list', 'required', 'when' => function($model) { return ($model->type_fk == 1 && $model->status == 1 && !$model->id_sender_fk); }, 'message' => 'Proszę wybrać odbiorcę' ],
            ['addresses_list', 'required', 'when' => function($model) { return ($model->type_fk == 2 && $model->status == 1 && !$model->id_recipient_fk); }, 'message' => 'Proszę wybrać nadawcę' ],
            //['id_sender_fk', 'required', 'when' => function($model) { return ($model->type_fk == 1 && $model->status == 1 &&  $model->id_sender_fk == 0); } ],
			//['id_recipient_fk', 'required', 'when' => function($model) { return ($model->type_fk == 2 && $model->status == 1 &&  $model->id_recipient_fk == 0); } ],
            ['date_delivery', 'validateDeliveryDate', 'when' => function($model) { return ($model->status == 1 && $model->user_action != 'send' && $model->user_action != 'change-note'  && $model->user_action != 'change-case' && $model->type_fk == 1); } ],
            ['id_employee_fk', 'required', 'message' => 'Musisz przypisać przynajmniej jednego pracownika', 'when' => function($model) { return ($model->status == 1 && $model->user_action != 'send' && $model->user_action != 'change-case'); } ],
           // ['isFile', 'validateFileExist'],
           // ['date_delivery', 'required', 'when' => function($model) { return ($model->date_delivery <  ); }, 'message' => 'Nie możesz wybrać daty wcześniejszej niż dzisiejsza' ]
            //['emloyees_list', 'unique', 'filter' => ['status' => 1], 'message' => 'Klient o podanej nazwie już występuje w systemie.','when' => function($model) { return $model->is_ignore == 0; }],
            ['employees_list', 'checkEmployees', 'when' => function($model) { return ($model->is_ignore == 0 && $model->status == 1); } ],
            ['docs_list', 'checkTransferDocs', 'when' => function($model) { return ($model->user_action == 'transfer'); } ],
        ];
    }
    
    public function validateDeliveryDate($attribute, $params)
    {
        if ( $this->$attribute < date('Y-m-d') && $this->user_action != 'transfer' ) {
            $this->addError($attribute, 'Nie możesz wybrać daty wcześniejszej niż dzisiejsza');
        }
    }
    
    public function validateFileExist($attribute, $params) {
        $files = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 5, 'id_fk' => $params['id']])->count();
        if ( $files == 0 && $params['status'] == 1 ) {
            $this->addError($attribute, 'Proszę dołączyć dokument');
        }
    }
    
    public function checkTransferDocs($attribute, $params) {
        $files = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 5, 'id_fk' => $this->id_transfer_fk])->count();
        if ( $files == count($this->docs_list) ) {
            $this->addError($attribute, 'Nie można przenieść wszystkich dokumentów');
        }
    }
    
    public function checkEmployees($attribute, $params) {
		if(!empty($this->cases_list) ) {
            $unchecked = \backend\Modules\Task\models\CaseEmployee::find()->where(['status' => 1, 'is_show' => 1])
                                                                          ->andWhere("id_case_fk in (".implode(',',$this->cases_list).") ")
                                                                          ->andWhere("id_employee_fk not in (". implode(',', $this->employees_list) .") ")->all();
                    
            if(count($unchecked) > 0) {
                $temp = [];
                foreach($unchecked as $key => $value) array_push($temp, $value->employee['fullname']);
                $this->addError($attribute, 'Z wysyłki korespondencji zostali wyłączeni pracownicy przypisani do wybranej sprawy [<b>'.implode(',', $temp).'</b>]. Jeśli to zamierzona akcja'
                                            .' to zaznacz tutaj <input type="checkbox" value="1" name="Correspondence[is_ignore]" id="Correspondence-is_ignore"> i ponownie zapisz zmiany.');
            }
        } else {
            $this->addError('cases_list', 'Proszę wybrać postępowanie');
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_branch_fk' => Yii::t('app', 'Place'),
            'place' => Yii::t('app', 'Place'),
            'type_fk' => Yii::t('app', 'Type'),
            //'date_delivery' => Yii::t('app', 'Date Delivery'),
			'date_delivery' => Yii::t('app', 'Data rejestracji'),
            'date_posting' => Yii::t('app', 'Data odbioru'),
            'sender' => Yii::t('app', 'Sender'),
            'recipient' => Yii::t('app', 'Recipient'),
            'id_sender_fk' => Yii::t('app', 'Sender'),
            'id_recipient_fk' => Yii::t('app', 'Recipient'),
            'id_customer_fk' => Yii::t('app', 'Customer'),
            'id_case_fk' => isset(Yii::$app->params['set-types']) ? implode(',/', Yii::$app->params['set-types']) : 'Postępowanie',
            'id_event_fk' => Yii::t('app', 'Zdarzenie'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'name' => Yii::t('app', 'Name'),
            'createTask' => 'Utwórz zadanie',
            'note' => 'Opis',
            'isFile' => 'Dokument',
            'cases_list' => 'Postępowanie',
            'id_case_instance_fk' => 'Postępowanie'
        ];
    }
    
    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} /*else {
                if($this->type_fk == 2) {
                    $this->send_by = \Yii::$app->user->id;
                    $this->send_at = new Expression('NOW()');
                }
            }*/
        
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function afterSave($insert, $changedAttributes) {
                
        if ($this->user_action == 'change-note' || $this->user_action == 'change-case' || $this->user_action == 'delete') {
            $modelArch = new CorrespondenceArch();
            $modelArch->id_root_fk = $this->id;
            $modelArch->user_action = $this->user_action;
            $modelArch->data_arch = \yii\helpers\Json::encode( [ 'data' => $changedAttributes, 'events' => $this->events, 'cases' => $this->cases ] );
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
        }  

		if( $this->user_action == 'create' ) {
            \common\models\Files::updateAll(['type_fk' => $this->type_fk],'id_fk = :correspondence and id_type_file_fk = 5', [':correspondence' => $this->id]);
		}

        if( $this->user_action == 'update' ) {
            \common\models\Files::updateAll(['type_fk' => $this->type_fk],'id_fk = :correspondence and id_type_file_fk = 5', [':correspondence' => $this->id]);

            //$oldAttributes = array_merge($this->getOldAttributes(), $changedAttributes);			
                
            $changedAttributes['employees_add'] = []; $changedAttributes['employees_del'] = []; $employeesByUser = []; $employeesByCase = [];
            $departments = []; array_push($departments, 0);
            $changedAttributes['new'] = $this->isNewRecord;
             
            $modelArch = new CorrespondenceArch();
            $modelArch->id_root_fk = $this->id;
            $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
            $modelArch->data_arch = \yii\helpers\Json::encode( [ 'data' => $changedAttributes ] );
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
                
            /*if( $this->events_list && !empty($this->events_list) ) {
                CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence', [ ':correspondence' => $this->id ]);
                foreach($this->events_list as $key => $value) {
                    $model_cd = new CorrespondenceTask();
                    $model_cd->id_correspondence_fk = $this->id;
                    $model_cd->id_task_fk = $value;
                    $model_cd->save();
                }
            } else {
                CorrespondenceTask::deleteAll('id_correspondence_fk = :correspondence', [ ':correspondence' => $this->id ]);
            }   */  
        }
        
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
        
        if($result) {
            \common\models\Files::updateAll(['status' => -1], 'status = 1 and id_type_file_fk = 5 and id_fk = '.$this->id);
        }
		//var_dump($this); exit;
		return $result;
	}
    
    public function getCustomer()
    {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
	
	public function getBranch()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyBranch::className(), ['id' => 'id_company_branch_fk']);
    }
    
    public function getDepartments()
    {
		$items = $this->hasMany(\backend\Modules\Correspondence\models\CorrespondenceDepartment::className(), ['id_correspondence_fk' => 'id']); 
		return $items;
    }
    
    public function getEmployees()
    {
		//$items = $this->hasMany(\backend\Modules\Correspondence\models\CorrespondenceEmployee::className(), ['id_correspondence_fk' => 'id'])->where(['is_show' => 1, 'status' => 1]); 
        $sql = "select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname, email, address, phone, is_show, dv.name as typename"
                ." from {{%correspondence_employee}} ce join {{%company_employee}} e on ce.id_employee_fk = e.id left join {{%dictionary_value}} dv on dv.id = id_dict_employee_type_fk"
                . " where ce.status = 1 and e.status = 1 and is_show = 1 and id_correspondence_fk = ".$this->id;
        $items = Yii::$app->db->createCommand($sql)->queryAll();
		return $items;
    }
    
    public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 5])->orderby('name_file')->all();
        return $filesData;
    }
    
    public function getDfiles() {
        $filesData = \common\models\Files::find()->where([ 'id_fk' => $this->id, 'id_type_file_fk' => 5])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getUpdating() {
		$user = \common\models\User::findOne($this->updated_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getTask() {
        $task = CorrespondenceTask::find()->where(['id_correspondence_fk' => $this->id])->one();
        if($task)
            return $task->task;
        else
            return false;
    }
    
    public function getEvent() {
        if($this->id_event_fk)
            return $task = \backend\Modules\Task\models\CalTask::findOne($this->id_event_fk);
        else
            return false;
    }
    
    public function getEvents() {
        $items = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['status' => 1, 'id_correspondence_fk' => $this->id])->andWhere('id_task_fk != -1')->all();
        return ($items) ? $items : [];
    }
    
    public function getCases() {
        $items = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['status' => 1, 'id_correspondence_fk' => $this->id])->andWhere('id_task_fk = -1')->all();
        return ($items) ? $items : [];
    }
    
    public function getAddresses() {
        $items = \backend\Modules\Correspondence\models\CorrespondenceAddresses::find()->where(['status' => 1, 'id_correspondence_fk' => $this->id])->all();
        return ($items) ? $items : [];
    }
    
    public function getAuthor() {
        $author = 'brak danych';
        if($this->type_fk == 1) {
            $model = \backend\Modules\Correspondence\models\CorrespondenceAddress::findOne($this->id_sender_fk);
        } else if( $this->type_fk == 2) {
            $model = \backend\Modules\Correspondence\models\CorrespondenceAddress::findOne($this->id_recipient_fk);
        }
        
        if($model) $author = $model->name;
        
        return $author;
    }
    
    public function getOsender()
    {
		return $this->hasOne(\backend\Modules\Correspondence\models\CorrespondenceAddress::className(), ['id' => 'id_sender_fk']);
    }
    public function getOrecipient()
    {
		return $this->hasOne(\backend\Modules\Correspondence\models\CorrespondenceAddress::className(), ['id' => 'id_recipient_fk']);
    }
}
