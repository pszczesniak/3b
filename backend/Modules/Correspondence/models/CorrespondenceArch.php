<?php

namespace backend\Modules\Correspondence\models;

use Yii;

/**
 * This is the model class for table "{{%correspondence_arch}}".
 *
 * @property integer $id
 * @property integer $id_root_fk
 * @property string $user_action
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class CorrespondenceArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_root_fk'], 'required'],
            [['id_root_fk', 'created_by'], 'integer'],
            [['data_arch'], 'string'],
            [['created_at'], 'safe'],
            [['user_action'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_root_fk' => Yii::t('app', 'Id Root Fk'),
            'user_action' => Yii::t('app', 'User Action'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
