<?php

namespace backend\Modules\Correspondence\models;

use Yii;

/**
 * This is the model class for table "{{%correspondence_send}}".
 *
 * @property integer $id
 * @property integer $id_company_branch_fk
 * @property string $email_title
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 */
class CorrespondenceSend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_send}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_company_branch_fk', 'status', 'created_by'], 'integer'],
            [['created_at', 'posting_date'], 'safe'],
            [['email_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Rodzaj wysyłki'),
            'id_company_branch_fk' => Yii::t('app', 'Miejsce'),
            'email_title' => Yii::t('app', 'Email Title'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function getBranch()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyBranch::className(), ['id' => 'id_company_branch_fk']);
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getItems() {
        $items = \backend\Modules\Correspondence\models\CorrespondenceSendItem::find()->where(['id_send_fk' => $this->id])->all();
        return ($items) ? $items : [];
    }
}
