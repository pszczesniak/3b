<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%correspondence_addresses}}".
 *
 * @property integer $id
 * @property integer $id_correspondence_fk
 * @property integer $id_address_fk
 * @property string $created_at
 * @property integer $created_by
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CorrespondenceAddresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_addresses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_correspondence_fk', 'id_address_fk'], 'required'],
            [['id_correspondence_fk', 'id_address_fk', 'created_by', 'status', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_correspondence_fk' => Yii::t('app', 'Id Correspondence Fk'),
            'id_address_fk' => Yii::t('app', 'Id Address Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->is_ignore = 1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function getAddress()
    {
		return $this->hasOne(\backend\Modules\Correspondence\models\CorrespondenceAddress::className(), ['id' => 'id_address_fk']);
    }
}
