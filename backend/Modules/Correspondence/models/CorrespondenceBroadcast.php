<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%correspondence_broadcast}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_company_branch_fk
 * @property string $name
 * @property string $date_broadcasting
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CorrespondenceBroadcast extends \yii\db\ActiveRecord
{
    public $date_from;
	public $date_to;
	public $user_action;
	public $isFile;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_broadcast}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_company_branch_fk', 'date_broadcasting'], 'required'],
			[['type_fk', 'id_company_branch_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_broadcasting', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['note'], 'string'],
            [['name'], 'string', 'max' => 1000],
			['isFile', 'required', 'when' => function($model) { return (\common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 8, 'id_fk' => $model->id])->count() == 0 && $model->status == 1); } ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_fk' => 'Nazwa',
            'id_company_branch_fk' => 'Oddział',
            'name' => 'Nazwa',
            'date_broadcasting' => 'Date Broadcasting',
            'note' => 'Notatka',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
			'isFile' => 'Dokument'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else {
                $this->updated_by = \Yii::$app->user->id;
            }
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public static function getTypes() {
        return [1 => 'Lista nadawcza', 2 => 'Potwierdzenie nadania'];   
    }
	
	public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 8])->orderby('id desc')->all();
        return $filesData;
    }
	
	public function getBranch() {
		return $this->hasOne(\backend\Modules\Company\models\CompanyBranch::className(), ['id' => 'id_company_branch_fk']);
    }
	
	public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getUpdating() {
		$user = \common\models\User::findOne($this->updated_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
}
