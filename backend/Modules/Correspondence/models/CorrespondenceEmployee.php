<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%correspondence_employee}}".
 *
 * @property integer $id
 * @property integer $id_correspondence_fk
 * @property integer $id_employee_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CorrespondenceEmployee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_employee}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_correspondence_fk', 'id_employee_fk'], 'required'],
            [['id_correspondence_fk', 'id_employee_fk', 'status'], 'unique', 'targetAttribute' => ['id_correspondence_fk', 'id_employee_fk', 'status']],
            [['id_correspondence_fk', 'id_employee_fk', 'created_by', 'deleted_by', 'status', 'is_show'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_correspondence_fk' => Yii::t('app', 'Id Correspondence Fk'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function delete() {
		
        $this->status = -1;
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		
		return $result;
	}
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCorrespondence()
    {
		return $this->hasOne(\backend\Modules\Correspondence\Correspondence\CalCase::className(), ['id' => 'id_correspondence_fk']);
    }
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
}
