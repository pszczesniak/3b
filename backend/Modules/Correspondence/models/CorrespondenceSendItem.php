<?php

namespace backend\Modules\Correspondence\models;

use Yii;

/**
 * This is the model class for table "{{%correspondence_send_item}}".
 *
 * @property integer $id
 * @property integer $id_send_fk
 * @property integer $id_employee_fk
 * @property string $email_to
 * @property string $email_cc
 * @property string $email_temp
 * @property string $sending_error
 * @property integer $status
 * @property string $send_at
 */
class CorrespondenceSendItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_send_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_send_fk', 'id_employee_fk', 'status', 'files_night', 'files_big'], 'integer'],
            [['email_to', 'email_cc', 'email_temp', 'sending_error'], 'string'],
            [['send_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_send_fk' => Yii::t('app', 'Id Send Fk'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'email_to' => Yii::t('app', 'Email To'),
            'email_cc' => Yii::t('app', 'Email Cc'),
            'email_temp' => Yii::t('app', 'Email Temp'),
            'sending_error' => Yii::t('app', 'Sending Error'),
            'status' => Yii::t('app', 'Status'),
            'send_at' => Yii::t('app', 'Send At'),
        ];
    }
    
    public function getSend()
    {
		return $this->hasOne(\backend\Modules\Correspondence\models\CorrespondenceSend::className(), ['id' => 'id_send_fk']);
    }
}
