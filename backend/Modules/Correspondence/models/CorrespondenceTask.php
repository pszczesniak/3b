<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%correspondence_task}}".
 *
 * @property integer $id
 * @property integer $id_correspondence_fk
 * @property integer $id_task_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CorrespondenceTask extends \yii\db\ActiveRecord
{
    public $id_customer_fk;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_correspondence_fk', 'id_task_fk', 'id_case_fk'], 'required'],
            [['id_correspondence_fk', 'id_task_fk', 'created_by', 'deleted_by', 'status', 'id_case_fk', 'id_case_instance_fk'], 'integer'],
            [['created_at', 'id_customer_fk', 'deleted_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_correspondence_fk' => Yii::t('app', 'Id Correspondence Fk'),
            'id_case_fk' => Yii::t('app', 'Sprawa/ Projekt'),
            'id_task_fk' => Yii::t('app', 'Zdarzenie'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'id_customer_fk' => 'Klient',
            'id_case_instance_fk' => 'Postępowanie'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
                $this->status = 1;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getTask()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalTask::className(), ['id' => 'id_task_fk']);
    }
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
}
