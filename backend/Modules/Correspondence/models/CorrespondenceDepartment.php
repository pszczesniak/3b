<?php

namespace backend\Modules\Correspondence\models;

use Yii;

/**
 * This is the model class for table "{{%correspondence_department}}".
 *
 * @property integer $id
 * @property integer $id_correspondence_fk
 * @property integer $id_department_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CorrespondenceDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_correspondence_fk', 'id_department_fk'], 'required'],
            [['id_correspondence_fk', 'id_department_fk', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_correspondence_fk' => Yii::t('app', 'Id Correspondence Fk'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function getCorrespondence()
    {
		return $this->hasOne(\backend\Modules\Correspondence\models\Correspondence::className(), ['id' => 'id_correspondence_fk']);
    }
    
    public function getDepartment()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyDepartment::className(), ['id' => 'id_department_fk']);
    }
}
