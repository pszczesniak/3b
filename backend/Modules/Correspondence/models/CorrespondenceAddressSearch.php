<?php

namespace backend\Modules\Correspondence\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Correspondence\models\CorrespondenceAddress;

/**
 * CorrespondenceAddressSearch represents the model behind the search form about `backend\Modules\Correspondence\models\CorrespondenceAddress`.
 */
class CorrespondenceAddressSearch extends CorrespondenceAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_company_branch_fk', 'type_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'address', 'email', 'note', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CorrespondenceAddress::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company_branch_fk' => $this->id_company_branch_fk,
            'type_fk' => $this->type_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
