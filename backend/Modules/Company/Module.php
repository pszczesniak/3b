<?php

namespace app\Modules\Company;

use Yii;
use yii\helpers\Url;

/**
 * Company module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\Modules\Company\controllers';
    
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        if(!Yii::$app->user->isGuest) {
			if(!in_array(Yii::$app->controller->id, [ 'site' ]) ){
				/*if(!$this->params['departments'] ) {
					throw new \yii\web\HttpException(405, 'Parametry sesji nie zosta�y ustawione lub wygas�y. Aby rozpocz�� prac� z systemem nale�y wybra� opcj� konfiguracji sesji i ustawi� wymagane parametry..');
				}*/
                if(Yii::$app->runAction('/site/mustresetpassword', []) >= 30 && \Yii::$app->user->identity->username != 'superadmin')
                    return Yii::$app->getResponse()->redirect(Url::to(['/site/changepassword']));
			}
        } else {
			//return $this->redirect(Url::to(['/site/login']));
			return Yii::$app->getResponse()->redirect(Url::to(['/site/login']));
		}

        return true; // or false to not run the action
    }

    /**
     * @inheritdoc
     */
     
     public function init()
    {
        $grants = [];  $departments = [0 => 0];  $employees = []; $this->params['isAdmin'] = 0;
        // \Yii::$app->session->set('user.idEmployee', $employee->id);
        $session = \Yii::$app->session;
		$this->params['replacementBranch'] = 0;
        if(!Yii::$app->user->isGuest) {
            if(\Yii::$app->user->identity->username != 'superadmin') {
                
                /*$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $this->params['isAdmin'] = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();*/
                $this->params['isAdmin'] = $session->get('user.isAdmin');
                $this->params['isSpecial'] = $session->get('user.isSpecial'); 
                $this->params['employeeId'] = $session->get('user.idEmployee');
                $this->params['employeeKind'] = $session->get('user.kindEmployee');
                $this->params['employeeBranch'] = /*($this->params['isAdmin']) ? -1 :*/ $session->get('user.idBranch');
				
				/*replacement begin*/
				$replacement = \backend\Modules\Company\models\CompanyReplacement::find()->where(['status' => 1, 'id_employee_fk' => ( isset($this->params['employeeId']) ? $this->params['employeeId'] : 0) ])->andWhere("'".date('Y-m-d')."' between date_from and date_to")->one();
				if($replacement && $replacement->id_substitute_branch_fk) $this->params['replacementBranch'] = $replacement->id_substitute_branch_fk;
				/*replacement end*/

                if($this->params['isAdmin'] == 1) {
                    array_push($grants, 'grantAll');
                } else {
                    if($session->get('user.departments') && $this->params['isAdmin'] == 0) {
                        foreach($session->get('user.departments') as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    } else {
                        $dapartmentsData = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->all();
                        foreach($dapartmentsData as $key => $value) {
                            array_push($departments, $value->id);
                        }
                    }

                    $auth = Yii::$app->authManager;
                    $roles = $auth->getRolesByUser(Yii::$app->user->identity->id);//var_dump($roles); exit;
                    foreach($roles as $key=>$value) {
                        $grantsRole = $auth->getPermissionsByRole($value->name); 
                        foreach($grantsRole as $key1=>$value1) {
                            array_push($grants, $key1);
                        }                      
                        array_push($grants, $key);
                    }
                    $grantsUser = $auth->getPermissionsByUser(Yii::$app->user->identity->id); 
                }
            } else {
                $this->params['isAdmin'] = 1;
                $this->params['isSpecial'] = 1;
                $this->params['employeeId'] = 0;
                $this->params['employeeKind'] = 100;
                $this->params['employeeBranch'] = 0;
                array_push($grants, 'grantAll');
            }
        }
        $this->params['grants'] = $grants;
        $this->params['departments'] = $departments;
        $this->params['employees'] = $employees;
        
	    $app = Yii::$app;
	    $app->session->set('user.module','task');

		parent::init();
    }
}
