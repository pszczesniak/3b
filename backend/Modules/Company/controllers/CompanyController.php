<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\Company;
use backend\Modules\Company\models\CompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;
use yii\web\UploadedFile;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id), 'grants' => $this->module->params['grants'],
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();
		if(Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return array('success' => true, 'html' => $this->renderAjax('_form', [  'model' => $model,]) );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}
	    } else {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				return $this->render('create', [
					'model' => $model,
				]);
			}
	    }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
		if(Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return array('success' => true, 'alert' => 'Zmiany zostały zapisane.', 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}
	    } else {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		}
    }
    
    public function actionConfig($id)
    {
        $model = $this->findModel($id);
        $model->user_action = 'config';
        
        $customData = \yii\helpers\Json::decode($model->custom_data);
        
        if( isset($customData['calendar']) ) {
            $model->calendarColorConfirmed = ($customData['calendar']['calendarColorConfirmed']) ? $customData['calendar']['calendarColorConfirmed'] : '#ffffff';
            $model->calendarColorUnconfirmed = ($customData['calendar']['calendarColorUnconfirmed']) ? $customData['calendar']['calendarColorUnconfirmed'] : '#000000';
            $model->calendarColorDone = ($customData['calendar']['calendarColorDone']) ? $customData['calendar']['calendarColorDone'] : '#ffe3ad';
            $model->sendEmailInfo = ($customData['calendar']['sendEmailInfo']) ? $customData['calendar']['sendEmailInfo'] : 'Dziękujemy za kontakt.|Odezwiemy się w ciągu 48h.';
        }
        
		if(Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            //$box = [];
            //$box['emails'] = $model->box_special_emails;
            //$box['employees'] = $model->box_special_employees;
            $calendar = [];
            $calendar['calendarColorConfirmed'] = $model->calendarColorConfirmed;
            $calendar['calendarColorUnconfirmed'] = $model->calendarColorUnconfirmed;
            $calendar['calendarColorDone'] = $model->calendarColorDone;
            $calendar['sendEmailInfo'] = $model->sendEmailInfo;
            $model->custom_data = \yii\helpers\Json::encode(['calendar' => $calendar]);
			if ( $model->save() ) {
				return array('success' => true, 'alert' => 'Zmiany zostały zapisane.', 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}
	    } else {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['config', 'id' => $model->id]);
			} else {
				return $this->render('config', [
					'model' => $model,
				]);
			}
		}
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        //$id = CustomHelpers::decode($id);
		if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionLogo($id)   {
		
		$model = $this->findModel($id);
        $model->user_action = "logo";
        //$mimeType = 'image/jpg';
        $mimeType = '.jpg, .png';
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->saveLogo()) {
				return array('success' => true, 'action' => 'logoChange', 'alert' => 'Plik z logo został zamieniony', 'refresh' => 'yes', 
                             'newLogo' => '<img src="'.Company::getLogoAbsolute().'?v='.time().'" alt="logo" class="logo-img" style="max-height:65px;">');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('change', ['model' => $model, 'mimeType' => $mimeType]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('change', ['model' => $model, 'mimeType' => $mimeType]) ;	
		}
    }
}
