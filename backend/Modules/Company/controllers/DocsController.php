<?php

namespace app\Modules\Company\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\web\UploadedFile;

use backend\Modules\Folder\models\Folder;
use backend\Modules\Folder\models\FolderUser;
use backend\Modules\Folder\models\File;
use backend\Modules\Folder\models\FileUser;

/**
 * Docs controller for the `Folder` module
 */
class DocsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionFolder()  {
        $personalFolder = Folder::find()->where(['id_parent_fk' => 0 /*, 'created_by' => Yii::$app->user->id, 'item_type' => 2*/])->one();
        if(!$personalFolder) {
            $personalFolder = new Folder();
            $personalFolder->id_parent_fk = 0;
            $personalFolder->item_type = 2;
            $personalFolder->item_name = 'Dokumenty firmowe';
            $personalFolder->save();
            /*if($personalFolder->save()) {
                $personalFolderUser = new FolderUser();
                $personalFolderUser->id_folder_fk = $personalFolder->id;
                $personalFolderUser->id_user_fk = \Yii::$app->user->id;
                $personalFolderUser->save();
            }*/
        }
		
		return $this->render('index', ['folder' => $personalFolder, 'breadcrumbs' => false]);
    }
	
	public function actionView($id)  {
        $selectedFolder = Folder::findOne($id);
        
        $sql = "select fullpath(".$selectedFolder->id.") as folderPath";
        $dataPath = \Yii::$app->db->createCommand($sql)->queryOne();	
        $breadcrumbs = [];
        $dataPathArr = explode(' / ', $dataPath['folderPath']);
        foreach($dataPathArr as $key => $item) {
            $itemArr = explode('-', $item);
            if($key == 0) {
                array_push($breadcrumbs, ['name' => '<i class="fa fa-home"></i>', 'path' => Url::to(['/company/docs/folder'])]);
            } 
            if($id == $itemArr[0])
                array_push($breadcrumbs, ['name' => $itemArr[1], 'path' => false]);
            else
                array_push($breadcrumbs, ['name' => $itemArr[1], 'path' => Url::to(['/company/docs/view', 'id' => $itemArr[0]])]);
            
        }
		//var_dump($breadcrumbs);exit;
		return $this->render('index', ['folder' => $selectedFolder, 'breadcrumbs' => $breadcrumbs] );
    }
	
	public function actionData($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		
		$sql = "select 'folder' as item_type, fu.id as fuid, f.id, is_starred, item_name, f.created_at, concat_ws(' ', firstname, lastname) as creator, '' as sizeEl, '' as extension_file "
					." from {{%folder_users}} fu join {{%folder}} f on fu.id_folder_fk = f.id join {{%user}} u on u.id = fu.id_user_fk where id_parent_fk = ".$id
				." union "
				."select 'file' as item_type, fu.id as fuid, f.id, is_starred, name_file as item_name, f.created_at, concat_ws(' ', firstname, lastname) as creator, round(size_file/1024/1024,2) as sizeEl, extension_file "
					." from {{%file_users}} fu join {{%file}} f on fu.id_file_fk = f.id join {{%user}} u on u.id = fu.id_user_fk where id_folder_fk = ".$id
				." order by item_type desc, item_name";
		
		$data = Yii::$app->db->createCommand($sql)->queryAll();		
		
        //$value = ['mid' => 0, 'is_starred' => 0];
		$tmp = []; $fields = [];
		
		foreach($data as $key => $item) {
			if($item['item_type'] == 'folder') {
				$tmp['type'] = '<a href="'.Url::to(['/company/docs/view', 'id' => $item['id']]).'" title="Otwórz katalog"><i class="fas fa-folder fa-3x text--yellow"></i></a>';
			} else {
				if($item['extension_file'] == 'pdf') {
					$tmp['type'] = '<i class="fas fa-file-pdf fa-2x text--red"></i>';
				} else if($item['extension_file'] == 'xls' || $item['extension_file'] == 'xlsx' || $item['extension_file'] == 'xlsm') {
					$tmp['type'] = '<i class="fas fa-file-excel fa-2x text--green"></i>';
				} else if($item['extension_file'] == 'odt' || $item['extension_file'] == 'doc' || $item['extension_file'] == 'docx') {
					$tmp['type'] = '<i class="fas fa-file-word fa-2x text--blue"></i>';
				} else if($item['extension_file'] == 'jpg' || $item['extension_file'] == 'png') {
					$tmp['type'] = '<i class="fas fa-file-image fa-2x text--pink"></i>';
				} else if($item['extension_file'] == 'txt') {
					$tmp['type'] = '<i class="fas fa-file-alt fa-2x text--teal"></i>';
				} else if($item['extension_file'] == 'zip') {
					$tmp['type'] = '<i class="fas fa-file-archive fa-2x text--purple"></i>';
				} else {
					$tmp['type'] = '<i class="fas fa-file fa-2x text--grey"></i>';
				}
			}
            $tmp['title'] =  $item['item_name'];
			$tmp['items'] = 4;
            $tmp['size'] = $item['sizeEl'];
			if($tmp['size']) {
				if($tmp['size']) $tmp['size'] .= ' MB';
			}
            $tmp['creator'] = $item['creator'];
            $tmp['date'] = $item['created_at'];
            if($item['item_type'] == 'folder') {
                $tmp['actions'] = '<a href="'.Url::to(['/company/docs/config', 'id' => $item['id']]).'" class="action update" data-target="#modal-grid-item" data-id="'.$item['id'].'" data-table="#table-folder" data-title="Konfiguracja folderu" title="Konfiguracja folderu"><i class="fa fa-cog text--grey"></i></a>' 
                                    .'<a href="'.Url::to(['/company/docs/delete', 'id' => $item['id']]).'" class="modalConfirm" data-label="Usuń folder"><i class="fa fa-trash text--red"></i></a>';
			} else {
                $tmp['actions'] = '<a href="'.Url::to(['/company/docs/fconfig', 'id' => $item['id']]).'" class="action update" data-target="#modal-grid-item" data-id="'.$item['id'].'" data-table="#table-folder" data-title="Konfiguracja pliku" title="Konfiguracja folderu"><i class="fa fa-cog text--grey"></i></a>' 
                                    .'<a href="'.Url::to(['/company/docs/fdelete', 'id' => $item['id']]).'" class="modalConfirm" data-label="Usuń plik"><i class="fa fa-trash text--red"></i></a>';
            }
			array_push($fields, $tmp);
			$tmp = [];
		}
        /*$fields =  [
            ['type' => '<i class="fas fa-folder fa-3x text--yellow"></i>', 'title' => 'Projekty', 'items' => 4, 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],
            ['type' => '<i class="fas fa-folder fa-3x text--yellow"></i>', 'title' => 'Sprawy', 'items' => 2, 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],
            ['type' => '<i class="fas fa-file-pdf fa-3x text--red"></i>', 'title' => 'Informacja', 'items' => '', 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],
            ['type' => '<i class="fas fa-file-pdf fa-3x text--red"></i>', 'title' => 'Informacja', 'items' => '', 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],
            ['type' => '<i class="fas fa-file-pdf fa-3x text--red"></i>', 'title' => 'Informacja', 'items' => '', 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],
            ['type' => '<i class="fas fa-file-pdf fa-3x text--red"></i>', 'title' => 'Informacja', 'items' => '', 'size' => '23MB', 'creator' => 'SuperAdmin', 'date' => date('Y-m-d H:i:s'),
             'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="far fa-star'.(($value['is_starred']) ? ' text--yellow' : '').'"></i></a>'],

        ];*/
        
        return ['total' => count($fields),'rows' => $fields];
    }
	
	public function actionCreate($id) {
		
		$model = new Folder();
        $model->id_parent_fk = $id;
        $model->item_type = 2;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Folder został dodany', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_form', ['model' => $model]) ;	
		}
	}
	
	public function actionUploads($id) {
		
		$model = Folder::findOne($id);
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Pliki zostały dodane', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_uploads', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_uploads', ['model' => $model]) ;	
		}
	}
    
    public function actionBrowser($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $files = [];
        
        if($id == 0) {
            $personalFolder = Folder::find()->where(['id_parent_fk' => Yii::$app->user->id, 'item_type' => 2])->one();
            $id = $personalFolder->id;
        }
        
        $folders = Folder::find()->where(['id_parent_fk' => $id, 'item_type' => 2])->one();
        
        foreach($folders as $key => $folder) {
            array_push($files, array(
                        "name" => $folder->item_name,
                        "type" => "folder",
                        "path" => $folder->item_name,
                        "items" => [], 
                        "url" => Url::to(['/company/docs/browser', 'id' => $folder->id])  
                    ));
        }
        return  $files;
    }
    
    public function actionConfig($id) {
		
		$model = Folder::findOne($id);
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Zmiany zostały zapisane', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_config', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_config', ['model' => $model]) ;	
		}
	}
    
    public function actionDelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'delete';
        
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -2;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Folder został usunięty', 'table' => '#table-folder', 'refresh' => 'yes');	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Folder nie został usunięty', 'table' => '#table-folder', 'refresh' => 'yes' );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionFconfig($id) {
		
		$model = File::findOne($id);
        $model->scenario = 'change';
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Zmiany zostały zapisane', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_fconfig', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_fconfig', ['model' => $model]) ;	
		}
	}
    
    public function actionFdelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = File::findOne($id);
        $model->user_action = 'delete';
        
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -2;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Plik został usunięty', 'table' => '#table-folder', 'refresh' => 'yes');	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Plik nie został usunięty', 'table' => '#table-folder', 'refresh' => 'yes' );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionTarget($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $files = []; $folders = [];
        
        if($id > 0) {
            $currentFolder = Folder::findOne($id);
            
            $dataFolders = Folder::find()->where(['id_parent_fk' => $id, 'item_type' => 2, 'status' => 1])
                //->andWhere('id in (select id_folder_fk from {{%folder_users}} where status >= 1 and is_admin = 1 and id_user_fk = '.Yii::$app->user->id.')')
                ->all();
            foreach($dataFolders as $key => $folder) {
             
                array_push($folders, array(
                    "id" => $folder->id,
                    "name" => $folder->item_name,
                    "type" => "folder",
                    "path" => $folder->fullpath,
                    "length" => '', 
                    "className" => 'folders-check',
                    "url" => Url::to(['/company/docs/target', 'id' => $folder->id]),
                ));
            }
            
            $dataFiles = [];
            $breadcrumbs = '<a href="'.Url::to(['/company/docs/target', 'id' => $currentFolder->id_parent_fk]).'" class="folderUp"><i class="fa fa-level-up-alt">&nbsp;'.$currentFolder->item_name.'</a>';
        } else {
            $model = Folder::find()->where(['id_parent_fk' => 0, 'item_type' => 1, 'status' => 1])
                //->andWhere('id in (select id_folder_fk from {{%folder_users}} where status >= 1 and is_admin = 1 and id_user_fk = '.Yii::$app->user->id.')')
                ->all();
            foreach($model as $key => $folder) {
                array_push($folders, array(
                    "id" => $folder->id,
                    "name" => $folder->item_name,
                    "type" => "folder",
                    "path" => $folder->fullpath,
                    "length" => '', 
                    "className" => 'folders-check',
                    "url" => Url::to(['/company/docs/target', 'id' => $folder->id]),
                ));
            }
            $breadcrumbs = '';
        }     
     
        return  ['folders' => $folders, 'files' => $files, 'breadcrumbs' => $breadcrumbs];
    }
}
