<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyBranch;
use backend\Modules\Company\models\CompanyBranchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;


/**
 * BranchController implements the CRUD actions for CompanyBranch model.
 */
class BranchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyBranch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyBranchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsData = CompanyBranch::find()->where(['status' => 1])->all();
        			
		$fields = [];
		$tmp = [];
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/company/branch/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'"  ><i class="fa fa-pencil-alt"></i></a>';
        $actionColumn .= '<a href="/company/branch/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';
		foreach($fieldsData as $key=>$value) {
			
            $tmp['name'] = $value->name;
            $tmp['symbol'] = $value->symbol;
            $tmp['address'] = $value['address'];
			$tmp['phone'] = $value['phone'];
			$tmp['email'] = $value['email'];
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    /**
     * Displays a single CompanyBranch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompanyBranch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyBranch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionCreateajax() {
		
		$model = new CompanyBranch();
		$departments = []; $employees = [];
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="/company/branch/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
				$actionColumn .= '<a href="/company/branch/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
				$actionColumn .= '</div>';
				
                $data['name'] = $model->name;
				$data['address'] = $model['address'];
				$data['phone'] = $model['phone'];
				$data['email'] = $model['email'];
				$data['actions'] = sprintf($actionColumn, $model['id'], $model['id']);
				$data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
				$data['id'] = $model->id;
				
				return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' =>1);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing CompanyBranch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionUpdateajax($id)
    {
        $model = $this->findModel($id);
       

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="/company/branch/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
				$actionColumn .= '<a href="/company/branch/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
				$actionColumn .= '</div>';
				
                $data['name'] = $model->name;
				$data['address'] = $model['address'];
				$data['phone'] = $model['phone'];
				$data['email'] = $model['email'];
				$data['actions'] = sprintf($actionColumn, $model['id'], $model['id']);
				$data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
				$data['id'] = $model['id'];
				
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index']);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing CompanyBranch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $model->id);	
        } else {
            return array('success' => false, 'alert' => 'Dane nie zostały usunię', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyBranch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyBranch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        //$id = CustomHelpers::decode($id);
		if (($model = CompanyBranch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSet($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $listEmployees = []; $optionsEmployees = '<option value="">- wybierz -</option>';
        $listResources = []; $optionsResources = '<option value="">- wybierz -</option>';
         
		if( $id > 0 )
			$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1, 'id_company_branch_fk' => $id])->orderby('lastname,firstname')->all();  
		else		
			$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->orderby('lastname,firstname')->all();   
        foreach($employees as $key => $model) {
            $optionsEmployees .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
            /*$list .= '<li><input id="d'.$model->id.'" class="checkbox_employee" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked . $disabled.'>'
                    .'<label for="d'.$model->id.'" class="'. ( (in_array($model->id, $managers)) ? "text--blue" : "text--black" ) .'">'.$model->fullname.'</label></li>';*/
        }
        if( $id > 0 )   
			$resources = \backend\Modules\Company\models\CompanyResources::find()->where(['status' => 1, 'id_company_branch_fk' => $id])->orderby('name')->all();
        else
			$resources = \backend\Modules\Company\models\CompanyResources::find()->where(['status' => 1])->orderby('name')->all();

        foreach($resources as $key => $item) {
            $listResources[$item->type][$item->id] = $item->name;
        }
        foreach($listResources as $key => $value) {
            $optionsResources .= '<optgroup label="'.$key.'">';
            foreach($value as $i => $item) {
                $optionsResources .= '<option value="'.$i.'">'.$item.'</option>';
            }
        }
            
        return ['listEmployees' => $listEmployees, 'employees' => $optionsEmployees, 'listResources' => $listResources, 'resources' => $optionsResources];
    }
}
