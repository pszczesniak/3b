<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyReplacement;
use backend\Modules\Company\models\CompanyEmployee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\helpers\Url;

/**
 * ReplacementController implements the CRUD actions for CompanyReplacement model.
 */
class ReplacementController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all CompanyDepartment models.
     * @return mixed
     */
    public function actionIndex()  {
        /*if( count(array_intersect(["departmentPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
		
		$searchModel = new CompanyReplacement();
		//$searchModel->id_company_branch_fk = ($this->module->params['isAdmin'] == 1) ? 0 : $this->module->params['employeeBranch'];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.replacement'); 
            if($params) {
                foreach($params as $key => $value) {
                    $searchModel->$key = $value;
                }
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
			'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],
            'isSpecial' => $this->module->params['isSpecial'],
            'isAdmin' => $this->module->params['isAdmin']
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = CompanyReplacement::find()->where(['status' => 1]);
        
        if(isset($_GET['CompanyReplacement'])) {
            $params = $_GET['CompanyReplacement'];
            \Yii::$app->session->set('search.replacement', $params);
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['substitute_for']) && !empty($params['substitute_for']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("substitute_for = ".$params['substitute_for']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                //array_push($where, "event_date >= '".$params['date_from']."'");
                $fieldsDataQuery = $fieldsDataQuery->andWhere("date_from >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                //array_push($where, "event_date, '%Y-%m-%d' <= '".$params['date_to']."'");
                $fieldsDataQuery = $fieldsDataQuery->andWhere("date_to <= '".$params['date_to']."'");
            }
        } 
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];

		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<a href="'.Url::to(['/company/replacement/view/', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/replacement/updateajax/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-replacement" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/replacement/deleteajax/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-replacement"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['employee'] = $value->employee['fullname'];
            $tmp['substitute'] = $value->substitute['fullname'];
            $tmp['date_from'] = $value->date_from;
            $tmp['date_to'] = $value->date_to;
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
   
    /**
     * Creates a new CompanyResources model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()  {
        $model = new CompanyResources();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
		$model = new CompanyReplacement();
        //$model->id_company_branch_fk = $this->module->params['employeeBranch'];
		$employees = [];
        
        $isSpecial = $this->module->params['isSpecial'];
        if($this->module->params['isAdmin'])  $isSpecial = 1;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->substitute_for) $employee = CompanyEmployee::findOne($model->substitute_for);
            if($employee) {
                $model->id_substitute_branch_fk = ($employee->id_company_branch_fk) ? $employee->id_company_branch_fk : 0;
            }
			if($model->validate() && $model->save()) {
				/*$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/company/replacement/view/', 'id' => $model->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/replacement/updateajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/replacement/deleteajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
                $actionColumn .= '</div>';
				
                
				$data['employee'] = $model->employee['fullname'];
				$data['substitute'] = $model->substitute['fullname'];
                $data['date_from'] = $model->date_from;
                $data['date_to'] = $model->date_to;
                $data['actions'] = $actionColumn;
                $data['id'] = $model->id;*/
				
				
				return array('success' => true,  'action' => 'insertRow', 'index' => 0, 'alert' => 'Zastepstwo zostało dodane' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => $this->module->params['employeeBranch'],  'isSpecial' => $isSpecial, 'isAdmin' => $this->module->params['isAdmin']]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],  'isSpecial' => $isSpecial, 'isAdmin' => $this->module->params['isAdmin']]) ;	
		}
	}

    /**
     * Updates an existing CompanyDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateajax($id)  {
        $model = $this->findModel($id);
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->substitute_for) $employee = CompanyEmployee::findOne($model->substitute_for);
            if($employee) {
                $model->id_substitute_branch_fk = ($employee->id_company_branch_fk) ? $employee->id_company_branch_fk : 0;
            }
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Zastępstwo zostało zakualizowane' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin']]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin']]) ;	
		}
    }
    
    /**
     * Deletes an existing CompanyDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        //$model->status = -1;
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Zastępstwo zostało usunięte', 'id' => $model->id, 'table' => '#table-replacement');	
        } else {
            return array('success' => false,  'alert' => 'Zastępstwo nie zostało usunięte', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyResources model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyResources the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = CompanyReplacement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
