<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyEmployee;
use backend\Modules\Company\models\CompanyEmployeeSearch;
use backend\Modules\Company\models\EmployeeDepartment;
use backend\Modules\Company\models\EmployeeShare;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalCase;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * EmployeeController implements the CRUD actions for CompanyEmployee model.
 */
class EmployeeController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'CompanyEmployee' => CompanyEmployee::className()
				]
			],
		];
	}
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CompanyEmployeeSearch();
        
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.employees'); 
            if($params) {
                foreach($params as $key => $value) {
                    $searchModel->$key = $value;
                }
            }
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $id = false;
        if(isset($_GET['id'])) { $model = CalCase::findOne($_GET['id']); $id = $model->id; $searchModel->name = $model->name; }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'id' => $id
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];
        
		$fieldsDataQuery = CompanyEmployee::find()->where('status >= -1' );
        
        if(isset($_GET['CompanyEmployeeSearch'])) {
            $params = $_GET['CompanyEmployeeSearch'];
            \Yii::$app->session->set('search.employees', $params);
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(lastname) like '%".strtolower($params['lastname'])."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_employee_fk from law_employee_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_employee_kind_fk']) && !empty($params['id_dict_employee_kind_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_employee_kind_fk = ".$params['id_dict_employee_kind_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
        }
        
        if(count(array_intersect(["grantAll"], $grants)) == 0) {
			$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
            /*if($this->module->params['employeeKind'] == 70)
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			else  if($this->module->params['employeeKind'] < 70)
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id_dict_employee_kind_fk < '.$this->module->params['employeeKind'].' and id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');*/
        }  
        
        $fieldsData = $fieldsDataQuery->orderby('lastname')->all();
			
		$fields = [];
		$tmp = [];
        $types = CompanyEmployee::listTypes();

		foreach($fieldsData as $key=>$value) {
			$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['firstname'] = $value->firstname;
            $tmp['lastname'] = $value->lastname;
            $tmp['mail'] = $value->email;
            $tmp['phone'] = $value->phone;
            $tmp['empStatus'] = $value->status;
            $tmp['office'] = $value->kindname;
            $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$value->id."/preview.jpg"))?"/uploads/employees/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            $tmp['avatar'] = '<figure><img class="img-circle" alt="avatar" src="'.$avatar.'" title="avatar"></img></figure>';
			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/company/employee/view', 'id' => $value->id]).'" class="btn btn-xs btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["employeeEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/update', 'id' => $value->id]).'" class="btn btn-xs btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil-alt"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["grantAll"], $grants)) == -1 ) ?  '<a href="'.Url::to(['/company/employee/share', 'id' => $value->id]).'" class="btn btn-xs btn-'.(($value->shares)?'info':'default').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-share-alt\'></i>Udostępnianie" title="Udostępnianie"><i class="fa fa-share-alt"></i></a>': '';				
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
				$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-xs btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    /**
     * Displays a single CompanyEmployee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id); 
		$departmentsList = '';
        $filesList = '';
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';  
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        $model->files_list = ($filesList) ? '<ul>'.$filesList.'<ul>' :  'brak danych';
        
		
		return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionProfile()  {
        
        $model = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one(); 
		$departmentsList = '';
        $filesList = '';
        if($model->departments) {
            foreach($model->departments as $key => $item) {
                $departmentsList .= '<li>'.$item->department['name'] .'</li>';  
            }
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        $model->files_list = ($filesList) ? '<ul>'.$filesList.'<ul>' :  'brak danych';
        
		
		return $this->render('profile', [
            'model' => $model,
        ]);
    }
    
    public function actionViewajax($id)  {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        $model->files_list = ($filesList) ? '<ul>'.$filesList.'<ul>' :  'brak danych';
        
        return $this->renderAjax('_viewAjax', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CompanyEmployee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
		
		if( count(array_intersect(["employeeAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = new CompanyEmployee();
        $model->user_action = "create";
        $grants = $this->module->params['grants'];
        
		$departments = [];
		if (Yii::$app->request->isPost ) {
			
			$model->load(Yii::$app->request->post());
            if(isset($_POST['CompanyEmployee']['departments_list']) && !empty($_POST['CompanyEmployee']['departments_list']) ) {
                $model->id_department_fk = implode(',',$_POST['CompanyEmployee']['departments_list']);
            }
			if($model->validate() && $model->save()) {
				if(isset($_POST['CompanyEmployee']['departments_list']) && !empty($_POST['CompanyEmployee']['departments_list']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['CompanyEmployee']['departments_list'] as $key=>$value) {
                        $model_ed = new EmployeeDepartment();
                        $model_ed->id_employee_fk = $model->id;
                        $model_ed->id_department_fk = $value;
                        $model_ed->save();
                        
                       /* $department = \backend\Modules\Company\models\CompanyDepartment::findOne($value);
                        $department->id_employee_manager_fk = $model->id;
                        $department->save();*/
                        
                       // $model->id_department_fk .= $value.',';
                    }
				} 
				
				if($model->copyUser && $model->copyUser > 0) {
					$commandCase = 'insert into {{%case_employee}} (id_case_fk,id_employee_fk,is_show,created_at,created_by) '
								.'select id_case_fk, '.$model->id.' employee_fk,is_show, NOW() created_at, '.\Yii::$app->user->id.' created_by '
									.'from {{%case_employee}} where status >= 0 and id_employee_fk='.$model->copyUser;
					$resultCase = Yii::$app->db->createCommand($commandCase)->execute();
					$commandTask = 'insert into {{%task_employee}} (id_task_fk,id_employee_fk,is_show,created_at,created_by) '
								.'select id_task_fk, '.$model->id.' employee_fk,is_show, NOW() created_at, '.\Yii::$app->user->id.' created_by '
									.'from {{%task_employee}} where status >= 0 and id_employee_fk='.$model->copyUser;
					$result = Yii::$app->db->createCommand($commandTask)->execute();
					$commandBox = 'insert into {{%correspondence_employee}} (id_correspondence_fk,id_employee_fk,is_show,created_at,created_by) '
								.'select id_correspondence_fk, '.$model->id.' employee_fk,is_show, NOW() created_at, '.\Yii::$app->user->id.' created_by '
									.'from {{%correspondence_employee}} where status >= 0 and id_employee_fk='.$model->copyUser;
					$resultBox = Yii::$app->db->createCommand($commandBox)->execute();
				}
					
				
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                //Yii::$app->response->format = Response::FORMAT_JSON;
                //return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
               return  $this->render('create', [  'model' => $model, 'departments' => $departments]) ;
			}		
		} else {
			return  $this->render('create', [  'model' => $model, 'departments' => $departments]) ;	
		}
	}
    
    public function actionCreateajax() {
		
		$model = new CompanyEmployee();
		$departments = [];
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/company/employee/viewajax/%d" class="btn btn-default gridViewModal" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
                $actionColumn .= '<a href="/company/employee/updateajax/%d" class="btn btn-default  gridViewModal" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
                $actionColumn .= '<a href="/company/employee/deleteajax/%d" class="btn  btn-default remove" data-table="#table-items">U</a>';
                $actionColumn .= '</div>';
				
                $status = CalCase::listStatus('normal');
                
				$data['fisrtname'] = $model->fisrtname;
                $data['lastname'] = $model->lastname;
                $data['mail'] = $model->email;
                $data['phone'] = $model->phone;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id);
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]) ;	
		}
	}

    /**
     * Updates an existing CompanyEmployee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        if( count(array_intersect(["employeeEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        $grants = $this->module->params['grants'];
       
        $departments = []; $model->departments_list = [];
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
            array_push($model->departments_list, $item->id_department_fk);
        }
        
        $userRoles = \Yii::$app->authManager->getRolesByUser($model->id_user_fk); 
        $user = \common\models\User::findOne($model->id_user_fk);
        foreach($userRoles as $key=>$value) {
            if('$_user_'.$user->username != $key)
                $model->user_type = $key;
        }

       /* $filesList = '';
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul class="files-set">'.$filesList.'</ul>' :  '<ul class="files-set"></ul>';*/

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            /*if(isset($_POST['CompanyEmployee']['departments_list'])  && !empty($_POST['CompanyEmployee']['departments_list']) ) {
                $model->id_department_fk = implode(',',$_POST['CompanyEmployee']['departments_list']);
            }*/
			if($model->validate() && $model->save()) {
				/*EmployeeDepartment::deleteAll('id_employee_fk = :employee', [':employee' => $model->id]);
				if(isset($_POST['CompanyEmployee']['departments_list']) ) {
					
                    foreach($_POST['CompanyEmployee']['departments_list'] as $key=>$value) {
                        $model_ed = new EmployeeDepartment();
                        $model_ed->id_employee_fk = $model->id;
                        $model_ed->id_department_fk = $value;
                        $model_ed->save();
                    }
				} */
				
				return  $this->render('update', [  'model' => $model, ]) ;	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model,]) ;	
		}
    }
    
    public function actionUpdateajax($id)  {
        $model = $this->findModel($id);
        $departments = [];
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
        }

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/task/case/updateajax/%d" class="btn btn-xs btn-success btn-icon gridViewModal" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

                $actionColumn .= '<button class="btn btn-xs btn-danger remove">'.Yii::t('lsdd', 'Delete').'</button>';
                $actionColumn .= '</div>';
				
                $status = CalCase::listStatus('normal');
                
				$data['client'] = $model->client['name'];
                $data['name'] = $model->name;
                $data['status'] = $status[$model->id_dict_cate_status_fk];
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id);
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]) ;	
		}
    }


    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'lock';
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', );	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Dane nie zostały usunięte', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyEmployee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyEmployee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        //$id = CustomHelpers::decode($id);
		if (($model = CompanyEmployee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pracownik o wskazanym identyfikatorze nie został odnaleziony w systemie.');
        }
    }
	
	public function actionExport() {
		
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Pracownicy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Imię')
			->setCellValue('B3', 'Nazwisko')
			->setCellValue('C3', 'Eamil')
			->setCellValue('D3', 'Telefon')
            ->setCellValue('E3', 'Oddział')
            ->setCellValue('F3', 'Adres')
            ->setCellValue('G3', 'Stanowisko')
            ->setCellValue('H3', 'Działy');
        $objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('B3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->getStartColor()->setARGB('D9D9D9');
 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('C3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('E3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('E3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('G3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('G3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('G3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('H3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4; 
		$data = [];
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        $fieldsDataQuery = CompanyEmployee::find()->where('status >= -1' );
        
        if(isset($_GET['CompanyEmployeeSearch'])) {
            $params = $_GET['CompanyEmployeeSearch'];
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(lastname) like '%".strtolower($params['lastname'])."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_employee_fk from law_employee_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_employee_kind_fk']) && !empty($params['id_dict_employee_kind_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_employee_kind_fk = ".$params['id_dict_employee_kind_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
        }
        $grants = $this->module->params['grants'];
        if(count(array_intersect(["grantAll"], $grants)) == 0) {
			if($this->module->params['employeeKind'] == 70)
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			else  if($this->module->params['employeeKind'] < 70)
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id_dict_employee_kind_fk < '.$this->module->params['employeeKind'].' and id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        } 
            
        $data = $fieldsDataQuery->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
			
		if(count($data) > 0) {
			foreach($data as $model){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $model->firstname ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $model->lastname); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $model->email); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $model->phone); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $model->branch['name']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $model->address);
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $model->kindname);
                
                $objRichText = new \PHPExcel_RichText();
                /*$run1 = $objRichText->createTextRun('RED ');
                $run1->getFont()->setColor( new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_RED ) );

                $run2 = $objRichText->createTextRun('BLUE ');
                $run2->getFont()->setColor( new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLUE ) );*/
                
               /* $departments = []; */
                foreach($model->departments as $key => $item) {
                    //array_push($departments, $item->department['name']);  
                    $break = ($key == 0) ? '' : PHP_EOL;
                    $objRichText->createTextRun($break .$item->department['name'].(($item->department['id_employee_manager_fk'] == $model->id) ? ' (kierownik)' : ''))->getFont()->setColor( new \PHPExcel_Style_Color( ($item->department['id_employee_manager_fk'] == $model->id) ? \PHPExcel_Style_Color::COLOR_BLUE : \PHPExcel_Style_Color::COLOR_BLACK ) );
                }
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $objRichText/*implode(', ', $departments)*/);
                
                $employees = '';
                	
				$i++; 
			}  
		}
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setAutoSize(true);        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(80);
		--$i;

        $objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Pracownicy');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Pracownicy'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionEvents($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$fieldsDataQuery = CalTask::find()->where(['status' => 1]);

		$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$id.' )')->orderby('event_date desc,event_time desc');

        $fieldsData = $fieldsDataQuery->all();
        
		$fields = [];
		$tmp = [];
        $status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		      
        /*$actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/%s/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
        //$actionColumn .= '<a href="/task/event/update/%d" class="btn btn-sm btn-default  " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        //$actionColumn .= '<a href="/task/event/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';*/
		foreach($fieldsData as $key=>$value) {
			$type = ($value->type_fk == 1) ? 'case' : 'event';
			$tmp['ename'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
            $tmp['sname'] = $value->name;
            $tmp['delay'] = $value['delay'];
            $tmp['className'] = ( ( $value['id_dict_task_status_fk'] == 1 && $value->delay > 1 && $value->type_fk == 2) || ($value['id_dict_task_status_fk'] == 3 && $value->type_fk == 1) ) ?  'danger' : ( ( $value->delay > 0 && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['etype'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Rozprawa</span>' : '<span class="label bg-teal">Zadanie</span>' ;
            $tmp['deadline'] = $value['date_to'];
            $tmp['term'] = $value['event_date'].' '.( ( !empty($value['event_time']) && $value['event_time'] != '00:00' ) ? $value['event_time']: '' );
            $tmp['client'] = $value['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['estatus'] = isset($status[$value->id_dict_task_status_fk]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value->id_dict_task_status_fk].'<span>' : '';
            $tmp['category'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';

            //$tmp['actions'] = sprintf($actionColumn, $type, $value->id, $value->id, $value->id);
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
            $tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionActions($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$fieldsDataQuery = CalTask::find()->where(['status' => 1]);

		$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$id.' )')->orderby('event_date desc,event_time desc');

        $fieldsData = $fieldsDataQuery->all();
        
		$fields = [];
		$tmp = [];
        $status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		      
        /*$actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/%s/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
        //$actionColumn .= '<a href="/task/event/update/%d" class="btn btn-sm btn-default  " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        //$actionColumn .= '<a href="/task/event/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';*/
		foreach($fieldsData as $key=>$value) {
			$type = ($value->type_fk == 1) ? 'case' : 'event';
			$tmp['ename'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
            $tmp['sname'] = $value->name;
            $tmp['delay'] = $value['delay'];
            $tmp['className'] = ( ( $value['id_dict_task_status_fk'] == 1 && $value->delay > 1 && $value->type_fk == 2) || ($value['id_dict_task_status_fk'] == 3 && $value->type_fk == 1) ) ?  'danger' : ( ( $value->delay > 0 && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['etype'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Rozprawa</span>' : '<span class="label bg-teal">Zadanie</span>' ;
            $tmp['deadline'] = $value['date_to'];
            $tmp['term'] = $value['event_date'].' '.( ( !empty($value['event_time']) && $value['event_time'] != '00:00' ) ? $value['event_time']: '' );
            $tmp['client'] = $value['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['estatus'] = isset($status[$value->id_dict_task_status_fk]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value->id_dict_task_status_fk].'<span>' : '';
            $tmp['category'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';

            //$tmp['actions'] = sprintf($actionColumn, $type, $value->id, $value->id, $value->id);
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
            $tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionCases($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $grants = $this->module->params['grants'];

		$fieldsDataQuery =  CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$id.' )');
           
        $fieldsData = $fieldsDataQuery->orderby('name')->all();
			
			
		$fields = [];
		$tmp = [];
        $status = CalCase::listStatus('advanced');
        $status[6] = 'Scalona';
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/%s/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
        //$actionColumn .= '<a href="/panel/task/case/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        //$actionColumn .= '<a href="/panel/task/case/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($fieldsData as $key=>$value) {
			$type = ($value->type_fk == 1) ? 'matter' : 'project';
			$tmp['customer'] = $value->customer['name'];
            $tmp['type'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Sprawa</span>' : '<span class="label bg-teal">Projekt</span>' ;
			$tmp['name'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
            $tmp['sname'] = $value->name;
            $tmp['status'] = '<label class="label '.( ($value['status'] == 1) ? $colors[$value['id_dict_case_status_fk']] : 'bg-red').'">'.( ($value['status'] == 1) ? $status[$value['id_dict_case_status_fk']] : 'Usunięta').'</label>';
            //$tmp['actions'] = sprintf($actionColumn, $type, $value->id, $value->id, $value->id);
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
                //$actionColumn .= '<a href="/panel/task/case/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
                //$actionColumn .= '<a href="/panel/task/case/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
            $tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
	
	public function actionLock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = -1;
        $model->user_action = 'lock';
        
        $user = \common\models\User::findOne($model->id_user_fk);
        $user->status = 0;
		$status = 'unlock';
		$grants = $this->module->params['grants'];
        
        $actionColumn = '<div class="edit-btn-group">';
			$actionColumn .= '<a href="'.Url::to(['/company/employee/view', 'id' => $model->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
			$actionColumn .= ( count(array_intersect(["employeeEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/update', 'id' => $model->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil-alt"></i></a>': '';
			$actionColumn .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $model->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$actionColumn .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $model->id]).'" class="btn btn-sm btn-default unlock" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
		$actionColumn .= '</div>';
		
		/*$actionColumn = '<div class="edit-btn-group"><a href="/company/employee/viewajax/'.$id.'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
        $actionColumn .= '<a href="/company/employee/updateajax/'.$id.'" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
        $actionColumn .= '<a href="/company/employee/deleteajax/'.$id.'" class="btn btn-sm  btn-default remove" data-table="#table-items">U</a>';
        $actionColumn .= '<a href="/company/employee/unlock/'.$id.'" class="btn btn-sm btn-default unlock" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-unlock\'></i>Odblokuj" >O</a></div>';
        */
        if($user->save() && $model->save()) {
            $result = \backend\Modules\Company\models\EmployeeDepartment::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id); 
           // \backend\Modules\Task\models\CaseEmployee::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id);
           // \backend\Modules\Task\models\TaskEmployee::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id);
           // \backend\Modules\Correspondence\models\CorrespondenceEmployee::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id);
            return array('success' => true, 'alert' => 'Pracownik <b>'.$model->fullname.'</b> został zablokowany', 'buttons' => $actionColumn, 'table' => '#table-employees', 'refresh' => 'yes', 'index' => (isset($_GET['index'])) ? $_GET['index'] : 0);	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Pracownik <b>'.$model->fullname.'</b> nie został zablokowany',  );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = 1;
        $model->user_action = 'lock';
        
        $user = \common\models\User::findOne($model->id_user_fk);
        $user->status = 10;
        $status = 'lock';
		$grants = $this->module->params['grants'];
        /*$actionColumn = '<div class="edit-btn-group"><a href="/company/employee/viewajax/'.$id.'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
        $actionColumn .= '<a href="/company/employee/updateajax/'.$id.'" class="btn btn-sm btn-default  " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
        $actionColumn .= '<a href="/company/employee/deleteajax/'.$id.'" class="btn btn-sm  btn-default remove" data-table="#table-items">U</a>';
        $actionColumn .= '<a href="/company/employee/lock/'.$id.'" class="btn btn-sm btn-default lock" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-lock\'></i>Zablokuj" >B</a></div>';
         */
		$actionColumn = '<div class="edit-btn-group">';
			$actionColumn .= '<a href="'.Url::to(['/company/employee/view', 'id' => $model->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
			$actionColumn .= ( count(array_intersect(["employeeEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/update', 'id' => $model->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil-alt"></i></a>': '';
			$actionColumn .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $model->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$actionColumn .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $model->id]).'" class="btn btn-sm btn-default lock" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
		$actionColumn .= '</div>';
        if($user->save() && $model->save()) {
            //\backend\Modules\Company\models\EmployeeDepartment::updateAll(['status' => 1], 'status = 0 and id_employee_fk = '.$model->id);
            //\backend\Modules\Task\models\CaseEmployee::updateAll(['status' => 1], 'status = 0 and id_employee_fk = '.$model->id);
            //\backend\Modules\Task\models\TaskEmployee::updateAll(['status' => 1], 'status = 0 and id_employee_fk = '.$model->id);
            //\backend\Modules\Correspondence\models\CorrespondenceEmployee::updateAll(['status' => 1], 'status = 0 and id_employee_fk = '.$model->id);
            return array('success' => true, 'alert' => 'Pracownik <b>'.$model->fullname.'</b> został odblokowany', 'buttons' => $actionColumn, 'table' => '#table-employees', 'refresh' => 'yes', 'index' => (isset($_GET['index'])) ? $_GET['index'] : 0 );	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Pracownik <b>'.$model->fullname.'</b> nie został odblokowany',);	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionTasks() {
	
	    $type = 'firm';
		if( isset($_GET['type']) && !empty($_GET['type']) ) $type = $_GET['type'];
        
        $id = $this->module->params['employeeId'];
        if( isset($_GET['id']) && !empty($_GET['id']) ) $id = $_GET['id'];
        
        Yii::$app->response->format = Response::FORMAT_JSON;
		
		$tasksList = '<option value=""> - wybierz - </option>';
        
        if($type == 'personal') {
            $events = \backend\Modules\Task\models\CalTodo::find()->where(['status' => 1, 'is_close' => 0, 'id_employee_fk' => $id])->orderby('name')->all();            
            
			foreach($events as $key => $event) {
                $tasksList .= '<option value="'.$event->id.'">'.$event->name.'</option>';
            }
        } else {

            $events = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1])->andWhere('id_dict_task_status_fk = 1 and id in (select id_task_fk from {{%task_employee}} where status = 1 and id_employee_fk = '.$id.')')->orderby('name')->all();            
            
            $cases = [];
            $tasks = [];
            foreach($events as $key => $value) {
                if($value->type_fk == 1) 
                    $cases[$value->id] = $value->name;
                else
                    $tasks[$value->id] = $value->name;
            }
            
            if( count($cases) > 0 ) {
                $tasksList .= '<optgroup label="Rozprawy">';
                foreach($cases as $key => $value) {
                    $tasksList .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
            if( count($tasks) > 0 ) {
                $tasksList .= '<optgroup label="Zadania">';
                foreach($tasks as $key => $value) {
                    $tasksList .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
        }
        
        return ['list' => $tasksList];
    }
    
    public function actionInfo($id) {
	    
        Yii::$app->response->format = Response::FORMAT_JSON;
	    //$model = $this->findModel($id);
        $model = \backend\Modules\Company\models\CompanyEmployee::findOne($id);
        $departments = '<option value="">- wszystko -</option>';
		if($model) {
			//foreach($model->departments as $key => $item) {
			$query = \backend\Modules\Company\models\EmployeeDepartment::find()->where(['id_employee_fk' => $id, 'status' => 1]);
		    if(!$this->module->params['isAdmin'] && !$this->module->params['isSpecial'])
				$query->andWhere('id_department_fk in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$this->module->params['employeeId'].')');
			$models = $query->all(); 
			foreach($models as $key => $item) {
				$departments .= '<option value="'.$item->id_department_fk.'">'.$item->department['name'].'</option>';
			}
	    } else {
			$models = \backend\Modules\Company\models\CompanyDepartment::getList();
			foreach($models as $key => $item) {
				$departments .= '<option value="'.$item->id.'">'.$item->name.'</option>';
			}
		}
        
        $list_sets = ''; $list_events = ''; $cases = []; $tasks = [];
        if(isset($_GET['date'])) {
			$eventsData = \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $_GET['cid']])->andWhere("id_dict_task_status_fk != 3 and event_date >= '".date('Y-m-d', $_GET['date'])."'")->orderby('type_fk,name')->all();
			foreach($eventsData as $key => $value) {
				if($value->type_fk == 1) 
					$cases[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
				else
					$tasks[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
			}
        
			if( count($cases) > 0 ) {
				$list_events .= '<optgroup label="Rozprawy">';
				foreach($cases as $key => $value) {
					$list_events .= '<option value="'.$key.'">'.$value.'</option>';
				}
			}
			if( count($tasks) > 0 ) {
				$list_events .= '<optgroup label="Zadania">';
				foreach($tasks as $key => $value) {
					$list_events .= '<option value="'.$key.'">'.$value.'</option>';
				}
			}
	    }
        
        if( isset($_GET['customer']) && !empty($_GET['customer']) ) {
            $sets = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $_GET['customer']])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$id.')')->orderby('name')->all();
            $matters = [];
            $projects = [];
            foreach($sets as $key => $value) {
                if($value->type_fk == 1) 
                    $matters[$value->id] = $value->name;
                else
                    $projects[$value->id] = $value->name;
            }
            
            $list_sets .= '<option value=""> - wybierz - </option>';
            if( count($matters) > 0 ) {
                $list_sets .= '<optgroup label="Sprawy">';
                foreach($matters as $key => $value) {
                    $list_sets .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
            if( count($projects) > 0 ) {
                $list_sets .= '<optgroup label="Projekty">';
                foreach($projects as $key => $value) {
                    $list_sets .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
        }
        //$stats = \Yii::$app->runAction('/task/personal/personalStats', ['employeeId' => $id, 'date' => $_GET['date']]);
        if(isset($_GET['date']))
			$stats = \app\Modules\Task\controllers\PersonalController::pStats($id, date('Y-m-d', $_GET['date']));
        else
			$stats = [];
        return ['departments' => $departments, 'sets' => $list_sets, 'events' => $list_events, 'stats' => $stats];
    }
    
    public function actionContacts() {
    
        $searchModel = new CompanyEmployeeSearch();
        
        //$queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        //$queryParams["CompanyEmployeeSearch"]["status"] = 1 ;
        //$dataProvider = $searchModel->search($queryParams);
        /* $sql = "SELECT distinct SUBSTRING(lastname, 1, 1) as filter FROM law_company_employee order by lastname collate `utf8_polish_ci`";
        $filter = Yii::$app->db->createCommand($sql)->query();*/
        //var_dump($dataProvider->getModels());exit;
        $filter = [];
        $employeesQuery = CompanyEmployee::find()->where(['status' => 1]);
        if(Yii::$app->request->isPost){ 
            
            if( isset($_POST["CompanyEmployeeSearch"]) && !empty($_POST["CompanyEmployeeSearch"]) ) {
                $queryParams = $_POST["CompanyEmployeeSearch"];
                foreach($queryParams as $key => $value) {
                    if($key == 'id_company_branch_fk' && !empty($value) ) {
                        $employeesQuery->andWhere('id_company_branch_fk = ' . $value);
                        $searchModel["id_company_branch_fk"] = $value ;
                    }
                    if($key == 'id_dict_employee_type_fk' && !empty($value) ) {
                        $employeesQuery->andWhere('id_dict_employee_type_fk = ' . $value);
                        $searchModel["id_dict_employee_type_fk"] = $value ;
                    }
                    if($key == 'id_department_fk' && !empty($value) ) {
                        $employeesQuery->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk = ' . $value . ')');
                        $searchModel["id_department_fk"] = $value ;
                    }
                }
            }
        }
        //$employees = $dataProvider->getModels();
        $employees = $employeesQuery->orderby('lastname collate `utf8_polish_ci`')->all();
        foreach($employees as $key => $employee) {
            $char = mb_substr($employee->lastname,0,1,'UTF-8');
            if(!in_array($char, $filter)) array_push($filter, $char);
        }
        return  $this->render('contacts', [ 'employees' => $employees, 'filter' => $filter, 'searchModel' => $searchModel ]) ;	
    }
    
    public function actionEmployeesbytype($id) {
	    //$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
	    //$model = $this->findModel($id);
        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1, 'id_dict_employee_type_fk' => $id])->orderby('lastname')->all();
        $employees = '<option value="">-- wybierz pracownika --</option>';
        foreach($employeesData as $key => $item) {
            $employees .= '<option value="'.$item->id.'">'.$item->fullname.'</option>';
        }
        
        return ['employees' => $employees];
    }
    
    public function actionShare($id)  {
        $model = $this->findModel($id);
        $model->user_action = 'share';
        $shares_1 = EmployeeShare::find()->where(['id_from_fk' => $model->id, 'mode' => 1, 'status' => 1])->all();
        $shares_1_temp = [];
        foreach($shares_1 as $key => $item) {
            array_push($model->share_mode_1, $item->id_to_fk);
            array_push($shares_1_temp, $item->id_to_fk);
        }
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
	            foreach($shares_1 as $key => $item) {
                    if(!$model->share_mode_1)  $model->share_mode_1 = [];
                    if(!in_array($item->id_to_fk, $model->share_mode_1)) {
                        $item->status = -1;
                        $item->save();
                    }
                }
                if($model->share_mode_1) {
                    foreach($model->share_mode_1 as $key => $value) {
                        if(!in_array($value, $shares_1_temp)) {
                            $new = new EmployeeShare();
                            $new->id_from_fk = $model->id;
                            $new->id_to_fk = $value;
                            $new->status = 1;
                            $new->mode = 1;
                            $new->save();
                        }
                    }
                }
				return array('success' => true, 'table' => '#table-employees' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formShare', [  'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formShare', [  'model' => $model]) ;	
		}
    }
    
    public function actionOffers($id) {
		//$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['Offer'])) { 
            $params = $_GET['Offer']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  
			
		$sortColumn = 'o.created_at';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.name as name','o.created_at', 'o.state', "concat_ws(' ', ce.lastname, ce.firstname) as empname", "concat_ws(' ', u.lastname, u.firstname) as creator",
                      "concat_ws(' ', cu.lastname, cu.firstname) as contact", 'o.id as amount_net', 'p.name as pname', 'p.id as pid', 'c.name as cname', 'c.id as cid'])
            ->from('{{%offer}} as o')
            ->join('JOIN', '{{%customer}} as c', 'o.id_customer_fk=c.id')
            ->join('JOIN', '{{%user}} as u', 'o.created_by=u.id')
            ->join('LEFT JOIN', '{{%company_employee}} as ce', 'o.id_employee_fk=ce.id')
            ->join('LEFT JOIN', '{{%customer_person}} as cu', 'o.id_contact_fk=cu.id')
            ->join('LEFT JOIN', '{{%cal_case}} as p', 'o.id_project_fk=p.id')
            ->where( ['o.status' => 1, 'o.id_employee_fk' => $id] );
            //->groupby(["i.id", "system_no", "o.id", "o.name", "o.id_customer_fk", "c.name"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
        $presentationAll = \backend\Modules\Offer\models\Offer::Viewer();
		foreach($rows as $key=>$value) {
            $value['state'] = ($value['state']) ? $value['state'] : 1;
			$presentation = $presentationAll[$value['state']];
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/sale/offer/view', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd oferty').'" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
           /* $tmp['symbol'] = $value['system_no'].'<br /><small><a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a></small>'
                            .(($value['cid'] != $id) ? '<br /><small><a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" class="text--pink">'.$value['cname'].'<a></small>': '');*/
			$tmp['name'] = $value['name'];
            $tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['cname'].'</a>';
            if($value['pid'])
                $tmp['client'] .= '<br /><small class="text--purple"><a href="'.Url::to(['/task/project/view', 'id' => $value['pid']]).'">'.$value['pname'].'</a></small>';
            $tmp['employee'] = $value['empname'];
            $tmp['contact'] = $value['contact'];
            $tmp['created'] = $value['created_at'].'<br /><small>'.$value['creator'].'</small>';
            $tmp['amount_net'] = number_format ($value['amount_net'], 2, "," ,  " ");
            $tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
}
