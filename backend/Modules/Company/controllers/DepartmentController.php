<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyDepartment;
use backend\Modules\Company\models\CompanyDepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\components\CustomHelpers;


/**
 * DepartmentController implements the CRUD actions for CompanyDepartment model.
 */
class DepartmentController extends Controller
{
     public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = CompanyDepartment::find()->where(['status' => 1]);
        
        if(isset($_GET['CompanyDepartmentSearch'])) {
            $params = $_GET['CompanyDepartmentSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
        }
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/company/department/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/company/department/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil-alt"></i></a>';
        $actionColumn .= '<a href="/company/department/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';
		foreach($fieldsData as $key=>$value) {
			
			$tmp['name'] = $value->name;
            $tmp['symbol'] = $value->symbol;
            $tmp['manager'] = $value->manager;
            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['notice'] = ($value->notification_email == 1) ? '<i class="fa fa-bell text--green"></i>' : '<i class="fa fa-bell-slash text--grey"></i>';
            $tmp['all'] = ($value->all_employee == 1) ? '<i class="fa fa-cogs text--orange"></i>' : '';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    /**
     * Lists all CompanyDepartment models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["departmentPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$searchModel = new CompanyDepartmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Displays a single CompanyDepartment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewajax($id)
    {
        $model = $this->findModel($id);
        $employeeList = '';
        foreach($model->employees as $key => $item) {
            $employeeList .= '<li>'.$item->employee['lastname'] . ' '.$item->employee['firstname'].'</li>';
        }
        $model->employees_list = ($employeeList) ? '<ul>'.$employeeList.'<ul>' :  'brak danych';
        return $this->renderAjax('_viewAjax', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CompanyDepartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyDepartment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
		$model = new CompanyDepartment();
		$employees = [];
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/company/department/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
                $actionColumn .= '<a href="/company/department/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil-alt"></i></a>';
                $actionColumn .= '<a href="/company/department/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
                $actionColumn .= '</div>';
				
                
				$data['name'] = $model->name;
				$data['notice'] = ($model->notification_email == 1) ? '<i class="fa fa-bell text--green"></i>' : '<i class="fa fa-bell-slash text--grey"></i>';
                $data['all'] = ($model->all_employee == 1) ? '<i class="fa fa-gears text--orange"></i>' : '';
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' => 0, 'alert' => 'Dział <b>'.$model->name.'</b> został dodany', 'refresh' => 'yes', 'table' => '#table-data' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'employees' => $employees]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'employees' => $employees]) ;	
		}
	}

    /**
     * Updates an existing CompanyDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateajax($id)
    {
        $model = $this->findModel($id);
        $employees = [];
        $employeeList = '';
        foreach($model->employees as $key => $item) {
            array_push($employees, $item->id_employee_fk);
            $employeeList .= '<li>'.$item->employee['lastname'] . ' '.$item->employee['firstname'].'</li>';
        }
        $model->employees_list = ($employeeList) ? '<ul>'.$employeeList.'<ul>' :  'brak danych';
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/company/department/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
                $actionColumn .= '<a href="/company/department/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
                $actionColumn .= '<a href="/company/department/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
                $actionColumn .= '</div>';
                
                $data['name'] = $model->name;
				$data['manager'] = $model->manager;
				$data['notice'] = ($model->notification_email == 1) ? '<i class="fa fa-bell text--green"></i>' : '<i class="fa fa-bell-slash text--grey"></i>';
                $data['all'] = ($model->all_employee == 1) ? '<i class="fa fa-gears text--orange"></i>' : '';
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Dział <b>'.$model->name.'</b> został zakualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'employees' => $employees]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'employees' => $employees]) ;	
		}
    }


    /**
     * Deletes an existing CompanyDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Dział <b>'.$model->name.'</b> został usunięty', 'id' => $model->id);	
        } else {
            return array('success' => false,  'alert' => 'Dział <b>'.$model->name.'</b> nie został usunięty', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyDepartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyDepartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        //$id = CustomHelpers::decode($id);
		if (($model = CompanyDepartment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionEmployees() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = ''; $dropdown = '<option value="">-- wybierz --</option>';
        $checked = '';
        $ids = ( isset($_POST['ids'] ) && !empty($_POST['ids'])) ? $_POST['ids'] : '0';
        $box = ( isset($_GET['box']) && !empty($_GET['box']) ) ? $_GET['box'] : 0;
        $checkedEmployees = (isset($_POST['employees']) && !empty($_POST['employees'])) ? explode(',', $_POST['employees']) : [];
        
        $specialDepartmens = [];
        $specialDepartmensData = \backend\Modules\Company\models\CompanyDepartment::getSpecials();
        foreach($specialDepartmensData as $key => $value) {
            array_push($specialDepartmens, $value->id);
        }
        if(isset($_GET['type']) && $_GET['type'] == 'matter' ) {
            $departments = explode(',', $ids);
            foreach( $departments as $key => $value ) {
                if( in_array($value, $specialDepartmens) ) {
                    unset($departments[$key]);
                }
            }
            $ids = implode(',',$departments);
            $ids = (isset($ids) && !empty($ids)) ? $ids : '0';
        }

        $managers = \backend\Modules\Company\models\CompanyDepartment::getManagers();
        
        /*$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();*/
        
		//$ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
        $specialEmployees = [];
        if( isset($_GET['box']) && $_GET['box'] == 1) {
            $company = \backend\Modules\Company\models\Company::findOne(1);
            $customData = \yii\helpers\Json::decode($company->custom_data);
            
            if( isset($customData['box']) ) {
                $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
            } else {
                $company->box_special_emails = '';
                $company->box_special_employees = [ 0 => 0 ];
            }
            $specialEmployees = \backend\Modules\Company\models\CompanyDepartment::getSpecialEmployees();
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->andWhere('id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.$ids.'))')->orderby('lastname')->all();        
        } else if(isset($_GET['list']) && $_GET['list'] == 'acc') {
			//$dropdown = '';
			if($_GET['did'] == 0)
				$employees = \backend\Modules\Company\models\CompanyEmployee::getListAcc((isset($_GET['estatus'])?$_GET['estatus']:0));
			else
				$employees = \backend\Modules\Company\models\CompanyEmployee::getListAccByDepartment($_GET['did'], (isset($_GET['estatus'])?$_GET['estatus']:0));
		} else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.$ids.'))')->orderby('lastname')->all();        
        }
        foreach($employees as $key => $model) {
            if($model->id == $this->module->params['employeeId'] || in_array($model->id, $managers) || in_array($model->id, $specialEmployees) || in_array($model->id, $checkedEmployees) )  $checked = 'checked'; else $checked = '';
            $list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'>'
                    .'<label for="d'.$model->id.'" class="'. ( (in_array($model->id, $managers)) ? "text--blue" : ( (in_array($model->id, $specialEmployees)) ? "text--pink" : "text--black") ) .'">'.$model->fullname.'</label></li>';
            $dropdown .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
		
		$departments = '<option value="">- wszystko -</option>';

		$dmodels = \backend\Modules\Company\models\CompanyDepartment::getList();
		foreach($dmodels as $key => $item) {
			$departments .= '<option value="'.$item->id.'">'.$item->name.'</option>';
		}
        
        return ['list' => $list, 'dropdown' => $dropdown, 'departments' => $departments];
    }
}
