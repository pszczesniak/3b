<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyEmployee;
use backend\Modules\Company\models\CompanyEmployeeSearch;
use backend\Modules\Company\models\EmployeeDepartment;

use app\Modules\Admin\models\AuthItem;
use app\Modules\Admin\models\searchs\AuthItem as AuthItemSearch;
use yii\rbac\Item;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;


/**
 * AdminController implements the CRUD actions for CompanyEmployee model.
 */
class AdminController extends Controller
{
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'CompanyEmployee' => CompanyEmployee::className()
				]
			],
		];
	}
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyEmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $searchModelGroup = new AuthItemSearch(['type' => Item::TYPE_ROLE]);
        $dataProviderGroup = $searchModelGroup->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'searchModelGroup' => $searchModelGroup,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = CompanyEmployee::find()->where(['status' => 1]);
        
        if(isset($_GET['CompanyEmployeeSearch'])) {
            $params = $_GET['CompanyEmployeeSearch'];
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(lastname) like '%".strtolower($params['lastname'])."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_employee_fk from law_employee_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_employee_kind_fk']) && !empty($params['id_dict_employee_kind_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_employee_kind_fk = ".$params['id_dict_employee_kind_fk']);
            }
        }
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
        $types = CompanyEmployee::listTypes();
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/company/employee/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
        $actionColumn .= '<a href="/company/employee/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
        $actionColumn .= '<a href="/company/employee/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items">U</a>';
        $actionColumn .= '</div>';
		foreach($fieldsData as $key=>$value) {
			
			$tmp['firstname'] = $value->firstname;
            $tmp['lastname'] = $value['lastname'];
            $tmp['mail'] = $value->email;
            $tmp['phone'] = $value->phone;
            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    /**
     * Displays a single CompanyEmployee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewajax($id)
    {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        $model->files_list = ($filesList) ? '<ul>'.$filesList.'<ul>' :  'brak danych';
        
        return $this->renderAjax('_viewAjax', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CompanyEmployee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyEmployee();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
		$model = new CompanyEmployee();
		$departments = [];
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				if(isset($_POST['departments']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('departments') as $key=>$value) {
                        $model_ed = new EmployeeDepartment();
                        $model_ed->id_employee_fk = $model->id;
                        $model_ed->id_department_fk = $value;
                        $model_ed->save();
                    }
				} 
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/company/employee/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
                $actionColumn .= '<a href="/company/employee/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
                $actionColumn .= '<a href="/company/employee/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items">U</a>';
                $actionColumn .= '</div>';
				
                $data['firstname'] = $model->firstname;
                $data['lastname'] = $model->lastname;
                $data['mail'] = $model->email;
                $data['phone'] = $model->phone;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' => 0, 'alert' => $model->custom_data);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]) ;	
		}
	}

    /**
     * Updates an existing CompanyEmployee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateajax($id)
    {
        $model = $this->findModel($id);
        $departments = [];
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
        }
        $filesList = '';
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul class="files-set">'.$filesList.'</ul>' :  '<ul class="files-set"></ul>';

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				EmployeeDepartment::deleteAll('id_employee_fk = :employee', [':employee' => $id]);
				if(isset($_POST['departments']) ) {
					
                    foreach(Yii::$app->request->post('departments') as $key=>$value) {
                        $model_ed = new EmployeeDepartment();
                        $model_ed->id_employee_fk = $model->id;
                        $model_ed->id_department_fk = $value;
                        $model_ed->save();
                    }
				} 
				
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/company/employee/viewajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >W</a>';
                $actionColumn .= '<a href="/company/employee/updateajax/%d" class="btn btn-sm btn-default  update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
                $actionColumn .= '<a href="/company/employee/deleteajax/%d" class="btn btn-sm  btn-default remove" data-table="#table-items">U</a>';
                $actionColumn .= '</div>';
				
                 $data['firstname'] = $model->firstname;
                $data['lastname'] = $model->lastname;
                $data['mail'] = $model->email;
                $data['phone'] = $model->phone;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'] );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments]) ;	
		}
    }


    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', );	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunięte', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyEmployee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyEmployee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyEmployee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	

    
    public function actionTreestructure($id) {
        $employee = CompanyEmployee::findOne($id);
        $grants = $employee->grants;
        
        $searchModel = new AuthItemSearch(['type' => Item::TYPE_PERMISSION]);
        $dataProvider = $searchModel->search(Yii::$app->getRequest()->getQueryParams());//var_dump($dataProvider->allModels); exit;
        $permissions = $dataProvider->allModels;
        
        
        
        if(count($permissions) > 0) {
            foreach($permissions as $key => $value) {
                $selected = (in_array($value->name, $grants))?"selected='selected'":"";
                echo '<option value="'.$value->name.'" data-section="Uprawnienia" data-index="'.$value->name.'" '.$selected.' >'.$value->description.'</option>';

            }
        } else {
            echo false;
        }
    }
    
    public function actionGrants($id) {
        $employee = CompanyEmployee::findOne($id);
        $user = $employee->user;
        $grants = $employee->grants;
        
        $role = false;
        
        $auth = Yii::$app->authManager;
        $roleGrants = [];
        //$roleUser = $auth->getRole('$_user_'.$user->username);
		$userGrants = $auth->getPermissionsByRole('$_user_'.$user->username);
		$userRoles = \Yii::$app->authManager->getRolesByUser($user->id); 
		// var_dump($loggedUserRole); exit;
		foreach($userRoles as $key=>$value) {
			if('$_user_'.$user->username != $key) {
                $roleGrants = $auth->getPermissionsByRole($key);
                $role = $value->description;
            }
		}
        
        return $this->renderPartial('_formGrants', [
                'roleGrants' => $roleGrants, 'userGrants' => $userGrants, 'roleLoggedUserGrants' => [], 'role' => $role, 'user' => $user
            ]);
    }
    
    public function actionGgrants() {
        /*$employee = CompanyEmployee::findOne($id);
        $user = $employee->user;
        $grants = $employee->grants;*/
        
        $role = '';
        
        $auth = Yii::$app->authManager;
        $roleGrants = $auth->getPermissionsByRole(/*$user->role->name*/$_POST['role']); 
        
        return $this->renderPartial('_formGroupGrants', [
                'roleGrants' => $roleGrants, 'role' => $_POST['role'], 'userGrants' => [], 'roleLoggedUserGrants' => []
            ]);
    }
    
    public function actionGrantsuser($user) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = Yii::$app->getRequest()->post();
		
		$auth = Yii::$app->authManager;
   	    $user =  \common\models\User::findOne($user);
		$role = $auth->getRole('$_user_'.$user->username);
		if(!$role) {
			$role = $auth->createRole('$_user_'.$user->username);
			$auth->add($role);
			$auth->assign($role, $user->id);
		}
		$auth->removeChildren($role);
		if(isset($post['grant'])) {
            foreach($post['grant'] as $key => $value) {
                $grant = $auth->getPermission($value);
                // var_dump($grant);
                if($grant)
                    $auth->addChild($role, $grant);
                $grant = false;
                //echo $key.' => '.$value.'<br />';
            }
		}
		//$auth->addChild($role, $grant);
        //$auth->assign($role, $user->getId());
		$res = array('result' => true, 'success' => 'Zmiany zostały zapisane');
		return $res;
	}
    
    public function actionGrantsrole() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = Yii::$app->getRequest()->post();
		
		$auth = Yii::$app->authManager;
		$role = $auth->getRole($_GET['role']);
		if(!$role) {
			$role = $auth->createRole($_GET['role']);
			$auth->add($role);
		}
		$auth->removeChildren($role);
		if(isset($post['grant'])) {
            foreach($post['grant'] as $key => $value) {
                $grant = $auth->getPermission($value);
                if($grant)
                    $auth->addChild($role, $grant);
                $grant = false;
                //echo $key.' => '.$value.'<br />';
            }
		}
		//$auth->addChild($role, $grant);
        //$auth->assign($role, $user->getId());
		$res = array('result' => true, 'success' => 'Zmiany zostały zapisane');
		return $res;
	}
}
