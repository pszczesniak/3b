<?php

namespace app\Modules\Company\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\web\UploadedFile;

use backend\Modules\Folder\models\Folder;
use backend\Modules\Folder\models\FolderUser;
use backend\Modules\Folder\models\File;
use backend\Modules\Folder\models\FileUser;

/**
 * Option controller for the `Folder` module
 */
class OptionController extends Controller
{
    /**
     * Renders the move view for the module
     * @return string
     */
    public function actionMove() {
        
        if(!isset($_GET['ids']) || empty($_GET['ids'])) {
            return '<div class="modal-body"><div class="alert alert-danger">Proszę wybrać obiekty do przeniesienia</div></div>';
        }
        $folders = [];
        $dataFolders = Folder::find()->where(['id_parent_fk' => 0, 'item_type' => 2, 'status' => 1])
                //->andWhere('id in (select id_folder_fk from {{%folder_users}} where status >= 1 and is_admin = 1 and id_user_fk = '.Yii::$app->user->id.')')
                ->all();

        foreach($dataFolders as $key => $folder) {
            array_push($folders, ['id' => $folder->id, 'name' => $folder->item_name, 'items' => 2, 'path' => $folder->fullpath]);
        }
            
        $model = new \frontend\Modules\Company\models\ActionsGroup();
        $model->objects = isset($_GET['ids']) ? explode(',', $_GET['ids']) : [];
        $model->user_action = 'move';
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->moveObjects()) {
				return array('success' => true, 'alert' => 'Obiekty zostały przeniesione', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderPartial('move', ['model' => $model, 'folders' => $folders, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return $this->renderPartial('move', ['model' => $model, 'folders' => $folders, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]);
		} 
    }
    
    public function actionSdelete() {
        
        if(!isset($_GET['ids']) || empty($_GET['ids'])) {
            return '<div class="modal-body"><div class="alert alert-danger">Proszę wybrać obiekty do usunięcia</div></div>';
        }
                   
        $model = new \frontend\Modules\Company\models\ActionsGroup();
        $model->objects = isset($_GET['ids']) ? explode(',', $_GET['ids']) : [];
        $model->user_action = 'delete';
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->deleteObjects()) {
				return array('success' => true, 'alert' => 'Obiekty zostały usunięte', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderPartial('delete', ['model' => $model, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return $this->renderPartial('delete', ['model' => $model, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]);
		} 
    }
    
    public function actionBrowser($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $files = [];
        
        if($id == 0) {
            array_push($files, array(
                        "name" => 'Folder osobisty',
                        "type" => "folder",
                        "path" => 'Folder osobisty',
                        "items" => [], 
                        "url" => Url::to(['/folder/personal/'])  
                    ));
            
            array_push($files, array(
                        "name" => 'Foldery współdzielone',
                        "type" => "folder",
                        "path" => 'Folder współdzielone',
                        "items" => [] 
                    ));
        }
        return  $files;
    }
    
    public function actionStarredfolder($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = FolderUser::find()->where(['id_folder_fk' => $id, 'id_user_fk' => Yii::$app->user->id])->andWhere('status >= 1')->one();
        $model->user_action = 'starred';
        $model->is_starred = ($model->is_starred) ? 0 : 1;
        $model->save();
        //$sqlStarred = "update {{%folder_users}} set is_starred = ".(($model->is_starred) ? 0 : 1)." where status = 1 and id_folder_fk = ".$id." and id_user_fk = ".Yii::$app->user->id;
        //\Yii::$app->db->createCommand($sqlStarred)->execute();
        
        $star = '<a href="'.Url::to(['/folder/option/starredfolder', 'id' => $id]).'" class="action starred" data-id="'.$id.'" data-table="#table-folder" data-label="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').'" title="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="'.(($model->is_starred) ? 'fas ' : 'far ').' fa-star'.(($model->is_starred) ? ' text--yellow' : ' text--grey').'"></i></a>';
        $row['star'] = $star;
        return ['index' => $_GET['index'], 'refresh' => 'inline', 'row' => $row, 'table' => '#table-folder', 'alert' => ''];
    }
    
    public function actionStarredfile($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = FileUser::find()->where(['id_file_fk' => $id, 'id_user_fk' => Yii::$app->user->id])->one();
        if(!$model) {
            $model = new FileUser();
            $model->id_file_fk = $id;
            $model->id_user_fk = Yii::$app->user->id;
            $model->is_starred = 1;
        } else {
            $model->is_starred = ($model->is_starred) ? 0 : 1;
        } 
        $model->save();
        
        $star = '<a href="'.Url::to(['/folder/option/starredfile', 'id' => $id]).'" class="action starred" data-id="'.$id.'" data-table="#table-folder" data-label="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').'" title="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="'.(($model->is_starred) ? 'fas ' : 'far ').' fa-star'.(($model->is_starred) ? ' text--yellow' : ' text--grey').'"></i></a>';
        return ['index' => $_GET['index'], 'refresh' => 'inline', 'row' => ['star' => $star], 'table' => '#table-folder', 'alert' => ''];
    }
    
    public function actionAddmembers() {
        if(!isset($_GET['ids']) || empty($_GET['ids'])) {
            return '<div class="modal-body"><div class="alert alert-danger">Proszę wybrać obiekty do udostępnienia</div></div>';
        }
        
        $model = new \frontend\Modules\Company\models\ActionsGroup();
        $model->objects = isset($_GET['ids']) ? explode(',', $_GET['ids']) : [];
        $model->user_action = 'add_member';
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->addMember()) {
				return array('success' => true, 'alert' => 'Folder został udostępniony', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_addmembers', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return $this->renderPartial('_addmembers', ['model' => $model, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]);
		}
        
    }
    
    public function actionDelmembers() {
        if(!isset($_GET['ids']) || empty($_GET['ids'])) {
            return '<div class="modal-body"><div class="alert alert-danger">Proszę wybrać obiekty do odebrania uprawnień</div></div>';
        }
        
        $model = new \frontend\Modules\Company\models\ActionsGroup();
        $model->objects = isset($_GET['ids']) ? explode(',', $_GET['ids']) : [];
        $model->user_action = 'del_member';
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->delMember()) {
				return array('success' => true, 'alert' => 'Folder został udostępniony', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_delmembers', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return $this->renderPartial('_delmembers', ['model' => $model, 'modelName' => ((isset($_GET['modelName'])) ? $_GET['modelName'] : 'Folder')]);
		}
        
    }
}
