<?php

namespace app\Modules\Company\controllers;

use Yii;
use backend\Modules\Company\models\CompanyResources;
use backend\Modules\Company\models\CompanyResourcesSearch;
use backend\Modules\Company\models\CompanyResourcesSchedule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\helpers\Url;

/**
 * ResourcesController implements the CRUD actions for CompanyResources model.
 */
class ResourcesController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = CompanyResources::find()->where(['status' => 1]);
        
        $where = [];
        $post = $_GET;
        if(isset($_GET['CompanyResourcesSearch'])) {
            $params = $_GET['CompanyResourcesSearch'];
            \Yii::$app->session->set('search.resources', $params);
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
                array_push($where, "lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$params['id_company_branch_fk']);
                array_push($where, "id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_dict_company_resource_fk']) && !empty($params['id_dict_company_resource_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_company_resource_fk = ".$params['id_dict_company_resource_fk']);
                array_push($where, "id_dict_company_resource_fk = ".$params['id_dict_company_resource_fk']);
            }
        } else {
			if($this->module->params['isAdmin'] != 1)
				$fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$this->module->params['employeeBranch']);
                array_push($where, "id_company_branch_fk = ".$this->module->params['employeeBranch']);
		}
        
        $fieldsData = $fieldsDataQuery->all();
        
			
		$fields = [];
		$tmp = [];

		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/bookingajax/', 'id' => $value->id]).'" class="btn btn-sm bg-yellow2 update" data-table="#table-resources" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-hand-pointer-o\'></i>'.Yii::t('app', 'Szybka rezerwacja').'" title="'.Yii::t('app', 'Szybka rezerwacja').'"><i class="fa fa-hand-pointer-o  text--grey"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/view/', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-resources" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/updateajax/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-resources" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/deleteajax/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-resources"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = $value->name;
            $tmp['type'] = $value->type;
            $tmp['branch'] = $value->branch;
            $tmp['guardian'] = $value->guardian;
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			$tmp['className'] = ($value->only_management) ? 'bg-purple2 text--purple' : '';
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => count($fields),'rows' => $fields];
	}
    
    public function actionRdata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = CompanyResourcesSchedule::find();
        $where = [];
        $post = $_GET;
        if(isset($_GET['CompanyResourcesSchedule'])) { 
            $params = $_GET['CompanyResourcesSchedule']; 
            if( isset($params['status']) && strlen($params['status']) ) {
               // echo 'status: '.$params['status'];exit;
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status'] );
                array_push($where, "rs.status = ".$params['status']);
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$params['id_company_branch_fk'].")");
                array_push($where, "r.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_resource_fk']) && !empty($params['id_resource_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk = ".$params['id_resource_fk']);
                array_push($where, "id_resource_fk = ".$params['id_resource_fk']);
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
                array_push($where, "DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
                array_push($where,"DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'" );
            }
        } else {
			//$fieldsDataQuery = $fieldsDataQuery->andWhere("status = 0");
			//$fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".date('Y-m-d', strtotime("-1 month"))."'");
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".date('Y-m-d')."'");
            array_push($where, "DATE_FORMAT(date_from, '%Y-%m-%d') >= '".date('Y-m-d')."'");
            /*if($this->module->params['isAdmin'] != 0)
				$fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$this->module->params['employeeBranch'].")");*/
		}
        
        $sortColumn = 'rs.id';
        if( isset($post['sort']) && $post['sort'] == 'resource' ) $sortColumn = 'r.name';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'id_employee_fk';
        if( isset($post['sort']) && $post['sort'] == 'date_from' ) $sortColumn = 'date_from';
        if( isset($post['sort']) && $post['sort'] == 'date_to' ) $sortColumn = 'date_to';
		
		$query = (new \yii\db\Query())
            ->select(['rs.id as id',  "rs.status as status", "type_event", "is_meeting", "r.id_company_branch_fk as id_company_branch_fk", "rs.id_employee_fk as id_employee_fk", "rs.id_resource_fk as id_resource_fk", "r.name as rname", 
                      "concat_ws(' ', e.lastname, e.firstname) as ename", "concat_ws(' ', u.lastname, u.firstname) as uname", "date_from", "date_to"])
            ->from('{{%company_resources_schedule}} as rs')
            ->join(' JOIN', '{{%company_resources}} as r', 'r.id = rs.id_resource_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = rs.id_employee_fk')
            ->join(' JOIN', '{{%user}} as u', 'u.id = rs.created_by');
            //->where( ['b.status' => 1] );
	   
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );

        $rows = $query->all();

        //$fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
        $states = \backend\Modules\Company\models\CompanyResourcesSchedule::listStates();
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= ( $value['status'] == 0 && 
								( ($this->module->params['isSpecial'] && $this->module->params['employeeBranch'] == $value['id_company_branch_fk']) || 
								  ($this->module->params['isSpecial'] && $this->module->params['replacementBranch'] == $value['id_company_branch_fk']) || 
								  $this->module->params['isAdmin']
								)
							) ? '<a href="'.Url::to(['/company/resources/accept/', 'id' => $value['id']]).'" class="btn btn-sm bg-green deleteConfirm" data-label="Zatwierdź" data-table="#table-booking" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check\'></i>'.Yii::t('app', 'Zatwierdź rezerwację').'" title="'.Yii::t('app', 'Zatwierdź rezerwację').'"><i class="fa fa-check"></i></a>' : '';
            $actionColumn .= ( $value['status'] == 0 && 
								( ($this->module->params['isSpecial'] && $this->module->params['employeeBranch'] == $value['id_company_branch_fk']) || 
								  ($this->module->params['isSpecial'] && $this->module->params['replacementBranch'] == $value['id_company_branch_fk']) ||
								  $this->module->params['isAdmin']
								)
							) ? '<a href="'.Url::to(['/company/resources/discard/', 'id' => $value['id']]).'" class="btn btn-sm bg-red deleteConfirmWithComment" data-label="Odrzuć" data-table="#table-booking" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-ban\'></i>'.Yii::t('app', 'Odrzuć rezerwację').'" title="'.Yii::t('app', 'Odrzuć rezerwację').'"><i class="fa fa-ban"></i></a>' : '';
            $actionColumn .= ( $value['status'] >= 0 && 
								( ($this->module->params['isSpecial'] && $this->module->params['employeeBranch'] == $value['id_company_branch_fk']) || 
								  ($this->module->params['isSpecial'] && $this->module->params['replacementBranch'] == $value['id_company_branch_fk']) ||
								  $this->module->params['isAdmin'] || 
								  ($this->module->params['employeeId'] == $value['id_employee_fk']) 
								)
							) ? '<a href="'.Url::to(['/company/resources/cancel/', 'id' => $value['id']]).'" class="btn btn-sm bg-orange deleteConfirmWithComment" data-label="Odwołaj" data-table="#table-booking" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-rotate-ban\'></i>'.Yii::t('app', 'Odwołaj rezerwację').'" title="'.Yii::t('app', 'Odwołaj rezerwację').'"><i class="fa fa-rotate-left"></i></a>' : '';
            $actionColumn .= ( $value['status'] == 1 && $value['is_meeting'] == 1 && 
								( ($this->module->params['isSpecial'] && $this->module->params['employeeBranch'] == $value['id_company_branch_fk']) || 
								  ($this->module->params['isSpecial'] && $this->module->params['replacementBranch'] == $value['id_company_branch_fk']) ||
								  $this->module->params['isAdmin'] || 
								  ($this->module->params['employeeId'] == $value['id_employee_fk']) 
								)
						    ) ? '<a href="'.Url::to(['/company/resources/change/', 'id' => $value['id']]).'" class="btn btn-sm bg-purple update" data-label="Zmień parametry rezerwacji" data-table="#table-booking" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-exchange-ban\'></i>'.Yii::t('app', 'Zmień parametry rezerwacji').'" title="'.Yii::t('app', 'Zmień parametry rezerwacji').'"><i class="fa fa-exchange"></i></a>' : '';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/bookingupdate/', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-booking" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'Szybka rezerwacja').'" title="'.Yii::t('app', 'Szybka rezerwacja').'"><i class="fa fa-eye "></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['sresource'] = $value['rname'];
			$tmp['resource'] = '<a href="'.Url::to(['/company/resources/view','id' => $value['id_resource_fk']]).'" >'.$value['rname'].'</a>';
            $tmp['employee'] = ($value['ename'] == $value['uname']) ? $value['ename'] : ('<small>'.$value['uname'].'</small><br /><small class="text--pink">w imieniu '.$value['ename'].'</small>');
            $tmp['date_from'] = $value['date_from'];
            $tmp['date_to'] = $value['date_to'];
            $tmp['state'] = '<span class="label bg-'.$colors[$value['status']].'">'.$states[$value['status']].'</span>';
            $tmp['event_type'] = ($value['is_meeting'] == 1) ? '<i class="fa fa-handshake-o text--orange"></i>' : '<i class="fa fa-hand-o-up text--grey"></i>';
            $tmp['className'] = ($value['status'] == -2) ? 'danger' : ( ($value['status'] == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    /**
     * Lists all CompanyDepartment models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["departmentPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
		
		$searchModel = new CompanyResourcesSearch();
		$searchModel->id_company_branch_fk = ($this->module->params['isAdmin'] == 1) ? 0 : $this->module->params['employeeBranch'];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.resources'); 
            if($params) {
                foreach($params as $key => $value) {
                    $searchModel->$key = $value;
                }
            }
        }
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
			'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],
            'isSpecial' => $this->module->params['isSpecial'],
            'isAdmin' => $this->module->params['isAdmin']
        ]);
    }

    /**
     * Displays a single CompanyDepartment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$id = CustomHelpers::decode($id);
        $isSpecial = $this->module->params['isSpecial'];
        if($this->module->params['isAdmin'])  $isSpecial = 1;
        
        $term = false;$txtError = false;
        if( isset($_GET['term']) ) {
            $term = CompanyResourcesSchedule::findOne($_GET['term']);
            $statusTemp = $term->status;
            
            if (Yii::$app->request->isPost ) {
                $term->status = -2;
				$term->user_action = 'discard';
                
                if ($term->load(Yii::$app->request->post()) && $term->save()) {
                   /* if( $term->resource['id_employee_fk'] ) { 
                        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rezerwacja została odrzucona')  );
                         \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $term])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                ->setCc($term->resource['manager']['email'])
                                //->setTo(\Yii::$app->params['testEmail'])
								->setTo($term->employee['email'])
                                ->setSubject('Rezerwacja została odrzucona w systemie ' . \Yii::$app->name )
                                ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $term])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                //->setTo(\Yii::$app->params['testEmail'])
								->setTo($term->employee['email'])
                                ->setSubject('Rezerwacja została odrzucona w systemie ' . \Yii::$app->name )
                                ->send();
                    }*/
                    $term = false;
                } else {
                    $term->status = $statusTemp;
                    
                    foreach($term->getErrors() as $key => $error) {
                        $txtError = $error[0];
                    }
                }
            }
        }
		
		$terms = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['id_resource_fk' => CustomHelpers::decode($id), 'status' => 1])->andWhere(" '".date('Y-m-d')."' between DATE_FORMAT(date_from, '%Y-%m-%d') and DATE_FORMAT(date_to, '%Y-%m-%d')")->orderby('date_from')->all(); 
        $forAccept = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['id_resource_fk' => CustomHelpers::decode($id), 'status' => 0])->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".date('Y-m-d')."'")->orderby('date_from')->all(); 
       
        return $this->render('view', [
            'model' => $this->findModel($id), 'grants' => $this->module->params['grants'], 'terms' => $terms, 'forAccept' => $forAccept, 'employeeId' => $this->module->params['employeeId'], 'isSpecial' => $isSpecial, 'term' => $term, 'txtError' => $txtError
        ]);
    }
    
    public function actionViewajax($id)  {
        $model = $this->findModel($id);
        
        return $this->renderAjax('_viewAjax', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CompanyResources model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()  {
        $model = new CompanyResources();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
		$model = new CompanyResources();
        $model->id_company_branch_fk = $this->module->params['employeeBranch'];
		$employees = [];
        
        $isSpecial = $this->module->params['isSpecial'];
        if($this->module->params['isAdmin'])  $isSpecial = 1;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/bookingajax/', 'id' => $model->id]).'" class="btn btn-sm bg-yellow2 update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-hand-pointer-o\'></i>'.Yii::t('app', 'Szybka rezerwacja').'" title="'.Yii::t('app', 'Szybka rezerwacja').'"><i class="fa fa-hand-pointer-o  text--grey"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/view/', 'id' => $model->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/updateajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/deleteajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
                $actionColumn .= '</div>';
				
                
				$data['name'] = $model->name;
				$data['type'] = $model->type;
                $data['branch'] = $model->branch;
                $data['actions'] = $actionColumn;
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' => 0, 'alert' => 'Zasób <b>'.$model->name.'</b> został dodany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => $this->module->params['employeeBranch'],  'isSpecial' => $isSpecial, 'isAdmin' => $this->module->params['isAdmin']]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],  'isSpecial' => $isSpecial, 'isAdmin' => $this->module->params['isAdmin']]) ;	
		}
	}

    /**
     * Updates an existing CompanyDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateajax($id)  {
        $model = $this->findModel($id);
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/bookingajax/', 'id' => $model->id]).'" class="btn btn-sm bg-yellow2 update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-hand-pointer-o\'></i>'.Yii::t('app', 'Szybka rezerwacja').'" title="'.Yii::t('app', 'Szybka rezerwacja').'"><i class="fa fa-hand-pointer-o  text--grey"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/company/resources/view/', 'id' => $model->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/updateajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/company/resources/deleteajax/', 'id' => $model->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
                $actionColumn .= '</div>';
				
                
                $data['name'] = $model->name;
				$data['type'] = $model->type;
                $data['branch'] = $model->branch;
                $data['actions'] = $actionColumn;
                $data['id'] = $model->id;
				
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Dział <b>'.$model->name.'</b> został zakualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin']]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin']]) ;	
		}
    }
    
    public function actionChange($id)  {
        $id = CustomHelpers::decode($id);
		$model = CompanyResourcesSchedule::findOne($id);
        $model->change_reason = '';
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$new = new CompanyResourcesSchedule();
            $new->id_parent_fk = $model->id;
            $new->user_action = 'change';     
            $new->load(Yii::$app->request->post());
            if($new->id_resource_fk) {
                $resource = \backend\Modules\Company\models\CompanyResources::findOne($new->id_resource_fk);
                if($resource) {
                    if( $this->module->params['isSpecial'] && $resource->id_company_branch_fk == $this->module->params['employeeBranch'] ) {
                        $new->status = 1;
                    } else {
                        $new->status = 0;
                    }
                }
            }
			if($new->validate() && $new->save()) {
				$model->status = -1;
                $model->comment = $new->change_reason;
                $model->user_action = 'cancel';
                $model->save();
				return array('success' => true, 'action' => 'updateRow', 'index' => (isset($_GET['index']) ? $_GET['index'] : 0), 'alert' => 'Termin został został zakualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_changeBooking', [  'model' => $model, 'branchId' => $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId']]), 'errors' => $new->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_changeBooking', [  'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1 : $this->module->params['employeeBranch'],  'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId'] ]) ;	
		}
    }
    
    public function actionBookingajax($id) {
		
		$id = CustomHelpers::decode($id);
		$model = CompanyResources::findOne($id);
        
        $new = new CompanyResourcesSchedule();
        $new->user_action = 'create';
        if($model) {
            $new->id_resource_fk = $model->id;
            $new->id_company_branch_fk = $model->id_company_branch_fk;
        } else {
            $new->id_company_branch_fk = $this->module->params['employeeBranch'];
        }
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        $new->id_employee_fk = $employee->id;  
        $new->status = 0;
        $new->date_from = date( 'Y-m-d', (strtotime( date('Y-m-d') ) + 60*60*24) ).' 08:00';
        $new->date_to = date( 'Y-m-d', (strtotime( date('Y-m-d') ) + 60*60*24) ).' 09:00';
        
        if($this->module->params['isSpecial'] || $this->module->params['isAdmin'])
            $new->accept = 1;
		
		if(isset($_GET['start'])) {
		    $time = ( gmdate('H:i', ($_GET['start'])) == '00:00' ) ? '08:00' : gmdate('H:i', ($_GET['start']));
            $timeTo = ( gmdate('H:i', ($_GET['start'])) == '00:00' ) ? '09:00' : gmdate('H:i', ($_GET['start']+60*60));
			$new->date_from = gmdate('Y-m-d', ($_GET['start'])).' '.$time;
            $new->date_to = gmdate('Y-m-d', ($_GET['start'])).' '.$timeTo;
		}
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$new->load(Yii::$app->request->post());
            if(!$new->id_employee_fk) $new->id_employee_fk = $this->module->params['employeeId']; 
            if( $new->accept == 1 && 
				( ( $this->module->params['isSpecial'] && $new->resource['id_company_branch_fk'] == $this->module->params['employeeBranch']) || 
				  ( $this->module->params['isSpecial'] && $new->resource['id_company_branch_fk'] == $this->module->params['replacementBranch']) || 
					$this->module->params['isAdmin']
				) 
			) {
                $new->status = 1;
            } else {
				$new->status = 0;
			}
			if($new->validate() && $new->save()) {
                /*if($new->status == 1) {
                    $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($new->id_employee_fk);
                    \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $new])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        //->setTo(\Yii::$app->params['testEmail'])
						->setTo($employee->email)
						->setBcc('kamila_bajdowska@onet.eu')
                        ->setSubject('Potwierdzenie rezerwacji zasobu w systemie ' . \Yii::$app->name )
                        ->send();
                } else {
                    $branchAdministration = [];
                    $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                                    ->where(['status' => 1, 'id_company_branch_fk' => $new->resource['id_company_branch_fk'] ])
                                    ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                                    ->all();
                    foreach($branchAdministrationSql as $ii => $item) {
                        array_push($branchAdministration, $item['email']);
                    }
                    
                    \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $new])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        //->setTo(\Yii::$app->params['testEmail'])
						->setTo($branchAdministration)
						->setBcc('kamila_bajdowska@onet.eu')
                        ->setSubject('Nowa rezerwacja w systemie ' . \Yii::$app->name )
                        ->send();
                }*/
		      
				return array('success' => true, 'action' => 'insertRow', 'index' => 0, 'alert' => ($new->status == 0) ? 'Rezerwacja <b>'.$new->resource['name'].'</b> została wysłana do akceptacji' : 'Rezerwacja została zapisana' );	
			} else {
                $new->status = 0;
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formBooking_new' : '_formBooking_new'), [ 'model' => $new, 'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId'], 'branchId' => ($model) ? $model->id_company_branch_fk : $this->module->params['employeeBranch'] ]), 'errors' => $new->getErrors() );	
			}		
		} else {
			return  $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formBooking_new' : '_formBooking_new'), [  'model' => $new, 'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId'], 'branchId' => ($model) ? $model->id_company_branch_fk : $this->module->params['employeeBranch'] ]) ;	
		}
	}
    
    public function actionBookingupdate($id) {
		
		$id = CustomHelpers::decode($id);
		$model = CompanyResourcesSchedule::findOne($id);
        $model->id_company_branch_fk = $model->resource['id_company_branch_fk'];

		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			$model->user_action = 'update';
			if($model->validate() && $model->save()) {
		
				return array('success' => true, 'action' => 'insertRow', 'index' => 0, 'alert' => 'Rezerwacja <b>'.$model->resource['name'].'</b> została wysłana do akceptacji' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formBooking_new', [  'model' => $model, 'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId'], 'branchId' => $this->module->params['employeeBranch']  ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formBooking_new', [  'model' => $model, 'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'], 'employeeId' => $this->module->params['employeeId'], 'branchId' => $this->module->params['employeeBranch']  ]) ;	
		}
	}


    /**
     * Deletes an existing CompanyDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Zasób <b>'.$model->name.'</b> został usunięty', 'id' => $model->id, 'table' => '#table-resources');	
        } else {
            return array('success' => false,  'alert' => 'Zasób <b>'.$model->name.'</b> nie został usunięty', 'table' => '#table-resources' );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyResources model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyResources the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = CompanyResources::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionBusy($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $days = [];
        
        $month = date('m'); $year = date('Y');
        if( isset($_GET['month']) && !empty($_GET['month']) ) {
            $month = $_GET['month'];
            $year = $_GET['year'];
        }
        if($month == 1) {
            $dateFrom = ($year-1).'-11-1';
            $dateTo = ($year).'-03-31';
        } else if($month == 2) {
            $dateFrom = ($year).'-11-1';
            $dateTo = ($year+1).'-03-31';
        } else if($month == 11) {
            $dateFrom = ($year).'-09-1';
            $dateTo = ($year+1).'-01-31';
        } else if($month == 12) {
            $dateFrom = ($year).'-10-1';
            $dateTo = ($year+1).'-02-31';
        } else {
            $dateFrom = $year.'-'.str_pad(($month-1), 1, '0', STR_PAD_LEFT).'-1';
            $dateTo = $year.'-'.str_pad(($month+1), 1, '0', STR_PAD_LEFT).'-31';
        }
        
        //$days = CalTask::find()->where(['status' => 1])->andWhere("event_date between '". $dateFrom ."' and '". $dateTo ."' ")->select('event_date')->distinct()->all();
        //$sql = "select distinct( date_format(event_date, '%Y-%c-%e')) as event_date from {{%cal_task}} where event_date between '". $dateFrom ."' and '". $dateTo ."'";
        $sql = "select distinct( date_format(date_from, '%Y-%c-%e')) as term from {{%company_resources_schedule}} where status = 1 and id_resource_fk = ".$id." and date_from between '". $dateFrom ."' and '". $dateTo ."'";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($data as $key => $value) {
            array_push($days, $value['term']);
        }
        
        return ['days' => $days];
    }
    
    public function actionTerms($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        
        $timeStamp = date('Y-m-d');
        if( isset($_GET['time']) && !empty($_GET['time']) ) {
            $timeStamp = date('Y-m-d', $_GET['time']);
        }
        $termsData = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['status' => 1, 'id_resource_fk' => $id])->andWhere("'".$timeStamp."' between DATE_FORMAT(date_from, '%Y-%m-%d') and DATE_FORMAT(date_to, '%Y-%m-%d')")->orderby('date_from')->all(); 
        $forAcceptData = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['status' => 0, 'id_resource_fk' => $id])->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".date('Y-m-d')."'")->orderby('date_from')->all(); 

        $termList = '';
		$reservationList = '';
        //$casesList = $timeStamp;
        $isSpecial = $this->module->params['isSpecial'];
        if($this->module->params['isAdmin'])  $isSpecial = 1;
        foreach($termsData as $key => $item) {
            $meEvent = false;
            if($item->id_employee_fk == $this->module->params['employeeId']) {
                $meEvent = true;
            }
            
            $termList .= '<a id="db-event-'.$item->id.'" href="'.Url::to(['/company/resources/bookingupdate', 'id' => $item->id]).'" data-title="<i class=\'fa fa-hand-pointer-o\'></i>Rezerwacja" data-target="#modal-grid-item" class="list-group-item text-ellipsis gridViewModal">'
                            . ( ($meEvent) ? '<i class="fa fa-circle text--blue"></i>&nbsp;' : '')
                            .'<span class="badge bg-danger">'. ( date('H:i', strtotime($item->date_from)) ) .'</span> '.$item->employee['fullname']
                        .'</a>'; 
        }
        
        foreach($forAcceptData as $key => $item) {
            $meEvent = false;
            if($item->id_employee_fk == $this->module->params['employeeId']) {
                $meEvent = true;
            }
            $reservationList .=  '<li id="db-event-'.$item->id.'">'
                                    . ( ($meEvent) ? '<i class="fa fa-hourglass-half text--grey"></i>&nbsp;' : '')
                                    .'<a class="gridViewModal" href="'.Url::to(['/company/resources/bookingupdate', 'id' => $item->id]).'" data-title="<i class=\'fa fa-hend-pointer-o\'></i>Rezerwacja" data-target="#modal-grid-item">'.$item->employee['fullname'].' </a>'
                                    .'<br /> <small>'.date('Y-m-d H:i', strtotime($item->date_from)) . ' - ' . date('Y-m-d H:i', strtotime($item->date_to)) .'</small>'
                                    .'<br /> <div class="event-describe">'.$item->describe.'</div>'
                                    . ( ($isSpecial) ? ' <a href="'.Url::to(['/company/resources/accept', 'id' => $item->id]).'" class="event-accept deleteConfirm " data-label="Zaakceptuj rezerwację" title="Zaakceptuj rezerwację"><i class="fa fa-check-circle text--green"></i></a>' : '')
                                    . ( ($isSpecial) ? ' <a href="'.Url::to(['/company/resources/discard', 'id' => $item->id]).'" class="event-close deleteConfirmWithComment " data-label="Odrzuć rezerwację" title="Odrzuć rezerwację"><i class="fa fa-remove text--red"></i></a>' : '')
                                .'</li>';
        }
        if($termList == '') $termList = '<div class="alert alert-info">Brak terminów na wybrany dzień</div>';
        if($reservationList == '') $reservationList = '<li>Brak rezerwacji oczekujących</li>';
        return ['cases' => $termList, 'tasks' => $reservationList];
    }
    
    public function actionSchedule()  {
        
		/*if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        $model = new CompanyResourcesSchedule();
		$model->id_employee_fk = Yii::$app->user->id;
  
        return $this->render('schedule', [
            'grants' => $this->module->params['grants'], 'branchId' => ($this->module->params['isAdmin']) ? -1: $this->module->params['employeeBranch'], 'isAdmin' => $this->module->params['isAdmin'],
			'model' => $model, 'dicts' => \backend\Modules\Dict\models\DictionaryValue::find()->where( ['status' => 1, 'id_dictionary_fk' => 4])->orderby('name')->all()
        ]);
    }
    
    public function actionScheduledata() {
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$res = [];
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        /* gmdate('Y-m-d', $_GET['start']) */        
        $eventsSql = CompanyResourcesSchedule::find()->where('status >= 0');
		$eventsSql = $eventsSql->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
		$eventsSql = $eventsSql->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") <= \''. gmdate('Y-m-d', $_POST['end']).'\'');

        if(isset($_POST['typeEvent']) && !empty($_POST['typeEvent']) ) { 
            if($_POST['typeEvent'] != 0)
                $eventsSql = $eventsSql->andWhere('id_resource_fk in (select id from {{%company_resources}} where id_dict_company_resource_fk = '.$_POST['typeEvent'] . ' ) ');
        } 
        if(isset($_POST['statusEvent']) &&  strlen($_POST['statusEvent']) ) { 
            $eventsSql = $eventsSql->andWhere('status = '.$_POST['statusEvent']);
        } else {
            $eventsSql = $eventsSql->andWhere('status >= 0 ');
        }
		if(isset($_POST['resource']) && !empty($_POST['resource']) ) { 
            $eventsSql = $eventsSql->andWhere('id_resource_fk  = '.$_POST['resource']);
        } 
        if(isset($_POST['employee']) && !empty($_POST['employee']) ) { 
            if($_POST['employee'] != 0)
                $eventsSql = $eventsSql->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$_POST['employee'].')');
        } 
		if(isset($_POST['branch']) && !empty($_POST['branch']) ) { 
                $eventsSql = $eventsSql->andWhere('id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = '.$_POST['branch'].')');
        } 
        
        /*if($this->module->params['isSpecial'] == 0 && $this->module->params['isAdmin'] == 0) {
            $eventsSql = $eventsSql->andWhere("id_employee_fk = ".$this->module->params['employeeId']." ");
        } else {*/
            //if($this->module->params['isAdmin'] == 0)
           // $eventsSql = $eventsSql->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$this->module->params['employeeBranch']." ) ");
        //}

        $events = $eventsSql->all(); 
        $colors = [0 => '#E8E8E8', 1 => '#2AA4C9', 2 => '#8447F6', 3 => '#F45246', 4 => '#47F684'];
        foreach($events as $key=>$value) {
            $bgColor = '#8447F6';
        
            //$tStart = strtotime($value['date_from']);
            //$tEnd = strtotime('+30 minutes', $tStart);
            array_push($res, array('title' => $value->resource['name']. ' - ' .$value->employee['fullname'], 
                                   'start' => $value['date_from'], 
                                   'end' => $value['date_to'],
                                   'constraint' => 'freeTerm', 
                                   //'color' => '#f5f5f5', 
                                   'textColor' => '#fff',
                                   //'borderColor' => $value->resource['dict']['color'],
                                   'backgroundColor' => ($value->status == 1) ? ( (!$value->resource['custom']) ? "#4b87ae" : $value->resource['custom']['color'] ) : '#FFBCBC',
                                   //'className' => 'visit-free',
                                   'id' => CustomHelpers::encode($value['id']),
                                   //'allDay' => ($value['all_day'] == 0) ? false : true,
                                   'icon' => ($value->status == 1) ? ( (!$value->resource['dict']) ? "#4b87ae" : $value->resource['dict']['icon'] ) : 'fa fa-exclamation-triangle text--red'
                                )
            );
        }

		return $res;
	}
    
    public function actionReservations() { 
        $model = new CompanyResourcesSchedule();
		$model->id_employee_fk = Yii::$app->user->id;
		$model->id_company_branch_fk = ($this->module->params['isAdmin'] == 1) ? 0 : $this->module->params['employeeBranch'];
		//$model->date_from = date('Y-m-d', strtotime("-1 month"));
        $model->date_from = date('Y-m-d');
        //$model->status = 0;
  
        return $this->render('reservations', [
            'grants' => $this->module->params['grants'], 'isAdmin' => $this->module->params['isAdmin'],
			'model' => $model, 'branchId' => ($this->module->params['isAdmin']) ? -1: $this->module->params['employeeBranch'],
        ]);
    }
    
    public function actionAccept($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = CompanyResourcesSchedule::findOne($id);
        $model->status = 1;
        $model->user_action = 'accept';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            /*if($model->resource['id_employee_fk']) {
                \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
							->setTo($model->employee['email'])
                            ->setCc($model->resource['manager']['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Rezerwacja została zaakceptowana w systemie ' . \Yii::$app->name  )
                            ->send();
            } else {
                \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
							->setTo($model->employee['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Rezerwacja została zaakceptowana w systemie ' . \Yii::$app->name  )
                            ->send();
            }*/
            return ['success' => true, 'alert' => 'Rezerwacja została zaakceptowana', 'id' => $model->id, 'action' => 'accept', 'table' => '#table-booking'];
        } else {
            $txtError = 'Rezerwacja nie została zaakceptowana';
            foreach($model->getErrors() as $key => $error) {
                $txtError = $error;
            }
            return ['success' => false, 'alert' => $txtError, 'id' => $model->id, 'action' => 'accept'];
        }
    }
    
    public function actionAccepted($id)  {
        $id = CustomHelpers::decode($id);
        $model = CompanyResourcesSchedule::findOne($id);
        
        $branchAdministration = [];
        $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                        ->where(['status' => 1, 'id_company_branch_fk' => $model->resource['id_company_branch_fk'] ])
                        ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                        ->all();
        foreach($branchAdministrationSql as $ii => $item) {
            array_push($branchAdministration, $item['id_user_fk']);
        }
		
		if($this->module->params['replacementBranch'] != 0) array_push($branchAdministration, Yii::$app->user->id);
        if($this->module->params['isAdmin'] || ($this->module->params['isSpecial'] && in_array(Yii::$app->user->id, $branchAdministration) ) ) {
            if($model->status == 0) {
                $model->status = 1;
                $model->user_action = 'accept';
                if($model->save()) {
                    Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rezerwacja została zaakceptowana')  );
                } else {
                   Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Problem z akceptacją rezerwacji')  );
                }
            } else {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'REZERWACJA ZOSTAŁA JUŻ '. ( ($model->status == 1) ? 'ZATWIERDZONA' : 'ODRZUCONA') .' ')  );
            }
        } else {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Nie masz uprawnień do akceptacji tej rezerwacji')  );
        }
        return $this->redirect(['view', 'id' => $model->id_resource_fk]);
    }
    
    public function actionDiscard($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = CompanyResourcesSchedule::findOne($id);
        
        $branchAdministration = [];
        $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                        ->where(['status' => 1, 'id_company_branch_fk' => $model->resource['id_company_branch_fk'] ])
                        ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                        ->all();
        foreach($branchAdministrationSql as $ii => $item) {
            array_push($branchAdministration, $item['id_user_fk']);
        }
		if($this->module->params['replacementBranch'] != 0) array_push($branchAdministration, Yii::$app->user->id);
        if($this->module->params['isAdmin'] || ($this->module->params['isSpecial'] && in_array(Yii::$app->user->id, $branchAdministration) ) ) {
            $model->status = -2;
            $model->comment = ( isset($_POST['comment']) ) ?  $_POST['comment'] : '';
            $model->user_action = 'discard';
            if($model->save()) {
                return ['success' => true, 'alert' => 'Rezerwacja została odrzucona', 'id' => $model->id, 'action' => 'discard', 'table' => '#table-booking'];
            } else {
                $txtError = 'Rezerwacja nie została zaakceptowana';
                foreach($model->getErrors() as $key => $error) {
                    $txtError = $error;
                }
                return ['success' => false, 'alert' => $txtError, 'id' => $model->id, 'action' => 'discard', 'table' => '#table-booking'];
            }
        } else {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Nie masz uprawnień do akceptacji tej rezerwacji')  );
        }
    }
    
    public function actionDiscarded($id)  {
        $id = CustomHelpers::decode($id);
        $model = CompanyResourcesSchedule::findOne($id);
        $branchAdministration = [];
        $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                        ->where(['status' => 1, 'id_company_branch_fk' => $model->resource['id_company_branch_fk'] ])
                        ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                        ->all();
        foreach($branchAdministrationSql as $ii => $item) {
            array_push($branchAdministration, $item['id_user_fk']);
        }
		if($this->module->params['replacementBranch'] != 0) array_push($branchAdministration, Yii::$app->user->id);
        if($this->module->params['isAdmin'] || ($this->module->params['isSpecial'] && in_array(Yii::$app->user->id, $branchAdministration) ) ) {
            if($model->status == 0) {
                return $this->redirect(['view', 'id' => $model->id_resource_fk, 'term' => $model->id]);
            } else {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'REZERWACJA ZOSTAŁA JUŻ '. ( ($model->status == 1) ? 'ZATWIERDZONA' : 'ODRZUCONA') .' ')  );
                
            }   
        } else {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Nie masz uprawnień do odrzucenia tej rezerwacji')  );
        }
        return $this->redirect(['view', 'id' => $model->id_resource_fk]);
    }
    
    public function actionCancel($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = CompanyResourcesSchedule::findOne($id);
        $model->status = -1;
        $model->comment = ( isset($_POST['comment']) ) ?  $_POST['comment'] : '';
        $model->user_action = 'cancel';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
           /* if($model->resource['id_employee_fk']) {
                \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
							->setTo($model->employee['email'])
                            ->setCc($model->resource['manager']['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Rezerwacja została anulowana w systemie ' . \Yii::$app->name )
                            ->send();
            } else {
                \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
							->setTo($model->employee['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Rezerwacja została anulowana w systemie ' . \Yii::$app->name )
                            ->send();
            }*/
            return ['success' => true, 'alert' => 'Rezerwacja została odwołana', 'id' => $model->id, 'action' => 'cancel', 'table' => '#table-booking'];
        } else {
            $txtError = 'Rezerwacja nie została odwołana';
            foreach($model->getErrors() as $key => $error) {
                $txtError = $error;
            }
            return ['success' => false, 'alert' => $txtError, 'id' => $model->id, 'action' => 'cancel', 'table' => '#table-booking'];
        }
    }
    
    public function actionConfig($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        //$id = CustomHelpers::decode($id);
        $dict = \backend\Modules\Dict\models\DictionaryValue::findOne($id);
        return $dict->custom_data;
    }
}
