<?php

namespace app\Modules\Company\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\Modules\Company\models\CompanyEmployee;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * Default controller for the `Company` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		if(!$model)
			$model= CompanyEmployee::findOne(1);
	    
		$departmentsList = '';
		foreach($model->departments as $key => $item) {
            $departmentsList .= '<span class="label bg-purple">'.$item->department['name'] .'</span><br />';
        }
        
        if($departmentsList == '') {
            $departmentsList .= '<span class="label bg-grey">brak przypisanych działów</span>';
        }
        
        $cases = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1, "DATE_FORMAT(date_from, '%Y-%m-%d')" => date('Y-m-d')])->orderby('date_from')->all(); 
		
		return $this->render('index',  ['model' => $model, 'departmentsList' => $departmentsList, 'cases' => $cases]);
    }
    
    public function actionDashboard()  {
        $model = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        $employeeId = ($model) ? $model->id : 0;
        
		if(!$model) {
			$model = new CompanyEmployee();
            $model->lastname = 'SUPERADMIN';
            $model->id = 0;
        }    
	    
		$departmentsList = '';
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<br /><span class="label bg-purple">'.$item->department['name'] .'</span><br />';
        }
        if($departmentsList == '') {
            $departmentsList .= '<span class="label bg-grey">brak przypisanych działów</span>';
        }
        
        $cases = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1, "DATE_FORMAT(date_from, '%Y-%m-%d')" => date('Y-m-d')])->orderby('date_from')->all(); 
		$visits = \backend\Modules\Eg\models\Visit::find()->where(['status' => 1, "visit_day" => date('Y-m-d')])->orderby('visit_date')->all(); 
		//var_dump($timeStamp);exit;
        
        $stats['deadline'] = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1, 'id_dict_task_status_fk' => 1, 'event_date' => date('Y-m-d')])
                                   ->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employeeId.')')->count();
        
        $sqlVisitsToday = "select count(*) as resultData from {{%eg_visit}} where visit_day = '".date('Y-m-d')."'";
        $sqlVisitsMonth = "select count(*) as resultData from {{%eg_visit}} where DATE_FORMAT(visit_day,'%Y-%m') = '".date('Y-m')."'";
        $sqlPatients = "select count(*) as resultData from {{%eg_patient}} where status = 1";
        
        $stats['comments'] = 0;//\Yii::$app->db->createCommand($sql)->queryOne()['comments'];
        $stats['patients'] = \Yii::$app->db->createCommand($sqlPatients)->queryOne()['resultData'];
        $stats['visits_today'] = \Yii::$app->db->createCommand($sqlVisitsToday)->queryOne()['resultData'] ;
        $stats['visits_month'] = \Yii::$app->db->createCommand($sqlVisitsMonth)->queryOne()['resultData']; 
		
		return $this->render('index',  ['model' => $model, 'departmentsList' => $departmentsList, 'cases' => $cases, 'visits' => $visits, 'stats' => $stats, 'employeeId' => $this->module->params['employeeId'], 'isSpecial' => $this->module->params['isSpecial'], 'isAdmin' => $this->module->params['isAdmin'] ]);
    }
    
    public function actionEvents() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $timeStamp = date('Y-m-d');
        if( isset($_GET['time']) && !empty($_GET['time']) ) {
            $timeStamp = date('Y-m-d', $_GET['time']);
        }
        $eventsData = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1, "event_date" => $timeStamp])->orderby('event_time')->all(); 
        $casesList = '';
		$tasksList = '';
		$visitsList = '';
        //$casesList = $timeStamp;
        foreach($eventsData as $key => $item) {
            $meEvent = false;
            foreach($item->employees as $i => $emp) {
                if($emp['id_employee_fk'] == $this->module->params['employeeId']) {
                    $meEvent = true;
                }
            }
            if($item->type_fk == 1) {
				/*$casesList .= '<a id="db-event-'.$item->id.'" href="'.Url::to(['/task/event/showajax', 'id' => $item->id]).'" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>Podgląd" data-target="#modal-grid-item" class="list-group-item text-ellipsis gridViewModal '.( ($item->id_dict_task_status_fk == 3) ? ' bg-red2' : '').'">'
                            . ( ($meEvent) ? '<i class="fa fa-thumb-tack text--pink"></i>&nbsp;' : '')
                            .'<span class="badge bg-'.( ($item->id_dict_task_status_fk == 3) ? 'red' : 'grey').'">'. ( $item->event_time ) .'</span> '.$item->name
                            .' </a>'; */
                //$casesList .= '<a href="'.Url::to(['/task/case/view', 'id' => $item->id]).'" class="list-group-item text-ellipsis"> <span class="badge bg-danger">'. ( date('H:i', strtotime($item->date_from)) ) .'</span> '.$item->name.' </a>';
			} else {
				$tasksList .=  '<li id="db-event-'.$item->id.'">'
                                . ( ($meEvent) ? '<i class="fa fa-thumb-tack text--pink"></i>&nbsp;' : '')
                                .'<a class="gridViewModal" href="'.Url::to(['/task/event/showajax', 'id' => $item->id]).'" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>Podgląd" data-target="#modal-grid-item">'.$item->name.' </a>'
                                .'@ '.( (!$item->event_time || $item->event_time == '00:00') ? 'cały dzień' : $item->event_time )
                                .( ($meEvent || $this->module->params['isSpecial'] || $this->module->params['isAdmin']) ? ' <a href="'.Url::to(['/task/event/closeajax', 'id' => $item->id]).'" class="event-close deleteConfirm '.( ($item->id_dict_task_status_fk == 2) ? 'done' : '' ).'" data-label="Zamknij zadanie"><i class="fa fa-check"></i></a>' : '')
                            .'</li>';
                //$tasksList .= '<li><a href="'.Url::to(['/task/event/view', 'id' => $item->id]).'">'.$item->name.' </a>@ '.( date('H:i', strtotime($item->date_from)) ).' <a href="#" class="event-close"><i class="fa fa-check"></i></a></li>';			
            }
        }
		
		$visitsData = \backend\Modules\Eg\models\Visit::find()->where(['status' => 1, "visit_day" => $timeStamp])->orderby('visit_date')->all(); 
		//var_dump($timeStamp);exit;
		foreach($visitsData as $key => $item) {
			$casesList .= '<a id="db-event-'.$item->id.'" href="'.Url::to(['/eg/visit/showajax', 'id' => $item->id]).'" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>Podgląd" data-target="#modal-grid-item" class="list-group-item text-ellipsis viewModal '.( ($item->id_dict_eg_visit_status_fk == 6) ? ' bg-red2' : '').'">'
                            . ( ($item->is_confirm) ? '<i class="fa fa-check text--green"></i>&nbsp;' : '')
                            .'<span class="badge bg-'.( ($item->id_dict_eg_visit_status_fk == 6) ? 'red' : 'grey').'">'
							. ( date('H:i', strtotime($item->start)) ) .'</span> '
							. $item->patient['fullname'].' ['.$item->doctor['fullname'].']'
							. (($item->additional_info) ? ('<br />info: <b>'.$item->additional_info.'</b>') : '')
                            .' </a>';
		}
		
        if($casesList == '') $casesList = '<div class="alert alert-info">Brak rozpraw na wybrany dzień</div>';
        if($tasksList == '') $tasksList = '<li>Brak zadań na wybrany dzień</li>';
        return ['cases' => $casesList, 'tasks' => $tasksList];
    }
}
