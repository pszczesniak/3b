<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = Yii::t('app', 'New') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pracownicy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'New');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-create', 'title'=>Html::encode($this->title))) ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
<?php $this->endContent(); ?>
