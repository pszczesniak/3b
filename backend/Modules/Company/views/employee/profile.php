<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\CasesTable;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\tasks\ActionsTable;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Profil pracownika';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pracownicy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-view', 'title'=>Html::encode($this->title))) ?>
   <?= Alert::widget() ?>
    <div class="grid">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="grid profile">
                <div class="col-md-4">
                    <div class="profile-avatar">
                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->id."/preview.jpg"))?"/uploads/employees/cover/".$model->id."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= $avatar ?>" alt="Avatar">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="profile-name">
                        <h3><?= $model->fullname ?></h3>
                        <p class="job-title mb0"><i class="fa fa-building"></i> <?= $model->typename ?></p>
                        <!-- <p class="balance"> Pensja: <span>146 PLN</span> </p> -->
                        <!--<a href="#" class="btn bg-teal btn-large mr10"> <i class="fa fa-envelope"></i> Wyślij wiadomość</a>-->
						<a href="<?= Url::to(['/timeline/employee', 'id' => $model->id]) ?>" class="btn bg-pink"> <i class="fa fa-history"></i> Aktywność</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover personal-task">
                        <tbody>
                            <tr>
                                <td> <i class=" fa fa-map-marker"></i> </td>
                                <td><?= ($model->branch) ? $model->branch['name'] : 'nie przypisano oddziału' ?></td>
                            </tr>
                            <tr>
                                <td> <i class=" fa fa-phone"></i> </td>
                                <td><?= ($model->phone) ? $model->phone : 'brak danych' ?></td>
                            </tr>
                            <tr>
                                <td> <i class=" fa fa-envelope"></i> </td>
                                <td><?= ($model->email) ?  Html::mailto($model->email, $model->email) : 'brak danych' ?></td>
                            </tr> 
                            <tr>
                                <td colspan="2">
                                    <h5 class="text-muted">Stanowisko</h5>
                                    <p><?= $model->kindname ?></p>
                                    <h5 class="text-muted">Działy</h5>
                                    <p><?= $model->departments_list ?></p>
                                </td>
                            </tr>  
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="profile-about">
                
            </div>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-calendar"></i><span class="panel-tabs--text">Terminarz</span></a></li>
                        <li><a data-toggle="tab" href="#tab3"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Czynności</span></a></li>
						<li><a data-toggle="tab" href="#tab4"><i class="fa fa-folder"></i><span class="panel-tabs--text">Sprawy/Projekty</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?php /* $this->render('_files', ['model' => $model, 'type' => 1, 'onlyShow' => true]) */ ?>
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 1, 'parentId' => $model->id, 'onlyShow' => true]) ?>
						</div>
						<div class="tab-pane" id="tab2">
                            <?php /* $this->render('_events', ['id' => $model->id])*/ ?>
                            <?= EventsTable::widget(['dataUrl' => Url::to(['/company/employee/events', 'id'=>$model->id]) ]) ?>
						</div>
                        <div class="tab-pane" id="tab3">
                            <?php /* $this->render('_events', ['id' => $model->id])*/ ?>
                            <?= ActionsTable::widget(['dataUrl' => Url::to(['/company/employee/actions', 'id'=>$model->id]) ]) ?>
						</div>
                        <div class="tab-pane" id="tab4">
                            <?php /*$this->render('_cases', ['id' => $model->id])*/ ?>
                            <?= CasesTable::widget(['dataUrl' => Url::to(['/company/employee/cases', 'id'=>$model->id])]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    

<?php $this->endContent(); ?>
