<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use backend\widgets\Fileswidget;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-employees"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <fieldset><legend>Moduł rozliczeń - czynności</legend>
            <?= $form->field($model, 'share_mode_1')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->andWhere('id != '.$model->id)->orderby('lastname')->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'data-placeholder' => 'Wybierz pracowników',  'multiple' => true] )->label('Pracownicy') ?> 
        </fieldset>
    </div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>