<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>


<table  class="table table-striped table-items header-fixed"  id="table-events"
        data-toolbar="#toolbar-data" 
        data-toggle="table" 
        data-show-refresh="false" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"
        data-url=<?= Url::to(['/company/employee/events', 'id'=>$id]) ?>
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-side-pagination="client"
        data-row-style="rowStyle">	  
        
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="delay" data-visible="false">DELAY</th>
            <th data-field="name" data-sort-name="sname"  data-sortable="true" >Tytuł</th>
            <th data-field="type"  data-sortable="true" >Typ</th>
            <th data-field="status"  data-sortable="true" >Status</th>
            <th data-field="category"  data-sortable="true" >Waga</th>
            <th data-field="deadline"  data-sortable="true" >Deadline</th>
            <th data-field="actions" data-events="actionEvents" data-width="30px"></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

    

    

    



