<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['contacts'],
        'method' => 'post',
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control' ] ) ?></div>
        <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control'] ) ?></div>
        <div class="col-sm-3 col-xs-8"><?= $form->field($model, 'id_dict_employee_type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Company\models\CompanyEmployee::listTypes(), ['prompt' => ' - wybierz -', 'class' => 'form-control'] ) ?></div>
        <div class="col-sm-1 col-xs-4 align-right"><br /><?= Html::submitButton(Yii::t('app', 'Szukaj'), ['class' => 'btn bg-purple']) ?></div>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
