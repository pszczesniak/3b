<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use backend\widgets\files\FilesBlock;
use backend\widgets\tasks\CasesTable;
use backend\widgets\tasks\EventsTable;

use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Profil pracownika';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pracownicy'), 'url' => Url::to(['/company/employee/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-view', 'title'=>Html::encode($this->title))) ?>
   <?= Alert::widget() ?>
    <div class="grid">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="grid profile">
                <div class="col-md-4">
                    <div class="profile-avatar">
                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->id."/preview.jpg"))?"/uploads/employees/cover/".$model->id."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= \common\models\Avatar::getAvatar($model->id_user_fk) ?>" >
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="profile-name">
                        <h3><?= $model->fullname ?></h3>
                        <p class="job-title mb0"><i class="fa fa-building"></i> <?= $model->typename ?></p>
                        <!-- <p class="balance"> Pensja: <span>146 PLN</span> </p> -->
                        <!--<a href="#" class="btn bg-teal btn-large mr10"> <i class="fa fa-envelope"></i> Wyślij wiadomość</a>-->
                        <a href="<?= Url::to(['/company/employee/update', 'id' => $model->id]) ?>" class="btn bg-blue"> <i class="fa fa-pencil-alt"></i> Edytuj</a>
						<a href="<?= Url::to(['/timeline/employee', 'id' => $model->id]) ?>" class="btn bg-pink"> <i class="fa fa-history"></i> Aktywność</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="contact-info bt">
                        <div class="grid">
                            <!-- Start .row -->
                            <div class="col-md-4">
                                <dl class="mt20">
                                    <dt class="text-muted">Telefon</dt>
                                    <dd><?= ($model->phone) ? $model->phone : 'brak danych' ?></dd>
                                </dl>
                            </div>
                            <div class="col-md-8">
                                <dl class="mt20">
                                    <dt class="text-muted">Email</dt>
                                    <dd><?= ($model->email) ?  Html::mailto($model->email, $model->email) : 'brak danych' ?></dd>
                                </dl>
                            </div>
                        </div>
                        <!-- End .row -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="profile-about">
                <h4>Podstawowe informacje</h4>

                <div class="table-responsive about-table">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Oddział</th>
                                <td><?= ($model->branch) ? $model->branch['name'] : 'brak danych' ?></td>
                            </tr>
                            <tr>
                                <th>Rodzaj pracownika</th>
                                <td><?= $model->kindname ?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><?= ($model->user['status'] == 10) ? '<span class="label label-success">aktywny</span>' : '<span class="label label-danger">zablokowany</span>' ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Zdarzenia</span></a></li>
						<li><a data-toggle="tab" href="#tab3"><i class="fa fa-folder"></i><span class="panel-tabs--text">Sprawy</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?php /* $this->render('_files', ['model' => $model, 'type' => 1, 'onlyShow' => true]) */ ?>
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 1, 'parentId' => $model->id, 'onlyShow' => true]) ?>
						</div>
						<div class="tab-pane" id="tab2">
                            <?php /* $this->render('_events', ['id' => $model->id])*/ ?>
                            <?= EventsTable::widget(['dataUrl' => Url::to(['/company/employee/events', 'id'=>$model->id]) ]) ?>
						</div>
                        <div class="tab-pane" id="tab3">
                            <?php /*$this->render('_cases', ['id' => $model->id])*/ ?>
                            <?= CasesTable::widget(['dataUrl' => Url::to(['/company/employee/cases', 'id'=>$model->id])]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    

<?php $this->endContent(); ?>
