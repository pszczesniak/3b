<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-employee-form form">

    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => ($model->isNewRecord) ? '' : 'ajaxform', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
               // 'template' => '<div class="grid"><div class="col-xs-3">{label}</div><div class="col-xs-9">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <fieldset><legend>Podstawowe</legend>
            <div class="grid">
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="grid">
                <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
            </div>
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </fieldset>
        <fieldset><legend>Klasyfikacja</legend>
            
            <div class="grid">
                <div class="col-sm-6 col-xs-12">
                    <?php /*$form->field($model, 'id_dict_employee_type_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listTypes(), ['prompt' => ' - wybierz -'] )*/ ?> 
                    <?= $form->field($model, 'id_dict_employee_kind_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listKinds(), ['prompt' => ' - wybierz -'] ) ?> 
                    
                    <?php /*$form->field($model, 'user_type')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listRoles(), ['prompt' => ' - wybierz -'] )*/ ?> 
                </div>
                <div class="col-sm-6 col-xs-12"><br />
                    <?= $form->field($model, 'is_admin', ['template' => '{input} {label} {error}'])->checkbox() ?>
                </div>
				<div class="col-xs-12">
					<div class="example-content <?= ($model->id_dict_employee_kind_fk == 2) ? '' : 'none' ?>" id="employeeColor">
						<div class="example-content-widget">
							<div id="colorPicker" class="input-group colorpicker-component">
								<input type="text" value="<?= $model->custom_data ?>" class="form-control" name="CompanyEmployee[custom_data]" />
								<span class="input-group-addon"><i></i></span>
							</div>
						</div>
					</div>
				</div>
            </div>
        </fieldset>
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
	document.getElementById('companyemployee-id_dict_employee_kind_fk').onchange = function(event) {
        if(event.target.value == 2) {
			document.getElementById('employeeColor').classList.remove('none');
		} else {
			document.getElementById('employeeColor').classList.add('none');
        }
        
		return false;
    }
</script>