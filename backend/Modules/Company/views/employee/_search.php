<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-company-employees-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-employees']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3"><?= $form->field($model, 'lastname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-employees', 'data-form' => '#filter-company-employees-search' ]) ?></div>
        <div class="col-sm-3"><?= $form->field($model, 'id_dict_employee_kind_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Company\models\CompanyEmployee::listKinds(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-employees', 'data-form' => '#filter-task-employees-search' ] ) ?></div>
        <div class="col-sm-3"><?= $form->field($model, 'status', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Company\models\CompanyEmployee::listStatus(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-employees', 'data-form' => '#filter-task-employees-search' ] ) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
