<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = Yii::t('app', 'Update') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pracownicy'), 'url' => Url::to(['/company/employee/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->firstname.' '.$model->lastname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edycja' ;
?>

    <div class="grid">
        <div class="col-sm-6 col-md-7 col-xs-12">
            <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-update', 'title'=>Html::encode($this->title))) ?>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            <?php $this->endContent(); ?>
        </div>
        <div class="col-sm-6 col-md-5 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-teal"> 
                    <span class="panel-title"> <span class="fa fa-image"></span> Zdjęcie </span> 
                    <div class="panel-heading-menu pull-right">
                        <a aria-controls="company-employee-update-image" aria-expanded="false" href="#company-employee-update-image" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body" id="company-employee-update-image">
                    <?php /*echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                [
                                    'id' => 'imgAttachment',
                                    'model' => $model,
                                    'behaviorName' => 'coverBehavior',
                                    'apiRoute' => '/company/employee/imgAttachApi',
                                ]
                            
                            )*/
                    ?>
                    <div id="upload-demo" data-avtr="<?= ($model->id_user_fk == \Yii::$app->user->id) ? 1 : 0 ?>"></div>
                    <div class="imageAttachment" id="upload-demo-id" data-id="<?= (is_file("uploads/users/avatars/avatar-".$model->id.".jpg")) ? $model->id : 0 ?>">
                        <div class="btn-toolbar actions-bar">
                            <span class="btn btn-success btn-file">
                                <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                                <span data-replace-text="Zamień..." data-upload-text="Prześlij" class="file_label">Pobierz zdjęcie</span>
                                <input type="file" id="upload" name="Avatar[avatar]" value="Choose a file" data-path="<?= Url::to(['/admin/default/upload','id' => $model->id]) ?>">
                            </span>
                            <button class="btn btn-success upload-result" data-path="<?= Url::to(['/admin/default/saveavatar','id' => $model->id]) ?>">Zapisz avatar</button>
                            <!--<button class="btn btn-success change-view">Ustaw miniaturę</button>-->
                        </div>  
                    </div>
                </div>
            </div>
			
			<!-- -->
			
            <div class="panel panel-default">
                <div class="panel-heading bg-purple"> 
					<span class="panel-title"> <span class="fa fa-paperclip"></span> Dokumenty </span> 
					<div class="panel-heading-menu pull-right">
                        <a aria-controls="company-employee-update-docs" aria-expanded="true" href="#company-employee-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
				</div>
                <div class="panel-body" id="company-employee-update-docs">
                    <?php /*$this->render('_files', ['model' => $model, 'type' => 1, 'onlyShow' => false])*/ ?>
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 1, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
            </div>
			
			<!-- -->
			
			
        </div>
    </div>


