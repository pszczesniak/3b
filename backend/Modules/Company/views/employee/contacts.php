<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Książka kontaktów');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-index', 'title'=>Html::encode($this->title))) ?>
    <div class="panel panel-default">
        <div class="panel-heading bg-purple"> 
            <span class="panel-title"> <span class="fa fa-filter"></span> Filtorowanie danych </span> 
            <div class="panel-heading-menu pull-right">
                <a aria-controls="widget-company-contacts-filter" aria-expanded="true" href="#widget-company-contacts-filter" data-toggle="collapse" class="collapse-link collapse-window">
                    <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                </a>
            </div>
        </div>
        <div class="panel-body collapse in" id="widget-company-contacts-filter">
            <?php  echo $this->render('_csearch', ['model' => $searchModel]); ?>
        </div>
    </div>
    
    <div class="grid">
        <div class="col-sm-12 col-xs-1">
                <button class="active btn btn-alphabet" id="all" title="wszyscy"><i class="fa fa-users"></i></button>
                <?php
                    foreach($filter as $i => $value) {
                        echo '<button class="btn btn-alphabet" id="'.$value.'">'.$value.'</button> ';
                    }
                ?>
        </div>
        <div class="col-sm-12 col-xs-11">
            <div class="grid" id="parent">
                <?php
                    foreach($employees as $key => $employee) {
                        $avatar = (is_file(\Yii::getAlias('@webroot') . "/../.." . \Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee->id."/preview.jpg"))?"/uploads/employees/cover/".$employee->id."/preview.jpg":"/images/default-user.png";

                        echo '<div class="col-sm-4 col-xs-12 box '.mb_substr($employee->lastname,0,1,'UTF-8').'">'
                                .'<div class="contact-box">'
                                    .'<div class="grid">'
                                        .'<div class="col-sm-4 col-xs-12">'
                                            .'<div class="text-center">'
                                                .'<div class="avatar"><img alt="avatar"  src="'.$avatar.'"></div>'
                                            .'</div>'
                                        .'</div>'
                                        .'<div class="col-sm-8 col-xs-12">'
                                            .'<h4>'.$employee->lastname.' '.$employee->firstname.'</h4>'
											.'<p class="small">'.$employee->type['name'].'</p>'
                                            .'<p><i class="fa fa-map-marker"></i> '. (($employee->branch) ? $employee->branch['name'] : 'brak danych o oddziale') .'</p>'
                                            .'<p class="small"><i class="fa fa-address-card text--purple"></i> <a href="'.Url::to(['/company/employee/viewajax', 'id' => $employee->id]).'" class="gridViewModal text--purple" title="zobacz wizytówkę" data-title="<i class=\'fa fa-address-card\'></i>wizytówka" data-target="#modal-grid-item" >szczegóły</a></p>'
                                        .'</div>'
										.'<div class="col-xs-12">'
											.'<address>'
                                                //.'<strong>Twitter, Inc.</strong><br>'
                                                . '<i class="fa fa-envelope"></i> '.(($employee->email) ?  Html::mailto($employee->email, $employee->email) : 'brak danych').'<br />'
                                                .'<abbr title="Phone"><i class="fa fa-phone"></i></abbr> '.( ($employee->phone) ? $employee->phone : 'brak danych')
                                            .'</address>'
										.'</div>'
                                        .'<div class="clearfix"></div>'
                                    .'</div>'
                                .'</div>'
                            .'</div>';
                    }
                ?>     
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>

