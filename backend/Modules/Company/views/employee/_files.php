<?php
    use backend\widgets\FilesWidget;
?>

<?php if(!$model->isNewRecord) { ?>
    <?php if(!$onlyShow) echo FilesWidget::widget(['typeId' => $type, 'parentId' => $model->id, 'view' => false]) ?>
    <ul class="files-container">
        <?php
            foreach($model->files as $key => $file) {
                $actions = (!$onlyShow) ? '<a class="file-delete" href="/files/delete/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-trash"></i></a>'
                                           .'<a class="file-update gridViewModal" data-target="#modal-grid-file" href="/files/update/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-pencil"></i></a>' : '';
                echo '<li id="file-'.$file->id.'">'
                        .'<div class="desc">'
                            .'<span class="attachment"><a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a></span>'
                            .'<p>'.$file->user.' <i class="fa fa-clock"></i>'.$file->created_at.'</p>'
                        .'</div>'
                        .$actions
                    .'</li>';
            }
        ?>
    </ul>
<?php } else {
        echo '<div class="alert alert-warning">Aby dodać pliki musisz utworzyć pracownika.</div>';
    } 
?>