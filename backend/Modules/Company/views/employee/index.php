<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pracownicy');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 
	<div id="toolbar-employees" class="btn-group toolbar-table-widget">
		
        <?= ( ( count(array_intersect(["employeeAdd", "grantAll"], $grants)) > 0 ) ) ?Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/company/employee/create']) , 
					['class' => 'btn btn-success btn-icon', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) : '' ?>
        <?= ( ( count(array_intersect(["employeeExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/company/employee/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
                     'data-form' => "#filter-company-employees-search", 
					 'data-table' => "table-employees",
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-title' => "Eksport"
					]) : '' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-employees"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
        
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-employees"
                data-toolbar="#toolbar-employees" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-search-form="#filter-company-employees-search"
                data-url=<?= Url::to(['/company/employee/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="empStatus" data-visible="false">Status</th>
                    <th data-field="firstname"  data-sortable="true" >Imię</th>
                    <th data-field="lastname"  data-sortable="true" >Nazwisko</th>
                    <th data-field="office"  data-sortable="true" >Stanowisko</th>
                    <th data-field="mail"  data-sortable="true" >Mail</th>
                    <th data-field="phone"  data-sortable="true" >Telefon</th>
                    <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>-->
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="130px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	<!-- Render modal form -->
	<?php
		/*yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
          'footer' => Html::submitButton('Zapisz zmiany', ['class' => 'btn btn-primary']).' <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'size' => 'modal-lg',
          'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
		]);
	 
		echo '<div class="modalContent"><div style="text-align:center"><img src="/images/ajax-loader-img.gif"></div></div>';
        

		yii\bootstrap\Modal::end();*/
	?> 
   
</div>
<?php $this->endContent(); ?>


