<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use backend\widgets\tasks\TodoList;
?>
<div class="grid dashboard-stats" data-action="<?= Url::to(['/task/personal/stats']) ?>">
    <a class="col-md-3 col-sm-6 col-xs-12 bg-white bg-link" href="<?= Url::to(['/task/personal/actions']) ?>" title="Przejdź do zadań">
        <div class="block block--head">
            Liczba wizyt          
            <div class="block__spacer"></div>
            <span class="tag tag--pink">na dziś</span>
        </div>
        <div class="block block--col block--main">
            <h3 class="headline-1 stats-todos"><?= $stats['visits_today'] ?> </h3>
            <span> Okres: <?= date('Y-m-d') ?> </span>
            <i class="fa fa-calendar-alt icon--pink icon--bg"></i>
        </div>
    </a>
    
    <a class="col-md-3 col-sm-6 col-xs-12 bg-white bg-link" href="<?= Url::to(['/eg/patient/index']) ?>" title="Przejdź do rejestru klientów">
        <div class="block block--head">
            Liczba pacjentów
            <div class="block__spacer"></div>
            <span class="tag tag--teal">aktywnych</span>
        </div>
        <div class="block block--col block--main">
            <h3 class="headline-1 stats-deadline"><?= $stats['patients'] ?></h3>
            <span> &nbsp; </span>
            <i class="fa fa-users icon icon--teal icon--bg"></i>
        </div>
    </a>

    <a class="col-md-3 col-sm-6 col-xs-12 bg-white bg-link" href="<?= Url::to(['/eg/visit/index']) ?>" title="Przejdź do rejestru wizyt">
        <div class="block block--head">
            Liczba wizyt 
            <div class="block__spacer"></div>
            <span class="tag tag--purple">zrealizowanych</span>
        </div>
        <div class="block block--col block--main">
            <h3 class="headline-1 stats-actions"><?= $stats['visits_month'] ?> </h3>
            <span> Okres: <?= date('Y-m') ?> </span>
            <i class="fa fa-calendar-check icon--purple icon--bg"></i>
        </div>
    </a>

    <a class="col-md-3 col-sm-6 col-xs-12 bg-white bg-link" href="<?= Url::to(['/community/chat/index']) ?>" title="Przejdź do zadań">
        <div class="block block--head">
            Wiadomości
            <div class="block__spacer"></div>
            <span class="tag tag--blue">od pacjentów</span>
        </div>
        <div class="block block--col block--main">
            <h3 class="headline-1 stats-comments"><?= $stats['comments'] ?></h3>
            <span> &nbsp; </span>
            <i class="fa fa-envelope icon--blue icon--bg"></i>
        </div>
    </a>
</div>

<div class="grid grid--start">
    <div class="col-md-4 col-xs-12 col-grid">
        <div class="grid grid--column">
            <div class="col-xs-12 bg-white">
                <div class="block">
                    <div>
                        <h3 class="headline-2"><?= $model->fullname ?></h3>
                        <p><b>Stanowisko: </b><?= $model->type['name'] ?></p>
                    </div>
                    <div class="block__spacer"></div>
                    <figure class="avatar avatar--big">
                        <img src="<?= \common\models\Avatar::getAvatar(Yii::$app->user->id) ?>" alt="avatar" class="avtr">
                    </figure>
                </div>
                <div class="block block--footer">
                    <a class="btn bg-green viewModal" data-target="#modal-grid-item" href="<?= Url::to(['/task/personal/createajax', 'id' => 0]) ?>" title="Dodaj zadanie do kalendarza osobistego"><span class="fa fa-plus-circle"></span> Zadanie</a>
                    <a class="btn bg-blue" href="<?= Url::to(['/admin/profile']) ?>" title="Twój profil w systemie"><span class="fa fa-user"></span> Profil</a>
                    <!--<a class="btn btn--orange" href="#" title="TITLE"><span class="fa fa-cog"></span> Na skróty</a>-->
                </div>
            </div>
            <div class="col-xs-12 bg-white">
                <?= TodoList::widget([]) ?>
            </div>
            <!--<div class="col-xs-12">
                <div class="grid grid--0">
                    <div class="col-sm-5 col-xs-12 bg-teal">1a</div>
                    <div class="col-sm-7 col-xs-12 bg-white">1b</div>
                </div>
            </div>-->
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="grid grid--0">
            <div class="col-sm-5 col-xs-12 bg-teal event-list-block">
                <div class="cal-desc">
                    <div class="cal-desc-date">
                        <?= date('Y-m-d') ?>
                    </div>
                    <div class="cal-desc-link">
                        <?= Html::a('<i class="fas fa-calendar-plus text--white"></i>', Url::to(['/task/event/new', 'id' => 0]) , 
                                ['class' => 'btn btn-success viewModal', 
                                 'id' => 'datetimepicker_tasks-date',
                                 'data-target' => "#modal-grid-item", 
                                 'data-form' => "event-form", 
                                 'data-table' => "table-events",
                                 'data-title' => "<i class='fas fa-calendar-plus'></i>".Yii::t('app', 'New') 
                                ]) ?>
                    </div>
                    <div class="clear"></div>
                    <div class="cal-desc-name">  Kalendarz </div>  
                </div>
                <div class="event-list-container" >
                    <ul class="event-list" id="datetimepicker_tasks-tasks" >
                        <?php $iEvent = 0;
                        foreach($cases as $key => $case) {
                            if($case->type_fk == 2) {
                                ++$iEvent;
                                $meEvent = false;
                                foreach($case->employees as $i => $emp) {
                                    if($emp['id_employee_fk'] == $employeeId) {
                                        $meEvent = true;
                                    }
                                }
                                echo '<li id="db-event-'.$case->id.'">'
                                        . ( ($meEvent) ? '<i class="fa fa-thumbtack  text--pink"></i>&nbsp;' : '')
                                        .'<a class="gridViewModal" href="'.Url::to(['/eg/visit/showajax', 'id' => $case->id]).'" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>Podgląd" data-target="#modal-grid-item">'.$case->name.' </a>'
                                        .'@ '.( ( $case->event_time && $case->event_time != '00:00') ? $case->event_time : 'cały dzień'/*date('H:i', strtotime($case->date_from))*/ )
                                        .(($meEvent || $isSpecial || $isAdmin) ? ' <a href="'.Url::to(['/task/event/closeajax', 'id' => $case->id]).'" class="event-close deleteConfirm '.( ($case->id_dict_task_status_fk == 2) ? 'done' : '' ).'" data-label="Zamknij zadanie"><i class="fa fa-check"></i></a>' : '')
                                    .'</li>';
                            }
                        }
                        if($iEvent == 0)
                            echo '<li>Brak informacji na wybrany dzień</li>';
                        ?>
                        <!--<li>Lunch with jhon @ 3:30 <a href="#" class="event-close"><i class="fa fa-check"></i></a></li>
                        <li>Coffee meeting with Lisa @ 4:30 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                        <li>Skypee conf with patrick @ 5:45 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                        <li>Gym @ 7:00 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                        <li>Dinner with daniel @ 9:30 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>-->
                    </ul>
                </div>
                <!--<input type="text" class="form-control evnt-input" placeholder="NOTES">-->
            </div>
            <div class="col-sm-7 col-xs-12 bg-white">
                <div class="calendar-block">
                    <div class="cal"><div id="datetimepicker_tasks" data-action="<?= Url::to(['/company/default/events']) ?>"></div></div>
                    <div class="list-group" id="datetimepicker_tasks-cases"> 
                        <?php $iCase = 0;
                            foreach($visits as $key => $item) {
                                    ++$iCase;
								echo '<a id="db-event-'.$item->id.'" href="'.Url::to(['/eg/visit/showajax', 'id' => $item->id]).'" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>Podgląd" data-target="#modal-grid-item" class="list-group-item text-ellipsis viewModal '.( ($item->id_dict_eg_visit_status_fk == 6) ? ' bg-red2' : '').'">'
									. ( ($item->is_confirm) ? '<i class="fa fa-check text--green"></i>&nbsp;' : '')
									.'<span class="badge bg-'.( ($item->id_dict_eg_visit_status_fk == 6) ? 'red' : 'grey').'">'
									. ( date('H:i', strtotime($item->start)) ) .'</span> '
									. $item->patient['fullname'].' ['.$item->doctor['fullname'].']'
									. (($item->additional_info) ? ('<br />info: <b>'.$item->additional_info.'</b>') : '')
									.' </a>';
                                }
                            if($iCase == 0)
								echo '<div class="alert alert-info">Brak wiziyt na wybrany dzień</div>';
                        ?>
                        <!--<a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-danger">7:30</span> Meet a friend </a> 
                        <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-success">9:30</span> Have a kick off meeting with .inc company </a>
                        <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-light">19:30</span> Milestone release </a> 
                        <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-danger">7:30</span> Meet a friend </a> 
                        <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-success">9:30</span> Have a kick off meeting with .inc company </a>
                        <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-light">19:30</span> Milestone release </a>-->
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>