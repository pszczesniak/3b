<div class="event-calendar clearfix">
    <div class="col-lg-7 calendar-block">
    
        <div style="width:100%" id="datetimepicker-demo"></div>
                    
    </div>
    <div class="col-lg-5 event-list-block">
        <div class="cal-day">
            <span><?= date('Y-m-d') ?></span>
            <?= date('D') ?>
        </div>
        <ul class="event-list" style="overflow: hidden; width: auto; height: 305px;">
            <li>Spotkanie z prezem firmy XXX @ 3:30 <a href="#" class="event-close"><i class="fa fa-close"></i></a></li>
            <li>Rozprawa w sądzie rejonowym @ 4:30 <a href="#" class="event-close"><i class="fa fa-close"></i></a></li>
            <li>Konsultacje z działem prawa handlowego @ 5:45 <a href="#" class="event-close"><i class="fa fa-close"></i></a></li>

        </ul>
        <input type="text" class="form-control evnt-input" placeholder="NOTES">
    </div>
</div>