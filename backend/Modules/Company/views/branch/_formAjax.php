<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use backend\widgets\FilesWidget;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form">
    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="grid"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
        <div class="grid">
            <div class="col-sm-4 col-xs-4"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-8 col-xs-8"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        </div>
        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="modal-footer"> 
            <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

        </div>
    <?php ActiveForm::end(); ?>
</div>