<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <div class="filemanager" data-url="<?= Url::to(['/folder/default/destination']) ?>">
            <!--<div class="search">
                <input type="search" placeholder="Find a file.." />
            </div>-->
            <?= $form->field($model, 'id_folder_fk', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( [], ['data-placeholder' => 'wybierz odbiorców', 'class' => 'chosen-select', 'id' => 'selectedFolder'] ); ?>

            <!--<div><select id="selectedFolder" name="selected_folder" class="chosen-select" name="selectedFolder" data-placeholder=" "></select></div>-->
            <div class="breadcrumbs"></div>
            <ul class="data">
                <?php foreach($folders as $key => $item) { ?>
                    <li class="folders-check" data-path="<?= $item['path'] ?>" data-id="<?= $item['id'] ?>">
                        <a href="<?= Url::to(['/company/docs/target', 'id' => $item['id']]) ?>"  data-path="<?= $item['path'] ?>" title="Wybierz katalog" class="folders-check">
                            <i class="icon-folder fa fa-folder fa-3x text--yellow"></i>
                            <span class="name""><?= $item['name'] ?></span> 
                            <span class="details"></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if(count($folders) == 0) echo '<div class="alert alert-info">Brak zasobów</div>'; ?>
            <!--<div class="nothingfound">
                <div class="nofiles"></div>
                <span>No files here.</span>
            </div>-->
        </div>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton(Yii::t('app', 'Move'), ['class' => 'btn btn-success']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>
		
<script language="JavaScript">
    /*var WshNetwork = new ActiveXObject("WScript.Network");
    var oPrinters  = WshNetwork.EnumPrinterConnections();console.log('printer');
    console.log(oPrinters);
    for (var i=0; i<oPrinters.Count(); i+=2) {

      var printer_port = oPrinters.Item(i);
      var printer_name = oPrinters.Item(i+1);
      console.log("Printer " + printer_name + " attached to Port " + printer_port );
    }*/
</script>