<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <?php if(!$model->objects) { ?> <div class="alert alert-danger">Proszę wybrać foldery do udostępnienia</div>
        <?php } else { ?>
        <div class="grid">
            <div class="col-xs-12">
                <?= $form->field($model, 'id_user_fk', [])->dropDownList(ArrayHelper::map(\common\models\User::find()->where('status = 10 and id != '.Yii::$app->user->id)->all(), 'id', 'fullname'), ['id' => 'member', 'class' => 'form-control customer', 'prompt' => '-- wybierz użytkownika --'] )->label(false) ?>
            </div>
            
        </div>
        <small class="text--orange">UWAGA: usunięcie użytkownika spowoduje automatyczne odebranie mu uprawnień go do katalogów podrzędnych</small>
        <?php } ?>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('Udostępnij', ['class' =>'btn bg-purple']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>