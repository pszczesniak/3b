<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <?php if(!$model->objects) { ?> <div class="alert alert-danger">Proszę wybrać foldery do udostępnienia</div>
        <?php } else { ?>
        <div class="grid">
            <div class="col-xs-12">
                <?= $form->field($model, 'id_user_fk', ['template' => '
                                                  {label}
                                                   <div class="input-group ">
                                                        {input}
                                                        <span class="input-group-addon bg-green">'.
                                                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/admin/default/member']) , 
                                                                ['class' => 'viewModal text--white', 
                                                                 'data-target' => "#modal-grid-ajax", 
                                                                 'data-input' => ".member",
                                                                 'data-title' => 'Nowy użytkownik'
                                                                ])
                                                        .'</span>
                                                   </div>
                                                   {error}{hint}
                                               '])->dropDownList(ArrayHelper::map(\common\models\User::find()->where('status = 10 and id != '.Yii::$app->user->id)->all(), 'id', 'fullname'), ['id' => 'member', 'class' => 'form-control customer', 'prompt' => '-- wybierz użytkownika --'] )->label(false) ?>
            </div>
            <div class="col-xs-12">
                <table class="table table-border table-hover bg-navy2">
                    <thead>
                        <tr><th align="center">admin</th><th align="center">usuwanie<br />plików</th><th align="center">dodawanie <br />plików</th><th align="center">pobieranie <br />plików</th></tr>
                    </thead>
                    <tbody>
                        <?php
                            echo '<tr>'
                                     .'<td align="center">'.$form->field($model, 'is_admin', ['template' => '{input}'])->checkbox(['label' => null])->label(false).'</td>'
                                     .'<td align="center">'.$form->field($model, 'is_delete', ['template' => '{input}'])->checkbox(['label' => null])->label(false).'</td>'
                                     .'<td align="center">'.$form->field($model, 'is_upload', ['template' => '{input}'])->checkbox(['label' => null])->label(false).'</td>'
                                     .'<td align="center">'.$form->field($model, 'is_download', ['template' => '{input}'])->checkbox(['label' => null])->label(false).'</td>'
                                .'</tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <small class="text--orange">UWAGA: dodanie użytkownika spowoduje automatyczne przypisanie go do katalogów nadrzędnych</small>
        <?php } ?>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('Udostępnij', ['class' =>'btn bg-purple']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>