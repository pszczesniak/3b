<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Działy');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-department-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 
	<div id="toolbar-data" class="btn-group">
        <?= ( ( count(array_intersect(["departmentAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/company/department/createajax']) , 
                    ['class' => 'btn btn-success btn-icon viewModal', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]):'' ?>
   
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"

                data-method="get"
                data-url=<?= Url::to(['/company/department/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name" data-sortable="true">Nazwa</th>
                    <th data-field="symbol" data-sortable="true">Symbol</th>
					<th data-field="manager" data-sortable="true">Kierownik</th>
                    <!--<th data-field="notice" data-sortable="false" data-align="center"></th>-->
                    <th data-field="all" data-sortable="false" data-align="center"></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="120px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modalAjaxForm'
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 

<?php $this->endContent(); ?>
