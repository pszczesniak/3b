<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

        
        <?php if(!$model->isNewRecord)
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name', 'symbol',
                        [                      
                            'attribute' => 'notification_email',
                            'format'=>'raw',
                            'value' => ($model->notification_email == 1) ? 'TAK' : 'NIE',
                        ],  
                        [                      
                            'attribute' => 'all_employee',
                            'format'=>'raw',
                            'value' => ($model->all_employee == 1) ? 'TAK' : 'NIE',
                        ],    
                        [                      
                            'attribute' => 'id_employee_manager_fk',
                            'format'=>'raw',
                            'value' => $model->manager,
                        ], 
                        [                      
                            'attribute' => 'employees_list:html',
                            'format'=>'raw',
                            'label' => 'Pracownicy',
                            'value' => $model->employees_list,
                        ],         
                    ],
                ]) ?>
            
    <div class="modal-footer"> 
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Zamknij</button>
    </div>
<?php ActiveForm::end(); ?>