<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
    ],
]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

        <?php /*$form->field($model, 'notification_email')->checkbox()*/ ?>
        
        <?= $form->field($model, 'all_employee')->checkbox() ?>
        
        <?= $form->field($model, 'id_employee_manager_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getManagers(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 

        <?php if(!$model->isNewRecord)
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                       /* [                      
                            'attribute' => 'id_employee_manager_fk',
                            'format'=>'raw',
                            'value' => $model->manager,
                        ], */
                        [                      
                            'attribute' => 'employees_list:html',
                            'format'=>'raw',
                            'label' => 'Pracownicy',
                            'value' => $model->employees_list,
                        ],         
                    ],
                ]) ?>
            
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>