<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Folder') ?></span></a></li>
                    <li><a data-toggle="tab" href="#tab2b"><i class="fas fa-files-o"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Attachments') ?></span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
					<div class="tab-pane active" id="tab2a">
						<?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'item_description')->textarea(['rows' => 3, 'placeholder' => 'Wpisz dodatkowy opis...'])->label(false) ?>
					</div>
					<div class="tab-pane" id="tab2b">
						<div class="files-wr" data-count-files="1" data-title="<?= \Yii::t('app', 'Attach file') ?>">
							<div class="one-file">
								<label for="file-1"><?= \Yii::t('app', 'Attach file') ?></label>
								<input class="attachs" name="file-1" id="file-1" type="file">
								<div class="file-item hide-btn">
									<span class="file-name"></span>
									<span class="btn btn-del-file">x</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>