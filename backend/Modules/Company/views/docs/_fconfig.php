<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Plik') ?></span></a></li>
                    <!--<li><a data-toggle="tab" href="#tab2b"><i class="fa fa-users"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Users') ?></span> </a></li>
                    <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-paperclip"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Attachments') ?></span> </a></li>-->
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
					<div class="tab-pane active" id="tab2a">
						
                        <?= $form->field($model, 'name_file')->textInput(['maxlength' => true])->label('Nazwa') ?>
						<?= $form->field($model, 'describe_file')->textarea(['rows' => 3, 'placeholder' => 'Wpisz dodatkowy opis...'])->label(false) ?>
					</div>
			</div>
		</div>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="<?= Url::to(['/folder/shared/grants', 'id' => $model->id]) ?>" class="btn btn-icon bg-orange"><i class="fa fa-key"></i>Uprawnienia</a>-->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-icon bg-purple']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>