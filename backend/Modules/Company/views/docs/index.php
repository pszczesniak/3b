<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use common\widgets\Alert;
    
    $this->title = 'Dokumenty firmowe';
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-docs-index', 'title'=>Html::encode($this->title))) ?>

<div class="view-inbox">
    <div class="inbox-center">
        <?php if($breadcrumbs) { ?>
            <ol class="breadcrumb">
                <?php foreach($breadcrumbs as $key => $item) {
                    echo ($item['path']) ? '<li><a href="'.$item['path'].'">'.$item['name'].'</a></li>' : '<li class="active">'.$item['name'].'</li>';
                } ?>
                <!--<li class="active">Data</li>-->
            </ol>
        <?php } ?>
        <div id="toolbar-folder" class="btn-group toolbar-table-widget">
            <?= Html::a('<i class="fa fa-plus"></i> folder', Url::to(['/company/docs/create', 'id' => $folder->id]) , 
                ['class' => 'btn bg-green btn-icon btn-create viewModal', 
                 'id' => 'folder-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-task-inbox-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
            ]) ?>
            
            <?= Html::a('<i class="fa fa-upload"></i> pliki', Url::to(['/company/docs/uploads', 'id' => $folder->id]) , 
                ['class' => 'btn bg-blue btn-icon btn-create viewModal', 
                 'id' => 'folder-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-task-inbox-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='fa fa-upload'></i>Pobierz pliki"
            ]) ?>
            
            <div class="btn-group" id="actionsDates-btn">
                <button type="button" class="btn btn-icon bg-purple" id="group"><i class="fa fa-cogs"></i>Operacje (<span id="group-counter">0</span>)</button>
                <button type="button" class="btn bg-purple dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <?= '<li class="group-actions">'
                           . '<a href="'.Url::to(['/company/option/move']).'" class="viewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Prześlij dalej"><i class="fa fa-arrows-alt text--blue"></i>&nbsp;przenieś</a>'
                        .'</li>' ?>
                    <?= '<li class="group-actions">'
                           . '<a href="'.Url::to(['/company/option/sdelete']).'" class="viewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Usuń"><i class="fa fa-trash text--red"></i>&nbsp;usuń</a>'
                        .'</li>' ?>
                    <!--<li role="separator" class="divider"></li>-->
                </ul>
            </div>
        
            <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-folder"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
        </div>
        <div class="div-table">
            <table  class="table table-custom table-items header-fixed table-widget table-multi-action"  id="table-folder"
                    data-toolbar="#toolbar-folder" 
                    data-toggle="table-widget" 
                    data-show-refresh="false" 
                    data-show-toggle="true"  
                    data-show-columns="false" 
                    data-show-export="false"  
                
                    data-show-pagination-switch="false"
                    data-pagination="true"
                    data-id-field="id"
                    data-show-header="false"
                    data-show-footer="false"
                    data-side-pagination="server"
                    data-row-style="rowStyle"
                    data-sort-name="name"
                    data-sort-order="asc"
                    data-method="get"
                    data-search-form="#filter-folder-search"
                    data-unique-id="nid"
                    data-url=<?= Url::to(['/company/docs/data', 'id' => $folder->id]) ?>>
                <thead>
                    <tr>
                        <th data-field="id" data-visible="false">ID</th>
                        <th data-field="action" data-visible="false">ID</th>
                        <th data-field="state" data-visible="true" data-checkbox="true" data-align="center" data-width="20px"></th>
                        <th data-field="type"  data-sortable="false" data-align="center" data-width="30px"></th>
                        <th data-field="title"  data-sortable="false"></th>
                        <th data-field="size"  data-sortable="false"></th>
                        <th data-field="creator"  data-sortable="false"></th>
                        <th data-field="date" data-sortable="false" data-align="center" data-width="60px"></th>
                        <th data-field="actions" data-events="actionEvents" data-sortable="true" data-align="center" data-width="40px"></th>
                    </tr>
                </thead>
                <tbody class="ui-sortable">

                </tbody>
                
            </table>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>