<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zastępstwa');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-replacement-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'branchId' => $branchId]); ?>
 
	<div id="toolbar-replacement" class="btn-group toolbar-table-widget">
        <?= ( ( count(array_intersect(["grantAll"], $grants)) > 0 || $isSpecial == 1 || $isAdmin == 1) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/company/replacement/createajax']) , 
                    ['class' => 'btn btn-success btn-icon gridViewModal', 
                     'id' => 'resource-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-replacement"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-replacement"
                data-toolbar="#toolbar-replacement" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-search-form="#filter-company-replacement-search"
                data-method="get"
                data-url=<?= Url::to(['/company/replacement/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="employee"  data-sortable="true" data-width="35%">Pracownik</th>
					<th data-field="substitute"  data-sortable="true" data-width="15%">Zastępstwo za</th>
                    <th data-field="date_from"  data-sortable="true" data-width="20%">Od kiedy</th>
                    <th data-field="date_to"  data-sortable="true" data-width="20%">Do kiedy</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>


<?php $this->endContent(); ?>
