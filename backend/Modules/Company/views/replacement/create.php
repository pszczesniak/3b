<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyDepartment */

$this->title = Yii::t('app', 'Create Company Department');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-department-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
