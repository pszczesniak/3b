<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-replacement"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <div class="grid">
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-replacement', 'data-form' => '#filter-company-replacement-search' ] ) ?></div>
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'substitute_for')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-replacement', 'data-form' => '#filter-company-replacement-search' ] ) ?></div>
  
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="companyreplacement-date_from" class="control-label">Początek</label>
                        <div class='input-group date' id='datetimepicker_start'>
                            <input type='text' class="form-control" id="companyreplacement-date_from" name="CompanyReplacement[date_from]" value="<?= $model->date_from ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
                        $('#datetimepicker_start').on("dp.change", function (e) {
                                                /*console.log(e.date);*/
                                                console.log(e.date.add(1, 'hours').format('YYYY-MM-DD'));
                                                minDate = e.date.add(1, 'hours').format('YYYY-MM-DD');
                                               // $('#datetimepicker_end').data("DateTimePicker").minDate(minDate);
                                                $('#datetimepicker_end input').val(minDate);
                                            });
                    });
                </script>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="companyreplacement-date_to" class="control-label">Koniec</label>
                        <div class='input-group date' id='datetimepicker_end'>
                            <input type='text' class="form-control" id="companyreplacement-date_to" name="CompanyReplacement[date_to]" value="<?= $model->date_to ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
                    });
                </script>

            </div>
        </div>
    </div>   
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

