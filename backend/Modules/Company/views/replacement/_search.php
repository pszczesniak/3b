<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\company\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-company-replacement-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-replacement']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-12">
			<div class="form-group field-companyreplacement-date_from">
				<label for="companyreplacement-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="companyreplacement-date_from" name="CompanyReplacement[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-3 col-md-3 col-xs-12">
			<div class="form-group field-companyreplacement-date_to">
				<label for="companyreplacement-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="companyreplacement-date_to" name="CompanyReplacement[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-3"><?= $form->field($model, 'id_employee_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-replacement', 'data-form' => '#filter-company-replacement-search' ] ) ?></div>
        <div class="col-sm-3"><?= $form->field($model, 'substitute_for')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-replacement', 'data-form' => '#filter-company-replacement-search' ] ) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
