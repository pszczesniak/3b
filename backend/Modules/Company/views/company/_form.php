<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grid">
    <div class="col-sm-7">
        <div id="validation-form" class="alert none"></div>
        <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform', ],
            /*'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],*/
        ]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        <div class="grid">
            <div class="col-xs-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="grid">
            <div class="col-xs-6"><?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-6"><?= $form->field($model, 'regon')->textInput(['maxlength' => true]) ?></div>
        </div>
       
        <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>

     
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Logo firmy</h3>
            </div>
            <div class="panel-body">
                <ul>
                    <li><h4>Plik w formacie JPG lub PNG &nbsp;&nbsp;<a href="<?= Url::to(['/company/company/logo', 'id' => 1]) ?>" class="btn btn-sm btn-info viewModal" data-target="#modal-grid-item" data-title="Zmiana logo firmy">Zmień</a></h4></li>
                </ul>
            </div>
        </div>
        
    </div>

</div>
