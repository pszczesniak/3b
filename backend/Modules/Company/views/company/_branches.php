<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Company\models\CompanyBranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="ajax-table-items">	
	<div id="toolbar-data" class="btn-group">
		<?= (1==1)?Html::a('Dodaj', Url::to(['/company/branch/createajax']) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>

	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-url=<?= Url::to(['/company/branch/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true">Nazwa</th>
                    <th data-field="symbol"  data-sortable="true">Symbol</th>
                    <th data-field="address"  data-sortable="true">Address</th>
                    <th data-field="phone"  data-sortable="true" data-align="center">Telefon</th>
					<th data-field="email"  data-sortable="true" data-align="center">Email</th>
                    <th data-field="actions" data-events="actionEvents"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modalAjaxForm'
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 