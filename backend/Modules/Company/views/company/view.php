<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dane'), 'url' => ['view', 'id' => 1]];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-view', 'title'=>Html::encode($this->title))) ?>

    
    <?php /* DetailView::widget([
        'model' => $model,
        'attributes' => [
            'address',
            'phone',
            'email:email',
            'nip',
            'regon',
            'account_number',
            [                      
				'attribute' => 'branches_list:html',
				'format'=>'raw',
				'label' => 'Oddziały',
				'value' => $model->branches_list,
			], 
           
        ],
    ])*/ ?>
    <div class="grid">
        <div class="col-sm-7">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('<i class="fa fa-pencil-alt"></i>'.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-primary']) ?>
                <?php  if(count(array_intersect(["grantAll"], $grants)) > 0) { ?> 
					<?= Html::a('<i class="fa fa-cog"></i>'.Yii::t('app', 'Konfiguracja'), ['config', 'id' => $model->id], ['class' => 'btn btn-icon bg-pink']) ?>
				<?php } ?>
                <?php /*Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])*/ ?>
            </p>

            <div class="list-group">
                <div class="list-group-item btn-icon"><i class="fa fa-map-marker"></i><?= ($model->address) ? $model->address : 'brak danych' ?></div>
                <div class="list-group-item btn-icon"><i class="fa fa-envelope"></i> <?= ($model->email) ? Html::mailto($model->email, $model->email) : 'brak danych' ?></div>
                <div class="list-group-item btn-icon"><i class="fa fa-phone"></i><?= ($model->phone) ? $model->phone : 'brak danych' ?></div>
                <div class="list-group-item bg-purple2">REGON: <?= ($model->regon) ? $model->regon : 'brak danych' ?></div>
                <div class="list-group-item bg-purple2">NIP: <?= ($model->nip) ? $model->nip : 'brak danych' ?></div>
                <div class="list-group-item bg-purple2">NUMER KONTA: <?= ($model->account_number) ? $model->account_number : 'brak danych' ?></div>
            </div>
        </div>
        
    </div>

<?php $this->endContent(); ?>
