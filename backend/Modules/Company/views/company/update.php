<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = Yii::t('app', 'Aktualizacja');
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Aktualizacja');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-update', 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?php $this->endContent(); ?>
<?php /*$this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-branch', 'title'=>'Regiony'))*/ ?>

    <?php /* $this->render('_branches', [
        'model' => $model,
    ])*/ ?>

<?php /* $this->endContent(); */ ?>
