<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Company', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <div class="input-file-container">  
            <input class="input-file" id="my-file" type="file" name="assets" accept="<?= $mimeType ?>">
            <!--<label tabindex="0" for="my-file" class="input-file-trigger">wybierz plik...</label>-->
        </div>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>

<script>
    /* document.querySelector("html").classList.add('js');

        var fileInput  = document.querySelector( ".input-file" ),  
            button     = document.querySelector( ".input-file-trigger" ),
            the_return = document.querySelector(".file-return");
              
        button.addEventListener( "keydown", function( event ) {  
            if ( event.keyCode == 13 || event.keyCode == 32 ) {  
                fileInput.focus();  
            }  
        });
        button.addEventListener( "click", function( event ) {
           fileInput.focus();
           return false;
        });  
        fileInput.addEventListener( "change", function( event ) {  
            the_return.innerHTML = this.value;  
    }); */
</script>