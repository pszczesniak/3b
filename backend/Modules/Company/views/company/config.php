<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = 'Konfiguracja systemu';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dane firmy'), 'url' => ['view', 'id' => 1]];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-view', 'title'=>Html::encode($this->title))) ?>


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>'.Yii::t('app', 'Edycja danych firmy'), ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-primary']) ?>
    </p>
    
    <div class="company-form form">
        <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform', ],
            /*'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],*/
        ]); ?>
        <fieldset><legend>Terminarz</legend>
            <div class="example-content choseColor" >
                <div class="example-content-widget">
                    <label>Kolor wizyty potwierdzonej</label>
                    <div id="colorPicker1" class="input-group colorpicker-component">
                        <input type="text" value="<?= $model->calendarColorConfirmed ?>" class="form-control" name="Company[calendarColorConfirmed]" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="example-content choseColor" >
                <div class="example-content-widget">
                    <label>Kolor wizyty niepotwierdzonej</label>
                    <div id="colorPicker2" class="input-group colorpicker-component">
                        <input type="text" value="<?= $model->calendarColorUnconfirmed ?>" class="form-control" name="Company[calendarColorUnconfirmed]" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="example-content choseColor" >
                <div class="example-content-widget">
                    <label>Kolor wizyty zrealizowanej</label>
                    <div id="colorPicker3" class="input-group colorpicker-component">
                        <input type="text" value="<?= $model->calendarColorDone ?>" class="form-control" name="Company[calendarColorDone]" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <br />
        </fieldset>
        

        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


    

<?php $this->endContent(); ?>
