<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-booking-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-booking']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-companyresourcesschedule-date_from">
				<label for="companyresourcesschedule-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="companyresourcesschedule-date_from" name="CompanyResourcesSchedule[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-companyresourcesschedule-date_to">
				<label for="companyresourcesschedule-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="companyresourcesschedule-date_to" name="CompanyResourcesSchedule[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2"><?= $form->field($model, 'status')->dropDownList( \backend\Modules\Company\models\CompanyResourcesSchedule::listStates(), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-booking', 'data-form' => '#filter-booking-search' ] ) ?></div>
        <div class="col-sm-4 col-md-2"><?= $form->field($model, 'id_resource_fk')->dropDownList( \backend\Modules\Company\models\CompanyResources::getGroups($branchId), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-booking', 'data-form' => '#filter-booking-search' ] ) ?></div>
        <div class="col-sm-4 col-md-2"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['class' => 'form-control  widget-search-list', 'data-table' => '#table-booking', 'data-form' => '#filter-booking-search', 'prompt' => ( ($isAdmin) ? ' ' : ' ')  ] ) ?></div>    
	</div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    document.getElementById('companyresourcesschedule-id_company_branch_fk').onchange = function(event) { 
        $value = (this.value) ? this.value : 0;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/branch/set']) ?>/"+$value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('companyresourcesschedule-id_resource_fk').innerHTML = result.resources;    
           
            }
        }
        xhr.send();
        return false;
    }
</script>