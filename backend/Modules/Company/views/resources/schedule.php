<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Terminarz rezerwacji');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zasoby'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-event-calendar', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-sm-9">
           
            <div id="resources"></div>
         </div> 
        <div class="col-sm-3">
            <?= $this->render('_formFilter', ['model' => $model, 'dicts' => $dicts, 'branchId' => $branchId]) ?>
        </div>
    </div>
<?php $this->endContent(); ?>
    
	
	
	

