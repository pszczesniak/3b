<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-booking"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <!--<div class="well well-sm">
                <div class="grid">
                    <div class="col-sm-6"><h5>Pracownik <span class="label label-primary"><?= $model->employee['fullname'] ?></span></h5></div>
                    <div class="col-sm-6"><h5>Zasób <span class="label label-info"><?= $model->resource['name'] ?></span></h5></div>
                </div> 
            </div>-->
            <?php if($model->status == 1) { ?>
                <div class="alert alert-warning">Rezerwacja została zaakceptowana i nie ma możliwości jej edycji.</div>
            <?php } ?>
            <?php if($model->status <= -1) { ?>
                <div class="alert alert-danger">Rezerwacja została <?= ($model->status == -2) ? 'odrzucenia' : 'anulowania' ?> i nie ma możliwości jej edycji.</div>
                <div class="alert alert-info">Powód <?= ($model->status == -2) ? 'odrzucenia' : 'anulowania' ?>: <b><?= $model->comment ?></b></div>
            <?php } ?>
            
            <div class="grid grid--0">
                <div class='col-sm-4'>
                    <div class="form-group">
                        <label for="companyresourceschedule-date_from" class="control-label">Początek</label>
                        <div class='input-group date' id='datetimepicker_start'>
                            <input type='text' class="form-control" id="companyresourceschedule-date_from" name="CompanyResourcesSchedule[date_from]" value="<?= $model->date_from ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD HH:mm',  });
                        $('#datetimepicker_start').on("dp.change", function (e) {
                                                /*console.log(e.date);*/
                                                console.log(e.date.add(1, 'hours').format('YYYY-MM-DD HH:mm'));
                                                minDate = e.date.add(1, 'hours').format('YYYY-MM-DD HH:mm');
                                               // $('#datetimepicker_end').data("DateTimePicker").minDate(minDate);
                                                $('#datetimepicker_end input').val(minDate);
                                            });
                    });
                </script>
                <div class='col-sm-4'>
                    <div class="form-group">
                        <label for="companyresourceschedule-date_to" class="control-label">Koniec</label>
                        <div class='input-group date' id='datetimepicker_end'>
                            <input type='text' class="form-control" id="companyresourceschedule-date_to" name="CompanyResourcesSchedule[date_to]" value="<?= $model->date_to ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD HH:mm' });
                    });
                </script>
                <div class="col-sm-4">
                    <?= $form->field($model, 'id_company_branch_fk', [])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList( ($isAdmin) ? -1 : -1), 'id', 'name'), ['id' => 'id_branch_fk'] ) ?>
                </div>
            </div>
			<div class="grid">
				<div class="col-sm-6">
					<?php 
                        if($isSpecial == 1 || $isAdmin == 1 /*$model->id_employee_fk == $employeeId*/)
                            echo $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['id' => 'id_employee_fk', 'prompt' => ' - wybierz -', 'class' => 'form-control select2' ] );
                        else
                            echo '<div class="form-group field-companyresourcesschedule-id_employee_fk">'
                                    .'<label class="control-label" for="id_employee_fk">Pracownik</label>'
                                    .'<input id="id_employee_fk" class="form-control" name="CompanyResourcesSchedule[id_employee_fk]" type="text" value="'.$model->employee['fullname'].'" disabled></inpu>'
                                    .'</div>'; 
                    ?>
				</div>
				<div class="col-sm-6">
					<?= $form->field($model, 'id_resource_fk')->dropDownList( \backend\Modules\Company\models\CompanyResources::getGroups($model->id_company_branch_fk), ['prompt' => ' - wybierz -', 'class' => 'form-control select2', 'id' => 'id_resource_fk' ] ) ?>
				</div>
			</div>
            <div class="grid">
                <div class="col-xs-12"><?= $form->field($model, 'describe')->textarea(['rows' => 3, 'placeholder' => 'Podaj powód rezerwacji'])->label(false) ?></div>
                <div class="col-xs-12 align-right">   
                    <!--<fieldset><legend>Powiązane czynności</legend>
                        <?= $form->field($model, 'type_event')->radioList([1 => 'Kancelaryjne', 2 => 'Osobiste'], 
                                ['class' => 'btn-group', 'data-toggle' => "buttons",
                                'item' => function ($index, $label, $name, $checked, $value) {
                                        return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                            Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                    },
                            ])->label(false) ?>
                            <?= $form->field($model, 'id_event_fk')->dropDownList( \backend\Modules\Company\models\CompanyEmployee::getTasks($model->id_employee_fk, $model->type_event) , ['prompt' => '- wybierz -', 'disabled' => ( ($model->type_event) ? false : true ) ] )->label('Zdarzenia'); ?>
                    </fieldset>-->
                    
                    <?= ($model->isNewRecord && ($isSpecial || $isAdmin) ) ? $form->field($model, 'accept')->checkbox() : '' ?>
                </div>
            </div>
            
            <?php if(!$model->isNewRecord ) { ?>
            <div class="more-content">
               <table class="table table-hover table-condensed">
                    <thead><tr><th>Operacja</th><th>Pracownik</th><th>Data</th><th>Komentarz</th></tr></thead>
                    <tbody>
                    <?php
                        foreach($model->history as $key => $item) {
                            echo '<tr><td>'.$item['action'].'</td><td>'.$item['employee'].'</td><td>'.$item['date'].'</td><td>'.$item['comment'].'</td></tr>';
                        }
                    ?>
                    </tbody>
               </table>
            </div>
            <p><a class="showhistory text--pink" href="#">Rozwiń historię (+)</a>
        <?php } ?>        
        </div> 
    </div>       
    <div class="modal-footer"> 
        <?php if(!$model->isNewRecord && ($isSpecial != 1 && $model->id_employee_fk != $employeeId) ) { ?>
           <!-- <div class="alert alert-danger">Nie masz uprawnień do żadnych akcji dla tej rezerwacji</div> -->
        <?php } ?>
        
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?php if($model->status == 0) { echo ($model->isNewRecord || ($isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk']) || $isAdmin == 1 || $model->id_employee_fk == $employeeId) ? Html::submitButton($model->isNewRecord ? Yii::t('app', 'Rezerwuj') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) : ''; } ?>
        <?php if($model->status == 0 && !$model->isNewRecord && ( ($isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk'])  || $isAdmin == 1) ) { ?><a href="<?= Url::to(['/company/resources/accept', 'id' => $model->id]) ?>" title="Zatwierdź rezerwację" class="btn btn-sm bg-green deleteConfirm" data-label="Zatwierdź"><i class="fa fa-check"></i></a> <?php } ?>        
        <?php if($model->status == 0 && !$model->isNewRecord && ( ($isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk']) || $isAdmin == 1 ) ) { ?><a href="<?= Url::to(['/company/resources/discard', 'id' => $model->id]) ?>" title="odrzuć rezerwację" class="btn btn-sm bg-red deleteConfirmWithComment" data-label="Odrzuć"><i class="fa fa-ban"></i></a> <?php } ?>        		
		<?php if($model->status == 1 && ( ( $isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk'] ) || $isAdmin == 1 || $model->id_employee_fk == $employeeId) ) { ?><a href="<?= Url::to(['/company/resources/change', 'id' => $model->id]) ?>" title="zmień parametry rezerwacji" class="btn btn-sm bg-purple viewEditModal" data-target="#modal-grid-item"  data-title="<i class='fa fa-exchange'></i>Zmiana"><i class="fa fa-exchange"></i></a> <?php } ?>
		<?php if($model->status >= 1 && ( ( $isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk'] ) || $isAdmin == 1 || $model->id_employee_fk == $employeeId) ) { ?><a href="<?= Url::to(['/company/resources/cancel', 'id' => $model->id]) ?>" title="odwołaj rezerwację" class="btn btn-sm bg-orange deleteConfirmWithComment" data-label="Odwołaj"><i class="fa fa-rotate-left"></i></a> <?php } ?>        				
        <?php if($model->type_event == 2) { ?> <a href="<?= Url::to(['/community/meeting/view', 'id' => $model->id_event_fk]) ?>" title="kartka spotkania" class="btn btn-sm bg-pink" data-label="Odwołaj"><i class="fa fa-handshake-o"></i></a> <?php } ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    
    document.getElementById('id_branch_fk').onchange = function(event) { 
        $value = (this.value) ? this.value : 0;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/branch/set']) ?>/"+$value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('id_employee_fk').innerHTML = result.employees;    
                document.getElementById('id_resource_fk').innerHTML = result.resources;    
                /*$('#caltask-departments_list').multiselect('rebuild');
                $('#caltask-departments_list').multiselect('refresh');   */            
            }
        }
        xhr.send();
        return false;
    }
    $(document).ready( function() {
        $(".showhistory").on('click touchstart', function(event) {
            var txt = $(".more-content").is(':visible') ? 'Rozwiń historię (+)' : 'Zwiń historię (–)';
            $(this).parent().prev(".more-content").toggleClass("visible");
            $(this).html(txt);
            event.preventDefault();
        });
    });
</script>
