<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-booking"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">            
            <div class="grid grid--0">
                <div class='col-sm-4'>
                    <div class="form-group">
                        <label for="companyresourceschedule-date_from" class="control-label">Początek</label>
                        <div class='input-group date' id='datetimepicker_start'>
                            <input type='text' class="form-control" id="companyresourceschedule-date_from" name="CompanyResourcesSchedule[date_from]" value="<?= $model->date_from ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD  HH:mm',  });
                        $('#datetimepicker_start').on("dp.change", function (e) {
                                                console.log(e.date.add(1, 'hours').format('YYYY-MM-DD HH:mm'));
                                                minDate = e.date.add(1, 'hours').format('YYYY-MM-DD HH:mm');
                                                $('#datetimepicker_end input').val(minDate);
                                            });
                    });
                </script>
                <div class='col-sm-4'>
                    <div class="form-group">
                        <label for="companyresourceschedule-date_to" class="control-label">Koniec</label>
                        <div class='input-group date' id='datetimepicker_end'>
                            <input type='text' class="form-control" id="companyresourceschedule-date_to" name="CompanyResourcesSchedule[date_to]" value="<?= $model->date_to ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD  HH:mm' });
                    });
                </script>
                <div class="col-sm-4">
                    <?= $form->field($model, 'id_company_branch_fk', [])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList( ($isAdmin) ? -1 : -1), 'id', 'name'), ['id' => 'id_branch_fk', 'disabled' => true] ) ?>
                </div>
            </div>
			<div class="grid">
				<div class="col-sm-6">
					<?php 
                        if($isSpecial == 1 || $isAdmin == 1 /*$model->id_employee_fk == $employeeId*/)
                            echo $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['id' => 'id_employee_fk', 'prompt' => ' - wybierz -', 'class' => 'form-control select2' ] );
                        else
                           echo $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['id' => $model->id_employee_fk])->all(), 'id', 'fullname'), ['id' => 'id_employee_fk',  'class' => 'form-control select2' ] );

                           /* echo '<div class="form-group field-companyresourcesschedule-id_employee_fk">'
                                    .'<label class="control-label" for="id_employee_fk">Pracownik</label>'
                                    .'<input id="id_employee_fk" class="form-control" name="CompanyResourcesSchedule[id_employee_fk]" type="text" value="'.$model->employee['fullname'].'" disabled></inpu>'
                                    .'</div>'; */
                    ?>
				</div>
				<div class="col-sm-6">
					<?= $form->field($model, 'id_resource_fk')->dropDownList( \backend\Modules\Company\models\CompanyResources::getGroups($model->resource['id_company_branch_fk']), ['prompt' => ' - wybierz -', 'class' => 'form-control select2', 'id' => 'id_resource_fk' ] ) ?>
				</div>
			</div>
            <div class="grid">
                <div class="col-xs-12"><?= $form->field($model, 'describe')->textarea(['rows' => 2, 'placeholder' => 'Podaj powód rezerwacji'])->label(false) ?></div>
                <div class="col-xs-12"><?= $form->field($model, 'change_reason')->textarea(['rows' => 2, 'placeholder' => 'Podaj powód zmiany'])->label(false) ?></div>
                <div class="col-xs-12 align-right">   
                    <!--<fieldset><legend>Powiązane czynności</legend>
                        <?= $form->field($model, 'type_event')->radioList([1 => 'Kancelaryjne', 2 => 'Osobiste'], 
                                ['class' => 'btn-group', 'data-toggle' => "buttons",
                                'item' => function ($index, $label, $name, $checked, $value) {
                                        return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                            Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                    },
                            ])->label(false) ?>
                            <?= $form->field($model, 'id_event_fk')->dropDownList( \backend\Modules\Company\models\CompanyEmployee::getTasks($model->id_employee_fk, $model->type_event) , ['prompt' => '- wybierz -', 'disabled' => ( ($model->type_event) ? false : true ) ] )->label('Zdarzenia'); ?>
                    </fieldset>-->
                    
                    <?php /*($model->isNewRecord && ($isSpecial || $isAdmin) ) ? $form->field($model, 'accept')->checkbox() : ''*/ ?>
                </div>
            </div>
            
            <?php if(!$model->isNewRecord ) { ?>
            <div class="more-content">
               <table class="table table-hover table-condensed">
                    <thead><tr><th>Operacja</th><th>Pracownik</th><th>Data</th><th>Komentarz</th></tr></thead>
                    <tbody>
                    <?php
                        foreach($model->history as $key => $item) {
                            echo '<tr><td>'.$item['action'].'</td><td>'.$item['employee'].'</td><td>'.$item['date'].'</td><td>'.$item['comment'].'</td></tr>';
                        }
                    ?>
                    </tbody>
               </table>
            </div>
            <p><a class="showhistory text--pink" href="#">Rozwiń historię (+)</a>
        <?php } ?>        
        </div> 
    </div>       
    <div class="modal-footer"> 
        <?php if(!$model->isNewRecord && ($isSpecial != 1 && $model->id_employee_fk != $employeeId) ) { ?>
           <!-- <div class="alert alert-danger">Nie masz uprawnień do żadnych akcji dla tej rezerwacji</div> -->
        <?php } ?>
        
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?php if($model->status == 1) { echo ($model->isNewRecord || ($isSpecial == 1 && $branchId == $model->resource['id_company_branch_fk']) || $isAdmin == 1 || $model->id_employee_fk == $employeeId) ? Html::submitButton( 'Zapisz zmiany', ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) : ''; } ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    
    document.getElementById('id_branch_fk').onchange = function(event) { 
        $value = (this.value) ? this.value : 0;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/branch/set']) ?>/"+$value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('id_employee_fk').innerHTML = result.employees;    
                document.getElementById('id_resource_fk').innerHTML = result.resources;    
                /*$('#caltask-departments_list').multiselect('rebuild');
                $('#caltask-departments_list').multiselect('refresh');   */            
            }
        }
        xhr.send();
        return false;
    }
    $(document).ready( function() {
        $(".showhistory").on('click touchstart', function(event) {
            var txt = $(".more-content").is(':visible') ? 'Rozwiń historię (+)' : 'Zwiń historię (–)';
            $(this).parent().prev(".more-content").toggleClass("visible");
            $(this).html(txt);
            event.preventDefault();
        });
    });
</script>
