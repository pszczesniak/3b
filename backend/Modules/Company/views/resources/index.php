<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zasoby');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-resources-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'branchId' => $branchId]); ?>
 
	<div id="toolbar-resources" class="btn-group toolbar-table-widget">
        <?= ( ( count(array_intersect(["grantAll"], $grants)) > 0 || $isSpecial == 1 || $isAdmin == 1) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/company/resources/createajax']) , 
                    ['class' => 'btn btn-success btn-icon gridViewModal', 
                     'id' => 'resource-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]):'' ?>
        <?=  Html::a('<i class="fa fa-calendar"></i>Rezerwacje', Url::to(['/company/resources/schedule']) , 
                    ['class' => 'btn btn-info btn-icon', 
                     'id' => 'resources-schedule',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-title' => "Przejdź do terminarza rezerwacji"
                    ]) ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-resources"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-resources"
                data-toolbar="#toolbar-resources" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-company-resources-search"
                data-method="get"
                data-url=<?= Url::to(['/company/resources/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true" data-width="35%">Nazwa</th>
					<th data-field="type"  data-sortable="true" data-width="15%">Typ</th>
                    <th data-field="branch"  data-sortable="true" data-width="20%">Oddział</th>
                    <th data-field="guardian"  data-sortable="true" data-width="20%">Opiekun</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon bg-purple2 text--purple">xxx</td><td>Zasoby tylko dla zarządu</td></tr>
                </tbody>
            </table>
			<br />
        </fieldset>
    </div>


<?php $this->endContent(); ?>
