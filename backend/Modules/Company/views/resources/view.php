<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zasoby'), 'url' => Url::to(['/company/resources/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company_resource-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading bg-grey">
                    <strong>Informacje</strong>
                    <a href="<?= Url::to(['/company/resources/bookingajax', 'id' => $model->id]) ?>" class="btn bg-yellow btn-sm btn-flat pull-right gridViewModal"data-title="<i class=\'fa fa-hend-pointer-o\'></i>Rezerwacja" data-target="#modal-grid-item"  style="margin-top:-5px"><span class="fa fa-hand-pointer-o"></span> Rezerwuj</a>
                </div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p><span class="bold">Typ</span>: <?= ($model->dict) ? '<i class="'.$model->dict['icon'].'" style="color: '.$model->dict['color'].'"></i>' : '' ?> <?= $model->type ?></p>
                        </div>
                        <?php if($model->only_management) { ?>
							<div class="row-details">
								<p><span class="bold text--red"> <i class="fa fa-exclamation-triangle"></i>&nbsp; Rezerwacje tylko dla zarządu i dyrektorów </span></p>
							</div>
						<?php } ?>
                        <div class="row-details">
                            <p><span class="bold"> Oddział </span>: <?= $model->branch ?> </p>
                        </div>
                        
                        <div class="row-details">
                           <p><span class="bold"> Opis </span>: <?= ( $model->describe ) ? $model->describe : '<div class="alert alert-warning">brak opisu</div>' ?> </p> 
                        </div>
                        
                        <div class="row-details">
                            <p><span class="bold">Utworzono</span>: <?= $model->creator ?> [<?= $model->created_at ?>]</p>
                        </div>
                    </div>
                </div><!--/project-body-->
            </section>
            
            
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            
            <div class="task-body-content">
                <div class="task-content-top">
                    <div class="task-meta clearfix">
                        <div class="row">
                            <div class="col-md-12">
                                <?php if($term) { ?>
                                <div class="panel panel-default" style="width: 100%">
                                    <div class="panel-heading bg-white"> 
                                        <span class="panel-title text--grey"> <span class="fa fa-remove text--red"></span>Odrzucenie rezerwacji</span> 
                                        <div class="panel-heading-menu pull-right">
                                            <a class="collapse-link collapse-window" data-toggle="collapse" href="#termConfig" aria-expanded="true" aria-controls="termConfig">
                                                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="panel-body in" id="termConfig">
                                        
                                        <div id="message">
                                            <ul class="message-container">
                                                <li class="received">
                                                    <div class="details"> 
                                                        <div class="left"><?= $term->employee['fullname'] ?> <div class="arrow brand"></div> <?= date('Y-m-d H:i', strtotime($term->date_from)) .' - '.date('Y-m-d H:i', strtotime($term->date_to)) ?> </div>
                                                        <div class="right">
                                                            <?php
                                                                $colors = [ -2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
                                                                $labels = [-2 => 'odrzucona', -1 => 'anulowana', 0 => 'oczekująca', 1 => 'zaakceptowana'];
                                                            ?>
                                                            <span class="label bg-<?= $colors[$term->status] ?>"><?= $labels[$term->status] ?></span>
                                                        </div>
                                                    </div>
                                                    <div><b class="text--grey">Powód: </b><?= $term->describe ?></div>
                                                    <form method="POST"> 
                                                        <div class="form-group field-companyresourcesschedule-comment required <?= ($txtError) ? ' has-error' : '' ?>">
                                                            <textarea id="companyresourcesschedule-comment" class="form-control" name="CompanyResourcesSchedule[comment]" rows="2" placeholder="Podaj powód odrzucenia"></textarea>
                                                            <div class="help-block"><?= ($txtError) ? $txtError  : '' ?></div>
                                                        </div>
                                            
                                                        <div class="align-right pull-right">
                                                            <button class="btn bg-red" type="submit"  alt="Odrzuć" title="Odrzuć" value="Odrzuć">Odrzuć</button>
                                                        </div>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                   
                </div>
            </div>
           
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-calendar"></i><span class="panel-tabs--text">Terminarz</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?php /*EmployeesBlock::widget(['employees' => $model->employees, 'employeesLock' => $model->employeeslock])*/ ?>
                            <div class="grid grid--0">
                                <div class="col-sm-5 col-xs-12 bg-blue event-list-block">
                                    <div class="cal-desc">
                                        <div class="cal-desc-date">
                                            <?= date('Y-m-d') ?>
                                        </div>
                                       
                                        <div class="clear"></div>
                                        <div class="cal-desc-name"> Terminarz </div>  
                                        <div><small>Rezerwacje oczekujące na akceptację</small></div>                  
                                    </div>
                                    <div class="event-list-container" >
                                        <ul class="event-list-blue" id="datetimepicker_tasks-tasks" >
                                            <?php $reservations = 0;
                                                foreach($forAccept as $key => $term) {
                                                    if($term->status == 0) {
                                                        ++$reservations;
                                                        $meEvent = false;
                                                        if($term->id_employee_fk == $employeeId) {
                                                            $meEvent = true;
                                                        }
                                                        
                                                        echo '<li id="db-event-'.$term->id.'">'
                                                                . ( ($meEvent) ? '<i class="fa fa-hourglass-half text--grey"></i>&nbsp;' : '')
                                                                .'<a class="gridViewModal" href="'.Url::to(['/company/resources/bookingupdate', 'id' => $term->id]).'" data-title="<i class=\'fa fa-hend-pointer-o\'></i>Rezerwacja" data-target="#modal-grid-item">'.$term->employee['fullname'].' </a>'
                                                                .'<br /> <small>'.date('Y-m-d H:i', strtotime($term->date_from)) . ' - ' . date('Y-m-d H:i', strtotime($term->date_to)) .'</small>'
                                                                .'<br /> <div class="event-describe">'.$term->describe.'</div>'
                                                                . ( ($isSpecial) ? ' <a href="'.Url::to(['/company/resources/accept', 'id' => $term->id]).'" class="event-accept deleteConfirm " data-label="Zaakceptuj rezerwację" title="Zaakceptuj rezerwację"><i class="fa fa-check text--green"></i></a>' : '')
                                                                . ( ($isSpecial) ? ' <a href="'.Url::to(['/company/resources/discard', 'id' => $term->id]).'" class="event-close deleteConfirmWithComment " data-label="Odrzuć rezerwację" title="Odrzuć rezerwację"><i class="fa fa-ban text--red"></i></a>' : '')
                                                            .'</li>';
                                                    }
                                                }
                                                if($reservations == 0)
                                                    echo '<li>Brak rezerwacji oczekujących</li>';
                                                ?>
                                            <!--<li>Lunch with jhon @ 3:30 <a href="#" class="event-close"><i class="fa fa-check"></i></a></li>
                                            <li>Coffee term with Lisa @ 4:30 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                                            <li>Skypee conf with patrick @ 5:45 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                                            <li>Gym @ 7:00 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>
                                            <li>Dinner with daniel @ 9:30 <a href="#" class="event-close"><i class="ico-close2"></i></a></li>-->
                                        </ul>
                                    </div>
                                    <!--<input type="text" class="form-control evnt-input" placeholder="NOTES">-->
                                </div>
                                <div class="col-sm-7 col-xs-12 bg-white">
                                    <div class="calendar-block">
                                        <div class="cal"><div id="datetimepicker_resources" data-id="<?= $model->id ?>" data-action="<?= Url::to(['/company/resources/terms', 'id' => $model->id]) ?>"></div></div>
                                        <div class="list-group" id="datetimepicker_tasks-cases"> 
                                            <?php
                                                $iTerm = 0;
                                                foreach($terms as $key => $term) {
                                                    if($term->status == 1) {
                                                        $meEvent = false;
                                                        ++$iTerm;
                                                        if($term->id_employee_fk == $employeeId) {
                                                            $meEvent = true;
                                                        }
                                                     
                                                        echo '<a id="db-event-'.$term->id.'" href="'.Url::to(['/company/resources/bookingupdate', 'id' => $term->id]).'" data-title="<i class=\'fa fa-hand-pointer-o\'></i>Rezerwacja" data-target="#modal-grid-item" class="list-group-item text-ellipsis gridViewModal">'
                                                                . ( ($meEvent) ? '<i class="fa fa-circle text--blue"></i>&nbsp;' : '')
                                                                .'<span class="badge bg-danger">'. ( date('H:i', strtotime($term->date_from)) ) .'</span> '.$term->employee['fullname']
                                                            .'</a>'; 
                                                    }
                                                }
                                                if($iTerm == 0)
                                                    echo '<div class="alert alert-info">Brak zaakceptowanych rezerwacji</div>';
                                            ?>
                                            <!--<a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-danger">7:30</span> Meet a friend </a> 
                                            <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-success">9:30</span> Have a kick off term with .inc company </a>
                                            <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-light">19:30</span> Milestone release </a> 
                                            <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-danger">7:30</span> Meet a friend </a> 
                                            <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-success">9:30</span> Have a kick off term with .inc company </a>
                                            <a href="#" class="list-group-item text-ellipsis"> <span class="badge bg-light">19:30</span> Milestone release </a>-->
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            
						</div>
                    
				    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
