<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-resources"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <div class="grid grid--0">
                <div class="col-xs-10"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>  </div>
                <div class="col-xs-2">
                    <div class="example-content">
                        <div class="example-content-widget">
                            <label class="control-label" for="companyresources-color">Kolor</label>
                            <div id="cp2" class="input-group colorpicker-component">
                                <input type="text" value="<?= $model->color ?>" class="form-control" id="companyresources-color" name="CompanyResources[color]" />
                                <span class="input-group-addon"><i></i></span>
                            </div>
                            <script>
                                $(function () {
                                    $('#cp2').colorpicker();
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="grid">
                <div class="col-sm-4"><?= $form->field($model, 'id_dict_company_resource_fk')->dropDownList(\backend\Modules\Company\models\CompanyResources::listTypes(), ['prompt' => '-- wybierz grupę --'] ) ?></div>
                <div class="col-sm-4"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList($branchId), 'id', 'name'), [] ) ?> </div>
                <div class="col-sm-4"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListByBranch($model->id_company_branch_fk), 'id', 'fullname'), ['prompt' => '- wybierz -'] ) ?> </div>
            </div>   
			<?= $form->field($model, 'only_management')->checkbox()  ?>  
        </div>
    </div>   
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    document.getElementById('companyresources-id_company_branch_fk').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/branch/set']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('companyresources-id_employee_fk').innerHTML = result.employees;    

                /*$('#caltask-departments_list').multiselect('rebuild');
                $('#caltask-departments_list').multiselect('refresh');   */            
            }
        }
        xhr.send();
        return false;
    }
    
    document.getElementById('companyresources-id_dict_company_resource_fk').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/resources/config']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('companyresources-color').value = result.color;               
                $('#cp2').colorpicker('setValue', result.color);
            }
        }
        xhr.send();
        return false;
    }
</script>
