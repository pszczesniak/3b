<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\company\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-company-resources-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-resources']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4"><?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-resources', 'data-form' => '#filter-company-resources-search' ]) ?></div>
        <div class="col-sm-4"><?= $form->field($model, 'id_dict_company_resource_fk')->dropDownList( \backend\Modules\Company\models\CompanyResources::listTypes(), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-resources', 'data-form' => '#filter-company-resources-search' ] ) ?></div>
        <div class="col-sm-4"><?= $form->field($model, 'id_company_branch_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-resources', 'data-form' => '#filter-company-resources-search' ] ) ?></div>    
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
