<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\helpers\ArrayHelper;
?>

<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-cog"></span> Opcje </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>

    <div class="panel-body in" id="calendarFilter">
  
        <fieldset><legend class="text--purple2"><i class="fa fa-filter text--purple2"></i>Filtrowanie</legend>
            <form id="calendar-filtering">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'post',
                'id' => 'form-data-search'
            ]); ?>
                <div class="grid">
                <div class="col-sm-6">
                    <label class="control-label" for="companyresourcesschedule-type_fk">Rodzaj</label>
                    <div class="form-select">
                        <select id="companyresourcesschedule-type_fk" class="form-control schedule-filtering-options" name="CompanyResourcesSchedule[type_fk]">
                            <option value="0"> - wszystko -</option>
                            <?php
                                foreach($dicts as $key => $dict) {
                                    echo '<option value="'.$dict->id.'">'.$dict->name.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <?= $form->field($model, 'id_resource_fk')->dropDownList( \backend\Modules\Company\models\CompanyResources::getGroups($branchId), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] ) ?> 
                </div>
                <div class="col-sm-6">
                    <label class="control-label" for="companyresourcesschedule-status">Status</label>
                    <div class="form-select">
                        <select id="companyresourcesschedule-status" class="form-control schedule-filtering-options" name="CompanyResourcesSchedule[status]">
                            <option value=""> - wszystko -</option>
                            <!--<option value="-2">Anulowana</option>-->
							<option value="0">Oczekująca</option>
							<!--<option value="-1">Odrzucona</option>-->
                            <option value="1">Zaakceptowana</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'id_company_branch_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '', 'class' => 'form-control schedule-filtering-options' ] ) ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </fieldset>
        
        <fieldset><legend class="text--grey2"><i class="fa fa-map text--grey2"></i>Legenda</legend>
            <table class="calendar-legend">
                <tr><td class="calendar-legend-icon" style="background-color: #FFBCBC"><i class="fa fa-exclamation-triangle text--red"></i></td><td>oczekujące na akceptację</td></tr>
                <?php
                    foreach($dicts as $key => $dict) {
                        echo '<tr><td class="calendar-legend-icon" style="background-color: '.$dict->custom['color'].'"><i class="'.$dict->custom['icon'].'"></i></td><td>'.$dict->name.'</td></tr>';
                    }
                ?>
            </table>
        </fieldset>

    </div>
</div>

<script type="text/javascript">
    document.getElementById('companyresourcesschedule-id_company_branch_fk').onchange = function(event) { 
        $value = (this.value) ? this.value : 0;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/branch/set']) ?>/"+$value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('companyresourcesschedule-id_resource_fk').innerHTML = result.resources;    
           
            }
        }
        xhr.send();
        return false;
    }
    
</script>
