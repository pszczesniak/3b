<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rezerwacje');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zasoby'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-resources-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_rsearch', ['model' => $model, 'branchId' => $branchId, 'isAdmin' => $isAdmin]); ?>
 
    <div id="toolbar-booking" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/company/resources/bookingajax', 'id' => 0]) , 
                    ['class' => 'btn btn-success btn-icon gridViewModal', 
                     'id' => 'resource-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-data",
                     'data-title' => "Dodaj"
                    ]) ?>
        <?=  Html::a('<i class="fa fa-calendar"></i>Rezerwacje', Url::to(['/company/resources/schedule']) , 
                    ['class' => 'btn btn-info btn-icon', 
                     'id' => 'resources-schedule',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-title' => "Przejdź do terminarza rezerwacji"
                    ]) ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-booking"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-booking"
                data-toolbar="#toolbar-booking" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="id"
                data-sort-order="desc"
                data-method="get"
                data-search-form="#filter-booking-search"
                data-url=<?= Url::to(['/company/resources/rdata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="resource"  data-sortable="true">Zasób</th>
                    <th data-field="employee"  data-sortable="true">Pracownik</th>
					<th data-field="date_from"  data-sortable="true">Od</th>
                    <th data-field="date_to"  data-sortable="true">Do</th>
                    <th data-field="event_type"  data-sortable="false" data-align="center"></th>
                    <th data-field="state"  data-sortable="true" data-align="center">Status</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Rezerwacje odrzucone</td></tr>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Rezerwacje odwołane</td></tr>
                </tbody>
            </table>
        </fieldset>
        
    </div>


<?php $this->endContent(); ?>
