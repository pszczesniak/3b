<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

        <div class="row">
            <div class="col-sm-4">
                <?php
                    $avatar = (is_file("uploads/employees/cover/preview.jpg"))?"/uploads/employees/cover/preview.jpg":"/images/default-user.png";
                    echo '<img alt="avatar" src="'.$avatar.'" title="avatar"></img>';
                ?>
            </div>
            <div class="col-sm-8">
                <?php 
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'firstname',
                            'lastname',
                            'phone',
                            'email',
                            'address',
                            [                      
                                'attribute' => 'id_company_branch_fk',
                                'format'=>'raw',
                                'value' => $model->branch['name'],
                            ],
                            [                      
                                'attribute' => 'id_dict_employee_type_fk',
                                'format'=>'raw',
                                'value' => $model->type['name'],
                            ],    
                            [                      
                                'attribute' => 'departments_list:html',
                                'format'=>'raw',
                                'label' => 'Działy',
                                'value' => $model->departments_list,
                            ], 
[                      
                                'attribute' => 'files_list:html',
                                'format'=>'raw',
                                'label' => 'Dokumenty',
                                'value' => $model->files_list,
                            ],         
                        ],
                    ]) 
                ?>
            </div>
        </div>
        
            
    <div class="modal-footer"> 
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>