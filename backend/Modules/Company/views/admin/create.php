<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = Yii::t('app', 'Create Company Employee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-employee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
