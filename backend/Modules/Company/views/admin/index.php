<?php

use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\Structurewidget;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Uprawnienia');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-employee-admin', 'title'=>Html::encode($this->title))) ?>

    
    <div id="validation-form" class="alert none"></div>
 
    <div class="tab-block mb25">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Pracownicy</a></li>
            <li><a href="#tab2" data-toggle="tab"> Grupy</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab1" class="tab-pane active">
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="panel-grants">
                   
                </div>
            </div>
            <div id="tab2" class="tab-pane">
                <?php  echo $this->render('_searchGroup', ['model' => $searchModelGroup]); ?>
                <div class="panel-group-grants">
                   
                </div>
            </div>        
        </div>
    

	
</div>
<?php $this->endContent(); ?>
