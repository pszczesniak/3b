<?php
	use yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
	use yii\helpers\Json;
	use yii\helpers\Url;
?>
<!--<form id="permision-assign" method="post" action="/panel/pl/admin/role/update/manager">-->
<?php
	function generateItem($itemName, $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) {
		
		$item = 'name="grant[]" type="checkbox"';
		$item .= ' value="'.$itemName.'"';
		if(array_key_exists($itemName, $roleGrants))
			$item .=  ' checked="checked" ';
		/*if(array_key_exists($itemName, $roleGrants))
			$item .=  ' checked="checked" disabled="disabled" class="check-role-grant"';
		if(!array_key_exists($itemName, $roleLoggedUserGrants)  && $role != 'padre' && $role != 'labor')
			$item .=  ' disabled="disabled"';*/
		return $item;
	}
	
?>
<?php $formGrants = ActiveForm::begin(['id' => 'admin-grants', 'method' => 'post', 'action'=>Url::to(['/company/admin/grantsrole']).'?role='.$role]); ?>
	<div class="grid" id="grants">
		<?php/* var_dump($roleGrants);*/ ?>
		<div class="grid">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Pracownicy</h2>
                        <div class="item"> 
                            <input <?= generateItem('employeePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('employeeAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('employeeEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('employeeDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('employeeExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Klienci</h2>
                        <div class="item"> 
                            <input <?= generateItem('customerPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('customerAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('customerEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('customerDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('customerExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Sprawy i projekty</h2>
                        <div class="item"> 
                            <input <?= generateItem('casePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('caseAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('caseEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('caseDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('caseExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Zadania</h2>
                        <div class="item"> 
                            <input <?= generateItem('eventPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('eventAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('eventEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('eventDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('eventExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Książka korespondencyjna</h2>
                        <div class="item"> 
                            <input <?= generateItem('correspondencePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('correspondenceAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('correspondenceEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('correspondenceDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('correspondenceExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                            <input <?= generateItem('correspondenceSend', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Wysyłka</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Książka nadawcza</h2>
                        <div class="item"> 
                            <input <?= generateItem('broadcastPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Przeglądanie</p> 
                            <input <?= generateItem('broadcastManage', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Zarządzanie [dodawanie/edycja/usuwanie]</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Windykacja</h2>
                        <div class="item"> 
							<input <?= generateItem('debtSpecial', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Pełny dostęp</p> 
                            <input <?= generateItem('debtEstimateOf', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Preliminarz</p> 
							<input <?= generateItem('debtHistory', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Historia</p> 
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Pozostałe</h2>
                        <div class="item"> 
                            <input <?= generateItem('permissionPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel uprawnień</p> 
                            <input <?= generateItem('dictionaryPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Panel słowników</p>
                            <input <?= generateItem('replacementPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Zastępstwa</p>
                            <input <?= generateItem('billingPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Rozliczanie projektów</p>
                        </div>
						<div class="item">
							<input <?= generateItem('caseMerge', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Scalanie spraw</p>  
							<input <?= generateItem('caseUnmerge', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Odwrócenie scalania</p>
						</div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Działy</h2>
                        <div class="item"> 
                            <input <?= generateItem('depertmentPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('depertmentAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('depertmentEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('depertmentDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Dokumenty</h2>
                        <div class="item"> 
                            <input <?= generateItem('documentPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('documentAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('documentEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('documentDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Wzorce dokumentów</h2>
                        <div class="item"> 
                            <input <?= generateItem('documentPatternPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('documentPatternAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('documentPatternEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('documentPatternDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
	<div class="form-group align-right">
		<?php
		echo Html::submitButton( Yii::t('app', 'Save'), [ 'class' =>  'btn btn-primary'])
		?>
	</div>
<?php ActiveForm::end(); ?>