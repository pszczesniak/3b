<?php
	use yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
	use yii\helpers\Json;
	use yii\helpers\Url;
?>
<!--<form id="permision-assign" method="post" action="/panel/pl/admin/role/update/manager">-->
<?php
	function generateItem($itemName, $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) {
		
		$item = 'name="grant[]" type="checkbox"';
		$item .= ' value="'.$itemName.'"';
		if(array_key_exists($itemName, $userGrants))
			$item .=  ' checked="checked" ';
		if(array_key_exists($itemName, $roleGrants))
			$item .=  ' checked="checked" disabled="disabled" class="check-role-grant"';
		/*if(!array_key_exists($itemName, $roleLoggedUserGrants) )
			$item .=  ' disabled="disabled"';*/
		return $item;
	}
	
?>
<?php $formGrants = ActiveForm::begin(['id' => 'admin-grants', 'method' => 'post', 'action'=>Url::to(['/company/admin/grantsuser', 'user' => $user->id])]); ?>
	<div class="grid"><div class="col-xs-12"><div class="alert alert-info">Użytkownik przyspiany do grupy: <b><?= ($role) ? $role : 'BRAK' ?></b></div></div></div>
    <div class="grid" id="grants">
		<?php/* var_dump($roleGrants);*/ ?>
		<div class="grid">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Klienci</h2>
                        <div class="item"> 
                            <input <?= generateItem('customerPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('customerAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('customerEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('customerDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('customerExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Projekty</h2>
                        <div class="item"> 
                            <input <?= generateItem('casePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('caseAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('caseEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('caseDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('caseExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Oferty</h2>
                        <div class="item"> 
                            <input <?= generateItem('eventPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('eventAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('eventEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('eventDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('eventExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Pracownicy</h2>
                        <div class="item"> 
                            <input <?= generateItem('employeePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('employeeAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('employeeEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('employeeDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            <input <?= generateItem('employeeExport', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Eksport</p>  
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Działy</h2>
                        <div class="item"> 
                            <input <?= generateItem('depertmentPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                            <input <?= generateItem('depertmentAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                            <input <?= generateItem('depertmentEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                            <input <?= generateItem('depertmentDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                        </div>
                    </div>
                </div>
                <div class="access-panel">
                    <div class="inbox">
                        <h2>Pozostałe</h2>
                        <div class="item"> 
                            <input <?= generateItem('permissionPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel uprawnień</p> 
                            <input <?= generateItem('dictionaryPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Panel słowników</p>
                            <input <?= generateItem('replacementPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Zastępstwa</p>
                            <input <?= generateItem('billingPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Rozliczanie projektów</p>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
	<div class="form-group align-right">
		<?php
		echo Html::submitButton( Yii::t('app', 'Save'), [ 'class' =>  'btn btn-primary'])
		?>
	</div>
<?php ActiveForm::end(); ?>