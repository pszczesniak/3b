<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use backend\widgets\FilesWidget;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="accordion-toggle"> Podstawowe</a> </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-sm-6"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-sm-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Dodatkowe</a> </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -'] ) ?> 
                    
                    <?= $form->field($model, 'id_dict_employee_type_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listTypes(), ['prompt' => ' - wybierz -'] ) ?> 

                    <?= $form->field($model, 'id_dict_employee_kind_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listKinds(), ['prompt' => ' - wybierz -'] ) ?> 
                    
                    <?= $form->field($model, 'user_type')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listRoles(), ['prompt' => ' - wybierz -'] ) ?> 
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">  Dokumenty</a> </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php if(!$model->isNewRecord) { ?>
						<?= FilesWidget::widget(['typeId' => 1, 'parentId' => $model->id, 'view' => false]) ?>
						<?= $model->files_list ?>
					<?php } else {
							echo '<div class="alert alert-warning">Aby dodać pliki musisz utworzyć pracownika.</div>';
						} 
					?>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">  Zdjęcie</a> </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                [
                                    'id' => 'imgAttachment',
                                    'model' => $model,
                                    'behaviorName' => 'coverBehavior',
                                    'apiRoute' => '/company/employee/imgAttachApi',
                                ]
                            
                            )
                    ?>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">  Działy</a> </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <?= $this->render('_departmentsList', ['departments' => $departments]) ?>
                </div>
            </div>
        </div>
        
    </div> 

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>