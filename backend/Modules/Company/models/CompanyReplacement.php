<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%company_replacement}}".
 *
 * @property integer $id
 * @property integer $id_company_fk
 * @property integer $id_employee_fk
 * @property integer $substitute_for
 * @property integer $id_substitute_branch_fk
 * @property string $date_from
 * @property string $date_to
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyReplacement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_replacement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_fk', 'id_employee_fk', 'substitute_for', 'id_substitute_branch_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_employee_fk', 'substitute_for', 'date_from', 'date_to'], 'required'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['note'], 'string'],
            ['date_from','validateDates'],
            ['substitute_for','validateSubstitute'],
            ['id_employee_fk','validateEmployee'],
        ];
    }
    
    public function validateDates(){
		if($this->date_to && strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to', 'Wprowadź poprawną datę końca.');
		}
	}
    
    public function validateSubstitute(){
		if($this->substitute_for == $this->id_employee_fk){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('substitute_for', 'Pracownik zastepujący musi być różny od zastępowanego.');
		}
	}
    
    public function validateEmployee(){
        $startDate = $this->date_from; $endDate = $this->date_to;
		$exist = CompanyReplacement::find()->where(['status' => 1, 'id_employee_fk' => $this->id_employee_fk])
							->andWhere(" id != ". ( ($this->id) ? $this->id : 0) )
							->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                         ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
										 ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                         ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                            ->all();
		if( count($exist) > 0 ){
			$this->addError('date_from','Pracownik zastępuje w tym terminie pracownika <b>'.$exist[0]->substitute['fullname'].'</b>');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'substitute_for' => Yii::t('app', 'Zastępstwo za'),
            'id_substitute_branch_fk' => Yii::t('app', 'Id Substitute Branch Fk'),
            'date_from' => Yii::t('app', 'Od kiedy'),
            'date_to' => Yii::t('app', 'Do kiedy'),
            'note' => Yii::t('app', 'Notatka'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();

		return $result;
	}
    
    public function getEmployee() {
        return \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_fk); 
	}
    
    public function getSubstitute() {
        return \backend\Modules\Company\models\CompanyEmployee::findOne($this->substitute_for); 
	}
}
