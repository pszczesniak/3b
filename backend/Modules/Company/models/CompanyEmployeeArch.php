<?php

namespace app\Modules\Company\models;

use Yii;

/**
 * This is the model class for table "{{%company_employee_arch}}".
 *
 * @property integer $id
 * @property integer $id_employee_fk
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class CompanyEmployeeArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_employee_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_employee_fk'], 'required'],
            [['id_employee_fk', 'created_by'], 'integer'],
            [['data_arch', 'user_action'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
