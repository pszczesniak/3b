<?php

namespace backend\Modules\Company\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

use app\Modules\Company\models\CompanyArch;

/**
 * This is the model class for table "{{%company}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $nip
 * @property string $regon
 * @property string $account_number
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Company extends \yii\db\ActiveRecord
{
    public $departments_list;
	public $branches_list;
	public $user_action = 'aktualizacja';
    
    public $box_special_emails;
    public $box_special_employees;
    
    public $calendarColorConfirmed;
    public $calendarColorUnconfirmed;
    public $calendarColorDone;
    
    public $attachs;
    
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['calendarColorConfirmed', 'calendarColorUnconfirmed', 'calendarColorDone'], 'required', 'when' => function($model) { return ($model->user_action == 'config'); }],
            [['custom_data'], 'string'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'box_special_emails', 'box_special_employees', 'calendarColorConfirmed', 'calendarColorUnconfirmed', 'calendarColorDone', 'attachs'], 'safe'],
            [['name', 'address'], 'string', 'max' => 300],
            [['phone', 'nip', 'regon'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
			[['email'], 'email'],
            [['account_number'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nip' => Yii::t('app', 'Nip'),
            'regon' => Yii::t('app', 'Regon'),
            'account_number' => Yii::t('app', 'Account Number'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'box_special_emails' => 'Adresy e-mail, na które będzie trafiać cała poczta',
            'box_special_employees' =>'Pracownicy, którzy zawsze mają być dołączani do poczty',
            'calendarColorConfirmed' => 'Kolor tekstu wizyty potwierdzonej', 
            'calendarColorUnconfirmed' => 'Kolor tekstu wizyty niepotwierdzonej',
            'calendarColorDone' => 'Kolor tekstu wizyty zrealizowanej', 
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			} else { 
				$modelArch = new CompanyArch();
				$modelArch->table_fk = 1;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = Yii::$app->user->id;
		$result = $this->save();

		return $result;
	}
	
	public function behaviors()	{
		return 
			[
				"timestamp" =>  [
					'class' => TimestampBehavior::className(),
					'createdAtAttribute' => 'created_at',
					'updatedAtAttribute' => 'updated_at',
					'value' => new Expression('NOW()'),
				],			
			];
	}
	
	public function getDepartments() {
		$items = $this->hasMany(\backend\Modules\Company\models\CompanyDepartment::className(), ['id_company_fk' => 'id']); 
		return $items;
    }
	
	public function getBranches() {
		$items = $this->hasMany(\backend\Modules\Company\models\CompanyBranch::className(), ['id_company_fk' => 'id']); 
		return $items;
    }
    
public function saveLogo() {
        try {
            foreach($this->attachs as $key => $file) {
                $fileName = 'logo_original.png';
                $fileNamePrev = 'logo_'.time().'.png';
                //echo Yii::getAlias('@webroot').'/frontend/web/uploads/homepage/mask.png'; exit;
                //$file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                    
                if(is_file(Yii::getAlias('@webroot') . "/uploads/company/logo.png")) 
                    rename(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/logo.png', Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/'.$fileNamePrev);
                //echo Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/'.$fileName;exit;
                
                $file->saveAs(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/'.$fileName);
                //Image::thumbnail(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/'.$fileName, 280, 65)->save(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/logo_min.jpg', ['quality' => 80]);
                Image::getImagine()->open(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/'.$fileName)
                    ->thumbnail(new Box(280, 140))->save(Yii::getAlias('@webroot').'/../../frontend/web/uploads/company/logo.png' , ['quality' => 100]);

            }
        } catch (\Exception $e) {
            var_dump($e); exit;
        }
        return true;       
    }
    
    public static function getLogo() {
        $logoUrl = false;
        if(is_file(Yii::getAlias('@webroot') . "/uploads/company/logo.png")) 
            $logoUrl = "/uploads/company/logo.png";
            
        if(!$logoUrl)
            $logoUrl = "/images/nologo.png";
            
        return $logoUrl;
    }
    
    public static function getLogoUrl() {
        //return \Yii::getAlias('@frontend').'/web/'.self::getLogo();
        //return /*\Yii::$app->urlManager->createAbsoluteUrl(['site/index']).*/'/images/logo_dark.jpg';
        //return \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg';
        
        $logoUrl = false;
        if(is_file(\Yii::getAlias('@frontend') . "/web/uploads/company/logo.png")) 
            $logoUrl = \Yii::getAlias('@frontend') . "/web/uploads/company/logo.png";
            
        if(!$logoUrl)
            $logoUrl = \Yii::getAlias('@frontend') . "/web/images/nologo.png";
            
        return $logoUrl;
    }
    
    public static function getLogoAbsolute() {
        $logoUrl = false;
        if(is_file(\Yii::getAlias('@frontend') . "/web/uploads/company/logo.png")) 
            $logoUrl = \Yii::$app->params['frontend'] . "/uploads/company/logo.png";
            
        if(!$logoUrl)
            $logoUrl = \Yii::$app->params['frontend'] . "/images/nologo.png";
            
        return $logoUrl;
        // Yii::$app->params['frontend'] . '/uploads/pages/cover'
    }
}

