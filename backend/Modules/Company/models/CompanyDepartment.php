<?php

namespace backend\Modules\Company\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use app\Modules\Company\models\CompanyArch;

/**
 * This is the model class for table "{{%company_department}}".
 *
 * @property integer $id
 * @property integer $id_company_fk
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $notification_email
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyDepartment extends \yii\db\ActiveRecord
{
    public $id_manager_fk;
    public $employees_list;
	public $user_action = 'aktualizacja';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_fk', 'notification_email', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_employee_manager_fk', 'all_employee'], 'integer'],
            [['name', 'symbol'], 'required'],
            [['custom_data'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'symbol'], 'safe'],
            [['name'], 'string', 'max' => 300],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 100],
            [['symbol'], 'string', 'max' => 10],
            ['email', 'email'],
            ['name', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'notification_email' => Yii::t('app', 'Maile do dyrektora?'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_manager_fk' => 'Dyrektor',
            'id_employee_manager_fk' => 'Dyrektor',
            'employees_list' => 'Pracownicy',
            'all_employee' => 'Dział administracyjny / sekretariat'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				if(!$this->created_by) $this->created_by = Yii::$app->user->id;
			} else { 
				$this->updated_by = Yii::$app->user->id;
                
                $modelArch = new CompanyArch();
				$modelArch->table_fk = 3;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
	
        if ($this->id_employee_manager_fk) {
            $exist = \backend\Modules\Company\models\EmployeeDepartment::find()->where(['status' => 1, 'id_department_fk' => $this->id, 'id_employee_fk' => $this->id_employee_manager_fk])->count();
            if(!$exist) {
                $new = new \backend\Modules\Company\models\EmployeeDepartment();
                $new->id_department_fk = $this->id;
                $new->id_employee_fk = $this->id_employee_manager_fk;
                $new->status = 1;
                $new->save();
            }
        }
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = Yii::$app->user->id;
		$result = $this->save();

		return $result;
	}
	
	public function behaviors()	{
		return 
			[
				"timestamp" =>  [
					'class' => TimestampBehavior::className(),
					'createdAtAttribute' => 'created_at',
					'updatedAtAttribute' => 'updated_at',
					'value' => new Expression('NOW()'),
				],			
			];
	}
    
    public function getCompany()
    {
		return $this->hasOne(\backend\Modules\Company\models\Company::className(), ['id' => 'id_company_fk']);
    }
    
    /*public function getDepartment()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyDepartment::className(), ['id' => 'id_department_fk']);
    }*/
    
    public function getEmployees()
    {
		$items = $this->hasMany(\backend\Modules\Company\models\EmployeeDepartment::className(), ['id_department_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
    
    public static function getList() {
        $departments = [];
        if(!Yii::$app->user->isGuest) {
            if(\Yii::$app->user->identity->username == 'superadmin') {
                $departments = CompanyDepartment::find()->where(['status' => 1])->orderby('name')->all();
            } else {
                $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
                //$employee = CompanyEmployee::findOne($id);
                if($employee->is_admin == 1 || $admin || in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                    $departments = CompanyDepartment::find()->where(['status' => 1])->orderby('name')->all();
                } else {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->department);
                        }
                    }
                }
            }
        }
        return $departments;
    }
    
    public static function getListByEmployee($id) {
        $departments = [];
        //$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => $id])->one();
		if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && $id == 0)
            $data = \backend\Modules\Company\models\CompanyDepartment::find()->where('status = 1')->orderby('name')->all();
        else
            $data = \backend\Modules\Company\models\CompanyDepartment::find()->where('status = 1 and id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$id.')')->orderby('name')->all();

        foreach($data as $key => $model) {
            array_push($departments, $model);
        }

        return $departments;
    }
    
    public function getManager()
    {   
        if($this->id_employee_manager_fk) {
            return \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_manager_fk)->fullname;
        } else {
            //return 'brak';
            $manager = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id_dict_employee_kind_fk = 70')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$this->id.')')->one();
            
            if($manager) {
                return $manager->fullname;
            } else {
                return 'brak';
            }
        }
    }
    
    public static function allEmployees($ids, $type) { 
        $ids = ( empty($ids) ) ? [ 0 => 0 ] : $ids;
		$employees = [];
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();   
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        if($type == 'case' ) {
            if($admin > 0 || $employee->is_admin == 1) {
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where all_employee != 1 and ed.status = 1 and id_department_fk  in ('.implode(',', $ids).') )')->orderby('lastname')->all();
            } else
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where all_employee != 1 and ed.status = 1 and id_department_fk  in ('.implode(',', $ids).') )')->orderby('lastname')->all();
        } else {
            if($admin > 0 || $employee->is_admin == 1)
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where ed.status = 1 and id_department_fk  in ('.implode(',', $ids).') )')->orderby('lastname')->all();
            else
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where ed.status = 1 and id_department_fk  in ('.implode(',', $ids).') )')->orderby('lastname')->all();       
        }
        //foreach($employees as $key => $value) echo $value->fullname.'<br />'; exit;
        return $employees;
    }
    
    public static function getSpecials() {
        $specials = \backend\Modules\Company\models\CompanyDepartment::find()->where(['status' => 1, 'all_employee' => 1])->all();
        
        return $specials;
    }
    
    public static function getSpecialEmployees() {
        $specialEmployees = [];
        
        $company = \backend\Modules\Company\models\Company::findOne(1);
        $customData = \yii\helpers\Json::decode($company->custom_data);
        
        if( isset($customData['box']) ) {
            $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
            $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
        } else {
            $company->box_special_emails = '';
            $company->box_special_employees = [ 0 => 0 ];
        }
        $specialEmployees = $company->box_special_employees;
        
        return $specialEmployees;
    }
    
    public static function getManagers() {
        $managers = [];
        
        $sql = "select distinct id_employee_manager_fk from {{%company_department}} where status = 1";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($data as $key => $value) {
            array_push($managers, $value['id_employee_manager_fk']);
        }
        
        return $managers;
    }
}

