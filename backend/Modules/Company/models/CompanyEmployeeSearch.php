<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Company\models\CompanyEmployee;

/**
 * CompanyEmployeeSearch represents the model behind the search form about `app\Modules\Company\models\CompanyEmployee`.
 */
class CompanyEmployeeSearch extends CompanyEmployee
{
    public $id_department_fk;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_company_fk', 'id_company_branch_fk', 'id_dict_employee_type_fk', 'id_dict_employee_kind_fk', 'is_active', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['user_type', 'firstname', 'lastname', 'phone', 'email', 'custom_data', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyEmployee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['lastname' => 'asc']]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company_fk' => $this->id_company_fk,
            'id_company_branch_fk' => $this->id_company_branch_fk,
            'id_dict_employee_type_fk' => $this->id_dict_employee_type_fk,
            'id_dict_employee_kind_fk' => $this->id_dict_employee_kind_fk,
            'is_active' => $this->is_active,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data]);

        return $dataProvider;
    }
}
