<?php

namespace backend\Modules\Company\models;

use Yii;

/**
 * This is the model class for table "{{%company_resources_schedule_arch}}".
 *
 * @property integer $id
 * @property integer $id_parent_fk
 * @property string $user_action
 * @property string $data_arch
 * @property string $data_change
 * @property string $created_at
 * @property integer $created_by
 */
class CompanyResourcesScheduleArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_resources_schedule_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_fk'], 'required'],
            [['id_parent_fk', 'created_by'], 'integer'],
            [['data_arch', 'data_change'], 'string'],
            [['created_at'], 'safe'],
            [['user_action'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'user_action' => Yii::t('app', 'User Action'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'data_change' => Yii::t('app', 'Data Change'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
