<?php

namespace frontend\Modules\Company\models;

use Yii;
use yii\base\Model;

use backend\Modules\Folder\models\Folder;
use backend\Modules\Folder\models\FolderUser;

/**
 * ActionForm is the model behind the contact form.
 */
class ActionsGroup extends Model
{
    public $id_user_fk; 
    public $id_folder_fk;
    public $is_admin;
    public $is_upload;
    public $is_download;
    public $is_delete;
    public $objects = [];
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objects'], 'required'],
            [['is_admin', 'is_upload', 'is_download', 'is_delete', 'id_folder_fk'], 'integer'],
            [['objects'], 'safe'],
            ['id_user_fk', 'required', 'when' => function($model) { return ( $model->user_action == 'add_member' ); }, 'message' => 'Musisz wskazać użytkownika, któremu chcesz przypisać foldery' ],
            ['id_user_fk', 'required', 'when' => function($model) { return ( $model->user_action == 'del_member' ); }, 'message' => 'Musisz wskazać użytkownika, któremu chcesz odebrać uprawnienia do folderu' ],
            ['id_folder_fk', 'required', 'when' => function($model) { return ( $model->user_action == 'move' ); }, 'message' => 'Musisz wskazać folder, do którego chcesz przenieść obiekty' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_folder_fk' => Yii::t('app', 'Folder'),
            'id_user_fk' => Yii::t('app', 'Użytkownik'),
            'objects' => 'Foldery'
        ];
    }

    public function addMember() {            
        $folders = []; $files = [];
        
        foreach($this->objects as $key => $object) {
            $objectArr = explode('-', $object);
            if($objectArr[0] == 'fo') array_push($folders, $objectArr[1]);
            if($objectArr[0] == 'fi') array_push($files, $objectArr[1]);
        }
        
        if(count($folders) > 0) {
            $sqlInsert = "insert into {{%folder_users}} (id_folder_fk,id_user_fk,id_group_fk,is_admin,is_upload,is_download,is_delete,status,created_at,created_by) "
                               ." select f.id as id_folder_fk, ".$this->id_user_fk." as id_user_fk, 0 as id_group_fk, ".$this->is_admin." as is_admin, ".$this->is_upload." as is_upload, ".$this->is_download." as is_download, ".$this->is_delete." as is_delete, "
                                        ." 1 as status, NOW() as created_at, ".Yii::$app->user->id." as created_by "
                                ." from {{%folder}} f left join {{%folder_users}} fu on f.id = fu.id_folder_fk and fu.status = 1 and id_user_fk = ".$this->id_user_fk." where f.id in (".implode(',', $folders).") and fu.id is null";
            \Yii::$app->db->createCommand($sqlInsert)->execute();
            
            $childrens = [];
            $parent = implode(',', $folders);
            while ($parent) {
                $sqlChildren = "select group_concat(id) childrens from {{%folder}} where id_parent_fk in (".$parent.")";
                $parent = Yii::$app->db->createCommand($sqlChildren)->queryOne()['childrens'];
                if($parent) array_push($childrens, $parent);
            }	
            
            if(count($childrens) > 0) {
                $sqlInsert = "insert into {{%folder_users}} (id_folder_fk,id_user_fk,id_group_fk,is_admin,is_upload,is_download,is_delete,status,created_at,created_by) "
                               ." select f.id as id_folder_fk, ".$this->id_user_fk." as id_user_fk, 0 as id_group_fk, ".$this->is_admin." as is_admin, ".$this->is_upload." as is_upload, ".$this->is_download." as is_download, ".$this->is_delete." as is_delete, "
                                        ." 1 as status, NOW() as created_at, ".Yii::$app->user->id." as created_by "
                                ." from {{%folder}} f left join {{%folder_users}} fu on f.id = fu.id_folder_fk and fu.status = 1 and id_user_fk = ".$this->id_user_fk." where f.id in (".implode(',', $childrens).") and fu.id is null";
                \Yii::$app->db->createCommand($sqlInsert)->execute();
            }
        }
        
        return true;
    }
    
    public function delMember() {            
        $folders = []; $files = [];
        
        foreach($this->objects as $key => $object) {
            $objectArr = explode('-', $object);
            if($objectArr[0] == 'fo') array_push($folders, $objectArr[1]);
            if($objectArr[0] == 'fi') array_push($files, $objectArr[1]);
        }
        
        if(count($folders) > 0) {
            $sqlUpdate = "update {{%folder_users}} set status = -1, deleted_at = NOW(), deleted_by = ".Yii::$app->user->id
                        ." where id_folder_fk in (".implode(',', $folders).") and id_user_fk = ".$this->id_user_fk;
            Yii::$app->db->createCommand($sqlUpdate)->execute();
            $childrens = [];
            $parent = implode(',', $folders);
            while ($parent) {
                $sqlChildren = "select group_concat(id) childrens from {{%folder}} where id_parent_fk in (".$parent.")";
                $parent = Yii::$app->db->createCommand($sqlChildren)->queryOne()['childrens'];
                if($parent) array_push($childrens, $parent);
            }	
            
            if(count($childrens) > 0) {
                $sqlUpdate = "update {{%folder_users}} set status = -3, deleted_at = NOW(), deleted_by = ".Yii::$app->user->id
                        ." where id_folder_fk in (".implode(',', $childrens).") and id_user_fk = ".$this->id_user_fk;
                Yii::$app->db->createCommand($sqlUpdate)->execute();
            }
            
        }
        
        return true;
    }
    
    public function moveObjects() {
        $folders = []; $files = [];
        
        foreach($this->objects as $key => $object) {
            $objectArr = explode('-', $object);
            if($objectArr[0] == 'fo') array_push($folders, $objectArr[1]);
            if($objectArr[0] == 'fi') array_push($files, $objectArr[1]);
        }
        
        if(count($folders) > 0) {
            $sqlUpdate = "update {{%folder}} set id_parent_fk = ".$this->id_folder_fk.", updated_at = NOW(), updated_by = ".Yii::$app->user->id
                        ." where id in (".implode(',', $folders).")";
            Yii::$app->db->createCommand($sqlUpdate)->execute();	
        }
        
        if(count($files) > 0) {
            $sqlUpdate = "update {{%file}} set id_folder_fk = ".$this->id_folder_fk.", updated_at = NOW(), updated_by = ".Yii::$app->user->id
                        ." where id in (".implode(',', $files).")";
            Yii::$app->db->createCommand($sqlUpdate)->execute();	
        }
        
        return true;
    }
    
    public function deleteObjects() {
        $folders = []; $files = [];
        
        foreach($this->objects as $key => $object) {
            $objectArr = explode('-', $object);
            if($objectArr[0] == 'fo') array_push($folders, $objectArr[1]);
            if($objectArr[0] == 'fi') array_push($files, $objectArr[1]);
        }
        
        if(count($folders) > 0) {
            $sqlUpdate = "update {{%folder}} set status = -1, deleted_at = NOW(), deleted_by = ".Yii::$app->user->id
                        ." where id in (".implode(',', $folders).")";
            Yii::$app->db->createCommand($sqlUpdate)->execute();	
        }
        
        if(count($files) > 0) {
            $sqlUpdate = "update {{%file}} set status = -1, deleted_at = NOW(), deleted_by = ".Yii::$app->user->id
                        ." where id in (".implode(',', $files).")";
            Yii::$app->db->createCommand($sqlUpdate)->execute();	
        }
        
        return true;
    }
}
