<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use backend\Modules\Company\models\CompanyResourcesScheduleArch; 
use backend\Modules\Company\models\CompanyEmployee;
use common\models\User;

/**
 * This is the model class for table "{{%company_resources_schedule}}".
 *
 * @property integer $id
 * @property integer $id_resource_fk
 * @property integer $id_employee_fk
 * @property integer $id_event_fk
 * @property integer $type_event
 * @property string $date_from
 * @property string $date_to
 * @property string $describe
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyResourcesSchedule extends \yii\db\ActiveRecord
{
    public $term;
    public $user_action;
    public $accept = 0;
    
    public $id_company_branch_fk;
    
    const SCENARIO_DISCARD = 'discard';
    const SCENARIO_CHANGE = 'change';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_resources_schedule}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_resource_fk', /*'id_employee_fk', */ 'date_from', 'date_to'], 'required'],
			//[['describe'], 'required', 'when' => function($model) { return ($model->type_event == 1); }],
            [['id_resource_fk', 'id_employee_fk', 'id_event_fk', 'type_event', 'status', 'created_by', 'updated_by', 'deleted_by', 'accept', 'id_company_branch_fk', 'id_parent_fk', 'is_behalf', 'is_meeting'], 'integer'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at', 'accept', 'is_behalf', 'is_meeting', 'applicant_name'], 'safe'],
            [['describe', 'custom_data', 'comment', 'change_reason', 'applicant_name'], 'string'],
            ['date_from','validateDates', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel'); }/*,  'on' => self::SCENARIO_DISCARD*/],
            ['date_from','validateReal', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel' && $model->user_action != 'accept'); }],
			[['date_from'], 'validateReservation', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel'); }],
            ['comment', 'required', 'when' => function($model) { return ($model->user_action == 'discard' || $model->user_action == 'cancel'); }],
            ['change_reason', 'required', 'when' => function($model) { return ($model->user_action == 'change'); }],
            ['id_employee_fk','validateTypereservation', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel' ); }],
			['id_resource_fk', 'validateRequired', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel' ); }],
			['id_resource_fk', 'validateManagement', 'when' => function($model) { return ($model->user_action != 'discard' && $model->user_action != 'cancel' ); }],
        ];
    }
    
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			$this->addError('date_to','Wprowadź poprawną datę końca');
		}
	}
    
    public function validateReal(){
        $eventDate = $this->date_from;
		/*if( strtotime($this->date_from) < ( strtotime(date('Y-m-d H:i:s')) - 60*3 ) ){
			$this->addError('date_from','Zasób należy zarezerwować przynajmniej na 3 godziny wcześniej');
		} */
        if( strtotime($this->date_from) < ( strtotime(date('Y-m-d H:i:s')) + 60*15 ) ){
			$this->addError('date_from','Zasób należy zarezerwować przynajmniej na 15 minut wcześniej');
		} 
	}
    
    public function validateTypereservation(){ 
        $startDate = $this->date_from.':00';
		$endDate = ($this->date_to) ? $this->date_to.':00' : $startDate;
        $resource = false;
        if($this->id_resource_fk) $resource = \backend\Modules\Company\models\CompanyResources::findOne($this->id_resource_fk);
        if($resource && $this->id_employee_fk) {
            $query = (new \yii\db\Query())
                ->select(['cr.name as name', 'date_from', 'date_to'])
                ->from('{{%company_resources_schedule}} as crs')
                ->join(' JOIN', '{{%company_resources}} as cr', 'cr.id = crs.id_resource_fk')
                ->where( ['crs.id_employee_fk' => $this->id_employee_fk] )
              //  ->andWhere( " id_event_fk != ".(($this->id_event_fk)?$this->id_event_fk:0)."")
                ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                             ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                                             ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                             ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                ->andWhere('crs.status >= 0 and cr.id_dict_company_resource_fk = '.$resource->id_dict_company_resource_fk); 
            if($this->id_event_fk)  
                 $query = $query->andWhere('id_event_fk != '.$this->id_event_fk);
            if(!$this->isNewRecord)
                $query = $query->andWhere('crs.id != '.$this->id);
            if($this->id_parent_fk && $this->id_parent_fk > 0)
                $query = $query->andWhere('crs.id != '.$this->id_parent_fk);
            $result = $query->all();
            $count = count($result);
            if($count > 0) {
                $this->addError('id_employee_fk', 'W wybranym czasie pracownik ma już przypisany zasób tego typu ['.$result[0]['name'].' '.$result[0]['date_from'].'-'.$result[0]['date_to'].']');
            }
        } /*else {
            $this->addError('id_employee_fk', $this->id_resource_fk);
        }*/
	}
	
	public function validateReservation(){
        $startDate = $this->date_from.':00';
		$endDate = ($this->date_to) ? $this->date_to.':00' : $startDate;  
		if($this->user_action == 'change')
            $reservations = CompanyResourcesSchedule::find()->where([/*'status' => 1,*/ 'id_resource_fk' => $this->id_resource_fk])
                                ->andWhere(" status >= 0 and id != ". ( ($this->id_parent_fk) ? $this->id_parent_fk : 0) )
                                ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                             ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                                             ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                             ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                                ->all();
        else
            $reservations = CompanyResourcesSchedule::find()->where([/*'status' => 1,*/ 'id_resource_fk' => $this->id_resource_fk])
                                ->andWhere(" status >= 0 and id != ". ( ($this->id) ? $this->id : 0) )
                                ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                             ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                                             ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                             ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                                ->all();
		if( count($reservations) > 0 ){
			$this->addError('date_from','Zasób `<b>'.$this->resource['name'].'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to);
		} 
	}
	
	public function validateRequired(){
        if( $this->type_event == 2 ){
			$dict = \backend\Modules\Dict\models\DictionaryValue::findOne($this->resource['id_dict_company_resource_fk']);

			$customData = \yii\helpers\Json::decode($dict->custom_data);
			$dict->icon = $customData['icon'];
			$dict->color = $customData['color'];
			$dict->required_meetingWith = (isset($customData['required_meetingWith'])) ? $customData['required_meetingWith'] : 0;
	        
			if($dict->required_meetingWith && !$this->applicant_name)
				$this->addError('applicant_name', 'Proszę podać imię i nazwisko lub nazwę firmy');
                
            if(!$dict->required_meetingWith && !$this->describe)
				$this->addError('describe', 'Proszę podać powód rezerwacji');
    
		} else {
            if(!$this->describe)
                $this->addError('describe', 'Proszę podać powód rezerwacji');
        }
	}
	
	public function validateManagement(){
        if( $this->resource['only_management'] ){
			$creatorOk = false; $employeeOk = false;
			
			$sqlCreator = "select id_dict_employee_kind_fk, "
					  ." (select count(*) from law_employee_department ed join law_company_department d on d.id=ed.id_department_fk where ed.id_employee_fk=e.id and all_employee = 1 ) is_special, "
					  ." (select count(*) from law_company_department cd where id_employee_manager_fk = e.id) is_manager "
				 ." from law_company_employee e where id_user_fk = ".\Yii::$app->user->id;
			$creatorData = Yii::$app->db->createCommand($sqlCreator)->queryOne();
	        
			if($creatorData['id_dict_employee_kind_fk'] == 100 || $creatorData['is_special'] || $creatorData['is_manager'])
				$creatorOk = true;
			
			if(!$creatorOk) {
				$this->addError('id_resource_fk', 'Nie masz uprawnień do rezerwacji tego zasobu');
				return true;
			}	
		    $sqlEmployee = "select id_dict_employee_kind_fk, "
					  ." (select count(*) from law_employee_department ed join law_company_department d on d.id=ed.id_department_fk where ed.id_employee_fk=e.id and all_employee = 1 ) is_special, "
					  ." (select count(*) from law_company_department cd where id_employee_manager_fk = e.id) is_manager "
				 ." from law_company_employee e where id = ".$this->id_employee_fk;
			$employeeData = Yii::$app->db->createCommand($sqlEmployee)->queryOne();
	        
			if($employeeData['id_dict_employee_kind_fk'] == 100 || $employeeData['is_manager'])
				$employeeOk = true;
			
			if(!$employeeOk)
				$this->addError('id_employee_fk', 'Zasób dostępny jedynie dla zarządu i dyrektorów');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_resource_fk' => Yii::t('app', 'Zasób'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_event_fk' => Yii::t('app', 'Zdarzenie'),
            'type_event' => Yii::t('app', 'Typ spotkania'),
            'date_from' => Yii::t('app', 'Początek'),
            'date_to' => Yii::t('app', 'Koniec'),
            'describe' => Yii::t('app', 'Opis'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'accept' => 'Od razu zatwierdź rezerwację',
            'id_company_branch_fk' => 'Oddział',
            'comment' => 'Powód odrzucenia',
            'change_reason' => 'Powód zmiany',
			'is_behalf' => 'W imieniu',
			'applicant_name' => 'Spotkanie z'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
                
                $modelArch = new CompanyResourcesScheduleArch(); 
				$modelArch->id_parent_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this->oldAttributes);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            if($this->status == 1) {
                $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_fk);
                try { 
					if(Yii::$app->params['env'] == 'prod') {
						$compose = \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
							->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
							//->setTo(\Yii::$app->params['testEmail'])
							->setTo($employee->email);
                        if($this->resource['id_employee_fk'] && $this->resource['id_dict_company_resource_fk'] == 17) {
                            $compose->setCc($this->resource['manager']['email']);
                        }
						$compose->setBcc('kamila_bajdowska@onet.eu')
							->setSubject('Potwierdzenie rezerwacji zasobu w systemie ' . \Yii::$app->name )
							->send();
					} else {
						$compose = \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
							->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
							//->setTo(\Yii::$app->params['testEmail'])
							->setTo($employee->email.'dev');
                        if($this->resource['id_employee_fk'] && $this->resource['id_dict_company_resource_fk'] == 17) {
                            $compose->setCc($this->resource['manager']['email'].'dev');
                        }
						$compose->setBcc('kamila_bajdowska@onet.eu')
							->setSubject('Potwierdzenie rezerwacji zasobu w systemie ' . \Yii::$app->name )
							->send();
                        /*\Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
							->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
							//->setTo(\Yii::$app->params['testEmail'])
							->setTo(\Yii::$app->params['testEmail'])
							->setSubject('DEV: Potwierdzenie rezerwacji zasobu w systemie ' . \Yii::$app->name . ' - '. $employee->email)
							->send();*/
					}
			    } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'resource-booking';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
            } else {
                $branchAdministration = [];
                $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                                ->where(['status' => 1, 'id_company_branch_fk' => $this->resource['id_company_branch_fk'] ])
                                ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                                ->all();
                foreach($branchAdministrationSql as $ii => $item) {
                    array_push($branchAdministration, $item['email']);
                }
				/*replacement begin*/
				$replacements = \backend\Modules\Company\models\CompanyReplacement::find()->where(['status' => 1, 'id_substitute_branch_fk' => $this->resource['id_company_branch_fk'] ])
																						 ->andWhere("'".date('Y-m-d')."' between date_from and date_to")->all();
				foreach($replacements as $iii => $item) { 
					array_push($branchAdministration, $item->employee['email']);
				}
				/*replacement end*/
                try {
                    if(Yii::$app->params['env'] == 'prod') {
                        \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
                            ->setTo($branchAdministration)
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Nowa rezerwacja w systemie ' . \Yii::$app->name )
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
                            ->setTo(\Yii::$app->params['testEmail'])
                            ->setSubject('DEV: Nowa rezerwacja w systemie ' . \Yii::$app->name . ' - '.implode(',',$branchAdministration))
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'resource-booking';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
            }
        } else {
            if($this->user_action == 'update') {
				$branchAdministration = [];
                $branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
                                ->where(['status' => 1, 'id_company_branch_fk' => $this->resource['id_company_branch_fk'] ])
                                ->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
                                ->all();
                foreach($branchAdministrationSql as $ii => $item) {
                    array_push($branchAdministration, $item['email']);
                }
				/*replacement begin*/
				$replacements = \backend\Modules\Company\models\CompanyReplacement::find()->where(['status' => 1, 'id_substitute_branch_fk' => $this->resource['id_company_branch_fk'] ])
																						 ->andWhere("'".date('Y-m-d')."' between date_from and date_to")->all();
				foreach($replacements as $iii => $item) { 
					array_push($branchAdministration, $item->employee['email']);
				}
				/*replacement end*/
                try {
                    if(Yii::$app->params['env'] == 'prod') {
                        \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
                            ->setTo($branchAdministration)
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Rezerwacja z '.$this->created_at.' została zmieniona w systemie ' . \Yii::$app->name )
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo(\Yii::$app->params['testEmail'])
                            ->setTo(\Yii::$app->params['testEmail'])
                            ->setSubject('DEV: Rezerwacja z '.$this->created_at.' została w systemie ' . \Yii::$app->name . ' - '.implode(',',$branchAdministration))
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'resource-booking-update';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
			}
			
			$action = '';
            if($this->user_action == 'cancel') $action = 'anulowana';
            if($this->user_action == 'discard') $action = 'odrzucona';
            if($this->user_action == 'accept') $action = 'zaakceptowana';
			
			$branchAdministration = [];
			$branchAdministrationSql = \backend\Modules\Company\models\CompanyEmployee::find()
							->where(['status' => 1, 'id_company_branch_fk' => $this->resource['id_company_branch_fk'] ])
							->andWhere(' id in (select id_employee_fk from  {{%employee_department}} where id_department_fk in (select id from {{%company_department}} where all_employee = 1 ))')
							->all();
			foreach($branchAdministrationSql as $ii => $item) {
				array_push($branchAdministration, $item['email']);
			}
			
            if($action && $action != '') {
                try {
                    if(Yii::$app->params['env'] == 'prod') {
                     
						$compose = \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                        //->setTo(\Yii::$app->params['testEmail'])
                                        ->setTo($this->employee['email'])
                                        ->setBcc('kamila_bajdowska@onet.eu')
                                        ->setSubject('Rezerwacja została '.$action.' w systemie ' . \Yii::$app->name );
                        $ccEmails = [];
						if($this->resource['id_employee_fk'] && $this->resource['id_employee_fk'] != $this->id_employee_fk) {
                            array_push($ccEmails, $this->resource['manager']['email']);
                        } 
					    /*if($this->id_employee_fk != $this->resource['id_employee_fk']) {
                            $createdUser = \common\models\User::findOne($this->created_by);
							array_push($ccEmails, $createdUser['email']);
                        }*/
					    
						foreach($branchAdministration as $key => $specialEmail) {
							if(!in_array($specialEmail, $ccEmails)) {
								array_push($ccEmails, $specialEmail);
							}
						}
						
						if($ccEmails) {
							$compose->setCc($ccEmails);
						}
						$compose->send();
                    } else {
                        $compose = \Yii::$app->mailer->compose(['html' => 'resourceInfo-html', 'text' => 'resourceInfo-text'], ['model' => $this])
                                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                        ->setTo('kamila_bajdowska@onet.eu');
                        $ccEmails = [];
						if($this->resource['id_employee_fk'] && $this->resource['id_employee_fk'] != $this->id_employee_fk) {
                            array_push($ccEmails, $this->resource['manager']['email']);
                        }  
					    /*if($this->id_employee_fk != $this->resource['id_employee_fk']) {
                            $createdUser = \common\models\User::findOne($this->created_by);
							array_push($ccEmails, $createdUser['email']);
                        }*/
					    
						foreach($branchAdministration as $key => $specialEmail) {
							if(!in_array($specialEmail, $ccEmails)) {
								array_push($ccEmails, $specialEmail);
							}
						}
						
						if($ccEmails) {
							$compose->setCc($ccEmails);
						}
						$compose->setSubject('DEV: Rezerwacja została '.$action.' w systemie ' . \Yii::$app->name .'to: '.$this->employee['email'].' => cc: '.implode(',',$ccEmails));
									
                        $compose->send();
                    }
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'resource-booking';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
            }
        }

		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
	
	public function getCreator()
    {
		return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
    
    public function getTask()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalTask::className(), ['id' => 'id_event_fk']);
    }
    
    public function getResource()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyResources::className(), ['id' => 'id_resource_fk']);
    }
    
    public static function listStates() {
        $states[-2] = 'Odrzucona';
        $states[-1] = 'Odwołana';
        $states[0] = 'Oczekująca';
        $states[1] = 'Zaakceptowana';
        
        return $states;
    }
    
    public function getAccepturl() {
        return \Yii::$app->urlManager->createAbsoluteUrl(['/company/resources/accepted','id' => $this->id]);
    }
    
    public function getDiscardurl() {
        return \Yii::$app->urlManager->createAbsoluteUrl(['/company/resources/discarded','id' => $this->id]);
    }
    
    public function getHistory() {
        $history = [];
        
        $items = CompanyResourcesScheduleArch::find()->where(['id_parent_fk' => $this->id])->orderby('id desc')->all();
        foreach($items as $key => $item) {
            $archData = \yii\helpers\Json::decode($item->data_arch);
            if($item->user_action == 'accept')  { 
                $comment = ''; 
                $action = 'akceptacja';
            } else if($item->user_action == 'update')  { 
                $comment = 'przed zmianą: <ul>';
                    $comment .= '<li>początek: '.$archData['date_from'].'</li>';
                    $comment .= '<li>koniec: '.$archData['date_to'].'</li>';
                    $comment .= '<li>powód rezerwacji: '.$archData['describe'].'</li>';
                    $comment .= '<li>zasób: '.CompanyResources::findOne($archData['id_resource_fk'])->name.'</li>';
                    $comment .= '<li>pracownik: '.CompanyEmployee::findOne($archData['id_employee_fk'])->fullname.'</li>';
                $comment .= '</ul>';
                $action = 'aktualizacja';
            } else {
                $comment = $this->comment;
                $action = ($item->user_action == 'cancel') ? 'odwołanie' : 'odrzucenie';
            }
            if( isset($items[$key+1]) ) {$archData = \yii\helpers\Json::decode($items[$key+1]->data_arch); }
            $tmp = ['action' => $action, 'employee' => User::findOne($item->created_by)->fullname, 'date' => $item->created_at, 'comment' => $comment ];
            array_push($history, $tmp);
        }
        
        $action = 'utworzenie';
        if( count($items) == 0 && $this->status == 1) $action = 'utworzenie / akceptacja';
        $tmp = ['action' => $action, 'employee' => User::findOne($this->created_by)->fullname, 'date' => $this->created_at, 'comment' => $this->describe];
        array_push($history, $tmp);
        
        if($this->id_parent_fk && $this->id_parent_fk > 0) {
            $action = 'zmiana rezerwacji';
            $tmp = ['action' => $action, 'employee' => User::findOne($this->created_by)->fullname, 'date' => $this->created_at, 'comment' => $this->change_reason];
            array_push($history, $tmp);
        }
        
        return $history;
    }    
}
