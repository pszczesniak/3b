<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%employee_department}}".
 *
 * @property integer $id
 * @property integer $id_employee_fk
 * @property integer $id_department_fk
 * @property string $created_at
 * @property integer $created_by
 */
class EmployeeDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employee_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_employee_fk', 'id_department_fk'], 'required'],
            [['id_employee_fk', 'id_department_fk', 'created_by', 'deleted_by', 'status'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public function getDepartment()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyDepartment::className(), ['id' => 'id_department_fk']);
    }
}
