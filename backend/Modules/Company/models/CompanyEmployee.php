<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use app\Modules\Company\models\CompanyEmployeeArch;
/**
 * This is the model class for table "{{%company_employee}}".
 *
 * @property integer $id
 * @property integer $id_company_fk
 * @property integer $id_company_branch_fk
 * @property string $user_type
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $email
 * @property integer $id_dict_employee_type_fk
 * @property integer $id_dict_employee_kind_fk
 * @property string $custom_data
 * @property integer $is_active
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyEmployee extends \yii\db\ActiveRecord
{
    public $user_action;
    public $id_department_fk;
    public $departments_list = [];
    public $files_list;
    public $id_role_fk;
	public $pass;
    public $copyUser;
    public $login;
    
    public $share_mode_1 = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_employee}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_fk', 'id_company_branch_fk', 'id_dict_employee_type_fk', 'id_dict_employee_kind_fk', 'is_active', 'is_admin', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_user_fk','copyUser'], 'integer'],
            [['firstname', 'lastname', 'email', 'id_dict_employee_kind_fk'], 'required'],
            [['custom_data', 'address'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'login', 'share_mode_1'], 'safe'],
            [['user_type'], 'string', 'max' => 30],
            [['firstname', 'lastname'], 'string', 'max' => 300],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'email', 'when' => function($model) { return ($model->user_action != 'share' && $model->user_action != 'lock' && $model->user_action != 'unlock'); }],
            [['email'], 'unique'],
            [['login'], 'required', 'when' => function($model) { return ($model->user_action == 'create'); }],
            ['is_admin', 'validateAdmin', 'when' => function($model) { return $model->is_admin == 1; } ],
            ['id_dict_employee_kind_fk', 'validateManager', 'when' => function($model) { return ( /*$model->id_dict_employee_kind_fk == 70 &&*/ $model->id_department_fk ); } ],
            ['email', 'validateUser' ],
        ];
    }
    
    public function validateUser($attribute, $params)
    {
        $login = explode('@', $this->email)[0];
        $id = (!$this->isNewRecord) ? $this->id_user_fk : 0;
        if ( \common\models\User::find()->where(['username' => $login])->andWhere('id != '.$id)->count() >  0) {
            $this->addError($attribute, 'Użytkownik o wskazanym loginie już istnieje');
        }
    }
    
    public function validateAdmin($attribute, $params)
    {
        if ( CompanyEmployee::find()->where(['status' => 1, 'is_admin' => 1])->andWhere('id != '. ($this->id ? $this->id : 1) )->count() >=2 ) {
            $this->addError($attribute, 'W systemie może być tylko 2 administratorów');
        }
    }
    
    public function validateManager($attribute, $params)
    {
        $managers = CompanyEmployee::find()->where(['status' => 1, 'id_dict_employee_kind_fk' => 70])->andWhere('id != '.($this->id ? $this->id : 1))->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('. $this->id_department_fk .' ) )' )->count();
        $ok = true;
        
        /*if($managers > 0 && $this->id_dict_employee_kind_fk == 70) {
            $ok = false;
            $this->addError($attribute, 'Wybrany dział ma już przypisanego dyrektora');
        }*/
        
        if( !$this->isNewRecord ) {
            $manager = \backend\Modules\Company\models\CompanyDepartment::find()->where(['id_employee_manager_fk' => $this->id])->all();
            
            if($this->id_dict_employee_kind_fk < 70 && count($manager) > 0) {
                $ok = false;
                $this->addError($attribute, 'Pracownik jest kierownikiem działu i nie można zmienić jego stanowiska');
            }
        }
        
        /*foreach($managers as $key => $value) {
            if ( $value->id != $params->id )
                $ok = false;
        }*/
        
        return $ok;
        /*if ( CompanyEmployee::find()->where(['status' => 1, 'is_admin' => 1])->count() >=2 ) {
            $this->addError($attribute, 'W systemie może być tylko 2 administratorów');
        }*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'id_company_branch_fk' => Yii::t('app', 'Oddział'),
            'user_type' => Yii::t('app', 'Grupa'),
            'firstname' => Yii::t('app', 'Imię'),
            'lastname' => Yii::t('app', 'Nazwisko'),
            'phone' => Yii::t('app', 'Telefon'),
            'email' => Yii::t('app', 'Email'),
            'address' => Yii::t('app', 'Adres'),
            'id_dict_employee_type_fk' => Yii::t('app', 'Typ pracownika'),
            'id_dict_employee_kind_fk' => Yii::t('app', 'Rodzaj pracownika'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_admin' => Yii::t('app', 'Administrator'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_department_fk' => 'Dział',
            'departments_list' => 'Działy',
            'id_role_fk' => 'Grupa',
            'copyUser' => 'Przejęcie czynności po pracowniku'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$user = new \common\models\User();
				$user->generateAuthKey();
				$user->status = 10;
				//$user->login = ($this->firstname) ? substr(strtolower($this->firstname),0,1).' '.strtolower($this->lastname) : strtolower($this->lastname);
				//$user->username = \yii\helpers\BaseInflector::slug($user->login, '.', true);
				
				/*$unique = \common\models\User::find()->where(['username' => $user->username])->orderby('id desc')->all();
				if($unique) {
					if(count($unique) == 1)
						$user->username = substr($user->username,0,strlen($user->username)).'2';
					else
						$user->username = substr($user->username,0,strlen($user->username)-1).substr($unique[0]->username,-1,1);
				}*/
				$user->username = explode('@', $this->email)[0];
                $user->firstname = $this->firstname;
                $user->lastname = $this->lastname;
				$this->pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
				$user->setPassword($this->pass);
				$user->email = $this->email;
				if($user->save()) { 
					if($this->user_type) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole($this->user_type);
                        $auth->assign($role, $user->id);
					}
					$this->id_user_fk = $user->id;
					$this->created_by = \Yii::$app->user->id;
					$this->custom_data = \yii\helpers\Json::encode( ['copyUser' => $this->copyUser] );
                    
				} else {
					return false;
				}

				
			} else { 
				$modelArch = new CompanyEmployeeArch();
				$modelArch->id_employee_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				
				/*$this->custom_data = \yii\helpers\Json::encode( 
					[
						'custom_data' => \yii\helpers\Json::decode($this->custom_data), 
						'departments' => $this->departments
					] 
				);*/
				
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
                $auth = Yii::$app->authManager;
                if($this->user_action != 'lock') {
                    if($this->user_type) {
                        
                        $userRoles = \Yii::$app->authManager->getRolesByUser($this->id_user_fk); 
                        foreach($userRoles as $key=>$value) {
                            if('$_user_'.$this->user->username != $key)
                                $auth->revoke($value, $this->id_user_fk);
                        }
                        
                        $role = $auth->getRole($this->user_type);
                        $auth->assign($role, $this->id_user_fk);
                    } else {
                        $userRoles = \Yii::$app->authManager->getRolesByUser($this->id_user_fk); 
                        foreach($userRoles as $key=>$value) {
                            if('$_user_'.$this->user->username != $key)
                                $auth->revoke($value, $this->id_user_fk);
                        }
                    }
                }
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'CompanyEmployee',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/employees/cover',
				'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['basicurl'] . '/uploads/employees/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
        
        $user = \common\models\User::findOne($this->id_user_fk);
        $user->status = -1;
        $user->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public static function getList() {
        
        if(!Yii::$app->user->isGuest) {
            $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $admin = 0;
            if($employee)
                $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();

            $departments = ['0' => 0];
              
            if(Yii::$app->user->identity->username != 'superadmin') {
                if($employee->is_admin || $admin) {
                    return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
                } else {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    }
                    
                    return CompanyEmployee::find()->where(['status' => 1])->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('lastname,firstname')->all();
                }
            } else {
                return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            }
        } else {
            return [];
        }
    }
    
    public static function getListAcc($estatus=0) {
        
        if(!Yii::$app->user->isGuest) {
            $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $manager = 0;
            if($employee)
                $manager = \backend\Modules\Company\models\CompanyDepartment::find()->where(['id_employee_manager_fk' => $employee->id])->count();

            $departments = ['0' => 0];
              
            if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                return CompanyEmployee::find()->where(( ($estatus == 0) ? 'status != -2' : ['status' => $estatus]))->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            } else if($manager) {
                if($temp = $employee->departments) {
                    foreach($temp as $key => $department) {
                        array_push($departments, $department->id_department_fk);
                    }
                }
                return CompanyEmployee::find()->where(( ($estatus == 0) ? 'status != -2' : ['status' => $estatus]))
											 //->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $departments).') )')
											 ->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            } else {
                return CompanyEmployee::find()->where(['status' => 1])->andWhere('(id='.$employee->id.' or id in (select id_to_fk from {{%employee_share}} where id_from_fk = '.$employee->id.'))')->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            }
            
        } else {
            return [];
        }
    }
	
	public static function getListAccByDepartment($id, $estatus = 0) {
        
        if(!Yii::$app->user->isGuest) {
            $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $manager = 0;
            if($employee)
                $manager = \backend\Modules\Company\models\CompanyDepartment::find()->where(['id_employee_manager_fk' => $employee->id])->count();

            $departments = ['0' => 0];
              
            if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                return CompanyEmployee::find()->where(( ($estatus == 0) ? 'status != -2' : ['status' => $estatus]))->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$id.' )')->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            } else if($manager) {
                if($temp = $employee->departments) {
                    foreach($temp as $key => $department) {
                        array_push($departments, $department->id_department_fk);
                    }
                }
                
                return CompanyEmployee::find()->where(( ($estatus == 0) ? 'status != -2' : ['status' => $estatus]))->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$id.' )')->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            } else {
                 return CompanyEmployee::find()->where(['status' => 1, 'id' => $employee->id])->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$id.' )')->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            }
            
        } else {
            return [];
        }
    }
    
    public static function getListByType($id) {
        if($id)
            return CompanyEmployee::find()->where(['status' => 1, 'id_dict_employee_type_fk' => $id])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
        else
            return [];
    }
    
    public static function getListAll() {
        return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
    }
    
    public static function getListByProject($id) {
        
        if($id) {
            return CompanyEmployee::find()->where(['status' => 1])->andWhere('id in (select id_employee_fk from {{%case_employee}} where status = 1 and id_case_fk = '.$id.')')->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
        } else {
            if(!Yii::$app->user->isGuest) {
                $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                $admin = 0;
                if($employee)
                    $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();

                $departments = ['0' => 0];
                  
                if($employee->is_admin || $admin) {
                    return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
                } else {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    }
                    
                    return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
                    //->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
                }
      
            } else {
                return [];
            }
        }
    }
    
    public static function getListByBranch($id) {
        
        if(!Yii::$app->user->isGuest) {
            $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $admin = 0;
            if($employee)
                $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();

            $departments = ['0' => 0];
              
            /*if(Yii::$app->user->identity->username != 'superadmin') {
                if($employee->is_admin || $admin) {
                    return CompanyEmployee::find()->where(['status' => 1, 'id_company_branch_fk' => $id])->orderby('lastname,firstname')->all();
                } else {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    }
                    
                    return CompanyEmployee::find()->where(['status' => 1, 'id_company_branch_fk' => $id])->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('lastname,firstname')->all();
                }
            } else {*/
                return CompanyEmployee::find()->where(['status' => 1, 'id_company_branch_fk' => $id])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            //}
        } else {
            return [];
        }
    }
    
    public static function getManagers() {
        
        return CompanyEmployee::find()->where(['status' => 1])->andWhere('id_dict_employee_kind_fk = 70')->orderby('lastname collate `utf8_polish_ci`,firstname')->all();

    }
    
    public static function getListByDepartments($ids) {
        
        $employee = CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        $departments = [];
          
        if(Yii::$app->user->identity->username != 'superadmin') {
            if($employee->is_admin || $admin) {
                return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
            } else {
                $employeesList = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.$ids.'))')->orderby('lastname collate `utf8_polish_ci`')->all();
                return $employeesList;
               /* if($employeesList) {
                    foreach($employeesList as $key => $department) {
                        array_push($departments, $department->id_department_fk);
                    }
                }
                
                return CompanyEmployee::find()->where(['status' => 1])->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('lastname,firstname')->all();
                */
            }
        } else {
            return CompanyEmployee::find()->where(['status' => 1])->orderby('lastname collate `utf8_polish_ci`,firstname')->all();
        }
    }
    
    public function getBranch()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyBranch::className(), ['id' => 'id_company_branch_fk']);
    }
	
	public function getType()
    {
		return $this->hasOne(\backend\Modules\Dict\models\DictionaryValue::className(), ['id' => 'id_dict_employee_type_fk']);
    }
    
    public function getDepartments()
    {
		$items = $this->hasMany(\backend\Modules\Company\models\EmployeeDepartment::className(), ['id_employee_fk' => 'id'])->where(['status' => 1]); 
		return ( $items ) ? $items : [];
    }
    
    public function getFiles() {
        if($this->id_user_fk == \Yii::$app->user->id)
            $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 1])->orderby('id desc')->all();
        else
            $filesData = [];
        return $filesData;
    }
    
    public static function listTypes() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 1])->orderby('name')->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public static function listKinds() {
        
        //$kinds[0] = 'BRAK';
        $kinds[1] = 'Administrator';
        $kinds[2] = 'Operator';
        $kinds[3] = 'Sekretariat';
        $kinds[4] = 'Zarządzanie stroną';
        
        return $kinds;
    }
    
    public static function listStatus() {
        
        $status[-1] = 'nieaktywny';
        $status[1] = 'aktywny';
        
        return $status;
    }
    
    public function getKindname() {
        return ( isset(self::listKinds()[$this->id_dict_employee_kind_fk]) ) ? self::listKinds()[$this->id_dict_employee_kind_fk] : 'brak danych';
    }
    
    public static function listRoles() {
		$roles = Yii::$app->authManager->getRoles();
		foreach($roles as $key=>$value)	{
			if(substr($value->name, 0, 7) == '$_user_')
				unset($roles[$key]);
		}
		$roles = ArrayHelper::map($roles,'name','name');  
		foreach($roles as $key => $role) {
			$roles[$key] = Yii::t('app', $role);
		}
        //if(Yii::$app->user->identity->role->name != "padre") unset($roles["padre"]);
        return $roles;		   
	}
    
    public function getFullname() {
        return $this->lastname . ' ' . $this->firstname;
    }
    
    public function getGrants() {
        $grants = [];
        
        $auth = Yii::$app->authManager;
        $roleGrants = $auth->getPermissionsByRole($this->user_type);
		$userGrants = \Yii::$app->authManager->getPermissionsByUser($this->id_user_fk); 
		// var_dump($loggedUserRole); exit;
		foreach($roleGrants as $key=>$value) {
			array_push($grants, $value->name);
		}
        foreach($userGrants as $key=>$value) {
			array_push($grants, $value->name);
		}
        //var_dump($grants); exit;
        return $grants;
    }
    
    public function getUser() {
        return \common\models\User::findOne($this->id_user_fk);
    }
    
    public function getTypename() {
        
        $item = \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_employee_type_fk);
        return ($item) ? $item->name : 'brak danych';
    }
    
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
	
	public static function getTasks($id, $type) {
		
		$options = [];
		
		if($type == 1 && $id != 0) {
			$events = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1])->andWhere('id_dict_task_status_fk = 1 and id in (select id_task_fk from {{%task_employee}} where status = 1 and id_employee_fk = '.$id.')')->orderby('name')->all();            

			$options['Rozprawy'] = []; $options['Zadania'] = [];
			foreach($events as $key => $item) {
				$type = ($item->type_fk == 1) ? 'Rozprawy' : 'Zadania';
				$options[$type][$item->id] = $item->name;
			}
        
			if(count($options['Rozprawy']) == 0) unset($options['Rozprawy']);
			if(count($options['Zadania']) == 0) unset($options['Zadania']);
		}
		
		/*if($type == 2 && $id != 0) {
			$events = \backend\Modules\Task\models\CalTodo::find()->where(['status' => 1, 'is_close' => 0, 'id_employee_fk' => $this->module->params['employeeId']])->orderby('name')->all();
			foreach($events as $key => $value) {
				$options[$value->id] = $value->name;
			}
		}*/
        return ($options && $options != NULL) ? ($options) : [];
    }
    
    public function getShares() {
        return \backend\Modules\Company\models\EmployeeShare::find()->where(['status' => 1, 'id_from_fk' => $this->id])->all();
    }
	
	public static function getDoctors() {
		return self::find()->where(['status' => 1, 'id_dict_employee_kind_fk' => 2])->orderby('lastname collate `utf8_polish_ci`')->all();
	}

}
