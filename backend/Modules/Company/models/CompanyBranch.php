<?php

namespace backend\Modules\Company\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use app\Modules\Company\models\CompanyArch;

/**
 * This is the model class for table "{{%company_branch}}".
 *
 * @property integer $id
 * @property integer $id_company_fk
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyBranch extends \yii\db\ActiveRecord
{
    public $user_action = 'aktualizacja';
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_branch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'symbol'], 'required'],
            [['name'], 'unique', 'filter' => ['status'=>1]],
            [['custom_data'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'address'], 'string', 'max' => 300],
            [['symbol'], 'string', 'max' => 10],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 100],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			} else { 
				$modelArch = new CompanyArch();
				$modelArch->table_fk = 2;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = Yii::$app->user->id;
		$result = $this->save();

		return $result;
	}
	
	public function behaviors()	{
		return 
			[
				"timestamp" =>  [
					'class' => TimestampBehavior::className(),
					'createdAtAttribute' => 'created_at',
					'updatedAtAttribute' => 'updated_at',
					'value' => new Expression('NOW()'),
				],			
			];
	}
    
    public static function getList($only) {
       // if($only == -1) {
			return CompanyBranch::find()->where(['status' => 1])->orderby("name")->all();
		//} else {
		//	return CompanyBranch::find()->where(['status' => 1, 'id' => $only])->orderby("name")->all();
		//}
    }
}
