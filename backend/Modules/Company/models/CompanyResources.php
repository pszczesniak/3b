<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use app\Modules\Company\models\CompanyArch;

/**
 * This is the model class for table "{{%company_resources}}".
 *
 * @property integer $id
 * @property integer $id_company_fk
 * @property integer $id_company_branch_fk
 * @property integer $id_dict_company_resource_fk
 * @property string $name
 * @property string $describe
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CompanyResources extends \yii\db\ActiveRecord
{
    public $user_action;
    
    public $color = '#4b87ae';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_resources}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company_fk', 'id_company_branch_fk', 'id_dict_company_resource_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_employee_fk', 'only_management'], 'integer'],
            [['name', 'id_company_branch_fk', 'id_dict_company_resource_fk'], 'required'],
            [['name'], 'unique', 'filter' => ['status' => 1]],
            [['describe', 'custom_data'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'color', 'only_management'], 'safe'],
            [['name'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'id_company_branch_fk' => Yii::t('app', 'Oddział'),
            'id_dict_company_resource_fk' => Yii::t('app', 'Typ'),
            'id_employee_fk' => Yii::t('app', 'Opiekun'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
			'only_management' => 'Rezerwacje tylko dla zarządu i dyrektorów'
        ];
    }
    
    public function beforeSave($insert) {
		$this->custom_data = \yii\helpers\Json::encode(['color' => $this->color]);
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				if(!$this->created_by) $this->created_by = Yii::$app->user->id;
			} else { 
				$this->updated_by = Yii::$app->user->id;
                
                $modelArch = new CompanyArch();
				$modelArch->table_fk = 5;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = Yii::$app->user->id;
		$result = $this->save();

		return $result;
	}
	
	public function behaviors()	{
		return 
			[
				"timestamp" =>  [
					'class' => TimestampBehavior::className(),
					'createdAtAttribute' => 'created_at',
					'updatedAtAttribute' => 'updated_at',
					'value' => new Expression('NOW()'),
				],			
			];
	}
    
    public function getCompany()
    {
		return $this->hasOne(\backend\Modules\Company\models\Company::className(), ['id' => 'id_company_fk']);
    }
    
    public function getBranch()
    {
		$branch =  \backend\Modules\Company\models\CompanyBranch::find()->where(['id' => $this->id_company_branch_fk])->one();
        return ($branch) ? $branch->name : 'bez oddziału';
    }
    
    public function getType()
    {
		$item = \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_company_resource_fk);
        return ($item) ? $item->name : 'brak danych';
    }
    
    public function getDict()
    {
		$item = \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_company_resource_fk);
        if($item) {
            if($item->id_dictionary_fk == 4) {
                $customData = \yii\helpers\Json::decode($item->custom_data);
                $item->icon = $customData['icon'];
                $item->color = $customData['color'];
            }
        }
        return ($item) ? ['name' => $item->name, 'icon' => $item->icon, 'color' => $item->color] : false;
    }
    
    public static function listTypes() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 4])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public static function getGroups($id) {
        $resources = []; $options = []; 
        if(!Yii::$app->user->isGuest) {
            if($id == -1) {
                $branch = false;
            } else {
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                $branch = ($employee->id_company_branch_fk) ? $employee->id_company_branch_fk : 0;
                $isAdmin = ($employee->is_admin) ? $employee->is_admin : 0;
            
                if($id) $branch = $id;
            }
			if(/*$isAdmin && */!$branch) {
				$resources = CompanyResources::find()->where(['status' => 1])->orderby('id_dict_company_resource_fk, name')->all();
			} else {
				$resources = CompanyResources::find()->where(['status' => 1, 'id_company_branch_fk' => $branch])->orderby('name')->all();
            }
		}
        if($id == -1) {
            foreach($resources as $key => $item) {
                $options[$item->type.' '.$item->branch][$item->id] = $item->name;
            }
        } else {
            foreach($resources as $key => $item) {
                $options[$item->type][$item->id] = $item->name;
            }
        }
       
      //  var_dump($options); exit;
        
        return $options;
    }
    
     public static function getByDates($start, $end) {
        $resources = []; $options = []; $occupied = [];
        if(!Yii::$app->user->isGuest && $start && $end) {
            $sql = "select id_resource_fk from {{%company_resources_schedule}} where status >= 0 and "
                    . "( ( '".$start."' = date_from and '".$end."' = date_to) or "
                    ." ( '".$start."' > date_from and '".$start."' < date_to ) or "
                    ." ( '".$end."' > date_from and '".$end."' < date_to ) or "
                    ." ( '".$start."' <= date_from and '".$end."' >= date_to ) ) "; 
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            foreach($data as $key => $value) {
                array_push($occupied, $value['id_resource_fk']);
            }
            //var_dump($occupied);exit;
            $resourcesData = \backend\Modules\Company\models\CompanyResources::find()->where(['status' => 1])->all();
            foreach($resourcesData as $key => $item) {
                if( !in_array($item->id, $occupied) ) {
                    $options[$item->type.' '.$item->branch][$item->id] = $item->name;
                }
            }
		}
        /*foreach($resources as $key => $item) {
            $options[$item->type.' '.$item->branch][$item->id] = $item->name;
        }*/
       
      //  var_dump($options); exit;
        
        return $options;
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getUpdating() {
		$user = \common\models\User::findOne($this->updated_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getGuardian() {
		$user = \backend\Modules\Company\models\CompanyEmployee::findOne( ( ($this->id_employee_fk) ? $this->id_employee_fk : 0 )); 
        return ($user) ? $user->fullname : '';
	}
    
    public function getCustom() {
        $custom = [];
        $custom['color'] = false;
        $customData = \yii\helpers\Json::decode($this->custom_data);
        if($customData) {
            $custom['color'] = $customData['color'];
        } else {
            $custom['color'] = $this->dict['color'];
        }
        
        return $custom;
    }
    
    public function getManager()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
}
