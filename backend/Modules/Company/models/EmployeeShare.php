<?php

namespace backend\Modules\Company\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employee_share}}".
 *
 * @property integer $id
 * @property integer $id_from_fk
 * @property integer $id_to_fk
 * @property integer $mode
 * @property string $data_arch
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class EmployeeShare extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employee_share}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_from_fk', 'id_to_fk', 'mode', 'status', 'created_by', 'deleted_by'], 'integer'],
            [['data_arch'], 'string'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_from_fk' => Yii::t('app', 'Id From Fk'),
            'id_to_fk' => Yii::t('app', 'Id To Fk'),
            'mode' => Yii::t('app', 'Mode'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->deleted_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'deleted_at',
				'value' => new Expression('NOW()'),
            ]
		];
	}
}
