<?php

namespace backend\Modules\Offer\models;

use Yii;

/**
 * This is the model class for table "{{%offer_item}}".
 *
 * @property int $id
 * @property int $id_offer_fk
 * @property int $id_template_item_fk
 * @property string $name
 * @property string $describe
 * @property string $config
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class OfferItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_offer_fk', 'id_template_item_fk'], 'required'],
            [['id_offer_fk', 'id_template_item_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['describe', 'config'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'id_template_item_fk' => Yii::t('app', 'Id Template Item Fk'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
