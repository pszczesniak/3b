<?php

namespace backend\Modules\Offer\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%offer_template}}".
 *
 * @property int $id
 * @property int $id_parent_fk
 * @property string $name
 * @property string $describe
 * @property string $config
 * @property string $date_from
 * @property string $date_to
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class OfferTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_fk', 'is_active', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['describe', 'config'], 'string'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'is_active' => 'Szablon aktywny'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            $new = new \backend\Modules\Offer\models\OfferTemplateItem();
            $new->id_template_fk = $this->id;
            $new->name = 'Produkty';
            $new->status = 2;
            $new->save();
        }   
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getItems() {
        return \backend\Modules\Offer\models\OfferTemplateItem::find()->where(['id_template_fk' => $this->id])->andWhere('status >= 1')->orderby('rank_no')->all();
    }
    
    public static function getList() {
        $templates = [];
        
        $templatesData = self::find()->where(['status' => 1])->orderby('name')->all();
        foreach($templatesData as $key => $item) {
            $templates[$item->id] = $item['name'];
        } 
        
        return $templates;
    }
    
    public function getCreator() {
        return \common\models\User::findOne($this->created_by);
    }
}
