<?php

namespace backend\Modules\Offer\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%offer_template_item}}".
 *
 * @property int $id
 * @property int $id_template_fk
 * @property string $name
 * @property string $describe
 * @property string $config
 * @property int $is_require
 * @property int $value_type
 * @property string $form_type
 * @property string $default_value
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class OfferTemplateItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_template_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_template_fk'], 'required'],
            [['id_template_fk', 'rank_no', 'is_require', 'value_type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['describe', 'config', 'default_value'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'form_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_template_fk' => Yii::t('app', 'Id Template Fk'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'is_require' => Yii::t('app', 'Is Require'),
            'value_type' => Yii::t('app', 'Value Type'),
            'form_type' => Yii::t('app', 'Form Type'),
            'default_value' => Yii::t('app', 'Default Value'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
