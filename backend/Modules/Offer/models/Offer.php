<?php

namespace backend\Modules\Offer\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%offer}}".
 *
 * @property int $id
 * @property int $id_template_fk
 * @property int $id_customer_fk
 * @property int $id_contact_fk
 * @property int $id_employee_fk
 * @property int $type_fk
 * @property string $name
 * @property string $symbol
 * @property string $describe
 * @property string $config
 * @property int $discount_fk
 * @property string $discount_percent
 * @property string $discount_value
 * @property int $tax_fk
 * @property int $accept_required
 * @property string $accept_date
 * @property int $accept_user
 * @property string $accept_send
 * @property int $state
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Offer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_template_fk', 'id_customer_fk', 'id_company_branch_fk', 'id_contact_fk', 'id_employee_fk', 'id_project_fk', 'type_fk', 'discount_fk', 'tax_fk', 'accept_required', 'accept_user', 'state', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_customer_fk', 'id_company_branch_fk', 'id_template_fk', 'state'], 'required'],
            [['describe', 'config'], 'string'],
            [['discount_percent', 'discount_value'], 'number'],
            [['accept_date', 'accept_send', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_template_fk' => Yii::t('app', 'Szablon'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_customer_branch_fk' => Yii::t('app', 'Oddział'),
            'id_contact_fk' => Yii::t('app', 'Kontakt'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_company_branch_fk' => 'Region',
			'id_project_fk' => 'Projekt',
            'type_fk' => Yii::t('app', 'Type Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'discount_fk' => Yii::t('app', 'Discount Fk'),
            'discount_percent' => Yii::t('app', 'Discount Percent'),
            'discount_value' => Yii::t('app', 'Discount Value'),
            'tax_fk' => Yii::t('app', 'Tax Fk'),
            'accept_required' => Yii::t('app', 'Accept Required'),
            'accept_date' => Yii::t('app', 'Accept Date'),
            'accept_user' => Yii::t('app', 'Accept User'),
            'accept_send' => Yii::t('app', 'Accept Send'),
            'state' => Yii::t('app', 'Status'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getPresentation() {
		if(!$this->state) $this->state = 1;
        return isset(self::Viewer()[$this->state]) ? self::Viewer()[$this->state] : self::Viewer()[1];
	}
    
    public static function Viewer() {
		$dictTabs = [
						1 => ['label' => 'nowa', 'color' => 'blue', 'icon' => 'plus'],
						2 => ['label' => 'oczukująca', 'color' => 'orange', 'icon' => 'clock'],
						3 => ['label' => 'zaakceptowana', 'color' => 'green', 'icon' => 'check'],
						4 => ['label' => 'dostarczona', 'color' => 'purple', 'icon' => 'car'],
                        5 => ['label' => 'odrzucona', 'color' => 'red', 'icon' => 'ban'],
				    ];
		return $dictTabs;
	}
	
	public static function getStates() {
		return [1 => 'nowa', 2 => 'oczekująca', 3 => 'zaakceptowana', 4 => 'dostarczona', 5 => 'odrzucona'];
	}
    
    public function getTemplate() {
		return $this->hasOne(\backend\Modules\Offer\models\OfferTemplate::className(), ['id' => 'id_template_fk']);
    }
    
    public function getCustomer() {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getLeadingEmployee() {
        return  \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_fk);
    }
	
    public function getLeadingContact() {
        return  \backend\Modules\Crm\models\CustomerPerson::findOne($this->id_contact_fk);        
    }
    
    public function getNotes() {
        $notesData = \backend\Modules\Task\models\Note::find()->where(['status' => 1, 'id_case_fk' => $this->id, 'id_type_fk' => 0])->orderby('id desc')->all();
        return $notesData;
    }
    
    public function getRegions() {
        return '';
    }
    
    public function getFiles() {
        return \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 13])->orderby('created_at desc')->all();
    }
    
    public function getProducts() {
        return \backend\Modules\Offer\models\OfferProduct::find()->where(['status' => 1, 'id_offer_fk' => $this->id])->all();
    }
    
    public function getGrouping() {
        $groupingData = [];
        $products = \backend\Modules\Offer\models\OfferProduct::find()->where(['status' => 1, 'id_offer_fk' => $this->id])->all();
        foreach($products as $key => $item) {
            if($item->config)
                $params = \yii\helpers\Json::decode($item->config);
            else
                $params = false;
            
            $product = $item->product;
            
            if(isset($groupingData[$product['id_category_fk']])) {
                array_push($groupingData[$product['id_category_fk']]['products'], ['product' => $product, 'unit' => ($product->unit ? $product->unit['symbol'] : 'szt'),'config' => $params]);
            } else {
                $groupingData[$product['id_category_fk']]['name'] = $product->category['name'];
                $groupingData[$product['id_category_fk']]['describe'] = $product->category['describe'];
                $groupingData[$product['id_category_fk']]['products'] = [0 => ['product' => $product, 'unit' => ($product->unit ? $product->unit['symbol'] : 'szt'), 'config' => $params]];
            }
        }
        return $groupingData;
    }
    
    public function getItems() {
        return \backend\Modules\Offer\models\OfferItem::find()->where(['id_offer_fk' => $this->id])->all();
    }
    
    public function getBranch() {
        return \backend\Modules\Crm\models\CustomerBranch::find()->where(['id' => $this->id_customer_branch_fk])->one();
    }
}
