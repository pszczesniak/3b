<?php

namespace backend\Modules\Offer\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%offer_product}}".
 *
 * @property int $id
 * @property int $id_offer_fk
 * @property int $id_product_fk
 * @property string $name
 * @property string $symbol
 * @property string $describe
 * @property string $config
 * @property string $custom_price
 * @property int $discount_fk
 * @property string $discount_percent
 * @property string $discount_value
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class OfferProduct extends \yii\db\ActiveRecord
{
    public $color;
    public $resistance;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_offer_fk', 'id_product_fk', 'custom_price'], 'required'],
            [['id_offer_fk', 'id_product_fk', 'discount_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['describe', 'config'], 'string'],
            [['custom_price', 'discount_percent', 'discount_value'], 'number'],
            [['created_at', 'updated_at', 'deleted_at', 'resistance', 'color'], 'safe'],
            [['name', 'symbol'], 'string', 'max' => 255],
            ['discount_percent', 'required', 'when' => function($model) { return ($model->discount_fk == 1); } ],
            ['discount_value', 'required', 'when' => function($model) { return ($model->discount_fk == 2); } ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'id_product_fk' => Yii::t('app', 'Produkt'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'custom_price' => Yii::t('app', 'Cena'),
            'discount_fk' => Yii::t('app', 'Discount Fk'),
            'discount_percent' => Yii::t('app', 'Wartość procentowa'),
            'discount_value' => Yii::t('app', 'Redukcja kwotowa'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'resistance' => 'Zużycie',
            'color' => 'Kolor'
        ];
    }
    
    public function beforeSave($insert) {
        //if($this->color || $this->resistance)
            $this->config = \yii\helpers\Json::encode(['color' => $this->color, 'resistance' => $this->resistance]);
        
        if($this->discount_fk == 0) {
            $this->discount_percent = 0; $this->discount_value = 0;
        } else if($this->discount_fk == 1) {
            $this->discount_value = round($this->custom_price * $this->discount_percent / 100, 2); 
        } else {
            $this->discount_percent = round($this->discount_value * $this->custom_price / 100, 2); 
        }
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getProduct() {
        return \backend\Modules\Shop\models\ShopProduct::findOne($this->id_product_fk);
    }
}
