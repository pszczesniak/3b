<?php

namespace app\modules\Loc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Loc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
