<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Loc\models\LocProvince */

$this->title = Yii::t('app', 'Create Loc Province');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Provinces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loc-province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
