<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Loc\models\LocProvince */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Loc Province',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Loc Provinces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="loc-province-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
