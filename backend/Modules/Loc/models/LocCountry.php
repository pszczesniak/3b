<?php

namespace backend\Modules\Loc\models;

use Yii;

/**
 * This is the model class for table "{{%loc_country}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $describe
 * @property string $name_lang
 * @property string $describe_lang
 * @property string $config
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class LocCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%loc_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['describe', 'name_lang', 'describe_lang'], 'string'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'config'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
            'config' => Yii::t('app', 'Config'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }

    /**
     * @inheritdoc
     * @return LocCountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LocCountryQuery(get_called_class());
    }
}
