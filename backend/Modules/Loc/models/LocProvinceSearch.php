<?php

namespace app\Modules\Loc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\Modules\Loc\models\LocProvince;

/**
 * LocProvinceSearch represents the model behind the search form about `app\Modules\Loc\models\LocProvince`.
 */
class LocProvinceSearch extends LocProvince
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_country_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'describe', 'name_lang', 'describe_lang', 'config', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LocProvince::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_country_fk' => $this->id_country_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'describe', $this->describe])
            ->andFilterWhere(['like', 'name_lang', $this->name_lang])
            ->andFilterWhere(['like', 'describe_lang', $this->describe_lang])
            ->andFilterWhere(['like', 'config', $this->config]);

        return $dataProvider;
    }
}
