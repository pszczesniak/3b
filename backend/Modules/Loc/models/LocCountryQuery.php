<?php

namespace app\Modules\Loc\models;

/**
 * This is the ActiveQuery class for [[LocCountry]].
 *
 * @see LocCountry
 */
class LocCountryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LocCountry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LocCountry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}