<?php

namespace app\Modules\Svc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\Modules\Svc\controllers';

    /*public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // your custom code here
        if(!\Yii::$app->user->isGuest) {
            $rule = (isset(\Yii::$app->user->identity->role) && \Yii::$app->user->identity->role->name == 'admin');
        } else $rule = true;
        
        if( $rule ) 
            return true;
        else
            return \Yii::$app->getResponse()->redirect('/site/noaccess');
       
    }*/
    
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
