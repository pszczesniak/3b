<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcCategory;
use app\Modules\Svc\models\SvcCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SvccategoryController implements the CRUD actions for SvcCategory model.
 */
class SvccategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
		/*$searchModel = new SvcCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
		$categories = SvcCategory::find()->where(['id_parent_fk'=>0, 'status' => 1])->all();
		$buildStructure = $this->buildCategoryTree($categories, 0);
		
		return $this->render('index', [
            'buildStructure' => $buildStructure
        ]);
    }
	
	private function buildCategoryTree($items,$level) {
		return $this->renderPartial('structure', ['items' => $items, 'level' => $level]);
	}
	

    /**
     * Displays a single SvcCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SvcCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON; 
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
		
	        if ($model->save()) {
				$counter = $model->counter;
                if($counter == 1) $label = ' oferta';
                else if($counter == 2 || $counter == 3 || $counter == 4) $label = ' oferty';
                else $label = ' ofert';
                
                $res = array(
					'success' => true, 'id' => $model->id, 'name' => $model->name, 'type' => '[ '.$counter.$label.' ]' /*($model->id_parent_fk == 0)?'Kategoria':'Podkategoria'*/
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}
		/*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
    }

    /**
     * Deletes an existing SvcCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);//->delete();
        if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		
	        if ($model->delete()) {
				$res = array(
					'success' => true, 'structure' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}
        //return $this->redirect(['index']);
    }

    /**
     * Finds the SvcCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionCreateitem()
    {
        $model = new SvcCategory();
		$model->id_parent_fk = 0;
        
		if(Yii::$app->request->isGet) {
			return $this->renderPartial('_formItem', [  'model' => $model,  ]);
		}
        if (Yii::$app->request->isAjax ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true,'id' => $model->id, 'text' => $model->name, 'parent' => $model->id_parent_fk, 'html' => $this->renderPartial('itemElement', [ 'title' => $model->name, 'type' => 1 , 'counter' => 0, 'labelType' => '[0 ofert]','id' => $model->id, 'items' => [], 'level' => 0 ]) 
				);
			} else {
				$errors = $model->getErrors();
					foreach($errors as $key=>$value) {
						$error = $value;
					}
				$res = array(
					'error'    => $error ,
					'html' =>  $this->renderPartial('_formItem', [  'model' => $model,  ]),
					'success' => false,
				);
			}
			
			return $res;
		} 

    }
}
