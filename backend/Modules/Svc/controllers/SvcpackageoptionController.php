<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcPackageOption;
use backend\Modules\Svc\models\SvcPackageOptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * SvcpackageoptionController implements the CRUD actions for SvcPackageOption model.
 */
class SvcpackageoptionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcPackageOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcPackageOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SvcPackageOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcPackageOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {	
        $model = new SvcPackageOption();
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->rank = SvcPackageOption::find()->where(['status' => 1])->count();
            //$model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");  
            if($model->validate() && $model->save()) {
           
                return array('success' => true, 'refresh' => 'yes', 'table' => "#table-items", 'alert' => 'Pozycja <b>'.$model->name.'</b> została utworzona', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model, 'grants' => $this->module->params['grants']]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model, ]) ;	
		}
	}
    
    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

			if($model->validate() && $model->save()) {
                $data = [];
                $data['name'] = $model->name;
				return array('success' => true,  'alert' => 'Dane zostały zaktualizowane.', 'row' => $data, 'table' => '#table-items', 'refresh' => 'inline', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1) );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model,]) ;	
		}
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTask::SCENARIO_DELETE;

        $model->status = -1;
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Pozycja <b>'.$model->name.'</b> została usunięta')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Pozycja <b>'.$model->name.'</b> została usunięta', 'id' => $id, 'table' => '#table-items', 'refresh' => 'yes');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Pozycja <b>'.$model->name.'</b> nie została usunięta')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Pozycja <b>'.$model->name.'</b> nie została usunięta', 'id' => $id, 'table' => '#table-items', 'refresh' => 'yes');	
			}
        }
        
    }


    /**
     * Finds the SvcPackageOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcPackageOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcPackageOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $items = []; $fields = []; $tmp = [];
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		$options = SvcPackageOption::find()->where(['status' => 1])->orderby('rank')->all();
	
		foreach($options as $key => $value) {
            
            $actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/svc/svcpackageoption/update', 'id' => $value->id]).'" class="btn btn-xs btn-default update" data-table="#table-items" data-target="#modal-grid-item" data-title="Aktualizacja pozycji" title="'.Yii::t('app', 'View').'"><i class="fa fa-pencil-alt"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/svc/svcpackageoption/delete', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-items" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('lsdd', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
        
			$tmp['name'] = $value->name;

            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($items, $tmp); $tmp = [];
		}	
		return $items;
	}
    
    public function actionSave() {
		Yii::$app->response->format = Response::FORMAT_JSON;
 
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = \backend\Modules\Svc\models\SvcPackageOption::findOne($item['id']);
                $model->rank = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
}
