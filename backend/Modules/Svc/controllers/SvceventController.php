<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcEvent;
use app\Modules\Svc\models\SvcEventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SvceventController implements the CRUD actions for SvcEvent model.
 */
class SvceventController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcEvent models.
     * @return mixed
     */
    public function actionIndex()
    {        
		$searchModel = new SvcEventSearch();
        $searchModel->status = 1;
		
		/*$searchModel = new SvcEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);*/

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

        $post = $_GET; $where = [];
        if(isset($_GET['SvcEventSearch'])) {
            $params = $_GET['SvcEventSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, " (lower(name) like '%".strtolower( addslashes($params['name']) )."%' or lower(name) like '%".strtolower( addslashes($params['name']) )."%')");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "status = ".$params['status']);
            }
        } else {
            array_push($where, "status = 1");
        }
        
        $sortColumn = 'e.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        
		$query = (new \yii\db\Query())
            ->select(['e.id as id', 'e.name as city', 'e.status as status', 'name', '(select count(*) from {{%svc_offer_event}} where status = 1 and id_event_fk = e.id) as counter'])
            ->from('{{%svc_event}} as e');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            
        $rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-1' => 'danger', '1' => 'success'];
        $status = ['-1' => 'usunięty', '1' => 'aktywny'];//CompanyEmployee::listTypes();
  
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/svc/svcevent/updateajax/%d" class="btn btn-sm btn-default gridViewModal" data-table="#table-events" data-target="#modal-grid-item"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1 ) ? '<a href="/panel/svc/svcevent/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['name'] = $value['name'];
            $tmp['counter'] = $value['counter'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single SvcEvent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcEvent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
        $model = new SvcEvent();
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Subskrybent <b>'.$model->name.'</b> został zarejestrowany', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}
    
    /**
     * Updates an existing SvcEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]); 
        } else {
            return $this->renderPartial('_form', [
                'model' => $model
            ]);
        }
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }


    /**
     * Deletes an existing SvcEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-events');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-events');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the SvcEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcEvent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
