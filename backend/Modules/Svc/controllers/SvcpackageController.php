<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcPackage;
use app\Modules\Svc\models\SvcPackageSearch;
use backend\Modules\Svc\models\SvcPackageOption;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * SvcpackageController implements the CRUD actions for SvcPackage model.
 */
class SvcpackageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcPackage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcPackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SvcPackage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcPackage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcPackage();
        $config = [];
		
		$options = SvcPackageOption::find()->all();
		foreach($options as $key => $option) {
			$config[$option->id]['label'] = $option->name;
			$config[$option->id]['value'] = '-';
		}
		
		if($model->options) {
			$configPackage = \yii\helpers\Json::decode($model->options); 
			foreach($configPackage as $key => $option) {
				$config[$key]['value'] = $option;
			}
		} 
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model, 'config' => $config
            ]);
        }
    }

    /**
     * Updates an existing SvcPackage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        $model = $this->findModel($id);
		$config = [];
		
		$options = SvcPackageOption::find()->where(['status' => 1])->orderby('rank')->all();
		foreach($options as $key => $option) {
			$config[$option->id]['label'] = $option->name;
			$config[$option->id]['value'] = '-';
		}
		
		if($model->options) {
			$configPackage = \yii\helpers\Json::decode($model->options); 
			foreach($configPackage as $key => $option) {
				$config[$key]['value'] = $option;
			}
		} 
		
		if(Yii::$app->request->isPost) {
			$options = [];
			for ($i = 1; $i <= 20; $i++) {
				if(Yii::$app->request->post('option-'.$i)) {
					$options[$i] = Yii::$app->request->post('option-'.$i);
				} else {
					$options[$i] = '-';
				}
			}
			$model->options = \yii\helpers\Json::encode($options);
		}

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'config' => $config
            ]);
        }
    }

    /**
     * Deletes an existing SvcPackage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SvcPackage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcPackage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcPackage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $items = []; $fields = []; $tmp = [];
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		$packages = SvcPackage::find()->where(['status' => 1])->orderby('rank')->all();
	
		foreach($packages as $key => $value) {
            
            $actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/svc/svcpackage/update', 'id' => $value->id]).'" class="btn btn-xs btn-default" data-table="#table-items" title="'.Yii::t('app', 'View').'"><i class="fa fa-pencil-alt"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/svc/svcpackage/delete', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-items" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('lsdd', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
        
			$tmp['name'] = $value->name;
            $tmp['label'] = $value->price_label;

            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($items, $tmp); $tmp = [];
		}	
		return $items;
	}
    
    public function actionSave() {
		Yii::$app->response->format = Response::FORMAT_JSON;
 
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = \backend\Modules\Svc\models\SvcPackage::findOne($item['id']);
                $model->rank = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
}
