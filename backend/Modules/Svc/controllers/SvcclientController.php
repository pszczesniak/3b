<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcClient;
use app\Modules\Svc\models\SvcClientSearch;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SvcclientController implements the CRUD actions for SvcClient model.
 */
class SvcclientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcClient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcClientSearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

        $post = $_GET; $where = [];
        if(isset($_GET['SvcClientSearch'])) {
            $params = $_GET['SvcClientSearch'];
            if(isset($params['firstname']) && !empty($params['firstname']) ) {
				array_push($where, " (lower(firstname) like '%".strtolower( addslashes($params['firstname']) )."%' or lower(lastname) like '%".strtolower( addslashes($params['firstname']) )."%')");
            }
            if(isset($params['city']) && !empty($params['city']) ) {
				array_push($where, "lower(city) like '%".strtolower( addslashes($params['city']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "status = ".$params['status']);
            }
            if(isset($params['id_package_fk']) && !empty($params['id_package_fk']) ) {
				array_push($where, "id_package_fk = ".$params['id_package_fk']);
            }
        } 
        
        $sortColumn = 'c.id';
        if( isset($post['sort']) && $post['sort'] == 'firstname' ) $sortColumn = 'firstname';
        if( isset($post['sort']) && $post['sort'] == 'lastname' ) $sortColumn = 'lastname';
        if( isset($post['sort']) && $post['sort'] == 'city' ) $sortColumn = 'city';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'status';
        
		$query = (new \yii\db\Query())
            ->select(['c.id as id', 'c.city as city', 'c.status as status', 'firstname', 'lastname', 'id_user_fk'])
            ->from('{{%svc_client}} as c');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            
        $rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-1' => 'danger', '1' => 'success'];
        $status = ['-1' => 'usunięty', '1' => 'aktywny'];//CompanyEmployee::listTypes();
  
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/svc/svcclient/view/%d" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="/panel/svc/svcclient/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1 ) ? '<a href="/panel/svc/svcclient/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['firstname'] = $value['firstname'];
            $tmp['lastname'] = $value['lastname'];
            $tmp['city'] = $value['city'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/../images/default-user.png";
            $tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single SvcClient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcClient();
        $user = new User();

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/) {
            //return $this->redirect(['view', 'id' => $model->id]);
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $user = new \common\models\User();
				$user->generateAuthKey();
				$user->status = 10;
                $user->roleType = 'client';

				$user->username = ($model->username) ? $model->username : $model->email; 
				$pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
				$user->setPassword($pass);
				$user->email = $model->email;
				if($user->save()) {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole('client');
                    $auth->assign($role, $user->id);

					$model->id_user_fk = $user->id;
					$model->facebook_config = $pass;
                    /*\Yii::$app->mailer->compose(['html' => 'employeeAuthorization-html', 'text' => 'employeeAuthorization-text'], ['login' => $user->username, 'password' => $pass])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($model->email)
                    ->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
                    ->send();*/
				}
                if($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    $transaction->rollBack();
                    return $this->render('create', [
                        'model' => $model, 'user' => $user, 'errors' => array_merge($model->getErrors(), $user->getErrors())
                    ]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                        'model' => $model, 'user' => $user, 'errors' => array_merge($model->getErrors(), $user->getErrors())
                    ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model, 'user' => $user
            ]);
        }
    }

    /**
     * Updates an existing SvcClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->pos_lat = ($model->pos_lat) ? $model->pos_lat : 52.173931692568;
        $model->pos_lng = ($model->pos_lng) ? $model->pos_lat : 18.8525390625;
        
        $user = User::findOne($model->id_user_fk);
        if($user)
            $model->username = $user->username;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'user' => $user
            ]);
        }
    }

    /**
     * Deletes an existing SvcClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $user = \common\models\User::findOne($model->id_user_fk); 
        $user->status = 0;
        $user->save(); 
        if(Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Klient został usunięty', 'id' => $id, 'table' => '#table-clients'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the SvcClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
