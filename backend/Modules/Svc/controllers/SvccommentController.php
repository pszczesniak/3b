<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcComment;
use app\Modules\Svc\models\SvcCommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\helpers\Url;

/**
 * SvccommentController implements the CRUD actions for SvcComment model.
 */
class SvccommentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcComment models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new SvcCommentSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		//$queryParams["SvcCommentSearch"]["id_offer_fk"] = 0;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionManage()
    {
        $searchModel = new SvcCommentSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["SvcCommentSearch"]["id_offer_fk"] = 0;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData($type) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['SvcCommentSearch'])) {
            $params = $_GET['SvcCommentSearch'];
            if(isset($params['comment']) && !empty($params['comment']) ) {
				array_push($where, "lower(comment) like '%".strtolower( addslashes($params['comment']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "c.status = ".$params['status']);
            }
            if(isset($params['id_offer_fk']) && !empty($params['id_offer_fk']) ) {
				array_push($where, "lower(o.companyname) like '%".strtolower( addslashes($params['id_offer_fk']) )."%'");
            }
            if(isset($params['id_client_fk']) && !empty($params['id_client_fk']) ) {
				array_push($where, "(lower(c.email) like '%".strtolower( addslashes($params['id_client_fk']) )."%' or lower(cl.lastname) like '%".strtolower( addslashes($params['id_client_fk']) )."%' or lower(cl.firstname) like '%".strtolower( addslashes($params['id_client_fk']) )."%')");
            }
        } 
        
        $sortColumn = 'c.id';
        if( isset($post['sort']) && $post['sort'] == 'companyname' ) $sortColumn = 'o.companyname';
        if( isset($post['sort']) && $post['sort'] == 'lastname' ) $sortColumn = 'cl.lastname';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'c.status';
        if( isset($post['sort']) && $post['sort'] == 'rank' ) $sortColumn = 'rank';
		
        if($_GET['type'] == 1) {
            $query = (new \yii\db\Query())
                ->select(['c.id as id', 'c.comment as comment', 'c.status as status', 'rank', 'c.email as email', 'c.created_at as created_at', 'id_offer_fk', 'companyname', "concat_ws(' ', cl.lastname, cl.firstname) as cname"])
                ->from('{{%svc_comment}} as c')
                ->join(' JOIN', '{{%svc_offer}} as o', 'o.id = c.id_offer_fk')
                ->join(' LEFT JOIN', '{{%svc_client}} as cl', 'cl.id = c.id_client_fk');
        } else {
            $query = (new \yii\db\Query())
                ->select(['c.id as id', 'c.comment as comment', 'c.status as status', 'rank', 'c.email as email', 'c.created_at as created_at', 'id_offer_fk', "concat_ws('', 'portal') as companyname", "concat_ws(' ', cl.lastname, cl.firstname) as cname"])
                ->from('{{%svc_comment}} as c')
                ->join(' LEFT JOIN', '{{%svc_client}} as cl', 'cl.id = c.id_client_fk')
                ->where(['id_offer_fk' => 0]);
        }
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $status = [0 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'];
        $colors = [0 => 'danger', 1 => 'info', 2 => 'success'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/svc/svccomment/view/%d" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
           // $actionColumn .= '<a href="/panel/svc/svccomment/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
           // $actionColumn .= ( ($value['status'] == 1) ? '<a href="/panel/svc/svccomment/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['offer'] = '<a href="'.Url::to(['\svc\svcoffer\view', 'id' => $value['id_offer_fk']]).'" title="Przejdź do oferty">'.$value['companyname'].'</a>';
            $words = array_slice(explode(' ', strip_tags($value['comment'])), 0, 30);
            $tmp['comment'] = implode(' ', $words).( (count($words) > 30) ? ' [...]' : '');
            $tmp['rank'] = $value['rank'];
            $tmp['created'] = $value['created_at'];
            $tmp['status'] =' <span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>';
            $tmp['client'] = ($value['cname']) ? $value['cname'] : $value['email'];
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single SvcComment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'delete';
        
        $statusTmp = $model->status;
        
        if ($model->load(Yii::$app->request->post())) {
            $model->status = 0;
            if($model->save()) {
                Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Komentarz został odrzucony')  );
            } else {
                $model->status = $statusTmp;
                Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Proszę wpisac powód odrzucenia!')  );
            }
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionCommentaccept($id) {
        
       /* if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        } */
        
        $comment = SvcComment::findOne($id);
        $comment->status = 2;
        $comment->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Komentarz został opublikowany')  );
        return $this->redirect(['view', 'id' => $comment->id]);
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }

    /**
     * Creates a new SvcComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcComment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SvcComment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SvcComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SvcComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
