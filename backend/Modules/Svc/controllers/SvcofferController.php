<?php

namespace app\Modules\Svc\controllers;

use Yii;
use backend\Modules\Svc\models\SvcOffer;
use app\Modules\Svc\models\SvcOfferSearch;
use backend\Modules\Svc\models\SvcOfferChanges;
use backend\Modules\Svc\models\SvcCategory;
use backend\Modules\Svc\models\SvcEvent;
use backend\Modules\Svc\models\SvcPackage;
use backend\Modules\Svc\models\SvcOfferEvent;
use backend\Modules\Svc\models\SvcOfferCategory;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;

/**
 * SvcofferController implements the CRUD actions for SvcOffer model.
 */
class SvcofferController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SvcOffer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcOfferSearch();
        $searchModel->status = 1;
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
        ]);
    }

    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['SvcOfferSearch'])) {
            $params = $_GET['SvcOfferSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(o.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['city']) && !empty($params['city']) ) {
				array_push($where, "lower(o.city) like '%".strtolower( addslashes($params['city']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "o.status = ".$params['status']);
            }
            if(isset($params['id_package_fk']) && !empty($params['id_package_fk']) ) {
				array_push($where, "id_package_fk = ".$params['id_package_fk']);
            }
        } 
        
        $sortColumn = 'o.id';
        if( isset($post['sort']) && $post['sort'] == 'companyname' ) $sortColumn = 'o.companyname';
        if( isset($post['sort']) && $post['sort'] == 'city' ) $sortColumn = 'o.city';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'o.status';
        if( isset($post['sort']) && $post['sort'] == 'package' ) $sortColumn = 'p.name';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.city as city', 'o.status as status', 'companyname', 'p.name as pname', 'confirm', 'id_user_fk'])
            ->from('{{%svc_offer}} as o')
            ->join(' JOIN', '{{%svc_package}} as p', 'p.id = o.id_package_fk');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand())
		      
       /* $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/matter/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/task/matter/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/task/matter/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';*/
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'info', '1' => 'primary', '2' => 'success'];
        $status = ['-3' => 'rezygnacja przez email', '-2' => 'odrzucenie zaproszenia', '-1' => 'nie wysłano powiadomienia', '0' => 'oczekiwanie na decyzję', '1' => 'bezpośrednia', '2' => 'przyjęcie zaproszenia'];//CompanyEmployee::listTypes();

        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/svc/svcoffer/view/%d" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="/panel/svc/svcoffer/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="/panel/svc/svcoffer/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-offers"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['companyname'] = $value['companyname'];
            $tmp['package'] = $value['pname'];
            $tmp['city'] = $value['city'];
            $tmp['status'] = isset($status[$value['confirm']]) ? '<span class="label label-'.$colors[$value['confirm']].'">'.$status[$value['confirm']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/../images/default-user.png";
            $tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single SvcOffer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcOffer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcOffer();
        $model->confirm = -1;
        $model->confirm_payment = 0;
        $model->rule = 'on';
        $model->pos_lat = 52.173931692568;
        $model->pos_lng = 18.8525390625;

        $categoriesChecked = [];
		$categories = SvcCategory::find()->where(['status' => 1, 'id_parent_fk' => 0])->all();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $user = new \common\models\User();
				$user->generateAuthKey();
				$user->status = 10;
                $user->roleType = 'offer';

				$user->username = ($model->username) ? $model->username : $model->email; 
				$pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
				$user->setPassword($pass);
				$user->email = $model->email;
				if($user->save()) {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole('offer');
                    $auth->assign($role, $user->id);

					$model->id_user_fk = $user->id;
					$model->facebook_config = $pass;
                    /*\Yii::$app->mailer->compose(['html' => 'employeeAuthorization-html', 'text' => 'employeeAuthorization-text'], ['login' => $user->username, 'password' => $pass])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($model->email)
                    ->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
                    ->send();*/
				}
                if($model->save()) {
                    $transaction->commit();

                    if(Yii::$app->request->post('Categories')) {
                        foreach(Yii::$app->request->post('Categories') as $key=>$value) {
                            $model_oe = new SvcOfferCategory();
                            $model_oe->id_offer_fk = $model->id;
                            $model_oe->id_category_fk = substr($key,5);
                            $model_oe->save();
                        }
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    $transaction->rollBack();
                    return $this->render('create', [
                        'model' => $model, 'categories' => $categories, 'categoriesChecked' => $categoriesChecked, 'errors' => $user->getErrors()
                    ]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                        'model' => $model,
                    ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model, 'categories' => $categories, 'categoriesChecked' => $categoriesChecked, 'errors' => false
            ]);
        }
    }

    /**
     * Updates an existing SvcOffer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $categories = SvcCategory::find()->where(['status' => 1, 'id_parent_fk' => 0])->all();

        $model = $this->findModel($id);
        $model->rule = 'on';
        $user = User::findOne($model->id_user_fk);
        if($user)
        $model->username = $user->username;
        $model->pos_lat = ($model->pos_lat) ? $model->pos_lat : 52.173931692568;
        $model->pos_lng = ($model->pos_lng) ? $model->pos_lat : 18.8525390625;

        foreach($model->events as $key => $event) {
            $model->events_temp[$event->id_event_fk] = $event->id_event_fk;
        }
       // $model->events_temp = explode(',', $model->events_temp);
        $categoriesChecked = [];
        foreach($model->categories as $key => $category) {
            $model->categories_temp[$category->id_category_fk] = $category->id_category_fk;
            array_push($categoriesChecked, $category->id_category_fk);
        }
        $model->province_additional = explode(',', $model->province_additional);

        if ($model->load(Yii::$app->request->post()) ) {

            $province_additional = '';
            if($model->province_additional && is_array($model->province_additional))
                $model->province_additional = implode(',',$model->province_additional);

            if ($model->save()) {
                if(Yii::$app->request->post('Categories')) {
                    SvcOfferCategory::deleteAll('id_offer_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('Categories') as $key=>$value) {
                        $model_oe = new SvcOfferCategory();
                        $model_oe->id_offer_fk = $model->id;
                        $model_oe->id_category_fk = substr($key,5);
                        $model_oe->save();
                    }
                } else {
                    SvcOfferCategory::deleteAll('id_offer_fk = :offer', [':offer' => $id]);
                }
                if($model->events_temp && !empty($model->events_temp) ) {
                    SvcOfferEvent::deleteAll('id_offer_fk = :offer', [':offer' => $id]);
                    foreach($model->events_temp as $key=>$value) {
                        $model_oe = new SvcOfferEvent();
                        $model_oe->id_offer_fk = $model->id;
                        $model_oe->id_event_fk = $value;
                        $model_oe->save();
                    }
                } else {
                    SvcOfferEvent::deleteAll('id_offer_fk = :offer', [':offer' => $id]);
                }
                $user = \common\models\User::findOne($model->id_user_fk);
                $user->roleType = 'offer';
                $user->email = $model->email;
                $user->save();
            } else {
                return $this->render('update', [
                    'model' => $model, 'categories'=>$categories, 'categoriesChecked' => $categoriesChecked
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'categories'=>$categories, 'categoriesChecked' => $categoriesChecked
            ]);
        }
    }

    /**
     * Deletes an existing SvcOffer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Oferta została usunięta', 'id' => $id, 'table' => '#table-offers'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the SvcOffer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcOffer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SvcOffer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSend($id)
    {
        $model = $this->findModel($id);
        $model->confirm = 0;
        $model->rule = 'on';
        

        $user = User::findOne($model->id_user_fk);
        try {
            \Yii::$app->mailer->compose(['html' => 'offerInfo-html', 'text' => 'offerInfo-text'], ['login' => $user->username, 'pass' => $model->facebook_config])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($model->email)
                        ->setBcc('tp@teststage.eu')
                        ->setSubject('Propozcyja współpracy ' . \Yii::$app->name)
                        ->send();
            $model->save();
        } catch (\Swift_TransportException $e) {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Problem z wysłaniem e-maila'));
        }

        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionResignation($id)
    {
        $model = $this->findModel($id);
        $model->confirm = -3;
        $model->rule = 'on';
        $model->save();

        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionPublish($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $message = $model->brief.'<br />'.'<a href="'.\Yii::$app->urlManagerFrontEnd->createUrl('oferta/'.CustomHelpers::decode($model->id).'/'.$model->slug).'">Link do oferty</a>';
        $response = false;
        $appId = '682422408630872';
        $appSecret = '91a618f4fd16b6a08e511d54f33ff7b4';
        $accessToken = \Yii::$app->session->get('fb.token');
        
        if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
            $fb = new \Facebook\Facebook([
                'app_id' => $appId,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v2.8',
                'default_access_token' => \Yii::$app->session->get('fb.token'),
                'cookie' => true
            ]);
            //$imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg':'/images/fb_logo.jpg';
            $imgSrc = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/avatar-".$model->id_user_fk."-big.jpg")) ? "/../uploads/avatars/avatar-".$model->id_user_fk."-big.jpg" : "/../images/fb_logo.jpg";
            $linkData = [
              'link' => \Yii::$app->urlManagerFrontEnd->createUrl('oferta/'.CustomHelpers::encode($model->id).'/'.$model->slug),
              'message' => ($model->brief) ? $model->brief : trim(preg_replace('/\s\s+/', ' ', $model->note)),
              'picture' => 'http://twojeprzyjecia.pl'.$imgSrc,
              'caption' => 'twojeprzyjecia.pl',
              'name' => 'Integrator przyjęć - oferta',
              'description' => $model->companyname
              ];

            try {
             
                $permissions = $fb->get('/me/permissions');//var_dump($permissions);
                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                $user = $response->getGraphUser(); //var_dump($user);
                $msg = $fb->post( '/'.$user->getId().'/feed', $linkData, $accessToken  );
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
               // echo 'Graph returned an error: ' . $e->getMessage();
                return ['result' => false, 'error' => $e->getMessage(), 'alert' => '<div class="alert alert-success">Oferta nie została opublikowana na FB</div>'];
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                return ['result' => false,  'error' => $e->getMessage()];
            }
            return ['result' => true, 'msg' => $msg, 'alert' => '<div class="alert alert-success">Oferta została opublikowana na FB</div>'];
        } else {
        
            return ['result' => false, 'alert' => '<div class="alert alert-success">Oferta nie została opublikowana na FB</div>'];
        }
    }
}
