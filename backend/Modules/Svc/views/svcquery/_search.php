<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-query-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-svc-query-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-query']
    ]); ?>
    
    <div class="grid">
        <div class="col-xs-12"> <?= $form->field($model, 'message')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-query', 'data-form' => '#filter-svc-query-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
