<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcQuery */

$this->title = 'Zapytanie #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zapytania z integratora', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-query-view", 'title'=>Html::encode($this->title))) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'details:html',
            [                      
                'attribute' => 'details:html',
                'format'=>'raw',
                'label' => 'Szczegóły',
                'value' => $model->details,
            ],
            'email:email',
            'message:ntext',
            'created_at',
            [                      
                'label' => 'Utworzony przez',
                'value' => $model->author,
            ],
            [                      
                'label' => 'Liczba wysłanych wiadomości',
                'value' => count($model->messages),
            ],
        ],
    ]) ?>

<?= $this->endContent(); ?>