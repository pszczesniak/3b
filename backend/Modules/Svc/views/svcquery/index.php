<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\Modules\Svc\models\SvcQuerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zapytania z integratora';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-index", 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-query" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Svc Event'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-query"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-query"

            data-toolbar="#toolbar-query" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-svc-query-search"
            data-url=<?= Url::to(['/svc/svcquery/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="email"  data-sortable="true" >E-mail</th>
                <th data-field="message"  data-sortable="true" >Wiadomość</th>
                <th data-field="created"  data-sortable="true" >Utworzono</th>                    
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>
    
    
<?= $this->endContent(); ?>
<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modalConfirm', // <-- insert this modal's ID
	]);
 
	echo ' <div class="modal-body"><p>Czy na pewno chcesz usunąć ten element?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

	yii\bootstrap\Modal::end();
?> 

