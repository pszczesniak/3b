<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcQuery */

$this->title = Yii::t('app', 'Create Svc Query');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Queries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-query-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
