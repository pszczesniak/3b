<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcQuery */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Svc Query',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Queries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-query-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
