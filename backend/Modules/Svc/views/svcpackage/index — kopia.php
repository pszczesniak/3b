<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pakiety');
$this->params['breadcrumbs'][] = 'Biuro';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-package-index", 'title'=>Html::encode($this->title))) ?>

    <?php /* \yii\widgets\Pjax::begin(['id' => 'pjax-grid-view-dicts', 'timeout' => 1000]); */ ?>
		<div id="toolbar" class="btn-group">
			<?= Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Svc Event'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
			<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
		</div>
		<?= GridView::widget([
			'id' => 'grid-view-dicts',
			'dataProvider' => $dataProvider,
			'tableOptions'=>Yii::$app->params['tableOptions'],
			//'summary' => "<div class='summary'>". Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.').Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Lsdd Material Function'), ]), ['create'], ['class' => 'btn btn-xs btn-success btn-icon'])."</div>" ,
			'filterModel' => $searchModel,
			//'emptyText' => Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Lsdd Recipe Group'), ]), ['create'], ['class' => 'btn btn-success btn-icon']),
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				[
				   'class' => 'yii\grid\DataColumn',
				   'headerOptions' => ['data-searchable' => 'true', 'data-field' => "name", 'data-searchable' => "true", 'data-filter-control' => "input", /*'data-sortable' => 'true'*/],
				   'attribute' => 'name',
				],
				[
				   'class' => 'yii\grid\DataColumn',
				   'headerOptions' => ['data-searchable' => 'true', 'data-field' => "price_label", 'data-searchable' => "true", 'data-filter-control' => "input", /*'data-sortable' => 'true'*/],
				   'attribute' => 'price_label',
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'contentOptions' => ['class' => 'table-actions'],
					'template' => '{update}{delete}',
					'headerOptions' => ['data-switchable'=>'false'],
					'buttons' => [
						'view' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
									'title' => Yii::t('app', 'View'), 'class' => 'btn btn-default btn-sm', 'data-title' => '<i class="glyphicon glyphicon-eye-open"></i>'.Yii::t('lsdd', 'View')
							]);
						},
						'update' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
									'title' => Yii::t('app', 'Update'), 'class' => 'btn btn-default btn-sm', 'data-title' => '<i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Update')
							]);
						},
						'delete' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
									'title' => Yii::t('app', 'Remove'), 'class' => 'btn btn-default btn-sm',
									'data-pjax' => "0", 'data-method' => "post", 'data-confirm' => "Czy na pewno usunąć ten element?", 'aria-label' => "Usuń"
							]);
						}
					],
				],
			],
		]); ?>
	<?php /* \yii\widgets\Pjax::end(); */ ?>

<?= $this->endContent(); ?>

