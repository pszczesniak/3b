<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\Svcitemsearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pakiety');
$this->params['breadcrumbs'][] = 'Biuro';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-package-index", 'title'=>Html::encode($this->title))) ?>

    <div id="toolbar-items" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/svc/svcpackage/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]) ?>
        <a class="btn bg-purple btn-icon none" id="save-table-items-move" href="<?= Url::to(['/svc/svcpackage/save']) ?>"><i class="fa fa-save"></i>Zapisz zmiany</a>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-items"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-items"
                data-toolbar="#toolbar-items" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-show-footer="false"
                data-page-size="50"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-sort-name="lastname"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-eg-items-search"
                data-url=<?= Url::to(['/svc/svcpackage/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name" data-sortable="true">Nazwa</th>
                    <th data-field="label" data-sortable="true">Etykieta</th>
					
                    <th data-field="move" data-events="actionEvents" data-align="center" data-width="100px"></th>
                    <th data-field="actions" data-events="actionEvents" data-width="90px" data-align="center"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?= $this->endContent(); ?>

