<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcPackage */

$this->title = 'Nowy pakiet';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pakiety'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-package-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model, 'config' => $config
    ]) ?>

<?= $this->endContent(); ?>

