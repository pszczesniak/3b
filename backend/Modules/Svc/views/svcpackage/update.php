<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcPackage */

$this->title = 'Aktualizacja pakietu';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pakiety'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-package-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model, 'config' => $config
    ]) ?>

<?= $this->endContent(); ?>
