<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcPackage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-package-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="grid">
		<div class="col-sm-6">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <div class="grid">
                <div class="col-sm-4">
                    <?= $form->field($model, 'price')->textInput() ?>
                </div>
                <div class="col-sm-8">
                    <?= $form->field($model, 'price_label')->textInput() ?>
                </div>
            </div>
            <div class="grid">
                <div class="col-sm-4">
                    <?= $form->field($model, 'price_sale')->textInput() ?>
                </div>
                <div class="col-sm-8"><br />
                    <?= $form->field($model, 'is_sale', ['template' => '{input} {label} {error}'])->checkbox() ?>
                </div>
             </div>
            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
		</div>
		<div class="col-sm-6">
			<fieldset><legend>Opcje</legend>
				<table class="table table-striped table-hover">
				<?php foreach($config as $key => $option) { ?>
                    <?php if(isset($option['label'])) { ?>
					<tr><td><?= $option['label'] ?></td><td><input type="text" class="align-center" name="option-<?= $key ?>" value="<?= $option['value'] ?>"></input></td></tr>
                    <?php } ?>
				<?php } ?>
				</table>
			</fieldset>
			
		</div>
	</div>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
