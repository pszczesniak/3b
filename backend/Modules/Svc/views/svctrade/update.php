<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcTrade */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Svc Trade',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Trades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-trade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
