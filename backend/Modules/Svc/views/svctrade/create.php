<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcTrade */

$this->title = Yii::t('app', 'Create Svc Trade');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Trades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-trade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
