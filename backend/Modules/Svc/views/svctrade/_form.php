<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcTrade */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#pjax-grid-view-form-dicts").on("pjax:end", function() {
            $.pjax.reload({container:"#grid-view-dicts", timeout: 1000});  //Reload GridView
			/*$("table").bootstrapTable({locale:"en-US"});*/
        });
    });'
);
?>

<div class="svc-trade-form">
    <?php yii\widgets\Pjax::begin(['id' => 'pjax-grid-view-form-dicts']) ?>
        <?php $form = ActiveForm::begin(['id' => $model->isNewRecord?'modal-grid-view-form-dict-create':'modal-grid-view-form-dict', 'options' => ['data-pjax' => true, 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <!--<?= $form->field($model, 'describe')->textarea(['rows' => 6]) ?>-->

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>
