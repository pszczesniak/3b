<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcPackageOption */

$this->title = Yii::t('app', 'Create Svc Package Option');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Package Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-package-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
