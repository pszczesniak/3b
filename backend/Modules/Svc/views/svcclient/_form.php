<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\GoogleMapWidget;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <fieldset><legend>Dane autoryzacyjne</legend>
                <div class="row">
                    <div class="col-xs-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true, 'disabled'=>( ($model->isNewRecord) ? false : true )]) ?></div>
                    <div class="col-xs-6"> <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled'=>( ($model->isNewRecord) ? false : true )]) ?> </div>
                </div>
            </fieldset>
            <fieldset><legend>Dane personalne i kontaktowe</legend>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="svcclient-event_date" class="control-label">Data urodzenia</label>
                            <div class='input-group date clsDatePicker' id='datetimepicker_event'>
                                <input type='text' class="form-control" id="svcclient-event_date" name="SvcClient[event_date]" value="<?= $model->birthday ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset><legend>Dodatkowe informacje</legend>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'profession')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'id_trade_fk')->dropdownList(ArrayHelper::map( backend\Modules\Svc\models\SvcTrade::find()->orderby('name')->all(), 'id', 'name'), ['prompt' => 'Branża']); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'id_education_fk')->dropdownList(ArrayHelper::map( backend\Modules\Svc\models\SvcEducation::find()->orderby('name')->all(), 'id', 'name'), ['prompt' => 'Wykształcenie']); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'sex')->dropdownList(['F' => 'Kobieta', 'M' => 'Mężczyzna'], []); ?>                   
                    </div>
                </div>  
            </fieldset>
        </div>
        <div class="col-xs-12"><?= $form->field($model, 'note')->textarea(['rows' => 6]) ?></div>
    </div>
    <fieldset><legend>Lokalizacja</legend>
        <div class="row">
            <div class="col-sm-8">
                <?= $form->field($model, 'id_province_fk')->dropDownList( ArrayHelper::map(backend\Modules\Loc\models\LocProvince::find()->where(['status' => 1])->orderby('name')->all(), 'id', 'name'), array() )  ?>                

                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <div class="google-map">
                    <?= GoogleMapWidget::widget(['model' => $model]) ?>
                    <?= $form->field($model, 'pos_lat', ['template' => '{input}{error}'])->hiddenInput()->label(false) ?> <?= $form->field($model, 'pos_lng', ['template' => '{input}'])->hiddenInput()->label(false) ?>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-group align-right">
        <?= ($model->status != -1) ? Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>