<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */

$this->title = Yii::t('app', 'Edycja klienta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-client-update", 'title'=>Html::encode($this->title))) ?>
    <?php if($model->status == -1) { ?>
        <div class="alert alert-danger">Klient został usnięty</div>
    <?php }  ?>

    <?= $this->render('_form', [
        'model' => $model, 'user' => $user
    ]) ?>

<?= $this->endContent(); ?>
