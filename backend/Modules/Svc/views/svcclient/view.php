<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */

$this->title = '#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-client-view", 'title'=>Html::encode($this->title))) ?>

    <?php if($model->status == -1) { ?>
        <div class="alert alert-danger">Klient został usnięty</div>
    <?php }  ?>
       
    <?= $this->render('_view', [
        'model' => $model,
    ]) ?>

    <?php /* DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_user_fk',
            'id_country_fk',
            'id_province_fk',
            'city',
            'postal_code',
            'address',
            'pos_lat',
            'pos_lng',
            'firstname',
            'lastname',
            'email:email',
            'phone',
            'phone_additional:ntext',
            'website_url:url',
            'facebook_url:url',
            'facebook_config:ntext',
            'avatar_img',
            'sex',
            'birthday',
            'profession',
            'id_trade_fk',
            'id_education_fk',
            'note:ntext',
            'first_logged_date',
            'last_logged_date',
            'confirm',
            'status',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_at',
            'deleted_by',
        ],
    ])*/ ?>

<?= $this->endContent(); ?>
