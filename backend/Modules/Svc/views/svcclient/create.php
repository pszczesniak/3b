<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */

$this->title = Yii::t('app', 'Nowy klient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-client-create", 'title'=>Html::encode($this->title))) ?>


    <?= $this->render('_form', [
        'model' => $model, 'user' => $user
    ]) ?>

<?= $this->endContent(); ?>
