<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcUser */

$this->title = Yii::t('app', 'Create Svc User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'user' => $user
    ]) ?>

</div>
