<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Svc Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-service-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Svc Service'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_user_fk',
            'id_category_fk',
            'name',
            'name_lang:ntext',
            // 'describe:ntext',
            // 'describe_lang:ntext',
            // 'price',
            // 'price_lang:ntext',
            // 'is_sale',
            // 'price_sale',
            // 'price_sale_lang:ntext',
            // 'is_new',
            // 'is_recommended',
            // 'id_country_fk',
            // 'id_province_fk',
            // 'city',
            // 'postal_code',
            // 'address',
            // 'params:ntext',
            // 'status',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'deleted_at',
            // 'deleted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
