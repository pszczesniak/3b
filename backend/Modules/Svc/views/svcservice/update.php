<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcService */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Svc Service',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
