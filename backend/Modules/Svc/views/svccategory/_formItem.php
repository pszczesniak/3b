<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\Modules\Lsdd\models\LsddZoneStructure;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddMaterialFunction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grid-view-dicts-form">
    <?php $form = ActiveForm::begin(['id' => $model->isNewRecord?'modal-structure-create':'modal-structure-edit', 'options' => ['class' => 'modal-structure-create', 'data-target' => "#modal-grid-item", 'data-object' => '#modal-dropdown-function']]); ?>
        <div class="modal-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'id_parent_fk')->dropDownList( ArrayHelper::map(backend\Modules\Svc\models\SvcCategory::find()->where(['id_parent_fk' => 0, 'status'=>1])->all(), 'id', 'name'), array('prompt'=>'wybierz') )  ?>
            <?= $form->field($model, 'describe')->textarea(['rows' => 6]) ?>
        </div>
        <div class="modal-footer">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', ]) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>

