<?php
	use yii\helpers\Url;
?>

<ol <?php if($level == 0) echo 'id="menu-items-list"'; ?> class="dd-list"> 
	<?php
		foreach($items as $key=>$value) { 
            $counter = $value->counter;
            if($counter == 1) $label = ' oferta';
            else if($counter == 2 || $counter == 3 || $counter == 4) $label = ' oferty';
            else $label = ' ofert';
			echo $this->render('itemElement', [ 'title' => $value->name, 'type' => 1, 'counter' => $counter, 'labelType' => '[ '.$counter.$label.' ]' /*($value->id_parent_fk==0)?'Kategoria':'Podkategoria'*/, 'id' => $value->id, 'items' => $value->children, 'level' => $level ]); 	
		}
	?>
</ol>
