<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kategorie');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-category-index", 'title'=>'Kategorie')) ?>
	<div class="text-right dd-actions">
        <?= Html::a('<i class="fa fa-plus"></i>Nowa kategoria', Url::to(['/svc/svccategory/createitem']) , 
                ['class' => 'btn btn-success btn-icon gridViewModal', 
                 'id' => 'create-items',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "item-form", 
                 //'data-table' => "table-educations",
                 'data-title' => '<i class="fa fa-plus"></i>Nowa kategoria'
                ])  ?>  
		
		<?php /*Html::a('<i class="glyphicon glyphicon-floppy-disk"></i>'.Yii::t('lsdd', 'Save'), ['isave'], [
			'class' => 'btn btn-primary btn-icon', 'id' => 'save-structure',
			'data' => [ 'method' => 'post', ],
		])*/ ?>

	</div><div class="clear"></div>
	<hr />
	<div  class="dd dd-nodrag" data-alert="Przed wyjściem z arukusza zapisz zmiany!">
		<?= $buildStructure; ?>
	</div>
<?=  $this->endContent(); ?>

