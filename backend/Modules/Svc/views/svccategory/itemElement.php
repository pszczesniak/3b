<?php
	use yii\helpers\Url;
	if($type == 1) { $style = 'default'; $label = 'strona'; }
	if($type == 2) { $style = 'warning'; $label = 'kategoria'; }
	if($type == 3) { $style = 'info'; $label = 'link'; }
	if($type == 4) { $style = 'danger'; $label = 'separator'; }
	if($type == 5) { $style = 'success'; $label = 'galeria'; }
?>
<li class="dd-item dd3-item" data-id="<?= $id ?>">
	<!--<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>-->
	<div class="dd3-content">
		<span class="dd3-content-title"><?= $title ?></span>
		<div class="item-action-edit label-<?= $style ?>" id="item-<?= $id ?>"> 
			<p class="click-text">  <span class="type-name"><?= $labelType ?></span> <span class="arrow"></span> </p>
		</div>
	</div>
	<div class="item-edit" id="item-edit-<?= $id ?>">
		<div class="help-block-<?= $id ?>"></div>
        <form method="post" action="<?= Url::to(['/svc/svccategory/update', 'id' => $id]) ?>" id="structure-form-edit-<?= $id ?>" class="structure-form-edit">
			<div class="form-group field-lsddzonestructure-name required">
				<label for="lsddzonestructure-name" class="control-label">Tytuł</label>
				<input type="text" maxlength="255" name="SvcCategory[name]" class="form-control" id="uitem-<?= $id ?>" value="<?= $title ?>">
				<div class="help-block"></div>
			</div>

			<div class="form-group field-lsddzonestructure-item_describe">
				<label for="lsddzonestructure-item_describe" class="control-label">Opis</label>
				<textarea rows="3" name="SvcCategory[note]" class="form-control" id="lsddzonestructure-item_describe"></textarea>
				<div class="help-block"></div>
			</div>
			<div class="form-group align-right">
				<button class="btn btn-sm btn-primary" type="submit">Zapisz</button>   
				<?php if($counter == 0) { ?>
                    <a  title="Delete" href="<?= Url::to(['/svc/svccategory/delete', 'id' => $id]) ?>" id="d-<?= $id ?>" data-id="<?= $id ?>" class="deleteConfirm btn btn-sm btn-danger">Usuń</a>
                <?php } ?>
			</div>
		</form>

	</div>
	<?php if(count($items) > 0 ) {
		echo  $this->render('structure', [ 'items' => $items, 'level' => ($level+1) ]) ;
	} ?>
</li>