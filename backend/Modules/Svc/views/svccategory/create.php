<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcCategory */

$this->title = Yii::t('app', 'Create Svc Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
