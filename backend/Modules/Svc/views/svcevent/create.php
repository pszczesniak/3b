<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcEvent */

$this->title = Yii::t('app', 'Create Svc Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
