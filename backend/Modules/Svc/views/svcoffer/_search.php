<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcEvent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-svc-offers-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-offers']
    ]); ?>
    
    <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'firstname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-offers', 'data-form' => '#filter-svc-offers-search' ])->label('Imię/Nazwisko <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'city')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-offers', 'data-form' => '#filter-svc-offers-search' ])->label('Miasto <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'status')->dropDownList( [1 => 'aktywny', '-1' => 'usunięty'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-offers', 'data-form' => '#filter-svc-offers-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_package_fk')->dropDownList( ArrayHelper::map(backend\Modules\Svc\models\SvcPackage::find()->where(['status' => 1])->all(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-offers', 'data-form' => '#filter-svc-offers-search' ] ) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
