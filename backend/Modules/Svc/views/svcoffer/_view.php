<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="padding-md">
    <h4><?= $model->companyname ?></h4>
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <div class="row">
                <div class="col-xs-6 col-sm-12 col-md-6 text-center">
                    <a href="#">
                        <img class="img-thumbnail" alt="User Avatar" src="<?= (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? "/../uploads/avatars/thumb/avatar-".$model->id_user_fk.".png" : "/../images/default-user.png" ?>">
                    </a>
                    <div class="seperator"></div>
                    <div class="seperator"></div>
                </div><!-- /.col -->
                <div class="col-xs-6 col-sm-12 col-md-6">
                    <?= Html::a('<i class="fa fa-pencil"></i>'.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-primary']) ?>
                    <div class="seperator"></div>
                    <span class="btn btn-info btn-xs btn-icon m-bottom-sm"><i class="fa fa-phone" aria-hidden="true"></i><?= (($model->phone) ? $model->phone : 'brak numeru') ?></span>
                    <div class="seperator"></div>
                </div><!-- /.col -->
            </div><!-- /.row -->
            
            <div class="separator"></div>
            
            <div class="panel">
                <div class="panel-tab clearfix">
                    <ul class="tab-bar bg-primary">
                        <li class="active"><a data-toggle="tab" href="#config"><i class="fa fa-cog"></i> </a></li>
                        <li><a data-toggle="tab" href="#address"><i class="fa fa-map-marker"></i> </a></li>
                        <!--<li><a data-toggle="tab" href="#comment"><i class="fa fa-comment"></i> </a></li>-->
                    </ul>
                </div>
            <div class="tab-content">
                    <div id="config" class="tab-pane fade in active">
                        <h5 class="headline">  Obsługiwane imprezy   <span class="line"></span>   </h5>
                        <ul class="list-group">
                            <?php
                                if($model->events && count($model->events) > 0) {
                                    foreach($model->events as $key=>$value) {
                                        echo '<li class="list-group-item">'.$value->event['name'].'</li>';
                                    }
                                } else {
                                    echo '<li class="list-group-item warning">brak danych</li>';
                                }
                            ?>
                        </ul><!-- /list-group -->	
                        
                        <h5 class="headline">  Przypisane kategorie  <span class="line"></span> </h5>
                        <ul class="list-group">
                            <?php
                                $checkedCategories = [];
                                foreach($categories = $model->categories as $key=>$value) {
                                    $checkedCategories[$value->id_category_fk] = $value->id_category_fk;
                                }
                                foreach($categories as $key=>$value) {
                                    if($value->category['id_parent_fk'] == 0)
                                        echo '<li class="list-group-item">'.$value->category['name'].'</li>';
                                        
                                    if( $children = $value->category['children']) {
                                        foreach($children as $chi => $child) {
                                            if(in_array($child->id, $checkedCategories))
                                                echo '<li class="list-group-item align-right">'.$child->name.'</li>';
                                        }
                                    }
                                }
                            ?>
                        </ul><!-- /list-group -->	
                                                    
                    </div>
                
                    <div id="address" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item"><?= $model->province['name'] ?></li>
                            <li class="list-group-item"><?= $model->city ?></li>
                            <li class="list-group-item"><?= $model->address ?></li>
                        </ul> 							
                    </div>
                    <!--<div id="comment" class="tab-pane fade">
                        <div class="chat">
                            <ul class="chat-list">
                                <?php
                                    for($i=0;$i<3;++$i) {
                                        echo '<li class="left clearfix">'
                                                .'<span class="chat-img pull-left">'
                                                    .'<img src="/../images/default-user.png" alt="User Avatar">'
                                                .'</span>'
                                                .'<div class="chat-body clearfix">'
                                                    .'<div class="header">'
                                                        .'<strong class="primary-font">Klient</strong>'
                                                        .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 12 mins ago</small>'
                                                    .' </div>'
                                                    .'<p>'
                                                        .'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                                                    .'</p>'
                                                .'</div>'
                                            .'</li>'
                                            .'<li class="right clearfix">'
                                                .'<span class="chat-img pull-right">'
                                                    .'<img src="/../images/default-user.png" alt="User Avatar">'
                                                .'</span>'
                                                .'<div class="chat-body clearfix">'
                                                .'<div class="header">'
                                                    .'<strong class="primary-font">Usługodawca</strong>'
                                                    .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>'
                                                .'</div>'
                                                    .'<p>'
                                                        .'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. '
                                                    .'</p>'
                                                .'</div>'
                                            .'</li>';
                                    }
                                ?>
                            </ul>
                        </div>                   
                    </div> end comment -->
                </div><!-- /tab-content -->
            </div>
            
            
        </div><!-- /.col -->
        <div class="col-md-9 col-sm-9">
            <div class="row">         
                <div class="col-md-6">                  
                    <section class="panel panel-info portlet-item">
                        <header class="panel-heading"> Identyfikacja w sieci </header>
                        <div class="list-group bg-white">
                            <?php
                                if($model->website_url) {
                                    $url = ( (substr($model->website_url, 0, 4) == 'http') ? $model->website_url : 'http://'.$model->website_url);
                                } else {
                                    $url = "#";
                                }
                            ?>
                            <a href="#" class="list-group-item"> <i class="fa fa-fw fa-envelope"></i> <?= $model->email ?> </a> 
                            <a href="<?= $url ?>" class="list-group-item"> <i class="fa fa-fw fa-globe"></i> <?= (($model->website_url) ? ($model->website_url) : 'brak adresu strony') ?> </a> 
                            <a href="<?= (($model->facebook_url) ? $model->facebook_url : '#') ?>"" class="list-group-item"> <i class="fa fa-fw fa-facebook"></i> <?= (($model->facebook_url) ? ($model->facebook_url) : 'brak adresu strony') ?> </a> 
                        </div>
                    </section>
                    <div class="panel panel-white">
                        <div class="panel-heading">  Opis skrócony  </div>
                        <div class="panel-body">  
                            <?= $model->brief ?>  
                            <br /><br />
                            <div class="fb-publish-result"></div>
                            <?php 
                                $accessToken = \Yii::$app->session->get('fb.token');
                        
                                if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
                                    echo '<button class="btn btn-xs btn-primary fb-publish" data-action="'.Url::to(['/svc/svcoffer/publish', 'id' => $model->id]).'">Opublikuj na FB</button>';
                                }
                            ?>
                        </div>
                    </div><!-- /panel -->
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="panel panel-overview">
                        <?php $colors = [1 => 'info', 2 => 'success', 3 => 'danger'] ?>
                        <div class="overview-icon bg-<?= $colors[$model->id_package_fk] ?>-brand">
                            <i class="fa fa-cog"></i>
                        </div>
                        <div class="overview-value">
                            <div class="h2"><?= $model->package['price_label'] ?></div>
                            <div class="text-muted"><?= $model->package['name'] ?></div>
                        </div>
                    </div><!--/ panel -->
                    <div class="panel panel-overview">
                        <div class="overview-icon bg-warning-brand">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="overview-value">
                            <div class="h2">0</div>
                            <div class="text-muted">Zapytań integratora</div>
                        </div>
                    </div><!--/ panel -->
                </div><!-- /.col -->
                <div class="col-xs-12">
                    <div class="panel panel-white">
                        <div class="panel-heading">  Opis  </div>
                        <div class="panel-body">  
                            <?= $model->note ?>  
                           
                        </div>
                    </div><!-- /panel -->  
                </div>
            </div><!-- /.row -->
                
            <div class="row">
                <div class="col-xs-12">
                    <ul class="tab-bar grey-tab">
                        <li class="active">
                            <a data-toggle="tab" href="#image">
                                <span class="block text-center">
                                    <i class="fa fa-file-image-o fa-2x"></i> 
                                </span>
                                Galeria
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#video">
                                <span class="block text-center">
                                    <i class="fa fa-file-video-o fa-2x"></i> 
                                </span>
                                Video
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#audio">
                                <span class="block text-center">
                                    <i class="fa fa-file-audio-o fa-2x"></i> 
                                </span>	
                                Audio
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="image" class="tab-pane fade active in">
                            <?php 
                                if( count($model->images) == 0 ) {
                                    $imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg':'/images/no-image-big.jpg';
                                    echo '<ul id="imageGallery">'
                                            .'<li data-thumb="'.$imgSrc.'" data-src="'.$imgSrc.'">'
                                                .'<img src="'.$imgSrc.'" />'
                                            .'</li>'
                                       .'</ul>';
                                } else { 
                                    echo '<ul id="imageGallery">';
                                            foreach($model->images as $image) {
                                                echo '<li data-thumb="/uploads/thumbs/'.$image->systemname_file.'.'.$image->extension_file.'" data-src="/uploads/'.$image->systemname_file.'.'.$image->extension_file.'">'
                                                        .'<img src="/uploads/'.$image->systemname_file.'.'.$image->extension_file.'" />'
                                                    .'</li>';
                                            }
                                    echo '</ul>';
                                }
                            ?>
                        </div>
                        <div id="video" class="tab-pane">
                            <?php 
                                if( count($model->videos) == 0 ) {
                                    echo '<div class="alert">Brak materiałów video</div>';
                                } else {
                                    echo '<figure id="video_player">';
                                        echo '<video controls >';
                                            foreach($model->videos as $video) {
                                                echo '<source src="/uploads/'.$video->systemname_file.'.'.$video->extension_file.'" type="video/'.$video->extension_file.'">';
                                            }
                                            //<source src="http://html5videoformatconverter.com/data/images/happyfit2.mp4" type="video/mp4">
                                            //<source src="http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4" type="video/mp4">
                                        echo '</video>';
                                        echo '<figcaption>';
                                            foreach($model->videos as $video) {
                                                echo '<a href="/uploads/'.$video->systemname_file.'.'.$video->extension_file.'"><span>'.$video->title_file.'</span></a>';
                                            }
                                            //<a href="http://html5videoformatconverter.com/data/images/happyfit2.mp4"><span>Film 1</span></a>
                                            //<a href="http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4"><span>Film 2</span></a>
                                        echo '</figcaption>';
                                    echo '</figure>';
                                }
                            ?>
                        </div>
                        <div id="audio" class="tab-pane">
                            <?php 
                                if( count($model->audios) == 0 ) {
                                    echo '<div class="alert">Brak materiałów video</div>';
                                } else {
                                    echo '<figure id="audio_player">';
                                        foreach($model->audios as $audio) {
                                            echo '<h3 class="file-name">'.$audio->title_file.'</h3>';
                                            echo '<audio controls >';
                                                echo '<source src="/uploads/'.$audio->systemname_file.'.'.$audio->extension_file.'" type="audio/'.$audio->extension_file.'">';               
                                            echo '</audio>';
                                        }
                                      
                                    echo '</figure>';
                                }
                            ?>

                        </div>
                    </div>
                </div>
            </div>    

        </div><!-- /.col -->
    </div><!-- /.row -->			
</div>