<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Oferta #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rejestr ofert', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id.'-big.jpg':'/images/no-image-big.jpg';
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->companyname]);
\Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => ($model->brief) ? $model->brief : trim(preg_replace('/\s\s+/', ' ', $model->note)) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:og_url', 'content' => \Yii::$app->urlManagerFrontEnd->createUrl('oferta/'.\common\components\CustomHelpers::encode($model->id).'/'.$model->slug) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://twojeprzyjecia.pl'.$imgSrc ]);

?>
<?php if($model->status == -1) { ?>
    <div class="alert alert-danger">Oferta została usnięta</div>
<?php }  else { ?>
    <br />
    <p class="align-right">
        <?= ($model->confirm == -1) ? Html::a(Yii::t('app', 'Wyślij informację'), ['send', 'id' => $model->id], ['class' => 'btn btn-info']) : '' ?>
        <?= ($model->confirm == 0) ? Html::a(Yii::t('app', 'Wyślij ponownie informację'), ['send', 'id' => $model->id], ['class' => 'btn btn-info']) : '' ?>
        <?= ($model->confirm == 0) ? '<span class="btn btn-warning">Oczekiwanie na decyzję</span>&nbsp;'.Html::a(Yii::t('app', 'Rezygnacja mailowa'), ['resignation', 'id' => $model->id], ['class' => 'btn btn-danger']) : '' ?>
        <?= ($model->confirm == -2) ? '<span class="btn btn-danger">Rezygnacja ze strony</span>' : '' ?>
        <?= ($model->confirm == -3) ? '<span class="btn btn-danger">Rezygnacja przez email</span>' : '' ?>
        <!--<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>-->
    </p>
<?php } ?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-view", 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
  
    <?= $this->render('_view', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>