<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = Yii::t('app', 'Nowa oferta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Oferty'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-create", 'title'=>Html::encode($this->title))) ?>
    <?php 
        /*if($errors) {
            var_dump($errors);
        }*/
    ?>
        
    <?= $this->render('_form', [
        'model' => $model, 'categories' => $categories, 'categoriesChecked' => $categoriesChecked, 'errors' => $errors
    ]) ?>

<?= $this->endContent(); ?>
