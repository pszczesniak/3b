<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Structurewidget;
use backend\widgets\GoogleMapWidget;
use common\components\CustomHelpers;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= ($model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <?php if(isset($errors) && !empty($errors) ) { ?>
        <div class="alert alert-danger"> <ul>
            <?php foreach($errors as $key => $value) { echo '<li>'.$value[0].'</li>'; } ?>
        </ul></div>
    <?php } ?>
    
    <div class="row">
        <div class="col-md-6">
            <fieldset><legend>Dane autoryzacyjne</legend>
                <?= $form->field($model, 'companyname')->textInput(['maxlength' => true]) ?>
                <div class="row">
                    <!--<div class="col-xs-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true, 'disabled'=>( ($model->isNewRecord) ? false : true )])->label('Login') ?></div>-->
                    <div class="col-xs-12"> <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled'=>( ($model->isNewRecord || $model->confirm <= 0) ? false : true )])->label('Email / Login użytkownika') ?> </div>
                </div>
            </fieldset>
            <fieldset><legend>Dane personalne i kontaktowe</legend>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone_additional')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </fieldset>
            <fieldset><legend>Pakiet </legend>
                <?= $form->field($model, 'id_package_fk')->dropDownList( ArrayHelper::map(backend\Modules\Svc\models\SvcPackage::find()->where(['status' => 1])->all(), 'id', 'name'), array() )->label(false)  ?>                
            </fieldset>
            <fieldset><legend>Skrócony opis</legend>
                <?= $form->field($model, 'brief')->textarea(['rows' => 2])->label(false) ?>
            </fieldset>
            <?php if(!$model->isNewRecord) { ?>
            <fieldset><legend>Opis</legend>
                <?= $form->field($model, 'note')->textarea(['rows' => 8])->label(false) ?>
            </fieldset>
            <?php } ?>
        </div>
        <div class="col-md-6">
            <?php if(!$model->isNewRecord) { ?>
     
                <div id="upload-demo"></div>
                <div class="imageAttachment" id="upload-demo-id" data-id="<?= (is_file(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? $model->id_user_fk : 0 ?>">
                    <div class="btn-toolbar actions-bar">
                        <span class="btn btn-success btn-file">
                            <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                            <span data-replace-text="Zamień..." data-upload-text="Prześlij" class="file_label">Pobierz zdjęcie</span>
                            <input type="file" id="upload" name="Files[avatar]" value="Choose a file" data-path="<?= \Yii::$app->urlManagerFrontEnd->createUrl('/offer/upload?id='.CustomHelpers::encode($model->id_user_fk)) ?>">
                        </span>
                        <button class="btn btn-success upload-result" data-path="<?= \Yii::$app->urlManagerFrontEnd->createUrl('/offer/saveavatar?id='.CustomHelpers::encode($model->id_user_fk))  ?>">Zapisz avatar</button>
                        <!--<button class="btn btn-success change-view">Ustaw miniaturę</button>-->
                    </div>  
                </div>
            <?php } else { ?>
                <fieldset><legend>Opis</legend>
                    <?= $form->field($model, 'note')->textarea(['rows' => 8])->label(false) ?>
                </fieldset>
            <?php } ?>

            <fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'events_temp')->dropDownList( ArrayHelper::map(backend\Modules\Svc\models\SvcEvent::find()->where(['status' => 1])->orderby('name')->all(), 'id', 'name'), array('class' => 'ms-select', 'multiple' => 'multiple') )  ?>
                <?php
                    $counter = count($categories)/3; 
                    $tablica = explode('.', $counter); 
                    if( !isset($tablica[1]) ) {
                        $iter = (int) $tablica[0]; 
                    } else {
                        if((int) $tablica[1] == 0 ) $iter = (int) $tablica[0]; else $iter = (int) $tablica[0] + 1;  
                    }
                    $i = 1; $iterTmp = $iter;
                    foreach($categories as $key => $value) {
                        if($iterTmp == $iter) {
                            echo '<div class="col-md-4 col-sm-12">';
                        } 
                        $checked = '';
                        if(in_array($value->id, $categoriesChecked)) {
                            $checked = 'checked="checked"';
                        }
                        echo '<input type="checkbox" value="'.$value->id.'" name="Categories[item-'.$value->id.']" id="category-'.$value->id.'" class="form__checkbox" '.$checked.'>'
                            .'<label for="category-'.$value->id.'" class="form__checkbox-label">'.$value->name;
                            if($children = $value->children) {
                                echo '<div class="form__checkbox-label-subcategories">';
                                    foreach ($children as $chi=>$child) {
                                        $sub_checked = '';
                                        if(in_array($child->id, $categoriesChecked)) {
                                            $sub_checked = 'checked="checked"';
                                        }
                                        echo '<input type="checkbox" value="'.$child->id.'" name="Categories[item-'.$child->id.']" id="category-'.$child->id.'" class="form__checkbox" '.$sub_checked.'>'
                                            .'<label for="category-'.$child->id.'" class="form__checkbox-label">'.$child->name.'</label><br />';
                                    }
                                echo '</div>';
                            } /*else
                                echo '<div class="form__checkbox-label-subcategories">brak podkategorii</div>';*/
                            echo '</label>';
                        if($iterTmp == 1 || $i == count($categories)) {
                            echo '</div>';
                            $iterTmp = $iter;
                        } else {
                            echo '<br />';
                            $iterTmp = $iterTmp - 1;
                        }
                         $i = $i + 1;
                    }
                ?>
            </fieldset>
        </div>
    </div>
    <fieldset><legend>Lokalizacja</legend>
        <div class="row">
            <div class="col-sm-8">
                <?= $form->field($model, 'id_province_fk')->dropDownList( ArrayHelper::map(backend\Modules\Loc\models\LocProvince::find()->where(['status' => 1])->orderby('name')->all(), 'id', 'name'), array() )  ?>                
                <div class="row">
                    <div class="col-xs-8"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-xs-4"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
                </div>
        
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                <?php if($model->id_package_fk > 1) { ?>
 
                    <?= $form->field($model, 'province_additional', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList(ArrayHelper::map( backend\Modules\Loc\models\LocProvince::find()->orderby('name')->all(), 'id', 'name'), ['class' => 'province_additional', 'multiple' => 'multiple', 'data-amount' => ($model->id_package_fk == 2) ? 4 : 16]); ?>
                    <?php if($model->id_package_fk == 2) { ?><p class="text-brand"><i class="fa fa-info-circle"></i> Możesz wybrać 4 dodatkowe województwa</p> <?php } ?>

                <?php } ?>
            </div>
            <div class="col-sm-4">
                <div class="google-map">
                    <?= GoogleMapWidget::widget(['model' => $model]) ?>
                    <?= $form->field($model, 'pos_lat', ['template' => '{input}{error}'])->hiddenInput()->label(false) ?> <?= $form->field($model, 'pos_lng', ['template' => '{input}'])->hiddenInput()->label(false) ?>
                </div>
            </div>
        </div>
    </fieldset>
    <!--
    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_url')->textInput(['maxlength' => true]) ?>
    -->


    <div class="form-group align-right">
        <?= ($model->status != -1) ? Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


