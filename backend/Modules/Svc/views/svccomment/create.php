<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcComment */

$this->title = Yii::t('app', 'Create Svc Comment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-comment-create", 'title'=>Html::encode($this->title))) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
