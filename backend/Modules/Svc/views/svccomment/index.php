<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Komentarze do ofert');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-comment-index", 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 1]); ?>
    
    <div id="toolbar-comments" class="btn-group toolbar-table-widget">
        <button class="btn btn-default btn-refresh-table" type="button" title="Od�wie�" data-table="#table-comments"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-comments"

            data-toolbar="#toolbar-comments" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-svc-comments-search"
            data-url=<?= Url::to(['/svc/svccomment/data', 'type' => 1]) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="offer"  data-sortable="true" >Oferta</th>
                <th data-field="client"  data-sortable="true" >Klient</th>
                 <th data-field="comment"  data-sortable="true" >Komentarz</th>
                <th data-field="rank"  data-sortable="true" data-align="center">Ocena</th>
                <th data-field="status"  data-sortable="true" >Status</th>
                <th data-field="created"  data-sortable="true" >Utworzono</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>
    
<?= $this->endContent(); ?>
