<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%svc_comment}}".
 *
 * @property integer $id
 * @property integer $id_comment_fk
 * @property integer $id_offer_fk
 * @property integer $id_user_fk
 * @property integer $is_owner
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property string $comment_arch
 * @property integer $rank
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcComment extends \yii\db\ActiveRecord
{
    public $ranking = 0;
    public $type1;
    public $id_query_fk = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_comment_fk', 'id_offer_fk', 'id_user_fk', 'is_owner', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_offer_fk', 'name', 'email', 'comment', 'rank'], 'required'],
            [['email'],'email'],
            [['comment', 'comment_arch', 'deleted_comment'], 'string'],
            [['deleted_comment'], 'required', 'on' => 'delete'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_comment_fk' => Yii::t('app', 'Id Comment Fk'),
            'id_offer_fk' => Yii::t('app', 'Oferta'),
            'id_client_fk' => Yii::t('app', 'Klient'),
            'is_owner' => Yii::t('app', 'Is Owner'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'comment' => Yii::t('app', 'Comment'),
            'comment_arch' => Yii::t('app', 'Comment Arch'),
            'rank' => Yii::t('app', 'Ocena'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
     public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
                if($this->id_offer_fk != 0  && \backend\Modules\Svc\models\SvcOffer::findOne($this->id_offer_fk)->id_package_fk != 3) {
                    $this->status = 2;
                } else {
                    $this->status = 1;
                }
			} else { 
				/*$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
                if($this->status == 0) {
                    $this->deleted_at = date('Y-m-d H:i:s');
                    $this->deleted_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
                }
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getOffer()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcOffer::className(), ['id' => 'id_offer_fk']);
    }
    
    public function getClient()
    {
		return (!$this->id_client_fk || $this->id_client_fk == 0 ) ? false : $this->hasOne(\backend\Modules\Svc\models\SvcClient::className(), ['id' => 'id_client_fk']);
    }
    
    public function getChildren()  {
		$items = $this->hasMany(self::className(), ['id_comment_fk' => 'id'])->where(['status' => '2']); 
		if(count($items) > 0)
			return $items;
		else
			return false;
    }
    
    public function getOwnercomments()  {
		$items = $this->hasMany(self::className(), ['id_comment_fk' => 'id'])->where(['status' => '2', 'is_owner' => 1]); 
		if(count($items) > 0)
			return $items;
		else
			return false;
    }
}
