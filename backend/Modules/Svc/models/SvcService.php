<?php

namespace app\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "svc_service".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $id_category_fk
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property string $describe_lang
 * @property double $price
 * @property string $price_lang
 * @property integer $is_sale
 * @property double $price_sale
 * @property string $price_sale_lang
 * @property integer $is_new
 * @property integer $is_recommended
 * @property integer $id_country_fk
 * @property integer $id_province_fk
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $params
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'svc_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'created_by'], 'required'],
            [['id_user_fk', 'id_category_fk', 'is_sale', 'is_new', 'is_recommended', 'id_country_fk', 'id_province_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name_lang', 'describe', 'describe_lang', 'price_lang', 'price_sale_lang', 'params'], 'string'],
            [['price', 'price_sale'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'city', 'postal_code', 'address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_category_fk' => Yii::t('app', 'Id Category Fk'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
            'price' => Yii::t('app', 'Price'),
            'price_lang' => Yii::t('app', 'Price Lang'),
            'is_sale' => Yii::t('app', 'Is Sale'),
            'price_sale' => Yii::t('app', 'Price Sale'),
            'price_sale_lang' => Yii::t('app', 'Price Sale Lang'),
            'is_new' => Yii::t('app', 'Is New'),
            'is_recommended' => Yii::t('app', 'Is Recommended'),
            'id_country_fk' => Yii::t('app', 'Id Country Fk'),
            'id_province_fk' => Yii::t('app', 'Id Province Fk'),
            'city' => Yii::t('app', 'City'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'address' => Yii::t('app', 'Address'),
            'params' => Yii::t('app', 'Params'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }

    /**
     * @inheritdoc
     * @return SvcServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcServiceQuery(get_called_class());
    }
}
