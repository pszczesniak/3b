<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\Expression;

use backend\Modules\Newsletter\models\NewsletterSubscribe;


/**
 * This is the model class for table "{{%svc_offer}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $id_package_fk
 * @property integer $id_country_fk
 * @property integer $id_province_fk
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property double $pos_lat
 * @property double $pos_lng
 * @property string $companyname
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $phone_additional
 * @property string $email
 * @property string $website_url
 * @property string $facebook_url
 * @property string $note
 * @property string $first_logged_date
 * @property string $last_logged_date
 * @property string $facebook_config
 * @property integer $confirm
 * @property integer $confirm_payment
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcOffer extends \yii\db\ActiveRecord
{
    public $counter;
	public $items;
    public $events_temp = [];
    public $categories_temp = [];
    public $rule;
    public $ranking;
    public $stats = false;
    public $username;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_offer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'id_package_fk'], 'required'],
            [['id_user_fk', 'email', 'companyname', 'note'], 'required'],
             [['email'],'unique'],
           // [['rule'], 'required', 'message' => 'Aby umieścić swoją ofertę, musisz zaakceptować Nasz regulamin.'],
            [['id_user_fk', 'id_package_fk', 'id_country_fk', 'id_province_fk', 'confirm', 'confirm_payment', 'status', 'created_by', 'updated_by', 'deleted_by', 'guests_from', 'guests_to'], 'integer'],
            [['pos_lat', 'pos_lng', 'stats_click', 'stats_click_www'], 'number'],
            [['price_from', 'price_to', 'value_promotion'], 'double'],
            [['phone_additional', 'note', 'facebook_config', 'avatar_img', 'calendar_busy_days','brief','text_promotion', 'username','slug'], 'string'],
            [['first_logged_date', 'last_logged_date', 'created_at', 'updated_at', 'deleted_at', 'avatar_img', 'guests_from', 'guests_to', 'price_from', 'price_to', 'price_per_person', 'province_additional', 'stats', 'expiration_date','brief', 'is_promotion', 'date_payment','events_temp'], 'safe'],
            [['city', 'postal_code', 'address', 'companyname', 'firstname', 'lastname', 'phone', 'email', 'website_url', 'facebook_url'], 'string', 'max' => 255],
            /*['rule', 'required', 'when' => function($model) {
				return ($model->rules == 1);
			}],*/
            //['age', 'compare', 'compareValue' => 30, 'operator' => '>='],
            ['price_to', 'compare', 'compareAttribute' => 'price_from', 'operator' => '>=', 'message' => "Cena 'do' musi byc wyższa od ceny 'od'"],
            ['guests_to', 'compare', 'compareAttribute' => 'guests_from', 'operator' => '>=', 'message' => "Liczba gości 'do' musi byc wyższa od liczby gości 'od'"],
            //[['rule'], 'boolean'/*, 'strict' => true*/],
            //['rule', 'required', 'requiredValue' => 'on', 'message' => 'Aby zamieścić swoja ofertę, musisz zaakceptować Nasz regulamin.'],
            ['brief', 'string', 'max' => 200],
            ['note', 'string', 'max' => 1000, 'when' => function($model) {
				return ($model->id_package_fk == 1 ) ? true : false ;
			}],
            ['email', 'validatEuser'],
           /* ['username', 'required',  'when' => function($model) {
				return ($model->confirm == -1 ) ? true : false ;
			}],*/
          //  ['province_additional', 'validateProvince'],
     
			/*['items', 'required', 'when' => function($model) {
				return ($model->counter == 0 && $model->isNewRecord);
			}, */
        ];
    }
    
    public function validateProvince($attribute, $params)
    {
       // if ( count(explode(',', $this->$attribute)) > 4 ) {
            $this->addError($attribute, 'Możesz wybrać jedynie 4 dodatkowe województwa');
        //}
    }
    
    public function validatEuser($attribute, $params)
    {
       $exist = \common\models\User::find()->where(['email' => $this->email])->andWhere("id != ".$this->id_user_fk)->count();
        if ( $exist > 0 ) {
            $this->addError($attribute, 'Użytkownik o podanym e-mailu już istnieje');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Użytkownik'),
            'id_package_fk' => Yii::t('app', 'Pakiet'),
            'id_country_fk' => Yii::t('app', 'Id Country Fk'),
            'id_province_fk' => Yii::t('app', 'Województwo'),
            'city' => Yii::t('app', 'Miasto'),
            'postal_code' => Yii::t('app', 'Kod pocztowy'),
            'address' => Yii::t('app', 'Adres'),
            'pos_lat' => Yii::t('app', 'Pos Lat'),
            'pos_lng' => Yii::t('app', 'Pos Lng'),
            'companyname' => Yii::t('app', 'Nazwa firmy'),
            'firstname' => Yii::t('app', 'Imię'),
            'lastname' => Yii::t('app', 'Nazwisko'),
            'phone' => Yii::t('app', 'Telefon'),
            'phone_additional' => Yii::t('app', 'Dodatkowy telefon'),
            'email' => Yii::t('app', 'Email'),
            'website_url' => Yii::t('app', 'Website Url'),
            'facebook_url' => Yii::t('app', 'Facebook Url'),
            'note' => Yii::t('app', 'Opis'),
            'brief' => Yii::t('app', 'Skrót opisu'),
            'first_logged_date' => Yii::t('app', 'First Logged Date'),
            'last_logged_date' => Yii::t('app', 'Last Logged Date'),
            'facebook_config' => Yii::t('app', 'Facebook Config'),
            'confirm' => Yii::t('app', 'Confirm'),
            'confirm_payment' => Yii::t('app', 'Confirm Payment'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'events_temp' => 'Obsługiwane imprezy',
            'categories_temp' => 'Kategorie',
            'expiration_date' => 'Data wygaśnięcia ważności konta'
        ];
    }
	
	public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
		
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			    if(!$this->brief) $this->brief = implode(' ', array_slice(explode(' ', strip_tags($this->note)), 0, 30)).' [...]';
                
            }/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
        if($result) {
            $user = \common\models\User::findOne($this->id_user_fk);
            $user->status = 0;
            $user->save();
        }
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
            "slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'companyname',
				'slugAttribute' => 'slug',
			],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'SvcOffer',
				// image dimmentions for preview in widget 
				'previewHeight' => 100,
				'previewWidth' => 100,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/images/account/cover',
				'url' => Yii::getAlias('@web') . '/images/account/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(100));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function getUser()
    {
		return $this->hasOne(\common\models\User::className(), ['id' => 'id_user_fk']);
    }
	
	public function getEvents()
    {
		 $items = $this->hasMany(\backend\Modules\Svc\models\SvcOfferEvent::className(), ['id_offer_fk' => 'id'])->joinWith('event')->andWhere(['{{%svc_offer_event}}.status' => 1]); 
		//$items = $this->hasMany(\backend\Modules\Svc\models\SvcOfferEvent::className(), ['id_offer_fk' => 'id']);
		return $items;
    }
    
    public function getProvince()
    {
		return $this->hasOne(\backend\Modules\Loc\models\LocProvince::className(), ['id' => 'id_province_fk']);
    }
    
    public function getPackage()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcPackage::className(), ['id' => 'id_package_fk']);
    }
	
	public function getCategories()
    {
		$items = $this->hasMany(\backend\Modules\Svc\models\SvcOfferCategory::className(), ['id_offer_fk' => 'id']); 
		
		return $items;
    }
    
    public function getFullname() {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function getAvatar()  {
        //return \Yii::$app->request->BaseUrl.'/<path to image>/'.$this->logo;
        $url = file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->id.".png") ? "/../uploads/avatars/thumb/avatar-".$model->id.".png" : "/../images/default-user.png";
        return $url;
    } 
    
    public function getComments()
    {
		//$items = $this->hasMany(\backend\Modules\Svc\models\SvcComment::className(), ['id_offer_fk' => 'id']); 
        $items = \backend\Modules\Svc\models\SvcComment::find()->where(['id_offer_fk' => $this->id, 'id_comment_fk' => 0, 'status' => 2])->all();
		
		return $items;
    }
    
    public function getRank() {
        return \backend\Modules\Svc\models\SvcComment::find()->where(['id_offer_fk' => $this->id, 'status' => 2])->select('sum(rank)/count(*) as ranking')->one()['ranking'];

    }
    
    public function getImages() {
		//return $this->hasMany(\common\models\Files::className(), ['id_fk' => 'id', 'id_type_file_fk' => 1]); 
        return \common\models\Files::find()->where(['id_fk' => $this->id, 'id_type_file_fk' => 1, 'status' => '1'])->all();
	}
    
    public function getVideos() {
		return \common\models\Files::find()->where(['id_fk' => $this->id, 'id_type_file_fk' => 2, 'status' => '1'])->all();
	}
    
    public function getAudios() {
		return \common\models\Files::find()->where(['id_fk' => $this->id, 'id_type_file_fk' => 3, 'status' => '1'])->all();
	}
    
    public function getSubscribe() {
        $items = $this->hasMany(\backend\Modules\Newsletter\models\NewsletterGroupSubscribe::className(), ['email' => 'email']); 
		
		return $items;
    }
    
    public function getAmountmsg() {
        return \backend\Modules\Svc\models\SvcMessage::find()->where(['id_offer_fk' => $this->id])->andWhere('status >= 1')->count();
    }
    
    public function getAmountcmt() {
        return \backend\Modules\Svc\models\SvcComment::find()->where(['id_offer_fk'=>$this->id, 'status' => 2])->count();
    }
    
    public function getAmountcmttoaccept() {
        return \backend\Modules\Svc\models\SvcComment::find()->where(['id_offer_fk'=>$this->id, 'status' => 1])->count();
    }
    
    public function getNotifications() {
        $msg = \backend\Modules\Svc\models\SvcMessage::find()->where(['id_offer_fk' => $this->id])->andWhere('status = 1')->count();
        $cmt = \backend\Modules\Svc\models\SvcComment::find()->where(['id_offer_fk' => $this->id])->andWhere('status = 1')->count();
        
        return ($msg + $cmt);
    }
    
    public function getResignation() {
        return \backend\Modules\Svc\models\SvcOfferChanges::find()->where(['id_offer_fk' => $this->id, 'id_new_package_fk' => 1, 'is_changed' => 0])->one();

    }
    
    public static function getStates() {
        $states = [];
        
        return $states;
    }
}
