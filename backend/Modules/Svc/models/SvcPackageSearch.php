<?php

namespace app\Modules\Svc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Svc\models\SvcPackage;

/**
 * SvcPackageSearch represents the model behind the search form about `app\Modules\Svc\models\SvcPackage`.
 */
class SvcPackageSearch extends SvcPackage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_sale', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'note', 'price_lang', 'price_sale_lang', 'warning', 'options', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['price', 'price_sale'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcPackage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'is_sale' => $this->is_sale,
            'price_sale' => $this->price_sale,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'price_lang', $this->price_lang])
            ->andFilterWhere(['like', 'price_sale_lang', $this->price_sale_lang])
            ->andFilterWhere(['like', 'warning', $this->warning])
            ->andFilterWhere(['like', 'options', $this->options]);

        return $dataProvider;
    }
}
