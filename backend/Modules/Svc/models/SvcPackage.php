<?php

namespace backend\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_package}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $note
 * @property double $price
 * @property string $price_lang
 * @property integer $is_sale
 * @property double $price_sale
 * @property string $price_sale_lang
 * @property string $warning
 * @property string $options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_package}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['note', 'price_lang', 'price_label', 'price_sale_lang', 'price_sale_label', 'warning', 'options'], 'string'],
            [['price', 'price_sale'], 'number'],
            [['is_sale', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['price_sale'], 'required', 'when' => function($model) { return ($model->is_sale == 1) ? true : false; } ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'note' => Yii::t('app', 'Opis'),
            'price' => Yii::t('app', 'Cena'),
            'price_label' => Yii::t('app', 'Etykieta'),
            'price_lang' => Yii::t('app', 'Price Lang'),
            'is_sale' => Yii::t('app', 'Oznacz pakiet jako promocyjny'),
            'price_sale' => Yii::t('app', 'Cena promocyjna'),
            //'price_sale_label' => Yii::t('app', 'Cena promocyjna[opis, np: 35 PLN / rok'),
            'price_sale_lang' => Yii::t('app', 'Price Sale Lang'),
            'warning' => Yii::t('app', 'Warning'),
            'options' => Yii::t('app', 'Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public static function getList() {
        return  SvcPackage::find()->all();
    }

}
