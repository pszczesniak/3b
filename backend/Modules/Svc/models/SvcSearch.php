<?php

namespace backend\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_search}}".
 *
 * @property integer $id
 * @property string $offers
 * @property string $query
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_search}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offers', 'query','order_by'], 'string'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at','order_by'], 'safe'],
            [['request_remote_addr', 'request_user_agent', 'request_content_type'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'offers' => Yii::t('app', 'Offers'),
            'query' => Yii::t('app', 'Query'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
