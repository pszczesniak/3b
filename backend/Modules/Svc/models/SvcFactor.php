<?php

namespace app\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_factor}}".
 *
 * @property integer $id
 * @property integer $id_category_fk
 * @property string $name
 * @property string $name_lang
 * @property string $symbol
 * @property string $symbol_lang
 * @property string $describe
 * @property string $describe_lang
 * @property string $config
 * @property integer $is_require
 * @property integer $value_type
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcFactor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_factor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category_fk', 'is_require', 'value_type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name_lang', 'symbol_lang', 'describe', 'describe_lang'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'symbol', 'config'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_category_fk' => Yii::t('app', 'Id Category Fk'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'symbol' => Yii::t('app', 'Symbol'),
            'symbol_lang' => Yii::t('app', 'Symbol Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
            'config' => Yii::t('app', 'Config'),
            'is_require' => Yii::t('app', 'Is Require'),
            'value_type' => Yii::t('app', 'Value Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }

    /**
     * @inheritdoc
     * @return SvcFactorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcFactorQuery(get_called_class());
    }
}
