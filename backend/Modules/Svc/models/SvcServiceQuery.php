<?php

namespace app\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcService]].
 *
 * @see SvcService
 */
class SvcServiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}