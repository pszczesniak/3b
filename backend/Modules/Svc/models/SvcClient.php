<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%svc_client}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $id_country_fk
 * @property integer $id_province_fk
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property double $pos_lat
 * @property double $pos_lng
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $phone_additional
 * @property string $website_url
 * @property string $facebook_url
 * @property string $facebook_config
 * @property string $avatar_img
 * @property string $sex
 * @property string $birthday
 * @property string $profession
 * @property integer $id_trade_fk
 * @property integer $id_education_fk
 * @property string $note
 * @property string $first_logged_date
 * @property string $last_logged_date
 * @property integer $confirm
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcClient extends \yii\db\ActiveRecord
{
    public $rule;
    public $agree;
    public $username;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'username', 'email'], 'required'],
            //[['rule'], 'required', 'message' => 'Aby założyć konto klienta, musisz zaakceptować Nasz regulamin.'],
            //[['agree'], 'required', 'message' => 'Aby założyć konto klienta, musisz wyrazić zgodę na otrzymywanie informacji handlowej.'],
            [['id_user_fk', 'id_country_fk', 'id_province_fk', 'id_trade_fk', 'id_education_fk', 'confirm', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['pos_lat', 'pos_lng'], 'number'],
            [['phone_additional', 'facebook_config', 'note'], 'string'],
            [['birthday', 'first_logged_date', 'last_logged_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['city', 'postal_code', 'address', 'firstname', 'lastname', 'email', 'phone', 'website_url', 'facebook_url', 'avatar_img', 'sex', 'profession'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_country_fk' => Yii::t('app', 'Id Country Fk'),
            'id_province_fk' => Yii::t('app', 'Województwo'),
            'city' => Yii::t('app', 'Miasto'),
            'postal_code' => Yii::t('app', 'Kod pocztowy'),
            'address' => Yii::t('app', 'Adres'),
            'pos_lat' => Yii::t('app', 'Pos Lat'),
            'pos_lng' => Yii::t('app', 'Pos Lng'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Nazwisko'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Telefon'),
            'phone_additional' => Yii::t('app', 'Phone Additional'),
            'website_url' => Yii::t('app', 'Website Url'),
            'facebook_url' => Yii::t('app', 'Facebook Url'),
            'facebook_config' => Yii::t('app', 'Facebook Config'),
            'avatar_img' => Yii::t('app', 'Avatar Img'),
            'sex' => Yii::t('app', 'Płeć'),
            'birthday' => Yii::t('app', 'Data urodzenia'),
            'profession' => Yii::t('app', 'Zawód'),
            'id_trade_fk' => Yii::t('app', 'Branża'),
            'id_education_fk' => Yii::t('app', 'Wykształcenie'),
            'note' => Yii::t('app', 'Describe'),
            'first_logged_date' => Yii::t('app', 'First Logged Date'),
            'last_logged_date' => Yii::t('app', 'Last Logged Date'),
            'confirm' => Yii::t('app', 'Confirm'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'username' => 'Login'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
		
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
     
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'SvcOffer',
				// image dimmentions for preview in widget 
				'previewHeight' => 100,
				'previewWidth' => 100,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/images/account/cover',
				'url' => Yii::getAlias('@web') . '/images/account/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(100));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function getUser()
    {
		return $this->hasOne(\common\models\User::className(), ['id' => 'id_user_fk']);

    }
    
    public function getProvince()
    {
		return $this->hasOne(\backend\Modules\Loc\models\LocProvince::className(), ['id' => 'id_province_fk']);
    }
    
    public function getEducation()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcEducation::className(), ['id' => 'id_education_fk']);
    }
    
    public function getTrade()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcTrade::className(), ['id' => 'id_trade_fk']);
    }
    
    public function getAmountmsg() {
        return \backend\Modules\Svc\models\SvcMessage::find()->where(['created_by' => \Yii::$app->user->id])->andWhere('status >= 1')->count();
    }
    
    public function getAmountcmt() {
        return \backend\Modules\Svc\models\SvcComment::find()->where(['created_by'=>\Yii::$app->user->id, 'status' => 2])->count();
    }
    
    public function getAmountcmttoaccept() {
        return \backend\Modules\Svc\models\SvcComment::find()->where(['created_by'=>\Yii::$app->user->id, 'status' => 1])->count();
    }
    
    public function getNotifications() {
        $msg = \backend\Modules\Svc\models\SvcMessage::find()->where(['created_by' => \Yii::$app->user->id])->andWhere('status = 1')->count();
        $cmt = \backend\Modules\Svc\models\SvcComment::find()->where(['created_by' => \Yii::$app->user->id])->andWhere('status = 1')->count();
        
        return ($msg + $cmt);
    }
    
    public function getActivity() {
        $avtivity = [];
        $activity['comments'] = \backend\Modules\Svc\models\SvcComment::find()->where(['id_client_fk' => $this->id])->count();
        $activity['queries'] = \backend\Modules\Svc\models\SvcQuery::find()->where(['created_at' => $this->id])->count();
        
        return $activity;
    }
    
}
