<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%svc_client_observed}}".
 *
 * @property integer $id
 * @property integer $id_client_fk
 * @property integer $id_offer_fk
 * @property string $note
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcClientObserved extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_client_observed}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_client_fk', 'id_offer_fk', 'id_other_offer_fk', 'id_user_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['note'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_client_fk' => Yii::t('app', 'Id Client Fk'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
		
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	/*public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}*/
}
