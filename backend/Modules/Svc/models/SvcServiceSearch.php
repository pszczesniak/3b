<?php

namespace app\Modules\Svc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\Modules\Svc\models\SvcService;

/**
 * SvcServiceSearch represents the model behind the search form about `app\Modules\Svc\models\SvcService`.
 */
class SvcServiceSearch extends SvcService
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_fk', 'id_category_fk', 'is_sale', 'is_new', 'is_recommended', 'id_country_fk', 'id_province_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'name_lang', 'describe', 'describe_lang', 'price_lang', 'price_sale_lang', 'city', 'postal_code', 'address', 'params', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['price', 'price_sale'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcService::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user_fk' => $this->id_user_fk,
            'id_category_fk' => $this->id_category_fk,
            'price' => $this->price,
            'is_sale' => $this->is_sale,
            'price_sale' => $this->price_sale,
            'is_new' => $this->is_new,
            'is_recommended' => $this->is_recommended,
            'id_country_fk' => $this->id_country_fk,
            'id_province_fk' => $this->id_province_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_lang', $this->name_lang])
            ->andFilterWhere(['like', 'describe', $this->describe])
            ->andFilterWhere(['like', 'describe_lang', $this->describe_lang])
            ->andFilterWhere(['like', 'price_lang', $this->price_lang])
            ->andFilterWhere(['like', 'price_sale_lang', $this->price_sale_lang])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }
}
