<?php

namespace app\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcFactor]].
 *
 * @see SvcFactor
 */
class SvcFactorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcFactor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcFactor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}