<?php

namespace app\Modules\Svc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Svc\models\SvcClient;

/**
 * SvcClientSearch represents the model behind the search form about `backend\Modules\Svc\models\SvcClient`.
 */
class SvcClientSearch extends SvcClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_fk', 'id_country_fk', 'id_province_fk', 'id_trade_fk', 'id_education_fk', 'confirm', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['city', 'postal_code', 'address', 'firstname', 'lastname', 'email', 'phone', 'phone_additional', 'website_url', 'facebook_url', 'facebook_config', 'avatar_img', 'sex', 'birthday', 'profession', 'note', 'first_logged_date', 'last_logged_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pos_lat', 'pos_lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcClient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user_fk' => $this->id_user_fk,
            'id_country_fk' => $this->id_country_fk,
            'id_province_fk' => $this->id_province_fk,
            'pos_lat' => $this->pos_lat,
            'pos_lng' => $this->pos_lng,
            'birthday' => $this->birthday,
            'id_trade_fk' => $this->id_trade_fk,
            'id_education_fk' => $this->id_education_fk,
            'first_logged_date' => $this->first_logged_date,
            'last_logged_date' => $this->last_logged_date,
            'confirm' => $this->confirm,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_additional', $this->phone_additional])
            ->andFilterWhere(['like', 'website_url', $this->website_url])
            ->andFilterWhere(['like', 'facebook_url', $this->facebook_url])
            ->andFilterWhere(['like', 'facebook_config', $this->facebook_config])
            ->andFilterWhere(['like', 'avatar_img', $this->avatar_img])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
