<?php

namespace app\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "svc_service_category".
 *
 * @property integer $id
 * @property integer $id_category_fk
 * @property integer $id_service_fk
 * @property integer $ordering
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcServiceCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'svc_service_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category_fk', 'id_service_fk', 'created_by'], 'required'],
            [['id_category_fk', 'id_service_fk', 'ordering', 'status', 'created_by', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_category_fk' => Yii::t('app', 'Id Category Fk'),
            'id_service_fk' => Yii::t('app', 'Id Service Fk'),
            'ordering' => Yii::t('app', 'Ordering'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }

    /**
     * @inheritdoc
     * @return SvcServiceCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcServiceCategoryQuery(get_called_class());
    }
}
