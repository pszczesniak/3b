<?php

namespace backend\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_offer_category}}".
 *
 * @property integer $id
 * @property integer $id_offer_fk
 * @property integer $id_category_fk
 * @property integer $level
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcOfferCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_offer_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_offer_fk'], 'required'],
            [['id_offer_fk', 'id_category_fk', 'level', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'id_category_fk' => Yii::t('app', 'Id Category Fk'),
            'level' => Yii::t('app', 'Level'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function getCategory()
    {
        return $this->hasOne(\backend\Modules\Svc\models\SvcCategory::className(), ['id' => 'id_category_fk']);
    }
}
