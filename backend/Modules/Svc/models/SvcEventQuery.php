<?php

namespace backend\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcEvent]].
 *
 * @see SvcEvent
 */
class SvcEventQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcEvent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcEvent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}