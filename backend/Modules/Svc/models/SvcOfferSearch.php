<?php

namespace app\Modules\Svc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Svc\models\SvcOffer;

/**
 * SvcOfferSearch represents the model behind the search form about `backend\Modules\Svc\models\SvcOffer`.
 */
class SvcOfferSearch extends SvcOffer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_fk', 'id_package_fk', 'id_country_fk', 'id_province_fk', 'confirm', 'confirm_payment', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['city', 'postal_code', 'address', 'companyname', 'firstname', 'lastname', 'phone', 'phone_additional', 'email', 'website_url', 'facebook_url', 'note', 'first_logged_date', 'last_logged_date', 'facebook_config', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pos_lat', 'pos_lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcOffer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user_fk' => $this->id_user_fk,
            'id_package_fk' => $this->id_package_fk,
            'id_country_fk' => $this->id_country_fk,
            'id_province_fk' => $this->id_province_fk,
            'pos_lat' => $this->pos_lat,
            'pos_lng' => $this->pos_lng,
            'first_logged_date' => $this->first_logged_date,
            'last_logged_date' => $this->last_logged_date,
            'confirm' => $this->confirm,
            'confirm_payment' => $this->confirm_payment,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'companyname', $this->companyname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_additional', $this->phone_additional])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website_url', $this->website_url])
            ->andFilterWhere(['like', 'facebook_url', $this->facebook_url])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'facebook_config', $this->facebook_config]);

        return $dataProvider;
    }
}
