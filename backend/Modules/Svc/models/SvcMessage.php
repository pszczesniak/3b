<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Svc\models\SvcClient;
use backend\Modules\Svc\models\SvcOffer;

/**
 * This is the model class for table "{{%svc_message}}".
 *
 * @property integer $id
 * @property integer $id_message_fk
 * @property integer $id_offer_fk
 * @property integer $id_query_fk
 * @property string $type
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcMessage extends \yii\db\ActiveRecord
{
    public $type1;
    public $comment;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_offer_fk', 'type', 'email', 'message'], 'required'],
            [['email'],'email'],
            [['id_message_fk', 'id_offer_fk', 'id_query_fk', 'status', 'created_by', 'updated_by', 'deleted_offer_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_offer_at', 'email', 'name', 'message'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_message_fk' => Yii::t('app', 'Id Message Fk'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'id_query_fk' => Yii::t('app', 'Id Query Fk'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_offer_at' => Yii::t('app', 'Deleted At'),
            'deleted_offer_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
     public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status_offer = -1;
		$this->deleted_offer_at = new Expression('NOW()');
		$this->deleted_offer_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getOffer()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcOffer::className(), ['id' => 'id_offer_fk']);
    }
    
    public function getClient()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcClient::className(), ['id' => 'id_client_fk']);
    }
    
    public function getQuery()
    {
		return $this->hasOne(\backend\Modules\Svc\models\SvcQuery::className(), ['id' => 'id_query_fk']);
    }
    
    public function getParser() {
        $parser = [];
        if($this->type == 'in') {
            if($this->id_client_fk) {
                $client = SvcClient::find()->where( ['id' => $this->id_client_fk] )->one();
                if($client) 
                    $parser['from'] = ($client->firstname || $client->lastname) ? $client->firstname. ' '.$client->lastname : 'Klient portalu';
                else 
                    $parser['from'] = 'Klient portalu';
            } else {
                $parser['from'] = 'Klient portalu';
            }
            
            $offer = SvcOffer::findOne($this->id_offer_fk);
            if($offer) 
                $parser['to'] = $offer->companyname;
            else 
                $parser['to'] = 'Oferta nieaktywna';
                    
        } else {
            if($this->id_client_fk) {
                $client = SvcClient::find()->where( ['id' => $this->id_client_fk] )->one();
                if($client) 
                    $parser['to'] = $client->firstname. ' '.$client->lastname;
                else 
                    $parser['to'] = 'Klient portalu';
            } else {
                $parser['to'] = 'Klient portalu';
            }
            
            $offer = SvcOffer::findOne($this->id_offer_fk);
            if($offer) 
                $parser['from'] = $offer->companyname;
            else 
                $parser['from'] = 'Oferta nieaktywna';
        } 
        
        return $parser;
    }
}
