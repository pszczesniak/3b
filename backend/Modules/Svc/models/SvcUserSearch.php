<?php

namespace app\Modules\Svc\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\Modules\Svc\models\SvcUser;

/**
 * SvcUserSearch represents the model behind the search form about `app\Modules\Svc\models\SvcUser`.
 */
class SvcUserSearch extends SvcUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_fk', 'id_package_fk', 'id_country_fk', 'id_province_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['city', 'postal_code', 'address', 'firstname', 'lastname', 'phone', 'phone_additional', 'note', 'first_logged_date', 'last_logged_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user_fk' => $this->id_user_fk,
            'id_package_fk' => $this->id_package_fk,
            'id_country_fk' => $this->id_country_fk,
            'id_province_fk' => $this->id_province_fk,
            'first_logged_date' => $this->first_logged_date,
            'last_logged_date' => $this->last_logged_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_additional', $this->phone_additional])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
