<?php

namespace app\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcServiceCategory]].
 *
 * @see SvcServiceCategory
 */
class SvcServiceCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcServiceCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcServiceCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}