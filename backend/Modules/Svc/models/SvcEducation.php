<?php

namespace backend\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_education}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property string $describe_lang
 * @property string $config
 * @property integer $rank
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcEducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_education}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_lang', 'describe', 'describe_lang'], 'string'],
            [['rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'config'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Nazwa',
            'name_lang' => Yii::t('app', 'Name Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
            'config' => Yii::t('app', 'Config'),
            'rank' => Yii::t('app', 'Rank'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
