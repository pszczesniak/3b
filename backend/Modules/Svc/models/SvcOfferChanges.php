<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%svc_offer_changes}}".
 *
 * @property integer $id
 * @property integer $id_offer_fk
 * @property integer $id_old_package_fk
 * @property integer $id_new_package_fk
 * @property string $date_of_change
 * @property string $expiration_date
 * @property double $payment
 * @property integer $is_changed
 * @property integer $is_resigantion
 * @property integer $is_payment
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcOfferChanges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_offer_changes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_offer_fk', 'id_old_package_fk', 'id_new_package_fk', 'is_changed', 'is_resigantion', 'is_payment', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_of_change', 'expiration_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['payment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_offer_fk' => Yii::t('app', 'Id Offer Fk'),
            'id_old_package_fk' => Yii::t('app', 'Id Old Package Fk'),
            'id_new_package_fk' => Yii::t('app', 'Id New Package Fk'),
            'date_of_change' => Yii::t('app', 'Date Of Change'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
            'payment' => Yii::t('app', 'Payment'),
            'is_changed' => Yii::t('app', 'Is Changed'),
            'is_resigantion' => Yii::t('app', 'Is Resigantion'),
            'is_payment' => Yii::t('app', 'Is Payment'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
		
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
}
