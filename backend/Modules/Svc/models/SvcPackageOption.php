<?php

namespace backend\Modules\Svc\models;

use Yii;

/**
 * This is the model class for table "{{%svc_package_option}}".
 *
 * @property integer $id
 * @property integer $id_package_fk
 * @property string $name
 * @property string $note
 * @property double $price
 * @property string $price_lang
 * @property integer $is_sale
 * @property double $price_sale
 * @property string $price_sale_lang
 * @property integer $rank
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcPackageOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_package_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_package_fk', 'is_sale', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['note', 'price_lang', 'price_sale_lang'], 'string'],
            [['price', 'price_sale'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_package_fk' => Yii::t('app', 'Id Package Fk'),
            'name' => Yii::t('app', 'Name'),
            'note' => Yii::t('app', 'Note'),
            'price' => Yii::t('app', 'Price'),
            'price_lang' => Yii::t('app', 'Price Lang'),
            'is_sale' => Yii::t('app', 'Is Sale'),
            'price_sale' => Yii::t('app', 'Price Sale'),
            'price_sale_lang' => Yii::t('app', 'Price Sale Lang'),
            'rank' => Yii::t('app', 'Rank'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
