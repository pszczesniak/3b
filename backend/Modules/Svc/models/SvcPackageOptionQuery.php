<?php

namespace app\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcPackageOption]].
 *
 * @see SvcPackageOption
 */
class SvcPackageOptionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcPackageOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcPackageOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}