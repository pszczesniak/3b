<?php

namespace app\Modules\Svc\models;

/**
 * This is the ActiveQuery class for [[SvcPackage]].
 *
 * @see SvcPackage
 */
class SvcPackageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SvcPackage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SvcPackage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}