<?php

namespace backend\Modules\Svc\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Svc\models\SvcOffer;
use backend\Modules\Svc\models\SvcMessage;

/**
 * This is the model class for table "{{%svc_query}}".
 *
 * @property integer $id
 * @property string $query
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class SvcQuery extends \yii\db\ActiveRecord
{
    public $name;
    public $event;  
    public $categories_temp = [];
    public $event_date;
    public $city;
    public $postal_code;
    public $phone;
    public $describe;
    public $rule;
    public $search_id;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_query}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'string'],
            [['email', 'event', 'categories_temp', 'event_date', 'city', 'postal_code', 'phone', 'message'], 'required'],
            [['rule'], 'required', 'message' => 'Aby wysłać zapytanie, musisz zaakceptować Nasz regulamin.'],
            [['status', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'message'], 'safe'],
            ['event_date', 'date', 'format' => 'yyyy-M-d'],
            [['email'], 'string', 'max' => 255],
            [['request_remote_addr', 'request_user_agent', 'request_content_type'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'query' => Yii::t('app', 'Query'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'categories_temp' => 'Rodzaj usług',
            'event_date' => 'Data wydarzenia', 
            'city' => 'Mjescowość', 
            'postal_code' => 'Kod pocztowy', 
            'phone' => 'Telefon kontaktowy', 
            'message' => 'Treść wiadomości', 
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'detials' => 'Szczegóły zapytania',
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
		
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
                
                $params['email'] = $this->email;
                $params['event' ] = $this->event;
                $params['categories_temp'] = $this->categories_temp;
                $params['event_date'] = $this->event_date;
                $params['city'] = $this->city;
                $params['postal_code'] = $this->postal_code;
                $params['phone'] = $this->phone;
                $params['message'] = $this->message;
                
                $this->query = \yii\helpers\Json::encode($params);
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
        $offersTemp = [];
        $params = \yii\helpers\Json::decode($this->query);
        
        $query = SvcOffer::find();
        $query->where('id_package_fk >= 2');
        $query->where('confirm >= -1');
        $query->where('lower(city) = "'.strtolower($this->city).'"');
        //$query->andWhere('postal_code = "'.$this->postal_code.'"');
        $query->andWhere('id in (select id_offer_fk from {{%svc_offer_event}} where id_event_fk = '.$params['event'].')');
        $query->andWhere('id in (select id_offer_fk from {{%svc_offer_category}} where id_category_fk in ('.implode(",", $params['categories_temp']).') )');
       // echo '(select id_offer_fk from svc_offer_category where id_category_fk in ('.implode(",",$params['categories_temp']).')';exit;
        $offers = $query->all();
        
        //var_dump($offers); exit;
        
        foreach($offers as $key => $offer) {
            array_push($offersTemp, $offer->id);
            $message = new SvcMessage();
            $message->id_offer_fk = $offer->id;
            $message->id_message_fk = 0;
            $message->id_client_fk = (\Yii::$app->session->get('user.client')) ? \Yii::$app->session->get('user.client') : 0;
            $message->id_query_fk = $this->id;
            $message->message = $this->message;
            $message->email = $this->email;
            $message->type = 'in';
            $message->name = 'Integrator';
            $message->save();
        }
        
        $searchQuery = new \backend\Modules\Svc\models\SvcSearch();
        $searchQuery->created_by = (!\Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
        $searchQuery->query = \yii\helpers\Json::encode([]);
        $searchQuery->order_by = '';
        $searchQuery->deleted_by = $this->id;
        $searchQuery->offers = implode(',', $offersTemp); 
        $searchQuery->save();
        //$this->search_id = $searchQuery->id;
    }
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getAuthor()
    {
		//return $this->hasOne(\backend\Modules\Svc\models\SvcOffer::className(), ['id' => 'id_offer_fk']);
        if($this->created_by && $this->created_by > 0 ) {
            return \common\models\User::findOne($this->created_by)->username;
        } else {
            return 'niezarejestrowany';
        }
    }
    
    public function getDetails() {
		$details = \yii\helpers\Json::decode($this->query);
        //{"email":"klient@klient.pl","event":"19","categories_temp":["20","22","28","29"],"event_date":"2016-05-31","city":"Częstochowa","postal_code":"42-200","phone":"123 568 852","message":"kjiki8ok87"}
        $categories = ''; $categoriesNames = '';
        if($details['categories_temp'] && is_array($details['categories_temp'])) {
            foreach($details['categories_temp'] as $key => $value) {
                $categories .= $value.',';
            }
            
            $categories .= '0';
            
            $categories = \backend\Modules\Svc\models\SvcCategory::find()->where('id in ('.$categories.')')->orderby('name')->all();
            $categoriesNames = '<ol class="list-ol">';
            foreach($categories as $key => $value) {
                $categoriesNames .= '<li>'.$value->name.( ($value->parent) ? '  ['.$value->parent['name'].']' : '' ).'</li>';
            }
            $categoriesNames .= '</ol>';
        }
        
        $eventLabel = 'brak informacji'; $event = false;
        if($details['event'] && $details['event'] > 0)
            $event = \backend\Modules\Svc\models\SvcEvent::findOne($details['event']);
            
        if($event) $eventLabel = $event->name;
        $result = '<ul class="list-ul">'
                    .'<li>Miasto: '.$details['city'].'</li>'
                    .'<li>Kod pocztowy: '.$details['postal_code'].'</li>'
                    .'<li>Telefon: '.$details['phone'].'</li>'
                    .'<li>Data wydarzenia: '.$details['event_date'].'</li>'
                    .'<li>Rodzaj wydarzenia: '. $eventLabel .'</li>'
                    .'<li>Kategorie: '.substr($categoriesNames, 0, -1).'</li>'
                   .'</ul>';
         
        return $result;
    }
    
    public function getResult() {
		$details = \yii\helpers\Json::decode($this->query);
        //{"email":"klient@klient.pl","event":"19","categories_temp":["20","22","28","29"],"event_date":"2016-05-31","city":"Częstochowa","postal_code":"42-200","phone":"123 568 852","message":"kjiki8ok87"}
        $categories = ''; $categoriesNames = '';
        if($details['categories_temp'] && is_array($details['categories_temp'])) {
            foreach($details['categories_temp'] as $key => $value) {
                $categories .= $value.',';
            }
            
            $categories .= '0';
            
            $categories = \backend\Modules\Svc\models\SvcCategory::find()->where('id in ('.$categories.')')->orderby('name')->all();
            $categoriesNames = '<ol class="list-ol">';
            foreach($categories as $key => $value) {
                $categoriesNames .= '<li>'.$value->name.( ($value->parent) ? '  ['.$value->parent['name'].']' : '' ).'</li>';
            }
            $categoriesNames .= '</ol>';
        }
        
        $eventLabel = 'brak informacji'; $event = false;
        if($details['event'] && $details['event'] > 0)
            $event = \backend\Modules\Svc\models\SvcEvent::findOne($details['event']);
            
        if($event) $eventLabel = $event->name;
        $result = '<ul class="list-ul list-ul--bullet">'
                    .'<li><i>Opis:</i> '.$this->message.'</li>'
                    .'<li><i>E-mail:</i> '.$this->email.'</li>'
                    .'<li><i>Miasto:</i> '.$details['city'].'</li>'
                    .'<li><i>Kod pocztowy:</i> '.$details['postal_code'].'</li>'
                    .'<li><i>Telefon:</i> '.$details['phone'].'</li>'
                    .'<li><i>Data wydarzenia:</i> '.$details['event_date'].'</li>'
                    .'<li><i>Rodzaj wydarzenia:</i> '. $eventLabel .'</li>'
                    .'<li><i>Kategorie:</i> '.substr($categoriesNames, 0, -1).'</li>'
                .'</ul>';
         
        return $result;
    }
    
    public function getMessages()
    {
		return \backend\Modules\Svc\models\SvcMessage::find()->where(['id_query_fk' => $this->id])->all();
    }
	
}
