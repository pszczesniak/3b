<?php

namespace app\Modules\Admin\controllers;

use Yii;
use common\models\User;
use app\Modules\Admin\models\Assignment;
use app\Modules\Admin\models\searchs\Assignment as AssignmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    
    public function actionIndex()
    {
        $searchModel = new User();
        $searchModel->status = 10;

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = [0 => 'admin'];

		$post = $_GET; $where = [];
        if(isset($_GET['User'])) {
            $params = $_GET['User'];
            if(isset($params['email']) && !empty($params['email']) ) {
				array_push($where, "lower(u.email) like '%".strtolower( addslashes($params['email']) )."%'");
            }
            if(isset($params['username']) && !empty($params['username']) ) {
				array_push($where, "lower(u.username) like '%".strtolower( addslashes($params['username']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "u.status = ".$params['status']);
            }
        } else {
            array_push($where, "u.status = 10");
        } 
        
        $sortColumn = 'u.username';
        if( isset($post['sort']) && $post['sort'] == 'email' ) $sortColumn = 'u.email';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'u.status';
        if( isset($post['sort']) && $post['sort'] == 'username' ) $sortColumn = 'u.username';
		
		$query = (new \yii\db\Query())
            ->select(['u.id as id', 'u.email as email', 'u.username as username' , 'u.status as status', 'ai.name as ainame', 'firstname', 'lastname'])
            ->from('{{%user}} as u')
            ->join('JOIN', '{{%auth_assignment}} as aa', 'u.id = aa.user_id')
            ->join('JOIN', '{{%auth_item}} as ai', '(aa.item_name = ai.name and ai.type = 1)')
            ->where('name not like "$_user%" and name != "client" and username != "superadmin"');
          // ->where( ['s.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [10 => 'success', 0 => 'danger'];
        $statusLits = [10 => 'aktywny', 0 => 'zablokowany'];
        
		foreach($rows as $key=>$value) {
            $status = ($value['status'] == 10) ? 'lock' : 'unlock';
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/admin/user/updateajax','id' => $value['id']]).'" class="btn btn-sm btn-default gridViewModal" data-table="#table-users" data-target="#modal-grid-item"><i class="fa fa-pencil"></i></a>';
            //$actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/admin/user/deleteajax/'.$value['id'].'" class="btn btn-sm btn-default remove" data-table="#table-users"><i class="fa fa-trash"></i></a>' : '');
            $actionColumn .= ( count(array_intersect(["admin", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/admin/user/'.$status.'', 'id' => $value['id']]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-users" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $actionColumn .= ( count(array_intersect(["admin", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/admin/user/grants', 'id' => $value['id']]).'" class="btn btn-sm btn-default"  title="Uprawnienia"><i class="fa fa-key"></i></a>':'';            
            $actionColumn .= '</div>';
			$tmp['email'] = $value['email'];
            $tmp['login'] = $value['username'];
            $tmp['firstname'] = $value['firstname'];
            $tmp['lastname'] = $value['lastname'];
            $tmp['role'] = $value['ainame'];
            $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$statusLits[$value['status']].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}


    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   
    public function actionCreateajax() {
		
        $model = new User();
        $model->scenario = 'default';
        $model->roleType = 'admin';
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(\Yii::$app->request->post());
            $model->status = 10;
            
            $model->generateAuthKey();
            $pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
            $model->setPassword($pass);
	
            if($model->validate() && $model->save()) {
                $auth = \Yii::$app->authManager;
                $role = $auth->getRole($model->roleType); 
                if($role)
                    $auth->assign($role, $model->id);
                
                \Yii::$app->mailer->compose(['html' => 'userAuthorization-html', 'text' => 'userAuthorization-text'], ['login' => $model->username, 'password' => $pass])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                    ->setTo($model->email)
                    ->setBcc(\Yii::$app->params['bccEmail'])
                    ->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
                    ->send();
                    
                return array('success' => true, 'table' => '#table-users', 'index' =>1, 'alert' => 'Użytkownik <b>'.$model->username.'</b> został zarejestrowany',  'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'table' => '#table-users', 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]); 
        } else {
            return $this->renderPartial('_form', [
                'model' => $model
            ]);
        }
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
        
        $userRoles = \Yii::$app->authManager->getRolesByUser($model->id); 
        foreach($userRoles as $key=>$value) {
            if('$_user_'.$model->username != $key)
                $model->roleType = $key;
        }
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				$auth = \Yii::$app->authManager;
                $userRoles = \Yii::$app->authManager->getRolesByUser($model->id); 
                foreach($userRoles as $key => $value) {
                    if('$_user_'.$model->username != $key)
                        $auth->revoke($value, $model->id);
                }
                
                $role = $auth->getRole($model->roleType);
                $auth->assign($role, $model->id);
                
                return array('success' => true, 'table' => '#table-users', 'action' => 'updateRow', 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'table' => '#table-users', 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-users');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-users');	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionLock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = -1;
        $model->user_action = 'lock';
        
        $model->status = 0;
		$status = 'unlock';
		//$grants = $this->module->params['grants'];
        
        if($model->save()) {
            return array('success' => true, 'alert' => 'Użytkownik <b>'.$model->username.'</b> został zablokowany', 'table' => '#table-users', 'index' => (isset($_GET['index'])) ? $_GET['index'] : 0);	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Użytkownik <b>'.$model->username.'</b> nie został zablokowany', );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = 1;
        $model->user_action = 'lock';
        
        $model->status = 10;
        $status = 'lock';
		//$grants = $this->module->params['grants'];
        
        if($model->save()) {
            return array('success' => true, 'alert' => 'Użytkownik <b>'.$model->username.'</b> został odblokowany', 'table' => '#table-users', 'index' => (isset($_GET['index'])) ? $_GET['index'] : 0 );	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Użytkownik <b>'.$model->username.'</b> nie został odblokowany', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGrants($id) {
        $user = User::findOne($id);
        //$grants = $user->grants;
        
        $role = false;
        
        $auth = Yii::$app->authManager;
        $roleGrants = [];
        //$roleUser = $auth->getRole('$_user_'.$user->username);
		$userGrants = $auth->getPermissionsByRole('$_user_'.$user->username);
		$userRoles = \Yii::$app->authManager->getRolesByUser($user->id); 
		// var_dump($loggedUserRole); exit;
		foreach($userRoles as $key=>$value) {
			if('$_user_'.$user->username != $key) {
                $roleGrants = $auth->getPermissionsByRole($key);
                $role = $value->description;
            }
		}
        
        return $this->render('_formGrants', [
                'roleGrants' => $roleGrants, 'userGrants' => $userGrants, 'roleLoggedUserGrants' => [], 'role' => $role, 'user' => $user
            ]);
    }
    
    public function actionGrantsuser($user) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = Yii::$app->getRequest()->post();
		
		$auth = Yii::$app->authManager;
   	    $user =  \common\models\User::findOne($user);
		$role = $auth->getRole('$_user_'.$user->username);
		if(!$role) {
			$role = $auth->createRole('$_user_'.$user->username);
			$auth->add($role);
			$auth->assign($role, $user->id);
		}
		$auth->removeChildren($role);
		if(isset($post['grant'])) {
            foreach($post['grant'] as $key => $value) {
                $grant = $auth->getPermission($value);
                // var_dump($grant);
                if($grant)
                    $auth->addChild($role, $grant);
                $grant = false;
                //echo $key.' => '.$value.'<br />';
            }
		}
		//$auth->addChild($role, $grant);
        //$auth->assign($role, $user->getId());
		$res = array('result' => true, 'success' => 'Zmiany zostały zapisane');
		return $res;
	}
}
