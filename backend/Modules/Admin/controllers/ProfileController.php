<?php

namespace app\Modules\Admin\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use backend\Modules\Company\models\CompanyEmployee;

/**
 * Profile controller for the `Admin` module
 */
class ProfileController extends Controller
{
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'User' => User::className()
				]
			],
		];
	}
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()  {
        $folders = [];
       /* $dataFolders = Folder::find()->where([ 'item_type' => 2, 'status' => 1, 'created_by' => Yii::$app->user->id])->all();

        foreach($dataFolders as $key => $folder) {
            array_push($folders, ['id' => $folder->id, 'name' => $folder->item_name, 'items' => 2, 'path' => $folder->fullpath]);
        }*/
        
        $user = User::findOne(Yii::$app->user->id);
		$model = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        
        if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(isset($_POST['CompanyEmployee']['departments_list'])  && !empty($_POST['CompanyEmployee']['departments_list']) ) {
                $model->id_department_fk = implode(',',$_POST['CompanyEmployee']['departments_list']);
            }
			if($model->validate() && $model->save()) {
				return $this->render('view', ['model' => $model, 'folders' => $folders]);
			} 	
		} 
        
		return $this->render('view', ['model' => $model, 'folders' => $folders]);
    }
}
