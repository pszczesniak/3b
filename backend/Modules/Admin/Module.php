<?php

namespace app\Modules\Admin;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\Modules\Admin\controllers';

    public function init()
    {
        //$this->layout="lsdd";
		//\Yii::$app->session->set('user.module','lsdd');
		//\Yii::$app->user->identity->module = 'lsdd';
		//\Yii::$app->user->identity->setModule('lsdd');
		//var_dump(\Yii::$app->user->identity);
		parent::init();

        // custom initialization code goes here
    }
}
