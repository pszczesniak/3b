<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use common\widgets\Alert;
    
    $this->title = 'Twój profil';
    
    $this->params['breadcrumbs'][] = 'Administracja';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
	<div class="col-lg-6 col-md-6 col-sm-12">
		<?php $this->beginContent('@app/views/layouts/view-window.php',array('class' => 'acc-docs-index', 'title' => Html::encode('Podstawowe'))) ?>
            <?= $this->render('_form', ['model' => $model]) ?>
        <?php $this->endContent(); ?>
	</div>
    <div class="col-lg-6 col-md-6 col-sm-12">
		<?php $this->beginContent('@app/views/layouts/view-window.php',array('class' => 'acc-docs-index', 'title' => Html::encode('Zdjęcie profilowe'))) ?>
            <div class="grid profile">
                <!-- Start .row -->
                
                <div class="col-xs-12 align-center">
                    <div class="profile-avatar">
                        <?php /*echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                    [
                                        'id' => 'imgAttachment',
                                        'model' => $model,
                                        'behaviorName' => 'coverBehavior',
                                        'apiRoute' => '/admin/profile/imgAttachApi',
                                    ]
                                
                                )*/
                        ?>
                        <div id="upload-demo"></div>
                        <div class="imageAttachment" id="upload-demo-id" data-id="<?= (is_file("uploads/users/avatars/avatar-".$model->id.".jpg")) ? $model->id : 0 ?>">
                            <div class="btn-toolbar actions-bar">
                                <span class="btn btn-success btn-file">
                                    <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                                    <span data-replace-text="Zamień..." data-upload-text="Prześlij" class="file_label">Pobierz zdjęcie</span>
                                    <input type="file" id="upload" name="Avatar[avatar]" value="Choose a file" data-path="<?= Url::to(['/admin/default/upload','id' => $model->id]) ?>">
                                </span>
                                <button class="btn btn-success upload-result" data-path="<?= Url::to(['/admin/default/saveavatar','id' => $model->id]) ?>">Zapisz avatar</button>
                                <!--<button class="btn btn-success change-view">Ustaw miniaturę</button>-->
                            </div>  
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-12">
                    <div class="profile-info bt">
                        <h5 class="text-muted">Profile info</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores minus consequuntur corporis optio accusantium perspiciatis veritatis temporibus nostrum, saepe omnis aperiam illum odit, quos, cum eveniet
                            possimus illo. Saepe sunt porro culpa repellat dolorem aperiam, quae nihil, maxime ducimus officiis voluptate. Libero obcaecati laboriosam reprehenderit adipisci harum ad, veniam explicabo.</p>
                    </div>
                </div>-->
            </div>
                    
        <?php $this->endContent(); ?>
	</div>
	
</div>							