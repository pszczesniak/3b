<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meneżdżer użytkowników';
$this->params['breadcrumbs'][] = 'Admin';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"admin-user-index", 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-users" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/admin/user/createajax']) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-users",
                         'data-title' => "Nowy użytkownik"
                        ])  ?>    
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-users"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-users"

                data-toolbar="#toolbar-users" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="600"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="id"
                data-sort-order="desc"
                
                data-method="get"
                data-search-form="#filter-admin-users-search"
                data-url=<?= Url::to(['/admin/user/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="email"  data-sortable="true" >Email</th>
                    <th data-field="login"  data-sortable="true" >Login</th>
                    <th data-field="firstname"  data-sortable="true" >Imię</th>
                    <th data-field="lastname"  data-sortable="true" >Nazwisko</th>
                    <th data-field="role"  data-sortable="true" >Rola</th>
                    <th data-field="status" data-align="center" data-sortable="true" >Status</th>
                    <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>
<?php $this->endContent(); ?>
