<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-users"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<fieldset><legend>Dane systemowe</legend>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label('Login') ?>
                </div>
                <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('E-mail') ?>
                </div>
                <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                    <?= $form->field($model, 'roleType')->dropDownList($model->getRolesAsListData(['Lsddpadre', 'Lsddlabor', 'Lsddmanager', 'Lsddproduce', 'Lsdd']), [/*'prompt'=>' '*/] )->label('Rola');  ?>
                </div>
            </div>
        </fieldset>
        <fieldset><legend>Dane personalne</legend>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])->label('Imię') ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])->label('Nazwisko') ?>
                </div>
            </div>
        </fieldset>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-icon btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

