<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSubscribe */

$this->title = Yii::t('app', 'Create Newsletter Subscribe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"newsletter-subscribe-create", 'title'=>Html::encode($this->title))) ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
