<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSubscribe */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Newsletter Subscribe',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"newsletter-subscribe-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
