<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model commom\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-admin-users-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-users']
    ]); ?>
    
    <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'email')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-users', 'data-form' => '#filter-admin-users-search' ])->label('E-mail <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'username')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-users', 'data-form' => '#filter-admin-users-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>    
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'status', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( [10 => 'aktywny', 0 => 'zablokowany'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-users', 'data-form' => '#filter-admin-users-search' ] ) ?></div>        
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'roleType')->dropDownList($model->getRolesAsListData(['Lsddpadre', 'Lsddlabor', 'Lsddmanager', 'Lsddproduce', 'Lsdd']), array('prompt'=>' ') )->label('Rola');  ?>  </div>  
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
