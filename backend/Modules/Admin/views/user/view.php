<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSubscribe */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'status',
            'created_at',
            'deleted_at',
            'deleted_by',
        ],
    ]) ?>
</div>
<div class="modal-footer"> 
    <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
</div>

