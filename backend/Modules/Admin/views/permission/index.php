<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\AuthItem */

$this->title = Yii::t('rbac-admin', 'Permission');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="view-window role-index">
	<div class="view-window-title">
		<h5><?= Html::encode($this->title) ?></h5>
		<div class="view-window-tools">
			<a class="collapse-link">
				<i class="glyphicon glyphicon-menu-up"></i>
			</a>
			<a href="#" data-toggle="dropdown" class="dropdown-toggle">
				<i class="glyphicon glyphicon-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">Config option 1</a>
				</li>
				<li><a href="#">Config option 2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="glyphicon glyphicon-time"></i>
			</a>
		</div>
	</div>
	<div class="view-window-content">
		<div class="row">
			<div div class="col-md-12">
				<p>
					<?= Html::a(Yii::t('rbac-admin', 'Create Permission'), ['create'], ['class' => 'btn btn-success']) ?>
				</p>

				<?php
				Pjax::begin([
					'enablePushState'=>false,
				]);
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],
						[
							'attribute' => 'name',
							'label' => Yii::t('rbac-admin', 'Name'),
						],
						[
							'attribute' => 'description',
							'label' => Yii::t('rbac-admin', 'Description'),
						],
						[
							'class' => 'yii\grid\ActionColumn',
							'contentOptions' => ['class' => 'table-actions'],
							'buttons' => [
								'view' => function ($url, $model) {
									return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
											'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
									]);
								},
								'update' => function ($url, $model) {
									return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
											'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
									]);
								},
								'delete' => function ($url, $model) {
									return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
											'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
									]);
							}],
						]
					],
				]);
				Pjax::end();
				?>

			</div>
		</div>

	</div>
</div>

