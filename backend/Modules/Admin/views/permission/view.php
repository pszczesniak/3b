<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/*use mdm\admin\AdminAsset;*/
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="view-window auth-item-view">
	<div class="view-window-title">
		<h5><?= Html::encode($this->title) ?></h5>
		<div class="view-window-tools">
			<a class="collapse-link">
				<i class="glyphicon glyphicon-menu-up"></i>
			</a>
			<a href="#" data-toggle="dropdown" class="dropdown-toggle">
				<i class="glyphicon glyphicon-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">Config option 1</a>
				</li>
				<li><a href="#">Config option 2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="glyphicon glyphicon-time"></i>
			</a>
		</div>
	</div>
	<div class="view-window-content">
		<p>
			<?= Html::a(Yii::t('rbac-admin', 'Update'), ['update', 'id' => $model->name], ['class' => 'btn btn-primary']) ?>
			<?php
			echo Html::a(Yii::t('rbac-admin', 'Delete'), ['delete', 'id' => $model->name], [
				'class' => 'btn btn-danger',
				'data-confirm' => Yii::t('rbac-admin', 'Are you sure to delete this item?'),
				'data-method' => 'post',
			]);
			?>
		</p>

		<?php
		echo DetailView::widget([
			'model' => $model,
			'attributes' => [
				'name',
				'description:ntext',
				'ruleName',
				'data:ntext',
			],
		]);
		?>
		<div class="row">
			<div class="col-lg-5">
				<?= Yii::t('rbac-admin', 'Avaliable') ?>:
				<input id="search-avaliable"><br>
				<select id="list-avaliable" multiple size="20" style="width: 100%">
				</select>
			</div>
			<div class="col-lg-1">
				<br><br>
				<a href="#" id="btn-add" class="btn btn-success">&gt;&gt;</a><br>
				<a href="#" id="btn-remove" class="btn btn-danger">&lt;&lt;</a>
			</div>
			<div class="col-lg-5">
				<?= Yii::t('rbac-admin', 'Assigned') ?>:
				<input id="search-assigned"><br>
				<select id="list-assigned" multiple size="20" style="width: 100%">
				</select>
			</div>
		</div>


	</div>
</div>

<?php /*
AdminAsset::register($this);
$properties = Json::htmlEncode([
        'roleName' => $model->name,
        'assignUrl' => Url::to(['assign']),
        'searchUrl' => Url::to(['search']),
    ]);
$js = <<<JS
yii.admin.initProperties({$properties});

$('#search-avaliable').keydown(function () {
    yii.admin.searchRole('avaliable');
});
$('#search-assigned').keydown(function () {
    yii.admin.searchRole('assigned');
});
$('#btn-add').click(function () {
    yii.admin.addChild('assign');
    return false;
});
$('#btn-remove').click(function () {
    yii.admin.addChild('remove');
    return false;
});

yii.admin.searchRole('avaliable', true);
yii.admin.searchRole('assigned', true);
JS;
$this->registerJs($js); */

