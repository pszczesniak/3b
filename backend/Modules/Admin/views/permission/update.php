<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */

$this->title = Yii::t('rbac-admin', 'Update Permission') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'Update');
?>

<div class="view-window auth-item-update">
	<div class="view-window-title">
		<h5><?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Create Cms Category', ['modelClass' => 'Cms Page', ]), ['create'], ['class' => 'btn btn-xs btn-success btn-icon']) ?></h5>
		<div class="view-window-tools">
			<a class="collapse-link">
				<i class="glyphicon glyphicon-menu-up"></i>
			</a>
			<a href="#" data-toggle="dropdown" class="dropdown-toggle">
				<i class="glyphicon glyphicon-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">Config option 1</a>
				</li>
				<li><a href="#">Config option 2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="glyphicon glyphicon-time"></i>
			</a>
		</div>
	</div>
	<div class="view-window-content">
		<div class="row">
			<div div class="col-md-12">
				<?php
				echo $this->render('_form', [
					'model' => $model,
				]);
				?>
			</div>
		</div>
	</div>
</div>
