<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
//use mdm\admin\AutocompleteAsset;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">
    <?php $form = ActiveForm::begin(); ?>
		<div class="row">
			<div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?></div>
			<div class="col-md-6"><!--<?= $form->field($model, 'ruleName')->textInput(['id'=>'rule-name']) ?>--> <?= $form->field($model, 'ruleName')->dropDownList(common\models\User::getRolesAsListData(['Lsddpadre', 'Lsddlabor', 'Lsddmanager', 'Lsddproduce', 'Lsdd']), array('prompt'=>' ')  );  ?></div>
		</div>
        
        <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

        <!--<?= $form->field($model, 'data')->textarea(['rows' => 6]) ?>-->

        <div class="form-group align-right">
            <?php
            echo Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
            ?>
        </div>

    <?php ActiveForm::end(); ?>
</div> 
<?php /*
AutocompleteAsset::register($this);

$options = Json::htmlEncode([
    'source' => array_keys(Yii::$app->authManager->getRules())
]);
$this->registerJs("$('#rule-name').autocomplete($options);");*/