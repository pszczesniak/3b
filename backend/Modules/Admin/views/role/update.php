<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var mdm\admin\models\AuthItem $model
 */
$this->title = Yii::t('rbac-admin', 'Update Role').': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'Update');
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-company-index", 'title'=>Html::encode($this->title))) ?>

	<?php
		echo $this->render('_form', [
			'model' => $model,
		]);
	?>
<?php $this->endContent(); ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"lsdd-company-index", 'title'=>'Uprawnienia')) ?>
    <form id="permision-assign" method="post" action="/panel/pl/admin/role/update/manager">
		<div class="row">
			<?php/* var_dump($authorRole);*/ ?>
			<div class="access-panel col-md-6">
				<div class="inbox">
					<h2>Receptury</h2>
					<div class="row">
						<div class="col-xs-12">
							<div class="item"> 
								<input type="checkbox" value="viewsRecipe"  <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Receptury</p> 
								<input type="checkbox" value="createRecipe" <?= array_key_exists('createRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Dodawanie</p>
								<input type="checkbox" value="updateRecipe" <?= array_key_exists('updateRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Edycja</p>
								<input type="checkbox" value="deleteRecipe" <?= array_key_exists('deleteRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Usuwanie</p> 
								<input type="checkbox" value="priceRecipe"  <?= array_key_exists('priceRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?>> <p>Ceny</p>  
							</div>
						</div>
						<div class="col-md-6">
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Akceptuj recepturę'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Anuluj akceptację'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Produkuj recepturę'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Anuluj produkcję'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Utwórz na podstawie'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Utwórz kolejną wersję'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Skojarz z klientem'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Ceny prognozowane'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Wyślij'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Drukowanie'</p> </div>
						</div>
						<div class="col-md-6">
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Drukowanie cen prognozowanych'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Funkcja 'Dodaj dokument'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Mieszanie receptur</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Surowce'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Technologia produkcji'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Opakowania'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Kolor'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Dokumenty'</p> </div>
							<div class="item"> <input type="checkbox"> <p>Zakładka 'Klienci'</p> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="access-panel col-md-6">
				<div class="inbox">
					<h2>Słowniki</h2>
					<div class="item"> 
						<input type="checkbox" <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Surowce</p>            
						<input type="checkbox" <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Dodawanie</p>
						<input type="checkbox" <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Edycja</p>
						<input type="checkbox" <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Usuwanie</p> 
						<input type="checkbox" <?= array_key_exists('viewsRecipe', $authorRole)?'checked="checked" disabled="disabled"':'' ?> > <p>Ceny</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Funkcje materiałów</p> 
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Akcje materiałów</p>   
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> </div>
					<div class="item"> 
						<input type="checkbox"> <p>Rodzaje produktów</p>  
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Typy produktów</p>     
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Opakowania</p>         
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
						<input type="checkbox"> <p>Ceny</p>  
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Systemy pakowania</p>  
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Systemy barwienia</p>  
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Dostawcy</p>           
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p>
					</div>
					<div class="item"> 
						<input type="checkbox"> <p>Klienci</p>            
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p>
					</div>
				</div>
				<div class="inbox">
					<h2>Pozostałe</h2>
					<div class="item"> 
						<input type="checkbox"> <p>Produkcja</p>  
						<input type="checkbox"> <p>Dodawanie</p>
						<input type="checkbox"> <p>Edycja</p>
						<input type="checkbox"> <p>Usuwanie</p> 
					</div>			
				</div>
			</div>
		</div>
	</form>
<?php $this->endContent(); ?>
