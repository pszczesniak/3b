<?php
	use yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
	use yii\helpers\Json;
	use yii\helpers\Url;
    
    $this->title = 'Uprawnienia dla roli ' . $role;
    $this->params['breadcrumbs'][] = ['label' => 'Rejestr ról', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<!--<form id="permision-assign" method="post" action="/panel/pl/admin/role/update/manager">-->
<?php
	function generateItem($itemName, $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) {
		
		$item = 'name="grant[]" type="checkbox"';
		$item .= ' value="'.$itemName.'"';
		if(array_key_exists($itemName, $roleGrants))
			$item .=  ' checked="checked" ';
		/*if(array_key_exists($itemName, $roleGrants))
			$item .=  ' checked="checked" disabled="disabled" class="check-role-grant"';
		if(!array_key_exists($itemName, $roleLoggedUserGrants)  && $role != 'padre' && $role != 'labor')
			$item .=  ' disabled="disabled"';*/
		return $item;
	}
	
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-index", 'title' => Html::encode($this->title))) ?>
    <?php $formGrants = ActiveForm::begin(['id' => 'admin-grants', 'method' => 'post', 'action'=>Url::to(['/admin/role/grantsrole']).'?role='.$role]); ?>
        <div class="row" id="grands">
            <?php/* var_dump($roleGrants);*/ ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Użytkownicy</h2>
                            <div class="item"> 
                                <input <?= generateItem('userPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                                <input <?= generateItem('userAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                                <input <?= generateItem('userEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                                <input <?= generateItem('userDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            </div>
                        </div>
                    </div>
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Treść - strony</h2>
                            <div class="item"> 
                                <input <?= generateItem('pagePanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                                <input <?= generateItem('pageAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                                <input <?= generateItem('pageEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                                <input <?= generateItem('pageDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            </div>    
                        </div>
                    </div>
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Treść - kategorie</h2>
                            <div class="item"> 
                                <input <?= generateItem('categoryPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                                <input <?= generateItem('categoryAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                                <input <?= generateItem('categoryEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                                <input <?= generateItem('categoryDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            </div>   
                        </div>
                    </div>
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Treść - pozostałe</h2>
                            <div class="item"> 
                                <input <?= generateItem('widgetManage', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Dodatki</p> 
                                <input <?= generateItem('menuManage', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Menu</p>
                                <input <?= generateItem('galleryManage', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Galerie</p>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Blog - kategorie</h2>
                            <div class="item"> 
                                <input <?= generateItem('categoryBlogPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                                <input <?= generateItem('categoryBlogAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                                <input <?= generateItem('categoryBlogEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                                <input <?= generateItem('categoryBlogDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            </div>   
                        </div>
                    </div>
                    <div class="access-panel">
                        <div class="inbox">
                            <h2>Blog - wpisy</h2>
                            <div class="item"> 
                                <input <?= generateItem('postPanel', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role)  ?> > <p>Panel</p> 
                                <input <?= generateItem('postAdd', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Dodaj</p>
                                <input <?= generateItem('postEdit', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Edytuj</p>
                                <input <?= generateItem('postDelete', $roleGrants,  $userGrants, $roleLoggedUserGrants, $role) ?> > <p>Usuń</p> 
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group align-right">
            <?php
            echo Html::submitButton( Yii::t('app', 'Update'), [ 'class' =>  'btn btn-primary'])
            ?>
        </div>
    <?php ActiveForm::end(); ?>
<?= $this->endContent(); ?>