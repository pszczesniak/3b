<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model commom\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-admin-roles-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-roles']
    ]); ?>
    
    <div class="row">
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-roles', 'data-form' => '#filter-admin-roles-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>    
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
