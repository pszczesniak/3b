<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use mdm\admin\AdminAsset;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model yii\web\IdentityInterface */
/* @var $fullnameField string */

$userName = $model->{$usernameField};
if (!empty($fullnameField)) {
    $userName .= ' (' . ArrayHelper::getValue($model, $fullnameField) . ')';
}
$userName = Html::encode($userName);

$this->title = Yii::t('rbac-admin', 'Assignments') . ' : ' . $userName;;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $userName;
?>

<div class="view-window float-e-margins">
	<div class="view-window-title">
		<h5><?= $this->title ?></h5>
		<div class="view-window-tools">
			<a class="collapse-link">
				<i class="glyphicon glyphicon-menu-up"></i>
			</a>
			<a href="#" data-toggle="dropdown" class="dropdown-toggle">
				<i class="glyphicon glyphicon-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">Config option 1</a>
				</li>
				<li><a href="#">Config option 2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="glyphicon glyphicon-time"></i>
			</a>
		</div>
	</div>
	<div class="view-window-content">
		<div class="row">
			<div div class="col-md-12">
				<div class="row">
					<div class="col-lg-5">
						<?= Yii::t('rbac-admin', 'Avaliable') ?>:
						<input id="search-avaliable"><br>
						<select id="list-avaliable" multiple size="20" style="width: 100%">
						</select>
					</div>
					<div class="col-lg-1">
						<br><br>
						<a href="#" id="btn-assign" class="btn btn-success">&gt;&gt;</a><br>
						<a href="#" id="btn-revoke" class="btn btn-danger">&lt;&lt;</a>
					</div>
					<div class="col-lg-5">
						<?= Yii::t('rbac-admin', 'Assigned') ?>:
						<input id="search-assigned"><br>
						<select id="list-assigned" multiple size="20" style="width: 100%">
						</select>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php
AdminAsset::register($this);
$properties = Json::htmlEncode([
        'userId' => $model->{$idField},
        'assignUrl' => Url::to(['assign']),
        'searchUrl' => Url::to(['search']),
    ]);
$js = <<<JS
yii.admin.initProperties({$properties});

$('#search-avaliable').keydown(function () {
    yii.admin.searchAssignmet('avaliable');
});
$('#search-assigned').keydown(function () {
    yii.admin.searchAssignmet('assigned');
});
$('#btn-assign').click(function () {
    yii.admin.assign('assign');
    return false;
});
$('#btn-revoke').click(function () {
    yii.admin.assign('revoke');
    return false;
});

yii.admin.searchAssignmet('avaliable', true);
yii.admin.searchAssignmet('assigned', true);
JS;
$this->registerJs($js);

