<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model yii\web\IdentityInterface */
/* @var $fullnameField string */

$userName = $model->{$usernameField};
if (!empty($fullnameField)) {
    $userName .= ' (' . ArrayHelper::getValue($model, $fullnameField) . ')';
}
$userName = Html::encode($userName);

$this->title = Yii::t('rbac-admin', 'Assignments') . ' : ' . $userName;;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $userName;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-index', 'title'=>Html::encode($this->title))) ?>

	<div class="col-lg-5">
		<?= Yii::t('rbac-admin', 'Avaliable') ?>:
		<input id="search-avaliable"><br>
		<select id="list-avaliable" multiple size="20" style="width: 100%">
		</select>
	</div>
	<div class="col-lg-1">
		<br><br>
		<a href="#" id="btn-assign" class="btn btn-success">&gt;&gt;</a><br>
		<a href="#" id="btn-revoke" class="btn btn-danger">&lt;&lt;</a>
	</div>
	<div class="col-lg-5">
		<?= Yii::t('rbac-admin', 'Assigned') ?>:
		<input id="search-assigned"><br>
		<select id="list-assigned" multiple size="20" style="width: 100%">
		</select>
	</div>
<?php $this->endContent(); ?>

