<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-index', 'title'=>Html::encode($this->title))) ?>

	<div id="toolbar" class="btn-group">
		<?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Admin User'), ]), ['create'], ['class' => 'btn btn-success btn-icon'/*, 'data-toggle' => "modal", 'data-target' => "#modal-grid-view-dict-create"*/]) ?>
		<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
	</div>
	<?php
		Pjax::begin([
			'enablePushState'=>false,
		]);
		$columns = array_merge(
			[
				['class' => 'yii\grid\SerialColumn'],
				[
					'class' => 'yii\grid\DataColumn',
					'attribute' => $usernameField,
				],
			],
			$extraColumns,
			[
				[
					'class' => 'yii\grid\ActionColumn',
					'template'=>'{view}{update}{delete}',
					'contentOptions' => ['class' => 'table-actions'],
					'buttons' => [
						'view' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
									'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
							]);
						},
						'update' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
									'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
							]);
						},
						'delete' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
									'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
							]);
						}
					],
				],
			]
		);
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'tableOptions'=>Yii::$app->params['tableOptions'],
			'columns' => $columns,
		]);
		Pjax::end();
		?>
<?php $this->endContent(); ?>



