<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Eg\models\EgStaff */

$this->title = Yii::t('app', 'Create Eg Staff');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Eg Staff'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'eg-staff-create', 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'user' => $user
    ]) ?>

<?php $this->endContent(); ?>
