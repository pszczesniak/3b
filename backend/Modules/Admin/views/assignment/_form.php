<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user app\Modules\Eg\models\EgStaff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eg-staff-form">

    <?php $form = ActiveForm::begin(/*[
			'enableAjaxValidation' => false,
			//'errorMessageCssClass' => 'help-block'
		]*/); 
	?>
    <?php if($user->isNewRecord) { ?>
	<fieldset><legend>Dane autoryzacyjne</legend>
		<!--<?= $form->errorSummary($user, ['header' => '']); ?>-->
		<div class="row">
			<div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
				<?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
				<?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
				<?= $form->field($user, 'roleType')->dropDownList($user->getRolesAsListData(['Lsddpadre', 'Lsddlabor', 'Lsddmanager', 'Lsddproduce', 'Lsdd']), array('prompt'=>' ') );  ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
				<?= $form->field($user, 'password_hash')->passwordInput(['class' => 'form-control input-xlarge']) ?>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
				<?= $form->field($user, 'password_repeat')->passwordInput(['class' => 'form-control input-xlarge']) ?>
			</div>
		</div>
	</fieldset>
	<?php } ?>
	
	<div class="row">
		<div class="form-group col-xs-12 col-md-12 col-sm-12 col-lg-12">
			<?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
