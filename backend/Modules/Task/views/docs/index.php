<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Katalog dokumentów');
$this->params['breadcrumbs'][] = 'Sprawy';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-docs-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-docs" data-form="#filter-acc-docs-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="docs-filter" aria-expanded="true" href="#docs-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="docs-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
   
    <div id="toolbar-docs" class="btn-group toolbar-table-widget">
        <?= '' /*Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/claim/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-docs-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ])*/ ?>

        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-docs"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-docs"> 
        <table  class="table table-striped table-items table-widget"  id="table-docs"
                data-toolbar="#toolbar-docs" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                
                data-checkbox-header="true"
                data-maintain-selected="true"
                data-response-handler="responseHandlerChecked"
                
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-method="get"
				data-search-form="#filter-acc-docs-search"
                data-url=<?= Url::to(['/task/docs/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>                   
                    <th data-field="customer" data-sortable="true">Sprawa</th>
                    <th data-field="type" data-align="center">Typ</th>
                    <th data-field="title" data-sortable="true">Tytuł</th>
                    <th data-field="audit" data-sortable="false" data-align="center">Utworzono</th>
                    <th data-field="size" data-sortable="false" data-align="right">Rozmiar [MB]</th>
                    <th data-field="upload" data-sortable="false" data-width="20px"></th>
                    <!--<th data-field="actions" data-events="actionEvents" data-align="center" data-width="20px"></th>-->
                </tr>
            </thead>
            <tbody class="no-scroll">

            </tbody>            
        </table>
       
    </div>
</div>
<?php $this->endContent(); ?>

	