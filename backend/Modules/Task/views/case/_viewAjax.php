<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

    <div class="modal-body modal-body-max">
        <h3><?= ($model->type_fk == 1) ? 'Sprawa' : 'Projekt' ?></h3>
        <?php 
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    [                      
                        'attribute' => 'id_dict_case_status_fk',
                        'format'=>'raw',
                        'value' => $model->statusname,
                    ],   
                    [                      
                        'attribute' => 'notification_email',
                        'format'=>'raw',
                        'value' => ($model->notification_email) ? 'TAK' : 'NIE',
                        
                    ],   
                    [                      
                        'attribute' => 'id_customer_fk',
                        'format'=>'raw',
                        'value' => $model->customer['name'],
                    ],
                    'description', 
                    [                      
                        'attribute' => 'employees_list:html',
                        'format'=>'raw',
                        'label' => 'Pracownicy',
                        'value' => $model->employees_list,
                    ], 
                    [                      
                        'attribute' => 'departments_list:html',
                        'format'=>'raw',
                        'label' => 'Działy',
                        'value' => $model->departments_list,
                    ], 
                    [                      
                        'attribute' => 'files_list:html',
                        'format'=>'raw',
                        'label' => 'Dokumenty',
                        'value' =>$this->render('_files', ['model' => $model, 'type' => 3, 'onlyShow' => true])
                    ],  
                    [                      
                        'attribute' => 'correspondence_list:html',
                        'format'=>'raw',
                        'label' => 'Korespondencja',
                        'value' =>$this->render('_files', ['model' => $model, 'type' => 5, 'onlyShow' => true])
                    ],  
                    [                      
                        'attribute' => 'authority_carrying:html',
                        'format'=>'raw',
                        'label' => 'Organ prowadzący',
                        'value' => $model->authority_carrying,
                        'visible' => ($model->type_fk == 1)
                    ],    
                    [                      
                        'attribute' => 'authority_supervisory:html',
                        'format'=>'raw',
                        'label' => 'Organ nadzorujący',
                        'value' => $model->authority_supervisory,
                        'visible' => ($model->type_fk == 1)
                    ],         
                ],
            ]) 
        ?>
    </div>
        
            
    <div class="modal-footer"> 
		<?php if(!$edit) { ?><a href="/old/task/case/updateajax/<?= $model->id ?>?index=<?= $index ?>" class="btn btn-sm btn-primary viewEditModal" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class='glyphicon glyphicon-pencil'></i>Edycja">Edytuj</a> <?php } ?>
		<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>