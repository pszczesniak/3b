<div class="task-comments-list">
    <div class="task-comments-meta">
        <div class="task-comment-thumb">
            <?php $avatar = (is_file("uploads/employees/cover/".$model->id_employee_from_fk."/preview.jpg"))?"/uploads/employees/cover/".$model->id_employee_from_fk."/preview.jpg":"/images/default-user.png"; ?>
            <img src="<?= $avatar ?>" alt="user">
        </div>
        <div class="ask-comment-intro">
            <span class="poster-name"><?= $model->user ?></span>
            <!--<span class="poster-tagline">Dodał komentarz</span>-->
            <div class="commnet-post-date"> <?= $model->created_at ?>  </div>
        </div>
    </div>
    <div class="task-user-comments">
        <p>
            <?= $model->note ?>
        </p>
    </div>
</div>