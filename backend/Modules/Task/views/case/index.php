<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rozprawy');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Alert::widget() ?>
 
    <div class="ajax-table-items">	
	<div id="toolbar-cases" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["eventAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/case/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'event-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["eventExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/case/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'event-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-task-cases-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-cases"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-cases"
                data-toolbar="#toolbar-cases" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-task-cases-search"
                data-sort-name="id"
                data-sort-order="desc"
                data-method="get"
                data-url=<?= Url::to(['/task/case/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="className" data-visible="false">className</th>
                    <th data-field="name"  data-sortable="true" data-width="20%">Nazwa</th>
                    <th data-field="event_term"  data-sortable="true" data-width="10%">Termin</th>
                    <th data-field="client"  data-sortable="true" data-width="15%">Klient</th>
                    <th data-field="case"  data-sortable="true" data-width="15%">Postępowanie</th>
					<th data-field="leader"  data-sortable="true" data-width="15%">Prowadzący</th>
                    <th data-field="type"  data-sortable="true" data-width="5%">Waga</th>
                    <th data-field="status"  data-sortable="true" data-width="5%">Status</th>
                    <th data-field="delay"  data-sortable="true" data-width="5%">Opóźnienie</th>
                    <th data-field="actions" data-events="actionEvents" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Odwołane</td></tr>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nie ustawiono czasu</td></tr>
                </tbody>
            </table>
        </fieldset>
        
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 
</div>
<?php $this->endContent(); ?>
