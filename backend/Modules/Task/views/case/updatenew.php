<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;
use frontend\widgets\crm\SidesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = Yii::t('app', 'Edycja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rozprawy'), 'url' => Url::to(['/task/case/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/task/case/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa rozprawa</a>
            <?php if($model->id_dict_task_status_fk == 1 && $model->type_fk == 2) { ?> 
                <a href="<?= Url::to(['/task/case/close', 'id' => $model->id]) ?>" class="btn bg-green btn-flat"><span class="fa fa-calendar-check-o"></span> Zrobione</a> 
            <?php } ?>
            <a href="<?= Url::to(['/timeline/event', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Przebieg</a>
			<a href="<?= Url::to(['/task/case/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
        </div>
    </div>
</div>
<div class="grid">
    <div class="col-sm-12 col-md-9 col-xs-12">
        <?php if($model->id_dict_task_status_fk == 2 && $model->type_fk == 2) { ?> 
            <div class="alert bg-green btn-icon"><i class="fa fa-check"></i> Zadanie ma obecnie status <b>Zakończone</b><a href="<?= Url::to(['/task/case/open', 'id' => $model->id]) ?>" class="btn btn-sm bg-blue pull-right">wróć do poprzedniego statusu</a></div>
        <?php } ?>
        <?= Alert::widget() ?> 
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
            <?php if($model->delay > 0 && $model->id_dict_task_status_fk == 1 && $model->type_fk == 2) { echo '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>&nbsp;Przekroczyłeś czas wykonania o <b>'.$model->delay.'</b> h</div>'; } ?>
            
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item active" aria-expanded="false"><a href="#basic" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-pencil"></i></span><span class="hidden-xs"> Ogólne</span></a></li>
                <li role="presentation" class="nav-item"><a href="#docs" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-files-o"></i></span> <span class="hidden-xs">Dokumenty</span></a></li>
				<li role="presentation" class="nav-item"><a href="#notice" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-bell"></i></span> <span class="hidden-xs">Powiadomienia</span></a></li>
                <li role="presentation" class="nav-item"><a href="#sides" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-users"></i></span> <span class="hidden-xs">Uczestnicy</span></a></li>
            </ul>
            <div class="tab-content m-t-0">
                <div role="tabpanel" class="tab-pane fade in active" id="basic" aria-expanded="false">
                    <?= $this->render('_formnew', [
                        'model' => $model, 'lists' => $lists, 'employees' => $employees, 'employeeKind' => $employeeKind, 'all' => $all
                    ]) ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="docs" aria-expanded="false">
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="notice" aria-expanded="false">
                    <?= $this->render('_reminder', ['model' => $model,]) ?>
                    <fieldset><legend>Dodatkowe powiadomienia</legend>
                        <?= \frontend\widgets\alert\AlertsTable::widget(['id' => $model->id, 'type' => 1]) ?>
                    </fieldset>
                </div>
                <div role="tabpanel" class="tab-pane" id="sides" aria-expanded="false">
                    <?= SidesTable::widget(['dataUrl' => Url::to(['/task/event/sides', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addemember', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
                </div>
            </div>
        <?php $this->endContent(); ?>
    </div>
    <div class="col-sm-12 col-md-3 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading bg-blue"> 
                <span class="panel-title"> <span class="fa fa-comments"></span> Komentarze </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-task-update-notes" aria-expanded="true" href="#cal-task-update-notes" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body" id="cal-task-update-notes">
                <?= $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) ?>
            </div>
        </div>
    </div>
</div>

