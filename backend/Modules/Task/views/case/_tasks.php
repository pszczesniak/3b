<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
    <div class="ajax-table-items">	 
        <div id="toolbar-event" class="btn-group">
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('app', 'New')), Url::to(['/task/event/createajax', 'id' => $id]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'data-target' => "#modal-grid-event", 
                         'data-form' => "event-form", 
                         'data-table' => "table-events",
                         'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                        ]) ?>
            <!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
        </div>
        <table class="table table-striped table-items"  id="table-events"
                data-toolbar="#toolbar-event" 
                data-toggle="table" 
                data-show-refresh="true"
                data-show-toggle="false" 
                data-show-columns="false" 
                data-show-export="false" 
                data-filter-control="false"
                data-url=<?= Url::to(['/task/case/events', 'id'=>$id]) ?>>	 
        </table>
    </div>
</div>
<div class="modal-footer"> 
    <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
</div>
    

    

    



