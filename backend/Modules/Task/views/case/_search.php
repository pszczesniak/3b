<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-task-cases-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-cases']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_dict_task_status_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Task\models\CalTask::listStatus(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-cases', 'data-form' => '#filter-task-cases-search'  ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-cases', 'data-form' => '#filter-task-cases-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-cases', 'data-form' => '#filter-task-cases-search'  ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_case_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups(-1), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-cases', 'data-form' => '#filter-task-cases-search'  ] ) ?></div>
        <div class="col-sm-4 col-md-6 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-cases', 'data-form' => '#filter-task-cases-search' ]) 
                                                            ->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-caltasksearch-date_from">
				<label for="caltasksearch-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="caltasksearch-date_from" name="CalTaskSearch[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-caltasksearch-date_to">
				<label for="caltasksearch-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="caltasksearch-date_to" name="CalTaskSearch[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
	document.getElementById('caltasksearch-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltasksearch-id_case_fk').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }

</script>
