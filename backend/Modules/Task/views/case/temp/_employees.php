<div class="list-group contact-group">
    <?php 
        foreach($employees as $key => $employee) {
            $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee->id_employee_fk."/preview.jpg"))?"/uploads/employees/cover/".$employee->id_employee_fk."/preview.jpg":"/images/default-user.png"; 
            echo '<a href="#" class="list-group-item">'
                    .'<div class="media">'
                        .'<div class="media-avatar pull-left">'
                            .'<img class="img-circle" src="'.$avatar.'" alt="Avatar">'
                        .'</div>'
                        .'<div class="media-body">'
                            .'<h4 class="media-heading">'.$employee->employee['fullname'].' <small>'.$employee->employee['typename'].'</small></h4>'
                            .'<div class="media-content">'
                                .'<i class="fa fa-map-marker"></i>'. ( $employee->employee['address'] ? $employee->employee['address'] : 'brak danych' ) 
                                .'<ul class="list-unstyled">'
                                    .'<li><i class="fa fa-mobile"></i> '.( ($employee->employee['phone']) ? $employee->employee['phone'] : 'brak danych') .'</li>'
                                    .'<li><i class="fa fa-envelope-o"></i>'.( ($employee->employee['email']) ? $employee->employee['email'] : 'brak danych') .'</li>'
                                .'</ul>'
                            .'</div>'
                        .'</div>'
                    .'</div>'
                .'</a>';
        }
    ?>

</div>