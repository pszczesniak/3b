<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */

$this->title = Yii::t('app', 'Zadanie #'.$model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zadania'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-update', 'title'=>Html::encode($this->title))) ?>
    <div class="task-top-toolbar clearfix">
        <div class="btn-group pull-left">
            <button type="button" class="btn btn-default task-sidebar-toggle"><i class="fa fa-indent"></i>  </button>
        </div>
        
        <div class="btn-group pull-right">
            
            <button type="button" class="btn btn-default"><i class="fa fa-plus"></i> Nowe zadanie</button>
        </div>
    </div>
    <div class="task-container clearfix apps-container">
        <!--<div class="task-sidebar apps-panel-scroll" style="height: 810px;">
            <ul class="task-box-list">
                <li><a href="#"><i class="fa fa-tasks"></i><span>All Task</span><span class="new-item-count">20</span></a>
                </li>
                <li><a href="#"><i class="fa fa-file-text"></i><span>New Task</span></a>
                </li>
                <li><a href="#"><i class="fa fa-clipboard"></i><span>To Do</span></a>
                </li>
                <li><a href="#"><i class="fa fa-undo"></i><span>Overdue</span></a>
                </li>
                <li><a href="#"><i class="fa fa-check"></i><span>Done</span></a>
                </li>
                <li><a href="#"><i class="fa fa-archive"></i><span>Archive</span></a>
                </li>
            </ul>
        </div>-->
        <div class="task-content apps-panel-scroll" >
            <div class="task-list-toolbar list-toolbar clearfix">
                <!--<form class="task-search-form list-search-form">
                    <input type="text" class="form-control list-search-input" placeholder="Szukaj">
                    <span class="s-input-close"><i class="ico-cross"></i></span>
                </form>-->
                <label>Twoje zadania</label>
            </div>
            <div class="task-list apps-panel-scroll" >
                <?= $this->render('_task_list') ?>
            </div>
            <div class="task-body apps-panel-scroll" >
                <?= $this->render('_task_body', ['model' => $model ]) ?>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
