 <div class="task-list-item">
    <div class="task-list-action">
        <div class="icheckbox_minimal checked" style="position: relative;"><input type="checkbox" name="task-select" class="task-select" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
    </div>
    <div class="task-list-details">
        <div class="task-intro">
            <h4>Analiza dokumentów </h4>
            <p>
                Przed spotkaniem z klientem prosze dokładnie zapoznać się z...
            </p>
            <div class="task-date">
                <label class="label label-info"> Nowe</label>
                <label class="label label-warning"> Wysokie</label>
                <div class="task-due-date">
                    <i class="fa fa-clock-o"></i>Dodano: 2016-07-01 12:34:30
                </div>
            </div>
        </div>
    </div>
    <div class="task-status">
        <span class="epie-chart" data-percent="0" data-barcolor="#f9a825" data-tcolor="#e0e0e0" data-scalecolor="#e0e0e0" data-linecap="butt" data-linewidth="2" data-size="60" data-animate="1"><span class="percent" style="width: 60px; line-height: 60px;">0</span>
        <canvas height="60" width="60"></canvas></span>
    </div>
</div>
<div class="task-list-item task-completed">
    <div class="task-list-action">
        <div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" name="task-select" class="task-select" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
    </div>
    <div class="task-list-details">
        <div class="task-intro">
            <h4>Przygotowanie mowy </h4>
            <p>
                Proszę o sporządzenie listy argumentów...
            </p>
            <div class="task-date">
                <div class="task-due-date">
                    <i class="fa fa-clock-o"></i>Due By: Jun 16 at 12:00 PM
                </div>
                <div class="task-complete-date">
                    <i class="fa fa-calendar-o"></i>Completed By: Jun 18 at 11:00 PM
                </div>
            </div>
        </div>
    </div>
    <div class="task-status">
        <span class="epie-chart" data-percent="100" data-barcolor="#43a047" data-tcolor="#e0e0e0" data-scalecolor="#e0e0e0" data-linecap="butt" data-linewidth="2" data-size="60" data-animate="1"><span class="p-done" style="width: 60px; height: 60px; line-height: 60px; color: rgb(67, 160, 71);"><i class="fa fa-check"></i></span>
        <canvas height="60" width="60"></canvas></span>
    </div>
</div>
<div class="task-list-item task-due">
    <div class="task-list-action">
        <div class="icheckbox_minimal" style="position: relative;"><input type="checkbox" name="task-select" class="task-select" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -30%; left: -30%; display: block; width: 160%; height: 160%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
    </div>
    <div class="task-list-details">
        <div class="task-intro">
            <h4>Termin rozprawy </h4>
            <p>
                Wstępny termin ugody...
            </p>
            <div class="task-date">
                <label class="label label-danger"> Po terminie</label>
                <label class="label label-warning"> Wysoki</label>
                <div class="task-due-date">
                    <i class="fa fa-clock-o"></i>Due By: Jun 16 at 12:00 PM
                </div>
            </div>
        </div>
    </div>
    <div class="task-status">
        <span class="epie-chart" data-percent="30" data-barcolor="#f9a825" data-tcolor="#dddddd" data-scalecolor="#e0e0e0" data-linecap="butt" data-linewidth="2" data-size="60" data-animate="1"><span class="percent" style="width: 60px; line-height: 60px;">30</span>
        <canvas height="60" width="60"></canvas></span>
    </div>
</div>
