<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="validation-form" class="alert none"></div>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body modal-body-max">
		<fieldset><legend>Podstawowe</legend>
            <div class="content well">
                <div class="grid">
                    <div class="col-md-6">
                        <?= $form->field($model, 'type_fk')->radioList([1 => 'Rozprawa', 2 => 'Zadanie'], 
                                            ['class' => 'btn-group', 'data-toggle' => "buttons",
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                    return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                        Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                },
                                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'id_dict_task_type_fk')->radioList(\backend\Modules\Task\models\CalTask::listTypes(), 
                                            ['class' => 'btn-group', 'data-toggle' => "buttons",
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                    return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                                        Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                },
                                        ]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </fieldset>
        <fieldset class="collapsible"><legend>Czas i miejsce</legend>
            <div class="content well">
                <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'all_day', ['template' => '{input} {label}'])->checkbox() ?>
                <div class="grid">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_from" class="control-label">Start</label>
                            <div class='input-group date' id='datetimepicker_start'>
                                <input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $model->date_from ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD  HH:mm',  });
                        });
                    </script>
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_to" class="control-label">Koniec</label>
                            <div class='input-group date' id='datetimepicker_end'>
                                <input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $model->date_to ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD  HH:mm' });
                        });
                    </script>
                </div>
            </div>
        </fieldset>
        <fieldset class="collapsible"><legend>Dodatkowe</legend>
            <div class="content well">
                <?= $form->field($model, 'id_dict_task_category_fk')->radioList(\backend\Modules\Task\models\CalTask::listCategory(), 
                            ['class' => 'btn-group', 'data-toggle' => "buttons",
                            'item' => function ($index, $label, $name, $checked, $value) {
                                    return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                        Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                },
                        ]) ?>
                <?= $form->field($model, 'id_dict_task_status_fk')->radioList(\backend\Modules\Task\models\CalTask::listStatus(), 
                                ['class' => 'btn-group', 'data-toggle' => "buttons",
                                'item' => function ($index, $label, $name, $checked, $value) {
                                        return '<label class="btn btn-sm  btn-primary' . ($checked ? ' active' : '') . '">' .
                                            Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                    },
                        ]) ?>
            </div>
        </fieldset>
        <fieldset class="collapsible"><legend>Opis</legend>
            <div class="content well">
                <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
            </div>
        </fieldset>
		<fieldset class="collapsible"><legend>Pracownicy</legend>
            <div class="content well">
                <?= $this->render('_employeesList', ['employees' => $employees, 'case' => $model->id_case_fk]) ?>
            </div>
        </fieldset>
        <fieldset class="collapsible"> <legend>Pliki</legend>
            <div class="content well">
                <?= $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) ?>
            </div>
        </fieldset>

	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>


