<?php

use yii\helpers\Html;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */

$this->title = Yii::t('app', 'Nowa rozprawa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rozprawy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid">
    <div class="col-sm-6 col-md-7 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-event-create', 'title'=>Html::encode($this->title))) ?>
            <?= $this->render('_form'.((Yii::$app->params['env'] == 'dev') ? 'new' : ''), [
			'model' => $model, 'lists' => $lists, 'employees' => $employees, 'employeeKind' => $employeeKind, 'all' => $all
		]) ?>
        <?php $this->endContent(); ?>
    </div>
    <div class="col-sm-6 col-md-5 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading bg-purple"> 
                <span class="panel-title"> <span class="fa fa-paperclip"></span> Dokumenty </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-docs" aria-expanded="true" href="#crm-customer-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-docs">
                <?php /* $this->render('_files', ['model' => $model, 'type' => 3, 'onlyShow' => false]) */ ?>
                <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
            </div>
        </div>
    </div>
</div>

