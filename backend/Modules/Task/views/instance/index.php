<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Postępowania');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-instances" data-form="#filter-task-instances-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 'advanced']); ?>
        </div>
    </fieldset>
    
    <?= Alert::widget() ?>
	<div id="toolbar-instances" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/manage/addinstance', 'id' => 0]) , 
					['class' => 'btn btn-success btn-icon gridViewModal', 
					 'id' => 'case-create',
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/instance/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-task-instances-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?php if(count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 && 1==2) { ?>
                <div class="btn-group" id="actionsDates-btn">
                    <button type="button" class="btn btn-icon bg-navy"><i class="fa fa-cogs"></i>Operacje grupowe (<span id="settle-counter">0</span>)</button>
                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" id="postingDates-list">
                        <?= '<li class="settle-actions">'
                               . '<a href="'.Url::to(['/task/manage/addemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Dołącz pracownika do sprawy">dołącz pracownika</a>'
                            .'</li>' ?>
                        <?= '<li class="settle-actions">'
                               . '<a href="'.Url::to(['/task/manage/delemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Odłącz pracownika od sprawy">odłącz pracownika</a>'
                            .'</li>' ?>
                        <?= '<li class="settle-actions">'
                           . '<a href="'.Url::to(['/task/manage/changestatus']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Zmień status">zmień status</a>'
                        .'</li>' ?>
                        <!--<li role="separator" class="divider"></li>-->
                    </ul>
                </div>
            <?php } ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-instances"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-instances"
                data-toolbar="#toolbar-instances" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-task-instances-search"
                data-url=<?= Url::to(['/task/instance/data', 'type' => 'normal']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="sygn" data-sortable="true" >Sygnatura</th>
					<th data-field="name" data-sortable="true" data-align="center">Nazwa</th>
					<th data-field="address" data-sortable="true" data-align="center">Organ</th>
                    <th data-field="case" data-sortable="true" >Sprawa</th>
                    <th data-field="status" data-sortable="true" >Status</th>
                    <th data-field="mode" data-sortable="true" >Tryb</th>
                    <th data-field="active" data-align="center"><i class="fa fa-map-marker text--purple" data-title="Instancja aktywna" data-toggle="tooltip"></i></th>
                    <th data-field="lawsuit" data-align="center">Pozew</th>
                    <th data-field="warrant" data-align="center">Nakaz</th>
                    <th data-field="clause" data-align="center">Klauzula</th>
                    <!--<th data-field="status"  data-sortable="true" data-align="center" >Status</th>-->
                    <th data-field="actions" data-events="actionEvents" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

