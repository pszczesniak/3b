<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\crm\SidesTable;
use frontend\widgets\tasks\ClaimsTable;
use frontend\widgets\tasks\InstancesTable;

$this->title = Yii::t('app', 'Aktualizacja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/matter/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->no_label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/matter/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa sprawa</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/matter/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>
<div class="grid">
    
    <div class="col-sm-12 col-md-9 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item active" aria-expanded="false"><a href="#basic" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-pencil"></i></span><span class="hidden-xs"> Ogólne</span></a></li>
                <li role="presentation" class="nav-item"><a href="#docs" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-files-o"></i></span> <span class="hidden-xs">Dokumenty</span></a></li>
				<li role="presentation" class="nav-item"><a href="#tasks" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-tasks"></i></span> <span class="hidden-xs">Zdarzenia</span></a></li>
                <li role="presentation" class="nav-item"><a href="#instances" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-university"></i></span> <span class="hidden-xs">Instancje</span></a></li>                
                <li role="presentation" class="nav-item"><a href="#sides" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-users"></i></span> <span class="hidden-xs">Uczestnicy</span></a></li>
                <li role="presentation" class="nav-item"><a href="#claims" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-flash"></i></span> <span class="hidden-xs">Roszczenia</span></a></li>            
            </ul>
            <div class="tab-content m-t-0">
                <div role="tabpanel" class="tab-pane fade in active" id="basic" aria-expanded="false">
                    <?= $this->render('_formnew', [
                        'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $employeeKind
                    ]) ?>
                </div>
				<div role="tabpanel" class="tab-pane fade" id="docs" aria-expanded="false">
					<?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tasks" aria-expanded="false">
                    <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]) ]) ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="instances" aria-expanded="false">
                    <?= InstancesTable::widget(['dataUrl' => Url::to(['/task/manage/instances', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addinstance', 'id' => $model->id]) ]) ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="sides" aria-expanded="false">
                    <?= SidesTable::widget(['dataUrl' => Url::to(['/task/manage/sides', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addmember', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="claims" aria-expanded="false">
                    <?= ClaimsTable::widget(['dataUrl' => Url::to(['/task/manage/claims', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addclaim', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
                </div>
            </div>
        <?php $this->endContent(); ?>
      
    </div>
    <div class="col-sm-12 col-md-3 col-xs-12">
        <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-grey"> 
                <span class="panel-title"> 
                    <span class="fa fa-step-forward"></span> Status 
                </span> 
                <div class="panel-heading-menu pull-right">
                    <span class="dropdown">
                        <a href="#" class="fa fa-cog" id="dropdownStatusMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></a> 
                        <ul class="dropdown-menu" aria-labelledby="dropdownStatusMenu">
                            <li class="dropdown-header">Cykl sprawy</li>
                            <li role="separator" class="divider"></li>
                            <?= ($model->id_dict_case_status_fk == 1) ? '<li class="disabled"><a href="#"><span class="text--blue lead"><strong>'.(( Yii::$app->params['env'] == 'dev' ) ? 'Zlecenie' : 'Nowa').'</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]).'?status=1" class="modalConfirm"><span class="text--blue">'.(( Yii::$app->params['env'] == 'dev' ) ? 'Zlecenie' : 'Nowa').'</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 2) ? '<li class="disabled"><a href="#"><span class="text--orange lead"><strong>'.(( Yii::$app->params['env'] == 'dev' ) ? 'W toku' : 'Plan').'</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=2" class="modalConfirm"><span class="text--orange">'.(( Yii::$app->params['env'] == 'dev' ) ? 'W toku' : 'Plan').'</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 3) ? '<li class="disabled"><a href="#"><span class="text--purple lead"><strong>'.(( Yii::$app->params['env'] == 'dev' ) ? 'Proces' : 'W toku').'</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=3" class="modalConfirm"><span class="text--purple">'.(( Yii::$app->params['env'] == 'dev' ) ? 'Proces' : 'W toku').'</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 4) ? '<li class="disabled"><a href="#"><span class="text--green lead"><strong>Zakończona</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=4" class="modalConfirm"><span class="text--green">Zakończona</a></span></li>' ?>
                        </ul>
                    </span> 
					<a aria-controls="matter-update-status" aria-expanded="true" href="#matter-update-status" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                    
                </div>
            </div>
            
            <div class="panel-body collapse in" id="matter-update-status">
                
                <div class="alert <?= $colors[$model->id_dict_case_status_fk] ?>" > Obceny status: <b><?= $model->statusname ?></b></div>
                <div class="align-right">
                    <?php if($model->id_dict_case_status_fk != 4) { ?> 
						<a href="<?= Url::to(['/task/matter/status', 'id' => $model->id]) ?>" class="btn <?= $colors[($model->id_dict_case_status_fk+1)] ?> btn-flat modalConfirm">
							<span class="fa fa-step-forward"></span> Status [<?= \backend\Modules\Task\models\CalCase::listStatus('normal')[($model->id_dict_case_status_fk+1)] ?>]
						</a> <?php } ?>
                </div>
            </div>
        </div>
        <?php if( in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) || \Yii::$app->session->get('user.isManager') == 1 ) { ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-purple"> 
                <span class="panel-title"> <span class="fa fa-calculator"></span> Rozliczenie </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-matter-update-order" aria-expanded="true" href="#cal-matter-update-order" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="cal-matter-update-order">
                <?= $this->render('_order', ['model' => $model,]) ?>
            </div>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-blue"> 
                <span class="panel-title"> <span class="fa fa-comments"></span> Notatki </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-note" aria-expanded="true" href="#crm-customer-update-note" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-note">
                <?= $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) ?>
            </div>
        </div>


    </div>
</div>



