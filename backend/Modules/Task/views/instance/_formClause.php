<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-instances"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">

	<div class="grid">
		<div class="col-sm-3 col-xs-6">
			<div class="form-group field-caseinstanceclause-release_date">
				<label for="caseinstanceclause-release_date" class="control-label">Data wniosku</label>
				<input type='text' class="form-control date-calendar" id="date-release_date" name="CaseInstanceClause[application_date]" value="<?= $model->application_date ?>" />
				<div class="help-block"></div>
			</div>
		</div>
		<div class="col-sm-3 col-xs-6">
			<div class="form-group field-caseinstanceclause-delivery_date">
				<label for="caseinstanceclause-delivery_date" class="control-label">Data doręczenia</label>
				<input type='text' class="form-control date-calendar" id="date-delivery_date" name="CaseInstanceClause[delivery_date]" value="<?= $model->delivery_date ?>" />
				<div class="help-block"></div>
			</div>
		</div>
        <div class="col-sm-3 col-xs-6">
			<div class="form-group field-caseinstanceclause-post_date">
				<label for="caseinstanceclause-post_date" class="control-label">Data nadania</label>
				<input type='text' class="form-control date-calendar" id="date-post_date" name="CaseInstanceWarrant[post_date]" value="<?= $model->post_date ?>" />
				<div class="help-block"></div>
			</div>
		</div>
	</div> 
    <fieldset><legend>Opłaty i koszty</legend>
            <div class="grid">
            <div class="col-sm-3 col-xs-6"><?= $form->field($model, 'cost_replacement')->textInput( ['class' => 'form-control number'] ) ?></div>
            <div class="col-sm-3 col-xs-6"><?= $form->field($model, 'cost_court')->textInput( ['class' => 'form-control number'] ) ?></div>
        </div>
    </fieldset>
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('.date-calendar').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    });
</script>    
