<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;
use frontend\widgets\crm\SidesTable;
use frontend\widgets\tasks\ClaimsTable;
use frontend\widgets\tasks\InstancesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

$this->title = $model->no_label;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/matter/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/matter/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa sprawa</a>
		    <?php } ?>
			<a href="<?= Url::to(['/timeline/set', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Przebieg</a>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) && 1 == 2) { ?>
				<a href="<?= Url::to(['/task/matter/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
		</div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-matter-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid view-project">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <section class="panel">
                <div class="project-heading <?= ($model->type_fk == 1) ? ' bg-purple' : 'bg-teal' ?>">
                    <strong><?= ($model->type_fk == 1) ? 'Sprawa' : 'Projekt '.$model->type_fk.'' ?></strong>
                    <?php if( ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ) { ?>
						<a href="<?= Url::to(['/task/matter/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-flat pull-right" style="margin-top:-8px"><span class="fa fa-pencil"></span> Edytuj</a>
					<?php } ?>
				</div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p><span class="bold">Etykieta</span>: <b><?= $model->no_label ?></b></p>
							<p><i><?= $model->subname ?></i></p>
                        </div>
                        <div class="row-details">
                            <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
                            <p><span class="bold">Status</span>: <span class="label <?= $colors[$model->id_dict_case_status_fk] ?>"><?= $model->statusname ?></span></p>
                        </div>

                        <div class="row-details">
                            <p>
                                <span class="bold">Prowadzący</span>: 
                                <?php if(!$model->leadingEmployee) { echo 'brak informacji'; } else { ?>
                                    <div class="grid profile">
                                        <div class="col-md-4">
                                            <div class="profile-avatar">
                                                <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg"))?"/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg":"/images/default-user.png"; ?>
                                                <img src="<?= $avatarContact ?>" alt="Avatar">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="profile-name">
                                                <h3><?= $model->leadingEmployee['fullname'] ?></h3>
                                                <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingEmployee['kindname']) ? $model->leadingEmployee['typename'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingEmployee['phone']) ? $model->leadingEmployee['phone'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingEmployee['email']) ? Html::mailto($model->leadingEmployee['email'], $model->leadingEmployee['email']) : 'brak danych'  ?></p>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                
                                <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
                           </p>
                        </div>
                        <!--<div class="row-details">
                            <div class="grid profile">
                                <div class="col-md-4">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="profile-name">
                                        <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['statusname'] ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="row-details">
                            <p>
                                <span class="bold">Osoba kontaktowa</span>: 
                                <?php if(!$model->leadingContact) { echo 'brak informacji'; } else { ?>
                                    <div class="grid profile">
                                        <div class="col-md-4">
                                            <div class="profile-avatar">
                                                <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg"))?"/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg":"/images/default-user.png"; ?>
                                                <img src="<?= $avatarContact ?>" alt="Avatar">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="profile-name">
                                                <h3><?= $model->leadingContact['fullname'] ?></h3>
                                                <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingContact['position']) ? $model->leadingContact['position'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingContact['phone']) ? $model->leadingContact['phone'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingContact['email']) ? Html::mailto($model->leadingContact['email'], $model->leadingContact['email']) : 'brak danych'  ?></p>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
                           </p>
                        </div>
                        <div class="row-details">
                            <p>
                                <span class="bold">Bieżąca instancja</span>: 
                                <ul>
                                    <li><label>Nazwa:</label>&nbsp;<?= $model->a_instance_name ?></li>
                                    <li><label>Sygnatura:</label>&nbsp;<?= $model->a_instance_sygn ?></li>
                                    <li><label>Organ:</label>&nbsp;<?= $model->a_instance_sygn ?></li>
                                    <li><label>Kontakt:</label>&nbsp;<?= $model->a_instance_contact ?></li>
                                </ul>
                                
                           </p>
                        </div>
                        <div class="row-details">
                            <p>
                                <span class="bold">Działy</span>: 
                                <?= $model->departments_list ?>
                           </p>
                        </div>
                    </div>
                </div><!--/project-body-->
            </section>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="grid">
                <div class="col-sm-6 col-xs-12">
                    <fieldset><legend class="bg-teal">Klient <a href="<?= Url::to(['/crm/customer/card', 'id' => $model->id_customer_fk]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka klienta" title="Wizytówka klienta"><i class="fa fa-address-card text--lightgrey"></i></a><span class="pull-right label bg-white"><?= $model->role ?></span></legend>
                        <div class="grid profile">
                            <div class="col-md-4">
                                <div class="profile-avatar">
                                    <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                    <img src="<?= $avatar ?>" alt="Avatar">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="profile-name">
                                    <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                                    <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['role'] ?></p>

                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <fieldset>
                        <legend class="bg-red">Strona przeciwna
                            <?php if($model->id_opposite_side_fk) { ?>
                                <a href="<?= Url::to(['/crm/side/card', 'id' => $model->id_opposite_side_fk]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka strony przeciwnej" title="Wizytówka strony przeciwnej"><i class="fa fa-address-card text--lightgrey"></i></a>
                            <?php } ?>
                        </legend>
                        <?php if($model->id_opposite_side_fk) { ?>
						<div class="grid profile">
                            <div class="col-md-4">
                                <div class="profile-avatar">
                                    <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_opposite_side_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                    <img src="<?= $avatar ?>" alt="Avatar">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="profile-name">
                                    <h5><a href="<?= Url::to(['/crm/side/view', 'id' => $model->id_opposite_side_fk]) ?>"><?= $model->side['name'] ?></a></h5>
                                    <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->side['role'] ?></p>
                                </div>
                            </div>
                        </div>
						<?php } else { ?>
						<div class="alert alert-info">Nie ustawiono strony przeciwnej</div>
						<?php } ?>
                    </fieldset>
                </div>
            </div>
            <!--<div class="profile-about">
                <h4>Dodatkowe informacje</h4>

                <div class="table-responsive about-table">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Utworzono</th>
                                <td><?= \common\models\User::findOne($model->created_by)->fullname . '[ '.$model->created_at.' ]' ?></td>
                            </tr>
                            <?php if($model->updated_by) { ?>
                            <tr>
                                <th>Zmodyfikowano</th>
                                <td><?= \common\models\User::findOne($model->updated_by)->fullname . '[ '.$model->updated_at.' ]' ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <th>Opis</th>
                                <td><?= $model->description ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>-->
            <div class="panel with-nav-tabs panel-default">
                <?php $notes = $model->notes; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                        <li><a data-toggle="tab" href="#tab3"><i class="fa fa-university"></i><span class="panel-tabs--text">Instancje</span> </a></li>
                       <!-- <li><a data-toggle="tab" href="#tab3"><i class="fa fa-inbox"></i><span class="panel-tabs--text">Korespondencja </span></a></li> -->
						<li><a data-toggle="tab" href="#tab4"><i class="fa fa-comments"></i><span class="panel-tabs--text">Notatki </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>
						<li><a data-toggle="tab" href="#tab5"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Zdarzenia</span></a></li>
                        <li><a data-toggle="tab" href="#tab6"><i class="fa fa-users"></i><span class="panel-tabs--text">Uczestnicy</span></a></li>
                        <li><a data-toggle="tab" href="#tab7"><i class="fa fa-flash text--red"></i><span class="panel-tabs--text text--red">Roszczenia</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?php /*$this->render('_employees', ['employees' => $model->employees])*/ ?>
                            <?= EmployeesBlock::widget(['employees' => $model->employees, 'employeesLock' => $model->employeeslock]) ?>
						</div>
						<div class="tab-pane" id="tab2">
                            <?php /*$this->render('_files', ['model' => $model, 'type' => 3, 'onlyShow' => true])*/ ?>
                            <!--<div class="align-right"><a href="<?= Url::to(['/task/matter/download', 'id' => $model->id]) ?>" class="btn btn-sm btn-icon bg-teal"><i class="fa fa-download"></i>pobierz wszystkie dokumenty</a></div>-->
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => true, 'download' => Url::to(['/task/matter/download', 'id' => $model->id]) ]) ?>
						</div>
                        <div class="tab-pane" id="tab3">
                            <?= InstancesTable::widget(['dataUrl' => Url::to(['/task/manage/instances', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addinstance', 'id' => $model->id]) ]) ?>
						</div>
						<div class="tab-pane" id="tab4">
                            <?php /* $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes])*/ ?>
                            <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/task/matter/note', 'id' => $model->id])]) ?>
						</div>
                        <div class="tab-pane" id="tab5">
                             <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
						</div>
                        <div class="tab-pane" id="tab6">
                            <?= SidesTable::widget(['dataUrl' => Url::to(['/task/manage/sides', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addmember', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
						</div>
                        <div class="tab-pane" id="tab7">
                            <?= ClaimsTable::widget(['dataUrl' => Url::to(['/task/manage/claims', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addclaim', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

<?php $this->endContent(); ?>
