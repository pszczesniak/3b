<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-task-instances-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-instances']
    ]); ?>

    <div class="grid">
        <div class="col-md-3 col-sm-3 col-xs-6"><?= $form->field($model, 'instance_sygn')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search']) ?></div>                
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'instance_address_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search'  ] ) ?></div>
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_dict_instance_status_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listStatuses( ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search'] ) ?> </div>
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'is_active', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listActive( ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search'] ) ?> </div>
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_dict_instance_mode_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listmodes( ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search'] ) ?> </div>
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search' ] ) ?></div>
        <div class="col-md-6 col-sm-6 col-xs-6"><?= $form->field($model, 'instance_name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-instances', 'data-form' => '#filter-task-instances-search']) ?></div>                              
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
