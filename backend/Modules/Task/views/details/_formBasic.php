<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-cases"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'time_limit', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} '])->textInput( [] ) ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="grid grid--0">
                <div class="col-xs-9"><?= $form->field($model, 'budget_limit')->textInput( [] ) ?></div>
                <div class="col-xs-3"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [ 'class' => 'form-control select2'] ) ?></div>
            </div> 
        </div>
    </div>
    <div class="grid">
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <label for="calcasedetails-date_from" class="control-label">Data rozpoczęcia</label>
                <div class='input-group date' id='datetimepicker_start'>
                    <input type='text' class="form-control" id="calcasedetails-date_from" name="CalCaseDetails[date_from]" value="<?= $model->date_from ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
                $('#datetimepicker_start').on("dp.change", function (e) {
                                        /*console.log(e.date); console.log(e.date.add(1, 'hours').format('YYYY-MM-DD HH:mm'));*/
                                        minDate = e.date.add(1, 'hours').format('YYYY-MM-DD');
                                       //$('#datetimepicker_end').data("DateTimePicker").minDate(minDate);
                                        $('#datetimepicker_end input').val(minDate);
                                    });
            });
        </script>
        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <label for="calcasedetails-date_to" class="control-label">Deadline</label>
                <div class='input-group date' id='datetimepicker_end'>
                    <input type='text' class="form-control" id="calcasedetails-date_to" name="CalCaseDetails[date_to]" value="<?= $model->date_to ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
            });
        </script>
    </div>
    <?= $form->field($model, 'is_limited')->checkbox() ?>
        
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
