<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\helpers\ArrayHelper;
    use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item"],
]); ?>
    <div class="modal-body calendar-task">
    
        <?= $form->field($model, 'cases_list')->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::find()->where(['type_fk' => 1, 'status' => 1, 'id_customer_fk' => $model->id_customer_fk])->orderby('name')->all(), 'id', 'name'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true, 'data-placeholder' => 'wybierz sprawę'] )->label(false) ?> 

    </div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Połącz z projektem'), ['class' => 'btn btn-sm bg-teal']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>