<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

?>

<ul class="list-group">
	<?php
		if($model->cases) {
			foreach($model->cases as $key => $case) {
				echo '<li class="list-group-item">'
						.'<span class="badge bg-red">'
							.'<a href="'.Url::to(['/task/details/cremove', 'id' => $case['id']]).'" class="deleteConfirm" title="Odłącz sprawę"><i class="fa fa-minus text--white"></i></a>'
						.'</span>'
						.$case->case['name']
					  .'</li>';
			}
		} else {
			echo '<div class="alert alert-info">brak przypisanych spraw</div>';
		}
	?>
</ul>
