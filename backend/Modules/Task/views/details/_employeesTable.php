<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */


?>

<table id="project-details-employees" class="table table-condensed table-striped table-hover">
    <thead><tr><th rowspan="2">Pracownik</th><th colspan="2">Czas [h]</th></tr><th>przydzielony</th><th>wykorzystany</th><th></th></tr></thead>
    <?php
        $employees = $model->employees;
        $colors = Yii::$app->params['colors'];
        $colorsChart = []; $labelChart = []; $dataChart = [];
        foreach($employees as $key => $employee) {
            array_push($colorsChart, ( isset($colors[$key]) ? '"'.$colors[$key].'"' : '"'.'#33bfeb'.'"'));
            array_push($labelChart, '"'.$employee['fullname'].'"');
            array_push($dataChart, $employee['time_limit']);
            echo '<tr>' 
                    .'<td style="border-left: 3px solid '.( isset($colors[$key]) ? $colors[$key] : '#33bfeb').'">'.$employee['fullname'].'</td>'
                    .'<td class="text-center">'.$employee['time_limit'].'</td>'
                    .'<td class="text-center">'.round($employee['time_used']/60, 2).'</td>'
                    .'<td class="text-center" style="width:20px;"><a href="" title="Usuń pracownika"><i class="fa fa-user-time text--red"></i></a></td>'
                .'</tr>';
        }
    ?>
</table>
            