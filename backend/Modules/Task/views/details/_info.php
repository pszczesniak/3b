<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

?>
<?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        
<div class="grid">
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="table-head">Szczegóły 
            <span class=" pull-right option-icon"> 
                <a href="<?= Url::to(['/task/details/basic', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" data-title="Edycja konfiguracji" title="Edycja konfiguracji" class="btn btn-xs bg-purple gridViewModal"><i class="fa fa-pencil"></i></a>
            </span>
        </div>

        <div class="subcont">
            
            <ul class="details col-xs-12 col-sm-12">
                <li><!--<span>Klient</span>--> <a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>" class="align-center"><img src="<?= $avatar ?>" alt="Avatar"><br /><?= $model->customer['name'] ?></a></li>      
                <li><span>Kategoria</span> <?= $model->category ?></li>
                <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
                <li><span>Status</span> <span class="label <?= $colors[$model->id_dict_case_status_fk] ?>"><?= $model->statusname ?></span></li>
                <li><span>Data rozpoczęcia</span> <span id="meeting-start-<?= $model->id ?>"><?= ($details->date_from) ? $details->date_from : '<span class="text--lightgrey">brak danych</span>' ?></span></li>
                <li><span>Deadline</span> <span id="meeting-deadline-<?= $model->id ?>"><?= ($details->date_to) ? $details->date_to : '<span class="text--lightgrey">brak danych</span>' ?></span></li>
                <li><span>Limit czasowy [h]</span> <span id="meeting-time-<?= $model->id ?>"><?= ($details->time_limit) ? number_format($details->time_limit, 0, ",", " ") : '<span class="text--lightgrey">brak danych</span>' ?></span> </li>
                <li><span>Budżet</span> <span id="meeting-budget-<?= $model->id ?>"><?= ($details->budget_limit) ? (number_format($details->budget_limit, 2, ",", " "). ' '.$details->currency) : '<span class="text--lightgrey">brak danych</span>' ?></span> </li>
                <li><span>Utworzono</span> <?= $model->created_at ?></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="grid grid--0">
            <div class="col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-teal">
                    <span class="mini-stat-icon"><i class="fa fa-clock-o text--teal"></i></span>
                    <div class="mini-stat-info">  <span class="details_time"><?= round($stats['execution_time']/60,2).' / '.$details->time_limit ?> </span> Wykorzystany czas [h] </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-pink">
                    <span class="mini-stat-icon"><i class="fa fa-money text--pink"></i></span>
                    <div class="mini-stat-info">  <span class="details_settlements"><?= $stats['settlements']  ?> </span> Szacowany przychód [PLN] </div>
                </div>
            </div>
        </div>
        <div class="table-head">Pracownicy 
            <span class=" pull-right option-icon"> 
                <a href="<?= Url::to(['/task/details/eadd', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" data-title="Dodaj pracowników" title="Dodaj pracowników" class="btn btn-xs bg-green gridViewModal"><i class="fa fa-user-plus"></i></a>
                <a href="<?= Url::to(['/task/details/employees', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" data-title="Konfiguracja czasu pracy" title="Konfiguracja czasu pracy" class="btn btn-xs bg-purple gridViewModal"><i class="fa fa-cogs"></i></a>
            </span>
        </div>
        <div class="tile-base no-padding" id="employees-limit"> 
            <?= $this->render('_employees', ['model' => $model, 'details' => $details, 'leftTime' => $stats['leftTime']] ); ?>
        </div>   
        <!--<br />
        <div class="grid">
            <div class="col-sm-12 col-md-4">
                <div class="tile-base tile-with-icon">
                    <div class="tile-icon hidden-md hidden-sm" style="margin: -11px 36px 2px 0px;"><i class="fa fa-tasks"></i></div>
                    <div class="tile-small-header">  Zadania </div>
                    <div class="tile-body"> <div class="number" id="number1">  2 / 10 </div> </div>
                    <div class="tile-bottom">
                        <div class="progress tile-progress tile-progress--red">
                            <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 67%"></div>
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="tile-base tile-with-icon">
                    <div class="tile-icon hidden-md hidden-sm" style="margin: -11px 36px 2px 0px;"><i class="fa fa-comments"></i></div>
                    <div class="tile-small-header">  Komentarze </div>
                    <div class="tile-body"> <div class="number" id="number1">  5 </div> </div>
                    <div class="tile-bottom">
                        <div class="progress tile-progress tile-progress--red">
                            <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 67%"></div>
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="tile-base tile-with-icon">
                    <div class="tile-icon hidden-md hidden-sm" style="margin: -11px 36px 2px 0px;"><i class="fa fa-users"></i></div>
                    <div class="tile-small-header">  Dzień projektu </div>
                    <div class="tile-body"> <div class="number" id="number1">  67 % </div> </div>
                    <div class="tile-bottom">
                        <div class="progress tile-progress tile-progress--red">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 67%"></div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>-->
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="stdpad"> 
            <div class="table-head">Komentarze</div>
            <div class="subcont">   
                <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/task/matter/note', 'id' => $model->id])]) ?>
            </div>
        </div>
    </div>
</div>
  
