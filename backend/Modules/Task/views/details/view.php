<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\TodoTable;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;
use frontend\widgets\accounting\ActionsTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => ( ($model->type_fk == 1 ) ? 'Sprawy' : 'Projekty' ), 'url' => Url::to(['/task/'.( ($model->type_fk ==1 ) ? 'matter' : 'project' ).'/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/1/preview.jpg"))?"/uploads/employees/cover/1/preview.jpg":"/images/default-user.png"; ?>

<div class="grid">
    <div class="col-xs-12 col-sm-12">
        <div class="grid tile-row tile-view">
            <div class="col-md-1 col-xs-3">
                <div id="project-details-percent" class="c100 p<?= $details->percent ?> small green center"> <span><?= $details->percent ?>%</span><div class="slice"><div class="bar"></div><div class="fill"></div></div></div>
            </div>
            <div class="col-md-11 col-xs-9 smallscreen"> 
                <h2><?= $model->name ?>&nbsp;<a href="<?= Url::to(['/task/project/update', 'id' => $model->id]) ?>" class="btn btn-primary" title="Edycja projektu"><i class="fa fa-pencil"></i></a>
                    &nbsp;<a href="<?= Url::to(['/accounting/manager/specification']) ?>?set=<?= $model->id ?>" class="btn bg-pink" title="Rozliczenie projektu"><i class="fa fa-calculator"></i></a>
                </h2>
                <p class="truncate description" id="meeting-limited-<?= $model->id ?>"><?= ($details->is_limited) ? '<i class="text--green">Kontrola limitów włączona</i>' : '<i class="text--red">Kontrola limitów wyłączona</i>' ?></p>
            </div>
    
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#projectdetails-tab" aria-controls="projectdetails-tab" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-cog"></i></span><span class="hidden-xs">Szczegóły</span></a></li>
                <li role="presentation"><a href="#tasksp-tab" id="task_menu_link" aria-controls="tasksp-tab" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-tasks"></i></span><span class="hidden-xs">Zadania</span></a></li>
                <li role="presentation"><a href="#tasksf-tab" id="task_menu_link" aria-controls="tasksf-tab" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-share-alt"></i></span><span class="hidden-xs">Sprawy i zdarzenia</span></a></li>
                <li role="presentation"><a href="#docs-tab" id="task_menu_link" aria-controls="docs-tab" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-files-o"></i></span><span class="hidden-xs">Dokumenty</span></a></a></li>
                <!--<li role="presentation" class="hidden-xs"><a href="#actions-tab" aria-controls="actions-tab" role="tab" data-toggle="tab" aria-expanded="false">Czynności</a></li>-->
                <!--<li role="presentation" class="hidden-xs"><a href="#activities-tab" aria-controls="activities-tab" role="tab" data-toggle="tab" aria-expanded="false">Aktywność</a></li>->
        
                <!--<li role="presentation" class="dropdown visible-xs">
                    <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Overview <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                        <li role="presentation"><a href="#projectdetails-tab" aria-controls="projectdetails-tab" role="tab" data-toggle="tab">Project Details</a></li>
                        <li role="presentation"><a href="#tasks-tab" aria-controls="tasks-tab" role="tab" data-toggle="tab">Tasks</a></li>
                        <li role="presentation"><a href="#actions-tab" aria-controls="tasks-tab" role="tab" data-toggle="tab">actions</a></li>
                        <li role="presentation"><a href="#gantt-tab" class="resize-gantt" aria-controls="gantt-tab" role="tab" data-toggle="tab">Gantt</a></li>
                        <li role="presentation"><a href="#media-tab" aria-controls="media-tab" class="media-tab-trigger" role="tab" data-toggle="tab">Media</a></li>
                        <li role="presentation"><a href="#notes-tab" aria-controls="notes-tab" role="tab" data-toggle="tab">Notes</a></li>
                        <li role="presentation"><a href="#invoices-tab" aria-controls="invoices-tab" role="tab" data-toggle="tab">Invoices</a></li>
                        <li role="presentation"><a href="#activities-tab" aria-controls="activities-tab" role="tab" data-toggle="tab">Activities</a></li>
                    </ul>
                </li>-->

                <!--<li class="pull-right">
                    <a href="http://demo.freelancecockpit.com/projects/copy/15" class="btn-option tt" title="" data-toggle="mainmodal" data-original-title="Copy Project"><i class="fa fa-copy"></i></a>
                </li>
                <li class="pull-right">
                    <a href="http://demo.freelancecockpit.com/projects/sticky/15"><i class="fa fa-star-o"></i></a>
                </li>
                <li class="pull-right">
                    <a href="http://demo.freelancecockpit.com/projects/update/15" data-toggle="mainmodal" data-target="#mainModal"><i class="fa fa-cog"></i></a>
                </li>
                <li class="pull-right">
                    <a href="http://demo.freelancecockpit.com/projects/tracking/15" class="tt green" title="" data-original-title="Start Timer"><i class="fa fa-clock-o"></i> </a>
                </li> -->
            </ul>
        </div> 
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane fade active in" role="tabpanel" id="projectdetails-tab">
        <?= $this->render('_info', ['model' => $model, 'details' => $details, 'stats' => $stats]); ?>
    </div>
    <div class="tab-pane fade bg-white" role="tabpanel" id="tasksp-tab">
        <div class="table-head">Zadania </div>
        <div class="tile-base no-padding"> 
            <?= TodoTable::widget(['dataUrl' => Url::to(['/task/project/pevents', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/personal/createajax', 'id' => $model->id]).'?projectId='.$model->id, 'actionWidth' => '80px' ]) ?>
        </div>
    </div>
    <div class="tab-pane fade bg-white" role="tabpanel" id="tasksf-tab">
        <div class="table-head">Sprawy i zdarzenia</div>
        <div class="tile-base no-padding"> 
			<div class="grid">
				<div class="col-md-4 col-sm-5 col-xs-12">
					<fieldset>
						<legend>Powiązane sprawy
							<span class=" pull-right option-icon"> 
								<a href="<?= Url::to(['/task/details/cadd', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" data-title="Dołącz sprawę" title="Dołącz sprawę" class="btn btn-xs bg-green gridViewModal"><i class="fa fa-plus"></i>sprawę</a>
							</span>
						</legend>
						<div  id="details-cases"><?= $this->render('_cases', ['model' => $model]); ?></div>						
					</fieldset>
				</div>
				<div class="col-md-8 col-sm-7 col-xs-12">
					<?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>        
				</div>				
			</div>
		</div>
    </div>
    <div class="tab-pane fade bg-white" role="tabpanel" id="docs-tab">
        <div class="table-head">Dokumenty </div>
        <div class="tile-base no-padding"> 
            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => true, 'download' => Url::to(['/task/matter/download', 'id' => $model->id]) ]) ?>
        </div>
    </div>
    <!--<div class="tab-pane fade bg-white" role="tabpanel" id="actions-tab">
        <div class="table-head">Czynności </div>
        <div class="tile-base no-padding"> 
            <?php /* ActionsTable::widget(['dataUrl' => Url::to(['/task/project/actions', 'id' => $model->id]), 'invoice' => false, 'customerId' => $model->id_customer_fk, 'setId' => $model->id ])*/ ?>
        </div>
    </div>-->
    <!--<div class="tab-pane fade" role="tabpanel" id="activities-tab">
        aktywność
    </div>-->
</div>

