<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */


?>

<script src="/js/chart/Chart.bundle.js"></script>
<script src="/js/chart/utils.js"></script>

<?php  if($leftTime > 0) { ?><div class="alert bg-purple2 text--purple">Do przydzielenia pozostało jeszcze <span id="meeting-left-<?= $model->id ?>"><?= $leftTime ?> </span>h</div><?php } ?>
<div class="tile-extended-header">
    <div>
        <span class="p-team">
            <?php
                $employees = $model->employees;
                $colors = Yii::$app->params['colors'];
                foreach($employees as $key => $employee) {
                    $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee['id_user_fk']."/preview.jpg"))?"/uploads/employees/cover/".$employee['id_user_fk']."/preview.jpg":"/images/default-user.png";
                    echo '<a href="#" class="p-team-thumb" style="border: 3px solid '.( isset($colors[$key]) ? $colors[$key] : '#33bfeb').'"><img alt="image" class="" title="'.$employee['fullname'].'" src="'.$avatar .'"></a>';
                }
            ?>
        </span>
    </div>
    <div class="tile-body">
		<div class="grid">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <canvas id="canvas"></canvas>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <table id="project-details-employees" class="table table-condensed table-striped table-hover">
                    <thead><tr><th rowspan="2">Pracownik</th><th colspan="3">Czas [h]</th></tr><th>przydzielony</th><th>wykorzystany</th><th></th></tr></thead>
                    <?php
                        $colorsChart = []; $labelChart = []; $dataChart = [];
                        foreach($employees as $key => $employee) {
                            array_push($colorsChart, ( isset($colors[$key]) ? '"'.$colors[$key].'"' : '"'.'#33bfeb'.'"'));
                            array_push($labelChart, '"'.$employee['fullname'].'"');
                            array_push($dataChart, $employee['time_limit']);
                            echo '<tr>' 
                                    .'<td style="border-left: 3px solid '.( isset($colors[$key]) ? $colors[$key] : '#33bfeb').'">'.$employee['fullname'].'</td>'
                                    .'<td class="text-center">'.$employee['time_limit'].'</td>'
                                    .'<td class="text-center">'.round($employee['time_used']/60, 2).'</td>'
                                    .'<td class="text-center" style="width:20px;"><a href="'.Url::to(['/task/details/eremove', 'id' => $employee['rel_id']]).'" class="deleteConfirm" title="Usuń pracownika"><i class="fa fa-user-times text--red"></i></a></td>'
                                .'</tr>';
                        }
                    ?>
                </table>
            </div>
        </div>                 
    </div>
</div>
       
  
<script>
    var chartData =  {
        labels: [<?= implode(',', $labelChart) ?>],
        datasets: [
            {
                data: [<?= implode(',', $dataChart) ?>],
                backgroundColor: [ <?= implode(',', $colorsChart) ?> ],
                hoverBackgroundColor: [ <?= implode(',', $colorsChart) ?> ]
            }]
    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myMixedChart = new Chart(ctx, {
            type: 'doughnut',
            data: chartData,
            options: {
                legend: { display: false },
               /* tooltips: { enabled: false }*/
            }
        });
    };


</script>