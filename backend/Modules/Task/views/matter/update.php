<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\EventsTable;

$this->title = Yii::t('app', 'Edycja sprawy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/matter/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/matter/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa sprawa</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/matter/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>
<div class="grid">
    
    <div class="col-sm-6 col-md-8 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
            <?= $this->render('_form', [
                'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $employeeKind
            ]) ?>

        <?php $this->endContent(); ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-teal"> 
                <span class="panel-title"> <span class="fa fa-calendar"></span> Zdarzenia </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="matter-update-tasks" aria-expanded="true" href="#matter-update-tasks" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="matter-update-tasks">
                <?php /* $this->render('_events', ['id' => $model->id,]) */ ?>
                <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]) ]) ?>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 col-xs-12">
        <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-grey"> 
                <span class="panel-title"> 
                    <span class="fa fa-step-forward"></span> Status 
                </span> 
                <div class="panel-heading-menu pull-right">
                    <span class="dropdown">
                        <a href="#" class="fa fa-cog" id="dropdownStatusMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></a> 
                        <ul class="dropdown-menu" aria-labelledby="dropdownStatusMenu">
                            <li class="dropdown-header">Cykl sprawy</li>
                            <li role="separator" class="divider"></li>
                            <?= ($model->id_dict_case_status_fk == 1) ? '<li class="disabled"><a href="#"><span class="text--blue lead"><strong>Nowa</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]).'?status=1" class="modalConfirm"><span class="text--blue">Nowa</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 2) ? '<li class="disabled"><a href="#"><span class="text--orange lead"><strong>Plan</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=2" class="modalConfirm"><span class="text--orange">Plan</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 3) ? '<li class="disabled"><a href="#"><span class="text--purple lead"><strong>W toku</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=3" class="modalConfirm"><span class="text--purple">W toku</a></span></li>' ?>
                            <?= ($model->id_dict_case_status_fk == 4) ? '<li class="disabled"><a href="#"><span class="text--green lead"><strong>Zakończona</strong></a></span></li>' : '<li><a href="'. Url::to(['/task/matter/status', 'id' => $model->id]) .'?status=4" class="modalConfirm"><span class="text--green">Zakończona</a></span></li>' ?>
                        </ul>
                    </span> 
					<a aria-controls="matter-update-status" aria-expanded="true" href="#matter-update-status" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                    
                </div>
            </div>
            <div class="panel-body collapse in" id="matter-update-status">
                
                <div class="alert <?= $colors[$model->id_dict_case_status_fk] ?>" > Obceny status: <b><?= $model->statusname ?></b></div>
                <div class="align-right">
                    <?php if($model->id_dict_case_status_fk != 4) { ?> 
						<a href="<?= Url::to(['/task/matter/status', 'id' => $model->id]) ?>" class="btn <?= $colors[($model->id_dict_case_status_fk+1)] ?> btn-flat modalConfirm">
							<span class="fa fa-step-forward"></span> Status [<?= \backend\Modules\Task\models\CalCase::listStatus('normal')[($model->id_dict_case_status_fk+1)] ?>]
						</a> <?php } ?>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading bg-purple"> 
                <span class="panel-title"> <span class="fa fa-paperclip"></span> Dokumenty </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-docs" aria-expanded="true" href="#crm-customer-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-docs">
                <?php /* $this->render('_files', ['model' => $model, 'type' => 3, 'onlyShow' => false]) */ ?>
                <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => false]) ?>
            </div>
        </div>
        
        
        
        <div class="panel panel-default">
            <div class="panel-heading bg-blue"> 
                <span class="panel-title"> <span class="fa fa-comments"></span> Komentarze </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-note" aria-expanded="true" href="#crm-customer-update-note" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-note">
                <?= $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) ?>
            </div>
        </div>


    </div>
</div>



