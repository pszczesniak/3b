<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-cases"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Podstawowe</span></a></li>
					<li><a data-toggle="tab" href="#tab2b"><i class="fa fa-user"></i><span class="panel-tabs--text">Pracownicy</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-cogs"></i><span class="panel-tabs--text">Dodatkowe</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-commenting-o"></i><span class="panel-tabs--text">Notatka</span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
						<div class="grid">
							<div class="col-sm-4 col-xs-12"><?= $form->field($model, 'no_label')->textInput(['maxlength' => true, 'readonly' => true]) ?></div>
							<div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '- wybierz -', 'readonly' => (($model->status == 1) ? false : false)] ) ?></div>
							<div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'readonly' => (($model->status == 1) ? false : false)] ) ?></div>
						</div>
						<div class="grid">
							<div class="col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div> 
							<div class="col-sm-4 col-xs-12"><?= $form->field($model, 'customer_role')->dropDownList(\backend\Modules\Task\models\CalCase::listRoles(false), [/*'prompt' => '-wybierz-', 'class' => 'form-control select2'*/] ) ?> </div>
							<div class="col-sm-4 col-xs-12"> <?= $form->field($model, 'type_fk')->dropDownList( isset(Yii::$app->params['set-types']) ? Yii::$app->params['set-types'] : [1 => 'Sprawa', 2 => 'Projekt' ], ['class' => 'form-control select2', 'class' => 'form-control'] ) ?> </div>
							<div class="col-sm-4 col-xs-12"> 
								<?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
										  {label}
										   <div class="input-group ">
												{input}
												<span class="input-group-addon bg-green">'.
													Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/'.( ($model->type_fk == 1) ? '5' : '6' ) , 
														['class' => 'insertInline text--white', 
														 'data-target' => "#case-type-insert", 
														 'data-input' => ".case-type",
														 'id' => 'insertType'
														])
												.'</span>
										   </div>
										   {error}{hint}
									   '])->dropDownList(\backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 5 : 6 )), ['class' => 'form-control case-type'] ) ?>
								<div id="case-type-insert" class="insert-inline bg-purple2 none"> </div>
							</div>      
						</div>
						<div class="grid">
							<div class="col-sm-12 col-xs-12">
								<?php if(Yii::$app->params['env'] == 'dev') { ?>
								<fieldset><legend class="bg-red">Strona przeciwna</legend>
									<?= $form->field($model, 'id_opposite_side_fk', ['template' => '
										  {label}
										   <div class="input-group ">
												{input}
												<span class="input-group-addon bg-green">'.
													Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/side/createinline']) , 
														['class' => 'insertInline text--white', 
														 'data-target' => "#side-insert", 
														 'data-input' => ".matter-side",
														 'id' => 'insertSide'
														])
												.'</span>
										   </div>
										   {error}{hint}
									   '])->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\Customer::getOppositeSides(), 'id', 'name'), ['id' => 'caseside-id_side_fk', 'class' => 'form-control matter-side'] ) ?>
									<div id="side-insert" class="insert-inline bg-purple2 none"> </div>
											  
								</fieldset>
								<?php } ?>
							</div>
						</div>   
					</div>
			        <div class="tab-pane" id="tab2b">
						<?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select-ajax', 'data-type' => 'matter', 'multiple' => 'multiple', ] ); ?>
						<?= $this->render('_employeesListAjax', ['employees' => $lists['employees'], 'employees_ch' => $employees, 'case' => $model->id/*, 'ids' => ($departments) ? implode(',',$departments) : '0', 'all' => $model->employees*/ ]) ?>
					</div>
					<div class="tab-pane" id="tab2c">
						<fieldset><legend>Postępowanie</legend>
							<?= $form->field($model, 'a_instance_sygn')->textInput(['maxlength' => true]) ?>
							<div class="grid">
								<div class="col-sm-3 col-xs-12">
									<?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => 'wybierz', 'class' => 'form-control'] )->label('Typ') ?>
								</div>
								<div class="col-sm-9 col-xs-12">
									<?= $form->field($model, 'a_instance_address_fk', ['template' => '
									  {label}
									   <div class="input-group ">
											{input}
											<span class="input-group-addon bg-green">'.
												Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-a_instance_address_fk' , 
													['class' => 'gridViewModal text--white', 
													 'id' => 'carrying-create',
													 //'data-toggle' => ($gridViewModal)?"modal":"none", 
													 'data-target' => "#modal-grid-event", 
													 'data-form' => "item-form", 
													 'data-input' => ".correspondence-address",
													 'data-title' => "Dodaj"
													])
											.'</span>
									   </div>
									   {error}{hint}
								   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
								</div>
							</div>
							
							<?= $form->field($model, 'a_instance_contact')->textInput(['maxlength' => true]) ?>
						</fieldset>
					</div>
					<div class="tab-pane" id="tab2d">
						<?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
					</div>
			    </div>
			</div>
	    </div>
	</div>
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">

    document.getElementById('calcase-type_fk').onchange = function(event) {
        if(event.target.value == 1) {
            type = 5; 
            document.getElementById('calcase-departments_list').setAttribute("data-type", "matter");
            document.getElementById('calcase-customer_role').disabled = false;
        } else {
            type = 6;
            document.getElementById('calcase-departments_list').setAttribute("data-type", "project");
            document.getElementById('calcase-customer_role').disabled = true;
        }
        var dids = document.getElementById('calcase-departments_list');
        var ids = [];
        for (var i = 0; i < dids.options.length; i++) {
            if (dids.options[i].selected) {
                ids.push(dids.options[i].value);
            }
        }
       
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/dict']) ?>/"+type+"?dids="+ids, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_dict_case_type_fk').innerHTML = result.types; 
                document.getElementById('menu-list-employee-task').innerHTML = result.employees; 
                
                if(type == 5) {
                    document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/5' ?>");
                } else {
                    document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/6' ?>");
                }
            }
        }
        xhr.send();
        return false;
      
    }
	
	document.getElementById('caseside-id_side_fk').onchange = function() {
	   buildName();      
       return false;
    }
	
	document.getElementById('calcase-customer_role').onchange = function() {
	   buildName();      
       return false;
    }
	
	document.getElementById('calcase-a_instance_sygn').onfocusout = function() {
	   buildName();      
       return false;
    }
	
	function buildName() {
		var xhr_name = new XMLHttpRequest();
		xhr_name.open('POST', "<?= Url::to(['/task/manage/name']) ?>?customer="+<?= $model->id_customer_fk ?>+"&side="+document.getElementById('caseside-id_side_fk').value+"&sygn="+document.getElementById('calcase-a_instance_sygn').value+'&role='+document.getElementById('calcase-customer_role').value, true);
		xhr_name.onreadystatechange=function()  {
			if (xhr_name.readyState==4 && xhr_name.status==200)  {
				var result = JSON.parse(xhr_name.responseText);
				document.getElementById('calcase-name').value = result.name; 
			}
		}
	   xhr_name.send();
		
		return false;
	}
	
	document.getElementById('calcase-id_department_fk').onchange = function() {
        $('#calcase-departments_list').multiselect('select',this.value,true);
        
        if(document.getElementById('calcase-id_company_branch_fk').value) {
            var xhr_label = new XMLHttpRequest();
            xhr_label.open('POST', "<?= Url::to(['/task/manage/label']) ?>?department="+this.value+"&branch="+document.getElementById('calcase-id_company_branch_fk').value, true);
            xhr_label.onreadystatechange=function()  {
                if (xhr_label.readyState==4 && xhr_label.status==200)  {
                    var result = JSON.parse(xhr_label.responseText);
                    document.getElementById('calcase-no_label').value = result.no_label; 
                }
            }
           xhr_label.send();
        }
       
       return false;
    }
    
    document.getElementById('calcase-id_company_branch_fk').onchange = function() {        
        if(document.getElementById('calcase-id_department_fk').value) {
            var xhr_label = new XMLHttpRequest();
            xhr_label.open('POST', "<?= Url::to(['/task/manage/label']) ?>?branch="+this.value+"&department="+document.getElementById('calcase-id_department_fk').value, true);
            xhr_label.onreadystatechange=function()  {
                if (xhr_label.readyState==4 && xhr_label.status==200)  {
                    var result = JSON.parse(xhr_label.responseText);
                    document.getElementById('calcase-no_label').value = result.no_label; 
                }
            }
           xhr_label.send();
        }
       
       return false;
    }
</script>