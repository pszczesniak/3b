<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'createForm', /*'method' => 'POST', 'action' => 'http://api.vdr.dev/admin/employees',*/
                                     'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                     'options' => ['class'=> 'modalAjaxForm', 'data-table' => '#table-folder', 'data-prefix' => 'folder', 'data-model' => 'Folder', 'data-target' => '#modal-grid-item']]); ?>
    <div class="modal-body">
        <div class="filemanager" data-url="<?= Url::to(['/task/matter/folders', 'id' => $model->id]) ?>">
            <!--<div class="search">
                <input type="search" placeholder="Find a file.." />
            </div>-->
            <div class="breadcrumbs"></div>
            <ul class="data">
                <?php foreach($model->browser as $key => $item) { ?>
                    <li class="folders">
                        <a href="<?= Url::to(['/task/matter/folders', 'mid' => $model->id, 'type' => $item['id']]) ?>" title="Przeglądaj pliki" class="folders">
                            <i class="icon-folder fa fa-folder fa-3x text--yellow"></i>
                            <span class="name"><?= $item['name'] ?></span> 
                            <span class="details"><?= $item['items'] ?> plików</span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if(count($model->browser) == 0) echo '<div class="alert alert-info">Brak plików</div>'; ?>
            <!--<div class="nothingfound">
                <div class="nofiles"></div>
                <span>No files here.</span>
            </div>-->
        </div>
    </div>          

    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?php /* Html::submitButton(Yii::t('app', 'Wybierz'), ['class' => 'btn btn-success'])*/ ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>

    </div>
<?php ActiveForm::end(); ?>
	