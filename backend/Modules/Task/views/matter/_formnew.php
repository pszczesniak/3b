<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
                <fieldset><legend class="bg-teal">Klient <?php if(!$model->isNewRecord && $model->status == 1) { ?><a href="<?= Url::to(['/crm/customer/card', 'id' => $model->id_customer_fk]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka klienta" title="Wizytówka klienta"><i class="fa fa-address-card text--lightgrey"></i></a><?php } ?></legend>
                <?= $form->field($model, 'id_customer_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/customer/createajax']) , 
                                                            ['class' => 'gridViewModal text--white', 
                                                             'id' => 'customer-create',
                                                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                                             'data-target' => "#modal-grid-item", 
                                                             'data-form' => "item-form", 
                                                             'data-input' => ".correspondence-customer",
                                                             'data-title' => "Nowy klient"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-customer select2'] ) 
                                    ?>                 
                <div class="grid grid--0">
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'customer_role')->dropDownList( \backend\Modules\Task\models\CalCase::listRoles(false), [/*'prompt' => '-wybierz-', 'class' => 'form-control select2'*/] ) ?> </div>
                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> </div>
                </div>              
            </fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend class="bg-red">Strona przeciwna</legend>
                <?= $form->field($model, 'id_opposite_side_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/side/createajax']) , 
                                                            ['class' => 'gridViewModal text--white', 
                                                             'id' => 'side-create',
                                                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                                             'data-target' => "#modal-grid-event", 
                                                             'data-form' => "item-form", 
                                                             'data-input' => ".side-customer",
                                                             'data-title' => "Nowa strona"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getOppositeSides(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control side-customer select2'] ) 
                                    ?>                 
                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
        </div>
    </div>
    <?= $form->field($model, 'show_client')->checkBox()  ?>
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="grid">
		<div class="col-sm-6 col-md-8">
			<fieldset><legend>Etykieta <?= (!$model->isNewRecord && $model->status == 1) ? '<a href="'.Url::to(['/task/manage/print', 'id' => $model->id]).'"><i class="fa fa-print text--blue"></i></a>' : '' ?></legend>

                <?php /*echo $form->field($model, 'id_dict_case_status_fk')->radioList(\backend\Modules\Task\models\CalCase::listStatus(), 
                                            ['class' => 'btn-group', 'data-toggle' => "buttons",
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                    return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                        Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                },
                                        ])*/ ?>
                <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
                <div class="grid">
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'no_label')->textInput(['maxlength' => true, 'readonly' => true]) ?></div>
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => '- wybierz -', 'readonly' => (($model->status == 1) ? true : false)] ) ?></div>
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'readonly' => (($model->status == 1) ? true : false)] ) ?></div>
                </div>
                <?= $form->field($model, 'subname')->textInput(['maxlength' => true]) ?>
            </fieldset>
            <?php if($model->status == -2) { ?>
                <div id="case-block">
                    <fieldset><legend>Postępowanie</legend>
                        <?= $form->field($model, 'a_instance_sygn')->textInput(['maxlength' => true]) ?>
                        <div class="grid">
                            <div class="col-sm-3 col-xs-12">
                                <?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => 'wybierz', 'class' => 'form-control'] )->label('Typ') ?>
                            </div>
                            <div class="col-sm-9 col-xs-12">
                                <?= $form->field($model, 'a_instance_address_fk', ['template' => '
                                  {label}
                                   <div class="input-group ">
                                        {input}
                                        <span class="input-group-addon bg-green">'.
                                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-a_instance_address_fk' , 
                                                ['class' => 'gridViewModal text--white', 
                                                 'id' => 'carrying-create',
                                                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                                 'data-target' => "#modal-grid-event", 
                                                 'data-form' => "item-form", 
                                                 'data-input' => ".correspondence-address",
                                                 'data-title' => "Dodaj"
                                                ])
                                        .'</span>
                                   </div>
                                   {error}{hint}
                               '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
                            </div>
                        </div>
                        
                        <?= $form->field($model, 'a_instance_contact')->textInput(['maxlength' => true]) ?>
                    </fieldset>               
                </div> 
            <?php } ?>
            <fieldset><legend>Opis</legend>   
                <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
			</fieldset>	 
		</div>
		<div class="col-sm-6 col-md-4">
			<fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
                          {label}
                           <div class="input-group ">
                                {input}
                                <span class="input-group-addon bg-green">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/5' , 
                                        ['class' => 'insertInline text--white', 
                                         'data-target' => "#project-type-insert", 
                                         'data-input' => ".project-type"
                                        ])
                                .'</span>
                           </div>
                           {error}{hint}
                       '])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(5), ['class' => 'form-control project-type'] ) ?>
                <div id="project-type-insert" class="insert-inline bg-purple2 none"> </div>
            </fieldset>

            <fieldset><legend>Personalizacja</legend>
                <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'matter', 'multiple' => 'multiple', ] ); ?>
                <?= EmployeesCheck::widget(['idName' => 'menu-list-employee', 'employees' => $lists['employees'], 'employeesCheck' => $employees]) ?>
                <!--<?= $this->render('_employeesList', ['employees' => $lists['employees'], 'employees_ch' => $employees, 'case' => $model->id/*, 'ids' => ($departments) ? implode(',',$departments) : '0', 'all' => $model->employees*/ ]) ?>-->
                <br /><br />
                <?= $form->field($model, 'id_employee_leading_fk')->dropDownList(  $lists['employees']/*ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 

            </fieldset>
		</div>
        
	</div>
    <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    document.getElementById('calcase-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/lists']) ?>/"+this.value+"?type=matter", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_customer_person_leading_fk').innerHTML = result.contacts; 
                //document.getElementById('calcase-departments_list').innerHTML = result.departments; 
                //$('#calcase-departments_list').multiselect('rebuild');
                //$('#calcase-departments_list').multiselect('refresh');
                //document.getElementById('menu-list-employee').innerHTML = result.employees; 
            }
        }
       xhr.send();
	   
	   buildName();      
       return false;
    }
	
	document.getElementById('calcase-id_opposite_side_fk').onchange = function() {
	   buildName();      
       return false;
    }
	
	document.getElementById('calcase-customer_role').onchange = function() {
	   buildName();      
       return false;
    }
	
	document.getElementById('calcase-a_instance_sygn').onfocusout = function() {
	   buildName();      
       return false;
    }
    
    document.getElementById('calcase-id_department_fk').onchange = function() {
        $('#calcase-departments_list').multiselect('select',this.value,true);
        
        if(document.getElementById('calcase-id_company_branch_fk').value) {
            var xhr_label = new XMLHttpRequest();
            xhr_label.open('POST', "<?= Url::to(['/task/manage/label']) ?>?department="+this.value+"&branch="+document.getElementById('calcase-id_company_branch_fk').value, true);
            xhr_label.onreadystatechange=function()  {
                if (xhr_label.readyState==4 && xhr_label.status==200)  {
                    var result = JSON.parse(xhr_label.responseText);
                    document.getElementById('calcase-no_label').value = result.no_label; 
                }
            }
           xhr_label.send();
        }
       
       return false;
    }
    
    document.getElementById('calcase-id_company_branch_fk').onchange = function() {        
        if(document.getElementById('calcase-id_department_fk').value) {
            var xhr_label = new XMLHttpRequest();
            xhr_label.open('POST', "<?= Url::to(['/task/manage/label']) ?>?branch="+this.value+"&department="+document.getElementById('calcase-id_department_fk').value, true);
            xhr_label.onreadystatechange=function()  {
                if (xhr_label.readyState==4 && xhr_label.status==200)  {
                    var result = JSON.parse(xhr_label.responseText);
                    document.getElementById('calcase-no_label').value = result.no_label; 
                }
            }
           xhr_label.send();
        }
       
       return false;
    }
	
	function buildName() {
		var xhr_name = new XMLHttpRequest();
		xhr_name.open('POST', "<?= Url::to(['/task/manage/name']) ?>?customer="+document.getElementById('calcase-id_customer_fk').value+"&side="+document.getElementById('calcase-id_opposite_side_fk').value+"&sygn="+document.getElementById('calcase-a_instance_sygn').value+'&role='+document.getElementById('calcase-customer_role').value, true);
		xhr_name.onreadystatechange=function()  {
			if (xhr_name.readyState==4 && xhr_name.status==200)  {
				var result = JSON.parse(xhr_name.responseText);
				document.getElementById('calcase-name').value = result.name; 
			}
		}
	   xhr_name.send();
		
		return false;
	}
    
    document.getElementById('calcase-address_type_fk').onchange = function() {
        var opts = [], opt;
        var sel = document.getElementById('calcase-address_type_fk');
        for (var i=0, len=sel.options.length; i<len; i++) {
            opt = sel.options[i];
            if ( opt.selected ) {
                opts.push(opt.value);
                /*if (fn) {  fn(opt);  }*/
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/correspondence/address/filter']) ?>?ids="+opts.join(), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('calcase-a_instance_address_fk').innerHTML = result.addresses; 
                //$('#calcase-a_instance_address_fk').multiselect('rebuild');
                //$('#calcase-a_instance_address_fk').multiselect('refresh');
            }
        }
       xhr.send();
       
       return false;
    }
</script>
<?php if( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) || \Yii::$app->session->get('user.isManager') == 1) && $model->status != -2) { ?>
<script type="text/javascript">
    
    document.getElementById('calcase-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0), true)
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_order_fk').innerHTML = result.listOrder;     
            }
        }
       xhr.send();
        return false;
    }
    
</script>
<?php } ?>
