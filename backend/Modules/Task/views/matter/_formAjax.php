<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-cases"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">

    <div class="grid">
        <div class="col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        
		<div class="col-sm-4 col-xs-12"><?= $form->field($model, 'customer_role')->dropDownList(\backend\Modules\Task\models\CalCase::listRoles(false), [/*'prompt' => '-wybierz-', 'class' => 'form-control select2'*/] ) ?> </div>
		<div class="col-sm-4"> <?= $form->field($model, 'type_fk')->dropDownList( isset(Yii::$app->params['set-types']) ? Yii::$app->params['set-types'] : [1 => 'Sprawa', 2 => 'Projekt' ], ['class' => 'form-control select2', 'class' => 'form-control'] ) ?> </div>
        <div class="col-sm-4"> 
            <?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
                      {label}
                       <div class="input-group ">
                            {input}
                            <span class="input-group-addon bg-green">'.
                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/'.( ($model->type_fk == 1) ? '5' : '6' ) , 
                                    ['class' => 'insertInline text--white', 
                                     'data-target' => "#case-type-insert", 
                                     'data-input' => ".case-type",
                                     'id' => 'insertType'
                                    ])
                            .'</span>
                       </div>
                       {error}{hint}
                   '])->dropDownList(\backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 5 : 6 )), ['class' => 'form-control case-type'] ) ?>
            <div id="case-type-insert" class="insert-inline bg-purple2 none"> </div>
        </div>      
    </div>
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
            <?php if(Yii::$app->params['env'] == 'dev') { ?>
			<fieldset><legend class="bg-red">Strona przeciwna</legend>
                <?= $form->field($model, 'id_opposite_side_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/side/createajax']) , 
                                                            ['class' => 'gridViewModal text--white', 
                                                             'id' => 'side-create',
                                                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                                             'data-target' => "#modal-grid-event", 
                                                             'data-form' => "item-form", 
                                                             'data-input' => ".side-customer",
                                                             'data-title' => "Nowa strona"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getOppositeSides(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control side-customer select2'] ) 
                                    ?>                 
                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
			<?php } ?>
            <fieldset><legend>Postępowanie</legend>
                <?= $form->field($model, 'a_instance_sygn')->textInput(['maxlength' => true]) ?>
                <div class="grid">
                    <div class="col-sm-3 col-xs-12">
                        <?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => 'wybierz', 'class' => 'form-control'] )->label('Typ') ?>
                    </div>
                    <div class="col-sm-9 col-xs-12">
                        <?= $form->field($model, 'a_instance_address_fk', ['template' => '
                          {label}
                           <div class="input-group ">
                                {input}
                                <span class="input-group-addon bg-green">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-a_instance_address_fk' , 
                                        ['class' => 'gridViewModal text--white', 
                                         'id' => 'carrying-create',
                                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                         'data-target' => "#modal-grid-event", 
                                         'data-form' => "item-form", 
                                         'data-input' => ".correspondence-address",
                                         'data-title' => "Dodaj"
                                        ])
                                .'</span>
                           </div>
                           {error}{hint}
                       '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
                    </div>
                </div>
                
                <?= $form->field($model, 'a_instance_contact')->textInput(['maxlength' => true]) ?>
            </fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Personalizacja</legend>
                <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select-ajax', 'data-type' => 'matter', 'multiple' => 'multiple', ] ); ?>
                <?= $this->render('_employeesListAjax', ['employees' => $lists['employees'], 'employees_ch' => $employees, 'case' => $model->id/*, 'ids' => ($departments) ? implode(',',$departments) : '0', 'all' => $model->employees*/ ]) ?>
            </fieldset>
        </div>
    </div>   
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">

    document.getElementById('calcase-type_fk').onchange = function(event) {
        if(event.target.value == 1) {
            type = 5; 
            document.getElementById('calcase-departments_list').setAttribute("data-type", "matter");
            document.getElementById('calcase-customer_role').disabled = false;
        } else {
            type = 6;
            document.getElementById('calcase-departments_list').setAttribute("data-type", "project");
            document.getElementById('calcase-customer_role').disabled = true;
        }
        var dids = document.getElementById('calcase-departments_list');
        var ids = [];
        for (var i = 0; i < dids.options.length; i++) {
            if (dids.options[i].selected) {
                ids.push(dids.options[i].value);
            }
        }
       
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/dict']) ?>/"+type+"?dids="+ids, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_dict_case_type_fk').innerHTML = result.types; 
                document.getElementById('menu-list-employee-task').innerHTML = result.employees; 
                
                if(type == 5) {
                    document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/5' ?>");
                } else {
                    document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/6' ?>");
                }
            }
        }
        xhr.send();
        return false;
      
    }
</script>