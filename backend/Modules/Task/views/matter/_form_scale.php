<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            //'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
            'fieldConfig' => [  ],
        ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <div class="grid">
		<div class="col-sm-6 col-md-8">
			<fieldset><legend>Podstawowe</legend>
                <div class="form-group field-calmerge-id_case_fk">
                    <label class="control-label" for="calmerge-id_case_fk">Scal do</label>
                    <select id="calmerge-id_case_fk" class="form-control" name="CalMerge[id_case_fk]">
                        <option value="0">Nowej sprawy</option>
                        <?php foreach($cases as $key => $case) {
                            echo '<option value="'.$case->id.'">'.$case->name.'</option>';
                        } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
                
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </fieldset>
            
            <fieldset><legend>Klient</legend>
                <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['class' => 'form-control select2', 'prompt' => '-wybierz-', 'class' => 'form-control select2'] )->label('Nazwa') ?> 
                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
             </fieldset>
             
            <fieldset><legend>Opis</legend>   
                <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(false) ?>
			</fieldset>	
            
            <div id="case-block" >
                <div class="grid">
                    <div class="col-sm-6">
                        <fieldset><legend>Organ prowadzący</legend>
                            <?php /*$form->field($model, 'authority_carrying_name')->textInput(['maxlength' => true])*/ ?>
                            <?= $form->field($model, 'authority_carrying_name', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-authority_carrying_name' , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'carrying-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-address",
											 'data-title' => "Dodaj"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
                            <?= $form->field($model, 'authority_carrying_contact')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'authority_carrying_sygn')->textInput(['maxlength' => true]) ?>
                        </fieldset>
                    </div>
                    <div class="col-sm-6">
                        <fieldset><legend>Organ nadzorujący</legend>
                            <?php /*$form->field($model, 'authority_supervisory_name')->textInput(['maxlength' => true])*/ ?>
                            <?= $form->field($model, 'authority_supervisory_name', ['template' => '
						      {label}
							   <div class="input-group ">
									{input}
									<span class="input-group-addon bg-green">'.
										Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-authority_supervisory_name' , 
											['class' => 'gridViewModal text--white', 
											 'id' => 'supervisory-create',
											 //'data-toggle' => ($gridViewModal)?"modal":"none", 
											 'data-target' => "#modal-grid-item", 
											 'data-form' => "item-form", 
											 'data-input' => ".correspondence-address",
											 'data-title' => "Dodaj"
											])
									.'</span>
							   </div>
							   {error}{hint}
						   '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
                            <?= $form->field($model, 'authority_supervisory_contact')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'authority_supervisory_sygn')->textInput(['maxlength' => true]) ?>
                        </fieldset>
                    </div>
                </div>
            </div>
		</div>
		<div class="col-sm-6 col-md-4">
			<fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'id_dict_case_type_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::listTypes(5), ['class' => 'form-control'] ) ?> 
            </fieldset>
            <fieldset><legend>Personalizacja</legend>
                <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'matter', 'multiple' => 'multiple', ] ); ?>
                <?= EmployeesCheck::widget(['idName' => 'menu-list-employee', 'employees' => $lists['employees'], 'employeesCheck' => $employees]) ?>
                <br /><br />
                <?= $form->field($model, 'id_employee_leading_fk')->dropDownList(  $lists['employees']/*ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
				<?= $form->field($merge, 'cases')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList(-1), 'id', 'name'), ['class' => 'none', 'prompt' => '-wybierz-', 'multiple' => true] )->label(false) ?> 
            </fieldset>
		</div>       
	</div>
    <div class="form-group align-right">
        <?= Html::submitButton('<i class="fa fa-object-group"></i> Scal postępowania', ['class' => 'btn btn-icon bg-purple']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
 	
	document.getElementById('calmerge-id_case_fk').onchange = function() {
	    if(this.value != 0 ) {
			var xhr = new XMLHttpRequest();
			xhr.open('POST', "/task/matter/info/"+this.value, true);
			xhr.onreadystatechange=function()  {
				var result = JSON.parse(xhr.responseText);
				if (xhr.readyState==4 && xhr.status==200)  {
					document.getElementById('calcase-name').value = result.name; 	
				}
			}
		   xhr.send();
		} else {
			document.getElementById('calcase-name').value = ''; 
		}
       
       return false;
    }
</script>
