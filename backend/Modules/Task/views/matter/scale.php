<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
$this->title = Yii::t('app', 'Edycja sprawy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Alert');
?>

<div class="alert alert-warning">
    Sprawa <b><?= $model->name ?></b> została scalona do <a href="<?= Url::to(['/task/matter/view', 'id' => $model->id_parent_fk]) ?>">tej sprawy</a> <br /><br />
    <?php $merge = \backend\Modules\Task\models\CalMerge::findOne($model->id_merge_fk); ?>
    <?php if($merge) { ?>
    Data scalenia: <?= $merge->created_at ?><br />
    Osoba scalająca: <?= \common\models\User::findOne($merge->created_by)->fullname ?>
    <?php } ?>
    
</div>