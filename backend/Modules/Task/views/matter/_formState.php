<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([/*'action' => Url::to(['/task/matter/state', 'id' => $model->id]),*/
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-matters"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <?= ($modal) ? '<div class="modal-body">' : '' ?>
        <div class="content ">
            <?php if($model->id_dict_case_status_fk == 1) { ?>
				<?= $form->field($model, 'id_dict_case_category_fk', ['template' => 
							 '{label}'
								.'<div class="input-group ">'
									.'{input}'
									.'<div class="input-group-btn">'
										.Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/18' , 
											['class' => 'insertInline text--white btn bg-green', 
											 'data-target' => "#matter-state-insert", 
											 'data-input' => ".matter-state"
											])
											.( (!$modal) ? Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn bg-teal']) : '')
									.'</div>'
								.'</div>'
							   .'{error}{hint}'
						   ])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(18), ['prompt' => '- wybierz -', 'class' => 'form-control matter-state'] )->label(false) ?>
				<div id="matter-state-insert" class="insert-inline bg-purple2 none"> </div> 
			<?php } else { ?>
				<div class="alert bg-<?= $model->presentation['color'] ?>"><i class="fa fa-<?= $model->presentation['icon'] ?>"></i>&nbsp;<?= $model->presentation['label'] ?></div>
			<?php } ?>
        </div>  
    <?php if($modal) { ?>
	</div>       
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('<i class="fa fa-save"></i>Zapisz zmiany', ['class' => 'btn btn-primary btn-icon']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
	<?php } ?>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    

</script>
