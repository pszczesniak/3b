<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = Yii::t('app', 'Scal sprawy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wybierz sprawy do scalenia'), 'url' => ['merge']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="grid">
    <div class="col-sm-6 col-md-8 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-create', 'title'=>Html::encode($this->title))) ?>
            <?= $this->render('_form_scale', [
                    'model' => $case, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => 100, 'cases' => $cases, 'merge' => $model
            ]) ?>
        <?php $this->endContent(); ?>
    </div>
    <div class="col-sm-6 col-md-4 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading bg-orange"> 
                <span class="panel-title"> <span class="fa fa-object-group"></span> Wybrane sprawy </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-docs" aria-expanded="true" href="#crm-customer-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-docs">
                <ul class="list-group">
				<?php
					foreach($cases as $key => $case) {
						echo '<li class="list-group-item">'.$case->name.'</li>';
					}
				?>
				</ul>
            </div>
        </div>
    </div>
</div>


