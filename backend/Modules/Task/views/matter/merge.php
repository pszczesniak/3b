<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sprawy');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 'merge']); ?>
    <?= Alert::widget() ?>
	<div id="toolbar-matters" class="btn-group toolbar-table-widget">
        <?= ( ( count(array_intersect(["caseMerge", "grantAll"], $grants)) > 0 ) ) ? '<form method="POST" action="'.Url::to(['/task/matter/merging']).'" id="merge-matters-form"> <input type="hidden" name="merge" value="1"></input><button type="submit" id="merge" class="btn bg-orange" disabled><i class="fa fa-object-group"></i> Scal sprawy (<span id="merge-counter">0</span>)</button></form>':'' ?>
        <!--<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-matters"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>-->
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-matters"
                data-toolbar="#toolbar-matters" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-checkbox-header="false"
                data-maintain-selected="true"
                data-response-handler="responseHandler"
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-task-matters-search"
                data-url=<?= Url::to(['/task/matter/data', 'type' => 'merge']) ?>>
            <thead>
                <tr>
                    <th data-field="nid" data-visible="false">ID</th>
                    <th data-field="state" data-visible="true" data-checkbox="true"></th>
                    <th data-field="client"  data-sortable="true" data-width="30%">Klient</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true" data-width="30%">Nazwa</th>
                    <th data-field="status"  data-sortable="true" data-align="center" data-width="10%">Status</th>
                    <th data-field="actions" data-events="actionEvents" data-width="20%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

