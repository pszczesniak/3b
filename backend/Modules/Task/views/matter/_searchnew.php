<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-task-matters-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-matters']
    ]); ?>

    <div class="grid">
        <div class="col-md-2 col-sm-2 col-xs-7"><?= $form->field($model, 'a_instance_sygn')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search'])->label('Sygnatura') ?></div>                
        <div class="col-md-2 col-sm-2 col-xs-5"><?= $form->field($model, 'customer_role')->dropDownList( \backend\Modules\Task\models\CalCase::listRoles(false), ['prompt' => '-wybierz-', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search'  ] ) ?></div>        
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search' ] ) ?></div>
        <!--<div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_opposite_side_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getOppositeSides(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search' ] ) ?></div>  --> 
        <div class="col-md-3 col-sm-3 col-xs-12"><?= $form->field($model, 'id_dict_case_status_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCase::listStatus( $type ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search' , 'data-default' => $model->id_dict_case_status_fk ] ) ?></div>
        <div class="col-md-2 col-sm-2 col-xs-12"><?= $form->field($model, 'id_dict_case_type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes( 5 ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search' , 'data-default' => $model->id_dict_case_category_fk ] ) ?></div>
		<div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search'  ] ) ?></div>        
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search'  ] ) ?></div>        
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search'  ] ) ?></div>
        <!--<div class="col-sm-9 col-md-10 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-matters', 'data-form' => '#filter-task-matters-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>-->
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
