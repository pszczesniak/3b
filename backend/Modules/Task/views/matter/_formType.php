<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([/*'action' => Url::to(['/task/matter/type', 'id' => $model->id]),*/
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-matters"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
                      {label}
                       <div class="input-group ">
                            {input}
                            <span class="input-group-addon bg-green">'.
                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/5' , 
                                    ['class' => 'insertInline text--white', 
                                     'data-target' => "#project-type-insert", 
                                     'data-input' => ".project-type"
                                    ])
                            .'</span>
                       </div>
                       {error}{hint}
                   '])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(5), ['class' => 'form-control project-type'] ) ?>
            <div id="project-type-insert" class="insert-inline bg-purple2 none"> </div>
        </div>  
	</div>       
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('<i class="fa fa-save"></i>Zapisz zmiany', ['class' => 'btn btn-primary btn-icon']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    

</script>
