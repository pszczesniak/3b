<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sprawy - nowe');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_nsearch', ['model' => $searchModel]); ?>
    <?= Alert::widget() ?>
	<div id="toolbar-data" class="btn-group">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/matter/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/matter/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-matters"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-matters"
                data-toolbar="#toolbar-matters" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
				data-page-list="[10, 25, 50, 100]"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-task-matters-search"
                data-url=<?= Url::to(['/task/matter/ndata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="client"  data-sortable="true" data-width="30%">Klient</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true" data-width="30%">Nazwa</th>
                    <th data-field="status"  data-sortable="true" data-align="center" data-width="10%">Status</th>
                    <th data-field="actions" data-events="actionEvents" data-width="20%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 

<?php $this->endContent(); ?>

<div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-event" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>Nowe zdarzenie</h4>'
            </div>
            <div class="modalContent">  </div>
            
        </div>
    </div>
</div>

<?php
    yii\bootstrap\Modal::begin([
      'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Update').'</h4>',
      //'toggleButton' => ['label' => 'click me'],
      'id' => 'modal-grid-file', // <-- insert this modal's ID
      'class' => 'modal-grid'
    ]);
 
    echo '<div class="modalContent"></div>';

    yii\bootstrap\Modal::end();
?>
