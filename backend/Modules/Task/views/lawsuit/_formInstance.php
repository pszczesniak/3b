<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/task/manage/editinstance', 'id' => $model->id]),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-claims"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="grid">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'instance_name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'instance_sygn')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-3 col-xs-12">
            <?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => 'wybierz', 'class' => 'form-control'] )->label('Typ') ?>
        </div>
        <div class="col-sm-9 col-xs-12">
            <?= $form->field($model, 'instance_address_fk', ['template' => '
              {label}
               <div class="input-group ">
                    {input}
                    <span class="input-group-addon bg-green">'.
                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createajax', 'id' => 0]).'?input=calcase-a_instance_address_fk' , 
                            ['class' => 'gridViewModal text--white', 
                             'id' => 'carrying-create',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "item-form", 
                             'data-input' => ".correspondence-address",
                             'data-title' => "Dodaj"
                            ])
                    .'</span>
               </div>
               {error}{hint}
           '])->dropDownList(  ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-address select2'] ) ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'id_dict_instance_status_fk', ['template' => '
                      {label}
                       <div class="input-group ">
                            {input}
                            <span class="input-group-addon bg-green">'.
                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/16' , 
                                    ['class' => 'insertInline text--white', 
                                     'data-target' => "#instance-status-insert", 
                                     'data-input' => ".instance-status"
                                    ])
                            .'</span>
                       </div>
                       {error}{hint}
                   '])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listStatuses(), ['prompt' => '- wybierz -', 'class' => 'form-control instance-status'] ) ?>
            <div id="instance-status-insert" class="insert-inline bg-purple2 none"> </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'id_dict_instance_mode_fk', ['template' => '
                      {label}
                       <div class="input-group ">
                            {input}
                            <span class="input-group-addon bg-green">'.
                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/17' , 
                                    ['class' => 'insertInline text--white', 
                                     'data-target' => "#instance-mode-insert", 
                                     'data-input' => ".instance-mode"
                                    ])
                            .'</span>
                       </div>
                       {error}{hint}
                   '])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listModes(), ['prompt' => '- wybierz -', 'class' => 'form-control instance-mode'] ) ?>
            <div id="instance-mode-insert" class="insert-inline bg-purple2 none"> </div>
        </div>
    </div>
       
    <?= $form->field($model, 'instance_contact')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'instance_note')->textarea(['rows' => 3]) ?>
        
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    document.getElementById('calcaseinstance-address_type_fk').onchange = function() {
        var opts = [], opt;
        var sel = document.getElementById('calcaseinstance-address_type_fk');
        for (var i=0, len=sel.options.length; i<len; i++) {
            opt = sel.options[i];
            if ( opt.selected ) {
                opts.push(opt.value);
                /*if (fn) {  fn(opt);  }*/
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/correspondence/address/filter']) ?>?ids="+opts.join(), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('calcaseinstance-instance_address_fk').innerHTML = result.addresses; 
            }
        }
       xhr.send();
       
       return false;
    }
</script>
