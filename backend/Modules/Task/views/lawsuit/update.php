<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;


$this->title = Yii::t('app', 'Postępowanie sądowe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/matter/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $instance->case['no_label'], 'url' => Url::to(['/task/matter/view', 'id' => $model->id_case_fk])];
$this->params['breadcrumbs'][] = Yii::t('app', $instance->instance_name);
?>

<?= Alert::widget() ?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <!--<a href="<?= Url::to(['/task/lawsuit/print', 'id' => $model->id]) ?>" class="btn bg-purple btn-flat btn-add-new-user" ><i class="fa fa-print"></i> Drukuj pozew</a>-->
            <a href="<?= Url::to(['/task/matter/view', 'id' => $model->id_case_fk, 'save' => false]) ?>" class="btn bg-purple btn-flat btn-add-new-user" ><i class="fa fa-mail-reply"></i> Powrót do sprawy</a>
			<a href="<?= Url::to(['/task/generate/printpdf', 'id' => $model->id, 'save' => false]) ?>" class="btn btn-info btn-flat btn-add-new-user" ><i class="fa fa-print"></i> Drukuj pozew</a>
        </div>    
    </div>
</div>    
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
    
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Postępowanie</legend><?= $this->render('_formInstance', ['model' => $instance, ]) ?></fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Pozew</legend><?= $this->render('_form', ['model' => $model, ]) ?></fieldset>
        </div>
    </div>
    

<?php $this->endContent(); ?>
       
