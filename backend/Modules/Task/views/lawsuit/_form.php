<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;
use frontend\widgets\tasks\ClaimsTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1" title="Roszczenia"><i class="fa fa-flash"></i><span class="panel-tabs--text">Roszczenia</span></a></li>
                <li><a data-toggle="tab" href="#tab2" title="Uzasadnienie"><i class="fa fa-cog"></i><span class="panel-tabs--text">Treść</span> </a></li>   
                <li><a data-toggle="tab" href="#tab3" title="Instancje"><i class="fa fa-money"></i><span class="panel-tabs--text">Opłaty</span> </a></li>                 
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                     <?= ClaimsTable::widget(['dataUrl' => Url::to(['/task/manage/claims', 'id'=>$model->id_case_fk]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addclaim', 'id' => $model->id_case_fk]), 'actionWidth' => '20px' ]) ?>
                </div>
                <div class="tab-pane" id="tab2">
                    <fieldset><legend>Uzasadnienie</legend>
                         <?= $form->field($model, 'lawsuit_reason')->textarea(['rows' => 6])->label(false) ?>
                    </fieldset>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_replacement')->textInput( ['class' => 'form-control number'] ) ?></div>
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_stamp_duty')->textInput( ['class' => 'form-control number'] ) ?></div>
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_entry')->textInput( ['class' => 'form-control number'] ) ?></div>
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_postage')->textInput( ['class' => 'form-control number'] ) ?></div>
                    </div>
                </div>
            </div>
        </div>
    <!--<fieldset><legend>Roszczenia</legend></fieldset>-->
    
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
