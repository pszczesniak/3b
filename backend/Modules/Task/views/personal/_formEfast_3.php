<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\components\CustomHelpers;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modal-grid',  'enableAjaxValidation' => true, 'enableClientValidation' => true, 'data-target' => "#modal-grid-event"],
    //'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
]); ?>
    <div class="modal-body action-form">
        <div class="content">
            <div class="grid grid--0">
                <div class=" col-md-3 col-xs-5">
                    <label class="control-label">Termin </label>
                    <input type='text' class="form-control" id="task_date" name="AccActions[action_date]" value="<?= $model->action_date ?>"/>
                </div>
                <div class="col-md-4 col-xs-7">
                    <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-actions']) ?>
                </div>
                <div class="col-md-5 col-xs-12">
                    <?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control' ] ) ?>
                </div>
            </div>
            <div class="grid">
                <div class="col-sm-6 col-xs-6"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2', /*'disabled' => (  ($model->is_confirm) ? true : false ) */] ) ?> </div>
                <div class="col-sm-6 col-xs-6"><?= $form->field($model, 'id_set_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>                        
            </div>
            <fieldset><legend class="text--red">Czynność specjalna</legend>
                <div class="grid">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $form->field($model, 'unit_price')->textInput(['placeholder' => '0,00','maxlength' => true])->label('Kwota') ?> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <?= $form->field($model, 'id_currency_fk')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] )->label('Waluta') ?> 
                    </div>
                </div>
                <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
            </fieldset>
   
            <fieldset><legend class="text--red">Koszt</legend>
                <div class="grid">
                    <div class="col-md-3 col-sm-8 col-xs-8">
                        <?= $form->field($model, 'acc_cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-4">
                        <?= $form->field($model, 'acc_cost_currency_id')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] ) ?> 
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'acc_cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                    </div>
                </div>
            </fieldset>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-primary']) ?>
        <a class="btn btn-sm bg-teal" href="<?= Url::to(['/task/personal/actions']) ?>" title="Przejdź do rejestru czynności"><i class="fa fa-table"></i></a>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date_edit').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
		$('#task_date_edit').on("dp.change", function (e) {
			$(".panel-stats").parent().addClass('overlay');
			var changeDate = new Date(e.date).getTime()/1000; console.log(changeDate);
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $(this).serialize(),
				url: "/task/personal/acc/"+$("#actionform-id_employee_fk").val()+"?time="+changeDate,
		
				success: function(data) {
					$(".panel-stats").parent().removeClass('overlay');

					$(".todo-hours_per_day").text(data.stats.hours_per_day);
					$(".todo-hours_per_month").text(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					//alert('Something went to wrong.Please Try again later...');
					$(".panel-stats").parent().removeClass('overlay');
				}
			});
			return false;
		});
       
    });
</script>
<script type="text/javascript">
	
    document.getElementById('accactions-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('actionform-id_order_fk').innerHTML = result.listOrder;     
                document.getElementById('accactions-id_set_fk').innerHTML = result.listCase; 
                document.getElementById('accactions-id_event_fk').innerHTML = '';                   
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('accactions-id_set_fk').onchange = function() {
        changeDate = new Date($("#task_date").val()).getTime()/1000; 
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/default/events']) ?>/"+this.value+'?eid='+$("#accactions-id_employee_fk").val()+'&date='+changeDate+'&without=1&box=0', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_event_fk').innerHTML = result.events;     
            }
        }
       xhr.send();
        return false;
    }

</script>