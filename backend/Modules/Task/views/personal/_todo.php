<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Rozliczenie czasu pracy');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <form  method="get">
            <div class="input-group">
                <select class="form-control" name="id" method="POST">
                    <?php
                        $employeesList = \backend\Modules\Company\models\Companyemployee::getList(\Yii::$app->user->id);
                        foreach($employeesList as $key => $employee) {
                            if($employee->id == $id || $manager)
                                echo '<option '. ( ($employee->id == $id) ? 'selected' : '' ) .' value="'.$employee->id.'">'.$employee->fullname.'</option>';
                        }
                    ?> 
                </select>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" data-original-title="Search"><i class="fa fa-search"></i></button>
                </span>
            </div><!-- /input-group -->
        </form>
        <div id="datetimepicker_personal" data-employee="<?= $id ?>" data-events="<?= Url::to(['/task/personal/events', 'id' => $id]) ?>"></div>
        <div class="file-manager">
            <a class="btn btn-block btn-icon bg-dark" href="<?= Url::to(['/task/personal/chart']) ?>" title="Przejdź do analizy swojej pracy"><i class="fa fa-bar-chart"></i>Statystyki</a>
            <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-stats', 'title'=>'Ogólne')) ?>
                <h5>Kategorie</h5>
                <ul class="folder-list m-b-md" style="padding: 0">
                    <li><a href="#"> <i class="fa fa-gavel"></i> Rozprawy <span class="label bg-purple pull-right"><?= $stats['cases'] ?></span> </a></li>
                    <li><a href="#"> <i class="fa fa-tasks"></i> Zadania kancelaryjne <span class="label bg-teal pull-right"><?= $stats['tasks'] ?></span> </a></li>
                    <li><a href="#"> <i class="fa fa-tasks"></i> Czynności administracyjne <span class="label bg-grey pull-right"><?= $stats['action_admin'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-suitcase"></i> Czynności na rzecz klienta <span class="label bg-grey pull-right"><?= $stats['action_customer'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-user"></i> Zadania osobiste <span class="label bg-navy pull-right"><?= $stats['todo'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-handshake-o"></i> Spotkania <span class="label bg-orange pull-right"><?= $stats['meetings'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-flash"></i> Marketing <span class="label bg-pink pull-right"><?= $stats['marketing'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-sun-o"></i> Urlop / Dzień wolny <span class="label bg-yellow pull-right"><?= $stats['holiday'] ?></span></a></li>
                    <li><a href="#"> <i class="fa fa-heartbeat"></i> L4 <span class="label bg-blue pull-right"><?= $stats['l4'] ?></span></a></li>
                </ul>
                <h5>Priorytety</h5>
                <ul class="category-list" style="padding: 0">
                    <li><i class="fa fa-circle text--blue"></i> niski </li>
                    <li><i class="fa fa-circle text--yellow"></i> średni </li>
                    <li><i class="fa fa-circle text--red"></i> ważny </li>
                </ul>

                <div class="clearfix"></div>
            <?php $this->endContent(); ?>
        </div>
    </div>
    <div class="col-md-9 col-sm-6 col-xs-12">
        <div class="grid">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="mini-stat clearfix bg-purple rounded">
                    <span class="mini-stat-icon"><i class="fa fa-calendar text--purple"></i></span>
                    <div class="mini-stat-info">  <span class="todo-date"><?= date('Y-m-d') ?></span> aktywny dzień </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="mini-stat clearfix bg-teal rounded">
                    <span class="mini-stat-icon"><i class="fa fa-hourglass-3 text--teal"></i></span>
                    <div class="mini-stat-info">  <span class="todo-hours_per_day"><?= $stats['hours_per_day'] ?> </span> Liczba h na wybrany dzień </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="mini-stat clearfix bg-orange rounded">
                    <span class="mini-stat-icon"><i class="fa fa-hourglass text--orange"></i></span>
                    <div class="mini-stat-info">  <span class="todo-hours_per_month"><?= $stats['hours_per_month'] ?></span> Liczba h na wybrany miesiąc </div>
                </div>
            </div>
        </div>
        <div class="grid">
            
            <div class="col-md-6 col-sm-12 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-personal', 'title'=>'Czynności')) ?>
                    <div id="datetimepicker_personal-tasks">
                        <?php
                            $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
                            $dictPersonal = \backend\Modules\Task\models\CalTodo::dictTypes();
                            foreach($actions as $key => $action) {
                                if($action['action_type'] == 'todo') {
                                    $color = $dictPersonal[$action['type_fk']]['color'];
                                    $icon = $dictPersonal[$action['type_fk']]['icon'];
                                    $url = Url::to(['/task/personal/updateajax', 'id' => $action['id']]);
                                } else {
                                    $color = $dict[$action['type_fk']]['color'];
                                    $icon = $dict[$action['type_fk']]['icon'];
                                    $url = Url::to(['/accounting/action/change', 'id' => $action['id']]);
                                }
                                $btn = '<div class="btn-group pull-right">'
                                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                                    .'Operacje <span class="caret"></span>'
                                                .'</button>'
                                                .'<ul class="dropdown-menu">'
                                                    //. (($task->is_close == 0) ? '<li><a href="'.Url::to(['/task/personal/state', 'id' => $task->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                                                    .'<li><a href="'.$url .'" data-target="#modal-grid-item" title="Edytuj zadanie" class="gridViewModal"><i class="fa fa-pencil text--grey2"></i>Edytuj zadanie</a></li>'
                                                    //.'<li><a href="'.Url::to(['/task/personal/task', 'id'=>$action['id']]).'?type=todo&e='.$id.'" data-target="#modal-grid-item" class="gridViewModal" title="Zarejestruj czynność"><i class="fa fa-calendar-plus-o text--purple2"></i>Zarejestruj czynność</a></li>' 
                                                .'</ul>'
                                            .'</div><div class="clear"></div>';
                                    $bg = '';
                                    //if($task->is_close == 1) $bg = " bg-green2";
                                    echo    '<div class="bs-callout bs-callout-'.$color. $bg.'" id="tid-'.$action['id'].'">'
                                                .'<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.$action['name'].$btn.'</h4>'
                                                .'<p>'
                                                    .'<small><span class="text-muted"><i class="fa fa-clock-o"></i> </span> '.( ($action['todo_time']) ? $action['todo_time'] : 'brak').'</small> <span class="divider">|</span>'
                                                    .'<small><span class="text-muted"><i class="fa fa-hourglass-3"></i> </span> ' .( ($action['execution_time']) ? round($action['execution_time']/60,2) : '0'). ' h</small>'
                                                .'</p>'
                                                 . ( ($action['action_type'] == 'action') ? '<p><i>na rzecz klienta: </i>'.(($action['csymbol'])?$action['csymbol']:$action['csymbol']).'</p>' : '<p><i>czynności administracyjne: </i></p>' )
                                            .'</div>';
                            }
                            if(count($actions) == 0) echo '<div class="alert alert-warning">Brak zadań osobistych na dziś</div>';
                        ?>
                    </div>
                <?php $this->endContent(); ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-firm', 'title'=>'Zadania')) ?>
                    <div id="datetimepicker_personal-events">
                        <?php
                            foreach($events as $key => $event) {
                                $color = 'default'; $icon = 'tasks';
                                if($event['event_type'] == 'task') {
                                    if($event['id_dict_todo_type_fk'] == 1) { $color = 'purple'; $icon = 'gavel'; }
                                    if($event['id_dict_todo_type_fk'] == 2) { $color = 'teal'; }
                                } else {
                                    $color = 'navy'; $icon = 'user';
                                }
                                $btn = '<div class="btn-group pull-right">'
                                            .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                                .'Opcje <span class="caret"></span>'
                                            .'</button>'
                                            .'<ul class="dropdown-menu">'
                                                . (($event['id_dict_task_status_fk']==1) ? '<li><a href="'.Url::to(['/task/event/closeajax', 'id' => $event['id']]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                                                .'<li><a href="'.Url::to(['/task/event/showajax', 'id'=>$event['id']]).'" data-target="#modal-grid-item" class="gridViewModal" title="Podglad zadania"><i class="fa fa-eye text--blue2"></i>Podgląd zadania</a></li>'
                                                .'<li><a href="'.Url::to(['/task/personal/task', 'id'=>$event['id']]).'?e='.$id.'" data-target="#modal-grid-item" class="gridViewModal" title="Zarejestruj czynność"><i class="fa fa-calendar-plus-o text--purple2"></i>Zarejestruj czynność</a></li>'
                                            .'</ul>'
                                        .'</div><div class="clear"></div>';
                                $bg = '';
                                if($event['id_dict_task_status_fk'] == 2) $bg = " bg-green2";
                                echo    '<div class="bs-callout bs-callout-'.$color. $bg.'" id="eid-'.$event['id'].'">'
                                            .'<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.($event['csymbol']?$event['csymbol']:$event['cname']).$btn.'</h4>'
                                            .'<p>'
                                                .'<small><span class="text-muted"><i class="fa fa-clock-o"></i> </span> '.( ($event['event_time']) ? $event['event_time'] : 'brak').'</small> <span class="divider">|</span>'
                                                .'<small><span class="text-muted"><i class="fa fa-hourglass-3"></i> </span> ' .( ($event['execution_time']) ? round($event['execution_time']/60,2) : '0'). ' h</small>'
                                            .'</p>'
                                        .'</div>';
                            }
                            if(count($events) == 0) echo '<div class="alert alert-warning">Brak zadań kancelaryjnych na dziś</div>';
                        ?>
                    </div>
                <?php $this->endContent(); ?>
              
            </div>
        </div>
    </div>
</div>


  <!--<div class="bs-callout bs-callout-default">
      <h4>Default Callout</h4>
      This is a default callout.
    </div>

    <div class="bs-callout bs-callout-primary">
      <h4>Primary Callout</h4>
      This is a primary callout.
    </div>

    <div class="bs-callout bs-callout-success">
      <h4>Success Callout</h4>
      This is a success callout.
    </div>

    <div class="bs-callout bs-callout-info">
      <h4>Info Callout</h4>
      This is an info callout.
    </div>

    <div class="bs-callout bs-callout-warning">
      <h4>Warning Callout</h4>
      This is a warning callout.
    </div>

    <div class="bs-callout bs-callout-danger">
      <h4>Danger Callout</h4>
      This is a danger callout.
    </div>-->