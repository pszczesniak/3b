<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <div class="grid grid--0"> 
                <div class="col-sm-5 col-xs-8">
                    <label class="control-label">Termin <i class="fa fa-info-circle text--blue" title="Brak podania godziny ustawi zadanie za cały dzień"></i></label>
                    <div class="grid grid--0">
                        <div class="col-xs-7"><input type='text' class="form-control" id="task_date" name="CalTodo[todo_date]" value="<?= $model->todo_date ?>"/></div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <div class='input-group date' id='task_time' >
                                    <input type='text' class="form-control" name="CalTodo[todo_time]" value="<?= $model->todo_time ?>" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
       
                </div>
                <div class="col-sm-3 col-xs-4">
                    <?php /*$form->field($model, 'execution_time')->textInput(['maxlength' => true])*/ ?>
                    <div class="form-group">
                        <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                        <div class="time-element">
                            <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                            <div class="time-element_divider">:</div>
                            <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'mm','maxlength' => true])->label(false) ?></div>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-3 col-xs-11">
                    <div class="form-group">
                        <label class="control-label"> Deadline </label>
                        <div class='input-group date' id='task_deadline' >
                            <input type='text' class="form-control" name="CalTodo[todo_deadline]" value="<?= $model->todo_deadline ?>" />
                            <span class="input-group-addon bg-red" title="Ustaw deadline">
                                <span class="fa fa-warning text--white" ></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1">
                    <input id="caltodo-id_priority_fk" class="form-control" name="CalTodo[id_priority_fk]" type="hidden" value="<?= $model->id_priority_fk ?>" />
                    <?php $priorityColors = [ 1 => 'blue', 2 => 'yellow', 3 => 'red' ]; ?> 
                    <div class="icons">
                        <div class="dropdown">
                            <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->id_priority_fk] ?>" id="priority-flag-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                            <ul class="dropdown-menu " role="menu">
                                <li>
                                    <ul class="list-flags">
                                        <li><span class="priority-flag fa fa-flag icon text--blue <?= ($model->id_priority_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="blue"></span> </li>
                                        <li><span class="priority-flag fa fa-flag icon text--yellow <?= ($model->id_priority_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="yellow"></span> </li>
                                        <li><span class="priority-flag fa fa-flag icon text--red <?= ($model->id_priority_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="red"></span> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?= ($model->is_close == 0) ? $form->field($model, 'todo_show')->checkbox() : '<span class="alert alert-success">Zakończone</span>' ?>
            <div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Opis</a> </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
							<?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Dokumenty</a> </h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse ">
						<div class="panel-body">
							<?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
							<?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 7, 'parentId' => $model->id, 'onlyShow' => false]) ?>
						</div>
					</div>
				</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> Kalendarz</a> </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $this->render('_calendar', ['model' => $model]) ?>
                            <?= $form->field($model, 'all_day', [/*'template' => '{input} {label}'*/])->checkbox(['id' => 'calendar-all-day']) ?>
                        </div>
                    </div>
                </div>
			</div>
		</div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?php if($model->g_action == 2) { ?> <div class="alert alert-info">Zdarzenie zostało importowane z kalendarza Google</div><?php } ?>
        <?php if($model->g_action == 1) { ?> <div class="alert alert-warning">Zdarzenie było już importowane do kalendarza Google</div><?php } ?>
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <?php if(!$model->isNewRecord) { ?> <a href="<?= Url::to(['/task/personal/deleteajax', 'id' => $model->id]) ?>" title="Usuń zadanie" class="btn btn-sm btn-danger deleteConfirm"><i class="fa fa-trash"></i></a> <?php } ?>
        <?php if($model->is_close == 0 && !$model->isNewRecord) { ?> <a href="<?= Url::to(['/task/personal/state', 'id' => $model->id]) ?>" title="Zakończ zadanie" class="btn btn-sm btn-success deleteConfirm" data-label="Zakończ"><i class="fa fa-check"></i></a> <?php } ?>
        
        <?php if(!$model->isNewRecord && $model->g_action != 2) { ?> <a href="<?= Url::to(['/task/personal/exporttogoogle', 'id' => $model->id]) ?>" title="Eksportuje zadanie do kalendarza Google" class="btn btn-sm btn-default deleteConfirm" data-label="Eksportuj"><i class="fa fa-google text--red"></i></a> <?php } ?>

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_date').on("dp.change", function (e) {
                
                                    minDate = e.date.format('YYYY-MM-DD');
                                    if( $('#task_fromDate').length > 0 ) {
                                       // $('#task_fromDate').data("DateTimePicker").minDate(minDate);
                                        $('#task_fromDate input').val(minDate);
                                    }
                                });
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
        $('#task_time').on("dp.change", function (e) {
                                    if(!e.date) {
                                        
                                        if( $('#task_fromTime').length > 0 ) {
                                            $("#task_toTime > input").prop('disabled', true).val('');
                                            $("#task_fromTime > input").prop('disabled', true).val('');
                                        }
                                    
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', true);
                                        }
                                    } else {
                                        minDate = e.date.format('HH:mm');
                                        if( $('#task_fromTime').length > 0 ) {
                                            //$('#task_fromTime').data("DateTimePicker").minDate(minDate);
                                            $('#task_fromTime input').val(minDate);
                                            
                                            $("#task_toTime > input").prop('disabled', false);
                                            $("#task_fromTime > input").prop('disabled', false);
                                        }
                                        
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', false);
                                        }
                                    }
                                });
        $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
    });
</script>

<script type="text/javascript">
    var priorityLink = document.querySelectorAll('.priority-flag');
    var priorityColors = ['grey', 'blue', 'yellow', 'red'];
    var priorityInput = document.getElementById('caltodo-id_priority_fk');
    
    for (var i = 0; i < priorityLink.length; i++) {
        priorityLink[i].addEventListener('click', function(event) {
            console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityLink.length; j++) {
               priorityLink[j].classList.remove('none');
            }
            document.getElementById('priority-flag-choice').classList.remove('text--'+priorityColors[priorityInput.value]);
            priorityInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
        
</script>
