<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php /*$form->field($model, 'name')->textInput(['maxlength' => true])*/ ?>

            <div class="grid grid--0">
                <div class=" col-md-3 col-xs-5">
                    <label class="control-label">Termin </label>
                    <input type='text' class="form-control" id="task_date" name="ActionForm[timeDate]" value="<?= $model->timeDate ?>"/>
                </div>
                <div class="col-md-4 col-xs-7">
                    <div class="form-group">
                        <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                        <div class="time-element">
                            <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                            <div class="time-element_divider">:</div>
                            <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'mm','maxlength' => true])->label(false) ?></div>
                        </div> 
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <label class="control-label">Czynność </label>
                    <?= $form->field($model, 'type')->dropDownList( \frontend\Modules\Task\models\ActionForm::listTypes(), [ 'class' => 'form-control'] )->label(false) ?> 
                </div>
            </div>

            <div class="grid">
                <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['class' => 'form-control select2' ] ) ?> </div>
                <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getListByEmployee($model->id_employee_fk), 'id', 'name'), ['class' => 'form-control select2' ] ) ?> </div>
                
                <div class="col-sm-4 col-xs-12 categories1 <?= (($model->type == 2) ? 'none' : '' ) ?>"><?= $form->field($model, 'method_1')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('normal'), [ 'class' => 'form-control'] ) ?> </div>
                <!--<div class="col-sm-3 col-xs-6 categories1"><?= $form->field($model, 'name_1')->dropDownList( \frontend\Modules\Task\models\Actionform::categories1(1), [ 'class' => 'form-control'] ) ?> </div>-->
     
                <div class="col-sm-4 col-xs-12 categories2  <?= (($model->type == 1) ? 'none' : '' ) ?>"><?= $form->field($model, 'method_2')->dropDownList( \backend\Modules\Task\models\CalTodo::listTypes(1), [ 'class' => 'form-control'] ) ?> </div>
                <!--<div class="col-sm-3 col-xs-6 categories2 none"><?= $form->field($model, 'name_2')->dropDownList( \frontend\Modules\Task\models\ActionForm::categories2(1), [ 'class' => 'form-control'] ) ?> </div>-->
            </div>
      
            <div class="categories1 <?= ($model->type == 1) ? '' : 'none' ?>">
                <?php /* $form->field($model, 'id_order_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk), 'id', 'description'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] )*/ ?> 

                <div class="grid">
                    <div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'id_case_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'id_event_fk')->dropDownList( \backend\Modules\Task\models\CalTask::getGroups($model->id_case_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                     </div>
                </div>
            </div>           
            
			<?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
            <div id="cost" class="<?= ($model->type == 1) ? '' : 'none' ?>">
                <fieldset><legend class="text--red">Koszt</legend>
                    <div class="grid">
                         <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                        </div>
                        <div class="col-sm-8 col-xs-12">
                            <?= $form->field($model, 'cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                        </div>
                        <!--<div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'cost_vat')->dropDownList(  \backend\Modules\Accounting\models\AccTax::listTaxes(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'cost_gros')->textInput(['placeholder' => 'mm','maxlength' => true]) ?> 
                        </div>-->
                    </div>
                </fieldset>
            </div>
            <div id="way" class="none">
               
            </div>
		</div>
	</div>
    
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-sm btn-success']) ?>
        <a href="<?= Url::to(['/community/meeting/view', 'id' => $meetingId]) ?>" title="Przejdź do karty spotkania" class="btn btn-sm bg-orange"><i class="fa fa-handshake-o"></i></a>
       <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
       
    });
</script>
<script type="text/javascript">
    
	document.getElementById('actionform-type').onchange = function(event) {
        $containers1 = document.getElementsByClassName('categories1');  var i1;
        $containers2 = document.getElementsByClassName('categories2');  var i2;
        
        if(event.target.value == 2) {
            for (i1 = 0; i1 < $containers1.length; i1++) {  $containers1[i1].classList.add('none');  }
            for (i2 = 0; i2 < $containers2.length; i2++) {  $containers2[i2].classList.remove('none');  }
            document.getElementById('actionform-id_department_fk').disabled = true; 
            document.getElementById('cost').classList.add('none');  
        } else {
            for (i1 = 0; i1 < $containers1.length; i1++) {  $containers1[i1].classList.remove('none');  }
            for (i2 = 0; i2 < $containers2.length; i2++) {  $containers2[i2].classList.add('none');  }
            document.getElementById('actionform-id_department_fk').disabled = false; 
            document.getElementById('cost').classList.remove('none');  
        }
    }
    
    document.getElementById('actionform-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('actionform-id_order_fk').innerHTML = result.listOrder;     
                document.getElementById('actionform-id_case_fk').innerHTML = result.listCase;               
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('actionform-id_case_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/default/events']) ?>/"+this.value+'?without=1&box=0', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-id_event_fk').innerHTML = result.events;     
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('actionform-id_employee_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/employee/info']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-id_department_fk').innerHTML = result.departments;     
            }
        }
       xhr.send();
        return false;
    }
    
    /*document.getElementById('actionform-method_1').onchange = function() {
        if(this.value == 4) {
            document.getElementById('cost').classList.remove('none');  
        } else {
            document.getElementById('cost').classList.add('none');  
        }
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/accounting/setting/services']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-name_1').innerHTML = result.list;     
            }
        }
        xhr.send();
        return false;
    }
    
    document.getElementById('actionform-method_2').onchange = function() {
        document.getElementById('cost').classList.add('none');  
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/personal/services']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-name_2').innerHTML = result.list;     
            }
        }
        xhr.send();
        return false;
    }*/
 
</script>

