<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-filter', 'title'=>'Filtr')) ?>
    <form action="<?= Url::to(['/task/personal/'.$report]) ?>" method="get">
        <div class="form-group">
            <select class="select2" name="employee">
                <?php
                    foreach(\backend\Modules\Company\models\CompanyEmployee::getList() as $key => $item) {
                        echo "<option ". ( ($employee->id == $item['id']) ? 'selected' : '' ) ." value='".$item['id']."'>".$item['fullname']."</option>";
                    }
                ?>
            </select>
        </div>
        <div class="input-group">
            <select class="form-control" name="year">
            <?php
                for( $i=date('Y'); $i>=2013; --$i ) {
                    echo "<option ". ( ($year == $i) ? 'selected' : '' ) ." value='".$i."'>".$i."</option>";
                }
            ?>
            </select>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit" data-original-title="Search"><i class="fa fa-search"></i></button>
            </span>
        </div><!-- /input-group -->
    </form>
    <br />
    <fieldset><legend>Operacje</legend>
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/personal/statsexport', 'eid' => $employee->id, 'year' => $year]) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-actions-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) ?>
    </fieldset>
<?php $this->endContent(); ?>
<!--
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-filter', 'title'=>'Inne zestawienia')) ?>
    <ul class="list-group">
        <?php 
            if($report != 'chart')
                echo '<li class="list-group-item"><a href="'. Url::to(['/accounting/report/employees']) .'">Aktywność pracowników</a></li>';
            /*if($report != 'index')
                echo '<li class="list-group-item"><a href="'. Url::to(['/accounting/report/index']) .'">Pracownicy - klienci</a></li>';*/
        ?>
    </ul>
<?php $this->endContent(); ?>
-->