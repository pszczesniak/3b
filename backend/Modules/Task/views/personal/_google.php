<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if($action == 'import') { ?>
    <div class="modal-body">
        <div class="content ">
            <div class="alert alert-info">Liczba zdarzeń odczytana z kalendarza google: <?= $events ?>. Wykonanie operacji 'Importuj' spowoduje zaciągnięcie do systemu informacji w Twojego GoogleCalendar i zapisanie ich jako Twoich zadań osobistych.</div>
            <div class="text--red"><small>* jeśli zadanie zostało wcześniej zaciągnięte do systemu to zostanie pominięte w procesie importu danych.</small></div> 
        </div>
    </div>
    <div class="modal-footer"> 
        <a href="<?= Url::to(['/task/personal/gimportdata',]) ?>" title="Importuj zadanie z kalendarza Google" class="btn btn-sm btn-default google-data" data-target="#modal-grid-item" data-label="Importuj">Importuj</a> 

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php } else { ?>
    <div class="modal-body">
        <div class="content ">
            <div class="alert alert-info">Liczba zadań osobistych do wysłania do Twojego GoogleCalendar: <?= $events ?>. Wykonanie operacji 'Eksportuj' spowoduje wysłanie do Twojego GoogleCalendar Twoich zadań osobistych.</div>
            <!--<div class="text--red"><small>* jeśli zadanie zostało wcześniej wyeksportowane  do systemu to zostanie pominięte w procesie importu danych.</small></div> -->
        </div>
    </div>
    <div class="modal-footer"> 
        <a href="<?= Url::to(['/task/personal/gexportdata', 'type' => 1]) ?>" title="Eksportuj zadania do kalendarza Google" class="btn btn-sm btn-default google-data" data-target="#modal-grid-item" data-label="Eksportuj">Tylko osobiste</a> 
        <a href="<?= Url::to(['/task/personal/gexportdata', 'type' => 2]) ?>" title="Eksportuj zadania do kalendarza Google" class="btn btn-sm btn-default google-data" data-target="#modal-grid-item" data-label="Eksportuj">Tylko kancelaryjne</a>
        <a href="<?= Url::to(['/task/personal/gexportdata', 'type' => 3]) ?>" title="Eksportuj zadania do kalendarza Google" class="btn btn-sm btn-default google-data" data-target="#modal-grid-item" data-label="Eksportuj">Wszystkie</a>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php } ?>
