<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\Correspondence */

$this->title = 'Statystyki aktywności';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twoje zadania'), 'url' => ['todo']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="/js/chart/Chart.bundle.js"></script>
<script src="/js/chart/utils.js"></script>

<div class="grid">
    <div class="col-sm-3 col-xs-12">
        <?= $this->render('_filter', ['report' => 'chart', 'year' => $year, 'employee' => $employee]) ?>
    </div>
    <div class="col-sm-9 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'correspondence-update', 'title'=>Html::encode($this->title))) ?>
            <canvas id="canvas"></canvas>
        <?php $this->endContent(); ?>
    </div>
</div>  
    
    <!--<button id="randomizeData">Randomize Data</button>
    <button id="addDataset">Add Dataset</button>
    <button id="removeDataset">Remove Dataset</button>
    <button id="addData">Add Data</button>
    <button id="removeData">Remove Data</button>-->
    <script>
        var chartData = {
            labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
            datasets: [{
                type: 'line',
                label: 'czas pracy',
                borderColor: window.chartColors.blue,
                borderWidth: 2,
                fill: false,
                data: [<?= implode(',', $stats['total']) ?>]
            }, {
                type: 'bar',
                label: 'administracyjne',
                backgroundColor: window.chartColors.red,
                data: [<?= implode(',', $stats['events']) ?>],
                borderColor: 'white',
                borderWidth: 2
            }, {
                type: 'bar',
                label: 'na rzecz klienta',
                backgroundColor: window.chartColors.green,
                data: [<?= implode(',', $stats['actions']) ?>]
            }]

        };
        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myMixedChart = new Chart(ctx, {
                type: 'bar',
                data: chartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Aktywność pracownika <?= $employee->fullname ?> w roku <?= $year ?>'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    }
                }
            });
        };

        /*document.getElementById('randomizeData').addEventListener('click', function() {
            chartData.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });
            });
            window.myMixedChart.update();
        });*/
    </script>