<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-todos"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?= ($model->is_close == 2) ? '<span class="alert alert-success">Zakończone</span>' : '' ?>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
                    <ul class="nav panel-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                        <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-file-alt"></i><span class="panel-tabs--text">Opis</span> </a></li>
                        <?php if(!$model->isNewRecord) { ?>
                        <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2a">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            <div id="no-holiday" <?= ($model->type_fk == 4) ? 'class="disabled"' : ''?>>
                                <div class="grid grid--0" > 
                                    <div class="col-sm-4 col-xs-8">
                                        <label class="control-label">Termin <i class="fa fa-info-circle text--blue" title="Brak podania godziny ustawi zadanie za cały dzień"></i></label>
                                        <div class="grid grid--0">
                                            <div class="col-xs-7"><input type='text' class="form-control" id="task_date" name="CalTodo[todo_date]" value="<?= $model->todo_date ?>"/></div>
                                            <div class="col-xs-5">
                                                <div class="form-group">
                                                    <div class='input-group date' id='task_time' >
                                                        <input type='text' class="form-control" name="CalTodo[todo_time]" value="<?= $model->todo_time ?>" />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-clock"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-4">
                                        <?php /*$form->field($model, 'execution_time')->textInput(['maxlength' => true])*/ ?>
                                        <div class="form-group">
                                            <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                                            <div class="time-element">
                                                <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                                                <div class="time-element_divider">:</div>
                                                <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'min','maxlength' => true])->label(false) ?></div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label class="control-label"> Deadline </label>
                                            <div class='input-group date' id='task_deadline' >
                                                <input type='text' class="form-control" name="CalTodo[todo_deadline]" value="<?= $model->todo_deadline ?>" />
                                                <span class="input-group-addon bg-red" title="Ustaw deadline">
                                                    <span class="fa fa-exclamation-triangle  text--white" ></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-5">
                                        <?= $form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] ) ?> 
                                    </div>
                                    <div class="col-sm-1 col-xs-1">
                                        <input id="caltodo-id_priority_fk" class="form-control" name="CalTodo[id_priority_fk]" type="hidden" value="<?= $model->id_priority_fk ?>" />
                                        <?php $priorityColors = [ 1 => 'blue', 2 => 'yellow', 3 => 'red' ]; ?> 
                                        <div class="icons">
                                            <div class="dropdown"> <?php $model->id_priority_fk = ($model->id_priority_fk) ? $model->id_priority_fk : 1 ?>
                                                <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->id_priority_fk] ?>" id="priority-flag-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                                                <ul class="dropdown-menu " role="menu">
                                                    <li>
                                                        <ul class="list-flags">
                                                            <li><span class="priority-flag fa fa-flag icon text--blue <?= ($model->id_priority_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="blue"></span> </li>
                                                            <li><span class="priority-flag fa fa-flag icon text--yellow <?= ($model->id_priority_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="yellow"></span> </li>
                                                            <li><span class="priority-flag fa fa-flag icon text--red <?= ($model->id_priority_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="red"></span> </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="col-sm-4 col-xs-6">
                                    <?= $form->field($model, 'id_dict_todo_type_fk')->dropDownList( \backend\Modules\Task\models\CalTodo::listTypes(0), [ 'class' => 'form-control select2Modal'] )->label(false) ?> 
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <?= $form->field($model, 'id_employee_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListByProject($projectId), 'id', 'fullname'), ['prompt' => '- wybierz pracownika -', 'class' => 'form-control'] )->label(false) ?> 
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <?= ($model->is_close == 0) ? $form->field($model, 'todo_show')->checkbox() : '' ?>
                                </div>
                            </div>
                            <!--
                            <div class="grid">
                                <div class="col-sm-6 cols-xs-12">
                                    <?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList($model->id_customer_fk), [ 'prompt' => '-- wybierz klienta --', 'id' => 'selectAutoCompleteModal', 'data-url' => Url::to('/crm/default/autocomplete'), ] )->label('Kontrahent') ?>             
                                </div>
                                <div class="col-sm-6 cols-xs-12">
                                    <?= $form->field($model, 'id_set_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\CustomerBranch::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] )->label('Sprawa') ?> 
                                </div>
                            </div>-->
                            <?php if($model->isNewRecord) { ?>
                            <fieldset id="createAction" <?= ($model->id_dict_todo_type_fk != 8) ? 'class="none"' : ''?>><legend>Rejestracja czynności do rozliczenia</legend>
                                <div class="alert alert-info"><?= $form->field($model, 'createAction')->checkBox() ?></div>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_service_fk')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('normal'), [ 'class' => 'form-control'] ) ?> </div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getListByEmployee($model->id_employee_fk), 'id', 'name'), ['class' => 'form-control select2' ] ) ?> </div>
                                </div>
                            </fieldset>
                            <?php } ?>
                        </div>
                        <div class="tab-pane" id="tab2b">
                            <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
                        </div>
                        <?php if(!$model->isNewRecord) { ?>
                        <div class="tab-pane" id="tab2c">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 7, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?php if($model->g_action == 2) { ?> <div class="alert alert-info">Zdarzenie zostało importowane z kalendarza Google</div><?php } ?>
        <?php if($model->g_action == 1) { ?> <div class="alert alert-warning">Zdarzenie było już importowane do kalendarza Google</div><?php } ?>
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <?php if(!$model->isNewRecord) { ?> <a href="<?= Url::to(['/task/personal/deleteajax', 'id' => $model->id]) ?>" title="Usuń zadanie" class="btn btn-sm btn-danger deleteConfirm"><i class="fa fa-trash"></i></a> <?php } ?>
        <!--<?php if($model->is_close == 0 && !$model->isNewRecord) { ?> <a href="<?= Url::to(['/task/personal/state', 'id' => $model->id]) ?>" title="Zakończ zadanie" class="btn btn-sm btn-success deleteConfirm" data-label="Zakończ"><i class="fa fa-check"></i></a> <?php } ?>-->
        
        <?php if(!$model->isNewRecord && $model->g_action != 2 && 1==2) { ?> <a href="<?= Url::to(['/task/personal/exporttogoogle', 'id' => $model->id]) ?>" title="Eksportuje zadanie do kalendarza Google" class="btn btn-sm btn-default deleteConfirm" data-label="Eksportuj"><i class="fa fa-google text--red"></i></a> <?php } ?>
        <?php if($model->type_fk == 3) { echo '<a class="btn btn-sm bg-orange" href="'.Url::to(['/community/meeting/view', 'id' => $model->id_group_fk]).'" title="Przejdź do karty spotkania"><i class="fa fa-handshake-o"></i></a>'; } ?>
        <?php if($member) { ?> <a href="<?= Url::to(['/community/meeting/view', 'id' => $member->id_meeting_fk]) ?>" title="Przejdź do karty spotkania" class="btn btn-sm bg-orange"><i class="fa fa-handshake-o"></i></a> <?php } ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_date').on("dp.change", function (e) {
                
                                    minDate = e.date.format('YYYY-MM-DD');
                                    if( $('#task_fromDate').length > 0 ) {
                                       // $('#task_fromDate').data("DateTimePicker").minDate(minDate);
                                        $('#task_fromDate input').val(minDate);
                                    }
                                });
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
        $('#task_time').on("dp.change", function (e) {
                                    if(!e.date) {
                                        
                                        if( $('#task_fromTime').length > 0 ) {
                                            $("#task_toTime > input").prop('disabled', true).val('');
                                            $("#task_fromTime > input").prop('disabled', true).val('');
                                        }
                                    
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', true);
                                        }
                                    } else {
                                        minDate = e.date.format('HH:mm');
                                        if( $('#task_fromTime').length > 0 ) {
                                            //$('#task_fromTime').data("DateTimePicker").minDate(minDate);
                                            $('#task_fromTime input').val(minDate);
                                            
                                            $("#task_toTime > input").prop('disabled', false);
                                            $("#task_fromTime > input").prop('disabled', false);
                                        }
                                        
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', false);
                                        }
                                    }
                                });
        $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
    });
</script>

<script type="text/javascript">
    var priorityLink = document.querySelectorAll('.priority-flag');
    var priorityColors = ['grey', 'blue', 'yellow', 'red'];
    var priorityInput = document.getElementById('caltodo-id_priority_fk');
    
    for (var i = 0; i < priorityLink.length; i++) {
        priorityLink[i].addEventListener('click', function(event) {
            console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityLink.length; j++) {
               priorityLink[j].classList.remove('none');
            }
            document.getElementById('priority-flag-choice').classList.remove('text--'+priorityColors[priorityInput.value]);
            priorityInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
    
    document.getElementById('caltodo-id_employee_fk').onchange = function() {
        $selectedProject = document.getElementById('caltodo-id_set_fk').value;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/personal/projects']) ?>/"+((this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltodo-id_set_fk').innerHTML = result.list;      
                document.getElementById('caltodo-id_set_fk').value = $selectedProject; 
                document.getElementById('caltodo-id_department_fk').innerHTML = result.departments;         
            }
        }
       xhr.send();
        return false;
    }
    
    /*document.getElementById('caltodo-id_set_fk').onchange = function() {
        $selectedEmployee = document.getElementById('caltodo-id_employee_fk').value;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/project/employees']) ?>/"+((this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltodo-id_employee_fk').innerHTML = result.select; 
                document.getElementById('caltodo-id_employee_fk').value = $selectedEmployee                
            }
        }
       xhr.send();
        return false;
    }*/
    <?php if($model->isNewRecord) { ?>
    document.getElementById('caltodo-id_dict_todo_type_fk').onchange = function() {
        if(this.value == 8) {
            document.getElementById('createAction').classList.remove('none');
        } else {
            document.getElementById('createAction').classList.add('none');
        }
    }
     <?php } ?>   
</script>
<script type="text/javascript">
    /*document.getElementById('selectAutoCompleteModal').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+(this.value ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                
                document.getElementById('caltodo-id_customer_branch_fk').innerHTML = result.listBranches;  
                document.getElementById('caltodo-id_set_fk').innerHTML = result.listCase;         
            }
        }
        xhr.send();
        return false;
    }*/
</script>