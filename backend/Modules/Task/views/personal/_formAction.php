<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\components\CustomHelpers;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body action-form">
        <div class="content ">
            <div class="grid">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="grid grid--0">
                        <div class=" col-md-3 col-xs-5">
                            <label class="control-label">Termin </label>
                            <input type='text' class="form-control" id="task_date" name="ActionForm[timeDate]" value="<?= $model->timeDate ?>"/>
                        </div>
                        <div class="col-md-4 col-xs-7">
                            <div class="form-group">
                                <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                                <div class="time-element">
                                    <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => '0','maxlength' => true])->label(false) ?></div>
                                    <div class="time-element_divider">:</div>
                                    <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => '0','maxlength' => true])->label(false) ?></div>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <label class="control-label">Czynność </label>
                            <?= $form->field($model, 'type')->dropDownList( \frontend\Modules\Task\models\ActionForm::listTypes(), [ 'class' => 'form-control'] )->label(false) ?> 
                        </div>
                    </div>

                    <div class="grid">
                        <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['class' => 'form-control select2' ] ) ?> </div>
                        <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getListByEmployee($model->id_employee_fk), 'id', 'name'), ['class' => 'form-control select2' ] ) ?> </div>
                        
                        <div class="col-sm-4 col-xs-12 categories1 <?= (($model->type == 2) ? 'none' : '' ) ?>"><?= $form->field($model, 'method_1')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('normal'), [ 'class' => 'form-control'] ) ?> </div>
                        <!--<div class="col-sm-3 col-xs-6 categories1"><?= $form->field($model, 'name_1')->dropDownList( \frontend\Modules\Task\models\Actionform::categories1(1), [ 'class' => 'form-control'] ) ?> </div>-->
             
                        <div class="col-sm-4 col-xs-12 categories2  <?= (($model->type == 1) ? 'none' : '' ) ?>"><?= $form->field($model, 'method_2')->dropDownList( \backend\Modules\Task\models\CalTodo::listTypes(1), [ 'class' => 'form-control'] ) ?> </div>
                        <!--<div class="col-sm-3 col-xs-6 categories2 none"><?= $form->field($model, 'name_2')->dropDownList( \frontend\Modules\Task\models\ActionForm::categories2(1), [ 'class' => 'form-control'] ) ?> </div>-->
                    </div>
              
                    <div class="categories1 <?= ($model->type == 1) ? '' : 'none' ?>">
                        <?php /* $form->field($model, 'id_order_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk), 'id', 'description'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] )*/ ?> 

                        <div class="grid">
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'id_case_fk')->dropDownList( \backend\Modules\Task\models\CalCase::getListByEmployee($model->id_customer_fk, $model->id_employee_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                            </div>
                        </div>
                    </div>           
                    
                    <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
                    <div id="cost" class="<?= ($model->type == 1) ? '' : 'none' ?>">
                        <fieldset><legend class="text--red">Koszt</legend>
                            <div class="grid">
                                <div class="col-md-3 col-sm-8 col-xs-8">
                                    <?= $form->field($model, 'cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-4">
                                    <?= $form->field($model, 'cost_currency_id')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] ) ?> 
                                </div>
                                <div class="col-md-7 col-sm-12 col-xs-12">
                                    <?= $form->field($model, 'cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                                </div>
                                <!--<div class="col-sm-4 col-xs-12">
                                    <?= $form->field($model, 'cost_vat')->dropDownList(  \backend\Modules\Accounting\models\AccTax::listTaxes(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <?= $form->field($model, 'cost_gros')->textInput(['placeholder' => 'mm','maxlength' => true]) ?> 
                                </div>-->
                            </div>
                        </fieldset>
                    </div>
                    <div id="way" class="none">
                       
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 panel-stats">
                    <div class="mini-stat clearfix bg-teal rounded">
                        <span class="mini-stat-icon"><i class="fa fa-header text--teal"></i></span>
                        <div class="mini-stat-info">  <span class="todo-hours_per_day"><?= $stats['hours_per_day'] ?> </span> w wybranym dniu </div>
                    </div>
                    <div class="mini-stat clearfix bg-orange rounded">
                        <span class="mini-stat-icon"><i class="fa fa-header text--orange"></i></span>
                        <div class="mini-stat-info">  <span class="todo-hours_per_month"><?= $stats['hours_per_month'] ?></span> w wybranym miesiącu </div>
                    </div>
                    <div class="mini-stat clearfix bg-purple rounded">
                        <div class="mini-stat-info">
                            <ol class="panel-stats-tasks">
                                <?php
									echo $stats['tasks'];
								?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-sm btn-success']) ?>
        <a class="btn btn-sm bg-teal" href="<?= Url::to(['/task/personal/actions']) ?>" title="Przejdź do rejestru czynności"><i class="fa fa-table"></i></a>
        <?= (!$isManager) ? '' : '<a class="btn btn-sm bg-pink" href="'. Url::to(['/accounting/manager/index']) .'" title="Rozliczanie czynności"><i class="fa fa-calculator"></i></a>'; ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
		$('#task_date').on("dp.change", function (e) {
			$(".panel-stats").parent().addClass('overlay');
			var changeDate = new Date(e.date).getTime()/1000; 
			//console.log(changeDate);
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $(this).serialize(),
				url: "/task/personal/acc/"+$("#actionform-id_employee_fk").val()+"?time="+changeDate+"&cid="+$("#actionform-id_customer_fk").val(),
		
				success: function(data) {
					$(".panel-stats").parent().removeClass('overlay');

					$(".todo-hours_per_day").html(data.stats.hours_per_day);
					$(".todo-hours_per_month").html(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);
					
					document.getElementById('actionform-id_order_fk').innerHTML = data.listOrder; 
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					//alert('Something went to wrong.Please Try again later...');
					$(".panel-stats").parent().removeClass('overlay');
				}
			});
			return false;
		});
       
    });
</script>
<script type="text/javascript">
    
	document.getElementById('actionform-type').onchange = function(event) {
        $containers1 = document.getElementsByClassName('categories1');  var i1;
        $containers2 = document.getElementsByClassName('categories2');  var i2;
        
        if(event.target.value == 2) {
            for (i1 = 0; i1 < $containers1.length; i1++) {  $containers1[i1].classList.add('none');  }
            for (i2 = 0; i2 < $containers2.length; i2++) {  $containers2[i2].classList.remove('none');  }
            document.getElementById('actionform-id_department_fk').disabled = true; 
            document.getElementById('cost').classList.add('none');  
        } else {
            for (i1 = 0; i1 < $containers1.length; i1++) {  $containers1[i1].classList.remove('none');  }
            for (i2 = 0; i2 < $containers2.length; i2++) {  $containers2[i2].classList.add('none');  }
            document.getElementById('actionform-id_department_fk').disabled = false; 
            document.getElementById('cost').classList.remove('none');  
        }
    }
    
    document.getElementById('actionform-id_customer_fk').onchange = function() {
        changeDate = new Date($("#task_date").val()).getTime()/1000;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0)+"?actionPanel=true&eid="+$("#actionform-id_employee_fk").val()+"&date="+changeDate, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                //document.getElementById('actionform-id_order_fk').innerHTML = result.listOrder;     
                document.getElementById('actionform-id_case_fk').innerHTML = result.listCase; 
                document.getElementById('actionform-id_order_fk').innerHTML = result.listOrder; 
                document.getElementById('actionform-id_event_fk').innerHTML = '';                   
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('actionform-id_case_fk').onchange = function() {
        changeDate = new Date($("#task_date").val()).getTime()/1000; 
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/default/events']) ?>/"+( (this.value) ? this.value : 0)+'?eid='+$("#actionform-id_employee_fk").val()+'&date='+changeDate+'&without=0&box=0', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-id_event_fk').innerHTML = result.events;     
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('actionform-id_employee_fk').onchange = function() {
        changeDate = new Date($("#task_date").val()).getTime()/1000; 
        $(".panel-stats").parent().addClass('overlay');
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/employee/info']) ?>/"+( (this.value) ? this.value : 0)+"?cid="+$("#actionform-id_case_fk").val()+"&date="+changeDate+"&customer="+$("#actionform-id_customer_fk").val(), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-id_department_fk').innerHTML = result.departments;  
                document.getElementById('actionform-id_case_fk').innerHTML = result.sets;   
                document.getElementById('actionform-id_event_fk').innerHTML = result.events;    

                $(".panel-stats").parent().removeClass('overlay');

                $(".todo-hours_per_day").html(result.stats.hours_per_day);
                $(".todo-hours_per_month").html(result.stats.hours_per_month);
                $(".panel-stats-tasks").html(result.stats.tasks);   
            }
        }
       xhr.send();
        return false;
    }
    
    /*document.getElementById('actionform-method_1').onchange = function() {
        if(this.value == 4) {
            document.getElementById('cost').classList.remove('none');  
        } else {
            document.getElementById('cost').classList.add('none');  
        }
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/accounting/setting/services']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-name_1').innerHTML = result.list;     
            }
        }
        xhr.send();
        return false;
    }
    
    document.getElementById('actionform-method_2').onchange = function() {
        document.getElementById('cost').classList.add('none');  
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/personal/services']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('actionform-name_2').innerHTML = result.list;     
            }
        }
        xhr.send();
        return false;
    }*/
 
</script>

