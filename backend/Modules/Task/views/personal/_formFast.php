<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task <?= ($model->type==3) ? 'bg-yellow2' : 'bg-blue2' ?>">
        <div class="content ">
            <div class="grid">
                <div class="col-sm-5 col-xs-6"> 
                    <div class="form-group">
                        <label class="control-label" for="actionfast-task_fromDate">Początek</label>
                        <input type='text' class="form-control" id="task_fromDate" name="ActionFast[date_from]" value="<?= $model->date_from ?>"/>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-6">
                    <div class="form-group">
                        <label class="control-label" for="actionfast-task_toDate">Koniec</label>
                        <input type='text' class="form-control" id="task_toDate" name="ActionFast[date_to]" value="<?= $model->date_to ?>"/>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12"><?= $form->field($model, 'hours_of_day')->textInput( \frontend\Modules\Task\models\ActionForm::listTypes(), [ 'class' => 'form-control'] )->label('Ilość godzin') ?></div>
            </div>
            <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Podaj dodatkowy opis...'])->label(false) ?>
            <p><small class="text--red">* szybka rejestracja spowoduje automatyczne rozbicie czynności na dni w zadanym zakresie dat</small></p>
        </div>      
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-sm '.(($model->type==3) ? 'bg-yellow' : 'bg-blue') ]) ?>
       <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $('#task_fromDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#task_fromDate').on("dp.change", function (e) {
    
                        //minDate = e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm');
                        minDate = e.date.format('YYYY-MM-DD');
                        //$('#task_toDate').data("DateTimePicker").minDate(minDate);
                        $('#task_toDate').val(minDate);
                    });
    $('#task_toDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });      
</script>
