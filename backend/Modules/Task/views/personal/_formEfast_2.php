<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\components\CustomHelpers;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modal-grid',  'enableAjaxValidation' => true, 'enableClientValidation' => true, 'data-target' => "#modal-grid-event"],
    //'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
]); ?>
    <div class="modal-body action-form">
        <div class="content">
            <div class="grid grid--0">
                <div class=" col-md-3 col-xs-5">
                    <label class="control-label">Termin </label>
                    <input type='text' class="form-control" id="fast_action_date" name="CalTodo[todo_date]" value="<?= $model->todo_date ?>"/>
                </div>
                <div class="col-md-4 col-xs-7">
                    <div class="form-group">
                        <label class="control-label" for="caltodo-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                        <div class="time-element">
                            <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                            <div class="time-element_divider">:</div>
                            <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'min','maxlength' => true])->label(false) ?></div>
                        </div> 
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <?= $form->field($model, 'id_dict_todo_type_fk')->dropDownList( \backend\Modules\Task\models\CalTodo::listTypes(1), [ 'class' => 'form-control'] ) ?>
                </div>
            </div>
            <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
		</div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-primary']) ?>
        <a class="btn btn-sm bg-teal" href="<?= Url::to(['/task/personal/actions']) ?>" title="Przejdź do rejestru czynności"><i class="fa fa-table"></i></a>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#fast_action_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
		/*$('#fast_action_date').on("dp.change", function (e) {
			$(".panel-stats").parent().addClass('overlay');
			var changeDate = new Date(e.date).getTime()/1000; console.log(changeDate);
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $(this).serialize(),
				url: "/task/personal/acc/"+$("#actionform-id_employee_fk").val()+"?time="+changeDate,
		
				success: function(data) {
					$(".panel-stats").parent().removeClass('overlay');

					$(".todo-hours_per_day").text(data.stats.hours_per_day);
					$(".todo-hours_per_month").text(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					//alert('Something went to wrong.Please Try again later...');
					$(".panel-stats").parent().removeClass('overlay');
				}
			});
			return false;
		});*/
       
    });
</script>


