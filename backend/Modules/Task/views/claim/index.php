<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rejestr roszczeń');
$this->params['breadcrumbs'][] = 'Sprawy';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-claims-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-claims" data-form="#filter-acc-claims-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="claims-filter" aria-expanded="true" href="#claims-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="claims-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
    <div class="grid">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-green rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
                <div class="mini-stat-info">  <span class="claims_all">0 </span> Należność [PLN] </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-purple rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--purple"></i></span>
                <div class="mini-stat-info">  <span class="claims_amount">0 </span> Kwota [PLN] </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-red rounded">
                <span class="mini-stat-icon"><i class="fa fa-percent text--red"></i></span>
                <div class="mini-stat-info">  <span class="claims_lump">0 </span> Odsetki [PLN] </div>
            </div>
        </div>
    </div>
    <div id="toolbar-claims" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/claim/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-claims-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) ?>

        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-claims"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-claims"> 
        <table  class="table table-striped table-items table-widget"  id="table-claims"
                data-toolbar="#toolbar-claims" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                
                data-checkbox-header="true"
                data-maintain-selected="true"
                data-response-handler="responseHandlerChecked"
                
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-method="get"
				data-search-form="#filter-acc-claims-search"
                data-url=<?= Url::to(['/task/claim/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>                   
                    <th data-field="customer" data-sortable="true">Klient</th>
                    <th data-field="type" data-align="center">Typ</th>
                    <th data-field="name" data-sortable="true">Nazwa</th>
                    <th data-field="dates" data-sortable="false" data-align="center" data-width="70px">Data wystawienia</br>Data płatności</th>
                    <th data-field="value" data-sortable="false" data-align="right">Kwota</th>
                    <th data-field="details" data-sortable="false"><i class="fa fa-calculator"></i></th>
                    <th data-field="note" data-sortable="false" data-width="20px"><i class="fa fa-comment"></i></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="20px"></th>
                </tr>
            </thead>
            <tbody class="no-scroll">

            </tbody>            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <!--<tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>-->
                    <tr><td class="calendar-legend-icon" style="background-color: #d0e9c6"></td><td>Rozliczone</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
<?php $this->endContent(); ?>

	