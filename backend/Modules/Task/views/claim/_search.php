<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-claims-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-claims']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-4 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-claims', 'data-form' => '#filter-acc-claims-search'  ] ) ?></div>        
		<div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-caseclaim-date_from">
				<label for="caseclaim-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="caseclaim-date_from" name="CaseClaim[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-caseclaim-date_to">
				<label for="caseclaim-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="caseclaim-date_to" name="CaseClaim[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        
        <div class="col-md-2 col-sm-4 col-xs-12"><?= $form->field($model, 'id_dict_claim_type_fk')->dropDownList(\backend\Modules\Task\models\CaseClaim::claimTypes(), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-claims', 'data-form' => '#filter-acc-claims-search' ] ) ?></div>               
        <div class="col-md-4 col-sm-8 col-xs-12"><?= $form->field($model, 'title')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-claims', 'data-form' => '#filter-acc-claims-search' ]) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    
    document.getElementById('accclaimssearch-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0), true)
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accclaimssearch-id_order_fk').innerHTML = result.listOrder;     
            }
        }
       xhr.send();
        return false;
    }
    
</script>