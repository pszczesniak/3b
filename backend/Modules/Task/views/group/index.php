<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rejestr scalania spraw');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 'advanced']); ?>
    <?= Alert::widget() ?>
	<div id="toolbar-matters" class="btn-group toolbar-table-widget">
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-matters"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-matters"
                data-toolbar="#toolbar-matters" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-order="desc"
                data-method="get"
				data-detail-view="true"
				data-detail-formatter="mergeFormatter"
				data-search-form="#filter-task-matters-search"
                data-url=<?= Url::to(['/task/group/data', 'type' => 'normal']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="typeFormatter" data-visible="false">formatter</th>
					<th data-field="client"  data-sortable="true" >Klient</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true" >Sprawa</th>
                    <th data-field="mdate"  data-sortable="true" data-align="center" >Data</th>
					<th data-field="memployee"  data-sortable="true" data-align="center" >Scalił(a)</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="160px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

