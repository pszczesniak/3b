<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-matters"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select-ajax', 'data-type' => 'matter', 'multiple' => 'multiple', ] ); ?>
            <?= $form->field($model, 'employees_list')->dropDownList( []/* ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['data-placeholder' => 'wybierz pracowników', 'class' => 'form-control chosen-select', 'multiple' => true] ) ?>    
        </div>
        <small class="text--blue"><i class="fa fa-info-circle"></i>&nbsp;przy opracji dołączania pracowników do sprawy zostaną automatycznie dodatni kierownicy wybranych działów </small>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(( ($model->user_action == 'addEmployees') ? 'Dołącz pracowników' : 'Odłącz pracowników'), ['class' => 'btn btn-sm bg-purple']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

