<table  class="table table-striped table-items header-fixed"  id="table-cpayments"
        data-toolbar="#toolbar-cpayments" 
        data-toggle="table" 
        data-search="false" 
		data-show-refresh="true" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"
        data-url=<?= $dataUrl ?>
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-side-pagination="client"
        data-row-style="rowStyle">	  
        
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="delay" data-visible="false">DELAY</th>
            <th data-field="className" data-visible="false">className</th>
            <th data-field="title" data-sortable="true">Tytuł</th>
            <th data-field="term" data-sortable="true" data-width="50px" data-align="center">Data</th>
            <th data-field="amount" data-sortable="true" data-align="right">Kwota</th>
            <th data-field="employee" data-sortable="true">Pracownik</th>
            <th data-field="capital" data-sortable="true" data-align="right">Kapitał [PLN]</th>
            <th data-field="interest" data-sortable="true" data-align="right">Odsetki [PLN]</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    $('#table-cpayments').bootstrapTable({});
</script>