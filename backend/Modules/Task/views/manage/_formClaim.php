<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxFormWithAttach', 'data-target' => "#modal-grid-item", 'data-table' => "#table-claims", 'enctype' => 'multipart/form-data', 'data-model' => 'CaseClaim'/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                <li><a data-toggle="tab" href="#tab2a"><i class="fa fa-file-text"></i><span class="panel-tabs--text">Opis</span> </a></li>
                <li><a data-toggle="tab" href="#tab3a"><i class="fa fa-percent"></i><span class="panel-tabs--text">Prowizje</span> </a></li>
                <li><a data-toggle="tab" href="#tab4a"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Załączniki</span> </a></li>
                <?php if(!$model->isNewRecord) { ?>
                <li><a data-toggle="tab" href="#tab5a"><i class="fa fa-money"></i><span class="panel-tabs--text">Wpłaty</span> </a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1a">
                    <div class="grid">        
                        <div class="col-xs-12"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?> </div>
                        <div class="col-sm-6 col-xs-12"> 
                            <fieldset><legend>Szczegóły</legend>
                                <div class="grid">
                                    <div class="col-sm-6">
                                        <div class="form-group field-caseclaim-occurence_date">
                                            <label for="caseclaim-history_date" class="control-label">Data</label>
                                            <input type='text' class="form-control date-calendar" id="date-occurence_date" name="CaseClaim[occurence_date]" value="<?= $model->occurence_date ?>" />
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Task\models\CaseClaim::claimGroups(), ['class' => 'form-control'] ) ?> </div>
                                </div>
                                
                                <div>
                                    <?= $form->field($model, 'id_dict_claim_type_fk', ['template' => '
                                                  {label}
                                                   <div class="input-group ">
                                                        {input}
                                                        <span class="input-group-addon bg-green">'.
                                                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/13' , 
                                                                ['class' => 'insertInline text--white', 
                                                                 'data-target' => "#claim-type-insert", 
                                                                 'data-input' => ".claim-type"
                                                                ])
                                                        .'</span>
                                                   </div>
                                                   {error}{hint}
                                               '])->dropDownList( \backend\Modules\Task\models\CaseClaim::claimTypes(), ['class' => 'form-control claim-type'] ) ?>
                                    <div id="claim-type-insert" class="insert-inline bg-purple2 none"> </div>
                                </div>
                                <div class="grid <?= ($model->type_fk == 2) ? 'none' : '' ?>" id="group-1">
                                    <div class="col-sm-4 col-xs-3"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [ 'class' => 'form-control'] ) ?> </div>
                                    <div class="col-sm-8 col-xs-9"><?= $form->field($model, 'amount')->textInput( ['class' => 'form-control number'] ) ?></div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_original')->textInput( ['class' => 'form-control number'] ) ?></div>
                                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'amount_cost')->textInput( ['class' => 'form-control number'] ) ?></div>
                                </div>
                            </fieldset>
                        </div>   
                        <div class="col-sm-6 col-xs-12"> 
                            <fieldset><legend>Odsetki <small id="caseclaim-interest" class="text--red"></small></legend>
                                <div class="grid">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group field-caseclaim-payment_date">
                                            <label for="caseclaim-history_date" class="control-label">Termin płatności</label>
                                            <input type='text' class="form-control date-calendar" id="date-payment_date" name="CaseClaim[payment_date]" value="<?= $model->payment_date ?>" />
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group field-caseclaim-interest_date">
                                            <label for="caseclaim-history_date" class="control-label">Naliczaj odsetki od</label>
                                            <input type='text' class="form-control date-calendar" id="date-interest_date" name="CaseClaim[interest_date]" value="<?= $model->interest_date ?>" />
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>
                                <?= $form->field($model, 'id_dict_interest_type_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Config\models\Indicator::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control'] ) ?> 
                                <?= $form->field($model, 'calcualte_contractual_interest')->checkBox()  ?>
                                <div class="grid alert alert-info">
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="form-group field-caseclaim-contractual_interest_date">
                                            <label for="caseclaim-history_date" class="control-label">Naliczaj od</label>
                                            <input type='text' class="form-control date-calendar" id="date-contractual_interest_date" name="CaseClaim[contractual_interest_date]" value="<?= $model->contractual_interest_date ?>" disabled="<?= (($model->calcualte_contractual_interest) ? false : true) ?>" />
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'contractual_interest_rate')->textInput( ['class' => 'form-control', 'disabled' => (($model->calcualte_contractual_interest) ? false : true)] ) ?></div>
                                </div>
                            </fieldset>
                        </div>       
                    </div>
                </div>
                <div class="tab-pane" id="tab2a">
                    <?= $form->field($model, 'note')->textarea(['rows' => 3])->label(false) ?>
                </div>
                <div class="tab-pane" id="tab3a">
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'provision_capital')->textInput( ['class' => 'form-control number'] ) ?></div>
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'provision_interest')->textInput( ['class' => 'form-control number'] ) ?></div>
                    </div>
                </div>
                <div class="tab-pane" id="tab4a">
                    <?php if(!$model->isNewRecord) { ?>
                        <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 10, 'parentId' => $model->id, 'onlyShow' => false]) ?> 
                    <?php } else { ?>
                        <div class="files-wr" data-count-files="1" data-title="<?= \Yii::t('app', 'Attach file') ?>">
                            <div class="one-file">
                                <label for="file-1"><?= \Yii::t('app', 'Attach file') ?></label>
                                <input class="attachs" name="file-1" id="file-1" type="file">
                                <div class="file-item hide-btn">
                                    <span class="file-name"></span>
                                    <span class="btn btn-del-file">x</span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php if(!$model->isNewRecord) { ?>
                <div class="tab-pane" id="tab5a">
                    <?= $this->render('_payments', ['dataUrl' => Url::to(['/task/claim/payments', 'id' => $model->id])]) ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
        
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">
    $(function () {
        $('.date-calendar').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#date-occurence_date').on("dp.change", function (e) {            
                        minDate = e.date.format('YYYY-MM-DD');
                        minDateNextDay = e.date.add(1, 'day').format('YYYY-MM-DD');
                        if( $('#date-payment_date').length > 0 ) {
                            $('#date-payment_date').val(minDate);
                        }
                        if( $('#date-interest_date').length > 0 ) {
                            $('#date-interest_date').val(minDateNextDay);
                        }
                    });
    });
    $('#table-payments').bootstrapTable({});
</script>
<script type="text/javascript">
    document.getElementById('caseclaim-type_fk').onchange = function() {
        if(this.value == 1) {
            document.getElementById('group-1').classList.remove('none');
        } else {
            document.getElementById('group-1').classList.add('none');
        }
       
       return false;
    }
    
    document.getElementById('caseclaim-calcualte_contractual_interest').onchange = function(event) {
        if(!event.target.checked) document.getElementById('date-contractual_interest_date').disabled = true; 
        if(!event.target.checked) document.getElementById('caseclaim-contractual_interest_rate').disabled = true; 
        if(event.target.checked) document.getElementById('date-contractual_interest_date').disabled = false; 
        if(event.target.checked) document.getElementById('caseclaim-contractual_interest_rate').disabled = false; 
    }
    
    document.getElementById('caseclaim-id_dict_interest_type_fk').onchange = function() {
	   calculateInterest();      
       return false;
    }
	
	document.getElementById('caseclaim-amount').onfocusout = function() {
	   calculateInterest();      
       return false;
    }
    
    function calculateInterest() {
		var xhr_name = new XMLHttpRequest();
		xhr_name.open('POST', "<?= Url::to(['/task/claim/interest']) ?>?amount="+document.getElementById('caseclaim-amount').value+"&date="+document.getElementById('date-payment_date').value+"&indicator="+document.getElementById('caseclaim-id_dict_interest_type_fk').value, true);
		xhr_name.onreadystatechange=function()  {
			if (xhr_name.readyState==4 && xhr_name.status==200)  {
				var result = JSON.parse(xhr_name.responseText);
                if(result.interest != false)
                    document.getElementById('caseclaim-interest').innerHTML = result.interest+" ["+result.delay+" dni]"; 
			}
		}
	    xhr_name.send();
		
		return false;
	}
</script>
