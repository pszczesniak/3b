<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-instances"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <?php if($model->id_case_fk == 0) { ?>
	<div class="grid">
		<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>
		<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_case_fk')->dropDownList( \backend\Modules\Task\models\CalCase::getGroups($model->id_customer_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?></div>
	</div>
	<?php } ?>
	<div class="col-xs-12"><?= $form->field($model, 'is_active')->checkBox()  ?></div>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                <li><a data-toggle="tab" href="#tab2a"><i class="fa fa-file-text"></i><span class="panel-tabs--text">Rezultat</span> </a></li>
				<li><a data-toggle="tab" href="#tab4a"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Załączniki</span> </a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1a">
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'instance_name')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'instance_sygn')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-sm-3 col-xs-12">
                            <?= $form->field($model, 'address_type_fk')->dropDownList( \backend\Modules\Correspondence\models\CorrespondenceAddress::listTypes(), ['prompt' => 'wybierz', 'class' => 'form-control'] )->label('Typ') ?>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <?= $form->field($model, 'instance_address_fk', ['template' => '
                                      {label}
                                       <div class="input-group ">
                                            {input}
                                            <span class="input-group-addon bg-green">'.
                                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/correspondence/address/createinline']) , 
                                                    ['class' => 'insertInline text--white', 
                                                     'data-target' => "#address-insert", 
                                                     'data-input' => ".matter-instance"
                                                    ])
                                            .'</span>
                                       </div>
                                       {error}{hint}
                                   '])->dropDownList(ArrayHelper::map(\backend\Modules\Correspondence\models\CorrespondenceAddress::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control matter-instance select2'] ) ?>        
                            <div id="address-insert" class="insert-inline bg-teal2 none"> </div>
                        </div>
                    </div>

                    <?= $form->field($model, 'instance_contact')->textInput(['maxlength' => true]) ?>
                    <div class="grid">
                        <div class="col-sm-6 col-xs-12">
                            <?= $form->field($model, 'id_dict_instance_status_fk', ['template' => '
                                      {label}
                                       <div class="input-group ">
                                            {input}
                                            <span class="input-group-addon bg-green">'.
                                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/16' , 
                                                    ['class' => 'insertInline text--white', 
                                                     'data-target' => "#instance-status-insert", 
                                                     'data-input' => ".instance-status"
                                                    ])
                                            .'</span>
                                       </div>
                                       {error}{hint}
                                   '])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listStatuses(), ['prompt' => '- wybierz -', 'class' => 'form-control instance-status'] ) ?>
                            <div id="instance-status-insert" class="insert-inline bg-purple2 none"> </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?= $form->field($model, 'id_dict_instance_mode_fk', ['template' => '
                                      {label}
                                       <div class="input-group ">
                                            {input}
                                            <span class="input-group-addon bg-green">'.
                                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/17' , 
                                                    ['class' => 'insertInline text--white', 
                                                     'data-target' => "#instance-mode-insert", 
                                                     'data-input' => ".instance-mode"
                                                    ])
                                            .'</span>
                                       </div>
                                       {error}{hint}
                                   '])->dropDownList( \backend\Modules\Task\models\CalCaseInstance::listModes(), ['prompt' => '- wybierz -', 'class' => 'form-control instance-mode'] ) ?>
                            <div id="instance-mode-insert" class="insert-inline bg-purple2 none"> </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2a">
                    <div class="grid">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group field-calcaseinstance-date_end">
                                <label for="calcaseinstance-history_date" class="control-label">Data końca</label>
                                <input type='text' class="form-control date-calendar" id="date-calendar_end" name="CalCaseInstance[date_end]" value="<?= $model->date_end ?>" />
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group field-calcaseinstance-date_legalization">
                                <label for="calcaseinstance-date_legalization" class="control-label">Data uprawomocnienia</label>
                                <input type='text' class="form-control date-calendar" id="date-date_legalization" name="CalCaseInstance[date_legalization]" value="<?= $model->date_legalization ?>" />
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group field-calcaseinstance-date_opposition">
                                <label for="calcaseinstance-date_opposition" class="control-label">Data sprzeciwu</label>
                                <input type='text' class="form-control date-calendar" id="date-calendar_opposition" name="CalCaseInstance[date_opposition]" value="<?= $model->date_opposition ?>" />
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <?= $form->field($model, 'judgment')->textarea(['rows' => 3]) ?>
                </div>
				<div class="tab-pane" id="tab4a">
                    <?php if(!$model->isNewRecord) { ?>
                        <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 11, 'parentId' => $model->id, 'onlyShow' => false]) ?> 
                    <?php } else { ?>
                        <div class="files-wr" data-count-files="1" data-title="<?= \Yii::t('app', 'Attach file') ?>">
                            <div class="one-file">
                                <label for="file-1"><?= \Yii::t('app', 'Attach file') ?></label>
                                <input class="attachs" name="file-1" id="file-1" type="file">
                                <div class="file-item hide-btn">
                                    <span class="file-name"></span>
                                    <span class="btn btn-del-file">x</span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>                    
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('.date-calendar').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    });
</script>    
<script type="text/javascript">
    document.getElementById('calcaseinstance-address_type_fk').onchange = function() {
        var opts = [], opt;
        var sel = document.getElementById('calcaseinstance-address_type_fk');
        for (var i=0, len=sel.options.length; i<len; i++) {
            opt = sel.options[i];
            if ( opt.selected ) {
                opts.push(opt.value);
                /*if (fn) {  fn(opt);  }*/
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/correspondence/address/filter']) ?>?ids="+opts.join(), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('calcaseinstance-instance_address_fk').innerHTML = result.addresses; 
            }
        }
       xhr.send();
       
       return false;
    }
</script>
<?php if($model->id_case_fk == 0) { ?>
	<script type="text/javascript"> 
		document.getElementById('calcaseinstance-id_customer_fk').onchange = function() {
			var xhr = new XMLHttpRequest();
			xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+( (this.value) ? this.value : 0 ), true);
			xhr.onreadystatechange=function()  {
				if (xhr.readyState==4 && xhr.status==200)  {
					var result = JSON.parse(xhr.responseText);
					document.getElementById('calcaseinstance-id_case_fk').innerHTML = result.list; 
				}
			}
		   xhr.send();
			return false;
		}
	</script>
<?php } ?>