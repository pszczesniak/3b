<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-matters"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="content ">
            <?= $form->field($model, 'resultDict', ['template' => '
                          {label}
                           <div class="input-group ">
                                {input}
                                <span class="input-group-addon bg-green">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/14' , 
                                        ['class' => 'insertInline text--white', 
                                         'data-target' => "#result-type-insert", 
                                         'data-input' => ".result-type"
                                        ])
                                .'</span>
                           </div>
                           {error}{hint}
                       '])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(14), ['prompt' => '- wybierz -', 'class' => 'form-control result-type'] ) ?>
            <div id="result-type-insert" class="insert-inline bg-purple2 none"> </div>

            <fieldset><legend>Wyrok/Notatki</legend>   
                <?= $form->field($model, 'resultNote')->textarea(['rows' => 3])->label(false) ?>
            </fieldset>	  
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton('Zamknij sprawy', ['class' => 'btn btn-sm bg-green']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

