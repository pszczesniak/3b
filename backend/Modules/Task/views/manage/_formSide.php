<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'], 
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => $tableId/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">

    <div class="grid">        
		<div class="col-xs-12">
			<?= $form->field($model, 'id_side_fk', ['template' => '
					  {label}
					   <div class="input-group ">
							{input}
							<span class="input-group-addon bg-green">'.
								Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/side/createinline']) , 
									['class' => 'insertInline text--white', 
									 'data-target' => "#side-insert", 
									 'data-input' => ".matter-side"
									])
							.'</span>
					   </div>
					   {error}{hint}
				   '])->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\Customer::getOppositeSides(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control matter-side select2'] ) ?>        
			<div id="side-insert" class="insert-inline bg-teal2 none"> </div>
		</div>
		<div class="col-sm-6 col-xs-12"> <?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Task\models\CaseSide::sideTypes(), ['class' => 'form-control'] ) ?> </div>
        <div class="col-sm-6 col-xs-12">
			<?= $form->field($model, 'id_dict_side_role_fk', ['template' => '
					  {label}
					   <div class="input-group ">
							{input}
							<span class="input-group-addon bg-green">'.
								Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/11' , 
									['class' => 'insertInline text--white', 
									 'data-target' => "#side-role-insert", 
									 'data-input' => ".side-role"
									])
							.'</span>
					   </div>
					   {error}{hint}
				   '])->dropDownList( \backend\Modules\Task\models\CaseSide::sideRoles(), ['class' => 'form-control side-role'] ) ?>
			<div id="side-role-insert" class="insert-inline bg-purple2 none"> </div>
		</div>       
    </div>
    <fieldset><legend>Notatka</legend>   
        <?= $form->field($model, 'note')->textarea(['rows' => 3])->label(false) ?>
    </fieldset>	 
        
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
