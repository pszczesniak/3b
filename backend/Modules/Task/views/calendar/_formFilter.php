<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\ArrayHelper;
?>
<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-cog"></span> Opcje </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>

    <div class="panel-body in" id="calendarFilter">
        <div class="grid">
            <div class="col-xs-12">
                <fieldset><legend class="text--blue2"><i class="fa fa-print text--blue2"></i>Eksport zdarzeń kancelaryjnych</legend>
                    <?= $this->render('_formExport', ['employees' => []]) ?>
                </fieldset>
            </div>
            <div class="col-xs-12">
                <fieldset><legend class="text--purple2"><i class="fa fa-filter text--purple2"></i>Filtrowanie</legend>
                    <form id="calendar-filtering">
					<?php $form = ActiveForm::begin([
						'action' => ['index'],
						'method' => 'post',
						'id' => 'form-data-search'
					]); ?>
                       
						<label class="control-label" for="caltasksearch-type_fk">Rodzaj</label>
						<div class="form-select">
							<select id="caltasksearch-type_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[type_fk]">
								<option value=""> - wszystko -</option>
								<option value="1">Zadania</option>
								<option value="2">Informacje</option>
							</select>
						</div>
						<label class="control-label" for="caltasksearch-id_dict_task_status_fk">Status</label>
						<div class="form-select">
							<select id="caltasksearch-id_dict_task_status_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[id_dict_task_status_fk]">
								<option value=""> - wszystko -</option>
								<option value="1">Zaplanowane</option>
								<option value="2">Zakończone</option>
                                <option value="3">Odwołane</option>
							</select>
						</div>
					    <?php if(Yii::$app->params['departments']) { ?>
						<?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] ) ?>
						<?php } ?>
						<?php if(Yii::$app->params['branches']) { ?>
						<?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] )
                                                                    ->label('Region') ?>
                        <?php } ?>
						<?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control schedule-filtering-options select2' ] ) ?>
					
                    <?php ActiveForm::end(); ?>
                </fieldset>
				
				<fieldset><legend class="text--grey2"><i class="fa fa-map text--grey2"></i>Legenda</legend>
					<table class="calendar-legend">
						<tr><td class="bg-purple calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Zadania</td></tr>
						<tr><td class="label-danger calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Odwołane</td></tr>
						<tr><td class="bg-teal calendar-legend-icon"><i class="fa fa-comment"></i></td><td>Informacje</td></tr>
						<!--<tr><td class="label-success calendar-legend-icon"><i class="fa fa-comment"></i></td><td>Zakończone</td></tr>-->
					</table>
				</fieldset>
            </div>        
        </div>
    </div>
</div>
