<?php
	use yii\helpers\Html;
    use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\helpers\ArrayHelper;
    
    use frontend\widgets\socialmedia\Google;
?>

<?php /*Google::widget([])*/ ?>

<!--<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-hand-o-up"></span> Dodaj zdarzenie </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarExternal" aria-expanded="true" aria-controls="calendarExternal">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>
    
    <div class="panel-body in" id="calendarExternal">
        <div id="external-events" class="list-group">
			<div class="fc-event list-group-item bg-orange btn-icon" data-type="3" ><i class="fa fa-handshake-o"></i>Spotkanie</div>
			<div class="fc-event list-group-item bg-pink btn-icon" data-type="4"><i class="fa fa-sun-o"></i>Urlop</div>
		</div>
        <div class="alert alert-info"><small>Wybierz zdarzenie z listy i przeciągnij je w odpowiednie miejsce na kalendarzu</small></div>
    </div> 
</div>-->
    
<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-cog"></span> Opcje </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>
 
    <div class="panel-body in" id="calendarFilter">
        <div class="grid">
            <!--<div class="col-xs-12">
                <fieldset><legend class="text--blue2"><i class="fa fa-print text--blue2"></i>Eksport zadań osobistych</legend>
                    <?php /* $this->render('_formExportPersonal', ['model' => $model, 'employees' => []]) */ ?>
                </fieldset>
            </div>-->

            <div class="col-xs-12">
                <fieldset><legend class="text--teal2"><i class="fa fa-hand-up text--teal2"></i>Filtrowanie</legend>
                    <form id="calendar-filtering">
					<?php $form = ActiveForm::begin([
						'action' => ['index'],
						'method' => 'post',
						'id' => 'form-data-search'
					]); ?>
                        <div class="grid">
                            <div class="col-xs-12">
                                <label class="control-label" for="caltasksearch-type_fk">Rodzaj</label>
                                <div class="form-select">
                                    <select id="caltasksearch-type_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[type_fk]">
                                        <option value="0"> - wszystko -</option>
                                        <option value="1" selected>Osobiste</option>
                                        <option value="2">Firmowe</option>
                                    </select>
                                </div>
                            </div>
                     
                            <div class="col-xs-12">
                                <label class="control-label" for="caltasksearch-id_dict_task_status_fk">Status</label>
                                <div class="form-select">
                                    <select id="caltasksearch-id_dict_task_status_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[id_dict_task_status_fk]">
                                        <option value=""> - wszystko -</option>
                                        <option value="1">Zaplanowane</option>
                                        <option value="2">Zakończone</option>
                                        <option value="3">Odwołane</option>
                                    </select>
                                </div>
                            </div>
						<?php if(Yii::$app->params['departments']) { ?>
						<div class="col-xs-12">
						<?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] ) ?>
						</div>
						<?php } ?>
						<?php if(Yii::$app->params['branches']) { ?>
						<div class="col-xs-12">
						<?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] )
                                                                    ->label('Region') ?>
                        </div>
						<?php } ?>                        
					</div>
                    <?php ActiveForm::end(); ?>
                </fieldset>
				
				<fieldset><legend class="text--grey2"><i class="fa fa-map text--grey2"></i>Legenda</legend>
					<table class="calendar-legend">
                        <?php foreach($dicts as $key => $dict) {
                            echo '<tr><td class="calendar-legend-icon" style="background-color: '.$dict->dict_color.'"><i class="'.$dict->dict_icon.'"></i></td><td>'.$dict->name.'</td></tr>';
                        }
                        ?>
                        <tr><td class="bg-orange calendar-legend-icon"><i class="fa fa-handshake"></i></td><td>Spotkania</td></tr>
                        <!--<tr><td class="bg-teal calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Zadania firmowe</td></tr>
                        <tr><td class="label-danger calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Zadania firmowe odwołane</td></tr>
                        <tr><td class="bg-teal calendar-legend-icon"><i class="fa fa-comment-alt"></i></td><td>Informacje firmowe</td></tr>-->
                        
                        <!--<tr><td colspan=2" class="align-center">Czynności administracyjne</td></tr>-->
                        <!--<tr><td class="bg-grey calendar-legend-icon"><i class="fa fa-user"></i></td><td>Zadania osobiste</td></tr>
                        <tr><td class="bg-grey calendar-legend-icon"><i class="fa fa-hourglass"></i></td><td>TODO</td></tr>
                        <tr><td class="bg-orange calendar-legend-icon"><i class="fa fa-chart-pie"></i></td><td>Zarządzanie projektami</td></tr>
                        <tr><td class="bg-pink calendar-legend-icon"><i class="fa fa-bolt"></i></td><td>Marketing</td></tr>
                        <tr><td class="bg-yellow calendar-legend-icon"><i class="fa fa-sun"></i></td><td>Urlop <a href="<?= Url::to(['/task/personal/fast', 'type' => 3]) ?>" title="Szybkie wprowadzanie" data-title="Rejestracja urlopu" data-target="#modal-grid-item" class="viewModal"><i class="fa fa-calendar-plus text--yellow"></i></a></td></tr>
                        <tr><td class="bg-blue calendar-legend-icon"><i class="fa fa-heartbeat"></i></td><td>L4 <a href="<?= Url::to(['/task/personal/fast', 'type' => 4]) ?>" title="Szybkie wprowadzanie" data-title="Rejestracja L4" data-target="#modal-grid-item" class="viewModal"><i class="fa fa-calendar-plus text--blue"></i></a></td></tr>
                        
                        <tr><td class="bg-navy calendar-legend-icon"><i class="fa fa-briefcase"></i></td><td>Czynności na rzecz klienta</td></tr>
                        <tr><td class="bg-navy calendar-legend-icon"><i class="fa fa-car"></i></td><td>Przejazd</td></tr>
						-->
					</table>
				</fieldset>
            </div>        
        </div>
    </div>
</div>
