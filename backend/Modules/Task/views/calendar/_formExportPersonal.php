<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/task/personal/export']),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxFormExport', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data-export"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
       <div class="grid grid--0">
            <div class='col-sm-6'>
                <div class="form-group">
                    <label for="accactionssearch-date_from" class="control-label">od</label>
                    <div class='input-group date' id='datetimepicker_hstart'>
                        <input type='text' class="form-control" id="accactionssearch-date_from" name="AccActionsSearch[date_from]" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        
                    </div>
                </div>
            </div>
           
            <div class='col-sm-6'>
                <div class="form-group">
                    <label for="accactionssearch-date_to" class="control-label">do</label>
                    <div class='input-group date' id='datetimepicker_hend'>
                        <input type='text' class="form-control" id="accactionssearch-date_to" name="AccActionsSearch[date_to]" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <input id="accactionssearch-id_employee_fk" class="form-control" name="AccActionsSearch[id_employee_fk]" value="<?= $model->id_employee_fk ?>" type="hidden">
        </div>
        <div class="align-right"><?= Html::submitButton('Eksportuj swoje zadania', ['class' =>'btn btn-default']) ?></div>
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        


<?php ActiveForm::end(); ?>

