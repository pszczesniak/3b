<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
                <fieldset><legend class="bg-teal">Kontrahent <?php if(!$model->isNewRecord && $model->status == 1) { ?><a href="<?= Url::to(['/crm/customer/card', 'id' => $model->id_customer_fk]) ?>" class="viewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka klienta" title="Wizytówka klienta"><i class="fa fa-address-card text--lightgrey"></i></a><?php } ?></legend>
                <?= $form->field($model, 'id_customer_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/customer/createajax']) , 
                                                            ['class' => 'viewModal text--white', 
                                                             'id' => 'customer-create',
                                                             //'data-toggle' => ($viewModal)?"modal":"none", 
                                                             'data-target' => "#modal-grid-item", 
                                                             'data-form' => "item-form", 
                                                             'data-input' => ".correspondence-customer",
                                                             'data-title' => "Nowy klient"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-customer select2'] )->label('Kontrahent')
                                    ?>   
                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend class="bg-pink">Konfiguracja</legend>
                <?php /* $form->field($details, 'is_limited')->checkbox(['disabled' => ($model->status == -2) ? false : true])*/ ?>
 
                <div class="grid">
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="calcasedetails-date_from" class="control-label">Data rozpoczęcia</label>
                            <div class='input-group date' id='datetimepicker_date_from'>
                                <input type='text' class="form-control" id="calcasedetails-date_from" name="CalCaseDetails[date_from]" value="<?= $details->date_from ?>" <?= ($model->status == -2) ? '' : 'disabled' ?> />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="calcasedetails-date_to" class="control-label">Deadline</label>
                            <div class='input-group date' id='datetimepicker_date_to'>
                                <input type='text' class="form-control" id="calcasedetails-date_to" name="CalCaseDetails[date_to]" value="<?= $details->date_to ?>" <?= ($model->status == -2) ? '' : 'disabled' ?> />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
            </fieldset>
        </div>
    </div>
	
    <div class="grid">
		<div class="col-sm-6 col-md-8">
            <fieldset><legend>Nazwa</legend>   
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
			</fieldset>	 
            <fieldset><legend>Opis</legend>   
                <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
			</fieldset>	 
		</div>
		<div class="col-sm-6 col-md-4">
            <fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
                          {label}
                           <div class="input-group ">
                                {input}
                                <span class="input-group-addon bg-green">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/6' , 
                                        ['class' => 'insertInline text--white', 
                                         'data-target' => "#project-type-insert", 
                                         'data-input' => ".project-type"
                                        ])
                                .'</span>
                           </div>
                           {error}{hint}
                       '])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(6), ['class' => 'form-control project-type'] ) ?>
                <div id="project-type-insert" class="insert-inline bg-purple2 none"> </div>
            </fieldset>
            <fieldset><legend>Personalizacja</legend>
                <?php if(Yii::$app->params['departments']) { ?>
                    <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'project', 'multiple' => 'multiple', ] ); ?>
                <?php } ?>
                <?= $form->field($model, 'employees_list')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->orderby('lastname')->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true] ) ?> 
                <br /><br />
                <?= $form->field($model, 'id_employee_leading_fk')->dropDownList(  $lists['employees']/*ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 

            </fieldset>
		</div>
        
	</div>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    document.getElementById('calcase-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/lists']) ?>/"+this.value+"?type=matter&actionPanel=true", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_customer_person_leading_fk').innerHTML = result.contacts; 
				//document.getElementById('calcase-id_order_fk').innerHTML = result.orders; 
                //document.getElementById('calcase-departments_list').innerHTML = result.departments; 
                //$('#calcase-departments_list').multiselect('rebuild');
                //$('#calcase-departments_list').multiselect('refresh');
                //document.getElementById('menu-list-employee').innerHTML = result.employees; 
            }
        }
       xhr.send();
	   
	   //buildName();      
       return false;
    }

	function buildName() {
	
		return false;
	}

</script>