<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="validation-form" class="alert none"></div>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
		<?= $form->field($model, 'type_fk')->radioList([1 => 'Rozprawa', 2 => 'Zadanie'], 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-info' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
									]) ?>
        
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::find()->where(['id' => $model->id_customer_fk])->all(), 'id', 'name'), [] ) ?> 

		<?= $form->field($model, 'id_case_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::find()->where(['id' => $model->id_case_fk])->all(), 'id', 'name'), [] ) ?> 
		<?= $form->field($model, 'id_dict_task_type_fk')->radioList(\backend\Modules\Task\models\CalTask::listTypes(), 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-primary' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
									]) ?>
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Podstawowe</a> </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body" style="overflow: inherit !important;">
						<?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
						<div class="row">
							<div class='col-sm-6'>
								<div class="form-group">
									<label for="caltask-date_from" class="control-label">Start</label>
									<div class='input-group date' id='datetimepicker_start'>
										<input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $model->date_from ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(function () {
									$('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD  HH:mm',  });
								});
							</script>
							<div class='col-sm-6'>
								<div class="form-group">
									<label for="caltask-date_to" class="control-label">Koniec</label>
									<div class='input-group date' id='datetimepicker_end'>
										<input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $model->date_to ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(function () {
									$('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD  HH:mm' });
								});
							</script>
						</div>
						<?= $form->field($model, 'all_day', ['template' => '{input} {label}'])->checkbox() ?>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Dodatkowe</a> </h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body">

						<?= $form->field($model, 'id_dict_task_category_fk')->radioList(\backend\Modules\Task\models\CalTask::listCategory(), 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-primary' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
									]) ?>
						<?= $form->field($model, 'id_dict_task_status_fk')->radioList(\backend\Modules\Task\models\CalTask::listStatus(), 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-primary' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
									]) ?>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">  Pracownicy</a> </h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="panel-body-max">
                            <?= $this->render('_employeesListTask', ['employees' => $employees, 'case' => $model->id_case_fk]) ?>
                        </div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">  Dokumenty</a> </h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse">
					<div class="panel-body">
						<?= $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) ?>
					</div>
				</div>
			</div>
			
		</div> 
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    document.getElementById('caltask-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltask-id_case_fk').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById("caltask-id_case_fk").onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/case/employees']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('menu-list-employee').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
</script>
