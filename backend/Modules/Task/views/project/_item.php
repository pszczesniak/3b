<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

use frontend\widgets\chat\Comments;

?>
<?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/1/preview.jpg"))?"/uploads/employees/cover/1/preview.jpg":"/images/default-user.png"; ?>

<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
    <div class="main-box clearfix project-box green-box">
        <div class="main-box-body clearfix">
            <div class="project-box-header bg-purple">
                <div class="name">
                    <a href="<?= Url::to(['/task/details/config', 'id' => $model->id]) ?>">  <?= $model->name ?>  </a>
                </div>
            </div>
            <div class="project-box-content">
                <div class="tab-content widget-tabs-content">
                    <div class="tab-pane active" id="w-<?= $model->id ?>-tab-1" role="tabpanel" aria-expanded="true">
                        <div class="c100 p<?= $model->details['percent'] ?> green center"> <span><?= $model->details['percent'] ?>%</span><div class="slice"><div class="bar"></div><div class="fill"></div></div></div>
                    </div>
                    <div class="tab-pane" id="w-<?= $model->id ?>-tab-2" role="tabpanel" aria-expanded="false">
                        <div class="project-details-info">
                            <ul class="details col-xs-12 col-sm-12">
                                <li><span>Kategoria</span> <?= $model->category ?></li>
                                <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
                                <li><span>Status</span> <span class="label <?= $colors[$model->id_dict_case_status_fk] ?>"><?= $model->statusname ?></span></li>
                                <li><span>Klient</span> <a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></li>      
                                <?php if($model->details) { ?>
                                <li><span>Data rozpoczęcia</span> <span id="meeting-start-<?= $model->id ?>"><?= $model->details['date_from'] ?></span></li>
                                <li><span>Deadline</span> <span id="meeting-deadline-<?= $model->id ?>"><?= $model->details['date_to'] ?></span></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="w-<?= $model->id ?>-tab-3" role="tabpanel" aria-expanded="false">
                        <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/task/matter/note', 'id' => $model->id]), 'view' => 'small']) ?>
                    </div>
                </div>
            </div>
            <div class="project-box-footer clearfix widget-tabs-nav bordered" role="tablist">
                <!--<a href="#">  <span class="value">93</span>  <span class="label">Tasks</span>  </a>
                <a href="#">  <span class="value">3</span>  <span class="label">Alerts</span>  </a>
                <a href="#">   <span class="value">483</span>  <span class="label">Messages</span>  </a>-->
                <ul class="tbl-row" role="tablist">
                    <li class="nav-item">  <a class="nav-link" data-toggle="tab" href="#w-<?= $model->id ?>-tab-1" role="tab" aria-expanded="true">  <span class="label">Progres</span> </a> </li>
                    <li class="nav-item">  <a class="nav-link" data-toggle="tab" href="#w-<?= $model->id ?>-tab-2" role="tab" aria-expanded="false">  <span class="label">Info</span>  </a> </li>
                    <li class="nav-item">  <a class="nav-link" data-toggle="tab" href="#w-<?= $model->id ?>-tab-3" role="tab" aria-expanded="false">  <span class="label">Czat</span>  </a>   </li>
                </ul>
            </div>
            <div class="project-box-ultrafooter clearfix">
                <?php
                    foreach($model->employees as $key => $employee) {
                        $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee['id_user_fk']."/preview.jpg"))?"/uploads/employees/cover/".$employee['id_user_fk']."/preview.jpg":"/images/default-user.png";
                        echo '<span class="project-box-ultrafooter-thumb"><img class="project-img-owner" alt="" src="'.$avatar.'" title="'.$employee['fullname'].'" data-toggle="tooltip" title="" data-original-title="'.$employee['fullname'].'"></span>';
                    }
                ?>
                <!--<img class="project-img-owner" alt="" src="<?= $avatar ?>" data-toggle="tooltip" title="" data-original-title="Adriana Lima">
                <img class="project-img-owner" alt="" src="<?= $avatar ?>" data-toggle="tooltip" title="" data-original-title="Robert Downey Jr.">
                <img class="project-img-owner" alt="" src="<?= $avatar ?>" data-toggle="tooltip" title="" data-original-title="Angelina Jolie">-->
                <a href="<?= Url::to(['/task/details/config', 'id' => $model->id]) ?>" class="link pull-right"> <i class="fa fa-arrow-circle-right fa-lg"></i>   </a>
            </div>
        </div>
    </div>
</div>