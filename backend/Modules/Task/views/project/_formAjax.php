<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-cases"/*, 'data-input' => '.correspondence-case'*/],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Podstawowe</span></a></li>
					<li><a data-toggle="tab" href="#tab2b"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-stick-note"></i><span class="panel-tabs--text">Opis</span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        <div class="grid">
                            <div class="col-sm-6 col-xs-12">
                                 <?= $form->field($model, 'id_customer_fk', ['template' => '
                                                          {label}
                                                           <div class="input-group ">
                                                                {input}
                                                                <span class="input-group-addon bg-green">'.
                                                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/crm/customer/createajax']) , 
                                                                        ['class' => 'viewModal text--white', 
                                                                         'id' => 'customer-create',
                                                                         //'data-toggle' => ($viewModal)?"modal":"none", 
                                                                         'data-target' => "#modal-grid-item", 
                                                                         'data-form' => "item-form", 
                                                                         'data-input' => ".correspondence-customer",
                                                                         'data-title' => "Nowy klient"
                                                                        ])
                                                                .'</span>
                                                           </div>
                                                           {error}{hint}
                                                       '])->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control correspondence-customer select2'] )->label('Kontrahent')
                                                ?>   
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
                            </div>
                        </div>
                        <fieldset><legend>Klasyfikacja</legend>
                            <?= $form->field($model, 'id_dict_case_type_fk', ['template' => '
                                      {label}
                                       <div class="input-group ">
                                            {input}
                                            <span class="input-group-addon bg-green">'.
                                                Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/6' , 
                                                    ['class' => 'insertInline text--white', 
                                                     'data-target' => "#project-type-insert", 
                                                     'data-input' => ".project-type"
                                                    ])
                                            .'</span>
                                       </div>
                                       {error}{hint}
                                   '])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(6), ['class' => 'form-control project-type'] ) ?>
                            <div id="project-type-insert" class="insert-inline bg-purple2 none"> </div>
                        </fieldset>
					</div>
			        <div class="tab-pane" id="tab2b">
                        <?php if(Yii::$app->params['departments']) { ?>
                            <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'project', 'multiple' => 'multiple', ] ); ?>
                        <?php } ?>
                        <?= $form->field($model, 'employees_list')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->orderby('lastname')->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true] ) ?> 
                        <br /><br />
                        <?= $form->field($model, 'id_employee_leading_fk')->dropDownList(  $lists['employees']/*ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
                    </div>
					<div class="tab-pane" id="tab2d">
						<?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
					</div>
			    </div>
			</div>
	    </div>
	</div>
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">


</script>