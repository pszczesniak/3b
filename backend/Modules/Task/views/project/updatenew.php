<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\tasks\TodoTable;
use frontend\widgets\crm\SidesTable;
use frontend\widgets\tasks\ClaimsTable;
use frontend\widgets\tasks\InstancesTable;

$this->title = Yii::t('app', 'Aktualizacja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projekty'), 'url' => Url::to(['/task/project/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/project/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa sprawa</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/project/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>
<div class="grid">
    
    <div class="col-sm-12 col-md-9 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item active" aria-expanded="false"><a href="#basic" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-pencil"></i></span><span class="hidden-xs"> Ogólne</span></a></li>
                <li role="presentation" class="nav-item"><a href="#docs" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-files-o"></i></span> <span class="hidden-xs">Dokumenty</span></a></li>
				<li role="presentation" class="nav-item"><a href="#tasks" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-tasks"></i></span> <span class="hidden-xs">Zadania</span></a></li>
            </ul>
            <div class="tab-content m-t-0">
                <div role="tabpanel" class="tab-pane fade in active" id="basic" aria-expanded="false">
                    <?= $this->render('_formnew', [
                        'model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $employeeKind
                    ]) ?>
                </div>
				<div role="tabpanel" class="tab-pane fade" id="docs" aria-expanded="false">
					<?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tasks" aria-expanded="false">
                    <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]) ]) ?>
                    <?php /*TodoTable::widget(['dataUrl' => Url::to(['/task/project/pevents', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/personal/createajax', 'id' => $model->id]).'?projectId='.$model->id, 'actionWidth' => '70px' ])*/ ?>
                </div>
            </div>
        <?php $this->endContent(); ?>
      
    </div>
    <div class="col-sm-12 col-md-3 col-xs-12">
        <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
        <div class="panel panel-default">
            <div class="panel-heading bg-grey"> 
                <span class="panel-title"> 
                    <span class="fa fa-step-forward"></span> Status 
                </span> 
                <div class="panel-heading-menu pull-right">
					<a aria-controls="matter-update-status" aria-expanded="true" href="#matter-update-status" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            
            <div class="panel-body collapse in" id="matter-update-status">
                <!--<div class="alert <?= $colors[$model->id_dict_case_status_fk] ?>" > Obceny status: <b><?= $model->statusname ?></b></div>-->
				<fieldset><legend>Obecny stan</legend>
					<?= $this->render('_formState', ['model' => $model, 'modal' => false]) ?>
				</fieldset>
                <div class="align-right">
                    <?php if($model->id_dict_case_status_fk < 4) { ?> 
						<a href="<?= Url::to(['/task/project/close', 'id' => $model->id]) ?>" class="btn bg-green btn-xs btn-flat gridViewModal" data-target="#modal-grid-item" data-title="Zamknięcie projektu">
							<span class="fa fa-check"></span> Zakończ
						</a> 
                    <?php } ?>
                    <a href="<?= Url::to(['/task/project/archive', 'id' => $model->id]) ?>" class="btn bg-purple btn-xs btn-flat modalConfirm">
						<span class="fa fa-archive"></span> Przenieś do archiwum
					</a> 
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading bg-blue"> 
                <span class="panel-title"> <span class="fa fa-comments"></span> Notatki </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="crm-customer-update-note" aria-expanded="true" href="#crm-customer-update-note" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="crm-customer-update-note">
                <?= $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) ?>
            </div>
        </div>


    </div>
</div>



