<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <div class="grid">
		<div class="col-sm-6 col-md-8">
			<fieldset><legend>Podstawowe</legend>
                <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?php /* echo $form->field($model, 'id_dict_case_status_fk')->radioList(\backend\Modules\Task\models\CalCase::listStatus(), 
                                            ['class' => 'btn-group', 'data-toggle' => "buttons",
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                    return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
                                                        Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                                },
                                        ])*/ ?>
            </fieldset>
            <fieldset><legend>Klient</legend>
                <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['class' => 'form-control select2'] )->label('Nazwa') ?> 
                <?= $form->field($model, 'id_customer_person_leading_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getPersonsList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?> 
             </fieldset>
            <fieldset><legend>Opis</legend>   
                <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(false) ?>
			</fieldset>	

		</div>
		<div class="col-sm-6 col-md-4">
			<fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'id_dict_case_type_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::listTypes(6), ['class' => 'form-control select2', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
            <fieldset><legend>Personalizacja</legend>
                <?= $form->field($model, 'id_employee_leading_fk')->dropDownList(  $lists['employees']/*ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname')*/, ['prompt' => '-wybierz-', 'class' => 'form-control select2'] ) ?>
                    
                <?= $form->field($model, 'departments_list', ['template' => '{label}{input}{error}',  'options' => ['class' => 'form-group']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'project', 'multiple' => 'multiple', ] ); ?>
                <?php /*$this->render('_employeesList', ['employees' => $lists['employees'], 'employees_ch' => $employees, 'case' => $model->id ])*/ ?>
                <?= EmployeesCheck::widget(['idName' => 'menu-list-employee', 'employees' => $lists['employees'], 'employeesCheck' => $employees]) ?>

            </fieldset>
		</div>
        
	</div>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    document.getElementById('calcase-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/lists']) ?>/"+this.value+"?type=project", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('calcase-id_customer_person_leading_fk').innerHTML = result.contacts; 
                //document.getElementById('calcase-departments_list').innerHTML = result.departments; 
                //$('#calcase-departments_list').multiselect('rebuild');
                //$('#calcase-departments_list').multiselect('refresh');
                //document.getElementById('menu-list-employee').innerHTML = result.employees; 
            }
        }
       xhr.send();
       
       return false;
    }
    

</script>