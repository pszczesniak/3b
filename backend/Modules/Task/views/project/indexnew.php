<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->params['projectNameMulti'];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-projects" data-form="#filter-task-projects-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_searchnew', ['model' => $searchModel, 'type' => 'advanced']); ?>
        </div>
    </fieldset>
    
    <?= Alert::widget() ?>
	<div id="toolbar-projects" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/project/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/project/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-task-projects-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?php /*if(count(array_intersect(["caseExport", "grantAll"], $grants)) > 0) {*/ ?>
                <!--<div class="btn-group" id="actionsDates-btn">
                    <button type="button" class="btn btn-icon bg-navy"><i class="fa fa-cogs"></i>Operacje grupowe (<span id="settle-counter">0</span>)</button>
                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" id="postingDates-list">
                        <?= '<li class="settle-actions">'
                               . '<a href="'.Url::to(['/task/manage/addemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Dołącz pracownika do sprawy">dołącz pracownika</a>'
                            .'</li>' ?>
                        <?= '<li class="settle-actions">'
                               . '<a href="'.Url::to(['/task/manage/delemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Odłącz pracownika od sprawy">odłącz pracownika</a>'
                            .'</li>' ?>
                        <?= '<li class="settle-actions">'
                           . '<a href="'.Url::to(['/task/manage/changestatus']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Zmień status">zmień status</a>'
                        .'</li>' ?>
                    </ul>
                </div>-->
            <?php /*}*/ ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-projects"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-projects"
                data-toolbar="#toolbar-projects" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-task-projects-search"
                data-url=<?= Url::to(['/task/project/data'/*, 'type' => 'normal'*/]) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
					<!--<th data-field="state" data-visible="true" data-checkbox="true"></th>-->
					<th data-field="name" data-sortable="true" data-align="center">Nazwa</th>
                    <th data-field="client" data-sortable="true" >Klient</th>
                    <th data-field="start_date"  data-sortable="true" data-align="center" >Rozpoczęto</th>
                    <th data-field="end_date"  data-sortable="true" data-align="center" >Zakończono</th>
                    <th data-field="type"  data-sortable="true" data-align="center" >Typ</th>
                    <th data-field="status"  data-sortable="true" data-align="center" >Status</th>
                    <th data-field="progress" data-sortable="false" data-visible="false" data-width="100px"></th>
                    <th data-field="actions" data-events="actionEvents" data-width="70px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

