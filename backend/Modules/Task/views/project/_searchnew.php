<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-task-projects-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-projects']
    ]); ?>

    <div class="grid">
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search' ] ) ?></div>
        <?php if(Yii::$app->params['departments']) { ?>
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search'  ] ) ?></div>        
        <?php } ?>
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search'  ] ) ?></div>
        <div class="col-md-2 col-sm-2 col-xs-6"><?= $form->field($model, 'id_dict_case_status_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCase::listStatus( $type ), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search' , 'data-default' => $model->id_dict_case_status_fk ] ) ?></div>
        <div class="col-md-2 col-sm-2 col-xs-6"><?= $form->field($model, 'id_dict_case_type_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Task\models\CalCase::listTypes(5), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search' , 'data-default' => $model->id_dict_case_status_fk ] ) ?></div>
        <div class="col-sm-8 col-md-8 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
