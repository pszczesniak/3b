<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projekty');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 'advanced']); ?>
    <?= Alert::widget() ?>
	<div id="toolbar-projects" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/project/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/project/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-form' => "#filter-task-projects-search",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-projects"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-projects"
                data-toolbar="#toolbar-projects" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-task-projects-search"
                data-url=<?= Url::to(['/task/project/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="client"  data-sortable="true" data-width="30%">Kontrahent</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true" data-width="30%">Nazwa</th>
					<th data-field="progress" data-sortable="false" data-width="20%"></th>
                    <th data-field="status"  data-sortable="true" data-align="center" data-width="10%">Status</th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>
<?php $this->endContent(); ?>