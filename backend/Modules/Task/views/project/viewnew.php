<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;
use frontend\widgets\crm\SidesTable;
use frontend\widgets\tasks\ClaimsTable;
use frontend\widgets\tasks\InstancesTable;
use frontend\widgets\debt\TimelineTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

$this->title = $model->no_label;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/project/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/project/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa sprawa</a>
		    <?php } ?>
			<a href="<?= Url::to(['/timeline/set', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Przebieg</a>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) && 1 == 2) { ?>
				<a href="<?= Url::to(['/task/project/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
		</div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-matter-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="view-project">
        
        <div class="grid">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <section class="panel">
                    <div class="project-heading bg-grey">
                        <strong>Szczegóły</strong>
                        <?php if( ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ) { ?>
                            <a href="<?= Url::to(['/task/matter/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-flat pull-right" style="margin-top:-8px"><span class="fa fa-pencil"></span> Edytuj</a>
                        <?php } ?>
                    </div>
                    <div class="panel-body project-body">
                        
                        <div class="row project-details">
                            <div class="row-details">
                                <p><b>Pełna nazwa</b>: <i><?= $model->name ?></i></p>
                                <p><b>Dodatkowa nazwa</b>: <i><?= ($model->subname) ? $model->subname : 'brak' ?></i></p>
                            </div>
                            <!--<div class="row-details">
                                <?php $colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green']; ?>
                                <p><b>Status</b>: <span class="label <?= $colors[$model->id_dict_case_status_fk] ?>"><?= $model->statusname ?></span></p>
                            </div>-->
                            <div class="row-details">
                                <p><b>Osoba prowadząca</b>: <?= (!$model->leadingEmployee) ? 'brak danych' : $model->leadingEmployee['fullname'] ?></span></p>
                            </div>
                        </div>
                        
                    </div><!--/project-body-->
                </section>
            </div>
            <div class="col-sm-4 col-xs-12">
                <fieldset><legend class="bg-teal">Klient <a href="<?= Url::to(['/crm/customer/card', 'id' => $model->id_customer_fk]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka klienta" title="Wizytówka klienta"><i class="fa fa-address-card text--lightgrey"></i></a><span class="pull-right label bg-white"><?= $model->role ?></span></legend>
                    <div class="grid grid--0 profile">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="profile-avatar">
                                <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                <img src="<?= $avatar ?>" alt="Avatar">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div class="profile-name">
                                <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                                <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['role'] ?></p>

                            </div>
                        </div>
                        <div class="col-xs-12"><div class="alert bg-teal2 text--teal">Osoba kontaktowa: <?= (!$model->leadingContact) ? '<i>brak danych</i>' : '<b>'.$model->leadingContact['fullname'].'</b>' ?></div></div>
                    </div>
                </fieldset>
            </div>
            <div class="col-sm-4 col-xs-12">
                <fieldset>
                    <legend class="bg-red">Strona przeciwna
                        <?php if($model->id_opposite_side_fk) { ?>
                            <a href="<?= Url::to(['/crm/side/card', 'id' => $model->id_opposite_side_fk]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-address-card'></i>Wizytówka strony przeciwnej" title="Wizytówka strony przeciwnej"><i class="fa fa-address-card text--lightgrey"></i></a>
                        <?php } ?>
                    </legend>
                    <?php if($model->id_opposite_side_fk) { ?>
                    <div class="grid grid--0 profile">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="profile-avatar">
                                <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_opposite_side_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                <img src="<?= $avatar ?>" alt="Avatar">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div class="profile-name">
                                <h5><a href="<?= Url::to(['/crm/side/view', 'id' => $model->id_opposite_side_fk]) ?>"><?= $model->side['name'] ?></a></h5>
                                <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->side['role'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="alert alert-info">Nie ustawiono strony przeciwnej</div>
                    <?php } ?>
                    <div class="col-xs-12"><div class="alert bg-red2 text--red">Należności: <b>0</b> | Saldo: <b>0</b></div></div>
                </fieldset>
            </div>
        </div>
        
        <div class="panel with-nav-tabs panel-default">
            <?php $notes = $model->notes; ?>
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1" title="Pracownicy"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span></a></li>
                    <li><a data-toggle="tab" href="#tab3" title="Instancje"><i class="fa fa-university"></i><span class="panel-tabs--text">Instancje</span> </a></li>                    
                    <li><a data-toggle="tab" href="#tab2" title="Dokumenty"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                    <li><a data-toggle="tab" href="#tab5" title="Terminy"><i class="fa fa-calendar"></i><span class="panel-tabs--text">Terminy</span></a></li>
                    <li><a data-toggle="tab" href="#tab6" title="Uczestnicy"><i class="fa fa-users"></i><span class="panel-tabs--text">Uczestnicy</span></a></li>
                    <li><a data-toggle="tab" href="#tab7" title="Roszczenia"><i class="fa fa-flash text--pink"></i><span class="panel-tabs--text text--pink">Roszczenia</span></a></li>                  
                    <li><a data-toggle="tab" href="#tab8" title="Windykacja"><i class="fa fa-exclamation-triangle text--red"></i><span class="panel-tabs--text text--red">Windykacja</span></a></li>
                    <li><a data-toggle="tab" href="#tab4" title="Komentarze"><i class="fa fa-comments text--teal2"></i><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>                      
                    <!--<li><a data-toggle="tab" href="#tab9" title="Rozliczenie"><i class="fa fa-calculator text--teal"></i></a></li>-->
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="grid">
                            <div class="col-sm-3 col-xs-12 alert alert-info"><h3>Działy</h3><?= $model->departments_list ?></div>
                            <div class="col-sm-9 col-xs-12"><?= EmployeesBlock::widget(['employees' => $model->employees, 'employeesLock' => $model->employeeslock]) ?></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <?= InstancesTable::widget(['dataUrl' => Url::to(['/task/manage/instances', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addinstance', 'id' => $model->id]) ]) ?>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => true, 
                                                'download' => Url::to(['/task/matter/download', 'id' => $model->id]), 'browser' => Url::to(['/task/matter/browser', 'id' => $model->id]) ]) ?>
                    </div>
                    <div class="tab-pane" id="tab5">
                         <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <?= SidesTable::widget(['dataUrl' => Url::to(['/task/manage/sides', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addmember', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <?= ClaimsTable::widget(['dataUrl' => Url::to(['/task/manage/claims', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/manage/addclaim', 'id' => $model->id]), 'actionWidth' => '20px' ]) ?>
                    </div>
                    <div class="tab-pane" id="tab8">
                        <?= TimelineTable::widget(['dataUrl' => Url::to(['/debt/default/timeline', 'id' => $model->id]), 'customerId' => $model->id_customer_fk, 'setId' => $model->id]) ?>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/task/matter/note', 'id' => $model->id])]) ?>
                    </div>
                    <!--<div class="tab-pane" id="tab9">
                        rozliczenie
                    </div>-->
                </div>
            </div>
        </div>
    </div>
 
<?php $this->endContent(); ?>
