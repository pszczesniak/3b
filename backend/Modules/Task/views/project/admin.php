<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projekty');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>
    <div class="panel panel-default">
        <div class="panel-heading bg-navy"> 
            <span class="panel-title"> <span class="fa fa-filter"></span> Filtorowanie danych </span> 
            <div class="panel-heading-menu pull-right">
                <a aria-controls="widget-company-contacts-filter" aria-expanded="true" href="#widget-company-contacts-filter" data-toggle="collapse" class="collapse-link collapse-window">
                    <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                </a>
            </div>
        </div>
        <div class="panel-body collapse in" id="widget-company-contacts-filter">
            <?php  echo $this->render('_dsearch', ['model' => $searchModel]); ?>
        </div>
    </div>
    
    <div class="grid">
        <?php
            foreach($models as $key => $model) {
                echo $this->render('_item', ['model' => $model]);
            }
        ?>
    </div>
    <div class="align-center">
        <?php echo LinkPager::widget([ 'pagination' => $pages, ]); ?>
    </div>
    
<?php $this->endContent(); ?>