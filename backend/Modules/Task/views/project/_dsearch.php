<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['admin'],
        'method' => 'post',
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-7 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control  widget-search-text', 'data-table' => '#table-projects', 'data-form' => '#filter-task-projects-search' ])->label('Nazwa') ?></div>
        <div class="col-sm-4 col-xs-8"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2' ] ) ?></div>
        <div class="col-sm-1 col-xs-4 align-right"><br /><?= Html::submitButton(Yii::t('app', 'Szukaj'), ['class' => 'btn bg-purple']) ?></div>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
