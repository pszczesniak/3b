<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = Yii::t('app', 'Edycja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zdarzenia'), 'url' => Url::to(['/task/event/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/task/event/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowe zadanie</a>
            <?php if($model->id_dict_task_status_fk == 1) { ?> 
                <a href="<?= Url::to(['/task/event/close', 'id' => $model->id]) ?>" class="btn bg-green btn-flat"><span class="fa fa-calendar-check-o"></span> Zrobione</a> 
            <?php } ?>
            <a href="<?= Url::to(['/task/event/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
        </div>
    </div>
</div>
<div class="grid">
    <div class="col-sm-6 col-md-8 col-xs-12">
        <?php if($model->id_dict_task_status_fk == 2) { ?> 
            <div class="alert bg-green btn-icon"><i class="fa fa-check"></i> Zadanie ma obecnie status <b>Zakończone</b><a href="<?= Url::to(['/task/event/open', 'id' => $model->id]) ?>" class="btn btn-sm bg-blue pull-right">wróć do statusu 'w toku'</a></div>
        <?php } ?>
        <?= Alert::widget() ?> 
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
            <?php if($model->delay > 0 && $model->id_dict_task_status_fk == 1) { echo '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>&nbsp;Przekroczyłeś czas wykonania o <b>'.$model->delay.'</b> h</div>'; } ?>
            <?= $this->render('_form', [
                'model' => $model, 'lists' => $lists, 'employees' => $employees, 'employeeKind' => $employeeKind, 'all' => $all
            ]) ?>

        <?php $this->endContent(); ?>
    </div>
    <div class="col-sm-6 col-md-4 col-xs-12">
       
        <div class="panel panel-default">
            <div class="panel-heading bg-purple"> 
                <span class="panel-title"> <span class="fa fa-paperclip"></span> Dokumenty </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-task-update-docs" aria-expanded="true" href="#cal-task-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="cal-task-update-docs">
                <?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
                <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading bg-red"> 
                <span class="panel-title"> <span class="fa fa-bell"></span> Przypomnienie </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-task-update-alerts" aria-expanded="true" href="#cal-task-update-alerts" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="cal-task-update-alerts">
                <?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
                <?= $this->render('_reminder', ['model' => $model,]) ?>
                <fieldset><legend>Dodatkowe powiadomienia</legend>
                    <?= \frontend\widgets\alert\AlertsTable::widget(['id' => $model->id, 'type' => 1]) ?>
                </fieldset>
            </div>
        </div>
        
        <!--<div class="panel panel-default">
            <div class="panel-heading bg-yellow"> 
                <span class="panel-title"> <span class="fa fa-calendar"></span> Kalendarz </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-task-update-docs" aria-expanded="true" href="#cal-task-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="cal-task-update-docs">
                <?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
                <?= $this->render('_calendar', ['model' => $model,]) ?>
            </div>
        </div>-->
        
        <div class="panel panel-default">
            <div class="panel-heading bg-blue"> 
                <span class="panel-title"> <span class="fa fa-comments"></span> Komentarze </span> 
                <div class="panel-heading-menu pull-right">
                    <a aria-controls="cal-task-update-notes" aria-expanded="true" href="#cal-task-update-notes" data-toggle="collapse" class="collapse-link collapse-window">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body collapse in" id="cal-task-update-notes">
                <?= $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) ?>
            </div>
        </div>

    </div>
</div>

