<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use frontend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

	<div class="modal-body">
        <?php
            $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
            $colors2 = [1 => 'bg-teal', 2 => 'bg-green'];
        ?>
        
        <h4><?= ($model->type_fk == 1) ? 'Rozprawa' : 'Zadanie' ?></h4><label class="label <?= $colors2[$model->id_dict_task_status_fk] ?>"> <?= $model->statusname ?> </label> <label class="label <?= $colors1[$model->id_dict_task_category_fk] ?>"> <?= $model->category ?> </label>
        
        <?php 
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'place',
                    'date_from',
                    'date_to',
                    [                      
                        'attribute' => 'all_day',
                        'format'=>'raw',
                        'value' => ($model->all_day == 0) ? 'NIE' : 'TAK',
                    ],  
                    [
                        'attribute' => 'notification_email',
                        'format'=>'raw',
                        'value' => ($model->notification_email == 0) ? 'NIE' : 'TAK',
                    ],
                    [                      
                        'attribute' => 'id_dict_task_type_fk',
                        'format'=>'raw',
                        'value' => $model->type,
                    ],  
                   /* [                      
                        'attribute' => 'id_dict_task_category_fk',
                        'format'=>'raw',
                        'value' => $model->category,
                    ],  
                    [                      
                        'attribute' => 'id_dict_task_status_fk',
                        'format'=>'raw',
                        'value' => $model->statustask,
                    ], */  
                    [                      
                        'attribute' => 'id_customer_fk',
                        'format'=>'raw',
                        'value' => $model->customer['name'],
                    ],
                    [                      
                        'attribute' => 'id_case_fk',
                        'format'=>'raw',
                        'value' => $model->case['name'],
                    ],
                    'description', 
                    [                      
                        'attribute' => 'departments_list:html',
                        'format'=>'raw',
                        'label' => 'Działy',
                        'value' => $model->departments_list,
                    ],
                    [                      
                        'attribute' => 'employees_list:html',
                        'format'=>'raw',
                        'label' => 'Pracownicy',
                        'value' => $model->employees_list,
                    ], 
                    [                      
                        'attribute' => 'files_list:html',
                        'format'=>'raw',
                        'label' => 'Dokumenty',
                        'value' => FilesBlock::widget(['files' => $model->correspondence, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => true]) 
                    ],         
                ],
            ]) 
        ?>
	</div>
        
            
    <div class="modal-footer"> 
		<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Zamknij</button>
    </div>
<?php ActiveForm::end(); ?>