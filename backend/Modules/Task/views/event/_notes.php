<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-body">
    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
        <?= $form->field($note, 'note')->textarea(['row' => 2, 'placeholder' => 'Wpisz notatkę'])->label(false) ?>
        <div class="align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    <hr />
    <h3>Notatki</h3>
    <?php 
        $notes = $model->notes;
        if(count($notes) > 0) {
            foreach($notes as $key => $note) {
                echo '<div class="well"><label>'.$note->user. ' [ '.$note->created_at.' ]</label><div>'.$note->note.'</div></div>';
            }
        } else {
            echo '<div class="well">brak wpisów</div>';
        }
        
    ?>
</div>
<div class="modal-footer"> 
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
</div>
