<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\Modules\Eg\models\EgVisitStatus;
use app\Modules\Eg\models\EgClient;
use app\Modules\Eg\models\EgStaff;

/* @var $this yii\web\View */
/* @var $model app\Modules\Eg\models\EgVisit */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Nowa wizyta</h4>
</div>
<?php $form = ActiveForm::begin(); ?>	
<div class="modal-body">
    <div class="row">
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
			<?= $form->field($model, 'visit_day')->textInput(['class'=>'form-control text-center', 'readonly'=>'readonly']) ?>
		</div>
		<div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
			<?= $form->field($model, 'visit_time')->textInput(['class'=>'form-control text-center', 'readonly'=>'readonly']) ?>
		</div>
		<div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
			<label for="egvisit-visit_duration" class="control-label"> <?=  Yii::t('app', 'Visit Duration') ?> </label>
			<div class="input-group number-spinner form-group field-egvisit-visit_duration">		
				<span class="input-group-btn data-dwn">
					<button type="button" class="btn btn-default btn-info button-number-spinner" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
				</span>
				
				<input type="text" max="360" min="0" readonly="readonly" value="30" name="EgVisit[visit_duration]" class="form-control text-center" id="egvisit-visit_duration">
				<div class="input-group-addon visit-duration-unit">min</div>
				<?php /*$form->field($model, 'visit_duration')->textInput(['class'=>'form-control text-center', 'readonly'=>'readonly', 'min'=>15, 'max'=>360])*/ ?>
				<span class="input-group-btn data-up">
					<button type="button" class="btn btn-default btn-info button-number-spinner" data-dir="up"><span class="glyphicon glyphicon-plus"></span></buttoni>
				</span>			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
			<?= $form->field($model, 'fk_client_id')->dropDownList( ArrayHelper::map(EgClient::find()->orderby('lastname')->all(), 'id', 'fullname') )  ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
			<?= $form->field($model, 'fk_status_id')->dropDownList( ArrayHelper::map(EgVisitStatus::find()->all(), 'id', 'status_name') )  ?>
		</div>
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
			<?= $form->field($model, 'fk_staff_doctor_id')->dropDownList( ArrayHelper::map(EgStaff::find()->where(['is_doctor' => '1'])->orderby('lastname')->all(), 'id', 'fullname') )  ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
			<?= $form->field($model, 'confirm')->checkBox(['label' => 'wizyta potwierdzona', 'uncheck' => null, 'checked' => true]) ?>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>