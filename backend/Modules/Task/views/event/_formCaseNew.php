<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    //'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => "#table-events", 'data-input' => ".correspondence-event"],
    'options' => ['class' => 'modalAjaxForm',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-item", 'data-input' => ".correspondence-event"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body modal-body-max calendar-task">
        <div class="content ">
            <div class="grid">
                
                <div class="col-sm-1 col-xs-1">
                    <input id="caltask-id_dict_task_category_fk" class="form-control" name="CalTask[id_dict_task_category_fk]" type="hidden" value="<?= $model->id_dict_task_category_fk ?>" />
                    <?php $priorityColors = [ 1 => 'red', 2 => 'orange', 3 => 'yellow']; ?> 
                    <div class="icons-left">
                        <div class="dropdown">
                            <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->id_dict_task_category_fk] ?>" id="priority-flag-task-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                            <ul class="dropdown-menu " role="menu">
                                <li>
                                    <ul class="list-flags">
                                        <li><span class="priority-flag-task fa fa-flag icon text--red <?= ($model->id_dict_task_category_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="red"></span> </li>
                                        <li><span class="priority-flag-task fa fa-flag icon text--orange <?= ($model->id_dict_task_category_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="orange"></span> </li>
                                        <li><span class="priority-flag-task fa fa-flag icon text--yellow <?= ($model->id_dict_task_category_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="yellow"></span> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="col-sm-5 col-xs-11">
					<!--<label>Termin</label>-->
                    <div class="grid grid--0">
                        <div class="col-xs-7"><input type='text' class="form-control" id="task_date" name="CalTask[event_date]" value="<?= $model->event_date ?>"/></div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <div class='input-group date' id='task_time' >
                                    <input type='text' class="form-control" name="CalTask[event_time]" value="<?= $model->event_time ?>" />
                                    <span class="input-group-addon bg-blue" title="Ustaw godzinę. Brak podania godziny ustawi zadanie za cały dzień.">
                                        <span class="fa fa-clock-o text--white"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <script type="text/javascript">
                        $(function () {
                            $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
                            $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
                            $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
                        });
                    </script>
				</div>
                
                <div class="col-sm-6 col-xs-12">
                    <div class="grid">
                        
                        <div class="col-xs-6">
                            <?= $form->field($model, 'type_fk')->dropDownList( isset(Yii::$app->params['task-types']) ? Yii::$app->params['task-types'] : [1 => 'Rozprawa', 2 => 'Zadanie' ], [ 'class' => 'form-control'] )->label(false) ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'id_dict_task_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 7 : 8 ) ), [ 'class' => 'form-control'] )->label(false) ?> 
                        </div>
                    </div>
					
                </div>
            </div>
            <div class="grid grid--0">
                <div class="col-sm-4 col-xs-6">
                    <div class="form-group">
                        <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                        <div class="time-element">
                            <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                            <div class="time-element_divider">:</div>
                            <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'min','maxlength' => true])->label(false) ?></div>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-5 col-xs-6">
                    <div class="form-group">
                        <label class="control-label" for="caltask-task_deadline"> Deadline </label>
                        <div class='input-group date' id='task_deadline' >                          
                            <input type='text' class="form-control" name="CalTask[event_deadline]" value="<?= $model->event_deadline ?>" />
                            <span class="input-group-addon bg-red" title="Ustaw deadline">
                                <span class="fa fa-warning text--white" ></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?= $form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] ) ?>
               </div>
            </div>
            <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<div class="grid grid--0">
                <div class="col-xs-12">
                    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

		<div class="panel-group" id="accordion">
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Pracownicy</a> </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
                            <?php /*$form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select-ajax', 'data-type' => 'matter', 'multiple' => 'multiple', ] );*/ ?>                               							
                            <?php /*$this->render('_employeesListAjax', ['employees' => $employees, 'case' => $model->id_case_fk, 'departments' => $model->departments_list, 'type' => $model->type_fk])*/ ?>
                            <?= EmployeesCheck::widget(['idName' => 'menu-list-employee-task', 'employees' => $lists['employees'], 'employeesCheck' => $employees]) ?>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Opis</a> </h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse ">
						<div class="panel-body">
							<?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
						</div>
					</div>
				</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> Kalendarz</a> </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $this->render('_calendarAjax', ['model' => $model]) ?>
                            <?= $form->field($model, 'all_day', [/*'template' => '{input} {label}'*/])->checkbox(['id' => 'calendar-all-day']) ?>
                        </div>
                    </div>
                </div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Dokumenty</a> </h4>
					</div>
					<div id="collapse4" class="panel-collapse collapse ">
						<div class="panel-body">
							<?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
							<?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton(($model->status == -2) ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => ($model->status == -2) ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_date').on("dp.change", function (e) {
                
                                    minDate = e.date.format('YYYY-MM-DD');
                                    if( $('#task_fromDate').length > 0 ) {
                                        $('#task_fromDate').data("DateTimePicker").minDate(minDate);
                                        $('#task_fromDate input').val(minDate);
                                    }
                                    
                                    minDeadline = e.date.format('YYYY-MM-DD 16:00');
                                    if( $('#task_deadline').length > 0 ) {
                                        $('#task_deadline').data("DateTimePicker").minDate(minDeadline);
                                        $('#task_deadline input').val(minDeadline);
                                    }
                                });
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
        $('#task_time').on("dp.change", function (e) {
                                    if(!e.date) {
                                        
                                        if( $('#task_fromTime').length > 0 ) {
                                            $("#task_toTime > input").prop('disabled', true).val('');
                                            $("#task_fromTime > input").prop('disabled', true).val('');
                                        }
                                    
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', true);
                                        }
                                    } else {
                                        minDate = e.date.format('HH:mm');
                                        if( $('#task_fromTime').length > 0 ) {
                                            //$('#task_fromTime').data("DateTimePicker").minDate(minDate);
                                            $('#task_fromTime input').val(minDate);
                                            
                                            $("#task_toTime > input").prop('disabled', false);
                                            $("#task_fromTime > input").prop('disabled', false);
                                        }
                                        
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', false);
                                        }
                                    }
                                });
        $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
    });
</script>
           

<script type="text/javascript">
    var priorityTask = document.querySelectorAll('.priority-flag-task');
    var priorityTaskColors = ['grey', 'red', 'orange', 'yellow'];
    var priorityTaskInput = document.getElementById('caltask-id_dict_task_category_fk');
    
    for (var i = 0; i < priorityTask.length; i++) {
        priorityTask[i].addEventListener('click', function(event) {
            console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityTask.length; j++) {
               priorityTask[j].classList.remove('none');
            }
            document.getElementById('priority-flag-task-choice').classList.remove('text--'+priorityTaskColors[priorityTaskInput.value]);
            priorityTaskInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-task-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
    

                
    document.getElementById('caltask-type_fk').onchange = function(event) {
        var type = 'case';
        if(event.target.value == 2) type = 'event';
        
        var set = <?= $model->id_case_fk ?>;
        
        if(set && set > 0) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+set+"?type="+type, true);
            xhr.onreadystatechange=function()  {
                if (xhr.readyState==4 && xhr.status==200)  {
                    var result = JSON.parse(xhr.responseText);console.log(result.list);
                    document.getElementById('menu-list-employee-task').innerHTML = result.list;  
                    document.getElementById('caltask-id_dict_task_type_fk').innerHTML = result.types;               
                }
            }
            xhr.send();
            
        }
        /*if(event.target.value == 1) {
           
        }*/
        return false;
    }

  

    
</script>
