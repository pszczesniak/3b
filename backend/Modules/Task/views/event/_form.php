<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-task-form form calendar-task">
     
    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    
    <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>

    <div class="grid">          
        <div class="col-sm-1 col-xs-1">
            <input id="caltask-id_dict_task_category_fk" class="form-control" name="CalTask[id_dict_task_category_fk]" type="hidden" value="<?= $model->id_dict_task_category_fk ?>" />
            <?php $priorityColors = [ 1 => 'red', 2 => 'orange', 3 => 'yellow']; $priorityTitle = [ 1 => 'ważne', 2 => 'średnie', 3 => 'niskie']; ?> 
            <div class="icons-left">
                <div class="dropdown">
                    <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->id_dict_task_category_fk] ?>" title="<?= $priorityTitle[$model->id_dict_task_category_fk] ?>" id="priority-flag-task-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                    <ul class="dropdown-menu " role="menu">
                        <li>
                            <ul class="list-flags">
                                <li><span class="priority-flag-task fa fa-flag icon text--red <?= ($model->id_dict_task_category_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="red" title="<?= $priorityTitle[1] ?>"></span> </li>
                                <li><span class="priority-flag-task fa fa-flag icon text--orange <?= ($model->id_dict_task_category_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="orange" title="<?= $priorityTitle[2] ?>"></span> </li>
                                <li><span class="priority-flag-task fa fa-flag icon text--yellow <?= ($model->id_dict_task_category_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="yellow" title="<?= $priorityTitle[3] ?>"></span> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div> 
        <div class="col-xs-11">
            <!--<label>Termin</label>-->
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Tytuł', 'maxlength' => true])->label(false) ?>
        </div>
    </div>
     
    <div class="grid">
        <div class="col-sm-6">
            <fieldset><legend>Podstawowe</legend>   
                <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 

                <?= $form->field($model, 'id_case_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups($model->id_customer_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
            <fieldset><legend>Czas i miejsce  </legend>
                <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                
                <div class="grid grid--0">
                    
                    <div class="col-md-4 col-xs-7">
                        <div class="form-group">
                            <label class="control-label" for="caltask-task_date">Data</label>
                            <input type='text' class="form-control" id="task_date" name="CalTask[event_date]" value="<?= $model->event_date ?>"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-5">
                        <div class="form-group">
                            <label class="control-label" for="caltask-task_time">Godzina</label>
                            <div class='input-group date' id='task_time' >
                                <input type='text' class="form-control" name="CalTask[event_time]" value="<?= $model->event_time ?>" />
                                <span class="input-group-addon bg-blue" title="Ustaw godzinę">
                                    <span class="fa fa-clock-o text--white"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                            <div class="time-element">
                                <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                                <div class="time-element_divider">:</div>
                                <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'mm','maxlength' => true])->label(false) ?></div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'all_day', [/*'template' => '{input} {label}'*/])->checkbox(['id' => 'calendar-all-day']) ?>
                    </div>
                </div>
                <div class="grid">   
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class='input-group date' id='task_deadline' >
                                <input type='text' class="form-control" name="CalTask[event_deadline]" value="<?= $model->event_deadline ?>" />
                                <span class="input-group-addon bg-red" title="Ustaw deadline">
                                    <span class="fa fa-warning text--white" ></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?= ($model->status == -2) ? $form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] )->label(false) : ''; ?>
                    </div>
                </div>
                <!--<div class="grid">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_from" class="control-label">Start</label>
                            <div class='input-group' id='datetimepicker_start'>
                                <input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $model->date_from ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_to" class="control-label">Koniec</label>
                            <div class='input-group' id='datetimepicker_end'>
                                <input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $model->date_to ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                </div>-->

            </fieldset>
            <fieldset><legend>Opis</legend>
                
                <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
                
            </fieldset>
            <!--<fieldset><legend>Kalendarz <i class="fa fa-info-circle text--blue" title="W tym miejscu można ustawića trybuty widoczności zdarzenia w kalendarzu"></i></legend>-->
                <?php /* $this->render('_calendar', ['model' => $model]) */ ?>
                
            <!--</fieldset>-->
        </div>
        <div class="col-sm-6">
            <fieldset><legend>Klasyfikacja</legend>
                <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), [] )
                                                                    ->label('Oddział <i class="fa fa-info-circle text--blue" title="Oddział, w którym zarejestrowano zdarzenie"></i>') ?>
                <?= $form->field($model, 'id_dict_task_type_fk')->dropDownList(  \backend\Modules\Task\models\CalTask::listTypes(8), ['class' => 'form-control select2', 'class' => 'form-control select2'] ) ?> 
            </fieldset>
            <fieldset><legend>Personalizacja</legend>
                <div class="form-group">
                    <?= EmployeesCheck::widget(['idName' => 'menu-list-employee', 'type' => 'event', 'employees' => $lists['employees'], 'employeesCheck' => $employees]) ?>
                    <!--<label class="control-label" for="caltask-departments_list">Działy</label>-->
                    <?php /*$form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'data-type' => 'matter', 'multiple' => 'multiple', ] );*/ ?>
                    <?php /*$this->render('_employeesList', ['employees' => $employees, 'case' => $model->id_case_fk, 'employeeKind' => $employeeKind])*/ ?>
                </div>
            </fieldset>
            
            <!--<fieldset><legend>Zasoby</legend>
                <?= $form->field($model, 'resources_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( \backend\Modules\Company\models\CompanyResources::getGroups(0), ['class' => 'ms-resource',  'multiple' => 'multiple', ] )->label(false); ?>
            </fieldset>-->
        </div>
    </div>
    

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
   /* document.onreadystatechange = function(){
        if (document.readyState === 'complete') {console.log('datetimepicker');
            
        }
    };*/
	
	document.getElementById('caltask-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+( (this.value) ? this.value : 0 ), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltask-id_case_fk').innerHTML = result.list;   
                document.getElementById('menu-list-employee').innerHTML = '';             
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById("caltask-id_case_fk").onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+ ( (this.value) ? this.value : 0 )+"?type=event&id=<?= $model->id ?>", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('menu-list-employee').innerHTML = result.list; 

               /* var x = document.getElementById('caltask-departments_list');
                for (i = 0; i < x.length; i++) {
                    //console.log(x.options[i].value+ ' - '+result.departments.indexOf(x.options[i].value));
                    if(result.departments.indexOf(x.options[i].value*1) >= 0) {
                        x.options[i].selected = true;
                    } else {
                        x.options[i].selected = false;
                    }
                }
                $('#caltask-departments_list').multiselect('rebuild');
                $('#caltask-departments_list').multiselect('refresh'); */             
            }
        }
       xhr.send();
        return false;
    }
    
    var priorityTask = document.querySelectorAll('.priority-flag-task');
    var priorityTaskColors = ['grey', 'red', 'orange', 'yellow'];
    var priorityTaskInput = document.getElementById('caltask-id_dict_task_category_fk');
    
    for (var i = 0; i < priorityTask.length; i++) {
        priorityTask[i].addEventListener('click', function(event) {
            console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityTask.length; j++) {
               priorityTask[j].classList.remove('none');
            }
            document.getElementById('priority-flag-task-choice').classList.remove('text--'+priorityTaskColors[priorityTaskInput.value]);
            priorityTaskInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-task-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
    
</script>
