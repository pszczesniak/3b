<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kalendarz');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-event-calendar', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading bg-purple"> 
					<span class="panel-title"> <span class="fa fa-filter"></span> Filtorwanie </span> 
					<div class="panel-heading-menu pull-right">
						<a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
							<i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
						</a>
					</div>
				</div>

                <div class="panel-body in" id="calendarFilter">
                    <form id="calendar-filtering">
                        <div class="btn-group" data-toggle="buttons">
							<label class="btn btn-success active">
                                <input name="calendar" value="2" type="radio" class="filtering-options" checked>Kalendarz osobisty
                            </label>
                            <label class="btn btn-success">
                                <input name="calendar" value="1" type="radio" class="filtering-options">Kalendarz kancelaryjny
                            </label>
                        </div>
                        <br /><br />
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-warning">
                                <input name="type" value="0" type="radio" class="filtering-options" >Wszystko
                            </label>
							<label class="btn btn-warning active">
                                <input name="type" value="2" type="radio" class="filtering-options" checked>Zdarzenia osobiste
                            </label>
                            <label class="btn btn-warning">
                                <input name="type" value="1" type="radio" class="filtering-options">Zdarzenia kancelaryjne
                            </label>
                        </div>
                        <!--<br /><br />
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input name="group" value="1" type="radio" class="filtering-options" checked>Wszystkie
                            </label>
                            <label class="btn btn-primary">
                                <input name="group" value="2" type="radio" class="filtering-options">kancelaryjne
                            </label>
                            <label class="btn btn-primary">
                                <input name="group" value="2" type="radio" class="filtering-options">osobiste
                            </label>
                        </div>-->
                        <br /><br />
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-info">
                                <input name="status" value="0" type="radio" class="filtering-options" >Wszystkie
                            </label>
                            <label class="btn btn-info active">
                                <input name="status" value="1" type="radio" class="filtering-options" checked>w trakcie
                            </label>
                            <label class="btn btn-info">
                                <input name="status" value="2" type="radio" class="filtering-options">zakończone
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <div id="calendar"></div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading bg-blue"> 
					<span class="panel-title"> <span class="fa fa-print"></span> Eksport rozpraw</span> 
					<div class="panel-heading-menu pull-right">
						<a class="collapse-link collapse-window" data-toggle="collapse" href="#exportFilter" aria-expanded="true" aria-controls="exportFilter">
							<i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
						</a>
					</div>
				</div>
				
                <div class="panel-body in" id="exportFilter"> 
                    <?= $this->render('_formExport', ['employees' => []]) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading bg-grey"> 
					<span class="panel-title"> <span class="fa fa-users"></span> Pracownicy </span> 
					<div class="panel-heading-menu pull-right">
						<a class="collapse-link collapse-window" data-toggle="collapse" href="#usersFilter" aria-expanded="true" aria-controls="usersFilter">
							<i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
						</a>
					</div>
				</div>
				
                <div class="panel-body in" id="usersFilter"> 
                    <?= $this->render('_employeesPanel', ['employees' => []]) ?>
                </div>
            </div>
        </div>
    </div>
    
	
	
	
<?php $this->endContent(); ?>
