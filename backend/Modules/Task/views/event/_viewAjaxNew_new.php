<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\chat\Comments;
use frontend\widgets\crm\SidesTable;
use common\components\CustomHelpers;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

	<div class="modal-body">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                    <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
                        <?php 
                            $priorityColors = [ 1 => 'red', 2 => 'orange', 3 => 'yellow']; 
                            $statusColors = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
                            echo DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [                      
                                        'attribute' => 'name',
                                        'format'=>'raw',
                                        'label' => ( ($model->type_fk == 1) ? '<span class="text--purple">Rozprawa</span>' : '<span class="text--teal">Zadanie</span>' ) . '&nbsp;<span class="label '.$statusColors[$model->id_dict_task_status_fk].'">'.$model->statustask.'</span>',
                                        'value' => '<i class="fa fa-flag text--'.$priorityColors[$model->id_dict_task_category_fk].'" title="'.$model->category.'"></i>&nbsp;'.$model->name
                                    ], 
                                    [                      
                                        'attribute' => 'event_date',
                                        'format'=>'raw',
                                        'value' => $model->event_date. '  [ <span class="text--grey"><b>'.( ($model->event_time) ? 'godz. '.$model->event_time  : 'cały dzień').'</b></span> ]'. ( ($model->event_deadline && $model->type_fk == 2) ? '<span class="label label-danger" title="Ustawiono deadline!">'.$model->event_deadline.'</span>' : '' ) ,
                                        'label' => 'Termin',
                                    ],
                                    [                      
                                        'attribute' => 'execution_time',
                                        'format'=>'raw',
                                        'value' => ($model->execution_time) ? ($model->timeH.'h '.$model->timeM. ' min') : 'nie ustawiono',
                                        'label' => 'Czas',
                                    ],
                                    [                      
                                        'attribute' => 'id_customer_fk',
                                        'format'=>'raw',
                                        'value' => $model->customer['name'],
                                    ],
                                    [                      
                                        'attribute' => 'id_case_fk',
                                        'format'=>'raw',
                                        'value' => $model->case['name'],
                                    ],
                                    'place',  
                                    'description', 
                                    /*[                      
                                        'attribute' => 'employees_list:html',
                                        'format'=>'raw',
                                        'label' => 'Pracownicy',
                                        'value' => $model->employees_list,
                                    ], 
                                    [                      
                                        'attribute' => 'files_list:html',
                                        'format'=>'raw',
                                        'label' => 'Dokumenty',
                                        'value' => FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => true]),
                                        'visible' => $meEvent
                                    ], */ 
                                    /*[                      
                                        'attribute' => 'correspondence_list:html',
                                        'format'=>'raw',
                                        'label' => 'Korespondencja',
                                        'value' =>  FilesBlock::widget(['files' => $model->correspondence, 'isNew' => $model->isNewRecord, 'typeId' => 5, 'parentId' => $model->id, 'onlyShow' => true]) 
                                    ],       */  
                                ],
                            ]) 
                        ?>
                    </div>
                    <div class="tab-pane" id="tab2b">
                        <?= $model->employees_list ?>
                    </div>
                    <div class="tab-pane" id="tab2c">
                        <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/task/event/note', 'id' => $model->id])]) ?>
                    </div>
                    <div class="tab-pane" id="tab2d">
                        <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => true]) ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
        
            
    <div class="modal-footer"> 
		<?php if(!$edit && $personal == 0 && $meEvent) { ?><a href="/task/event/editajax/<?= CustomHelpers::encode($model->id) ?>?index=<?= $index ?>" title="Edytuj zdarzenie" class="btn btn-sm btn-primary viewEditModal" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class='glyphicon glyphicon-pencil'></i>Edycja"><i class="fa fa-pencil"></i></a> <?php } ?>
		<a href="<?= Url::to(['/task/'.( ($model->type_fk == 1) ? 'case' : 'event' ).'/view', 'id' => $model->id]) ?>" title="Przejdź do karty zdarzenia" class="btn btn-sm bg-pink"><i class="fa fa-link"></i></a>
        <?php if(!$edit && $personal == 0 && $model->type_fk == 1 && $model->id_dict_task_status_fk != 3 && $meEvent) { ?><a href="<?= Url::to(['/task/event/cancelajax', 'id' => $model->id]) ?>" title="Odwołanie sprawy" class="btn btn-sm bg-orange deleteConfirm" data-label="Odwołaj"><i class="fa fa-ban"></i></a> <?php } ?>        
        <?php if(!$edit && $personal == 0 && $model->type_fk == 2 && $model->id_dict_task_status_fk == 1 && $meEvent) { ?><a href="<?= Url::to(['/task/event/closeajax', 'id' => $model->id]) ?>" title="Zamknij zadanie" class="btn btn-sm bg-green deleteConfirm" data-label="Zamknij"><i class="fa fa-check"></i></a> <?php } ?>
        <?php if(!$edit && $personal == 0 && $meEvent) { ?><a href="<?= Url::to(['/task/event/deleteajax', 'id' => $model->id]) ?>" title="Usuń zdarzenie" class="btn btn-sm btn-danger deleteConfirm" data-label="Usuń"><i class="fa fa-trash"></i></a> <?php } ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" title="Zamknij okno">Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>