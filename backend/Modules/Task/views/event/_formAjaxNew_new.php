<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use common\components\CustomHelpers;
use frontend\widgets\company\EmployeesCheck;
use frontend\widgets\crm\SidesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php if(/*$model->delay > 0 &&*/ $model->id_dict_task_status_fk == 3) { echo '<div class="alert alert-danger"><i class="fa fa-ban"></i>&nbsp;Rozprawa została odwołana</div>'; } ?>
            <?= (Yii::$app->params['showClient']) ? $form->field($model, 'show_client')->checkBox() : '' ?>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
                    <ul class="nav panel-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                        <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span> </a></li>
                        <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-file-alt"></i><span class="panel-tabs--text">Opis</span> </a></li>
                        <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2a">
                            <div class="grid">
                                <div class="col-sm-1 col-xs-1">
                                    <input id="caltask-id_dict_task_category_fk" class="form-control" name="CalTask[id_dict_task_category_fk]" type="hidden" value="<?= $model->id_dict_task_category_fk ?>" />
                                    <?php $priorityColors = [ 1 => 'red', 2 => 'orange', 3 => 'yellow']; ?> 
                                    <div class="icons-left">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->id_dict_task_category_fk] ?>" id="priority-flag-task-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                                            <ul class="dropdown-menu " role="menu">
                                                <li>
                                                    <ul class="list-flags">
                                                        <li><span class="priority-flag-task fa fa-flag icon text--red <?= ($model->id_dict_task_category_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="red"></span> </li>
                                                        <li><span class="priority-flag-task fa fa-flag icon text--orange <?= ($model->id_dict_task_category_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="orange"></span> </li>
                                                        <li><span class="priority-flag-task fa fa-flag icon text--yellow <?= ($model->id_dict_task_category_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="yellow"></span> </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-xs-11">
                                    <?= $form->field($model, 'type_fk')->dropDownList( [1 => 'Zadanie', 2 => 'Informacja' ], [ 'class' => 'form-control'] )->label(false) ?>
                                </div>                                
                                <div class="col-sm-6 col-xs-12">
                                    <div class="grid grid--0">
                                        <div class="col-xs-7"><input type='text' class="form-control" id="task_date" name="CalTask[event_date]" value="<?= $model->event_date ?>"/></div>
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <div class='input-group date' id='task_time' >
                                                    <input type='text' class="form-control" name="CalTask[event_time]" value="<?= $model->event_time ?>" />
                                                    <span class="input-group-addon bg-blue" title="Ustaw godzinę. Brak podania godziny ustawi zadanie za cały dzień.">
                                                        <span class="fa fa-clock text--white"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <?php /*$form->field($model, 'id_dict_task_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 7 : 8 ) ), [ 'class' => 'form-control'] )*/ ?> 
                                    <?= $form->field($model, 'id_dict_task_type_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/'.( ($model->type_fk == 1) ? '7' : '8' ) , 
                                                            ['class' => 'insertInline text--white', 
                                                             'data-target' => "#event-type-insert", 
                                                             'data-input' => ".event-type",
                                                             'id' => 'insertType'
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(\backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 7 : 8 )), ['prompt' => ' -- wybierz --', 'class' => 'form-control event-type'] ) ?>
                                    <div id="event-type-insert" class="insert-inline bg-purple2 none"> </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <?= $form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] ) ?>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="col-sm-6 cols-xs-12">
                                    <?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList($model->id_customer_fk), [ 'prompt' => '-- wybierz klienta --', 'id' => 'selectAutoCompleteModal', 'data-url' => Url::to('/crm/default/autocomplete'), ] )->label('Kontrahent') ?>             
                                </div>
                                <div class="col-sm-6 cols-xs-12">
                                    <?= $form->field($model, 'id_case_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\CustomerBranch::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] )->label('Sprawa') ?> 
                                </div>
                            </div>
                            <div class="grid">
                                <?php if(Yii::$app->params['branches']) { ?>
								<div class="col-sm-4 col-xs-12">
                                    <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), [] )
                                                                                    ->label('Region') ?>
                                </div>
								<?php } ?>
                                <div class="col-sm-<?= (Yii::$app->params['branches']) ? '8' : '12' ?> col-xs-12">
                                    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="tab-pane" id="tab2b">
                            <?php /* EmployeesCheck::widget(['idName' => 'menu-list-employee-task', 'employees' => $lists['employees'], 'employeesCheck' => $employees])*/ ?>
                            <?php if(Yii::$app->params['departments']) { ?>
                                <?= $form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select-ajax', 'data-type' => 'matter', 'data-box' => 1, 'multiple' => true ] ); ?>
                            <?php } ?>
                            <?= $form->field($model, 'employees_list')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->orderby('lastname')->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true] ) ?> 
                        </div>
                        <div class="tab-pane" id="tab2c">
                            <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
                        </div>
                        <div class="tab-pane" id="tab2d">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= ($model->status != -2) ? '<a href="/task/event/showajax/'. CustomHelpers::encode($model->id) .'" title="Podgląd zdarzenia" class="btn btn-sm btn-info viewEditModal" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-eye\'></i>Edycja"><i class="fa fa-eye"></i></a>' : ''; ?>
		<?php if($model->status == 1) { ?> <a href="<?= Url::to(['/task/'.( ($model->type_fk == 1) ? 'case' : 'event' ).'/update', 'id' => $model->id]) ?>" title="Przejdź do karty zdarzenia" class="btn btn-sm bg-pink"><i class="fa fa-link"></i></a> <?php } ?>
        <?= Html::submitButton( ($model->status == -2) ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => ($model->status == -2) ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true }); 
    });
</script>
                    
<script type="text/javascript">
    var priorityTask = document.querySelectorAll('.priority-flag-task');
    var priorityTaskColors = ['grey', 'red', 'orange', 'yellow'];
    var priorityTaskInput = document.getElementById('caltask-id_dict_task_category_fk');
    
    for (var i = 0; i < priorityTask.length; i++) {
        priorityTask[i].addEventListener('click', function(event) {
            //console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityTask.length; j++) {
               priorityTask[j].classList.remove('none');
            }
            document.getElementById('priority-flag-task-choice').classList.remove('text--'+priorityTaskColors[priorityTaskInput.value]);
            priorityTaskInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-task-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
    
	document.getElementById('caltask-type_fk').onchange = function(event) {
        var type = 'case';
        if(event.target.value == 2) type = 'event';
        
        var set = document.getElementById("caltask-id_case_fk").value;
        set = (set) ? set : 0;
        
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+set+"?type="+type, true);
            xhr.onreadystatechange=function()  {
                if (xhr.readyState==4 && xhr.status==200)  {
                    var result = JSON.parse(xhr.responseText);
                    document.getElementById('menu-list-employee-task').innerHTML = result.list;     
                    document.getElementById('caltask-id_dict_task_type_fk').innerHTML = result.types; 
                    buildName(); 
                    if(type == 'case') {
                        document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/7' ?>");
                    } else {
                        document.getElementById("insertType").setAttribute("href", "<?= Url::to(['/dict/dictionary/createinline']).'/8' ?>");
                    }
                }
            }
            xhr.send();
            return false;
    }
    
    document.getElementById('selectAutoCompleteModal').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+(this.value ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                
                document.getElementById('caltask-id_customer_branch_fk').innerHTML = result.listBranches;  
                document.getElementById('caltask-id_set_fk').innerHTML = result.listCase;       
                /*document.getElementById('commeeting-meeting_contacts').innerHTML = result.listContacts; 
                $('#commeeting-meeting_contacts').trigger("chosen:updated");    */     
            }
        }
        xhr.send();
        return false;
    }
    
    document.getElementById("caltask-id_case_fk").onchange = function() { 
        var type = 'case';
		//type = document.getElementById('caltask-type_fk');
        /*if(document.querySelector('input[name = "CalTask[type_fk]"]:checked').value == 2) type = 'event';*/
		if(document.getElementById('caltask-type_fk').value == 2) type = 'event';
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+this.value+"?type="+type, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('menu-list-employee-task').innerHTML = result.list;   
                buildName();
                /*var x = document.getElementById('caltask-departments_list');
                for (i = 0; i < x.length; i++) {
                    //console.log(x.options[i].value+ ' - '+result.departments.indexOf(x.options[i].value));
                    if(result.departments.indexOf(x.options[i].value*1) >= 0) {
                        x.options[i].selected = true;
                    } else {
                        x.options[i].selected = false;
                    }
                }
                $('#caltask-departments_list').multiselect('rebuild');
                $('#caltask-departments_list').multiselect('refresh');   */           
            }
        }
       xhr.send();
        return false;
    }
   
    document.getElementById('caltask-place').onfocusout = function() {
	   buildName();      
       return false;
    }
    
    function buildName() {
		var xhr_name = new XMLHttpRequest();
		xhr_name.open('POST', "<?= Url::to(['/task/event/bname']) ?>?case="+document.getElementById('caltask-id_case_fk').value+"&place="+document.getElementById('caltask-place').value+"&type="+document.getElementById('caltask-type_fk').value, true);
		xhr_name.onreadystatechange=function()  {
			if (xhr_name.readyState==4 && xhr_name.status==200)  {
				var result = JSON.parse(xhr_name.responseText);
				document.getElementById('caltask-name').value = result.name; 
			}
		}
	   xhr_name.send();
		
		return false;
	}
    
</script>
