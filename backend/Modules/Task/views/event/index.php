<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zadania');
$this->params['breadcrumbs'][] = 'Sprawy';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-index', 'title'=>Html::encode($this->title))) ?>
	
	<fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-cases" data-form="#filter-task-cases-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
			<?php  echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
    </fieldset>
    <?= Alert::widget() ?>
	<div id="toolbar-cases" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["eventAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/event/new', 'id' => 0]) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'event-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["eventExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/event/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'event-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-task-cases-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-cases"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-cases"
                data-toolbar="#toolbar-cases" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-search-form="#filter-task-cases-search"
                data-sort-name="id"
                data-sort-order="desc"
                data-method="get"
                data-url=<?= Url::to(['/task/event/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="className" data-visible="false">className</th>
                    <th data-field="name"  data-sort-name="sname" data-sortable="true">Nazwa</th>
                    <th data-field="event_term"  data-sortable="true">Termin</th>
                    <th data-field="client"  data-sortable="true">Klient</th>
                    <th data-field="case"  data-sortable="true">Sprawa</th>
                    <th data-field="group"  data-sortable="true" data-align="center"></th>
                    <th data-field="type"  data-sortable="true" data-align="center">Typ</th>
                    <th data-field="status"  data-sortable="true" data-align="center">Status</th>
                    <th data-field="delay"  data-sortable="true">Opóźnienie</th>
                    <?php if(Yii::$app->params['env'] == 'dev' && Yii::$app->params['showClient'] ) { ?>
                    <th data-field="show"  data-sortable="true" data-align="center" data-width="20px"></th>
                    <?php } ?>
                    <th data-field="actions" data-events="actionEvents" data-width="70px" data-align="center"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nie ustawiono czasu</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>

<?php $this->endContent(); ?>
