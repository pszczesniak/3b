<div class="task-body-content">
    <div class="task-content-top">
        <div class="task-meta clearfix">
            <div class="row">
                <div class="col-md-12">
                    <div class="task-assignee">
                        <div>
                            <div class="task-thumb">
                                <img src="/images/avatar.png" alt="user">
                            </div>
                            <div class="task-recipient">
                                <span><span>Dodany przez: </span><?= $model->created_at ?></span><br />
                                <span><span>Przypisany do: </span>Jan Kowalski, Adam Nowak</span>
                            </div>
                        </div>
                    </div>
                    <div class="task-date">
                        <div class="task-date">
                            <div class="task-due-date">
                                <i class="fa fa-clock-o"></i>Dodano: 2016-07-03 12:23:33
                            </div>
                            <div class="task-complete-date">
                                <i class="fa fa-calendar-o"></i>Deadline: 2016-07-10 10:00:00
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="task-header">
            <div class="task-title">
                <h3>Sporządzenie dokumentów</h3>
                <div class="task-tags">
                    <label class="label label-info"> Nowy</label>
                    <label class="label label-danger"> Wysoki</label>
                </div>
            </div>
            <div class="task-details-status">
                <span class="epie-chart" data-percent="100" data-barcolor="#43a047" data-tcolor="#e0e0e0" data-scalecolor="#e0e0e0" data-linecap="butt" data-linewidth="2" data-size="60" data-animate="1"><span class="p-done" style="width: 60px; height: 60px; line-height: 60px; color: rgb(67, 160, 71);"><i class="fa fa-check"></i></span>
                <canvas height="60" width="60"></canvas></span>
            </div>
        </div>
    </div>
    <div class="task-info">
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sem felis, placerat at porttitor at, sollicitudin eleifend eros. Duis at justo consequat neque volutpat convallis. Integer efficitur lorem vitae quam dapibus, nec hendrerit turpis rutrum. Morbi ligula mi, imperdiet nec placerat et, hendrerit non ipsum. Sed ut fermentum ex. Aenean elementum finibus eros, id luctus velit aliquam eget. Quisque lobortis ornare mauris, vitae imperdiet nisi faucibus id. Nulla ac mauris sodales, tempor mauris in, finibus nisi. Proin accumsan ullamcorper dolor ac semper. In pulvinar varius elementum. Nulla malesuada ligula mi, vitae imperdiet nunc porta ut. 
        </p>
        <h4 class="task-inner-header"><i class="ico-attachment"></i> Załączniki</h4>
        <div class="attachement-list">
            <ul>
                <li>
                <div class="attachment-thumb">
                    <a href="#"><i class="fa fa-file-pdf-o"></i></a>
                </div>
                <div class="attachment-info">
                    <div class="attachment-file-name">
                        załącznik_1.pdf
                    </div>
                    <div class="attachment-action-bar">
                        <span class="list-file-download"><a href="#"><i class="fa fa-download"></i> Pobierz</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Usuń</a></span>
                    </div>
                </div>
                </li>
                <li>
                <div class="attachment-thumb">
                    <a href="#"><i class="fa fa-file-text-o"></i></a>
                </div>
                <div class="attachment-info">
                    <div class="attachment-file-name">
                       załącznik_2.jpg
                    </div>
                    <div class="attachment-action-bar">
                        <span class="list-file-download"><a href="#"><i class="fa fa-download"></i> Pobierz</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Usuń</a></span>
                    </div>
                </div>
                </li>
                <li>
                <div class="attachment-thumb">
                    <a href="#"><i class="fa fa-file-image-o"></i></a>
                </div>
                <div class="attachment-info">
                    <div class="attachment-file-name">
                        załącznik_3.jpg
                    </div>
                    <div class="attachment-action-bar">
                        <span class="list-file-download"><a href="#"><i class="fa fa-download"></i> Pobierz</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Usuń</a></span>
                    </div>
                </div>
                </li>
            </ul>
            <a href="#" class="btn btn-default btn-link"><i class="ico-download"></i> Pobierz wszystkie</a>
            <a href="#" class="btn btn-default btn-link"><i class="ico-plus"></i> Dodaj załącznik</a>
        </div>
        <h4 class="task-inner-header">Notatki</h4>
        <div class="task-comments">
            <textarea class="task-comment-input form-control" ></textarea>
            <button class="btn btn-primary">Dodaj notatkę</button>
            <div class="task-comments-list">
                <div class="task-comments-meta">
                    <div class="task-comment-thumb">
                        <img src="/images/avatar.png" alt="user">
                    </div>
                    <div class="ask-comment-intro">
                        <span class="poster-name">Jan Kowlaski</span>
                        <span class="poster-tagline">Dodał komentarz</span>
                        <div class="commnet-post-date">
                             2016-07-03 20:20:36
                        </div>
                    </div>
                </div>
                <div class="task-user-comments">
                    <p>
                        Proszę również o dodanie dokumentów archiwlanych.
                    </p>
                </div>
                <!--<div class="attachment-action-bar">
                    <span class="list-comment-edit"><a href="#"><i class="ico-pen"></i> Edit</a></span><span class="list-comment-reply"><a href="#"><i class="ico-reply"></i> Reply</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Delete</a></span>
                </div>-->
            </div>
            <div class="task-comments-list">
                <div class="task-comments-meta">
                    <div class="task-comment-thumb">
                        <img src="/images/avatar.png" alt="user">
                    </div>
                    <div class="ask-comment-intro">
                        <span class="poster-name">Adam Nowak</span>
                        <span class="poster-tagline">Dodał załącznik</span>
                        <div class="commnet-post-date">
                             2016-07-03 18:31:52
                        </div>
                    </div>
                </div>
                <div class="task-user-comments">
                    <div class="attachement-list">
                        <ul>
                            <li>
                            <div class="attachment-thumb">
                                <a href="#"><i class="fa fa-file-pdf-o"></i></a>
                            </div>
                            <div class="attachment-info">
                                <div class="attachment-file-name">
                                    załącznik_1.pdf
                                </div>
                                <!--<div class="attachment-action-bar">
                                    <span class="list-file-download"><a href="#"><i class="fa fa-download"></i> Download</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Delete</a></span>
                                </div>-->
                            </div>
                            </li>
                        </ul>
                    </div>
                    
                </div>
                <!--<div class="attachment-action-bar">
                    <span class="list-comment-edit"><a href="#"><i class="ico-pen"></i> Edit</a></span><span class="list-comment-reply"><a href="#"><i class="ico-reply"></i> Reply</a></span><span class="list-file-del"><a href="#"><i class="fa fa-trash"></i> Delete</a></span>
                </div>-->
            </div>
        </div>
    </div>
</div>
           