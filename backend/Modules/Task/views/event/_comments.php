<?php
    use yii\helpers\Url;
?>
<div class="task-comments">
    <div class="form-group">
        <textarea rows="2" name="user-comment" class="form-control" id="user-comment" placeholder="Wpisz notatkę..."></textarea>
    </div>
    <div class="form-group align-right">
        <button class="btn btn-primary" id="user-comment-send" data-action="<?= Url::to(['/task/event/note', 'id' => $id]) ?>">Dodaj notatkę</button>
    </div>
    <div class="task-comments-content">
        <?php
            foreach($notes as $key => $note) {
                echo $this->render('_note', ['model' => $note]);
            }
        ?>
    </div>
</div>