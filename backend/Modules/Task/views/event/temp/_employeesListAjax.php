<?php
    /*$specialEmployees = [];
    $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
    foreach($specialEmployeesSql as $key => $value) {
        array_push($specialEmployees, $value->id_employee_fk);
    }*/
    
    $managers = [];
    $managersSql = \backend\Modules\Company\models\CompanyDepartment::find()->all();
    foreach($managersSql as $key => $value) {
        array_push($managers, $value->id_employee_manager_fk);
    }
    
?>
<div class="content-employee-all-select custom-inputs"><input type="checkbox" class="selecctall" id="checkbox_employee"/> <label for="checkbox_employee">Zaznacz wszystkich</label></div>
<div class="content-employee-select custom-inputs">
	<ul id="menu-list-employee-task" class="employee-container ">
		<?php 
			if($case)
                $employeesList = \backend\Modules\Task\models\CalCase::findOne($case)->employees;
            else
                $employeesList = [];
           /* $employeesList = [];
            $employeesList = \backend\Modules\Company\models\CompanyDepartment::allEmployees($departments, ( ($type == 1) ? 'case' : 'event' ) );*/
            //foreach(\backend\Modules\Company\models\CompanyEmployee::getList() as $key=>$value){
            foreach($employeesList as $key=>$value){
				//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
				//array_push($itemsemployees, $tmpData);
                $checked = ''; $disabled = '';
                if(in_array($value->id, $employees)) $checked = ' checked="checked" ';
                if(in_array($value->id, $managers)) $checked = ' checked="checked" ';
                //if(isset($all) && !in_array($value->id, $all) && $employeeKind != 100) $disabled = ' disabled="readonly" ';
                echo '<li><input class="checkbox_employee" id="e'.$value->id.'" type="checkbox" name="employees[]" value="'.$value->id.'" '.$checked . $disabled.'>'
                    .'<label for="e'.$value->id.'" class="'. ( (in_array($value->id, $managers)) ? "text--blue" : "text--black" ) .'">'.($value->lastname.' '.$value->firstname).'</label></li>';
            }
		?>
	</ul> 
</div>
