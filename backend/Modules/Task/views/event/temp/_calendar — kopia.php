<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-task-form form-calendar">
     
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/task/case/dates', 'id' => $model->id]),
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => (!$model->isNewRecord) ? 'ajaxform' : '', 'data-form' => 'form-calendar'],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    
    <div class="grid grid--0">
        <div class="col-md-3 col-xs-7">
            <div class="form-group">
                <label class="control-label" for="caltask-task_fromDate">Start</label>
                <input type='text' class="form-control" id="task_fromDate" name="CalTask[fromDate]" value="<?= $model->fromDate ?>"/>
            </div>
        </div>
        <div class="col-md-3 col-xs-5">
            <div class="form-group">
                <label class="control-label" for="caltask-fromTime">&nbsp;</label>
                <div class='input-group date' id='task_fromTime' >
                    <input type='text' class="form-control" name="CalTask[fromTime]" value="<?= $model->fromTime ?>" />
                    <span class="input-group-addon bg-grey" title="Ustaw godzinę startu">
                        <span class="fa fa-clock-o text--white"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-7">
            <div class="form-group">
                <label class="control-label" for="caltask-toDate">Koniec</label>
                <input type='text' class="form-control" id="task_toDate" name="CalTask[toDate]" value="<?= $model->toDate ?>"/>
            </div>
        </div>
        <div class="col-md-3 col-xs-5">
            <div class="form-group">
                <label class="control-label" for="caltask-toTime">&nbsp;</label>
                <div class='input-group date' id='task_toTime' >
                    <input type='text' class="form-control" name="CalTask[toTime]" value="<?= $model->toTime ?>" />
                    <span class="input-group-addon bg-grey" title="Ustaw godzinę końca">
                        <span class="fa fa-clock-o text--white"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'all_day', [/*'template' => '{input} {label}'*/])->checkbox(['id' => 'calendar-all-day']) ?>


    <!--<div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>-->

    <?php ActiveForm::end(); ?>

</div>
