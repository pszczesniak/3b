<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-task-form form">

    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => (!$model->isNewRecord && $model->status != -2) ? 'ajaxform' : '', ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <?= ( ($model->isNewRecord || $model->status == -2) && $model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <div class="grid">
        <div class="col-sm-6">
            <fieldset><legend>Podstawowe</legend>
                   
				<?= $form->field($model, 'id_dict_task_type_fk')->radioList(\backend\Modules\Task\models\CalTask::listTypes(), 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-sm btn-primary' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
									]) ?>
                <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 

                <?= $form->field($model, 'id_case_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups($model->id_customer_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] )->label('Sprawa/Projekt') ?> 
                

            </fieldset>
            <fieldset><legend>Czas i miejsce  </legend>
                <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'all_day', [/*'template' => '{input} {label}'*/])->checkbox() ?>
                <div class="grid">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_from" class="control-label">Start</label>
                            <div class='input-group' id='datetimepicker_start'>
                                <input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $model->date_from ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <label for="caltask-date_to" class="control-label">Koniec</label>
                            <div class='input-group' id='datetimepicker_end'>
                                <input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $model->date_to ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <?php 
                    if($model->isNewRecord)
                        echo$form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] );
                ?>
                
            </fieldset>
        </div>
        <div class="col-sm-6">
            <fieldset><legend>Opis</legend>
                
                <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
                
            </fieldset>
            <fieldset><legend>Personalizacja</legend>
                <?= ($employeeKind == 70) ? $form->field($model, 'notification_email', ['template' => '{input} {label}'])->checkbox(['uncheck' => 0], true) : '' ?>
                <?= $this->render('_employeesList', ['employees' => $employees, 'case' => $model->id_case_fk, 'all' => $all, 'employeeKind' => $employeeKind]) ?>
            </fieldset>
        </div>
    </div>
    

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
   /* document.onreadystatechange = function(){
        if (document.readyState === 'complete') {console.log('datetimepicker');
            
        }
    };*/
	
	document.getElementById('caltask-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltask-id_case_fk').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById("caltask-id_case_fk").onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+ ( (this.value) ? this.value : 0 )+"?type=event", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('menu-list-employee-task').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
</script>
