<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="validation-form" class="alert none"></div>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body modal-body-max">
        <div class="content ">
            <div class="grid">
                <div class="col-md-6">
                    <?= $form->field($model, 'type_fk')->radioList([1 => 'Rozprawa', 2 => 'Zadanie'], 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-xs btn-info' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'id_dict_task_type_fk')->radioList(\backend\Modules\Task\models\CalTask::listTypes(), 
                                        ['class' => 'btn-group', 'data-toggle' => "buttons",
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                return '<label class="btn btn-xs btn-primary' . ($checked ? ' active' : '') . '">' .
                                                    Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                            },
                                    ]) ?>
                </div>
            </div>
            <?php if( $model->isNewRecord || $model->status == -2 ) echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?> 

            <?= $form->field($model, 'id_case_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -'] ) ?> 
        </div>
        <div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> Czas i miejsce</a> </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body">
						<?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
						<?= $form->field($model, 'all_day', ['template' => '{input} {label}'])->checkbox() ?>
						<div class="grid">
							<div class='col-sm-6'>
								<div class="form-group">
									<label for="caltask-date_from" class="control-label">Start</label>
									<div class='input-group date' id='datetimepicker_start'>
										<input type='text' class="form-control" id="caltask-date_from" name="CalTask[date_from]" value="<?= $model->date_from ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(function () {
									$('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD HH:mm',  });
                                    $('#datetimepicker_start').on("dp.change", function (e) {
                                        /*console.log(e.date);
                                        console.log(e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm'));*/
                                        minDate = e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm');
                                        $('#datetimepicker_end').data("DateTimePicker").minDate(minDate);
                                        $('#caltask-date_to').val(minDate);
                                    });
								});
							</script>
							<div class='col-sm-6'>
								<div class="form-group">
									<label for="caltask-date_to" class="control-label">Koniec</label>
									<div class='input-group date' id='datetimepicker_end'>
										<input type='text' class="form-control" id="caltask-date_to" name="CalTask[date_to]" value="<?= $model->date_to ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(function () {
									$('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD HH:mm' });
                                   
								});
							</script>
						</div>
                        <?= $form->field($model, 'id_reminder_type_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] ) ?> 
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Dodatkowe</a> </h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse ">
					<div class="panel-body">
						<?= $form->field($model, 'id_dict_task_category_fk')->radioList(\backend\Modules\Task\models\CalTask::listCategory(), 
									['class' => 'btn-group', 'data-toggle' => "buttons",
									'item' => function ($index, $label, $name, $checked, $value) {
											return '<label class="btn btn-xs btn-primary' . ($checked ? ' active' : '') . '">' .
												Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
										},
								]) ?>
						<?= $form->field($model, 'id_dict_task_status_fk')->radioList(\backend\Modules\Task\models\CalTask::listStatus(), 
										['class' => 'btn-group', 'data-toggle' => "buttons",
										'item' => function ($index, $label, $name, $checked, $value) {
												return '<label class="btn btn-xs  btn-primary' . ($checked ? ' active' : '') . '">' .
													Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
											},
								]) ?>
					</div>
			    </div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> Opis</a> </h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse ">
					<div class="panel-body">
						<?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> Pracownicy</a> </h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse ">
					<div class="panel-body">
						<?= $this->render('_employeesListAjax', ['employees' => $employees, 'case' => $model->id_case_fk]) ?>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"> Dokumenty</a> </h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse ">
					<div class="panel-body">
						<?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
						<?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
				    </div>
				</div>
			</div>
		</div>

	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
    document.getElementById('caltask-type_fk').onchange = function(event) {
        var type = 'case';
        if(event.target.value == 2) type = 'event';
        
        var set = document.getElementById("caltask-id_case_fk").value;
        
        if(set && set > 0) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+set+"?type="+type, true);
            xhr.onreadystatechange=function()  {
                if (xhr.readyState==4 && xhr.status==200)  {
                    var result = JSON.parse(xhr.responseText);
                    document.getElementById('menu-list-employee-task').innerHTML = result.list;               
                }
            }
            xhr.send();
            return false;
        }
        /*if(event.target.value == 1) {
           
        }*/
    }
    
    document.getElementById('caltask-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('caltask-id_case_fk').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById("caltask-id_case_fk").onchange = function() { 
        var type = 'case';
        if(document.querySelector('input[name = "CalTask[type_fk]"]:checked').value == 2) type = 'event';
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+this.value+"?type="+type, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('menu-list-employee-task').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
</script>
