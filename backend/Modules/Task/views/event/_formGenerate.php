<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    //'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => "#table-events", 'data-input' => ".correspondence-event"],
    'options' => ['class' => 'modalAjaxForm',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-item"/*, 'data-input' => ".correspondence-event"*/],
    'fieldConfig' => [   ],
]); ?>
    <div class="modal-body modal-body-max calendar-task">
        <div class="content ">
            <div class="grid">
                
                <div class="col-sm-1 col-xs-1">
                    <input id="actiongenerate-category_fk" class="form-control" name="ActionGenerate[category_fk]" type="hidden" value="<?= $model->category_fk ?>" />
                    <?php $priorityColors = [ 1 => 'red', 2 => 'orange', 3 => 'yellow']; ?> 
                    <div class="icons-left">
                        <div class="dropdown">
                            <a href="#" class="dropdown__toggle text--<?= $priorityColors[$model->category_fk] ?>" id="priority-flag-task-choice" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-flag"></i></a>
                            <ul class="dropdown-menu " role="menu">
                                <li>
                                    <ul class="list-flags">
                                        <li><span class="priority-flag-task fa fa-flag icon text--red <?= ($model->category_fk == 1) ? ' none' : '' ?>" data-value="1" data-color="red"></span> </li>
                                        <li><span class="priority-flag-task fa fa-flag icon text--orange <?= ($model->category_fk == 2) ? ' none' : '' ?>" data-value="2" data-color="orange"></span> </li>
                                        <li><span class="priority-flag-task fa fa-flag icon text--yellow <?= ($model->category_fk == 3) ? ' none' : '' ?>" data-value="3" data-color="yellow"></span> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="col-sm-5 col-xs-11">
					<!--<label>Termin</label>-->
                    <div class="grid grid--0">
                        <div class="col-xs-7"><input type='text' class="form-control" id="task_date" name="ActionGenerate[event_date]" value="<?= $model->event_date ?>"/></div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <div class='input-group date' id='task_time' >
                                    <input type='text' class="form-control" name="ActionGenerate[event_time]" value="<?= $model->event_time ?>" />
                                    <span class="input-group-addon bg-blue" title="Ustaw godzinę. Brak podania godziny ustawi zadanie za cały dzień.">
                                        <span class="fa fa-clock-o text--white"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <script type="text/javascript">
                        $(function () {
                            $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
                            $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
                            $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
                        });
                    </script>
				</div>
                
                <div class="col-sm-6 col-xs-12">
                    <div class="grid">
                        
                        <div class="col-xs-6">
                            <?= $form->field($model, 'type_fk')->dropDownList( isset(Yii::$app->params['task-types']) ? Yii::$app->params['task-types'] : [1 => 'Rozprawa', 2 => 'Zadanie' ], [ 'class' => 'form-control'] )->label(false) ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'dict_fk')->dropDownList( \backend\Modules\Task\models\CalTask::listTypes( ( ($model->type_fk == 1) ? 7 : 8 ) ), [ 'class' => 'form-control'] )->label(false) ?> 
                        </div>
                    </div>
					
                </div>
            </div>
            <div class="grid grid--0">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label" for="actiongenerate-task_deadline"> Deadline </label>
                        <div class='input-group date' id='task_deadline' >                          
                            <input type='text' class="form-control" name="ActionGenerate[event_deadline]" value="<?= $model->event_deadline ?>" />
                            <span class="input-group-addon bg-red" title="Ustaw deadline">
                                <span class="fa fa-warning text--white" ></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <?= $form->field($model, 'reminder')->dropDownList( \backend\Modules\Task\models\CalTask::listReminder(), [ 'class' => 'form-control'] ) ?>
               </div>
            </div>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<div class="grid grid--0">

                <div class="col-xs-12">
                    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
                </div>
                
            </div>
			
        
		<div class="panel-group" id="accordion">
				
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Opis</a> </h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse in">
						<div class="panel-body">
							<?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('Wygeneruj zdarzenia', ['class' => 'btn btn-sm bg-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_date').on("dp.change", function (e) {
                
                                    minDate = e.date.format('YYYY-MM-DD');
                                    if( $('#task_fromDate').length > 0 ) {
                                        $('#task_fromDate').data("DateTimePicker").minDate(minDate);
                                        $('#task_fromDate input').val(minDate);
                                    }
                                    
                                    /*minDeadline = e.date.format('YYYY-MM-DD 16:00');
                                    if( $('#task_deadline').length > 0 ) {
                                        $('#task_deadline').data("DateTimePicker").minDate(minDeadline);
                                        $('#task_deadline input').val(minDeadline);
                                    }*/
                                });
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
        $('#task_time').on("dp.change", function (e) {
                                    if(!e.date) {
                                        
                                        if( $('#task_fromTime').length > 0 ) {
                                            $("#task_toTime > input").prop('disabled', true).val('');
                                            $("#task_fromTime > input").prop('disabled', true).val('');
                                        }
                                    
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', true);
                                        }
                                    } else {
                                        minDate = e.date.format('HH:mm');
                                        if( $('#task_fromTime').length > 0 ) {
                                            //$('#task_fromTime').data("DateTimePicker").minDate(minDate);
                                            $('#task_fromTime input').val(minDate);
                                            
                                            $("#task_toTime > input").prop('disabled', false);
                                            $("#task_fromTime > input").prop('disabled', false);
                                        }
                                        
                                        if( $("#calendar-all-day").length > 0 ) {
                                            $("#calendar-all-day").prop('checked', false);
                                        }
                                    }
                                });
        $('#task_deadline').datetimepicker({ format: 'YYYY-MM-DD HH:mm', showClear: true, showClose: true }); 
    });
</script>
           

<script type="text/javascript">
    var priorityTask = document.querySelectorAll('.priority-flag-task');
    var priorityTaskColors = ['grey', 'red', 'orange', 'yellow'];
    var priorityTaskInput = document.getElementById('actiongenerate-category_fk');
    
    for (var i = 0; i < priorityTask.length; i++) {
        priorityTask[i].addEventListener('click', function(event) {
            console.log(event.target.getAttribute("data-color"));
            for (var j = 0; j < priorityTask.length; j++) {
               priorityTask[j].classList.remove('none');
            }
            document.getElementById('priority-flag-task-choice').classList.remove('text--'+priorityTaskColors[priorityTaskInput.value]);
            priorityTaskInput.value = event.target.getAttribute("data-value");
            document.getElementById('priority-flag-task-choice').classList.add('text--'+event.target.getAttribute("data-color"));
            event.target.classList.add('none');
        });
    }
    

                
    document.getElementById('actiongenerate-type_fk').onchange = function(event) {
        var type = 'case';
        if(event.target.value == 2) type = 'event';
        
        var set = 0;
        
        if(set && set > 0) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "<?= Url::to(['/task/matter/employees']) ?>/"+set+"?type="+type, true);
            xhr.onreadystatechange=function()  {
                if (xhr.readyState==4 && xhr.status==200)  {
                    var result = JSON.parse(xhr.responseText);console.log(result.list);
                    document.getElementById('menu-list-employee-task').innerHTML = result.list;  
                    document.getElementById('actiongenerate-id_dict_task_type_fk').innerHTML = result.types;               
                }
            }
            xhr.send();
            
        }
        /*if(event.target.value == 1) {
           
        }*/
        return false;
    }

  

    
</script>
