<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
$this->title = Yii::t('app', 'Edycja zadania');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Alert');
?>

<div class="alert alert-danger">
    Zadanie <b><?= $model->name ?></b> zostało usunięte <br /><br />
    Data usunięcia: <?= $model->deleted_at ?><br />
    Osoba usuwająca: <?= \common\models\User::findOne($model->deleted_by)->fullname ?>
</div>