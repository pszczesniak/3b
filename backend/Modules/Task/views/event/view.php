<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\files\FilesBlock;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zdarzenia'), 'url' => Url::to(['/task/event/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["eventAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/task/event/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowe zadanie</a>
		    <?php } ?>
			<a href="<?= Url::to(['/timeline/event', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Przebieg</a>
            <?php if( ( count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) && 1==2 ) { ?>
				<a href="<?= Url::to(['/task/event/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
		</div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-task-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading <?= ($model->type_fk == 1) ? ' bg-blue' : 'bg-teal' ?>">
                    <strong><?= ($model->type_fk == 1) ? 'Rozprawa' : 'Zadanie' ?></strong>
                    <?php if($model->id_dict_task_status_fk == 1) { echo '<a href="'. Url::to(['/task/event/close', 'id' => $model->id]) .'" class="btn bg-green btn-sm btn-flat pull-right" style="margin-top:-5px"><span class="fa fa-calendar-check-o"></span> Zrobione</a>'; } ?>
                    <a href="<?= Url::to(['/task/event/update', 'id' => $model->id]) ?>" class="btn btn-primary btn-sm btn-flat pull-right" style="margin-top:-5px"><span class="fa fa-pencil"></span> Edytuj</a>
                </div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p><span class="bold">Typ</span>: <?= $model->type ?></p>
                        </div>
                        
                        <div class="row-details">
                            <p><span class="bold"> Miejsce </span>: <?= ($model->place) ? $model->place : 'brak danych' ?> </p>
                        </div>

                        
                        <div class="row-details">
                            <p><span class="bold"> <?= ($model->case['type_fk'] == 1) ? 'Sprawa' : 'Projekt' ?></span>: <a href="<?= Url::to(['/task/'.(($model->case['type_fk'] == 1) ? 'matter' : 'project').'/view', 'id' => $model->id_case_fk]) ?>"><?= $model->case['name'] ?></a></p>
                        </div>

                        <div class="row-details">
                            <div class="grid profile">
                                <div class="col-md-4">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="profile-name">
                                        <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['statusname'] ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="row-details">
                            <p>
                                <span class="bold">Działy</span>: 
                                <ol>
                                <?php 
                                    foreach($model->departments as $key => $item) { 
                                        echo '<li>'.$item->department['name'].'</li>';
                                    }
                                ?>
                                </ol>
                            </p>
                        </div>-->
                        <?php if( count($model->resources) > 0 ) { ?>
						<div class="row-details">
                            <p>
                                <span class="bold">Zasoby</span>: 
                                <ol>
                                <?php 
                                    foreach($model->resources as $key => $item) { 
                                        echo '<li>'.$item->resource['name'].'</li>';
                                    }
                                ?>
                                </ol>
                            </p>
                        </div>
                        <?php } ?>
                    </div>
                </div><!--/project-body-->
            </section>
            <div class="panel panel-default">
                <div class="panel-heading bg-red"> 
                    <span class="panel-title"> <span class="fa fa-bell"></span> Przypomnienie </span> 
                    <div class="panel-heading-menu pull-right">
                        <a aria-controls="cal-task-update-alerts" aria-expanded="true" href="#cal-task-update-alerts" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body collapse in" id="cal-task-update-alerts">
                    <?php /* $this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => false]) */ ?>
                    <?= $this->render('_reminder', ['model' => $model,]) ?>
                    <fieldset><legend>Dodatkowe powiadomienia</legend>
                        <?= \frontend\widgets\alert\AlertsTable::widget(['id' => $model->id, 'type' => 1]) ?>
                    </fieldset>
                </div>
            </div>
            
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            
            <div class="task-body-content">
                <div class="task-content-top">
                    <div class="task-meta clearfix">
                        <div class="row">
                            <?php if($model->delay > 0 && $model->id_dict_task_status_fk == 1) { echo '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>&nbsp;Przekroczyłeś czas wykonania o <b>'.$model->delay.'</b> h</div>'; } ?>
                            <div class="col-md-12">
                                <div class="task-assignee">
                                    <div>
                                        <!--<div class="task-thumb">
                                            <img src="/images/avatar.png" alt="user">
                                        </div>-->
                                        <div class="task-recipient">
                                            <span><span>Utworzono: </span> <?= $model->creator ?> [<?= $model->created_at ?>]</span><br />
                                            <?php if($model->updated_by) { ?><span><span>Aktualizowano: </span> <?= $model->updating ?> [<?= $model->updated_at ?>]</span><br /><?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="task-date">
                                    <div class="task-date">
                                        <div class="task-due-date">
                                            <i class="fa fa-calendar-o text--blue"></i>Termin: <?= $model->event_date . ' ' . $model->event_time ?>
                                        </div>  
                                        <div class="task-complete-date">
                                            <i class="fa fa-clock-o text--pink"></i>Czas: <?= ($model->execution_time) ? ($model->timeH.'h '.$model->timeM.'min') : 'nie ustawiono' ?>                                   
                                        </div>
                                        <div class="task-reminder-date reminder-info">
                                            <i class="fa fa-bell text--yellow"></i>Przypomnienie: <span><?= ($model->reminded == 1) ? date('Y-m-d', strtotime($model->reminder_date)).' 08:00' : 'nie ustawiono' ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="task-header">
                        <div class="task-title">
                            <h3><?= $model->name ?></h3>
                            <div class="task-tags">
                                <?php
                                    $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
                                    $colors2 = [1 => 'bg-teal', 2 => 'bg-green'];
                                ?>
                                <label class="label <?= $colors2[$model->id_dict_task_status_fk] ?>"> <?= $model->statusname ?> </label>
                                <label class="label <?= $colors1[$model->id_dict_task_category_fk] ?>"> <?= $model->category ?> </label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="task-info">
                <?= ($model->description) ? $model->description : '<div class="alert alert-warning">brak opisu</div>' ?>
            </div>
            <div class="panel with-nav-tabs panel-default">
                <?php $notes = $model->notes; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-users"></i><span class="panel-tabs--text">Pracownicy</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                        <!--<li><a data-toggle="tab" href="#tab3"><i class="fa fa-inbox"></i><span class="panel-tabs--text">Korespondecnja </span></a></li>-->
                        <li><a data-toggle="tab" href="#tab4"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?php /*$this->render('_employees', ['employees' => $model->employees])*/ ?>
                            <?= EmployeesBlock::widget(['employees' => $model->employees, 'employeesLock' => $model->employeeslock]) ?>
						</div>
						<div class="tab-pane" id="tab2">
                            <?php /*$this->render('_files', ['model' => $model, 'type' => 4, 'onlyShow' => true])*/ ?>
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => true]) ?>
						</div>
                        <!--<div class="tab-pane" id="tab3">
                            <?php /* $this->render('_files', ['model' => $model, 'type' => 5, 'onlyShow' => true])*/ ?>
                            <?= FilesBlock::widget(['files' => $model->correspondence, 'isNew' => $model->isNewRecord, 'typeId' => 5, 'parentId' => $model->id, 'onlyShow' => true]) ?>
						</div>-->
                        <div class="tab-pane" id="tab4">
                            <?php /* $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes])*/ ?>
                            <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/task/event/note', 'id' => $model->id])]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

<?php $this->endContent(); ?>
