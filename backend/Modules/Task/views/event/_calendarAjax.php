<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

    
<div class="grid grid--0">
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="caltask-task_fromDate">Start</label>
            <input type='text' class="form-control" id="task_fromDate" name="CalTask[fromDate]" value="<?= $model->fromDate ?>"/>
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="caltask-fromTime">&nbsp;</label>
            <div class='input-group date' id='task_fromTime' >
                <input type='text' class="form-control" name="CalTask[fromTime]" value="<?= $model->fromTime ?>" <?= ($model->all_day == 1) ? "disabled" : '' ?> />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę startu">
                    <span class="fa fa-clock-o text--white"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="caltask-toDate">Koniec</label>
            <input type='text' class="form-control" id="task_toDate" name="CalTask[toDate]" value="<?= $model->toDate ?>"/>
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="caltask-toTime">&nbsp;</label>
            <div class='input-group date' id='task_toTime' >
                <input type='text' class="form-control" name="CalTask[toTime]" value="<?= $model->toTime ?>" <?= ($model->all_day == 1) ? "disabled" : '' ?> />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę końca">
                    <span class="fa fa-clock-o text--white"></span>
                </span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#task_fromDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#task_fromDate').on("dp.change", function (e) {
    
                        //minDate = e.date.add(1/12, 'hours').format('YYYY-MM-DD HH:mm');
                        minDate = e.date.format('YYYY-MM-DD HH:mm');
                        $('#task_toDate').data("DateTimePicker").minDate(minDate);
                        $('#task_toDate input').val(minDate);
                    });

            
    $('#task_fromTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });

    $('#task_toDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 

    $('#task_toTime').datetimepicker({  format: 'LT', showClear: true, showClose: true, ignoreReadonly: false });
          
</script>
