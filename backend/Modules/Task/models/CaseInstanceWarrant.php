<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%case_instance_warrant}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $release_date
 * @property string $delivery_date
 * @property integer $id_case_fk
 * @property integer $id_case_instance_fk
 * @property double $awarded_date
 * @property double $amount_awarded
 * @property double $amount_replacement
 * @property double $amount_stamp_duty
 * @property double $amount_entry
 * @property double $amount_postage
 * @property double $amount_registration
 * @property string $warrant_content
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseInstanceWarrant extends \yii\db\ActiveRecord
{
    public $user_action;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_instance_warrant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_case_fk', 'id_case_instance_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['release_date', 'delivery_date', 'awarded_date', 'created_at', 'updated_at', 'deleted_at', 'user_action'], 'safe'],
            [['id_case_fk', 'id_case_instance_fk'], 'required'],
            [['amount_awarded', 'amount_replacement', 'amount_stamp_duty', 'amount_entry', 'amount_postage', 'amount_registration'], 'number'],
            [['warrant_content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'release_date' => Yii::t('app', 'Data wydania'),
            'delivery_date' => Yii::t('app', 'Data doręczenia'),
            'id_case_fk' => Yii::t('app', 'Sprawa'),
            'id_case_instance_fk' => Yii::t('app', 'Postępowanie'),
            'awarded_date' => Yii::t('app', 'Data zasądzenia'),
            'amount_awarded' => Yii::t('app', 'Kwota zasądzona'),
            'amount_replacement' => Yii::t('app', 'Koszty zastępstwa'),
            'amount_stamp_duty' => Yii::t('app', 'Opłata skarbowa'),
            'amount_entry' => Yii::t('app', 'Koszty wpisu'),
            'amount_postage' => Yii::t('app', 'Opłata pocztowa'),
            'amount_registration' => Yii::t('app', 'Amount Registration'),
            'warrant_content' => Yii::t('app', 'Treść nakazu'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}

	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
	
        if($this->user_action == 'create') {
            $modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $this->id_case_fk;
			$modelArch->user_action = 'warrant';
			$modelArch->data_change = \yii\helpers\Json::encode(['id' => $this->id]);
			//$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
        } 
		parent::afterSave($insert, $changedAttributes);
	}
}
