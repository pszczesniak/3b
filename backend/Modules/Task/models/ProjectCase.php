<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%project_case}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $id_side_fk
 * @property string $created_at
 * @property integer $created_by
 */
class ProjectCase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_case}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'id_project_fk'], 'required'],
            [['id_project_fk', 'id_case_fk', 'created_by', 'deleted_by', 'status'], 'integer'],
            [['created_at', 'deleted_at', 'note', 'custom_data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_project_fk' => 'Rola',
            'id_case_fk' => Yii::t('app', 'Postępowanie'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function delete() {
		
        $this->status = -1;
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		
		return $result;
	}
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public function getProject()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_project_fk']);
    }
}
