<?php

namespace backend\Modules\Task\models;

use Yii;

/**
 * This is the model class for table "{{%case_debtcollector_installment}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $rate_title
 * @property string $rate_date
 * @property double $rate_value
 * @property string $notes
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseDebtcollectorInstallment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_debtcollector_installment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['rate_title', 'rate_date'], 'required'],
            [['rate_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['rate_value'], 'number'],
            [['notes'], 'string'],
            [['rate_title'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'rate_title' => Yii::t('app', 'Rate Title'),
            'rate_date' => Yii::t('app', 'Rate Date'),
            'rate_value' => Yii::t('app', 'Rate Value'),
            'notes' => Yii::t('app', 'Notes'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
