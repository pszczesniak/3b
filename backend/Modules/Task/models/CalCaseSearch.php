<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Task\models\CalCase;

/**
 * CalCaseSearch represents the model behind the search form about `backend\Modules\Task\models\CalCase`.
 */
class CalCaseSearch extends CalCase
{
    public $id_employee_fk;
    public $filter_status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'update_for_all', 'id_dict_case_type_fk', 'id_dict_case_category_fk', 'id_dict_case_status_fk', 'id_customer_fk', 'id_company_fk', 'id_company_branch_fk', 'status', 'created_by', 'updated_by', 'deleted_by','filter_status'], 'integer'],
            [['name', 'description', 'custom_data', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CalCase::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_fk' => $this->type_fk,
            'update_for_all' => $this->update_for_all,
            'id_dict_case_type_fk' => $this->id_dict_case_type_fk,
            'id_dict_case_category_fk' => $this->id_dict_case_category_fk,
            'id_dict_case_status_fk' => $this->id_dict_case_status_fk,
            'id_customer_fk' => $this->id_customer_fk,
            'id_company_fk' => $this->id_company_fk,
            'id_company_branch_fk' => $this->id_company_branch_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data]);

        return $dataProvider;
    }
}
