<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%case_claim}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_case_fk
 * @property string $title
 * @property integer $id_dict_claim_type_fk
 * @property string $occurence_date
 * @property string $limitation_date
 * @property integer $limitation_days
 * @property double $amount
 * @property double $amount_original
 * @property double $amount_cost
 * @property string $payment_date
 * @property string $interest_date
 * @property integer $id_dict_interest_type_fk
 * @property integer $calcualte_contractual_interest
 * @property string $contractual_interest_details
 * @property double $provision_capital
 * @property double $provision_interest
 * @property integer $is_security
 * @property string $security_details
 * @property integer $is_settlement
 * @property string $settlement_details
 * @property string $note
 * @property string $custom_data
 * @property string $created_at
 * @property integer $created_by
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseClaim extends \yii\db\ActiveRecord
{
    
    public $id_customer_fk;
    public $date_from;
    public $date_to;
    
    public $attachs;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_claim}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_case_fk', 'title', 'id_dict_claim_type_fk'], 'required'],
            [['type_fk', 'id_case_fk', 'id_dict_claim_type_fk', 'id_currency_fk', 'limitation_days', 'id_dict_interest_type_fk', 'calcualte_contractual_interest', 'is_security', 'is_settlement', 'created_by', 'status', 'deleted_by'], 'integer'],
            [['occurence_date', 'limitation_date', 'payment_date', 'interest_date', 'created_at', 'deleted_at', 'contractual_interest_date', 'attachs'], 'safe'],
            [['amount', 'interest', 'debt_amount', 'debt_interest', 'debt_balance', 'amount_original', 'amount_cost', 'provision_capital', 'provision_interest', 'contractual_interest_rate'], 'number'],
            [['security_details', 'settlement_details', 'note', 'custom_data'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['contractual_interest_date', 'contractual_interest_rate'], 'required', 'when' => function($model) { return ($model->calcualte_contractual_interest == 1); },],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Grupa'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'title' => Yii::t('app', 'Nazwa'),
            'id_dict_claim_type_fk' => Yii::t('app', 'Type'),
            'occurence_date' => Yii::t('app', 'Data'),
            'limitation_date' => Yii::t('app', 'Limitation Date'),
            'limitation_days' => Yii::t('app', 'Limitation Days'),
            'id_currency_fk' => 'Waluta',
            'amount' => Yii::t('app', 'Należność'),
            'amount_original' => Yii::t('app', 'Należność pierwotna'),
            'amount_cost' => Yii::t('app', 'Koszt zakupu'),
            'payment_date' => Yii::t('app', 'Data płatności'),
            'interest_date' => Yii::t('app', 'Naliczaj od'),
            'id_dict_interest_type_fk' => Yii::t('app', 'Odsetki'),
            'calcualte_contractual_interest' => Yii::t('app', 'Naliczanie odsetek umownych'),
            'contractual_interest_date' => Yii::t('app', 'Naliczaj od'),
            'contractual_interest_rate' => Yii::t('app', 'Stopa %'),
            'provision_capital' => Yii::t('app', 'Prowizja od wpłat na kapitał'),
            'provision_interest' => Yii::t('app', 'Prowizja od wpłat na odsetki'),
            'is_security' => Yii::t('app', 'Is Security'),
            'security_details' => Yii::t('app', 'Security Details'),
            'is_settlement' => Yii::t('app', 'Is Settlement'),
            'settlement_details' => Yii::t('app', 'Settlement Details'),
            'note' => Yii::t('app', 'Note'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_customer_fk' => 'Klient'
        ];
    }
    
    public function beforeSave($insert) {
        if($this->type_fk == 1) {
            $calcualtion = self::calculate($this->amount, $this->interest_date, date('Y-m-d'), $this->id_dict_interest_type_fk);
            $this->debt_interest = $calcualtion['interest'];
            $this->debt_balance = $calcualtion['interest']+$this->amount;
        }
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert && $this->attachs) {
            foreach($this->attachs as $key => $file) {
                if($file) {
                    $modelFile = new \common\models\Files(['scenario' => 'save']);
                    $modelFile->extension_file = $file->extension;
                    $modelFile->size_file = $file->size;
                    $modelFile->mime_file = $file->type;
                    $modelFile->name_file = $file->baseName;
                    $modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
                    $modelFile->title_file = $file->baseName.'.'.$file->extension;
                    $modelFile->id_fk = $this->id;
                    $modelFile->id_type_file_fk = 10;
                    $modelFile->id_dict_type_file_fk = 0;
                    $modelFile->type_fk = 0;
                    
                    if($modelFile->save()) {
                        /*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
                        try {
                            $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                        } catch (\Exception $e) {
                            var_dump($e); exit;
                        }
                    }
                }
            }
        } 
    }
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public static function claimGroups() {
        return [1 => 'finansowe', 2 => 'niefinansowe'];
    } 
    
    public static function claimTypes() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 13])->all();
        $roles = [];
        foreach($items as $key => $item) { 
            $roles[$item->id] = $item->name;
        }
        return $roles;
    }
    
    public static function interestTypes() {
        /*$interestList = [];
        $interestList[1] = 'Ustawowe';
        $interestList[2] = 'Podatkowe';
        $interestList[3] = 'Ustawowe w tr. handl.';
        $interestList[4] = 'Kapitałowe';*/
        $data = \backend\Modules\Config\models\Indicator::find()->where(['type_fk' => 1, 'status' => 1])->all();
        foreach($data as $key => $value) {
            $interestList[$value->id] = $value->name;
        }
        
        return $interestList;
    }
    
    public static function getList($id) {
        if($id == -1) {		
            $claims = CaseClaim::find()->where(['status' => 1])->orderby('title')->all();
        } else {
            $claims = CaseClaim::find()->where(['status' => 1, 'id_case_fk' => $id])->orderby('title')->all();
        }
        
        return ($claims) ? $claims : [];
    }
    
    public static function calculate($amount, $dateFrom, $dateTo, $indicator) {
        $interest = false; $delay = false;
        
        //$model = CaseClaim::findOne($id);
      
        $rates = \backend\Modules\Config\models\IndicatorValue::find()->where(['status' => 1, 'id_indicator_fk' => $indicator])->andWhere("(date_to >= '".$dateFrom."' or date_to is null)")->orderby('date_from')->all();
        foreach($rates as $key => $value) {
            
            $dateToTemp = ($value->date_to && strtotime($value->date_to) < strtotime($dateTo)) ? $value->date_to : $dateTo;
            $dateFromTemp = (strtotime($value->date_from) < strtotime($dateFrom)) ? $dateFrom : $value->date_from;
            //echo $dateFromTemp.' - '.$dateToTemp.'; ';
            ////$datediff = strtotime($dateToTemp) - strtotime($dateFromTemp);
            ////$delayTemp = floor($datediff / (60 * 60 * 24));
            
            $date1=date_create($dateFromTemp);
            $date2=date_create($dateToTemp);
            $interval=date_diff($date1,$date2);
            $delayTemp = $interval->format('%a');
            
            /*$interestAll = $amount * $value->value / 100;
            $interestPerDay = $interestAll / 365; echo $delayTemp.' => '.$interestPerDay.'; ';
            $interest += ($interestPerDay*$delayTemp);*/
            
            $interestPerDay = $amount * $delayTemp * ($value->value / 100) / 365; 
            //echo $interestPerDay.' ('.$delayTemp.'); ';
            $interest += $interestPerDay;
            
            $delay += $delayTemp;
        }
        
        return ['interest' => $interest, 'delay' => $delay];
    }
    
    public function getFiles() {
        $filesData = [];
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 10])->orderby('id desc')->all(); 
        return $filesData;
    }
}
