<?php

namespace frontend\Modules\Task\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;

use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalCase;
/**
 * ActionForm is the model behind the contact form.
 */
class ActionGenerate extends Model
{
    public $type_fk; 
    public $dict_fk;
    public $category_fk;
    public $event_date;
    public $event_time;
    public $event_deadline;
    public $employees_list;
    public $cases_list; 
    public $name;
    public $description;
    public $place;
    public $reminder;
    public $boxId;
    
    public $events_list;
    public $build_list = '';
    public $errors;
    
    public $id_company_branch_fk;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'dict_fk', 'event_date', 'name'], 'required'],
            [['reminder', 'category_fk', 'id_company_branch_fk'], 'integer'],
            [['place', 'description', 'cases_list'],'safe'],
            ['event_time', 'required', 'message' => 'Proszę podać godzinę rozprawy', 'when' => function($model) { return ($model->type_fk == 1); }],
            ['event_deadline','validateDeadline'],
        ];
    }
    
    public function validateDeadline(){
        $eventDate = $this->event_date.' '. ( ($this->event_time) ? $this->event_time : '00:00') .':00';
		if(strtotime($this->event_deadline.':00') < strtotime($eventDate)){
			$this->addError('event_deadline','Deadline nie może być wcześniejszy niż termin');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Rodzaj',
            'place' => 'Miejsce',
            'name' => 'Nazwa',
            'reminder' => 'Przypomnienie',
            'dict_fk' => 'Typ',
            'id_company_branch_fk' => 'Oddział'
        ];
    }

    public function saveAction()  {            
        $this->events_list = [];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach($this->cases_list as $key => $case) {
                $tempCase = explode('|', $case);
                $caseModel = CalCase::findOne($tempCase[0]);

                $new = new CalTask();
                $new->name = $this->name;
                $new->id_customer_fk = $caseModel->id_customer_fk;
                $new->type_fk = $this->type_fk;
                $new->id_dict_task_category_fk = $this->category_fk;
				$new->id_dict_task_type_fk = $this->dict_fk;
                $new->id_dict_task_status_fk = 1;
                $new->event_date = $this->event_date;
                $new->event_time = $this->event_time;
                $new->event_deadline = $this->event_deadline;
                $new->description = $this->description;
                $new->place = $this->place;
                $new->status = 1;
                $new->id_case_fk = $caseModel->id;
                $new->id_instance_fk = (isset($tempCase[1])) ? $tempCase[1] : 0;
                
                 /*reminder start*/
                if($this->event_deadline) {
                    $deadline = $this->event_deadline;
                } else {
                    $deadline = $this->event_date.' '.( ($this->event_time) ? $this->event_time : '16:00').':00';
                }
            
                if($this->reminder == 0) {
                    $new->reminded = 0;
                } else {
                    $new->reminded = 1;
                    $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $this->reminder, 'dateTimestamp' => $deadline]);
                    
                    $new->id_reminder_type_fk = $this->reminder;
                    $new->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
                    $new->reminder_user = \Yii::$app->user->id;
                }
                /*reminder end*/
                
                $new->date_from = $new->event_date.' '. ( ($new->event_time) ? $new->event_time.':00' : '00:00:00' );
                $new->date_to = ($new->event_deadline) ? $new->event_deadline : null;
                $new->id_employee_fk = 1;
                
                if(!$new->date_from) $new->date_from = $new->event_date.' '. ( ($new->event_time) ? $new->event_time : '00:00' ) .':00';            
                if(!$new->date_to) $new->date_to = $new->event_date.' '. ( ($new->event_time) ? $new->event_time : '00:00' ) .':00';
                
                if($new->save()) {
                    foreach($caseModel->employees as $i => $employee) {
                        $newTE = new \backend\Modules\Task\models\TaskEmployee();
                        $newTE->id_case_fk = $new->id_case_fk;
                        $newTE->id_task_fk = $new->id;
                        $newTE->id_employee_fk = $employee['id_employee_fk'];
                        $newTE->status = 1;
                        
                        $newTE->save();
                    } 
                    array_push($this->events_list, $new->id);
                    $this->build_list .= '<li><a href="'.Url::to(['/task/event/editajax', 'id' => $new->id]).'" class="text--navy gridViewModal" data-target="#modal-grid-item">'.$new->name.' ['.$caseModel->name.']'.'</a></li>';
                } else {
                    $transaction->rollBack();
                    $this->errors = $new->getErrors();
                    return false;
                }
            }
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $this->errors = $new->getErrors();
            $transaction->rollBack();
            return false;              
        }	
        return false;
    }
}
