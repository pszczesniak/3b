<?php

namespace frontend\Modules\Task\models;

use Yii;
use yii\base\Model;

use backend\Modules\Task\models\CalTodo;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccPeriod;

/**
 * ActionForm is the model behind the contact form.
 */
class ActionForm extends Model
{
    public $id;
    public $id_employee_fk;
    public $id_department_fk;
    public $name_1;
    public $name_2;
    public $description;
    public $type; 
    public $method_1; 
    public $method_2; 
    
    public $timeDate;
    public $timeH;
    public $timeM;
    public $executionTime;
    
    public $id_customer_fk;
    public $id_case_fk;
    public $id_order_fk;
    public $id_event_fk;
    public $id_meeting_fk;
    public $id_task_fk;
    
    public $cost_net;
    public $cost_vat;
    public $cost_gros;
    public $cost_description;
    public $cost_currency_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'timeDate', 'id_employee_fk'], 'required'],
            [['method_1', 'method_2', 'description', 'cost_net', 'cost_gros', 'cost_vat', 'id_case_fk', 'id_event_fk'], 'safe'],
            [['timeH', 'timeM', 'executionTime', 'id_case_fk', 'id_meeting_fk', 'cost_currency_id'], 'integer'],
            [['id_customer_fk'], 'required', 'message' => 'Proszę podać klienta, na rzecz którego rejestrowana jest czynność', 'when' => function($model) { return ($model->type == 1); } ],
            [['id_customer_fk'], 'checkDebt', 'message' => 'Klient ma włączoną blokadę windykacyjną', 'when' => function($model) { return ($model->type == 1); } ],
           // ['name_1', 'required', 'message' => 'Proszę podać kategorię', 'when' => function($model) { return ($model->type == 1); }],
           // ['name_2', 'required', 'message' => 'Proszę podać kategorię', 'when' => function($model) { return ($model->type == 2); }],
            ['id_department_fk', 'required', 'when' => function($model) { return $model->type == 1; }],
            ['description', 'required', 'message' => 'Proszę podać opis', 'when' => function($model) { return ($model->type == 1 && $model->method_1 != 4); }],
            [['cost_net'], 'required', 'message' => 'Proszę podać wartość netto kosztu', 'when' => function($model) { return ($model->type == 1 && $model->method_1 == 4); }],
            [['cost_description'], 'required', 'message' => 'Proszę podać opis kosztu', 'when' => function($model) { return (($model->type == 1 && $model->method_1 == 4) || $model->cost_net > 0); }],
            //[['timeM'], 'validateExecutiontime',  'when' => function($model) { return ( $model->type == 2 || ($model->type == 1 && $model->method_1 != 4) ); }],
            [['timeM'], 'required',  'when' => function($model) { return ( ($model->type == 2 || ($model->type == 1 && $model->method_1 != 4)) && !$model->timeH && !$model->timeM ); }, 'message' => 'Proszę podać czas wykonania'],            
			[['id_employee_fk'], 'validateLimittime',  'when' => function($model) { return ( !empty($model->id_case_fk) ); }],
            ['timeDate', 'checkPeriod' ],
            /*[['id_order_fk'], 'required',  'when' => function($model) { 
                                                            $customer = \backend\Modules\Crm\models\Customer::findOne(($model->id_customer_fk) ? $model->id_customer_fk : 0);
                                                            return $customer->acc_order_required;
                                                        }],*/
			[['id_order_fk'], 'validateOrderactive',  'when' => function($model) { return ( !empty($model->id_order_fk) ); }],
        ];
    }
    
    public function validateExecutiontime(){
		if(!$this->timeH && !$this->timeM){
			$this->addError('timeM','Proszę podać czas wykonania czynności');
		} 
	}
    
    public function checkPeriod($attribute, $params)  {                
        $acc_period = date('Y-m', strtotime($this->timeDate));
        $periodArr = explode('-', $acc_period);
        $periodModel = AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            $this->addError($attribute, 'Okres rozliczeniowy '.$acc_period.' został zamknięty i nie można już modyfikować czynności');
        }
    }
    
    public function validateLimittime(){
        $set = \backend\Modules\Task\models\CalCase::findOne($this->id_case_fk); 
        if($set && $set->details['is_limited']) {
            $modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_case_fk' => $this->id_case_fk])->one();
            
            if($modelCe) {
                $sql = "select sum(unit_time) as unit_time from {{%acc_actions}} where status = 1 and id_employee_fk = ".$this->id_employee_fk." and id_set_fk = ".$this->id_case_fk;
               
                $dataStats = Yii::$app->db->createCommand($sql)->queryOne();
                    
                $toUsed = $modelCe->time_limit*60 - $dataStats['unit_time'];
                //$this->addError('id_employee_fk', $sql);
                $H = ($this->timeH) ? $this->timeH*60 : 0;
                $m = ($this->timeM) ? $this->timeM : 0;
                $execution_time = $H + $m;
                //$this->addError('id_employee_fk', 'Liczba godzin: '.$toUsed.' => '.$execution_time);
                if($toUsed < $execution_time) {
                    $timeH = intval($toUsed/60);
                    $timeM = $toUsed - ($timeH*60);
                    $this->addError('id_employee_fk', 'Liczba godzin możliwa do wykorzystania w ramach limitu dla tego projektu wynosi: '.round($toUsed/60,2).' ['.$timeH.' h '.$timeM.' min]');
                }
            }
        }	 
	}
	
	public function validateOrderactive(){
        if($this->id_order_fk) {
			$order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
            if($order) {
				if($order->id_customer_fk != $this->id_customer_fk) {
					$bindCustomer = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['status' => 1, 'id_order_fk' => $this->id_order_fk, 'id_customer_fk' => $this->id_customer_fk])->count();
					if(!$bindCustomer) { 
						$this->addError('id_order_fk', 'Klient nie jest przypisany do tego zlecenia');
					}
				}
				if($order->date_from > $this->timeDate) {
					$this->addError('id_order_fk', 'Zlecenie jest aktywne od '.$order->date_from);
				}
				if($order->date_to) {
					if($order->date_to < $this->timeDate) {
						$this->addError('id_order_fk', 'Zlecenie jest aktywne od '.$order->date_to);
					}
				}
			}
		} 
	}
    
    public function checkDebt($attribute, $params)  {                
        $customer = \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk);

        if($customer && $customer['debt_lock'] == 1) {
            $this->addError($attribute, 'Klient ma włączoną blokadę windykacyjną');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Nazwa',
            'type' => 'Rodzaj',
            'method_1' => 'Typ',
            'method_2' => 'Typ',
            'name_1' => 'Rodzaj usługi',
            'name_2' => 'Rodzaj czynności',
            'description' => 'Opis',
            'id_customer_fk' => 'Klient',
            'id_case_fk' => 'Sprawa',
            'id_order_fk' => 'Zlecenie',
            'cost_net' => 'netto',
            'cost_gros' => 'brutto',
            'cost_vat' => 'stawka VAT',
            'cost_description' => 'opis',
            'cost_currency_id' => 'waluta',
            'id_employee_fk' => 'Pracownik',
            'id_department_fk' => 'Dział',
            'id_event_fk' => 'Zdarzenie kancelaryjne', 
            'timeH' => 'Liczba godzin',
            'timeM' => 'Liczba minut',
        ];
    }

    public static function listTypes() {
        $types = [];
        /*$items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => $type])->all();
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;*/
        $types[1] = 'na rzecz klienta';
        $types[2] = 'administracyjna';
        
        return $types;
    }
        
    public static function categories1($id) {
        $types = [];
        $items = \backend\Modules\Accounting\models\AccService::find()->where( ['status' => 1, 'type_fk' => $id])->orderby('name')->all();
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        
        return $types;
    }
    
    public static function categories2($id) { 
        $types = [];
        $items = \backend\Modules\Accounting\models\AccDictionary::find()->where( ['status' => 1, 'type_fk' => $id])->orderby('name')->all();
        
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public function saveAction()
    {            
        if( $this->type == 1) {
            $new = new AccActions();
            //$new->name = \backend\Modules\Accounting\models\AccService::findOne($this->name_1)['name'];
            $new->name = \backend\Modules\Accounting\models\AccService::listTypes('advanced')[$this->method_1];
            $new->type_fk = \backend\Modules\Accounting\models\AccService::findOne($this->method_1)->type_fk;
            $new->id_service_fk = $this->method_1;
            $new->id_employee_fk= $this->id_employee_fk;
            $new->id_department_fk = $this->id_department_fk;
            $new->id_customer_fk= $this->id_customer_fk;
            $new->id_order_fk= $this->id_order_fk;
            $new->id_set_fk= $this->id_case_fk;
            $new->id_event_fk= $this->id_event_fk;
            $new->id_task_fk= $this->id_task_fk;
            $new->action_date = $this->timeDate;
            //$new->unit_time = $this->timeH*60 + $this->timeM;
			$new->timeH = $this->timeH;
			$new->timeM = $this->timeM;
            $new->description = $this->description;
            $new->acc_cost_net = $this->cost_net;
            $new->acc_cost_description = $this->cost_description;
            $new->acc_cost_currency_id = $this->cost_currency_id;
            $new->id_meeting_fk = $this->id_meeting_fk;
            
            if($new->save()) $this->id = $new->id;
            
            if($this->id_meeting_fk) {
                $member = \backend\Modules\Community\models\ComMeetingMember::findOne($this->id_meeting_fk);
                $member->id_action_fk = $new->id;
                $member->save();
            }

        } else {
            $dict = \backend\Modules\Accounting\models\AccDictionary::findOne($this->method_2);
            
            $new = new CalTodo();
            //$new->name = $dict['name'];
            $new->name = $dict->name;
            $new->type_fk = $dict->type_fk;
            $new->id_dict_todo_type_fk = $this->method_2;
            $new->todo_date = $this->timeDate;
            $new->timeH = $this->timeH;
            $new->timeM = $this->timeM;
            $new->description = $this->description;
            $new->id_employee_fk= $this->id_employee_fk;
            $new->id_priority_fk = 2;
            $new->is_close = 0;
            $new->todo_show = 0;
            $new->id_meeting_fk = $this->id_meeting_fk;
            
            if($new->save()) $this->id = $new->id;
            
            if($this->id_meeting_fk) {
                $member = \backend\Modules\Community\models\ComMeetingMember::findOne($this->id_meeting_fk);
                $member->id_task_fk = $new->id;
                $member->save();
            }
        }
        return true;
    }
}
