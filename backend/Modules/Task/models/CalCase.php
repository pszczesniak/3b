<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Task\models\CalCaseArch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CalCaseDetails;

/**
 * This is the model class for table "{{%cal_case}}".
 *
 * @property integer $id
 * @property integer $type_fk => 1 - sprawa, 2 - projekt
 * @property string $name
 * @property string $description
 * @property integer $update_for_all
 * @property integer $id_dict_case_type_fk
 * @property integer $id_dict_case_category_fk
 * @property integer $id_dict_case_status_fk
 * @property integer $id_customer_fk
 * @property integer $id_company_fk
 * @property integer $id_company_branch_fk
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CalCase extends \yii\db\ActiveRecord
{
    const SCENARIO_MODIFY = 'modify';
    const SCENARIO_DELETE = 'delete';
    const SCENARIO_STATUS = 'status';
	const SCENARIO_MERGE = 'merge';
    const SCENARIO_ORDER = 'order';
	const SCENARIO_STATE = 'state';
	const SCENARIO_ARCHIVE = 'archive';
    const SCENARIO_SHOW = 'show';
    
    public $is_ignore = 0;
    
    public $user_action;
    public $arch = 0;
    public $id_employee_fk;
    public $id_departments_fk;
    public $departments_list;
    public $files_list;
    public $correspondence_list;
    public $employees_list;
	public $cases_list;
	public $id_merge_fk = 0;
    
    public $employees_rel = [];
    public $departments_rel = [];
    
    public $authority_carrying_name;
    public $authority_carrying_contact;
    public $authority_carrying_sygn;
    
    public $authority_supervisory_name;
    public $authority_supervisory_contact;
    public $authority_supervisory_sygn;
    
    public $address_type_fk;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_case}}';
    }

    /** 
     * @inheritdoc
     */
    public function rules()  {
        return [
            [['id_dict_case_status_fk', /*'id_customer_fk',*/ 'type_fk', 'name'/*, 'id_company_branch_fk', 'id_department_fk'*/], 'required'],
			//[['id_company_branch_fk', 'id_department_fk'], 'required', 'when' => function($model) { return (\Yii::$app->params['env'] == 'dev'); }],
            [['description', 'custom_data'], 'string'],
            [['notification_email'], 'boolean'],
			[['description', 'departments_list','employees_list', 'cases_list', 'a_instance_sygn', 'a_instance_name', 'a_instance_contact', 'a_instance_department', 'filing_date'], 'safe'],
            [['update_for_all', 'id_dict_case_type_fk', 'id_dict_case_category_fk', 'id_dict_case_status_fk', 'id_customer_fk', 'id_company_fk', 'id_company_branch_fk', 'id_department_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 
              'type_fk', 'id_customer_person_leading_fk', 'id_employee_leading_fk', 'id_employee_fk', 'is_ignore', 'id_parent_fk', 'id_merge_fk', 'customer_role', 'id_opposite_side_fk', 'id_order_fk', 'a_instance_address_fk', 'no_label_rank', 'show_client'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'close_date', 'close_user', 'archive_date', 'archive_user', 
			  'authority_carrying', 'authority_carrying_name', 'authority_carrying_contact', 'authority_carrying_sygn', 'authority_supervisory', 'authority_supervisory_name', 'authority_supervisory_contact', 'authority_supervisory_sygn', 'employees_rel', 'departments_rel'], 'safe'],
            [['name', 'subname'], 'string', 'max' => 300],
            [['no_label', 'no_label_arch'], 'string', 'max' => 50],
            ['name', 'unique', 'filter' => ['status' => 1]],
           // ['id_departments_fk', 'required', 'message' => 'Musisz przypisać przynajmniej jedn dział',  'on' => self::SCENARIO_MODIFY],
           // ['id_employee_fk', 'required', 'message' => 'Musisz przypisać przynajmniej jednego pracownika', 'on' => self::SCENARIO_MODIFY],
            ['id_customer_fk', 'checkCorrespondence', 'when' => function($model) { return ($model->is_ignore == 0 && $model->status == 1); },  'on' => self::SCENARIO_MODIFY ]
        ];
    }
    
    public function checkCorrespondence($attribute, $params)  {
		//$unchecked = \backend\Modules\Correspondence\models\Correspondence::find()->where(['status' => 1, 'id_case_fk' => $this->id])->all();
        $unchecked = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['status' => 1, 'id_case_fk' => $this->id])->andWhere('id_task_fk = -1')->all();
                
        if(count($unchecked) > 0) {
            $temp = [];
            //foreach($unchecked as $key => $value) array_push($temp, $value->employee['fullname']);
            $this->addError($attribute, 'Do tej sprawy przypisana jest korespondencja. Jeśli chcesz zmienić klienta '
                                        .' to zaznacz tutaj <input type="checkbox" value="1" name="CalCase[is_ignore]" id="CalCase-is_ignore"> i ponownie zapisz zmiany. Operacja ta spowoduje automatyczna zmianę klienta w korespondencji przypisanej do tej sprawy.');
        }
    }

    public function scenarios()  {
        $scenarios = parent::scenarios();
		$scenarios['default'] = ['employees_list', 'cases_list'];
        $scenarios[self::SCENARIO_MODIFY] = ['show_client', 'name', 'description', 'id_dict_case_status_fk', 'id_customer_fk', 'type_fk', 'id_department_fk', 'id_employee_fk', 'id_customer_person_leading_fk', 'id_employee_leading_fk', 
                                             'notification_email', 'authority_carrying', 'authority_carrying_name', 'authority_carrying_contact', 'authority_carrying_sygn', 'authority_supervisory', 'authority_supervisory_name', 'authority_supervisory_contact', 
                                             'authority_supervisory_sygn', 'departments_list', 'is_ignore', 'customer_role', 'id_opposite_side_fk', 'no_label', 'no_label_arch', 'no_label_rank', 'id_company_branch_fk', 'id_departments_fk', 'subname', 'id_order_fk',
                                             'a_instance_sygn', 'a_instance_name', 'a_instance_address_fk', 'a_instance_contact', 'id_dict_case_category_fk', 'id_dict_case_type_fk', 'filing_date'];
        $scenarios[self::SCENARIO_DELETE] = ['deleted_at', 'deleted_by', 'status', 'user_action'];
        $scenarios[self::SCENARIO_STATUS] = ['status', 'user_action'];
		$scenarios[self::SCENARIO_MERGE] = ['name', 'description', 'id_dict_case_status_fk', 'id_customer_fk', 'type_fk', 'id_department_fk', 'id_employee_fk', 'id_customer_person_leading_fk', 'id_employee_leading_fk', 
                                             'notification_email', 'authority_carrying', 'authority_carrying_name', 'authority_carrying_contact', 'authority_carrying_sygn', 'authority_supervisory', 'authority_supervisory_name', 'authority_supervisory_contact', 
                                             'authority_supervisory_sygn', 'departments_list', 'is_ignore', 'id_parent_fk', 'id_merge_fk', 'user_action','customer_role', 'id_opposite_side_fk'];
        $scenarios[self::SCENARIO_ORDER] = ['id_order_fk'];
        $scenarios[self::SCENARIO_SHOW] = ['show_client'];
		$scenarios[self::SCENARIO_STATE] = ['id_dict_case_result_fk', 'close_note', 'id_dict_case_category_fk', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'close_date', 'close_user', 'archive_date', 'archive_user'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()  {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => 'Rodzaj',
			'name' => 'Nazwa',
            'subname' => Yii::t('app', 'Dodatkowy tytuł'),
            'description' => Yii::t('app', 'Description'),
            'update_for_all' => Yii::t('app', 'Update For All'),
            'id_dict_case_type_fk' => Yii::t('app', 'Typ'),
            'id_dict_case_category_fk' => Yii::t('app', 'Stan'),
            'id_dict_case_status_fk' => Yii::t('app', 'Status'),
            'id_dict_case_result_fk' => Yii::t('app', 'Rezultat'),
            'id_customer_fk' => 'Kontrahent',
            'id_company_fk' => Yii::t('app', 'Id Company Fk'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'notification_email' => Yii::t('app', 'Notification Mail'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_employee_fk' => 'Pracownik',
            'id_department_fk' => 'Dział',
            'id_customer_person_leading_fk' => 'Osoba kontaktowa',
            'id_employee_leading_fk' => 'Pracownik prowadzący',
            'authority_carrying' => 'Organ prowadzący',
            'authority_carrying_name' => 'Nazwa',
            'authority_carrying_contact' => 'Kontakt',
            'authority_carrying_sygn' => 'Sygn. akt',
            'authority_supervisory' => 'Organ nadzorujący',
            'authority_supervisory_name' => 'Nazwa',
            'authority_supervisory_contact' => 'Kontakt',
            'authority_supervisory_sygn' => 'Sygn. akt',
            'departments_list' => 'Działy',
            'id_merge_fk' => 'Scal do',
            'customer_role' => 'Rola',
            'id_opposite_side_fk' => 'Strona przeciwna',
            'id_company_branch_fk' => 'Oddział',
            'no_label' => 'Etykieta',
            'id_order_fk' => 'Umowa',
            'a_instance_sygn' => Yii::t('app', 'Sygnatura akt'),
            'a_instance_address_fk' => Yii::t('app', 'Organ prowadzący'),
            'a_instance_department' => Yii::t('app', 'Wydział'),
            'a_instance_contact' => Yii::t('app', 'Kontakt'),
            'a_instance_note' => Yii::t('app', 'Notatka'),
            'close_note' => 'Wyrok/Notatki',
            'show_client' => 'Udostępnij klientowi',
            'employees_list' => 'Pracownicy'
        ];
    }
    
    public function beforeSave($insert) {
		if(Yii::$app->params['env'] != 'dev') {  
			$this->authority_carrying = \yii\helpers\Json::encode(['name' => $this->authority_carrying_name, 'contact' => $this->authority_carrying_contact, 'sygn' => $this->authority_carrying_sygn]);
			$this->authority_supervisory = \yii\helpers\Json::encode(['name' => $this->authority_supervisory_name, 'contact' => $this->authority_supervisory_contact, 'sygn' => $this->authority_supervisory_sygn]);
        } 
		
		if(Yii::$app->params['env'] == 'dev' && $this->user_action == 'create' && $this->type_fk == 1) {
			$this->no_label = self::getLabel($this->id_company_branch_fk, $this->id_department_fk);
			$labelArr = explode('/', $this->no_label);
			$this->no_label_rank = $labelArr[0];
		}  
		
		if(Yii::$app->params['env'] == 'dev' && ($this->user_action == 'archive' || $this->user_action == 'delete') && $this->type_fk == 1) {
			$labelRow = \backend\Modules\Task\models\CaseLabel::find()->where(['id_company_branch_fk' => $this->id_company_branch_fk, 'id_department_fk' => $this->id_department_fk])->one();
			if(!$labelRow) {
				$labelRow = new \backend\Modules\Task\models\CaseLabel();
				$labelRow->type_fk = 1;
				$labelRow->id_company_branch_fk = $this->id_company_branch_fk;
				$labelRow->id_department_fk = $this->id_department_fk;
				if($this->user_action == 'archive' || ($this->user_action == 'delete' && $this->id_dict_case_status_fk != 7) )
					$labelRow->free_numbers = $this->no_label_rank;
				else
					$labelRow->free_numbers_arch = $this->no_label_rank;
			} else {
				if($this->user_action == 'archive' || ($this->user_action == 'delete' && $this->id_dict_case_status_fk != 7) ) {
					$freeNumbers = ($labelRow->free_numbers) ? explode(',', $labelRow->free_numbers) : [];
					array_push($freeNumbers, $this->no_label_rank);
					$labelRow->free_numbers = implode(',', $freeNumbers);
				} else {
					$freeNumbers = ($labelRow->free_numbers_arch) ? explode(',', $labelRow->free_numbers_arch) : [];
					array_push($freeNumbers, $this->no_label_rank);
					$labelRow->free_numbers_arch = implode(',', $freeNumbers);
				}
			}
			if(!$labelRow->save()) { var_dump($labelRow->getErrors()); exit;}
			
			$this->no_label = self::getLabelArch($this->id_company_branch_fk, $this->id_department_fk);
			$labelArr = explode('/', $this->no_label);
			$this->no_label_rank = $labelArr[0];
			
			if($this->user_action == 'archive' && $labelRow->free_numbers_arch) {
				$freeNumbersArch = array_diff( array_map('intval', explode(',', $labelRow->free_numbers_arch)), [$this->no_label_rank] );
				if($freeNumbersArch) $labelRow->free_numbers_arch = implode(',', $freeNumbersArch); else $labelRow->free_numbers_arch = '';
				$labelRow->save();
			}

		}  
		
		//var_dump($this->authority_carrying);exit;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
	
        if (!$insert && $this->status == 1 && $this->type_fk == 1) {
            //$oldAttributes = array_merge($this->getOldAttributes(), $changedAttributes);			
                
            //$changedAttributes['employees'] = $this->employees_rel; 
            $changedAttributes['departments_add'] = [];
            $changedAttributes['departments_del'] = [];
            $departmentsByCase = []; $departmentsByEmployee = [];
            $titleChanges = [];
            if($this->user_action == 'status') {
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                array_push($titleChanges, 'zmiana statusu');
            } else if($this->user_action == 'order') {
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                array_push($titleChanges, (($this->id_order_fk) ? 'dodanie umowy' : 'usunięcie umowy') );
            } else if($this->user_action == 'delete') {
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                array_push($titleChanges, 'usunięcie '.( ($this->type_fk == 1) ? 'sprawy' : 'projektu' ) );
            } else if($this->user_action == 'create') {
                $modelArch = new \backend\Modules\Crm\models\CustomerArch();
                $modelArch->table_fk = 1;
                $modelArch->id_root_fk = $this->id_customer_fk;
                $modelArch->user_action = ($this->type_fk == 1) ? 'matter' : 'project';
                $modelArch->data_change = $this->id;
                $modelArch->save();
				
                if(Yii::$app->params['env'] == 'dev') {
                    $labelRow = \backend\Modules\Task\models\CaseLabel::find()->where(['id_company_branch_fk' => $this->id_company_branch_fk, 'id_department_fk' => $this->id_department_fk])->one();
                    if(!$labelRow) {
                        $labelRow = new \backend\Modules\Task\models\CaseLabel();
                        $labelRow->type_fk = 1;
                        $labelRow->id_company_branch_fk = $this->id_company_branch_fk;
                        $labelRow->id_department_fk = $this->id_department_fk;
                    } else {
                        if($labelRow->free_numbers) {
                            $freeNumbers = array_diff( array_map('intval', explode(',', $labelRow->free_numbers)), [$this->no_label_rank] );
                            if($freeNumbers) $labelRow->free_numbers = implode(',', $freeNumbers); else $labelRow->free_numbers = '';
                        }
                    }
                    $labelRow->next_number = $this->no_label_rank + 1;
                    if(!$labelRow->save()) { var_dump($labelRow->getErrors()); exit;}
                }
            } else {
                if(isset($changedAttributes['id_dict_case_status_fk']) && $changedAttributes['id_dict_case_status_fk'] != $this->id_dict_case_status_fk) {
                    array_push($titleChanges, 'zmiana statusu');
                } 
                if(isset($changedAttributes['id_customer_fk']) && $changedAttributes['id_customer_fk'] != $this->id_customer_fk) {
                    array_push($titleChanges, 'zmiana klienta');
                    if($this->is_ignore == 1) {
                        \backend\Modules\Correspondence\models\Correspondence::updateAll(['id_customer_fk' => $this->id_customer_fk], 'status = 1 and id_customer_fk = '.$changedAttributes['id_customer_fk'].' and id_case_fk = '.$this->id);
                    }
                } 
                if(isset($changedAttributes['name']) && $changedAttributes['name'] != $this->name) {
                    array_push($titleChanges, 'zmiana nazwy');
                } 
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                $changedAttributes['new'] = $this->isNewRecord;
                $employeesByUser = []; $employeesByCase = [];
                $departmentsByEmployee = []; $departmentsByCase = [];
            
                foreach($this->employees as $key => $value) {  array_push($employeesByCase, $value['id_employee_fk']);  }
                           
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
                if($admin || $employee->is_admin) {
                    if($temp = \backend\Modules\Company\models\CompanyDepartment::find()->where(['status' => 1])->all() ) {
                        foreach($temp as $key => $department) {
                            array_push($departmentsByEmployee, $department->id);
                        }
                    }
                } else {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departmentsByEmployee, $department->id_department_fk);
                        }
                    }
                }
                
                $employeesList = \backend\Modules\Company\models\CompanyEmployee::getListByDepartments( implode(',', $departmentsByEmployee) );
                foreach($employeesList as $key => $value) {
                    array_push($employeesByUser, $value->id);
                }
                
                foreach($this->departments as $key => $value) {
                    array_push($departmentsByCase, $value->id_department_fk);
                }
                if(empty($this->departments_list)) $this->departments_list = [];
                foreach($this->departments_list as $key => $value) {
                    if( !in_array($value, $departmentsByCase) ) {
                        array_push($changedAttributes['departments_add'], $value); 
                    }
                }
                
                foreach($departmentsByCase as $key => $value) {
                    if(in_array($value, $departmentsByEmployee) && !in_array($value, $this->departments_list)) {
                        array_push($changedAttributes['departments_del'], $value); 
                    }
                }
              
                foreach($this->employees_rel as $key => $value) {
                    if( in_array($value, $employeesByUser) && !in_array($value, $employeesByCase) ) {
                        array_push($changedAttributes['employees_add'], $value); 
                        $model_ce = new CaseEmployee();
                        $model_ce->id_case_fk = $this->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
                }
                if( count($changedAttributes['employees_add']) > 0)  array_push($titleChanges, 'dodanie pracowników');
                foreach($employeesByCase as $key => $value) {
                    if(in_array($value, $employeesByUser) && !in_array($value, $this->employees_rel)) {
                        array_push($changedAttributes['employees_del'], $value); 
                        CaseEmployee::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id],'id_case_fk = :case and id_employee_fk = '.$value.'', [':case' => $this->id]);
                        TaskEmployee::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id],'id_case_fk = :case and id_employee_fk = '.$value.'', [':case' => $this->id]);
                    }
                }
                if( count($changedAttributes['employees_add']) > 0)  array_push($titleChanges, 'usunięcie pracowników');
                $changedAttributes['employeesUser'] = $employeesByUser;
                $changedAttributes['employeesCase'] = $employeesByCase;
                $changedAttributes['employeesRel'] = $this->employees_rel;
            }
                
            if($this->user_action != 'create' && $this->user_action != 'merge' && $this->user_action != 'scale' && $this->user_action != 'show') {
                $modelArch = new CalCaseArch();
                $modelArch->id_case_fk = $this->id;
                $modelArch->user_action = $this->user_action; //($this->status == -1) ? 'delete' : 'update';
                $modelArch->data_change = \yii\helpers\Json::encode($this);
                $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = new Expression('NOW()');
                if ( $modelArch->save() ){   
                    
                    foreach($changedAttributes['departments_add'] as $key => $value) {
                        $model_cd = new CaseDepartment();
                        $model_cd->id_case_fk = $this->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->id_arch_fk = $modelArch->id;
                        $model_cd->save();
                    }
                    foreach($changedAttributes['departments_del'] as $key => $value) {
                        CaseDepartment::updateAll(['status' => 0, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id, 'id_arch_fk' => $modelArch->id], 'status = 1 and id_case_fk = :case and id_department_fk = '.$value.'', [':case' => $this->id]);
                    }
                    
                    if(count($titleChanges) > 0) {
                        $titleChangesTxt = ' ['.implode(',', $titleChanges).']';
                    } else {
                        $titleChangesTxt = '';
                    }
                }
            }
            
            if($this->user_action == 'create' && ($this->a_instance_sygn || $this->a_instance_address_fk) ) {
                $newInstance = new \backend\Modules\Task\models\CalCaseInstance();
                $newInstance->id_case_fk = $this->id;
                $newInstance->instance_level = 1;
                $newInstance->instance_name = 'I Instancja';
                $newInstance->instance_sygn = $this->a_instance_sygn;
                $newInstance->instance_address_fk = $this->a_instance_address_fk;
                $newInstance->instance_contact = $this->a_instance_contact;
                $newInstance->save();
            }
            
        } 
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public static function getList($id) {
        
        if(Yii::$app->user->identity->username == 'superadmin') {
            if($id == -1) {		
				return CalCase::find()->where(['status' => 1])->orderby('name')->all();
			} else {
				return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->orderby('name')->all();
			}
        } else {
            $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $departments = []; array_push($departments, 0);
           // if($employee->is_admin) {
				if($id == -1) {		
					return CalCase::find()->where(['status' => 1])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
				} else {
					return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
				}
			//} else {
				/*if($id == -1) {		
					return CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$employee->id.')')->orderby('name')->all();
				} else {
					return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$employee->id.')')->orderby('name')->all();
				}*/
                /*foreach($employee->departments as $key => $department) {
                    array_push($departments, $department->id_department_fk);
				}
				if($id == -1) {
					if($employee->id_dict_employee_kind_fk >= 70)
						return CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
					else 
						return CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
				} else {
					//return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
                    if($employee->id_dict_employee_kind_fk >= 70)
						return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
					else 
						return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
				}*/
			//}
        }
    }
    
    public static function getListInstances($id) {

        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        $departments = []; array_push($departments, 0);
        if($id == -1) {		
            //return CalCase::find()->where(['status' => 1])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
            $sql = "select c.type_fk as type_fk, concat_ws('|', c.id, i.id) as id, concat_ws(',', c.name, i.instance_sygn) as name from {{%cal_case}} c left join {{%cal_case_instance}} i on i.id_case_fk = c.id order by name";
        } else {
            //return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
            $sql = "select c.type_fk as type_fk, concat_ws('|', c.id, i.id) as id, concat_ws(',', c.name, i.instance_sygn) as name from {{%cal_case}} c left join {{%cal_case_instance}} i on i.id_case_fk = c.id where c.id_customer_fk = ".$id." order by name";
        }
        
        return \Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getListByEmployee($id, $eid) {
        
        if(Yii::$app->user->identity->username == 'superadmin') {
            if($id == -1) {		
				$cases = CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$eid.')')->orderby('name')->all();
			} else {
				$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$eid.')')->orderby('name')->all();
			}
        } else {
            $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            $departments = []; array_push($departments, 0);
            if($id == -1) {		
                $cases = CalCase::find()->where(['status' => 1])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$eid.')')->all();
            } else {
                $cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$eid.')')->orderby('name')->all();
            }
        }
        
        $options = []; $options['Sprawy'] = []; $options['Projekty'] = [];
        foreach($cases as $key => $item) {
            $type = ($item->type_fk == 1) ? 'Sprawy' : 'Projekty';
            $options[$type][$item->id] = $item->name;
        }
      //  var_dump($options); exit;
        if(count($options['Sprawy']) == 0) unset($options['Sprawy']);
        if(count($options['Projekty']) == 0) unset($options['Projekty']);
        return ($options && $options != NULL) ? ($options) : [];
    }
	
	public static function getGroups($id) {
        $cases = [];
        if(!Yii::$app->user->isGuest) {
			if(Yii::$app->user->identity->username == 'superadmin') {
				if($id == -1) {		
					$cases = CalCase::find()->where(['status' => 1])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
				} else {
					$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
				}
			} else {
				/*$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
				$departments = []; array_push($departments, 0);
				if($employee->is_admin) {*/
					if($id == -1) {		
						$cases = CalCase::find()->where(['status' => 1])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
					} else {
						$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->orderby('name')->all();
					}
				/*} else {
					foreach($employee->departments as $key => $department) {
						array_push($departments, $department->id_department_fk);
					}
					if($id == -1) {
						if($employee->id_dict_employee_kind_fk >= 70)
							$cases = CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
						else 
							$cases = CalCase::find()->where(['status' => 1])->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
					} else {
						//return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
						if($employee->id_dict_employee_kind_fk >= 70)
							$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
						else 
							$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
					}
				}*/
			}
		}
        
        $options = []; $options['Sprawy'] = []; $options['Projekty'] = [];
        foreach($cases as $key => $item) {
            $type = ($item->type_fk == 1) ? 'Sprawy' : 'Projekty';
            $options[$type][$item->id] = $item->name;
        }
      //  var_dump($options); exit;
        if(count($options['Sprawy']) == 0) unset($options['Sprawy']);
        if(count($options['Projekty']) == 0) unset($options['Projekty']);
        return ($options && $options != NULL) ? ($options) : [];
    }
    
    public function getDepartments()  {
		$items = $this->hasMany(\backend\Modules\Task\models\CaseDepartment::className(), ['id_case_fk' => 'id'])->leftJoin('{{%company_department}}', '{{%company_department}}.id={{%case_department}}.id_department_fk')
										->where('{{%case_department}}.status = 1')
										->orderby('{{%company_department}}.name'); 
		return $items;
    }
    
    public function getEmployees()  {
		$items = [];
        
        /*$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
      
        if(!$this->isNewRecord) {
            if($employee && !$employee->is_admin && !$admin)
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')
                              //->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or {{%company_employee}}.id = '.$employee->id.') and {{%case_employee}}.status >= 0 and {{%case_employee}}.is_show = 1')
                              ->where(' {{%case_employee}}.status = 1 and {{%case_employee}}.is_show = 1')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
            else
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')->where(' {{%case_employee}}.status = 1 and {{%case_employee}}.is_show = 1')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
        }*/
        $sql = "select id_employee_fk, id_user_fk, concat_ws(' ',lastname, firstname) as fullname, email, address, phone, is_show, dv.name as typename, time_limit, time_used, ce.id as rel_id"
                ." from {{%case_employee}} ce join {{%company_employee}} e on ce.id_employee_fk = e.id left join {{%dictionary_value}} dv on dv.id = id_dict_employee_type_fk"
                . " where ce.status = 1 and e.status = 1 and is_show = 1 and id_case_fk = ".$this->id;
        $items = Yii::$app->db->createCommand($sql)->queryAll();
		return $items;
    }
    
    public function getEmployeeslock()  {
		$items = [];
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();

        //$employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();  
        
        if(!$this->isNewRecord) {
            if($employee && !$employee->is_admin && !$admin)
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')
                              //->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or {{%company_employee}}.id = '.$employee->id.') and {{%case_employee}}.status >= 0 and {{%case_employee}}.is_show = 1')
                              ->where(' {{%case_employee}}.status = 0 and {{%case_employee}}.is_show = 1')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
            else
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')->where(' {{%case_employee}}.status = 0 and {{%case_employee}}.is_show = 1')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
        }
		return $items;
    }
    
    public function getEmployeesall() {
		$items = [];
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();

        //$employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();  
        
        if(!$this->isNewRecord) {
            if($employee && !$employee->is_admin && !$admin)
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')
                              //->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or {{%company_employee}}.id = '.$employee->id.') and {{%case_employee}}.status >= 0 and {{%case_employee}}.is_show = 1')
                              ->where(' {{%case_employee}}.status = 1 and {{%case_employee}}.is_show = 1')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
            else
                $items = $this->hasMany(\backend\Modules\Task\models\CaseEmployee::className(), ['id_case_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%case_employee}}.id_employee_fk')->where(' {{%case_employee}}.status = 1 ')
                              ->select(['id_case_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 
        }
		return $items;
    }
    
    public function getFiles() {
        $filesData = [];
        //$filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 3])->orderby('id desc')->all();
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1])
                                                 ->andWhere(' ( (id_fk='.$this->id.' and id_type_file_fk = 3) '
                                                            .' or (id_type_file_fk = 4 and id_fk in (select id from {{%cal_task}} where status = 1 and id_case_fk='.$this->id.')) )')->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getBrowser() {
        $filesData = [];
        //$filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 3])->orderby('id desc')->all();
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1])
                                                 ->andWhere(' ( (id_fk='.$this->id.' and id_type_file_fk = 3) '
                                                            .' or (id_type_file_fk = 4 and id_fk in (select id from {{%cal_task}} where status = 1 and id_case_fk='.$this->id.')) )')->orderby('id desc')->all();
        $sql = "select dv.id, dv.name, count(*) as items "
            . " from {{%files}} f join {{%dictionary_value}} dv on dv.id = f.id_dict_type_file_fk "
            . " where (id_fk = ".$this->id." and id_type_file_fk = 3) "
            . " or (id_type_file_fk = 4 and id_fk in (select id from {{%cal_task}} where status = 1 and id_case_fk=".$this->id.")) "
            . " group by dv.id, dv.name order by name";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        return $data;
    }
    
    public function getSfiles() {
        $filesData = [];
        //$filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 3])->orderby('id desc')->all();
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1, 'show_client' => 1])
                                                 ->andWhere(' ( (id_fk='.$this->id.' and id_type_file_fk = 3) '
                                                            .' or (id_type_file_fk = 5 and id_fk in (select c.id from {{%correspondence}} c join {{%correspondence_task}} ct on ct.id_correspondence_fk=c.id where ct.status = 1 and c.status = 1 and ct.id_case_fk='.$this->id.' and ( (type_fk = 1 and send_by is not null and send_at is not null) or type_fk=2) ))'
                                                            .' or (id_type_file_fk = 4 and id_fk in (select id from {{%cal_task}} where status = 1 and id_case_fk='.$this->id.')) )')->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getCorrespondence() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 5])->andWhere('id_fk in (select id from {{%correspondence}} where send_at is not null and send_by is not null and id_case_fk='.$this->id.')')->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getMessages() {
        return \backend\Modules\Community\models\ComMessage::find()->where(['id_set_fk' => $this->id])->all();
    }
    
    public function getCustomer() {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getSide()  {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_opposite_side_fk']);
    }
    
    public function getEvents() {
        return \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $this->id, 'status' => 1])->orderby('created_at desc')->all();
    }
    
    public function getPevents() {
        return \backend\Modules\Task\models\CalTodo::find()->where(['id_set_fk' => $this->id, 'status' => 1])->orderby('created_at desc')->all();
    }
    
    public static function listTypes($type) {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => $type])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
        /*$status[1] = 'Kancelaryjne';
        $status[2] = 'Osobiste';
        
        return $status;*/
    }
    
    public static function listStatus($type) {
        $status = [];
        if( Yii::$app->params['env'] == 'dev' ) {
            $status[1] = 'W toku';
            $status[4] = 'Zakończono';
            $status[7] = 'Archiwalne';
			//$status[6] = 'Scalona';
			//$status[8] = 'Usunięte';
        } else {
            $status[1] = 'Nowa';
            $status[2] = 'Plan';
            $status[3] = 'W toku';
            $status[4] = 'Zakończona';
            if($type == 'advanced') {
                $status[5] = 'Aktywne';
                //$status[6] = 'Scalona';
            }
            if($type == 'merge') {
                $status[5] = 'Aktywne';
            }
		}
        return $status;
    }
    
    public static function listProject($employeeId) {
        $projects = CalCase::find()->where(['status' => 1, 'type_fk' => 2])
                                   ->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$employeeId.')')
                                   //->andWhere('id in (select id_case_fk from {{%cal_case_details}} where time_limit > 0)')
                                   ->all();
        return ($projects) ? $projects : [];
    }
    
    public function getStatusname() {
        return ( isset(self::listStatus('advanced')[$this->id_dict_case_status_fk]) ) ? self::listStatus('advanced')[$this->id_dict_case_status_fk] : 'nieznany';
    }
    
    public function getCategory() {
        $name = 'nieokreślony';
        $category = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id' => $this->id_dict_case_type_fk])->one();
        if($category)  $name = $category->name;
        
        return $name;
    }
	
	public function getState() {
        $name = false;
        $category = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id' => $this->id_dict_case_category_fk])->one();
        if($category)  $name = $category->name;
        
        return $name;
    }
    
    public function getLeadingEmployee() {
        return  \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_leading_fk);
        
    }
	
    public function getLeadingContact() {
        return  \backend\Modules\Crm\models\CustomerPerson::findOne($this->id_customer_person_leading_fk);        
    }
	
	public function getLeader() {
		$ename = '';
		
		if($this->id_employee_leading_fk) {
			$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_leading_fk);
			if($employee) $ename = $employee->fullname;
	    }
		
		return $ename;
	}
    
    public function getNotes() {
        $notesData = \backend\Modules\Task\models\Note::find()->where(['status' => 1, 'id_case_fk' => $this->id, 'id_type_fk' => 1])->orderby('id desc')->all();
        return $notesData;
    }
	
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
    
    public function getDetails() {
        return CalCaseDetails::find()->where(['status' => 1, 'id_case_fk' => $this->id])->one();
    }
    
    public function getInstances() {
        return CalCaseInstance::find()->where(['status' => 1, 'id_case_fk' => $this->id])->all();
    }
    
    public static function customerRoles() {
        return [1 => 'POWÓD', 2 => 'POZWANY'];
    }
    
    public function getRole() {
        $roles = $this->listRoles(true);
        return (isset($roles[$this->customer_role])) ? $roles[$this->customer_role] : 'brak';
    }
    
    public static function listRoles($advanced) {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 12])->orderby('name')->all();
        $types = [];
        foreach($items as $key => $item) { 
            $customData = \yii\helpers\Json::decode($item->custom_data);
            $types[$item->id] = ($customData['color'] && $advanced) ? '<span style="color: '.$customData['color'].'">'.$item->name.'</span>' : $item->name;
        }
        return $types;
    }    
    
    public static function resultTypes() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 14])->all();
        $roles = [];
        foreach($items as $key => $item) { 
            $roles[$item->id] = $item->name;
        }
        return $roles;
    }
	
	public function getCases() {
		return \backend\Modules\Task\models\ProjectCase::find()->where(['status' => 1, 'id_project_fk' => $this->id])->all();
	}
	
	public function getPresentation() {
		return isset(self::Viewer()[$this->id_dict_case_status_fk]) ? self::Viewer()[$this->id_dict_case_status_fk] : self::Viewer()[1];
	}
	
	public static function Viewer() {
		$dictTabs = [
						1 => ['label' => 'w toku', 'color' => 'blue', 'icon' => 'institution'],
						4 => ['label' => 'zakończona', 'color' => 'green', 'icon' => 'check'],
						7 => ['label' => 'archiwalna', 'color' => 'purple', 'icon' => 'archive'],
						6 => ['label' => 'scalona', 'color' => 'pink', 'icon' => 'object-group']
				    ];
		return $dictTabs;
	}
	
	public static function getLabel($branchId, $departmentId) {			
		$branch = \backend\Modules\Company\models\CompanyBranch::findOne($branchId);
		$department = \backend\Modules\Company\models\CompanyDepartment::findOne($departmentId);
		
		$no = 0;
			
		$freeLabels = \backend\Modules\Task\models\CaseLabel::find()->where(['id_company_branch_fk' => $branchId, 'id_department_fk' => $departmentId])->one();
		if($freeLabels) {
			if($freeLabels->free_numbers) {
				$no = min(array_map('intval', explode(',', $freeLabels->free_numbers)));
			} 
		} else {
            $no = CalCase::find()->where(['status' => 1, 'id_company_branch_fk' => $branchId, 'id_department_fk' => $departmentId])->andWhere('id_dict_case_status_fk != 7')->count() + 1;
        }
		
		$label = $no.'/'.$department->symbol.'/'.$branch->symbol;
		
		return $label;
	}
	
	public static function getLabelArch($branchId, $departmentId) {
		$branch = \backend\Modules\Company\models\CompanyBranch::findOne($branchId);
		$department = \backend\Modules\Company\models\CompanyDepartment::findOne($departmentId);
		
		$no = 0;
			
		$freeLabels = \backend\Modules\Task\models\CaseLabel::find()->where(['id_company_branch_fk' => $branchId, 'id_department_fk' => $departmentId])->one();
		if($freeLabels) {
			if($freeLabels->free_numbers_arch) {
				$no = min(array_map('intval', explode(',', $freeLabels->free_numbers_arch)));
			} else {
				$no = CalCase::find()->where(['status' => 1, 'id_company_branch_fk' => $branchId, 'id_department_fk' => $departmentId, 'id_dict_case_status_fk' => 7])->count() + 1;
			}
		}

		$label = $no.'/'.$department->symbol.'/'.$branch->symbol.'/ARCH';
		
		return $label;
	}
    
    public static function getListForClient() {
        $cid = (\Yii::$app->session->get('user.cid')) ? \Yii::$app->session->get('user.cid') : 0;
        
        return CalCase::find()->where(['status' => 1, 'show_client' => 1, 'id_customer_fk' => $cid])->all();
    }
	
	public static function getProjects($id) {
		return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
	}
}
