<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%case_department}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $id_department_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CaseDepartment extends \yii\db\ActiveRecord
{
    
    public $name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'id_department_fk'], 'required'],
            [['id_case_fk', 'id_department_fk', 'created_by', 'deleted_by', 'status'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public function getDepartment()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyDepartment::className(), ['id' => 'id_department_fk']);
    }
}
