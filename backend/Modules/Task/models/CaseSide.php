<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%case_side}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $id_side_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CaseSide extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_side}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'id_side_fk', 'id_dict_side_role_fk', 'type_fk'], 'required'],
            [['id_case_fk', 'id_event_fk', 'id_side_fk', 'created_by', 'deleted_by', 'status'], 'integer'],
            [['created_at', 'deleted_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_dict_side_role_fk' => 'Rola',
            'type_fk' => 'Po stronie',
            'id_case_fk' => Yii::t('app', 'Postępowanie'),
            'id_event_fk' => Yii::t('app', 'Zdarzenie'),
            'id_side_fk' => Yii::t('app', 'Uczestnik'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function delete() {
		
        $this->status = -1;
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		
		return $result;
	}
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public function getEvent()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalTask::className(), ['id' => 'id_event_fk']);
    }
    
    public function getSide()
    {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_side_fk']);
    }
    
    public static function sideTypes() {
        return [1 => 'klienta', 2 => 'strony przeciwnej'];
    } 
    
    public static function sideRoles() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 11])->all();
        $roles = [];
        foreach($items as $key => $item) { 
            $roles[$item->id] = $item->name;
        }
        return $roles;
    }
}
