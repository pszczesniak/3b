<?php

namespace frontend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\base\Model;

use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseArch;

/**
 * GroupAction is the model behind the contact form.
 */
class GroupAction extends Model
{
    public $ids; 
    public $departments_list;
    public $employees_list;
	public $resultDict;
	public $resultNote;
	public $user_action;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ids'], 'required'],
            ['departments_list', 'required', 'when' => function($model) { return ($model->user_action == 'addEmployees'); }, 'message' => 'Proszę wybrać działy' ],
			['employees_list', 'required', 'when' => function($model) { return ($model->user_action == 'addEmployees' || $model->user_action == 'delEmployees'); }, 'message' => 'Proszę wybrać pracowników' ],
            //[['hours_of_day', 'id_employee_fk'], 'integer'],
            [['ids', 'departments_list', 'employees_list', 'resultDict', 'resultNote'], 'safe'],
            //['date_to','validateDates'],
        ];
    }
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Proszę podać poprawną datę końca');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids' => 'Sprawy',
            'departments_list' => 'Działy',
            'employees_list' => 'Pracownicy',
			'resultDict' => 'Rezultat',
			'resultNote' => 'Wyrok / Wnioski'
        ];
    }

    public function addEmployees() {            
        $cases = [];
		
		/* departments */
        $sql = "select c.id as cid,c.name, d.id as did, d.name "
				." from {{%cal_case}} c join {{%company_department}} d on c.id in (".$this->ids.") and d.id in(".implode(',', $this->departments_list).") "
				." where (c.id,d.id) not in (select id_case_fk,id_department_fk from law_case_department) order by c.id";
		$relData = \Yii::$app->db->createCommand($sql)->query();
        foreach($relData as $key => $value) {
			//array_push($cases[$value['cid']], $value['eid']);
			$sqlInsert = "insert into {{%case_department}} (id_case_fk, id_department_fk, status, created_at, created_by) "
			          ." values (".$value['cid'].", ".$value['did'].", 1, now(), ".\Yii::$app->user->id.")";
		    \Yii::$app->db->createCommand($sqlInsert)->execute();
            
            $sqlMangers = "select group_concat(id_employee_manager_fk) as managers from {{%company_department}} where id in(".implode(',', $this->departments_list).") ";
            $managers = \Yii::$app->db->createCommand($sqlMangers)->queryOne();
		}    
        
        $employeesAll = implode(',', $this->employees_list);
        if($managers['managers']) {
            $employeesAll .= ', '.$managers['managers'];
        }
        /* employees */
        $sql = "select c.id as cid,c.name, e.id as eid, e.lastname "
				." from {{%cal_case}} c join {{%company_employee}} e on c.id in (".$this->ids.") and e.id in(".$employeesAll.") "
				." where (c.id,e.id) not in (select id_case_fk,id_employee_fk from law_case_employee) order by c.id";
		$relData = \Yii::$app->db->createCommand($sql)->query();
        foreach($relData as $key => $value) {
			//array_push($cases[$value['cid']], $value['eid']);
			$sqlInsert = "insert into {{%case_employee}} (id_case_fk, id_employee_fk, status, created_at, created_by) "
			          ." values (".$value['cid'].", ".$value['eid'].", 1, now(), ".\Yii::$app->user->id.")";
		    \Yii::$app->db->createCommand($sqlInsert)->execute();
		}    
		
		/*foreach($cases as $key => $value) {
			$modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $key;
			$modelArch->user_action = 'addEmployee';
			$modelArch->data_change = \yii\helpers\Json::encode($this);
			$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
		}*/
		
        return true;
    }
	
	public function delEmployees() {            
        $cases = [];

		//array_push($cases[$value['cid']], $value['eid']);
		$sqlUpdate = "update {{%case_employee}} set status = -1, deleted_at = now(),  deleted_by = ".\Yii::$app->user->id
				  ." where id_case_fk in (".$this->ids.") and id_employee_fk in(".implode(',', $this->employees_list).") ";
		\Yii::$app->db->createCommand($sqlUpdate)->execute();   
		
		/*foreach($cases as $key => $value) {
			$modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $key;
			$modelArch->user_action = 'addEmployee';
			$modelArch->data_change = \yii\helpers\Json::encode($this);
			$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
		}*/
		
        return true;
    }
	
	public function closeCases() {            
        $cases = \Yii::$app->db->createCommand("SELECT group_concat(id) as ids FROM {{%cal_case}} where id_dict_case_status_fk < 4 and status = 1 and id in (".$this->ids.")")->queryOne();
        
		$this->resultDict = ($this->resultDict) ? $this->resultDict : 0;
		$this->resultNote = ($this->resultNote) ? $this->resultNote : '';
		
		if($cases && $cases['ids']) {
			$sqlUpdate = "update {{%cal_case}} set id_dict_case_status_fk = 4, close_date = now(), close_user = ".\Yii::$app->user->id
											   .", id_dict_case_result_fk = ".$this->resultDict.", close_note = '".$this->resultNote."'"
					  ." where id in (".$cases['ids'].")";
			\Yii::$app->db->createCommand($sqlUpdate)->execute();   
		}
		
		foreach($cases as $key => $value) {
			$modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $value;
			$modelArch->user_action = 'close';
			//$modelArch->data_change = \yii\helpers\Json::encode($this);
			//$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
			$modelArch->save();
		}
		
        return true;
    }
	
	public function archiveCases() {            
        $cases = \Yii::$app->db->createCommand("SELECT group_concat(id) as ids FROM {{%cal_case}} where id_dict_case_status_fk != 7 and status = 1 and id in (".$this->ids.")")->queryOne();
		
		foreach($cases as $key => $value) {
			$model = CalCase::findOne($value);
			$model->scenario = 'state';
			$model->user_action = 'archive';
			$model->id_dict_case_status_fk = 7;
			$model->save();
		}
		
        return true;
    }
}
