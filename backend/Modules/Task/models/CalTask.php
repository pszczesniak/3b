<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Task\models\CalTaskArch;
use backend\Modules\Task\models\TaskDepartments;
use backend\Modules\Company\models\CompanyResourcesSchedule;
/**
 * This is the model class for table "{{%cal_task}}".
 *
 * @property integer $id
 * @property integer $type_fk => 1: rozprawa, 2: zadanie
 * @property string $name
 * @property string $place
 * @property string $description
 * @property string $date_from
 * @property string $date_to
 * @property integer $all_day
 * @property integer $update_for_all
 * @property integer $id_dict_task_type_fk
 * @property integer $id_dict_task_category_fk
 * @property integer $id_dict_task_status_fk
 * @property string $reminder_date
 * @property integer $id_reminder_type_fk
 * @property integer $reminded
 * @property string $period
 * @property string $period_end
 * @property integer $period_parent
 * @property integer $id_customer_fk
 * @property integer $id_case_fk
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CalTask extends \yii\db\ActiveRecord
{
    const SCENARIO_MODIFY = 'modify';
    const SCENARIO_DELETE = 'delete';
    const SCENARIO_CLOSE = 'close';
    const SCENARIO_OPEN = 'open';
    const SCENARIO_REMINDER = 'reminder';
    const SCENARIO_EVENT = 'event';
    const SCENARIO_CALENDAR = 'calendar';
    const SCENARIO_GOOGLE = 'google';
    const SCENARIO_SHOW = 'show';
    
    public $user_action;
    public $id_department_fk;
    public $id_employee_fk;
    public $departments_list;
    public $files_list;
    public $correspondence_list;
    public $resources_list;
    public $employees_list;
    public $employees_rel = [];
    
    public $filter_status = 1;
    
    public $timeH;
    public $timeM;
    
    public $fromDate;
    public $fromTime;
    public $toDate;
    public $toTime;
    
    public $counter;
    
    public $is_ignore = 0;
    
   /* public $event_date;
    public $event_time;
    public $execution_time;*/
    //public $delay;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', /*'id_customer_fk', 'id_case_fk',*/ 'id_dict_task_type_fk', 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'type_fk', 'date_from', 'date_to'], 'required', 'on' => self::SCENARIO_MODIFY],
            [['name', /*'id_customer_fk', 'id_case_fk',*/ 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'type_fk', 'event_date'], 'required', 'on' => self::SCENARIO_EVENT],
           // [['reminded', 'id_reminder_type_fk', 'reminder_date', 'reminder_user'], 'required', 'on' => self::SCENARIO_REMINDER],
            [['description', 'custom_data', 'event_time', 'fromDate', 'fromTime', 'toDate', 'toTime', 'g_id'], 'string'],
            [['date_from', 'date_to', 'reminder_date', 'event_deadline', 'period_end', 'created_at', 'updated_at', 'deleted_at', 'id_reminder_type_fk'], 'safe'],
            [['id_dict_task_type_fk', 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'reminded', 'period_parent', 'id_customer_fk', 'id_case_fk', 'id_instance_fk', 
              'show_client', 'all_day', 'update_for_all', 'status', 'created_by', 'updated_by', 'deleted_by', 'type_fk', 'id_employee_fk','filter_status', 'execution_time', 'timeH', 'timeM', 'g_action', 'counter', 'id_company_branch_fk', 'id_customer_branch_fk'], 'integer'],
            [['name'], 'string', 'max' => 300],
            //['name', 'unique', 'filter' => ['status' => 1]],
			[['notification_email'], 'boolean'],
            [['place', 'period'], 'string', 'max' => 500],
			[['description', 'departments_list', 'resources_list'], 'safe'],
            ['id_employee_fk', 'required', 'message' => 'Musisz przypisać przynajmniej jednego pracownika',  'on' => self::SCENARIO_MODIFY],
            ['id_employee_fk', 'required', 'message' => 'Musisz przypisać przynajmniej jednego pracownika',  'on' => self::SCENARIO_EVENT],
            //['date_from','validateDates',  'on' => self::SCENARIO_EVENT],
            ['date_from','validateDates',  'on' => self::SCENARIO_CALENDAR],
            ['event_time', 'required', 'message' => 'Proszę podać godzinę rozprawy', 'when' => function($model) { return ($model->type_fk == 1); },  'on' => self::SCENARIO_EVENT],
            //['event_deadline','validateDeadline',  'on' => self::SCENARIO_EVENT, 'when' => function($model) { return ($model->type_fk == 2); }],
            [['date_from', 'date_to'], 'required', 'on' => self::SCENARIO_CALENDAR],
            ['id_case_fk', 'checkCorrespondence', 'when' => function($model) { return ($model->is_ignore == 0 && $model->status == 1); },  'on' => self::SCENARIO_EVENT ],
           // [['deleted_by', 'deleted_at', 'user_action'], 'required', 'on' => self::SCENARIO_DELETE],
        ];
    }
    
    public function checkCorrespondence($attribute, $params)  {
		$unchecked = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['id_task_fk' => $this->id])->all();
                
        if(count($unchecked) > 0) {
            $temp = [];
            //foreach($unchecked as $key => $value) array_push($temp, $value->employee['fullname']);
            $this->addError($attribute, 'Nie można zmieć sprawy/projektu w tym zdarzeniu, ponieważ przez obecną sprawę/projekt jest ono powiązane z korespondencją.');
        }

    }
	
	public function scenarios()  {
        $scenarios = parent::scenarios();
		$scenarios['default'] = [];
        $scenarios[self::SCENARIO_MODIFY] = ['name', 'description', 'id_customer_fk', 'id_customer_branch_fk', 'id_case_fk', 'id_instance_fk', 'id_dict_task_type_fk', 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'type_fk', 
                                             'date_from', 'date_to', 'id_employee_fk', 'fromDate', 'fromTime', 'toDate', 'toTime', 'all_day', 'show_client',
                                             'place', 'description', 'notification_email', 'reminded', 'id_reminder_type_fk', 'reminder_date', 'reminder_user', 'departments_list', 'id_company_branch_fk'];
        $scenarios[self::SCENARIO_DELETE] = ['deleted_at', 'deleted_by', 'status', 'user_action'];
        $scenarios[self::SCENARIO_CLOSE] = ['id_dict_task_status_fk', 'user_action'];
        $scenarios[self::SCENARIO_OPEN] = ['id_dict_task_status_fk', 'user_action'];
        $scenarios[self::SCENARIO_REMINDER] = ['reminded', 'id_reminder_type_fk', 'reminder_date', 'reminder_user', 'user_action'];
        $scenarios[self::SCENARIO_EVENT] = ['name', 'description', 'id_customer_fk', 'id_customer_branch_fk', 'id_case_fk', 'id_instance_fk', 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'type_fk', 'id_dict_task_type_fk',
                                            'date_from', 'date_to', 'fromDate', 'fromTime', 'toDate', 'toTime', 'all_day', 'timeH', 'timeM', 'show_client',
                                            'id_employee_fk', 'place', 'description', 'reminded', 'id_reminder_type_fk', 'reminder_date', 'reminder_user', 'event_date', 'event_time', 'event_deadline', 'execution_time', 'departments_list', 'resources_list'];
        $scenarios[self::SCENARIO_CALENDAR] = ['fromDate', 'fromTime', 'toDate', 'toTime', 'date_from', 'date_to', 'all_day'];
        $scenarios[self::SCENARIO_GOOGLE] = ['g_id', 'g_action'];
        $scenarios[self::SCENARIO_SHOW] = ['show_client'];
        
        return $scenarios;
    }
    
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Wprowadź poprawną datę końca');
		}
	}
    
    public function validateDeadline(){
        if($this->event_deadline) {
            $eventDate = $this->event_date.' '. ( ($this->event_time) ? $this->event_time : '00:00') .':00';
            if(strtotime($this->event_deadline.':00') < strtotime($eventDate)){
                //$this->addError('date_from','Please give correct Start and End dates');
                $this->addError('event_deadline','Deadline nie może być wczesniejszy niż termin');
            } 
        }
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => 'Rodzaj',
            'name' => Yii::t('app', 'Title'),
            'place' => Yii::t('app', 'Miejsce'),
            'description' => Yii::t('app', 'Opis'),
            'date_from' => Yii::t('app', 'Start'),
            'date_to' => Yii::t('app', 'Koniec'),
            'todo_date' => Yii::t('app', 'Todo Date'),
            'todo_time' => Yii::t('app', 'Todo Time'),
            'execution_time' => Yii::t('app', 'Czas [w min]'),
            'all_day' => Yii::t('app', 'Cały dzień'),
            'update_for_all' => Yii::t('app', 'Update For All'),
            'id_dict_task_type_fk' => Yii::t('app', 'Typ'),
            'id_dict_task_category_fk' => Yii::t('app', 'Waga'),
            'id_dict_task_status_fk' => Yii::t('app', 'Status'),
            'reminder_date' => Yii::t('app', 'Reminder Date'),
            'id_reminder_type_fk' => Yii::t('app', 'Przypomnienie'),
            'reminded' => Yii::t('app', 'Reminded'),
            'period' => Yii::t('app', 'Period'),
            'period_end' => Yii::t('app', 'Period End'),
            'period_parent' => Yii::t('app', 'Period Parent'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_case_fk' => Yii::t('app', 'Projekt'),
            'id_instance_fk' => 'Postępowanie',
            'id_event_fk' => Yii::t('app', 'Zdarzenie'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'delay' => 'Opóźnienie',
            'id_employee_fk' => 'Pracownik',
			'id_department_fk' => 'Dział',
            'notification_email' => Yii::t('app', 'Notification Mail'),
            'filter_status' => 'Nie wyświetlaj zakończonych',
            'event_deadline' => 'Deadline',
            'id_company_branch_fk' => 'Oddział',
            'show_client' => 'Udostępnij klientowi',
            'id_customer_branch_fk' => 'Oddział',
            'employees_list' => 'Pracownicy',
            'departments_list' => 'Działy'
        ];
    }
    
    public function beforeSave($insert) {
		
        $H = ($this->timeH) ? $this->timeH*60 : 0;
        $m = ($this->timeM) ? $this->timeM : 0;
        $this->execution_time = $H + $m;
        
        if($this->status != -2) {
            if($this->type_fk == 2 && !$this->event_deadline) {
                if($this->event_time)
                    $this->event_deadline = $this->event_date.' '.$this->event_time.':00';
                else
                    $this->event_deadline = $this->event_date.' 15:30:00';
            }
        }
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
			/*if($this->id_reminder_type_fk == 0) {
                $this->reminded = 0;
            } else {
                $this->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $this->id_reminder_type_fk, 'dateTimestamp' => $this->event_deadline]);
                
                $this->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
            }	*/		
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        $employee = false;
        $changedAttributes['departments_add'] = [];
        $changedAttributes['departments_del'] = [];
        $departmentsByTask = []; $departmentsByEmployee = [];
        if (!$insert && $this->status == 1) {
            //$oldAttributes = array_merge($this->getOldAttributes(), $changedAttributes);			
            $titleChanges = [];
            if($this->user_action == 'close' || $this->user_action == 'open' || $this->user_action == 'reminder' || $this->user_action == 'cancel' || $this->user_action == 'correspondence-case-change') {
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                array_push($titleChanges, 'zmiana statusu');
            } else if($this->user_action == 'delete') {
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                array_push($titleChanges, 'usunięcie zdarzenia');
            } else if($this->user_action == 'create') {
                $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $this->id_case_fk;
                $modelArch->user_action = ($this->type_fk == 1) ? 'case' : 'event';
                $modelArch->data_change = $this->id;
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = new Expression('NOW()');
                $modelArch->save();
            } else {
                if(isset($changedAttributes['id_dict_task_status_fk']) && $changedAttributes['id_dict_task_status_fk'] != $this->id_dict_task_status_fk) {
                    array_push($titleChanges, 'zmiana statusu');
                } 
                if(isset($changedAttributes['id_customer_fk']) && $changedAttributes['id_customer_fk'] != $this->id_customer_fk) {
                    array_push($titleChanges, 'zmiana klienta');
                } 
                if(isset($changedAttributes['id_case_fk']) && $changedAttributes['id_case_fk'] != $this->id_case_fk) {
                    array_push($titleChanges, 'zmiana sprawy');
                } 
                if(isset($changedAttributes['name']) && $changedAttributes['name'] != $this->name) {
                    array_push($titleChanges, 'zmiana nazwy');
                } 
                if(isset($changedAttributes['date_from']) && date('Y-m-d H:i', strtotime($changedAttributes['date_from'])) != date('Y-m-d H:i', strtotime($this->date_from))) {
                    array_push($titleChanges, 'zmiana daty startu');
                } 
                if(isset($changedAttributes['date_to']) && date('Y-m-d H:i', strtotime($changedAttributes['date_to'])) != date('Y-m-d H:i', strtotime($this->date_to))) {
                    array_push($titleChanges, 'zmiana terminu realizacji');
                } 
                //$changedAttributes['employees'] = $this->employees_rel; 
                $changedAttributes['employees_add'] = [];
                $changedAttributes['employees_del'] = [];
                $employeesByUser = []; $employeesByTask = [];
                
                foreach($this->employees as $key => $value) {
                    array_push($employeesByTask, $value['id_employee_fk']);
                }
                
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
                //if($admin || $employee->is_admin) { } 
                                  
                foreach($this->employees_rel as $key => $value) {
                    if(/*in_array($value, $employeesByUser) &&*/ !in_array($value, $employeesByTask)) {
                        array_push($changedAttributes['employees_add'], $value); 
                        $model_ce = new TaskEmployee();
                        $model_ce->id_case_fk = $this->id_case_fk;
                        $model_ce->id_task_fk = $this->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->status = 1;
                        $model_ce->save();
                    }
                }
                if( count($changedAttributes['employees_add']) > 0)  array_push($titleChanges, 'dodanie pracowników');
                foreach($employeesByTask as $key => $value) {
                    if( !in_array($value, $this->employees_rel) ) {
                        array_push($changedAttributes['employees_del'], $value); 
                        //TaskEmployee::deleteAll('id_task_fk = :task and id_employee_fk = '.$value.'', [':task' => $this->id]);
                        TaskEmployee::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id],'id_task_fk = :task and id_employee_fk = '.$value.'', [':task' => $this->id]);
                    }
                }
                if( count($changedAttributes['employees_del']) > 0)  array_push($titleChanges, 'usunięcie pracowników');
                
                $changedAttributes['employeesUser'] = $employeesByUser;
                $changedAttributes['employeesTask'] = $employeesByTask;
                $changedAttributes['employeesRel'] = $this->employees_rel;
            }
            if($this->user_action != 'create') {
                $modelArch = new CalTaskArch();
                $modelArch->id_task_fk = $this->id;
                $modelArch->id_case_fk = $this->id_case_fk;
                if($this->user_action) {
                    $modelArch->user_action = $this->user_action;
                } else {
                    $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
                }
                
                $modelArch->data_change = \yii\helpers\Json::encode($this);
                $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = new Expression('NOW()'); 
                $modelArch->save();
            }
        } else {
            if($this->status == 1) {
                $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $this->id_case_fk;
                $modelArch->user_action = ($this->type_fk == 1) ? 'case' : 'event';
                $modelArch->data_change = $this->id;
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = new Expression('NOW()');
                $modelArch->save(); 
            }
            
        }
        
		parent::afterSave($insert, $changedAttributes);
	}
	
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function delete() {
		
        $this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function getCustomer()  {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getCase()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public function getInstance()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCaseInstance::className(), ['id' => 'id_instance_fk']);
    }
    
    /*public function getDepartments()
    {
		$items = $this->hasMany(\backend\Modules\Task\models\TaskDepartment::className(), ['id_task_fk' => 'id']); 
		return $items;
    }*/
    public function getDepartments()
    {
		$items = \backend\Modules\Task\models\TaskDepartment::find()->leftJoin('{{%company_department}}', '{{%company_department}}.id={{%task_department}}.id_department_fk')
                ->where(['id_task_fk' => $this->id, '{{%task_department}}.status' => 1])->select('id_department_fk, name')
                ->orderby('{{%company_department}}.name')->all();
        
       /* $this->hasMany(\backend\Modules\Task\models\TaskDepartment::className(), ['id_task_fk' => 'id'])->leftJoin('{{%company_department}}', '{{%company_department}}.id={{%task_department}}.id_department_fk')
										->where('{{%task_department}}.status = 1')->select('{{%company_department}}.id, {{%company_department}}.name')
										->orderby('{{%company_department}}.name');*/
		if(!$items || empty($items) )
            $items = \backend\Modules\Task\models\CaseDepartment::find()->leftJoin('{{%company_department}}', '{{%company_department}}.id={{%case_department}}.id_department_fk')
                ->where(['id_case_fk' => $this->id_case_fk, '{{%case_department}}.status' => 1])->select('id_department_fk, name')
                ->orderby('{{%company_department}}.name')->all();
        
        return $items;
    }
    
    public function getEmployees()
    {
		//$items = $this->hasMany(\backend\Modules\Task\models\TaskEmployee::className(), ['id_task_fk' => 'id']); 
        /*$items = $this->hasMany(\backend\Modules\Task\models\TaskEmployee::className(), ['id_task_fk' => 'id'])->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%task_employee}}.id_employee_fk')
                      ->where( ' {{%task_employee}}.status = 1 and {{%task_employee}}.is_show = 1 ')
                      ->select(['id_task_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); */
        $sql = "select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname, email, address, phone, is_show, dv.name as typename"
                ." from {{%task_employee}} te join {{%company_employee}} e on te.id_employee_fk = e.id left join {{%dictionary_value}} dv on dv.id = id_dict_employee_type_fk"
                . " where te.status = 1 and e.status = 1 and is_show = 1 and id_task_fk = ".$this->id;
        $items = Yii::$app->db->createCommand($sql)->queryAll();

		return $items;
    }
	
	public function getEmployeeslock()
    {
		//$items = $this->hasMany(\backend\Modules\Task\models\TaskEmployee::className(), ['id_task_fk' => 'id']); 
        $items = $this->hasMany(\backend\Modules\Task\models\TaskEmployee::className(), ['id_task_fk' => 'id'])->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%task_employee}}.id_employee_fk')
                      ->where( ' {{%task_employee}}.status = 0 and {{%task_employee}}.is_show = 1 ')
                      ->select(['id_task_fk','id_employee_fk','firstname','lastname'])->distinct()->orderby('{{%company_employee}}.lastname'); 

		return $items;
    }
    
    public function getFiles() {
        $filesData = [];
        //$filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 4])->orderby('id desc')->all(); 
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1])
                                                 ->andWhere( ' ( id_fk = '. $this->id . ' and id_type_file_fk = 4) ' )->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getNotes() {
        $notesData = \backend\Modules\Task\models\Note::find()->where(['status' => 1, 'id_task_fk' => $this->id, 'id_type_fk' => 2])->orderby('id desc')->all();
        return $notesData;
    }
    
    public function getResources() {
        $notesData = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['status' => 1, 'id_event_fk' => $this->id])->orderby('id desc')->all();
        return $notesData;
    }
    
	public static function listTypes($type) {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => $type])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
        /*$status[1] = 'Kancelaryjne';
        $status[2] = 'Osobiste';
        
        return $status;*/
    }
    
    public function getType() {
        //return ( isset(self::listTypes()[$this->id_dict_task_type_fk]) ) ? self::listTypes()[$this->id_dict_task_type_fk] : 'nieznany';
        $value = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id' => $this->id_dict_task_type_fk])->one();
        return ($value) ? $value->name : 'nieznany';
    }
	
    public static function listStatus() {
        $status[1] = 'Ustalone';
        $status[2] = 'Zakończone';
        $status[3] = 'Odwołane';
        
        return $status;
    }
    
    public static function listReminder() {
        $status[0] = 'ustaw';
        $status[1] = '3 dni przed';
        $status[2] = '2 dni przed';
        $status[3] = '1 dzien przed';
        //$status[4] = 'godzinę przed';
        //$status[5] = '30 minut przed';
        
        return $status;
    }
    
    public function getStatustask() {
        return ( isset(self::listStatus()[$this->id_dict_task_status_fk]) ) ? self::listStatus()[$this->id_dict_task_status_fk] : 'nieznany';
    }
    
    public function getStatusname() {
        return ( isset(self::listStatus()[$this->id_dict_task_status_fk]) ) ? self::listStatus()[$this->id_dict_task_status_fk] : 'nieznany';
    }
    
    public static function listCategory() {
        $types[1] = 'Wysoka';
        $types[2] = 'Średnia';
        $types[3] = 'Niska';
       // $types[4] = 'Prywatna';
        
        return $types;
    }
    
    public function getCategory() {
        return (  isset(self::listCategory()[$this->id_dict_task_category_fk]) ) ? self::listCategory()[$this->id_dict_task_category_fk] : 'nieznany';
    }
	
	public function getDelay() {
		$t1 = StrToTime ( $this->date_to );
		$t2 = StrToTime ( date('Y-m-d H:i:s') );
		$diff = $t2 - $t1;
		$hours = round($diff / ( 60 * 60 ));
		return $hours;
	}
	
	public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getUpdating() {
		$user = \common\models\User::findOne($this->updated_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getCorrespondence() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 5])
							->andWhere('id_fk in (select id_correspondence_fk from {{%correspondence_task}} ct join {{%correspondence}} c on c.id=ct.id_correspondence_fk where send_at is not null and send_by is not null and id_task_fk='.$this->id.')')
							->orderby('id desc')->all();
        return $filesData;
    }
    
    public static function getGroups($id) {
        $events = []; $departments = [];
        if($id != 0) {
            if(Yii::$app->user->identity->username == 'superadmin') {
                if($id == -1) {		
                    $events = CalTask::find()->where(['status' => 1])->orderby('name')->all();
                } else {
                    $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $id])->orderby('name')->all();
                }
            } else {
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee->is_admin) {
                    if($id == -1) {		
                        $events = CalTask::find()->where(['status' => 1])->orderby('name')->all();
                    } else {
                        $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $id])->orderby('name')->all();
                    }
                } else {
                    foreach($employee->departments as $key => $department) {
                        array_push($departments, $department->id_department_fk);
                    }
                    if($id == -1) {
                        if($employee->id_dict_employee_kind_fk >= 70)
                            $events = CalTask::find()->where(['status' => 1])->andWhere('id_case_fk in (select id_case_fk from {{%case_employee}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
                        else 
                            $events = CalTask::find()->where(['status' => 1])->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
                    } else {
                        //return CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
                        /*if($employee->id_dict_employee_kind_fk >= 70)
                            $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $id])->andWhere('id_case_fk in (select id_case_fk from {{%case_employee}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
                        else */
                            //$events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $id])->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employee->id.' )')->orderby('name')->all();
                            $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $id])->orderby('name')->all();
                    }
                }
            }
            
            $options = []; $options['Rozprawy'] = []; $options['Zadania'] = [];
            foreach($events as $key => $item) {
                $type = ($item->type_fk == 1) ? 'Rozprawy' : 'Zadania';
                $options[$type][$item->id] = $item->name . ' ['.date('Y-m-d', strtotime($item['date_from'])).']';
            }
            
            if(count($options['Rozprawy']) == 0) unset($options['Rozprawy']);
            if(count($options['Zadania']) == 0) unset($options['Zadania']);
            return ($options && $options != NULL) ? ($options) : [];
        } else {
            return [];
        }
    }
    
    public static function getForbox($ids) {
        $events = []; $departments = [];
     
        $events = \backend\Modules\Task\models\CalTask::find()->where('id_case_fk in ('. implode(',', $ids) .')')->andWhere('status = 1 and id_dict_task_status_fk != 3')->orderby('id_case_fk, type_fk,name')->all(); 
        $options = [];
        if( count($ids) == 1 ) {
            $options = []; $options['Rozprawy'] = []; $options['Zadania'] = [];
            foreach($events as $key => $item) {
                $type = ($item->type_fk == 1) ? 'Rozprawy' : 'Zadania';
                $options[$type][$item->id] = $item->name . ' ['.date('Y-m-d', strtotime($item['date_from'])).']';
            }
            
            if(count($options['Rozprawy']) == 0) unset($options['Rozprawy']);
            if(count($options['Zadania']) == 0) unset($options['Zadania']);
        } else {
            if( count($events) > 0 ) {
                $tempMatter = $events[0]->id_case_fk;
                foreach($events as $key => $item) {
                   /* if($key == 0 || $tempMatter != $value->id_case_fk) { $list_events .= '<optgroup label="'.$value->case['name'].'">'; }
                    $list_events .= '<option value="'.$value->id.'" '.( in_array($value->id, $selected) ? ' selected ' : '' ).'>'.$value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']' .'</option>';
                    $tempMatter = $value->id_case_fk;*/
                    $options[$item->case['name']][$item->id] = $item->name . ' ['.date('Y-m-d', strtotime($item['date_from'])).']';
                }
            }
        }
            
        return ($options && $options != NULL) ? ($options) : [];
    }
}
