<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Task\models\CalTask;

/**
 * CalTaskSearch represents the model behind the search form about `backend\Modules\Task\models\CalTask`.
 */
class CalTaskSearch extends CalTask
{
    public $id_employee_fk;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'all_day', 'update_for_all', 'id_dict_task_type_fk', 'id_dict_task_category_fk', 'id_dict_task_status_fk', 'id_reminder_type_fk', 'reminded', 'period_parent', 'id_customer_fk', 'id_case_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'place', 'description', 'date_from', 'date_to', 'reminder_date', 'period', 'period_end', 'custom_data', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CalTask::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'all_day' => $this->all_day,
            'update_for_all' => $this->update_for_all,
            'id_dict_task_type_fk' => $this->id_dict_task_type_fk,
            'id_dict_task_category_fk' => $this->id_dict_task_category_fk,
            'id_dict_task_status_fk' => $this->id_dict_task_status_fk,
            'reminder_date' => $this->reminder_date,
            'id_reminder_type_fk' => $this->id_reminder_type_fk,
            'reminded' => $this->reminded,
            'period_end' => $this->period_end,
            'period_parent' => $this->period_parent,
            'id_customer_fk' => $this->id_customer_fk,
            'id_case_fk' => $this->id_case_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data]);

        return $dataProvider;
    }
}
