<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cal_case_details}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_case_fk
 * @property integer $time_limit
 * @property integer $time_used
 * @property double $budget_limit
 * @property double $budget_used
 * @property string $date_from
 * @property string $date_to
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CalCaseDetails extends \yii\db\ActiveRecord
{
    
    public $percent_t = 0;
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_case_details}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_case_fk', 'time_limit', 'time_used', 'is_limited', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_currency_fk'], 'integer'],
            [['budget_limit', 'budget_used'], 'number'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['custom_options'], 'string'],
            ['time_limit', 'required', 'when' => function($model) { return ($model->is_limited); }, 'message' => 'Przy włączonej konroli limit czasowy nie może pozostać bez wartości' ],
            ['time_limit', 'checkEmployeesLimit', 'when' => function($model) { return ($model->user_action == 'basic'); } ],
        ];
    }
    
    public function checkEmployeesLimit($attribute, $params) {
        $sql = "select sum(time_limit) as time_limits from {{%case_employee}} where status = 1 and id_case_fk = ".$this->id_case_fk ;
        $data = \Yii::$app->db->createCommand($sql)->queryOne(); 
        if ( $this->time_limit < $data['time_limits'] ) {
            $this->addError($attribute, 'Limit nie może być mniejszy niż suma limtów przypisanych poszczególnym pracownikom');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'time_limit' => Yii::t('app', 'Limit czasowy'),
            'time_used' => Yii::t('app', 'Time Used'),
            'budget_limit' => Yii::t('app', 'Budżet'),
            'budget_used' => Yii::t('app', 'Budget Used'),
            'date_from' => Yii::t('app', 'Data rozpoczęcia'),
            'date_to' => Yii::t('app', 'Deadline'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'is_limited' => 'Kontrola limitu czasu',
            'id_currency_fk' => 'Waluta'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}

	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getPercent() {
        $percent = 0;
        if($this->time_limit > 0) $percent = round(($this->time_used*100)/($this->time_limit*60));
        
        return $percent;
    }
    
    public static function getStats($id) {
		$sql = "select sum(execution_time) as execution_time, count(*) as tasks_all, sum(case when is_close=0 then 1 else 0 end) as tasks_open from {{%cal_todo}} "
                ." where status = 1 and id_set_fk = ".$id;
        $data = \Yii::$app->db->createCommand($sql)->queryOne(); 
		$data['tasks_open'] = ($data['tasks_open']) ? $data['tasks_open'] : 0;
		
		$model = CalCaseDetails::find()->where(['id_case_fk' => $id])->one();
		if($model) {
            $sql = "select sum(time_limit) as limits from {{%case_employee}} "
                    ." where status = 1 and id_case_fk = ".$id;
            $dataLimits = \Yii::$app->db->createCommand($sql)->queryOne(); 
        }
		$data['leftTime'] = $model->time_limit-$dataLimits['limits'];
        
        $sqlActions = "select sum(unit_time/60*unit_price_native) as settlements from {{%acc_actions}} "
                ." where status = 1 and id_set_fk = ".$id;
        $dataActions = \Yii::$app->db->createCommand($sqlActions)->queryOne(); 
		$data['settlements'] = ($dataActions['settlements']) ? round($dataActions['settlements'],2) : 0;
		
        return $data;
    }
    
    public static function convertLimit($id) {
        $model = CalCaseDetails::find()->where(['id_case_fk' => $id])->one();
        if($model) {
            $sql = "select sum(execution_time) as execution_time from {{%cal_todo}} "
                    ." where status = 1 and id_set_fk = ".$id;
            $data = \Yii::$app->db->createCommand($sql)->queryOne(); 
            $model->time_used = $data['execution_time']*1;
            $model->save();
        }
        return true;
    }
    
    public function getCurrency() {
		$currencies = [1 => 'PLN', 2 => 'EUR', 3 => 'USD'];
		
		return (isset($currencies[$this->id_currency_fk])) ? $currencies[$this->id_currency_fk] : 'PLN';
	}
}
