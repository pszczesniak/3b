<?php

namespace backend\Modules\Task\models;

use Yii;

/**
 * This is the model class for table "{{%case_debtcollector}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $address_fk
 * @property integer $id_dict_debt_procedure_fk
 * @property string $sygn_execution
 * @property string $initiation_date
 * @property string $suspension_date
 * @property string $end_date
 * @property integer $id_case_fk
 * @property integer $id_case_instance_fk
 * @property double $cost_replacement
 * @property string $notes
 * @property integer $execution_unsuccessful
 * @property integer $execution_repaid
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseDebtcollector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_debtcollector}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'address_fk', 'id_dict_debt_procedure_fk', 'id_case_fk', 'id_case_instance_fk', 'execution_unsuccessful', 'execution_repaid', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['initiation_date', 'suspension_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id_case_fk', 'id_case_instance_fk'], 'required'],
            [['cost_replacement'], 'number'],
            [['notes'], 'string'],
            [['sygn_execution'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'address_fk' => Yii::t('app', 'Address Fk'),
            'id_dict_debt_procedure_fk' => Yii::t('app', 'Id Dict Debt Procedure Fk'),
            'sygn_execution' => Yii::t('app', 'Sygn Execution'),
            'initiation_date' => Yii::t('app', 'Initiation Date'),
            'suspension_date' => Yii::t('app', 'Suspension Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'id_case_instance_fk' => Yii::t('app', 'Id Case Instance Fk'),
            'cost_replacement' => Yii::t('app', 'Cost Replacement'),
            'notes' => Yii::t('app', 'Notes'),
            'execution_unsuccessful' => Yii::t('app', 'Execution Unsuccessful'),
            'execution_repaid' => Yii::t('app', 'Execution Repaid'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
