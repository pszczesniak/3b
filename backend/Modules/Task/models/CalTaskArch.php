<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\helpers\Url;
use  backend\Modules\Task\models\CalTask;

/**
 * This is the model class for table "{{%cal_task_arch}}".
 *
 * @property integer $id
 * @property integer $id_task_fk
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class CalTaskArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_task_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task_fk', 'id_case_fk'], 'required'],
            [['id_task_fk', 'id_case_fk', 'created_by'], 'integer'],
            [['data_arch', 'user_action'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_task_fk' => Yii::t('app', 'Id Task Fk'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
	
	public function getCase() {
        return ($this->user_action == 'matter' || $this->user_action == 'project') ? \backend\Modules\Task\models\CalCase::findOne($this->data_change) : false;
    }
	
	public function getTask() {
        return ($this->user_action == 'case' || $this->user_action == 'event') ? \backend\Modules\Task\models\CalTask::findOne($this->data_change) : false;
    }
	
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
	
	public static function setStyle($type) {

		$style="bg-teal";
		if($type == 'docs') {
			$style="bg-blue";
		}
		if($type == 'matter') {
			$style="bg-purple"; 
		}
		if($type == 'project') {
			$style="bg-orange"; 
		}
        if($type == 'close') {
			$style="bg-green"; 
		}
        if($type == 'cancel') {
			$style="bg-orange"; 
		}
		if($type == 'open') {
			$style="bg-orange"; 
		}
		if($type == 'delete') {
			$style="bg-red"; 
		}
        if($type == 'note') {
			$style="bg-grey"; 
		}
		
		return $style;
	}
	
	public function getStyle() {
		return self::setStyle($this->user_action);
	}
	
	public static function setIcon($type) {
		$icon="pencil"; 
		if($type == 'docs') {
			$icon="file"; 
		}
		if($type == 'matter') {
			$icon="balance-scale"; 
		}
		if($type == 'project') {
			$icon="folder-open"; 
		}
        if($type == 'close') {
			$icon="stop-circle"; 
		}
        if($type == 'cancel') {
			$icon="ban"; 
		}
		if($type == 'open') {
			$icon="play-circle"; 
		}
		if($type == 'delete') {
			$icon="trash"; 
		}
        if($type == 'note') {
			$icon="comment"; 
		}
		
		return $icon;
	}
	
	public function getIcon() {
		return self::setIcon($this->user_action);
	}
	
	public static function setTitle($type, $id, $short, $avatar,$user) {
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => $user])->one();
        $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee->id."/preview.jpg"))?"/uploads/employees/cover/".$employee->id."/preview.jpg":"/images/default-user.png";			
		$title = 'Aktualizacja';
		if($type == 'docs') {
			$task = CalTask::findOne($id);
			$title = 'Nowe dokumenty'; 
			if(!$short)
				$title .= ' do '.( ($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/'.( ($task->type_fk == 1) ? 'case' : 'event').'/view', 'id' => $id]).'">'.$task->name.'</a>';
		}
        if($type == 'note') {
			$task = CalTask::findOne($id);
			$title = 'Nowy komentarz'; 
			if(!$short)
				$title .= ' do '.( ($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/'.( ($task->type_fk == 1) ? 'case' : 'event').'/view', 'id' => $id]).'">'.$task->name.'</a>';
		}
		if($type == 'matter') {
			$title = 'Przypisanie sprawy: '.'<a href="'.Url::to(['/task/matter/view','id'=>$this->case['id']]).'">'.$this->case['name'].'</a>';
		}
		if($type == 'project') {
			$title = 'Przypisanie projektu: '.'<a href="'.Url::to(['/task/project/view','id'=>$this->case['id']]).'">'.$this->case['name'].'</a>';
		}
        if($type == 'close') {
			$task = CalTask::findOne($id);
            if($short)
                $title = 'Zamknięcie'; 
            else
                $title = 'Zamknięcie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/case/view', 'id' => $id]).'">'.$task->name.'</a>';
		}
        if($type == 'cancel') {
			$task = CalTask::findOne($id);
            if($short)
                $title = 'Odwołanie'; 
            else
                $title = 'Odwołanie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/case/view', 'id' => $id]).'">'.$task->name.'</a>';
		}
        if($type == 'open') {
			$task = CalTask::findOne($id);
            if($short)
                $title = 'Ponowne otwarcie'; 
            else
                $title = 'Ponowne otwarcie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/case/view', 'id' => $id]).'">'.$task->name.'</a>';
		}
		if($type == 'delete') {
			$task = CalTask::findOne($id);
            $title = 'Usunięcie '.(($task->type_fk == 1) ? 'rozprawy ' : 'zadania ').$task->name; 
		}
		if($type == 'update') {
			$task = CalTask::findOne($id);
			if(!$short)
				$title = 'Aktualizacja '. ( ($task->type_fk == 1) ? 'rozprawy' : 'zadania' ) . ' <a href="'.Url::to(['/task/'.( ($task->type_fk == 1) ? 'case' : 'event' ).'/view', 'id' => $id]).'">'.$task->name.'</a>'; 
		}
		
		return ($avatar) ? '<figure><img src="'. $avatar .'" alt="Avatar"></figure>'.$title : $title;
	}
	
	public function getTitle() {
		return self::setTitle($this->user_action, $this->data_change, true, true, $this->created_by);
	}
	
	public static function setContent($type, $short, $avatar, $data_change, $data_arch) {
		$content = '';
		
		if($type == 'update' || $type == 'edit' ) {
            $changeData = \yii\helpers\Json::decode($data_change);
			$archData = \yii\helpers\Json::decode($data_arch);
			$content = '<ul>';
			
            /*if( isset($changeData['date_from']) && isset($archData['date_from']) && trim($archData['date_from']) != trim($changeData['date_from'].':00') ) {

				$content .= '<li> zmiana daty rozpoczęcia z "<span class="old">'.$archData['date_from'].'</span>" na <span class="new">"'.$changeData['date_from'].':00'.'"</span></li>';
			}*/
            if( isset($changeData['event_date']) && isset($archData['event_date']) && trim($archData['event_date']) != trim($changeData['event_date']) ) {

				$content .= '<li> zmiana terminu z "<span class="old">'.$archData['event_date'].'</span>" na <span class="new">"'.$changeData['event_date'].'"</span></li>';
			}
            if( isset($changeData['event_time']) && isset($archData['event_time']) && trim($archData['event_time']) != trim($changeData['event_time']) ) {

				$content .= '<li> zmiana godziny z "<span class="old">'.( ($archData['event_time']) ? $archData['event_time'] : 'brak' ).'</span>" na <span class="new">"'.( ($changeData['event_time']) ? $changeData['event_time'] : 'brak' ).'"</span></li>';
			}
            
            if( isset($changeData['event_deadline']) && isset($archData['event_deadline']) && trim($archData['event_deadline']) != trim($changeData['event_deadline'].':00') ) {

				$content .= '<li> zmiana deadline z "<span class="old">'.$archData['event_deadline'].'</span>" na <span class="new">"'.$changeData['event_deadline'].':00'.'"</span></li>';
			}
			
			if( isset($changeData['name']) && isset($archData['name']) && trim($archData['name']) != trim($changeData['name']) ) {

				$content .= '<li> zmiana nazwy z "<span class="old">'.$archData['name'].'</span>" na <span class="new">"'.$changeData['name'].'"</span></li>';
			}
            
            /*if( isset($changeData['date_to']) && isset($archData['date_to']) && trim($archData['date_to']) != trim($changeData['date_to'].':00') ) {

				$content .= '<li> zmiana daty zakończenia z "<span class="old">'.$archData['date_to'].'</span>" na <span class="new">"'.$changeData['date_to'].':00'.'"</span></li>';
			}*/
            
			if( isset($changeData['place']) && isset($archData['place']) && $archData['place'] != $changeData['place'] ) {
				if(empty($archData['place']))
					$content .= '<li> dodanie miejsca <b>"'.$changeData['place'].'"</b></li>';
				else
					$content .= '<li> zmiana miejsca z "<span class="old">'.$archData['place'].'</span>" na <span class="new">"'.$changeData['place'].'"</span></li>';
			}
			
			if( isset($changeData['description']) && isset($archData['description']) && $archData['description'] != $changeData['description'] ) {
				if(empty($archData['description']))
					$content .= '<li> dodanie opisu <b>"'.$changeData['description'].'"</b></li>';
				else
					$content .= '<li> zmiana opisu z "<span style="text-decoration: line-through; color:red;">'.$archData['description'].'</span>" na <b>"'.$changeData['description'].'"</b></li>';
			}
			
			if( isset($archData['employees_add']) && count( $archData['employees_add'] ) > 0 ) {
				$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('id in ('.implode(',',$archData['employees_add']) .') ')->all();
				$content .= '<li> dodanie pracowników<ol>';
				foreach($employees as $key => $value) {
					$content .= '<li>'.$value->fullname.'</li>'; 
				}
				$content .= '</ol></li>';
			}
			if( isset($archData['employees_del']) && count( $archData['employees_del'] ) > 0 ) {
				$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('id in ('.implode(',',$archData['employees_del']) .') ')->all();
				$content .= '<li> usunięcie pracowników<ol>';
				foreach($employees as $key => $value) {
					$content .= '<li>'.$value->fullname.'</li>'; 
				}
				$content .= '</ol></li>';
			}
			$content .= '</ul>';
			$item['content'] = $content;
		}
		
		if($type == 'close') {
			/*$item['style'] = 'bg-green';
			$item['icon'] = 'stop-circle';
			$item['title'] = 'Zakończenie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/case/view', 'id' => $item['id_fk']]).'">'.$task->name.'</a>';*/
			$item['content'] = 'Zakończenie zdarzenia';
            $content = 'Zakończenie zdarzenia';
		}
		
		if($type == 'open') {
			/*$item['style'] = 'bg-orange';
			$item['icon'] = 'play-circle';
			$item['title'] = 'Ponowne otwarcie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/event/view', 'id' => $item['id_fk']]).'">'.$task->name.'</a>';*/
			$item['content'] = 'Ponowne otwarcie zdarzenia';
            $content = 'Ponowne otwarcie zdarzenia';
		}
		
		if($type == 'cancel') {
			/*$item['style'] = 'bg-orange';
			$item['icon'] = 'play-circle';
			$item['title'] = 'Ponowne otwarcie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/event/view', 'id' => $item['id_fk']]).'">'.$task->name.'</a>';*/
			$item['content'] = 'Rozprawa została odwołana';
            $content = 'Rozprawa została odwołana';
		}
        
        if($type == 'delete') {
			/*$item['style'] = 'bg-orange';
			$item['icon'] = 'play-circle';
			$item['title'] = 'Ponowne otwarcie '.(($task->type_fk == 1) ? 'rozprawy' : 'zadania').' <a href="'.Url::to(['/task/event/view', 'id' => $item['id_fk']]).'">'.$task->name.'</a>';*/
			$item['content'] = 'Usunięcie zdarzenia';
            $content = 'Usunięcie zdarzenia';
		}
		
		if($type == 'docs') {
			$content = '<p><ol>';
			foreach(\yii\helpers\Json::decode($data_change) as $key => $value) {
				$file = \common\models\Files::findOne($value);
				$content .= '<li><a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a></li>';
			}
			$content .= '</ol></p>';
		}
        
        if($type == 'note') {
            $archData = \yii\helpers\Json::decode($data_arch);
            //$listStatus = \backend\Modules\Task\models\CalCase::listStatus();
            //$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
            $content = 'Komentarz: '.$archData['note'];
        }
				
		return $content;
	}
	
	public function getContent() {
		return self::setContent($this->user_action,true,true,$this->data_change, $this->data_arch);
	}
	
	public static function getRender($type, $id, $short, $avatar,$user, $data_change, $data_arch) {
		$item['style'] = self::setStyle($type);
		$item['icon'] = self::setIcon($type);
		$item['title'] = self::setTitle($type, $id, $short, $avatar,$user);
		$item['content'] = self::setContent($type, $short, $avatar, $data_change, $data_arch);
		
		return $item;
	}
}
