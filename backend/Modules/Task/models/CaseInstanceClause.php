<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%case_instance_clause}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $application_date
 * @property string $post_date
 * @property string $delivery_date
 * @property integer $id_case_fk
 * @property integer $id_case_instance_fk
 * @property double $cost_replacement
 * @property double $cost_court
 * @property double $cost_entry
 * @property double $cost_postage
 * @property string $clause_content
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseInstanceClause extends \yii\db\ActiveRecord
{
    public $user_action;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_instance_clause}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_case_fk', 'id_case_instance_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['application_date', 'post_date', 'delivery_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id_case_fk', 'id_case_instance_fk'], 'required'],
            [['cost_replacement', 'cost_court', 'cost_entry', 'cost_postage'], 'number'],
            [['clause_content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'application_date' => Yii::t('app', 'Data wniosku'),
            'post_date' => Yii::t('app', 'Data nadania'),
            'delivery_date' => Yii::t('app', 'Data doręczenia'),
            'id_case_fk' => Yii::t('app', 'Sprawa'),
            'id_case_instance_fk' => Yii::t('app', 'Postępowanie'),
            'cost_replacement' => Yii::t('app', 'Koszty zastępstwa'),
            'cost_court' => Yii::t('app', 'Koszty sądowe'),
            'cost_entry' => Yii::t('app', 'Cost Entry'),
            'cost_postage' => Yii::t('app', 'Cost Postage'),
            'clause_content' => Yii::t('app', 'Opis'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}

	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
	
        if($this->user_action == 'create') {
            $modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $this->id_case_fk;
			$modelArch->user_action = 'clause';
			$modelArch->data_change = \yii\helpers\Json::encode(['id' => $this->id]);
			//$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
        } 
		parent::afterSave($insert, $changedAttributes);
	}
}
