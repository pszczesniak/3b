<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cal_note}}".
 *
 * @property integer $id
 * @property integer $id_type_fk
 * @property integer $id_parent_fk
 * @property string $note
 * @property string $data_arch
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Note extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_note}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['note','id_case_fk'], 'required'],
            [['id_type_fk', 'id_parent_fk', 'id_case_fk', 'id_task_fk', 'id_employee_from_fk', 'id_employee_to_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['note', 'data_arch'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_type_fk' => Yii::t('app', 'Id Type Fk'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'note' => Yii::t('app', 'Note'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				/*$modelArch = new CalTaskArch();
				$modelArch->id_task_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function delete() {
		$this->status = -1;
      //  $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function getUser() {
        return \common\models\User::findOne($this->created_by)->fullname;
    }
}
