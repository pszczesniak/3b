<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Task\models\CalTodoArch;
use backend\Modules\Task\models\CalCaseDetails;
use backend\Modules\Task\models\CaseEmployee;

/**
 * This is the model class for table "{{%cal_todo}}".
 *
 * @property integer $id
 * @property string $type_fk - 1: normal, 2: todo, 3: meeting, 4: urlop
 * @property string $name
 * @property integer $id_group_fk
 * @property integer $id_priority_fk
 * @property string $todo_date
 * @property string $todo_time
 * @property integer $execution_time
 * @property string $description
 * @property string $notes
 * @property string $custom_data
 * @property string $config_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CalTodo extends \yii\db\ActiveRecord
{
    public $user_action;
    
    public $timeH;
    public $timeM;
    
    public $fromDate;
    public $fromTime;
    public $toDate;
    public $toTime;
    
    public $meeting_employees;
    public $meeting_resource;
    public $meeting_type;
    public $meeting_purpose;
    
    public $counter = 0;
    
    public $id_department_fk;
    
    public $projectInfo = false;
    
    public $createAction;
    public $id_service_fk;
    public $id_order_fk;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_todo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'todo_date', 'id_employee_fk', 'id_dict_todo_type_fk'], 'required'],
            [['id_action_fk', 'type_fk', 'id_group_fk', 'id_priority_fk', 'id_employee_fk', 'id_department_fk', 'execution_time', 'is_close', 'todo_show', 
              'status', 'created_by', 'updated_by', 'deleted_by', 'timeH', 'timeM', 'id_dict_todo_type_fk', 'all_day', 'g_action', 'counter', 'id_meeting_fk', 'id_customer_fk', 'id_customer_branch_fk','id_set_fk', 'createAction', 'id_service_fk', 'id_order_fk'], 'integer'],
            [['todo_date', 'todo_deadline', 'created_at', 'updated_at', 'deleted_at', 'fromDate', 'fromTime', 'toDate', 'toTime', 'date_from', 'date_to',
              'meeting_employees', 'meeting_resource', 'meeting_type', 'meeting_purpose', 'reminded', 'reminder_user', 'reminder_date', 'id_reminder_type_fk', 'createAction'], 'safe'],
            [['description', 'notes', 'custom_data', 'config_data', 'fromDate', 'fromTime', 'toDate', 'toTime', 'date_from', 'date_to', 'g_id'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['todo_time'], 'string', 'max' => 5],
            ['todo_deadline','validateDeadline'],
            [['fromDate', 'toDate'], 'required', 'message' => 'Proszę podać daty startu i końca', 'when' => function($model) { return ($model->type_fk == 4); }],
            [['meeting_employees', 'meeting_purpose'], 'required', 'when' => function($model) { return ($model->type_fk == 3); }],
            [['date_to'], 'required', 'message' => 'Jeśli chcesz zarezerwować zasób musisz podać datę zakończenia spotkania', 'when' => function($model) { return ($model->type_fk == 3 && $model->meeting_resource); }],
            [['date_from'], 'validateReservation', 'when' => function($model) { return ($model->type_fk == 3 && $model->meeting_resource); }],
            [['timeM'], 'validateExecutiontime',  'when' => function($model) { return ( $model->id_set_fk || $model->id_dict_todo_type_fk == 8 || \backend\Modules\Task\models\CalDictionary::findOne($model->id_dict_todo_type_fk)['type_fk'] == 1); }, 'message' => 'Proszę podać czas wykonania zadania'],
            //[['id_set_fk'], 'required',  'when' => function($model) { return ( $model->id_dict_todo_type_fk == 8 ); }],
            //[['id_employee_fk'], 'validateLimittime',  'when' => function($model) { return ( !empty($model->id_set_fk) ); }],
            //[['timeM'], 'validateExecutiontime', 'when' => function($model) { return ($model->id_dict_todo_type_fk <= 4); }],
           // ['date_from','validateDates'/*,  'on' => self::SCENARIO_CALENDAR*/],
        ];
    }
    
    public function validateExecutiontime(){
		//if($this->id_set_fk || $this->id_dict_todo_type_fk <= 4) {
            if((!$this->timeH && !$this->timeM) /*|| $this->execution_time == 0*/){
                $this->addError('timeM', 'Proszę podać czas wykonania zadania');
            } 
        //} 
	}
    
    public function validateLimittime(){
		$details = \backend\Modules\Task\models\CalCaseDetails::find()->where(['id_case_fk' => $this->id_set_fk])->one();
        if($this->status == 1 && $details && $details->is_limited){
			$modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_case_fk' => $this->id_set_fk])->one();
            if($modelCe) {
                $toUsed = $modelCe->time_limit*60-$modelCe->time_used;
                
                $H = ($this->timeH) ? $this->timeH*60 : 0;
                $m = ($this->timeM) ? $this->timeM : 0;
                $execution_time = $H + $m;
                
                if($this->isNewRecord) {
                    $currentTime = 0;
                } else {
                    $currentTime = CalTodo::findOne($this->id)->execution_time;
                }
                $toUsed = $toUsed + $currentTime;
                if($toUsed < $execution_time) {
                    $timeH = intval($toUsed/60);
                    $timeM = $toUsed - ($timeH*60);
                    $this->addError('id_employee_fk', 'Liczba godzin możliwa do wykorzystania w ramach limitu dla tego projektu wynosi: '.round($toUsed/60,2).' ['.$timeH.' h '.$timeM.' min]');
                } else {
                    if($this->createAction) {
                        $sql = "select sum(unit_time) as unit_time from {{%acc_actions}} where status = 1 and id_employee_fk = ".$this->id_employee_fk." and id_set_fk = ".$this->id_set_fk;
                       
                        $dataStats = Yii::$app->db->createCommand($sql)->queryOne();
                        
                        $toUsed = $modelCe->time_limit*60 - $dataStats['unit_time'];
                        
                        $H = ($this->timeH) ? $this->timeH*60 : 0;
                        $m = ($this->timeM) ? $this->timeM : 0;
                        $execution_time = $H + $m;
                        
                        if($toUsed < $execution_time) {
                            $timeH = intval($toUsed/60);
                            $timeM = $toUsed - ($timeH*60);
                            $this->addError('createAction', 'Nie można uwtorzyć czynności na rzecz klienta. Liczba godzin możliwa do wykorzystania w ramach limitu dla tego projektu wynosi: '.round($toUsed/60,2).' ['.$timeH.' h '.$timeM.' min]');
                        }
                    }
                }
            }
		} 
	}
    
    public function validateDeadline(){
        $todoDate = $this->todo_date.' '. ( ($this->todo_time) ? $this->todo_time : '00:00') .':00';
		if(strtotime($this->todo_deadline) < strtotime($todoDate)){
			$this->addError('event_deadline','Deadline nie może być wczesniejszy niż termin');
		} 
	}
    
    public function validateDates(){
		if($this->date_to && strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Wprowadź poprawną datę końca');
		}
	}
    
    public function validateReservation(){
        $startDate = $this->date_from.':00';
		$endDate = ($this->date_to) ? $this->date_to.':00' : $startDate;
        foreach($this->meeting_resource as $key => $resource) {
            $reservations = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['status' => 1, 'id_resource_fk' => $resource])
                                ->andWhere(" id != ". ( ($this->id) ? $this->id : 0) )
                                //->andWhere( " ( ( '".$startDate."' >= date_from and '".$startDate."' < date_to ) or ( '".$endDate."' between date_from and date_to ) or (  date_from between '".$startDate."' and date_to ) )")
                                ->andWhere( " ( ( '".$startDate."' >= date_from and '".$startDate."' < date_to ) or ( '".$endDate."' between date_from and date_to )  )")
                                ->all();
            if( count($reservations) > 0 ){
                $temp = \backend\Modules\Company\models\CompanyResources::findOne($resource);
                $this->addError('date_from','Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to);
            } 
        }		
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()  {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'id_group_fk' => Yii::t('app', 'Id Group Fk'),
            'id_priority_fk' => Yii::t('app', 'Id Priority Fk'),
            'id_dict_todo_type_fk' => Yii::t('app', 'Typ'),
            'todo_date' => Yii::t('app', 'Todo Date'),
            'todo_time' => Yii::t('app', 'Todo Time'),
            'todo_deadline' => Yii::t('app', 'Deadline'),
            'execution_time' => Yii::t('app', 'Czas [w min]'),
            'date_from' => Yii::t('app', 'Start'),
            'date_to' => Yii::t('app', 'Koniec'),
            'description' => Yii::t('app', 'Description'),
            'notes' => Yii::t('app', 'Notes'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'config_data' => Yii::t('app', 'Config Data'),
            'is_close' => 'Zakończone',
            'all_day' => 'Cały dzień',
            'todo_show' => 'Pokaż na liście TODO',
            'status' => Yii::t('app', 'Status'),
			'id_employee_fk' => 'Pracownik',
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'meeting_employees' => 'Uczstnicy spotkania',
            'meeting_resource' => 'Zasób',
            'meeting_type' => 'Rodzaj spotkania',
            'meeting_purpose' => 'Cel spotkania',
            'id_department_fk' => 'Dział',
            'id_service_fk' => 'Typ czynności',
            'id_order_fk' => 'Zlecenie',
            'id_customer_fk' => 'Klient',
            'id_set_fk' => 'Projekt',
            'id_reminder_type_fk' => 'Przypomnienie',
            'delay' => 'Opóźnienie',
            'createAction' => 'Rozlicz czynność na rzecz klienta',
            'id_customer_branch_fk' => 'Oddział'
        ];
    }
    
    public function beforeSave($insert) {
		if($this->user_action != 'delete' && $this->user_action != 'close' && $this->user_action != 'create_action') {
            $H = ($this->timeH) ? $this->timeH*60 : 0;
            $m = ($this->timeM) ? $this->timeM : 0;
            $this->execution_time = $H + $m;
        }
        /*if( $this->execution_time == 0 && !empty($this->id_set_fk) ) {
            $this->addError('timeM', 'Proszę podać czas wykonania zadania');
        }*/
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if (!$insert && $this->status == 1) {       
            $modelArch = new CalTodoArch();
            $modelArch->id_todo_fk = $this->id;
            if($this->user_action) {
                $modelArch->user_action = $this->user_action;
            } else {
                $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
            }
            
            $modelArch->data_change = \yii\helpers\Json::encode($this);
            $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();  
        } 
        if($this->user_action != 'close' && $this->user_action != 'create_action') {
            if($this->id_set_fk) {
                CalCaseDetails::convertLimit($this->id_set_fk);
                $this->projectInfo = CaseEmployee::convertLimit($this->id_employee_fk, $this->id_set_fk);
            }
            if(isset($changedAttributes['id_set_fk']) && $changedAttributes['id_set_fk'] != $this->id_set_fk) {
                CalCaseDetails::convertLimit($changedAttributes['id_set_fk']);
                CaseEmployee::convertLimit($this->id_employee_fk, $changedAttributes['id_set_fk']);
            }
        }
     
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getFiles() {
        $filesData = [];
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 7])->orderby('id desc')->all(); 
       return $filesData;
    }
    
    public function getPriority() {
        $dict = [0 => 'brak', 1 => 'niski', 2 => 'średni', 3 => 'wysoki'];
        return $dict[$this->id_priority_fk];
    }
    
    public function getState() {
        $dict = [0 => 'otwarte', 1 => 'zamknięte'];
        return $dict[$this->is_close];
    }
    
    public static function listTypes($type) {
        $typesDict = \backend\Modules\Task\models\CalDictionary::find()->where('status >= 1')->orderby('rank_no')->all();
        $types = [];
        foreach($typesDict as $key => $value) {
            $types[$value->id] = $value['name'];
        }
        return $types;
    }
    
    public static function listGroups() {
        
        //$items = \backend\Modules\Accounting\models\AccTax::find()->where( ['status' => 1])->all();
        $groups = [];
        $groups[1] = 'Administracyjne';
        $groups[2] = 'Marketing';
        $groups[3] = 'Urlop';
        $groups[4] = 'L4';
        $groups[7] = 'Dzień wolny';
        
        return $groups;
    }
    
    public function getMeeting() {
		$meeting = false;
        if($this->type_fk == 3)
            $meeting = \backend\Modules\Community\models\ComMeeting::findOne($this->id_group_fk); 
        return $meeting;
	}
    
    public static function Dict() { 
        //return ( isset(self::listTypes()[$this->id_dict_task_type_fk]) ) ? self::listTypes()[$this->id_dict_task_type_fk] : 'nieznany';
        $values = [];
        $values = \backend\Modules\Task\models\CalDictionary::find()->where( ['type_fk' => 1])->orderby('name')->all();
        return $values;
    }
    
    public static function dictTypes() {
        $types = [];

        /*$types[1] = ['name' => 'administracyjne', 'icon' => 'calendar-check-o', 'color' => 'grey', 'colorHex' => '#4B87AE'];
        $types[2] = ['name' => 'marketing', 'icon' => 'flash', 'color' => 'pink', 'colorHex' => '#DE1771'];       
        $types[3] = ['name' => 'urlop', 'icon' => 'sun-o', 'color' => 'yellow', 'colorHex' => '#EFC109'];
        $types[4] = ['name' => 'L4', 'icon' => 'heartbeat', 'color' => 'blue', 'colorHex' => '#33BFEB'];
        $types[5] = ['name' => 'osobiste', 'icon' => 'user', 'color' => 'grey', 'colorHex' => '#4B87AE'];
        $types[6] = ['name' => 'TODO', 'icon' => 'hourglass', 'color' => 'grey', 'colorHex' => '#4B87AE'];
        $types[7] = ['name' => 'dzień wolny', 'icon' => 'calendar-times-o', 'color' => 'red', 'colorHex' => '#FC5B3F'];
        $types[8] = ['name' => 'zarządzanie projektami', 'icon' => 'pie-chart', 'color' => 'orange', 'colorHex' => '#F9892E'];*/
        //$types[3] = ['name' => 'spotkanie', 'icon' => 'handshake-o', 'color' => 'orange'];
		foreach(self::dict() as $key => $dict) {
			$types[$dict->id] = ['name' => $dict->name, 'icon' => $dict->dict_icon, 'color' => $dict->dict_color, 'colorHex' => $dict->dict_color];
		}
        
        return $types;
    }
    
    public function getDelay() {
		$t1 = StrToTime ( $this->date_to );
		$t2 = StrToTime ( date('Y-m-d H:i:s') );
		$diff = $t2 - $t1;
		$hours = round($diff / ( 60 * 60 ));
		return $hours;
	}
    
    public function getCase()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_set_fk']);
    }
    
    public function getEmployee()  {
		return $this->hasOne(\backend\Modules\Company\models\Companyemployee::className(), ['id' => 'id_employee_fk']);
    } 
    
    public function getCreator() {
        $user = \common\models\User::findOne($this->created_by);
		return ($user) ? $user->fullname : 'Administrator';
	}
}
