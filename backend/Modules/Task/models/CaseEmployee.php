<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%case_employee}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $id_employee_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CaseEmployee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_employee}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'id_employee_fk'], 'required'],
            [['id_case_fk', 'id_employee_fk', 'time_limit', 'time_used', 'created_by', 'deleted_by', 'status', 'is_show'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function delete() {
		
        $this->status = -1;
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		
		return $result;
	}
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCase()
    {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public static function convertLimit($eid, $id) {
        $model = CaseEmployee::find()->where(['id_employee_fk' => $eid, 'id_case_fk' => $id])->one();
        $sql = "select sum(execution_time) as execution_time from {{%cal_todo}} "
                ." where status = 1 and id_set_fk = ".$id." and id_employee_fk = ".$eid;
        $data = Yii::$app->db->createCommand($sql)->queryOne(); 
        $model->time_used = ($data['execution_time']) ? $data['execution_time'] : 0;
        $model->save();
        
        $toUse = ($model->time_limit*60)-$model->time_used;
        $timeH = intval($toUse/60);
        $timeM = $toUse - ($timeH*60);
        
        return $timeH . 'h ' . $timeM . ' min';
    }
}
