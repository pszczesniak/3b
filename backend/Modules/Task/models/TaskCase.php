<?php

namespace backend\Modules\Task\models;

use Yii;

/**
 * This is the model class for table "{{%task_case}}".
 *
 * @property integer $id
 * @property integer $id_task_fk
 * @property integer $id_case_fk
 * @property string $created_at
 * @property integer $created_by
 * @property integer $id_arch_fk
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class TaskCase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_case}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task_fk'], 'required'],
            [['id_task_fk', 'id_case_fk', 'created_by', 'id_arch_fk', 'status', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_task_fk' => Yii::t('app', 'Id Task Fk'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'id_arch_fk' => Yii::t('app', 'Id Arch Fk'),
            'status' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
