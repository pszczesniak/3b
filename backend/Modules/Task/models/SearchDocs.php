<?php

namespace frontend\Modules\Task\models;

use Yii;
use yii\base\Model;


/**
 * ActionForm is the model behind the contact form.
 */
class SearchDocs extends Model
{
    public $type; 
    public $customer;
    public $case;
    public $date_from;
    public $date_to;
    public $title; 
    public $description;
    
    /**
     * @inheritdoc
     */
   /* public function rules()
    {
        return [
            [['type', 'hours_of_day', 'id_employee_fk', 'date_from', 'date_to'], 'required'],
            [['hours_of_day', 'id_employee_fk'], 'integer'],
            [['description'],'safe'],
            ['date_to','validateDates'],
        ];
    }*/
      /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Typ',
            'customer' => 'Klient',
            'case' => 'Sprawa',
            'date_from' => 'Data początku',
            'date_to' => 'Data końca',
            'title' => 'Tytuł',
            'description' => 'Opis'
        ];
    }
    
    public static function getGropus() {
        return [1 => 'do sprawy', 2 => 'do zdarzeń', 3 => 'do rozpraw', 4 => 'do zadań', 5 => 'do koreposndencji', 6 => 'korespondencji przychodzącej', 7 => 'do korespondencji wychodzącej'];
    }
}
