<?php

namespace frontend\Modules\Task\models;

use Yii;
use yii\base\Model;

use backend\Modules\Task\models\CalTodo;
use backend\Modules\Accounting\models\AccActions;

/**
 * ActionForm is the model behind the contact form.
 */
class ActionFast extends Model
{
    public $type; 
    public $date_from;
    public $date_to;
    public $id_employee_fk;
    public $hours_of_day; 
    public $description;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'hours_of_day', 'id_employee_fk', 'date_from', 'date_to'], 'required'],
            [['hours_of_day', 'id_employee_fk'], 'integer'],
            [['description'],'safe'],
            ['date_to','validateDates'],
        ];
    }
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Proszę podać poprawną datę końca');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Rodzaj',
            'hours_of_day' => 'Ilość godzin dziennie',
            'date_from' => 'Data początku',
            'date_to' => 'Data końca'
        ];
    }

    public function saveAction()
    {            
 
        /*while (strtotime($this->date_from) <= strtotime($this->date_to)) {
                echo "$date\n";
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }*/
        $startTime = strtotime($this->date_from);
        $endTime = strtotime($this->date_to);
        for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
            $thisDate = date( 'Y-m-d', $i ); 
            $new = new CalTodo();
            $new->name = ($this->type == 3) ? 'Urlop' : 'L4';
            $new->type_fk = 1;
            $new->id_dict_todo_type_fk = $this->type;
            $new->todo_date = $thisDate;
            $new->timeH = $this->hours_of_day;
            $new->timeM = 0;
            $new->description = ($this->description) ? $this->description : $new->name;
            $new->id_employee_fk= $this->id_employee_fk;
            $new->id_priority_fk = 2;
            $new->is_close = 0;
            $new->todo_show = 0;
            $new->id_meeting_fk = 0;
            
            $new->save();
        }
    
        return true;
    }
}
