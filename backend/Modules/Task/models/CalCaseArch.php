<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\helpers\Url;
use backend\Modules\Task\models\CalCase;

/**
 * This is the model class for table "{{%cal_case_arch}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class CalCaseArch extends \yii\db\ActiveRecord
{
    public $id_fk;
    public $type_fk;
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_case_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk'], 'required'],
            [['id_case_fk', 'created_by'], 'integer'],
            [['data_arch', 'user_action'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_case_fk' => Yii::t('app', 'Id Case Fk'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
	
	public function getCase() {
        return ($this->user_action == 'matter' || $this->user_action == 'project') ? \backend\Modules\Task\models\CalCase::findOne($this->data_change) : false;
    }
	
	public function getTask() {
        return ($this->user_action == 'case' || $this->user_action == 'event') ? \backend\Modules\Task\models\CalTask::findOne($this->data_change) : false;
    }
	
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
	
	public static function setStyle($type) {

		$style="bg-teal";
		if($type == 'docs' || $type == 'correspondence') {
			$style="bg-blue";
		}
		if($type == 'case') {
			$style="bg-pink"; 
		}
		if($type == 'event') {
			$style="bg-yellow"; 
		}
		if($type == 'delete') {
			$style="bg-red"; 
		}
        if($type == 'note') {
			$style="bg-grey"; 
		}
        if($type == 'merge') {
            $style="bg-orange"; 
        }
		
		return $style;
	}
	
	public function getStyle() {
		return self::setStyle($this->user_action);
	}
	
	public static function setIcon($type) {
		$icon="pencil"; 
		if($type == 'docs') {
			$icon="file"; 
		}
		if($type == 'case') {
			$icon="gavel"; 
		}
		if($type == 'event') {
			$icon="tasks"; 
		}
		if($type == 'delete') {
			$icon="trash"; 
		}
        if($type == 'note') {
			$icon="comment"; 
		}
        if($type == 'correspondence') {
			$icon="envelope"; 
		}
        if($type == 'merge') {
			$icon="object-group"; 
		}
		
		return $icon;
	}
	
	public function getIcon() {
		return self::setIcon($this->user_action);
	}
	
	public static function setTitle($type, $id, $short, $avatar,$user) {
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => $user])->one();
        $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee->id."/preview.jpg"))?"/uploads/employees/cover/".$employee->id."/preview.jpg":"/images/default-user.png";			
		$title = 'Aktualizacja sprawy';
		if($type == 'docs') {
			$case = CalCase::findOne($id);
			$title = 'Nowe dokumenty'; 
			if(!$short)
				$title .= ' do '.( ($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/'.( ($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>';
		}
		if($type == 'status') {
			$case = CalCase::findOne($id);
			if($short)
				$title = 'Zmiana statusu'; 
			else
				$title = 'Zmiana statusu '.(($case->type_fk == 1) ? 'sprawy' : 'projektu'). ' <a href="'.Url::to(['/task/'.( ($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>'; 
		}
		if($type == 'case') {
			$model = \backend\Modules\Task\models\CalTask::findOne($id);
			$title = 'Przypisanie rozprawy: '.'<a href="'.Url::to(['/task/case/view','id'=>$model->id]).'" title="do '.(($model->case['type_fk'] == 1) ? 'sprawy' : 'projektu').' '.$model->case['name'].'">'.$model->name.'</a>';
		}
		if($type == 'event') {
		    $model = \backend\Modules\Task\models\CalTask::findOne($id);
			$title = 'Przypisanie zadania: '.'<a href="'.Url::to(['/task/event/view','id'=>$model->id]).'">'.$model->name.'</a>';
		}
		
		if($type == 'update') {
			$case = \backend\Modules\Task\models\CalCase::findOne($id);
			if(!$short)
				$title = 'Aktualizacja '.(($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/'.(($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>';
		}
		
		if($type == 'delete') {
			$case = \backend\Modules\Task\models\CalCase::findOne($id);
			//$title = 'Usunięcie '.(($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/event/view', 'id' => $id]).'">'.$case->name.'</a>';
			$title = 'Usunięcie '.(($case->type_fk == 1) ? 'sprawy' : 'projektu').' '.$case->name.'';
		}
        if($type == 'note') {
			$case = CalCase::findOne($id);
			$title = 'Nowy komentarz'; 
			if(!$short)
				$title .= ' do '.( ($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/'.( ($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>';
		}
        if($type == 'correspondence') {
			$case = CalCase::findOne($id);
			$title = 'Nowa korespondencja'; 
			if(!$short)
				$title .= ' do '.( ($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/'.( ($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>';
		}
        if($type == 'merge') {
			$case = CalCase::findOne($id);
			$title = 'Scalenie spraw';
			if(!$short)
				$title .= ' do '.( ($case->type_fk == 1) ? 'sprawy' : 'projektu').' <a href="'.Url::to(['/task/'.( ($case->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $id]).'">'.$case->name.'</a>';
		}
		
		return ($avatar) ? '<figure><img src="'. $avatar .'" alt="Avatar"></figure>'.$title : $title;
	}
	
	public function getTitle() {
		return self::setTitle($this->user_action, $this->data_change, true, true, $this->created_by);
	}
	
	public static function setContent($type, $short, $avatar, $data_change, $data_arch) {
		$content = '';
		if($type == 'docs') {
			$content = '<p><ol>';
			foreach(\yii\helpers\Json::decode($data_change) as $key => $value) {
				$file = \common\models\Files::findOne($value);
				$content .= '<li><a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a></li>';
			}
			$content .= '</ol></p>';
		}
        
        if($type == 'merge') {
			$content = '<p>Scalone sprawy: <ol>';
			foreach(\yii\helpers\Json::decode($data_change) as $key => $value) {
				$case = \backend\Modules\Task\models\CalCase::findOne($value);
				$content .= '<li>'.$case->name.'</li>';
			}
			$content .= '</ol></p>';
		}
        
		if($type == 'status') {
			$data_change = \yii\helpers\Json::decode($data_change);
	        $data_arch = \yii\helpers\Json::decode($data_arch);
			$listStatus = \backend\Modules\Task\models\CalCase::listStatus('normal');
			$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
			$content = 'Zmiana statusu z <span class="label '.$colors[$data_arch['id_dict_case_status_fk']].'">'.$listStatus[$data_arch['id_dict_case_status_fk']].'</span> na <span class="label '.$colors[$data_change['id_dict_case_status_fk']].'">'.$listStatus[$data_change['id_dict_case_status_fk']].'</span>';
		}
        
        if($type == 'delete') {
			//$listStatus = \backend\Modules\Task\models\CalCase::listStatus();
			//$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
			$content = 'Usunięcie z systemu';
		}
        
        if($type == 'note') {
            $archData = \yii\helpers\Json::decode($data_arch);
            //$listStatus = \backend\Modules\Task\models\CalCase::listStatus();
            //$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
            $content = 'Komentarz: '.$archData['note'];
        }
		
		if($type == 'update' || empty($type) || $type == 'edit') {
			$changeData = \yii\helpers\Json::decode($data_change);
			$archData = \yii\helpers\Json::decode($data_arch);
	
			$content = '<ul>';
	
			
			if( isset($archData['id_customer_fk']) &&  $archData['id_customer_fk'] != $changeData['id_customer_fk'] ) {
                $content .= '<li> zmiana klienta z <span class="old">'.\backend\Modules\Crm\models\Customer::findOne($archData['id_customer_fk'])->name.'</span> na <span class="new">'.\backend\Modules\Crm\models\Customer::findOne($changeData['id_customer_fk'])->name.'</span> </li>';			
            }
			
            if( isset($archData['name']) && isset($changeData['name']) &&  $archData['name'] != $changeData['name'] ) {
				if(empty($archData['name']))
					$content .= '<li> dodanie nazwy <b>"'.$changeData['name'].'"</b></li>';
				else
					$content .= '<li> zmiana opisu z "<span class="old">'.$archData['name'].'</span>" na <span class="new">"'.$changeData['name'].'"</span></li>';
			}
            
			if( isset($archData['description']) && isset($changeData['description']) &&  $archData['description'] != $changeData['description'] ) {
				if(empty($archData['description']))
					$content .= '<li> dodanie opisu <b>"'.$changeData['description'].'"</b></li>';
				else
					$content .= '<li> zmiana opisu z "<span style="text-decoration: line-through; color:red;">'.$archData['description'].'</span>" na <b>"'.$changeData['description'].'"</b></li>';
			}
			
			if( isset($archData['employees_add']) && count( $archData['employees_add'] ) > 0 ) {
				$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('id in ('.implode(',',$archData['employees_add']) .') ')->all();
				$content .= '<li> dodanie pracowników<ol>';
				foreach($employees as $key => $value) {
					$content .= '<li>'.$value->fullname.'</li>'; 
				}
				$content .= '</ol></li>';
			}
			if( $archData['employees_del'] && count( $archData['employees_del'] ) > 0 ) {
				$employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('id in ('.implode(',',$archData['employees_del']) .') ')->all();
				$content .= '<li> usunięcie pracowników<ol>';
				foreach($employees as $key => $value) {
					$content .= '<li>'.$value->fullname.'</li>'; 
				}
				$content .= '</ol></li>';
			}
            
            if( isset($archData['authority_carrying']) && isset($changeData['authority_carrying']) && $changeData['authority_carrying'] != $archData['authority_carrying']) { 
                if(empty($archData['authority_carrying'])) {
                    $content .= '<li> dodanie organu prowadzącego</li>';
                } else {
                    $content .= '<li> zmiana organu prowadzącego</li>';
                }
            }
            
            if( isset($archData['authority_supervisory']) && isset($changeData['authority_supervisory']) && $changeData['authority_supervisory'] != $archData['authority_supervisory']) { 
                if(empty($archData['authority_supervisory'])) {
                    $content .= '<li> dodanie organu nadzorującego</li>';
                } else {
                    $content .= '<li> zmiana organu nadzorującego</li>';
                }
            }

			$content .= '</ul>';
		}
		
		if($type == 'correspondence') {
            $archData = \yii\helpers\Json::decode($data_arch);
            //$listStatus = \backend\Modules\Task\models\CalCase::listStatus();
            //$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
            $content = /*'Dodano dokumenty:'. */$archData['files'];
        }

				
		return $content;
	}
	
	public function getContent() {
        if(!$this->user_action) $this->user_action = 'update';
		return self::setContent($this->user_action,true,true,$this->data_change, $this->data_arch); 
	}
	
	public static function getRender($type, $id, $short, $avatar,$user, $data_change, $data_arch) {
		$item['style'] = self::setStyle($type);
		$item['icon'] = self::setIcon($type);
        if($type == 'event' || $type == 'case') $id = $data_change;
		$item['title'] = self::setTitle($type, $id, $short, $avatar,$user);
		$item['content'] = self::setContent($type, $short, $avatar, $data_change, $data_arch);
		
		return $item;
	}
}
