<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cal_case_instance}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $instance_level
 * @property string $instance_name
 * @property string $instance_sygn
 * @property integer $instance_address_fk
 * @property string $instance_contact
 * @property string $instance_note
 * @property string $instance_custom
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CalCaseInstance extends \yii\db\ActiveRecord
{
    
    public $address_type_fk;
    public $id_customer_fk;
    public $id_opposite_side_fk;
    
    public $user_action;
	
	public $attachs;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_case_instance}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'instance_level', 'instance_name'], 'required'],
            [['id_dict_instance_status_fk', 'id_dict_instance_mode_fk', 'id_case_fk', 'instance_level', 'instance_address_fk', 'is_active', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_customer_fk'], 'integer'],
            [['instance_contact', 'instance_note', 'instance_custom'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'date_start', 'date_end', 'date_legalization', 'date_opposition', 'judgment', 'attachs'], 'safe'],
            [['instance_name', 'instance_sygn'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_case_fk' => Yii::t('app', 'Sprawa'),
            'instance_level' => Yii::t('app', 'Instance Level'),
            'instance_name' => Yii::t('app', 'Nazwa'),
            'instance_sygn' => Yii::t('app', 'Sygnatura akt'),
            'instance_address_fk' => Yii::t('app', 'Organ prowadzący'),
            'instance_department' => Yii::t('app', 'Wydział'),
            'instance_contact' => Yii::t('app', 'Kontakt'),
            'instance_note' => Yii::t('app', 'Notatka'),
            'instance_custom' => Yii::t('app', 'Instance Custom'),
            'status' => Yii::t('app', 'Status'),
            'id_dict_instance_status_fk' => 'Status', 
            'id_dict_instance_mode_fk' => 'Tryb',
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_customer_fk' => 'Klient',
            'id_opposite_side_fk' => 'Strona przeciwna',
            'date_start' => 'Data rozpoczęcia', 
            'date_end' => 'Data zakończenia', 
            'date_legalization' => 'Data uprawomocnienia', 
            'date_opposition' => 'Data sprzeciwu', 
            'judgment' => 'Wyrok',
            'is_active' => 'Aktywna'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}

	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
	    
		if ($insert && $this->attachs) {
            foreach($this->attachs as $key => $file) {
                if($file) {
                    $modelFile = new \common\models\Files(['scenario' => 'save']);
                    $modelFile->extension_file = $file->extension;
                    $modelFile->size_file = $file->size;
                    $modelFile->mime_file = $file->type;
                    $modelFile->name_file = $file->baseName;
                    $modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
                    $modelFile->title_file = $file->baseName.'.'.$file->extension;
                    $modelFile->id_fk = $this->id;
                    $modelFile->id_type_file_fk = 11;
                    $modelFile->id_dict_type_file_fk = 0;
                    $modelFile->type_fk = 0;
                    
                    if($modelFile->save()) {
                        /*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
                        try {
                            $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                        } catch (\Exception $e) {
                            var_dump($e); exit;
                        }
                    }
                }
            }
        } 
		
        if($this->is_active) {
            $sqlInstances = "update {{%cal_case_instance}} set is_active = 0 where status = 1 and id != ".$this->id." and id_case_fk = ".$this->id_case_fk;
            $result = Yii::$app->db->createCommand($sqlInstances)->execute();
            
            $sqlCase = "update {{%cal_case}} "
                            ." set a_instance_sygn = '".( ($this->instance_sygn) ? $this->instance_sygn : '' )."'"
                            .", a_instance_address_fk = ".( ($this->instance_address_fk) ? $this->instance_address_fk : 0)
                            .", a_instance_name = '".( ($this->instance_name) ? $this->instance_name : '')."' "
                        ." where id = ".$this->id_case_fk;
            $result = Yii::$app->db->createCommand($sqlCase)->execute();
        }

		if(Yii::$app->params['env'] == 'dev') {
			$sqlUpdate = "update {{%cal_case}} "
							." set authority_carrying = (select GROUP_CONCAT(instance_sygn) from {{%cal_case_instance}} where id_case_fk = ".$this->id_case_fk.")"
							." where id = ".$this->id_case_fk;
		    $result = Yii::$app->db->createCommand($sqlUpdate)->execute();
		}
 
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function getCase()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_case_fk']);
    }
    
    public static function listStatuses() {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['status' => 1, 'id_dictionary_fk' => 16])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public static function listActive() {
        return ['1' => 'Aktwyne', 2 => 'Zamknięte'];
    }
    
    public static function listModes() {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['status' => 1, 'id_dictionary_fk' => 17])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public function getAddress() {
        $sql = "select fulladdress(".$this->instance_address_fk.") as fullname";
        return \Yii::$app->db->createCommand($sql)->queryOne();
    }
    
    public static function getList($id) {
        return CalCaseInstance::find()->where(['status' => 1, 'id_case_fk' => $id])->orderby('id')->all();
    }
    
    public static function buildSelectList($id) {
        $instancesData = \backend\Modules\Task\models\CalCaseInstance::find()->where(['status' => 1, 'id_case_fk' => $id])->all();
        $instances = '';
        if($instancesData) {
            foreach($instancesData as $key => $value) {
                $instances .= '<option value="'.$value->id.'" '.(($value->is_active) ? 'selected' : '').'>'.$value->instance_name.' [sygn: '.(($value->instance_sygn) ? $value->instance_sygn : 'brak sygn.').']'.'</option>';
            }
        }
    
        return $instances;
    }
	
	public function getWarrant()  {
		return \backend\Modules\Task\models\CaseInstanceWarrant::find()->where(['status' => 1, 'id_case_instance_fk' => $this->id])->one();
    }
    
    public function getClause()  {
		return \backend\Modules\Task\models\CaseInstanceClause::find()->where(['status' => 1, 'id_case_instance_fk' => $this->id])->one();
    }
    
    public function getFullname() {
        return $this->instance_name.' [sygn: '.(($this->instance_sygn) ? $this->instance_sygn : 'brak sygn').']';
    }
	
	public function getFiles() {
        $filesData = [];
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 11])->orderby('id desc')->all(); 
		
		return $filesData;

    }
}
