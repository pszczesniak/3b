<?php

namespace backend\Modules\Task\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cal_merge}}".
 *
 * @property integer $id
 * @property integer $id_case_fk
 * @property integer $id_merge_fk
 * @property string $note
 * @property string $created_at
 * @property integer $created_by
 */
class CalMerge extends \yii\db\ActiveRecord
{	
	public $id_customer_fk;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cal_merge}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_case_fk', 'cases'], 'required'],
            [['id_case_fk', 'created_by'], 'integer'],
            [['note'], 'string'],
            [['created_at', 'cases'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_case_fk' => 'Sprawa',
            'cases' => 'Id Merge Fk',
			'id_customer_fk' => 'Klient',
            'note' => 'Note',
            'created_at' => 'Scalono',
            'created_by' => 'Scalił(a)',
        ];
    }
	
	public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getItems() {
		return \backend\Modules\Task\models\CalCase::find()->where('id in ('.$this->cases.')')->all();
	}
	
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
	
	public function getRemoving() {
		return $user = \common\models\User::findOne($this->removed_by);
	}
}
