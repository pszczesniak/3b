<?php

namespace backend\Modules\Task\models;

use Yii;

/**
 * This is the model class for table "{{%case_label}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_company_branch_fk
 * @property integer $id_department_fk
 * @property integer $next_number
 * @property integer $next_number_arch
 * @property string $free_numbers
 * @property string $free_numbers_arch
 * @property string $template_str
 * @property string $custom_data
 * @property string $created_at
 * @property integer $created_by
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CaseLabel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case_label}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_company_branch_fk', 'id_department_fk'], 'required'],
            [['type_fk', 'id_company_branch_fk', 'id_department_fk', 'next_number', 'next_number_arch', 'created_by', 'status', 'deleted_by'], 'integer'],
            [['template_str', 'custom_data'], 'string'],
            [['free_numbers', 'free_numbers_arch', 'created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_company_branch_fk' => Yii::t('app', 'Id Company Branch Fk'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'next_number' => Yii::t('app', 'Next Number'),
            'next_number_arch' => Yii::t('app', 'Next Number Arch'),
            'free_numbers' => Yii::t('app', 'Free Numbers'),
            'free_numbers_arch' => Yii::t('app', 'Free Numbers Arch'),
            'template_str' => Yii::t('app', 'Template Str'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
