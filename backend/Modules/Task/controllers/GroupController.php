<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use backend\Modules\Task\models\CalMerge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * GroupController implements the CRUD actions for CalCase model.
 */
class GroupController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalMerge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
      
    public function actionIndex()
    {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalMerge();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.merge'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['CalMerge'])) {
            $params = $_GET['CalMerge'];
            \Yii::$app->session->set('search.merge', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
                //array_push($where, "json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk != 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4");
        }
		
		if(isset($_GET['type']) && $_GET['type'] == 'merge') {
			array_push($where, "id_dict_case_status_fk != 6");
		}
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.created_at';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.note as note', 'm.created_at as mdate', "concat_ws(' ', ce.lastname, ce.firstname)  as memployee", 'm.status', 'cc.id as ccid', 'cc.no_label', 'cc.name as ccname', 'ifnull(c.symbol, c.name) as cname', 'c.id as cid'])
            ->from('{{%cal_case}} as cc')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = cc.id_customer_fk')
			->join(' JOIN', '{{%cal_merge}} as m', 'm.id_case_fk = cc.id')
			->join(' JOIN', '{{%company_employee}} as ce', 'm.created_by = ce.id')
            ->where( ['cc.type_fk' => 1] );

        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($rows as $key=>$value) {
			//if($value['id_dict_case_status_fk'] == 6) $value['id'] = $value['id_parent_fk'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.$value['cname'].'</a>';            
			$tmp['name'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['ccid']]).'" >'.$value['ccname'].'</a>';
			$tmp['mdate'] = $value['mdate'];
			$tmp['memployee'] = $value['memployee'];
            //$tmp['status'] = '<label class="label '.$colors[$value['id_dict_case_status_fk']].'">'.$status[$value['id_dict_case_status_fk']].'</label>';
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= ( $value['status'] == 1 ) ? '<a href="'.Url::to(['/task/group/unmerge', 'id' => $value['id']]).'" class="btn btn-xs btn-icon bg-grey remove" data-label="Rozdziel sprawy" data-table="#table-matters"><i class="fa fa-object-ungroup" title="Rozdziel sprawy"></i>Rozdziel sprawy</a>'
															: '<span class="label label-danger">operacja wycofana</span>';
			$tmp['actions'] .= '</div>';
			$tmp['typeFormatter'] = 'merge';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
            $tmp['nid'] = $value['id'];
			$tmp['className'] = ( $value['status'] == -1 ) ? 'danger' : 'default'; 
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
	
	public function actionCases($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$merge = CalMerge::findOne($id);
        
		if($merge->status == 1)  
			$html = '';
		else
			$html = '<div class="alert alert-danger">Operacja odwrócenia scalania została wykonana '.$merge->removed_at.' przez '.$merge->removing['fullname'].' </div>';
        $html .= '<table class="table-logs">';
        $html .= '<thead><tr><th>#</th><th>Nazwa</th></tr><thead><tbody>';
        
        foreach($merge->items as $key => $item) {
            $html .= '<tr><td>'.($key+1).'</td><td>'.$item->name.'</td></tr>';
        }
        
        $html .= '</tbody></table>';
               
        return [ 'html' => $html ];
    }
	
	public function actionUnmerge($id)  {
        //$this->findModel($id)->delete();
        if( count(array_intersect(["caseUnmerge", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
		if($model->status == -1) {
			if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Operacja odwrócenia scalania na tym zestawie spraw została już wykonana')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Operacja odwrócenia scalania na tym zestawie spraw została już wykonana', 'id' => $id, 'table' => '#table-matters');	
			}
		}
		
        $model->status = -1;
        //$model->user_action = 'delete';
		$model->removed_at = date('Y-m-d H:i:s');
        $model->removed_by = \Yii::$app->user->id;
        
        if($model->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
			try {
				$model->save();
				$cases_arch = [];
				foreach($model->items as $key => $value) {
					if($value->id != $model->id_case_fk) {
						array_push($cases_arch, $value->id);
		
						CalTask::updateAll(['id_case_fk' => $value->id], 'status = 1 and id_original_fk = :case', [':case' => $value->id]);
						\common\models\Files::updateAll(['id_fk' => $value->id], 'status = 1 and id_type_file_fk = 3 and id_original_fk = :case', [':case' => $value->id]);
						//\backend\Modules\Task\models\TaskEmployee::updateAll(['id_case_fk' =>  $value->id_original_fk], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);
						//\backend\Modules\Correspondence\models\CorrespondenceTask::updateAll(['id_case_fk' => $parent->id], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);	
					}
				}
				$sqlTaskUpdate = "update {{%task_employee}} ct "
								." set id_case_fk = (select id_case_fk from {{%cal_task}} t where t.id = ct.id_task_fk) where id_case_fk = ".$model->id_case_fk;
				\Yii::$app->db->createCommand($sqlTaskUpdate)->execute(); 
				$sqlCorrespondenceUpdate = "update {{%correspondence_task}} ct "
								." set id_case_fk = id_original_fk where id_original_fk is not null and id_case_fk = ".$model->id_case_fk;
				\Yii::$app->db->createCommand($sqlCorrespondenceUpdate)->execute(); 
				
				$sqlInsertCorrespondence = " insert into {{%correspondence_task}}(id_correspondence_fk, id_case_fk, id_task_fk, status, created_at, created_by) "
											." select ct.id_correspondence_fk,cc.id,-1 as id_task_fk,1 status, now(), ".\Yii::$app->user->id." created_by"
											." from {{%correspondence_task}} ct right join {{%cal_case}} cc on cc.id in (".$model->cases.") "
											." where ct.created_at > '".$model->created_at."' and ct.status=1 and id_task_fk = -1 and ct.id_case_fk = ".$model->id_case_fk." and cc.id != ".$model->id_case_fk;
				\Yii::$app->db->createCommand($sqlInsertCorrespondence)->execute();
				$sqlInsertFile = "insert into {{%files}}(id_type_file_fk,id_fk,id_dict_type_file_fk,title_file,name_file,systemname_file,extension_file,mime_file,size_file,status,created_at,created_by) "
								." select id_type_file_fk,cc.id as id_fk,id_dict_type_file_fk,title_file,name_file,systemname_file,extension_file,mime_file,size_file,f.status,now(), ".\Yii::$app->user->id." as created_by"
								." from law_files f right join law_cal_case cc on cc.id in (".$model->cases.") "
								." where f.created_at > '".$model->created_at."' and f.status=1 and id_type_file_fk = 3 and f.id_fk = ".$model->id_case_fk." and cc.id != ".$model->id_case_fk;
				\Yii::$app->db->createCommand($sqlInsertFile)->execute();
				CalCase::updateAll(['id_dict_case_status_fk' => 1], 'id in ('.implode(',', $cases_arch).')', []);
		
				$modelArch = new \backend\Modules\Task\models\CalCaseArch();
				$modelArch->id_case_fk = $model->id_case_fk;
				$modelArch->user_action = 'unmerge';
				$modelArch->data_change = \yii\helpers\Json::encode([]);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = date('Y-m-d H:i:s');
				$modelArch->save();
				$transaction->commit();
				if(!Yii::$app->request->isAjax) {
					Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Operacja scalania została odwrócona')  );
					return $this->redirect(['index']);
				} else {
					Yii::$app->response->format = Response::FORMAT_JSON;
					return array('success' => true, 'alert' => 'Operacja scalania została odwrócona.', 'id' => $id, 'table' => '#table-matters');	
				}
			} catch (Exception $e) {
                $transaction->rollBack();
				return array('success' => false, 'alert' => 'Operacja scalania została odwrócona', 'id' => $id, 'table' => '#table-matters');	
            }

        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Operacja scalania została odwrócona')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Operacja scalania została odwrócona', 'id' => $id, 'table' => '#table-matters');	
			}
        }
    }
}
