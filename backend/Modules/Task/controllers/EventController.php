<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalTaskSearch;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Company\models\CompanyEmployee;
use backend\Modules\Correspondence\models\CorrespondenceTask;
use backend\Modules\Company\models\CompanyResourcesSchedule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * EventController implements the CRUD actions for CalTask model.
 */
class EventController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalTask models.
     * @return mixed
     */
    public function actionIndex() {
        
		if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$searchModel = new CalTaskSearch();
        $searchModel->id_dict_task_status_fk = 1;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.events'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = $this->module->params['grants'];
		$where = [];  $post = $_GET;
        
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch'];
            \Yii::$app->session->set('search.events', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(t.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            //if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {  }
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                array_push($where, "id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "t.id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                array_push($where, "t.id_case_fk = ".$params['id_case_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "event_date <= '".$params['date_to']."'");
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                array_push($where, "t.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
        } else {
             array_push($where, "id_dict_task_status_fk = 1");
        }
        
		//if(count(array_intersect(["grantAll"], $grants)) == 0) { }  
        
        
		$fields = []; $tmp = [];
        $status = CalTask::listStatus();  $categories = CalTask::listCategory();
        $types = CalTask::listTypes(8);
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green'];

		$sortColumn = 't.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 't.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'cc.name';
        if( isset($post['sort']) && ($post['sort'] == 'event_term' || $post['sort'] == 'delay')) $sortColumn = 't.event_date '.(isset($post['order']) ? $post['order'] : 'asc').',t.event_time';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'id_dict_task_category_fk';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'id_dict_task_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['t.id as id', 't.type_fk', 't.name as name', 'id_dict_task_status_fk', 't.status as status', 'event_date', 'event_time', 'execution_time', 'id_dict_task_status_fk', 'id_dict_task_category_fk', 'id_dict_task_type_fk',
					  'c.name as cname', 'c.id as cid', 'cc.name as ccname', 'cc.id as ccid', 'cc.type_fk as ctype', 't.show_client as show_client'])
            ->from('{{%cal_task}} as t')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = t.id_case_fk')
            ->join('LEFT JOIN', '{{%customer}} as c', 'c.id = cc.id_customer_fk')
            ->where( ['t.status' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as te', 't.id_case_fk = te.id_case_fk and te.status=1 and te.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
            $query = $query->join(' JOIN ', '{{%case_employee}} as ete', 't.id_case_fk = ete.id_case_fk and ete.status=1 and ete.id_employee_fk = '.$params['id_employee_fk']);
        }
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );

        $rows = $query->all();
		foreach($rows as $key=>$value) {		
			$tmp['name'] = '<a href="'.Url::to(['/task/event/view','id' => $value['id']]).'"  data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['event_term'] = $value['event_date'] . ' ' . (($value['event_time'] && $value['event_time'] != '00:00') ? $value['event_time'] : '');
            $tmp['event_time'] = $value['event_time'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['cid']]).'"  title="Karta klienta" >'.$value['cname'].'</a>';
            $tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['ctype'] == 1) ? 'matter' : 'project').'/view','id' => $value['ccid']]).'"  title="Karta sprawy/projektu" >'.$value['ccname'].'</a>';
            $tmp['status'] = isset($status[$value['id_dict_task_status_fk']]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value['id_dict_task_status_fk']].'<span>' : '';
            $tmp['type'] = isset($types[$value['id_dict_task_type_fk']]) ? $types[$value['id_dict_task_type_fk']] : '';
            $tmp['show'] = ($value['show_client']) ? '<i class="fa fa-eye text--blue" data-toggle="tooltip" data-title="Widoczne dla klienta"></i>' : '<i class="fa fa-eye-slash text--yellow" data-toggle="tooltip" data-title="Ukryte przed klientem"></i>';
            $tmp['group'] = ($value['type_fk'] == 1) ? '<span class="label bg-purple">Zadanie</span>' : '<span class="label bg-blue">Informacja</span>';
            $tmp['delay'] = ($value['id_dict_task_status_fk']==1) ? '<span class="small-text">'.Yii::$app->runAction('common/ago', ['date' => $value['event_date'].' '.(($value['event_time'])?$value['event_time']:'08:00').':00']).'</span>':'';//$value->delay;
            //$tmp['className'] = ($value['id_dict_task_status_fk'] == 1 && $value->delay > 1) ?  'danger' : 'normal';
            $tmp['className'] = ($value['id_dict_task_status_fk'] == 1 && time() > (strtotime($value['event_date'].' '.(($value['event_time'])?$value['event_time']:'08:00').':00')) ) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2 && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
			
			$tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . '<li><a href="'.Url::to(['/task/event/view', 'id' => $value['id']]).'" title="Karta zdarzenia"><i class="fa fa-vcard text--teal"></i>&nbsp;Szczegóły</a></li>'
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/event/update', 'id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ? '<li class="deleteConfirm" data-table="#table-cases" href="'.Url::to(['/task/event/closeajax', 'id' => $value['id']]).'" data-label="Zamknij zadanie" data-title="<i class=\'fa fa-check\'></i>'.Yii::t('app', 'Zamknij zadanie').'"><a href="#"><i class="fa fa-check text--green"></i>&nbsp;Zamknij</a></li>' : '')                                        
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ? '<li class="deleteConfirm" data-table="#table-cases" href="'.Url::to(['/task/event/cancelajax', 'id' => $value['id']]).'" data-label="Odwołaj" data-title="<i class=\'fa fa-ban\'></i>'.Yii::t('app', 'Odwołaj').'"><a href="#"><i class="fa fa-ban text--orange"></i>&nbsp;Odwołaj</a></li>' : '')                                        
                                        . ( (count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-cases" href="'.Url::to(['/task/event/delete', 'id' => $value['id']]).'" data-label="Usuń" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" href="'.Url::to(['/task/event/sclient', 'id' => $value['id']]).'" data-table="#table-cases" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><a href="#" ><i class="fa fa-eye'.(($value['show_client'])?'-slash':'').' text--'.(($value['show_client'])?'yellow':'blue').'"></i>&nbsp;'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'</a></li>' : '')
         
                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
            /*$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/event/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ?  '<a href="'.Url::to(['/task/event/closeajax', 'id' => $value['id']]).'" class="btn btn-xs btn-success deleteConfirm" data-label="Zamknij zadanie" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Zamknij zadanie').'"><i class="fa fa-check"></i></a>': '';
                $tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/event/update', 'id' => $value['id']]).'" class="btn btn-xs btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/event/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
	
	
	/**
     * Calendar all CalTask models.
     * @return mixed
     */
    public function actionCalendar()  {

        return $this->render('calendar', [  ]);
    }
    
    private function lists($id, $type) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
        $model = \backend\Modules\Task\models\CalCase::findOne($id);
        $departments = []; $ids = []; array_push($ids, 0); $employees = []; $contacts = [];
        $checked = 'checked';

        /*if($employee) {
            if($employee->is_admin) {
                $employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
            } else {
                $employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();            
            }
        } else {    
            $employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        }*/
        if($model) {
            if($type == 2) {
                $sql = "select distinct id_employee_fk, fullname from ( "
                        . " select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname"
                        . " from {{%case_employee}} ce join {{%company_employee}} e on ce.id_employee_fk = e.id"
                        . " where ce.status = 1 and e.status = 1 and is_show = 1 and id_case_fk = ".$model->id
                        . " union "
                        . " select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname"
                        . " from {{%company_employee}} ce join {{%employee_department}} ed on ed.id_employee_fk = ce.id "
                        . " where ce.status = 1 and id_department_fk in (select id from {{%company_department}} where all_employee = 1)"
                        . " ) t order by 2 ";
            } else {
                 $sql = "select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname"
                        . " from {{%case_employee}} ce join {{%company_employee}} e on ce.id_employee_fk = e.id"
                        . " where ce.status = 1 and e.status = 1 and is_show = 1 and id_case_fk = ".$model->id;
            }
            $employeesData = Yii::$app->db->createCommand($sql)->queryAll();
            foreach($employeesData as $key => $item) {
                //if($model->id == $this->module->params['employeeId'])  $checked = 'checked'; else $checked = '';
                //$employees_list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'><label for="d'.$model->id.'">'.$model->fullname.'</label></li>';
                $employees[$item['id_employee_fk']] = $item['fullname'];
            }
        }
        $list = '';
        
        $cases = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $model->id_customer_fk])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($cases as $key => $model) {
            //$list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
            $contacts[$model->id] = $model->fullname;
        }
        
        return ['contacts' => $contacts, 'departments' => $departments, 'employees' => $employees];
    }
	

    /**
     * Displays a single CalTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        $model = $this->findModel($id);
		if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if(!$this->module->params['isAdmin'] && !$this->module->params['isSpecial']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id_case_fk])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego zadania.');
            }
        }
		
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        return $this->render('view', [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }
 
    /**
     * Creates a new CalTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNew($id) {
		
		$grants = $this->module->params['grants'];
        $departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']); $lists = ['employees' => []];
        
        $id = CustomHelpers::decode($id);
        
        if( !isset($_POST['CalTask']['id']) || empty($_POST['CalTask']['id']) ) {
            $model = new CalTask();
            $model->type_fk = 1;
            $model->id_case_fk = ($id != 0) ? $id : null;
            $model->name = 'Wersja robocza '.time();
            $model->status = -2;
            $model->save();
            $model->name = '';
        } else {
            $model = CalTask::findOne($_POST['CalTask']['id']);
        }
        
        $model->scenario = CalTask::SCENARIO_EVENT;
        $model->user_action = 'create';
        $model->id_company_branch_fk = $this->module->params['employeeBranch'];
        $model->all_day = 1;
        $model->id_dict_task_status_fk = 1;
        //$model->id_dict_task_type_fk = 1;
        $model->id_dict_task_category_fk = 2;
        /*$model->date_from = date('Y-m-d').' 08:00';
        $model->date_to = date('Y-m-d').' 08:05';*/
        $model->event_date = ( isset($_GET['start']) && !empty($_GET['start']) ) ? gmdate('Y-m-d', ($_GET['start'])) : date('Y-m-d');
        //$model->event_deadline = ( isset($_GET['start']) && !empty($_GET['start']) ) ? gmdate('Y-m-d', ($_GET['start'])).' 16:00' : date('Y-m-d').' 16:00';
        $model->fromDate = $model->event_date;
        $model->toDate = $model->event_date;
        $model->event_time = ( isset($_GET['start']) && !empty($_GET['start']) ) ? gmdate('H:i', ($_GET['start'])) : '';
        
        if($model->event_time == '00:00') $model->event_time = '';
        
        if($id != 0) {
            $case = \backend\Modules\Task\models\CalCase::findOne($id);
            $model->id_case_fk = $id;
            $model->id_customer_fk = $case->id_customer_fk;

            foreach($case->employees as $key => $item) {
                array_push($employees, $item['id_employee_fk']);
            }
        } else {
            if( isset($_GET['cid']) && !empty($_GET['cid']) ) {
                $id = $_GET['cid'];
                $case = \backend\Modules\Task\models\CalCase::findOne($id);
                $model->id_case_fk = $id;
                $model->id_customer_fk = $case->id_customer_fk;
                
                foreach($case->employees as $key => $item) {
                    array_push($employees, $item->id_employee_fk);
                }
            }
        }       
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->date_from = $model->event_date.' '. ( ($model->event_time) ? $model->event_time.':00' : '00:00:00' );
            $model->execution_time = $model->execution_time ? $model->execution_time : 0;
            $model->id_employee_fk = is_array($model->employees_list) ? count($model->employees_list) : 0;
			
            //$model->date_from = $model->event_date.' '.$model->event_time.':00';
            //$model->date_to = ($model->event_deadline) ? $model->event_deadline : null;
            if($model->all_day == 1) {
                $model->date_from = (!$model->fromDate) ? false : ( $model->fromDate . ' 00:00:00' );
                $model->date_to = (!$model->toDate) ? false : ( $model->toDate . ' 00:00:00' );
            } else {
                $model->date_from = (!$model->fromDate || !$model->fromTime) ? false : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                $model->date_to = (!$model->toDate || !$model->toTime) ? false : ( $model->toDate . ' ' . $model->toTime . ':00' );
            }
            if(!$model->date_from) $model->date_from = $model->event_date.' '. ( ($model->event_time) ? $model->event_time : '00:00' ) .':00';            
            if(!$model->date_to) $model->date_to = $model->event_date.' '. ( ($model->event_time) ? $model->event_time : '00:00' ) .':00';
			
            /*if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            } */
            
             /*reminder start*/
            $model->reminder_user = \Yii::$app->user->id;
            if($model->event_deadline) {
                $deadline = $model->event_deadline;
            } else {
                $deadline = $model->date_from;
            }

            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
                $model->reminder_user = \Yii::$app->user->id;
            }
            /*reminder end*/
            if($model->id_case_fk)
                $lists = $this->lists($model->id_case_fk, $model->type_fk);
            if($model->validate() && $model->save()) {				
				//CaseEmployee::deleteAll('id_case_fk = :case', [':case' => $id]);
				if( is_array($model->employees_list) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($model->employees_list as $key => $value) { 
                        $model_ce = new TaskEmployee();
                        $model_ce->id_task_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
				} 
				
				return array('success' => true, 'refresh' => 'yes', 'index' => 0, 'id' => $model->id, 'name' => $model->name, 'table' => '#table-events', 'calendar' => '#schedule'  );	
			} else {
                $model->status = -2;
                return array('success' => false, 'html' => $this->renderAjax(((($id == 0) ? '_formAjaxNew' : '_formCaseNew').((Yii::$app->params['env'] == 'dev') ? '_new' : '')), [  'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind'], 'docs' => ( isset($_GET['docs']) ? $_GET['docs'] : true ) ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			if($model->id_case_fk)
                $lists = $this->lists($model->id_case_fk, $model->type_fk);
            return  $this->renderAjax(((($id == 0) ? '_formAjaxNew' : '_formCaseNew').((Yii::$app->params['env'] == 'dev') ? '_new' : '')), [ 'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind'], 'docs' => ( isset($_GET['docs']) ? $_GET['docs'] : true ) ]) ;	
		}
	}
    
    public function actionShow($id) {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        $employeesList = '';
        $grants = $this->module->params['grants'];
        $meEvent = 0;
        if($this->module->params['isSpecial'] || $this->module->params['isAdmin']) $meEvent = 1;
                
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        $model->employees_list = '<ol>';
        foreach($model->employees as $key => $employee) {
            $model->employees_list .= '<li>'.$employee['fullname'].'</li>';   
        }
        $model->employees_list .= '</ol>';        
        return $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_viewAjaxNew_new':'_viewAjaxNew'), [
            'model' => $model,  'edit' => isset($grants['caseEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'personal' => ( isset($_GET['personal']) ? 1 : 0), 'meEvent' => $meEvent 
        ]);
    }
    
    public function actionShowajax($id) {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        $employeesList = '';
        $grants = $this->module->params['grants'];
        $meEvent = 0;
        if($this->module->params['isSpecial'] || $this->module->params['isAdmin']) $meEvent = 1;
                
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        $model->employees_list = '<ol>';
        foreach($model->employees as $key => $employee) {
            $model->employees_list .= '<li>'.$employee['fullname'].'</li>';   
        }
        $model->employees_list .= '</ol>';        
        return $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_viewAjaxNew_new':'_viewAjaxNew'), [
            'model' => $model,  'edit' => isset($grants['caseEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'personal' => ( isset($_GET['personal']) ? 1 : 0), 'meEvent' => $meEvent 
        ]);
    }

    /**
     * Updates an existing CalTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        if( count(array_intersect(["eventEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$model = $this->findModel($id);
        $caseTemp = $model->id_case_fk;        
        if(!$this->module->params['isAdmin'] && !$this->module->params['isSpecial']){
            $checked = \backend\Modules\Task\models\TaskEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_task_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego zadania.');
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        $model->scenario = CalTask::SCENARIO_EVENT;
        
        $departments = []; $employees = [];
		$status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        $grants = $this->module->params['grants'];
        $lists = [];

        foreach($model->employees as $key => $item) {
            array_push($employees, $item['id_employee_fk']);
        }
        
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($caseTemp == $model->id_case_fk) $model->is_ignore = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            
            /*$model->date_from = $model->event_date.' '.$model->event_time.':00';
            $model->date_to = ($model->event_deadline) ? $model->event_deadline : null;*/
            if($model->all_day == 1) {
                $model->date_from = (!$model->fromDate) ? false : ( $model->fromDate . ' 00:00:00' );
                $model->date_to = (!$model->toDate) ? false : ( $model->toDate . ' 00:00:00' );
            } else {
                $model->date_from = (!$model->fromDate || !$model->fromTime) ? false : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                $model->date_to = (!$model->toDate || !$model->toTime) ? false : ( $model->toDate . ' ' . $model->toTime . ':00' );
            }
            if(!$model->date_from) $model->date_from = $model->event_date.' '. ( ($model->event_time) ? $model->event_time : '00:00' ) .':00';            
            if(!$model->date_to) $model->date_to = $model->event_date.' '. ( ($model->event_time) ? $model->event_time : '00:00' ) .':00';
			
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            } 
            
             /*reminder start*/
            $model->reminder_user = \Yii::$app->user->id;
            if($model->event_deadline) {
                $deadline = $model->event_deadline;
            } else {
                $deadline = $model->event_date.' '. ( ($model->event_time) ? $model->event_time : '00:00' ) .':00';
            }
               
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
            }
            if($model->id_case_fk)
                $lists = $this->lists($model->id_case_fk, $model->type_fk);
            else
                $lists = ['contacts' => [], 'departments' => [], 'employees' => []];
            /*reminder end*/
			if($model->validate() && $model->save()) {

				return array('success' => true,  'action' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało zaktualizowane', 'reminder_date' => ($model->reminded == 1) ? $model->reminder_date : 'nie ustawiono' );
			} else {
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formnew' : '_form'), [  'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind'] ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			$lists = $this->lists($model->id_case_fk, $model->type_fk);
            return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'updatenew' : 'update'), ['model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
    }
    

    /**
     * Deletes an existing CalTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $correspondence = CorrespondenceTask::find()->where(['id_task_fk' => $model->id, 'status' => 1])->andWhere('id_correspondence_fk in (select id from {{%correspondence}} where status = 1)')->count();
        
        if($correspondence != 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Zadanie <b>'.$model->name.'</b> nie może zostać usunięte, ponieważ ma przypisaną korespondencję!')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> nie może zostać usunięte, ponieważ ma przypisaną korespondencję!<br />', 'id' => $id, 'table' => '#table-cases');	
			}
        }
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie <b>'.$model->name.'</b> zostało usunięte')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało usunięte.<br/>', 'id' => $id, 'table' => '#table-cases');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte.<br />', 'id' => $id, 'table' => '#table-cases');	
			}
        } 
    }
    
    public function actionDeleteajax($id)  {
        //$this->findModel($id)->delete();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $correspondence = CorrespondenceTask::find()->where(['id_task_fk' => $model->id])->count();
        
        if($correspondence != 0) {
			return array('success' => false, 'alert' => 'Zdarzenie <b>'.$model->name.'</b> nie może zostać usunięte, ponieważ posiada przypisaną korespondencję!', 'id' => $id, 'action' => 'delete', 'table' => '#table-cases');	
        }
        if($model->save()) {
            return array('success' => true, 'alert' => 'Zdarzenie <b>'.$model->name.'</b> zostało usunięte', 'id' => $model->id, 'action' => 'delete', 'table' => '#table-cases');	
        } else {
            return array('success' => false, 'alert' => 'Zdarzenie nie <b>'.$model->name.'</b> zostało usunięte', 'id' => $model->id, 'action' => 'delete', 'table' => '#table-cases');	
        }
        
    }
    
    public function actionClose($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 2;
        $model->user_action = 'close';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie zostało zamknięte')  );
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
    public function actionOpen($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 1;
        $model->user_action = 'close';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie zostało ponownie otwarte')  );
        return $this->redirect(['update', 'id' => $model->id]);
    }
    
    public function actionCloseajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTodo::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 2;
        $model->user_action = 'close';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        
        $color = 'default'; $icon = 'tasks';
        if($model->type_fk == 1) { $color = 'purple'; $icon = 'gavel'; }
        if($model->type_fk == 2) { $color = 'teal'; }
        $btn = '<div class="btn-group pull-right">'
                    .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                        .'Opcje <span class="caret"></span>'
                    .'</button>'
                    .'<ul class="dropdown-menu">'
                        . (($model->id_dict_task_status_fk==1) ? '<li><a href="'.Url::to(['/task/event/closeajax', 'id' => $model->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                        .'<li><a href="'.Url::to(['/task/event/showajax', 'id'=>$model->id]).'" data-target="#modal-grid-item" class="gridViewModal" title="Podglad zadania"><i class="fa fa-eye text--blue2"></i>Podgląd zadania</a></li>'
                    .'</ul>'
                .'</div><div class="clear"></div>';
        $bg = '';
        $bg = " bg-green2";
        $eventItem = '<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.$model->event_time.$btn.'</h4>'.$model->name;
        return ['result' => true, 'alert' => ($model->type_fk == 1) ? 'Rozprawa została zamknięta' : 'Zadanie zostało zamknięte', 'id' => $model->id, 'action' => 'close', 'eventItem' => $eventItem, 'table' => '#table-cases'];
    }
    
    public function actionCancelajax($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTodo::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 3;
        $model->user_action = 'cancel';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        return ['result' => true, 'alert' => ($model->type_fk == 1) ? 'Rozprawa przeszła w status <b>`odwołana`</b>' : 'Rozprawa przeszła w status <b>`odwołane`</b>', 'id' => $model->id, 'action' => 'close'];
    }

    /**
     * Finds the CalTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 2;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_task_fk = $model->id;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                $modelArch = new \backend\Modules\Task\models\CalTaskArch();
                $modelArch->id_task_fk = $model->id;
                $modelArch->id_case_fk = $model->id_case_fk;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->save();
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }

    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Zadania');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Data')
            ->setCellValue('B3', 'Godzina rozpoczęcia')
            ->setCellValue('C3', 'Temat zadania')
			->setCellValue('D3', 'Klient')
			->setCellValue('E3', 'Sprawa')
			->setCellValue('F3', 'Miejsce')
			->setCellValue('G3', 'Opis');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:G3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = [];
		
		$fieldsDataQuery = CalTask::find()->where(['status' => 1, 'type_fk' => 2]);
        
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_task_fk from law_task_employee where id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_case_fk = ".$params['id_case_fk']);
            }
           /* if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
            }*/
           /* if( isset($params['filter_status']) && $params['filter_status'] == 1) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
            } else {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 2");
            }*/
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date <= '".$params['date_to']."'");
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
        } else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
        }
        
		if(count(array_intersect(["grantAll"], $grants)) == 0) {
		//	if($this->module->params['employeeKind'] >= 70)
			//	$fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			//else 
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }  
        
        $data = $fieldsDataQuery->orderby('date_from')->all();
			
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->event_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, ( empty($record->event_time) || $record->event_time == '00:00' ) ? 'cały dzień' : $record->event_time ); 
				//$objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->name); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->customer['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record->case['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->place); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, str_replace(array("\r\n", "\r", "\n"), "", $record->description)); 
						
				$i++; 
			}  
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(80);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Zadania');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Zadania'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionTimeline() {
        return $this->render('_timeline');
    }
       
    public function actionSclient($id) {
        $model = $this->findModel($id);
		$model->scenario = 'show';
		$model->user_action = 'show';
        $model->show_client = ($model->show_client) ? 0 : 1;
        $model->save();
		Yii::$app->response->format = Response::FORMAT_JSON;
                    
        $info = '<a href="'. Url::to(['/task/event/sclient', 'id' => $model->id]) .'" class="btn bg-'. ((!$model->show_client) ? 'yellow' : 'blue') .' btn-flat deleteConfirm" data-label="'. ((!$model->show_client) ? 'Pokaż klientowi' : 'Nie pokazuj klientowi' ).'" data-title="'. ((!$model->show_client) ? 'Pokaż klientowi' : 'Nie pokazuj klientowi') .'" data-toggle="tooltip"><i class="fa fa-'. ((!$model->show_client) ? 'eye-slash' : 'eye' ).'"></i></a>';
        return array('success' => true, 'action' => 'showClient', 'alert' => 'Zmiany zostały zapisane', 'id'=>$model->id, 'info' => $info, 'table' => '#table-matters' );	   
	}
}

