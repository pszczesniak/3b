<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalTaskSearch;
use backend\Modules\Task\models\CalDictionary;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Company\models\CompanyEmployee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * CalendarController implements the CRUD actions for CalTask model.
 */
class CalendarController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
	{
		return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
		];
	}
    
    public function onAuthSuccess($client) {
 
        $reponse = $client->getUserAttributes(); 

        $session = \Yii::$app->session;
        //echo $client->accessToken->params['access_token']; exit;
        $token = $client->accessToken->params['access_token'];

        $session->set('google.token', $token);
        //$id = ArrayHelper::getValue($reponse , 'id');
        //$session->set('fb.id', $id);
        $session->set('google.id', $client->accessToken);
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['personal']);
       // }
    
    }
    
    public function actionGoogleout() {
        
        $session = \Yii::$app->session;
        
        $session->remove('google.token');
        $session->remove('google.id');
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['personal']);
       // }
    
    }

    /**
     * Lists all CalTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        
		/*if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        $model = new CalTaskSearch();
        return $this->render('index', [
            'grants' => $this->module->params['grants'],
			'model' => $model
        ]);
    }
    
    public function actionPersonal()  {
        
		/*if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        $model = new CalTaskSearch();
		$model->id_employee_fk = $this->module->params['employeeId'];
    
        return $this->render('personal', [
            'grants' => $this->module->params['grants'],
			'model' => $model,
            'dicts' => CalDictionary::find()->where('status >= 1')->orderby('rank_no')->all()
        ]);
    }
    
    public function actionExporttogoogle($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $client = new \Google_Client();
        $client->setApplicationName("Lawfirm");
        $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
        $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
       // $client->setRedirectUri('http://lawfirm.dev/task/calendar/personal'); //I made a file called "worked.html" in the same directory that just says "it worked!"
        $client->setAccessToken(\Yii::$app->session->get('google.token'));
        $cal = new \Google_Service_Calendar_Calendar($client);
        //var_dump($cal); exit;
        /*if ($client->getAccessToken()){*/

           /* $event = new \Google_Service_Calendar_Event();

                $event->setSummary('Halloween');
                $event->setLocation('The Neighbourhood');
                $start = new \Google_Service_Calendar_EventDateTime();
                $start->setDateTime('2016-10-31T10:00:00.000-05:00');
                $event->setStart($start);
                $end = new \Google_Service_Calendar_EventDateTime();
                $end->setDateTime('2016-10-31T10:25:00.000-05:00');
                $event->setEnd($end);
                $createdEvent = $cal->insert('kontakt.kapisoft@gmail.com', $event);*/
            $event = new \Google_Service_Calendar_Event(array(
              'summary' => 'Google I/O 2015',
              'location' => '800 Howard St., San Francisco, CA 94103',
              'description' => 'A chance to hear more about Google\'s developer products.',
              'start' => array(
                'dateTime' => '2015-05-28T09:00:00-07:00',
                'timeZone' => 'America/Los_Angeles',
              ),
              'end' => array(
                'dateTime' => '2015-05-28T17:00:00-07:00',
                'timeZone' => 'America/Los_Angeles',
              ),
              'recurrence' => array(
                'RRULE:FREQ=DAILY;COUNT=2'
              ),
              'attendees' => array(
                array('email' => 'lpage@example.com'),
                array('email' => 'sbrin@example.com'),
              ),
              'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                  array('method' => 'email', 'minutes' => 24 * 60),
                  array('method' => 'popup', 'minutes' => 10),
                ),
              ),
            ));

            $calendarId = 'kontakt.kapisoft@gmail.com';
            $event = $cal->events->insert($calendarId, $event);

        return array('success' => true, 'alert' => 'Zdarzenie <b>'.$model->name.'</b> zostało wyeksportowane do kalendarza Google', 'id' => $model->id, 'action' => 'googleExport');
       // }
    }

	public function actionScheduledata() {
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$res = [];
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        /* gmdate('Y-m-d', $_GET['start']) */        
        $events = CalTask::find()->where(['status' => '1']);
        $events = $events->andWhere('event_date >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
		$events = $events->andWhere('event_date <= \''. gmdate('Y-m-d', $_POST['end']).'\'');

        if(isset($_POST['typeEvent']) && !empty($_POST['typeEvent']) ) { 
            if($_POST['typeEvent'] != 0)
                $events = $events->andWhere('type_fk = '.$_POST['typeEvent']);
        } 
        if(isset($_POST['statusEvent']) && !empty($_POST['statusEvent']) ) { 
            if($_POST['statusEvent'] != 0)
                $events = $events->andWhere('id_dict_task_status_fk = '.$_POST['statusEvent']);
        } 
		if(isset($_POST['department']) && !empty($_POST['department']) ) { 
            if($_POST['department'] != 0)
                $events = $events->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk = '.$_POST['department'].')');
        } 
        if(isset($_POST['employee']) && !empty($_POST['employee']) ) { 
            if($_POST['employee'] != 0)
                $events = $events->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$_POST['employee'].')');
        } 
        if(isset($_POST['branch']) && !empty($_POST['branch']) ) { 
            if($_POST['branch'] != 0)
                $events = $events->andWhere('id_company_branch_fk = '.$_POST['branch']);
        } 
       
		$events = $events->all(); 
		$colors = [0 => '#E8E8E8', 1 => '#2AA4C9', 2 => '#8447F6', 3 => '#F45246', 4 => '#47F684'];
		foreach($events as $key=>$value) {
			$bgColor = '#8447F6';
		
			array_push($res, array('title' => $value['name'], 
								   'start' => $value['event_date'].' '.(($value['event_time']) ? $value['event_time'] : '00:00').':00', 
								   //'end' => $value['date_to'],
								   'constraint' => 'freeTerm', 
								   //'color' => '#f5f5f5', 
								   'textColor' => '#fff',
								   'backgroundColor' => ($value['id_dict_task_status_fk'] == 2 && $value['type_fk'] == 2) ? '#5cb85c' : ( ($value['id_dict_task_status_fk'] == 3) ? '#d9534f' : ( ($value['type_fk'] == 1) ? '#a287d4' : "#2dc5c7") ),
								   //'className' => 'visit-free',
								   'id' => CustomHelpers::encode($value['id']),
								   'allDay' => ($value['all_day'] == 1 || !$value['event_time']) ? true : false,
								   'icon' => ($value['type_fk'] == 1) ? 'tasks' : 'comment-alt'
								)
			);
		}       
		
		return $res;
	}
	
    /**
     * Finds the CalTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 2;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_task_fk = $model->id;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }

    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności kancelaryjne');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Data')
            ->setCellValue('B3', 'Godzina rozpoczęcia')
            ->setCellValue('C3', 'Temat zadania')
			->setCellValue('D3', 'Klient')
			->setCellValue('E3', 'Sprawa')
			->setCellValue('F3', 'Miejsce')
			->setCellValue('G3', 'Opis')
            ->setCellValue('H3', 'Typ');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = [];
		
		$fieldsDataQuery = CalTask::find()->where(['status' => 1]);
        
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_task_fk from law_task_employee where id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_case_fk = ".$params['id_case_fk']);
            }
           /* if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
            }*/
           /* if( isset($params['filter_status']) && $params['filter_status'] == 1) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
            } else {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 2");
            }*/
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
               $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
               $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date <= '".$params['date_to']."'");
            }
        } else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
        }
        
		if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isSpecial'] && !$this->module->params['isAdmin']) {
		//	if($this->module->params['employeeKind'] >= 70)
			//	$fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			//else 
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }  
        
        $data = $fieldsDataQuery->orderby('event_date, event_time')->all();
			
		if(count($data) > 0) {
			foreach($data as $record){ 
				//$objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->event_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, ( empty($record->event_time) || $record->event_time == '00:00' ) ? 'cały dzień' : $record->event_time ); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->name); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->customer['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record->case['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->place); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, str_replace(array("\r\n", "\r", "\n"), "", $record->description)); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, ( ($record->id_dict_task_status_fk == 3) ? 'ODWOŁANA ' : '' ) . str_replace(array("\r\n", "\r", "\n"), "", $record->description)); 
                if($record->id_dict_task_status_fk == 3)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getFont()->getColor()->setARGB('FF0000');	
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, ($record->type_fk == 1) ? 'rozprawa' : 'zadanie' ); 
						
				$i++; 
			}  
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Czynności kancelaryjne');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Czynności_kancelaryjne'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionTimeline() {
        return $this->render('_timeline');
    }
    
    public function actionGoogle() {
        //kontakt.kapisoft@gmail.com
        //1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com 
        //Ma6OjeQJkreNBO_uRKiA6yiO 
        $client = new \Google_Client();
        $client->setApplicationName("Google Calendar PHP Starter Application");
        $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
        $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
        $client->setRedirectUri('http://lawfirm.dev/task/calendar/personal'); //I made a file called "worked.html" in the same directory that just says "it worked!"
        $client->setDeveloperKey('SecretLongDeveloperKey');
        $cal = new \Google_Service_Calendar_Calendar($client);
        
        if ($client->getAccessToken()){

            $session->set('google.token', $client->getAccessToken());
            $session->set('google.id', 1);

        }

    }
}

