<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalTodo;
use backend\Modules\Task\models\CalTodoSearch;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalTaskSearch;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccActionsSearch;
use frontend\Modules\Task\models\ActionForm;
use frontend\Modules\Task\models\ActionFast;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

use frontend\widgets\tasks\TodoWidget;
use frontend\widgets\tasks\TasksWidget;

/**
 * PersonalController implements the CRUD actions for CalTodo model.
 */
class PersonalController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalTodo models.
     * @return mixed
     */
    public function actionIndex() {
		if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$searchModel = new CalTodoSearch();
        $searchModel->id_dict_task_status_fk = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants']
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = $this->module->params['grants'];
		$fieldsDataQuery = CalTodo::find()->where(['status' => 1, 'type_fk' => 2]);
        
        if(isset($_GET['CalTodoSearch'])) {
            $params = $_GET['CalTodoSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_task_fk from law_task_employee where id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_case_fk = ".$params['id_case_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
        } else {
             $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
        }
        
		if(count(array_intersect(["grantAll"], $grants)) == 0) {
			//if($this->module->params['employeeKind'] >= 70)
			//	$fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			//else 
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }  
        
        $fieldsData = $fieldsDataQuery->all();
        
		$fields = [];
		$tmp = [];
        $status = CalTodo::listStatus();
        $categories = CalTodo::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green'];
		      
        /*$actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/new/task/event/view/%d" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/new/task/event/update/%d" class="btn btn-sm btn-default  " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/new/task/event/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';*/
		foreach($fieldsData as $key=>$value) {	
			$tmp['name'] = '<a href="'.Url::to(['/task/event/view','id' => $value->id]).'"  data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value->name.'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['deadline'] = $value['date_to'];
            $tmp['client'] = $value['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['status'] = isset($status[$value->id_dict_task_status_fk]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value->id_dict_task_status_fk].'<span>' : '';
            $tmp['type'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';

			$tmp['delay'] = $value->delay;

            //$tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
			/*$tmp['actions'] = '<div class="edit-btn-group">'								
			                        .'<a href="'.Url::to(['/task/event/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>'
									.'<a href="'.Url::to(['/task/event/update', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>'
									.'<a href="'.Url::to(['/task/event/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>'
								.'</div>';*/
            $tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/event/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/event/update', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/event/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value->id);
			
			array_push($fields, $tmp); $tmp = [];
		}
		return $fields;
	}
	
		
	public function actionScheduledata() {
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$res = [];
		
		if(isset($_POST['typeEvent']) &&  in_array($_POST['typeEvent'], [0,1] ) ) { 
            $events = CalTodo::find()->where(['status' => '1']);
			$events = $events->andWhere('DATE_FORMAT(todo_date, "%Y-%m-%d") >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
			$events = $events->andWhere('DATE_FORMAT(todo_date, "%Y-%m-%d") <= \''. gmdate('Y-m-d', $_POST['end']).'\'');
			
			if(isset($_POST['employee']) && !empty($_POST['employee']) ) { 
				if($_POST['employee'] != 0)
					$events = $events->andWhere('id_employee_fk = '.$_POST['employee']);
			} 
			if(isset($_POST['statusEvent']) && !empty($_POST['statusEvent']) ) { 
				if($_POST['statusEvent'] == 1)
					$events = $events->andWhere('is_close = 0');
				if($_POST['statusEvent'] == 2)
					$events = $events->andWhere('is_close = 1');
			} 
			if(isset($_POST['department']) && !empty($_POST['department']) ) { 
				if($_POST['department'] != 0)
					$events = $events->andWhere('id_employee_fk in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$_POST['department'].')');
			} 
			
			$events = $events->all(); 
            $personalTypes = CalTodo::dictTypes();
			foreach($events as $key=>$value) {
				$priorityColors = [ 4 => '#FC5B3F', 3 => '#f9892e', 2 => '#EFC109', 1 => '#33bfeb' ];
			
				//$tStart = strtotime($value['date_from']);
				//$tEnd = strtotime('+30 minutes', $tStart);
				if( $value['todo_time'] == '00:00' || empty($value['todo_time']) ) {
					$tStart = '00:00:00';
				} else {
					$tStart = $value['todo_time'].':00';
				}
                
                $bgColor = $personalTypes[$value->id_dict_todo_type_fk]['colorHex'];
                $bgIcon = $personalTypes[$value->id_dict_todo_type_fk]['icon'];
                
				$value['execution_time'] = $value['execution_time']*1;
				//if($value['execution_time'] == 0) $value['execution_time'] = 60;
				$tEnd = strtotime('+'.$value['execution_time'].' minutes', strtotime($value['todo_date'].' '.$tStart) );
				array_push($res, array('title' => (($value['description']) ? $value['description'] : $value['name']), 
									   'start' => $value['todo_date'].' '.$tStart, 
									   'end' => date('Y-m-d H:i:s', $tEnd), 
									   'constraint' => 'freeTerm', 
									   //'color' => '#f5f5f5', 
									   'textColor' => '#fff',
									   'backgroundColor' => $bgColor,
									   //'className' => 'visit-free',
									   'id' => CustomHelpers::encode($value['id']),
									   'allDay' => ( $value['todo_time'] == '00:00' || empty($value['todo_time']) || $value->type_fk == 4 ) ? true : false,
									   'icon' => $bgIcon
									)
				);
			}
            /*
            $actions = AccActions::find()->where(['status' => '1'])->andWhere('id_service_fk < 4');
			$actions = $actions->andWhere('DATE_FORMAT(action_date, "%Y-%m-%d") >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
			$actions = $actions->andWhere('DATE_FORMAT(action_date, "%Y-%m-%d") <= \''. gmdate('Y-m-d', $_POST['end']).'\'');
			
			if(isset($_POST['employee']) && !empty($_POST['employee']) ) { 
				if($_POST['employee'] != 0)
					$actions = $actions->andWhere('id_employee_fk = '.$_POST['employee']);
			} 
	
			if(isset($_POST['department']) && !empty($_POST['department']) ) { 
				if($_POST['department'] != 0)
					$actions = $actions->andWhere('id_department_fk = '.$_POST['department']);
                    //$actions = $actions->andWhere('id_employee_fk in (select id_employee_fk from {{%employee_department}} where id_department_fk = '.$_POST['department'].')');
			} 
			
			$actions = $actions->all(); 
            $actionDict = \backend\Modules\Accounting\models\AccService::dictTypes();
			foreach($actions as $key=>$value) {
                $bgColor = '#1B3EBA';
                $bgIcon = $actionDict[$value->id_service_fk]['icon'];
        
				array_push($res, array('title' => (($value['description']) ? $value['description'] : $value['name']), 
									   'start' => $value['action_date'], 
									   'end' => $value['action_date'], 
									   'constraint' => 'freeTerm', 
									   //'color' => '#f5f5f5', 
									   'textColor' => '#fff',
									   'backgroundColor' => $bgColor,
									   //'className' => 'visit-free',
									   'id' => CustomHelpers::encode($value['id']),
									   'allDay' => true,
									   'icon' => $bgIcon
									)
				);
			} */
            
            $query = (new \yii\db\Query())
            ->select(['mm.id as id', 'mm.id_meeting_fk', 'm.title as name', 'date_from', 'date_to'])
            ->from('{{%com_meeting}} as m')
            ->join(' JOIN', '{{%com_meeting_member}} as mm', 'm.id = mm.id_meeting_fk')
            ->where( ['m.status' => 1, 'id_employee_fk' => ( (isset($_POST['employee'])) ? $_POST['employee'] : $this->module->params['employeeId']) ] )
			->andWhere('m.status >= 0')
			//->andWhere('id_action_fk = 0 and id_task_fk = 0') 
            ->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") >= \''. gmdate('Y-m-d', $_POST['start']).'\'')
			->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") <= \''. gmdate('Y-m-d', $_POST['end']).'\'');
            
            $meetings = $query->all();
            foreach($meetings as $key => $meeting) {
                array_push($res, array('title' => $meeting['name'], 
									   'start' => $meeting['date_from'], 
									   'end' => $meeting['date_to'], 
									   'constraint' => 'freeTerm', 
									   //'color' => '#f5f5f5', 
									   'textColor' => '#fff',
									   'backgroundColor' => '#F9892E',
									   //'className' => 'visit-free',
									   'id' => CustomHelpers::encode($meeting['id_meeting_fk']),
									   //'allDay' => true,
									   'icon' => 'fa fa-handshake'
									)
				);
            }
        } 
		
		if(isset($_POST['typeEvent']) &&   in_array($_POST['typeEvent'], [0,2] ) ) {
			$events = CalTask::find()->where(['status' => '1']);
			$events = $events->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
			$events = $events->andWhere('DATE_FORMAT(date_from, "%Y-%m-%d") <= \''. gmdate('Y-m-d', $_POST['end']).'\'');

			if(isset($_POST['statusEvent']) && !empty($_POST['statusEvent']) ) { 
				if($_POST['statusEvent'] != 0)
					$events = $events->andWhere('id_dict_task_status_fk = '.$_POST['statusEvent']);
			} 
			if(isset($_POST['department']) && !empty($_POST['department']) ) { 
				if($_POST['department'] != 0)
					$events = $events->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk = '.$_POST['department'].')');
			} 
			if(isset($_POST['employee']) && !empty($_POST['employee']) ) { 
				if($_POST['employee'] != 0)
					$events = $events->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$_POST['employee'].')');
			} 
			
			$events = $events->all(); 
            $colors = [0 => '#E8E8E8', 1 => '#2AA4C9', 2 => '#8447F6', 3 => '#F45246', 4 => '#47F684'];
            foreach($events as $key=>$value) {
                $bgColor = '#8447F6';
       
                array_push($res, array('title' => $value['name'], 
                                       'start' => $value['date_from'], 
                                       //'end' => $value['date_to'],
                                       'constraint' => 'freeTerm', 
                                       //'color' => '#f5f5f5', 
                                       'textColor' => '#fff',
                                       'backgroundColor' => ($value['id_dict_task_status_fk'] == 2) ? '#5cb85c' : ( ($value['type_fk'] == 1) ? '#a287d4' : "#2dc5c7"),
                                       //'className' => 'visit-free',
                                       'id' => CustomHelpers::encode($value['id']),
                                       'allDay' => ($value['all_day'] == 0) ? false : true,
                                       'icon' => ($value['type_fk'] == 1) ? 'fa fa-tasks' : 'fa fa-comment-alt'
                                    )
                );
            }
		}
		
		return $res;
	}
    
    public function actionCreateshort() {
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = new CalTodo();
        $model->id_priority_fk = 1;
        $model->type_fk = 2;
        $model->id_dict_todo_type_fk = 6;
        $model->is_close = 0;
        $model->todo_show = 1;
		$model->id_employee_fk = $employee->id;
        $model->all_day = 1;
        $time = '00:00';
        if(isset($_GET['start'])) { 
            $model->todo_date = gmdate('Y-m-d', ($_GET['start']));
            $time = gmdate('H:i', ($_GET['start'])); 
        } else {
            $model->todo_date = date('Y-m-d');
        }
        $model->todo_time = ($time != '00:00') ? $time : '';
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            
            if($model->validate() && $model->save()) {				
				$listItem = '<li class="todo-'.$model->id.' todo-short  '.( ($model->is_close == 1) ? "completed" : '' ).'">'
                                .'<div>'
                                    .'<input type="checkbox" id="checkbox-'.$model->id.'" value="'.$model->id.'" name="check" '.( ($model->is_close == 1) ? "checked" : '' ).'>'
                                    .'<label class="checkbox" data-value="'.$model->id.'" for="checkbox-'.$model->id.'" data-action="'.Url::to(['/task/personal/state','id' => $model->id]).'">'
                                .'</div>'
                                .'<p>'.$model->name.'</p><a class="close" href="'.Url::to(['/task/personal/hide','id' => $model->id]).'"><i class="fa fa-close"></i></a>'
                            .'</li>';
                return array('success' => true, 'id' => $model->id, 'name' => $model->name, 'listItem' => $listItem, 'alert' => 'Zadanie zostało dodane', 'todoWidget' => TodoWidget::widget(['refresh' => true])  );	
			} else {
                //$model->status = -2;
                return array('success' => false,  'errors' => $model->getErrors() );	
			}		
		} else {
            return array('success' => false,  'errors' => $model->getErrors() );
        }
	}
	
	public function actionCreateajax($id) {
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = new CalTodo();
        $model->id_priority_fk = 1;
        $model->type_fk = 1;
        $model->is_close = 0;
        $model->todo_show = 1;
        $model->execution_time = 0;
		$model->id_employee_fk = $employee->id;
        $model->meeting_employees = [];
        array_push($model->meeting_employees, Yii::$app->user->id);
        
        if( isset($_GET['projectId']) ) { 
            $model->id_set_fk = $_GET['projectId']; $model->type_fk = 2; $model->id_dict_todo_type_fk = 8; $set = CalCase::findOne($model->id_set_fk); 
            $model->id_customer_fk = ($set) ? $set->id_customer_fk : 0;
            $model->id_order_fk = ($set) ? $set->id_order_fk : 0;
        }
        
        $time = '00:00';
        $model->todo_date = date('Y-m-d');
        if(isset($_GET['start'])) { 
            $model->todo_date = gmdate('Y-m-d', ($_GET['start']));
            $time = gmdate('H:i', ($_GET['start'])); 
        }
        $model->todo_time = ($time != '00:00') ? $time : '';
        if(!$model->todo_time) {
            $model->all_day = 1;
        }
        
        $model->fromDate = $model->todo_date;
        $model->fromTime = $model->todo_time;
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->id_dict_todo_type_fk) {
                $dict = \backend\Modules\Task\models\CalDictionary::findOne($model->id_dict_todo_type_fk);
                $model->type_fk = $dict->type_fk;
            }
            $model->status = 1;
            
            if($model->all_day == 1) {
                $model->date_from = (!$model->fromDate) ? null : ( $model->fromDate . ' 00:00:00' );
                $model->date_to = (!$model->toDate) ? null : ( $model->toDate . ' 00:00:00' );
            } else {
                $model->date_from = (!$model->fromDate || !$model->fromTime) ? null : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                $model->date_to = (!$model->toDate || !$model->toTime) ? null : ( $model->toDate . ' ' . $model->toTime . ':00' );
            }
            if(!$model->date_from) $model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';            
            if(!$model->date_to) $model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';
            
            if(empty($model->timeM)) $model->timeM = 0;
            if(empty($model->timeH)) $model->timeH = 0;
		
            /*reminder start*/
            $model->reminder_user = \Yii::$app->user->id;
            if($model->todo_deadline) {
                $deadline = $model->todo_deadline;
            } else {
                $deadline = $model->date_from;
            }
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
                $model->reminder_user = \Yii::$app->user->id;
            }
            /*reminder end*/    
            if( $model->validate() ) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if($model->type_fk == 3) {
                        $newMeeting = new \backend\Modules\Community\models\ComMeeting();
                        $newMeeting->title = $model->name;
                        $newMeeting->type_fk = $model->meeting_type;
                        $newMeeting->purpose = $model->meeting_purpose;
                        $newMeeting->date_from = $model->date_from;
                        $newMeeting->date_to = $model->date_to;
                        if(!$newMeeting->save()) var_dump($newMeeting->getErrors());
                        
                        foreach($model->meeting_employees as $key => $employee) {
                            $newMeetingEmployee = new \backend\Modules\Community\models\ComMeetingMember();
                            $newMeetingEmployee->id_meeting_fk = $newMeeting->id;
                            $newMeetingEmployee->id_user_fk = $employee;
                            $newMeetingEmployee->id_employee_fk = $employee;
                            $newMeetingEmployee->save();
                        }        
                        $model->id_group_fk = $newMeeting->id;
                    }
                
                    if($model->save()) {				
                        $priorityColors = [ 1 => 'blue', 2 => 'yellow', 3 => 'orange', 4 => 'red'];
                        $task = '<i class="fa fa-flag text--'.$priorityColors[$model->id_priority_fk].'"></i>'.$model->name . ' <span class="todo-normal-time">@'.( (!$model->todo_time || $model->todo_time == '00:00') ? 'brak terminu' : $model->todo_time ).'</span>';
                        $listElement = '<li class="todo-'.$model->id.' todo-normal  '.( ($model->is_close == 1) ? "completed" : '' ).'">'
                            .'<div>'
                                .'<input type="checkbox" id="checkbox-'.$model->id.'" value="'.$model->id.'" name="check" '.( ($model->is_close == 1) ? "checked" : '' ).'>'
                                .'<label class="checkbox" data-value="'.$model->id.'" for="checkbox-'.$model->id.'" data-action="'.Url::to(['/task/personal/state','id' => $model->id]).'">'
                            .'</div>'
                            .'<p>'.$task.'</p><a class="close" href="'.Url::to(['/task/personal/hide','id' => $model->id]).'"><i class="fa fa-close"></i></a>'
                        .'</li>';
                        
                        if($model->type_fk == 3) {
                            if(!$model->meeting_resource) $model->meeting_resource = [];
                            foreach($model->meeting_resource as $key => $resource) {
                                $newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                                $newSchedule->status = 0;
                                $newSchedule->id_resource_fk = $resource;
                                $newSchedule->id_employee_fk = $this->module->params['employeeId'];
                                $newSchedule->date_from = $model->date_from;
                                $newSchedule->date_to = $model->date_to;
                                $newSchedule->describe = $model->meeting_purpose;
                                $newSchedule->type_event = 2;
                                $newSchedule->id_event_fk = $model->id;
                                $newSchedule->save();
                                
                                $newMeetingResource = new \backend\Modules\Community\models\ComMeetingResource();
                                $newMeetingResource->id_meeting_fk = $newMeeting->id;
                                $newMeetingResource->id_resource_fk = $resource;
                                $newMeetingResource->id_resource_schedule_fk = $newSchedule->id;
                                $newMeetingResource->save();
                            }
                        }
                        
                        $projectDetailsPercent = false;
                        $projectDetailsEmployees = false;
                        $alertAdvanced = '';
                        $detailsStats = false;
                        if($model->id_set_fk) { 
                            $projectDetails = $model->case['details'];
                            if($projectDetails) $projectDetailsPercent = $projectDetails->percent;
                            $projectDetailsEmployees = $this->renderPartial('@frontend/Modules/Task/views/details/_employeesTable', [ 'model' => $model->case]); 
                            $modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $model->id_employee_fk, 'id_case_fk' => $model->id_set_fk])->one();
                            if($modelCe && $projectDetails->is_limited)
                            $alertAdvanced = '<br/>Liczba godzin jaka pozostała pracownikowi do wykorzystania w ramach limitu dla tego projektu: '.( round( ($modelCe->time_limit*60-$modelCe->time_used)/60, 2));
                            $stats = \backend\Modules\Task\models\CalCaseDetails::getStats($model->id_set_fk); 
                            $time = round($stats['execution_time']/60,2).' / '.$projectDetails->time_limit;
                            $tasks = $stats['tasks_open'].' / '.$stats['tasks_all'];
                            $settlements = number_format(0, 2, '.', ' ');
                            $detailsStats = ['time' => $time, 'tasks' => $tasks, 'settlements' => $settlements];
                        }
                        $transaction->commit();
                        return array('success' => true,  'action' => 'insertRow', 'calendar' => '#personal', 'listElement' => $listElement, 'show' => ( ($model->todo_date == date('Y-m-d') ) ? true : false ), 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Zadanie zostało dodane'.$alertAdvanced, 'projectDetailsPercent' => $projectDetailsPercent, 'projectDetailsEmployees' => $projectDetailsEmployees, 'detailsStats' => $detailsStats  );	
                    } 
                } catch (Exception $e) {
                    $transaction->rollBack();
                    return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_formAjax_new':'_formAjax'), [ 'model' => $model, 'member' => false, 'projectId' => ( isset($_GET['projectId']) ? $_GET['projectId'] : false)]), 'errors' => $model->getErrors() );	
                }
            } else {
                $model->status = -2;
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_formAjax_new':'_formAjax'), [ 'model' => $model, 'member' => false, 'projectId' => ( isset($_GET['projectId']) ? $_GET['projectId'] : false)]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model, 'member' => false, 'projectId' => ( isset($_GET['projectId']) ? $_GET['projectId'] : false) ]);	
        }
	}
    
    public function actionUpdateajax($id) {
		$model = $this->findModel($id);
        $oldSet = $model->id_set_fk;
        $action = 'update';
        $member = false;
        if($model->id_meeting_fk)
            $member = \backend\Modules\Community\models\ComMeetingMember::findOne($model->id_meeting_fk);
        //$model->scenario = CalTodo::SCENARIO_MODIFY;
        
        $grants = $this->module->params['grants'];
        
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        if($meeting = $model->meeting) {
            $model->meeting_purpose = $meeting->purpose;
            $model->meeting_type = $meeting->type_fk;
            $model->meeting_employees = [];
            foreach($meeting->employees as $key => $employee) {
                array_push($model->meeting_employees,$employee->id_employee_fk);
            }
            $model->meeting_resource = [];
            foreach($meeting->resources as $key => $resource) {
                array_push($model->meeting_resource, $resource->id_resource_fk);
            }
        }
        
        $eventDateTemp = $model->todo_date;
    
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            if($model->id_dict_todo_type_fk) {
                $dict = \backend\Modules\Accounting\models\AccDictionary::findOne($model->id_dict_todo_type_fk);
                $model->type_fk = $dict->type_fk;
            }
            
            if($model->all_day == 1) {
                $model->date_from = (!$model->fromDate) ? null : ( $model->fromDate . ' 00:00:00' );
                $model->date_to = (!$model->toDate) ? null : ( $model->toDate . ' 00:00:00' );
            } else {
                $model->date_from = (!$model->fromDate || !$model->fromTime) ? null : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                $model->date_to = (!$model->toDate || !$model->toTime) ? null : ( $model->toDate . ' ' . $model->toTime . ':00' );
            }
            if(!$model->date_from) $model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';            
            if(!$model->date_to) $model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';
            //$model->date_from = '2016-11-28 12:00:00'; $model->date_to = '2016-11-28 16:00:00';
            //if( !isset($_POST['CalTodo']['todo_time']) ) $model->todo_time = null;
            
            if(empty($model->timeM)) $model->timeM = 0;
            if(empty($model->timeH)) $model->timeH = 0;
            
            /*reminder start*/
            $model->reminder_user = \Yii::$app->user->id;
            if($model->todo_deadline) {
                $deadline = $model->todo_deadline;
            } else {
                $deadline = $model->date_from;
            }
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
                $model->reminder_user = \Yii::$app->user->id;
            }
            /*reminder end*/
           
			if($model->validate() && $model->save()) {
                $color = 'blue'; $icon = 'user';
                if($model->type_fk == 1) { $color = 'blue'; $icon = 'user'; }
                if($model->type_fk == 2) { $color = 'grey'; $icon = 'thumb-tack'; }
                if($model->type_fk == 3) { $color = 'orange'; $icon = 'handshake-o'; }
                if($model->type_fk == 4) { $color = 'pink'; $icon = 'sun-o'; }
                $btn = '<div class="btn-group pull-right">'
                            .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                .'Opcje <span class="caret"></span>'
                            .'</button>'
                            .'<ul class="dropdown-menu">'
                                . (($model->is_close == 0) ? '<li><a href="'.Url::to(['/task/personal/state', 'id' => $model->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                                .'<li><a href="'.Url::to(['/task/personal/updateajax', 'id' => $model->id]) .'" data-target="#modal-grid-item" title="Edytuj zadanie" class="gridViewModal"><i class="fa fa-pencil text--grey2"></i>Edytuj zadanie</a></li>'
                            .'</ul>'
                        .'</div><div class="clear"></div>';
                $bg = '';
                if($model->is_close == 1) $bg = " bg-green2";
                $eventItem = '<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.$model->todo_time.$btn.'</h4>'.$model->name;
                if($eventDateTemp != $model->todo_date) $action = 'delete'; 
                
                $projectDetailsPercent = false;
                $projectDetailsEmployees = false;
                $alertAdvanced = '';
                if($model->id_set_fk) {
                    $projectDetails = $model->case['details'];
                    if($projectDetails) $projectDetailsPercent = $projectDetails->percent;
                    $projectDetailsEmployees = $this->renderPartial('@frontend/Modules/Task/views/details/_employeesTable', [ 'model' => $model->case]); 
                    $modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $model->id_employee_fk, 'id_case_fk' => $model->id_set_fk])->one();
                    if($modelCe)
                    $alertAdvanced = '<br/>Ilość zadań jaka pozostała pracownikowi do wykorzystania w ramach limitu dla tego projektu: '.( round( ($modelCe->time_limit*60-$modelCe->time_used)/60, 2));
                }
				return array('success' => true,  'action' => $action, 'index' => isset($_GET['index']) ? $_GET['index'] : 0, 'id' => $model->id, 'eventItem' => $eventItem, 'alert' => 'Zadanie zostało dodane'.$alertAdvanced, 'projectDetailsPercent' => $projectDetailsPercent, 'projectDetailsEmployees' => $projectDetailsEmployees  );	
			} else {
               // $lists = $this->lists($model->id_customer_fk);
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_formAjax_new':'_formAjax'), [  'model' => $model, 'member' => $member, 'projectId' => $model->id_set_fk]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax(((Yii::$app->params['env'] == 'dev')?'_formAjax_new':'_formAjax'), [ 'model' => $model, 'member' => $member, 'projectId' => $model->id_set_fk]) ;	
		}
	}	

    public function actionViewajax($id)  {
        $model = $this->findModel($id);
        $departmentsList = '';
        $filesList = '';
        $employeesList = '';
        $grants = $this->module->params['grants'];
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        foreach($model->employees as $key => $item) {
            $employeesList .= '<li>'.$item->employee['fullname']  .'</li>';
        }
        $model->employees_list = ($employeesList) ? '<ul>'.$employeesList.'</ul>' :  'brak danych';
        
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul>'.$filesList.'<ul>' :  'brak danych';
        
        return $this->renderAjax('_viewAjax', [
            'model' => $model,  'edit' => isset($grants['caseEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }
   
    /**
     * Updates an existing CalTodo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        if( count(array_intersect(["eventEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$model = $this->findModel($id);
                
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\TaskEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_task_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego zadania.');
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        $model->scenario = CalTodo::SCENARIO_MODIFY;
        
        $departments = []; $employees = [];
		$status = CalTodo::listStatus();
        $categories = CalTodo::listCategory();
        $grants = $this->module->params['grants'];
        
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
        }
        foreach($model->employees as $key => $item) {
            array_push($employees, $item->id_employee_fk);
        }
        $filesList = '';
        foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul class="files-set">'.$filesList.'</ul>' :  '<ul class="files-set"></ul>';

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
			if($model->validate() && $model->save()) {
				return array('success' => true,  'action' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało zaktualizowane' );
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'departments' => $departments, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind'] ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model, 'departments' => $departments, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
    }
    
    /**
     * Deletes an existing CalTodo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTodo::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie <b>'.$model->name.'</b> zostało usunięte')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało usuniętye.<br/>', 'id' => $id);	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte.<br />', 'id' => $id);	
			}
        }       
    }
    
    public function actionDeleteajax($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $action = 'delete';
        
        $model= $this->findModel($id);
       // $model->scenario = CalTask::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        $alertAdvanced = ''; $projectDetailsPercent = false; $projectDetailsEmployees = false;
        if($model->save()) {
            if($model->id_meeting_fk) {
                $member = \backend\Modules\Community\models\ComMeetingMember::findOne($model->id_meeting_fk);
                $member->id_task_fk = 0;
                $member->save();
            }
            if($model->id_set_fk) {
                $projectDetails = $model->case['details'];
                if($projectDetails) $projectDetailsPercent = $projectDetails->percent;
                $projectDetailsEmployees = $this->renderPartial('@frontend/Modules/Task/views/details/_employeesTable', [ 'model' => $model->case]); 
                $modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $model->id_employee_fk, 'id_case_fk' => $model->id_set_fk])->one();
                if($modelCe)
                $alertAdvanced = '<br/>Ilość zadań jaka pozostała pracownikowi do wykorzystania w ramach limitu dla tego projektu: '.( round( ($modelCe->time_limit*60-$modelCe->time_used)/60, 2));
                $action = 'deleteTodoSet';
            }
            return array('success' => true, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało usunięte.'.$alertAdvanced, 'id' => $model->id, 'action' => $action, 'projectDetailsPercent' => $projectDetailsPercent, 'projectDetailsEmployees' => $projectDetailsEmployees, 'table' => '#table-todos' );	
        } else {
            return array('success' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte', 'id' => $model->id, 'action' => 'delete', 'table' => '#table-todos');	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionHide($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTodo::SCENARIO_CLOSE;
        $model->todo_show = 0;
        $model->user_action = 'hide';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        return ['result' => true, 'alert' => 'Zadanie zostało usunięte', 'todoWidget' => TodoWidget::widget(['refresh' => true])];
    }
    
    public function actionOpen($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTodo::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 1;
        $model->user_action = 'close';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie zostało ponownie otwarte')  );
        return $this->redirect(['update', 'id' => $model->id]);
    }
    
    public function actionState($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
       // $model->scenario = CalTodo::SCENARIO_CLOSE;
        $model->is_close = ($model->is_close == 0) ? 1 : 0;
        $model->user_action = ($model->is_close == 0) ? 'close' : 'open';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        if(!$model->save()) {var_dump($model->getErrors());exit;}
        
        $color = 'blue'; $icon = 'user';
        if($model->type_fk == 1) { $color = 'blue'; $icon = 'user'; }
        if($model->type_fk == 2) { $color = 'grey'; $icon = 'thumb-tack'; }
        if($model->type_fk == 3) { $color = 'orange'; $icon = 'handshake-o'; }
        if($model->type_fk == 4) { $color = 'pink'; $icon = 'sun-o'; }
        $btn = '<div class="btn-group pull-right">'
                    .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                        .'Opcje <span class="caret"></span>'
                    .'</button>'
                    .'<ul class="dropdown-menu">'
                        . (($model->is_close == 0) ? '<li><a href="'.Url::to(['/task/personal/state', 'id' => $model->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                        .'<li><a href="'.Url::to(['/task/personal/updateajax', 'id' => $model->id]) .'" data-target="#modal-grid-item" title="Edytuj zadanie" class="gridViewModal"><i class="fa fa-pencil text--grey2"></i>Edytuj zadanie</a></li>'
                    .'</ul>'
                .'</div><div class="clear"></div>';
        $bg = '';
        if($model->is_close == 1) $bg = " bg-green2";
        $eventItem = '<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.$model->todo_time.$btn.'</h4>'.$model->name;
        //Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zadanie zostało zamknięte')  );
        return ['result' => true, 'alert' => 'Zadanie zostało zamknięte', 'id' => $model->id, 'action' => 'close', 'eventItem' => $eventItem, 'todoWidget' => TodoWidget::widget(['refresh' => true]) ];
    }

    /**
     * Finds the CalTodo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalTodo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = CalTodo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 2;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_task_fk = $model->id;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }

    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        
        $data = []; $where_t = []; $where_a = [];
		$post = $_GET; $type = 0; $employee = false; $department = false; $customer = false; $dateFrom = false;  $dateTo = false;  
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where_t, "lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where_a, "a.id_customer_fk = ".$params['id_customer_fk']);
                array_push($where_t, "a.id = 0");
                $customer = \backend\Modules\Crm\models\Customer::findOne($params['id_customer_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where_a, "a.id_set_fk = ".$params['id_set_fk']);
                array_push($where_t, "a.id = 0");
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where_t, "lower(a.description) like '%".strtolower($params['description'])."%'");
                array_push($where_a, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where_t, "id_employee_fk = ".$params['id_employee_fk']);
                array_push($where_a, "id_employee_fk = ".$params['id_employee_fk']);
                $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($params['id_employee_fk']);
            } else {
               // $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->module->params['employeeId']);
               // array_push($where_t, "id_employee_fk = ".$employee->id);
               // array_push($where_a, "id_employee_fk = ".$employee->id);
				if(!$this->module->params['isAdmin'] && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && !$this->module->params['managers']) {
                    array_push($where_t, "id_employee_fk = ".$this->module->params['employeeId']);
                    array_push($where_a, "id_employee_fk = ".$this->module->params['employeeId']);
                } else if ($this->module->params['managers'] && !$this->module->params['isAdmin'] && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                    array_push($where_t, "id_employee_fk in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} d on d.id=ed.id_department_fk where ed.status = 1 and id_employee_manager_fk = ".$this->module->params['employeeId'].")");
                    array_push($where_a, "a.id_department_fk in (select id from {{%company_department}} where id_employee_manager_fk = ".$this->module->params['employeeId'].") and id_employee_fk in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} d on d.id=ed.id_department_fk where ed.status = 1 and id_employee_manager_fk = ".$this->module->params['employeeId'].")");
                }
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where_a, "a.id_department_fk = ".$params['id_department_fk']);
                array_push($where_t, "id_employee_fk in (select id_employee_fk from {{%employee_department}} where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
                $department = \backend\Modules\Company\models\CompanyDepartment::findOne($params['id_department_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $type = $params['type_fk'];
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where_t, "todo_date >= '".$params['date_from']."'");
                array_push($where_a, "action_date >= '".$params['date_from']."'");
                $dateFrom = $params['date_from'];
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where_t, "todo_date <= '".$params['date_to']."'");
                array_push($where_a, "action_date <= '".$params['date_to']."'");
                $dateTo = $params['date_to'];
            }
        }  else {
            $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->module->params['employeeId']);
            array_push($where_t, "id_employee_fk = ".$employee->id);
            array_push($where_a, "id_employee_fk = ".$employee->id);
        }
		
        //if(!$employee) $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->module->params['employeeId']);
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
        
        $periodLabel = ($dateFrom || $dateTo) ? ' w okresie'.((($dateFrom) ? ' od '.$dateFrom : '').(($dateTo) ? ' do '.$dateTo : '')) : '';
        
        if($employee)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności pracownika '.$employee->fullname.$periodLabel);
        else if($department)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności działu '.$department->name.$periodLabel);
        else if($customer)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności dla klienta '.(($customer->symbol) ? $customer->symbol : $customer->symbol).$periodLabel);
        else 
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności'.$periodLabel);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Data')
            ->setCellValue('B3', 'Czas')
            ->setCellValue('C3', 'Pracownik')
            ->setCellValue('D3', 'Czynność')
			->setCellValue('E3', 'Kategoria')
			->setCellValue('F3', 'Opis')
            ->setCellValue('G3', 'Klient')
            ->setCellValue('H3', 'Postępowanie')
			->setCellValue('I3', 'Dział');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:I3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4;  
		     
		$sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		$count = 0; $summary = 0;
        /* action */
		$query1 = (new \yii\db\Query())
            ->select(["concat_ws('1', '1') as type_action", "a.id as id", "id_service_fk as type_fk", "a.name as name", "a.description as description", "action_date", "unit_time", "concat_ws(' ', e.lastname, e.firstname) as ename",
                      "c.id as cid", "c.name as cname", "c.symbol as csymbol", "is_confirm", "cc.id as ccid", "cc.type_fk as cctype", "cc.name as ccname", "d.name as dname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
			->join('JOIN', '{{%company_department}}  as d', 'd.id=a.id_department_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = a.id_set_fk')
            ->where( ['a.status' => 1] );
            
	    $queryTotal1 = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60, 2)) as unit_time"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1] );
            
         if( count($where_a) > 0 ) {
            $query1 = $query1->andWhere(implode(' and ',$where_a));
            $queryTotal1 = $queryTotal1->andWhere(implode(' and ',$where_a));
        }
		$totalRow1 = $queryTotal1->one(); $count1 = $totalRow1['total_rows']; $summary1 = $totalRow1['unit_time'];

        /* actions */
        
        /* personal */
        $query2 = (new \yii\db\Query())
            ->select(["concat_ws('2', '2') as type_action", "a.id as id", "id_dict_todo_type_fk as type_fk", "a.name as name", "a.description as description", "todo_date as action_date", "execution_time as unit_time", "concat_ws(' ', e.lastname, e.firstname) as ename",
                      "concat_ws('', '') as cid", "concat_ws('', '') as cname", "concat_ws('', '') as csymbol", "concat_ws('', '0') as is_confirm",
                      "concat_ws('', '') as ccid", "concat_ws('', '') as cctype", "concat_ws('', '') as ccname", "concat_ws('', '') as dname"])
            ->from('{{%cal_todo}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1] )->andWhere('type_fk != 2');
            
	    $queryTotal2 = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(execution_time/60,2)) as unit_time"])
            ->from('{{%cal_todo}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1] )->andWhere('type_fk != 2');
       
       if( count($where_t) > 0 ) {
            $query2 = $query2->andWhere(implode(' and ',$where_t));
            $queryTotal2 = $queryTotal2->andWhere(implode(' and ',$where_t));
        }
		$totalRow2 = $queryTotal2->one(); $count2 = $totalRow2['total_rows']; $summary2 =$totalRow2['unit_time'];
        
        /* personal */
        if($type == 0) {
            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query1->union($query2)])
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 + $count2; $summary = $summary1 + $summary2;
        } else if($type == 1) {
            $unionQuery = $query1->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 ; $summary = $summary1 ;
        } else {
            $unionQuery = $query2->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count =  $count2; $summary =  $summary2;
        }

        $rows = $unionQuery->all();
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        $dictPersonal = \backend\Modules\Task\models\CalTodo::dictTypes();
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				if($record['type_action'] == 1) {
                    $symbol = 'na rzecz klienta';
                    $tmpType = $dict[$record['type_fk']];
                    $type = $tmpType['name'];
                    //$orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
                } else {
                    $symbol = 'administracyjna';
                    $tmpType = $dictPersonal[$record['type_fk']];
                    $type = $tmpType['name'];
                }
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['action_date'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, round($record['unit_time']/60,2)); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['ename']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $symbol); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $type); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, str_replace(array("\n", "\r"), '', trim($record['description']))); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, ($record['csymbol']) ? $record['csymbol'] : $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['ccname']); 
				$objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['dname']); 
						
				$i++; 
			}  
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:I'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Rejestr czynności');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Zadania'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionTimeline() {
        return $this->render('_timeline');
    }
    
    public function actionExporttogoogle($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $dataToken = \Yii::$app->session->get('google.id');
        $client = new \Google_Client();
        $client->setApplicationName("Lawfirm");
        $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
        $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( implode(' ', [ \Google_Service_Calendar::CALENDAR, /*'https://www.googleapis.com/auth/calendar.readonly',*/ 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email' ]  ));
        //$client->setAuthConfig('F:\WebDir\projekty\lawfirm\frontend\web\client_secrets.json');
        $client->setAccessType('offline');
        $client->setDeveloperKey('AIzaSyBbTvU77Fk6aULzj_yYx3ItkPRnFBecdOE');
        $client->setHttpClient(new \GuzzleHttp\Client(['verify' => false]));
        if(  \Yii::$app->session->get('google.token')  )
            $client->setAccessToken( json_encode( ['expires_in'=>3599, 'access_token'=> \Yii::$app->session->get('google.token'), 'id_token' => $dataToken->params['id_token'] ] ) );

        if( !$client->getAccessToken() )
            return new \Application\Model\Google\AuthRequest( $client->createAuthUrl() );
        if ($client->isAccessTokenExpired()) {
           \Yii::$app->session->remove('google.token');  \Yii::$app->session->remove('google.id');
           header('Location: login.php');
        }
        $service = new \Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
          'maxResults' => 10,
          'orderBy' => 'startTime',
          'singleEvents' => TRUE,
          'timeMin' => date('c'),
        );
        //date('Y-m-d\TH:i:sP');
        /*if( $model->all_day == 1 ) {
            
            $event = new \Google_Service_Calendar_Event(array(
                'summary' => $model->name,
               // 'location' => '800 Howard St., San Francisco, CA 94103',
                'description' => $model->description,
                'start' => array(
                    'date' => $model->todo_date,
                    //'timeZone' => 'America/Los_Angeles',
                ),
                'end' => array(
                    'date' => $model->todo_date,
                    //'timeZone' => 'America/Los_Angeles',
                ),
            ));
        } else {
            if($model->date_to) {
                $event = new \Google_Service_Calendar_Event(array(
                    'summary' => $model->name,
                   // 'location' => '800 Howard St., San Francisco, CA 94103',
                    'description' => $model->description,
                    'start' => array(
                        //'dateTime' => '2016-11-19T09:00:00-09:00',
                        'dateTime' => date('Y-m-d\TH:i:sP', strtotime($model->date_from)),
                        //'timeZone' => 'America/Los_Angeles',
                    ),
                    'end' => array(
                        'dateTime' => date('Y-m-d\TH:i:sP', strtotime($model->date_to)),
                        //'timeZone' => 'America/Los_Angeles',
                    ),
                ));
            } else {
                $event = new \Google_Service_Calendar_Event([
                        'summary' => $model->name, 'description' => $model->description,
                        'start' => array( 'date' => $model->todo_date, ),
                        'endTimeUnspecified' => true,   
                    ]);
               // $event->setEndTimeUnspecified(true);
            }
        }*/
        $event = new \Google_Service_Calendar_Event();
        $event->setSummary($model->name);
        $event->setDescription($model->description);
        
        if( $model->all_day == 1 ) {
            $start = new \Google_Service_Calendar_EventDateTime();
            $start->setDate( $model->todo_date );
            $event->setStart($start);
            
            $end = new \Google_Service_Calendar_EventDateTime();
            $end->setDate( $model->todo_date );
            $event->setEnd($end);
        } else {
            $start = new \Google_Service_Calendar_EventDateTime();
            $start->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_from)) );
            $event->setStart($start);
            
            if($model->date_to) {
                $end = new \Google_Service_Calendar_EventDateTime();
                $end->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_to)) );
                $event->setEnd($end);
            } else {
                $end = new \Google_Service_Calendar_EventDateTime();
                $end->setDateTime( date('Y-m-d\TH:i:sP', ( strtotime($model->date_from) + 60*60) ) );
                $event->setEnd($end);
                //$event->setEndTimeUnspecified(true);
            }
        }

        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);
        //var_dump($event);
        $model->g_id = $event->id; $model->g_action = 1; $model->save();
        //var_dump( $service->calendarList->listCalendarList() );
        //$results = $service->events->listEvents($calendarId, $optParams);

        /*if (count($results->getItems()) == 0) {
          print "No upcoming events found.\n";
        } else {
          print "Upcoming events:\n";
          foreach ($results->getItems() as $event) {
            $start = $event->start->dateTime;
            if (empty($start)) {
              $start = $event->start->date;
            }
            printf("%s (%s)\n", $event->getSummary(), $start);
          }
        }*/
        return ['success' => true, 'lists' => $service->calendarList];

    }
    
    public function actionGimport() {
        if(\Yii::$app->session->get('google.token') && \Yii::$app->session->get('google.id')) {
            $dataToken = \Yii::$app->session->get('google.id');// print_r($dataToken->params['id_token']); exit;
            //echo 'Jesteś połączony z Google: '.\Yii::$app->session->get('google.token');
            
            //var_dump(\Yii::$app->session->get('google.id'));
            $client = new \Google_Client();
            $client->setApplicationName("Lawfirm");
            $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
            $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
            $client->setApplicationName('Google Calendar API PHP Quickstart');
            $client->setScopes( implode(' ', [ \Google_Service_Calendar::CALENDAR, /*'https://www.googleapis.com/auth/calendar.readonly',*/ 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email' ]  ));
            //$client->setAuthConfig('F:\WebDir\projekty\lawfirm\frontend\web\client_secrets.json');
            $client->setAccessType('offline');
            $client->setDeveloperKey('AIzaSyBbTvU77Fk6aULzj_yYx3ItkPRnFBecdOE');
            $client->setHttpClient(new \GuzzleHttp\Client(['verify' => false]));
            if(  \Yii::$app->session->get('google.token')  )
                $client->setAccessToken( json_encode( ['expires_in'=>3599, 'access_token'=> \Yii::$app->session->get('google.token'), 'id_token' => $dataToken->params['id_token'] ] ) );

            if( !$client->getAccessToken() ) 
                return new \Application\Model\Google\AuthRequest( $client->createAuthUrl() );
            if ($client->isAccessTokenExpired()) {
               \Yii::$app->session->remove('google.token');  \Yii::$app->session->remove('google.id');
               header('Location: '.Url::toRoute('/task/calendar/personal', true));
            } else {

                $service = new \Google_Service_Calendar($client);

                $calendarId = 'primary';
                $optParams = array(
                    'maxResults' => 10,
                    'orderBy' => 'startTime',
                    'singleEvents' => TRUE,
                    'timeMin' => date('c'),
                );
                $oauth = new \Google_Service_Oauth2($client);            
                $eventsData = $service->events->listEvents($calendarId, $optParams);
                $events = count( $eventsData->getItems() );
            }
        } 
        
        return $this->renderAjax('_google', ['action' => 'import', 'events' => $events]);
    }
    
    public function actionGexport() {
        return $this->renderAjax('_google', ['action' => 'export', 'events' => 1]);
    }
    
    public function actionGimportdata() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(\Yii::$app->session->get('google.token') && \Yii::$app->session->get('google.id')) {
            $dataToken = \Yii::$app->session->get('google.id');// print_r($dataToken->params['id_token']); exit;
            //echo 'Jesteś połączony z Google: '.\Yii::$app->session->get('google.token');
            
            //var_dump(\Yii::$app->session->get('google.id'));
            $client = new \Google_Client();
            $client->setApplicationName("Lawfirm");
            $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
            $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
            $client->setApplicationName('Google Calendar API PHP Quickstart');
            $client->setScopes( implode(' ', [ \Google_Service_Calendar::CALENDAR, /*'https://www.googleapis.com/auth/calendar.readonly',*/ 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email' ]  ));
            //$client->setAuthConfig('F:\WebDir\projekty\lawfirm\frontend\web\client_secrets.json');
            $client->setAccessType('offline');
            $client->setDeveloperKey('AIzaSyBbTvU77Fk6aULzj_yYx3ItkPRnFBecdOE');
            $client->setHttpClient(new \GuzzleHttp\Client(['verify' => false]));
            if(  \Yii::$app->session->get('google.token')  )
                $client->setAccessToken( json_encode( ['expires_in'=>3599, 'access_token'=> \Yii::$app->session->get('google.token'), 'id_token' => $dataToken->params['id_token'] ] ) );

            if( !$client->getAccessToken() ) 
                return new \Application\Model\Google\AuthRequest( $client->createAuthUrl() );
            if ($client->isAccessTokenExpired()) {
               \Yii::$app->session->remove('google.token');  \Yii::$app->session->remove('google.id');
               header('Location: '.Url::toRoute('/task/calendar/personal', true));
            } else {

                $service = new \Google_Service_Calendar($client);

                $calendarId = 'primary';
                $optParams = array(
                   /* 'maxResults' => 10,
                    'orderBy' => 'startTime',
                    'singleEvents' => TRUE,
                    'timeMin' => date('c'),*/
                );
                $oauth = new \Google_Service_Oauth2($client);            
                $eventsData = $service->events->listEvents($calendarId, $optParams);
                
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
                $events = 0;
                
                foreach ( $eventsData->getItems() as $key => $event ) { 
                    //var_dump(date('Y-m-d H:i:s', strtotime($event->start->dateTime))); exit;
                    $exsits = CalTodo::find()->where(['g_id' => $event->id])->one();
                    if(!$exsits) {
                        $new = new CalTodo();
                        $new->g_id = $event->id; $new->g_action = 2;
                        $new->id_priority_fk = 1;
                        $new->todo_date = date('Y-m-d', strtotime($event->start->dateTime));
                        $new->todo_time = date('H:i', strtotime($event->start->dateTime));
                        $new->date_from = date('Y-m-d H:i:s', strtotime($event->start->dateTime));
                        $new->date_to = date('Y-m-d H:i:s', strtotime($event->end->dateTime));
                        $new->type_fk = 1;
                        $new->is_close = 0;
                        $new->todo_show = 1;
                        $new->execution_time = 0;
                        $new->name = $event->summary;
                        $new->description = $event->description;
                        $new->id_employee_fk = $employee->id;
                        if($new->date_from != '1970-01-01 01:00:00')
                            $new->save();
                    }
                }
            }
        } 
        
        return ['success' => true];
    }
    
    public function actionGexportdata($type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(\Yii::$app->session->get('google.token') && \Yii::$app->session->get('google.id')) {
            $dataToken = \Yii::$app->session->get('google.id');// print_r($dataToken->params['id_token']); exit;
            //echo 'Jesteś połączony z Google: '.\Yii::$app->session->get('google.token');
            
            //var_dump(\Yii::$app->session->get('google.id'));
            $client = new \Google_Client();
            $client->setApplicationName("Lawfirm");
            $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
            $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
            $client->setApplicationName('Google Calendar API PHP Quickstart');
            $client->setScopes( implode(' ', [ \Google_Service_Calendar::CALENDAR, /*'https://www.googleapis.com/auth/calendar.readonly',*/ 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email' ]  ));
            //$client->setAuthConfig('F:\WebDir\projekty\lawfirm\frontend\web\client_secrets.json');
            $client->setAccessType('offline');
            $client->setDeveloperKey('AIzaSyBbTvU77Fk6aULzj_yYx3ItkPRnFBecdOE');
            $client->setHttpClient(new \GuzzleHttp\Client(['verify' => false]));
            if(  \Yii::$app->session->get('google.token')  )
                $client->setAccessToken( json_encode( ['expires_in'=>3599, 'access_token'=> \Yii::$app->session->get('google.token'), 'id_token' => $dataToken->params['id_token'] ] ) );

            if( !$client->getAccessToken() ) 
                return new \Application\Model\Google\AuthRequest( $client->createAuthUrl() );
            if ($client->isAccessTokenExpired()) {
               \Yii::$app->session->remove('google.token');  \Yii::$app->session->remove('google.id');
               header('Location: '.Url::toRoute('/task/calendar/personal', true));
            } else {
                $service = new \Google_Service_Calendar($client);

                $calendarId = 'primary';
                $optParams = array(
                   /* 'maxResults' => 10,
                    'orderBy' => 'startTime',
                    'singleEvents' => TRUE,
                    'timeMin' => date('c'),*/
                );
                $oauth = new \Google_Service_Oauth2($client); 
                
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
                if($type == 1 || $type == 3) {
                    $events = CalTodo::find()->where(['status' => 1, 'id_employee_fk' => $employee->id])->andWhere('g_id is null')->all();
                    foreach($events as $key => $model)
                        $event = new \Google_Service_Calendar_Event();
                        $event->setSummary($model->name);
                        $event->setDescription($model->description);
                        
                        if( $model->all_day == 1 ) {
                            $start = new \Google_Service_Calendar_EventDateTime();
                            $start->setDate( $model->todo_date );
                            $event->setStart($start);
                            
                            $end = new \Google_Service_Calendar_EventDateTime();
                            $end->setDate( $model->todo_date );
                            $event->setEnd($end);
                        } else {
                            $start = new \Google_Service_Calendar_EventDateTime();
                            $start->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_from)) );
                            $event->setStart($start);
                            
                            if($model->date_to) {
                                $end = new \Google_Service_Calendar_EventDateTime();
                                $end->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_to)) );
                                $event->setEnd($end);
                            } else {
                                $end = new \Google_Service_Calendar_EventDateTime();
                                $end->setDateTime( date('Y-m-d\TH:i:sP', ( strtotime($model->date_from) + 60*60) ) );
                                $event->setEnd($end);
                                //$event->setEndTimeUnspecified(true);
                            }
                        }

                        $calendarId = 'primary';
                        $event = $service->events->insert($calendarId, $event);
                        $model->g_id = $event->id; $model->g_action = 1; $model->save();
                    }
                    
                if($type == 2 || $type == 3) {
                    $events = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1])
                                                                          ->andWhere('g_id is null')
                                                                          ->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employee->id.')')
                                                                          ->all();
                    foreach($events as $key => $model) {
                        $event = new \Google_Service_Calendar_Event();
                        $event->setSummary($model->name);
                        $event->setDescription($model->description);
                        $event->setLocation($model->place);
                        
                        if( $model->all_day == 1 ) {
                            $start = new \Google_Service_Calendar_EventDateTime();
                            $start->setDate( $model->event_date );
                            $event->setStart($start);
                            
                            $end = new \Google_Service_Calendar_EventDateTime();
                            $end->setDate( $model->event_date );
                            $event->setEnd($end);
                        } else {
                            $start = new \Google_Service_Calendar_EventDateTime();
                            $start->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_from)) );
                            $event->setStart($start);
                            
                            if($model->date_to) {
                                $end = new \Google_Service_Calendar_EventDateTime();
                                $end->setDateTime( date('Y-m-d\TH:i:sP', strtotime($model->date_to)) );
                                $event->setEnd($end);
                            } else {
                                $end = new \Google_Service_Calendar_EventDateTime();
                                $end->setDateTime( date('Y-m-d\TH:i:sP', ( strtotime($model->date_from) + 60*60) ) );
                                $event->setEnd($end);
                                //$event->setEndTimeUnspecified(true);
                            }
                        }

                        $calendarId = 'primary';
                        $event = $service->events->insert($calendarId, $event);
                        //$model->g_id = $event->id; $model->g_action = 1; $model->save();
                    }
                }
            }
        } 
        
        return ['success' => true];
    }
    
    public function actionTodo() {
        $id = $this->module->params['employeeId'];
        if( isset($_GET['id']) && !empty($_GET['id']) ) $id = $_GET['id'];
        /*$tasks = CalTodo::find()->where(['status' => 1, 'id_employee_fk' => $id])
                   ->andWhere( "todo_date = '".date('Y-m-d')."'" )
                   ->orderby('todo_time')->all();
                   
        $actions = AccActions::find()->where(['status' => 1, 'id_employee_fk' => $id])
                   ->andWhere( "action_date = '".date('Y-m-d')."'" )->all();*/
           
        /*$events = \backend\Modules\Task\models\CalTask::find()
                    ->where(['status' => 1, "event_date" => date('Y-m-d')])
                    ->andWhere('id in (select id_task_fk from {{%task_employee}} where status = 1 and is_show = 1 and id_employee_fk = '.$id.')')
                    ->orderby('event_time')->all(); */
        $eventsSql = "select 'todo' as event_type, id, name, todo_time as event_time, execution_time, id_dict_todo_type_fk, 1 as id_dict_task_status_fk, '' as cname, '' as csymbol from {{%cal_todo}} ct "
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$id." and todo_date = '".date('Y-m-d')."'"
                ." union"
                ." select 'task' as event_type, e.id, e.name as name, event_time, execution_time, e.type_fk as type_fk, id_dict_task_status_fk, c.name as cname, c.symbol as csymbol from {{%cal_task}} e join {{%task_employee}} te on e.id=te.id_task_fk join {{%customer}} c on c.id=e.id_customer_fk"
                ." where e.status = 1 and te.status = 1 and te.id_employee_fk = ".$id." and event_date = '".date('Y-m-d')."'"
                ." order by 3";
        $events = Yii::$app->db->createCommand($eventsSql)->queryAll();
   
        $stats['cases'] = 0;
        $stats['tasks'] = 0;
        $stats['action_admin'] = 0;
        $stats['action_customer'] = 0;
        $stats['todo'] = 0;
        $stats['meetings'] = 0;
        $stats['holiday'] = 0;
        $stats['l4'] = 0;
        $stats['marketing'] = 0;
        
        $stats['hours_per_day'] = 0;
        $stats['hours_per_month'] = 0;
        
        $sql = "select count(*) as counter from {{%acc_actions}}"
                ." where id_employee_fk = ".$id." and status = 1 " ;
        $stats['action_customer'] = Yii::$app->db->createCommand($sql)->queryOne()['counter'];
                
        $sql = "select id_dict_todo_type_fk, count(*) as counter from {{%cal_todo}}"
                ." where id_employee_fk = ".$id." and status = 1 and type_fk = 1 group by id_dict_todo_type_fk order by id_dict_todo_type_fk" ;
        $dataTodo = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataTodo as $key => $todo) {
            if( $todo['id_dict_todo_type_fk'] == 1 ) $stats['action_admin'] = $todo['counter'];
            if( $todo['id_dict_todo_type_fk'] == 6 ) $stats['todo'] = $todo['counter'];
           // if( $todo['id_dict_todo_type_fk'] == 3 ) $stats['meetings'] = $todo['counter'];
            if( $todo['id_dict_todo_type_fk'] == 3 || $todo['id_dict_todo_type_fk'] == 7 ) $stats['holiday'] = $todo['counter'];
            if( $todo['id_dict_todo_type_fk'] == 4 ) $stats['l4'] = $todo['counter'];
            if( $todo['id_dict_todo_type_fk'] == 2 ) $stats['marketing'] = $todo['counter'];
        }
        
        $sql = "select type_fk, count(*) as counter from {{%cal_task}}"
                ." where status = 1 and id in (select id_task_fk from {{%task_employee}} where status = 1 and is_show = 1 and id_employee_fk = ".$id.")  group by type_fk order by type_fk" ;
        $dataTask = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataTask as $key => $task) {
            if( $task['type_fk'] == 1 ) $stats['cases'] = $task['counter'];
            if( $task['type_fk'] == 2 ) $stats['tasks'] = $task['counter'];
        }
        
        $sql = "select sum(case when todo_date = '".date('Y-m-d')."' then execution_time else 0 end) per_day, sum(execution_time) per_month from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$id." and date_format(todo_date, '%Y-%m') = '".date('Y-m')."'"
                ." union"
                ." select sum(case when action_date = '".date('Y-m-d')."' then unit_time else 0 end) per_day, sum(unit_time) per_month from law_acc_actions"
                ." where status = 1 and id_employee_fk = ".$id." and date_format(action_date, '%Y-%m') = '".date('Y-m')."'";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataStats as $key => $stat) {
            $stats['hours_per_day'] += round($stat['per_day']/60, 2);
            $stats['hours_per_month'] += round($stat['per_month']/60, 2);
        }
        
        $actionsSql = "select 'todo' as action_type, id, todo_time, execution_time, name, id_dict_todo_type_fk as type_fk, '' as cname, '' as csymbol, 0 as id_event_fk, 0 as id_task_fk from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$id." and date_format(todo_date, '%Y-%m-%d') = '".date('Y-m-d')."'"
                ." union"
                ." select 'action' as action_type, a.id, 0 as todo_time, unit_time, a.name, id_service_fk as type_fk, c.name as cname, c.symbol as csymbol, id_event_fk, id_task_fk from law_acc_actions a join law_customer c on a.id_customer_fk=c.id"
                ." where a.status = 1 and id_employee_fk = ".$id." and date_format(action_date, '%Y-%m-%d') = '".date('Y-m-d')."'";
        $actions = Yii::$app->db->createCommand($actionsSql)->queryAll();
        
        $manager = false;
        $mangersData = \backend\Modules\Company\models\CompanyEmployee::getManagers();
        $managers = \Yii::$app->params['accEmployees'];
        foreach($mangersData as $key => $item) {
            array_push($managers, $item->id);
        }
        if( in_array($this->module->params['employeeId'], $managers) ) $manager = true;
        return $this->render('_todo', ['events' => $events, 'actions' => $actions, 'stats' => $stats, 'id' => $id, 'manager' => $manager]);   
    }
    
    public function actionBusydays() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $days = [];
        
        $month = date('m'); $year = date('Y');
        if( isset($_GET['month']) && !empty($_GET['month']) ) {
            $month = $_GET['month'];
            $year = $_GET['year'];
        }
        if( isset($_GET['id']) && !empty($_GET['id']) ) {
            $id = $_GET['id'];
        } else {
            $id = $this->module->params['employeeId'];
        }
        if($month == 1) {
            $dateFrom = ($year-1).'-11-1';
            $dateTo = ($year).'-03-31';
        } else if($month == 2) {
            $dateFrom = ($year-1).'-11-1';
            $dateTo = ($year).'-03-31';
        } else if($month == 11) {
            $dateFrom = ($year).'-09-1';
            $dateTo = ($year+1).'-01-31';
        } else if($month == 12) {
            $dateFrom = ($year).'-10-1';
            $dateTo = ($year+1).'-02-31';
        } else {
            $dateFrom = $year.'-'.str_pad(($month-2), 1, '0', STR_PAD_LEFT).'-1';
            $dateTo = $year.'-'.str_pad(($month+2), 1, '0', STR_PAD_LEFT).'-31';
        }
        
        //$days = CalTask::find()->where(['status' => 1])->andWhere("event_date between '". $dateFrom ."' and '". $dateTo ."' ")->select('event_date')->distinct()->all();
        //$sql = "select distinct( date_format(event_date, '%Y-%c-%e')) as event_date from {{%cal_task}} where event_date between '". $dateFrom ."' and '". $dateTo ."'";
        $sql = "select distinct( date_format(todo_date, '%Y-%c-%e')) as todo_date from {{%cal_todo}}"
                ." where status = 1 and id_employee_fk = ".$id." and todo_date between '". $dateFrom ."' and '". $dateTo ."'" ;
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($data as $key => $value) {
            array_push($days, $value['todo_date']);
        }
        
        $sql = "select distinct( date_format(event_date, '%Y-%c-%e')) as event_date from {{%cal_task}}"
                ." where event_date between '". $dateFrom ."' and '". $dateTo ."'"
                ." and id in (select id_task_fk from {{%task_employee}} where status = 1 and is_show = 1 and id_employee_fk = ".$id.")";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($data as $key => $value) {
            array_push($days, $value['event_date']);
        }
        
        $sql = "select distinct( date_format(action_date, '%Y-%c-%e')) as action_date from {{%acc_actions}}"
                ." where id_employee_fk = ".$id." and action_date between '". $dateFrom ."' and '". $dateTo ."'" ;
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($data as $key => $value) {
            array_push($days, $value['action_date']);
        }
        return ['days' => $days];
    }
    
    public function actionEvents($id) {
        $id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        //$employeeId = ( isset($_GET['e']) && !empty($_GET['e']) ) ? $_GET['e'] : $this->module->params['employeeId'];
        
        $timeStamp = date('Y-m-d');
        $period = date('Y-m');
        
        $stats['hours_per_day'] = 0;
        $stats['hours_per_month'] = 0;
		
		$tasksOl = '';
        
        if( isset($_GET['time']) && !empty($_GET['time']) ) {
            $timeStamp = date('Y-m-d', $_GET['time']);
            $period = date('Y-m', $_GET['time']);
        }
                  
        /*$events = \backend\Modules\Task\models\CalTask::find()
                ->where(['status' => 1, "event_date" => $timeStamp])
                ->andWhere('id in (select id_task_fk from {{%task_employee}} where status = 1 and is_show = 1 and id_employee_fk = '.$id.')')
                ->orderby('event_time')->all(); */
        $eventsSql = "select 'todo' as event_type, ct.id as id, ct.name as name, todo_time as event_time, execution_time, id_dict_todo_type_fk, 1 as id_dict_task_status_fk, c.name as cname, c.symbol as csymbol "
                ." from {{%cal_todo}} ct left join {{%cal_case}} cc on cc.id=ct.id_set_fk left join {{%customer}} c on c.id=cc.id_customer_fk"
                ." where ct.status = 1 and ct.id_dict_todo_type_fk = 8 and id_employee_fk = ".$id." and todo_date = '".$timeStamp."'"
                ." union"
                ." select 'task' as event_type, e.id, e.name as name, event_time, execution_time, e.type_fk as type_fk, id_dict_task_status_fk, c.name as cname, c.symbol as csymbol "
                ." from {{%cal_task}} e join {{%task_employee}} te on e.id=te.id_task_fk join {{%customer}} c on c.id=e.id_customer_fk"
                ." where e.status = 1 and te.status = 1 and te.id_employee_fk = ".$id." and event_date = '".$timeStamp."'"
                ." order by 3";
        $events = Yii::$app->db->createCommand($eventsSql)->queryAll();
        
		$tasksList = ''; $eventsList = '';
        foreach($events as $key => $event) {
            $color = 'default'; $icon = 'tasks';
            if($event['event_type'] == 'task') {
                if($event['id_dict_todo_type_fk'] == 1) { $color = 'purple'; $icon = 'gavel'; }
                if($event['id_dict_todo_type_fk'] == 2) { $color = 'teal'; }
                $btn = '<div class="btn-group pull-right">'
                        .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                            .'Opcje <span class="caret"></span>'
                        .'</button>'
                        .'<ul class="dropdown-menu">'
                            . (($event['id_dict_task_status_fk']==1) ? '<li><a href="'.Url::to(['/task/event/closeajax', 'id' => $event['id']]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                            .'<li><a href="'.Url::to(['/task/event/showajax', 'id'=>$event['id']]).'" data-target="#modal-grid-item" class="gridViewModal" title="Podglad zadania"><i class="fa fa-eye text--blue2"></i>Podgląd zadania</a></li>'
                            .'<li><a href="'.Url::to(['/task/personal/task', 'id'=>$event['id']]).'?e='.$id.'" data-target="#modal-grid-item" class="gridViewModal" data-label="Zarejestruj czynność" title="Zarejestruj czynność"><i class="fa fa-calendar-plus-o text--purple2"></i>Zarejestruj czynność</a></li>'
                        .'</ul>'
                    .'</div><div class="clear"></div>';
            } else {
                $color = 'navy'; $icon = 'user';
                $btn = '<div class="btn-group pull-right">'
                            .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                .'Operacje <span class="caret"></span>'
                            .'</button>'
                            .'<ul class="dropdown-menu">'
                                //. (($task->is_close == 0) ? '<li><a href="'.Url::to(['/task/personal/state', 'id' => $task->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                                .'<li><a href="'.Url::to(['/task/personal/updateajax', 'id' => $event['id']]) .'" data-target="#modal-grid-item" title="Edytuj zadanie" class="gridViewModal"><i class="fa fa-pencil text--grey2"></i>Edytuj zadanie</a></li>'
                                .'<li><a href="'.Url::to(['/task/personal/task', 'id'=>$event['id']]).'?type=todo&e='.$id.'" data-target="#modal-grid-item" class="gridViewModal" title="Zarejestruj czynność"><i class="fa fa-calendar-plus-o text--purple2"></i>Zarejestruj czynność</a></li>' 
                            .'</ul>'
                        .'</div><div class="clear"></div>';
            }
            
            $bg = '';
            if($event['id_dict_task_status_fk'] == 2) $bg = " bg-green2";
            $eventsList .=  '<div class="bs-callout bs-callout-'.$color. $bg.'" id="eid-'.$event['id'].'">'
                                .'<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.(($event['csymbol'])?$event['csymbol']:$event['cname']).$btn.'</h4>'.$event['name']
                                .'<p>'
                                    .'<small><span class="text-muted"><i class="fa fa-clock-o"></i> </span> '.( ($event['event_time']) ? $event['event_time'] : 'brak').'</small> <span class="divider">|</span>'
                                    .'<small><span class="text-muted"><i class="fa fa-hourglass-3"></i> </span> ' .( ($event['execution_time']) ? round($event['execution_time']/60,2) : '0'). ' h</small>'
                                .'</p>'
                            .'</div>';
        }
        
        $actionsSql = "select 'todo' as action_type, id, todo_time, execution_time, name, id_dict_todo_type_fk as type_fk, '' as cname, '' as csymbol, description, 0 as id_event_fk, 0 as id_task_fk  from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$id." and date_format(todo_date, '%Y-%m-%d') = '".$timeStamp."'"
                ." union"
                ." select 'action' as action_type, a.id, 0 as todo_time, unit_time, a.name, id_service_fk as type_fk, c.name as cname, c.symbol as csymbol, a.description, id_event_fk, id_task_fk  from law_acc_actions a join law_customer c on a.id_customer_fk=c.id"
                ." where a.status = 1 and id_employee_fk = ".$id." and date_format(action_date, '%Y-%m-%d') = '".$timeStamp."'";
        $tasks = Yii::$app->db->createCommand($actionsSql)->queryAll();
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        $dictPersonal = \backend\Modules\Task\models\CalTodo::dictTypes();
        foreach($tasks as $key => $task) {
            $color = 'blue'; $icon = 'user';
            if($task['action_type'] == 'todo') {
                $color = $dictPersonal[$task['type_fk']]['color'];
                $icon = $dictPersonal[$task['type_fk']]['icon'];
                $url = Url::to(['/task/personal/updateajax', 'id' => $task['id']]);
            } else {
                $color = $dict[$task['type_fk']]['color'];
                $icon = $dict[$task['type_fk']]['icon'];
                $url = Url::to(['/accounting/action/change', 'id' => $task['id']]);
            }
            $tasksOl = '<li>'.$task['description'].' ['.round($task['execution_time'] / 60, 2).' h]</li>';
            $stats['hours_per_day'] += round($task['execution_time'] / 60, 2);
            
            $btn = '<div class="btn-group pull-right">'
                        .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                            .'Opcje <span class="caret"></span>'
                        .'</button>'
                        .'<ul class="dropdown-menu">'
                            //. (($task->is_close == 0) ? '<li><a href="'.Url::to(['/task/personal/state', 'id' => $task->id]).'" class="deleteConfirm" data-label="Zamknij zadanie" title="Oznacz jako zakończone"><i class="fa fa-check text--green2"></i>Zakończ</a></li>' : '' )
                            .'<li><a href="'. $url .'" data-target="#modal-grid-item" title="Edytuj czynność" class="gridViewModal"><i class="fa fa-pencil text--grey2"></i>Edytuj czynność</a></li>'
                            //.'<li><a href="'.Url::to(['/task/personal/task', 'id'=>$task['id']]).'?type=todo&e='.$id.'" data-target="#modal-grid-item" class="gridViewModal" title="Zarejestruj czynność"><i class="fa fa-calendar-plus-o text--purple2"></i>Zarejestruj czynność</a></li>'                         
                        .'</ul>'
                    .'</div><div class="clear"></div>';
            $bg = '';
           // if($task->is_close == 1) $bg = " bg-green2";
            $tasksList .=  '<div class="bs-callout bs-callout-'.$color. $bg.'" id="tid-'.$task['id'].'">'
                                .'<h4 class="btn-icon"><i class="fa fa-'.$icon.'"></i>'.$task['name'].$btn.'</h4>'.$task['description']
                                .'<p>'
                                    .'<small><span class="text-muted"><i class="fa fa-clock-o"></i> </span> '.( ($task['todo_time']) ? $task['todo_time'] : 'brak').'</small> <span class="divider">|</span>'
                                    .'<small><span class="text-muted"><i class="fa fa-hourglass-3"></i> </span> ' .( ($task['execution_time']) ? round($task['execution_time']/60,2) : '0'). ' h</small>'
                                .'</p>'
                                . ( ($task['action_type'] == 'action') ? '<p><i>na rzecz klienta: </i>'.(($task['csymbol'])?$task['csymbol']:$task['cname']).'</p>' : '<p><i>czynności administracyjne: </i></p>' )
                            .'</div>';
 
        }
        
        if($eventsList == '') $eventsList = '<div class="alert alert-warning">Brak zadań kancelaryjnych na wybrany dzień</div>';
        if($tasksList == '') $tasksList = '<div class="alert alert-warning">Brak zadań osobistych na wybrany dzień</div>';
        
        $sql = "select sum(case when todo_date = '".$timeStamp."' then execution_time else 0 end) per_day, sum(execution_time) per_month from {{%cal_todo}}"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$id." and date_format(todo_date, '%Y-%m') = '".$period."'"
                ." union"
                ." select sum(case when action_date = '".$timeStamp."' then unit_time else 0 end) per_day, sum(unit_time) per_month from {{%acc_actions}}"
                ." where status = 1 and id_employee_fk = ".$id." and date_format(action_date, '%Y-%m') = '".$period."'";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataStats as $key => $stat) {
            //$stats['hours_per_day'] += $stat['per_day'];
            $stats['hours_per_month'] += round($stat['per_month']/60,2);
        }
        if($tasksOl == '') $tasksOl = '<li>brak zadań na wybrany dzień</li>';
        return ['tasks' => $tasksList, 'events' => $eventsList, 'stats' => $stats, 'checkDay' => $timeStamp, 'tasksOl' => $tasksOl];
    }
	
	public function actionAcc($id) {
       // $id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        //$employeeId = ( isset($_GET['e']) && !empty($_GET['e']) ) ? $_GET['e'] : $this->module->params['employeeId'];        
        $timeStamp = date('Y-m-d');
        $period = date('Y-m');
        
        $stats['hours_per_day'] = 0;
        $stats['hours_per_month'] = 0;
		
		$tasksOl = '';
        
        if( isset($_GET['time']) && !empty($_GET['time']) ) {
            $timeStamp = date('Y-m-d', $_GET['time']);
            $period = date('Y-m', $_GET['time']);
        }
		
		$listOrder = '';
		
		if(isset($_GET['cid']) && !empty($_GET['cid']) ) {
			$orders = [];
			$dataOrderSql = \backend\Modules\Accounting\models\AccOrder::find()
					->where(['status' => 1])
					->andWhere('(id_customer_fk = '.$_GET['cid'].' or id in (select id_order_fk from {{%acc_order_item}} where id_customer_fk = '.$_GET['cid'].'))');
					if((isset($_GET['time']) && !empty($_GET['time']))) { 
						$dataOrderSql->andWhere(" '".$timeStamp."' between date_from and ifnull(date_to, '".$timeStamp."')");
					}
			$dataOrder = $dataOrderSql->all();
		
			
			if($dataOrder) {
				foreach($dataOrder as $key => $order) {
					//$orders[$order->id] = $order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : '');
					//array_push($orders, ['id' => $order->id, 'name' => ($order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : ''))]);
					$listOrder .= '<option value="'.$order->id.'">'.($order->name.( ($order->id_customer_fk != $_GET['cid']) ? ' ['.$order->customer['name'].']' : '')).'</option>';
				}
			}
	    } 
                  
        $stats = $this->personalStats($id, $timeStamp);
        return ['stats' => $stats, 'listOrder' => $listOrder];
    }
    
    public function actionChart() {
        $stats = [];
        
        if( isset($_GET['year']) && !empty($_GET['year']) ) $year = $_GET['year']; else $year = date('Y');
        if( isset($_GET['employee']) && !empty($_GET['employee']) ) $employee = $_GET['employee']; else $employee = $this->module->params['employeeId'];
        
        $stats['actions'][0] = 0;  $stats['events'][0] = 0;  $stats['total'][0] = 0;
        $stats['actions'][1] = 0;  $stats['events'][1] = 0;  $stats['total'][1] = 0;
        $stats['actions'][2] = 0;  $stats['events'][2] = 0;  $stats['total'][2] = 0;
        $stats['actions'][3] = 0;  $stats['events'][3] = 0;  $stats['total'][3] = 0;
        $stats['actions'][4] = 0;  $stats['events'][4] = 0;  $stats['total'][4] = 0;
        $stats['actions'][5] = 0;  $stats['events'][5] = 0;  $stats['total'][5] = 0;
        $stats['actions'][6] = 0;  $stats['events'][6] = 0;  $stats['total'][6] = 0;
        $stats['actions'][7] = 0;  $stats['events'][7] = 0;  $stats['total'][7] = 0;
        $stats['actions'][8] = 0;  $stats['events'][8] = 0;  $stats['total'][8] = 0;
        $stats['actions'][9] = 0;  $stats['events'][9] = 0;  $stats['total'][9] = 0;
        $stats['actions'][10] = 0; $stats['events'][10] = 0; $stats['total'][10] = 0;
        $stats['actions'][11] = 0; $stats['events'][11] = 0; $stats['total'][11] = 0;
        
        $query = (new \yii\db\Query())
            ->select(["date_format(action_date, '%Y-%m') as period_name","sum(round(unit_time/60)) as period_sum"])
            ->from('{{%acc_actions}} as a')
            ->where( ['status' => 1, 'id_employee_fk' => $employee, 'extract(year from action_date)' => $year] )
            ->groupby(["date_format(action_date, '%Y-%m')"]);
        $actions = $query->all();
        
        foreach($actions as $key => $value) {
            if($value['period_name'] == $year.'-01') { $stats['actions'][0] += $value['period_sum']; $stats['total'][0] += $value['period_sum']; }
            if($value['period_name'] == $year.'-02') { $stats['actions'][1] += $value['period_sum']; $stats['total'][1] += $value['period_sum']; }
            if($value['period_name'] == $year.'-03') { $stats['actions'][2] += $value['period_sum']; $stats['total'][2] += $value['period_sum']; } 
            if($value['period_name'] == $year.'-04') { $stats['actions'][3] += $value['period_sum']; $stats['total'][3] += $value['period_sum']; }
            if($value['period_name'] == $year.'-05') { $stats['actions'][4] += $value['period_sum']; $stats['total'][4] += $value['period_sum']; }
            if($value['period_name'] == $year.'-06') { $stats['actions'][5] += $value['period_sum']; $stats['total'][5] += $value['period_sum']; }
            if($value['period_name'] == $year.'-07') { $stats['actions'][6] += $value['period_sum']; $stats['total'][6] += $value['period_sum']; }
            if($value['period_name'] == $year.'-08') { $stats['actions'][7] += $value['period_sum']; $stats['total'][7] += $value['period_sum']; }
            if($value['period_name'] == $year.'-09') { $stats['actions'][8] += $value['period_sum']; $stats['total'][8] += $value['period_sum']; }
            if($value['period_name'] == $year.'-10') { $stats['actions'][9] += $value['period_sum']; $stats['total'][9] += $value['period_sum']; }
            if($value['period_name'] == $year.'-11') { $stats['actions'][10] += $value['period_sum']; $stats['total'][10] += $value['period_sum']; }
            if($value['period_name'] == $year.'-12') { $stats['actions'][11] += $value['period_sum']; $stats['total'][11] += $value['period_sum']; }
        }
        
        $query = (new \yii\db\Query())
            ->select(["date_format(todo_date, '%Y-%m') as period_name","sum(round(execution_time/60)) as period_sum"])
            ->from('{{%cal_todo}} as t')
            ->where( ['status' => 1, 'type_fk' => 1, 'id_employee_fk' => $employee, 'extract(year from todo_date)' => $year] )
            ->groupby(["date_format(todo_date, '%Y-%m')"]);
        $events = $query->all();
        
        foreach($events as $key => $value) {
            if($value['period_name'] == $year.'-01') { $stats['events'][0] += $value['period_sum']; $stats['total'][0] += $value['period_sum']; }
            if($value['period_name'] == $year.'-02') { $stats['events'][1] += $value['period_sum']; $stats['total'][1] += $value['period_sum']; }
            if($value['period_name'] == $year.'-03') { $stats['events'][2] += $value['period_sum']; $stats['total'][2] += $value['period_sum']; } 
            if($value['period_name'] == $year.'-04') { $stats['events'][3] += $value['period_sum']; $stats['total'][3] += $value['period_sum']; }
            if($value['period_name'] == $year.'-05') { $stats['events'][4] += $value['period_sum']; $stats['total'][4] += $value['period_sum']; }
            if($value['period_name'] == $year.'-06') { $stats['events'][5] += $value['period_sum']; $stats['total'][5] += $value['period_sum']; }
            if($value['period_name'] == $year.'-07') { $stats['events'][6] += $value['period_sum']; $stats['total'][6] += $value['period_sum']; }
            if($value['period_name'] == $year.'-08') { $stats['events'][7] += $value['period_sum']; $stats['total'][7] += $value['period_sum']; }
            if($value['period_name'] == $year.'-09') { $stats['events'][8] += $value['period_sum']; $stats['total'][8] += $value['period_sum']; }
            if($value['period_name'] == $year.'-10') { $stats['events'][9] += $value['period_sum']; $stats['total'][9] += $value['period_sum']; }
            if($value['period_name'] == $year.'-11') { $stats['events'][10] += $value['period_sum']; $stats['total'][10] += $value['period_sum']; }
            if($value['period_name'] == $year.'-12') { $stats['events'][11] += $value['period_sum']; $stats['total'][11] += $value['period_sum']; }
        }
        $model = \backend\Modules\Company\models\CompanyEmployee::findOne($employee);
        return $this->render('_stats', ['stats' => $stats, 'year' => $year, 'employee' => $model]);   
    }
    
    public function actionStatsexport($eid, $year) {
        
        $query = (new \yii\db\Query())
            ->select(["date_format(action_date, '%Y-%m') as period_name","sum(round(unit_time/60)) as period_sum"])
            ->from('{{%acc_actions}} as a')
            ->where( ['status' => 1, 'id_employee_fk' => $eid, 'extract(year from action_date)' => $year] )
            ->groupby(["date_format(action_date, '%Y-%m')"]);
        $actions = $query->all();
        
        $query = (new \yii\db\Query())
            ->select(["date_format(todo_date, '%Y-%m') as period_name","sum(round(execution_time/60)) as period_sum"])
            ->from('{{%cal_todo}} as t')
            ->where( ['status' => 1, 'type_fk' => 1, 'id_employee_fk' => $eid, 'extract(year from todo_date)' => $year] )
            ->groupby(["date_format(todo_date, '%Y-%m')"]);
        $events = $query->all();
        
        $objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $objWorksheet = $objPHPExcel->getActiveSheet();
        
        $tempArr = [];
        array_push($tempArr, ['', 'na rzecz klienta', 'administracyjne', 'wszystkie']);
        
        $stats[0]['actions'] = 0;  $stats[0]['events'] = 0;  $stats[0]['total'] = 0;
        $stats[1]['actions'] = 0;  $stats[1]['events'] = 0;  $stats[1]['total'] = 0;
        $stats[2]['actions'] = 0;  $stats[2]['events'] = 0;  $stats[2]['total'] = 0;
        $stats[3]['actions'] = 0;  $stats[3]['events'] = 0;  $stats[3]['total'] = 0;
        $stats[4]['actions'] = 0;  $stats[4]['events'] = 0;  $stats[4]['total'] = 0;
        $stats[5]['actions'] = 0;  $stats[5]['events'] = 0;  $stats[5]['total'] = 0;
        $stats[6]['actions'] = 0;  $stats[6]['events'] = 0;  $stats[6]['total'] = 0;
        $stats[7]['actions'] = 0;  $stats[7]['events'] = 0;  $stats[7]['total'] = 0;
        $stats[8]['actions'] = 0;  $stats[8]['events'] = 0;  $stats[8]['total'] = 0;
        $stats[9]['actions'] = 0;  $stats[9]['events'] = 0;  $stats[9]['total'] = 0;
        $stats[10]['actions'] = 0; $stats[10]['events'] = 0; $stats[10]['total'] = 0;
        $stats[11]['actions'] = 0; $stats[11]['events'] = 0; $stats[11]['total'] = 0;
        foreach($actions as $key => $value) {
            if($value['period_name'] == $year.'-01') { $stats[0]['actions'] += $value['period_sum']; $stats[0]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-02') { $stats[1]['actions'] += $value['period_sum']; $stats[1]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-03') { $stats[2]['actions'] += $value['period_sum']; $stats[2]['total'] += $value['period_sum']; } 
            if($value['period_name'] == $year.'-04') { $stats[3]['actions'] += $value['period_sum']; $stats[3]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-05') { $stats[4]['actions'] += $value['period_sum']; $stats[4]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-06') { $stats[5]['actions'] += $value['period_sum']; $stats[5]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-07') { $stats[6]['actions'] += $value['period_sum']; $stats[6]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-08') { $stats[7]['actions'] += $value['period_sum']; $stats[7]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-09') { $stats[8]['actions'] += $value['period_sum']; $stats[8]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-10') { $stats[9]['actions'] += $value['period_sum']; $stats[9]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-11') { $stats[10]['actions'] += $value['period_sum']; $stats[10]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-12') { $stats[11]['actions'] += $value['period_sum']; $stats[11]['total'] += $value['period_sum']; }
        }
        foreach($events as $key => $value) {
            if($value['period_name'] == $year.'-01') { $stats[0]['events'] += $value['period_sum']; $stats[0]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-02') { $stats[1]['events'] += $value['period_sum']; $stats[1]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-03') { $stats[2]['events'] += $value['period_sum']; $stats[2]['total'] += $value['period_sum']; } 
            if($value['period_name'] == $year.'-04') { $stats[3]['events'] += $value['period_sum']; $stats[3]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-05') { $stats[4]['events'] += $value['period_sum']; $stats[4]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-06') { $stats[5]['events'] += $value['period_sum']; $stats[5]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-07') { $stats[6]['events'] += $value['period_sum']; $stats[6]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-08') { $stats[7]['events'] += $value['period_sum']; $stats[7]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-09') { $stats[8]['events'] += $value['period_sum']; $stats[8]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-10') { $stats[9]['events'] += $value['period_sum']; $stats[9]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-11') { $stats[10]['events'] += $value['period_sum']; $stats[10]['total'] += $value['period_sum']; }
            if($value['period_name'] == $year.'-12') { $stats[11]['events'] += $value['period_sum']; $stats[11]['total'] += $value['period_sum']; }
        }
        $months = [0 => 'Styczeń', 1 => 'Luty', 2 => 'Marzec', 3 => 'Kwiecień', 4 => 'Maj', 5 => 'Czerwiec', 6 => 'Lipiec', 7 => 'Sierpień', 8 => 'Wrzesień', 9 => 'Październik', 10 => 'Listopad', 11 => 'Grudzień'];
        foreach($stats as $key => $value) {
            array_push($tempArr, [$months[$key], (($value['actions']) ? $value['actions'] : "0"), (($value['events'])?$value['events']:"0"), (($value['total'])?$value['total']:"0")]);
        }
        
        $objWorksheet->fromArray($tempArr
           /* array(
                array('',		'Rainfall (mm)',	'Temperature (°F)',	'Humidity (%)'),
                array('Jan',		78,   				52,					61),
                array('Feb',		64,   				54,					62),
                array('Mar',		62,   				57,					63),
                array('Apr',		21,   				62,					59),
                array('May',		11,   				75,					60),
                array('Jun',		1,   				75,					57),
                array('Jul',		1,   				79,					56),
                array('Aug',		1,   				79,					59),
                array('Sep',		10,   				75,					60),
                array('Oct',		40,   				68,					63),
                array('Nov',		69,   				62,					64),
                array('Dec',		89,   				57,					66),
            )*/
        );


        //	Set the Labels for each data series we want to plot
        //		Datatype
        //		Cell reference for data
        //		Format Code
        //		Number of datapoints in series
        //		Data values
        //		Data Marker
        $dataSeriesLabels1= array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$1', NULL, 1),	
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$C$1', NULL, 1),	
        );

        $dataSeriesLabels2 = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$D$1', NULL, 1),	
        );

        //	Set the X-Axis Labels
        //		Datatype
        //		Cell reference for data
        //		Format Code
        //		Number of datapoints in series
        //		Data values
        //		Data Marker
        $xAxisTickValues = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$2:$A$13', NULL, 12),	
        );


        //	Set the Data values for each data series we want to plot
        //		Datatype
        //		Cell reference for data
        //		Format Code
        //		Number of datapoints in series
        //		Data values
        //		Data Marker
        $dataSeriesValues1 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$B$2:$B$13', NULL, 12),
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$C$2:$C$13', NULL, 12),
        );

        //	Build the dataseries
        $series1 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
            \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,	// plotGrouping
            range(0, count($dataSeriesValues1)-1),			// plotOrder
            $dataSeriesLabels1,								// plotLabel
            $xAxisTickValues,								// plotCategory
            $dataSeriesValues1								// plotValues
        );
        //	Set additional dataseries parameters
        //		Make it a vertical column rather than a horizontal bar graph
        $series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);


        //	Set the Data values for each data series we want to plot
        //		Datatype
        //		Cell reference for data
        //		Format Code
        //		Number of datapoints in series
        //		Data values
        //		Data Marker
        $dataSeriesValues2 = array(
            new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$D$2:$D$13', NULL, 12),
        );

        //	Build the dataseries
        $series2 = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
            \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
            range(0, count($dataSeriesValues2)-1),			// plotOrder
            $dataSeriesLabels2,								// plotLabel
            NULL,											// plotCategory
            $dataSeriesValues2								// plotValues
        );
        
        $layout = new \PHPExcel_Chart_Layout();

        //	Set the series in the plot area
        $plotArea = new \PHPExcel_Chart_PlotArea($layout, array($series1, $series2));
        //	Set the chart legend
        $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($eid);
        $title = new \PHPExcel_Chart_Title('Aktywność pracownika '.$employee->fullname.' na rok '.$year);


        //	Create the chart
        $chart = new \PHPExcel_Chart(
            'chart1',		// name
            $title,			// title
            $legend,		// legend
            $plotArea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            NULL,			// xAxisLabel
            NULL			// yAxisLabel
        );

        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition('B2');
        $chart->setBottomRightPosition('P25');
        
        $objPHPExcel->getActiveSheet()->setTitle('Data');
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('Wykres');
                
        //	Add the chart to the worksheet
        $objPHPExcel->getActiveSheet()->addChart($chart);        
        //$objPHPExcel->getActiveSheet()->setTitle('Zestawienie za rok 2017');
        //$objPHPExcel->setActiveSheetIndex(0); 
        
        $filename = 'Roczne_zestawienie_godzin'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');

    }
    
    public function actionTask($id) {
        $id = CustomHelpers::decode($id);
        $event = false; $todo = false;
        if($id != 0) {
            if(isset($_GET['type']) && $_GET['type'] == 'todo') {
                $todo = \backend\Modules\Task\models\CalTodo::findOne($id);
            } else {
                $event = \backend\Modules\Task\models\CalTask::findOne($id);
            }
        }
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = new ActionForm();
        $model->timeDate = date('Y-m-d');
        $model->id_employee_fk = ( isset($_GET['e']) && !empty($_GET['e']) ) ? $_GET['e'] : $this->module->params['employeeId'];
        //$model->timeH = 0;
        //$model->timeM = 0;
        $model->type = 1;
        
        if($event) {
            $model->timeDate = $event->event_date;
            $model->description = $event->name;
            $model->id_customer_fk = $event->case['id_customer_fk'];
            $model->id_case_fk = $event->id_case_fk;
            $model->id_event_fk = $id;
        }
        
        if($todo) {
            $model->timeDate = $todo->todo_date;
            $model->description = $todo->name;
            $model->id_customer_fk = $todo->case['id_customer_fk'];
            $model->id_case_fk = $todo->id_set_fk;
            $model->id_task_fk = $id;
            
            $model->timeH = intval($todo->execution_time/60);
            $model->timeM = $todo->execution_time - ($model->timeH*60);
        }
                      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
           // if(empty($model->timeM)) $model->timeM = 0;
           // if(empty($model->timeH)) $model->timeH = 0;
            
	        $model->executionTime = $model->timeH*60 + $model->timeM;
                      
            if($model->validate() && $model->saveAction()) {				
                $stats = $this->personalStats($model->id_employee_fk, $model->timeDate);
                $model->method_1 = 1;
                $model->timeH = '';
				$model->timeM = '';
				$model->description = null;
				//$model->id_customer_fk = null;
                $model->id_case_fk = null;
				$model->cost_net = null;
				$model->cost_description = null;
                return array('success' => true, 'action' => 'create', 'close' => false, 'type' => 'action_'.$model->type, 'id' => $model->id, 
                             'alert' => 'Czynność została zarejestrowana', 'table' => '#table-actions', 'html' => $this->renderAjax('_formAction', [ 'model' => $model, 'stats' => $stats, 'isManager' => count($this->module->params['managers'])]),'todoWidget' => TodoWidget::widget(['refresh' => true])  );	
			
            } else {
                $stats = $this->personalStats($model->id_employee_fk, $model->timeDate);
				return array('success' => false, 'table' => '#table-actions', 'html' => $this->renderAjax('_formAction', [ 'model' => $model, 'stats' => $stats, 'isManager' => count($this->module->params['managers'])]), 'errors' => $model->getErrors() );	
			}		
		} else {
            $stats = $this->personalStats($model->id_employee_fk, $model->timeDate);
			return  $this->renderAjax('_formAction', [ 'model' => $model, 'stats' => $stats, 'isManager' => count($this->module->params['managers'])]);	
        }
    }
    
    public function actionFast($type) {
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = new ActionFast();
        $model->type = $type;
        $model->hours_of_day = 8;
        $model->id_employee_fk = $employee->id;
                         
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->validate() && $model->saveAction()) {				
                return array('success' => true, 'action' => 'create', 'type' => 'action_'.$model->type, 'name' => 'Nowa czynność', 'alert' => 'Czynność została zarejestrowana', 'table' => '#table-actions', 'todoWidget' => TodoWidget::widget(['refresh' => true])  );	
            } else {
                return array('success' => false, 'table' => '#table-actions', 'html' => $this->renderAjax('_formFast', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formFast', [ 'model' => $model]);	
        }
    }
    
    public function actionMeeting($id) {
        $id = CustomHelpers::decode($id);
        $member = \backend\Modules\Community\models\ComMeetingMember::findOne($id);
        $meeting = $member->meeting;
       /* $event = false;
        if($id != 0) {
            $event = \backend\Modules\Task\models\CalTask::findOne($id);
        }*/
		//$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = new ActionForm();
        $model->timeDate = date('Y-m-d', strtotime($meeting->date_from));
        $model->id_employee_fk = $member->id_employee_fk;
        $time = (strtotime($meeting->date_to) - strtotime($meeting->date_from))/60;
        if($meeting->date_to) {
            $model->timeH = floor($time/60);
            $model->timeM = $time-($model->timeH*60);
        }
        if( $meeting['id_customer_fk'] ) {
            $model->type = 1;
            $model->id_customer_fk = $meeting['id_customer_fk'];
        } else {
            $model->type = 2;
        }
        $model->description = $meeting['title'];
        $model->id_meeting_fk = $member->id;
                        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
	        $model->executionTime = $model->timeH*60 + $model->timeM;
            if($model->validate() && $model->saveAction()) {				
                return array('success' => true, 'action' => 'create', 'type' => 'action_'.$model->type, 'id' => $model->id, 'name' => 'Nowa czynność', 'alert' => 'Czynność została zarejestrowana', 'table' => '#table-actions', 'todoWidget' => TodoWidget::widget(['refresh' => true])  );		
            } else {
                return array('success' => false, 'table' => '#table-actions', 'html' => $this->renderAjax('_formMeeting', [ 'model' => $model, 'meetingId' => $member->id_meeting_fk]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formMeeting', [ 'model' => $model, 'meetingId' => $member->id_meeting_fk]);	
        }
    }
    
    public function actionStats() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $timeStamp = date('Y-m-d');
        $period = date('Y-m');
        
        $stats['hours_per_day'] = 0;
        $stats['hours_per_month'] = 0;
        $stats['actions_per_month'] = 0;
        $stats['todos_per_month'] = 0;
        
        $sql = "select sum(case when todo_date = '".$timeStamp."' then execution_time else 0 end) per_day, sum(execution_time) per_month from law_cal_todo"
                ." where id_employee_fk = ".$this->module->params['employeeId']." and date_format(todo_date, '%Y-%m') = '".$period."'"
                ." union"
                ." select sum(case when action_date = '".$timeStamp."' then unit_time else 0 end) per_day, sum(unit_time) per_month from law_acc_actions"
                ." where id_employee_fk = ".$this->module->params['employeeId']." and date_format(action_date, '%Y-%m') = '".$period."'";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataStats as $key => $stat) {
            $stats['hours_per_day'] += round($stat['per_day']/60,2);
            $stats['hours_per_month'] += round($stat['per_month']/60,2);
            
            if($key == 0)
                $stats['todos_per_month'] += round($stat['per_month']/60,2);
            else
                $stats['actions_per_month'] += round($stat['per_month']/60,2);
        }
        
        return $stats;
    }
    
    public function actionServices($type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $services = '';
        $data = \backend\Modules\Accounting\models\AccDictionary::find()->where( ['type_fk' => $type])->orderby('name')->all();
        foreach($data as $key => $value) {
            $services .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return ['list' => $services];
    }
    
    public function actionActions() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActionsSearch();
        $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
        if($dateDay <= 10 && $dateMonth > 1) { 
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->date_from = $dateYear.'-'.$month.'-01';
            $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
            $searchModel->date_from = ($dateYear-1).'-12-01';
            $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
        } else {
            $searchModel->date_from = $dateYear.'-'.date('m').'-01';
            $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        }
        
        if(isset($_GET['year']) && !empty($_GET['year']) && isset($_GET['month']) && !empty($_GET['month'])) {
            $searchModel->date_from = $_GET['year'].'-'.$_GET['month'].'-01';
            $searchModel->date_to = $_GET['year'].'-'.$_GET['month'].date('t', mktime(0, 0, 0, ($_GET['year']-1), 1, ($_GET['month']-1)));
            $searchModel->id_employee_fk = '';
        }
		
		if(isset($_GET['employeeId'])) {
			$searchModel->id_employee_fk = $_GET['employeeId'];
		}
        if(isset($_GET['period'])) {
			$searchModel->date_from = $_GET['period'].'-01';
			$periodArr = explode('-', $_GET['period']);
			$searchModel->date_to = $_GET['period'].date('t', mktime(0, 0, 0, $periodArr[1], 1, $periodArr[0]));
        }
   
        return $this->render('actions', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'manager' => true
        ]);
    }
    
	public function personalStats($employeeId, $date)  {
		$stats['hours_per_day'] = 0;
		$stats['hours_per_month'] = 0;
		$stats['tasks'] = '';
        
        $hours_per_day_action = 0;
        $hours_per_month_action = 0;
        $hours_per_day_admin = 0;
        $hours_per_month_admin = 0;
        
        if(!$employeeId) $employeeId = 0;
		
		$sql = "select sum(case when todo_date = '".$date."' then execution_time else 0 end) per_day, sum(execution_time) per_month from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$employeeId." and date_format(todo_date, '%Y-%m') = '".date('Y-m', strtotime($date))."'"
                ." union"
                ." select sum(case when action_date = '".$date."' then unit_time else 0 end) per_day, sum(unit_time) per_month from law_acc_actions"
                ." where status = 1 and id_employee_fk = ".$employeeId." and date_format(action_date, '%Y-%m') = '".date('Y-m', strtotime($date))."'";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataStats as $key => $stat) {
            if($key == 0) { $hours_per_day_admin = round($stat['per_day']/60, 2); $hours_per_month_admin = round($stat['per_month']/60, 2);}
            if($key == 1) { $hours_per_day_action = round($stat['per_day']/60, 2); $hours_per_month_action = round($stat['per_month']/60, 2);}
            $stats['hours_per_day'] += round($stat['per_day']/60, 2);
            $stats['hours_per_month'] += round($stat['per_month']/60, 2);
        }
		$stats['hours_per_day'] .= '<small><i class="fa fa-briefcase" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.$hours_per_day_action
                                    .' / <i class="fa fa-building" data-toggle="tooltip" data-title="Czynności administracyjne"></i>'.$hours_per_day_admin.'</small>';
        $stats['hours_per_month'] .= '<small><i class="fa fa-briefcase" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.$hours_per_month_action
                                    .' / <i class="fa fa-building" data-toggle="tooltip" data-title="Czynności administracyjne"></i>'.$hours_per_month_admin.'</small>';
		/*$sqlTask = "select 2 as type_event, id, execution_time, description, '' as symbol, '' as cname from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$employeeId." and todo_date = '".$date."'"
                ." union"
                ." select 1 as type_event, a.id, unit_time, a.description, c.symbol as symbol, c.name as cname from law_acc_actions a join {{%customer}} c on c.id=a.id_customer_fk"
                ." where a.status = 1 and a.id_employee_fk = ".$employeeId." and action_date = '".$date."'";*/
        $sqlTask = "select 2 as type_event, id, execution_time, description as description, 'Administracyjne' as symbol, '' as cname, '' as dcost, 0 as id_service_fk, 0 as is_confirm  from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$employeeId." and todo_date = '".$date."'"
                ." union"
                ." select 1 as type_event, a.id, unit_time, a.description, c.symbol as symbol, c.name as cname, acc_cost_description, id_service_fk, is_confirm from law_acc_actions a join {{%customer}} c on c.id=a.id_customer_fk"
                ." where a.status = 1 and a.id_employee_fk = ".$employeeId." and action_date = '".$date."'";
        $tasks = Yii::$app->db->createCommand($sqlTask)->queryAll();
		
		foreach($tasks as $key => $task) {
			$cname = ($task['symbol']) ? $task['symbol'] : $task['cname'];
            if($cname) $cname = '<b>'.($cname).'</b> - ';
            if($task['id_service_fk'] == 4) $task['description'] = '<i class="text--yellow">'.$task['dcost'].'</i>';
            $actions = '';
            if(!$task['is_confirm']) {
                $actions = '<a href="'.Url::to(['/task/personal/efast', 'id' => $task['id'], 'type' => $task['type_event']]).'" class="icon-edit gridViewModal" data-target="#modal-grid-event" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>'
                        .'&nbsp;<a href="'.Url::to(['/task/personal/edelete', 'id' => $task['id'], 'type' => $task['type_event']]).'" class="icon-delete deleteConfirm"  title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' ;
            }
            $stats['tasks'] .= '<li>'.$cname.$task['description'].' ['.round($task['execution_time']/60, 2).' h] '
                                     .$actions                              
                                .'</li>';
		}
		
		if(count($tasks) == 0) {
			$stats['tasks'] = '<small class"alert alert-warning">brak czynności na wybrany dzień</small>';
		}
		return $stats;
	}
    
    public static function pStats($employeeId, $date)  {
        $stats['hours_per_day'] = 0;
		$stats['hours_per_month'] = 0;
		$stats['tasks'] = '';
        
        $hours_per_day_action = 0;
        $hours_per_month_action = 0;
        $hours_per_day_admin = 0;
        $hours_per_month_admin = 0;
		
		$sql = "select sum(case when todo_date = '".$date."' then execution_time else 0 end) per_day, sum(execution_time) per_month from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$employeeId." and date_format(todo_date, '%Y-%m') = '".date('Y-m', strtotime($date))."'"
                ." union"
                ." select sum(case when action_date = '".$date."' then unit_time else 0 end) per_day, sum(unit_time) per_month from law_acc_actions"
                ." where status = 1 and id_employee_fk = ".$employeeId." and date_format(action_date, '%Y-%m') = '".date('Y-m', strtotime($date))."'";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataStats as $key => $stat) {
            if($key == 0) { $hours_per_day_admin = round($stat['per_day']/60, 2); $hours_per_month_admin = round($stat['per_month']/60, 2);}
            if($key == 1) { $hours_per_day_action = round($stat['per_day']/60, 2); $hours_per_month_action = round($stat['per_month']/60, 2);}
            $stats['hours_per_day'] += round($stat['per_day']/60, 2);
            $stats['hours_per_month'] += round($stat['per_month']/60, 2);
        }
		$stats['hours_per_day'] .= '<small><i class="fa fa-briefcase" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.$hours_per_day_action
                                    .' / <i class="fa fa-building" data-toggle="tooltip" data-title="Czynności administracyjne"></i>'.$hours_per_day_admin.'</small>';
        $stats['hours_per_month'] .= '<small><i class="fa fa-briefcase" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.$hours_per_month_action
                                    .' / <i class="fa fa-building" data-toggle="tooltip" data-title="Czynności administracyjne"></i>'.$hours_per_month_admin.'</small>';
                                    
		$sqlTask = "select 2 as type_event, id, execution_time, description as description, 'Administracyjne' as symbol, '' as cname, '' as dcost, 0 as id_service_fk, 1 as is_confirm  from law_cal_todo"
                ." where status = 1 and type_fk = 1 and id_employee_fk = ".$employeeId." and todo_date = '".$date."'"
                ." union"
                ." select 1 as type_event, a.id, unit_time, a.description, c.symbol as symbol, c.name as cname, acc_cost_description, id_service_fk, is_confirm from law_acc_actions a join {{%customer}} c on c.id=a.id_customer_fk"
                ." where a.status = 1 and a.id_employee_fk = ".$employeeId." and action_date = '".$date."'";
        $tasks = Yii::$app->db->createCommand($sqlTask)->queryAll();
		
		foreach($tasks as $key => $task) {
            $cname = ($task['symbol']) ? $task['symbol'] : $task['cname'];
            if($cname) $cname = '<b>'.$cname.'</b> - ';
            if($task['id_service_fk'] == 4) $task['description'] = '<i class="text--yellow">'.$task['dcost'].'</i>';
			$actions = '';
            if(!$task['is_confirm']) {
                $actions = '<a href="'.Url::to(['/task/personal/efast', 'id' => $task['id'], 'type' => $task['type_event']]).'" class="icon-edit gridViewModal" data-target="#modal-grid-event" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>'
                        .'&nbsp;<a href="'.Url::to(['/task/personal/edelete', 'id' => $task['id'], 'type' => $task['type_event']]).'" class="icon-delete deleteConfirm"  title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' ;
            }
            $stats['tasks'] .= '<li>'.$cname.$task['description'].' ['.round($task['execution_time']/60, 2).' h] '
                                     .$actions                              
                                .'</li>';
		}
		
		if(count($tasks) == 0) {
			$stats['tasks'] = '<small class"alert alert-warning">brak czynności na wybrany dzień</small>';
		}
		return $stats;
    }
    
    public function actionEfast($id, $type) {
        $id = CustomHelpers::decode($id);
        if($type == 1 || $type == 3) {
            $model = AccActions::findOne($id);
            $customerId = $model->id_customer_fk;
            $date = $model->action_date;
            if($model->unit_time) {
                $model->timeH = intval($model->unit_time/60);
                $model->timeM = $model->unit_time - ($model->timeH*60);
            }
        } else {
            $model = CalTodo::findOne($id);
            $date = $model->todo_date;
            if($model->execution_time) {
                $model->timeH = intval($model->execution_time/60);
                $model->timeM = $model->execution_time - ($model->timeH*60);
            }
        }
    
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->type_fk = ($type == 1 || $type == 3) ? $model->id_service_fk : 1;
            if($type == 2) $model->name = \backend\Modules\Accounting\models\AccDictionary::findOne($model->id_dict_todo_type_fk)['name'];
            
            if(empty($model->timeM)) $model->timeM = 0;
            if(empty($model->timeH)) $model->timeH = 0;
            
            if($type == 1 || $type == 3) {
                if($customerId != $model->id_customer_fk) {
                    $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $model->id_customer_fk, 'status' => '1'])->all();
                    if( count($orders) == 1 ) {
                        $model->id_order_fk = $orders[0]->id;
                    } else {
                        $model->id_order_fk = 0;
                    }
                }
            }
            
            //$model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';            
            //if(!$model->date_to) $model->date_from = $model->todo_date.' '. ( ($model->todo_time) ? $model->todo_time : '00:00' ) .':00';
                      
			if($model->validate() && $model->save()) {
				if($type == 3) {
                    $stats = \app\Modules\Accounting\controllers\ActionController::specialStats($model->id_department_fk, $date);
                } else {
                    $stats = $this->personalStats($model->id_employee_fk, $date);
                }
                return array('success' => true, 'id' => $model->id, 'action' => 'fastAction', 'stats' => $stats, 'todoWidget' => TodoWidget::widget(['refresh' => true]) );	
			} else {
                //return  $this->renderAjax('_formEfast_'.$type, [ 'model' => $model, 'type' => $type]);		
                return array('success' => false, 'id' => $model->id, 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formEfast_'.$type, [ 'model' => $model, 'type' => $type]);	
		}
    }
    
    public function actionEdelete($id, $type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        if($type == 1 || $type == 3) {
            $model = AccActions::findOne($id); 
            $date = $model->action_date;
        } else {
            $model = CalTodo::findOne($id);
            $date = $model->todo_date;
        }
        $model->user_action = 'delete';
        $model->status = -1;
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save(); //) { var_dump($model->getErrors()); exit; }
        if($type == 3) {
            $stats = \app\Modules\Accounting\controllers\ActionController::specialStats($model->id_department_fk, $date);
        } else {
            $stats = $this->personalStats($model->id_employee_fk, $date);
        }
        
        return array('success' => true, 'id' => $model->id, 'action' => 'fastAction', 'stats' => $stats, 'todoWidget' => TodoWidget::widget(['refresh' => true]) );	
    }
    
    public function actionProjects($id) {
	    //$id = CustomHelpers::decode($id);
        $projectsList = '<option value=""> - wybierz projekt - </option>';
	    $projects = CalCase::listProject($id);
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($projects) {
            foreach($projects as $key => $project) {
                $projectsList .= '<option value="'.$project->id.'"> '.$project->name.' </option>';
            }
        } else {
            $projectsList = '<option value=""> - brak danych - </option>';
        }
        
        $departments = ''; //'<option value="">- wszystko -</option>';
        $query = \backend\Modules\Company\models\EmployeeDepartment::find()->where(['id_employee_fk' => $id, 'status' => 1]);
        if(!$this->module->params['isAdmin'] && !$this->module->params['isSpecial'])
            $query->andWhere('id_department_fk in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$this->module->params['employeeId'].')');
        $models = $query->all(); 
        foreach($models as $key => $item) {
            $departments .= '<option value="'.$item->id_department_fk.'">'.$item->department['name'].'</option>';
        }

		       
        return ['list' => $projectsList, 'departments' => $departments];
    }
    
    public function actionClose($id) {
        $model = $this->findModel($id);
        if($model->id_action_fk) {
            $action = AccActions::findOne($model->id_action_fk);
            if($action->unit_time) {
                $action->timeH = intval($action->unit_time/60);
                $action->timeM = $action->unit_time - ($action->timeH*60);
            }
        } else {
            $action = new AccActions();
            $action->id_employee_fk = $model->id_employee_fk;
            $action->id_set_fk = $model->id_set_fk;
            $action->id_customer_fk = $model->case['id_customer_fk'];
            $action->description = ($model->description) ? $model->description : $model->name;
            $action->action_date = $model->todo_date;
            if($model->execution_time) {
                $action->timeH = intval($model->execution_time/60);
                $action->timeM = $model->execution_time - ($action->timeH*60);
            }
        }
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $action->load(Yii::$app->request->post());
            $model->is_close = 1;
            $model->user_action = 'close';
           
            /*$model->close_date = date('Y-m-d H:i:s');
            $model->close_user = \Yii::$app->user->id;*/
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if($model->createAction) {
                    if($action->validate() && $action->save()) {                      
                        $model->id_action_fk = $action->id;
                        if($model->save()) {
                            $transaction->commit();
                            return array('success' => true, 'alert' => 'Zadanie zostało zamknięte a czynność utworzona', 'id' => $model->id );
                        } else {
                            $transaction->rollBack();
                            return array('success' => false, 'html' => $this->renderAjax('_formClose', ['todo' => $model, 'model' => $action]), 'errors' => $model->getErrors() );	
                        }
                    } else {
                        $transaction->rollBack();
                        return array('success' => false, 'html' => $this->renderAjax('_formClose', ['todo' => $model, 'model' => $action]), 'errors' => $action->getErrors() );	
                    }  
                } else {
                    if($model->save()) {
                        $transaction->commit();
                        return array('success' => false, 'html' => $this->renderAjax('_formClose', ['todo' => $model, 'model' => $action]), 'errors' => $action->getErrors() );	
                    } else {
                        $transaction->rollBack();
                        return array('success' => false, 'html' => $this->renderAjax('_formClose', ['todo' => $model, 'model' => $action]), 'errors' => $model->getErrors() );	
                    }
                }
            } catch (Exception $e) {
                $transaction->rollBack();
           
                return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'create'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'], 'errors' => array_merge($model->getErrors(), $details->getErrors())]) ;	
            }	  	
		} else {
            return  $this->renderAjax('_formClose', ['todo' => $model, 'model' => $action]) ;	
		}
	}
    
    public function actionAproject($id) {
        $model = $this->findModel($id);
        $model->user_action = "create_action";
        $action = new AccActions();
        $action->id_employee_fk = $model->id_employee_fk;
        $action->id_customer_fk = $model->id_customer_fk;
        $action->description = ($model->description) ? $model->description : $model->name;
        $action->action_date = $model->todo_date;
        if($model->execution_time) {
            $action->timeH = intval($model->execution_time/60);
            $action->timeM = $model->execution_time - ($action->timeH*60);
        }
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            $action->load(Yii::$app->request->post());

            if($action->validate() && $action->save()) {                      
                $model->id_action_fk = $action->id;
                $model->save();
                return array('success' => true, 'alert' => 'Czynność została utworzona', 'id' => $model->id, 'table' => "#table-events" );
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAproject', ['todo' => $model, 'model' => $action]), 'errors' => $action->getErrors() );	
            }  
                	
		} else {
            return  $this->renderAjax('_formAproject', ['todo' => $model, 'model' => $action]) ;	
		}
	}
	
	public function actionReplace($id) {  
		$model = $this->findModel($id); 
              
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
		
		$new = new \backend\Modules\Accounting\models\AccActions();
		$new->id_employee_fk = $model->id_employee_fk;
		$new->timeH = $model->timeH;
		$new->timeM = $model->timeM;
		$new->action_date = $model->todo_date;
		//$new->name = 'Administracyjne';
		$new->description = $model->description;
      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$new->load(Yii::$app->request->post());                     
            $dict = \backend\Modules\Accounting\models\AccService::findOne($new->id_service_fk);
			$new->type_fk = $new->id_service_fk;
			$new->name = $dict->name;
			if($new->validate() && $new->save()) {	
				$sql = "update {{%cal_todo}} set status = -2, id_action_fk = ".$new->id
						." where id = ".$model->id;
				Yii::$app->db->createCommand($sql)->execute(); 
				return array('success' => true, 'id' => $new->id, 'name' => $new->name, 'alert' => 'Czynność została przypisana na rzecz klienta', 'table' => '#table-actions'  );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_replace', [ 'model' => $new]), 'errors' => $new->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_replace', ['model' => $new]);	
        }
	}
}

