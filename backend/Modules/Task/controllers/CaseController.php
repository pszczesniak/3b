<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\CalTaskSearch;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Company\models\CompanyEmployee;
use backend\Modules\Correspondence\models\CorrespondenceTask;
use backend\Modules\Company\models\CompanyResourcesSchedule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * CaseController implements the CRUD actions for CalTask model.
 */
class CaseController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalTaskSearch();
        $searchModel->id_dict_task_status_fk = 1;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.cases'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(((Yii::$app->params['env'] == 'dev') ? 'indexnew' : 'index'), [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = $this->module->params['grants']; $where = [];
        $post = $_GET;
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch'];
            \Yii::$app->session->set('search.cases', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(t.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            /*if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) { }*/
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                array_push($where, "id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "cc.id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                array_push($where, "t.id_case_fk = ".$params['id_case_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "event_date <= '".$params['date_to']."'");
            }
        } else {
             array_push($where, "id_dict_task_status_fk = 1");
        }
        
		//if(count(array_intersect(["grantAll"], $grants)) == 0) {  }  
             
		$fields = []; $tmp = [];
        $status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
        
        $sortColumn = 't.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 't.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'cc.name';
        if( isset($post['sort']) && ($post['sort'] == 'event_term' || $post['sort'] == 'delay')) $sortColumn = 't.event_date '.(isset($post['order']) ? $post['order'] : 'asc').',t.event_time';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'id_dict_task_category_fk';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'id_dict_task_status_fk';
		if( isset($post['sort']) && $post['sort'] == 'leader' ) $sortColumn = 'ce.lastname collate `utf8_polish_ci`';
		
		$query = (new \yii\db\Query())
            ->select(['t.id as id', 't.name as name', 'id_dict_task_status_fk', 't.status as status', 'event_date', 'event_time', 'execution_time', 'id_dict_task_status_fk', 'id_dict_task_category_fk', 'c.name as cname', 'c.id as cid', 'os.name as osname', 'os.id as osid', 'cc.name as ccname', 'cc.id as ccid', 'cc.type_fk as ctype', "concat_ws(' ', ce.lastname, ce.firstname) as ename"])
            ->from('{{%cal_task}} as t')
            ->join(' JOIN', '{{%cal_case}} as cc', 'cc.id = t.id_case_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = cc.id_customer_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = cc.id_opposite_side_fk')
			->join(' LEFT JOIN', '{{%company_employee}} as ce', 'cc.id_employee_leading_fk = ce.id')
            ->where( ['t.status' => 1, 't.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN ', '{{%task_employee}} as te', 't.id = te.id_task_fk and te.status=1 and te.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
            $query = $query->join(' JOIN ', '{{%task_employee}} as ete', 't.id = ete.id_task_fk and ete.status=1 and ete.id_employee_fk = '.$params['id_employee_fk']);
        }
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      
        $rows = $query->all();
		foreach($rows as $key=>$value) {
			$tmp['name'] = '<a href="'.Url::to(['/task/case/view','id' => $value['id']]).'"  data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['event_term'] = $value['event_date'] . ' ' . $value['event_time'];
            $tmp['event_time'] = $value['event_time'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['cid']]).'"  title="Karta klienta" >'.$value['cname'].'</a>';
            if( Yii::$app->params['env'] == 'dev' ) {
				$tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['ctype'] == 1) ? 'matter' : 'project').'/view','id' => $value['ccid']]).'"  title="Karta sprawy/projektu" >'.$value['cname'].(($value['osid'] ? (' vs '. $value['osname']) : '')).'</a>';
			}else {
				$tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['ctype'] == 1) ? 'matter' : 'project').'/view','id' => $value['ccid']]).'"  title="Karta sprawy/projektu" >'.$value['ccname'].'</a>';
            }
			$tmp['status'] = isset($status[$value['id_dict_task_status_fk']]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value['id_dict_task_status_fk']].'<span>' : '';
            $tmp['type'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';
				
			$tmp['leader'] = $value['ename'];
			
            $tmp['delay'] = ($value['id_dict_task_status_fk']==1) ? '<span class="small-text">'.Yii::$app->runAction('common/ago', ['date' => $value['event_date'].' '.(($value['event_time'])?$value['event_time']:'08:00').':00']).'</span>':'';//$value->delay;
            $tmp['className'] = ($value['id_dict_task_status_fk'] == 3) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2 && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );

			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/case/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ?  '<a href="'.Url::to(['/task/event/cancelajax', 'id' => $value['id']]).'" class="btn btn-xs btn-danger deleteConfirm" data-label="Odwołanie rozprawy" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-ban\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Odwołana').'"><i class="fa fa-ban"></i></a>': '';
                $tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/case/update', 'id' => $value['id']]).'" class="btn btn-xs btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';				
				$tmp['actions'] .= ( count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/case/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionDatanew() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $grants = $this->module->params['grants']; $where = [];
        $post = $_GET;
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch'];
            \Yii::$app->session->set('search.cases', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(t.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            /*if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) { }*/
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                array_push($where, "id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "cc.id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                array_push($where, "t.id_case_fk = ".$params['id_case_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "event_date <= '".$params['date_to']."'");
            }
        } else {
             array_push($where, "id_dict_task_status_fk = 1");
        }
        
		//if(count(array_intersect(["grantAll"], $grants)) == 0) {  }  
             
		$fields = []; $tmp = [];
        $status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
        
        $sortColumn = 't.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 't.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'cc.name';
        if( isset($post['sort']) && ($post['sort'] == 'event_term' || $post['sort'] == 'delay')) $sortColumn = 't.event_date '.(isset($post['order']) ? $post['order'] : 'asc').',t.event_time';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'id_dict_task_category_fk';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'id_dict_task_status_fk';
		if( isset($post['sort']) && $post['sort'] == 'leader' ) $sortColumn = 'ce.lastname collate `utf8_polish_ci`';
		
		$query = (new \yii\db\Query())
            ->select(['t.id as id', 't.name as name', 'id_dict_task_status_fk', 't.status as status', 'event_date', 'event_time', 'execution_time', 'id_dict_task_status_fk', 'id_dict_task_category_fk', 
                     'c.name as cname', 'c.id as cid', 'os.name as osname', 'os.id as osid', 'cc.name as ccname', 'cc.id as ccid', 'cc.type_fk as ctype', "concat_ws(' ', ce.lastname, ce.firstname) as ename", "t.show_client"])
            ->from('{{%cal_task}} as t')
            ->join(' JOIN', '{{%cal_case}} as cc', 'cc.id = t.id_case_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = cc.id_customer_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = cc.id_opposite_side_fk')
			->join(' LEFT JOIN', '{{%company_employee}} as ce', 'cc.id_employee_leading_fk = ce.id')
            ->where( ['t.status' => 1, 't.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN ', '{{%task_employee}} as te', 't.id = te.id_task_fk and te.status=1 and te.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
            $query = $query->join(' JOIN ', '{{%task_employee}} as ete', 't.id = ete.id_task_fk and ete.status=1 and ete.id_employee_fk = '.$params['id_employee_fk']);
        }
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      
        $rows = $query->all();
		foreach($rows as $key=>$value) {
			$tmp['name'] = '<a href="'.Url::to(['/task/case/view','id' => $value['id']]).'"  data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['event_term'] = $value['event_date'] . '<br />' . $value['event_time'];
            $tmp['event_time'] = $value['event_time'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['cid']]).'"  title="Karta klienta" >'.$value['cname'].'</a>';
            if( Yii::$app->params['env'] == 'dev' ) {
				$tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['ctype'] == 1) ? 'matter' : 'project').'/view','id' => $value['ccid']]).'" class="btn btn-icon btn-xs bg-purple" title="Karta sprawy/projektu" ><i class="fa fa-eye"></i> Karta '.(($value['ctype'] == 1) ? 'sprawy' : 'projektu').'</a>';
			}else {
				$tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['ctype'] == 1) ? 'matter' : 'project').'/view','id' => $value['ccid']]).'"  title="Karta sprawy/projektu" >'.$value['ccname'].'</a>';
            }
			$tmp['status'] = isset($status[$value['id_dict_task_status_fk']]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value['id_dict_task_status_fk']].'<span>' : '';
            $tmp['type'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';
				
			$tmp['leader'] = $value['ename'];
			
            $tmp['delay'] = ($value['id_dict_task_status_fk']==1) ? '<span class="small-text">'.Yii::$app->runAction('common/ago', ['date' => $value['event_date'].' '.(($value['event_time'])?$value['event_time']:'08:00').':00']).'</span>':'';//$value->delay;
            $tmp['className'] = ($value['id_dict_task_status_fk'] == 3) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2 && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['show'] = ($value['show_client']) ? '<i class="fa fa-eye text--blue" data-toggle="tooltip" data-title="Widoczne dla klienta"></i>' : '<i class="fa fa-eye-slash text--yellow" data-toggle="tooltip" data-title="Ukryte przed klientem"></i>';
            
            $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . '<li><a href="'.Url::to(['/task/case/view', 'id' => $value['id']]).'" title="Karta rozprawy"><i class="fa fa-vcard text--teal"></i>&nbsp;Szczegóły</a></li>'
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/case/update', 'id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ? '<li class="deleteConfirm" data-table="#table-cases" href="'.Url::to(['/task/event/cancelajax', 'id' => $value['id']]).'" data-label="Odwołana" data-title="<i class=\'fa fa-ban\'></i>'.Yii::t('app', 'Odwołaj').'"><a href="#"><i class="fa fa-ban text--orange"></i>&nbsp;Odwołaj</a></li>' : '')                                        
                                        . ( (count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-cases" href="'.Url::to(['/task/case/delete', 'id' => $value['id']]).'" data-label="Usuń" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" href="'.Url::to(['/task/event/sclient', 'id' => $value['id']]).'" data-table="#table-cases" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><a href="#" ><i class="fa fa-eye'.(($value['show_client'])?'-slash':'').' text--'.(($value['show_client'])?'yellow':'blue').'"></i>&nbsp;'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'</a></li>' : '')
         
                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
                                      
			/*$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/case/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 && $value['id_dict_task_status_fk'] == 1 ) ?  '<a href="'.Url::to(['/task/event/cancelajax', 'id' => $value['id']]).'" class="btn btn-xs btn-danger deleteConfirm" data-label="Odwołanie rozprawy" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-ban\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Odwołana').'"><i class="fa fa-ban"></i></a>': '';
                $tmp['actions'] .= ( count(array_intersect(["eventEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/case/update', 'id' => $value['id']]).'" class="btn btn-xs btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';				
				$tmp['actions'] .= ( count(array_intersect(["eventDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/case/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
	
	/**
     * Calendar all CalTask models.
     * @return mixed
     */
    public function actionCalendar()  {

        return $this->render('calendar', [  ]);
    }
	
	public function actionScheduledata() {
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$res = [];
                
        $events = CalTask::find()
					->where(['status' => '1'])
					->andWhere('date_from>=\''.$_GET['start'].'\'')
					->andWhere('date_to<=\''.$_GET['end'].'\'')
					->all(); 
		foreach($events as $key=>$value) {
			$tStart = strtotime($value['date_from']);
			$tEnd = strtotime('+30 minutes', $tStart);
			array_push($res, array('title' => $value['name'], 
			                       'start' => $value['date_from'].'T'.date("H:i",$tStart).':00', 
								   'end' => $value['date_to'].'T'.date("H:i",$tEnd).':00',
								   'constraint' => 'freeTerm', 
								   'color' => '#f5f5f5', 
								   'textColor' => '#979494',
								   //'textColor' => $value['doctor']['calendar_color'],
								   'className' => 'visit-free',
								   'id' => $value['id']
								)
		    );
		}
		return $res;
	}

    /**
     * Displays a single CalTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if(!$this->module->params['isAdmin']){
            //$checked = \backend\Modules\Task\models\TaskEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_task_fk' => $model->id])->count();
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id_case_fk])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej rozprawy.');
                }
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }
        
        return $this->render('view', [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Creates a new CalTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
		
		if( count(array_intersect(["eventAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		if( !isset($_POST['CalTask']['id']) || empty($_POST['CalTask']['id']) ) {
            $model = new CalTask();
            $model->type_fk = 1;
            $model->name = 'Wersja robocza '.time();
            $model->status = -2;
            $model->save();
            $model->name = '';
        } else {
            $model = CalTask::findOne($_POST['CalTask']['id']);
        }
        
        $model->scenario = CalTask::SCENARIO_EVENT;
        $model->all_day = 1;
        $model->id_dict_task_status_fk = 1;
        //$model->id_dict_task_type_fk = 1;
        $model->id_dict_task_category_fk = 2;
        $model->id_company_branch_fk = $this->module->params['employeeBranch'];
        //$model->date_from = date('Y-m-d').' 08:00';
        //$model->date_to = date('Y-m-d').' 16:00';
        $model->event_date = date('Y-m-d');
        //$model->event_deadline = date('Y-m-d').' 16:00';
         
		$departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']); $lists = ['employees' => []];
		$status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        $grants = $this->module->params['grants'];
        
		if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->user_action = 'create';
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            
            $model->date_from = $model->event_date.' '.$model->event_time.':00';
            $model->date_to = ($model->event_deadline) ? $model->event_deadline : null;
			
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            } 
            
             /*reminder start*/
            $model->reminder_user = \Yii::$app->user->id;
            if($model->event_deadline) {
                $deadline = $model->event_deadline;
            } else {
                $deadline = $model->date_from;
            }
         
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $model->reminder_user = \Yii::$app->user->id;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
            }

            /*reminder end*/
            if($model->id_case_fk)
                $lists = $this->lists($model->id_case_fk);
            if($model->validate() && $model->save()) {				
				//CaseEmployee::deleteAll('id_case_fk = :case', [':case' => $id]);
				if(isset($_POST['employees']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('employees') as $key=>$value) {
                        $model_ce = new TaskEmployee();
                        $model_ce->id_task_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
				} 
             
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                $model->status = -2;
				return  $this->render('create', [  'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind']]) ;	

			}		
		} else {
			$model->event_time = '08:00';
            return  $this->render('create', [  'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
	}

    /**
     * Updates an existing CalTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        if( count(array_intersect(["eventEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
		
		$model = $this->findModel($id);
        $caseTemp = $model->id_case_fk;         
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\TaskEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_task_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej rozprawy.');
                }
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }

        $model->scenario = CalTask::SCENARIO_EVENT;
        
        $departments = []; $employees = [];
		$status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        $grants = $this->module->params['grants'];
        
        foreach($model->employees as $key => $item) {
            array_push($employees, $item['id_employee_fk']);
        }
        
        if($model->execution_time) {
            $model->timeH = intval($model->execution_time/60);
            $model->timeM = $model->execution_time - ($model->timeH*60);
        }

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($caseTemp == $model->id_case_fk) $model->is_ignore = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            
            $model->date_from = $model->event_date.' '.$model->event_time.':00';
            $model->date_to = ($model->event_deadline) ? $model->event_deadline : $model->event_date.' '.$model->event_time.':00';
			
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            } 
            
             /*reminder start*/
            /*if($model->event_deadline) {
                $deadline = $model->event_deadline;
            } else {
                $deadline = $model->event_date;
            }*/
			$model->event_deadline = $model->event_date.' '.$model->event_time.':00';
			$deadline = $model->event_date.' 08:00:00';
                       
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
                $model->reminder_user = \Yii::$app->user->id;
            } else {
                $model->reminded = 1;
                $model->reminder_user = \Yii::$app->user->id;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
            }
            /*reminder end*/
            $lists = $this->lists($model->id_case_fk);
			if($model->validate() && $model->save()) {

                return array('success' => true,  'action' => false, 'alert' => 'Rozprawa <b>'.$model->name.'</b> została zaktualizowana', 'form' => 'main-form', 'reminder_date' => ($model->reminded == 1) ? $model->reminder_date : 'nie ustawiono'  );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formnew' : '_form'), [  'model' => $model, 'lists' => $lists, 'employees' => $employees, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind'] ]), 'errors' => $model->getErrors(), 'form' => 'main-form' );	
			}		
		} else {
			$lists = $this->lists($model->id_case_fk);
            return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'updatenew' : 'update'), [ 'model' => $model, 'employees' => $employees, 'lists' => $lists, 'all' => $this->module->params['employees'], 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
    }
    
    public function actionReminder($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->scenario = CalTask::SCENARIO_REMINDER;
        $model->user_action = 'reminder';
        $model->reminder_user = \Yii::$app->user->id;
        if($model->type_fk == 1) {
            $deadline = $model->date_from;
        } else {
            $deadline = $model->date_to;
        }
     
        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            if($model->id_reminder_type_fk == 0) {
                $model->reminded = 0;
            } else {
                $model->reminded = 1;
                $dateTempTimestamp = \Yii::$app->runAction('/common/reminder', ['type' => $model->id_reminder_type_fk, 'dateTimestamp' => $deadline]);
                $model->reminder_user = Yii::$app->user->id;
                $model->reminder_date = date('Y-m-d H:i:s', $dateTempTimestamp);
            }
			if($model->validate() && $model->save() ) {
                $model->reminder_date = date('Y-m-d', strtotime($model->reminder_date)).' 08:00';
                return array('success' => true,  'action' => false, 'alert' => ($model->reminded == 1) ? ('Przpomnienie zostało ustawione na '.$model->reminder_date) : 'Przypomnienie zostało usunięte', 'reminder_date' => ($model->reminded == 1) ? $model->reminder_date : 'nie ustawiono' );	
			} else {
                return array('success' => false,  'action' => false, 'alert' => 'Data przypomnienia nie może być wcześniejsza niż data dzisiejsza' );	
			}		
		} 
    }
    
    public function actionDates($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->scenario = CalTask::SCENARIO_CALENDAR;
        $model->user_action = 'calendar';
        
        if (Yii::$app->request->isPost ) {
			
			$model->load(Yii::$app->request->post());
            
            if($model->all_day == 1) {
                $model->date_from = (!$model->fromDate) ? null : ( $model->fromDate . ' 00:00:00' );
                $model->date_to = (!$model->toDate) ? null : ( $model->toDate . ' 00:00:00' );
            } else {
                $model->date_from = (!$model->fromDate || !$model->fromTime) ? null : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                $model->date_to = (!$model->toDate || !$model->toTime) ? null : ( $model->toDate . ' ' . $model->toTime . ':00' );
            }
            
			if($model->validate() && $model->save() ) {
                // $model->reminder_date = date('Y-m-d', strtotime($model->reminder_date)).' 08:00';
                return array('success' => true,  'action' => false, 'alert' => 'Daty kalendarzowe zostały ustawione' );	
			} else {
                return array('success' => false,  'action' => false, 'errors' => $model->getErrors(), 'alert' => 'Proszę uzupełnić dane' );	
			}		
		} 
    }

    /**
     * Deletes an existing CalTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $correspondence = CorrespondenceTask::find()->where(['id_task_fk' => $model->id])->andWhere('id_correspondence_fk in (select id from {{%correspondence}} where status = 1)')->count();
        
        if($correspondence != 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Rozprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisaną korespondencję!')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Rozprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisaną korespondencję!<br />', 'id' => $id);	
			}
        }
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rozprawa <b>'.$model->name.'</b> została usunięta')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Rozprawa <b>'.$model->name.'</b> została usunięta.<br/>', 'id' => $id, 'table' => '#table-cases');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Rozprawa <b>'.$model->name.'</b> nie została usunięta')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Rozprawa <b>'.$model->name.'</b> nie została usunięta.<br />', 'id' => $id, 'table' => '#table-cases');	
			}
        }
        
    }
    
    public function actionClose($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_CLOSE;
        $model->id_dict_task_status_fk = 2;
        $model->user_action = 'close';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rozprawa została zakmnięta')  );
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
    public function actionOpen($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalTask::SCENARIO_OPEN;
        $model->id_dict_task_status_fk = 1;
        $model->user_action = 'open';
		//$model->deleted_at = date('Y-m-d H:i:s');
        //$model->deleted_by = \Yii::$app->user->id;
        $model->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rozprawa została ponownie otwarta')  );
        return $this->redirect(['update', 'id' => $model->id]);
    }

    /**
     * Finds the CalTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = $id;
            $note->id_type_fk = 2;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_task_fk = $model->id;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                $modelArch = new \backend\Modules\Task\models\CalTaskArch();
                $modelArch->id_task_fk = $model->id;
                $modelArch->id_case_fk = $model->id_case_fk;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                $modelArch->save();
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }

    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozprawy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Data')
            ->setCellValue('B3', 'Godzina rozpoczęcia')
            ->setCellValue('C3', 'Temat zadania')
			->setCellValue('D3', 'Klient')
			->setCellValue('E3', 'Sprawa')
			->setCellValue('F3', 'Miejsce')
			->setCellValue('G3', 'Opis')
			->setCellValue('H3', 'Prowadzący');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = [];
		
		$fieldsDataQuery = CalTask::find()->where(['status' => 1, 'type_fk' => 1]);
        
        if(isset($_GET['CalTaskSearch'])) {
            $params = $_GET['CalTaskSearch']; 
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_task_fk from law_task_employee where id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_dict_task_status_fk']) && !empty($params['id_dict_task_status_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = ".$params['id_dict_task_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_case_fk']) && !empty($params['id_case_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_case_fk = ".$params['id_case_fk']);
            }
           /* if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
            }*/
           /* if( isset($params['filter_status']) && $params['filter_status'] == 1) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
            } else {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 2");
            }*/
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("event_date <= '".$params['date_to']."'");
            }
        } else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_task_status_fk = 1");
        }
        
		if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			//if($this->module->params['employeeKind'] >= 70)
			///	$fieldsDataQuery = $fieldsDataQuery->andWhere('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
			//else 
				$fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }  
        
        $data = $fieldsDataQuery->orderby('event_date,event_time')->all();
			
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->event_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, ( ($record->event_time == '00:00' || !$record->event_time) ? '' : $record->event_time)); 
				//$objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->name); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->customer['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record->case['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->place); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, str_replace(array("\r\n", "\r", "\n"), "", $record->description)); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, ( ($record->id_dict_task_status_fk == 3) ? 'ODWOŁANA ' : '' ) . str_replace(array("\r\n", "\r", "\n"), "", $record->description)); 
                if($record->id_dict_task_status_fk == 3)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getFont()->getColor()->setARGB('FF0000');	
				$objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record->case['leader']); 		
				$i++; 
			}  
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(80);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Rozprawy');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Rozprawy'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}

    private function lists($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one(); 
        $model = \backend\Modules\Task\models\CalCase::findOne($id);
        $departments = []; $ids = []; array_push($ids, 0); $employees = []; $contacts = [];
        $checked = 'checked';
        if($model) {
            foreach($model->employees as $key => $value) {
                $employees[$value['id_employee_fk']] = $value['fullname'];
            }
        }     
        $list = '';
        
        return ['employees' => $employees];
    }

}

