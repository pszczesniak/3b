<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use backend\Modules\Task\models\CalCaseInstance;
use backend\Modules\Task\models\CaseInstanceLawsuit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * GenerateController implements the CRUD actions for CalCase model.
 */
class GenerateController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalCaseInstance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
      
    public function actionPrintpdf($id, $save) {
        error_reporting(0); 
        $id = CustomHelpers::decode($id);
        $model = CaseInstanceLawsuit::findOne($id);
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "pozew.pdf";
        
        // instantiate and use the dompdf class
        //$dompdf = new \Dompdf\Dompdf();
       // $dompdf->loadHtml($this->renderPartial('printpdf', array('model' => $model, 'employees' => ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false )), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');
        //$dompdf->set_paper("A4", "landscape");

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
       /* $dompdf->render();
        
        if($save) {
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	*/
        // Output the generated PDF to Browser
        
        // Read contents
        $source = \Yii::getAlias('@webroot'). "/templates/pozew.rtf";
        //$phpWord = \PhpOffice\PhpWord\IOFactory::load($source, 'RTF');
        $value[1] = 'Lorem ipsum';
        $file = file_get_contents($source);
        
       /* $sections = $phpWord->getSections();
        foreach ($sections as $key => $value) {
            $sectionElement = $value->getElements();
            foreach ($sectionElement as $elementKey => $elementValue) {
                if ($elementValue instanceof \PhpOffice\PhpWord\Element\TextRun) {
                    $secondSectionElement = $elementValue->getElements();
                    foreach ($secondSectionElement as $secondSectionElementKey => $secondSectionElementValue) {
                            if ($secondSectionElementValue instanceof \PhpOffice\PhpWord\Element\Text) {
                                echo $secondSectionElementValue->getText() . '<br/>';
                            }
                    }
                }
            }
        }exit;*/
        // Save file
       // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'RTF');

		//header("Content-Type: application/msword");
		//header('Content-Disposition: attachment;filename='.$filename .' ');
        header("Content-type: application/rtf; ");
        header("Content-Disposition: attachment;filename=pozew.rtf; charset=us-ascii");
		header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); 
       // $data_base[0]['body'] = 'I am a {$club} fan.'; // Tests

        $vars = array(
          '$amount' => '10 000.00',
          '$amount_in_words' => $this->utf8_2_win(\rigor\controllers\CommonController::amountToWords(1021.24)),
          '$instance' => $this->utf8_2_win('Sąd Rejonowy w Częstochowie'),
          '$customer'       =>  $this->utf8_2_win($model->case['customer']['name']), 
          '$side'        => ($model->case['side']) ? $this->utf8_2_win($model->case['side']['name']) : '',
          '$reason' => $this->utf8_2_win($model->lawsuit_reason)
       );

        echo strtr($file, $vars);
        exit;		
		//$objWriter->save('php://output');	
    }
    
    function utf8_2_win ($nowytekst) {
        //$tab2 = Array("\'b9", "\'a5", "\'9c", "\'8c", "\'9f", "\'8f");
        $nowytekst = str_replace('ą',"\'b9",$nowytekst);    //ą
        $nowytekst = str_replace('ć',"\'e6",$nowytekst);    //ć
        $nowytekst = str_replace('ę',"\'ea",$nowytekst);    //ę
        $nowytekst = str_replace('ł',"\'b3",$nowytekst);    //ł
        $nowytekst = str_replace('ń',"\'f1",$nowytekst);    //ń
        $nowytekst = str_replace('ó',"\'f3",$nowytekst);    //ó
        $nowytekst = str_replace('ś',"\'9c",$nowytekst);    //ś
        $nowytekst = str_replace('ź',"\'9f",$nowytekst);    //ź
        $nowytekst = str_replace('ż',"\'bf",$nowytekst);    //ż
        
        $nowytekst = str_replace('Ą',"\'a5",$nowytekst);    //Ą
        $nowytekst = str_replace('Ć',"\'c6",$nowytekst);    //Ć
        $nowytekst = str_replace('Ę',"\'ca",$nowytekst);    //Ę
        $nowytekst = str_replace('Ł',"\'a3",$nowytekst);    //Ł
        $nowytekst = str_replace('Ń',"\'d1",$nowytekst);    //Ń
        $nowytekst = str_replace('Ó',"\'d3",$nowytekst);    //Ó
        $nowytekst = str_replace('Ś',"\'8c",$nowytekst);    //Ś
        $nowytekst = str_replace('Ź',"\'8f",$nowytekst);    //Ź
        $nowytekst = str_replace('Ż',"\'af",$nowytekst);    //Ż
        
        return ($nowytekst);
   } 
    
    function utf16_2_utf8 ($nowytekst) {
        $nowytekst = str_replace('%u0104','Ą',$nowytekst);    //Ą
        $nowytekst = str_replace('%u0106','Ć',$nowytekst);    //Ć
        $nowytekst = str_replace('%u0118','Ę',$nowytekst);    //Ę
        $nowytekst = str_replace('%u0141','Ł',$nowytekst);    //Ł
        $nowytekst = str_replace('%u0143','Ń',$nowytekst);    //Ń
        $nowytekst = str_replace('%u00D3','Ó',$nowytekst);    //Ó
        $nowytekst = str_replace('%u015A','Ś',$nowytekst);    //Ś
        $nowytekst = str_replace('%u0179','Ź',$nowytekst);    //Ź
        $nowytekst = str_replace('%u017B','Ż',$nowytekst);    //Ż
      
        $nowytekst = str_replace('%u0105','ą',$nowytekst);    //ą
        $nowytekst = str_replace('%u0107','ć',$nowytekst);    //ć
        $nowytekst = str_replace('%u0119','ę',$nowytekst);    //ę
        $nowytekst = str_replace('%u0142','ł',$nowytekst);    //ł
        $nowytekst = str_replace('%u0144','ń',$nowytekst);    //ń
        $nowytekst = str_replace('%u00F3','ó',$nowytekst);    //ó
        $nowytekst = str_replace('%u015B','ś',$nowytekst);    //ś
        $nowytekst = str_replace('%u017A','ź',$nowytekst);    //ź
        $nowytekst = str_replace('%u017C','ż',$nowytekst);    //ż
        return ($nowytekst);
   } 

    function utf8_2_utf16 ($nowytekst) {
        $nowytekst = str_replace('Ą','%u0104',$nowytekst);    //Ą
        $nowytekst = str_replace('Ć','%u0106',$nowytekst);    //Ć
        $nowytekst = str_replace('Ę','%u0118',$nowytekst);    //Ę
        $nowytekst = str_replace('Ł','%u0141',$nowytekst);    //Ł
        $nowytekst = str_replace('Ń','%u0143',$nowytekst);    //Ń
        $nowytekst = str_replace('Ó','%u00D3',$nowytekst);    //Ó
        $nowytekst = str_replace('Ś','%u015A',$nowytekst);    //Ś
        $nowytekst = str_replace('Ź','%u0179',$nowytekst);    //Ź
        $nowytekst = str_replace('Ż','%u017B',$nowytekst);    //Ż
      
        $nowytekst = str_replace('ą','%u0105',$nowytekst);    //ą
        $nowytekst = str_replace('ć','%u0107',$nowytekst);    //ć
        $nowytekst = str_replace('ę','%u0119',$nowytekst);    //ę
        $nowytekst = str_replace('ł','%u0142',$nowytekst);    //ł
        $nowytekst = str_replace('ń','%u0144',$nowytekst);    //ń
        $nowytekst = str_replace('ó','%u00F3',$nowytekst);    //ó
        $nowytekst = str_replace('ś','%u015B',$nowytekst);    //ś
        $nowytekst = str_replace('ź','%u017A',$nowytekst);    //ź
        $nowytekst = str_replace('ż','%u017C',$nowytekst);    //ż
        return ($nowytekst);
   }    

}
