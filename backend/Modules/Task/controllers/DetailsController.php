<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseDetails;
use backend\Modules\Company\models\CompanyEmployee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * CaseController implements the CRUD actions for CalTask model.
 */
class DetailsController extends Controller
{
   
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single CalTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionConfig($id)
    {
        if( count(array_intersect(["billingPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej sprawy.');
                }
            }
        }
       
        $details = CalCaseDetails::find()->where(['id_case_fk' => $model->id, 'status' => 1])->one();
        
        if(!$details) {
            $details = new CalCaseDetails();
            $details->id_case_fk = $model->id;
            $details->save();
        }
        $stats = CalCaseDetails::getStats($model->id);
        
        return $this->render('view', [
            'model' => $model, 'details' => $details, 'stats' => $stats, 'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Finds the CalTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = CalCase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionBasic($id) {
        $case = $this->findModel($id);
        $model = CalCaseDetails::find()->where(['id_case_fk' => $case->id, 'status' => 1])->one();
        $model->user_action = 'basic';
          
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
    
            if($model->validate() && $model->save()) {     
                $changes = [];
				$stats = CalCaseDetails::getStats($case->id); 
                if($model->is_limited) $isLimited = '<i class="text--green">Kontrola limitów włączona</i>'; else $isLimited = '<i class="text--red">Kontrola limitów wyłączona</i>';
                $emptyText = '<span class="text--lightgrey">brak danych</span>';
				$changes = [
                    'start-'.$case->id => $model->date_from, 'deadline-'.$case->id => $model->date_to, 'time-'.$case->id => $model->time_limit, 
				    'budget-'.$case->id => ($model->budget_limit) ? (number_format($model->budget_limit, 2, ",", " ").' '.$model->currency) : $emptyText, 
					'limited-'.$case->id => $isLimited, 'left-'.$case->id => $stats['leftTime']." ",
                ];
                
                $time = round($stats['execution_time']/60,2).' / '.$model->time_limit;
                $tasks = $stats['tasks_open'].' / '.$stats['tasks_all'];
                return array('success' => true, 'action' => 'detailsCase', 'index' =>1, 'changes' => $changes, 'alert' => 'Zmiany zostały zapisane', 'id'=>$model->id, 'detailsStats' => ['time' => $time, 'tasks' => $tasks, 'left' => $stats['leftTime']]);	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formBasic', [ 'model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formBasic', [ 'model' => $model]) ;	
		}
	}
    
    public function actionEmployees($id) {
        $case = $this->findModel($id);
        $model = CalCaseDetails::find()->where(['id_case_fk' => $case->id, 'status' => 1])->one();
          
        $grants = $this->module->params['grants'];
		$colors = Yii::$app->params['colors'];
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $colorsChart = []; $labelChart = []; $dataChart = []; $i = 0;
            $limit = 0;
            foreach($_POST as $key => $value) {
                $limit += $value;
            }
            
            if($limit > $model->time_limit) {
                return array('success' => false, 'html' => $this->renderAjax('_formEmployees', [ 'model' => $case ]), 'errors' => ['limit' => 'Suma limitów poszczególnych pracowników nie może przekroczyć limitu projektu'] );	
            }
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach($_POST as $key => $value) { 
                    $modelCe = \backend\Modules\Task\models\CaseEmployee::findOne(substr($key,3));
                    if( $modelCe->time_used > ($value*60) ) {
                        $transaction->rollBack();
                        $alert = "Pracownik ".$modelCe->employee['fullname']." ze względu na przypisane zadania nie może mieć limitu mniejszego niż ".(round($modelCe->time_used/60, 2) )." h";
                        return array('success' => false, 'html' => $this->renderAjax('_formEmployees', [ 'model' => $case ]), 'errors' => ['alert' => $alert] );	
                    }
                    $modelCe->time_limit = $value;
                    $modelCe->save();
                    array_push($colorsChart, ( isset($colors[$i]) ? $colors[$i] : '#33bfeb'));
                    array_push($labelChart, $modelCe->employee['fullname']);
                    array_push($dataChart, $modelCe['time_limit']);
                    ++$i;
                }
                
                $transaction->commit();
				$stats = CalCaseDetails::getStats($case->id); 
                return array('success' => true, 'action' => 'detailsLimit', 'index' =>1, 'html' => $this->renderAjax('_employees', [ 'model' => $case, 'leftTime' => $stats['leftTime'] ]), 'alert' => 'Zmiany zostały zapisane', 'id'=>$model->id, 
                            'chartColor' => $colorsChart, 'chartLabel' =>  $labelChart, 'chartData' =>  $dataChart);	
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formEmployees', [ 'model' => $case ]), 'errors' => $modelCe->getErrors() );	
            }
    
            /*if($model->validate() && $model->save()) {     
                         
                return array('success' => true, 'action' => 'detailsLimit', 'index' =>1, 'html' => $this->renderAjax('_employees', [ 'model' => $case]), 'alert' => 'Zmiany zostały zapisane', 'id'=>$model->id, 
                            'chartColor' => $colorsChart, 'chartLabel' =>  $labelChart, 'chartData' =>  $dataChart);	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formEmployees', [ 'model' => $case]), 'errors' => $model->getErrors() );	
            } */    	
		} else {
            return  $this->renderAjax('_formEmployees', [ 'model' => $case]) ;	
		}
	}
    
    public function actionEadd($id) {
        $case = $this->findModel($id);
        $model = CalCaseDetails::find()->where(['id_case_fk' => $case->id, 'status' => 1])->one();
          
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$case->load(Yii::$app->request->post());
            
            if(!$case->employees_list) {
                return array('success' => false, 'html' => $this->renderAjax('_employeeAdd', [ 'model' => $case ]), 'errors' => ['employees_list' => 'Proszę wybrać pracowników'] );
            } else {
                foreach($case->employees_list as $key => $employee) {
                    $exist = \backend\Modules\Task\models\CaseEmployee::find()->where(['status' => 1, 'id_case_fk' => $case->id, 'id_employee_fk' => $employee])->count();
                    $model_ce = new \backend\Modules\Task\models\CaseEmployee();
                    $model_ce->id_case_fk = $case->id;
                    $model_ce->id_employee_fk = $employee;
                    $model_ce->status = 1;
                    if($exist == 0)
                        $model_ce->save();
                }
                $stats = CalCaseDetails::getStats($case->id); 
                return array('success' => true, 'action' => 'detailsLimit', 'index' =>1, 'html' => $this->renderAjax('_employees', [ 'model' => $case, 'leftTime' => $stats['leftTime'] ]), 'alert' => 'Pracownicy zostali dodani', 'id'=>$case->id);
            }

		} else {
            return  $this->renderAjax('_employeeAdd', ['model' => $case]) ;	
		}
	}
    
    public function actionEremove($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        $model_ce = \backend\Modules\Task\models\CaseEmployee::findOne($id);
        $model_ce->status = 0;
        $model_ce->save();
        
        $case = \backend\Modules\Task\models\CalCase::findOne($model_ce->id_case_fk);
        
        $stats = CalCaseDetails::getStats($model_ce->id_case_fk); 
        return array('success' => true, 'action' => 'detailsLimit', 'index' =>1, 'html' => $this->renderAjax('_employees', [ 'model' => $case, 'leftTime' => $stats['leftTime'] ]), 'alert' => 'Pracownicy został usunięty', 'id'=>$model_ce->id_case_fk);

	}
	
	public function actionCadd($id) {
        $project = $this->findModel($id);
        //$model = CalCaseDetails::find()->where(['id_case_fk' => $case->id, 'status' => 1])->one();
          
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$project->load(Yii::$app->request->post());
            
            if(!$project->cases_list) {
                return array('success' => false, 'html' => $this->renderAjax('_caseAdd', [ 'model' => $project ]), 'errors' => ['cases_list' => 'Proszę wybrać sprawy'] );
            } else {
                foreach($project->cases_list as $key => $case) {
                    $exist = \backend\Modules\Task\models\ProjectCase::find()->where(['status' => 1, 'id_project_fk' => $project->id, 'id_case_fk' => $case])->count();
                    $model_pc = new \backend\Modules\Task\models\ProjectCase();
                    $model_pc->id_case_fk = $case;
                    $model_pc->id_project_fk = $project->id;
                    $model_pc->status = 1;
                    if($exist == 0)
                        $model_pc->save();
                }
                return array('success' => true, 'action' => 'detailsCases', 'html' => $this->renderAjax('_cases', [ 'model' => $project ]), 'alert' => 'Sprawy zostały dołączone', 'id'=>$project->id);
            }

		} else {
            return  $this->renderAjax('_caseAdd', ['model' => $project]) ;	
		}
	}
    
    public function actionCremove($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        $model_pc = \backend\Modules\Task\models\ProjectCase::findOne($id);
        $model_pc->status = 0;
        $model_pc->save();
		
		$project = \backend\Modules\Task\models\CalCase::findOne($model_pc->id_project_fk);
        
        return array('success' => true, 'action' => 'detailsCases', 'html' => $this->renderAjax('_cases', [ 'model' => $project ]), 'alert' => 'Sprawa zostałą odłączona', 'id'=>$model_pc->id_project_fk);

	}
}

