<?php

namespace app\Modules\Task\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\components\CustomHelpers;

use backend\Modules\Company\models\CompanyEmployee;
use backend\Modules\Task\models\CalTask;

/**
 * Default controller for the `Task` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionEvents($id) {
       // $id = CustomHelpers::decode($id);
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $case = \backend\Modules\Task\models\CalCase::findOne($id);
        if($case) {
            if( isset($_GET['eid']) && !empty($_GET['eid']) ) {
                $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($_GET['eid']);
                $events = \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $id])->andWhere("id_dict_task_status_fk != 3 and event_date >= '".date('Y-m-d', $_GET['date'])."'")->orderby('type_fk,name')->all();
                $admin = 0;
            } else {
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                if($employee)
                    $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
                $events = [];
                if($employee) {
                    if($employee->is_admin) {
                        $events = \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $id])->andWhere('id_dict_task_status_fk != 3')->orderby('type_fk,name')->all();
                    } else {
                        $events = \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $id])->andWhere('id_dict_task_status_fk != 3 and id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employee->id.')')->orderby('name')->all();            
                    }
                } else {
                        $events = \backend\Modules\Task\models\CalTask::find()->where(['id_case_fk' => $id])->orderby('type_fk,name')->all();
                }
            }
            
            $list_events = '';
            if( !isset($_GET['without']) || $_GET['without'] == 0)
                $list_events = '<option value=""> - wybierz - </option>';

            $cases = [];
            $tasks = [];
            foreach($events as $key => $value) {
                if($value->type_fk == 1) 
                    $cases[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
                else
                    $tasks[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
            }
            
            if( count($cases) > 0 ) {
                $list_events .= '<optgroup label="Rozprawy">';
                foreach($cases as $key => $value) {
                    $list_events .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
            if( count($tasks) > 0 ) {
                $list_events .= '<optgroup label="Zadania">';
                foreach($tasks as $key => $value) {
                    $list_events .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
            
            $departments = [0 => 4];
            foreach($case->departments as $key => $item) {
                array_push($departments, $item->id_department_fk);
            }
            
            $employeesCase = [];
            foreach($case->employees as $key => $item) {
                array_push($employeesCase, $item['id_employee_fk']);
            }
            
            $specialDepartmens = [];
            $specialDepartmensSql = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
            foreach($specialDepartmensSql as $key => $value) {
                array_push($specialDepartmens, $value->id);
            }
            $managers = [];
            $managersSql = \backend\Modules\Company\models\CompanyDepartment::find()->all();
            foreach($managersSql as $key => $value) {
                array_push($managers, $value->id_employee_manager_fk);
            }
            
            $ids = $departments;
            foreach( $departments as $key => $value ) {
                if( in_array($value, $specialDepartmens) ) {
                    unset($ids[$key]);
                }
            }
            $ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
            $specialEmployees = [];
            if( isset($_GET['box']) && $_GET['box'] == 1) {
                $company = \backend\Modules\Company\models\Company::findOne(1);
                $customData = \yii\helpers\Json::decode($company->custom_data);
                
                if( isset($customData['box']) ) {
                    $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                    $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
                } else {
                    $company->box_special_emails = '';
                    $company->box_special_employees = [ 0 => 0 ];
                }
                $specialEmployees = $company->box_special_employees;
                if($employee) {
                    if($employee->is_admin || $admin) 
                        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) ) ')->orderby('lastname')->all();        
                    else
                        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( (id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('( id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) ) )')->orderby('lastname')->all();
                }  else  {
                    $employeesData = [];
                }
            } else {
                if($employee) {
                    if($employee->is_admin || $admin) 
                        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) )')->orderby('lastname')->all();        
                    else
                        $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( (id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) )')->orderby('lastname')->all();
                }  else  {
                    $employeesData = [];
                }
            }
            $employees = '';
            foreach($employeesData as $key => $model) {
                $checked = ''; $disabled = '';
                if($model->id == $this->module->params['employeeId'] || in_array($model->id, $managers) || in_array($model->id, $employeesCase) || $model->id_dict_employee_kind_fk == 100 )  $checked = ' checked ';
                //if(!in_array($model->id, $this->module->params['employees']) &&  $this->module->params['employeeKind'] != 100 && $admin == 0) $disabled = ' disabled="disabled" ';
                $employees .= '<li><input id="d'.$model->id.'" class="checkbox_employee" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked . $disabled.'>'
                        .'<label for="d'.$model->id.'" class="'. ( (in_array($model->id, $managers)) ? "text--blue" : ( (in_array($model->id, $specialEmployees)) ? "text--pink" : "text--black") ) .'">'.$model->fullname.'</label></li>';
            }
        } else {
            $list_events = ''; $departments = []; $employees = '';
        } 
        
        return ['events' => $list_events, 'departments' => $departments, 'employees' => $employees];
    }
    
    public function actionMevents() {
       // $id = CustomHelpers::decode($id);
		Yii::$app->response->format = Response::FORMAT_JSON;
        $selected = [];
        if(isset($_GET['events']) && !empty($_GET['events']) ) {
            $selected = explode(',', $_GET['events']);
        }
        //$case = \backend\Modules\Task\models\CalCase::findOne($id);
        if( isset($_GET['ids']) && !empty($_GET['ids']) ) {
            if(Yii::$app->params['env'] == 'dev') {
                $idsArr = explode(',', $_GET['ids']); $idsTemp = [];
                foreach($idsArr as $key => $value) {
                    $temp = explode('|', $value); array_push($idsTemp, $temp[0]);
                }
                $ids = implode(',', $idsTemp);
            } else {
                $ids = $_GET['ids'];
            }
            $events = \backend\Modules\Task\models\CalTask::find()->where('id_case_fk in ('. $ids .')')->andWhere('status = 1 and id_dict_task_status_fk != 3')->orderby('event_date')->all();
           
            $list_events = '';
            if( !isset($_GET['without']) || $_GET['without'] == 0)
                $list_events = '<option value=""> - wybierz - </option>';

            $cases = []; $tasks = [];  $matters = explode(',', $ids);
            if( count($matters) == 1 ) {
                foreach($events as $key => $value) {
                    if($value->type_fk == 1) 
                        $cases[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
                    else
                        $tasks[$value->id] = $value->name . ' ['.date('Y-m-d', strtotime($value->date_from)).']';
                }
                
                if( count($cases) > 0 ) {
                    $list_events .= '<optgroup label="Rozprawy">';
                    foreach($cases as $key => $value) { $list_events .= '<option value="'.$key.'" '.( in_array($key, $selected) ? ' selected ' : '' ).'>'.$value.'</option>'; }
                }
                if( count($tasks) > 0 ) {
                    $list_events .= '<optgroup label="Zadania">';
                    foreach($tasks as $key => $value) { $list_events .= '<option value="'.$key.'" '.( in_array($key, $selected) ? ' selected ' : '' ).'>'.$value.'</option>'; }
                }
            } else {
                if( count($events) > 0 ) {
                    $tempMatter = $events[0]->id_case_fk;
                    foreach($events as $key => $value) {
                        if($key == 0 || $tempMatter != $value->id_case_fk) { $list_events .= '<optgroup label="'.$value->case['name'].'">'; }
                        $list_events .= '<option value="'.$value->id.'" '.( in_array($value->id, $selected) ? ' selected ' : '' ).'>'.$value->name . ' ['.date('Y-m-d', strtotime($value->event_date)).']' .'</option>';
                        $tempMatter = $value->id_case_fk;
                    }
                }
            }
            
            $departments = [0 => 4];
            $selected = \backend\Modules\Task\models\CalCase::find()->where('id in ('.$ids.')')->all();
            foreach($selected as $key => $case) {
                foreach($case->departments as $key => $item) {
                    array_push($departments, $item->id_department_fk);
                }
            }
            
            $employeesCase = [];
            foreach($selected as $key => $case) {
                foreach($case->employees as $key => $item) {
                    array_push($employeesCase, $item['id_employee_fk']);
                }
            }
            
            $specialDepartmens = [];
            $specialDepartmensSql = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
            foreach($specialDepartmensSql as $key => $value) {
                array_push($specialDepartmens, $value->id);
            }
            $managers = [];
            $managersSql = \backend\Modules\Company\models\CompanyDepartment::find()->all();
            foreach($managersSql as $key => $value) {
                array_push($managers, $value->id_employee_manager_fk);
            }
            
            $ids = $departments;
            foreach( $departments as $key => $value ) {
                if( in_array($value, $specialDepartmens) ) {
                    unset($ids[$key]);
                }
            }
            $ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
            $specialEmployees = [];
            if( isset($_GET['box']) && $_GET['box'] == 1) {
                $company = \backend\Modules\Company\models\Company::findOne(1);
                $customData = \yii\helpers\Json::decode($company->custom_data);
                
                if( isset($customData['box']) ) {
                    $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                    $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
                } else {
                    $company->box_special_emails = '';
                    $company->box_special_employees = [ 0 => 0 ];
                }
                $specialEmployees = $company->box_special_employees;
                
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) ) ')->orderby('lastname')->all();        
            } else {
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and ( id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).')) )')->orderby('lastname')->all();        
            }
            $employees = '';
            foreach($employeesData as $key => $model) {
                $checked = ''; $disabled = '';
                if($model->id == $this->module->params['employeeId'] || in_array($model->id, $managers) || in_array($model->id, $specialEmployees) || in_array($model->id, $employeesCase) || $model->id_dict_employee_kind_fk == 100 )  $checked = ' checked ';
                //if(!in_array($model->id, $this->module->params['employees']) &&  $this->module->params['employeeKind'] != 100 && $admin == 0) $disabled = ' disabled="disabled" ';
                $employees .= '<li><input id="d'.$model->id.'" class="checkbox_employee" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked . $disabled.'>'
                        .'<label for="d'.$model->id.'" class="'. ( (in_array($model->id, $managers)) ? "text--blue" : ( (in_array($model->id, $specialEmployees)) ? "text--pink" : "text--black") ) .'">'.$model->fullname.'</label></li>';
            }
        } else {
            $list_events = ''; $departments = []; $employees = '';
        } 
        
        return ['events' => $list_events, 'departments' => $departments, 'employees' => $employees];
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 3;
            $note->id_case_fk = 0;
            $note->id_task_fk = 0;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }
    
    public function actionBusydays() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $days = [];
        
        $month = date('m'); $year = date('Y');
        if( isset($_GET['month']) && !empty($_GET['month']) ) {
            $month = $_GET['month'];
            $year = $_GET['year'];
        }
        if($month == 1) {
            $dateFrom = ($year-1).'-11-1';
            $dateTo = ($year).'-03-31';
        } else if($month == 2) {
            $dateFrom = ($year-1).'-11-1';
            $dateTo = ($year).'-03-31';
        } else if($month == 11) {
            $dateFrom = ($year).'-09-1';
            $dateTo = ($year+1).'-01-31';
        } else if($month == 12) {
            $dateFrom = ($year).'-10-1';
            $dateTo = ($year+1).'-02-31';
        } else {
            $dateFrom = $year.'-'.str_pad(($month-2), 1, '0', STR_PAD_LEFT).'-1';
            $dateTo = $year.'-'.str_pad(($month+2), 1, '0', STR_PAD_LEFT).'-31';
        }
        
        //$days = CalTask::find()->where(['status' => 1])->andWhere("event_date between '". $dateFrom ."' and '". $dateTo ."' ")->select('event_date')->distinct()->all();
        //$sql = "select distinct( date_format(event_date, '%Y-%c-%e')) as event_date from {{%cal_task}} where event_date between '". $dateFrom ."' and '". $dateTo ."'";
        $sql = "select distinct( date_format(event_date, '%Y-%c-%e')) as event_date from {{%cal_task}} where event_date between '". $dateFrom ."' and '". $dateTo ."'";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($data as $key => $value) {
            array_push($days, $value['event_date']);
        }
        
        return ['days' => $days];
    }
    
    public function actionCinfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $claimsData = \backend\Modules\Task\models\CaseClaim::getList($id);
        $claims = []; $claimsList = '';
        if($claimsData) {
            foreach($claimsData as $key => $value) {
                $claims[$value->id] = $value->title;
                $claimsList .= '<option value="'.$value->id.'">'.$value->title.'</option>';
            }
        }
        
        return ['cliams' => $claims, 'claimsList' => $claimsList];
    }
}
