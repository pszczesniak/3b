<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseClaim;
use backend\Modules\Task\models\CalCaseInstance;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * ClaimController implements the CRUD actions for CaseClaim model.
 */
class ClaimController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex() {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CaseClaim();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.claims'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['CaseClaim'])) {
            $params = $_GET['CaseClaim'];
            \Yii::$app->session->set('search.claims', ['params' => $params, 'post' => $post]);
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(cc.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['id_dict_claim_type_fk']) && !empty($params['id_dict_claim_type_fk']) ) {
                 array_push($where, "id_dict_claim_type_fk = ".$params['id_dict_claim_type_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'cc.title';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
		
		$query = (new \yii\db\Query())
            ->select(['cc.id', 'cc.type_fk', 'cc.title as cname', 'dict.name as claim_type', 'cc.note as note', 'amount', 'occurence_date', 'payment_date', 'debt_interest', 'debt_balance',
                      'c.id as cid', 'c.name as customer', 'm.id as mid', 'm.name as mname'])
            ->from('{{%cal_case}} as m')
			->join(' JOIN', '{{%case_claim}} as cc', 'm.id = cc.id_case_fk')
            ->join(' JOIN', '{{%dictionary_value}} as dict', 'dict.id = cc.id_dict_claim_type_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = m.id_opposite_side_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/task/manage/editclaim', 'id' => $value['id']]).'" class="btn btn-xs btn-default text--blue gridViewModal" data-table="#table-sides" data-form="person-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/task/manage/deleteclaim', 'id' => $value['id']]).'" class="btn btn-xs btn-default text--red remove" data-table="#table-sides"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['type'] = $value['claim_type'].'<br /><span class="label '.(($value['type_fk'] == 1) ? 'bg-blue">finansowe' : 'bg-purple">niefinansowe').'</span>';
			$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['customer'].'</a>'
                                .'<br /><a href="'.Url::to(['/task/matter/viewnew', 'id' => $value['mid']]).'" class="text--purple"><small><i class="fa fa-balance-scale"></i>&nbsp;'.$value['mname'].'</small></a>';
            $tmp['name'] = $value['cname'];
            $tmp['value'] = $value['amount'];
            $tmp['dates'] = '<span class="text--navy">'.$value['occurence_date'].'</span></br><span class="text--red">'.$value['payment_date'].'</span>';
            $tmp['details'] = '<span class="text--purple">Odsetki: <b>'.number_format($value['debt_interest'], 2, '.',' ').'</b></span><br /><span class="text--orange">Należność: <b>'.number_format($value['debt_balance'], 2, '.',' ').'</b></span>';
            $tmp['note'] = ($value['note']) ? '<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            //$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            //$tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			//$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
			$tmp['actions'] = $actionColumn;
            //$tmp['className'] = 'success';
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CaseClaim::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
  
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Roszczenia');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
			->setCellValue('B3', 'Sprawa')
			->setCellValue('C3', 'Typ')
			->setCellValue('D3', 'Nazwa')
            ->setCellValue('E3', 'Data wystawienia')
            ->setCellValue('F3', 'Data płatności')
            ->setCellValue('G3', 'Kwota')
            ->setCellValue('H3', 'Odsetki')
            ->setCellValue('I3', 'Należność');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:I3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CaseClaim'])) {
            $params = $_GET['CaseClaim'];
            \Yii::$app->session->set('search.claims', ['params' => $params, 'post' => $post]);
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(cc.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['id_dict_claim_type_fk']) && !empty($params['id_dict_claim_type_fk']) ) {
                 array_push($where, "id_dict_claim_type_fk = ".$params['id_dict_claim_type_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'cc.title';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
		
		$query = (new \yii\db\Query())
            ->select(['cc.id', 'cc.type_fk', 'cc.title as cname', 'dict.name as claim_type', 'cc.note as note', 'amount', 'occurence_date', 'payment_date', 'debt_interest', 'debt_balance',
                      'c.id as cid', 'c.name as customer', 'm.id as mid', 'm.name as mname'])
            ->from('{{%cal_case}} as m')
			->join(' JOIN', '{{%case_claim}} as cc', 'm.id = cc.id_case_fk')
            ->join(' JOIN', '{{%dictionary_value}} as dict', 'dict.id = cc.id_dict_claim_type_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = m.id_opposite_side_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				//$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['customer'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['mname']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['claim_type']); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['occurence_date']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['payment_date']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $record['amount']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['debt_interest']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['debt_balance']); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getStyle('A1:I'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:I'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Roszczenia');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Roszczenia'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
	
    public function actionPayments($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $grants = $this->module->params['grants']; $where = [];

        $post = $_GET;
        if(isset($_GET['DebtPaymentSearch'])) {
            $params = $_GET['DebtPaymentSearch'];
           
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
				array_push($where, "p.id_set_fk = ".$params['id_set_fk']);
            }
        }   
        
		$sortColumn = 'payment_date';
        if( isset($post['sort']) && $post['sort'] == 'history_date' ) $sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'id_debt_action_fk' ) $sortColumn = 'dict.dict_name';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query = (new \yii\db\Query())
            ->select(['p.id as id', 'payment_title', 'payment_note', 'account_type_fk', 'payment_date', 'payment_amount', 'e.firstname', 'e.lastname', 's_capital', 's_interest'])
            ->from('{{%debt_payment}} p')
            ->join(' JOIN', '{{%debt_payment_settlements}} as s', 'p.id = s.id_payment_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = p.id_employee_fk')
            ->where( ['p.status' => 1, 's.id_claim_fk' => $model->id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
			
		$fields = [];
		$tmp = [];
	
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/debt/payment/updateajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-payment" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/debt/payment/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-payment"><i class="fa fa-trash" title="Usuń"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['term'] = $value['payment_date'];
            $tmp['amount'] = number_format($value['payment_amount'], 2, "," ,  " ");
            $tmp['capital'] = number_format($value['s_capital'], 2, "," ,  " ");
            $tmp['interest'] = number_format($value['s_interest'], 2, "," ,  " ");
            $tmp['p_currency'] = $value['payment_amount'];
			$tmp['title'] = $value['payment_title'];
			$tmp['employee'] = $value['lastname'].' '.$value['firstname'];
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		//return ['total' => $count,'rows' => $fields];
        return $fields;
	}
    
    public function actionInterest() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [];
        
        if(isset($_GET['amount']) && isset($_GET['date']) && isset($_GET['indicator']) && !empty($_GET['amount']) && !empty($_GET['date']) && !empty($_GET['indicator'])){
            $result = $this->calculate($_GET['amount'], $_GET['date'], date('Y-m-d'), $_GET['indicator']);
        } 
            
        return ['interest' => ($result['interest']) ? round($result['interest'], 2) : false, 'delay' => $result['delay']];
    }
    
    private function calculate($amount, $dateFrom, $dateTo, $indicator) {
        $interest = false; $delay = false;
      
        $rates = \backend\Modules\Config\models\IndicatorValue::find()->where(['status' => 1, 'id_indicator_fk' => $indicator])->andWhere("(date_to >= '".$dateFrom."' or date_to is null)")->orderby('date_from')->all();
        foreach($rates as $key => $value) {
            
            $dateToTemp = ($value->date_to && strtotime($value->date_to) < strtotime($dateTo)) ? $value->date_to : $dateTo;
            $dateFromTemp = (strtotime($value->date_from) < strtotime($dateFrom)) ? $dateFrom : $value->date_from;
            //echo $dateFromTemp.' - '.$dateToTemp.'; ';
            ////$datediff = strtotime($dateToTemp) - strtotime($dateFromTemp);
            ////$delayTemp = floor($datediff / (60 * 60 * 24));
            
            $date1=date_create($dateFromTemp);
            $date2=date_create($dateToTemp);
            $interval=date_diff($date1,$date2);
            $delayTemp = $interval->format('%a');
            
            /*$interestAll = $amount * $value->value / 100;
            $interestPerDay = $interestAll / 365; echo $delayTemp.' => '.$interestPerDay.'; ';
            $interest += ($interestPerDay*$delayTemp);*/
            
            $interestPerDay = $amount * $delayTemp * ($value->value / 100) / 365; 
            //echo $interestPerDay.' ('.$delayTemp.'); ';
            $interest += $interestPerDay;
            
            $delay += $delayTemp;
        }
        
        return ['interest' => $interest, 'delay' => $delay];
    }
}
