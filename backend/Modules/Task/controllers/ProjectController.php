<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseDetails;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\data\Pagination;

/**
 * ProjectController implements the CRUD actions for CalCase model.
 */
class ProjectController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex()  {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalCaseSearch();
        $searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.projects'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(((Yii::$app->params['env'] == 'dev') ? 'indexnew' : 'indexnew'), [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

        $post = $_GET;
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            \Yii::$app->session->set('search.projects', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from {{%case_department}} where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_type_fk']) && !empty($params['id_dict_case_type_fk']) ) {
				array_push($where, "id_dict_case_type_fk = ".$params['id_dict_case_type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk != 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4");
        }
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'c.name as cname', 'c.id as cid', 'cd.time_limit as time_limit', 'dv.name as dvname',
                      'cd.date_from', 'cd.date_to', 'cd.time_used as time_used', 'cd.budget_limit as budget_limit', 'cd.budget_used as budget_used', 'cd.date_from as date_from', "ifnull(cd.date_to,date_format(m.close_date,'%Y-%m-%d')) as date_to"])
            ->from('{{%cal_case}} as m')
            ->join('LEFT JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->join('LEFT JOIN', '{{%dictionary_value}} as dv', 'dv.id = m.id_dict_case_type_fk')
            ->join('LEFT JOIN', '{{%cal_case_details}} as cd', 'm.id = cd.id_case_fk and cd.status = 1')
            ->where( ['m.status' => 1, 'm.type_fk' => 2] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
		foreach($rows as $key=>$value) {
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.$value['cname'].'</a>';            
            $tmp['sname'] = $value['name'];
            $tmp['start_date'] = $value['date_from'];
            $tmp['end_date'] = $value['date_to'];
            $tmp['type'] = $value['dvname'];
			$tmp['name'] = '<a href="'.Url::to(['/task/project/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
            $tmp['status'] = '<label class="label '.$colors[$value['id_dict_case_status_fk']].'">'.$status[$value['id_dict_case_status_fk']].'</label>';
			/*$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/project/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/project/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/project/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-projects"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
            $tmp['actions'] = '<div class="btn-group">'
                              .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                              .'</button>'
                              .'<ul class="dropdown-menu dropdown-menu-right">'
                                . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/project/update', 'id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                . ( (count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-projects" href="'.Url::to(['/task/project/delete', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 && $value['id_dict_case_status_fk'] == 1) ? '<li class="update" data-table="#table-projects" href="'.Url::to(['/task/project/close', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check text--green\'></i>'.Yii::t('app', 'Zamknij projekt').'" title="'.Yii::t('app', 'Zamknij projekt').'"><a href="#"><i class="fa fa-check text--green"></i>&nbsp;Zamknij</a></li>' : '')
                                . '<li><a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" title="Pobierz wszystkie dokumenty"><i class="fa fa-download text--teal"></i>&nbsp;Pobierz dokumenty</a></li>'
    
                                //.'<li role="separator" class="divider"></li>'
                                //.'<li><a href="#">Separated link</a></li>'
                              .'</ul>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			//$tmp['progress'] = '<div class="progress"><div class="progress-bar" data-transitiongoal="85" style="width: 85%;" aria-valuenow="85">85%</div></div>';
            $percent = 0;
            if($value['time_limit'] > 0) $percent = round(($value['time_used']*100)/($value['time_limit']*60));
            $tmp['progress'] = '<div class="c100 p'.$percent.' small green center"> <span>'.$percent.'%</span><div class="slice"><div class="bar"></div><div class="fill"></div></div></div>';
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionAdmin()  {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalCaseSearch();
        $searchModel->type_fk = 2;
        
        /*if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.projects'); 
            if($params) {
                foreach($params as $key => $value) {
                    $searchModel->$key = $value;
                }
            }
        }*/
        
        $params = Yii::$app->request->post();
        $name = false;  $customer = false; $status = false;
        if(isset($params['CalCaseSearch']['name'])) $name = $params['CalCaseSearch']['name']; 
        if(isset($params['CalCaseSearch']['id_customer_fk'])) $customer = $params['CalCaseSearch']['id_customer_fk']; 
        if(isset($params['CalCaseSearch']['id_dict_case_status_fk'])) $status = $params['CalCaseSearch']['id_dict_case_status_fk']; 
        
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(count(array_intersect(["grantAll"], $this->module->params['grants'])))
            $query = CalCase::find()->where(['status' => 1, 'type_fk' => 2]);
        else
            $query = CalCase::find()->where(['status' => 1, 'type_fk' => 2])->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$this->module->params['employeeId'].')');
        if($name) { $query->andWhere(' lower(name) like lower("%'.$name.'%")');  $searchModel->name = $name; }
        if($customer) { $query->andWhere('id_customer_fk = '.$customer); $searchModel->id_customer_fk = $customer;}
       // if($text) $query->andWhere(' ( lower(companyname) like "%'.strtolower($text).'%" or lower(note) like "%'.strtolower($text).'%" )');
       
        $countQuery = clone $query;
        $totalCount = $countQuery->count();
        $pages = new Pagination(['totalCount' => $totalCount, 'defaultPageSize' => 9]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'models' => $models,
            'pages' => $pages,
            'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Displays a single CalCase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego zadania.');
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        $departmentsList = '<ul>';  $filesList = '';  $employeesList = '';  $correspondenceList = '';
        foreach($model->departments as $key => $department) {
            $departmentsList .= '<li>'.$department->department['name'].'</li>';
        }
        $departmentsList .= '</ul>';
        $model->departments_list = $departmentsList;
        $grants = $this->module->params['grants'];
                
        return $this->render('view', [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        if( !isset($_POST['CalCase']['id']) || empty($_POST['CalCase']['id']) ) {
            $model = new CalCase();
            $model->type_fk = 2;
            $model->name = 'Wersja robocza '.time();
            $model->status = -2;
            $model->save();
            $model->name = '';
        } else {
            $model = CalCase::findOne($_POST['CalCase']['id']);
        }
        $details = new CalCaseDetails();
        $details->id_case_fk = $model->id;
        
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $model->id_dict_case_status_fk = 1;
		$departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']); 
		
		$status = CalCase::listStatus('normal');
        $lists = ['departments' => [], 'employees' => [], 'contacts' => []];
        $ids = [0 => 0];
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $details->load(Yii::$app->request->post());
            $model->status = 1;
            $model->user_action = 'create';
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) ? count($_POST['CalCase']['departments_list']) : '';
            
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            }  
                       
            
			if($model->validate() && $details->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if($model->save() && $details->save()) {
                        if(isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) {
                            //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                            foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                                $model_cd = new CaseDepartment();
                                $model_cd->id_case_fk = $model->id;
                                $model_cd->id_department_fk = $value;
                                $model_cd->save();
                            }
                        } 
                        
                        if(isset($_POST['employees']) ) {
                            //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                            foreach(Yii::$app->request->post('employees') as $key=>$value) {
                                $model_ce = new CaseEmployee();
                                $model_ce->id_case_fk = $model->id;
                                $model_ce->id_employee_fk = $value;
                                $model_ce->save();
                            }
                        } 
                        $transaction->commit();	
                    } else {
                        $model->status = -2;
                        $lists = $this->lists($model->id_customer_fk, $model->departments_list);
               
                        return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'createnew'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'], 'errors' => array_merge($model->getErrors(), $details->getErrors())]) ;	
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $model->status = -2;
                    $lists = $this->lists($model->id_customer_fk, $model->departments_list);
               
                    return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'createnew'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'], 'errors' => array_merge($model->getErrors(), $details->getErrors())]) ;	
                }		
				
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                $model->status = -2;
                $lists = $this->lists($model->id_customer_fk, $model->departments_list);
               
                return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'createnew'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'], 'errors' => array_merge($model->getErrors(), $details->getErrors())]) ;	
			}		
		} else {
			return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'createnew'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
	}
    
    public function actionCreateajax($id) {
        $model = new CalCase();
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $customerId = CustomHelpers::decode($id);
       // $customerId = ( isset($_GET['id']) ) ? $_GET['id'] : null;
        $model->id_customer_fk = $customerId;
        $model->id_dict_case_status_fk = 1;
        $model->status = 1;
        $model->type_fk = 1;
        if( isset($_GET['cid']) && !empty($_GET['cid']) )
            $model->id_customer_fk = $_GET['cid'];
            
        $specialsDepartments = \backend\Modules\Company\models\CompanyDepartment::getSpecials();
        $model->departments_list = [];
        if($specialsDepartments) {
            foreach($specialsDepartments as $key => $item) {
                array_push($model->departments_list, $item->id);
            }
        }
   
        $grants = $this->module->params['grants'];
        $employees = [];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) ? count($_POST['CalCase']['departments_list']) : '';  
            
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            }  

            if($model->validate() && $model->save()) {                      
                if(isset($_POST['CalCase']['departments_list']) ) {
                    //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                        $model_cd = new CaseDepartment();
                        $model_cd->id_case_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
                } 
                
                if(isset($_POST['employees']) ) {
                    //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('employees') as $key=>$value) {
                        $model_ce = new CaseEmployee();
                        $model_ce->id_case_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
                } 

                return array('success' => true, 'action' => 'boxCase', 'index' =>1, 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $lists = $this->lists($model->id_customer_fk, $model->departments_list);
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, 'lists' => $lists, 'employees' => $employees ]), 'errors' => $model->getErrors() );	
            }     	
		} else {
			$lists = $this->lists($model->id_customer_fk, $model->departments_list);
            return  $this->renderAjax('_formAjax', [  'model' => $model, 'lists' => $lists, 'employees' => $employees ]) ;	
		}
	}

    /**
     * Updates an existing CalCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     public function actionUpdate($id)  {
        if( count(array_intersect(["caseEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        $customerTemp = $model->id_customer_fk;
        
        $details = $model->details;
        if(!$details) {
            $details = new CalCaseDetails();
            $details->id_case_fk = $model->id;
            $details->save();
        }
        
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tego zadania.');
            }
        }
        
        if($model->status == -1) {
            return  $this->render('delete', [ 'model' => $model, ]) ;	
        }
        
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $departments = []; $employees = [];
		$status = CalCase::listStatus('normal');
        $grants = $this->module->params['grants'];
    
        $model->departments_list = [];
        foreach($model->departments as $key => $item) {
            array_push($model->departments_list, $item->id_department_fk);
        }
        foreach($model->employees as $key => $item) {
            array_push($employees, $item['id_employee_fk']);
        }
      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($customerTemp == $model->id_customer_fk) $model->is_ignore = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list']) ) ? count($_POST['CalCase']['departments_list']) : ''; 
            if(isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) {
                foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                    array_push($model->departments_rel, $value);
                    array_push($model->departments_list, $value);
                }
            } 
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);
           
			if($model->validate() && $model->save()) {			

				return array('success' => true,  'action' => false, 'alert' => 'Projekt <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]), 'errors' => $model->getErrors() );	
			}		
		} else {
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);
            /*$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            if($employee)
                $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
            if($employee->is_admin || $admin) 
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$model->departments_list).'))')->orderby('lastname')->all();        
            else
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$model->departments_list).'))')->orderby('lastname')->all();
            $temp = [];
            foreach($employeesData as $key => $employee) {
                $temp[$employee->id] = $employee->fullname;                    
            }
            $lists['employees'] = $temp;*/
            return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'updatenew' : 'updatenew'), ['model' => $model, 'details' => $details, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'],'grants' => $this->module->params['grants']]) ;	
		}
    }

    /**
     * Deletes an existing CalCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        if( count(array_intersect(["caseDelete", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalCase::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $model->id])->count();
        $correspondence = Correspondence::find()->where(['id_case_fk' => $model->id])->andWhere('status >= 1')->count();
        
        if($events != 0 || $correspondence != 0) {
            if(!Yii::$app->request->isAjax) {
				if($events > 0)
                    Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Projekt <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane zdarzenia!')  );
                if($correspondence > 0)
                    Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Projekt <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisaną korespondencję!')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				if($events > 0)
                    return array('success' => false, 'alert' => 'Projekt <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane zdarzenia!<br />', 'id' => $id, 'table' => '#table-projects');	
                if($correspondence > 0)
                    return array('success' => false, 'alert' => 'Projekt <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisaną korespondencję!<br />', 'id' => $id, 'table' => '#table-projects');	
                
			}
        }
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Projekt <b>'.$model->name.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Projekt <b>'.$model->name.'</b> został usunięty.<br/>', 'id' => $id, 'table' => '#table-projects');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Projekt <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Projekt <b>'.$model->name.'</b> nie została usunięty.<br />', 'id' => $id, 'table' => '#table-projects');	
			}
        }
        
    }
    
    public function actionStatus($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalCase::SCENARIO_STATUS;
        if(isset($_GET['status']) && !empty($_GET['status']) && in_array($_GET['status'], [1,2,3,4])) {
            $model->id_dict_case_status_fk = ($_GET['status']);
            $model->user_action = 'status';

            $model->save();
            
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app',  'Projekt przeszedł w status <b>'.$model->statusname.'</b>' ) );
        } else {
            if($model->id_dict_case_status_fk < 4) {
                $model->id_dict_case_status_fk = ($model->id_dict_case_status_fk+1);
                $model->user_action = 'status';

                $model->save();
                
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app',  'Projekt przeszedł w status <b>'.$model->statusname.'</b>' ) );
            } else {
                Yii::$app->getSession()->setFlash( 'warning', Yii::t('app', 'Projekt jest już zamknięty.')  );
            }
        }
   
        return $this->redirect(['update', 'id' => CustomHelpers::decode($id)]);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', );	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = CalCase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function lists($id, $departmentsList) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        if(!$departmentsList) $departmentsList = [];               
        $model = \backend\Modules\Crm\models\Customer::findOne($id);
        $departments = []; $ids = $departmentsList; array_push($ids, 0); $employees = []; $contacts = [];
        $checked = 'checked';
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        if($model) {
            /*foreach($model->departments as $key => $value) {
                if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                    $departments[$value->id_department_fk] = $value->department['name'];
                    array_push($ids, $value->id_department_fk);
                } else {
                    if(in_array($value->id_department_fk, $this->module->params['departments'])) {                
                       // $departments .= '<li><input class="checkbox_department" id="d-'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.'><label for="d'.$value->id_department_fk.'">'.$value->department['name'].'</label></li>';
                        $departments[$value->id_department_fk] = $value->department['name'];
                        array_push($ids, $value->id_department_fk);
                    }
                }
            }
            //var_dump($departments);exit;
            if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                $employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
            } else {
                $employeesTemp = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
            }
            foreach($employeesTemp as $key => $model) {
                //if($model->id == $this->module->params['employeeId'])  $checked = 'checked'; else $checked = '';
                //$employees_list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'><label for="d'.$model->id.'">'.$model->fullname.'</label></li>';
                $employees[$model->id] = $model->fullname;
            }*/
            
            $list = '';
            
            $cases = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id])->orderby('lastname')->all();
            $list .= '<option value=""> - wybierz - </option>';
            foreach($cases as $key => $model) {
                //$list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
                $contacts[$model->id] = $model->fullname;
            }
        }
        
        $specialDepartmens = [];
        $specialDepartmensSql = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartmensSql as $key => $value) {
            array_push($specialDepartmens, $value->id);
        }
        
       /* foreach( $departmentsList as $key => $value ) {
            if( in_array($value, $specialDepartmens) ) {
                unset($ids[$key]);
            }
        }*/
        $ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
        
        //if($employee->is_admin || $admin) 
            $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
        //else
        //    $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and  (id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        foreach($employeesData as $key => $item) {
            $employees[$item->id] = $item->fullname;                    
        }
                
        return ['contacts' => $contacts, 'departments' => $departments, 'employees' => $employees];
    }
    
    public function actionEvents($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$events = [];
		$tmp = [];
		
		$model = $this->findModel($id);
		$eventsData = $model->events;
		
		$actionColumn = '<div class="edit-btn-group">';
		//$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
		$actionColumn .= '<a href="/task/case/viewevent/%d" class="btn btn-xs btn-info btn-icon gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-event" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >W</a>';
        $actionColumn .= '<a href="/task/case/updateevent/%d" class="btn btn-xs btn-success btn-icon gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-event" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';

		$actionColumn .= '<a href="/task/case/deleteevent/%d" class="btn btn-xs btn-danger remove" data-table="#table-events">U</a>';
		$actionColumn .= '</div>';
		
		foreach($eventsData as $key=>$value) {
			$tmp = [];
			$tmp['id'] = $value->id;
			$tmp['name'] = $value->name;
			$tmp['date_start'] = $value->date_from;
			$tmp['date_end'] = $value->date_to;
            $employeesList = '';
            foreach($value->employees as $key1 => $item) {
                $employeesList .= '<li>'.$item->employee['fullname']  .'</li>';
            }
            $value->employees_list = ($employeesList) ? '<ol>'.$employeesList.'</ol>' :  'brak danych';
            $tmp['employees'] = $value->employees_list;
			$tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
			//array_push($tmp, '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>');
			
			array_push($events, $tmp); $tmp = [];
		}
		
		return $events;
	}
    
    public function actionExport() {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Hufgard")
                    ->setLastModifiedBy("Hufgard")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sprawy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Kontrahent')
			->setCellValue('B3', 'Nazwa')
			->setCellValue('C3', 'Status')
            ->setCellValue('D3', 'Typ')
			->setCellValue('E3', 'Pracownicy');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:E3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        $i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from {{%case_department}} where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_type_fk']) && !empty($params['id_dict_case_type_fk']) ) {
				array_push($where, "id_dict_case_type_fk = ".$params['id_dict_case_type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } /*else {
            array_push($where, "id_dict_case_status_fk != 4");
        }*/
        
        $fields = []; $tmp = [];
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'c.name as cname', 'c.id as cid', 'cd.time_limit as time_limit', 'dv.name as dvname',
                      'cd.date_from', 'cd.date_to', 'cd.time_used as time_used', 'cd.budget_limit as budget_limit', 'cd.budget_used as budget_used', 'cd.date_from as date_from', 'cd.date_to as date_to'])
            ->from('{{%cal_case}} as m')
            ->join('LEFT JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->join('LEFT JOIN', '{{%dictionary_value}} as dv', 'dv.id = m.id_dict_case_type_fk')
            ->join('LEFT JOIN', '{{%cal_case_details}} as cd', 'm.id = cd.id_case_fk and cd.status = 1')
            ->where( ['m.status' => 1, 'm.type_fk' => 2] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, isset($status[$record['id_dict_case_status_fk']]) ? $status[$record['id_dict_case_status_fk']] : 'nieznany'); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, isset($status[$record['id_dict_case_status_fk']]) ? $status[$record['id_dict_case_status_fk']] : 'nieznany'); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
                $employees = '';
                foreach($case->employees as $key => $employee) {
                    $employees .= $employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Sprawy');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Sprawy'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionEmployees($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = ''; $select = '';
        
        //$id = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : '0';
        $employees = \backend\Modules\Company\models\CompanyEmployee::find()
                    //->where('(id_dict_employee_kind_fk < '.$this->module->params['employeeKind'].' or id = '.$this->module->params['employeeId'].')')
                    ->where('id in (select id_employee_fk from {{%case_employee}} where id_case_fk  ='.$id.')')->orderby('lastname collate `utf8_polish_ci`')->all();

        foreach($employees as $key => $model) {
            $checked = ''; $disabled = '';
            if($model->id == $this->module->params['employeeId'])  $checked = ' checked ';
            if(!in_array($model->id, $this->module->params['employees']) &&  $this->module->params['employeeKind'] != 100) $disabled = ' disabled="disabled" ';
            $list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked . $disabled.'><label for="d'.$model->id.'">'.$model->fullname.'</label></li>';
            $select .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
        
        if(!$id) {
            $employees = \backend\Modules\Company\models\CompanyEmployee::getListAll();
            foreach($employees as $key => $model) {
                $select .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
            }
        }
        
        if(!$list)
            $list .= '<li>brak przypisanych do sprawy pracowników</li>';
        
        return ['list' => $list, 'select' => $select];
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = $id;
            $note->id_type_fk = 1;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActions'])) {
            $params = $post['AccActions']; 
            if(isset($params['description']) && !empty($params['description']) ) {
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "id_dict_order_type_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "is_confirm = 1");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0");
            }
        } 
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "id_department_fk in (".implode(',', $this->module->params['managers']).")");
        }
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) == 0 ) {
            array_push($where, "id_employee_fk = ".$this->module->params['employeeId']."");
        }
        
        $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time ';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price ';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit ';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'is_confirm', "action_date", "unit_time", "confirm_price", "confirm_time", "acc_cost_net", "is_confirm", "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color"])
            ->from('{{%acc_actions}} a')
            ->join('JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.id_set_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
       
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", "sum(acc_cost_net) as costs", "sum(confirm_time) as confirm_time", "sum(confirm_time*confirm_price) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.id_set_fk' => $id] );
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time'];
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
        
       // if( $count <= 4000 ) {
           // $fieldsData = $fieldsDataQuery->orderby('action_date desc')->all();
            $tmp = [];
            $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
            foreach($rows as $key=>$value) {
                $tmpType = $dict[$value['type_fk']];
                $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].' cursor-pointer" title="'.$tmpType['name'].'"></i>';
                $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
                   
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                $tmp['name'] = $value['description'];
                $tmp['action_date'] = $value['action_date'];
                $tmp['employee'] = $value['ename'];
                $tmp['unit_time'] = number_format ($value['unit_time'], 2, "," ,  " "); //round($value['unit_time']/60,2);
                $tmp['price'] = round($value['confirm_price'], 2);
                $tmp['amount'] = round( ($value['confirm_price']*$value['confirm_time']),2);
                $tmp['service'] = $type;
                $tmp['description'] = $value['description'];
                $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                $tmp['confirm'] = $value['is_confirm'];
                $tmp['cost'] = $value['acc_cost_net'];
                $tmp['actions'] = '<div class="edit-btn-group">';
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                    //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
                $tmp['actions'] .= '</div>';
                //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
                $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $tmp['id'] = $value['id'];
                
                array_push($fields, $tmp); $tmp = [];
            }
       /* } else {
            echo $count;
        }*/
		
		return ['total' => $count,'rows' => $fields, 'cost' => number_format($totalRow['costs'], 2, "," ,  " "), 'time1' => number_format($summary, 2, "," ,  " "), 'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 'amount' => number_format($totalRow['amount'], 2, "," ,  " ")];
	}
    
    public function actionPevents($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $events = []; $fields = []; $tmp = [];
        $personalTypes = \backend\Modules\Task\models\CalTodo::dictTypes();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		$model = $this->findModel($id);
        
        $isManager = \backend\Modules\Company\models\CompanyDepartment::find()->where(['status' => 1, 'id_employee_manager_fk' => $this->module->params['employeeId']])->count();
        if($isManager == 1 || $this->module->params['isAdmin'])
            $eventsData = \backend\Modules\Task\models\CalTodo::find()->where(['id_set_fk' => $model->id, 'status' => 1])->orderby('created_at desc')->all();
        else
            $eventsData = \backend\Modules\Task\models\CalTodo::find()->where(['id_set_fk' => $model->id, 'status' => 1, 'id_employee_fk' => $this->module->params['employeeId']])->orderby('created_at desc')->all();
            
            
		//$eventsData = $model->pevents;
	
		foreach($eventsData as $key=>$value) {
			$bgColor = $personalTypes[$value->id_dict_todo_type_fk]['color'];
            $bgIcon = $personalTypes[$value->id_dict_todo_type_fk]['icon'];
            
            $actionColumn = '<div class="edit-btn-group">';
                //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
                $actionColumn .= ((!$value->is_close) ? '<a href="'.Url::to(['/task/personal/close', 'id' => $value->id]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-check\'></i>Zamknij zadanie" title="Zamkinij zadanie"><i class="fa fa-check text--green"></i></a>' : '');                
                $actionColumn .= '<a href="'.Url::to(['/task/personal/updateajax', 'id' => $value->id]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil text--blue"></i></a>';
                //$actionColumn .= '<a href="'.Url::to(['/task/'.$type.'/update', 'id' => $value->id]).'" class="btn btn-xs btn-default  "  ><i class="fa fa-pencil" title="'.Yii::t('app', 'Edit').'"></i></a>';
                $actionColumn .= (($value->id_action_fk) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value->id_action_fk]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-briefcase\'></i>Czynność na rzecz klienta" title="Czynność na rzecz klienta"><i class="fa fa-briefcase text--purple"></i></a>' : '');                
                $actionColumn .= ((!$value->id_action_fk && $value->is_close) ? '<a href="'.Url::to(['/task/personal/aproject', 'id' => $value->id]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-plus\'></i>Czynność na rzecz klienta" title="Utwórz czynność na rzecz klienta"><i class="fa fa-plus text--purple"></i></a>' : '');                

                $actionColumn .= '<a href="'.Url::to(['/task/personal/deleteajax', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-todos" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash text--red"></i></a>';
            $actionColumn .= '</div>';
        
            $type = ($value->type_fk == 1) ? 'case' : 'event';
			//$tmp['ename'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
			$tmp['name'] = $value->name;
            $tmp['employee'] = $value->employee['fullname'];
            $tmp['delay'] = $value->delay;
           // $tmp['className'] = ( ( $value['id_dict_task_status_fk'] == 1 && $value->delay > 1 && $value->type_fk == 2)  ) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2  && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['etype'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Administracyjne</span>' : '<span class="label bg-teal">Osobiste</span>' ;
            $tmp['deadline'] = ($value['todo_deadline'])?$value['todo_deadline']:'nie ustawiono';
            $tmp['term'] = $value['todo_date'].' '.( ( !empty($value['todo_time']) && $value['todo_time'] != '00:00' ) ? $value['todo_time']: '' );
            $tmp['created'] = $value->creator.'<br/>'.$value->created_at;
            $tmp['status'] = ($value->is_close) ? '<span class="label bg-green">zamknięte</span>' : '<span class="label bg-blue">ustalone</span>';
            $tmp['time'] = round($value->execution_time/60, 2);
            $tmp['client'] = $value['case']['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['category'] = '<i class="fa fa-'.$bgIcon.' text--'.$bgColor.'"></i><span>';
            $tmp['className'] = ($value->is_close) ? 'success' : 'default';

            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($events, $tmp); $tmp = [];
		}	
		return $events;
	}
    
    public function actionState($id) {
        $model = $this->findModel($id);
		$model->scenario = 'state';
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
 
            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'alert' => 'Stan projektu został zmieniony', 'id' => $model->id, 'table' => '#table-projects', );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formState', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formState', ['model' => $model, 'modal' => true]) ;	
		}
	}
    
    public function actionClose($id) {
        $model = $this->findModel($id);
        if($model->id_dict_case_status_fk < 4) $close = false; else $close = true;
        if(!$close) {
            $model->scenario = 'state';
            $model->user_action = 'close';
            $model->id_dict_case_status_fk = 4;
        } else {
            $model->scenario = 'state';
            $model->user_action = 'result';
        }
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$close) {
                $model->close_date = date('Y-m-d H:i:s');
                $model->close_user = \Yii::$app->user->id;
            }
            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'alert' => 'Projekt został zamknięty', 'id'=>$model->id, 'action' => 'matterStatus', 'table' => '#table-matters',
							 'html' => $this->renderAjax('_formState', ['model' => $model, 'modal' => false, 'close' => $close]) );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formClose', ['model' => $model, 'close' => $close]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formClose', ['model' => $model, 'close' => $close]) ;	
		}
	}
    
    public function actionArchive($id) {
        $model = $this->findModel($id);
		$model->scenario = 'state';
		$model->user_action = 'close';
		$model->id_dict_case_status_fk = 7;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Projekt został przeniesiony do archiwum', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formArchive', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formArchive', ['model' => $model]) ;	
		}
	}
    
    public function actionOffers($id) {
		//$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['Offer'])) { 
            $params = $_GET['Offer']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  
			
		$sortColumn = 'o.created_at';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.name as name','o.created_at', 'o.state', "concat_ws(' ', ce.lastname, ce.firstname) as empname", "concat_ws(' ', u.lastname, u.firstname) as creator",
                      "concat_ws(' ', cu.lastname, cu.firstname) as contact", 'o.id as amount_net', 'p.name as pname', 'p.id as pid', 'c.name as cname', 'c.id as cid'])
            ->from('{{%offer}} as o')
            ->join('JOIN', '{{%customer}} as c', 'o.id_customer_fk=c.id')
            ->join('JOIN', '{{%user}} as u', 'o.created_by=u.id')
            ->join('LEFT JOIN', '{{%company_employee}} as ce', 'o.id_employee_fk=ce.id')
            ->join('LEFT JOIN', '{{%customer_person}} as cu', 'o.id_contact_fk=cu.id')
            ->join('LEFT JOIN', '{{%cal_case}} as p', 'o.id_project_fk=p.id')
            ->where( ['o.status' => 1, 'o.id_project_fk' => $id] );
            //->groupby(["i.id", "system_no", "o.id", "o.name", "o.id_customer_fk", "c.name"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
        $presentationAll = \backend\Modules\Offer\models\Offer::Viewer();
		foreach($rows as $key=>$value) {
            $value['state'] = ($value['state']) ? $value['state'] : 1;
			$presentation = $presentationAll[$value['state']];
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/sale/offer/view', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd oferty').'" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
           /* $tmp['symbol'] = $value['system_no'].'<br /><small><a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a></small>'
                            .(($value['cid'] != $id) ? '<br /><small><a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" class="text--pink">'.$value['cname'].'<a></small>': '');*/
			$tmp['name'] = $value['name'];
            $tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['cname'].'</a>';
            if($value['pid'])
                $tmp['name'] .= '<br /><small class="text--purple"><a href="'.Url::to(['/task/project/view', 'id' => $value['pid']]).'">'.$value['pname'].'</a></small>';
            $tmp['employee'] = $value['empname'];
            $tmp['contact'] = $value['contact'];
            $tmp['created'] = $value['created_at'].'<br /><small>'.$value['creator'].'</small>';
            $tmp['amount_net'] = number_format ($value['amount_net'], 2, "," ,  " ");
            $tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
}
