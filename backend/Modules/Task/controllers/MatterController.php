<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * MatterController implements the CRUD actions for CalCase model.
 */
class MatterController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalCaseSearch();
        $searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.matters'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render(((Yii::$app->params['env'] == 'dev') ? 'indexnew' : 'index'), [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            \Yii::$app->session->set('search.matters', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
                //array_push($where, "json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk < 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4 and id_dict_case_status_fk != 6");
        }
		
		if(isset($_GET['type']) && $_GET['type'] == 'merge') {
			array_push($where, "id_dict_case_status_fk != 6");
		}
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'id_parent_fk', 'm.status as status', 'c.name as cname', 'c.id as cid'])
            ->from('{{%cal_case}} as m')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($rows as $key=>$value) {
			if($value['id_dict_case_status_fk'] == 6) $value['id'] = $value['id_parent_fk'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.$value['cname'].'</a>';            
            $tmp['sname'] = $value['name'];
			$tmp['name'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
            $tmp['status'] = '<label class="label '.$colors[$value['id_dict_case_status_fk']].'">'.$status[$value['id_dict_case_status_fk']].'</label>';
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" class="btn btn-sm bg-teal" title="Pobierz wszystkie dokumenty"><i class="fa fa-download"></i></a>';
                $tmp['actions'] .= ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-matters"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
	public function actionDatanew() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            \Yii::$app->session->set('search.matters', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
                //array_push($where, "json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk != 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
            
            if(isset($params['id_dict_case_type_fk']) && !empty($params['id_dict_case_type_fk']) ) {
				array_push($where, "m.id_dict_case_type_fk = ".$params['id_dict_case_type_fk']);
            }
            
			if(isset($params['customer_role']) && !empty($params['customer_role']) ) {
				array_push($where, "m.customer_role = ".$params['customer_role']);
            }
            
			if(isset($params['id_opposite_side_fk']) && !empty($params['id_opposite_side_fk']) ) {
				array_push($where, "m.id_opposite_side_fk = ".$params['id_opposite_side_fk']);
            }
            if(isset($params['a_instance_sygn']) && !empty($params['a_instance_sygn']) ) {
				//array_push($where, "m.id in (select id_case_fk from {{%cal_case_instance}} where instance_sygn like '%".$params['a_instance_sygn']."%')");
				array_push($where, "lower(m.authority_carrying) like '%".strtolower( addslashes($params['a_instance_sygn']) )."%'");
            }
        } else {
            array_push($where, "id_dict_case_status_fk == 1");
        }
		
		if(isset($_GET['type']) && $_GET['type'] == 'merge') {
			array_push($where, "id_dict_case_status_fk != 6");
		}
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%cal_case}} as m')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['m.id as id', 'm.name as name', 'm.show_client', 'no_label', 'm.subname as subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk', 'id_dict_case_result_fk',
					  'id_parent_fk', 'm.status as status', 'c.name as cname', 'c.symbol as csymbol', 'c.id as cid', 'authority_carrying as sygns', "concat_ws(' ', e.lastname, e.firstname) as lemployee",
					  //"(select concat_ws(',', instance_sygn) from {{%cal_case_instance}} where id_case_fk = m.id and status = 1) as sygns"
					  'os.name as osname', 'os.symbol as ossymbol', 'os.id as osid'/*"GROUP_CONCAT(ci.instance_sygn) as sygns"*/
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
		$query->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk');
        $query->join(' LEFT JOIN', '{{%company_employee}} as e', 'e.id = m.id_employee_leading_fk')
			  //->join(' LEFT JOIN', '{{%cal_case_instance}} as ci', 'm.id = ci.id_case_fk')
			  ->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = m.id_opposite_side_fk');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		$status = \backend\Modules\Task\models\CalCase::listStatus('advanced');
        $roles = \backend\Modules\Task\models\CalCase::listRoles(true);
		$types = \backend\Modules\Task\models\CalCase::listTypes(5);
        $states = \backend\Modules\Task\models\CalCase::listTypes(18);
        $results = \backend\Modules\Task\models\CalCase::listTypes(14);
        
		$presentationAll = CalCase::Viewer();
		foreach($rows as $key=>$value) {
			//$authority_carrying = \yii\helpers\Json::decode($value['authority_carrying']);
			//$model->authority_carrying_name = isset($authority_carrying['name']) ? $authority_carrying['name'] : 'brak danych'; 
			//$model->authority_carrying_contact = isset($authority_carrying['contact']) ? $authority_carrying['contact'] : 'brak danych'; 
			//$model->authority_carrying_sygn = isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : 'brak danych'; 
			
			if($value['id_dict_case_status_fk'] == 6) $value['id'] = $value['id_parent_fk'];
			if($value['id_dict_case_status_fk'] < 4) $value['id_dict_case_status_fk'] = 1;
			$presentation = $presentationAll[$value['id_dict_case_status_fk']];
			
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.(($value['csymbol']) ? $value['csymbol'] : $value['cname']).'</a>'; 
			$tmp['role'] = '<span class="label">'. ((isset($roles[$value['customer_role']])) ? $roles[$value['customer_role']] : '') .'</span>';
            $tmp['employee'] = $value['lemployee'];
			$tmp['side'] = '<a href="'.Url::to(['/crm/side/view', 'id'=>$value['osid']]).'" >'.(($value['ossymbol']) ? $value['ossymbol'] : $value['osname']).'</a>';           
            $tmp['sname'] = $value['name'];
			$tmp['name'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
            $tmp['label'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['no_label'].'</a>';
			$tmp['label'] .= ($value['subname']) ? '<br /><small class="text--grey">'.$value['subname'].'</small>' : '';
            $tmp['sygn'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['sygns'].'</a>'; //isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : '';
            $value['id_dict_case_status_fk'] = ($value['id_dict_case_status_fk'] == 2 || $value['id_dict_case_status_fk'] == 3) ? 1 : $value['id_dict_case_status_fk'];
			$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'"><i class="fa fa-'.$presentation['icon'].'"></i></label>';
            if((count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 )) {
                $tmp['show'] = '<a class="inlineConfirm" href="'.Url::to(['/task/matter/sclient', 'id' => $value['id']]).'" data-table="#table-matters" title="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><i class="fa fa-eye'.((!$value['show_client'])?'-slash':'').' text--'.((!$value['show_client'])?'yellow':'blue').'"></i></a>';
            } else {
                $tmp['show'] = ($value['show_client']) ? '<i class="fa fa-eye text--blue" data-toggle="tooltip" data-title="Widoczne dla klienta"></i>' : '<i class="fa fa-eye-slash text--yellow" data-toggle="tooltip" data-title="Ukryte przed klientem"></i>';
            }
            if($value['id_dict_case_status_fk'] == 1) {
                $stage = ($value['id_dict_case_category_fk']) ? '<small class="text--purple"><b>'.(($value['id_dict_case_category_fk']) ? $states[$value['id_dict_case_category_fk']] : ' ').'</b></small>' : '<small class="text--edit">ustaw</span>';
                $tmp['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/state', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw bieżący stan sprawy">'.$stage.'</a>' : '';
            } else if($value['id_dict_case_status_fk'] != 6) {
                $stage = ($value['id_dict_case_result_fk']) ? '<small class="text--green"><b>'.(($value['id_dict_case_result_fk']) ? $results[$value['id_dict_case_result_fk']] : ' ').'</b></small>' : '<small class="text--edit">ustaw rezultat</span>';
                $tmp['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/close', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw rezultat sprawy">'.$stage.'</a>' : '';            
            }
            
            if($value['id_dict_case_type_fk']) {
				$type = '<small class="text--purple"><b>'.(($value['id_dict_case_type_fk']) ? $types[$value['id_dict_case_type_fk']] : ' ').'</b></small>';
			} else {
				$type = '<small class="text--edit">ustaw</span>';
			}
            $tmp['type'] = '<a href="'.Url::to(['/task/matter/type', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw typ sprawy">'.$type.'</a>';
			/*$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" class="btn btn-sm bg-teal" title="Pobierz wszystkie dokumenty"><i class="fa fa-download"></i></a>';
                $tmp['actions'] .= ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-matters"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
            if($value['id_dict_case_status_fk'] == 6) {
                $tmp['actions'] = ' ';
                $tmp['stage'] = ' ';
                $tmp['type'] = ' ';
            } else {
                $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/matter/update', 'id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-matters" href="'.Url::to(['/task/matter/delete', 'id' => $value['id']]).'" data-label="Usuń" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 && $value['id_dict_case_status_fk'] == 1) ? '<li class="update" data-table="#table-matters" href="'.Url::to(['/task/matter/close', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check text--green\'></i>'.Yii::t('app', 'Zamknij sprawę').'" title="'.Yii::t('app', 'Zamknij sprawę').'"><a href="#"><i class="fa fa-check text--green"></i>&nbsp;Zamknij</a></li>' : '')
                                        . '<li><a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" title="Pobierz wszystkie dokumenty"><i class="fa fa-download text--teal"></i>&nbsp;Pobierz dokumenty</a></li>'
                                        //. ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li class="inline" href="'.Url::to(['/task/matter/sclient', 'id' => $value['id']]).'" data-table="#table-matters" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><a href="#" ><i class="fa fa-eye'.(($value['show_client'])?'-slash':'').' text--'.(($value['show_client'])?'yellow':'blue').'"></i>&nbsp;'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'</a></li>' : '')
           
                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
            }
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
	
    /**
     * Displays a single CalCase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej sprawy.');
                }
            }
        }
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        if($model->id_parent_fk && $model->id_dict_case_status_fk == 6) {
            return  $this->render('scale', [  'model' => $model, ]) ;	
        }
			if(Yii::$app->params['env'] != 'dev') {
			$authority_carrying = \yii\helpers\Json::decode($model->authority_carrying);
			$model->authority_carrying_name = isset($authority_carrying['name']) ? $authority_carrying['name'] : 'brak danych'; 
			$model->authority_carrying_contact = isset($authority_carrying['contact']) ? $authority_carrying['contact'] : 'brak danych'; 
			$model->authority_carrying_sygn = isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : 'brak danych'; 
			if( $temp = \backend\Modules\Correspondence\models\CorrespondenceAddress::findOne($model->authority_carrying_name) )
				$model->authority_carrying_name = $temp->name;
			else 
				$model->authority_carrying_name = ($model->authority_carrying_name != 'brak danych' && $model->authority_carrying_name  != '') ? '<span class="text--pink">'.$model->authority_carrying_name.'</span> <small>[proszę uzupełnić dane]</small>' : 'brak danych';
			$model->authority_carrying = '<ul>'
											.'<li>Nazwa:     <b>'.$model->authority_carrying_name.'</b></li>'
											.'<li>Kontakt:   <b>'.$model->authority_carrying_contact.'</b></li>'
											.'<li>Sygn. akt: <b>'.$model->authority_carrying_sygn.'</b></li>'
										.'</ul>';
        
			$authority_supervisory = \yii\helpers\Json::decode($model->authority_supervisory);
			$model->authority_supervisory_name = isset($authority_supervisory['name']) ? $authority_supervisory['name'] : 'brak danych'; 
			$model->authority_supervisory_contact = isset($authority_supervisory['contact']) ? $authority_supervisory['contact'] : 'brak danych'; 
			$model->authority_supervisory_sygn = isset($authority_supervisory['sygn']) ? $authority_supervisory['sygn'] : 'brak danych'; 
			if( $temp = \backend\Modules\Correspondence\models\CorrespondenceAddress::findOne($model->authority_supervisory_name) )
				$model->authority_supervisory_name = $temp->name;
			else 
				$model->authority_supervisory_name = ($model->authority_supervisory_name != 'brak danych' && $model->authority_supervisory_name  != '') ? '<span class="text--pink">'.$model->authority_supervisory_name.'</span> <small>[proszę uzupełnić dane]</small>' : 'brak danych';
			
			$model->authority_supervisory = '<ul>'
											.'<li>Nazwa:     <b>'.$model->authority_supervisory_name.'</b></li>'
											.'<li>Kontakt:   <b>'.$model->authority_supervisory_contact.'</b></li>'
											.'<li>Sygn. akt: <b>'.$model->authority_supervisory_sygn.'</b></li>'
										.'</ul>';
        }                            
        $departmentsList = '<ul>';  $filesList = '';  $employeesList = '';  $correspondenceList = '';
        foreach($model->departments as $key => $department) {
            $departmentsList .= '<li>'.$department->department['name'].'</li>';
        }
        $departmentsList .= '</ul>';
        $model->departments_list = $departmentsList;
        
        return $this->render( ( (Yii::$app->params['env'] == 'dev') ? 'viewnew' : 'view'), [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }

    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {	
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        if( !isset($_POST['CalCase']['id']) || empty($_POST['CalCase']['id']) ) {
            $model = new CalCase();
            $model->type_fk = 1;
            $model->name = 'Wersja robocza '.time();
            $model->status = -2;
            $model->save();
            $model->name = '';
        } else {
            $model = CalCase::findOne($_POST['CalCase']['id']);
        }
        
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $model->id_dict_case_status_fk = 1;
        $model->departments_list = [];
		$model->id_company_branch_fk = $this->module->params['employeeBranch']; 
        $departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']);
        $specialsDepartments = \backend\Modules\Company\models\CompanyDepartment::getSpecials();
        if($specialsDepartments) {
            foreach($specialsDepartments as $key => $item) {
                array_push($departments, $item->id);
                array_push($model->departments_list, $item->id);
            }
        }
	
		$status = CalCase::listStatus('normal');
        $lists = ['departments' => $departments, 'employees' => [], 'contacts' => []];
        
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->user_action = 'create';
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) ? count($_POST['CalCase']['departments_list']) : '';         
            
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            }     
    
			if($model->validate() && $model->save()) {
				if(isset($_POST['CalCase']['departments_list']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                        $model_cd = new CaseDepartment();
                        $model_cd->id_case_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} 
				
				if(isset($_POST['employees']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('employees') as $key=>$value) {
                        $model_ce = new CaseEmployee();
                        $model_ce->id_case_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
				} 
                
                /*$specialEmployees = [];
                $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('status = 1 and id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
                foreach($specialEmployeesSql as $key => $value) {
                    $model_ce = new CaseEmployee();
                    $model_ce->id_case_fk = $model->id;
                    $model_ce->id_employee_fk = $value->id_employee_fk;
                    $model_ce->is_show = 0;
                    $model_ce->save();
                }*/
				
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                $model->status = -2;
                //return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]), 'errors' => $model->getErrors() );	
                //var_dump($model->departments_list);exit;
                $lists = $this->lists($model->id_customer_fk, $model->departments_list);
                
                return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'create'), [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]) ;	
			}		
		} else {
			return  $this->render(((Yii::$app->params['env'] == 'dev') ? 'createnew' : 'create'), [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]) ;	
		}
	}

    /**
     * Updates an existing CalCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)  {
        if( count(array_intersect(["caseEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        $customerTemp = $model->id_customer_fk;
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej sprawy.');
                }
            }
        }
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $departments = []; $employees = [];
		$status = CalCase::listStatus('normal');
        $grants = $this->module->params['grants'];
    
        $model->departments_list = [];
        foreach($model->departments as $key => $item) {
           // array_push($departments, $item->id_department_fk);
            array_push($model->departments_list, $item->id_department_fk);
           // array_push($model->departments_rel, $item->id_department_fk);
        }
        foreach($model->employees as $key => $item) {
            array_push($employees, $item['id_employee_fk']);
        }
        
		if(Yii::$app->params['env'] != 'dev') {   
			$authority_carrying = \yii\helpers\Json::decode($model->authority_carrying);
			$model->authority_carrying_name = isset($authority_carrying['name']) ? $authority_carrying['name'] : ''; 
			$model->authority_carrying_contact = isset($authority_carrying['contact']) ? $authority_carrying['contact'] : ''; 
			$model->authority_carrying_sygn = isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : ''; 
			
			$authority_supervisory = \yii\helpers\Json::decode($model->authority_supervisory);
			$model->authority_supervisory_name = isset($authority_supervisory['name']) ? $authority_supervisory['name'] : ''; 
			$model->authority_supervisory_contact = isset($authority_supervisory['contact']) ? $authority_supervisory['contact'] : ''; 
			$model->authority_supervisory_sygn = isset($authority_supervisory['sygn']) ? $authority_supervisory['sygn'] : ''; 
        }
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($customerTemp == $model->id_customer_fk) $model->is_ignore = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list']) ) ? count($_POST['CalCase']['departments_list']) : ''; 
           /* if(isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) {
                foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                    array_push($model->departments_rel, $value);
                    array_push($model->departments_list, $value);
                }
            } */
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($model->employees_rel, $value);
                    array_push($employees, $value);
                }
            }
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);

			if($model->validate() && $model->save()) {			

				return array('success' => true,  'action' => false, 'alert' => 'Sprawa <b>'.$model->name.'</b> została zaktualizowana' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind']]), 'errors' => $model->getErrors() );	
			}		
		} else {
			if(!$model->departments_list) $model->departments_list = [0 => 0];
            $lists = $this->lists($model->id_customer_fk, $model->departments_list);
           
            return  $this->render( ((Yii::$app->params['env'] == 'dev') ? 'updatenew' : 'update'), [
                        'model' => $model, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'employeeKind' => $this->module->params['employeeKind'], 'grants' => $this->module->params['grants']
                    ]) ;	
		}
    }

    /**
     * Deletes an existing CalCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)  {
        //$this->findModel($id)->delete();
        if( count(array_intersect(["caseDelete", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalCase::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $events = CalTask::find()->where(['status' => 1, 'id_case_fk' => $model->id])->count();
        //$correspondence = Correspondence::find()->where(['id_case_fk' => $model->id])->andWhere('status >= 1')->count();
        $correspondence = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['status' => 1, 'id_case_fk' => $model->id])->andWhere('id_task_fk = -1')->andWhere('id_correspondence_fk in (select id from {{%correspondence}} where status = 1)')->count();
        
        if($events != 0 || $correspondence != 0) {
            if(!Yii::$app->request->isAjax) {
				if($events > 0)
                    Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Sprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisane zdarzenia!')  );
                if($correspondence > 0)
                    Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Sprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisaną korespondencję!')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				if($events > 0)
                    return array('success' => false, 'alert' => 'Sprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisane zdarzenia!<br />', 'id' => $id, 'table' => '#table-matters');	
                if($correspondence > 0)
                    return array('success' => false, 'alert' => 'Sprawa <b>'.$model->name.'</b> nie może zostać usunięta, ponieważ ma przypisaną korespondencję!<br />', 'id' => $id, 'table' => '#table-matters');	
                
			}
        }
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Sprawa <b>'.$model->name.'</b> została usunięta')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Sprawa <b>'.$model->name.'</b> została usunięta.<br/>', 'id' => $id, 'table' => '#table-matters');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Sprawa <b>'.$model->name.'</b> nie została usunięta')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Sprawa <b>'.$model->name.'</b> nie została usunięta.<br />', 'id' => $id, 'table' => '#table-matters');	
			}
        }
    }
    
    public function actionStatus($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        $model->scenario = CalCase::SCENARIO_STATUS;
        if(isset($_GET['status']) && !empty($_GET['status']) && in_array($_GET['status'], [1,2,3,4])) {
            $model->id_dict_case_status_fk = ($_GET['status']);
            $model->user_action = 'status';

            $model->save();
            
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app',  'Sprawa przeszła w status <b>'.$model->statusname.'</b>' ) );
        } else {
            if($model->id_dict_case_status_fk < 4) {
                $model->id_dict_case_status_fk = ($model->id_dict_case_status_fk+1);
                $model->user_action = 'status';

                $model->save();
                
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app',  'Sprawa przeszła w status <b>'.$model->statusname.'</b>' ) );
            } else {
                Yii::$app->getSession()->setFlash( 'warning', Yii::t('app', 'Sprawa jest już zamknięta.')  );
            }
        }       
        return $this->redirect(['update', 'id' => $model->id]);
    }
    
    public function actionDeleteajax($id)   {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'table' => '#table-matters' );	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'table' => '#table-matters' );	
        }
       // return $this->redirect(['index']);
    }

    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalCase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function lists($id, $departmentsList) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        if(!$departmentsList) $departmentsList = [];               
        $model = \backend\Modules\Crm\models\Customer::findOne($id);
        $departments = []; $ids = $departmentsList; array_push($ids, 0); $employees = []; $contacts = [];
        $checked = 'checked';
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        $specialDepartmens = [];
        $specialDepartmensSql = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartmensSql as $key => $value) {
            array_push($specialDepartmens, $value->id);
        }
        
        foreach( $departmentsList as $key => $value ) {
            if( in_array($value, $specialDepartmens) ) {
                unset($ids[$key]);
            }
        }
        $ids = (isset($ids) && !empty($ids)) ? $ids : [ 0 => 0 ];
        
        if($employee) {
            if($employee->is_admin || $admin) 
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
            else
                $employeesData = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('status = 1 and (id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();

            foreach($employeesData as $key => $item) {
                $employees[$item->id] = $item->fullname;                    
            }
        }    
        $list = '';
        
        $cases = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($cases as $key => $case) {
            //$list .= '<option value="'.$case->id.'">'.$case->fullname.'</option>';
            $contacts[$case->id] = $case->fullname;
        }     
        return ['contacts' => $contacts, 'departments' => $departments, 'employees' => $employees];
    }
    
    public function actionEvents($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $events = []; $fields = []; $tmp = [];
        $status = CalTask::listStatus();
        $categories = CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		$model = $this->findModel($id);
		$eventsData = $model->events;
	
		foreach($eventsData as $key=>$value) {
			if($value->type_fk == 1) {
                $tmp['name'] = '<a href="'.Url::to(['/task/case/view', 'id' => $value->id]).'" title="Przejdź do karty zdarzenia">'.$value->name.'</a>';
                $type='case';
            } else {
                $tmp['name'] = '<a href="'.Url::to(['/task/event/view', 'id' => $value->id]).'" title="Przejdź do karty zdarzenia">'.$value->name.'</a>';
                $type="event";
            }
            
            $actionColumn = '<div class="edit-btn-group">';
                //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
                $actionColumn .= '<a href="'.Url::to(['/task/event/showajax', 'id' => $value->id]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
                //$actionColumn .= '<a href="'.Url::to(['/task/'.$type.'/update', 'id' => $value->id]).'" class="btn btn-xs btn-default  "  ><i class="fa fa-pencil" title="'.Yii::t('app', 'Edit').'"></i></a>';

                //$actionColumn .= '<a href="'.Url::to(['/task/event/deleteajax', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-events" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
        
            $type = ($value->type_fk == 1) ? 'case' : 'event';
			$tmp['ename'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['delay'] = $value['delay'];
            $tmp['className'] = ( ( $value['id_dict_task_status_fk'] == 1 && $value->delay > 1 && $value->type_fk == 2)  ) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2  && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['etype'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Rozprawa</span>' : '<span class="label bg-teal">Zadanie</span>' ;
            $tmp['deadline'] = $value['date_to'];
            $tmp['term'] = $value['event_date'].' '.( ( !empty($value['event_time']) && $value['event_time'] != '00:00' ) ? $value['event_time']: '' );
            $tmp['client'] = $value['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['estatus'] = isset($status[$value->id_dict_task_status_fk]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value->id_dict_task_status_fk].'<span>' : '';
            $tmp['category'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';

            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($events, $tmp); $tmp = [];
		}	
		return $events;
	}
       
    public function actionViewevent($id)  {
        $model = \backend\Modules\Task\models\CalTask::findOne($id);
        $departmentsList = '';
        $filesList = '';
        $employeesList = '';
        $grants = $this->module->params['grants'];
        
        return $this->renderAjax('_viewEvent', [
            'model' => $model,  'edit' => isset($grants['caseEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }
    
    public function actionDeleteevent($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model= \backend\Modules\Task\models\CalTask::findOne($id);
        $model->scenario =  \backend\Modules\Task\models\CalTask::SCENARIO_DELETE;
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Zadanie <b>'.$model->name.'</b> zostało usunięte', 'id' => $id, 'table' => "#table-events");	
        } else {
            return array('success' => false, 'alert' => 'Zadanie <b>'.$model->name.'</b> nie zostało usunięte', 'id' => $id, 'table' => "#table-events");	
        }
    }
    
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sprawy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
			->setCellValue('B3', 'Nazwa')
			->setCellValue('C3', 'Status')
			->setCellValue('D3', 'Pracownicy');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            /*if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                 $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }*/
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk < 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4 and id_dict_case_status_fk != 6");
        }
        
        $fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'c.name as cname', 'c.id as cid'])
            ->from('{{%cal_case}} as m')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, isset($status[$record['id_dict_case_status_fk']]) ? $status[$record['id_dict_case_status_fk']] : 'nieznany'); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
                $employees = '';
                foreach($case->employees as $key => $employee) {
                    $employees .= $employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Sprawy');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Sprawy'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
    
    public function actionEmployees($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $set = CalCase::findOne($id);
        $list = '';
        $instances = '<option>- wybierz -</option>';
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();   
        /*$admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();*/
            
        $specialEmployees = [];
        if(isset($_GET['type']) && $_GET['type'] == 'case' ) {
            $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('status = 1 and id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
            foreach($specialEmployeesSql as $key => $value) {
                array_push($specialEmployees, $value->id_employee_fk);
            }
        }
        
        $managers = [];
        $managersSql = \backend\Modules\Company\models\CompanyDepartment::find()->all();
        foreach($managersSql as $key => $value) {
            array_push($managers, $value->id_employee_manager_fk);
        }
        
        $departments = [0 => 0]; $employeesCase = [0 => 0];$types = '';
        if($set) {
            foreach($set->departments as $key => $value) {
                array_push($departments, $value->id_department_fk);
            } 
            foreach($set->employees as $key => $value) {
                array_push($employeesCase, $value['id_employee_fk']);
            }
        }
        
        $dicts = \backend\Modules\Task\models\CalTask::listTypes( (isset($_GET['type']) && $_GET['type'] == 'event' ) ? 8 : 7 );
        foreach($dicts as $key => $value) {
            $types .= '<option value="'.$key.'">'.$value.'</option>';
        }
      
       /*  if(isset($_GET['type']) && $_GET['type'] == 'case' ) {
           if($this->module->params['isSpecial'] == 1 || $this->module->params['isAdmin'] == 1)
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where all_employee != 1 and ed.status = 1 and id_department_fk  in ('.implode(',', $departments).') )')->orderby('lastname')->all();
            else
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('(id_dict_employee_kind_fk < '.$this->module->params['employeeKind'].' or id = '.$this->module->params['employeeId'].')')->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where all_employee != 1 and ed.status = 1 and id_department_fk  in ('.implode(',', $departments).') )')->orderby('lastname')->all();
        } else {
            if($this->module->params['isSpecial'] == 1 || $this->module->params['isAdmin'] == 1)
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where ed.status = 1 and id_department_fk  in ('.implode(',', $departments).') )')->orderby('lastname')->all();
            else
                $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where('(id_dict_employee_kind_fk < '.$this->module->params['employeeKind'].' or id = '.$this->module->params['employeeId'].')')->andWhere('id in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} cd on cd.id=ed.id_department_fk where ed.status = 1 and id_department_fk  in ('.implode(',', $departments).') )')->orderby('lastname')->all();       
        }*/
        
        if(isset($_GET['type']) && $_GET['type'] == 'case' ) {
            $employees = ($set) ? $set->employees : [];
        } else {
            if($set) {
                $sql = "select distinct id_employee_fk, fullname from ( "
                    . " select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname"
                    . " from {{%case_employee}} ce join {{%company_employee}} e on ce.id_employee_fk = e.id"
                    . " where ce.status = 1 and e.status = 1 and is_show = 1 and id_case_fk = ".$set->id
                    . " union "
                    . " select id_employee_fk, concat_ws(' ',lastname, firstname) as fullname"
                    . " from {{%company_employee}} ce join {{%employee_department}} ed on ed.id_employee_fk = ce.id "
                    . " where ce.status = 1 and id_department_fk in (select id from {{%company_department}} where all_employee = 1)"
                    . " ) t order by 2 ";
                $employees = Yii::$app->db->createCommand($sql)->queryAll();
            } else {
                $employees = [];
            }
        }
        foreach($employees as $key => $model) {
            $checked = ''; $disabled = '';
            if($model['id_employee_fk'] == $this->module->params['employeeId'] || in_array($model['id_employee_fk'], $managers) || in_array($model['id_employee_fk'], $employeesCase) )  $checked = ' checked ';
            //if(!in_array($model->id, $this->module->params['employees']) &&  $this->module->params['employeeKind'] != 100 && $admin == 0) $disabled = ' disabled="disabled" ';
            $list .= '<li><input id="d'.$model['id_employee_fk'].'" class="checkbox_employee" type="checkbox" name="employees[]" value="'.$model['id_employee_fk'].'" '.$checked . $disabled.'>'
                    .'<label for="d'.$model['id_employee_fk'].'" class="'. ( (in_array($model['id_employee_fk'], $managers)) ? "text--blue" : "text--black" ) .'">'.$model['fullname'].'</label></li>';
        }
        
        if(!$list)
            $list .= '<li>brak przypisanych do sprawy pracowników</li>';
        
        if($set) {    
            $instances .= \backend\Modules\Task\models\CalCaseInstance::buildSelectList( $set->id );
            /*foreach($instancesData as $key => $value) {
                $instances .= '<option value="'.$value->id.'">'.$value->fullname.'</option>';
            }*/
        }
        
        return ['list' => $list, 'departments' => $departments, 'types' => $types, 'instances' => $instances];
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {		
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 1;
            $note->id_case_fk = $model->id;
            $note->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                if(!$modelArch->save()) var_dump($modelArch->getErrors());
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
    }
    
    public function actionCreateajax($id) {
        $model = new CalCase();
        $model->scenario = CalCase::SCENARIO_MODIFY;
        $customerId = CustomHelpers::decode($id);
       // $customerId = ( isset($_GET['id']) ) ? $_GET['id'] : null;
        $model->id_customer_fk = $customerId;
        $model->id_dict_case_status_fk = 1;
        $model->status = 1;
        $model->type_fk = 1;
        if( isset($_GET['cid']) && !empty($_GET['cid']) )
            $model->id_customer_fk = $_GET['cid'];
            
        $specialsDepartments = \backend\Modules\Company\models\CompanyDepartment::getSpecials();
        $model->departments_list = [];
        if($specialsDepartments) {
            foreach($specialsDepartments as $key => $item) {
                array_push($model->departments_list, $item->id);
            }
        }
   
        $grants = $this->module->params['grants'];
        $employees = [];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $model->id_departments_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) ? count($_POST['CalCase']['departments_list']) : '';  
            
            if(isset($_POST['employees']) ) {
                foreach(Yii::$app->request->post('employees') as $key=>$value) {
                    array_push($employees, $value);
                }
            }  

            if($model->validate() && $model->save()) {                      
                if(isset($_POST['CalCase']['departments_list']) ) {
                    //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                        $model_cd = new CaseDepartment();
                        $model_cd->id_case_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
                } 
                
                if(isset($_POST['employees']) ) {
                    //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach(Yii::$app->request->post('employees') as $key=>$value) {
                        $model_ce = new CaseEmployee();
                        $model_ce->id_case_fk = $model->id;
                        $model_ce->id_employee_fk = $value;
                        $model_ce->save();
                    }
                } 
                /*if($model->type_fk == 1) {
                    $specialEmployees = [];
                    $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('status = 1 and id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
                    foreach($specialEmployeesSql as $key => $value) {
                        $model_ce = new CaseEmployee();
                        $model_ce->id_case_fk = $model->id;
                        $model_ce->id_employee_fk = $value->id_employee_fk;
                        $model_ce->is_show = 0;
                        $model_ce->save();
                    } 
                }*/
                return array('success' => true, 'action' => 'boxCase', 'index' =>1, 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $lists = $this->lists($model->id_customer_fk, $model->departments_list);
                return array('success' => false, 'html' => $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formAjax_new' : '_formAjax'), [  'model' => $model, 'lists' => $lists, 'employees' => $employees ]), 'errors' => $model->getErrors() );	
            }     	
		} else {
			$lists = $this->lists($model->id_customer_fk, $model->departments_list);
            return  $this->renderAjax(((Yii::$app->params['env'] == 'dev') ? '_formAjax_new' : '_formAjax'), [  'model' => $model, 'lists' => $lists, 'employees' => $employees ]) ;	
		}
	}
    
    public function actionDict($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
  
        $types = '';
        $list = 'brak danych'; $checkedEmployees = [];
        $dicts = \backend\Modules\Task\models\CalTask::listTypes( $id );
        foreach($dicts as $key => $value) {
            $types .= '<option value="'.$key.'">'.$value.'</option>';
        } 
        
        $ids = ( isset($_GET['dids'] ) && !empty($_GET['dids'])) ? $_GET['dids'] : '0';
        $specialDepartmens = [];
        $specialDepartmensData = \backend\Modules\Company\models\CompanyDepartment::getSpecials();
        foreach($specialDepartmensData as $key => $value) {
            array_push($specialDepartmens, $value->id);
        }
        if( $id == 5 ) {
            $departments = explode(',', $ids);
            foreach( $departments as $key => $value ) {
                if( in_array($value, $specialDepartmens) ) {
                    unset($departments[$key]);
                }
            }
            $ids = implode(',',$departments);
            $ids = (isset($ids) && !empty($ids)) ? $ids : '0';
        }

        $managers = \backend\Modules\Company\models\CompanyDepartment::getManagers();

        $specialEmployees = [];
        if( isset($_GET['box']) && $_GET['box'] == 1) {
            $company = \backend\Modules\Company\models\Company::findOne(1);
            $customData = \yii\helpers\Json::decode($company->custom_data);
            
            if( isset($customData['box']) ) {
                $company->box_special_emails = ($customData['box']['emails']) ? $customData['box']['emails'] : '';
                $company->box_special_employees = ($customData['box']['employees']) ? $customData['box']['employees'] : [ 0 => 0 ];
            } else {
                $company->box_special_emails = '';
                $company->box_special_employees = [ 0 => 0 ];
            }
            $specialEmployees = \backend\Modules\Company\models\CompanyDepartment::getSpecialEmployees();
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->andWhere('id in ('.implode(',', $company->box_special_employees).') or id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.$ids.'))')->orderby('lastname')->all();        
        } else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.$ids.'))')->orderby('lastname')->all();        
        }
        foreach($employees as $key => $model) {
            if($model->id == $this->module->params['employeeId'] || in_array($model->id, $managers) || in_array($model->id, $specialEmployees) || in_array($model->id, $checkedEmployees) )  $checked = 'checked'; else $checked = '';
            $list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'>'
                    .'<label for="d'.$model->id.'" class="'. ( (in_array($model->id, $managers)) ? "text--blue" : ( (in_array($model->id, $specialEmployees)) ? "text--pink" : "text--black") ) .'">'.$model->fullname.'</label></li>';
        }
   
        return ['types' => $types, 'employees' => $list];
    }
	
	public function actionInfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
  
        $case = \backend\Modules\Task\models\CalCase::findOne( $id );
            
        return ['name' => $case->name];
    }
    
    public function actionMerge()  {
        if( count(array_intersect(["caseMerge", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalCaseSearch();
        $searchModel->id_dict_case_status_fk = 5;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('merge', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants']
        ]);
    }
	
	public function actionMerging() {
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
		$model = new \backend\Modules\Task\models\CalMerge();
		$model->id_case_fk = 0;
		
		if( isset($_POST['merge']) == 1 ) {
            if( !isset($_POST['ids']) || count($_POST['ids']) < 2) {
				Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Proszę wybrać przynajmniej 2 sprawy do scalenia')  );
				return $this->redirect(['merge']);
			} else {
				$cases = CalCase::find()->where('id in ('.implode(',', $_POST['ids']).')')->all();
				$model->cases = $_POST['ids'];
			}
        } else {
            if( !isset($_POST['CalMerge']['cases']) || count($_POST['CalMerge']['cases']) < 2) {
				Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Proszę wybrać przynajmniej 2 sprawy do scalenia')  );
				return $this->redirect(['merge']);
			} else {
				$cases = CalCase::find()->where('id in ('.implode(',', $_POST['CalMerge']['cases']).')')->all();
			} 
        }
		
		$case = new CalCase();
        $case->scenario = CalCase::SCENARIO_MERGE;
        $case->id_dict_case_status_fk = 1;
		$case->id_merge_fk = 0;
		$case->id_parent_fk = 0;
        
		$departments = []; $employees = []; //array_push($employees, $this->module->params['employeeId']);
        $case->departments_list = [];
        foreach($cases as $key => $item) {
            foreach($item->departments as $i => $value) {
                array_push($departments, $value->id_department_fk);
                array_push($case->departments_list, $value->id_department_fk);
            }
            foreach($item->employees as $i => $value) {
                array_push($employees, $value['id_employee_fk']);
            }
            $case->id_customer_fk = $item->id_customer_fk;
        }
		
		$status = CalCase::listStatus('normal');
        //$lists = ['departments' => $departments, 'employees' => $employees, 'contacts' => []];
        $lists = $this->lists($case->id_customer_fk, $case->departments_list);
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost && !isset($_POST['merge']) ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->id_case_fk == 0) {
				$parent = new CalCase();
			} else {
				$parent = CalCase::findOne($model->id_case_fk);
			}
			$parent->scenario = CalCase::SCENARIO_MERGE;
			$parent->type_fk = 1;
			$parent->id_dict_case_status_fk = 3;
			
			$parent->load(Yii::$app->request->post());
			$parent->user_action = 'scale';
            $parent->id_employee_fk = isset($_POST['employees']) ? count($_POST['employees']) : '';
            $parent->id_department_fk = ( isset($_POST['CalCase']['departments_list']) && !empty($_POST['CalCase']['departments_list'])) ? count($_POST['CalCase']['departments_list']) : '';  

			$transaction = \Yii::$app->db->beginTransaction();
            try {
				if($parent->validate() && $parent->save()) {
					if($model->id_case_fk == 0) {
                        if(isset($_POST['CalCase']['departments_list']) ) {
                            //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                            foreach($_POST['CalCase']['departments_list'] as $key => $value) {
                                $model_cd = new CaseDepartment();
                                $model_cd->id_case_fk = $parent->id;
                                $model_cd->id_department_fk = $value;
                                $model_cd->save();
                            }
                        } 
                        
                        if(isset($_POST['employees']) ) {
                            //EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                            foreach(Yii::$app->request->post('employees') as $key => $value) {
                                $model_ce = new CaseEmployee();
                                $model_ce->id_case_fk = $parent->id;
                                $model_ce->id_employee_fk = $value;
                                $model_ce->save();
                            }
                        } 
                    } else {
                        $departmentsTemp = [];
                        $employeesTemp = [];
                        
                        foreach($parent->departments as $key => $value) {
                            array_push($departmentsTemp, $value->id_department_fk);
                        }
                        if(isset($_POST['CalCase']['departments_list']) ) {
                            foreach($_POST['CalCase']['departments_list'] as $key=>$value) {
                                if( !in_array($value, $departmentsTemp) ){
                                    $model_cd = new CaseDepartment();
                                    $model_cd->id_case_fk = $parent->id;
                                    $model_cd->id_department_fk = $value;
                                    $model_cd->save();
                                }
                            }
                        } 
                        CaseDepartment::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id],'id_case_fk = :case and id_department_fk not in ( '.implode(',', $_POST['CalCase']['departments_list']).')', [':case' => $parent->id]);
                        
                        foreach($parent->employees as $key => $value) {
                            array_push($employeesTemp, $value['id_department_fk']);
                        }
                        $employeesDeleted = []; array_push($employeesDeleted, 0);
                        if(isset($_POST['employees']) ) {
                            foreach($_POST['employees'] as $key=>$value) {
                                array_push($employeesDeleted, $value);
                                if( !in_array($value, $employeesTemp) ){
                                    $model_cd = new CaseEmployee();
                                    $model_cd->id_case_fk = $parent->id;
                                    $model_cd->id_employee_fk = $value;
                                    $model_cd->save();
                                }
                            }
                        } 
                        CaseEmployee::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => \Yii::$app->user->id],'id_case_fk = :case and id_employee_fk not in ( '.implode(',', $employeesDeleted).')', [':case' => $parent->id]);
                    }
					
					$model->id_case_fk = $parent->id;
                    $cases_arch = [];
                    $model->cases = implode(',', $model->cases);
					if($model->save()) {
						foreach($cases as $key => $value) {
							if($value->id != $model->id_case_fk) {
								array_push($cases_arch, $value->id);
								/*$mergeCase = CalCase::findOne($value->id);
								$mergeCase->scenario = CalCase::SCENARIO_MERGE;
								$mergeCase->id_parent_fk = $model->id_case_fk;
								$mergeCase->id_merge_fk = $model->id;
								$mergeCase->id_dict_case_status_fk = 6;
								$mergeCase->user_action = 'scale';
								$mergeCase->save();*/
                                CalTask::updateAll(['id_original_fk' => $value->id], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);
                                CalTask::updateAll(['id_case_fk' => $parent->id], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);
                                \backend\Modules\Task\models\TaskEmployee::updateAll(['id_case_fk' => $parent->id], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);
                                \backend\Modules\Correspondence\models\CorrespondenceTask::updateAll(['id_case_fk' => $parent->id, 'id_original_fk' => $value->id], 'status = 1 and id_case_fk = :case', [':case' => $value->id]);
                                \common\models\Files::updateAll(['id_original_fk' => $value->id], 'status = 1 and id_type_file_fk = 3 and id_fk = :case', [':case' => $value->id]);
                                \common\models\Files::updateAll(['id_fk' => $parent->id], 'status = 1 and id_type_file_fk = 3 and id_fk = :case', [':case' => $value->id]);
							}
						}
						CalCase::updateAll(['id_parent_fk' => $model->id_case_fk, 'id_merge_fk' => $model->id, 'id_dict_case_status_fk' => 6], 'id in ('.implode(',', $cases_arch).')', []);
					} 
                    
                    $modelArch = new \backend\Modules\Task\models\CalCaseArch();
                    $modelArch->id_case_fk = $parent->id;
                    $modelArch->user_action = 'merge';
                    $modelArch->data_change = \yii\helpers\Json::encode($cases_arch);
                    $modelArch->created_by = \Yii::$app->user->id;
                    $modelArch->created_at = date('Y-m-d H:i:s');
                    $modelArch->save();
					
					$transaction->commit();
					return $this->redirect(['view', 'id' => $parent->id]);
				} else {
					return  $this->render('merging', [  'model' => $model, 'case' => $parent, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'cases' => $cases]) ;	
				}
				
			} catch (Exception $e) {
                $transaction->rollBack();
				return  $this->render('merging', [  'model' => $model, 'case' => $parent, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'cases' => $cases]) ;	
            }	
		}		
	    return  $this->render('merging', [  'model' => $model, 'case' => $case, 'lists' => $lists, 'departments' => $departments, 'employees' => $employees, 'cases' => $cases]) ;	
	}
    
    public function actionDownload($id) {
        $model = $this->findModel($id);
        $ids = ( isset($_POST['ids']) ) ? explode(',', $_POST['ids']) : false;
        $zip = new \ZipArchive();
		//$zipname = 'uploads/matters/docs_'.$id.'.zip';
        $tempPath = 'uploads/zip/';
        $zipname = 'docs_'.$model->id.'_'.\Yii::$app->user->id.'_'.time().'.zip';
		$zip->open($tempPath.$zipname, \ZipArchive::CREATE);
        $i = 0;
		
        foreach($model->files as $key => $item) { 
            if(!$ids) {
                if($item->version_fk)
                    $file = "uploads". $item->version_fk . $item->systemname_file; 
                else
                    $file = "uploads/" . $item->systemname_file. '.' .$item->extension_file;  
                if(substr($item->title_file, -4) != $item->extension_file) $item->title_file = $item->title_file. '.' .$item->extension_file;
                
                //if(file_exists("/var/www/html/ultima/frontend/web/".$file)) { 
                    ++$i;
                    if($item->category)
                        $zip->addFile($file, $item->category['name'].'/'.$item->title_file);
                    else    
                        $zip->addFile($file, $item->title_file);
                //}
            }  else {
                if(in_array($item->id, $ids)) {
                    if($item->version_fk)
                        $file = "uploads". $item->version_fk . $item->systemname_file; 
                    else
                        $file = "uploads/" . $item->systemname_file. '.' .$item->extension_file;  
                    if(substr($item->title_file, -4) != $item->extension_file) $item->title_file = $item->title_file. '.' .$item->extension_file;
                    
                    //if(file_exists("/var/www/html/ultima/frontend/web/".$file)) { 
                        ++$i;
                        if($item->category)
                            $zip->addFile($file, $item->category['name'].'/'.$item->title_file);
                        else    
                            $zip->addFile($file, $item->title_file);
                    //}
                }
            }
        }	

        if(!$model->files || $i == 0) {
            $file = "uploads/brak_dokumentow.txt";  
            $zip->addFile($file, 'brak dokumentów');
        }	 
		$zip->close();
				
		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename='.$zipname);
		header('Content-Length: ' . filesize($tempPath.$zipname));
		
		if(!$ids) {
            readfile($tempPath.$zipname);
            exit;
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['zip' => '/'.$tempPath.$zipname];
        }
    }
    
    public function actionSides($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$sides = [];
		$tmp = [];
		
		$sql = "select cs.id, cs.type_fk, s.name as sname, dict.name as side_role, cs.note as note, s.phone, s.email"
                ." from {{%case_side}} cs join {{%customer}} s on s.id = cs.id_side_fk join {{%dictionary_value}} dict on dict.id = cs.id_dict_side_role_fk"
                ." where cs.status = 1 and id_case_fk = ".CustomHelpers::decode($id);
        $sidesData = \Yii::$app->db->createCommand($sql)->query();     
		
		foreach($sidesData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/task/matter/editmember', 'id' => $value['id']]).'" class="btn btn-xs bg-blue2 text--blue gridViewModal" data-table="#table-sides" data-form="person-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/task/matter/deletemember', 'id' => $value['id']]).'" class="btn btn-xs bg-red2 text--red remove" data-table="#table-sides"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['role'] = $value['side_role'].'<br /><span class="label '.(($value['type_fk'] == 1) ? 'bg-green">strona klienta' : 'bg-red">strona przeciwna').'</span>';
			$tmp['name'] = $value['sname'];
            $tmp['note'] = ($value['note']) ? '<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            //$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            //$tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
			$tmp['actions'] = $actionColumn;
           
			array_push($sides, $tmp); $tmp = [];
		}
		
		return $sides;
	}
    
    public function actionAddmember($id) {
        $case = $this->findModel($id);
        
        $model = new \backend\Modules\Task\models\CaseSide();
        $model->status = 1;
        $model->id_case_fk = $case->id;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Uczestnik został dodany', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSide', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formSide', ['model' => $model]) ;	
		}
	}
    
    public function actionEditmember($id) {
        $model = \backend\Modules\Task\models\CaseSide::findOne(CustomHelpers::decode($id));
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Informacje zostały zaktualizowane', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSide', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formSide', ['model' => $model]) ;	
		}
	}
    
    public function actionDeletemember($id)  {
        $model = \backend\Modules\Task\models\CaseSide::findOne(CustomHelpers::decode($id));
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Uczestnik został usunięty', 'id' => $id, 'table' => '#table-sides');	
        } else {
            return array('success' => false, 'row' => 'Uczestnik nie może zostać usunięty', 'id' => $id, 'table' => '#table-sides');	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionOrder($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->scenario = CalCase::SCENARIO_ORDER;
        $model->user_action = 'order';
       
        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            
			if($model->validate() && $model->save() ) {
                return array('success' => true,  'action' => false, 'alert' => 'Umowa została ustawiona' );	
			} else {
                return array('success' => false,  'action' => false, 'alert' => 'Umowa nie została ustawiona' );	
			}		
		} 
    }
    
    public function actionState($id) {
        $model = $this->findModel($id);
		$model->scenario = 'state';
        
        $states = \backend\Modules\Task\models\CalCase::listTypes(18);
        $results = \backend\Modules\Task\models\CalCase::listTypes(14);
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            //$model->close_date = date('Y-m-d H:i:s');
            //$model->close_user = \Yii::$app->user->id;
            if($model->validate() && $model->save()) {    
                if($model->id_dict_case_status_fk == 1) {
                    $stage = ($model->id_dict_case_category_fk) ? '<small class="text--purple"><b>'.(($model->id_dict_case_category_fk) ? $states[$model->id_dict_case_category_fk] : ' ').'</b></small>' : '<small class="text--edit">ustaw</span>';
                    $row['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/state', 'id' => $model->id]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw bieżący stan sprawy">'.$stage.'</a>' : '';
                } else if($model->id_dict_case_status_fk != 6) {
                    $stage = ($model->id_dict_case_result_fk) ? '<small class="text--green"><b>'.(($value['id_dict_case_result_fk']) ? $results[$model->id_dict_case_result_fk] : ' ').'</b></small>' : '<small class="text--edit">ustaw rezultat</span>';
                    $row['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/close', 'id' => $model->id]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw rezultat sprawy">'.$stage.'</a>' : '';            
                }
            
                return array('success' => true, 'index' => $_GET['index'], 'alert' => 'Stan sprawy został zmieniony', 'id' => $model->id, 'table' => '#table-matters', 'row' => $row, 'refresh' => 'inline', 'index' => $_GET['index'] );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formState', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formState', ['model' => $model, 'modal' => true]) ;	
		}
	}
    
    public function actionType($id) {
        $model = $this->findModel($id);
		$model->scenario = 'state';
        
        $types = \backend\Modules\Task\models\CalCase::listTypes(5);
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            //$model->close_date = date('Y-m-d H:i:s');
            //$model->close_user = \Yii::$app->user->id;
            if($model->validate() && $model->save()) {                      
                if($model->id_dict_case_type_fk) {
                    $type = '<small class="text--purple"><b>'.(($model->id_dict_case_type_fk) ? $types[$model->id_dict_case_type_fk] : ' ').'</b></small>';
                } else {
                    $type = '<small class="text--edit">ustaw</span>';
                }
                $row['type'] = '<a href="'.Url::to(['/task/matter/type', 'id' => $model->id]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw typ sprawy">'.$type.'</a>';
                
                return array('success' => true, 'alert' => 'Typ sprawy został zmieniony', 'id' => $model->id, 'table' => '#table-matters', 'row' => $row, 'refresh' => 'inline', 'index' => $_GET['index']);	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formType', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formType', ['model' => $model, 'modal' => true]) ;	
		}
	}
	
	public function actionClose($id) {
        $model = $this->findModel($id);
        if($model->id_dict_case_status_fk < 4) $close = false; else $close = true;
        
        $states = \backend\Modules\Task\models\CalCase::listTypes(18);
        $results = \backend\Modules\Task\models\CalCase::listTypes(14);
        
        if(!$close) {
            $model->scenario = 'state';
            $model->user_action = 'close';
            $model->id_dict_case_status_fk = 4;
        } else {
            $model->scenario = 'state';
            $model->user_action = 'result';
        }
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$close) {
                $model->close_date = date('Y-m-d H:i:s');
                $model->close_user = \Yii::$app->user->id;
            }
            if($model->validate() && $model->save()) {                      
                if($model->id_dict_case_status_fk == 1) {
                    $stage = ($model->id_dict_case_category_fk) ? '<small class="text--purple"><b>'.(($model->id_dict_case_category_fk) ? $states[$model->id_dict_case_category_fk] : ' ').'</b></small>' : '<small class="text--edit">ustaw</span>';
                    $row['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/state', 'id' => $model->id]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw bieżący stan sprawy">'.$stage.'</a>' : '';
                } else if($model->id_dict_case_status_fk != 6) {
                    $stage = ($model->id_dict_case_result_fk) ? '<small class="text--green"><b>'.(($value['id_dict_case_result_fk']) ? $results[$model->id_dict_case_result_fk] : ' ').'</b></small>' : '<small class="text--edit">ustaw rezultat</span>';
                    $row['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/close', 'id' => $model->id]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw rezultat sprawy">'.$stage.'</a>' : '';            
                }
            
                return array('success' => true, 'alert' => 'Sprawa została zamknięta', 'id'=>$model->id, 'action' => 'matterStatus', 'table' => '#table-matters', 'row' => $row, 'refresh' => 'inline', 'index' => $_GET['index'],
							 'html' => $this->renderAjax('_formState', ['model' => $model, 'modal' => false, 'close' => $close]) );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formClose', ['model' => $model, 'close' => $close]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formClose', ['model' => $model, 'close' => $close]) ;	
		}
	}
    
    public function actionArchive($id) {
        $model = $this->findModel($id);
		$model->scenario = 'state';
		$model->user_action = 'archive';
		$model->id_dict_case_status_fk = 7;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Sprawa została przeniesiona do archiwum', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formArchive', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            if($model->validate() && $model->save()) { 
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Sprawa została przeniesiona do archiwum')  );
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Sprawa nie została przeniesiona do archiwum!')  );
				return $this->redirect(['update', 'id' => $model->id]);
			}
			//return  $this->renderAjax('_formArchive', ['model' => $model]) ;	
		}
	}
    
    public function actionSclient($id) {
        $model = $this->findModel($id);
		$model->scenario = 'show';
		$model->user_action = 'show';
        $model->show_client = ($model->show_client) ? 0 : 1;
        $model->save();
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];
                    
        $info = '<a href="'. Url::to(['/task/matter/sclient', 'id' => $model->id]) .'" class="btn bg-'. ((!$model->show_client) ? 'yellow' : 'blue') .' btn-flat deleteConfirm" data-label="'. ((!$model->show_client) ? 'Pokaż klientowi' : 'Nie pokazuj klientowi' ).'" data-title="'. ((!$model->show_client) ? 'Pokaż klientowi' : 'Nie pokazuj klientowi') .'" data-toggle="tooltip"><i class="fa fa-'. ((!$model->show_client) ? 'eye-slash' : 'eye' ).'"></i></a>';
        
        $btnAction = '<div class="btn-group">'
                          .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                            .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                          .'</button>'
                          .'<ul class="dropdown-menu dropdown-menu-right">'
                            . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/matter/update', 'id' => $model->id]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                            . ( (count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-matters" href="'.Url::to(['/task/matter/delete', 'id' => $model->id]).'" data-label="Usuń" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                            . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 && $model->id_dict_case_status_fk == 1) ? '<li class="update" data-table="#table-matters" href="'.Url::to(['/task/matter/close', 'id' => $model->id]).'" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check text--green\'></i>'.Yii::t('app', 'Zamknij sprawę').'" title="'.Yii::t('app', 'Zamknij sprawę').'"><a href="#"><i class="fa fa-check text--green"></i>&nbsp;Zamknij</a></li>' : '')
                            . '<li><a href="'.Url::to(['/task/matter/download', 'id' => $model->id]).'" title="Pobierz wszystkie dokumenty"><i class="fa fa-download text--teal"></i>&nbsp;Pobierz dokumenty</a></li>'
                            . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li class="inline" href="'.Url::to(['/task/matter/sclient', 'id' => $model->id]).'" data-table="#table-matters" data-label="'.(($model->show_client)?'Ukryj przed klientem':'Pokaż klientowi').'"><a href="#" ><i class="fa fa-eye'.(($model->show_client)?'-slash':'').' text--'.(($model->show_client)?'yellow':'blue').'"></i>&nbsp;'.(($model->show_client)?'Ukryj przed klientem':'Pokaż klientowi').'</a></li>' : '')

                            //.'<li role="separator" class="divider"></li>'
                            //.'<li><a href="#">Separated link</a></li>'
                          .'</ul>';

        $colShow = '<a class="inlineConfirm" href="'.Url::to(['/task/matter/sclient', 'id' => $model->id]).'" data-table="#table-matters" title="'.(($model->show_client)?'Ukryj przed klientem':'Pokaż klientowi').'" data-label="'.(($model->show_client)?'Ukryj przed klientem':'Pokaż klientowi').'"><i class="fa fa-eye'.((!$model->show_client)?'-slash':'').' text--'.((!$model->show_client)?'yellow':'blue').'"></i></a>';
        $row = ['show' => $colShow];

        return array('success' => true, 'action' => 'showClient', 'alert' => 'Zmiany zostały zapisane', 'index' => $_GET['index'], 'id' => $model->id, 'info' => $info, 'table' => '#table-matters', 'refresh' => 'inline', 'row' => $row );	   
	}
    
    public function actionBrowser($id) {
        $model = $this->findModel($id);
        
        return $this->renderPartial('browser', ['model' => $model]);
    }
    
    public function actionFolders($mid, $type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $files = []; $folders = [];
        
        if($type > 0) {
            $filesData = \common\models\Files::find()->where(['status' => 1, 'id_dict_type_file_fk' => $type])
                     ->andWhere(' ( (id_fk='.$mid.' and id_type_file_fk = 3) '
                                .' or (id_type_file_fk = 5 and id_fk in (select c.id from {{%correspondence}} c join {{%correspondence_task}} ct on ct.id_correspondence_fk=c.id where ct.status = 1 and c.status = 1 and ct.id_case_fk='.$mid.' and ( (type_fk = 1 and send_by is not null and send_at is not null) or type_fk=2) ))'
                                .' or (id_type_file_fk = 4 and id_fk in (select id from {{%cal_task}} where status = 1 and id_case_fk='.$mid.')) )')->orderby('id desc')->all();
            
            foreach($filesData as $key => $item) {
                array_push($files, array(
                    "name" => str_replace('_', ' ',$item->title_file),
                    "type" => "file",
                    "path" => 'Folder osobisty',
                    "items" => [], 
                    "url" => '/files/getfile/'  .$item->id,
                    "size" => $item->size_file
                ));
            }
            $breadcrumbs = '<a href="'.Url::to(['/task/matter/folders', 'mid' => $mid, 'type' => 0]).'" class="folderUp">Powrót do katalogu głównego</a><span class="folderName">';
        } else {
            $model = CalCase::findOne($mid);
            foreach($model->browser as $key => $folder) {
                array_push($folders, array(
                    "name" => $folder['name'],
                    "type" => "folder",
                    "path" => $folder['name'],
                    "length" => $folder['items']. 'plików', 
                    "url" => Url::to(['/task/matter/folders', 'mid' => $model->id, 'type' => $folder['id']]),
                ));
            }
            $breadcrumbs = '';
        }     
     
        return  ['folders' => $folders, 'files' => $files, 'breadcrumbs' => $breadcrumbs];
    }
}
