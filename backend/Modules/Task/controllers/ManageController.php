<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\web\UploadedFile;

/**
 * ManageController implements the CRUD actions for CalCase model.
 */
class ManageController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalCase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionLabel() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if(isset($_GET['branch']) && isset($_GET['department']) && !empty($_GET['branch']) && !empty($_GET['department'])){
            $label = CalCase::getLabel($_GET['branch'], $_GET['department']);
        } else {
            $label = '';
        }
        return ['no_label' => $label];
    }
	
	public function actionLabelArch($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		
		$model = $this->findModel($id);

		$label = CalCase::getLabelArch($model->id_company_branch_fk, $model->id_department_fk);

        return ['no_label' => $label];
    }
	
	public function actionName() {
        Yii::$app->response->format = Response::FORMAT_JSON;
		
		$name = '';  $sideOrder = 1;
		$dict = \backend\Modules\Dict\models\DictionaryValue::findOne(( (isset($_GET['role']) && !empty($_GET['role'])) ? $_GET['role'] : 0));
		if($dict) {
			$customData = \yii\helpers\Json::decode($dict->custom_data);
            $dict->color = $customData['color'];
            $sideOrder = (isset($customData['sideOrder'])) ? $customData['sideOrder'] : 1;
		}
        if(isset($_GET['customer']) && !empty($_GET['customer'])){
            $customer = \backend\Modules\Crm\models\Customer::findOne($_GET['customer']);
			$name = ($customer->symbol) ? $customer->symbol : $customer->name;
        } 
		
		if(isset($_GET['side']) && !empty($_GET['side'])){
            $side = \backend\Modules\Crm\models\Customer::findOne($_GET['side']);
			
			if($sideOrder == 1)
				$name .= ' vs '. (($side->symbol) ? $side->symbol : $side->name);
			else
				$name = (($side->symbol) ? $side->symbol : $side->name) .' vs '. $name;
        }

		/*if(isset($_GET['sygn']) && !empty($_GET['sygn'])){
			$name .= ', '. $_GET['sygn'];
        } */
		
        return ['name' => $name];
    }
    
    public function actionPrint($id) {
        $model = $this->findModel($id);
        
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        
        $section = $phpWord->addSection();
        
        /*$tableStyle = array(
            'borderColor' => '006699',
            'valign' => 'center',
            'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
            'borderSize'  => 6,
        );*/
        
        //$phpWord->addTableStyle('myTable', $tableStyle);
        $phpWord->addFontStyle('hStyle', array('bold'=>true, 'italic'=>false, 'size'=>36));
        $phpWord->addFontStyle('nStyle', array('bold'=>true, 'italic'=>false, 'size'=>26));
        
        /*$table = $section->addTable('myTable');
                
        $table->addRow();
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69))->addText('1','hStyle');
        $table->addRow(); //'exactHeight' = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(9.01)
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), array('textDirection'=>\PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR) )->addText('Przewozy regionakne', 'nStyle');
        $table->addRow();
        $table->addCell()->addImage(
            'http://rigor.dev/images/logo.jpg',
            array(
                'width'         => 100,
                'marginTop'     => -1,
                'marginLeft'    => -1,
                'wrappingStyle' => 'behind'
            )
        );*/
        
        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, /*'borderColor' => '006699', */'cellMargin' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER);
        $fancyTableFirstRowStyle = array('borderBottomSize' => 8 /*, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF'*/);
        $fancyTableCellStyle = array('valign' => 'center');
        $fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
        $fancyTableFontStyle1 = array('bold' => true, 'size'=>36);
        $fancyTableFontStyle2 = array('bold' => true, 'size'=>12);
        $fancyTableFontStyle3 = array('bold' => true, 'size'=>16);
        $fancyTableFontStyle4 = array('bold' => false, 'size'=>11);
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
        $table = $section->addTable($fancyTableStyleName);
        $table->addRow(200);
        //$ClientSide = 'Warter Fuels S.A.<w:br/>-<w:br/>PKN Orlen S.A.';
        $ClientSide = $model->customer['name'];
        if($model->side) {
            $ClientSide .= '<w:br/>-<w:br/>'.$model->side['name'];
        }
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), $fancyTableCellStyle)->addText('1', $fancyTableFontStyle1, ['alignment'=>'center']);
        $table->addRow(3000);
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), $fancyTableCellBtlrStyle)->addText($ClientSide, $fancyTableFontStyle2, ['alignment'=>'center']);
        $table->addRow(100);
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), $fancyTableCellStyle)->addText($model->a_instance_sygn, $fancyTableFontStyle3, ['alignment'=>'center']);
        $table->addRow(50);
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), $fancyTableCellStyle)->addText('1/1', $fancyTableFontStyle4, ['alignment'=>'center']);
        $table->addRow();
        $table->addCell(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(4.69), $fancyTableCellStyle)->addImage(\Yii::$app->params['siw'].'/images/logo.jpg', ['alignment'=>'center']);
        /*for ($i = 1; $i <= 8; $i++) {
            $table->addRow();
            $table->addCell(2000)->addText("Cell {$i}");
            $table->addCell(2000)->addText("Cell {$i}");
            $table->addCell(2000)->addText("Cell {$i}");
            $table->addCell(2000)->addText("Cell {$i}");
            $text = (0== $i % 2) ? 'X' : '';
            $table->addCell(500)->addText($text);
        }*/
        
       /* $section = $phpWord->addSection();
        $header = array('size' => 16, 'bold' => true);
        
        $section->addPageBreak();
        $section->addText('Table with colspan and rowspan', $header);
        $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
        $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
        $table = $section->addTable('Colspan Rowspan');
        $row = $table->addRow();
        $row->addCell(null, array('vMerge' => 'restart'))->addText('A');
        $row->addCell(null, array('gridSpan' => 2, 'vMerge' => 'restart',))->addText('B');
        $row->addCell()->addText('1');
        $row = $table->addRow();
        $row->addCell(null, array('vMerge' => 'continue'));
        $row->addCell(null, array('vMerge' => 'continue','gridSpan' => 2,));
        $row->addCell()->addText('2');
        $row = $table->addRow();
        $row->addCell(null, array('vMerge' => 'continue'));
        $row->addCell()->addText('C');
        $row->addCell()->addText('D');
        $row->addCell()->addText('3');
        // 5. Nested table
        $section->addTextBreak(2);
        $section->addText('Nested table in a centered and 50% width table.', $header);
        $table = $section->addTable(array('width' => 50 * 50, 'unit' => 'pct', 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER));
        $cell = $table->addRow()->addCell();
        $cell->addText('This cell contains nested table.');
        $innerCell = $cell->addTable(array('alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER))->addRow()->addCell();
        $innerCell->addText('Inside nested table');*/



        $filename = 'Etykieta'.'_'.date("Y_m_d__His").".docx";
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

		header("Content-Type: application/msword");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');	
    }
    
    /* sides */
    public function actionSides($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$sides = [];
		$tmp = [];
		
		$sql = "select min(cs.id) as id, cs.type_fk, s.name as sname, dict.name as side_role, cs.note as note, s.phone, s.email"
                ." from {{%case_side}} cs join {{%customer}} s on s.id = cs.id_side_fk join {{%dictionary_value}} dict on dict.id = cs.id_dict_side_role_fk"
                ." where cs.status = 1 and id_case_fk = ".CustomHelpers::decode($id)
                ." group by  cs.type_fk, s.name, dict.name, cs.note, s.phone, s.email";
        $sidesData = \Yii::$app->db->createCommand($sql)->query();     
		
		foreach($sidesData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/task/manage/editmember', 'id' => $value['id']]).'" class="btn btn-xs bg-blue2 text--blue gridViewModal" data-table="#table-sides" data-form="person-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/task/manage/deletemember', 'id' => $value['id']]).'" class="btn btn-xs bg-red2 text--red remove" data-table="#table-sides"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['role'] = $value['side_role'].'<br /><span class="label '.(($value['type_fk'] == 1) ? 'bg-green">strona klienta' : 'bg-red">strona przeciwna').'</span>';
			$tmp['name'] = $value['sname'];
            $tmp['note'] = ($value['note']) ? '<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            //$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            //$tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
			$tmp['actions'] = $actionColumn;
           
			array_push($sides, $tmp); $tmp = [];
		}
		
		return $sides;
	}
    
    public function actionAddmember($id) {
        $case = $this->findModel($id);
        
        $model = new \backend\Modules\Task\models\CaseSide();
        $model->status = 1;
        $model->id_case_fk = $case->id;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Uczestnik został dodany', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSide', ['model' => $model, 'tableId' => "#table-sides"]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formSide', ['model' => $model, 'tableId' => "#table-sides"]) ;	
		}
	}
    
    public function actionAddemember($id) {
        $id = CustomHelpers::decode($id);
        $event = \backend\Modules\Task\models\CalTask::findOne($id);
        
        $model = new \backend\Modules\Task\models\CaseSide();
        $model->status = 1;
        $model->id_case_fk = $event->id_case_fk;
        $model->id_event_fk = $event->id;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Uczestnik został dodany', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSide', ['model' => $model, 'tableId' => "#table-sides-e"]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formSide', ['model' => $model, 'tableId' => "#table-sides-e"]) ;	
		}
	}
    
    public function actionEditmember($id) {
        $model = \backend\Modules\Task\models\CaseSide::findOne(CustomHelpers::decode($id));
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Informacje zostały zaktualizowane', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSide', ['model' => $model, 'tableId' => (($model->id_event_fk) ? "#table-sides-e" : "#table-sides")]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formSide', ['model' => $model, 'tableId' => (($model->id_event_fk) ? "#table-sides-e" : "#table-sides")]) ;	
		}
	}
    
    public function actionDeletemember($id)  {
        $model = \backend\Modules\Task\models\CaseSide::findOne(CustomHelpers::decode($id));
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Uczestnik został usunięty', 'id' => $id, 'table' => (($model->id_event_fk) ? "#table-sides-e" : "#table-sides"), 'action' => 'delSide');	
        } else {
            return array('success' => false, 'row' => 'Uczestnik nie może zostać usunięty', 'id' => $id, 'table' => (($model->id_event_fk) ? "#table-sides-e" : "#table-sides"));	
        }

       // return $this->redirect(['index']);
    }
    
    /* claims */
    public function actionClaims($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$claims = [];
		$tmp = [];
		
		$sql = "select c.id, c.type_fk, c.title as cname, dict.name as claim_type, c.note as note, amount, occurence_date, payment_date, debt_interest, debt_balance"
                ." from {{%case_claim}} c join {{%dictionary_value}} dict on dict.id = c.id_dict_claim_type_fk"
                ." where c.status = 1 and id_case_fk = ".CustomHelpers::decode($id);
        $claimsData = \Yii::$app->db->createCommand($sql)->query();     
		
		foreach($claimsData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/task/manage/editclaim', 'id' => $value['id']]).'" class="btn btn-xs btn-default text--blue gridViewModal" data-table="#table-sides" data-form="person-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/task/manage/deleteclaim', 'id' => $value['id']]).'" class="btn btn-xs btn-default text--red remove" data-table="#table-sides"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['type'] = $value['claim_type'].'<br /><span class="label '.(($value['type_fk'] == 1) ? 'bg-blue">finansowe' : 'bg-purple">niefinansowe').'</span>';
			$tmp['name'] = $value['cname'];
            $tmp['value'] = $value['amount'];
            $tmp['dates'] = '<span class="text--navy">'.$value['occurence_date'].'</span></br><span class="text--red">'.$value['payment_date'].'</span>';
            $tmp['details'] = '<span class="text--purple">Odsetki: <b>'.number_format($value['debt_interest'], 2, '.',' ').'</b></span><br /><span class="text--orange">Należność: <b>'.number_format($value['debt_balance'], 2, '.',' ').'</b></span>';
            $tmp['note'] = ($value['note']) ? '<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            //$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            //$tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			//$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
			$tmp['actions'] = $actionColumn;
           
			array_push($claims, $tmp); $tmp = [];
		}
		
		return $claims;
	}
  
    public function actionAddclaim($id) {
        $case = $this->findModel($id);
        
        $model = new \backend\Modules\Task\models\CaseClaim();
        $model->status = 1;
        $model->id_case_fk = $case->id;
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            $model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Roszczenie zostało dodane', 'id'=>$model->id, 'table' => '#table-claims', 'refresh' => 'yes' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formClaim', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formClaim', ['model' => $model]) ;	
		}
	}
    
    public function actionEditclaim($id) {
        $model = \backend\Modules\Task\models\CaseClaim::findOne(CustomHelpers::decode($id));
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Roszczenie zostało zaktualizowane', 'id'=>$model->id, 'table' => '#table-claims', 'refresh' => 'yes' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formClaim', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formClaim', ['model' => $model]) ;	
		}
	}
    
    public function actionDeleteclaim($id)  {
        $model = \backend\Modules\Task\models\CaseSide::findOne(CustomHelpers::decode($id));
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Roszczenie zostało usunięte', 'id' => $id, 'table' => '#table-claims');	
        } else {
            return array('success' => false, 'row' => 'Roszczenie nie może zostać usunięte', 'id' => $id, 'table' => '#table-claims');	
        }

       // return $this->redirect(['index']);
    }
    
    /* instances */
    public function actionInstances($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$sides = [];
		$tmp = [];
		
		$sql = "select ci.id, ci.instance_sygn, instance_name, fulladdress(ca.id) as canmae, ca.phone, ca.email, is_active"
                ." from {{%cal_case_instance}} ci left join {{%correspondence_address}} ca on ca.id = ci.instance_address_fk"
                ." where ci.status = 1 and id_case_fk = ".CustomHelpers::decode($id);
        $sidesData = \Yii::$app->db->createCommand($sql)->query();     
		
		foreach($sidesData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/task/manage/editinstance', 'id' => $value['id']]).'" class="btn btn-xs bg-blue2 text--blue gridViewModal" data-table="#table-instances" data-form="person-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/task/manage/deleteinstance', 'id' => $value['id']]).'" class="btn btn-xs bg-red2 text--red remove" data-table="#table-instances"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['instance'] = $value['canmae'];
			$tmp['name'] = $value['instance_name'];
            $tmp['sygn'] = $value['instance_sygn'];
            $tmp['lawsuit'] = '<a href="'.Url::to(['/task/lawsuit/update', 'id' => $tmp['id']]).'" data-title="Edytuj pozew" data-toggle="tooltip" data-placement="left"><i class="fa fa-edit"></i></a>';
            $tmp['active'] = ($value['is_active']) ? '<i class="fa fa-check text--green"></i>' : '<a class="deleteConfirm" data-label="Ustaw instancję jako aktywną" href="'.Url::to(['/task/instance/active', 'id' => $tmp['id']]).'" data-title="Ustaw jako aktywną" data-toggle="tooltip" data-placement="left"><i class="fa fa-check-square-o text--teal"></i></a>';
            //$tmp['note'] = ($value['note']) ? '<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            //$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            //$tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
			$tmp['actions'] = $actionColumn;
           
			array_push($sides, $tmp); $tmp = [];
		}
		
		return $sides;
	}
    
    public function actionAddinstance($id) {
        //$case = $this->findModel($id);
		$id = CustomHelpers::decode($id);
		$case = \backend\Modules\Task\models\CalCase::findOne($id);
        
        $model = new \backend\Modules\Task\models\CalCaseInstance();
        $model->status = 1;
        $model->is_active = 1;
        $model->id_case_fk = ($case) ? $case->id : 0;
		$level = \backend\Modules\Task\models\CalCaseInstance::find()->where(['status' => 1, 'id_case_fk' => $model->id_case_fk])->count();
        $model->instance_level = ( ($level) ? $level : 0 ) + 1;
        $model->instance_name = \Yii::$app->runAction('common/integertoroman', ['integer' => $model->instance_level]).' Instancja';
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Instancja została dodana', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formInstance', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formInstance', ['model' => $model]) ;	
		}
	}
    
    public function actionEditinstance($id) {
        $model = \backend\Modules\Task\models\CalCaseInstance::findOne(CustomHelpers::decode($id));
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Informacje zostały zaktualizowane', 'id'=>$model->id, 'table' => '#table-instances' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formInstance', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formInstance', ['model' => $model]) ;	
		}
	}
    
    public function actionDeleteinstance($id)  {
        $model = \backend\Modules\Task\models\CalCaseInstance::findOne(CustomHelpers::decode($id));
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Instancja została usunięta', 'id' => $id, 'table' => '#table-instances');	
        } else {
            return array('success' => false, 'row' => 'Instancja nie może zostać usunięta', 'id' => $id, 'table' => '#table-instances');	
        }

       // return $this->redirect(['index']);
    }
	
	public function actionAddemployee() {        
		$model = new \frontend\Modules\Task\models\GroupAction();
		$model->user_action = "addEmployees";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać sprawy!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->addEmployees()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Pracownicy zostali dołączeni', 'table' => '#table-matters' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formEmployees', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formEmployees', ['model' => $model]) ;	
		}
	}
	
	public function actionDelemployee() {        
		$model = new \frontend\Modules\Task\models\GroupAction();
		$model->user_action = "delEmployees";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać sprawy!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->delEmployees()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Pracownicy zostali odłączeni', 'table' => '#table-matters' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formEmployees', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formEmployees', ['model' => $model]) ;	
		}
	}
    
	public function actionCaseclose() {        
		$model = new \frontend\Modules\Task\models\GroupAction();
		$model->user_action = "closeCases";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać sprawy!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->closeCases()) {                      
                return array('success' => true, 'alert' => 'Sprawy zostały zamknięte', 'table' => '#table-matters' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formCasesClose', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formCasesClose', ['model' => $model]) ;	
		}
	}
	
	public function actionCasearchive() {        
		$model = new \frontend\Modules\Task\models\GroupAction();
		$model->user_action = "archiveCases";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać sprawy!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->archiveCases()) {                      
                return array('success' => true, 'alert' => 'Sprawy zostały przeniesione do archiwum', 'table' => '#table-matters' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formCasesArchive', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formCasesArchive', ['model' => $model]) ;	
		}
	}
}
