<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CalCaseInstance;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * InstanceController implements the CRUD actions for CalCaseInstance model.
 */
class InstanceController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CalCaseInstance();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.instances'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['CalCaseInstance'])) {
            $params = $_GET['CalCaseInstance'];
            \Yii::$app->session->set('search.matters', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['instance_sygn']) && !empty($params['instance_sygn']) ) { 
                array_push($where, "instance_sygn LIKE '%".strtolower($params['instance_sygn'])."%'");
            }
            if(isset($params['instance_name']) && !empty($params['instance_name']) ) { 
                array_push($where, "instance_name LIKE '%".strtolower($params['instance_name'])."%'");
            }
            if(isset($params['instance_address_fk']) && !empty($params['instance_address_fk']) ) {
				array_push($where, "instance_address_fk = ".$params['instance_address_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['is_active']) && !empty($params['is_active']) ) {
				if($params['is_active'] == 1 )
                    array_push($where, "is_active = 1");
                else if($params['is_active'] == 2 )
                    array_push($where, "is_active = 0");
            }
            if(isset($params['id_dict_instance_status_fk']) && !empty($params['id_dict_instance_status_fk']) ) {
                 array_push($where, "id_dict_instance_status_fk = ".$params['id_dict_instance_status_fk']);
            }
            if(isset($params['id_dict_instance_mode_fk']) && !empty($params['id_dict_instance_mode_fk']) ) {
                 array_push($where, "id_dict_instance_mode_fk = ".$params['id_dict_instance_mode_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['ci.id as id', 'm.id as mid', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'm.no_label', 'ci.id_dict_instance_status_fk', 'ci.id_dict_instance_mode_fk',
			          'c.name as cname', 'c.symbol as csymbol', 'c.id as cid', 'os.name as osname', 'os.symbol as ossymbol', 'os.id as osid', 
					  'instance_name', 'instance_sygn', 'fulladdress(ca.id) as caname', 'is_active', 'ciw.id as warrant', 'ciw.delivery_date as warrant_date', 'cic.id as clause', 'cic.delivery_date as clause_date'])
            ->from('{{%cal_case}} as m')
			->join(' JOIN', '{{%cal_case_instance}} as ci', 'm.id = ci.id_case_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
			->join(' LEFT JOIN', '{{%correspondence_address}} as ca', 'ca.id = ci.instance_address_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = m.id_opposite_side_fk')
            ->join(' LEFT JOIN', '{{%case_instance_warrant}} as ciw', 'ci.id = ciw.id_case_instance_fk')
            ->join(' LEFT JOIN', '{{%case_instance_clause}} as cic', 'ci.id = cic.id_case_instance_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] )->andWhere('id_dict_case_status_fk != 6');
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$statuses = \backend\Modules\Task\models\CalCaseInstance::listStatuses();
        $modes = \backend\Modules\Task\models\CalCaseInstance::listModes();
		foreach($rows as $key=>$value) {
            $value['id_dict_case_status_fk'] = ($value['id_dict_case_status_fk'] < 4) ? 1 : $value['id_dict_case_status_fk'];
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.(($value['csymbol']) ? $value['csymbol'] : $value['cname']).'</a>';   
			$tmp['side'] = '<a href="'.Url::to(['/crm/side/view', 'id'=>$value['osid']]).'" >'.(($value['ossymbol']) ? $value['ossymbol'] : $value['osname']).'</a>';    
            $tmp['case'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['mid']]).'" >'.$value['name'].'</a>';       
            $tmp['name'] = $value['instance_name'];
			$tmp['address'] = $value['caname'];
			$tmp['sygn'] = $value['instance_sygn'];
            $tmp['lawsuit'] = '<a href="'.Url::to(['/task/lawsuit/update', 'id' => $value['id']]).'" data-title="Edytuj pozew" data-toggle="tooltip" data-placement="left"><i class="fa fa-edit"></i></a>';
            $tmp['active'] = ($value['is_active']) ? '<i class="fa fa-check text--green"></i>' : '<a class="deleteConfirm" data-label="Ustaw instancję jako aktywną" href="'.Url::to(['/task/instance/active', 'id' => $value['id']]).'" data-title="Ustaw jako aktywną" data-toggle="tooltip" data-placement="left"><i class="fa fa-square-o text--teal"></i></a>';
            $tmp['warrant'] = ($value['warrant']) ? 
                                    '<a href="'.Url::to(['/task/instance/warrant', 'id' => $value['id']]).'" class="btn btn-xs btn-icon bg-blue gridViewModal" data-target="#modal-grid-item" data-title="Edytuj nakaz" data-toggle="tooltip" data-placement="left"><i class="fa fa-pencil-square"></i>'.$value['warrant_date'].'</a>'
                                  : '<a href="'.Url::to(['/task/instance/warrant', 'id' => $value['id']]).'" class="gridViewModal" data-target="#modal-grid-item" data-title="Ustaw nakaz" data-toggle="tooltip" data-placement="left"><i class="fa fa-square-o text--teal"></i></a>';
            $tmp['clause'] = ($value['clause']) ? 
                                    '<a href="'.Url::to(['/task/instance/clause', 'id' => $value['id']]).'" class="btn btn-xs btn-icon bg-teal gridViewModal" data-target="#modal-grid-item" data-title="Edytuj klauzulę" data-toggle="tooltip" data-placement="left"><i class="fa fa-pencil-square"></i>'.$value['clause_date'].'</a>'
                                  : '<a href="'.Url::to(['/task/instance/clause', 'id' => $value['id']]).'" class="gridViewModal" data-target="#modal-grid-item" data-title="Ustaw kaluzulę" data-toggle="tooltip" data-placement="left"><i class="fa fa-square-o text--teal"></i></a>';
            
			//$tmp['name'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
            $tmp['status'] = ($value['id_dict_instance_status_fk']) ? $statuses[$value['id_dict_instance_status_fk']] : '';
            $tmp['mode'] = ($value['id_dict_instance_mode_fk']) ? $modes[$value['id_dict_instance_mode_fk']] : '';
		    $actionColumn = '<div class="edit-btn-group">';		
                //$actionColumn .= '<a href="'.Url::to(['/task/instance/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-instances" data-target="#modal-grid-item" data-title="<i class=\'fa fa-info\'></i>Informacje o nakazie"><i class="fa fa-eye"></i></a>';                
                $actionColumn .= '<a href="'.Url::to(['/task/manage/editinstance', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-instances" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/task/manage/deleteinstance', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-instances"><i class="fa fa-trash"></i></a>';
			$actionColumn .= '</div>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalCaseInstance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
  
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Postępowania');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Sygnatura')
			->setCellValue('B3', 'Instancja')
			->setCellValue('C3', 'Organ')
			->setCellValue('D3', 'Klient')
            ->setCellValue('E3', 'Strona');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:E3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CalCaseInstance'])) {
            $params = $_GET['CalCaseInstance'];
            \Yii::$app->session->set('search.matters', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['instance_sygn']) && !empty($params['instance_sygn']) ) { 
                array_push($where, "instance_sygn LIKE '%".strtolower($params['instance_sygn'])."%'");
            }
            if(isset($params['instance_name']) && !empty($params['instance_name']) ) { 
                array_push($where, "instance_name LIKE '%".strtolower($params['instance_name'])."%'");
            }
            if(isset($params['instance_address_fk']) && !empty($params['instance_address_fk']) ) {
				array_push($where, "instance_address_fk = ".$params['instance_address_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['is_active']) && !empty($params['is_active']) ) {
				if($params['is_active'] == 1 )
                    array_push($where, "is_active = 1");
                else if($params['is_active'] == 2 )
                    array_push($where, "is_active = 0");
            }
            if(isset($params['id_dict_instance_status_fk']) && !empty($params['id_dict_instance_status_fk']) ) {
                 array_push($where, "id_dict_instance_status_fk = ".$params['id_dict_instance_status_fk']);
            }
            if(isset($params['id_dict_instance_mode_fk']) && !empty($params['id_dict_instance_mode_fk']) ) {
                 array_push($where, "id_dict_instance_mode_fk = ".$params['id_dict_instance_mode_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'm.no_label',
			          'c.name as cname', 'c.symbol as csymbol', 'c.id as cid', 'os.name as osname', 'os.symbol as ossymbol', 'os.id as osid', 
					  'instance_name', 'instance_sygn', 'ca.name as caname'])
            ->from('{{%cal_case}} as m')
			->join(' JOIN', '{{%cal_case_instance}} as ci', 'm.id = ci.id_case_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
			->join(' LEFT JOIN', '{{%correspondence_address}} as ca', 'ca.id = ci.instance_address_fk')
			->join(' LEFT JOIN', '{{%customer}} as os', 'os.id = m.id_opposite_side_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
        
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				//$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['instance_sygn'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['instance_name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['caname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['osname']); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:E'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Postępowania');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Postępowania'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
	    
    public function actionActive($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
                
        $model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_active = 1;
      		
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'action' => 'forward', 'alert' => 'Instancja została ustawiona jako aktywna', 'table' => '#table-instances'  );		
        } else {
            return array('success' => false,  'action' => 'forward', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-instances', 'errors' => $model->getErrors()  );	
        }	
	}
    
	public function actionWarrant($id) {
        $id = CustomHelpers::decode($id);
        $instance = \backend\Modules\Task\models\CalCaseInstance::findOne($id);
        $model = $instance->warrant;
        if(!$model) {
            $model = new \backend\Modules\Task\models\CaseInstanceWarrant();
            $model->status = 1;
            $model->id_case_fk = $instance->id_case_fk;
            $model->id_case_instance_fk = $instance->id;
        }
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Nakaz został zapisany', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formWarrant', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formWarrant', ['model' => $model]) ;	
		}
	}
    
    public function actionClause($id) {
        $id = CustomHelpers::decode($id);
        $instance = \backend\Modules\Task\models\CalCaseInstance::findOne($id);
        $model = $instance->clause;
        if(!$model) {
            $model = new \backend\Modules\Task\models\CaseInstanceClause();
            $model->status = 1;
            $model->id_case_fk = $instance->id_case_fk;
            $model->id_case_instance_fk = $instance->id;
        }
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->save()) {                      
                return array('success' => true, 'index' =>1, 'alert' => 'Klauzula została zapisana', 'id'=>$model->id );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formClause', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formClause', ['model' => $model]) ;	
		}
	}
}
