<?php

namespace app\Modules\Task\controllers;

use Yii;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Task\models\CalCaseSearch;
use backend\Modules\Task\models\CaseEmployee;
use backend\Modules\Task\models\CaseDepartment;
use backend\Modules\Task\models\CalTask;
use backend\Modules\Task\models\TaskEmployee;
use backend\Modules\Task\models\TaskDepartment;
use backend\Modules\Correspondence\models\Correspondence;
use backend\Modules\Task\models\CalCaseInstance;
use backend\Modules\Task\models\CaseInstanceLawsuit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * LawsuitController implements the CRUD actions for CalCase model.
 */
class LawsuitController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = CalCaseInstance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionUpdate($id) {
       
		$instance = $this->findModel($id);
        $model = CaseInstanceLawsuit::find()->where(['status' => 1, 'id_case_instance_fk' => $instance->id])->one();
        if(!$model) {
            $model = new CaseInstanceLawsuit();
            $model->id_case_fk = $instance->id_case_fk;
            $model->id_case_instance_fk = $instance->id;
            $model->status = 1;
            $model->save();
        }
                      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->validate() && $model->save()) {				
                return array('success' => true, 'id' => $model->id, 'alert' => 'Zmiany zostały zapisane', /*'todoWidget' => TodoWidget::widget(['refresh' => true]) */ );	
			
            } else {
                return  $this->render('update', ['model' => $model, 'instance' => $instance]) ;
			}		
		} 
        
        return  $this->render('update', ['model' => $model, 'instance' => $instance]) ;
    }
    
    public function actionPrint($id) {
        $id = CustomHelpers::decode($id);
        $model = CaseInstanceLawsuit::findOne($id);
        
        return  $this->renderPartial('printpdf', ['model' => $model]) ;
    }
    
    public function actionPrintpdf($id, $save) {
        error_reporting(0); 
        $id = CustomHelpers::decode($id);
        $model = CaseInstanceLawsuit::findOne($id);
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "pozew.pdf";
        
        // instantiate and use the dompdf class
        //$dompdf = new \Dompdf\Dompdf();
       // $dompdf->loadHtml($this->renderPartial('printpdf', array('model' => $model, 'employees' => ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false )), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');
        //$dompdf->set_paper("A4", "landscape");

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
       /* $dompdf->render();
        
        if($save) {
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	*/
        // Output the generated PDF to Browser
        
        // Read contents
        $source = __DIR__ . "/templates/pozew.rtf";
        //$phpWord = \PhpOffice\PhpWord\IOFactory::load($source, 'RTF');
        $value[1] = 'Lorem ipsum';
        $file = file_get_contents($source);
        
       /* $sections = $phpWord->getSections();
        foreach ($sections as $key => $value) {
            $sectionElement = $value->getElements();
            foreach ($sectionElement as $elementKey => $elementValue) {
                if ($elementValue instanceof \PhpOffice\PhpWord\Element\TextRun) {
                    $secondSectionElement = $elementValue->getElements();
                    foreach ($secondSectionElement as $secondSectionElementKey => $secondSectionElementValue) {
                            if ($secondSectionElementValue instanceof \PhpOffice\PhpWord\Element\Text) {
                                echo $secondSectionElementValue->getText() . '<br/>';
                            }
                    }
                }
            }
        }exit;*/
        // Save file
       // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'RTF');

		//header("Content-Type: application/msword");
		//header('Content-Disposition: attachment;filename='.$filename .' ');
        header("Content-type: application/rtf; charset=charset=utf8");
        header("Content-Disposition: attachment;filename=pozew.rtf");
		header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); 
       // $data_base[0]['body'] = 'I am a {$club} fan.'; // Tests

        $vars = array(
          '{$amount}' => '10 000.00',
          '{$amount_in_word}' => 'dziesięć tysięcy złotych',
          '{$instance}' => 'sąd',
          '{$customer}'       =>  mb_convert_encoding($model->case['customer']['name'], 'windows-1252', 'utf-8'), 
          '{$side}'        => $model->case['side']['name'],
          '{$reason}' => 'someothertext'
        );

        echo strtr($file, $vars);
        exit;		
		//$objWriter->save('php://output');	
    }

}
