<?php

namespace backend\Modules\Alert\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use \backend\Modules\Alert\models\AlertArch;

/**
 * This is the model class for table "{{%alert}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_fk
 * @property string $alert_date
 * @property string $alert_time
 * @property string $name
 * @property string $note
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
 
 /**
    * 1 - zdarzenia kkancelaryjne
    * 2 - historia windykacji
 **/
 
class Alert extends \yii\db\ActiveRecord
{
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%alert}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_fk', 'is_done', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_fk', 'type_fk', 'name', 'alert_date'], 'required'],
            [['alert_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['note', 'custom_data'], 'string'],
            [['alert_time'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'alert_date' => Yii::t('app', 'Data'),
            'alert_time' => Yii::t('app', 'Czas'),
            'name' => Yii::t('app', 'Name'),
            'note' => Yii::t('app', 'Dodatkowy opis'),
            'custom_data' => Yii::t('app', 'Custom Data'),
			'is_done' => 'zrobione',
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {       
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
        if (!$insert && $this->status == 1) {
            $modelArch = new AlertArch();
            $modelArch->id_alert_fk = $this->id;
            if(!$this->user_action) {
                $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
            }
            
            $modelArch->data_change = \yii\helpers\Json::encode($this);
            $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
        } 

		parent::afterSave($insert, $changedAttributes);
	}
}
