<?php

namespace app\Modules\Eg\controllers;

use Yii;
use backend\Modules\Eg\models\Patient;
use backend\Modules\Eg\models\Visit;
use backend\Modules\Eg\models\VisitService;
use backend\Modules\Eg\models\Tooth;
use backend\Modules\Eg\models\ToothHistory;
use backend\Modules\Eg\models\Service;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * ToothController implements the CRUD actions for CalCase model.
 */
class ToothController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex() {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new Service();
        //$searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.services'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['Service'])) {
            $params = $_GET['Service'];
            \Yii::$app->session->set('search.services', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 's.service_name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'service_name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'service_symbol';
        if( isset($post['sort']) && $post['sort'] == 'price' ) $sortColumn = 'service_price';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_service}} as s')
            ->where( ['s.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['s.id as id', 's.service_name as name', 's.service_symbol as symbol', 's.service_price as price', "concat_ws(' ', e.lastname, e.firstname) as creator", 's.created_by', 's.created_at', 'is_active'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%user}} as e', 'e.id = s.created_by');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        
		foreach($rows as $key=>$value) {
			$tmp['name'] = $value['name'];
            $tmp['symbol'] = $value['symbol'];
            $tmp['price'] = number_format($value['price'], 2, '.', ' ');
			$tmp['created'] = '<small>'.$value['creator'].'<br />'.$value['created_at'].'</small>';
			//$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'"><i class="fa fa-'.$presentation['icon'].'"></i></label>';
          
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/service/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil-alt"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/service/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-services"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
			$tmp['actions'] .= '</div>';
            if($value['is_active'])
                $tmp['active'] = '<a class="modalConfirm" data-label="Ustaw zabieg jako nieaktywny" href="'.Url::to(['/eg/service/inactive', 'id' => $value['id']]).'" data-title="Ustaw jako nieaktywny" data-toggle="tooltip" data-placement="left"><i class="fa fa-check text--green"></i></a>';
            else
                $tmp['active'] = '<a class="modalConfirm" data-label="Ustaw zabieg jako aktywny" href="'.Url::to(['/eg/service/active', 'id' => $value['id']]).'" data-title="Ustaw jako aktywny" data-toggle="tooltip" data-placement="left"><i class="fa fa-square text--teal"></i></a>';
       
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
   
    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $tooth) {
	    $model = Tooth::find()->where(['status' => 1, 'tooth_no' => $tooth, 'id_patient_fk' => $id])->one();
        if(!$model) {
            $model = new Tooth();
            $model->id_patient_fk = $id;
            $model->tooth_no = $tooth;
            $model->status = 1;
            if(!$model->save()) {var_dump($model->getErrors()); exit;}
        }
        
        $new = new ToothHistory();
        $new->id_tooth_fk = $model->id;
        $new->id_dict_tooth_action_fk = $model->id_dict_tooth_status_fk;
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$new->load(Yii::$app->request->post());
            if($new->id_dict_tooth_action_fk != $model->id_dict_tooth_status_fk) {
                $model->id_dict_tooth_status_fk = $new->id_dict_tooth_action_fk;
                $model->save();
            }
			if($new->validate() && $new->save()) {
				if($model->dict['custom_data']) {
                    $customData = \yii\helpers\Json::decode($model->dict['custom_data']);
                    $color = $customData['color'];
                } else {
                    $color = 'gray';
                }
                
                $notes = ($model->notes) ? '<small class="badge bg-teal">'.count($model->notes).'</small>' : '';
                $status = ($model->id_dict_tooth_status_fk) ? '<small class="label" style="background-color: '.$color.'">'.$model->dict['name'].'</small>' : '';
                $toothInfo = '<img src="/images/tooths/'.$tooth.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $id, 'tooth' => $tooth]).'" class="viewModal" data-title="Ustaw status zęba" data-target="#modal-grid-item">ustaw status zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $id, 'tooth' => $tooth]).'" class="viewModal" data-title="Wpis do historii leczenia" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/view', 'id' => $id, 'tooth' => $tooth]).'" class="viewModal" data-title="Podgląd zęba" data-target="#modal-grid-item">podgląd zęba</a></li>'                                    
                                .'</ul>'
                            .'</div>';
                            
                if($new->services && $new->id_visit_fk) {
                    foreach($new->services as $key => $serviceId) {
                        $newService = new VisitService();
                        $newService->id_visit_fk = $new->id_visit_fk;
                        $newService->id_tooth_fk = $new->id_tooth_fk;
                        $newService->tooth_no = $tooth;
                        $newService->id_service_fk = $serviceId;
                        $newService->payment = Service::findOne($serviceId)->service_price;
                        $newService->save();
                    }
                }
                
                return array('success' => true, 'refresh' => 'yes', 'table' => '#table-treatment', 'index' => 1, 'action' => 'toothEdit', 'id' => $model->tooth_no, 'toothInfo' => $toothInfo, );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $new, 'patient' => $id]), 'errors' => $new->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_form', [ 'model' => $new, 'patient' => $id ]) ;	
		}
	}
    
    public function actionState($id, $tooth) {
	    $model = Tooth::find()->where(['status' => 1, 'tooth_no' => $tooth, 'id_patient_fk' => $id])->one();
        if(!$model) {
            $model = new Tooth();
            $model->id_patient_fk = $id;
            $model->tooth_no = $tooth;
            $model->status = 1;
            //if(!$model->save()) {var_dump($model->getErrors()); exit;}
        }
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				if($model->dict['custom_data']) {
                    $customData = \yii\helpers\Json::decode($model->dict['custom_data']);
                    $color = $customData['color'];
                } else {
                    $color = 'gray';
                }
                
                $notes = ($model->notes) ? '<small class="badge bg-teal">'.count($model->notes).'</small>' : '';
                $status = ($model->id_dict_tooth_status_fk) ? '<small class="label" style="background-color: '.$color.'">'.$model->dict['name'].'</small>' : '';
                $toothInfo = '<img src="/images/tooths/'.$tooth.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $id, 'tooth' => $tooth]).'" class="viewModal" data-title="Ustaw status zęba" data-target="#modal-grid-item">ustaw status zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $id, 'tooth' => $tooth]).'" class="viewModal" data-title="Wpis do historii leczenia" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                .'</ul>'
                            .'</div>';
                return array('success' => true, 'refresh' => 'yes', 'table' => '#table-treatment', 'action' => 'toothEdit', 'id' => $model->tooth_no, 'toothInfo' => $toothInfo, 'index' => 1);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formState', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formState', [ 'model' => $model, ]) ;	
		}
	}
    
    public function actionUpdate($id) {
	    $model = ToothHistory::findOne($id);
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
                return array('success' => true, 'refresh' => 'yes', 'table' => '#table-treatment', 'action' => 'toothEdit', 'id' => $model->tooth_no, 'toothInfo' => $toothInfo, 'index' => 1);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model, 'patient' => false]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_form', [ 'model' => $model, 'patient' => false]) ;	
		}
	}
    
    public function actionDelete($id)  {
        $model = ToothHistory::findOne($id);
        $model->status = -1;
        //$model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Wpis został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Wpis został usunięty.<br/>', 'id' => $id, 'table' => '#table-treatment');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Wpis nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Wpis nie został usunięty.<br />', 'id' => $id, 'table' => '#table-treatment');	
			}
        }
    }
    
    public function actionView($id, $tooth) {
	    $model = Tooth::find()->where(['status' => 1, 'tooth_no' => $tooth, 'id_patient_fk' => $id])->one();
        if(!$model) {
            $model = new Tooth();
            $model->id_patient_fk = $id;
            $model->tooth_no = $tooth;
            $model->status = 1;
            if(!$model->save()) {var_dump($model->getErrors()); exit;}
        }
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
                return array('success' => true, 'refresh' => 'yes', 'table' => '#table-treatment', 'action' => 'toothEdit', 'id' => $model->tooth_no, 'toothInfo' => $toothInfo, 'index' => 1);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_view', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_view', [ 'model' => $model, ]) ;	
		}
	}
	    
    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested template does not exist.');
        }
    }
    
    public function actionExport($id) {	
		$patient = Patient::findOne($id);
        $objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Denatl")
                    ->setLastModifiedBy("Dental")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Przebieg leczenia pacjenta '.$patient->fullname);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Ząb')
			->setCellValue('B3', 'Data')
			->setCellValue('C3', 'Utworzył(a)')
			->setCellValue('D3', 'Komentarz');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        		
		$sortColumn = 'h.created_at';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())->from('{{%eg_tooth_history}} as h')
            ->select(['h.id', 'h.created_at', 'h.note', 't.tooth_no', 't.id as tid', "concat_ws(' ', c.lastname, c.firstname) as creator"])
            ->join(' JOIN', '{{%eg_patient_tooth}} as t', 't.id = h.id_tooth_fk')
            ->join(' JOIN', '{{%user}} as c', 'c.id = h.created_by')
            ->where( ['t.id_patient_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['tooth_no'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['created_at']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['creator']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['note']); 
               
				$i++; 
			}  
		}
		
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(100);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Przebieg leczenia');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Leczenie'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
    
    public function actionPrint($id) {
        error_reporting(0); 
        $sortColumn = 'h.created_at';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())->from('{{%eg_tooth_history}} as h')
            ->select(['h.id', 'h.created_at', 'h.note', 't.tooth_no', 't.id as tid', "concat_ws(' ', c.lastname, c.firstname) as creator"])
            ->join(' JOIN', '{{%eg_patient_tooth}} as t', 't.id = h.id_tooth_fk')
            ->join(' JOIN', '{{%user}} as c', 'c.id = h.created_by')
            ->where( ['t.id_patient_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		      		
		$rows = $query->all();
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "leczenie.pdf";
  
        // instantiate and use the dompdf class
        $dompdf = new \Dompdf\Dompdf();
        //$dompdf->set_option('fontHeightRatio', 0.6);
        $dompdf->loadHtml($this->renderPartial('printpdf', array('model' => $rows), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
        $dompdf->render();
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	
        // Output the generated PDF to Browser
    }
}
