<?php

namespace app\Modules\Eg\controllers;

use Yii;
use backend\Modules\Eg\models\Patient;
use backend\Modules\Eg\models\Visit;
use backend\Modules\Eg\models\VisitService;
use backend\Modules\Cms\CmsGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * VisitController implements the CRUD actions for CalCase model.
 */
class VisitController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'Visit' => Visit::className()
				]
			],
		];
	}

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex() {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new Visit();
        $searchModel->start = date('Y-m-d');
        $searchModel->id_dict_eg_visit_status_fk = [1,3];

        //$searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.visits'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['Visit'])) {
            $params = $_GET['Visit'];
            \Yii::$app->session->set('search.visits', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_patient_fk']) && !empty($params['id_patient_fk']) ) {
				array_push($where, "v.id_patient_fk = ".$params['id_patient_fk']);
            }
			if(isset($params['id_dict_eg_visit_status_fk']) && !empty($params['id_dict_eg_visit_status_fk']) ) {
				array_push($where, "id_dict_eg_visit_status_fk in (".implode(',',$params['id_dict_eg_visit_status_fk']).")");
            }
            if(isset($params['id_visit_tpe_fk']) && !empty($params['id_visit_tpe_fk']) ) {
				array_push($where, "id_visit_tpe_fk in (".implode(',',$params['id_visit_tpe_fk']).")");
            }
            if(isset($params['id_doctor_fk']) && !empty($params['id_doctor_fk']) ) {
				array_push($where, "v.id_doctor_fk = ".$params['id_doctor_fk']);
            }
            if(isset($params['start']) && !empty($params['start']) ) {
				array_push($where, "visit_day >= '".$params['start']."'");
            }
            if(isset($params['end']) && !empty($params['end']) ) {
				array_push($where, "visit_day <= '".$params['end']."'");
            }
            if(isset($params['is_confirm']) && strlen($params['is_confirm']) ) {
				array_push($where, "v.is_confirm = ".$params['is_confirm']);
            }
        } 
		
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'v.start';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
        if( isset($post['sort']) && $post['sort'] == 'type' ) $sortColumn = 'type_name';
		
		$query = (new \yii\db\Query())->from('{{%eg_visit}} as v')
            ->where( ['v.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['v.id', 'v.visit_day', 'v.start', 'v.end', 'id_dict_eg_visit_status_fk', 'v.additional_info', 'v.note', 'v.is_confirm', 'v.place_postalcode', 'v.place_city', 'v.place_address',
                        'p.id as pid', 'p.firstname', 'p.lastname', 'p.phone', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at', 't.type_name', 'id_gallery_fk']);
        $query->join(' JOIN', '{{%eg_patient}} as p', 'p.id = v.id_patient_fk');
		$query->join(' JOIN', '{{%company_employee}} as  d', 'd.id = v.id_doctor_fk');
        $query->join(' LEFT JOIN', '{{%eg_visit_type}} as  t', 't.id = v.id_visit_type_fk');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        $presentationAll = Visit::Viewer();
		foreach($rows as $key => $value) {
            $presentation = $presentationAll[$value['id_dict_eg_visit_status_fk']];
            
			$tmp['lastname'] = '<a href="'.Url::to(['/eg/patient/view', 'id'=>$value['pid']]).'" >'.$value['lastname'].'</a>';
			$tmp['firstname'] = $value['firstname'];
			$tmp['doctor'] = $value['doctor'];
			$tmp['term'] = $value['visit_day'];
            $tmp['time'] = round((strtotime($value['end']) - strtotime($value['start']))/60,0);
            $tmp['info'] = ($value['additional_info']) ? $value['additional_info'] : '';
            $tmp['address'] = (($value['place_postalcode']) ? ($value['place_postalcode'].', ') : '').$value['place_city'];
            $tmp['address'] .= (($tmp['address']) ? '<br />' : '').$value['place_address'];
            $tmp['note'] = ($value['note']) ? '<i class="fas fa-sticky-note fa-2x text--yellow" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
            $tmp['type'] = ($value['type_name']) ? $value['type_name'] : '';
			$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
            $value['phone'] = $value['phone'] ? $value['phone'] : 'brak';
			$value['email'] = $value['email'] ? $value['email'] : 'brak';
            $tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone"></i>'.$value['phone'].'</div><div class="btn-icon"><i class="fa fa-envelope"></i>'.$value['email'].'</div>';
            $tmp['gallery'] = ($value['id_gallery_fk']) ? '<a href="'.Url::to(['/cms/cmsgallery/images', 'id' => $value['id_gallery_fk']]).'" class="text--orange"><i class="fa fa-images"></i></a>' : '<a href="'.Url::to(['/eg/visit/gallery', 'id' => $value['id']]).'" class="btn btn-sm bg-green update" data-table="#table-visits" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-images\'></i>Dodanie galerii" title="Dodanie galerii"><i class="fa fa-plus"></i></a>';
            //$tmp['confirm'] = ($value['id_dict_eg_visit_status_fk'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<a class="starred" data-label="Wizyta potwierdzona" data-icon="check" data-color="green" href="'.Url::to(['/eg/visit/confirm', 'id' => $value['id']]).'" data-title="Potwierdzenie wizyty" data-toggle="tooltip" data-placement="left"><i class="fa fa-exclamation-circle text--orange"></i></a>';
            //$tmp['confirm'] = ($value['is_confirm']) ? '<i class="fa fa-check text--green"></i>' : '<a class="starred" data-label="Wizyta potwierdzona" data-icon="check" data-color="green" href="'.Url::to(['/eg/visit/confirm', 'id' => $value['id']]).'" data-title="Potwierdzenie wizyty" data-toggle="tooltip" data-placement="left"><i class="fa fa-exclamation-circle text--orange"></i></a>';

			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/print', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-visits" title="'.Yii::t('app', 'View').'"><i class="fa fa-print text--blue"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/showajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-visits" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>Edycja wizyty" title="Edycja wizyty"><i class="fa fa-pencil-alt text--purple"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/eg/visit/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-visits"><i class="fa fa-trash text--red" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
   
    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {	
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        $model = new Patient();
        $model->id_patient_fk = 1;
        
		//$model->id_company_branch_fk = $this->module->params['employeeBranch']; 
        $departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']);
       	
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            
        	if($model->validate() && $model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
                return  $this->render('create', [ 'model' => $model, ]) ;	
			}		
		} else {
			return  $this->render('create', [ 'model' => $model,]) ;	
		}
	}

    /**
     * Updates an existing CalCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)  {
        /*if( count(array_intersect(["caseEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $model = $this->findModel($id);
        $offer = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej sprawy.');
                }
            }
        }
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        $departments = []; $employees = [];
        $grants = $this->module->params['grants'];
    
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {			

				return array('success' => true,  'action' => false, 'alert' => 'Profil klienta <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->render( 'update', [
                        'model' => $model, 'grants' => $this->module->params['grants']
                    ]) ;	
		}
    }
	
	public function actionNew($id) {
        
		$model = new Visit();
        $model->user_action = 'create';
        $model->id_patient_fk = $id;
        $model->id_dict_eg_visit_status_fk = 3;
        if($id) {
            $patient = Patient::findOne($id);
            $model->id_doctor_fk = $patient->id_doctor_fk;
        }
        $model->fromDate = ( isset($_GET['start']) && !empty($_GET['start']) ) ? gmdate('Y-m-d', ($_GET['start'])) : date('Y-m-d');
        $model->fromTime = ( isset($_GET['start']) && !empty($_GET['start']) ) ? gmdate('H:i', ($_GET['start'])) : '';
        $model->toDate = $model->fromDate;
        $model->id_doctor_fk = ( isset($_GET['doctor']) && !empty($_GET['doctor']) ) ? $_GET['doctor'] : '';
        
        if($model->fromDate && $model->fromTime)
            $model->toTime = date("H:i", (strtotime($model->fromDate.' '.$model->fromTime.':00') + 60*30));
        /*if($model->visit_duration) {
            $model->timeH = intval($model->visit_duration/60);
            $model->timeM = $model->visit_duration - ($model->timeH*60);
        }*/
        
        /*if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }*/
        
        $services = [ 0 => new VisitService() ]; 
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->start = $model->fromDate . ' ' . (($model->fromTime) ? $model->fromTime : '00:00') . ':00';
            $model->end = $model->toDate . ' ' . (($model->toTime) ? $model->toTime : '00:00') . ':00' ;
			if($model->validate() && $model->save()) {
                $data = [];
				return array('success' => true, 'row' => $data, 'index' => isset($_GET['index']) ? $_GET['index'] : 0, 'id' => $model->id, 'calendar' => '#timetable', 'table' => '#table-visits', 'refresh' => 'yes'  );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model, 'services' => $services]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model, 'services' => $services]) ;	
		}
	}
	
	public function actionShowajax($id) {
        
		$model = $this->findModel($id);
        $model->user_action = 'change';
        
        if($model->start) {
            $model->fromDate = date('Y-m-d', strtotime($model->start));
            $model->fromTime = date('H:i', strtotime($model->start));
        } else {
            $model->fromDate = $model->visit_day;
        }
        
        if($model->end) {
            $model->toDate = date('Y-m-d', strtotime($model->end));
            $model->toTime = date('H:i', strtotime($model->end));
        } else {
            $model->toDate = $model->visit_day;
        }
        
        /*if($model->visit_duration) {
            $model->timeH = intval($model->visit_duration/60);
            $model->timeM = $model->visit_duration - ($model->timeH*60);
        }*/
        
        /*if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }*/
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->fromTime)
                $model->start = $model->fromDate . ' ' . $model->fromTime . ':00';
            if($model->toTime)
                $model->end = $model->toDate . ' ' . $model->toTime . ':00' ;
			if($model->validate() && $model->save()) {
                $data = [];
                $data['doctor'] = $model->doctor['fullname'];
                $data['term'] = $model->fromDate;
                $data['time'] = round((strtotime($model->end) - strtotime($model->start))/60,0);
                $data['info'] = ($model->additional_info) ? $model->additional_info : '';
                $data['note'] = ($model->note) ? '<i class="fas fa-sticky-note fa-2x text--yellow" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$model->note.'"></i>' : '';
				return array('success' => true, 'row' => $data, 'index' => isset($_GET['index']) ? $_GET['index'] : 0, 'id' => $model->id, 'calendar' => '#timetable', 'table' => '#table-visits', 'refresh' => 'inline'  );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model]),'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model]) ;	
		}
	}

    /**
     * Deletes an existing CalCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)  {
        //$this->findModel($id)->delete();
        /*if( count(array_intersect(["caseDelete", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalCase::SCENARIO_DELETE;
        $model->status = -1;
        //$model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        /*$offer = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        
        if($offer != 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty')  );
				return $this->redirect(['index', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty', 'id' => $id, 'table' => '#table-patients');	
			}
        }*/
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Termin został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Termin został usunięty.<br/>', 'id' => $id, 'table' => '#table-visits');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Termin nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Termin nie został usunięty.<br />', 'id' => $id, 'table' => '#table-visits');	
			}
        }
    }
    
    public function actionConfirm($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_confirm = 1;
      	$row = ['confirm' => '<i class="fa fa-check text--green"></i>'];	
        //$transaction = \Yii::$app->db->beginTransaction();
        if($model->save()) {     
			return array('success' => true, 'row' => $row, 'action' => 'visitConfirm', 'refresh' => 'inline', 'row' => $row, 'alert' => 'Wizyta została potwierdzona', 'table' => '#table-visits', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'calendar' => '#timetable'  );		
        } else {
            return array('success' => false, 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-visits', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionCompleted($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->id_dict_eg_visit_status_fk = 5;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'inline', 'row' => $row, 'alert' => 'Wizyta została zrealizowana', 'table' => '#table-visits', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'calendar' => '#timetable'  );		
        } else {
            return array('success' => false,  'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-visits', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionCancel($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->id_dict_eg_visit_status_fk = 6;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'inline', 'row' => $row, 'alert' => 'Wizyta została odwołana', 'table' => '#table-visits', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'calendar' => '#timetable'  );		
        } else {
            return array('success' => false,  'refresh' => 'inline', 'alert' => 'Operacja nie może zostać odwołana', 'table' => '#table-visits', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionRemove($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->id_dict_eg_visit_status_fk = 7;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'yes', 'row' => $row, 'alert' => 'Wizyta została usunięta', 'table' => '#table-visits', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1), 'calendar' => '#timetable'  );		
        } else {
            return array('success' => false,  'refresh' => 'yes', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-visits', 'errors' => $model->getErrors()  );	
        }	
	}
    
    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = Visit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested template does not exist.');
        }
    }
    
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Wizyty');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Pacjent')
			->setCellValue('B3', 'Termin')
			->setCellValue('C3', 'Czas [min]')
			->setCellValue('D3', 'Lekarz')
            ->setCellValue('E3', 'Informacja')
            ->setCellValue('F3', 'Notatka')
            ->setCellValue('G3', 'Status')
            ->setCellValue('H3', 'Potwierdzenie');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;

        if(isset($_GET['Visit'])) {
            $params = $_GET['Visit'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_patient_fk']) && !empty($params['id_patient_fk']) ) {
				array_push($where, "v.id_patient_fk = ".$params['id_patient_fk']);
            }
			if(isset($params['id_dict_eg_visit_status_fk']) && !empty($params['id_dict_eg_visit_status_fk']) ) {
				array_push($where, "id_dict_eg_visit_status_fk in (".implode(',',$params['id_dict_eg_visit_status_fk']).")");
            }
            if(isset($params['id_doctor_fk']) && !empty($params['id_doctor_fk']) ) {
				array_push($where, "v.id_doctor_fk = ".$params['id_doctor_fk']);
            }
            if(isset($params['start']) && !empty($params['start']) ) {
				array_push($where, "visit_day >= '".$params['start']."'");
            }
            if(isset($params['end']) && !empty($params['end']) ) {
				array_push($where, "visit_day >= '".$params['end']."'");
            }
            if(isset($params['is_confirm']) && strlen($params['is_confirm']) ) {
				array_push($where, "v.is_confirm = ".$params['is_confirm']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'v.start';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_visit}} as v')
            ->where( ['v.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['v.id', 'v.start', 'v.end', 'id_dict_eg_visit_status_fk', 'v.additional_info', 'v.note', 'v.is_confirm', 'p.id as pid', 'p.firstname', 'p.lastname', 'p.phone', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%eg_patient}} as p', 'p.id = v.id_patient_fk');
		$query->join(' JOIN', '{{%company_employee}} as  d', 'd.id = v.id_doctor_fk');
			  
        /*$query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );*/
		      		
		$rows = $query->all();
        $presentationAll = Visit::Viewer();
		if(count($rows) > 0) {
			foreach($rows as $record){ 
                $presentation = $presentationAll[$record['id_dict_eg_visit_status_fk']];
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['lastname'].' '.$record['firstname'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['start']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ((strtotime($record['end'])-strtotime($record['start']))/60)); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['doctor']); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['additional_info']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['note']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $presentation['label']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, (($record['is_confirm']) ? 'potwierdzona' : 'niepotwierdzona')); 
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Wizyty');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Wizyty'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
    
    public function actionPrintpdf() {
        error_reporting(0); 
        $where = [];
        if(isset($_GET['Visit'])) {
            $params = $_GET['Visit'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_patient_fk']) && !empty($params['id_patient_fk']) ) {
				array_push($where, "v.id_patient_fk = ".$params['id_patient_fk']);
            }
			if(isset($params['id_dict_eg_visit_status_fk']) && !empty($params['id_dict_eg_visit_status_fk']) ) {
				array_push($where, "id_dict_eg_visit_status_fk in (".implode(',',$params['id_dict_eg_visit_status_fk']).")");
            }
            if(isset($params['id_doctor_fk']) && !empty($params['id_doctor_fk']) ) {
				array_push($where, "v.id_doctor_fk = ".$params['id_doctor_fk']);
            }
            if(isset($params['start']) && !empty($params['start']) ) {
				array_push($where, "visit_day >= '".$params['start']."'");
            }
            if(isset($params['end']) && !empty($params['end']) ) {
				array_push($where, "visit_day <= '".$params['end']."'");
            }
            if(isset($params['is_confirm']) && strlen($params['is_confirm']) ) {
				array_push($where, "v.is_confirm = ".$params['is_confirm']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'v.start';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_visit}} as v')
            ->where( ['v.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['v.id', 'v.start', 'v.end', 'id_dict_eg_visit_status_fk', 'v.additional_info', 'v.note', 'v.is_confirm', 'p.id as pid', "concat_ws(' ', p.lastname, p.firstname) as patient", 'p.phone', 'p.phone_additional', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%eg_patient}} as p', 'p.id = v.id_patient_fk');
		$query->join(' JOIN', '{{%company_employee}} as  d', 'd.id = v.id_doctor_fk');
			  
        /*$query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )*/
        $query->limit( 300 )->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        $presentationAll = Visit::Viewer();
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "wizyta.pdf";
  
        // instantiate and use the dompdf class
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->renderPartial('printall', array('model' => $rows), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
        $dompdf->render();
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	
        // Output the generated PDF to Browser
    }
    
    public function actionPrint($id) {
        error_reporting(0); 
        $model = $this->findModel($id);
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "wizyta.pdf";
  
        // instantiate and use the dompdf class
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->renderPartial('printpdf', array('model' => $model), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
        $dompdf->render();
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	
        // Output the generated PDF to Browser
    }
    
    public function actionServices($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $where = []; $post = $_GET;
		$fields = []; $tmp = [];
        $status = [];
		
		$sortColumn = 's.service_name';
		
		$query = (new \yii\db\Query())->from('{{%eg_visit_service}} as v')
            ->join(' JOIN', '{{%eg_service}} as s', 's.id = v.id_service_fk')
            ->select(['v.id', 's.service_name', 'v.tooth_no', 'payment', 'paid', 'discount'])
            ->where( ['v.status' => 1, 'v.id_visit_fk' => $id] );

        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        
		foreach($rows as $key => $value) {
			$tmp['service'] = $value['service_name'];
            $tmp['tooth'] = $value['tooth_no'];
            $tmp['payment'] = number_format($value['payment'], 2, '.', ' ');
            $tmp['paid'] = number_format($value['paid'], 2, '.', ' ');
            $tmp['discount'] = number_format($value['discount'], 2, '.', ' ');
			
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/supdate', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-services" data-target="#modal-grid-ajax"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil-alt"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/sdelete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-services"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
			$tmp['actions'] .= '</div>';
            
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionScreate($id) {
		
		$model = new VisitService();
        $model->id_visit_fk = $id;
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'refresh' => 'yes', 'table' => '#table-services', 'index' => 1, 'append' => 'element', 'id' => 'visitPayment', 'prefix' => 'tmpl', 'value' => $model->visit['saldo']);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formService', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formService', [ 'model' => $model, ]) ;	
		}
	}
   
	public function actionSupdate($id)  {
        $model = VisitService::findOne($id);
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				
                $data['service'] = $model->service['service_name'];
                $data['tooth'] = $model->tooth_no;
                $data['payment'] = number_format($model->payment, 2, '.', ' ');
                $data['paid'] = number_format($model->paid, 2, '.', ' ');
                $data['discount'] = number_format($model->discount, 2, '.', ' ');
				
				return array('success' => true, 'row' => $data, 'refresh' => 'inline', 'table' => '#table-services', 'index' => $_GET['index'], 'append' => 'element', 'id' => 'visitPayment', 'prefix' => 'tmpl', 'value' => $model->visit['saldo']);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formService', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formService', [ 'model' => $model, ]) ;	
		}
    }

	public function actionSdelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = VisitService::findOne($id);
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $model->id, 'table' => '#table-services', 'refresh' => 'yes');	
        } else {
            return array('success' => false, 'alert' => 'Dane nie zostały usunięte', );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionGallery($id) {
        
		$model = $this->findModel($id);
        $model->user_action = 'gallery';
        if(!$model->access_code)
            $model->access_code = \Yii::$app->getSecurity()->generateRandomString(8);
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			$newGallery = new \backend\Modules\Cms\models\CmsGallery();
            $newGallery->title = 'ugallery#'.$id;
            $newGallery->type_fk = 2;
            $newGallery->save();
            
            $model->id_gallery_fk = $newGallery->id;
            
            if($model->validate() && $model->save()) {
                $data = [];
                $data['doctor'] = $model->doctor['fullname'];
                $data['info'] = $model->additional_info;
                $data['gallery'] = '<a href="'.Url::to(['/cms/cmsgallery/images', 'id' => $newGallery->id]).'" title="Przejdź do galerii"><i class="fa fa-images text--orange"></i></a>';
               
				return array('success' => true, 'row' => $data, 'index' => isset($_GET['index']) ? $_GET['index'] : 0, 'id' => $model->id, 'calendar' => '#timetable', 'table' => '#table-visits', 'refresh' => 'inline'  );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formGallery', ['model' => $model]),'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formGallery', ['model' => $model]) ;	
		}
	}
}
