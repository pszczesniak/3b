<?php

namespace app\Modules\Eg\controllers;

use Yii;
use backend\Modules\Eg\models\Patient;
use backend\Modules\Eg\models\Visit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * PatientController implements the CRUD actions for CalCase model.
 */
class PatientController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex() {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new Patient();
        //$searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.patients'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['Patient'])) {
            $params = $_GET['Patient'];
            \Yii::$app->session->set('search.patients', ['params' => $params, 'post' => $post]);
            if(isset($params['firstname']) && !empty($params['firstname']) ) {
				array_push($where, "lower(p.firstname) like '%".strtolower( addslashes($params['firstname']) )."%'");
            }
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
				array_push($where, "lower(p.lastname) like '%".strtolower( addslashes($params['lastname']) )."%'");
            }
			if(isset($params['id_status_fk']) && !empty($params['id_status_fk']) ) {
				array_push($where, "p.id_status_fk = ".$params['id_status_fk']);
            }
            if(isset($params['id_group_fk']) && !empty($params['id_group_fk']) ) {
				array_push($where, "p.id_group_fk = ".$params['id_group_fk']);
            }
            if(isset($params['id_doctor_fk']) && !empty($params['id_doctor_fk']) ) {
				array_push($where, "p.id_doctor_fk = ".$params['id_doctor_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_patient}} as p')
            ->where( ['p.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['p.id as id', 'p.firstname', 'p.lastname', 'p.phone', 'p.phone_additional', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at', 'id_status_fk', 'id_group_fk', 'dv.name as dict_name'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' LEFT JOIN', '{{%dictionary_value}} as  dv', 'dv.id = p.id_group_fk');
        $query->join(' LEFT JOIN', '{{%company_employee}} as  d', 'd.id = p.id_doctor_fk');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        $presentationAll = Patient::Viewer();
		foreach($rows as $key => $value) {
			$presentation = $presentationAll[$value['id_status_fk']];
            $tmp['lastname'] = '<a href="'.Url::to(['/eg/patient/view', 'id'=>$value['id']]).'" >'.$value['lastname'].'</a>';
			$tmp['firstname'] = $value['firstname'];
			$tmp['doctor'] = $value['doctor'];
			$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
            $tmp['group'] = ($value['dict_name']) ? $value['dict_name'] : '';
            $value['phone'] = $value['phone'] ? $value['phone'] : 'brak';
            //$value['phone_additional'] = ($value['phone_additional']) ? $value['phone_additional'] : 'brak';
			$value['email'] = $value['email'] ? $value['email'] : 'brak';
            $tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone"></i>'.$value['phone'].'</div>'
                             .(($value['phone_additional']) ? ('<div class="btn-icon"><i class="fa fa-phone"></i>'.$value['phone_additional'].'</div>') : '')
                             .'<div class="btn-icon"><i class="fa fa-envelope"></i>'.$value['email'].'</div>';
			
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/patient/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-patients" title="'.Yii::t('app', 'View').'"><i class="fa fa-pencil-alt text--blue"></i></a>';
				//$tmp['actions'] .= '<a href="'.Url::to(['/eg/patient/treatment', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-patients" title="'.Yii::t('app', 'Przebieg leczenia').'"><i class="fa fa-heartbeat text--pink"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/eg/patient/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-patients"><i class="fa fa-trash text--red" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
	
	public function actionView($id)  {        
        $model = $this->findModel($id);

        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
                
        return $this->render('view', [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }
    
    public function actionTreatment($id) {
        $model = $this->findModel($id);

        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
                
        return $this->render('treatment', [
            'model' => $model, 'grants' => $this->module->params['grants']
        ]);
    }
   
    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {	
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        $model = new Patient();
		$model->user_action = 'create';
        $model->id_status_fk = 3;
        $model->postal_code = '42-200';
        $model->city = 'Częstochowa';
        //$model->type_fk = 1;
        
		//$model->id_company_branch_fk = $this->module->params['employeeBranch']; 
        $departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']);
       	
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            
        	if($model->validate() && $model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                $model->isNewRecord = true;
                return  $this->render('create', [ 'model' => $model, ]) ;	
			}		
		} else {
			return  $this->render('create', [ 'model' => $model,]) ;	
		}
	}

    /**
     * Updates an existing CalCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)  {
        $model = $this->findModel($id);
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        $grants = $this->module->params['grants'];
    
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {			
                $elements = [];
                array_push($elements, ['prefix' => 'tmpl', 'id' => 'info', 'value' => $this->renderPartial('_info', ['model' => $model])]);
                array_push($elements, ['prefix' => 'tmpl', 'id' => 'name', 'value' => $model->fullname]);
				return array('success' => true,  'action' => false, 'alert' => 'Profil klienta <b>'.$model->fullname.'</b> został zaktualizowany', 'refresh' => 'element', 'elements' => $elements, 'id' => $model->id );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderPartial( '_form', [
                        'model' => $model, 'grants' => $this->module->params['grants']
                    ]) ;	
		}
    }

    /**
     * Deletes an existing CalCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)  {
        //$this->findModel($id)->delete();
        if( count(array_intersect(["caseDelete", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalCase::SCENARIO_DELETE;
        $model->status = -1;
        //$model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        /*$offer = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        
        if($offer != 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty')  );
				return $this->redirect(['index', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty', 'id' => $id, 'table' => '#table-patients');	
			}
        }*/
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Klient <b>'.$model->fullname.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Klient <b>'.$model->fullname.'</b> został usunięty.<br/>', 'id' => $id, 'table' => '#table-patients', 'refresh' => 'yes');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Klient <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Klient <b>'.$model->name.'</b> nie został usunięty.<br />', 'id' => $id, 'table' => '#table-patients');	
			}
        }
    }
    
    public function actionActive($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_active = 1;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'yes', 'row' => $row, 'alert' => 'Szablon został ustawiony jako aktywny', 'table' => '#table-patients', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)  );		
        } else {
            return array('success' => false,  'refresh' => 'yes', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-patients', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionInactive($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_active = 0;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'yes', 'row' => $row, 'alert' => 'Szablon został ustawiony jako nieaktywny', 'table' => '#table-patients', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)  );		
        } else {
            return array('success' => false,  'refresh' => 'yes', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-patients', 'errors' => $model->getErrors()  );	
        }	
	}
    
    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = Patient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Klient o wskazanym identyfikatorze nie istnieje.');
        }
    }
    
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sprawy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
			->setCellValue('B3', 'Nazwa')
			->setCellValue('C3', 'Status')
			->setCellValue('D3', 'Pracownicy');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            /*if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                 $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }*/
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk < 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4 and id_dict_case_status_fk != 6");
        }
        
        $fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'c.name as cname', 'c.id as cid'])
            ->from('{{%cal_case}} as m')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, isset($status[$record['id_dict_case_status_fk']]) ? $status[$record['id_dict_case_status_fk']] : 'nieznany'); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
                $employees = '';
                foreach($case->employees as $key => $employee) {
                    $employees .= $employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Klient');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Klienci'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
    
    public function actionVisits($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['Visit'])) {
            $params = $_GET['Visit'];
            \Yii::$app->session->set('search.visits', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'v.start';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_visit}} as v')
            ->where( ['v.status' => 1, 'v.id_patient_fk' => $id] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['v.id', 'v.start', 'v.end', 'id_dict_eg_visit_status_fk', 'v.additional_info', 'v.note', 'v.is_confirm', 'p.id as pid', 'p.firstname', 'p.lastname', 'p.phone', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%eg_patient}} as p', 'p.id = v.id_patient_fk');
		$query->join(' JOIN', '{{%company_employee}} as  d', 'd.id = v.id_doctor_fk');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		      		
		$rows = $query->all();
        $presentationAll = Visit::Viewer();
		foreach($rows as $key => $value) {
			$presentation = $presentationAll[$value['id_dict_eg_visit_status_fk']];
            $tmp['lastname'] = '<a href="'.Url::to(['/eg/patient/view', 'id'=>$value['id']]).'" >'.$value['lastname'].'</a>';
			$tmp['firstname'] = $value['firstname'];
			$tmp['doctor'] = $value['doctor'];
			$tmp['term'] = $value['start'];
            $tmp['time'] = round((strtotime($value['end']) - strtotime($value['start']))/60,0);
            $tmp['info'] = ($value['additional_info']) ? $value['additional_info'] : '';
            $tmp['note'] = ($value['note']) ? '<i class="fas fa-sticky-note fa-2x text--yellow" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value['note'].'"></i>' : '';
			$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
			$value['email'] = $value['email'] ? $value['email'] : 'brak';
            $tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone"></i>'.$value['phone'].'</div><div class="btn-icon"><i class="fa fa-envelope"></i>'.$value['email'].'</div>';
            $tmp['confirm'] = ($value['is_confirm']) ? '<i class="fa fa-check text--green"></i>' : '<a class="starred" data-label="Wizyta potwierdzona" data-icon="check" data-color="green" href="'.Url::to(['/eg/visit/confirm', 'id' => $value['id']]).'" data-title="Potwierdzenie wizyty" data-toggle="tooltip" data-placement="left"><i class="fa fa-exclamation-circle text--orange"></i></a>';

			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/print', 'id' => $value['id']]).'" class="btn btn-xs btn-default" data-table="#table-visits" title="'.Yii::t('app', 'View').'"><i class="fa fa-print text--blue"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/visit/showajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-visits" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>Edycja wizyty" title="Edycja wizyty"><i class="fa fa-pencil-alt text--purple"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/eg/patient/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-visits"><i class="fa fa-trash text--red" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionAutocomplete() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $result = false;
        if(isset($_GET['q'])) {
            $sql = "SELECT id, concat_ws(' ', lastname, firstname) as name FROM {{%eg_patient}} WHERE status = 1 and lower(concat_ws(' ', lastname, firstname)) LIKE '%".strtolower($_GET['q'])."%' ORDER BY lastname, firstname LIMIT 20"; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
        if(isset($_GET['id'])) {
            $sql = "SELECT id, concat_ws(' ', lastname, firstname) as name FROM {{%eg_patient}} WHERE id = ".$_GET['id']; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
		
        if(!$result) {
            $sql = "SELECT id, concat_ws(' ', lastname, firstname) as name FROM {{%eg_patient}} WHERE status = 1 ORDER BY lastname LIMIT 20"; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
        
        $data = [];
        if($result) {
            foreach($result as $key => $row){
                $data[] = ['id' => $row['id'], 'text' => $row['name']];
            }
        }
		return $data;
	}
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {		
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = $id;
            $note->id_type_fk = 0;
            $note->id_case_fk = $model->id;
            //$note->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                //var_dump($note->getErrors());exit;
                return array('success' => false, 'alert' => 'Komentarz nie został dodany' );
            }
        }
        return array('success' => true );
    }
    
    public function actionHistory($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['Visit'])) {
            $params = $_GET['Visit'];
            \Yii::$app->session->set('search.visits', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'h.created_at';
        if( isset($post['sort']) && $post['sort'] == 'lastanme' ) $sortColumn = 'p.lastname';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%eg_tooth_history}} as h')
            ->join(' JOIN', '{{%eg_patient_tooth}} as t', 't.id = h.id_tooth_fk')
            ->where( ['h.status' => 1, 't.id_patient_fk' => $id] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['h.id', 'h.created_at', 'h.note', 't.tooth_no', 't.id as tid', "concat_ws(' ', c.lastname, c.firstname) as creator",
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
		$query->join(' JOIN', '{{%user}} as c', 'c.id = h.created_by');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		      		
		$rows = $query->all();
        
		foreach($rows as $key => $value) {
			$tmp['creator'] = $value['creator'];
            $tmp['created'] = $value['created_at'];
			$tmp['tooth'] = $value['tooth_no'];
            $tmp['note'] = $value['note'];
			//$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'"><i class="fa fa-'.$presentation['icon'].'"></i></label>';
            			
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/tooth/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-treatment" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil-alt"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/eg/tooth/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-treatment"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
			$tmp['actions'] .= '</div>';
            
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionCreateinline() {
		
		$model = new Patient();
		$model->user_action = 'create';
        $model->id_status_fk = 3;
		//$model->postal_code = '42-200';
        //$model->city = 'Częstochowa';
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            //$model->name = $_POST['value'];
            //$exist = DictionaryValue::find()->where
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Klient został dodany', 'id' => $model->id, 'name' => $model->fullname );
			} else {
                $alert = "Proszę podać imię i nazwisko klienta";
                foreach($model->getErrors() as $key => $value){
                    $alert = $value;
                }
                return array('success' => false, 'alert' => $alert );	
			}		
		} else {
	
			return  $this->renderAjax('_formInline', [  'model' => $model,]) ;	
		}
	}
}
