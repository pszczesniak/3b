<?php

namespace app\Modules\Eg\controllers;

use Yii;
use backend\Modules\Eg\models\Visit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * CalendarController implements the CRUD actions for CalTask model.
 */
class CalendarController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
	{
		return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
		];
	}
    
    public function onAuthSuccess($client) {
 
        $reponse = $client->getUserAttributes(); 

        $session = \Yii::$app->session;
        //echo $client->accessToken->params['access_token']; exit;
        $token = $client->accessToken->params['access_token'];

        $session->set('google.token', $token);
        //$id = ArrayHelper::getValue($reponse , 'id');
        //$session->set('fb.id', $id);
        $session->set('google.id', $client->accessToken);
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['personal']);
       // }
    
    }
    
    public function actionGoogleout() {
        
        $session = \Yii::$app->session;
        
        $session->remove('google.token');
        $session->remove('google.id');
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['personal']);
       // }
    
    }

    /**
     * Lists all EgVisit models.
     * @return mixed
     */
    public function actionIndex()  {
        
		/*if( count(array_intersect(["eventPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        $model = new Visit();
        return $this->render('index', [
            'grants' => $this->module->params['grants'],
			'model' => $model
        ]);
    }
	/*
	public function actionScheduleData($doctor)
    {
        $model=new Visit;
        $where = '';
       if(isset($_GET['start']))   $where .= " and visit_day>='".date('Y-m-d',$_GET['start'])."'";
        if(isset($_GET['end']))   $where .= " and visit_day<='".date('Y-m-d',$_GET['end'])."'";
        
        if(Yii::app()->user->roleSym == 'A') {
            if($doctor != 0)
                $visitData = $model->findAll(array('condition'=>'visit_id=parent_id and status_id not in (6,7) and doctor_id = '.$doctor.$where, 'select'=>'parent_id as id,title,start,end,color,textColor,borderColor,backgroundColor,className'));            
            else
                $visitData = $model->findAll(array('condition'=>'visit_id=parent_id and status_id not in (6,7) '.$where, 'select'=>'parent_id as id,title,start,end,color,textColor,borderColor,backgroundColor,className'));
        } else
            $visitData = $model->findAll(array('condition'=>'status_id not in (6,7) and doctor_id='.Yii::app()->user->getId(),'select'=>'visit_id as id,title,start,end,color,textColor,borderColor,backgroundColor,className'));
		//$query = "SELECT id,title,start, end FROM events";
		$arr = array();
		foreach($visitData as $row){
			 $arr[] = $row; 
		}

        echo CJSON::encode($arr);  
		//echo json_encode($arr); 
    }
	*/
   
	public function actionScheduledata() {
		Yii::$app->response->format = Response::FORMAT_JSON; 
		$res = [];
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
        /* gmdate('Y-m-d', $_GET['start']) */        
        $events = Visit::find()->where(['status' => '1']);
		$events->andWhere('(id = id_parent_fk or id_parent_fk is null) ');
        $events->andWhere('visit_day >= \''. gmdate('Y-m-d', $_POST['start']).'\'');
		$events->andWhere('visit_day <= \''. gmdate('Y-m-d', $_POST['end']).'\'');

        if(isset($_POST['confirmVisit']) && !empty($_POST['confirmVisit']) ) { 
            if($_POST['typeEvent'] != 0)
                $events->andWhere('is_confirm = '.$_POST['confirmVisit']);
        } 
        if(isset($_POST['statusVisit']) && !empty($_POST['statusVisit']) ) { 
            $events->andWhere('id_dict_eg_visit_status_fk = '.$_POST['statusVisit']);
        } else {
            $events->andWhere('id_dict_eg_visit_status_fk not in (6,7)');
        }
        if(isset($_POST['patient']) && !empty($_POST['patient']) ) { 
            $events->andWhere('id_patient_fk = '.$_POST['patient']);
        } 
        if(isset($_POST['doctor']) && !empty($_POST['doctor']) ) { 
            $events->andWhere('id_doctor_fk = '.$_POST['doctor']);
        } 
       
		$events = $events->all(); 
        $presentationAll = Visit::Viewer();
		foreach($events as $key=>$value) {
            $bgColor = '#8447F6';
            $presentation = $presentationAll[$value['id_dict_eg_visit_status_fk']];
            $bgColor = $presentation['hashColor'];
            if($value['id_dict_eg_visit_status_fk'] == 3) {
                if(!$value['is_confirm'])
                    $presentation['icon'] = 'exclamation-circle';
            }
			
			/*if($value->doctor) {
				if($value->doctor['custom_data'])
					$bgColor = $value->doctor['custom_data'];
			}*/
            
            $company = \backend\Modules\Company\models\Company::findOne(1);
            $customData = \yii\helpers\Json::decode($company->custom_data);
            if( isset($customData['calendar']) ) {
                $calendarColorConfirmed = ($customData['calendar']['calendarColorConfirmed']) ? $customData['calendar']['calendarColorConfirmed'] : '#ffffff';
                $calendarColorUnconfirmed = ($customData['calendar']['calendarColorUnconfirmed']) ? $customData['calendar']['calendarColorUnconfirmed'] : '#000000';
                $calendarColorDone = ($customData['calendar']['calendarColorDone']) ? $customData['calendar']['calendarColorDone'] : '#ffe3ad';
            } else { 
                $calendarColorConfirmed = '#ffffff';
                $calendarColorUnconfirmed = '#000000';
                $calendarColorDone = '#ffe3ad';
            }
		    
			array_push($res, array('title' => $value->patient['fullname'].(($value->additional_info) ? (' + '.$value->additional_info) : '' ), 
								   'start' => $value->start, 
								   'end' => $value->end,
								   'constraint' => 'freeTerm', 
								   //'color' => '#f5f5f5', 
								   'textColor' => ($value->id_dict_eg_visit_status_fk == 5) ? $calendarColorDone : ((!$value->is_confirm) ? $calendarColorUnconfirmed : $calendarColorConfirmed),
								   'backgroundColor' => $bgColor,
                                   'borderColor' => ($value->id_dict_eg_visit_status_fk == 5) ? '#000' : $bgColor,
								   'className' => ($value->is_confirm) ? 'fc-event-bold' : 'fc-event',
								   'id' => $value['id'], //CustomHelpers::encode($value['id']),
								   //'allDay' => ($value['all_day'] == 1 || !$value['event_time']) ? true : false,
								   'icon' => $presentation['icon']
								)
			);
		}       
		
		return $res;
	}
	
    /**
     * Finds the Visit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = Visit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Task\models\Note();
        
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->id_type_fk = 2;
            $note->id_case_fk = $model->id_case_fk;
            $note->id_task_fk = $model->id;
            $note->id_employee_from_fk = CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }

    public function actionExport() {
		$date = (isset($_GET['visits_date'])) ? $_GET['visits_date'] : date('Y-m-d');
        $isConfirm = (isset($_GET['visits_confirm']) && $_GET['visits_confirm'] == 'on') ? true : false;
        $query = (new \yii\db\Query())->from('{{%eg_visit}} as v')
            ->where( ['v.status' => 1, 'visit_day' => $date] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( $isConfirm ) {
            $query = $query->andWhere('v.is_confirm = 1');
        }
		$count = $query->count();
		
		$query->select(['v.id', 'v.start', 'v.end', 'id_dict_eg_visit_status_fk', 'v.additional_info', 'v.note', 'v.is_confirm', 'p.id as pid', "concat_ws(' ', p.lastname, p.firstname) as patient", 'p.phone', 'p.phone_additional', 'p.email', "concat_ws(' ', d.lastname, d.firstname) as doctor", 'p.created_by', 'p.created_at'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%eg_patient}} as p', 'p.id = v.id_patient_fk');
		$query->join(' JOIN', '{{%company_employee}} as  d', 'd.id = v.id_doctor_fk');
			  
        /*$query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )*/
        $query->limit( 300 )->orderBy('v.start ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        $presentationAll = Visit::Viewer();
        
        //$dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = "wizyty_".$date.'_'.(($isConfirm) ? 'potwierdzone' : 'wszystkie').".pdf";
  
        // instantiate and use the dompdf class
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->renderPartial('printall', array('model' => $rows), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
        $dompdf->render();
        
       /* if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {*/
            $dompdf->stream($filename, array("Attachment" => true));
       /* }*/
		
	}
    
    public function actionTimeline() {
        return $this->render('_timeline');
    }
    
    public function actionGoogle() {
        //kontakt.kapisoft@gmail.com
        //1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com 
        //Ma6OjeQJkreNBO_uRKiA6yiO 
        $client = new \Google_Client();
        $client->setApplicationName("Google Calendar PHP Starter Application");
        $client->setClientId('1010314166620-3co6ba00rl0gd41gnn6k1vfbso8a0k12.apps.googleusercontent.com');
        $client->setClientSecret('Ma6OjeQJkreNBO_uRKiA6yiO');
        $client->setRedirectUri('http://lawfirm.dev/task/calendar/personal'); //I made a file called "worked.html" in the same directory that just says "it worked!"
        $client->setDeveloperKey('SecretLongDeveloperKey');
        $cal = new \Google_Service_Calendar_Calendar($client);
        
        if ($client->getAccessToken()){

            $session->set('google.token', $client->getAccessToken());
            $session->set('google.id', 1);

        }

    }
}

