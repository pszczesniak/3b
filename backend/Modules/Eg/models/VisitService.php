<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eg_visit_service}}".
 *
 * @property int $id
 * @property int $id_service_fk
 * @property int $id_visit_fk
 * @property double $payment
 * @property double $paid
 * @property double $discount
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class VisitService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_visit_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_service_fk', 'id_visit_fk', 'id_tooth_fk', 'tooth_no', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['payment', 'paid', 'discount'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id_service_fk', 'id_visit_fk'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_service_fk' => 'Usługa',
            'id_tooth_fk' => 'Ząb',
            'tooth_no' => 'Ząb',
            'id_visit_fk' => 'Id Visit Fk',
            'payment' => 'Cena',
            'paid' => 'Zapłacono',
            'discount' => 'Rabat',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
        ];
	}
    
    public function getService() {
        return \backend\Modules\Eg\models\Service::findOne($this->id_service_fk);
    }
    
    public function getVisit() {
        return \backend\Modules\Eg\models\Visit::findOne($this->id_visit_fk);
    }
}
