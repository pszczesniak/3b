<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eg_visit_type}}".
 *
 * @property int $id
 * @property string $type_name
 * @property string $type_symbol
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_visit_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['type_name', 'type_symbol'], 'string', 'max' => 255],
            [['type_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_name' => 'Nazwa',
            'type_symbol' => 'Symbol',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
        ];
	}
    
    public static function getList() {
        return self::find()->where(['status' => 1, 'is_active' => 1])->orderby('type_name')->all();
    }
    
    public static function listAttributes() {
        $options = [];
        $services = self::find()->where(['status' => 1, 'is_active' => 1])->orderby('type_name')->all();
        foreach($services as $key => $value) {
            $options[$value->id] = ['data-price' => $value->service_price];
        }
        
        return $options;
    }
}
