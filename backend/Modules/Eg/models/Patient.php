<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Eg\models\Visit;

/**
 * This is the model class for table "{{%eg_client}}".
 *
 * @property int $id
 * @property int $id_user_fk
 * @property int $id_group_fk
 * @property int $id_status_fk
 * @property string $firstname
 * @property string $lastname
 * @property string $birthdate
 * @property string $email
 * @property string $phone
 * @property string $phone_additional
 * @property int $confirm
 * @property int $force
 * @property string $note
 * @property string $reminder_date
 * @property string $reminder_note
 * @property int $fk_staff_verify_id
 * @property string $verify_date
 * @property int $id_doctor_fk
 * @property string $first_logged_date
 * @property string $last_logged_date
 * @property string $visit_date
 * @property int $term_id
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Patient extends \yii\db\ActiveRecord
{
    const SCENARIO_REGISTER = 'register';
    
    public $repeat_email;
    public $password;
    public $repeat_password;
    
    public $user_action;
    
    public $rules;
    public $accepts_1;
    public $accepts_2;
    public $accepts_3;
    
    public $c_account;
    public $c_send;
	
	public $startDate;
	public $endDate;
	public $place_postalcode;
	public $place_city;
	public $place_address;
	public $id_visit_type_fk;
	public $id_package_fk;
	public $term_date;
	public $term_note;
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%eg_patient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user_fk', 'id_group_fk', 'id_status_fk', 'confirm', 'force', 'fk_staff_verify_id', 'id_doctor_fk', 'term_id', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_visit_type_fk', 'id_package_fk'], 'integer'],
            [['birthdate', 'reminder_date', 'verify_date', 'first_logged_date', 'last_logged_date', 'visit_date', 'created_at', 'updated_at', 'deleted_at', 'term_date'], 'safe'],
            [['phone_additional', 'note', 'reminder_note', 'city', 'postal_code', 'address', 'place_postalcode', 'place_city'], 'string'],
            [['firstname', 'lastname', 'email', 'phone'], 'string', 'max' => 255],
            [['firstname', 'lastname', 'email'], 'required'],
            [['email', 'password', 'repeat_password', 'repeat_email', 'id_visit_type_fk', 'term_date', 'place_postalcode', 'place_city'], 'required', 'on' => self::SCENARIO_REGISTER],
			[['email'], 'unique'],
			['email', 'validateUser', 'on' => self::SCENARIO_REGISTER],
        ];
    }
    public function validateUser($attribute, $params) {
        if ( \common\models\User::find()->where(['email' => $this->email])->count() >  0) {
			//->andWhere('id != '.$id)->count() >  0) {
            $this->addError($attribute, 'Użytkownik o wskazanym adresie e-mail już istnieje');
        }
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
		$scenarios['default'] = ['firstname', 'lastname', 'email', 'phone'];
        $scenarios[self::SCENARIO_REGISTER] = ['email', 'firstname', 'lastname', 'password', 'repeat_password', 'repeat_email', 'accepts_1',
											   'term_date', 'id_visit_type_fk', 'id_package_fk', 'place_postalcode', 'place_city', 'place_address'];
        
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user_fk' => 'Id User Fk',
            'id_group_fk' => 'Grupa',
            'id_status_fk' => 'Status',
            'firstname' => 'Imię',
            'lastname' => 'Nazwisko',
            'birthdate' => 'Birthdate',
            'email' => 'Email',
            'phone' => 'Telefon',
            'phone_additional' => 'Telefon [dodatkowy]',
            'confirm' => 'Confirm',
            'force' => 'Force',
            'note' => 'Note',
            'reminder_date' => 'Reminder Date',
            'reminder_note' => 'Reminder Note',
            'fk_staff_verify_id' => 'Fk Staff Verify ID',
            'verify_date' => 'Verify Date',
            'id_doctor_fk' => 'Lekarz prowadzący',
            'first_logged_date' => 'First Logged Date',
            'last_logged_date' => 'Last Logged Date',
            'visit_date' => 'Visit Date',
            'term_id' => 'Term ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'postal_code' => 'Kod',
            'city' => 'Miasto',
            'postal_code' => 'Kod pocztowy',
            'address' => 'Adres',
            'password' => 'Hasło',
            'repeat_password' => 'Powtórz hasło',
            'repeat_email' => 'Powtórz email',
			'place_city' => 'Miejscowość', 
            'place_postalcode' => 'Kod pocztowy', 
            'place_address' => 'Adres',
            'id_package_fk' => 'Pakiet',
			'id_visit_type_fk' => 'Rodzaj imprezy',
			'term_date' => 'Data imprezy'
        ];
    }
	
	public function beforeSave($insert) {
        $this->lastname = trim($this->lastname);
        $this->firstname = trim($this->firstname);
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				if(!$this->id_status_fk) $this->id_status_fk = 1;
				$this->created_by = ($this->scenario == self::SCENARIO_REGISTER) ? -1 : \Yii::$app->user->id;
                
                if($this->scenario == self::SCENARIO_REGISTER || $this->user_action == 'create') {
                    $user = new \common\models\User();
                    $user->generateAuthKey();
                    $user->status = 10;
                    $user->email = $this->email;
                    $user->username = $this->email;
                    $user->roleType = 'client';
                    $user->firstname = $this->firstname;
                    $user->lastname = $this->lastname;
                    if($this->user_action == 'create')
                        $this->password = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
                    $user->setPassword($this->password);
                    
                    if($user->save()) { 
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('client');
                        $auth->assign($role, $user->id);
                        
                        $this->id_user_fk = $user->id;
                        $this->created_by = $user->id;
                        try {
                            \Yii::$app->mailer->compose(['html' => 'clientRegistration-html', 'text' => 'clientRegistration-text'], ['login' => $user->username, 'password' => $this->password, 'username' => ($this->firstname.' '.$this->lastname) ])
                            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->name ])
                            ->setTo($this->email)
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Potwierdzenie rejestracji w serwisie '.Yii::$app->name)
                            ->send();
                        } catch (\Swift_TransportException $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                            $request = Yii::$app->request;
                            /*$log = new \common\models\Logs();
                            $log->id_user_fk = Yii::$app->user->id;
                            $log->action_date = date('Y-m-d H:i:s');
                            $log->action_name = 'resource-booking';
                            $log->action_describe = $e->getMessage();
                            $log->request_remote_addr = $request->getUserIP();
                            $log->request_user_agent = $request->getUserAgent(); 
                            $log->request_content_type = $request->getContentType(); 
                            $log->request_url = $request->getUrl();
                            $log->save();*/
                        }
                    } else {
                        //var_dump($user->getErrors()); exit;
                        return false;
                    }
                } /*else if ($this->user_action == 'create') {
					$user = new \common\models\User();
                    $user->generateAuthKey();
                    $user->status = 10;
                    $user->email = $this->email;
                    $user->username = $this->email;
                    $user->roleType = 'client';
                    $user->firstname = $this->firstname;
                    $user->lastname = $this->lastname;
                    //$this->password = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
                    $user->setPassword($this->password);
                    
                    if($user->save()) { 
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('client');
                        $auth->assign($role, $user->id);
                        
                        $this->id_user_fk = $user->id;
                        $this->created_by = $user->id;
		            }
				}*/
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
        ];
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
			if($this->scenario == self::SCENARIO_REGISTER) {
				$term = new Visit();
				$term->user_action = 'booking';
				$term->id_dict_eg_visit_status_fk = 1;
				$term->id_patient_fk = $this->id;
				$term->id_package_fk = $this->id_package_fk;
				$term->id_visit_type_fk = $this->id_visit_type_fk;
				$term->visit_day = $this->term_date;
				$term->place_city = $this->place_city;
				$term->place_postalcode = $this->place_postalcode;
				$term->place_address = $this->place_address;
				$term->save();
			}
        } else {
            if(isset($changedAttributes['email']) && $changedAttributes['email'] != $this->email) {
                $user = \common\models\User::findOne($this->id_user_fk);
                if($user) {
                    $user->email = $this->email;
                    $user->save();
                }
            } 
        }
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function getFullname() {
		return $this->lastname.' '.$this->firstname;
	}
	
	public static function getList() {
		return self::find()->where(['status' => 1])->orderby('lastname')->all();
	}
    
    public static function getListShort() {
		return self::find()->where(['status' => 1])->orderby('lastname')->limit(50)->all();
	}
	
	public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 2])->orderby('id desc')->all();
        return $filesData;
    }
	
	public function getNotes() {
        $notesData = \backend\Modules\Task\models\Note::find()->where(['status' => 1, 'id_case_fk' => $this->id, 'id_type_fk' => 0])->orderby('id desc')->all();
        return $notesData;
    }
    
    public function getDoctor() {
        return \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_doctor_fk);
    }
    
    public function getFulladdress() {
        $fulladdress = $this->address;
        
        if($fulladdress) {
            if($this->postal_code) {
                $fulladdress .= ', '.$this->postal_code;
                
                if($this->city)
                    $fulladdress .= ' '.$this->city;
            } else {
                if($this->city)
                    $fulladdress .= ', '.$this->city;
            }
        }
        
        return ($fulladdress) ? $fulladdress : '<i class="text--grey">brak danych</i>';
    }
    
    public function getUser() {
        return ($this->id_user_fk) ? \common\models\crm\CUser::findOne($this->id_user_fk) : false;
    }
    
    public function getAccounts() {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT sum(case when status=10 then 1 else 0 end) as accounts_active, sum(case when status=10 then 0 else 1 end) as accounts_inactive  FROM {{%c_user}}
            WHERE id in (select id_user_fk from {{%customer_person}} where status = 1 and id_customer_fk = :customerId) ',
            [':customerId' => $this->id]);

        $accounts = $command->queryOne();
        if(!$accounts['accounts_active']) $accounts['accounts_active'] = 0;
        if(!$accounts['accounts_inactive']) $accounts['accounts_inactive'] = 0;
        
        return $accounts;
    }
    
    public function getPresentation() {
		if(!$this->state) $this->state = 1;
        return isset(self::Viewer()[$this->state]) ? self::Viewer()[$this->state] : self::Viewer()[1];
	}
    
    public static function Viewer() {
		$dictTabs = [
                1 => ['label' => 'nowy', 'color' => 'blue', 'icon' => 'exclamation-circle'],
                3 => ['label' => 'zweryfikowany', 'color' => 'purple', 'icon' => 'check'],
                //4 => ['label' => 'aktywny', 'color' => 'green', 'icon' => 'smile'],
                //5 => ['label' => 'zawieszony', 'color' => 'red', 'icon' => 'ban'],
                6 => ['label' => 'usunięty', 'color' => 'red', 'icon' => 'trash'],
            ];
		return $dictTabs;
	}
	
	public static function getStates() {
		return [1 => 'niezweryfikowany', 3 => 'zweryfikowany', 4 => 'aktywny', 5 => 'zawieszony', 6 => 'usunięty'];
	}
    
    public static function listCategory() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 24])->orderby('name')->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public static function getVisits($id) {
        //return \backend\Modules\Eg\models\Vist::find()->where(['id_patient_fk' => $id])->orderby('id desc')->all();
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT v.id, v.start, v.visit_day, v.end, v.note, v.id_dict_eg_visit_status_fk, v.id_gallery_fk, t.type_name '
                                            .' FROM {{%eg_visit}} v join {{%eg_visit_type}} t on t.id = v.id_visit_type_fk '
                                            .' WHERE v.status = 1 and id_patient_fk = :patientId ', [':patientId' => $id]);

        return $command->queryAll();
    }
}
