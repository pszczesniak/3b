<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eg_tooth_history}}".
 *
 * @property int $id
 * @property int $id_tooth_fk
 * @property int $id_visit_fk
 * @property int $id_dict_tooth_action_fk
 * @property string $note
 * @property string $created_at
 * @property int $created_by
 */
class ToothHistory extends \yii\db\ActiveRecord
{
    public $services;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_tooth_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tooth_fk', 'id_visit_fk', 'id_dict_tooth_action_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['price'], 'number'],
            [['note'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'services'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tooth_fk' => 'Ząb',
            'id_visit_fk' => 'Wizyta',
            'id_dict_tooth_action_fk' => 'Status zęba',
            'note' => 'Notatka',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'services' => 'Zabiegi'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
        ];
	}
}
