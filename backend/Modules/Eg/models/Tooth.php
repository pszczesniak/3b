<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eg_client_tooth}}".
 *
 * @property int $id
 * @property int $id_client_fk
 * @property int $id_dict_tooth_status_fk
 * @property string $note
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Tooth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_patient_tooth}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tooth_no', 'id_patient_fk', 'id_dict_tooth_status_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['note'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_patient_fk' => 'Pacjent',
            'id_dict_tooth_status_fk' => 'Status zęba',
            'note' => 'Note',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
        ];
	}
    
    public static function listStates() {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 12])->all();
        $states = [];
        foreach($items as $key => $item) { 
            $states[$item->id] = $item->name;
        }
        return $states;
    }
    
    public function getDict() {
        return \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_tooth_status_fk);
    }
    
    public function getNotes() {
        return \backend\Modules\Eg\models\ToothHistory::find()->where(['id_tooth_fk' => $this->id])->all();
    }
    
    public static function listTooths() {
        $tooths = [];
        for($i = 1; $i <= 4; ++$i) {
            for($j = 1; $j <= 8; ++$j) {
                $tooths[$i.$j] = $i.$j;
            }
        }
        
        return $tooths;
    }
    
    public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 4])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getHistory() {
        $historySql = "select h.created_at, concat_ws(' ', lastname, firstname) as creator, h.note "
                        ." from {{%eg_patient_tooth}} t join {{%eg_tooth_history}} h on h.id_tooth_fk = t.id join {{%user}} u on u.id = h.created_by"
                        ." where h.status = 1 and t.id = ".$this->id
                        ." order by h.id desc";
        $history = \Yii::$app->db->createCommand($historySql)->queryAll();
        
        return $history;
    }
}
