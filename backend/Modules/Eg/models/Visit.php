<?php

namespace backend\Modules\Eg\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eg_visit}}".
 *
 * @property int $id
 * @property int $type_fk
 * @property int $id_patient_fk
 * @property int $id_doctor_fk
 * @property int $is_confirm
 * @property string $visit_date
 * @property string $visit_day
 * @property string $visit_time
 * @property int $visit_duration
 * @property int $id_parent_fk
 * @property string $note
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class Visit extends \yii\db\ActiveRecord
{
    public $user_action;
    public $fromDate;
    public $fromTime;
    public $toDate;
    public $toTime;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_visit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_visit_type_fk', 'id_patient_fk', 'id_doctor_fk', 'is_confirm', 'visit_duration', 'id_parent_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_gallery_fk'], 'integer'],
            [['visit_date', 'visit_day', 'created_at', 'updated_at', 'deleted_at', 'fromDate', 'toDate', 'fromTime', 'toTime',], 'safe'],
            [['fromDate'], 'required', 'message' => 'Proszę podać datę rozpoczęcia wizyty', 'when' => function($model) { return ($model->user_action == 'create' || $model->user_action == 'change'); }],
            [['fromTime'], 'required', 'message' => 'Proszę podać godzinę rozpoczęcia wizyty', 'when' => function($model) { return $model->toTime && ($model->user_action == 'create' || $model->user_action == 'change'); }],
            [['toDate'], 'required', 'message' => 'Proszę podać datę zakończenia wizyty', 'when' => function($model) { return ($model->user_action == 'create' || $model->user_action == 'change'); }],
            //[['toTime'], 'required', 'message' => 'Proszę podać godzinę zakończenia wizyty', 'when' => function($model) { return ($model->user_action == 'create' || $model->user_action == 'change'); }],
            [['end'], 'validateDates', 'when' => function($model) { return ($model->user_action == 'create' || $model->user_action == 'change'); }],
            [[/*'start', 'end', */'id_patient_fk', 'id_doctor_fk', 'id_visit_type_fk'], 'required', 'when' => function($model) { return ($model->user_action == 'create' || $model->user_action == 'change'); }],
            [[/*'start', 'end', */'visit_day', 'id_visit_type_fk', 'place_city', 'place_postalcode'], 'required', 'when' => function($model) { return ($model->user_action == 'booking'); }],
            [['additional_info', 'access_code', 'id_gallery_fk'], 'required', 'when' => function($model) { return ($model->user_action == 'gallery'); }],
            [['note', 'additional_info', 'place_city', 'place_postalcode', 'place_address', 'access_code'], 'string'],
            [['visit_time'], 'string', 'max' => 255],
            [['payment_on_account'], 'number']
        ];
    }
    
    public function validateDates(){
		if($this->toTime) {
            if($this->end && $this->start) {
                if($this->end && strtotime($this->end) <= strtotime($this->start)){
                    //$this->addError('date_from','Please give correct Start and End dates');
                    $this->addError('date_to', 'Wprowadź poprawną datę końca');
                }
            }
        } else {
            if($this->toDate && $this->fromDate) {
                if($this->toDate && strtotime($this->toDate) < strtotime($this->fromDate)){
                    //$this->addError('date_from','Please give correct Start and End dates');
                    $this->addError('date_to', 'Wprowadź poprawną datę końca');
                }
            }
        }
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_fk' => 'Type Fk',
            'id_visit_type_fk' => 'Rodzaj imprezy',
            'id_patient_fk' => 'Klient',
            'id_doctor_fk' => 'Pracownik',
            'is_confirm' => 'Is Confirm',
            'visit_date' => 'Visit Date',
            'visit_day' => 'Dzień imprezy',
            'visit_time' => 'Visit Time',
            'visit_duration' => 'Visit Duration',
            'id_parent_fk' => 'Id Parent Fk',
            'note' => 'Note',
            'id_dict_eg_visit_status_fk' => 'Status',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'additional_info' => 'Nazwa galerii', //'Dodatkowa informacja',
            'place_city' => 'Miejscowość', 
            'place_postalcode' => 'Kod pocztowy', 
            'place_address' => 'Adres',
            'id_package_fk' => 'Pakiet',
            'payment_on_account' => 'Zaliczka',
            'access_code' => 'Kod dostępu do galerii'
        ];
    }
	
	public function beforeSave($insert) {
        if($this->user_action == 'booking') {
            $this->start = $this->visit_day.' 00:00:00';
        }
        
        if($this->start) {
            $this->visit_date = $this->start;
            $this->visit_day = date('Y-m-d', strtotime($this->start));
            $this->visit_time = date('H:i', strtotime($this->start));
        } else {
            $this->visit_day = $this->fromDate;
            $this->visit_time = $this->fromTime;
        }
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
       /* if (!$insert) {
        }   */
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
            'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'Visit',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/ugallery/cover',
				//'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['basicdir'] . '/web/uploads/pages/cover',
                'url' => Yii::$app->params['frontend'] . '/uploads/ugallery/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
        ];
	}
    
    public static function getList($id) {
        $visits = [];
        $visitsSql = "select id, start, additional_info from {{%eg_visit}} where id_dict_eg_visit_status_fk < 6 and id_patient_fk = ".$id." and visit_day <= '".date('Y-m-d')."' order by start desc limit 8";
        $data = \Yii::$app->db->createCommand($visitsSql)->queryAll();
        foreach($data as $key => $visit) {
            $visits[$visit['id']] = date('Y-m-d H:i', strtotime($visit['start'])) . ( ($visit['additional_info']) ? '['.$visit['additional_info'].']' : '');
        }
        
        return $visits;
    }
	
	public function getPresentation() {
		if(!$this->state) $this->state = 1;
        return isset(self::Viewer()[$this->state]) ? self::Viewer()[$this->state] : self::Viewer()[1];
	}
    
    public static function Viewer() {
		$dictTabs = [
						1 => ['label' => 'nowy', 'color' => 'blue', 'icon' => 'exclamation-circle', 'hashColor' => '#03a9f3'],
						3 => ['label' => 'zaplanowany', 'color' => 'purple', 'icon' => 'check', 'hashColor' => '#ab8ce4'],
						5 => ['label' => 'zrealizowany', 'color' => 'green', 'icon' => 'smile', 'hashColor' => '#A0D65A'],
						6 => ['label' => 'odwołany', 'color' => 'orange', 'icon' => 'ban', 'hashColor' => '#f9892e'],
                        7 => ['label' => 'usunięta', 'color' => 'red', 'icon' => 'trash', 'hashColor' => '#FC5B3F'],
				    ];
		return $dictTabs;
	}
	
	public static function getStates() {
		return [1 => 'nowy', 3 => 'zaplanowany', 5 => 'zrealizowany', 6 => 'odwołany'/*, 7 => 'usunięta'*/];
	}
    
    public function getPatient() {
		return $this->hasOne(\backend\Modules\Eg\models\Patient::className(), ['id' => 'id_patient_fk']);
    }
	
	public function getDoctor() {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_doctor_fk']);
    }
	
	public function getFiles() {
		return [];
	}
	
	public function getNotes() {
		return [];
	}
    
    public function getSaldo() {
        $paymentSql = "select sum(payment) as payment, sum(paid) as paid, sum(discount) as discount from {{%eg_visit_service}} where status = 1 and id_visit_fk = ".$this->id;
        $data = \Yii::$app->db->createCommand($paymentSql)->queryOne();
        return ($data['payment'] - $data['discount']) ? number_format($data['payment'] - $data['discount'], 2, '.', ' ') : '0.00';
    }
    
    public function getCreator() {
        return \common\models\User::findOne($this->created_by)->fullname;
    }
}
