<?php

namespace backend\Modules\Eg\models;

use Yii;

/**
 * This is the model class for table "{{%eg_visit_detail}}".
 *
 * @property int $id
 * @property int $id_visit_fk
 * @property string $detail_staff
 * @property string $detail_client
 * @property double $payment
 * @property double $paid
 * @property double $discount
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class VisitDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eg_visit_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_visit_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['detail_staff', 'detail_client'], 'string'],
            [['payment', 'paid', 'discount'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_visit_fk' => 'Id Visit Fk',
            'detail_staff' => 'Detail Staff',
            'detail_client' => 'Detail Client',
            'payment' => 'Payment',
            'paid' => 'Paid',
            'discount' => 'Discount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
