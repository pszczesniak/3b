<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kalendarz osobisty');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wykaz czynności'), 'url' => Url::to(['/task/personal/actions']) ];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-event-calendar', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-sm-9">
           
            <div id="personal"></div>
         </div> 
        <div class="col-sm-3">
            <?= $this->render('_formFilterPersonal', ['model' => $model]) ?>
        </div>
    </div>
<?php $this->endContent(); ?>
    
	
	
	

