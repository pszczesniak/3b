<?php
	use yii\helpers\Html;
    use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\helpers\ArrayHelper;
?>
<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-teal"> 
        <span class="panel-title"> <span class="fa fa-cog"></span> Opcje </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>

    <div class="panel-body in" id="calendarFilter">
        <div class="grid">
            <div class="col-xs-12">
                <fieldset><legend class="text--blue2"><i class="fa fa-print text--blue2"></i>Wydruk</legend>
                    <?= $this->render('_formExport') ?>
                </fieldset>
            </div>
            <div class="col-xs-12">
                <fieldset><legend class="text--blue2"><i class="fa fa-user-md text--teal2"></i>Pracownicy</legend>
                    <div id="external-events">
					<!--<div class="external-event ui-draggable" style="position: relative;">My Event 1</div>
					<div class="external-event ui-draggable" style="position: relative;">My Event 2</div>-->
					<?php
						$doctors = \backend\Modules\Company\models\CompanyEmployee::getDoctors();
						foreach( $doctors as $key => $doctor ) {
							echo '<div id="doctor-'.$doctor->id.'" alt="'.$doctor->custom_data.'" class="external-event ui-draggable" style="position: relative; font-size:1.2em; padding:8px 15px; color:#fff; background-color:'.$doctor->custom_data.'">'.$doctor->fullname.'</div>';
						}
					?>
					<!--<div class="external-event ui-draggable" style="position: relative; z-index: auto; left: 0px; top: 0px;">My Event 3</div>
					<div class="external-event ui-draggable" style="position: relative;">My Event 4</div>
					<div class="external-event ui-draggable" style="position: relative; z-index: auto; left: 0px; top: 0px;">My Event 5</div>-->
				
					
					<!--<p>
						<input type="checkbox" id="drop-remove">
						<label for="drop-remove">remove after drop</label>
					</p>-->
				</div>
                </fieldset>
            </div>
            <div class="col-xs-12">
                <fieldset><legend class="text--purple2"><i class="fa fa-filter text--purple2"></i>Filtrowanie</legend>
                    <form id="calendar-filtering">
					<?php $form = ActiveForm::begin([
						'action' => ['index'],
						'method' => 'post',
						'id' => 'form-data-search'
					]); ?>
                       
						<label class="control-label" for="visit-is_confirm">Potwierdzenie</label>
						<div class="form-select">
							<select id="visit-is_confirm" class="form-control schedule-filtering-options" name="Visit[is_confirm]">
								<option value=""> - wszystko -</option>
								<option value="1">Niepotwierdzone</option>
								<option value="0">Potwierdzone</option>
							</select>
						</div>
						<label class="control-label" for="visit-id_dict_eg_visit_status_fk">Status</label>
						<div class="form-select">
							<select id="visit-id_dict_eg_visit_status_fk" class="form-control schedule-filtering-options" name="Visit[id_dict_eg_visit_status_fk]">
								<option value=""> - wszystko -</option>
								<option value="1">Brak weryfikacji</option>
                                <option value="3">Zaplanowana</option>
								<option value="5">Zrealizowane</option>
                                <option value="6">Odwołane</option>
                                <!--<option value="7">Usunięte</option>-->
							</select>
						</div>
					
						<?= $form->field($model, 'id_patient_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Patient::getList(\Yii::$app->user->id), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'selectAutoComplete schedule-filtering-options',  'data-url' => Url::to('/eg/patient/autocomplete') ] ) ?>
						<?= $form->field($model, 'id_doctor_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getDoctors(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] ) ?>
					
                    <?php ActiveForm::end(); ?>
                </fieldset>
				
				<fieldset><legend class="text--grey2"><i class="fa fa-map text--grey2"></i>Legenda</legend>
					<table class="calendar-legend">
						<tr><td class="bg-blue calendar-legend-icon"><i class="fa fa-exclamation-circle"></i></td><td>Nowa [niepotwierdzona]</td></tr>
						<tr><td class="bg-purple calendar-legend-icon"><i class="fa fa-check"></i></td><td>Potwierdzona</td></tr>
						<tr><td class="bg-green calendar-legend-icon"><i class="fa fa-smile"></i></td><td>Zrealizowana</td></tr>
						<tr><td class="bg-orange calendar-legend-icon"><i class="fa fa-ban"></i></td><td>Odwołana</td></tr>
                        <tr><td class="bg-red calendar-legend-icon"><i class="fa fa-trash"></i></td><td>Usunięta</td></tr>
					</table>
				</fieldset>
            </div>        
        </div>
    </div>
</div>
