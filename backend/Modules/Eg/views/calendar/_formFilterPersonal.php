<?php
	use yii\helpers\Html;
    use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\helpers\ArrayHelper;
    
    use frontend\widgets\socialmedia\Google;
?>

<?php /*Google::widget([])*/ ?>

<!--<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-hand-o-up"></span> Dodaj zdarzenie </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarExternal" aria-expanded="true" aria-controls="calendarExternal">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>
    
    <div class="panel-body in" id="calendarExternal">
        <div id="external-events" class="list-group">
			<div class="fc-event list-group-item bg-orange btn-icon" data-type="3" ><i class="fa fa-handshake-o"></i>Spotkanie</div>
			<div class="fc-event list-group-item bg-pink btn-icon" data-type="4"><i class="fa fa-sun-o"></i>Urlop</div>
		</div>
        <div class="alert alert-info"><small>Wybierz zdarzenie z listy i przeciągnij je w odpowiednie miejsce na kalendarzu</small></div>
    </div> 
</div>-->
    
<div class="panel panel-default" style="width: 100%">
    <div class="panel-heading bg-grey"> 
        <span class="panel-title"> <span class="fa fa-cog"></span> Opcje </span> 
        <div class="panel-heading-menu pull-right">
            <a class="collapse-link collapse-window" data-toggle="collapse" href="#calendarFilter" aria-expanded="true" aria-controls="calendarFilter">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </div>
    </div>
 
    <div class="panel-body in" id="calendarFilter">
        <div class="grid">
            <div class="col-xs-12">
                <fieldset><legend class="text--blue2"><i class="fa fa-print text--blue2"></i>Eksport zadań osobistych</legend>
                    <?= $this->render('_formExportPersonal', ['model' => $model, 'employees' => []]) ?>
                </fieldset>
            </div>

            <div class="col-xs-12">
                <fieldset><legend class="text--teal2"><i class="fa fa-hand-up text--teal2"></i>Filtrowanie</legend>
                    <form id="calendar-filtering">
					<?php $form = ActiveForm::begin([
						'action' => ['index'],
						'method' => 'post',
						'id' => 'form-data-search'
					]); ?>
                        <div class="grid">
                            <div class="col-sm-6">
                                <label class="control-label" for="caltasksearch-type_fk">Rodzaj</label>
                                <div class="form-select">
                                    <select id="caltasksearch-type_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[type_fk]">
                                        <option value="0"> - wszystko -</option>
                                        <option value="1" selected>Osobiste</option>
                                        <option value="2">Kancelaryjne</option>
                                    </select>
                                </div>
                            </div>
                     
                            <div class="col-sm-6">
                                <label class="control-label" for="caltasksearch-id_dict_task_status_fk">Status</label>
                                <div class="form-select">
                                    <select id="caltasksearch-id_dict_task_status_fk" class="form-control schedule-filtering-options" name="CalTaskSearch[id_dict_task_status_fk]">
                                        <option value=""> - wszystko -</option>
                                        <option value="1">W trakcie</option>
                                        <option value="2">Zakończone</option>
                                        <option value="3">Odwołane</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 schedule-filtering-options' ] ) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['class' => 'form-control schedule-filtering-options select2' ] ) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </fieldset>
				
				<fieldset><legend class="text--grey2"><i class="fa fa-map text--grey2"></i>Legenda</legend>
					<table class="calendar-legend">
						<!--<tr><td class="bg-blue calendar-legend-icon"><i class="fa fa-user"></i></td><td>Osobiste - priorytet niski</td></tr>
						<tr><td class="bg-yellow calendar-legend-icon"><i class="fa fa-user"></i></td><td>Osobiste - priorytet średni</td></tr>
						<tr><td class="bg-red calendar-legend-icon"><i class="fa fa-user"></i></td><td>Osobiste - priorytet wysoki</td></tr>-->
                        <!--<tr><td colspan=2" class="align-center">Czynności administracyjne</td></tr>-->
                        <tr><td class="bg-grey calendar-legend-icon"><i class="fa fa-user"></i></td><td>Zadania osobiste</td></tr>
                        <tr><td class="bg-grey calendar-legend-icon"><i class="fa fa-hourglass"></i></td><td>TODO</td></tr>
                        <tr><td class="bg-orange calendar-legend-icon"><i class="fa fa-pie-chart"></i></td><td>Zarządzanie projektami</td></tr>
                        <tr><td class="bg-grey calendar-legend-icon"><i class="fa fa-calendar-check-o"></i></td><td>Czynności administracyjne</td></tr>
                        <tr><td class="bg-pink calendar-legend-icon"><i class="fa fa-flash"></i></td><td>Marketing</td></tr>
                        <tr><td class="bg-yellow calendar-legend-icon"><i class="fa fa-sun-o"></i></td><td>Urlop <a href="<?= Url::to(['/task/personal/fast', 'type' => 3]) ?>" title="Szybkie wprowadzanie" data-title="Rejestracja urlopu" data-target="#modal-grid-item" class="gridViewModal"><i class="fa fa-calendar-plus-o text--yellow"></i></a></td></tr>
                        <tr><td class="bg-red calendar-legend-icon"><i class="fa fa-calendar-times-o"></i></td><td>Dzień wolny</td></tr>
                        <tr><td class="bg-blue calendar-legend-icon"><i class="fa fa-heartbeat"></i></td><td>L4 <a href="<?= Url::to(['/task/personal/fast', 'type' => 4]) ?>" title="Szybkie wprowadzanie" data-title="Rejestracja L4" data-target="#modal-grid-item" class="gridViewModal"><i class="fa fa-calendar-plus-o text--blue"></i></a></td></tr>
                        <tr><td class="bg-orange calendar-legend-icon"><i class="fa fa-handshake-o"></i></td><td>Spotkanie</td></tr>
                        <tr><td class="bg-navy calendar-legend-icon"><i class="fa fa-briefcase"></i></td><td>Czynności merytoryczne na rzecz klienta</td></tr>
                        <tr><td class="bg-navy calendar-legend-icon"><i class="fa fa-folder-open"></i></td><td>Czynności pozamerytoryczne na rzecz klienta</td></tr>
                        <tr><td class="bg-navy calendar-legend-icon"><i class="fa fa-car"></i></td><td>Przejazd</td></tr>
						<tr><td class="bg-purple calendar-legend-icon"><i class="fa fa-gavel"></i></td><td>Kancelaryjne - rozprawy</td></tr>
						<tr><td class="label-danger calendar-legend-icon"><i class="fa fa-gavel"></i></td><td>Kancelaryjne - odwołane</td></tr>
						<tr><td class="bg-teal calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Kancelaryjne - zadania</td></tr>
						<tr><td class="label-success calendar-legend-icon"><i class="fa fa-tasks"></i></td><td>Kancelaryjne - zakończone</td></tr>
					</table>
				</fieldset>
            </div>        
        </div>
    </div>
</div>
