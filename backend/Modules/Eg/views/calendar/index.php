<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Terminarz wizyt');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-event-calendar', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-sm-9">
            <div id="timetable"></div>
         </div> 
        <div class="col-sm-3">
            <?= $this->render('_formFilter', ['model' => $model]) ?>
        </div>
    </div>
    
	
	
	
<?php $this->endContent(); ?>
