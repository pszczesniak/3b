<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="grid">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="grid profile">
            <div class="col-md-4 col-xs-12">
                <div class="profile-avatar">
                    <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/patient/cover/".$model->id."/preview.jpg"))?"/uploads/customers/cover/".$model->id."/patient.jpg":"/images/default-user.png"; ?>
                    <img src="<?= $avatar ?>" alt="Avatar">
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="profile-name">
                    <!-- <p class="balance"> Pensja: <span>146 PLN</span> </p> -->
                    <?php if($model->email) { ?> <a href="#" class="btn bg-teal btn-large mr10"> <i class="fab fa-telegram-plane"></i> wiadomość</a> <?php } ?>
                    &nbsp;<a href="<?= Url::to(['/eg/patient/update', 'id' => $model->id]) ?>" class="btn bg-blue viewModal" data-target="#modal-grid-item" data-title="Edycja profilu klienta"> <i class="fa fa-pencil-alt"></i> Edytuj</a>
                </div>
            </div>
            <div class="col-xs-12">
                <fieldset> <legend>Notatka</legend>
                    <p><?= ($model->note) ? $model->note : '<div class="alert alert-info">brak dodatkowego opisu</div>' ?></p>
                </fieldset>
            </div>
            <!--
            <div class="col-xs-12">
                <fieldset> <legend>Lekarz prowadzący</legend> 
                    <?php if(!$model->doctor) { echo '<div class="alert alert-warning">nie ustawiono</div>'; } else { ?>
                        <div class="grid profile">
                            <div class="col-md-4">
                                <div class="profile-avatar">
                                    <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->doctor['id']."/preview.jpg"))?"/uploads/employees/cover/".$model->doctor['id']."/preview.jpg":"/images/default-user.png"; ?>
                                    <img src="<?= $avatarContact ?>" alt="Avatar">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="profile-name">
                                    <h3><?= $model->doctor['fullname'] ?></h3>
                                    <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->doctor['kindname']) ? $model->doctor['typename'] : 'brak danych' ?></p>
                                    <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->doctor['phone']) ? $model->doctor['phone'] : 'brak danych' ?></p>
                                    <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->doctor['email']) ? Html::mailto($model->doctor['email'], $model->doctor['email']) : 'brak danych'  ?></p>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </fieldset>
            </div>  -->           
            <?php if(\Yii::$app->params['env'] == 'devv') { ?>
            <div class="col-md-12  col-xs-12">
                <fieldset> <legend>Konto użytkownika</legend>
                    <?php if(!$model->user) { ?>
                        <div class="align-center crm-account-create">
                            <a href="<?= Url::to(['/crm/customer/caccount', 'id' => $model->id]) ?>" class="btn bg-green viewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-user-plus'></i>Kreator konta"> <i class="fa fa-user-plus"></i> Utwórz konto </a>
                        </div><br />
                    <?php } ?>
                    <div class="meta-data">
                        <dl class="dl-horizontal crm-accounts-info">
                            <?php if($model->user) { ?>
                            <dt>Status:</dt>
                            <dd class="crm-accounts-info-status">
                                <span class="label label-<?= ($model->user['status'] == 10) ? 'success' : 'danger' ?>"><?= ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ?></span>
                                <a href="<?= Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]) ?>" class="btn btn-xs btn-<?= ($model->user['status'] == 10) ? 'danger' : 'success' ?> deleteConfirm" data-label="<?= ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ?>" title="<?= ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ?>"><?= ($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ?></a>
                            </dd>
                            <dt>Login:</dt> <dd><?= $model->user['username'] ?></dd>
                            <dt>Utworzono:</dt> <dd><?= date('Y-m-d H:i:s', $model->user['created_at']) ?></dd>
                            <!-- <dt>Utworzył:</dt> <dd>Joanna Pietras</dd> -->
                            <dt>Ostatnie logowanie:</dt> <dd><?= $model->user['lastlogin'] ?></dd>
                            <?php } ?>
                        </dl>
                    </div>
                </fieldset>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <fieldset> <legend>Dodatkowe informacje</legend>
            <div class="grid">
                <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Telefon</dt> <dd><?= ($model->phone) ? $model->phone : '<i class="text--grey">brak danych</i>' ?></dd> </dl> </div>
                <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Telefon [drugi]</dt> <dd><?= ($model->phone_additional) ? $model->phone_additional : '<i class="text--grey">brak danych</i>' ?></dd> </dl> </div>
                <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Email</dt> <dd><?=  ($model->email) ?  Html::mailto($model->email, $model->email) : '<i class="text--grey">brak danych</i>'  ?></dd> </dl> </div>
            </div>
            <div class="table-responsive about-table">
                <table class="table">
                    <tbody>
                        <tr> <th>Adres</th> <td><?= $model->fulladdress ?></td> </tr>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
</div>