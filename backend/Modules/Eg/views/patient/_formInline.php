<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'saveInlineFormPatient',
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'saveInlineForm', /*'data-target' => "#modal-grid-item", */'data-input' => '.patient-new'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="grid">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true, /*'class' => 'form-control uppercase'*/]) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true, /*'class' => 'form-control uppercase'*/]) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true, /*'class' => 'form-control uppercase'*/]) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true, /*'class' => 'form-control uppercase'*/]) ?></div>
    </div>
    
	<div class="text--red" id="errorValueHint"></div>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<button class="btn btn-sm btn-default closeInlineForm" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">

	document.getElementById("saveInlineFormPatient").onsubmit = function() {
		var data = new FormData(this);
		var http = new XMLHttpRequest();
		http.open('POST', '<?= Url::to(['/eg/patient/createinline']) ?>',true);
 
		http.onreadystatechange=function()  {
			if (http.readyState==4 && http.status == 200) {
				var result = JSON.parse(http.responseText);
				if(result.success) { 
					document.getElementById('patient-new-insert').classList.add('none');
					patientSelect = document.getElementById('selectAutoCompleteModal');
					patientSelect.innerHTML += "<option value='"+result.id+"'>"+result.name+"</option>";
					patientSelect.value = result.id;
                    $('#selectAutoCompleteModal').select2('data', {id: result.id, label: result.name });
                    $('#selectAutoCompleteModal').trigger('change');
                } else {
					document.getElementById("errorValueHint").innerHTML = result.alert;
				}
			} else {
				document.getElementById("errorValueHint").innerHTML = 'Wystąpił błąd. '+http.status;
			}
		};
 
		http.send(data); 
		return false;
	}    

</script>
