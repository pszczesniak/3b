<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\eg\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-eg-patients-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-patients']
    ]); ?>

    <div class="grid">
        <div class="col-md-3 col-sm-6 col-xs-6"> <?= $form->field($model, 'firstname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-patients', 'data-form' => '#filter-eg-patients-search' ]) ?></div>
        <div class="col-md-3 col-sm-6 col-xs-6"> <?= $form->field($model, 'lastname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-patients', 'data-form' => '#filter-eg-patients-search' ]) ?></div>
        <!--<div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'id_doctor_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getDoctors(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-patients', 'data-form' => '#filter-eg-patients-search'  ] ) ?></div> -->       
        <div class="col-md-3 col-sm-4 col-xs-6"><?= $form->field($model, 'id_status_fk')->dropDownList(\backend\Modules\Eg\models\Patient::getStates(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-patients', 'data-form' => '#filter-eg-patients-search' ] ) ?></div>
		<div class="col-md-3 col-sm-4 col-xs-6"><?= $form->field($model, 'id_group_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(-1), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-patients', 'data-form' => '#filter-eg-patients-search'  ] ) ?></div>        
        
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
