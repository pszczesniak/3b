<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use backend\widgets\files\FilesBlock;
use backend\widgets\tasks\CasesTable;
use backend\widgets\accounting\OrderTable;
use backend\widgets\sale\OffersTable;
use backend\widgets\chat\Comments;
use backend\widgets\term\TermsTable;
use backend\widgets\eg\Treatment;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Profil klienta';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => Url::to(['/eg/patient/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/eg/patient/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowy klient</a>
            <!--<a href="" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>-->
        </div>
    </div>
</div>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-view-add', 'title' => $this->title)) ?>
    <?php $notes = $model->notes; ?>
    <h3><span id="tmpl-name"><?= $model->fullname ?></span></h3>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="<?= (!isset($_GET['tab'])) ? "active" : "" ?>"><a data-toggle="tab" href="#tab1"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                <!--<li><a data-toggle="tab" href="#tab2"><i class="fa fa-heartbeat"></i><span class="panel-tabs--text">Leczenie</span></a></li>-->
				<li><a data-toggle="tab" href="#tab3"><i class="fa fa-calendar-alt"></i><span class="panel-tabs--text">Terminy</span></a></li>
                <li><a data-toggle="tab" href="#tab4"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>
                <li><a data-toggle="tab" href="#tab5"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty </span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane <?= (!isset($_GET['tab'])) ? "active" : "" ?>" id="tab1">
                    <div id="tmpl-info"><?= $this->render('_info', ['model' => $model]) ?></div>
                </div>
                <!--<div class="tab-pane" id="tab2">
                    <?php /* Treatment::widget([ 'dataUrl' => Url::to(['/eg/patient/visits', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/eg/visit/new', 'id' => $model->id]) ])*/ ?>
                </div>-->
			    <div class="tab-pane" id="tab3">
                    <?= TermsTable::widget([ 'dataUrl' => Url::to(['/eg/patient/visits', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/eg/visit/new', 'id' => $model->id]) ]) ?>
                </div>
                <div class="tab-pane" id="tab4">
                    <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/eg/patient/note', 'id' => $model->id])]) ?>
                </div>
                <div class="tab-pane" id="tab5">
                    <?php /*$this->render('_files', ['model' => $model, 'type' => 2, 'onlyShow' => true])*/ ?>
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
            </div>
        </div>
    </div>  

<?php $this->endContent(); ?>
