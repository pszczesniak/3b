<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord) ? 'modalAjaxForm' : '', 'data-target' => "#modal-grid-item"],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
        <div class="modal-body">
            <?= ($model->isNewRecord && count($model->getErrors()) > 0 ) ?  '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
            <div class="grid">
                <div class="col-sm-8 col-xs-12">
                    <fieldset><legend>Informacje personalne</legend>
                        <div class="grid">
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
                        </div>
                        <div class="grid">
                            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'phone_additional')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                        </div>
                        <div class="grid">
                            <div class="col-sm-3 col-xs-5"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-3 col-xs-7"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <!--<fieldset><legend>Lekarz prowadzący</legend>
                        <?= $form->field($model, 'id_doctor_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getDoctors(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2' ] ) ?>
                    </fieldset>-->
                    <fieldset><legend>Grupa</legend>
                        <?= $form->field($model, 'id_group_fk', ['template' => '
                                  {label}
                                   <div class="input-group ">
                                        {input}
                                        <span class="input-group-addon bg-green">'.
                                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/24' , 
                                                ['class' => 'insertInline text--white', 
                                                 'data-target' => "#patient-group-insert", 
                                                 'data-input' => ".patient-group"
                                                ])
                                        .'</span>
                                   </div>
                                   {error}{hint}
                               '])->dropDownList( \backend\Modules\Eg\models\Patient::listCategory(), ['prompt' => '- wybierz -', 'class' => 'form-control patient-group'] )->label(false) ?>
                        <div id="patient-group-insert" class="insert-inline bg-purple2 none"> </div>
                    </fieldset>
                    <fieldset><legend>Notatka</legend>
                        <?= $form->field($model, 'note')->textarea(['rows' => 4])->label(false) ?>
                    </fieldset>
                </div>
            </div>
        </div>
    
        <div class="modal-footer">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    
    <?php ActiveForm::end(); ?>

</div>
