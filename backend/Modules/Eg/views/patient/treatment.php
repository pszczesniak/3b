<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\CasesTable;
use frontend\widgets\accounting\OrderTable;
use frontend\widgets\sale\OffersTable;
use frontend\widgets\accounting\InvoicesTable;
use frontend\widgets\eg\VisitsTable;
use frontend\widgets\eg\Treatment;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Przebieg leczenia';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pacjenci'), 'url' => Url::to(['/eg/patient/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-view-add', 'title' => 'Profil pacjenta')) ?>
    <h3><?= $model->fullname ?>&nbsp;<a href="<?= Url::to(['/eg/patient/view', 'id' => $model->id]) ?>" class="btn bg-purple"> <i class="fa fa-pencil-alt"></i> Profil pacjenta</a></h3>
    <?= Treatment::widget(['patientId' => $model->id, 'dataUrl' => Url::to(['/eg/patient/history', 'id'=>$model->id]), 'insert' => true, 'exportUrlExcel' => Url::to(['/eg/tooth/export', 'id' => $model->id]), 'exportUrlPdf' => Url::to(['/eg/tooth/print', 'id' => $model->id]) ]) ?>

<?php $this->endContent(); ?>