<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\sale\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rodzaje imprez');
$this->params['breadcrumbs'][] = 'Biuro';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title' => Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-types" data-form="#filter-sale-types-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?= $this->render('_search', ['model' => $searchModel, 'type' => 'advanced']); ?>
        </div>
    </fieldset>
    
    <?= Alert::widget() ?>
	<div id="toolbar-types" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/eg/type/create']) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'type-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-types",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) == -1 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/eg/type/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-eg-types-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-types"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-types"
                data-toolbar="#toolbar-types" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-eg-types-search"
                data-url=<?= Url::to(['/eg/type/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name" data-sortable="true" >Nazwa</th>
                    <th data-field="symbol" data-sortable="true" >Symbol</th>
                    <th data-field="created"  data-sortable="true" data-align="center" >Utworzono</th>
                    <th data-field="active" data-align="center"><i class="fa fa-map-marker text--purple" data-title="Typ imprezy aktywny" data-toggle="tooltip"></i></th>
                    <th data-field="actions" data-events="actionEvents" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

