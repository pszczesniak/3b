<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = Yii::t('app', 'Nowy szablon');
$this->params['breadcrumbs'][] = 'Sprzedaż';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Szablony'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-create', 'title'=>Html::encode($this->title))) ?>
    <?= $this->render('_form', ['model' => $model, ]) ?>
<?php $this->endContent(); ?>



