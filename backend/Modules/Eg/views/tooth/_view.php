<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */

if($model->dict['custom_data']) {
    $customData = \yii\helpers\Json::decode($model->dict['custom_data']);
    $color = $customData['color'];
} else {
    $color = 'gray';
}
?>

<div class="modal-body">
    <?php if($model->dict) { ?>
    <h4>Status zęba: <span class="label" style="background-color: <?= $color ?>"><?= $model->dict['name'] ?></span></h4>
    <?php } ?>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <!--<li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>-->
                <li class="active"><a data-toggle="tab" href="#tab2b"><i class="fa fa-heartbeat"></i><span class="panel-tabs--text">Leczenie</span> </a></li>
                <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <!--<div class="tab-pane active" id="tab2a"> </div>-->
                <div class="tab-pane active" id="tab2b">
                    <table class="table table-striped table-border table-items header-fixed table table-hover table-scroll">
                        <thead><tr><th>Data</th><th>Pracownik</th><th>Notatka</th></tr></thead>
                        <tbody>
                            <?php if($model->history) { ?>
                                <?php foreach($model->history as $key => $item) { 
                                    echo '<tr><td>'.$item['created_at'].'</td><td>'.$item['creator'].'</td><td>'.$item['note'].'</td></tr>';
                                } ?>
                            <?php } else { ?>
                                <tr><td colspan="3"><div class="alert alert-info">brak wpisów</div></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab2c">
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">    
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Zamknij</button>
</div>

