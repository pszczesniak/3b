<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => '#table-treatment' ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
           // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
<div class="modal-body">
    <div class="grid">
        <?php if($patient) { ?>
        <div class="col-xs-12"><?= $form->field($model, 'id_visit_fk')->dropDownList( \backend\Modules\Eg\models\Visit::getList($patient), [] ) ?></div>
        <div class="col-xs-12"><?= $form->field($model, 'services')->dropDownList( ArrayHelper::map(\backend\Modules\Eg\models\Service::getList(), 'id', 'service_name'), ['class' => 'form-control chosen-select', 'multiple' => true] ) ?></div>
        <?php } ?>
        <div class="col-xs-12"><?= $form->field($model, 'id_dict_tooth_action_fk')->dropDownList( \backend\Modules\Eg\models\Tooth::listStates(), ['prompt' => ' - wybierz -' ] ) ?></div>
    </div>
	<?= $form->field($model, 'note')->textarea([/*'class' => 'mce-basic', */'placeholder' => 'Wpisz opis...']) ?>
</div>
<div class="modal-footer">    
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
