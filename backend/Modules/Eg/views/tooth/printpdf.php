<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
    <header id="header"><span class="page-number"></span></header>
    <div class="wrapper wrapper-content p-xl">
        <div class="ibox-content p-xl">
            <table class="table table-bordered">
                <thead><tr><th style="width: 40px; text-align:center;">Ząb</th><th style="width: 120px; text-align: center;">Data</th><th style="width: 180px; text-align: center;">Utworzył(a)</th><td style="text-align: center;">Opis</th></tr></thead>
                <tbody>
                    <?php foreach($model as $key => $item) {
                        echo '<tr><td style="text-align: center;">'.$item['tooth_no'].'</td><td style="text-align: center;">'.$item['created_at'].'</td><td>'.$item['creator'].'</td><td>'.$item['note'].'</td></tr>';
                    } ?>
                </tbody>
                <tfoot>
                    <tr><td colspan="4"><small>Data wydruku: <i><?= date('Y-m-d H:i:s')  ?></i></small></td></tr>
                </tfoot>
            </table>
        </div>
    </div>
    
</body>
</html>