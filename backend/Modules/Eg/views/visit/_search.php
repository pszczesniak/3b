<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\eg\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-eg-visits-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-visits']
    ]); ?>

    <div class="grid">
        <div class="col-md-2 col-sm-6 col-xs-6">
			<div class="form-group field-visit-start">
				<label for="visit-start" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="visit-start" name="Visit[start]" value="<?= $model->start ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-6 col-xs-6">
			<div class="form-group field-visit-end">
				<label for="visit-end" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="visit-end" name="Visit[end]" value="<?= $model->end ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-12 col-xs-12"><?= $form->field($model, 'is_confirm')->dropDownList( [0 => 'niepotwierdzona', 1 => 'potwierdzona'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-visits', 'data-form' => '#filter-eg-visits-search' ] )->label('Stan') ?></div>        
        <div class="col-md-3 col-sm-6 col-xs-12"><?= $form->field($model, 'id_doctor_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getDoctors(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-visits', 'data-form' => '#filter-eg-visits-search' ] ) ?></div>
        <div class="col-md-3 col-sm-6 col-xs-12"><?= $form->field($model, 'id_patient_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Eg\models\Patient::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list selectAutoComplete',  'data-url' => Url::to('/eg/patient/autocomplete'), 'data-table' => '#table-visits', 'data-form' => '#filter-eg-visits-search' ] ) ?></div>
        <div class="col-md-12 col-sm-12 col-xs-12"><?= $form->field($model, 'id_dict_eg_visit_status_fk')->dropDownList( \backend\Modules\Eg\models\Visit::getStates(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list chosen-select', 'data-table' => '#table-visits', 'data-form' => '#filter-eg-visits-search', 'multiple' => true ] ) ?></div>        
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
