<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use common\components\CustomHelpers;
use frontend\widgets\company\EmployeesCheck;
use frontend\widgets\crm\SidesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-visits"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php if($model->id_dict_eg_visit_status_fk == 1) { ?><div class="alert alert-warning">Termin nie został jeszcze potwierdzony</div><?php } ?>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
                    <ul class="nav panel-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Podstawowe</span></a></li>
                        <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-comment"></i><span class="panel-tabs--text">Notatka</span> </a></li>
                        <?php if(!$model->isNewRecord) { ?>
                        <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-calculator"></i><span class="panel-tabs--text">Rozliczenie</span> </a></li>
                        <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                        <li><a data-toggle="tab" href="#tab2e"><i class="fa fa-image"></i><span class="panel-tabs--text">Zdjęcie</span> </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2a">
                            <div class="grid">
                                <div class="col-sm-8 col-xs-12"><?= $this->render('_calendar', ['model' => $model]) ?></div>
                                <div class="col-sm-4 col-xs-12">
                                    <?= $form->field($model, 'id_visit_type_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/eg/type/createinline']) , 
                                                            ['class' => 'insertInline text--white', 
                                                             'data-target' => "#type-new-insert", 
                                                             'data-input' => ".type-new"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Type::find()->where(['status' => 1, 'is_active' => 1])->orderby('type_name')->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'type-new select2Modal',] ) ?>
                                    <div id="type-new-insert" class="insert-inline bg-purple2 none"> </div>
                                </div>
                            </div>
                            
                            <div class="grid">
                                <div class="col-sm-6 col-xs-12">
                                    <?php /* $form->field($model, 'id_patient_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Patient::find()->where(['id' => $model->id_patient_fk])->all(), 'id', 'fullname'), ['prompt' => '- wybierz -', 'id' => 'selectAutoCompleteModal', 'data-url' => Url::to('/eg/patient/autocomplete')] )*/ ?> 
                                    <?= $form->field($model, 'id_patient_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/eg/patient/createinline']) , 
                                                            ['class' => 'insertInline text--white', 
                                                             'data-target' => "#patient-new-insert", 
                                                             'data-input' => ".patient-new"
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Patient::find()->where(['id' => $model->id_patient_fk])->all(), 'id', 'fullname'), ['prompt' => '- wybierz -', 'class' => 'patient-new', 'id' => 'selectAutoCompleteModal', 'data-url' => Url::to('/eg/patient/autocomplete')] ) ?>
                                    <div id="patient-new-insert" class="insert-inline bg-purple2 none"> </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <?= $form->field($model, 'id_doctor_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getDoctors(), 'id', 'fullname'), ['class' => 'form-control select2Modal'] ) ?> 
                                </div>
                                <div class="col-xs-12"><?= $form->field($model, 'additional_info')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
                                <div class="col-sm-2 col-xs-4"><?= $form->field($model, 'place_postalcode')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
                                <div class="col-sm-3 col-xs-8"><?= $form->field($model, 'place_city')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
                                <div class="col-sm-7 col-xs-12"><?= $form->field($model, 'place_address')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2b">
                            <?= $form->field($model, 'note')->textarea(['rows' => 2])->label(false) ?>
                        </div>
                        <?php if(!$model->isNewRecord) { ?>
                        <div class="tab-pane" id="tab2c">
                            <?= $this->render('_services', ['model' => $model]) ?>
                        </div>
                        <div class="tab-pane" id="tab2d">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 4, 'parentId' => $model->id, 'onlyShow' => false]) ?> 
                        </div>
                        <div class="tab-pane" id="tab2e">
                            <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                [
                                    'id' => 'imgAttachmentVisit',
                                    'model' => $model,
                                    'behaviorName' => 'coverBehavior',
                                    'apiRoute' => '/eg/visit/imgAttachApi',
                                ]
                            
                            ); ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?php if(!$model->isNewRecord && 1 == 2) { ?> <div style="text-align: left; font-size:10px; color: grey;">Utworzono: <?= $model->created_at .' ['.$model->creator.']' ?></div> <?php } ?>
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?php if(!$model->is_confirm && !$model->isNewRecord && $model->id_dict_eg_visit_status_fk != 5) { ?> <a href="<?= Url::to(['/eg/visit/confirm', 'id' => $model->id]) ?>" title="Potwierdź wizytę" class="btn btn-sm bg-teal modalConfirm" data-label="Potwierdź wizytę"><i class="fa fa-check"></i></a> <?php } ?>
        <?php if($model->id_dict_eg_visit_status_fk != 5 && !$model->isNewRecord) { ?> <a href="<?= Url::to(['/eg/visit/completed', 'id' => $model->id]) ?>" title="Wizyta zrealizowna" class="btn btn-sm bg-green modalConfirm" data-label="Wizyta zrealizowana"><i class="fa fa-smile"></i></a> <?php } ?>
        <?php if($model->id_dict_eg_visit_status_fk < 5 && !$model->isNewRecord) { ?> <a href="<?= Url::to(['/eg/visit/cancel', 'id' => $model->id]) ?>" title="Odwołaj wizytę" class="btn btn-sm bg-orange modalConfirm" data-label="Odwołaj wizytę"><i class="fa fa-ban"></i></a> <?php } ?>
        <?php if($model->id_dict_eg_visit_status_fk != 7 && !$model->isNewRecord) { ?> <a href="<?= Url::to(['/eg/visit/remove', 'id' => $model->id]) ?>" title="Usuń wizytę" class="btn btn-sm bg-red modalConfirm" data-label="Usuń wizytę"><i class="fa fa-trash"></i></a> <?php } ?>
        <?= Html::submitButton( ($model->isNewRecord) ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => ($model->isNewRecord) ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true }); 
    });
</script>

