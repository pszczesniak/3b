<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\visit\models\Calvisit */
/* @var $form yii\widgets\ActiveForm */
?>

    
<div class="grid grid--0">
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="visit_fromDate">Start</label>
            <input type='text' class="form-control" id="visit_fromDate" name="Visit[fromDate]" value="<?= $model->fromDate ?>"/> 
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="visit-fromTime">&nbsp;</label>
            <div class='input-group date' id='visit_fromTime' >
                <input type='text' class="form-control" name="Visit[fromTime]" value="<?= $model->fromTime ?>"  />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę startu">
                    <span class="fa fa-clock text--white"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="visit-toDate">Koniec</label>
            <input type='text' class="form-control" id="visit_toDate" name="Visit[toDate]" value="<?= $model->toDate ?>"/>
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="visit-toTime">&nbsp;</label>
            <div class='input-group date' id='visit_toTime' >
                <input type='text' class="form-control" name="Visit[toTime]" value="<?= $model->toTime ?>" />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę końca">
                    <span class="fa fa-clock text--white"></span>
                </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#visit_fromDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#visit_fromDate').on("dp.change", function (e) {
        minDate = e.date.format('YYYY-MM-DD');
        $('#visit_toDate').val(minDate);
        if( $('#visit_toDate').val() && $('#visit_fromTime > input').val() && $('#visit_toTime > input').val() ) {
            $start = $('#visit_fromDate').val() + ' ' + $('#visit_fromTime > input').val() + ':00';
            $end = $('#visit_toDate').val() + ' ' + $('#visit_toTime > input').val() + ':00';
        }
    });
      
    $('#visit_fromTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });
    $('#visit_fromTime').on("dp.change", function (e) {
        minDate = e.date.add(30, 'minutes').format('HH:mm');
        $('#visit_toTime > input').val(minDate);
        if( $('#visit_toDate').val() && $('#visit_fromDate').val() && $('#visit_toTime > input').val() ) {
            $start = $('#visit_fromDate').val() + ' ' + $('#visit_fromTime > input').val() + ':00';
            $end = $('#visit_toDate').val() + ' ' + $('#visit_toTime > input').val() + ':00';
        }
    });
    $('#visit_toDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#visit_toDate').on("dp.change", function (e) {
        if( $('#visit_fromDate').val() && $('#visit_fromTime > input').val() && $('#visit_toTime > input').val() ) {
            $start = $('#visit_fromDate').val() + ' ' + $('#visit_fromTime > input').val() + ':00';
            $end = $('#visit_toDate').val() + ' ' + $('#visit_toTime > input').val() + ':00';
        }
    });
    $('#visit_toTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });
    $('#visit_toTime').on("dp.change", function (e) {
        if( $('#visit_fromDate').val() && $('#visit_fromTime > input').val() && $('#visit_toDate').val() ) {
            $start = $('#visit_fromDate').val() + ' ' + $('#visit_fromTime > input').val() + ':00';
            $end = $('#visit_toDate').val() + ' ' + $('#visit_toTime > input').val() + ':00';
        }
    });
    
</script>