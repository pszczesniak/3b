<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>
<h4>Do zapłaty <span id="tmpl-visitPayment" class="text--pink"><?= $model->saldo ?></span></h4>
<div id="toolbar-services" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/eg/visit/screate', 'id' => $model->id]) , 
                ['class' => 'btn btn-success btn-icon viewModal', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-ajax", 
                 'data-form' => "item-form", 
                 'data-table' => "table-services",
                 'data-title' => "<i class='fa fa-plus'></i>Zabieg"
                ]) ?>
    <!--<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-services"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>-->
</div>
<table  class="table table-striped table-items header-fixed table-widget"  id="table-services"
    data-toolbar="#toolbar-services" 
    data-toggle="table-widget" 
    data-show-refresh="yes" 
    data-show-toggle="true"  
    data-show-columns="false" 
    data-show-export="false"  

    data-show-pagination-switch="false"
    data-pagination="true"
    data-id-field="id"
    data-height="300px"
    data-show-footer="false"
    data-side-pagination="server"
    data-row-style="rowStyle"
    data-sort-name="lastname"
    data-sort-order="asc"
    data-method="get"
    data-checkbox-header="false"
    data-search-form="#filter-eg-services-search"
    data-url=<?= Url::to(['/eg/visit/services', 'id' => $model->id]) ?>>
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="service" data-sortable="true">Usługa</th>
            <th data-field="tooth" data-visible="false" data-sortable="true" data-width="30px">Ząb</th>
            <th data-field="payment" data-sortable="true" data-align="right">Do zapłaty</th>
            <th data-field="paid"  data-sortable="true" data-align="right">Zapłacono</th>
            <th data-field="discount"  data-sortable="true" data-align="right">Rabat</th>
            <!--<th data-field="actions" data-events="actionEvents" data-width="60px"></th>-->
        </tr>
    </thead>
    <tbody class="ui-sortable">

    </tbody>

</table>