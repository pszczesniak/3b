<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use common\components\CustomHelpers;
use frontend\widgets\company\EmployeesCheck;
use frontend\widgets\crm\SidesTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-visits"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php if($model->id_dict_eg_visit_status_fk == 1) { ?><div class="alert alert-warning">Termin nie został jeszcze potwierdzony</div><?php } ?>
            <?= $form->field($model, 'additional_info')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
            <?= $form->field($model, 'access_code')->textInput(['maxlength' => true, 'class' => 'form-control', 'readonly' => true]) ?>
        </div>
	</div>
    <div class="modal-footer"> 
      <?= Html::submitButton( 'Dodaj galerię', ['class' => 'btn btn-sm btn-success']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

</script>

