<?php $presentationAll = \backend\Modules\Eg\models\Visit::Viewer(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
    <div class="wrapper wrapper-content p-xl">
        <div class="ibox-content p-xl">
            <table class="table table-bordered">
                <thead><tr><th>Pacjent</th><th style="width: 80px; text-align: center;">Termin</th><th style="width: 80px; text-align: center;">Czas [min]</th><th>Lekarz</th><th>Informacja</th><th>Notatka</th><th>Status</th></tr></thead>
                <tbody>
                    <?php foreach($model as $key => $item) { 
                        $presentation = $presentationAll[$item['id_dict_eg_visit_status_fk']];
                        echo '<tr>'
                                .'<td>'.$item['patient'].'<br /><small style="color: gray;">'.$item['phone'].(($item['phone_additional']) ? (', '.$item['phone_additional']) : '').'</small></td>'
                                .'<td style="text-align: center;">'.$item['start'].'</td>'
                                .'<td  style="text-align: center;">'.round((strtotime($item['end'])-strtotime($item['start']))/60, 2).'</td>'
                                .'<td>'.$item['doctor'].'</td>'
                                .'<td>'.$item['additional_info'].'</td>'
                                .'<td>'.$item['note'].'</td>'
                                .'<td><small style="color: '.$presentation['color'].';">'.$presentation['label'].'</small><br /><span style="color: '.(($item['is_confirm']) ? 'green' : 'red').'">'.(($item['is_confirm']) ? 'potwierdzona' : 'niepotwierdzona').'</span></td>'
                            .'</tr>';
                    } ?>
                </tbody>
                <tfoot>
                    <tr><td colspan="7"><small>Data wydruku: <i><?= date('Y-m-d H:i:s')  ?></i></small></td></tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>
</html>