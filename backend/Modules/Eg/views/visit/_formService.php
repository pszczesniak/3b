<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-ajax", 'data-table' => '#table-services' ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
           // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
<div class="modal-body">
    <div class="grid">
        <div class="col-xs-12"><?= $form->field($model, 'id_service_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Eg\models\Service::getList(), 'id', 'service_name'), ['options' => \backend\Modules\Eg\models\Service::listAttributes(), 'prompt' => '- wybierz -', 'class' => 'select2Modal'] ) ?> </div>
        <!--<div class="col-xs-4"><?= $form->field($model, 'tooth_no')->dropDownList(\backend\Modules\Eg\models\Tooth::listTooths(), ['prompt' => '- wybierz -', 'class' => 'select2Modal'] ) ?> </div>-->
    </div>
    <fieldset><legend>Rozliczenie</legend>
        <div class="grid">
            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'payment')->textInput(['maxlength' => true, 'class' => 'form-control number']) ?></div>
            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'paid')->textInput(['maxlength' => true, 'class' => 'form-control number']) ?></div>
            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'discount')->textInput(['maxlength' => true, 'class' => 'form-control number']) ?></div>
        </div>
    </fieldset>
</div>
<div class="modal-footer">    
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">
	document.getElementById('visitservice-id_service_fk').onchange = function(event) {
        var servicePrice = this.options[this.selectedIndex].getAttribute("data-price");
        document.getElementById('visitservice-payment').value = servicePrice; 
        var discountValue = document.getElementById('visitservice-discount').value; discountValue = (discountValue) ? discountValue : 0;
        document.getElementById('visitservice-paid').value = servicePrice - discountValue; 
    }
    
    document.getElementById('visitservice-discount').onchange = function(event) {
        var servicePrice = document.getElementById('visitservice-payment').value; servicePrice = (servicePrice) ? servicePrice : 0;
        var discountValue = document.getElementById('visitservice-discount').value; discountValue = (discountValue) ? discountValue : 0;
        document.getElementById('visitservice-paid').value = servicePrice - discountValue; 
    }
</script>