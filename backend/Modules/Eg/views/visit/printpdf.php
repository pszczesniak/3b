<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
    <div class="wrapper wrapper-content p-xl">
        <div class="ibox-content p-xl">
            <table class="table table-bordered">
                <tbody>
                    <tr><td>Pacjent</td><td><?= $model->patient['fullname'] ?></td></tr>
                    <tr><td>Lekarz</td><td><?= $model->doctor['fullname'] ?></td></tr>
                    <tr><td>Godzina</td><td><?= $model->start ?></td></tr>
                    <tr><td>Informacja</td><td><?= ($model->additional_info) ? $model->additional_info : 'brak dodatkowej informacji' ?></td></tr>
                    <tr><td>Notatka</td><td><?= ($model->note) ? $model->note : 'brak notatki' ?></td></tr>
                </tbody>
                <tfoot>
                    <tr><td colspan="4"><small>Data wydruku: <i><?= date('Y-m-d H:i:s')  ?></i></small></td></tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>
</html>