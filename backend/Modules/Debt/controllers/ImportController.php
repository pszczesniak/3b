<?php

namespace app\Modules\Debt\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\web\UploadedFile;
use backend\Modules\Debt\models\DebtReport;

/**
 * Import controller for the `Debt` module
 */
class ImportController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DebtReport();
		
		$perDayData = Yii::$app->db->createCommand("select company_fk, max(date_format(created_at, '%Y-%m-%d')) as load_date from {{%debt_report}} group by company_fk")->query();
		$perDay = []; $perDay[0] = 'brak danych'; $perDay[1] = 'brak danych';
		foreach($perDayData as $key => $value) {
			if($value['company_fk'] == 1) $perDay[0] = $value['load_date'];
			if($value['company_fk'] == 2) $perDay[1] = $value['load_date'];
		}
        
        return $this->render('index', ['perDay' => $perDay, 'searchModel' => $searchModel, 'grants' => $this->module->params['grants']]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

        $post = $_GET;
        if(isset($_GET['DebtReport'])) {
            $params = $_GET['DebtReport'];
            if(isset($params['customer_name']) && !empty($params['customer_name']) ) {
				array_push($where, " lower(customer_name) like '%".strtolower($params['customer_name'])."%' ");
            }
            if(isset($params['nip']) && !empty($params['nip']) ) {
				array_push($where, " lower(nip) like '%".strtolower($params['nip'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "history_date >= '".$params['date_from']."'");
            }
            if(isset($params['company_fk']) && !empty($params['company_fk']) ) {
				array_push($where, " company_fk = ".$params['company_fk']);
            }
        }   
        
		$sortColumn = 'customer_name';
		if( isset($post['sort']) && $post['sort'] == 'h_customer' ) $sortColumn = 'ifnull(c.symbol, c.name)'.' collate `utf8_polish_ci` ';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query = (new \yii\db\Query())
            ->select(['customer_name', 'company_fk', 'h.nip', 'min(c.name) as c_ultima', 'sum(incomes) as incomes', 'sum(costs) as costs', 'max(after_deadline) as after_deadline'])
            ->from('{{%debt_report}} h')
            ->join(' LEFT JOIN', '{{%customer}} as c', 'c.id = h.id_customer_fk')
            ->groupBy(['customer_name', 'nip', 'company_fk']);
            //->where( ['h.status' => 1] );
        
		if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) || $this->module->params['employeeKind'] == 100 || count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0) {
            array_push($where, "1=1");
        } else { 
			array_push($where, "id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
		}
		
		if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }	
		
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn. ' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
			
		$fields = [];
		$tmp = [];
	    $companies = DebtReport::getCompanies();
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
           // $actionColumn .= '<a href="'.Url::to(['/debt/history/updateajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-history" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
           // $actionColumn .= '<a href="'.Url::to(['/debt/history/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-history"><i class="fa fa-trash" title="Usuń"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['company'] = $companies[$value['company_fk']];
			$tmp['customer'] = $value['customer_name'];
            $tmp['nip'] = $value['nip'];
            $tmp['ultima'] = $value['c_ultima'];
            $tmp['deadline'] = $value['after_deadline'];
			$tmp['incomes'] = number_format($value['incomes'], 2, '.', ' ');
            $tmp['costs'] = number_format($value['costs'], 2, '.', ' ');
			$tmp['typeFormatter'] = 'debt_report';
            //$tmp['h_customer'] = $value['cname'];
            /*$tmp['alert'] = ($value['alert']) ? 
                                '<a href="/alerts/update/'.$value['alert'].'?table=history" class="btn btn-xs bg-orange gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Edytuj przypomnienie"><i class="fa fa-bell"></i></a>'.
                                '&nbsp;<a href="/alerts/delete/'.$value['alert'].'?table=history" class="deleteConfirm" data-confirm="Czy na pewno usunąć to przypomnienie?" data-placement="left" data-toggle="tooltip" data-title="Usuń przypomnienie" data-table="#table-history"><i class="fa fa-close text--red"></i></a>' : 
                                '<a href="'.Url::to(['/alerts/create', 'fid' => $value['id'], 'type' => 2]).'?table=history" class="btn btn-xs bg-green gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Ustaw przypomnienie"><i class="fa fa-plus"></i></a>';*/
           // $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['company_fk'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionUpload(){
        $model = new DebtReport();
       
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->validate()) {
                if ($model->upload()) {
                    /* import data*/
        
                    $file = \Yii::getAlias('@webroot').'/uploads/debt/'.$model->fileName;
                    $fileArr = explode('.', $model->fileName);
                    $objReader = \PHPExcel_IOFactory::createReader( ($fileArr[1] == 'xlsx') ? 'Excel2007' : 'Excel5');
                    $objPHPExcel = $objReader->load($file);
                    $objWorksheet = $objPHPExcel->setActiveSheetIndex(1);
                    $i=1;
                    $customer_name = '';
                    $company_fk = 1;
                    //var_dump($objWorksheet->getColumnDimensions()); exit;
                    $rows = [];
                    Yii::$app->db->createCommand("delete from {{%debt_report}} where company_fk = ".$model->company_fk)->query();
                    foreach ($objWorksheet->getRowIterator() as $row) { 

                        //var_dump($row->getRowIndex());
                        if($i > 1){
                            $customer_name = $objPHPExcel->getActiveSheet()->getCell("H$i")->getValue();
                            $inv = $objPHPExcel->getActiveSheet()->getCell("A$i")->getValue();
                            $nip = $objPHPExcel->getActiveSheet()->getCell("F$i")->getValue();
                            $incomes = $objPHPExcel->getActiveSheet()->getCell("P$i")->getValue(); $incomes = ($incomes) ? $incomes : 0;
                            $costs = $objPHPExcel->getActiveSheet()->getCell("Q$i")->getValue(); $costs = ($costs) ? $costs : 0;
                            $after_deadline = $objPHPExcel->getActiveSheet()->getCell("R$i")->getValue(); $after_deadline = ($after_deadline) ? $after_deadline : 0;
                            
                            $temp = [$model->company_fk, $customer_name, $nip, $inv, $incomes, $costs, $after_deadline, date('Y-m-d H:i:s'), \Yii::$app->user->id];
                            array_push($rows, $temp);
                            
                            /* $sqlInsert = "insert into {{%debt_report}} (company_fk, customer_name, nip, invoice_no, incomes, costs, after_deadline, created_at, created_by) "
                                         ." values (".$company_fk.", '".addslashes($customer_name)."', '".$nip."', '".addslashes($inv)."',".$incomes.",".$costs.",'".$after_deadline."', NOW(), 0) ";
                            if(!\Yii::$app->db->createCommand($sqlInsert)->execute()) {
                                echo $sqlInsert; exit;
                                break;
                            }*/
                        }
                        ++$i;
                    }
                    Yii::$app->db->createCommand()->batchInsert('{{%debt_report}}', 
                        ['company_fk', 'customer_name', 'nip', 'invoice_no', 'incomes', 'costs', 'after_deadline', 'created_at', 'created_by'],
                        $rows)->execute();
                    $sqlUpdate = "update {{%debt_report}} dr set id_customer_fk = (select min(id) from {{%customer}} c where replace(replace(c.nip,'-',''), ' ', '') = replace(replace(dr.nip,'-',''), ' ', '') and c.status = 1)";
                    \Yii::$app->db->createCommand($sqlUpdate)->execute();
                    
                    return array('success' => true, 'alert' => 'Dane zostały zaimportowane', 'id' => $model->id, 'table' => '#table-report', 'company' => $model->company_fk, 'load_date' => date('Y-m-d') );	
                } else {
                    return array('success' => false, 'html' => $this->renderAjax('upload', ['model' => $model]), 'errors' => $model->getErrors() );
                }
            } else {
                return array('success' => false, 'html' => $this->renderAjax('upload', ['model' => $model]), 'errors' => $model->getErrors() );
            }
        } else {
            return $this->renderAjax('upload', ['model' => $model]);
        }
        
        /*if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('uploads/debt/'.$filename);
            if($upload){
                define('CSV_PATH','uploads/debt');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);
                print_r($filecsv);
                foreach($filecsv as $data){
                    $modelnew = new Mahasiswa;
                    $hasil = explode(",",$data);
                    $nim = $hasil[0];
                    $nama = $hasil[1];
                    $jurusan = $hasil[2];
                    $angkatan = $hasil[3];
                    $alamat = $hasil[4];
                    $foto = $hasil[5];
                    $modelnew->nim = $nim;
                    $modelnew->nama = $nama;
                    $modelnew->jurusan = $jurusan;
                    $modelnew->angkatan = $angkatan;
                    $modelnew->alamat = $alamat;
                    $modelnew->foto = $foto;
                    $modelnew->save();
                }
                //unlink('uploads/'.$filename);
                return $this->redirect(['site/index']);
            }
        }else{
            return $this->renderAjax('upload',['model'=>$model]);
        }*/
    }
	
	public function actionItems($cid, $nip) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $html = '<table class="table-logs">';
        $html .= '<thead><tr><th>Numer faktury</th><th>Wartość</th><th>Po terminie</th></tr><thead><tbody>';
        
        $invoices = DebtReport::find()->where( ['company_fk' => $cid, 'nip' => $nip] )->orderby('after_deadline desc')->all();
    
        foreach($invoices as $key => $item) {
            $docs = [];
            
            $html .= '<tr>'
                    .'<td>'.$item->invoice_no.'</td>'
                    .'<td>'.number_format($item->incomes, 2, '.', ' ').'</td>'
                    .'<td>'.$item->after_deadline.'</td></tr>';
        }
        
        $html .= '</tbody></table>';
               
        return [ 'html' => $html ];
    }
}
