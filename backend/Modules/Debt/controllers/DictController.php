<?php

namespace app\Modules\Debt\controllers;

use Yii;
use backend\Modules\Debt\models\DebtAction;
//use backend\Modules\Debt\models\DictionaryValue;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * DictController implements the CRUD actions for DebtAction model.
 */
class DictController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DebtAction models.
     * @return mixed
     */
    public function actionAction()
    {
        $searchModel = new DebtAction();

        return $this->render('actions', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionActions() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $params = Yii::$app->request->queryParams;
        
        $fieldsData = DebtAction::find()->where(['status' => 1])->all();
			
		$fields = [];
		$tmp = [];
		      
        
		foreach($fieldsData as $key=>$value) {
			
			$tmp['name'] = $value->dict_name;
            $tmp['symbol'] = $value->dict_symbol;
            $tmp['icon'] = '<i class="'.$value->dict_icon.'"></i>';
            $tmp['color'] = '<span style="display: block; width: 12px; height: 12px; background-color: '.$value->dict_color.'">&nbsp;&nbsp;</span>';
            
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/debt/dict/uaction', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/debt/dict/daction', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
           
            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    public function actionCaction() {
		
		$model = new DebtAction();
        $model->dict_type_fk = 1;
        $model->dict_color = "#4b87ae";
        $model->dict_icon = "fa fa-tasks";
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {				
				return array('success' => true,  'index' =>1, 'alert' => 'Wartość słownika <b>'.$model->dict_name.'</b> została dodana' );
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form_action', [  'model' => $model ]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_form_action', [  'model' => $model,]) ;	
		}
	}
    
    public function actionUaction($id) {
		
		$model = DebtAction::findOne(CustomHelpers::decode($id));
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {				
				return array('success' => true,  'index' =>1, 'alert' => 'Wartość słownika <b>'.$model->dict_name.'</b> została zmodyfikowana' );
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form_action', [  'model' => $model ]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_form_action', [  'model' => $model,]) ;	
		}
	}

    public function actionDaction($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = DebtAction::findOne(CustomHelpers::decode($id));
		$model->status = -1;
		$model->deleted_at = date('Y-m-d H:i:s');
		$model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Wartość słownika  została usunięta', 'id' => $id, 'table' => '#table-dict');	
        } else {
            return array('success' => false, 'alert' => 'Wartość słownika  nie została usunięta', 'id' => $id, 'table' => '#table-dict');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the Dictionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dictionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dictionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreateinline($id) {
		
		$model = new DictionaryValue();
        $model->id_dictionary_fk = $id;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->name = $_POST['value'];
            //$exist = DictionaryValue::find()->where
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Wartość słownika <b>'.$model->name.'</b> została dodana', 'id' => $model->id, 'name' => $model->name );
			} else {
                $alert = "Ten słownik juz posiada taką wartość";
                foreach($model->getErrors() as $key => $value){
                    $alert = $value;
                }
                return array('success' => false, 'alert' => $alert );	
			}		
		} else {
			if($id == 0)
                return '<div class="alert alert-danger">Proszę wybrać typ słownika</div>';
            else
                return  $this->renderAjax('_formInline', [  'model' => $model,]) ;	
		}
	}
}
