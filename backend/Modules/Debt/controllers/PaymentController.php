<?php

namespace app\Modules\Debt\controllers;

use Yii;
use backend\Modules\Debt\models\DebtPayment;
use backend\Modules\Debt\models\DebtPaymentSearch;
use backend\Modules\Debt\models\DebtPaymentClaim;
use backend\Modules\Debt\models\DebtPaymentSettlements;
use backend\Modules\Task\models\CaseClaim;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * PaymentController implements the CRUD actions for DebtAction model.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DebtPayment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DebtPaymentSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        
        $id = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'id' => $id
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

        $post = $_GET;
        if(isset($_GET['DebtPaymentSearch'])) {
            $params = $_GET['DebtPaymentSearch'];
            if(isset($params['payment_note']) && !empty($params['payment_note']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(name) like '%".strtolower($params['name'])."%' ");
				array_push($where, " lower(payment_note) like '%".strtolower($params['payment_note'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "payment_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "payment_date <= '".$params['date_to']."'");
            }
            if(isset($params['amount_from']) && !empty($params['amount_from']) ) {
                array_push($where, "payment_amount >= '".$params['amount_from']."'");
            }
            if(isset($params['amount_to']) && !empty($params['amount_to']) ) {
                array_push($where, "payment_amount <= '".$params['amount_to']."'");
            }
			if(isset($params['account_type_fk']) && !empty($params['id_debt_action_fk']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(email) like '%".strtolower($params['email'])."%' ");
				array_push($where, " id_debt_action_fk = ".$params['id_debt_action_fk']);
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, " id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "p.id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
				array_push($where, "p.id_set_fk = ".$params['id_set_fk']);
            }
        }   
        
		$sortColumn = 'payment_date';
        if( isset($post['sort']) && $post['sort'] == 'history_date' ) $sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'id_debt_action_fk' ) $sortColumn = 'dict.dict_name';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query = (new \yii\db\Query())
            ->select(['p.id as id', 'payment_note', 'account_type_fk', 'payment_date', 'payment_amount', 'e.firstname', 'e.lastname', 'c.name as cname', 'c.id as cid', 'cc.name as ccname'])
            ->from('{{%debt_payment}} p')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = p.id_customer_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = p.id_employee_fk')
            ->join(' LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = p.id_set_fk')
            ->where( ['p.status' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
			
		$fields = [];
		$tmp = [];
	
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/debt/payment/updateajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-payment" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/debt/payment/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-payment"><i class="fa fa-trash" title="Usuń"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['p_date'] = $value['payment_date'];
            $tmp['p_amount'] = number_format($value['payment_amount'], 2, "," ,  " ");
            $tmp['p_currency'] = $value['payment_amount'];
			$tmp['p_note'] = $value['payment_note'];
			$tmp['p_employee'] = $value['lastname'].' '.$value['firstname'];
            $tmp['p_customer'] = $value['cname'];
            $tmp['p_set'] = $value['ccname'];
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    /**
     * Displays a single CorrespondenceAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewajax($id)  {
        $model = $this->findModel($id);
        
        $grants = $this->module->params['grants'];

        return $this->renderAjax('_viewAjax', [
            'model' => $model,  'edit' => isset($grants['correspondenceEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }


    /**
     * Creates a new DebtPayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()   {
        $model = new DebtPayment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    private function settlements($model) { 
        $claims = ($model->claims) ? $model->claims : CaseClaim::find()->where(['status' => 1, 'id_case_fk' => $model->id_set_fk])->orderby('payment_date desc')->all();
        $amount = $model->payment_amount;
        foreach($claims as $key => $claim) {
            $new = new DebtPaymentSettlements();
            $new->id_claim_fk = $claim->id;
            $new->id_payment_fk = $model->id;
            $new->s_interest = ($amount - $claim->interest >= 0) ? $claim->interest : $amount; $amount -= $new->s_interest;
            $new->s_capital = ($amount - $claim->amount >= 0) ? $claim->amount : $amount; $amount -= $new->s_capital;
            $new->save();
        }
        
        return true;
    }
    
    public function actionCreateajax($id) {
		$id = CustomHelpers::decode($id);
        $model = new DebtPayment();
        $model->id_customer_fk = $id;
        $model->id_set_fk = (isset($_GET['set'])) ? $_GET['set'] : 0;
   
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
                 
            if($model->validate() && $model->save()) {
                if($model->claims_list) {
                    foreach($model->claims_list as $key => $value) {
                        $new = new DebtPaymentClaim();
                        $new->id_payment_fk = $model->id;
                        $new->id_claim_fk = $value;
                        $new->save();
                    }
                }
                
                if($model->settlement) {
                    $this->settlements($model);
                }
                return array('success' => true, 'alert' => 'Wpis został utworzony', 'id'=>$model->id);	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderPartial('_formAjaxNew', [ 'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-payment" ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderPartial('_formAjaxNew', [  'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-payment" ]) ;	
		}
	}

    /**
     * Updates an existing CorrespondenceAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
        $grants = $this->module->params['grants']; $tmpClaims = [];
        
        if($model->claims) {
            foreach($model->claims as $key => $value) {
                array_push($model->claims_list, $value->id_claim_fk);
                array_push($tmpClaims, $value->id_claim_fk);
            }
        }
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				if(isset($_POST['DebtPayment']['claims_list']) && !empty($_POST['DebtPayment']['claims_list']) ) {
                    DebtPaymentClaim::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'id_payment_fk = '.$model->id.' and status = 1 and id_claim_fk not in ('.implode(',', $_POST['DebtPayment']['claims_list']).')');
                    foreach($_POST['DebtPayment']['claims_list'] as $key => $value) {
                        $exist = DebtPaymentClaim::find()->where(['id_payment_fk' => $model->id, 'id_claim_fk' => $value, 'status' => 1])->count();
                        if(!$exist) {
                            $model_c = new DebtPaymentClaim();
                            $model_c->id_payment_fk = $model->id;
                            $model_c->id_claim_fk = $value;
                            $model_c->save();
                        }
                    }
                } else {
                    DebtPaymentClaim::updateAll(['status' => -1, 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => Yii::$app->user->id], 'id_payment_fk = '.$model->id.' and status = 1');
                }
                
                return array('success' => true, 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjaxNew', [  'model' => $model, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-payment" ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjaxNew', [  'model' => $model, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-payment" ]) ;	
		}
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-payment");	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-payment");	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the DebtPayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DebtPayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
        if (($model = DebtPayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The history does not exist.');
        }
    }
	
	public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
		$employee = false; $customer = false; $case = false; $dateFrom = false;  $dateTo = false; $where = [];
		
		if(isset($_GET['DebtPaymentSearch'])) {
            $params = $_GET['DebtPaymentSearch'];
            if(isset($params['history_note']) && !empty($params['history_note']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(name) like '%".strtolower($params['name'])."%' ");
				array_push($where, " lower(history_note) like '%".strtolower($params['history_note'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "payment_date >= '".$params['date_from']."'");
				$dateFrom = $params['date_from'];
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "payment_date <= '".$params['date_to']."'");
				$dateTo = $params['date_to'];
            }
			if(isset($params['id_debt_action_fk']) && !empty($params['id_debt_action_fk']) ) {
				array_push($where, " id_debt_action_fk = ".$params['id_debt_action_fk']);
            }
			if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk'])) {
				array_push($where, " id_customer_fk = ".$params['id_customer_fk']);
				$customer = \backend\Modules\Crm\models\Customer::findOne($params['id_customer_fk']);
			}
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk'])) {
				array_push($where, " id_employee_fk = ".$params['id_employee_fk']);
				$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($params['id_employee_fk']);
			}
        }   
        
        $fields = []; $tmp = [];
		
		$sortColumn = 'p.payment_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['p.id as id', 'payment_note', 'account_type_fk', 'payment_date', 'payment_amount', 'payment_title', 'e.firstname', 'e.lastname', 'c.name as cname', 'c.id as cid', 'cc.name as ccname'])
            ->from('{{%debt_payment}} p')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = p.id_customer_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = p.id_employee_fk')
            ->join(' LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = p.id_set_fk')
            ->where( ['p.status' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
       
		$periodLabel = ($dateFrom || $dateTo) ? ' w okresie'.((($dateFrom) ? ' od '.$dateFrom : '').(($dateTo) ? ' do '.$dateTo : '')) : '';
        
        if($employee)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia wpłat pracownika '.$employee->fullname.$periodLabel);
        else if($customer)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia wpłat dla klienta '.(($customer->symbol) ? $customer->symbol : $customer->symbol).$periodLabel);
        else 
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia wpłat'.$periodLabel);        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Kowa')
			->setCellValue('B3', 'Data')
			->setCellValue('C3', 'Opis')
			->setCellValue('D3', 'Klient')
			->setCellValue('E3', 'Sprawa')
			->setCellValue('F3', 'Pracownik');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['payment_amount'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['payment_date']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['payment_title']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['cname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['ccname']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['lastname'].' '.$record['firstname']);
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Wpłaty');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Wpłaty'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}

}
