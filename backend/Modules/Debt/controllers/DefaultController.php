<?php

namespace app\Modules\Debt\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
/**
 * Default controller for the `Debt` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $stats = ['charge' => 35000, 'interest' => 5000, 'costs' => 1200, 'payments' => 12000, 'balance' => 29200];
        return $this->render('index', ['stats' => $stats]);
    }
    
    public function actionTimeline() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where_history = []; $where_payment = []; $type = 0;

        $post = $_GET;
        if(isset($_GET['DebtHistorySearch'])) {
            $params = $_GET['DebtHistorySearch'];
            if(isset($params['history_note']) && !empty($params['history_note']) ) {
				array_push($where_history, " lower(history_note) like '%".strtolower($params['history_note'])."%' ");
                array_push($where_payment, " lower(payment_note) like '%".strtolower($params['history_note'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where_history, "history_date >= '".$params['date_from']."'");
                array_push($where_payment, "payment_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where_history, "history_date <= '".$params['date_to']."'");
                array_push($where_payment, "payment_date <= '".$params['date_to']."'");
            }
			if(isset($params['id_debt_action_fk']) && !empty($params['id_debt_action_fk']) ) {
				array_push($where_history, " id_debt_action_fk = ".$params['id_debt_action_fk']);
                array_push($where_payment, " p.id = 0");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where_history, " id_employee_fk = ".$params['id_employee_fk']);
                array_push($where_payment, " id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where_history, " id_customer_fk = ".$params['id_customer_fk']);
                array_push($where_payment, " id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
				array_push($where_history, " id_set_fk = ".$params['id_set_fk']);
                array_push($where_payment, " id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				$type = $params['type_fk'];
            }
        }   
        
		$sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'history_date' ) $sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'id_debt_action_fk' ) $sortColumn = 'dict.dict_name';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query_history = (new \yii\db\Query())
            ->select(['concat_ws("",1) as type_fk', 'h.id as id', 'history_date', 'history_note', 'e.firstname', 'e.lastname', 'id_debt_action_fk', 'dict_name', 'dict_color', 'dict_icon', 'history_time', 'concat_ws("",0) as payment_amount'])
            ->from('{{%debt_history}} h')
            ->join(' JOIN', '{{%debt_action}} as dict', 'dict.id = h.id_debt_action_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = h.id_employee_fk')
            ->where( ['h.status' => 1] );
        if( count($where_history) > 0 ) {
            $query_history = $query_history->andWhere(implode(' and ', $where_history));
        }   
        $count1 = $query_history->count(); 
        $query_payment = (new \yii\db\Query())
            ->select(['concat_ws("",2) as type_fk', 'p.id as id', 'payment_date as history_date', 'payment_note as history_note', 'e.firstname', 'e.lastname', 'concat_ws("",0) as id_debt_action_fk', 'concat_ws("","wpłata") as dict_name', 'concat_ws("","#f9892e") as dict_color', 'concat_ws("","fa fa-money") as dict_icon', 'concat_ws("","") as history_time',  'payment_amount'])
            ->from('{{%debt_payment}} p')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = p.id_employee_fk')
            ->where( ['p.status' => 1] );
        if( count($where_payment) > 0 ) {
            $query_payment = $query_payment->andWhere(implode(' and ', $where_payment));
        }   
        $count2 = $query_payment->count(); 
        if($type == 0) {
            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query_history->union($query_payment)])
                ->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 + $count2; 
        } else if($type == 1) {
            $unionQuery = $query_history->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 ; 
        } else {
            $unionQuery = $query_payment->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count =  $count2; 
        }
     
        $rows = $unionQuery->all();
			
		$fields = [];
		$tmp = [];

		foreach($rows as $key => $value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/debt/'.(($value['type_fk'] == 1) ? 'history' : 'payment').'/updateajax', 'id' => $value['id']]).'?timeline=true" class="btn btn-xs btn-default update" data-table="#table-timeline" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/debt/'.(($value['type_fk'] == 1) ? 'history' : 'payment').'/deleteajax', 'id' => $value['id']]).'?timeline=true" class="btn btn-xs btn-default remove" data-table="#table-timeline"><i class="fa fa-trash" title="Usuń"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['action'] = '<i class="'.$value['dict_icon'].'" style="color: '.$value['dict_color'].'" data-placement="right" data-toggle="tooltip" data-title="'.$value['dict_name'].'"></i>';
            $value['history_time'] = ($value['history_time'] == '00:00') ? '' : $value['history_time'];
            $tmp['date'] = $value['history_date'].( ($value['history_time']) ? '<br/><i class="fa fa-clock-o text--blue"></i><span class="text-navy">'.$value['history_time'].'</span>' : '');
			$tmp['note'] = $value['history_note'];
            $tmp['amount'] = ($value['payment_amount']) ? number_format($value['payment_amount'], 2, "," ,  " ") : '';
			$tmp['employee'] = $value['lastname'].' '.$value['firstname'];
           /* $tmp['alert'] = ($value['alert']) ? 
                                '<a href="/alerts/update/'.$value['alert'].'?table=history" class="btn btn-xs bg-orange gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Edytuj przypomnienie"><i class="fa fa-bell"></i></a>'.
                                '&nbsp;<a href="/alerts/delete/'.$value['alert'].'?table=history" class="deleteConfirm" data-confirm="Czy na pewno usunąć to przypomnienie?" data-placement="left" data-toggle="tooltip" data-title="Usuń przypomnienie" data-table="#table-history"><i class="fa fa-close text--red"></i></a>' : 
                                '<a href="'.Url::to(['/alerts/create', 'fid' => $value['id'], 'type' => 2]).'?table=history" class="btn btn-xs bg-green gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Ustaw przypomnienie"><i class="fa fa-plus"></i></a>';*/
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionEvents() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $events = []; $fields = []; $tmp = [];
        $status = \backend\Modules\Task\models\CalTask::listStatus();
        $categories = \backend\Modules\Task\models\CalTask::listCategory();
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		//$model = $this->findModel($id);
		//$eventsData = $model->events;
        $eventsData = \backend\Modules\Task\models\CalTask::find()->where(['status' => 1])->limit(10)->all(); 
	
		foreach($eventsData as $key=>$value) {
			if($value->type_fk == 1) {
                $tmp['name'] = '<a href="'.Url::to(['/task/case/view', 'id' => $value->id]).'" title="Przejdź do karty zdarzenia">'.$value->name.'</a>';
                $type='case';
            } else {
                $tmp['name'] = '<a href="'.Url::to(['/task/event/view', 'id' => $value->id]).'" title="Przejdź do karty zdarzenia">'.$value->name.'</a>';
                $type="event";
            }
            
            $actionColumn = '<div class="edit-btn-group">';
                //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
                $actionColumn .= '<a href="'.Url::to(['/task/event/showajax', 'id' => $value->id]).'" class="btn btn-xs btn-default  gridViewModal" data-table="#table-events" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
                //$actionColumn .= '<a href="'.Url::to(['/task/'.$type.'/update', 'id' => $value->id]).'" class="btn btn-xs btn-default  "  ><i class="fa fa-pencil" title="'.Yii::t('app', 'Edit').'"></i></a>';

                //$actionColumn .= '<a href="'.Url::to(['/task/event/deleteajax', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-events" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
        
            $type = ($value->type_fk == 1) ? 'case' : 'event';
			$tmp['ename'] = '<a href="'.Url::to(["/task/".$type."/view",'id'=>$value->id]).'">'.$value->name.'</a>';
			$tmp['sname'] = $value['name'];
            $tmp['delay'] = $value['delay'];
            $tmp['className'] = ( ( $value['id_dict_task_status_fk'] == 1 && $value->delay > 1 && $value->type_fk == 2)  ) ?  'danger' : ( ( $value['id_dict_task_status_fk'] == 2  && ($value['execution_time'] == 0 || empty($value['execution_time'])) ) ? 'warning' : 'normal' );
            $tmp['etype'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Rozprawa</span>' : '<span class="label bg-teal">Zadanie</span>' ;
            $tmp['deadline'] = $value['date_to'];
            $tmp['term'] = $value['event_date'].' '.( ( !empty($value['event_time']) && $value['event_time'] != '00:00' ) ? $value['event_time']: '' );
            $tmp['client'] = $value['customer']['name'];
            $tmp['case'] = $value['case']['name'];
            $tmp['estatus'] = isset($status[$value->id_dict_task_status_fk]) ? '<span class="label '.$colors2[$value['id_dict_task_status_fk']].'">'.$status[$value->id_dict_task_status_fk].'<span>' : '';
            $tmp['category'] = isset($categories[$value['id_dict_task_category_fk']]) ? '<span class="label '.$colors1[$value['id_dict_task_category_fk']].'">'.$categories[$value['id_dict_task_category_fk']].'</span>' : '';

            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($events, $tmp); $tmp = [];
		}	
		return $events;
	}
}
