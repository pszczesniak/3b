<?php

namespace app\Modules\Debt\controllers;

use Yii;
use backend\Modules\Debt\models\DebtHistory;
use backend\Modules\Debt\models\DebtHistorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

use frontend\widgets\alert\AlertWidget;

/**
 * HistoryController implements the CRUD actions for DebtAction model.
 */
class HistoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DebpHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DebtHistorySearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        
        $id = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'id' => $id
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

        $post = $_GET;
        if(isset($_GET['DebtHistorySearch'])) {
            $params = $_GET['DebtHistorySearch'];
            if(isset($params['history_note']) && !empty($params['history_note']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(name) like '%".strtolower($params['name'])."%' ");
				array_push($where, " lower(history_note) like '%".strtolower($params['history_note'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "history_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "history_date <= '".$params['date_to']."'");
            }
			if(isset($params['id_debt_action_fk']) && !empty($params['id_debt_action_fk']) ) {
				array_push($where, " id_debt_action_fk = ".$params['id_debt_action_fk']);
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, " id_employee_fk = ".$params['id_employee_fk']);
            }
			if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk'])) {
				array_push($where, " h.id_customer_fk = ".$params['id_customer_fk']);
			}
        }   
        
		$sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'h_date' ) $sortColumn = 'history_date';
        if( isset($post['sort']) && $post['sort'] == 'id_debt_action_fk' ) $sortColumn = 'dict.dict_name'.' collate `utf8_polish_ci` ';
		if( isset($post['sort']) && $post['sort'] == 'h_customer' ) $sortColumn = 'ifnull(c.symbol, c.name)'.' collate `utf8_polish_ci` ';
		if( isset($post['sort']) && $post['sort'] == 'h_employee' ) $sortColumn = 'e.lastname'.' collate `utf8_polish_ci` ';
		if( isset($post['sort']) && $post['sort'] == 'h_note' ) $sortColumn = 'history_note'.' collate `utf8_polish_ci` ';
        
        /*$fieldsData = $fieldsDataQuery->all();*/
		$query = (new \yii\db\Query())
            ->select(['h.id as id', 'history_note', 'id_debt_action_fk', 'dict_name', 'dict_color', 'dict_icon', 'history_date', 'history_time', 'e.firstname', 'e.lastname', 'ifnull(c.symbol,c.name) as cname', 'c.id as cid', 'cc.name as ccname',
                      '(select id from {{%alert}} where status = 1 and type_fk = 2 and id_fk = h.id) as alert'])
            ->from('{{%debt_history}} h')
            ->join(' JOIN', '{{%debt_action}} as dict', 'dict.id = h.id_debt_action_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = h.id_customer_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = h.id_employee_fk')
            ->join(' LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = h.id_set_fk')
            ->where( ['h.status' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn. ' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
			
		$fields = [];
		$tmp = [];
	
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/debt/history/updateajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-history" data-form="item-form" data-target="#modal-grid-item" title="Edycja" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/debt/history/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-history"><i class="fa fa-trash" title="Usuń"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['h_action'] = '<i class="'.$value['dict_icon'].'" style="color: '.$value['dict_color'].'" data-placement="right" data-toggle="tooltip" data-title="'.$value['dict_name'].'"></i>';
            $value['history_time'] = ($value['history_time'] == '00:00') ? '' : $value['history_time'];
            $tmp['h_date'] = $value['history_date'].( ($value['history_time']) ? '<br/><i class="fa fa-clock-o text--blue"></i><span class="text-navy">'.$value['history_time'].'</span>' : '');
			$tmp['h_note'] = $value['history_note'];
			$tmp['h_employee'] = $value['lastname'].' '.$value['firstname'];
            //$tmp['h_customer'] = $value['cname'];
			$tmp['h_customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.$value['cname'].'</a>';
            $tmp['alert'] = ($value['alert']) ? 
                                '<a href="/alerts/update/'.$value['alert'].'?table=history" class="btn btn-xs bg-orange gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Edytuj przypomnienie"><i class="fa fa-bell"></i></a>'.
                                '&nbsp;<a href="/alerts/delete/'.$value['alert'].'?table=history" class="deleteConfirm" data-confirm="Czy na pewno usunąć to przypomnienie?" data-placement="left" data-toggle="tooltip" data-title="Usuń przypomnienie" data-table="#table-history"><i class="fa fa-close text--red"></i></a>' : 
                                '<a href="'.Url::to(['/alerts/create', 'fid' => $value['id'], 'type' => 2]).'?table=history" class="btn btn-xs bg-green gridViewModal" data-target="#modal-grid-item" data-placement="left" data-toggle="tooltip" data-title="Ustaw przypomnienie"><i class="fa fa-plus"></i></a>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    /**
     * Displays a single CorrespondenceAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewajax($id)
    {
        $model = $this->findModel($id);
        
        $grants = $this->module->params['grants'];

        return $this->renderAjax('_viewAjax', [
            'model' => $model,  'edit' => isset($grants['correspondenceEdit']), 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)
        ]);
    }


    /**
     * Creates a new DebtHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DebtHistory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax($id) {
		$id = CustomHelpers::decode($id);
        $model = new DebtHistory();
		$model->history_date = date('Y-m-d');
		$model->id_employee_fk = $this->module->params['employeeId'];
        $model->id_customer_fk = $id;
        $model->id_set_fk = (isset($_GET['set'])) ? $_GET['set'] : 0;
   
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
           
            if($model->validate() && $model->save()) {
                return array('success' => true, 'alert' => 'Wpis został utworzony', 'id'=>$model->id);	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderPartial('_formAjaxNew', [ 'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-history" ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderPartial('_formAjaxNew', [  'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-history" ]) ;	
		}
	}

    /**
     * Updates an existing CorrespondenceAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
        $grants = $this->module->params['grants'];
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjaxNew', [  'model' => $model, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-history" ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjaxNew', [  'model' => $model, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-history"]) ;	
		}
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-history");	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => (isset($_GET['timeline'])) ? "#table-timeline" : "#table-history");	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the DebtHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DebtHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
        if (($model = DebtHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The history does not exist.');
        }
    }
    
    public function actionTimeline()
    {
        $searchModel = DebtHistory::find()->where(['status' => 1])->all();
        
        return $this->render('timeline', [
            'data' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
	
	public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
		$employee = false; $customer = false; $case = false; $dateFrom = false;  $dateTo = false; $where = [];
		
		if(isset($_GET['DebtHistorySearch'])) {
            $params = $_GET['DebtHistorySearch'];
            if(isset($params['history_note']) && !empty($params['history_note']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere(" lower(name) like '%".strtolower($params['name'])."%' ");
				array_push($where, " lower(history_note) like '%".strtolower($params['history_note'])."%' ");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "history_date >= '".$params['date_from']."'");
				$dateFrom = $params['date_from'];
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "history_date <= '".$params['date_to']."'");
				$dateTo = $params['date_to'];
            }
			if(isset($params['id_debt_action_fk']) && !empty($params['id_debt_action_fk']) ) {
				array_push($where, " id_debt_action_fk = ".$params['id_debt_action_fk']);
            }
			if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk'])) {
				array_push($where, " h.id_customer_fk = ".$params['id_customer_fk']);
				$customer = \backend\Modules\Crm\models\Customer::findOne($params['id_customer_fk']);
			}
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk'])) {
				array_push($where, " id_employee_fk = ".$params['id_employee_fk']);
				$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($params['id_employee_fk']);
			}
        }   
        
        $fields = []; $tmp = [];
		
		$sortColumn = 'h.history_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['h.id as id', 'history_note', 'id_debt_action_fk', 'dict_name', 'dict_color', 'dict_icon', 'history_date', 'history_time', 'e.firstname', 'e.lastname', 'c.name as cname', 'c.id as cid', 'cc.name as ccname',
                      '(select id from {{%alert}} where status = 1 and type_fk = 2 and id_fk = h.id) as alert'])
            ->from('{{%debt_history}} h')
            ->join(' JOIN', '{{%debt_action}} as dict', 'dict.id = h.id_debt_action_fk')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = h.id_customer_fk')
            ->join(' JOIN', '{{%company_employee}} as e', 'e.id = h.id_employee_fk')
            ->join(' LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = h.id_set_fk')
            ->where( ['h.status' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
       
		$periodLabel = ($dateFrom || $dateTo) ? ' w okresie'.((($dateFrom) ? ' od '.$dateFrom : '').(($dateTo) ? ' do '.$dateTo : '')) : '';
        
        if($employee)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia czynności pracownika '.$employee->fullname.$periodLabel);
        else if($customer)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia czynności dla klienta '.(($customer->symbol) ? $customer->symbol : $customer->symbol).$periodLabel);
        else 
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Historia czynności'.$periodLabel);        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Czynność')
			->setCellValue('B3', 'Data')
			->setCellValue('C3', 'Opis')
			->setCellValue('D3', 'Klient')
			->setCellValue('E3', 'Pracownik')
			->setCellValue('F3', 'Sprawa');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['dict_name'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['history_date']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['history_note']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['cname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['lastname'].' '.$record['firstname']);                
				$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['ccname']);
		
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Historia windykacji');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'HistoriaWindykacji'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
	
	public function actionDone($id) {
		$id = CustomHelpers::decode($id);
		
		$alert = \backend\Modules\Alert\models\Alert::findOne($id);
		$parent = DebtHistory::findOne($alert->id_fk);
		
        $model = new DebtHistory();
		$model->history_date = date('Y-m-d');
		$model->id_employee_fk = $parent->id_employee_fk;
        $model->id_customer_fk = $parent->id_customer_fk;
		$model->id_set_fk = $parent->id_set_fk;
   
        //$grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
             
            if($model->validate() && $model->save()) {
                $alert->is_done = 1; $alert->save();
				return array('success' => true, 'alert' => 'Wpis został utworzony', 'id'=>$model->id, 'action' => 'alertDone', 'alertWidget' => AlertWidget::widget(['refresh' => true]) );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderPartial('_formDone', [ 'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-history" ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderPartial('_formDone', [  'model' => $model, 'table' => ($id) ? "#table-timeline" : "#table-history" ]) ;	
		}
	}
}
