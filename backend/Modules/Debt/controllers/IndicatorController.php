<?php

namespace app\Modules\Debt\controllers;

use Yii;
use backend\Modules\Debt\models\Indicator;
use backend\Modules\Debt\models\IndicatorValue;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;

/**
 * IndicatorController implements the CRUD actions for Indicator model.
 */
class IndicatorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Indicator();

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $params = Yii::$app->request->queryParams;
        
		$fieldsData = [];
        if(isset($params['Indicator']['id']) && !empty($params['Indicator']['id']))
            $fieldsData = IndicatorValue::find()->where(['status' => 1, 'id_indicator_fk' => $params['Indicator']['id']])->orderby('id desc')->all();
			
		$fields = [];
		$tmp = [];
		      
        $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/config/indicator/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/config/indicator/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash"></i></a>';
        $actionColumn .= '</div>';
		foreach($fieldsData as $key=>$value) {
			
			$tmp['date_from'] = $value->date_from;
            $tmp['date_to'] = $value->date_to;
            $tmp['value'] = $value->value;
           
            $tmp['actions'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    /**
     * Displays a single Dictionary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dictionary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dictionary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax($id=0) {
		
		//$id = CustomHelpers::decode($id);
        if($id == 0) {
            return '<div class="alert alert-danger">Proszę wybrać słownik odsetek</div>';
        }
        $model = new IndicatorValue();
        $model->id_indicator_fk = $id;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/debt/indicator/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
                $actionColumn .= '<a href="/debt/indicator/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items">U</a>';
                $actionColumn .= '</div>';
				
                $data['name'] = $model->name;
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
				
				return array('success' => true, 'row' => $data, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Wartość słownika <b>'.$model->name.'</b> została dodana' );
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			if($id == 0)
                return '<div class="alert alert-danger">Proszę wybrać typ słownika</div>';
            else
                return  $this->renderAjax('_formAjax', [  'model' => $model,]) ;	
		}
	}

    /**
     * Updates an existing Dictionary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->id_dictionary_fk == 4) {
            $customData = \yii\helpers\Json::decode($model->custom_data);
            $model->icon = $customData['icon'];
            $model->color = $customData['color'];
        }
        if($model->id_dictionary_fk == 12) {
            $customData = \yii\helpers\Json::decode($model->custom_data);
            $model->color = $customData['color'];
            $model->sideOrder = (isset($customData['sideOrder'])) ? $customData['sideOrder'] : 1;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        } 
    }
    
    public function actionUpdateajax($id, $type)
    {
        $model = IndicatorValue::findOne($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="/debt/indicator/updateajax/%d" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >E</a>';
                $actionColumn .= '<a href="/debt/indicator/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items">U</a>';
                $actionColumn .= '</div>';
				
                 $data['name'] = $model->name;
               
                $data['actions'] = sprintf($actionColumn, $model->id, $model->id, $model->id);
                $data['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $data['id'] = $model->id;
				
				return array('success' => true, 'row' => $data, 'action' => 'updateRow', 'index' => $_GET['index'], 'alert' => 'Wartość słownika <b>'.$model->name.'</b> została zaktualizowana.' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing Dictionary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = IndicatorValue::findOne($id);
        if($model->delete()) {
            return array('success' => true, 'alert' => 'Wartość słownika <b>'.$model->name.'</b> została usunięta', 'id' => $id);	
        } else {
            return array('success' => false, 'alert' => 'Wartość słownika <b>'.$model->name.'</b> nie została usunięta', 'id' => $id);	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the Dictionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dictionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dictionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreateinline() {
		
		$model = new Indicator();
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->name = $_POST['value'];
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Wartość słownika <b>'.$model->name.'</b> została dodana', 'id' => $model->id, 'name' => $model->name );
			} else {
                $alert = "Ten słownik juz posiada taką wartość";
                foreach($model->getErrors() as $key => $value){
                    $alert = $value;
                }
                return array('success' => false, 'alert' => $alert );	
			}		
		} else {
            return  $this->renderAjax('_formInline', [  'model' => $model,]) ;	
		}
	}
}
