<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use \backend\Modules\Debt\models\DebtPaymentClaim;

/**
 * This is the model class for table "{{%debt_payment}}".
 *
 * @property integer $id
 * @property integer $account_type_fk
 * @property integer $id_dict_payment_type_fk
 * @property integer $id_employee_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property integer $id_claim_fk
 * @property integer $id_currency_fk
 * @property double $payment_amount
 * @property string $payment_date
 * @property string $impact_date
 * @property string $payment_note
 * @property string $payment_custom
 * @property integer $payment_settlements
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DebtPayment extends \yii\db\ActiveRecord
{
    public $claims_list = [];
    public $settlement = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type_fk', 'id_dict_payment_type_fk', 'id_employee_fk', 'id_customer_fk', 'payment_amount'], 'required'],
            [['account_type_fk', 'id_dict_payment_type_fk', 'id_employee_fk', 'id_customer_fk', 'id_set_fk', 'id_claim_fk', 'id_currency_fk', 'payment_settlements', 'status', 'created_by', 'updated_by', 'deleted_by', 'settlement'], 'integer'],
            [['payment_amount'], 'number'],
            [['payment_date', 'impact_date', 'created_at', 'updated_at', 'deleted_at', 'claims_list'], 'safe'],
            [['payment_note', 'payment_custom'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_type_fk' => Yii::t('app', 'Konto'),
            'id_dict_payment_type_fk' => Yii::t('app', 'Typ płatności'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_set_fk' => Yii::t('app', 'Sprawa'),
            'id_claim_fk' => Yii::t('app', 'Roszczenie'),
            'id_currency_fk' => Yii::t('app', 'Waluta'),
            'payment_amount' => Yii::t('app', 'Kwota'),
            'payment_date' => Yii::t('app', 'Data wpłaty'),
            'impact_date' => Yii::t('app', 'Data wpływu'),
            'payment_note' => Yii::t('app', 'Notatka'),
            'payment_custom' => Yii::t('app', 'Payment Custom'),
            'payment_settlements' => Yii::t('app', 'Payment Settlements'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'claims_list' => 'Roszczenia',
            'settlement' => 'rozlicz wpłatę'
        ];
    }
    
    public function beforeSave($insert) {
		       
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    static public function getAccounts() {
        $types = [];
        $types[1] = 'Własne';
        $types[2] = 'Klienta';
        
        return $types;
    }
    
    public static function listTypes() {
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 15])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public function getClaims() { 
        return DebtPaymentClaim::find()->where(['status' => 1, 'id_payment_fk' => $this->id])->all();
    }
}
