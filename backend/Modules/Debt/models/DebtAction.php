<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%debt_action}}".
 *
 * @property integer $id
 * @property integer $dict_type_fk
 * @property string $dict_name
 * @property string $dict_symbol
 * @property string $dict_icon
 * @property string $dict_color
 * @property string $dict_custom
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DebtAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_action}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dict_type_fk', 'dict_name'], 'required'],
            [['dict_type_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['dict_custom'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['dict_name', 'dict_symbol'], 'string', 'max' => 255],
            [['dict_icon', 'dict_color'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dict_type_fk' => Yii::t('app', 'Dict Type Fk'),
            'dict_name' => Yii::t('app', 'Nazwa'),
            'dict_symbol' => Yii::t('app', 'Symbol'),
            'dict_icon' => Yii::t('app', 'Ikonka'),
            'dict_color' => Yii::t('app', 'Kolor'),
            'dict_custom' => Yii::t('app', 'Dict Custom'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {       
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function getList() {
        return \backend\Modules\Debt\models\DebtAction::find()->where(['status' => 1])->orderby('dict_name')->all();   
    }
}
