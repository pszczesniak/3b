<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%debt_arch}}".
 *
 * @property integer $id
 * @property integer $table_fk
 * @property integer $id_root_fk
 * @property string $user_action
 * @property string $data_arch
 * @property string $data_change
 * @property string $created_at
 * @property integer $created_by
 */
class DebtArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_fk', 'id_root_fk'], 'required'],
            [['table_fk', 'id_root_fk', 'created_by'], 'integer'],
            [['data_arch', 'data_change'], 'string'],
            [['created_at'], 'safe'],
            [['user_action'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_fk' => Yii::t('app', 'Table Fk'),
            'id_root_fk' => Yii::t('app', 'Id Root Fk'),
            'user_action' => Yii::t('app', 'User Action'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'data_change' => Yii::t('app', 'Data Change'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function beforeSave($insert) {
		
        $this->custom_data = \yii\helpers\Json::encode(['color' => $this->color, 'icon' => $this->icon, 'sideOrder' => $this->sideOrder]);
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
