<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%debt_history}}".
 *
 * @property integer $id
 * @property integer $id_debt_action_fk
 * @property integer $id_employee_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property string $history_date
 * @property string $history_time
 * @property string $history_note
 * @property string $history_custom
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DebtHistory extends \yii\db\ActiveRecord
{
    public $date_from;
    public $date_to;
    public $acc_action = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_debt_action_fk', 'id_employee_fk', 'id_customer_fk', 'history_date'], 'required'],
            [['id_debt_action_fk', 'id_employee_fk', 'id_customer_fk', 'id_set_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_acc_action_fk', 'show_client'], 'integer'],
            [['history_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['history_note', 'history_custom'], 'string'],
            [['history_time'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_debt_action_fk' => Yii::t('app', 'Czynność'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_set_fk' => Yii::t('app', 'Postepowanie'),
            'history_date' => Yii::t('app', 'Data'),
            'history_time' => Yii::t('app', 'Czas'),
            'history_note' => Yii::t('app', 'Notatka'),
            'history_custom' => Yii::t('app', 'History Custom'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getDict()  {
		return $this->hasOne(\backend\Modules\Debt\models\DebtAction::className(), ['id' => 'id_debt_action_fk']);
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by); 
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getCustomer()  {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getCase()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_set_fk']);
    }
	
	public function getEmployee()  {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
}
