<?php

namespace backend\Modules\Dict\models;

use Yii;

/**
 * This is the model class for table "{{%dictionary}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $is_system
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name_langs', 'describe', 'describe_langs', 'custom_data'], 'string'],
            [['is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'is_system' => Yii::t('app', 'Is System'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public static function getList() {
        return Dictionary::find()->all();
    }
}
