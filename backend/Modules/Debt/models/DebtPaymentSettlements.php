<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%debt_payment_settlements}}".
 *
 * @property integer $id
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property integer $id_claim_fk
 * @property double $s_capital
 * @property double $s_interest
 * @property double $p_capital_percent
 * @property double $p_interest_percent
 * @property double $p_capital
 * @property double $p_interest
 * @property integer $custom_settlements
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DebtPaymentSettlements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_payment_settlements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_claim_fk'], 'required'],
            [['id_payment_fk', 'id_customer_fk', 'id_set_fk', 'id_claim_fk', 'custom_settlements', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['s_capital', 's_interest', 'p_capital_percent', 'p_interest_percent', 'p_capital', 'p_interest'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'id_set_fk' => Yii::t('app', 'Id Set Fk'),
            'id_claim_fk' => Yii::t('app', 'Id Claim Fk'),
            's_capital' => Yii::t('app', 'S Capital'),
            's_interest' => Yii::t('app', 'S Interest'),
            'p_capital_percent' => Yii::t('app', 'P Capital Percent'),
            'p_interest_percent' => Yii::t('app', 'P Interest Percent'),
            'p_capital' => Yii::t('app', 'P Capital'),
            'p_interest' => Yii::t('app', 'P Interest'),
            'custom_settlements' => Yii::t('app', 'Custom Settlements'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
