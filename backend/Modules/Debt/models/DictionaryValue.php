<?php

namespace backend\Modules\Dict\models;

use Yii;

/**
 * This is the model class for table "{{%dictionary_value}}".
 *
 * @property integer $id
 * @property integer $id_dictionary_fk
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $is_system
 * @property integer $in_use
 * @property string $custom_data
 * @property string $data_arch
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DictionaryValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dictionary_fk', 'is_system', 'in_use', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['name_langs', 'describe', 'describe_langs', 'custom_data', 'data_arch'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_dictionary_fk' => Yii::t('app', 'Id Dictionary Fk'),
            'name' => Yii::t('app', 'Name'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'is_system' => Yii::t('app', 'Is System'),
            'in_use' => Yii::t('app', 'In Use'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
