<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use \backend\Modules\Debt\models\IndicatorArch;

/**
 * This is the model class for table "{{%indicator}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $name
 * @property string $symbol
 * @property integer $is_system
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Indicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%indicator}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['name', 'symbol'], 'unique'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'is_system' => Yii::t('app', 'Is System'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {       
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],			
		];
	}
    
    /*
    public function afterSave($insert, $changedAttributes) {
        if (!$insert && $this->status == 1) {
            $modelArch = new AlertArch();
            $modelArch->id_alert_fk = $this->id;
            if(!$this->user_action) {
                $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
            }
            
            $modelArch->data_change = \yii\helpers\Json::encode($this);
            $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
        } 

		parent::afterSave($insert, $changedAttributes);
	}*/
    
    public static function getList() {
        return Indicator::find()->where(['status' => 1])->all();
    }
}
