<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Debt\models\DebtPayment;

/**
 * DebtPaymentSearch represents the model behind the search form about `backend\Modules\Debt\models\DebtPayment`.
 */
class DebtPaymentSearch extends DebtPayment
{
    public $date_from;
    public $date_to;
    public $amount_from;
    public $amount_to;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type_fk', 'id_dict_payment_type_fk', 'id_employee_fk', 'id_customer_fk', 'id_set_fk', 'id_claim_fk', 'id_currenct_fk', 'payment_settlements', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['payment_date', 'impact_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DebtHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'history_note', $this->history_note]);

        return $dataProvider;
    }
}
