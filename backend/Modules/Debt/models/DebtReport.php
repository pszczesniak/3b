<?php

namespace backend\Modules\Debt\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%debt_report}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_customer_fk
 * @property string $customer_name
 * @property string $symbol
 * @property string $nip
 * @property string $invoice_no
 * @property integer $company_fk
 * @property double $incomes
 * @property double $costs
 * @property integer $after_deadline
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DebtReport extends \yii\db\ActiveRecord
{
    public $file;
    public $fileName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%debt_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_customer_fk', 'company_fk', 'after_deadline', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['incomes', 'costs'], 'number'],
            [['describe', 'custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'file'], 'safe'],
            [['customer_name'], 'string', 'max' => 2000],
            [['symbol'], 'string', 'max' => 1000],
            [['nip', 'invoice_no'], 'string', 'max' => 50],
            [['file', 'company_fk'], 'required'],
            [['file'], 'file'/*, 'extensions' => 'xls'/*,'maxSize'=>1024 * 1024 * 5*/],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'customer_name' => Yii::t('app', 'Klient'),
            'symbol' => Yii::t('app', 'Symbol'),
            'nip' => Yii::t('app', 'Nip'),
            'invoice_no' => Yii::t('app', 'Invoice No'),
            'company_fk' => Yii::t('app', 'Firma'),
            'incomes' => Yii::t('app', 'Incomes'),
            'costs' => Yii::t('app', 'Costs'),
            'after_deadline' => Yii::t('app', 'After Deadline'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'file' => 'Pobierz plik',
        ];
    }
    
    public function upload()   {
        if ($this->validate()) {
            $this->fileName = str_replace(" ", "_", $this->file->baseName) . '_' . time() . '.' . $this->file->extension;
            $this->file->saveAs('uploads/debt/' . $this->fileName);
            return $this->fileName;
        } else {
            return false;
        }
    }
    
    public static function getCompanies() {
        return [1 => 'SIW SA', 2 => 'SIW SK'];
    }
}
