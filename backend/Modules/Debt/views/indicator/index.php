<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Odsetki');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'config-indicator-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 
	<div id="toolbar-data" class="btn-group">
		<?= (1==1)?Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/debt/indicator/createajax']) , 
					['class' => 'btn btn-success btn-icon gridViewModal', 
					 'id' => 'value-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]):'' ?>
        <!--<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-data"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>-->
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-pagination-h-align="left"
                data-pagination-detail-h-align="right"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-url=<?= Url::to(['/debt/indicator/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="date_from"  data-sortable="true">Data od</th>
                    <th data-field="date_to"  data-sortable="true">Data do</th>
                    <th data-field="value"  data-sortable="true">Wartość [%]</th>
                    <th data-field="actions" data-events="actionEvents"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>
</div>
<?php $this->endContent(); ?>
<script type="text/javascript">
    document.getElementById('indicator-id').onchange = function(event) {
        document.getElementById('value-create').setAttribute('href', '/debt/indicator/createajax/'+event.target.value );
    }
</script>