<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
       // 'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body">
        <div class="alert alert-info">Stawka dla słownika <b><?= $model->indicator['name'] ?></b></div>
        <div class="grid">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label for="accorder-date_from" class="control-label">Obowiązuje od</label>
                    <div class='input-group date' id='datetimepicker_start'>
                        <input type='text' class="form-control" id="accorder-date_from" name="AccOrder[date_from]" value="<?= $model->date_from ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
                });
            </script>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label for="accorder-date_to" class="control-label">Obowiązuje do</label>
                    <div class='input-group date' id='datetimepicker_end'>
                        <input type='text' class="form-control" id="accorder-date_to" name="AccOrder[date_to]" value="<?= $model->date_to ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
                   
                });
            </script>
            <div class="col-md-6 col-sm-12  col-xs-12"> <?= $form->field($model, 'value', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span>%</span></span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number'] ) ?> </div>
        </div>
    </div>   
                    
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

</script>
