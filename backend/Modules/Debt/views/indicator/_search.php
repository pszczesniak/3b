<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'form-data-search'
    ]); ?>
    
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'id', ['template' => 
							 '{label}'
								.'<div class="input-group ">'
									.'{input}'
									.'<div class="input-group-btn">'
										.Html::a('<span class="fa fa-plus"></span>', Url::to(['/debt/indicator/createinline']) , 
											['class' => 'insertInline text--white btn bg-green', 
											 'data-target' => "#indicator-dict-insert", 
											 'data-input' => ".indicator-dict"
											]) 
									.'</div>'
								.'</div>'
							   .'{error}{hint}'
						   ])->dropDownList( ArrayHelper::map(\backend\Modules\Config\models\Indicator::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control data-search-list indicator-dict'] ) ?>
				<div id="indicator-dict-insert" class="insert-inline bg-purple2 none"> </div> 
        </div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
