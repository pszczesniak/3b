<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="form-group field-dictValue required">
        <!--<label class="control-label" for="dictValue">Nazwa</label>-->
        <input id="dictValue" class="form-control" name="dictValue" maxlength="300" aria-required="true" type="text">
        <div class="help-block text--red" id="dictValueHint"></div>
    </div>
                 
    <div class="form-group align-right"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <a href="<?= Url::to(['/debt/indicator/createinline']) ?>" class="btn btn-xs btn-success saveInline">Dodaj wartość</a>
        <button class="btn btn-xs btn-default closeInline" type="button" >Odrzuć</button>
    </div>
