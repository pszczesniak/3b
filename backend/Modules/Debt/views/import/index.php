<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Preliminarz');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'address-index', 'title'=>Html::encode($this->title))) ?>

    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-report" data-form="#filter-debt-report-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="report-filter" aria-expanded="true" href="#report-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="report-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
    <div class="alert alert-info">Stan na dzień: SIW SA - <b id="company-1"><?= $perDay[0] ?></b>, SIW SK - <b id="company-2"><?= $perDay[1] ?></b></div>
	<div id="toolbar-report" class="btn-group toolbar-table-widget">
		<?= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) || count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0) ? Html::a('<i class="fa fa-upload"></i>Import danych', Url::to(['/debt/import/upload']) , 
					['class' => 'btn btn-info btn-icon gridViewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-table' => "table-report",
					 'data-title' => "<i class='fa fa-upload'></i>Importuj dane"
					]) : '' 	?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-report"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-report"
                data-toolbar="#toolbar-report" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
				data-detail-view="true"
                data-detail-formatter="logFormatter"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-debt-report-search"
                data-url=<?= Url::to(['/debt/import/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
					<th data-field="typeFormatter" data-visible="false">formatter</th>
                    <th data-field="company" data-sortable="true">Firma</th>
                    <th data-field="customer" data-sortable="true">Klient</th>
                    <th data-field="nip" data-sortable="true">NIP</th>
                    <th data-field="ultima" data-sortable="true">Ultima</th>
                    <th data-field="incomes" data-sortable="true">Przychód</th>
                    <!--<th data-field="costs" data-sortable="true">Rozchód</th>-->
                    <th data-field="deadline" data-sortable="true">Po terminie</th>
                    <!--<th data-field="actions" data-events="actionEvents" data-width="90px"></th>-->
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>
