<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxFormFile', 'data-target' => "#modal-grid-item", 'data-table' => "#table-report", 'enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
        <?= $form->field($model, 'company_fk')->dropDownList(\backend\Modules\Debt\models\DebtReport::getCompanies(), ['id' => 'import_file-company'/*'prompt' => '-wybierz'*/]) ?>
        <?php /* $form->field($model, 'file')->fileInput(['id' => 'import_file', 'class' => 'ufile'])*/ ?>
        <div style="position:relative;">
            <a class='btn btn-info' href='javascript:;'>
                Wybierz plik...
                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  id="import_file" onchange='$("#import_file-info").html($(this).val());'>
            </a>
            &nbsp;
            <span class='label label-info' id="import_file-info"></span>
        </div>
    </div>
    <div class="modal-footer"> 
        <div class="form-group">
            <?= Html::submitButton('Pobierz dane', ['class' => 'btn btn-info']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>