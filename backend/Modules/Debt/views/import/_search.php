<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-debt-report-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-report']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-6 col-md-6 col-xs-12"> <?= $form->field($model, 'customer_name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-report', 'data-form' => '#filter-debt_report-search' ]) ?></div>  
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'nip')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-report', 'data-form' => '#filter-debt_report-search' ]) ?></div> 
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'company_fk')->dropDownList(\backend\Modules\Debt\models\DebtReport::getCompanies(), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-report', 'data-form' => '#filter-debt_report-search' ]) ?></div> 
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>