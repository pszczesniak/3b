<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-debt-history-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-history']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'id_debt_action_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Debt\models\DebtAction::getList(), 'id', 'dict_name'), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-history', 'data-form' => '#filter-debt_history-search'] ) ?></div>
		<div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-debthistorysearch-date_from">
				<label for="debthistorysearch-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="debthistorysearch-date_from" name="DebtHistorySearch[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12">
			<div class="form-group field-debthistorysearch-date_to">
				<label for="debthistorysearch-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="debthistorysearch-date_to" name="DebtHistorySearch[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-history', 'data-form' => '#filter-debt-history-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-history', 'data-form' => '#filter-debt-history-search'  ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_set_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups(-1), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-history', 'data-form' => '#filter-debt-history-search'  ] )->label('Sprawa/Projekt') ?></div>
        <div class="col-sm-12 col-md-6 col-xs-12"> <?= $form->field($model, 'history_note')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-history', 'data-form' => '#filter-debt_history-search' ]) ?></div>  
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>