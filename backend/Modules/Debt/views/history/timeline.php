<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Historia');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'address-index', 'title'=>Html::encode($this->title))) ?>
    <div class="well well-small"></div>
	<ul class="cbp_tmtimeline">
        <?php
            foreach($data as $key => $item) {

                echo '<li>'
                        .'<time class="cbp_tmtime" datetime="'. $item->created_at .'"><span>'.date('Y-m-d', strtotime($item->created_at)).'</span> <span>'.date('H:i', strtotime($item->created_at)).'</span></time>'
                        .'<div class="cbp_tmicon" style="background-color: '.$item->dict['dict_color'].'" data-toggle="tooltip" data-title="'.$item->dict['dict_name'].'"><i class="'.$item->dict['dict_icon'].'"></i></div>'
                        .'<div class="cbp_tmlabel">'
                            .'<h2>'.$item->customer['name'].'</h2>'
                            //.'<p>Pracownik: '.$item->creator['fullname'].' </p>'
                            . '<div class="desc">'.$item->history_note.'</div>' 
                        .'</div>'
                    .'</li>';
              
            }
        ?>
    </ul>
 <?php $this->endContent(); ?>      