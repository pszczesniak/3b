<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Historia');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'address-index', 'title'=>Html::encode($this->title))) ?>

    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-history" data-form="#filter-debt-history-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="history-filter" aria-expanded="true" href="#history-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="history-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
 
	<div id="toolbar-history" class="btn-group toolbar-table-widget">
		<?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/debt/history/createajax', 'id' => 0]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ])  ?>
		<?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/debt/history/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-debt-history-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]) 	?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-history"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-history"
                data-toolbar="#toolbar-history" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-debt-history-search"
                data-url=<?= Url::to(['/debt/history/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="h_date" data-sortable="true" data-align="center" data-width="50px">Data</th>
                    <th data-field="h_action" data-sortable="true" data-width="20px" data-align="center"></th>
                    <th data-field="h_note" data-sortable="true">Opis</th>
                    <th data-field="h_customer" data-sortable="true">Klient</th>
                    <th data-field="h_employee" data-sortable="true">Pracownik</th>
                    <th data-field="alert" data-sortable="false" data-align="center" data-width="20px;"><i class="fa fa-bell text--yellow"></i></th>
                    <th data-field="actions" data-events="actionEvents" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>
