<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => $table],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="grid">
			<div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_debt_action_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Debt\models\DebtAction::getList(), 'id', 'dict_name'), ['prompt' => '-wybierz']) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList($model->id_employee_fk), 'id', 'fullname'), ['prompt' => '-wybierz']) ?></div>
            <div class="col-sm-4 col-xs-12">
                <div class="grid grid--0">
                    <div class="col-xs-7 ">
                        <div class="form-group field-debthistory-history_date">
                            <label for="debthistory-history_date" class="control-label">Dzień</label>
                            <input type='text' class="form-control" id="task_date" name="DebtHistory[history_date]" value="<?= $model->history_date ?>"/>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label for="debthistory-history_time" class="control-label">Godzina</label>
                            <div class='input-group date' id='task_time' >
                                <input type='text' class="form-control" name="DebtHistory[history_time]" value="<?= $model->history_time ?>" />
                                <span class="input-group-addon bg-blue" title="Ustaw godzinę. Brak podania godziny ustawi zadanie za cały dzień.">
                                    <span class="fa fa-clock-o text--white"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
            </div>
            <div class="col-sm-6 col-xs-12">
                <?= $form->field($model, 'id_set_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'history_note')->textarea(['rows' => 3]) ?>
            </div>
		</div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton('Zamknij powiadomienie i utwórz wpis w historii', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_time').datetimepicker({  format: 'LT', showClear: true, showClose: true });
    });
    
    document.getElementById('debthistory-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+((this.value) ? this.value : 0 ), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('debthistory-id_set_fk').innerHTML = result.list; 
            }
        }
       xhr.send();
        return false;
    }
</script>