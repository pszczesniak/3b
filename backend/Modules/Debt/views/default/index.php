<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\tasks\EventsTable;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Raport na dzień');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="grid">
    <div class="col-md-4 sm-6 xs-12"><?= $this->render('_stats', ['stats' => $stats]) ?></div>
    <div class="col-md-8 sm-6 xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php', array('class'=>'address-index', 'title' => 'Czynności na dziś' )) ?>
            <?= EventsTable::widget(['dataUrl' => Url::to(['/debt/default/events']), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => 0]), 'actionWidth' => '90px' ]) ?>
        <?php $this->endContent(); ?>
    </div>
</div>
