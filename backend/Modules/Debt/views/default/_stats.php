<div class="white-box">
    <h3 class="box-title">Bieżący stan <small>[PLN]</small>
        <!--<div class="col-md-3 col-sm-4 col-xs-6 pull-right"> <select class="form-control pull-right grid grid--0 b-none">  <option>March 2017</option> </select> </div>-->
    </h3>
    <div class="stats-report">
        <div class="grid">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h2>Saldo</h2>
                <p>Windykacja roszczeń</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 ">
                <h1 class="text-right text--green m-t-20"><?= number_format($stats['balance'], 2, '.', '  ') ?></h1>
            </div>
        </div>
    </div>
    <div class="table-responsive stats-table">
        <table class="table">
            <tbody>
                <tr>
                    <td class="txt-oflo">Należności główne</td>
                    <td align="right"><span class="label bg-purple label-rouded"><?= number_format($stats['charge'], 2, '.', '  ') ?></span> </td>
                </tr>
                <tr>
                    <td class="txt-oflo">Koszty</td>
                    <td align="right"><span class="label bg-red label-rouded"><?= number_format($stats['costs'], 2, '.', '  ') ?></span></td>
                </tr>
                <tr>
                    <td class="txt-oflo">Odsetki</td>
                    <td align="right"><span class="label bg-orange label-rouded"><?= number_format($stats['interest'], 2, '.', '  ') ?></span></td>
                </tr>
                <tr>
                    <td class="txt-oflo">Wpłaty</td>
                    <td align="right"><span class="label bg-teal label-rouded"><?= number_format($stats['payments'], 2, '.', '  ') ?></span></td>
                </tr>
               
            </tbody>
        </table> 
        <!-- <a href="#">Check all the sales</a>-->
    </div> 
</div>