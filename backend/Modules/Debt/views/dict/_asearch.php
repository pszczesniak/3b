<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-debt-dict-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-dict']
    ]); ?>
    
    <div class="col-xs-12"> <?= $form->field($model, 'dict_name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-dict', 'data-form' => '#filter-debt-dict-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>

    <?php ActiveForm::end(); ?>

</div>
