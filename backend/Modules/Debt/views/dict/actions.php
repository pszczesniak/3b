<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Słownik czynności');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'dict-dictionary-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_asearch', ['model' => $searchModel]); ?>
 
	<div id="toolbar-dict" class="btn-group toolbar-table-widget">
		 <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/debt/dict/caction'] ) , 
					['class' => 'btn btn-success btn-icon gridViewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) ?>

		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-dict"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-dict"
                data-toolbar="#toolbar-dict" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-pagination-h-align="left"
                data-pagination-detail-h-align="right"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-url=<?= Url::to(['/debt/dict/actions']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true">Nazwa</th>
                    <th data-field="symbol"  data-sortable="true">Symbol</th>
                    <th data-field="color"  data-sortable="false" data-align="center" data-width="20px"></th>
                    <th data-field="icon"  data-sortable="false" data-align="center" data-width="20px"></th>
                    <th data-field="actions" data-events="actionEvents" data-width="80px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modalAjaxForm',
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 
</div>
<?php $this->endContent(); ?>
