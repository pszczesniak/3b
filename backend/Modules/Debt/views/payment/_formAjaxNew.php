<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => $table],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		
        <div class="grid">
			<div class="col-sm-3 col-xs-6"><?= $form->field($model, 'account_type_fk')->dropDownList( \backend\Modules\Debt\models\DebtPayment::getAccounts()) ?></div>
            <div class="col-sm-3 col-xs-6"><?= $form->field($model, 'id_dict_payment_type_fk')->dropDownList( \backend\Modules\Debt\models\DebtPayment::listTypes()) ?></div>
            <div class="col-sm-2 col-xs-4"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [] ) ?> </div>
            <div class="col-sm-4 col-xs-8"><?= $form->field($model, 'payment_amount', [])->textInput( ['class' => 'form-control number'] ) ?></div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group field-debtpayment-payment_date">
                    <label for="debtpayment-payment_date" class="control-label">Data wpłaty</label>
                    <input type='text' class="form-control" id="task_date" name="DebtPayment[payment_date]" value="<?= $model->payment_date ?>"/>
                    <div class="help-block"></div>
                </div>
            </div>  
            <div class="col-sm-3 col-xs-6">
                <div class="form-group field-debtpayment-impact_date">
                    <label for="debtpayment-impact_date" class="control-label">Data wpływu</label>
                    <input type='text' class="form-control" id="impact_date" name="DebtPayment[impact_date]" value="<?= $model->impact_date ?>"/>
                    <div class="help-block"></div>
                </div>
            </div>      
            <div class="col-sm-6 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList($model->id_employee_fk), 'id', 'fullname'), ['prompt' => '-wybierz']) ?></div>       
            <div class="col-sm-4 col-xs-6">
                <?= $form->field($model, 'id_customer_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
            </div>
            <div class="col-sm-4 col-xs-6">
                <?= $form->field($model, 'id_set_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
            </div>
            <div class="col-sm-4 col-xs-6">
                <?= $form->field($model, 'claims_list')->dropDownList(ArrayHelper::map(\backend\Modules\Task\models\CaseClaim::getList($model->id_set_fk), 'id', 'title'), ['multiple' => true, 'data-placeholder' => '- wybierz -', 'class' => 'form-control ms-select-ajax'] ) ?> 
            </div>
            <div class="col-xs-12 alert alert-info"><?= $form->field($model, 'settlement', [/*'template' => '{input} {label}'*/])->checkbox([/*'id' => 'calendar-all-day'*/]) ?></div>
            <div class="col-xs-12">
                <?= $form->field($model, 'payment_note')->textarea(['rows' => 3]) ?>
            </div>
		</div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
        $('#task_date').on("dp.change", function (e) {            
                        minDate = e.date.format('YYYY-MM-DD');
                        if( $('#impact_date').length > 0 ) {
                            $('#impact_date input').val(minDate);
                        }
                    });
        $('#impact_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true });
    });
    
    document.getElementById('debtpayment-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+((this.value) ? this.value : 0 ), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('debtpayment-id_set_fk').innerHTML = result.list; 
            }
        }
       xhr.send();
        return false;
    }
    
    document.getElementById('debtpayment-id_set_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        var caseId = 0;
        if(this.value) caseId = this.value;
       
        xhr.open('POST', "<?= Url::to(['/task/default/cinfo']) ?>?id="+caseId+'&without=1', true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('debtpayment-claims_list').innerHTML = result.claimsList;
                $('#debtpayment-claims_list').multiselect('rebuild');
                $('#debtpayment-claims_list').multiselect('refresh');
            }
        }
       xhr.send();
       
       return false;
    }
</script>