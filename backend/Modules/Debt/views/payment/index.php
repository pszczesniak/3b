<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wpłaty');
$this->params['breadcrumbs'][] = 'Windykacja';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'address-index', 'title'=>Html::encode($this->title))) ?>

    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-payment" data-form="#filter-debt-payment-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="payment-filter" aria-expanded="true" href="#payment-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="payment-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
 
	<div id="toolbar-payment" class="btn-group toolbar-table-widget">
		<?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/debt/payment/createajax', 'id' => 0]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ])  ?>
		<?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/debt/payment/export']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-debt-payment-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]) 	?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-payment"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-payment"
                data-toolbar="#toolbar-payment" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-debt-payment-search"
                data-url=<?= Url::to(['/debt/payment/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="p_date" data-sortable="true" data-align="center" data-width="50px">Data</th>
                    <th data-field="p_amount" data-sortable="true" data-align="right">Kwota</th>
                    <th data-field="p_customer" data-sortable="true">Klient</th>
                    <th data-field="p_set" data-sortable="true">Postępowanie</th>        
                    <th data-field="p_employee" data-sortable="true">Pracownik</th>
                    <th data-field="actions" data-events="actionEvents" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>
