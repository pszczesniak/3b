<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calpaymentearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-debt-payment-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-payment']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'account_type_fk')->dropDownList(\backend\Modules\Debt\models\DebtPayment::getAccounts(), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-payment', 'data-form' => '#filter-debt-payment-search'] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'id_dict_payment_type_fk')->dropDownList(\backend\Modules\Debt\models\DebtPayment::listTypes(), ['prompt' => '-wybierz', 'class' => 'form-control widget-search-list', 'data-table' => '#table-payment', 'data-form' => '#filter-debt-payment-search'] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6">
			<div class="form-group field-datepayment-date_from">
				<label for="datepayment-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="datepayment-date_from" name="DebtPayment[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-3 col-md-3 col-xs-6">
			<div class="form-group field-datepayment-date_to">
				<label for="datepayment-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="datepayment-date_to" name="DebtPayment[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-sm-2 col-md-2 col-xs-6"> <?= $form->field($model, 'amount_from')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-payment', 'data-form' => '#filter-payment-search' ])->label('Kwota od') ?></div>
        <div class="col-sm-2 col-md-2 col-xs-6"> <?= $form->field($model, 'amount_to')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-payment', 'data-form' => '#filter-payment-search' ])->label('Kwota do') ?></div>
        <div class="col-sm-2 col-md-2 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-payment', 'data-form' => '#filter-debt-payment-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-payment', 'data-form' => '#filter-debt-payment-search'  ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"><?= $form->field($model, 'id_set_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups(-1), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-payment', 'data-form' => '#filter-debt-payment-search'  ] )->label('Sprawa/Projekt') ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>