<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Correspondence\models\CorrespondenceAddress */

$this->title = Yii::t('app', 'Create Correspondence Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Correspondence Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="correspondence-address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
