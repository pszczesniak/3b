<?php

namespace backend\Modules\Config\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%indicator_value}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_indicator_fk
 * @property string $name
 * @property string $describe
 * @property string $date_from
 * @property string $date_to
 * @property double $range_from
 * @property double $range_to
 * @property double $value
 * @property integer $is_system
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class IndicatorValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%indicator_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_indicator_fk', 'is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_indicator_fk', 'value'], 'required'],
            [['describe'], 'string'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['range_from', 'range_to', 'value'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_indicator_fk' => Yii::t('app', 'Id Indicator Fk'),
            'name' => Yii::t('app', 'Name'),
            'describe' => Yii::t('app', 'Describe'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'range_from' => Yii::t('app', 'Range From'),
            'range_to' => Yii::t('app', 'Range To'),
            'value' => Yii::t('app', 'Value'),
            'is_system' => Yii::t('app', 'Is System'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
}
