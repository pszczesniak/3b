<?php

namespace app\Modules\Community\controllers;

use Yii;
use backend\Modules\Community\models\ComGroup;
use backend\Modules\Community\models\ComMember;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * GroupController implements actions for ComGroup models.
 */
class GroupController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new ComMeeting();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionCreateajax() {
       
		$model = new ComGroup();
        $model->employees_list = [];
        array_push($model->employees_list, Yii::$app->user->id);
                
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    $tmpItem = '<li id="chat-group-'.$model->id.'">'
                                .'<a href="'.Url::to(['/community/group/updateajax', 'id' => $model->id]).'" class="gridViewModal" data-target="#modal-grid-item"> <i class="fa fa-circle text--'.( ($model->type_fk == 1) ? 'green' : 'red' ).'"></i> '.$model->name.' </a>'
                            .'</li>';   
                    return array('success' => true,  'action' => 'createGroup', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Grupa została dodane', 'tmpItem' => $tmpItem  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionUpdateajax($id) {
       
		$model = $this->findModel($id);
        $model->employees_list = [];
        foreach($model->members as $key => $member)
            array_push($model->employees_list, $member->id_user_fk);
                
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    $tmpItem = '<a href="'.Url::to(['/community/group/updateajax', 'id' => $model->id]).'"  class="gridViewModal" data-target="#modal-grid-item"> <i class="fa fa-circle text--'.( ($model->type_fk == 1) ? 'green' : 'red' ).'"></i> '.$model->name.'</a>';   
                    return array('success' => true,  'action' => 'updateGroup', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Grupa została dodane', 'tmpItem' => $tmpItem  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
		if (($model = ComGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pracownik o wskazanym identyfikatorze nie został odnaleziony w systemie.');
        }
    }
    
    public function actionMemberdelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
        $model = ComMember::findOne($id);
        $model->status = -1;
        $model->save();
        return array('success' => true,  'action' => 'deleteMember', 'index' =>0, 'id' => $model->id, 'alert' => 'Uczestnik został usunięty'  );	
	}
    
    public function actionMemberadd($id) {
       
		$model = $this->findModel($id);
                
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $tmpItem = '';
            if( $model->validate() /*&& $model->save()*/) {
                foreach($model->employees_list as $key => $value) {
                    $member = new ComMember();
                    $member->id_group_fk = $model->id;
                    $member->type_fk = 1;
                    $member->id_user_fk = $value;
                    $member->id_fk = $value;
                    $member->status = 1;
                    $member->save();
                    
                    $tmpItem .= '<li id="group-member-'.$member->id.'">'
                                    .'<a href="'.Url::to(['/community/group/memberdelete', 'id' => $member->id]).'" class="deleteConfirm" data-label="Usuń uczestnika" title="Usuń uczestnika"> <i class="fa fa-circle text--'. (($member->id_user_fk == \Yii::$app->user->id) ? 'blue' : 'yellow') .'"></i> '.$member->employee['fullname'].' <i class="fa fa-times pull-right"></i> </a>'
                                .'</li>';   
                }
                
                return array('success' => true,  'action' => 'addMember', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Zadanie zostało dodane', 'tmpItem' => $tmpItem  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_memberAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_memberAjax', [ 'model' => $model]);	
        }
	}
}
