<?php

namespace app\Modules\Community\controllers;

use Yii;
use backend\Modules\Community\models\ComMeeting;
use backend\Modules\Community\models\ComMeetingMember;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use common\components\CustomHelpers;
use yii\helpers\Url;

/**
 * MeetingController implements the CRUD actions for ComMeeting model.
 */
class MeetingController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new ComMeeting();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];
        
        $post = $_GET;
        if(isset($_GET['ComMeeting'])) {
            $params = $_GET['ComMeeting'];
            \Yii::$app->session->set('search.meetings', ['params' => $params, 'post' => $post]);
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(mtitle) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_meeting_fk from {{%com_meeting_member}} where status >= 0 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "m.status = ".$params['status']);
            }
            if(isset($params['id_dict_meeting_type_fk']) && !empty($params['id_dict_meeting_type_fk']) ) {
				array_push($where, "m.id_dict_meeting_type_fk = ".$params['id_dict_meeting_type_fk']);
            }
        } 
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
		
		$sortColumn = 'm.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.title';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.type_fk', 'm.title as title', 'id_dict_meeting_type_fk', 'm.status as status', 'c.name as cname', 'c.id as cid', 'cb.name as cbname', 'dv.name as dvname', 'date_from', 'date_to',
                        " concat_ws(' ', u.lastname, u.firstname) as creator", 'm.created_at',
                       ' (select count(*) from {{%cal_note}} where id_case_fk = m.id and id_type_fk = 4) as notes'])
            ->from('{{%com_meeting}} as m')
            ->join(' JOIN', '{{%user}} as u', 'u.id = m.created_by')
            ->join(' LEFT JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->join(' LEFT JOIN', '{{%customer_branch}} as cb', 'cb.id = m.id_customer_branch_fk')
            ->join('LEFT JOIN', '{{%dictionary_value}} as dv', 'dv.id = m.id_dict_meeting_type_fk');
            //->where( ['m.status' => 1, 'm.type_fk' => 2] );
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		      		
		$rows = $query->all();
    
		$fields = []; $tmp = [];

		foreach($rows as $key=>$value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['name'] = $value['title'];
            $tmp['type'] = ($value['dvname']) ? $value['dvname'] : ' ';
            $tmp['meeting_with'] = ($value['cname']) ? $value['cname'] : ' ';
            if($value['cbname']) {
                $tmp['meeting_with'] .= '<br /><small class="text--grey">Oddział: '.$value['cbname'].'</small>';
            }
            $tmp['notes'] = (count($value['notes'])>0) ? '<i class="fa fa-comments text--blue cursor-pointer" title="Liczba komentarzy: '.( count($value['notes']) ).'"></i>' : '';
            $tmp['date_from'] = $value['date_from'];
            $tmp['date_to'] = $value['date_to'];
            $tmp['creator'] = $value['creator'];
            $tmp['className'] = ($value['status'] == -1) ? 'danger' : 'normal';
			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/community/meeting/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				//$tmp['actions'] .= ( ( ($value->created_by == \Yii::$app->user->id && $value->type_fk==2) || ($value->type_fk==1 && ($value->created_by == \Yii::$app->user->id || $this->module->params['isAdmin'] || $this->module->params['isSpecial']) ) ) && $value['status'] == 1) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item" data-label="Odwołaj" data-title="<i class=\'fa fa-ban\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
				//$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
				//$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single CompanyEmployee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        $editable = false;
        
        $model = $this->findModel($id); 
        $member = ComMeetingMember::find()->where(['id_meeting_fk' => $model->id, 'id_user_fk' => \Yii::$app->user->id])->one();
        
        if($model->created_by == Yii::$app->user->id || ( $model->type_fk == 1 && ($this->module->params['isSpecial'] || $this->module->params['isAdmin'])) ) {
            $editable = true;
        }
        
        /*if(!$member && $model->type_fk == 2) {
            throw new \yii\web\HttpException(405, 'Nie jesteś przypisany do tego spotkania.');
        }*/
                
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        if ($member && $member->status != 2 && Yii::$app->request->isPost) {
            $statusTmp = $member->status;
            $member->load(Yii::$app->request->post());
            $member->status = -1;
            $member->discard_at = date('Y-m-d H:i:s');
            if($member->save()) {
                try {
                    $creator = \common\models\User::findOne($model->created_by);
                    if(Yii::$app->params['env'] == 'prod') {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $member, 'alert' => 'discard', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($creator['email'])
                            ->setCc($member->employee['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Odrzucenie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name)
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $member, 'alert' => 'discard', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo(\Yii::$app->params['testEmail'])
                            ->setSubject('DEV: Odrzucenie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name . ' - '. $creator['email'].' cc: '.$member->employee['email'])
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'meeting-employee: '.$employee->email;
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zaproszenie zostało odrzucone')  );
            } else {
                $member->status = $statusTmp;
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Proszę wpisać powód odrzucenia!')  );
            }
        }
				
		return $this->render('view', [
            'model' => $model, 'member' => $member, 'editable' => $editable
        ]);
    }
    
    public function actionInvite($id) {
        //$id = CustomHelpers::decode($id);
        $model = $this->findModel($id);
        $model->meeting_employees = [];
        $model->load(Yii::$app->request->post());
        $counter = 0;
       // var_dump($model->meeting_employees);exit;
	    if(!$model->meeting_employees) $model->meeting_employees = [];
        foreach($model->meeting_employees as $key => $employee) {
            $exist = \backend\Modules\Community\models\ComMeetingMember::find()->where(['id_employee_fk' => $employee, 'id_meeting_fk' => $model->id])->andWhere('status != -2')->count();
            if($exist == 0) {
                $modelEmployee = \backend\Modules\Company\models\CompanyEmployee::findOne($employee);
                $newMeetingEmployee = new \backend\Modules\Community\models\ComMeetingMember();
                $newMeetingEmployee->id_meeting_fk = $model->id;
                $newMeetingEmployee->id_user_fk = $modelEmployee->id_user_fk;
                $newMeetingEmployee->id_employee_fk = $employee;
                $newMeetingEmployee->status = 0;
                if($newMeetingEmployee->save()) { ++$counter; } 
            }
        }
		if( count($model->meeting_employees) == 0 )
			Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Proszę wybrać pracowników')  );
		else
			Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Ilość wysłanych zaproszeń: '.$counter)  );
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
    public function actionRemove($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = \backend\Modules\Community\models\ComMeetingMember::findOne($id);
        $model->status = -2;
        $model->save();
        
         try {
            if(Yii::$app->params['env'] == 'prod') {
                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'cancel', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                    ->setTo( $model->employee['email'])
                    ->setBcc('kamila_bajdowska@onet.eu')
                    ->setSubject('Usunięcie z listy uczestników spotkania wygenerowane przez system ' . \Yii::$app->name)
                    ->send();  
            } else {
                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'cancel', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                    ->setTo( \Yii::$app->params['testEmail'] )
                    ->setSubject('DEV: Usunięcie z listy uczestników spotkania wygenerowane przez system ' . \Yii::$app->name .' - '.$model->employee['fullname'])
                    ->send();
            }
        } catch (\Swift_TransportException $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
            $request = Yii::$app->request;
            $log = new \common\models\Logs();
            $log->id_user_fk = Yii::$app->user->id;
            $log->action_date = date('Y-m-d H:i:s');
            $log->action_name = 'meeting-employee: '.$employee->email;
            $log->action_describe = $e->getMessage();
            $log->request_remote_addr = $request->getUserIP();
            $log->request_user_agent = $request->getUserAgent(); 
            $log->request_content_type = $request->getContentType(); 
            $log->request_url = $request->getUrl();
            $log->save();
        }
            
        return ['success' => true, 'action' => 'delete', 'alert' => 'Uczestnik został usunięty', 'id' => $model->id];
    }
    
    public function actionChange($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
		$model = $this->findModel($id); 
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        $model->meeting_contacts = []; $contactsTmp = [];
        if($model->contacts['ids']) {
            $model->meeting_contacts = explode(',', $model->contacts['ids']);
            $contactsTmp = $model->meeting_contacts;
        }

        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() && $model->save()) {	
               // $model->date_from = (!$model->fromDate || !$model->fromTime) ? false : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
               // $model->date_to = (!$model->toDate || !$model->toTime) ? false : ( $model->toDate . ' ' . $model->toTime . ':00' );
                if(!$model->meeting_contacts) $model->meeting_contacts = [];
				$sqlUpdate = "update {{%com_meeting_member}} set status = -1 "
                            ." where type_fk = 2 and id_employee_fk not in (".((count($model->meeting_contacts) == 0) ? '0' : implode(',', $model->meeting_contacts)).") and id_meeting_fk = ".$model->id;
                \Yii::$app->db->createCommand($sqlUpdate)->execute();
                foreach($model->meeting_contacts as $key => $contact) {
					$modelContact = \backend\Modules\Crm\models\CustomerPerson::findOne($contact);
                    if($modelContact && !in_array($contact, $contactsTmp)) {
						$newMeetingContact = new \backend\Modules\Community\models\ComMeetingMember();
                        $newMeetingContact->type_fk = 2;
						$newMeetingContact->id_meeting_fk = $model->id;
						$newMeetingContact->id_user_fk = $contact;
						$newMeetingContact->id_employee_fk = $contact;
						$newMeetingContact->status = 1;
						$newMeetingContact->save();
					}
				}
                //Yii::$app->response->format = Response::FORMAT_HTML;
                $changes = [
                    'customer' => $model->customer['name'],
                    'branch' => $model->branch['name'],
                    'project' => $model->project['name'],
                    'purpose' => ($model->purpose) ? $model->purpose : '<div class="alert alert-warning">brak opisu</div>',
                    'title' => $model->title
                ];  
                Yii::$app->response->format = Response::FORMAT_JSON;  
                return array('success' => true,  'refresh' => 'element', 'elements' => [0 => ['prefix' => 'tmpl', 'id' => 'info', 'value' => $this->renderPartial('_info', ['model' => $model, 'editable' => true])]], 'alert' => 'Informacje zostały zaktualizowane',  'changes' => $changes );
                //return Yii::$app->getResponse()->redirect(['view', 'id' => $model->id]);	
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false, 'html' => $this->renderAjax('_changeAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_changeAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionTerm($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
		$model = $this->findModel($id); 
        $dateFromTemp = $model->date_from; $dateToTemp = $model->date_to;
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        if (Yii::$app->request->isPost ) {
			
			$model->load(Yii::$app->request->post());
            $model->user_action = 'change-term';
            $model->date_from = ($model->fromDate && $model->fromTime) ? ( $model->fromDate . ' ' . $model->fromTime . ':00' ) : false;
		    $model->date_to = ($model->toDate && $model->toTime) ? ( $model->toDate . ' ' . $model->toTime . ':00' ) : false;
            $model->meeting_resource = $model->resources;
            
            $startDate = $model->date_from;
            $endDate = $model->date_to;
            foreach($model->resources as $key => $item) {
                $reservations = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['id_resource_fk' => $item->id_resource_fk])
                                        ->andWhere(" status >= 0" )
                                        ->andWhere(" (id_event_fk is null or id_event_fk != ". $model->id.")" )
                                        ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                             ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                                             ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                             ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                                ->all();
                if( count($reservations) > 0 ){
                    $temp = \backend\Modules\Company\models\CompanyResources::findOne($item->id_resource_fk);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return array('success' => false,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id,  'errors' => [ 0 => 'Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to]  );
                    //$this->addError('date_from','Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to);
                }   
            }
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if( ($model->date_from != $dateFromTemp || $model->date_to != $dateToTemp) && $model->validate() ) {
                    if(  $model->save() ) {	
                           // $model->date_from = (!$model->fromDate || !$model->fromTime) ? false : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                           // $model->date_to = (!$model->toDate || !$model->toTime) ? false : ( $model->toDate . ' ' . $model->toTime . ':00' );
                            foreach($model->resources as $key => $item) {
                                /*$toRemove = \backend\Modules\Community\models\ComMeetingResource::find()->where(['id_meeting_fk' => $model->id, 'id_resource_fk' => $value, 'status' => 1])->one();
                                $toRemove->status = 0;
                                $toRemove->save();*/
                                
                                $toCancel = \backend\Modules\Company\models\CompanyResourcesSchedule::findOne($item->id_resource_schedule_fk);
                                $toCancel->status = -1;
                                $toCancel->user_action = 'cancel';
                                $toCancel->comment = 'Zmiana terminu spotkania';
                                $toCancel->save();
                                
                                $newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                                $newSchedule->user_action = 'create';
                                $newSchedule->status = 0;
                                $newSchedule->id_resource_fk = $item->id_resource_fk;
                                $newSchedule->id_employee_fk = $this->module->params['employeeId'];
                                $newSchedule->date_from = $model->date_from;
                                $newSchedule->date_to = $model->date_to;
                                $newSchedule->describe = 'Konfiguracja spotkania: '.$model->title;
                                $newSchedule->type_event = $model->type_fk;
								$newSchedule->is_meeting = 1;
                                $newSchedule->id_event_fk = $model->id;
                                $newSchedule->save();
                                if(!$newSchedule->save()) {
                                    $transaction->rollBack();
                                    Yii::$app->response->format = Response::FORMAT_JSON;
                                    return array('success' => false, 'html' => $this->renderAjax('_changeTerm', [ 'model' => $model]), 'errors' => $newSchedule->getErrors() );
                                }
                                
                                $item->id_resource_schedule_fk = $newSchedule->id;
                                $item->save();
                        
                            }
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            $afterChange = \backend\Modules\Community\models\ComMeetingResource::find()->where(['id_meeting_fk' => $model->id, 'status' => 1])->all();   
                            if( count($afterChange) == 0 ) {
                                $resources = 'nie przypisano';
                            } else {
                                $resources = '<ul>';
                                foreach($afterChange as $key => $resource) {
                                    $color = 'blue'; $title = 'rezerwacja oczekująca';
                                    if($resource->schedule['status'] == 1) { $color = 'green'; $title = 'rezerwacja zaakceptowana'; }
                                    if($resource->schedule['status'] == -1) { $color = 'orange'; $title = 'rezerwacja anulowana'; }
                                    if($resource->schedule['status'] == -2) { $color = 'red'; $title = 'rezerwacja odrzucona'; }
                                    $resources .= '<li><i class="fa fa-circle text--'.$color.' cursor-pointer" title="'.$title.'"></i>'.$resource->resource['name'].'</li>';
                                }
                                $resources .= '</ul>';
                            }
                            $changes = [
                                'date_to' => ($model->date_to) ? $model->date_to : ' ',
                                'date_from' => $model->date_from,
                                'cresources' => $resources
                            ];  
                            $transaction->commit();
                            
                            $title = 'Powiadomienie o zmianie daty spotkania spotkania wygenerowane przez system ' . \Yii::$app->name;
                            //$title = 'Informacja o odwołaniu spotkania automatycznie wygenerowana przez system ' . \Yii::$app->name;
                            foreach($model->employees as $key => $employee) {
                                $title_ =  $title; //.' - '.$employee->employee['fullname'];
                                if($employee->status == 2 && \Yii::$app->user->id == $model->created_by) {
                                    $send = 0;
                                } else {
                                    try {
                                        if(Yii::$app->params['env'] == 'prod') {
                                            \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => $model->user_action, 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                                ->setTo($employee->employee['email'])
                                                ->setBcc('kamila_bajdowska@onet.eu')
                                                ->setSubject($title_)
                                                ->send();
                                        } else {
                                            \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => $model->user_action, 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                                ->setTo(\Yii::$app->params['testEmail'])
                                                ->setSubject('DEV: '.$title_)
                                                ->send();

                                        }
                                    } catch (\Swift_TransportException $e) {
                                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                                        $request = Yii::$app->request;
                                        $log = new \common\models\Logs();
                                        $log->id_user_fk = Yii::$app->user->id;
                                        $log->action_date = date('Y-m-d H:i:s');
                                        $log->action_name = 'meeting-employee: '.$employee->employee['email'];
                                        $log->action_describe = $e->getMessage();
                                        $log->request_remote_addr = $request->getUserIP();
                                        $log->request_user_agent = $request->getUserAgent(); 
                                        $log->request_content_type = $request->getContentType(); 
                                        $log->request_url = $request->getUrl();
                                        $log->save();
                                    }
                                }
                            }
                            return array('success' => true, 'refresh' => 'element', 'elements' => [0 => ['prefix' => 'tmpl', 'id' => 'info', 'value' => $this->renderPartial('_info', ['model' => $model, 'editable' => true])]], 'changes' => $changes, 'alert' => 'Termin spotkania został zmieniony'  );
                            //return Yii::$app->getResponse()->redirect(['view', 'id' => $model->id]);	
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return array('success' => false, 'html' => $this->renderAjax('_changeTerm', [ 'model' => $model]), 'errors' => $model->getErrors() );	
                    }
                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return array('success' => true, 'html' => $this->renderAjax('_changeTerm', [ 'model' => $model]), 'errors' => $model->getErrors(), 'alert' => 'Nie wprowadzono żadnych zmian' );
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false, 'html' => $this->renderAjax('_changeTerm', [ 'model' => $model]), 'errors' => $model->getErrors() );		
            }
		} else {
            return  $this->renderAjax('_changeTerm', [ 'model' => $model]);	
        }
	}
    
    public function actionResource($id) {
        //Yii::$app->response->format = Response::FORMAT_JSON;
		$model = $this->findModel($id); 
        $model->user_action = 'change-resources';
        $model->meeting_resource = []; $resourcesTemp = []; $counter = 0;
        foreach($model->resources as $key => $item) {
            array_push($model->meeting_resource, $item->id_resource_fk);
            array_push($resourcesTemp, $item->id_resource_fk);
        }
        
        if (Yii::$app->request->isPost ) {
			$model->load(Yii::$app->request->post());
            $startDate = $model->date_from;
            $endDate = $model->date_to;
            foreach($model->meeting_resource as $key => $value) {
                if( !in_array($value, $resourcesTemp) ) {
                    $reservations = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where(['id_resource_fk' => $value])
                                            ->andWhere(" status >= 0" )
                                            ->andWhere(" (id_event_fk is null or id_event_fk != ". $model->id.")" )
                                            ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                                 ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                                                 ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                                 ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                                    ->all();
                    if( count($reservations) > 0 ){
                        $temp = \backend\Modules\Company\models\CompanyResources::findOne($value);
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return array('success' => false,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id,  'errors' => [ 0 => 'Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to]  );
                        //$this->addError('date_from','Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to);
                    } 
                }
            }
            
            if($model->validate()) {
                foreach($resourcesTemp as $key => $value) {
                    if( !in_array( $value, $model->meeting_resource ) ) {
                        $toRemove = \backend\Modules\Community\models\ComMeetingResource::find()->where(['id_meeting_fk' => $model->id, 'id_resource_fk' => $value, 'status' => 1])->one();
                        $toRemove->status = 0;
                        $toRemove->save();
                        
                        $toCancel = \backend\Modules\Company\models\CompanyResourcesSchedule::findOne($toRemove->id_resource_schedule_fk);
                        $toCancel->status = -1;
                        $toCancel->user_action = 'cancel';
                        $toCancel->comment = 'Usunięcie zasobu ze spotkania';
                        $toCancel->save();
                        ++$counter;
                    }
                }
                
                foreach($model->meeting_resource as $key => $value) {
                    if( !in_array($value, $resourcesTemp) ) {
                        $newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                        $newSchedule->user_action = 'create';
                        $newSchedule->status = 0;
                        $newSchedule->id_resource_fk = $value;
                        $newSchedule->id_employee_fk = $this->module->params['employeeId'];
                        $newSchedule->date_from = $model->date_from;
                        $newSchedule->date_to = $model->date_to;
                        $newSchedule->describe = 'Konfiguracja spotkania: '.$model->title;
                        $newSchedule->type_event = $model->type_fk;
						$newSchedule->is_meeting == 1;
                        $newSchedule->id_event_fk = $model->id;
                        $newSchedule->save();
                        
                        $newMeetingResource = new \backend\Modules\Community\models\ComMeetingResource();
                        $newMeetingResource->id_meeting_fk = $model->id;
                        $newMeetingResource->id_resource_fk = $value;
                        $newMeetingResource->id_resource_schedule_fk = $newSchedule->id;
                        $newMeetingResource->save();   
                    }
                }
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                $afterChange = \backend\Modules\Community\models\ComMeetingResource::find()->where(['id_meeting_fk' => $model->id, 'status' => 1])->all();  
                if( count($afterChange) == 0 ) {
                    $resources = 'nie przypisano';
                } else {
                    $resources = '<ul>';
                    foreach($afterChange as $key => $resource) {
                        $color = 'blue'; $title = 'rezerwacja oczekująca';
                        if($resource->schedule['status'] == 1) { $color = 'green'; $title = 'rezerwacja zaakceptowana'; }
                        if($resource->schedule['status'] == -1) { $color = 'orange'; $title = 'rezerwacja anulowana'; }
                        if($resource->schedule['status'] == 2) { $color = 'red'; $title = 'rezerwacja odrzucona'; }
                        $resources .= '<li><i class="fa fa-circle text--'.$color.' cursor-pointer" title="'.$title.'"></i>'.$resource->resource['name'].'</li>';
                    }
                    $resources .= '</ul>';
                }
                $changes = [
                    'cresources' => $resources
                ];  
                return array('success' => true,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id, 'changes' => $changes, 'alert' => 'Rezerwacje zostały zaktualizawane'  );
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;  
                if( count($model->resources) == 0 ) {
                    $resources = 'nie przypisano';
                } else {
                    $resources = '<ul>';
                    foreach($model->resources as $key => $resource) {
                        $color = 'blue'; $title = 'rezerwacja oczekująca';
                        if($resource->schedule['status'] == 1) { $color = 'green'; $title = 'rezerwacja zaakceptowana'; }
                        if($resource->schedule['status'] == -1) { $color = 'orange'; $title = 'rezerwacja anulowana'; }
                        if($resource->schedule['status'] == 2) { $color = 'red'; $title = 'rezerwacja odrzucona'; }
                        $resources .= '<li><i class="fa fa-circle text--'.$color.' cursor-pointer" title="'.$title.'"></i>'.$resource->resource['name'].'</li>';
                    }
                    $resources .= '</ul>';
                }
                $changes = [
                    'resources' => $resources
                ];  
                $alert = 'Rezerwacje nie zostały zaktualizowane';
                foreach($model->getErrors() as $key => $error) {
                    $alert = $error[0];
                }
                return array('success' => false,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id, 'changes' => $changes, 'alert' => $alert, 'errors' => $model->getErrors()  );
            }
           
           
		} else {
            return  $this->renderAjax('_changeResources', [ 'model' => $model]);	
        }
	}
    
    public function actionCreate() {
		$model = new ComMeeting; 
        $model->meeting_employees = [];
        $model->meeting_resource = [];
		$model->status = 1;
        array_push($model->meeting_employees, Yii::$app->user->id);
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$model->meeting_resource) $model->meeting_resource = [];
            $model->id_resource_fk = count($model->meeting_resource);
            $model->date_from = ($model->fromDate && $model->fromTime) ? ( $model->fromDate . ' ' . $model->fromTime . ':00' ) : false;
		    $model->date_to = ($model->toDate && $model->toTime) ? ( $model->toDate . ' ' . $model->toTime . ':00' ) : false;
            if( $model->validate() && $model->save()) {
				foreach($model->meeting_resource as $key => $resource) {
				//if($model->id_resource_fk && $model->id_resource_fk != 0) {
					$newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                    $newSchedule->user_action = 'create';
					$newSchedule->status = 0;
                    $newSchedule->user_action = 'create';
					$newSchedule->id_resource_fk = $resource;
					$newSchedule->id_employee_fk = $this->module->params['employeeId'];
					$newSchedule->date_from = $model->date_from;
					$newSchedule->date_to = $model->date_to;
					$newSchedule->describe = 'Konfiguracja spotkania: '.$model->title;
					$newSchedule->type_event = $model->type_fk;
					$newSchedule->is_meeting = 1;
					
					if($model->id_customer_fk) {
						$newSchedule->applicant_name = ($model->customer['symbol']) ? $model->customer['symbol'] : $model->customer['name'];
					}
					if($newSchedule->applicant_name)
						$newSchedule->applicant_name .= ($model->applicant_name) ? (' - '.$model->applicant_name) : '';
					else
						$newSchedule->applicant_name = $model->applicant_name;
					
					$newSchedule->id_event_fk = $model->id;
					$newSchedule->save();
					
					$newMeetingResource = new \backend\Modules\Community\models\ComMeetingResource();
					$newMeetingResource->id_meeting_fk = $model->id;
					$newMeetingResource->id_resource_fk = $resource;
					$newMeetingResource->id_resource_schedule_fk = $newSchedule->id;
					$newMeetingResource->save();
				}
				if(!$model->meeting_employees) $model->meeting_employees = [];
				foreach($model->meeting_employees as $key => $employee) {
					$modelEmployee = \backend\Modules\Company\models\CompanyEmployee::findOne($employee);
                    if($modelEmployee) {
						$newMeetingEmployee = new \backend\Modules\Community\models\ComMeetingMember();
						$newMeetingEmployee->id_meeting_fk = $model->id;
						$newMeetingEmployee->id_user_fk = $modelEmployee->id_user_fk;
						$newMeetingEmployee->id_employee_fk = $employee;
						$newMeetingEmployee->status = ($modelEmployee->id_user_fk == \Yii::$app->user->id || $model->created_by == $modelEmployee->id_user_fk) ? 2 : 0;
						$newMeetingEmployee->save();
					}
				}
                
                if(!$model->meeting_contacts) $model->meeting_contacts = [];
				foreach($model->meeting_contacts as $key => $contact) {
					$modelContact = \backend\Modules\Crm\models\CustomerPerson::findOne($contact);
                    if($modelContact) {
						$newMeetingContact = new \backend\Modules\Community\models\ComMeetingMember();
                        $newMeetingContact->type_fk = 2;
						$newMeetingContact->id_meeting_fk = $model->id;
						$newMeetingContact->id_user_fk = $newMeetingContact->id_user_fk;
						$newMeetingContact->id_employee_fk = $contact;
						$newMeetingContact->status = 1;
						$newMeetingContact->save();
					}
				}
				   
				return array('success' => true,  'refresh' => 'yes', 'index' => 0, 'id' => $model->id, 'name' => $model->title, 'alert' => 'Spotkanie zostało zapisane', 'table' => '#table-data'  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionUpdate($id) {
		$model = ComMeeting::find()->where(['id' => $id])->one(); 
        $model->meeting_employees = [];
        $model->meeting_resource = [];
		$model->status = 1;
        array_push($model->meeting_employees, Yii::$app->user->id);
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$model->meeting_resource) $model->meeting_resource = [];
            $model->id_resource_fk = count($model->meeting_resource);
            $model->date_from = ($model->fromDate && $model->fromTime) ? ( $model->fromDate . ' ' . $model->fromTime . ':00' ) : false;
		    $model->date_to = ($model->toDate && $model->toTime) ? ( $model->toDate . ' ' . $model->toTime . ':00' ) : false;
            if( $model->validate() && $model->save()) {
				foreach($model->meeting_resource as $key => $resource) {
				//if($model->id_resource_fk && $model->id_resource_fk != 0) {
					$newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                    $newSchedule->user_action = 'create';
					$newSchedule->status = 0;
                    $newSchedule->user_action = 'create';
					$newSchedule->id_resource_fk = $resource;
					$newSchedule->id_employee_fk = $this->module->params['employeeId'];
					$newSchedule->date_from = $model->date_from;
					$newSchedule->date_to = $model->date_to;
					$newSchedule->describe = 'Konfiguracja spotkania: '.$model->title;
					$newSchedule->type_event = $model->type_fk;
					$newSchedule->is_meeting = 1;
					
					if($model->id_customer_fk) {
						$newSchedule->applicant_name = ($model->customer['symbol']) ? $model->customer['symbol'] : $model->customer['name'];
					}
					if($newSchedule->applicant_name)
						$newSchedule->applicant_name .= ($model->applicant_name) ? (' - '.$model->applicant_name) : '';
					else
						$newSchedule->applicant_name = $model->applicant_name;
					
					$newSchedule->id_event_fk = $model->id;
					$newSchedule->save();
					
					$newMeetingResource = new \backend\Modules\Community\models\ComMeetingResource();
					$newMeetingResource->id_meeting_fk = $model->id;
					$newMeetingResource->id_resource_fk = $resource;
					$newMeetingResource->id_resource_schedule_fk = $newSchedule->id;
					$newMeetingResource->save();
				}
				if(!$model->meeting_employees) $model->meeting_employees = [];
				foreach($model->meeting_employees as $key => $employee) {
					$modelEmployee = \backend\Modules\Company\models\CompanyEmployee::findOne($employee);
                    if($modelEmployee) {
						$newMeetingEmployee = new \backend\Modules\Community\models\ComMeetingMember();
						$newMeetingEmployee->id_meeting_fk = $model->id;
						$newMeetingEmployee->id_user_fk = $modelEmployee->id_user_fk;
						$newMeetingEmployee->id_employee_fk = $employee;
						$newMeetingEmployee->status = ($modelEmployee->id_user_fk == \Yii::$app->user->id || $model->created_by == $modelEmployee->id_user_fk) ? 2 : 0;
						$newMeetingEmployee->save();
					}
				}
                
                if(!$model->meeting_contacts) $model->meeting_contacts = [];
				foreach($model->meeting_contacts as $key => $contact) {
					$modelContact = \backend\Modules\Crm\models\CustomerPerson::findOne($contact);
                    if($modelContact) {
						$newMeetingContact = new \backend\Modules\Community\models\ComMeetingMember();
                        $newMeetingContact->type_fk = 2;
						$newMeetingContact->id_meeting_fk = $model->id;
						$newMeetingContact->id_user_fk = $newMeetingContact->id_user_fk;
						$newMeetingContact->id_employee_fk = $contact;
						$newMeetingContact->status = 1;
						$newMeetingContact->save();
					}
				}
				   
				return array('success' => true,  'refresh' => 'yes', 'index' => 0, 'id' => $model->id, 'name' => $model->title, 'alert' => 'Spotkanie zostało zapisane', 'table' => '#table-data'  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */    
    public function actionDelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = Yii::$app->user->id;
        $model->user_action = 'remove';
        if($model->save()) {
            $members = [];
            $sender = '';
			foreach($model->employees as $key => $employee) {
				if($this->module->params['employeeId'] != $employee->id_employee_fk) 
                    array_push($members, $employee->employee['email']);
                else
                    $sender = $employee->employee['email'];
					
			}
            \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => 'remove', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
						->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
						->setTo($members)
                        ->setCc($sender)
						->setBcc('kamila_bajdowska@onet.eu') 
						->setSubject('Informacja o odwołaniu spotkania automatycznie wygenerowana przez system ' . \Yii::$app->name)
						->send();
            
            foreach($model->resources as $key => $resource) {
                $schedule = \backend\Modules\Company\models\CompanyResourcesSchedule::findOne($resource->id_resource_schedule_fk);
                $schedule->status = -1;
                $schedule->comment = 'Odwołanie spotkania';
                $schedule->user_action = 'cancel';
                $schedule->save();
            }
            return array('success' => true, 'alert' => 'Spotkanie zostało odwołane', );	
        } else {
            var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Spotkanie nie zostało odwołane', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the ComMeeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ComMeeting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = ComMeeting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pracownik o wskazanym identyfikatorze nie został odnaleziony w systemie.');
        }
    }

    public function actionAccept($id) {
        
       /* if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        } */
        $id = CustomHelpers::decode($id);
        $member = ComMeetingMember::findOne($id);
        $member->status = 1;
        $member->accept_at = date('Y-m-d H:i:s');
        if($member->save()) {
            try {
                $creator = \common\models\User::findOne($member->created_by);
                if(Yii::$app->params['env'] == 'prod') {
                    
                    \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $member, 'alert' => 'accept', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($creator['email'])
                        ->setBcc('kamila_bajdowska@onet.eu')
                        ->setSubject('Przyjęcie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name)
                        ->send();
                } else {
                    \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $member, 'alert' => 'accept', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo('kamila_bajdowska@onet.eu')
                        ->setSubject('DEV: Przyjęcie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name . ' - '.$creator['email'])
                        ->send();
                }
            } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                $request = Yii::$app->request;
                $log = new \common\models\Logs();
                $log->id_user_fk = Yii::$app->user->id;
                $log->action_date = date('Y-m-d H:i:s');
                $log->action_name = 'meeting-employee: '.$member->id;
                $log->action_describe = $e->getMessage();
                $log->request_remote_addr = $request->getUserIP();
                $log->request_user_agent = $request->getUserAgent(); 
                $log->request_content_type = $request->getContentType(); 
                $log->request_url = $request->getUrl();
                $log->save();
            }
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zaproszenie zostało przyjęte')  );
        }
        Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Przyjąłeś zaproszenie na spotkanie')  );
        return $this->redirect(['view', 'id' => $member->id_meeting_fk]);
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
    }
    
    public function actionAccepted($id)  {
        $id = CustomHelpers::decode($id);
        $model = ComMeetingMember::findOne($id);
        
        if($model->status != 1 && $model->status != -2 && $model->meeting['status'] == 1 && $model->id_employee_fk == $this->module->params['employeeId']) {
            $model->status = 1;
            $model->accept_at = date('Y-m-d H:i:s');
            $model->user_action = 'accept';

            if($model->save()) {
                try {
                    $creator = \common\models\User::findOne($model->created_by);
                    if(Yii::$app->params['env'] == 'prod') {
                        
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'accept', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($creator['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Przyjęcie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name)
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'accept', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo(\Yii::$app->params['testEmail'])
                            ->setSubject('DEV: Przyjęcie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name . ' - '.$creator['email'])
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'meeting-employee: '.$model->id;
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zaproszenie zostało przyjęte')  );
            }
        }  else {
                if($model->status == -2) {
                    Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Zostałeś usunięty z listy uczestników spotakania')  );
                }
                if($model->meeting['status'] != 1) {
                    Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Spotkanie zostało odwołane')  );
                }
                if($model->id_employee_fk != $this->module->params['employeeId']) {
                     Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Nie masz uprawnień do akceptacji uczestnictwa w spotkaniu pracownika '.$model->employee['fullname'])  );
                }
            } 
        return $this->redirect(['view', 'id' => $model->id_meeting_fk]);
    }
    
    public function actionDiscarded($id)  {
        $id = CustomHelpers::decode($id);
        $model = ComMeetingMember::findOne($id);
        
        if($model->status != -1 && $model->status != -2 && $model->meeting['status'] == 1 && $model->id_employee_fk == $this->module->params['employeeId']) {
            $model->status = -1;
            $model->discard_at = date('Y-m-d H:i:s');
            $model->user_action = 'discard';
            $model->comment = 'odrzucenie bezpośrednio z e-maila';
            if($model->save()) {

                try {
                    $creator = \common\models\User::findOne($model->created_by);
                    if(Yii::$app->params['env'] == 'prod') {
                        
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'discard', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($creator['email'].'test')
                            ->setCc($model->employee['email'])
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Odrzucenie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name)
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $model, 'alert' => 'discard', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo(\Yii::$app->params['testEmail'])
                            ->setSubject('DEV: Odrzucenie zaproszenia na spotkanie wygenerowane przez system ' . \Yii::$app->name . ' - '.$creator['email'].' -cc: '.$model->employee['email'])
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'meeting-employee: '.$employee->email;
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
            } /*else {
                var_dump($model->getErrors()); exit;
            }*/
            Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rezerwacja została odrzucona')  );
        } else {
            if($model->status == -2) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Zostałeś usunięty z listy uczestników spotakania')  );
            }
            if($model->meeting['status'] != 1) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Spotkanie zostało odwołane')  );
            }
            if($model->id_employee_fk != $this->module->params['employeeId']) {
                 Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Nie masz uprawnień do odrzucenia uczestnictwa w spotkaniu pracownika '.$model->employee['fullname'])  );
            }
        }   
        return $this->redirect(['view', 'id' => $model->id_meeting_fk]);
    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $note = new \backend\Modules\Task\models\Note();
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost ) {
			
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = $id;
            $note->id_type_fk = 4;
            $note->id_case_fk = $model->id;
            $note->id_task_fk = 0;
            //$note->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                foreach($model->employees as $key => $employee) {
                    if($employee->id_employee_fk != $note->id_employee_from_fk) {
                        try {
                            //$creator = \common\models\User::findOne(Yii::$app->user->id);
                            
                            if(Yii::$app->params['env'] == 'prod') {
                                
                                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => 'note', 'note' => $note, 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                    ->setTo($employee->employee['email'])
                                    ->setBcc('kamila_bajdowska@onet.eu')
                                    ->setSubject('Dodano komentarz do spotkania - informacja wygenerowana przez system ' . \Yii::$app->name)
                                    ->send();
                            } else {
                                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => 'note', 'note' => $note, 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                    ->setTo(\Yii::$app->params['testEmail'])
                                    ->setSubject('DEV: Dodano komentarz do spotkania - informacja wygenerowana przez system ' . \Yii::$app->name . ' - '.$employee->employee['email'])
                                    ->send();
                            }
                        } catch (\Swift_TransportException $e) {
                            //echo 'Caught exception: ',  $e->getMessage(), "\n";
                            $request = Yii::$app->request;
                            $log = new \common\models\Logs();
                            $log->id_user_fk = Yii::$app->user->id;
                            $log->action_date = date('Y-m-d H:i:s');
                            $log->action_name = 'meeting-employee-note: ';
                            $log->action_describe = $e->getMessage();
                            $log->request_remote_addr = $request->getUserIP();
                            $log->request_user_agent = $request->getUserAgent(); 
                            $log->request_content_type = $request->getContentType(); 
                            $log->request_url = $request->getUrl();
                            $log->save();
                        }
                    }
                }
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else {
                return array('success' => false );
            }
        }
        return array('success' => true );
       // return  $this->renderAjax('_notes', [  'model' => $model, 'note' => $note]) ;	
    }
    
    public function actionResources() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resources = []; $occupied = [];
        $options = '';
     
        if( !empty($_GET['start']) && !empty($_GET['end']) ) {
            $startDate = date('Y-m-d H:i:s', $_GET['start']);
            if($_GET['end'])
                $endDate= date('Y-m-d H:i:s', $_GET['end']);
            else
                $endDate= date('Y-m-d H:i:s', $_GET['start']);
            /*$reservations = CompanyResourcesSchedule::find()->where(['status' => 1, 'id_resource_fk' => $this->id_resource_fk])
							->andWhere(" id != ". ( ($this->id) ? $this->id : 0) )
                            //->andWhere( " ( ( '".$startDate."' >= date_from and '".$startDate."' < date_to ) or ( '".$endDate."' between date_from and date_to ) or (  date_from between '".$startDate."' and date_to ) )")
                            //->andWhere( " ( ( '".$startDate."' between date_from and date_to ) or ( '".$endDate."' between date_from and date_to )  )")
							->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                         ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
										 ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                         ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")*/
            $sql = "select id_resource_fk from {{%company_resources_schedule}} where status >= 0 and "
                    . "( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                    ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
                    ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                    ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) ) "; 
            $data = Yii::$app->db->createCommand($sql)->queryAll();
            foreach($data as $key => $value) {
                array_push($occupied, $value['id_resource_fk']);
            }
            //var_dump($occupied);exit;
            $resourcesData = \backend\Modules\Company\models\CompanyResources::find()->where(['status' => 1])->all();
            foreach($resourcesData as $key => $item) {
                if( !in_array($item->id, $occupied) ) {
                    $resources[$item->type.' '.$item->branch][$item->id] = $item->name;
                }
            }
        }
        
        foreach($resources as $key => $value) {
            $options .= '<optgroup label="'.$key.'">';
            foreach($value as $i => $item) {
                $options .= '<option value="'.$i.'">'.$item.'</option>';
            }
        }
      
        return ['options' => $options, 'resources' => $resources, 'occupied' => $occupied];
    }

}
