<?php
namespace app\Modules\Community\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;
use yii\web\UploadedFile;

use backend\Modules\Community\models\ComMessage;
use backend\Modules\Community\models\ComMessageCustomer;
use backend\Modules\Community\models\ComMessageEmployee;

/**
 * Inbox controller
 */
class InboxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'rules' => [
                   
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /*public function beforeAction($event)  {
        if(!\Yii::$app->session->get('user.cid')) {
            return $this->redirect(['/site/login']);
        }
        return parent::beforeAction($event);
    }*/

    public function actionIndex($option, $label) {
        /*if(!\Yii::$app->session->get('user.cid')) {
            return $this->redirect(['/site/login']);
        } */
        //$sqlTask = "select t.name as tname, c.name as cname, event_date, event_time, t.type_fk from law_cal_task t join law_cal_case c on id_case_fk = c.id where c.id_customer_fk = 98 and date_format(date_from,'%Y-%m')='2017-06'";
        //$tasks = Yii::$app->db->createCommand($sqlTask)->queryAll();
        $cid = (\Yii::$app->session->get('user.cid')) ? \Yii::$app->session->get('user.cid') : 0;
        $messages = [0 => [], 1 => [], 2 => [], 3 => [], 4 => []];
        /*$dict = \backend\Modules\Community\models\ComMessage::getDict();
        $msgSql = "select m.message, m.created_at, case when m.user_env = 1 then concat_ws(' ', u.lastname,u.firstname) else concat_ws(' ', cu.lastname,cu.firstname) end as author, m.user_env, m.type_fk "
                ." from {{%com_message}} m left join {{%user}} u on u.id=m.created_by and m.user_env = 1 left join {{%c_user}} cu on cu.id=m.created_by and m.user_env = 2 "
                ." where id_customer_fk = ".$cid." order by m.id desc"; 
        $msgData = Yii::$app->db->createCommand($msgSql)->queryAll();	
        foreach($msgData as $key => $value) {
            $msgTmp = ['msg' => $value['message'], 'date' => $value['created_at'], 'author' => $value['author'], 'dev' => $value['user_env'], 'type' => $value['type_fk'], 'type_name' => $dict['names'][$value['type_fk']],
                       'icon' => $dict['icons'][$value['type_fk']], 'color' => $dict['colors'][$value['type_fk']]]; 
            array_push($messages[0], $msgTmp);
            array_push($messages[$value['type_fk']], $msgTmp);
        }*/
        return $this->render('index', ['messages' => $messages, 'option' => $option, 'label' => $label]);
    }
        
    public function actionData($option, $label) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $connection = Yii::$app->getDb();
        $post = $_GET;
        
        //$cid = (\Yii::$app->session->get('user.cid')) ? \Yii::$app->session->get('user.cid') : 0;
        $messages = [0 => [], 1 => [], 2 => [], 3 => [], 4 => []];
        $dict = \backend\Modules\Community\models\ComMessage::getDict();
        if($option == 'inbox') {
            $sqlCount = "select count(*) as allRows "
                        ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id join {{%user}} u on u.id=m.created_by "
                        ." where mc.is_bin = 0 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
            $msgSql = "select mc.id as mid, m.message, m.created_at, m.type_fk, is_starred, is_read, with_files, m.user_env,  "
                              ." case when user_env=1 then concat_ws(' ', u.lastname,u.firstname) else concat_ws(' ', cu.lastname,cu.firstname) end author, "
                              ." c.id as cid, ifnull(c.symbol, c.name) as cname "
                    ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id left join {{%user}} u on u.id=m.created_by and user_env = 1 left join {{%c_user}} cu on cu.id=m.created_by and user_env = 2 left join {{%customer}} c on c.id = m.id_customer_fk"
                    ." where mc.is_bin = 0 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
        } else if($option == 'star') {
            $sqlCount = "select count(*) as allRows "
                        ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id join {{%user}} u on u.id=m.created_by "
                        ." where mc.is_bin = 0 and is_starred = 1 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
            $msgSql = "select mc.id as mid, m.message, m.created_at, m.type_fk, is_starred, is_read, with_files, m.user_env,  "
                              ." case when user_env=1 then concat_ws(' ', u.lastname,u.firstname) else concat_ws(' ', cu.lastname,cu.firstname) end author, "
                              ." c.id as cid, ifnull(c.symbol, c.name) as cname "
                    ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id left join {{%user}} u on u.id=m.created_by and user_env = 1 left join {{%c_user}} cu on cu.id=m.created_by and user_env = 2 left join {{%customer}} c on c.id = m.id_customer_fk"
                    ." where mc.is_bin = 0 and is_starred = 1 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
        } else if($option == 'send') {
            $sqlCount = "select count(*) as allRows "
                        ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id "
                        ." where mc.is_bin = 0 and m.created_by = mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
            $msgSql = "select mc.id as mid, m.message, m.created_at, m.type_fk, is_starred, is_read, with_files, m.user_env,  "
                              ." (select group_concat(concat_ws(' ', u.lastname,u.firstname)) from {{%com_message_employee}} cme join {{%user}} u on cme.id_user_fk=u.id where cme.id_message_fk = m.id and cme.id_user_fk != m.created_by) author, "
                              ." c.id as cid, ifnull(c.symbol, c.name) as cname "
                    ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id left join {{%customer}} c on c.id = m.id_customer_fk"
                    ." where mc.is_bin = 0 and m.created_by = mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
        } else if($option == 'bin') {
            $sqlCount = "select count(*) as allRows "
                        ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id join {{%user}} u on u.id=m.created_by "
                        ." where is_bin = 1 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
            $msgSql = "select mc.id as mid, m.message, m.created_at, m.type_fk, is_starred, is_read, with_files, m.user_env,  "
                              ." case when user_env=1 then concat_ws(' ', u.lastname,u.firstname) else concat_ws(' ', cu.lastname,cu.firstname) end author, "
                              ." c.id as cid, ifnull(c.symbol, c.name) as cname "
                    ." from {{%com_message}} m join {{%com_message_employee}} mc on mc.id_message_fk=m.id left join {{%user}} u on u.id=m.created_by and user_env = 1 left join {{%c_user}} cu on cu.id=m.created_by and user_env = 2 left join {{%customer}} c on c.id = m.id_customer_fk"
                    ." where is_bin = 1 and m.created_by != mc.id_user_fk and mc.id_user_fk = ".Yii::$app->user->id;
        }
        //echo $msgSql;exit;
        if($label > 0) {
            $msgSql .= " and m.type_fk = ".$label;
        } 
        
        $commandCount = $connection->createCommand($sqlCount);
        $count = $commandCount->queryOne()['allRows'];

        $msgSql .= " order by m.id desc"." limit ".(isset($post['offset']) ? $post['offset'] : 0 ).", ".(isset($post['limit']) ? $post['limit'] : 10); 
        $rows = Yii::$app->db->createCommand($msgSql)->queryAll();
        
        $fields = [];
        //$count = count($rows);
        foreach($rows as $key=>$value) {
            //$value['message'] = '<a href="inbox-detail.html"><span class="label label-success">Elite</span> Lorem ipsum perspiciatis unde omnis iste natus error sit voluptatem</a>';
			$value['message'] = '<span class="label bg-'.$dict['colors'][$value['type_fk']].'"><i class="fa fa-'.$dict['icons'][$value['type_fk']].'"></i></span>&nbsp;'.$value['message'];
            $msgTmp = ['id' => $value['mid'], 'nid' => $value['mid'], 'msg' => $value['message'], 'date' => $value['created_at'], 'author' => $value['author'].(($value['cname']) ? '<br /><span class="text--purple">'.$value['cname'].'</span>' : ''), 'dev' => $value['user_env'], 'type' => $value['type_fk'], 'type_name' => $dict['names'][$value['type_fk']],
                       'icon' => $dict['icons'][$value['type_fk']], 'color' => $dict['colors'][$value['type_fk']], 
                       'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $value['mid']]).'" class="action starred" data-id="'.$value['mid'].'" data-table="#table-inbox" data-label="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').'" title="'.(($value['is_starred']) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="fa fa-star'.(($value['is_starred']) ? ' text--yellow' : '-o').'"></i></a>', 
                       'file' => (($value['with_files']) ? '<i class="fa fa-paperclip"></i>' : ''), 'action' => Url::to(['/community/inbox/show', 'mid' => $value['mid']]), 'className' => ((!$value['is_read']) ? 'bold' : '')]; 
			
			array_push($fields, $msgTmp); 
		}
		return ['total' => $count,'rows' => $fields];
    }
    
    public function actionCreate($id) {
        //$cid = (\Yii::$app->session->get('user.cid')) ? \Yii::$app->session->get('user.cid') : 0;
		$cases = []; $casesEmails = [];
		//$customer = \backend\Modules\Crm\models\Customer::findOne($cid);
        
        $refresh = 'no';
        if(isset($_GET['o']) && ($_GET['o'] == 'send' || $_GET['o'] == 'bin')) {
            $refresh = 'yes';
        }
        
        $dict = \backend\Modules\Community\models\ComMessage::getDict();
        
        $note = new \backend\Modules\Community\models\ComMessage();
		//$note->id_customer_fk = $cid;
		$note->user_env = 1;
        $note->type_fk = 1;
        $note->with_files = 0;
		//$note->title = 'Wiadomość od klienta '.$customer->name;
        
        $id = CustomHelpers::decode($id);
        if($id != 0) $note->id_message_fk = $id;
		
		$casesData = \backend\Modules\Task\models\CalCase::getListForClient();
		foreach($casesData as $key => $item) {
			$cases[$item->id] = $item->name;
			if($item->leadingEmployee)
				$casesEmails[$item->id] = ['data-eid' => $item->leadingEmployee['id_user_fk'], 'data-emails' => $item->leadingEmployee['fullname']];
			else	
				$casesEmails[$item->id] = ['data-eid' => 23, 'data-emails' => 'Joanna Pietras'];
		}
		/*if($customer->guardian) {
            $gid = $customer->guardian['id_user_fk']; $gname = $customer->guardian['fullname'];
        } else {
            $gid = 23; $gname = 'Joanna Pietras';
        }*/
        
		/*$emails = [
			1 => ['data-eid' => $gid, 'data-emails' => $gname], 2 => ['data-eid' => $gid, 'data-emails' => $gname], 3 => ['data-eid' => 31, 'data-emails' => 'Małgorzata Gajewska']
		];*/ $emails = [];
       
        $note->status = 0;
       // $note->e_emails = $emails[1]['data-eid'];
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $note->attachs = UploadedFile::getInstances($note, 'attachs');
            $note->load(Yii::$app->request->post());

            $note->status = 1;
            if($note->save()) {
               
               $msgTmp = ['id' => $note->mid, 'msg' => $note->message, 'date' => $note->created_at, 'author' => $note->author, 'dev' => $note->user_env, 'type' => $note->type_fk, 'type_name' => $dict['names'][$note->type_fk],
                       'icon' => $dict['icons'][$note->type_fk], 'color' => $dict['colors'][$note->type_fk], 
                       'star' => '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $note->mid]).'" class="action starred" data-id="'.$note->mid.'" data-table="#table-inbox" data-label="Oznacz" title="Oznacz wiadomość"><i class="fa fa-star-o"></i></a>', 
                       'file' => (($note->with_files) ? '<i class="fa fa-paperclip"></i>' : ''), 'action' => Url::to(['/community/inbox/show', 'mid' => $note->mid]), 'className' => '']; 
			
                
                return array('success' => true,  'action' => 'add_msg', 'type' => $note->type_fk, 'title' => 'Potwierdzenie!', 'alert' => 'Wiadomność została wysłana', 'refresh' => $refresh, 'parent' => (($note->id_message_fk) ? $note->id_message_fk : 0), 'row' => $msgTmp, 'table' => '#table-inbox' );
            } else {
                return array('success' => false, 'errors' => $note->getErrors(), 'html' => $this->renderPartial('create', ['model' => $note, 'emails' => $emails, 'cases' => $cases, 'casesEmails' => $casesEmails ]), );
            }
        }
        return $this->renderPartial('create', ['model' => $note, 'emails' => $emails, 'cases' => $cases, 'casesEmails' => $casesEmails]/*, false, true*/);
    }
    
    public function actionStarred($mid) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = ComMessageEmployee::findOne($mid);
        $model->is_starred = ($model->is_starred) ? 0 : 1;
        $model->save();
        
        $star = '<a href="'.Url::to(['/community/inbox/starred', 'mid' => $mid]).'" class="action starred" data-id="'.$mid.'" data-table="#table-inbox" data-label="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').'" title="'.(($model->is_starred) ? 'Odznacz' : 'Oznacz').' wiadomość"><i class="fa fa-star'.(($model->is_starred) ? ' text--yellow' : '-o').'"></i></a>';
        return ['index' => $_GET['index'], 'refresh' => 'inline', 'row' => ['star' => $star], 'table' => '#table-inbox', 'alert' => ''];
    }
    
    public function actionShow($mid) {
        $model = \backend\Modules\Community\models\ComMessageEmployee::findOne($mid);
        $model->is_read = 1; $model->save();
        $message = $model->message;
        
        
        $reply = new ComMessage();
        return $this->renderPartial('view', ['message' => $message, 'reply' => $reply]);
    }
    
    public function actionSdelete() {        
		$model = new \frontend\Modules\Community\models\GroupAction();
		$model->user_action = "groupTrash";
        $model->env = 'employee';
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać wiadomości do usunięcia!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->delMessages()) {                      
                return array('success' => true, 'index' =>1, 'title' => 'Potwierdzenie', 'alert' => 'Wiadomości zostały przeniesione do kosza', 'table' => '#table-inbox', 'refresh' => 'yes' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMessages', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMessages', ['model' => $model]) ;	
		}
	}
    
    public function actionSstarred($flag) {        
		$model = new \frontend\Modules\Community\models\GroupAction();
		$model->user_action = "groupTrash";
        $model->env = 'employee';
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać wiadomości do usunięcia!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->starredMessages($flag)) {                      
                return array('success' => true, 'index' =>1, 'title' => 'Potwierdzenie', 'alert' => 'Wiadomości zostały oznaczone', 'table' => '#table-inbox', 'refresh' => 'yes' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMessages', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMessages', ['model' => $model]) ;	
		}
	}
}
