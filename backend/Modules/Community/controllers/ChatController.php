<?php

namespace app\Modules\Community\controllers;

use Yii;
use backend\Modules\Community\models\ComMeeting;
use backend\Modules\Community\models\ComTopic;
use backend\Modules\Community\models\ComGroup;
use backend\Modules\Community\models\ComPost;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;
use yii\filters\AccessControl;

/**
 * ChatController implements actions for Community models.
 */
class ChatController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $insertRead = "insert into {{%com_post_member}}(type_fk, id_fk, id_member_fk, id_user_fk, id_post_fk, read_at, status) "
                          ."select t.type_fk, 0 id_fk, m.id as id_member_fk, ".\Yii::$app->user->id." as id_user_fk, p.id, NOW() as read_at, 1 as status "
                          ." from {{%com_post}} p join {{%com_topic}} t on t.id=p.id_topic_fk join {{%com_group}} g on g.id=t.id_group_fk join {{%com_member}} m on g.id=m.id_group_fk  and m.id_user_fk = ".\Yii::$app->user->id
                          ." where (".\Yii::$app->user->id.",p.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})"
                          ." union "
                          ."select t.type_fk, 0 id_fk, 0 as id_member_fk, ".\Yii::$app->user->id." as id_user_fk, p.id, NOW() as read_at, 1 as status "
                          ."from {{%com_post}} p join {{%com_topic}} t on t.id=p.id_topic_fk join {{%com_group}} g on g.id=t.id_group_fk "
                          ."where g.type_fk=1 and (".\Yii::$app->user->id.",p.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})";
        \Yii::$app->db->createCommand($insertRead)->execute();                  
        $this->layout = "//main_full";
        /*$topics = ComTopic::find()->where('status >= 1')
                                  ->andWhere('(type_fk=1 or id in (select id_group_fk from {{%com_member}} where id_fk = '.\Yii::$app->user->id.'))')
                                  ->join('{{%com_group}}', )
                                  ->orderby('updated_at desc')->all();*/
        $connection = Yii::$app->getDb(); 
        $command = $connection->createCommand('SELECT t.id as tid, t.name as tname, g.type_fk as gtype '
                                               .' FROM {{%com_topic}} t join {{%com_group}} g on g.id=t.id_group_fk '
                                               .' WHERE t.status >= 1 and (g.type_fk=1 or g.id in (select id_group_fk from {{%com_member}} where id_fk = '.\Yii::$app->user->id.')) order by t.created_at desc');

        $topics = $command->queryAll();
        $groups = ComGroup::find()->where(['status' => 1])->andWhere('(type_fk=1 or id in (select id_group_fk from {{%com_member}} where id_fk = '.\Yii::$app->user->id.'))')->orderby('name')->all();
        //$posts = ComPost::find()->orderby('created_at desc')->limit(7)->all();
        $command = $connection->createCommand('SELECT p.*, t.id as tid, t.name as tname, g.type_fk as gtype, concat_ws(" ", u.lastname, u.firstname) as creator '
                                               .' FROM {{%com_post}} p join {{%com_topic}} t on t.id=p.id_topic_fk join {{%com_group}} g on g.id=t.id_group_fk join {{%user}} u on u.id=p.created_by '
                                               .' WHERE t.status >= 1 and (g.type_fk=1 or g.id in (select id_group_fk from {{%com_member}} where id_fk = '.\Yii::$app->user->id.')) order by t.created_at desc limit 7');
        $posts = $command->queryAll();
   
        return $this->render('index', [
            'topics' => $topics, 'groups' => $groups, 'posts' => $posts,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionRoom($id)
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $this->layout = "//main_full";
        $model = $this->findModel($id);
        
        if($model->group['type_fk'] == 2) {
            $isMember = false;
            foreach($model->group['members'] as $key => $value) {
                if($value->id_fk == \Yii::$app->user->id) $isMember = true;
            }
            
            if(!$isMember) {
                throw new \yii\web\HttpException(405, 'Rozmowa administrowana. Nie jesteś przypisany do grupy.');
            } 
            
            $insertRead = "insert into {{%com_post_member}}(type_fk, id_fk, id_member_fk, id_user_fk, id_post_fk, read_at, status) "
                          ."select t.type_fk, 0 id_fk, m.id as id_member_fk, ".\Yii::$app->user->id." as id_user_fk, p.id, NOW() as read_at, 1 as status "
                          ." from {{%com_post}} p join {{%com_topic}} t on t.id=p.id_topic_fk join {{%com_group}} g on g.id=t.id_group_fk join {{%com_member}} m on g.id=m.id_group_fk and m.id_user_fk = ".\Yii::$app->user->id
                          ." where p.id_topic_fk = ".$model->id." and (".\Yii::$app->user->id.",p.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})"; 
            \Yii::$app->db->createCommand($insertRead)->execute();
        } else {
            $insertRead = "insert into {{%com_post_member}}(type_fk, id_fk, id_member_fk, id_user_fk, id_post_fk, read_at, status) "
                          ."select t.type_fk, 0 id_fk, 0 as id_member_fk, ".\Yii::$app->user->id." as id_user_fk, p.id, NOW() as read_at, 1 as status "
                          ."from {{%com_post}} p join {{%com_topic}} t on t.id=p.id_topic_fk "
                          ."where p.id_topic_fk = ".$model->id." and (".\Yii::$app->user->id.",p.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})"; 
            \Yii::$app->db->createCommand($insertRead)->execute();
        }
        
        //$topics = ComTopic::find()->where('status >= 1')->orderby('updated_at desc')->limit(10)->all();
        $connection = Yii::$app->getDb(); 
        $command = $connection->createCommand('SELECT t.id as tid, t.name as tname, g.type_fk as gtype '
                                               .' FROM {{%com_topic}} t join {{%com_group}} g on g.id=t.id_group_fk '
                                               .' WHERE t.status >= 1 and (g.type_fk=1 or g.id in (select id_group_fk from {{%com_member}} where id_fk = '.\Yii::$app->user->id.')) order by t.created_at desc');

        $topics = $command->queryAll();
   
        return $this->render('room', [
            'model' => $model, 'topics' => $topics, 
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionTrefresh($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
               
        $html = '';
        foreach($model->posts as $key => $post) {
            $html .= '<div class="group-rom">'
                        .'<div class="first-part text--purple'.( (\Yii::$app->user->id==$post->id_user_fk) ? ' odd' : '').'">'. ( (\Yii::$app->user->id==$post->id_user_fk) ? 'TY' : $post->creator).'</div>'
                        .'<div class="second-part">'.$post->note.'</div>'
                        .'<div class="third-part" title="'.$post->created_at.'">'.Yii::$app->runAction('common/ago', ['date' => $post->created_at]).'</div>'
                    .'</div>';
        }
        
        return ['html' => $html];
    }
    
    public function actionCreateajax() {
       
		$model = new ComTopic();
                
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    $tmpItem = '<li>'
                                    .'<a href="'.Url::to(['/community/chat/room', 'id' => $model->id]).'"> <i class="fa fa-lock"></i> <span>'.$model->name.'</span>  </a>'
                                .'</li>'; 
                    $post = $model->lastPost; $tmpPost = false;
                    if($post)
                        $tmpPost = '<div class="room-box">'
                                            .'<h5 class="text-primary"><a href="'.Url::to(['/community/chat/room', 'id' => $post->id_topic_fk]).'">'.$post->topic['name'].'</a></h5>'
                                            .'<p>'.$post->note.'</p>'
                                            .'<p><span class="text-muted">Autor :</span> '.$post->creator.' | <span class="text-muted">Aktywność :</span> '.Yii::$app->runAction('common/ago', ['date' => date('Y-m-d H:i:s', (time()-1) )]).'</p>'
                                        .'</div>'; 
                    return array('success' => true,  'action' => 'createTopic', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Zadanie zostało dodane', 'tmpItem' => $tmpItem, 'tmpPost' => $tmpPost );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionAddpost($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = new ComPost();
        $model->id_topic_fk = $id;
        $model->type_fk = 1;
        $model->id_user_fk = \Yii::$app->user->id;
        $model->id_fk = $this->module->params['employeeId'];
        $model->load(Yii::$app->request->post());
        if( $model->save() ) {
            $html = '<div class="group-rom bg-green2">'
                        .'<div class="first-part odd text--purple">TY</div>'
                        .'<div class="second-part">'.$model->note.'</div>'
                        .'<div class="third-part">teraz</div>'
                    .'</div>';
            return [ 'success' => true, 'html' => $html ];
        } else {
            return [ 'success' => false ];
        }
    }
    
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = ComTopic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
