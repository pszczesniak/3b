<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\chat\Comments;

use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Community\models\ComMeeting */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spotkania'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'community_meeting-view', 'title'=>Html::encode($this->title))) ?>
    <?php if($model->status == -1) { ?>
		<div class="alert alert-danger">Spotkanie zostało odwołane</div>
	<?php } ?>
	<div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading bg-grey"> <strong>Szczegóły</strong>
                    <?php if($editable && $model->status != -1) { ?> <a href="<?= Url::to(['/community/meeting/change', 'id' => $model->id]) ?>" class="btn btn-info btn-sm btn-flat pull-right viewModal" data-title="<i class=\'fa fa-edit\'></i>Edycja" data-target="#modal-grid-item"  style="margin-top:-5px"><span class="fa fa-edit"></span> Edycja</a> <?php } ?>
                </div>
                <div class="panel-body project-body" id="tmpl-info">
                    <?= $this->render('_info', ['model' => $model, 'editable' => $editable]) ?>
                </div><!--/project-body-->
            </section>
            
            
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <?= Alert::widget() ?>
            <div class="task-body-content">
                <div class="task-content-top">
                    <?php if($member && $member->status < 2 && $model->status == 1) { ?>
                        <div class="task-meta clearfix">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default" style="width: 100%">
                                        <div class="panel-heading bg-white"> 
                                            <span class="panel-title text--grey"> <span class="fa fa-cog text--yellow"></span> Twoje uczestnictwo </span> 
                                            <div class="panel-heading-menu pull-right">
                                                <a class="collapse-link collapse-window" data-toggle="collapse" href="#meetingConfig" aria-expanded="true" aria-controls="meetingConfig">
                                                    <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="panel-body in" id="meetingConfig">
                                            <?php /* Alert::widget() */ ?>
                                            <div id="message">
                                                <ul class="message-container">
                                                    <li class="received">
                                                        <div class="details"> 
                                                            <div class="left"><?= $model->creator ?> <div class="arrow brand"></div> zaprasza na spotkanie </div>
                                                            <div class="right">
                                                                <?php 
                                                                    if($member->status == 1) echo '<span class="label label-success">zaakceptowany</span>';
                                                                    if($member->status == 0) echo '<span class="label label-warning">oczekuje</span>';
                                                                    if($member->status == -1) echo '<span class="label label-danger">odrzucony</span>';
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <form id="comment-form" method="POST"> 
                                                            <div class="form-group field-commeetingmember-comment ">
                                                                <textarea rows="3" name="ComMeetingMember[comment]" placeholder="Powód odrzucenia" class="form-control"></textarea>
                                                                <div class="help-block"></div>
                                                            </div>
                                                
                                                            <div class="tool-box">
                                                                <?php if($model->status != 2) { ?>
                                                                    <a class="circle-icon small green-hover fa fa-check" href="<?= Url::to(['meeting/accept', 'id' => $member->id]) ?>" alt="Zaakceptuj" title="Zaakceptuj"></a>
                                                                <?php } ?>
                                                                <button class="circle-icon small red-hover glyphicon glyphicon-remove" type="submit"  alt="Odrzuć" title="Odrzuć"></a>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    <?php } else if($editable && $model->status != -1 ) { ?>
                        <div class="task-meta clearfix">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default" style="width: 100%">
                                        <div class="panel-heading bg-white"> 
                                            <span class="panel-title text--grey"> <span class="fa fa-user-plus text--green"></span> Zaproszenie na spotkanie </span> 
                                            <div class="panel-heading-menu pull-right">
                                                <a class="collapse-link collapse-window" data-toggle="collapse" href="#meetingConfig" aria-expanded="true" aria-controls="meetingConfig">
                                                    <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="panel-body in" id="meetingConfig">
                                            <?php /* Alert::widget() */ ?>
                                            <div id="message">
                                                <ul class="message-container">
                                                    <li class="received">
                                                        
                                                        <?php $form = ActiveForm::begin([
                                                                'action' => Url::to(['/community/meeting/invite', 'id' => $model->id])
                                                            ]); ?>
                                                            <?= $form->field($model, 'meeting_employees')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->orderby('lastname')->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true, 'data-placeholder' => 'wybierz pracowników'] ) ?> 
                                                            <div class="align-right"> 
                                                                <?= Html::submitButton('Zaproś', ['class' => 'btn btn-sm bg-teal']) ?>
                                                            </div>

                                                        <?php ActiveForm::end(); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    <?php } ?>               
                </div>
            </div>
           
            <div class="panel with-nav-tabs panel-default">
                <?php $notes = $model->notes; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-users"></i><span class="panel-tabs--text">Uczestnicy</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>

				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <!--<fieldset><legend>Objaśnienia</legend> 
                                <table class="calendar-legend">
                                    <tbody>
                                        <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>
                                        <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nie ustawiono czasu</td></tr>
                                    </tbody>
                                </table>
                            </fieldset>-->
                            <div class="list-group contact-group">
                                <?php 
                                    foreach($model->employees as $key => $employee) {
                                        $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$employee->id_employee_fk."/preview.jpg"))?"/uploads/employees/cover/".$employee->id_employee_fk."/preview.jpg":"/images/default-user.png"; 
                                        if($employee->status == 2) $status = '<span class="label label-info">organizator</span>';
                                        if($employee->status == 1) $status = '<span class="label label-success">zaakceptowany</span>';
                                        if($employee->status == 0) $status = '<span class="label label-warning">oczekuje</span>';
                                        if($employee->status == -1) $status = '<span class="label label-danger">odrzucony</span><div class="text--red small">'.$employee->discard_at.': '.$employee->comment.'</div>';
                                        echo '<div class="list-group-item" id="db-event-'.$employee->id.'">'
                                                .'<div class="media">'
                                                    .'<div class="media-avatar pull-left">'
                                                        .'<img class="img-circle" src="'.$avatar.'" alt="Avatar">'
                                                    .'</div>'
                                                    .'<div class="media-body">'
                                                        .'<h4 class="media-heading">'
                                                            .$employee->employee['fullname']
                                                            . ( ($employee->status != 2 && $editable) ? ' <a href="'.Url::to(['/community/meeting/remove', 'id' => $employee->id]).'" title="Usuń uczestnika" class="btn btn-xs btn-danger deleteConfirm"><i class="fa fa-times"></i></a>' : '' )
                                                            .' <small>'.$employee->employee['typename'].'</small>'
                                                        .'</h4>'
                                                        .'<div class="media-content">'
                                                            //.'<i class="fa fa-map-marker"></i>'. ( $employee->employee['address'] ? $employee->employee['address'] : 'brak danych' ) 
                                                            .$status
                                                            .'<ul class="list-unstyled">'
                                                                .'<li><i class="fa fa-mobile"></i> '.( ($employee->employee['phone']) ? $employee->employee['phone'] : 'brak danych') .'</li>'
                                                                .'<li><i class="fa fa-envelope-o"></i>'.( ($employee->employee['email']) ?  Html::mailto($employee->employee['email'], $employee->employee['email']) : 'brak danych') .'</li>'
                                                            .'</ul>'
                                                        .'</div>'
                                                    .'</div>'
                                                .'</div>'
                                            .'</div>';
                                    }
                                ?>

                            </div>
						</div>  
                        <div class="tab-pane" id="tab2">
                            <?php /* $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes]) */ ?>
                            <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/community/meeting/note', 'id' => $model->id])]) ?>
						</div>                 
				    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>
