<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<div class="row project-details">
    <div class="row-details">
        <p><span class="bold">Tytuł</span>:  <span id="meeting-title"><?= $model->title ?></span></p>
    </div>
    <div class="row-details">
        <p><span class="bold">Typ</span>:  <span id="meeting-type"><?= ($model->dict) ? $model->dict['name'] : '' ?></span></p>
    </div>
    <div class="row-details">
        <p><span class="bold">Rozpoczęcie</span>: 
            <?php if($editable && $model->status != -1) { ?><a href="<?= Url::to(['/community/meeting/term', 'id' => $model->id]) ?>" data-target="#modal-grid-item" data-form="item-form" title="Zmień termin" class="btn btn-xs bg-purple viewModal"><i class="fa fa-pencil-alt"></i></a><?php } ?> 
            <span id="meeting-date_from"><?= $model->date_from ?></span></p>
        <p><span class="bold">Zakończenie</span>:  <span id="meeting-date_to"><?= ($model->date_to) ? $model->date_to : 'nie ustawiono' ?></span></p>
    </div>
     <div class="row-details">
        <p><span class="bold">Klient</span>:  <span id="meeting-customer"><?= ($model->customer) ? $model->customer['name'] : 'nie ustawiono' ?></span></p>
        <p><span class="bold">Oddział</span>:  <span id="meeting-branch"><?= ($model->branch) ? $model->branch['name']: 'nie ustawiono' ?></span></p>
        <p><span class="bold">Przedstawiciele</span>:  <span id="meeting-contacts"><?= ($model->persons['contacts']) ? $model->persons['contacts'] : 'nie ustawiono' ?></span></p>
        <p><span class="bold">Projekt</span>:  <span id="meeting-project"><?= ($model->project) ? $model->project['name']: 'nie ustawiono' ?></span></p>
    </div>  
    <div class="row-details">
       <p><span class="bold"> Cel </span>: <span id="meeting-purpose"><?= ( $model->purpose ) ? $model->purpose : '<div class="alert alert-warning">brak opisu</div>' ?> </span></p> 
    </div>
    <div class="row-details">
        <p><span class="bold">Organizator</span>:  <?= $model->creator ?></p>
    </div>    
    <div class="row-details">
        <p><span class="bold">Zarejestrowano</span>:  <?= $model->created_at ?></p>
    </div>   
</div>