<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Spotkania');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'com-meeting-index', 'title'=>Html::encode($this->title))) ?>

    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-offers" data-form="#filter-sale-offers-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
 
	<div id="toolbar-meetings" class="btn-group toolbar-table-widget">
		
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/community/meeting/create']) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					])  ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-meetings"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-meetings"
                data-toolbar="#toolbar-meetings" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-search-form="#filter-com-meetings-search"

                data-url=<?= Url::to(['/community/meeting/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="creator"  data-sortable="true" >Organizator</th>
                    <th data-field="type"  data-sortable="true" >Typ</th>
                    <th data-field="name"  data-sortable="true" >Tytuł</th>
                    <th data-field="date_from"  data-sortable="true" >Początek</th>
                    <th data-field="date_to"  data-sortable="true" >Koniec</th>
                    <th data-field="meeting_with"  data-sortable="true" >Klient</th>
                    <!--<th data-field="type"  data-sortable="false" data-align="center" data-width="20px" ></th>-->
                    <th data-field="notes"  data-sortable="false" data-align="center" data-width="20px" ></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="130px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Spotkania anulowane</td></tr>
                    <!--<tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nie ustawiono czasu</td></tr>-->
                </tbody>
            </table>
        </fieldset>
    </div>


</div>
<?php $this->endContent(); ?>


