<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-com-meetings-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-meetings']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-2"><?= $form->field($model, 'id_dict_meeting_type_fk')->dropDownList( \backend\Modules\Community\models\ComMeeting::listTypes(), ['prompt' => ' - wszystkie -', 'class' => 'form-control  widget-search-list select2', 'data-table' => '#table-meetings', 'data-form' => '#filter-com-meetings-search' ] ) ?></div>
        <div class="col-sm-3"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-meetings', 'data-form' => '#filter-com-meetings-search' ] ) ?></div>
        <div class="col-sm-2"><?= $form->field($model, 'status')->dropDownList(  ['1' => 'Aktywne', '-1' => 'Anulowane'], ['prompt' => ' - wszystkie -', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-meetings', 'data-form' => '#filter-com-meetings-search' ] ) ?></div>
        <div class="col-sm-5"><?= $form->field($model, 'title')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-meetings', 'data-form' => '#filter-com-meetings-search' ]) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
