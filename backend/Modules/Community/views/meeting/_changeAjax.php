<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <div class="grid">
                <div class="col-sm-4 col-xs-12">
                    <?php /*$form->field($model, 'type_fk')->dropDownList(  \backend\Modules\Community\models\ComMeeting::listTypes(), [] )*/ ?>
                    <?= $form->field($model, 'id_dict_meeting_type_fk', ['template' => '
                                              {label}
                                               <div class="input-group ">
                                                    {input}
                                                    <span class="input-group-addon bg-green">'.
                                                        Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/22' , 
                                                            ['class' => 'insertInline text--white', 
                                                             'data-target' => "#meeting-type-insert", 
                                                             'data-input' => ".meeting-type",
                                                             'id' => 'insertType'
                                                            ])
                                                    .'</span>
                                               </div>
                                               {error}{hint}
                                           '])->dropDownList(\backend\Modules\Community\models\ComMeeting::listTypes(), ['prompt' => ' -- wybierz --', 'class' => 'form-control meeting-type'] ) ?>
                                    <div id="meeting-type-insert" class="insert-inline bg-purple2 none"> </div>
                </div>
                <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
            </div>

			<fieldset><legend>Spotktanie z</legend>
                <div class="grid">
                    <div class="col-sm-4 cols-xs-12">
                        <?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList($model->id_customer_fk), [ 'prompt' => '-- wybierz klienta --', 'id' => 'selectAutoCompleteModal', 'data-url' => Url::to('/crm/default/autocomplete'), ] ) ?>             
                    </div>
                    <div class="col-sm-4 cols-xs-12">
                        <?= $form->field($model, 'id_customer_branch_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Crm\models\CustomerBranch::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] ) ?> 
                    </div>
                    <div class="col-sm-4 cols-xs-12">
                        <?= $form->field($model, 'id_resource_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] ) ?> 
                    </div>
                </div>
                <?= $form->field($model, 'meeting_contacts')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CustomerPerson::getList($model->id_customer_fk), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true] ) ?> 
            </fieldset>
			   
            <fieldset><legend>Cel spotkania</legend><?= $form->field($model, 'purpose')->textarea(['rows' => 2, 'placeholder' => 'Cel spotkania...'])->label(false) ?></fieldset>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    document.getElementById('selectAutoCompleteModal').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+(this.value ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                
                document.getElementById('commeeting-id_customer_branch_fk').innerHTML = result.listBranches;  
                document.getElementById('commeeting-id_resource_fk').innerHTML = result.listCase;       
                document.getElementById('commeeting-meeting_contacts').innerHTML = result.listContacts; 
                $('#commeeting-meeting_contacts').trigger("chosen:updated");         
            }
        }
        xhr.send();
        return false;
    }
</script>
