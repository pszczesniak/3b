<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

    
<div class="grid grid--0">
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="commeeting-task_fromDate">Start</label>
            <input type='text' class="form-control" id="task_fromDate" name="ComMeeting[fromDate]" value="<?= $model->fromDate ?>"/> 
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="commeeting-fromTime">&nbsp;</label>
            <div class='input-group date' id='task_fromTime' >
                <input type='text' class="form-control" name="ComMeeting[fromTime]" value="<?= $model->fromTime ?>" <?= ($model->all_day == 1) ? "disabled" : '' ?> />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę startu">
                    <span class="fa fa-clock text--white"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-7">
        <div class="form-group">
            <label class="control-label" for="commeeting-toDate">Koniec</label>
            <input type='text' class="form-control" id="task_toDate" name="ComMeeting[toDate]" value="<?= $model->toDate ?>"/>
        </div>
    </div>
    <div class="col-md-3 col-xs-5">
        <div class="form-group">
            <label class="control-label" for="commeeting-toTime">&nbsp;</label>
            <div class='input-group date' id='task_toTime' >
                <input type='text' class="form-control" name="ComMeeting[toTime]" value="<?= $model->toTime ?>" <?= ($model->all_day == 1) ? "disabled" : '' ?> />
                <span class="input-group-addon bg-grey" title="Ustaw godzinę końca">
                    <span class="fa fa-clock text--white"></span>
                </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#task_fromDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#task_fromDate').on("dp.change", function (e) {
        minDate = e.date.format('YYYY-MM-DD');
        $('#task_toDate').val(minDate);
        if( $('#task_toDate').val() && $('#task_fromTime > input').val() && $('#task_toTime > input').val() ) {
            $start = $('#task_fromDate').val() + ' ' + $('#task_fromTime > input').val() + ':00';
            $end = $('#task_toDate').val() + ' ' + $('#task_toTime > input').val() + ':00';
            if (document.getElementById("commeeting-meeting_resource")) {
                getResources($start, $end);
            }
        }
    });
      
    $('#task_fromTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });
    $('#task_fromTime').on("dp.change", function (e) {
        minDate = e.date.add(1, 'hours').format('HH:mm');
        $('#task_toTime > input').val(minDate);
        if( $('#task_toDate').val() && $('#task_fromDate').val() && $('#task_toTime > input').val() ) {
            $start = $('#task_fromDate').val() + ' ' + $('#task_fromTime > input').val() + ':00';
            $end = $('#task_toDate').val() + ' ' + $('#task_toTime > input').val() + ':00';
            if (document.getElementById("commeeting-meeting_resource")) {
                getResources($start, $end);
            }
        }
    });
    $('#task_toDate').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
    $('#task_toDate').on("dp.change", function (e) {
        if( $('#task_fromDate').val() && $('#task_fromTime > input').val() && $('#task_toTime > input').val() ) {
            $start = $('#task_fromDate').val() + ' ' + $('#task_fromTime > input').val() + ':00';
            $end = $('#task_toDate').val() + ' ' + $('#task_toTime > input').val() + ':00';
            if (document.getElementById("commeeting-meeting_resource")) {
                getResources($start, $end);
            }
        }
    });
    $('#task_toTime').datetimepicker({  format: 'LT', showClear: true, showClose: true });
    $('#task_toTime').on("dp.change", function (e) {
        if( $('#task_fromDate').val() && $('#task_fromTime > input').val() && $('#task_toDate').val() ) {
            $start = $('#task_fromDate').val() + ' ' + $('#task_fromTime > input').val() + ':00';
            $end = $('#task_toDate').val() + ' ' + $('#task_toTime > input').val() + ':00';
            if (document.getElementById("commeeting-meeting_resource")) {
                getResources($start, $end);
            }
        }
    });
    function getResources($start, $end) {
        $start = moment($start, 'YYYY-MM-DD HH:mm:ss').unix();
        $end = moment($end, 'YYYY-MM-DD HH:mm:ss').unix();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/community/meeting/resources']) ?>?start="+$start+"&end="+$end, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('commeeting-meeting_resource').innerHTML = result.options;
                $('#commeeting-meeting_resource').multiselect('rebuild');
                $('#commeeting-meeting_resource').multiselect('refresh');   
            }
        }
        xhr.send();
        return false;
    }
          
</script>