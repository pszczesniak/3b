<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chat');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper site-min-height">
    <!-- page start-->
  <div class="chat-room">
        <aside class="left-side">
            <div class="user-head"> <i class="fa fa-comments-o"></i> <h3>Rozmowy</h3> </div>
            <a class="lobby" href="<?= Url::to(['/community/chat/index']) ?>"> <span> <i class="fa fa-reply"></i> wróć do strony głównej</span> </a>
            <ul class="chat-list">
                <!-- <li class="">  </li>-->
                <?php
                    foreach($topics as $key => $topic) {
                        echo    '<li>'
                                    .'<a href="'.Url::to(['/community/chat/room', 'id' => $topic['tid']]).'"> '.( ($topic['gtype'] == 2) ? '<i class="fa fa-lock text--red"></i>' : '<i class="fa fa-commenting text--green"></i>').' <span>'.$topic['tname'].'</span> <i class="fa fa-times pull-right"></i>  </a>'
                                .'</li>';
                    }
                ?>
                
            </ul>
            <?php if( count($topics) == 0 ) echo '<div class="chat-topics-empty alert alert-warning">brak rozmów</div>'; ?>
            <!--<footer>
                <a class="chat-avatar" href="javascript:;"> pokaż pełna listę </a>
                <div class="user-status"> <i class="fa fa-circle text-success"></i>  Available  </div>
                <a class="chat-dropdown pull-right" href="javascript:;">  <i class="fa fa-chevron-down"></i> </a>
            </footer>-->
        </aside>
        <aside class="mid-side">
            <div class="chat-room-head">
                <!--<h3>Wpisy</h3>--><h5><i class="fa fa-<?= ($model->group['type_fk'] == 1) ? 'commenting text--green' : 'lock text--red' ?>"></i><?= $model->name ?></h5>
                <a href="<?= Url::to(['/community/chat/trefresh', 'id' => $model->id]) ?>" class="btn bg-green pull-right position" id="btn-refresh-talk"> <i class="fa fa-refresh"></i></a>
            </div>
            <div class="chat-room-talk">
                <?php
                    foreach($model->posts as $key => $post) {
                        echo    '<div class="group-rom">'
                                    .'<div class="first-part text--purple'.( (\Yii::$app->user->id==$post->id_user_fk) ? ' odd' : '').'">'. ( (\Yii::$app->user->id==$post->id_user_fk) ? 'TY' : $post->creator).'</div>'
                                    .'<div class="second-part">'.$post->note.'</div>'
                                    .'<div class="third-part" title="'.$post->created_at.'">'.Yii::$app->runAction('common/ago', ['date' => $post->created_at]).'</div>'
                                .'</div>';
                    }
                ?>
            </div>
            <footer>
                <div class="chat-txt">
                    <input type="text" class="form-control" id="chat-comment-text" placeholder="wpisz swój komentarz">
                </div>
                <!--<div class="btn-group">
                    <button type="button" class="btn btn-white"><i class="fa fa-meh-o"></i></button>
                    <button type="button" class="btn btn-white"><i class=" fa fa-paperclip"></i></button>
                </div>-->
                <button id="chat-comment-send" class="btn bg-green" data-action="<?= Url::to(['/community/chat/addpost', 'id' => $model->id]) ?>" >Wyślij</button>
            </footer>
        </aside>
        <aside class="right-side">
            <div class="user-head">
                <!--<a href="#" class="chat-tools btn-success"><i class="fa fa-cog"></i> </a>
                <a href="#" class="chat-tools btn-key"><i class="fa fa-key"></i> </a>-->
            </div>
            <div class="invite-row">
                <h4 class="pull-left">Uczestnicy</h4>
                <!--<a href="#" class="btn btn-success pull-right">+ nową</a>-->
                <?= ($model->group['type_fk'] == 2) ? Html::a('<i class="fa fa-plus"></i>nowi', Url::to(['/community/group/memberadd', 'id' => $model->id_group_fk]) , 
					['class' => 'btn btn-success btn-icon gridViewModal pull-right', 
					 'id' => 'group-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Nowi uczestnicy"
					]) : ''?>
            </div>
            <ul class="chat-available-user">
                <?php
                    if($model->group['type_fk'] == 2) {
                        foreach($model->group['members'] as $key => $member) {
                            echo    '<li id="group-member-'.$member->id.'">'
                                        .'<a href="'.Url::to(['/community/group/memberdelete', 'id' => $member->id]).'" class="deleteConfirm" data-label="Usuń uczestnika" title="Usuń uczestnika"> <i class="fa fa-circle text--'. (($member->id_user_fk == \Yii::$app->user->id) ? 'blue' : 'yellow') .'"></i> '.$member->employee['fullname'].' <i class="fa fa-times pull-right"></i> </a>'
                                    .'</li>';
                        }
                    } else {
                        echo '<li><div class="alert bg-green2">Rozmowa publiczna</div></li>';
                    }
                ?>
            </ul>
            
            <!--<footer>
                <a href="#" class="guest-on">
                    <i class="fa fa-check"></i>  Guest Access On
                </a>
            </footer>-->
        </aside>
    </div>
  <!-- page end-->
</section>