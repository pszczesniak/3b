<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chat');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="wrapper site-min-height">
    <!-- page start-->
    <div class="chat-room">
        <aside class="left-side">
            <div class="user-head"> <i class="fa fa-comments-o"></i> <h3>Rozmowy</h3> </div>
            <ul class="chat-list">
                <!--<li class=""> <a class="lobby" href="lobby.html"> <h4> <i class="fa fa-list"></i>  Lista </h4> </a> </li>-->
                <?php
                    foreach($topics as $key => $topic) {
                        echo    '<li>'
                                    .'<a href="'.Url::to(['/community/chat/room', 'id' => $topic['tid']]).'"> '.( ($topic['gtype'] == 2) ? '<i class="fa fa-lock text--red"></i>' : '<i class="fa fa-commenting text--green"></i>').' <span>'.$topic['tname'].'</span>  </a>'
                                .'</li>';
                    }
                ?>
                
            </ul>
            <?php if( count($topics) == 0 ) echo '<div class="chat-topics-empty alert alert-warning">brak rozmów</div>'; ?>
            <!--<footer>
                <a class="chat-avatar" href="javascript:;"> pokaż pełna listę </a>
                <div class="user-status"> <i class="fa fa-circle text-success"></i>  Available  </div>
                <a class="chat-dropdown pull-right" href="javascript:;">  <i class="fa fa-chevron-down"></i> </a>
            </footer>-->
      </aside>
      <aside class="mid-side">
            <div class="chat-room-head">
                <h3>Wpisy </h3>
                <!--<form action="#" class="pull-right position">
                    <input type="text" placeholder="Search" class="form-control search-btn ">
                </form>-->
            </div>
            <div class="room-desk">
                <h4 class="pull-left">Ostatnie wpisy</h4>
                <!--<a href="#" class="pull-right btn btn-default">+ Create Room</a>-->
                <?= Html::a('<i class="fa fa-plus"></i>Nowa rozmowa', Url::to(['/community/chat/createajax']) , 
					['class' => 'btn btn-success btn-icon gridViewModal pull-right', 
					 'id' => 'group-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Nowa rozmowa"
					]) ?>
                <div class="clear"></div>
                <div class="last-posts">
                    <?php
                        foreach($posts as $key => $post) {
                            echo    '<div class="room-box">'
                                        .'<h5 class="text-primary"><a href="'.Url::to(['/community/chat/room', 'id' => $post['id_topic_fk']]).'">'.( ($post['gtype'] == 2) ? '<i class="fa fa-lock text--red"></i>' : '<i class="fa fa-commenting text--green"></i>').$post['tname'].'</a></h5>'
                                        .'<p>'.$post['note'].'</p>'
                                        .'<p><span class="text-muted">Autor :</span> '.$post['creator'].' | <span class="text-muted">Aktywność :</span> '.Yii::$app->runAction('common/ago', ['date' => $post['created_at']]).'</p>'
                                    .'</div>';
                        }
                    ?>
                </div>
            </div>
            <!--<footer>
                <div class="chat-txt">
                    <input type="text" class="form-control">
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-white"><i class="fa fa-meh-o"></i></button>
                    <button type="button" class="btn btn-white"><i class=" fa fa-paperclip"></i></button>
                </div>
                <button class="btn btn-danger">Send</button>
            </footer>-->
        </aside>
        <aside class="right-side">
            <div class="user-head">
                <!--<a href="#" class="chat-tools btn-success"><i class="fa fa-cog"></i> </a>
                <a href="#" class="chat-tools btn-key"><i class="fa fa-key"></i> </a>-->
            </div>
            <div class="invite-row">
                <h4 class="pull-left">Grupy</h4>
                <!--<a href="#" class="btn btn-success pull-right">+ nową</a>-->
                <?= Html::a('<i class="fa fa-plus"></i>nową', Url::to(['/community/group/createajax']) , 
					['class' => 'btn btn-success btn-icon gridViewModal pull-right', 
					 'id' => 'group-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Nowa grupa"
					]) ?>
            </div>
            <ul class="chat-available-user">
                <?php
                    foreach($groups as $key => $group) {
                        echo    '<li id="chat-group-'.$group->id.'">'
                                    .'<a href="'.Url::to(['/community/group/updateajax', 'id' => $group->id]).'" class="gridViewModal" data-target="#modal-grid-item" title="Grupa '.( ($group->type_fk == 1) ? 'otwarta' : 'zamknięta' ).'"> <i class="fa fa-'.( ($group->type_fk == 1) ? 'commenting' : 'lock' ).' text--'.( ($group->type_fk == 1) ? 'green' : 'red' ).'"></i> '.$group->name.' </a>'
                                .'</li>';
                    }
                ?>
            </ul>
            <?php if( count($groups) == 0 ) echo '<div class="chat-groups-empty alert alert-warning">brak grup</div>'; ?>
            <!--<footer>
                <a href="#" class="guest-on">
                    <i class="fa fa-check"></i>  Guest Access On
                </a>
            </footer>-->
        </aside>
    </div>
  <!-- page end-->
</section>