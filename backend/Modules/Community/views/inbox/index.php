<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use common\widgets\Alert;
    
    $this->title = 'Wiadomości';
?>
<div class="view-inbox">
    <!-- row -->
    <div class="row">
        <div class="side-bar">
            <div class="action">
                <a href="<?= Url::to(['/community/inbox/create', 'id' => 0]) ?>?o=<?= $option ?>&l=<?= $label ?>" class="btn btn-sm bg-teal gridViewModal" data-target="#modal-grid-item" data-title="<?= \Yii::t('app', 'Write a message') ?>" data-icon="envelope"><?= \Yii::t('app', 'Write a message') ?></a>
            </div>
            <nav class="folders-wrapper">
                <ul class="folders-list list-unstyled">
                    <li <?= ($option == 'inbox') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => 'inbox', 'label' => $label]) ?>"><i class="fa fa-inbox"></i> <?= \Yii::t('app', 'Inbox') ?></a></li>
                    <li <?= ($option == 'star') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => 'star', 'label' => $label]) ?>"><i class="fa fa-star-o"></i> <?= \Yii::t('app', 'Starred') ?></a></li>
                    <li <?= ($option == 'send') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => 'send', 'label' => $label]) ?>"><i class="fa fa-send"></i> <?= \Yii::t('app', 'Sent') ?></a></li>
                    <li <?= ($option == 'bin') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => 'bin', 'label' => $label]) ?>"><i class="fa fa-trash"></i> <?= \Yii::t('app', 'Bin') ?></a></li>
                </ul>
            </nav>
            
            <div class="labels-wrapper">
                <h4 class="sub-title"><?= \Yii::t('app', 'Labels') ?></h4>
                <ul class="labels-list list-unstyled">
                    <li <?= ($label == 0) ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => $option, 'label' => 0]) ?>"><span class="label-color bg-white"></span> <?= \Yii::t('app', 'All') ?></a></li>                    
                    <li <?= ($label == 1) ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => $option, 'label' => 1]) ?>"><span class="label-color bg-blue"></span> <?= \Yii::t('app', 'Talk') ?></a></li>
                    <li <?= ($label == 2) ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => $option, 'label' => 2]) ?>"><span class="label-color bg-purple"></span> <?= \Yii::t('app', 'Case') ?></a></li>
                    <li <?= ($label == 3) ? 'class="active"' : '' ?>><a href="<?= Url::to(['/community/inbox/index', 'option' => $option, 'label' => 3]) ?>"><span class="label-color bg-pink"></span> <?= \Yii::t('app', 'Settlement') ?></a></li>
                </ul>
            </div>
        </div>
        <div class="content-panel">
            <div class="inbox-center">
                <div id="toolbar-inbox" class="btn-group toolbar-table-widget">
                        <?php /* Html::a('<i class="fa fa-print"></i>Export', Url::to(['/cases/export']) , 
                                ['class' => 'btn btn-info btn-icon btn-export', 
                                 'id' => 'case-create',
                                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                 'data-target' => "#modal-grid-item", 
                                 'data-form' => "#filter-task-inbox-search", 
                                 'data-table' => "table-items",
                                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                                ]) */ ?>
                    
                    <button type="button" class="btn btn-icon bg-purple" id="group"><i class="fa fa-cogs"></i>Operacje (<span id="group-counter">0</span>)</button>
                    <button type="button" class="btn bg-purple dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/community/inbox/sforward']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Prześlij dalej"><i class="fa fa-mail-forward text--blue"></i>&nbsp;prześlij dalej</a>'
                            .'</li>' ?>
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/community/inbox/sstarred', 'flag' => 1]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Oznacz"><i class="fa fa-star text--yellow"></i>&nbsp;oznacz</a>'
                            .'</li>' ?>
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/community/inbox/sstarred', 'flag' => 0]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Odznacz"><i class="fa fa-star-o"></i>&nbsp;odznacz</a>'
                            .'</li>' ?>
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/community/inbox/sdelete']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-inbox" data-title="Usuń"><i class="fa fa-trash text--red"></i>&nbsp;usuń</a>'
                            .'</li>' ?>
                        <!--<li role="separator" class="divider"></li>-->
                    </ul>
                   

                    <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-inbox"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
                </div>
                <div class="div-table">
                    <table  class="table table-custom table-items header-fixed table-widget table-multi-action"  id="table-inbox"
                            data-toolbar="#toolbar-inbox" 
                            data-toggle="table-widget" 
                            data-show-refresh="false" 
                            data-show-toggle="true"  
                            data-show-columns="false" 
                            data-show-export="false"  
                        
                            data-show-pagination-switch="false"
                            data-pagination="true"
                            data-id-field="id"
                            data-show-header="false"
                            data-show-footer="false"
                            data-side-pagination="server"
                            data-row-style="rowStyle"
                            data-sort-name="name"
                            data-sort-order="asc"
                            data-method="get"
                            data-search-form="#filter-task-inbox-search"
                            data-unique-id="nid"
                            data-url=<?= Url::to(['/community/inbox/data', 'option' => $option, 'label' => $label]) ?>>
                        <thead>
                            <tr>
                                <th data-field="id" data-visible="false">ID</th>
                                <th data-field="action" data-visible="false">ID</th>
                                <th data-field="state" data-visible="true" data-checkbox="true" data-align="center" data-width="20px"></th>
                                <th data-field="star" data-events="actionEvents" data-sortable="true" data-align="center" data-width="20px"></th>
                                <th data-field="author"  data-sortable="false"></th>
                                <th data-field="msg"  data-sortable="false"></th>
                                <th data-field="file"  data-sortable="false" data-align="center" data-width="20px"></th>
                                <th data-field="date" data-sortable="false" data-align="center" data-width="60px"></th>
                            </tr>
                        </thead>
                        <tbody class="ui-sortable">

                        </tbody>
                        
                    </table>
                </div>
            </div>
     
        </div>
    </div>
    <!-- /.row -->
</div>
