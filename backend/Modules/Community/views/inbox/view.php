<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([ 'options' => ['class' => 'modalAjaxFormWithAttach', 'data-target' => '#modal-grid-item', 'enctype' => 'multipart/form-data', 'data-model' => 'ComMessage'] ]); ?>
    <div class="modal-body">
		<div class="card-box msg-view">
            <h4 class="msg-title"><?= $message->symbol.' '.$message->title ?></h4>
            <?php if($message->type_fk == 2) { ?>
                <small class="text--purple">Dotyczy sprawy: <?= $message->case['name'] ?></small>
            <?php } ?>
            <?php if($message->type_fk == 3) { ?>
                <small class="text--pink">Dotyczy rozliczenia: <?= $message->order['name'] ?> za okres <?= $message->acc_period ?></small>
            <?php } ?>
            <hr>

            <div class="media m-b-30">
                <?php
                    $avatar = (is_file(Yii::getAlias('@webroot') . "/web/uploads/employees/cover/".$message->created_by."/preview.jpg"))? "/uploads/employees/cover/".$message->created_by."/preview.jpg":"/images/avatar.png"; 
                ?>
                <span class="thumb-sm"><img src="<?= $avatar ?>" alt="avatar"> </span>
                <div class="media-body">
                    <span class="media-meta pull-right"><?= $message->created_at ?></span>
                    <h4 class="text-primary font-16 m-0"><?= $message->author['fullname'] ?> [<?= $message->author['email'] ?>]</h4>
                    <small class="text-muted"><?= Yii::t('app', 'To') ?>: <?= $message->recipients ?></small>
                    <?php if($message->id_customer_fk) { ?>
                        <br /><small class="text-muted text--purple"><?= Yii::t('app', 'Customer') ?>: <?= $message->customer['name'] ?></small>
                    <?php } ?>
                </div>
            </div>

            <p><?= $message->message ?></p>
            <?php if($message->with_files) { ?>
                <hr>

                <h4> <i class="fa fa-paperclip"></i> Attachments <span>(<?= count($message->files) ?>)</span> </h4>
                <ul>
                <?php
                    foreach($message->files as $key => $file) {
                        echo '<li><a href="#">test '.$file->title_file.'</a></li>';
                    }
                ?>
                </ul>
             
            <?php }  ?>
        </div>
        <?= $form->field($reply, 'message')->textarea(['rows' => 3, 'placeholder' => 'Wpisz odpowiedź...'])->label(false) ?>
    </div>
    
    <div class="modal-footer"> 
        <div class="form-group align-right">
            <?= Html::submitButton('Odpowiedz', ['class' => 'btn btn-info']) ?>
            <button aria-hidden="true" data-dismiss="modal" class="btn" type="button" >Odrzuć</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
  
</script>