<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([ 'options' => ['class' => 'modalAjaxFormWithAttach', 'data-target' => '#modal-grid-item', 'data-table' => '#table-inbox', 'enctype' => 'multipart/form-data', 'data-model' => 'ComMessage'] ]); ?>
    <div class="modal-body">
		<div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-envelope"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Message') ?></span></a></li>
                    <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-paperclip"></i><span class="panel-tabs--text"><?= Yii::t('app', 'Attachments') ?></span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
                        <?= $form->field($model, 'e_emails', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['data-placeholder' => 'wybierz innych pracowników Twojej firmy', 'class' => 'ms-select-ajax', 'multiple' => 'multiple', ] ); ?>
                        <div class="grid">
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::getListWithAccount(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'/*, 'disabled' => ( ($model->is_confirm) ? true : false )*/] ) ?> 
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?= $form->field($model, 'c_emails', [/*'template' => '{input}{error}',  'options' => ['class' => '']*/])->dropdownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::getAccountsList(0), 'id', 'fullname'), ['data-placeholder' => 'wybierz osoby kontaktowe ze strony klienta', 'class' => 'ms-select-ajax', 'multiple' => 'multiple', ] )->label('Osoby kontaktowe'); ?>
                            </div>
                        </div>
                        <?= $form->field($model, 'title')->textInput(['placeholder' => 'Wpisz tytuł...', 'maxlength' => true]) ?>
                        <?= $form->field($model, 'message')->textarea(['rows' => 3, 'placeholder' => 'Wpisz wiadomość...'])->label(false) ?>
 
                        <?= $form->field($model, 'type_fk')
                         ->dropDownList( \backend\Modules\Community\models\ComMessage::listTypes(), 
                                        [ 'class' => 'form-control',
                                          'options' => $emails/*, 'disabled' => ( ($model->is_confirm) ? true : false )*/] )->label(false) ?> 

                        <div id="msg-cases" class="<?= ($model->type_fk == 2) ? '' : 'nodisplay' ?>">
                            <?= $form->field($model, 'id_set_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList(($model->id_customer_fk) ? $model->id_customer_fk : 0), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                        </div>
                        <div id="msg-settlements" class="<?= ($model->type_fk == 3) ? '' : 'nodisplay' ?>">
                            <div class="grid">
                                <div class="col-sm-8 col-xs-12">    
                                    <?= $form->field($model, 'id_order_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getList(($model->id_customer_fk) ? $model->id_customer_fk : 0), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'/*, 'disabled' => ( ($model->is_confirm) ? true : false )*/] ) ?> 
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group field-commessage-acc_period">
                                        <label for="commessage-acc_period" class="control-label">Okres rozliczeniowy</label>
                                        <div class='input-group date' id='datetimepicker_acc_period'>
                                            <input type='text' class="form-control" id="commessage-acc_period" name="ComMessage[acc_period]" value="<?= $model->acc_period ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#datetimepicker_acc_period').datetimepicker({ format: 'YYYY-MM',  });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2c">
                        <div class="files-wr" data-count-files="1" data-title="<?= \Yii::t('app', 'Attach file') ?>">
                            <div class="one-file">
                                <label for="file-1"><?= \Yii::t('app', 'Attach file') ?></label>
                                <input class="attachs" name="file-1" id="file-1" type="file">
                                <div class="file-item hide-btn">
                                    <span class="file-name"></span>
                                    <span class="btn btn-del-file">x</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php
        if($model->parent) {
            echo '<div class="chat-box">'.$this->render('_note', ['model' => $model->parent]).'</div>';
        }
    ?>
    
    <div class="modal-footer"> 
        <div class="form-group align-right">
            <?= Html::submitButton('Wyślij', ['class' => 'btn btn-info']) ?>
            <button aria-hidden="true" data-dismiss="modal" class="btn" type="button" >Odrzuć</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    
	document.getElementById('commessage-type_fk').onchange = function(event) {
        if(event.target.value == 1) {
            document.getElementById('msg-cases').classList.add('nodisplay');  
            document.getElementById('msg-settlements').classList.add('nodisplay');
        } else if(event.target.value == 2) {
            document.getElementById('msg-cases').classList.remove('nodisplay');  
            document.getElementById('msg-settlements').classList.add('nodisplay');  
        } else {
            document.getElementById('msg-cases').classList.add('nodisplay');  
            document.getElementById('msg-settlements').classList.remove('nodisplay');
        }
    }
    
    document.getElementById('commessage-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/mlists']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
				document.getElementById('commessage-c_emails').innerHTML = result.accounts; 
                $('#commessage-c_emails').multiselect('rebuild');
                $('#commessage-c_emails').multiselect('refresh');
                
                document.getElementById('commessage-id_set_fk').innerHTML = result.cases; 
                document.getElementById('commessage-id_order_fk').innerHTML = result.orders; 
            }
        }
       xhr.send();
       
       return false;
    }
</script>