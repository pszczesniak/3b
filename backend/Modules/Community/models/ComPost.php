<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Community\models\ComPostMember;

/**
 * This is the model class for table "{{%com_post}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_root_fk
 * @property integer $id_parent_fk
 * @property integer $id_user_fk
 * @property integer $id_fk
 * @property integer $id_topic_fk
 * @property string $note
 * @property string $created_at
 * @property integer $created_by
 */
class ComPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_root_fk', 'id_parent_fk', 'id_user_fk', 'id_fk', 'id_topic_fk', 'created_by'], 'integer'],
            [['id_user_fk', 'id_fk', 'id_topic_fk'], 'required'],
            [['note'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_root_fk' => Yii::t('app', 'Id Root Fk'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'id_topic_fk' => Yii::t('app', 'Id Topic Fk'),
            'note' => Yii::t('app', 'Note'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            if($this->topic['group']) {
                foreach($this->topic['group']['members'] as $key => $member){
                    $member = new ComPostMember();
                    $member->id_post_fk = $this->id;
                    $member->type_fk = 1;
                    $member->id_user_fk = $member->id_user_fk;
                    $member->id_fk = $member->id_fk;
                    $member->status = ($member->id_user_fk == \Yii::$app->user->id) ? 1 : 0;
                    
                    $member->created_by = \Yii::$app->user->id;
                    $member->created_at = new Expression('NOW()');
                    $member->save();
                }
            }
        } 

		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getTopic()
    {
		return $this->hasOne(\backend\Modules\Community\models\ComTopic::className(), ['id' => 'id_topic_fk']);
    }
    
    /*public function getGroup()
    {
		return $this->hasOne(\backend\Modules\Community\models\ComTopic::className(), ['id' => 'id_topic_fk']);
    }*/
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by);
        
        return ($user) ? $user->fullname : 'brak danych';
	}
}
