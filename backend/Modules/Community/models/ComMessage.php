<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_message}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_message_fk
 * @property integer $id_root_fk
 * @property integer $id_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property string $title
 * @property string $message
 * @property string $emails_cw
 * @property integer $user_env
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class ComMessage extends \yii\db\ActiveRecord
{
    public $attachs;
    public $e_emails;
    public $c_emails;
    
    public $mid;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_message_fk', 'id_root_fk', 'id_fk', 'id_customer_fk', 'id_set_fk', 'id_order_fk', 'user_env', 'status', 'created_by', 'updated_by', 'with_files'], 'integer'],
            [['title', 'message'], 'required'],
            [['message', 'emails_cw', 'acc_period'], 'string'],
            [['created_at', 'updated_at', 'attachs', 'e_emails', 'c_emails'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['id_set_fk'], 'required', 'message' => 'Proszę wskazać sprawę lub projekt. Jeśli na liście nie widzisz konkretnej sprawy/projektu to zgłość w kancelarii prośbę o udostępnienie.', 'when' => function($model) { return ($model->type_fk == 2); }],
            [['id_order_fk'], 'required', 'message' => 'Proszę wskazać umowę i okres rozliczeniowy z którymi związana jest wiadomość.', 'when' => function($model) { return ($model->type_fk == 3); }],
            //[['attachs'], 'file', 'skipOnEmpty' => true, /*'extensions' => 'png, jpg', 'maxFiles' => 4*/],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_fk' => 'Type Fk',
            'id_message_fk' => 'Id Message Fk',
            'id_root_fk' => 'Id Root Fk',
            'id_fk' => 'Id Fk',
            'id_customer_fk' => \Yii::t('app', 'Customer'),
            'id_set_fk' => \Yii::t('app', 'Case'),
            'id_order_fk' => \Yii::t('app', 'Order'),
            'acc_period' => \Yii::t('app', 'Period'),
            'title' => 'Tytuł',
            'message' => 'Treść wiadomości',
            'emails_cw' => 'Do wiadomości',
            'user_env' => 'User Env',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'e_emails' => 'Pracownicy kancelarii', 
            'c_emails' => 'Pracownicy firmy'
        ];
    }
	
	public function beforeSave($insert) {      
        if($this->attachs) $this->with_files = 1;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert && $this->attachs) {
            foreach($this->attachs as $key => $file) {
                if($file) {
                    $modelFile = new \common\models\Files(['scenario' => 'save']);
                    $modelFile->extension_file = $file->extension;
                    $modelFile->size_file = $file->size;
                    $modelFile->mime_file = $file->type;
                    $modelFile->name_file = $file->baseName;
                    $modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
                    $modelFile->title_file = $file->baseName.'.'.$file->extension;
                    $modelFile->id_fk = $this->id;
                    $modelFile->id_type_file_fk = 9;
                    $modelFile->id_dict_type_file_fk = 0;
                    $modelFile->type_fk = 0;
                    
                    if($modelFile->save()) {
                        /*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
                        try {
                            $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                        } catch (\Exception $e) {
                            var_dump($e); exit;
                        }
                    }
                }
            }
        } 
		
		if ($insert) {
			if($this->user_env == 2) {
				$newC = new \backend\Modules\Community\models\ComMessageCustomer();
				$newC->id_message_fk = $this->id;
				$newC->id_user_fk = \Yii::$app->user->id;
                $newC->email = \common\models\crm\CUser::findOne(\Yii::$app->user->id)->email;
				$newC->is_read = 1;
                $newC->status = 2;
				
				$newC->save();
                $this->mid = $newC->id;
				
				$newE = new \backend\Modules\Community\models\ComMessageEmployee();
				$newE->id_message_fk = $this->id;
				$newE->id_user_fk = $this->e_emails;
                $newE->email = \common\models\User::findOne($this->e_emails)->email;
				$newE->save();
                
                if($this->c_emails) {
                    foreach($this->c_emails as $key => $value) {
                        $newE = new \backend\Modules\Community\models\ComMessageCustomer();
                        $newE->id_message_fk = $this->id;
                        $newE->id_user_fk = $value;
                        $newE->email = \common\models\crm\CUser::findOne($value)->email;
                        $newE->save();
                    }
                }
			} else {	
				$newE = new \backend\Modules\Community\models\ComMessageEmployee();
				$newE->id_message_fk = $this->id;
				$newE->id_user_fk = \Yii::$app->user->id;
                $newE->email = \common\models\User::findOne(\Yii::$app->user->id)->email;
				$newE->is_read = 1;
                $newE->status = 2;
				
				$newE->save();
                $this->mid = $newE->id;
                
                if($this->id_customer_fk) {
                    $customer = \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk);
                    
                    $newC = new \backend\Modules\Community\models\ComMessageCustomer();
                    $newC->id_message_fk = $this->id;
                    $newC->id_user_fk = $customer->id_user_fk;
                    $newC->email = \common\models\crm\CUser::findOne($newC->id_user_fk)->email;
                    $newC->save();
                }
                
                if($this->c_emails) {
                    foreach($this->c_emails as $key => $value) {
                        $newC = new \backend\Modules\Community\models\ComMessageCustomer();
                        $newC->id_message_fk = $this->id;
                        $newC->id_user_fk = $value;
                        $newC->email = \common\models\crm\CUser::findOne($value)->email;
                        $newC->save();
                    }
                }
                
                if($this->e_emails) {
                    foreach($this->e_emails as $key => $value) {
                        $newE = new \backend\Modules\Community\models\ComMessageEmployee();
                        $newE->id_message_fk = $this->id;
                        $newE->id_user_fk = $value;
                        $newE->email = \common\models\User::findOne($value)->email;
                        $newE->save();
                    }
                }
			}
		}
    }
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getParent() {
        return \backend\Modules\Community\models\ComMessage::findOne($this->id_message_fk);
    }
    
    public function getFiles() {
        return \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 9, 'id_fk' => $this->id])->all();
    }
    
    public static function listTypes() {
        return [1 => \Yii::t('app', 'Talk'), 2 => Yii::t('app', 'Case'), 3 => Yii::t('app', 'Settlement')];
    }
    
     public function upload()  {
        if ($this->validate()) { 
            foreach ($this->attachs as $file) {
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
    
    public static function getDict() {
        $icons = [1 => 'comments-o', 2 => 'suitcase', 3 => 'calculator'];
        $colors = [1 => 'blue', 2 => 'purple', 3 => 'pink'];
        $names = [1 => Yii::t('app', 'Talk'), 2 => Yii::t('app', 'Case'), 3 => Yii::t('app', 'Settlement')];
        
        return ['icons' => $icons, 'colors' => $colors, 'names' => $names];
    }
    
    public function getSymbol() {
        return '<span class="label bg-'.self::getDict()['colors'][$this->type_fk].'"><i class="fa fa-'.self::getDict()['icons'][$this->type_fk].'"></i></span>'; 
    }
	
	public static function getCompanyRecipients($id) {
		$emails = [];
		
		$client = \backend\Modules\Crm\models\Customer::findOne($id);
		$guardian = $client->guardian;
		$emails[1] = ($guardian) ? $guardian->email : \Yii::$app->params['guardianEmails'];
		$emails[2] = ($guardian) ? $guardian->email : \Yii::$app->params['caseEmails'];
		$emails[3] = \Yii::$app->params['settlementsEmails'];
		
		return $emails;
	}
    
    public function getAuthor() {
        if($this->user_env == 1)
            return \common\models\User::findOne($this->created_by);
        if($this->user_env == 2)
            return \common\models\crm\CUser::findOne($this->created_by);
    }
    
    public function getRecipients() {
        $sql = "(select group_concat(concat_ws(' ', u.lastname,u.firstname)) as recipients from {{%com_message_employee}} cme join {{%user}} u on cme.id_user_fk=u.id where cme.id_message_fk = ".$this->id." and cme.status != 2)";
        $result = Yii::$app->db->createCommand($sql)->queryOne();
        return $result['recipients'];
    }
    
    public function getCustomer() {
        return \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk);
    }
    
    public function getCase() {
        return \backend\Modules\Task\models\CalCase::findOne($this->id_set_fk);
    }
    
    public function getOrder() {
        return \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
    }
}
