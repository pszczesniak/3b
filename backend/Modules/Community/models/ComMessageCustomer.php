<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_message_customer}}".
 *
 * @property integer $id
 * @property integer $id_message_fk
 * @property integer $id_user_fk
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ComMessageCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_message_customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_message_fk', 'id_user_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'is_starred', 'is_read', 'is_bin'], 'integer'],
            [['email'], 'required'],
            [['email'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_message_fk' => 'Id Message Fk',
            'id_user_fk' => 'Id User Fk',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
	
	public function beforeSave($insert) {      
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getMessage() {
        return \backend\Modules\Community\models\ComMessage::findOne($this->id_message_fk);
    }
}
