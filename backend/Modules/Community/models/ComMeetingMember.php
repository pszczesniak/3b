<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_meeting_member}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_meeting_fk
 * @property integer $id_user_fk
 * @property integer $id_employee_fk
 * @property string $comment
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $accept_at
 * @property string $discard_at
 */
class ComMeetingMember extends \yii\db\ActiveRecord
{
    
    public $meeting_employees;
    public $user_action;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_meeting_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_meeting_fk', 'id_user_fk', 'id_employee_fk', 'status', 'created_by', 'id_action_fk', 'id_task_fk'], 'integer'],
            [['id_meeting_fk', 'id_user_fk', 'id_employee_fk'], 'required'],
            [['comment'], 'string'],
            [['created_at', 'accept_at', 'discard_at', 'meeting_employees'], 'safe'],
            ['comment', 'required', 'message' => 'Prosz� podac pow�d odrzucenia', 'when' => function($model) { return ($model->status == -1); }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_meeting_fk' => Yii::t('app', 'Id Meeting Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_employee_fk' => Yii::t('app', 'Id Fk'),
            'comment' => Yii::t('app', 'Comment'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'accept_at' => Yii::t('app', 'Accept At'),
            'discard_at' => Yii::t('app', 'Discard At'),
        ];
    }
    
    public function beforeSave($insert) {      
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} /*else { 
				$this->updated_by = \Yii::$app->user->id;   
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            if($this->status != 2 && $this->type_fk == 1) {
				$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_fk);
				
                try {
                    if(Yii::$app->params['env'] == 'prod') {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $this, 'alert' => 'add', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($employee->email)
                            ->setBcc('kamila_bajdowska@onet.eu')
                            ->setSubject('Zaproszenie na spotkanie wygenerowane przez system ' . \Yii::$app->name /*. ' - ' . $employee->fullname*/)
                            ->send();
                    } else {
                        \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $this, 'alert' => 'add', 'logoImage' => \Yii::getAlias('@frontend').'/web/images/logo_dark.jpg'])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo('kamila_bajdowska@onet.eu')
                            ->setSubject('DEV: Zaproszenie na spotkanie wygenerowane przez system ' . \Yii::$app->name . ' - ' . $employee->fullname)
                            ->send();
                    }
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'meeting-employee: '.$employee->email;
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
            }
        } 

		parent::afterSave($insert, $changedAttributes);
	}
	
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getMeeting()
    {
		return $this->hasOne(\backend\Modules\Community\models\ComMeeting::className(), ['id' => 'id_meeting_fk']);
    }
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by);
        
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getAccepturl() {
        return \Yii::$app->urlManager->createAbsoluteUrl(['/community/meeting/accepted','id' => $this->id]);
    }
    
    public function getDiscardurl() {
        return \Yii::$app->urlManager->createAbsoluteUrl(['/community/meeting/discarded','id' => $this->id]);
    }
}
