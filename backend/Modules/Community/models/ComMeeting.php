<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_meeting}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_group_fk
 * @property integer $title
 * @property string $date_from
 * @property string $date_to
 * @property string $describe
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ComMeeting extends \yii\db\ActiveRecord
{
    const SCENARIO_CHANGE = 'change';
    const SCENARIO_DISCARD = 'discard';
    
    public $id_employee_fk;
	public $id_resource_fk;
    public $meeting_employees;
    public $meeting_resource;
    public $meeting_contacts;
    
    public $fromDate;
    public $fromTime;
    public $toDate;
    public $toTime;
    public $all_day = 0;
    
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_meeting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_group_fk', 'id_customer_fk', 'id_customer_branch_fk', 'id_resource_fk', 'id_dict_meeting_type_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_from'], 'required'/*, 'when' => function($model) { return ($model->status == 1); }*/],
            [['purpose', 'describe', 'title', 'applicant_name'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'fromDate', 'toDate', 'fromTime', 'toTime', 'meeting_employees', 'meeting_resource', 'meeting_contacts'], 'safe'],
            [['fromDate'], 'required', 'message' => 'Proszę podać datę rozpoczęcia', 'when' => function($model) { return ($model->status == 1 && $model->user_action != 'change-resources'); }],
            [['fromTime'], 'required', 'message' => 'Proszę podać godzinę rozpoczęcia', 'when' => function($model) { return ($model->status == 1 && $model->user_action != 'change-resources'); }],
            [['date_to'], 'required', 'message' => 'Jeśli chcesz zarezerwować zasób musisz podać datę zakończenia spotkania', 'when' => function($model) { return ($model->id_resource_fk && $model->status == 1); }],
            [['date_from'], 'validateReservation', 'when' => function($model) { return (count($model->meeting_resource) > 0 && $model->status == 1); }],
            [['date_to'], 'validateDates'],
            // [['meeting_resource'], 'validateType'],
            ['meeting_resource', 'validateReal', 'when' => function($model) { return $model->status = 1; }],
			[['applicant_name'], 'required', 'when' => function($model) { return ($model->type_fk == 2 && !$model->id_customer_fk); }, 'message' => 'Proszę wybrać kontakt z listy lub wpisać imię i nazwisko lub nazwę firmy'],
        ];
    }
    
    public function validateDates(){
		if($this->date_to && strtotime($this->date_to) <= strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Wprowadź poprawną datę końca');
		}
	}
    
    public function validateType(){
		if($this->type_fk == 2 && count($this->meeting_resource) > 0){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('meeting_resource','Dla spotkań osobistych nie ma możliwości rezerwacji zasobów');
		}
	}
    
    public function validateReal(){
        foreach($this->meeting_resource as $key => $resource) {
            if( strtotime($this->date_from) < ( strtotime(date('Y-m-d H:i:s')) + 60*15 ) ){
                $this->addError('meeting_resource','Zasób należy zarezerwować przynajmniej na 15 minut wcześniej');
            } 
        }
	}
    
    public function validateReservation(){
        $startDate = $this->date_from.':00';
		$endDate = ($this->date_to) ? $this->date_to.':00' : $startDate;
		if($this->date_from && $this->date_to) {
            foreach($this->meeting_resource as $key => $resource) {
                $reservations = \backend\Modules\Company\models\CompanyResourcesSchedule::find()->where([/*'status' => 1,*/ 'id_resource_fk' => $resource])
                                    ->andWhere(" status >= 0 and id != ". ( ($this->id) ? $this->id : 0) )
                                    ->andWhere(" id_event_fk != ". (($this->id) ? $this->id : 0) )
                                    //->andWhere( " ( ( '".$startDate."' >= date_from and '".$startDate."' < date_to ) or ( '".$endDate."' between date_from and date_to ) or (  date_from between '".$startDate."' and date_to ) )")
                                    ->andWhere( " ( ( '".$startDate."' = date_from and '".$endDate."' = date_to) or "
                                         ." ( '".$startDate."' > date_from and '".$startDate."' < date_to ) or "
										 ." ( '".$endDate."' > date_from and '".$endDate."' < date_to ) or "
                                         ." ( '".$startDate."' <= date_from and '".$endDate."' >= date_to ) )")
                                    ->all();
                if( count($reservations) > 0 ){
                    $temp = \backend\Modules\Company\models\CompanyResources::findOne($resource);
                    $this->addError('date_from','Zasób `<b>'.$temp->name.'</b>` został zarezerwowany w terminie '.$reservations[0]->date_from.' - '.$reservations[0]->date_to);
                } else {
					$tempResource = \backend\Modules\Company\models\CompanyResources::findOne($resource);
					if( $tempResource->only_management ){
						$creatorOk = false; $employeeOk = false;
						
						$sqlCreator = "select id_dict_employee_kind_fk, "
								  ." (select count(*) from law_employee_department ed join law_company_department d on d.id=ed.id_department_fk where ed.id_employee_fk=e.id and all_employee = 1 ) is_special, "
								  ." (select count(*) from law_company_department cd where id_employee_manager_fk = e.id) is_manager "
							 ." from law_company_employee e where id_user_fk = ".\Yii::$app->user->id;
						$creatorData = Yii::$app->db->createCommand($sqlCreator)->queryOne();
						
						if($creatorData['id_dict_employee_kind_fk'] == 100 || $creatorData['is_special'] || $creatorData['is_manager'])
							$creatorOk = true;
						
						if(!$creatorOk) {
							$this->addError('meeting_resource', 'Nie masz uprawnień do rezerwacji zasobu `'.$tempResource->name.'`');
							return true;
						}	
						$sqlEmployee = "select id_dict_employee_kind_fk, "
								  ." (select count(*) from law_employee_department ed join law_company_department d on d.id=ed.id_department_fk where ed.id_employee_fk=e.id and all_employee = 1 ) is_special, "
								  ." (select count(*) from law_company_department cd where id_employee_manager_fk = e.id) is_manager "
							 ." from law_company_employee e where id_user_fk = ".\Yii::$app->user->id;
						$employeeData = Yii::$app->db->createCommand($sqlEmployee)->queryOne();
						
						if($employeeData['id_dict_employee_kind_fk'] == 100 || $employeeData['is_manager'])
							$employeeOk = true;
						
						if(!$employeeOk)
							$this->addError('meeting_resource', 'Zasób `'.$tempResource->name.'` dostępny jedynie dla zarządu i dyrektorów');
					}
				}
            }
		} else {
			$this->addError('id_resource_fk','Jeśli chcesz zarezerwować zasób musisz podać datę i godzinę zakończenia spotkania');
		}
        //}
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type_fk' => Yii::t('app', 'Rodzaj spotkania'),
            'id_dict_meeting_type_fk' => 'Typ',
            'id_group_fk' => Yii::t('app', 'Id Group Fk'),
			'id_customer_fk' => Yii::t('app', 'Kontakt'),
            'id_customer_branch_fk' => 'Oddział',
            'title' => Yii::t('app', 'Title'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'purpose' => 'Cel spotkania',
            'describe' => Yii::t('app', 'Protokuł'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'meeting_employees' => 'Uczestnicy spotkania',
			'id_employee_fk' => 'Pracownik',
            'meeting_resource' => 'Zasoby',
			'id_resource_fk' => 'Projekt',
			'fromDate' => 'Data rozpoczęcia',
			'toDate' => 'Data zakończenia',
			'fromTime' => 'Godzina rozpoczęcia',
			'toTime' => 'Godzina zakończenia',
			'applicant_name' => 'Spotkanie z',
            'meeting_contacts' => 'Osoby kontaktowe'
        ];
    }
    
    public function beforeSave($insert) {      
        if(!$this->title) {
			if($this->type_fk == 1)
				$this->title = 'Spotkanie wewnętrzene';
			else
				$this->title = 'Spotkanie z '.(($this->id_customer_fk) ? $this->customer['name'].(' '.$this->applicant_name) : $this->applicant_name);
		}
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
   /* public function afterSave($insert, $changedAttributes) {
        if (!$insert) {
            
            if($this->user_action == 'change-term' || $this->user_action == 'remove') {
                $title = 'Powiadomienie o zmianie daty spotkania spotkania wygenerowane przez system ' . \Yii::$app->name;
                if($this->user_action == 'remove')
                    $title = 'Informacja o odwołaniu spotkania automatycznie wygenerowana przez system ' . \Yii::$app->name;
                foreach($this->employees as $key => $employee) {
                    $title_ =  $title; //.' - '.$employee->employee['fullname'];
                    if($employee->status == 2 && \Yii::$app->user->id == $this->created_by) {
                        $send = 0;
                    } else {
                        try {
                            if(Yii::$app->params['env'] == 'prod') {
                                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => $this->user_action])
                                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                    ->setTo($employee->employee['email'])
                                    ->setBcc('kamila_bajdowska@onet.eu')
                                    ->setSubject($title_)
                                    ->send();
                            } else {
                                \Yii::$app->mailer->compose(['html' => 'meetingInfo-html', 'text' => 'meetingInfo-text'], ['model' => $employee, 'alert' => $this->user_action])
                                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                    ->setTo(\Yii::$app->params['testEmail'])
                                    ->setSubject('DEV: '.$title_)
                                    ->send();

                            }
                        } catch (\Swift_TransportException $e) {
                            //echo 'Caught exception: ',  $e->getMessage(), "\n";
                            $request = Yii::$app->request;
                            $log = new \common\models\Logs();
                            $log->id_user_fk = Yii::$app->user->id;
                            $log->action_date = date('Y-m-d H:i:s');
                            $log->action_name = 'meeting-employee: '.$employee->employee['email'];
                            $log->action_describe = $e->getMessage();
                            $log->request_remote_addr = $request->getUserIP();
                            $log->request_user_agent = $request->getUserAgent(); 
                            $log->request_content_type = $request->getContentType(); 
                            $log->request_url = $request->getUrl();
                            $log->save();
                        }
                    }
                }
            }
            
        } 

		parent::afterSave($insert, $changedAttributes);
	} */
	
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function listTypes() {
        
        $types = [];
        /*$types[1] = 'wewnętrzne'; //'Kancelaryjne';
        $types[2] = 'zewnętrzne'; //'Osobiste';*/
        $data = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['status' => 1, 'id_dictionary_fk' => 22])->all();
        foreach($data as $key => $value) {
            $types[$value->id] = $value['name'];
        }
        
        return $types;
    }
    
    public function getType() {
        return self::listTypes()[$this->type_fk];
    }
    
    public function getCreator() {
		$user = \common\models\User::findOne($this->created_by);
        
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getRemoving() {
		$user = \common\models\User::findOne($this->deleted_by);
        
        return ($user) ? $user->fullname : 'brak danych';
	}
    
    public function getEmployees()
    {
		//$items = $this->hasMany(\backend\Modules\Community\models\ComMeetingMember::className(), ['id_meeting_fk' => 'id'])->where('status >= -1'); 
        $items = $this->hasMany(\backend\Modules\Community\models\ComMeetingMember::className(), ['id_meeting_fk' => 'id'])
                              ->leftJoin('{{%company_employee}}', '{{%company_employee}}.id={{%com_meeting_member}}.id_employee_fk')
                              //->where('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or {{%company_employee}}.id = '.$employee->id.') and {{%case_employee}}.status >= 0 and {{%case_employee}}.is_show = 1')
                              ->where(' {{%com_meeting_member}}.status != -2')
                              ->select(['{{%com_meeting_member}}.id as id', '{{%com_meeting_member}}.status as status', 'comment', 'id_meeting_fk','id_employee_fk','firstname','lastname', 'email', 'discard_at', 'accept_at'])->distinct()->orderby('{{%com_meeting_member}}.status desc,{{%company_employee}}.lastname'); 
		return $items;
    }
    
    public function getResources()
    {
		//$items = $this->hasMany(\backend\Modules\Community\models\ComMeetingResource::className(), ['id_meeting_fk' => 'id'])->where(['status' => 1]); 
        $items = \backend\Modules\Community\models\ComMeetingResource::find()->where(['status' => 1, 'id_meeting_fk' => $this->id])->all();
		return $items;
    }
	
	public function getResource()
    {
		if($this->id_resource_fk)
			$resource = \backend\Modules\Company\models\CompanyResources::findOne($this->id_resource_fk); 
		else
			$resource = false;
		return $resource;
    }
	
	public function getCustomer()
    {
		if($this->id_customer_fk)
			$customer = \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk); 
		else
			$customer = false;
		return $customer;
    }
    
    public function getBranch() {
        return \backend\Modules\Crm\models\CustomerBranch::findOne($this->id_customer_branch_fk); 
    }
    
    public function getPersons() {
        $sql = "select group_concat(concat_ws(' ', lastname, firstname)) as contacts from{{%com_meeting_member}} mm join {{%customer_person}} cp on cp.id = mm.id_employee_fk "
                ." where mm.type_fk = 2 and mm.status = 1 and mm.id_meeting_fk = ".$this->id;
        return \Yii::$app->db->createCommand($sql)->queryOne();
    }
    
    public function getContacts() {
        $sql = "select group_concat(mm.id_employee_fk) as ids from{{%com_meeting_member}} mm "
                ." where mm.type_fk = 2 and mm.status = 1 and mm.id_meeting_fk = ".$this->id;
        return \Yii::$app->db->createCommand($sql)->queryOne();
    }
    
    public function getProject() {
        return \backend\Modules\Task\models\CalCase::findOne($this->id_resource_fk); 
    }
    
    public function getNotes() {
        $notesData = \backend\Modules\Task\models\Note::find()->where(['status' => 1, 'id_case_fk' => $this->id, 'id_type_fk' => 4])->orderby('id desc')->all();
        return $notesData;
    }
	
	public static function getTypes() {
		return [1 => 'wewnętrzne', 2 => 'z klientem'];
	}
    
    public function getDict() {
        return \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_meeting_type_fk);
    }
}
