<?php

namespace frontend\Modules\Community\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\base\Model;

use backend\Modules\Community\models\ComMessage;
use backend\Modules\Community\models\ComMessageCustomer;
use backend\Modules\Community\models\ComMessageEmployee;

/**
 * GroupAction is the model behind the contact form.
 */
class GroupAction extends Model
{
    public $env;
    public $ids; 
    public $customers_list;
    public $employees_list;
	public $user_action;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ids'], 'required'],
            ['customers_list', 'required', 'when' => function($model) { return ($model->user_action == 'addCustomers'); }, 'message' => 'Proszę wybrać konta klienta' ],
			['employees_list', 'required', 'when' => function($model) { return ($model->user_action == 'addEmployees' || $model->user_action == 'delEmployees'); }, 'message' => 'Proszę wybrać pracowników' ],
            //[['hours_of_day', 'id_employee_fk'], 'integer'],
            [['ids', 'env', 'customers_list', 'employees_list'], 'safe'],
            //['date_to','validateDates'],
        ];
    }
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Proszę podać poprawną datę końca');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids' => 'Wiadomości',
            'customers_list' => 'Konta klienta',
            'employees_list' => 'Pracownicy'
        ];
    }

    public function addEmployees() {            
        $employeesAll = implode(',', $this->employees_list);
        if($managers['managers']) {
            $employeesAll .= ', '.$managers['managers'];
        }
        /* employees */
        $sql = "select c.id as cid,c.name, e.id as eid, e.lastname "
				." from {{%cal_case}} c join {{%company_employee}} e on c.id in (".$this->ids.") and e.id in(".$employeesAll.") "
				." where (c.id,e.id) not in (select id_case_fk,id_employee_fk from law_case_employee) order by c.id";
		$relData = \Yii::$app->db->createCommand($sql)->query();
        foreach($relData as $key => $value) {
			//array_push($cases[$value['cid']], $value['eid']);
			$sqlInsert = "insert into {{%case_employee}} (id_case_fk, id_employee_fk, status, created_at, created_by) "
			          ." values (".$value['cid'].", ".$value['eid'].", 1, now(), ".\Yii::$app->user->id.")";
		    \Yii::$app->db->createCommand($sqlInsert)->execute();
		}    
		
		/*foreach($cases as $key => $value) {
			$modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $key;
			$modelArch->user_action = 'addEmployee';
			$modelArch->data_change = \yii\helpers\Json::encode($this);
			$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
		}*/
		
        return true;
    }
	
	
	public function delMessages() {            
        //$cases = \Yii::$app->db->createCommand("SELECT group_concat(id) as ids FROM {{%cal_case}} where id_dict_case_status_fk < 4 and status = 1 and id in (".$this->ids.")")->queryOne();

		if($this->env == 'customer') {
			$sqlUpdateIn = "update {{%com_message_customer}} set is_bin = 1, deleted_at = now(), deleted_by = ".\Yii::$app->user->id
					  ." where status = 1 and is_bin = 0 and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateIn)->execute();  

            $sqlUpdateOut = "update {{%com_message_customer}} set is_bin = 2, deleted_at = now(), deleted_by = ".\Yii::$app->user->id
					  ." where (status = 2 or is_bin = 1) and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateOut)->execute();  
		}
        
        if($this->env == 'employee') {
			$sqlUpdateIn = "update {{%com_message_employee}} set is_bin = 1, deleted_at = now(), deleted_by = ".\Yii::$app->user->id
					  ." where status = 1 and is_bin = 0 and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateIn)->execute();  

            $sqlUpdateOut = "update {{%com_message_employee}} set is_bin = 2, deleted_at = now(), deleted_by = ".\Yii::$app->user->id
					  ." where (status = 2 or is_bin = 1) and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateOut)->execute();  
		}

        return true;
    }
    
    public function starredMessages($flag) {
        if($this->env == 'customer') {
			$sqlUpdateIn = "update {{%com_message_customer}} set is_starred = ".$flag
					  ." where status = 1 and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateIn)->execute();   
		}
        
        if($this->env == 'employee') {
			$sqlUpdateIn = "update {{%com_message_employee}} set is_starred = ".$flag
					  ." where status = 1 and id in (".$this->ids.")";
			\Yii::$app->db->createCommand($sqlUpdateIn)->execute();  
		}

        return true;
    }
	
	public function archiveCases() {            
        $cases = \Yii::$app->db->createCommand("SELECT group_concat(id) as ids FROM {{%cal_case}} where id_dict_case_status_fk != 7 and status = 1 and id in (".$this->ids.")")->queryOne();
		
		foreach($cases as $key => $value) {
			$model = CalCase::findOne($value);
			$model->scenario = 'state';
			$model->user_action = 'archive';
			$model->id_dict_case_status_fk = 7;
			$model->save();
		}
		
        return true;
    }
}
