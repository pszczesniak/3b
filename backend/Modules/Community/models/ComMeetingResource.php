<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_meeting_resource}}".
 *
 * @property integer $id
 * @property integer $id_meeting_fk
 * @property integer $id_resource_fk
 * @property integer $id_resource_schedule_fk
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 */
class ComMeetingResource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_meeting_resource}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_meeting_fk', 'id_resource_fk', 'id_resource_schedule_fk'], 'required'],
            [['id_meeting_fk', 'id_resource_fk', 'id_resource_schedule_fk', 'status', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_meeting_fk' => Yii::t('app', 'Id Meeting Fk'),
            'id_resource_fk' => Yii::t('app', 'Id Resource Fk'),
            'id_resource_schedule_fk' => Yii::t('app', 'Id Resource Schedule Fk'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function beforeSave($insert) {      
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} /*else { 
				$this->updated_by = \Yii::$app->user->id;   
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getMeeting()
    {
		return $this->hasOne(\backend\Modules\Community\models\ComMeeting::className(), ['id' => 'id_meeting_fk']);
    }
    
    public function getResource()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyResources::className(), ['id' => 'id_resource_fk']);
    }
    
    public function getSchedule()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyResourcesSchedule::className(), ['id' => 'id_resource_schedule_fk']);
    }
}
