<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_member}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_group_fk
 * @property integer $id_user_fk
 * @property integer $id_fk
 * @property string $custom_data
 * @property string $created_at
 * @property integer $created_by
 */
class ComMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_group_fk', 'id_user_fk', 'id_fk', 'created_by', 'deleted_by', 'status'], 'integer'],
            [['id_group_fk', 'id_user_fk', 'id_fk'], 'required'],
            [['custom_data'], 'string'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_group_fk' => Yii::t('app', 'Id Group Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_fk']);
    }
}
