<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_group}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_owner_fk
 * @property string $name
 * @property integer $id_company_branch_fk
 * @property integer $id_company_department_fk
 * @property string $description
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ComGroup extends \yii\db\ActiveRecord
{
    public $employees_list = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_owner_fk', 'id_company_branch_fk', 'id_company_department_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['description', 'custom_data'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'employees_list'], 'safe'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Typ'),
            'id_owner_fk' => Yii::t('app', 'Id Owner Fk'),
            'name' => Yii::t('app', 'Name'),
            'id_company_branch_fk' => Yii::t('app', 'Id Company Branch Fk'),
            'id_company_department_fk' => Yii::t('app', 'Id Company Department Fk'),
            'description' => Yii::t('app', 'Description'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'employees_list' => 'Uczestnicy'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            if($this->employees_list) {
                foreach($this->employees_list as $key => $value) {
                    $member = new ComMember();
                    $member->id_group_fk = $this->id;
                    $member->type_fk = 1;
                    $member->id_user_fk = $value;
                    $member->id_fk = $value;
                    $member->status = 1;
                    $member->created_by = \Yii::$app->user->id;
                    $member->created_at = new Expression('NOW()');
                    $member->save();
                }
            }
        } 

		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getMembers()
    {
		$items = $this->hasMany(\backend\Modules\Community\models\ComMember::className(), ['id_group_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
    
    public static function getList() {
        $groups = [];
        
        $groups = \backend\Modules\Community\models\ComGroup::find()->orderby('name')->all();
        
        return $groups;
    }
    
    public static function listTypes() {
        
        $types = [];
        $types[1] = 'Otwarta';
        $types[2] = 'Administrowana';
        
        return $types;
    }
    
    public function getType() {
        return self::listTypes()[$this->type_fk];
    }
}
