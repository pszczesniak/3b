<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%com_post_member}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_post_fk
 * @property integer $id_user_fk
 * @property integer $id_fk
 * @property integer $id_member_fk
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $read_at
 */
class ComPostMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_post_member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_post_fk', 'id_user_fk', 'id_fk', 'id_member_fk', 'status', 'created_by'], 'integer'],
            [['id_post_fk', 'id_user_fk', 'id_fk', 'id_member_fk'], 'required'],
            [['created_at', 'read_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_post_fk' => Yii::t('app', 'Id Post Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'id_member_fk' => Yii::t('app', 'Id Member Fk'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'read_at' => Yii::t('app', 'Read At'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
