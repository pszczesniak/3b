<?php

namespace backend\Modules\Community\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Community\models\ComMember;
use backend\Modules\Community\models\ComPostMember;

/**
 * This is the model class for table "{{%com_topic}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $name
 * @property integer $id_group_fk
 * @property integer $id_company_branch_fk
 * @property integer $id_company_department_fk
 * @property string $description
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ComTopic extends \yii\db\ActiveRecord
{
    public $note;
    public $lastPost = false;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_topic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_group_fk', 'id_company_branch_fk', 'id_company_department_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'note'], 'required'],
            [['description', 'custom_data', 'note'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'note'], 'safe'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'name' => Yii::t('app', 'Name'),
            'id_group_fk' => Yii::t('app', 'Grupa'),
            'id_company_branch_fk' => Yii::t('app', 'Id Company Branch Fk'),
            'id_company_department_fk' => Yii::t('app', 'Id Company Department Fk'),
            'description' => Yii::t('app', 'Description'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'note' => 'Twój pierwszy wpis'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
                if($this->id_group_fk) {
                    $this->status = 2;
                } else {
                    $this->status = 1;
                }
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
                $post = new ComPost();
                $post->id_topic_fk = $this->id;
                $post->note = $this->note;
                $post->id_user_fk = \Yii::$app->user->id;
                $post->id_fk = \Yii::$app->user->id;
  
                $post->save();
                $this->lastPost = $post;
        } 

		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getPosts()
    {
		$items = $this->hasMany(\backend\Modules\Community\models\ComPost::className(), ['id_topic_fk' => 'id'])->/*where(['status' => 1])->*/orderby('created_at desc'); 
		return $items;
    }
    
    public function getGroup()
    {
		return $this->hasOne(\backend\Modules\Community\models\ComGroup::className(), ['id' => 'id_group_fk']);
    }
}
