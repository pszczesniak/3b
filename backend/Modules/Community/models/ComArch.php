<?php

namespace app\Modules\Community\models;

use Yii;

/**
 * This is the model class for table "{{%com_arch}}".
 *
 * @property integer $id
 * @property integer $table_fk
 * @property integer $id_root_fk
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class ComArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%com_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_fk', 'id_root_fk'], 'required'],
            [['table_fk', 'id_root_fk', 'created_by'], 'integer'],
            [['data_arch', 'user_action'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_fk' => Yii::t('app', 'Table Fk'),
            'id_root_fk' => Yii::t('app', 'Id Root Fk'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
