<?php

namespace backend\Modules\Folder\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%file_users}}".
 *
 * @property integer $id
 * @property integer $id_folder_fk
 * @property integer $id_user_fk
 * @property string $grants
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class FileUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_file_fk', 'id_user_fk'], 'required'],
            [['id_file_fk', 'id_user_fk', 'is_starred', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'], 
            [['created_at', 'updated_at', 'deleted_at', 'grants', 'alerts'], 'safe'],
            [['grants', 'alerts'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_file_fk' => Yii::t('app', 'Id Folder Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'grants' => Yii::t('app', 'Grants'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
   /* public function afterSave($insert, $changedAttributes) {

		parent::afterSave($insert, $changedAttributes);
	}*/
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
