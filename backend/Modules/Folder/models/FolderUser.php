<?php

namespace backend\Modules\Folder\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Folder\models\Folder;

/**
 * This is the model class for table "{{%folder_users}}".
 *
 * @property integer $id
 * @property integer $id_folder_fk
 * @property integer $id_user_fk
 * @property string $grants
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class FolderUser extends \yii\db\ActiveRecord
{
    public $user_action;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_folder_fk'], 'required'],
            [['id_user_fk'], 'required', 'when' => function($model) { return ($model->user_action != 'addGroup'); } ],
            [['id_group_fk'], 'required', 'when' => function($model) { return ($model->user_action == 'addGroup'); } ],
            [['id_folder_fk', 'id_user_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'is_starred', 'is_admin', 'is_delete', 'is_upload', 'is_download'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'grants', 'alerts', 'is_starred', 'is_admin', 'is_delete', 'is_upload', 'is_download'], 'safe'],
            [['grants'], 'string', 'max' => 1000],
            ['id_user_fk', 'checkExistMember', 'when' => function($model) { return ($model->status >= 1 && $model->user_action != 'starred'); } ],
            //['id_group_fk', 'checkExistGroup', 'when' => function($model) { return ($model->status >= 1); } ],
        ];
    }
    
    public function checkExistMember(){
        $sql = "select count(*) as isExist from {{%folder_users}} where status >= 1 and id_folder_fk = ".$this->id_folder_fk." and id_user_fk = ".$this->id_user_fk;
        $exist = Yii::$app->db->createCommand($sql)->queryOne();
        if($exist['isExist'] > 0)
            $this->addError('id_user_fk', 'Użytkownik jest już przypisany do katalogu.');
	}
    
    public function checkExistGroup(){
        $sql = "select count(*) as isExist from {{%folder_users}} where status >= 1 and id_folder_fk = ".$this->id_folder_fk." and id_group_fk = ".$this->id_group_fk;
        $exist = Yii::$app->db->createCommand($sql)->queryOne();
        if($exist['isExist'] > 0)
            $this->addError('id_user_fk', 'Grupa jest już przypisana do katalogu.');
	}


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_folder_fk' => Yii::t('app', 'Id Folder Fk'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'grants' => Yii::t('app', 'Grants'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
        
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getUser() {
        return \common\models\User::findOne($this->id_user_fk);
    }
    
    public function getFolder() {
        return \api\modules\folder\models\Folder::findOne($this->id_folder_fk);
    }
    
    public function getGroup() {
        return \api\modules\admin\models\AuthGroup::findOne($this->id_group_fk);
    }
}
