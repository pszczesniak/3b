<?php

namespace backend\Modules\Folder\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Community\models\Note;
use backend\Modules\Folder\models\FolderUser;
use backend\Modules\Folder\models\FileUser;

use common\models\Logs;

/**
 * This is the model class for table "{{%folder}}".
 *
 * @property integer $id
 * @property integer $id_parent_fk
 * @property integer $item_type
 * @property string $item_name
 * @property string $item_description
 * @property integer $lvl
 * @property integer $lft
 * @property integer $rgt
 * @property integer $pos
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Folder extends \yii\db\ActiveRecord
{
    public $attachs;
    public $groups;
    public $users;
    
    public $user_action;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name'], 'required'],
            [['id_parent_fk', 'item_type', 'lvl', 'lft', 'rgt', 'pos', 'status', 'created_by', 'updated_by', 'deleted_by', 'inherit'], 'integer'],
            [['item_description'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'attachs', 'groups', 'users'], 'safe'],
            [['item_name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'item_type' => Yii::t('app', 'Item Type'),
            'item_name' => Yii::t('app', 'Name'),
            'item_description' => Yii::t('app', 'Description'),
            'lvl' => Yii::t('app', 'Lvl'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'pos' => Yii::t('app', 'Pos'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'inherit' => Yii::t('app', 'Uprawnienia kaskadowo'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
                
                /*$modelArch = new CompanyResourcesScheduleArch(); 
				$modelArch->id_parent_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this->oldAttributes);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if($insert) {
            $newShareUser = new FolderUser();
            $newShareUser->id_folder_fk = $this->id;
            $newShareUser->id_user_fk = \Yii::$app->user->id;
            $newShareUser->status = 2;
            $newShareUser->is_admin = 1;
            $newShareUser->is_delete = 1;
            $newShareUser->is_upload = 1;
            $newShareUser->is_download = 1;
            $newShareUser->save();
            
            if($this->users) {
                $insertUsers = "insert into {{%folder_users}} (id_folder_fk,id_user_fk,id_group_fk,is_admin,is_upload,is_download,is_delete,status,created_at,created_by) "
                               ." select ".$this->id." as id_folder_fk, id, 0 as id_group_fk, 0 as is_admin, 1 as is_upload, 1 as is_download, 0 as is_delete, 1 as status, NOW() as created_at, ".Yii::$app->user->id." as created_by "
                                ." from {{%user}} where id != ".\Yii::$app->user->id." and id in (".implode(',', $this->users).") and id not in (select id_user_fk from {{%folder_users}} where status = 1 and id_folder_fk = ".$this->id.")";
                \Yii::$app->db->createCommand($insertUsers)->execute();
            }   
            /*if($this->groups) {
                $insertGroups = "insert into {{%folder_users}} (id_folder_fk,id_user_fk,id_group_fk,is_admin,is_upload,is_download,is_delete,status,created_at,created_by) "
                               ." select ".$this->id." as id_folder_fk, id_user_fk, id_group_fk, 0 as is_admin, 0 as is_upload, 0 as is_download, 0 as is_delete, 1 as status, NOW() as created_at, ".Yii::$app->user->id." as created_by "
                                ." from {{%auth_group_users}} where id_group_fk in (".implode(',', $this->groups).")";
                \Yii::$app->db->createCommand($insertGroups)->execute();
            }*/
            
            $request = Yii::$app->request;
            $log = new Logs();
            $log->table_parent_id = $this->id;
            $log->id_user_fk = Yii::$app->user->id;
            $log->action_date = date('Y-m-d H:i:s');
            $log->action_name = 'folder_create';
            $log->request_remote_addr = $request->getUserIP();
            $log->request_user_agent = $request->getUserAgent(); 
            $log->request_content_type = $request->getContentType(); 
            $log->request_url = $request->getUrl();
            $log->save();
        }
        
        if ($this->attachs) {
            $filesIds = [];
            foreach($this->attachs as $key => $file) {
                if($file) {
                    $modelFile = new \backend\Modules\folder\models\File(['scenario' => 'save']);
                    $modelFile->extension_file = $file->extension;
                    $modelFile->size_file = $file->size;
                    $modelFile->mime_file = $file->type;
                    $modelFile->name_file = $file->baseName;
                    $modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
                    $modelFile->title_file = $file->baseName.'.'.$file->extension;
                    $modelFile->id_folder_fk = $this->id;
                    $modelFile->id_type_file_fk = 1;
                    $modelFile->id_dict_type_file_fk = 0;
                    
                    if($modelFile->save()) {
                        array_push($filesIds, $modelFile->id);
                        /*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
                        try {
                            //$file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
							$file->saveAs('uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                        } catch (\Exception $e) {
                            var_dump($e); exit;
                        }
                    } else {
						var_dump($modelFile); exit;
					}
                }
            }
            /*if(count($filesIds) > 0) {
                $sqlInsertFilesLogs
            }*/
		}
		
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => null,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getNotes() {
		$sql = "select n.id, n.note, n.created_at, concat_ws(' ', firstname, lastname) as creator "
					." from {{%com_note}} n join {{%user}} u on u.id = n.created_by where id_type_fk = 1 and id_folder_fk = ".$this->id
					." order by created_at desc";
		
		return Yii::$app->db->createCommand($sql)->queryAll();	
	}
    
    public static function publicNotes() {
		$sql = "select n.id, n.note, n.created_at, concat_ws(' ', firstname, lastname) as creator "
					." from {{%com_note}} n join {{%user}} u on u.id = n.created_by where id_type_fk = 1 and id_folder_fk = 0"
					." order by created_at desc";
		
		return Yii::$app->db->createCommand($sql)->queryAll();	
	}
    
    public function getMembers() {
		$sql = "select fu.id_user_fk, concat_ws(' ', firstname, lastname) as fullname, max(fu.status) as status, max(fu.is_admin) is_admin, max(fu.is_delete) as is_delete, max(fu.is_upload) is_upload, max(fu.is_download) is_download, min(ifnull(id_group_fk,0)) as id_group_fk "
					." from {{%folder_users}} fu join {{%user}} u on fu.status >= 1 and u.id = fu.id_user_fk where id_folder_fk = ".$this->id
                    ." group by fu.id_user_fk, concat_ws(' ', firstname, lastname) "
					." order by fu.status desc, lastname, firstname";
		
		return Yii::$app->db->createCommand($sql)->queryAll();	
	}
    
    public function getTeams() {
		$sql = "select fu.id_group_fk, name "
					." from {{%folder_users}} fu join {{%auth_group}} u on fu.status = 1 and u.id = fu.id_group_fk where id_folder_fk = ".$this->id
                    ." group by fu.id_group_fk, name "
					." order by name";
		
		return Yii::$app->db->createCommand($sql)->queryAll();	
	}
    
    public function getParent() {
        return Folder::findOne($this->id_parent_fk);
    }
    
    public function getPrm() {
        return FolderUser::find()->where(['id_user_fk' => Yii::$app->user->id, 'id_folder_fk' => $this->id])->andWhere('status >= 1')->one();
    }
    
    public function getFullpath() {
        $fullPath = Yii::$app->db->createCommand('select fullpath('.$this->id.') as folderPath')->queryOne()['folderPath'];
        
        $folderPathArr = [];
        $dataPathArr = explode(' / ', $fullPath);
        foreach($dataPathArr as $key => $item) {
            $itemArr = explode('-', $item);
            array_push($folderPathArr, $itemArr[1]);
        }
            
        return implode(' / ', $folderPathArr);
    }
}
