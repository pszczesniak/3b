<?php

namespace backend\modules\folder\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

use backend\Modules\Folder\models\FileUser;

use common\models\Logs;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property integer $id
 * @property integer $id_type_file_fk
 * @property integer $id_original_fk
 * @property integer $id_parent_fk
 * @property integer $id_fk
 * @property integer $id_dict_type_file_fk
 * @property string $tags
 * @property string $title_file
 * @property string $name_file
 * @property string $systemname_file
 * @property resource $content_file
 * @property string $extension_file
 * @property string $mime_file
 * @property string $size_file
 * @property string $describe_file
 * @property string $mod_reason
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class File extends \yii\db\ActiveRecord
{
    const SCENARIO_SAVE = 'save';
    const SCENARIO_UPLOAD = 'upload';
	const SCENARIO_UPDATE = 'update';
    const SCENARIO_CHANGE = 'change';
    const SCENARIO_REPLACE = 'replace';
    
    public $user_action;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }
    
	public function scenarios()
    {
        return [
            self::SCENARIO_SAVE => ['id_fk', 'id_type_file_fk', 'id_dict_type_file_fk', 'title_file', 'name_file', 'systemname_file', 'extension_file', 'size_file', 'type_fk', 'show_client'],
            self::SCENARIO_UPLOAD => ['id_fk', 'id_type_file_fk', 'id_dict_type_file_fk', 'title_file', 'files', 'type_fk', 'show_client'],
			self::SCENARIO_UPDATE => ['version_fk'],
            self::SCENARIO_CHANGE => ['name_file', 'title_file', 'describe_file', 'id_dict_type_file_fk', 'show_client'],
            self::SCENARIO_REPLACE => ['id_parent_fk', 'title_file', 'describe_file', 'id_dict_type_file_fk', 'name_file', 'systemname_file', 'extension_file', 'size_file', 'type_fk'],
			'default' => array('id'),
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_type_file_fk', 'id_original_fk', 'id_parent_fk', 'id_fk', 'id_dict_type_file_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['content_file', 'describe_file'], 'string'],
            [['size_file'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['tags', 'title_file', 'name_file'], 'string', 'max' => 2000],
            [['systemname_file', 'extension_file', 'mime_file', 'mod_reason'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_type_file_fk' => Yii::t('app', 'Id Type File Fk'),
            'id_original_fk' => Yii::t('app', 'Id Original Fk'),
            'id_parent_fk' => Yii::t('app', 'Id Parent Fk'),
            'id_fk' => Yii::t('app', 'Id Fk'),
            'id_dict_type_file_fk' => Yii::t('app', 'Id Dict Type File Fk'),
            'tags' => Yii::t('app', 'Tags'),
            'title_file' => Yii::t('app', 'Title File'),
            'name_file' => Yii::t('app', 'Name File'),
            'systemname_file' => Yii::t('app', 'Systemname File'),
            'content_file' => Yii::t('app', 'Content File'),
            'extension_file' => Yii::t('app', 'Extension File'),
            'mime_file' => Yii::t('app', 'Mime File'),
            'size_file' => Yii::t('app', 'Size File'),
            'describe_file' => Yii::t('app', 'Describe File'),
            'mod_reason' => Yii::t('app', 'Mod Reason'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
 
        if($insert) {
            $newShareUser = new FileUser(); 
            $newShareUser->id_file_fk = $this->id;
            $newShareUser->id_user_fk = \Yii::$app->user->id;
            $newShareUser->save();
        }
        
        parent::afterSave($insert, $changedAttributes);
	}
	
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function upload() {
        if ($this->validate()) { 
            foreach ($this->files as $file) {
                $modelFile = new File(['scenario' => self::SCENARIO_SAVE]);
				$modelFile->extension_file = $file->extension;
				$modelFile->size_file = $file->size;
				$modelFile->mime_file = $file->type;
				$modelFile->name_file = $file->baseName;
				$modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
				$modelFile->title_file = (count($this->files) == 1) ? $this->title_file : $file->baseName.'.'.$file->extension;
				$modelFile->id_fk = $this->id_fk;
				$modelFile->id_type_file_fk = $this->id_type_file_fk;
                $modelFile->id_dict_type_file_fk = $this->id_dict_type_file_fk;
                $modelFile->type_fk = $this->type_fk;
                
				if($modelFile->save()) {
					/*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
					try {
					    $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
						//$this->files_list .= '<li><a href="/files/getfile/'.$modelFile->id.'">'.$modelFile->title_file .'</a> [nowy] </li>';
						array_push($this->files_ids,$modelFile->id);

						$this->id = $modelFile->id;
					} catch (\Exception $e) {
						var_dump($e); exit;
					}
				} else {
					var_dump($modelFile->getErrors());
				}			
				
            }
            return true;
        } else {
            return false;
        }
    }
	
	/*public function getUser() {
        return \common\models\User::findOne($this->created_by)->fullname;
    }*/
}
