<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopTax */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Shop Tax',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Taxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="shop-tax-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
