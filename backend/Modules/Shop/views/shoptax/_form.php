<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopTax */
/* @var $form yii\widgets\ActiveForm */
?>
 

<div class="shop-tax-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'rate_tax')->textInput() ?>

        <?= $form->field($model, 'describe')->textarea(['rows' => 6]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
