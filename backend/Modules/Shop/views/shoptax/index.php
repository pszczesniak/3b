<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\Modules\Shop\models\ShopTaxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Słownik podatków';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"shop-tax-index", 'title'=>Html::encode($this->title))) ?>

    <div id="toolbar" class="btn-group">
			<?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Svc Event'), ]), ['#'], ['class' => 'btn btn-success btn-icon', 'data-toggle' => "modal", 'data-target' => "#modal-grid-view-dict", 'data-title' => '<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'New')]) ?>
			<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
		</div>
		<?= GridView::widget([
			'id' => 'grid-view-dicts',
			'dataProvider' => $dataProvider,
			'tableOptions'=>Yii::$app->params['tableOptions'],
			//'summary' => "<div class='summary'>". Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.').Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Lsdd Material Function'), ]), ['create'], ['class' => 'btn btn-xs btn-success btn-icon'])."</div>" ,
			'filterModel' => $searchModel,
			//'emptyText' => Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Lsdd Recipe Group'), ]), ['create'], ['class' => 'btn btn-success btn-icon']),
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
            'name',
            //'name_lang:ntext',
            'rate_tax',
           // 'describe:ntext',
            // 'config',
            // 'status',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'deleted_at',
            // 'deleted_by',

            [
					'class' => 'yii\grid\ActionColumn',
					'contentOptions' => ['class' => 'table-actions'],
					'template' => '{update}{delete}',
					'headerOptions' => ['data-switchable'=>'false'],
					'buttons' => [
						'view' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
									'title' => Yii::t('app', 'View'), 'class' => 'btn btn-default btn-sm gridViewDictModal',
									'data-toggle' => "modal", 'data-target' => "#modal-grid-view-dict", 'data-id' => $model->id, 'data-title' => '<i class="glyphicon glyphicon-eye-open"></i>'.Yii::t('lsdd', 'View')
							]);
						},
						'update' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
									'title' => Yii::t('app', 'Update'), 'class' => 'btn btn-default btn-sm gridViewDictModal',
									'data-toggle' => "modal", 'data-target' => "#modal-grid-view-dict", 'data-id' => $model->id, 'data-title' => '<i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Update')
							]);
						},
						'delete' => function ($url, $model) {
							return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
									'title' => Yii::t('app', 'Remove'), 'class' => 'btn btn-default btn-sm deleteConfirm',
									 'data-confirm' => "Czy na pewno usunąć ten element?", 'aria-label' => "Usuń"
							]);
						}
					],
				],
			],
		]); ?>
	<?php /* \yii\widgets\Pjax::end(); */ ?>

<?= $this->endContent(); ?>

<!-- Render update and view form -->
<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'New').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modal-grid-view-dict', // <-- insert this modal's ID
	  'class' => 'modal-grid-view-dict'
	]);
 
	echo '<div class="modalContent">'.$this->render('_form', [ 'model' => $model, ]).'</div>';

	yii\bootstrap\Modal::end();
?> 

<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modalConfirm', // <-- insert this modal's ID
	]);
 
	echo ' <div class="modal-body"><p>Czy na pewno chcesz usunąć ten element?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

	yii\bootstrap\Modal::end();
?> 