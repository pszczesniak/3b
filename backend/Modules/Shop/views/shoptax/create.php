<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopTax */

$this->title = Yii::t('app', 'Create Shop Tax');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Taxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-tax-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
