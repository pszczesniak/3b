<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategory */

$this->title = Yii::t('app', 'Edycja kategorii: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr kategorii'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Aktualizacja');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"shop-category-index", 'title'=>Html::encode($this->title))) ?>

    <div id="validation-form" class="alert"></div>
    <?php 
    
		$basicForm = yii\base\View::render('_form',array('model'=>$model));
		$fieldsForm = yii\base\View::render('_fields',array('model'=>$model));
        $seoForm = yii\base\View::render('_tabSeo',array('model'=>$model));
        $translateForm = yii\base\View::render('_tabTranslate',array('category' => $model, 'model'=>$model->getTranslate($model->id, 'de')));
		
		echo yii\bootstrap\Tabs::widget([
            'options'=> [
                    //'collapsible'=>true,
                ],

            'items' => [
                [
                    'label' => 'Podstawowe',
                    'content' => $basicForm, 
                    //'url' => ['ajax/content'],
                ],
                [
                    'label' => 'Parametry produktów',
                    'content' => $fieldsForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                [
                    'label' => 'SEO',
                    'content' => $seoForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                [
                    'label' => 'Tłumaczenia',
                    'content' => $translateForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                
            ],
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div'],
            'headerOptions' => ['class' => 'my-class'],
            'clientOptions' => ['collapsible' => false],
        ]);
    ?>
    

<?= $this->endContent(); ?>

<!-- Render modal form -->
<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modal-grid-item', // <-- insert this modal's ID
	  'class' => 'modal-grid',
	  'size' => 'modal-lg',
	]);
 
	echo '<div class="modalContent"></div>';

	yii\bootstrap\Modal::end();
?>
