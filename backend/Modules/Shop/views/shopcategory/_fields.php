<?php

use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\Relateditemswidget;

?>

<div class="ajax-table-items">	
   
    <?= Relateditemswidget::widget(['createUrl' => Url::to(['/shop/shopcategoryfactor/createajax', 'id' => $model->id]),
                            'createLabel' => 'Dodaj parametr',
                            'createIcon' => 'plus',
                            'createStyle' => 'success',
                            //'buttonId' => 'production-status',
                            'dataUrl' =>  Url::to(['/shop/shopcategory/fields', 'id'=>$model->id]),
                            'tableId' => "table-items",
                            'modalId' => "modal-grid-item",
                            'gridViewModal' => 'gridViewModal',
                            'tableOptions' => 'data-toggle="table" data-show-refresh="true" data-show-toggle="true"  data-show-columns="true" data-show-export="true"  data-filter-control="true" ',
                            'otherButtons' => '<a class="btn btn-primary btn-icon none" id="save-table-items-move" href="'.Url::to(['/shop/shopcategoryfactor/save', 'id' => $model->id]).'"><i class="glyphicon glyphicon-floppy-disk"></i>Zapisz zmiany</a>',
                            'columns' => [
                                0 => ['name' => Yii::t('lsdd', 'ID'), 'data' => 'data-field="id" data-visible="false"' ],
                                1 => ['name' => Yii::t('app', 'Name'), 'data' => 'data-field="name"' ],
								2 => ['name' => Yii::t('app', 'Value Type'), 'data' => 'data-field="value_type"' ],
                                3 => ['name' => '', 'data' => 'data-field="action" data-align="center"' ],
                                4 => ['name' => '', 'data' => 'data-field="move" data-align="center" data-events="actionEvents"' ],
                            ]   
                    ]) ?>
  
</div>
