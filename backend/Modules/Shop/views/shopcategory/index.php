<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\BaseStringHelper;


/* @var $this yii\web\View */
/* @var $searchModel backend\Modules\Shop\models\ShopCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'zarządzanie kategoriami';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"shop-category-index", 'title'=>Html::encode($this->title))) ?>

     <div id="toolbar" class="btn-group">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Shop Category'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
    </div>
    <?= GridView::widget([
        'id' => 'grid-view-dicts',
        'dataProvider' => $dataProvider,
        'tableOptions'=>Yii::$app->params['tableOptions'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'name_lang:ntext',
            //'describe:ntext',
			[
				'format' => 'html',
				'label' => 'Description',
				'value' => function ($dataProvider) {
					$url = $dataProvider->describe;
					return BaseStringHelper::truncateWords($url,10,'[...]',false);
				},
			],
            // 'config',
            // 'click',
            // 'slug',
            // 'status',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'deleted_at',
            // 'deleted_by',

           [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'table-actions'],
                'template' => '{view}{update}{delete}',
                'headerOptions' => ['data-switchable'=>'false'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', \Yii::$app->urlManagerFrontEnd->createUrl('pl/sklep/'.$model->id.'-'.$model->slug), [
                                'title' => Yii::t('app', 'View'), 'class' => 'btn btn-default btn-sm ', 'target' => '_blank'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
                                'title' => Yii::t('app', 'Update'), 'class' => 'btn btn-default btn-sm ', 'data-title' => '<i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Update')
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                'title' => Yii::t('app', 'Remove'), 'class' => 'btn btn-default btn-sm deleteConfirm',
                                'data-confirm' => "Czy na pewno usunąć tą ofertę?", 'aria-label' => "Usuń", "data-id" => $model->id
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?= $this->endContent(); ?>
<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modalConfirm', // <-- insert this modal's ID
	]);
 
	echo ' <div class="modal-body"><p>Czy na pewno chcesz usunąć ten element?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

	yii\bootstrap\Modal::end();
?> 