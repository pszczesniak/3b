<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


		<div class="row">
			<div class="col-sm-8">
				<?php $form = ActiveForm::begin([ 'action' => Url::to(['/translations/update', 'id' => $model->id]), 'id' => 'page-translate','options' => ['class' => 'ajaxform'] ]); ?>

				<div class="form-group field-lang">
					<label for="lang" class="control-label">Język</label>
					<select name="lang" class="form-control" id="lang">
						<option selected="" value="de">Niemiecki</option>
					</select>

					<div class="help-block"></div>
					</div>    
					<div class="form-group field-name">
						<label for="name" class="control-label">Tytuł</label>
						<input type="text" maxlength="255" value="<?= $model->attributes['name'] ?>" name="name" class="form-control" id="name">

						<div class="help-block"></div>
					</div>
					<div class="form-group field-describe">
						<label for="describe" class="control-label">Treść</label>
						<textarea  name="describe" class="form-control mce-full" id="describe"><?= $model->attributes['describe'] ?></textarea>

						<div class="help-block"></div>
					</div>
                    
                    <fieldset><legend>SEO</legend>
                        <div class="form-group field-meta_title">
                            <label for="meta_title" class="control-label">Meta title</label>
                            <input type="text" maxlength="255" value="<?= $model->attributes['meta_title'] ?>" name="meta_title" class="form-control" id="meta_title">

                            <div class="help-block"></div>
                        </div>
                        <div class="form-group field-meta_keywords">
                            <label for="meta_keywords" class="control-label">Meta keywors</label>
                            <input type="text" maxlength="255" value="<?= $model->attributes['meta_keywords'] ?>" name="meta_keywords" class="form-control" id="meta_keywords">

                            <div class="help-block"></div>
                        </div>
                        <div class="form-group field-meta_description">
                            <label for="meta_description" class="control-label">Meta description</label>
                            <textarea  name="meta_description" class="form-control" id="content"><?= $model->attributes['meta_description'] ?></textarea>

                            <div class="help-block"></div>
                        </div>
                    </fieldset>

				
					<div class="form-group align-right">
						<?= Html::submitButton('Zapisz zmiany', ['class' =>  'btn btn-primary']) ?>
					</div>
			    <?php ActiveForm::end(); ?>
			</div>
			<div class="col-sm-4">
				<?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                            [
                                'id' => 'imgAttachmentTranslate',
                                'model' => $category,
                                'behaviorName' => 'coverBehaviorDe',
                                'apiRoute' => '/shop/shopcategory/imgAttachApi',
                            ]
                        
                        )
                ?>
                
			</div>
		</div>
        

