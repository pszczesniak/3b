<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-form">

    <div class="row">
        <div class="col-sm-8">
             <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'status')->dropDownList( [0 => 'roboczy', 1 => 'opublikowany'], [/*'prompt'=>'wybierz kategoriÍ'*/] )  ?>

				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

                <?php echo  $form->field($model, 'describe')->textarea(['rows' => 6, 'class'=> 'mce-basic'])->label(false) ?>	



                <div class="form-group align-right">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-sm-4">
            <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                            [
                                'id' => 'imgAttachment',
                                'model' => $model,
                                'behaviorName' => 'coverBehavior',
                                'apiRoute' => '/shop/shopcategory/imgAttachApi',
                            ]
                        
                        )
                ?>
        </div>
    </div>
    
   

</div>
<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>