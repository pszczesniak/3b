<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */

$this->title = Yii::t('app', 'Create Shop Category Factor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Category Factors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-factor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
