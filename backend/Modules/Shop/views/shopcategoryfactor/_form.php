<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-factor-form">

    <?php $form = ActiveForm::begin(['id' => 'item-form-field', 
									'action' => ($model->isNewRecord)?Url::to(['/shop/shopcategoryfactor/create','id'=>$model->id_category_fk]):Url::to(['/shop/shopcategoryfactor/update','id'=>$model->id]),
									'options' => ['class' => 'modal-grid-field', 'data-pjax' => true, 'enableAjaxValidation'=>true, 'enableClientValidation'=>true, 'data-table' => '#table-items', 'data-target' => "#modal-grid-item"],
								 ]); ?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
        <div class="row">
            <div class="col-xs-4"><?= $form->field($model, 'value_type')->dropDownList( \backend\Modules\Shop\models\ShopCategoryFactor::getTypes(), array('class' => 'category-factor-value-type', 'prompt'=>'wybierz typ') )->label(false)  ?></div>
            <!--<div class="col-xs-8"><?= $form->field($model, 'is_require')->checkBox([/*'label' => ...,*/ 'uncheck' => null, 'checked' => true]) ?></div>          -->
        </div>

        <?= $form->field($model, 'options')->textarea(['rows' => 6, 'placeholder' => 'Wpisz wszyskie opcje wyboru używając \',\' jako separatora', 'class' => ($model->isNewRecord) ? 'hide' : 'display-block'])->label(false) ?>


        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
