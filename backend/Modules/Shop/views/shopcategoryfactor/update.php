<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Shop Category Factor',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Category Factors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="shop-category-factor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
