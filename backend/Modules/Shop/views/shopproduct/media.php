<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Multimedia: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rejestr wpisów', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Wróć do artykułu', 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-view", 'title'=>Html::encode($this->title))) ?>


    <p>
        <?= Html::a(Yii::t('app', 'Wróć do produktu'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <!--<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= $this->render('_media', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>