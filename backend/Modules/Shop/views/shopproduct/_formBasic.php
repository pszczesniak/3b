<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\Modules\Shop\models\ShopCategory;
use backend\Modules\Shop\models\ShopTax;
use backend\Modules\Shop\models\ShopUnit;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-product-form">

    <?php $form = ActiveForm::begin([ 'id' => 'product-basic', 'options' => ['class' => ($model->isNewRecord) ? 'createForm' : 'ajaxform'] ]); ?>
    
    <div class="row">
        <div class="col-sm-6">
            <fieldset><legend>Podstawowe informacje</legend>
				<?= $form->field($model, 'status')->dropDownList( [0 => 'roboczy', 1 => 'opublikowany'], [/*'prompt'=>'wybierz kategorię'*/] )  ?>

				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        
                <?= $form->field($model, 'subname')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'id_category_fk')->dropDownList( ArrayHelper::map(ShopCategory::find()->all(), 'id', 'name'), array('prompt'=>'wybierz kategorię') )  ?>
            </fieldset>
            
             <fieldset><legend>Skrócony opis</legend>
                <?php echo  $form->field($model, 'brief')->textarea(['rows' => 2, /*'class'=> 'mce-basic'*/])->label(false) ?>	
            </fieldset>
           
        </div>
        <div class="col-sm-6">
            <fieldset><legend>Konfiguracja ceny</legend>
                <div class="row">
                    <div class="col-xs-6"><?= $form->field($model, 'price')->textInput() ?></div>
                    <div class="col-xs-6"><?= $form->field($model, 'discount')->textInput() ?></div>
                </div>  
				<div class="row">
					<div class="col-xs-12">
						<?= $form->field($model, 'is_new')->checkBox([/*'label' => ...,*/ 'uncheck' => null, 'checked' => true]) ?>
						<?= $form->field($model, 'is_discount')->checkBox([/*'label' => ...,*/ 'uncheck' => null, 'checked' => true]) ?>
						<?= $form->field($model, 'is_featured')->checkBox([/*'label' => ...,*/ 'uncheck' => null, 'checked' => true]) ?>
					</div>
                </div>
            </fieldset>
            
            <fieldset><legend>Dodatkowa konfiguracja</legend>
				<div class="row">
					<div class="col-xs-6"><?= $form->field($model, 'id_unit_fk')->dropDownList( ArrayHelper::map(ShopUnit::find()->all(), 'id', 'name'), array('prompt'=>'-- wybierz --') )  ?></div>
					<div class="col-xs-6"><?= $form->field($model, 'id_tax_fk')->dropDownList( ArrayHelper::map(ShopTax::find()->all(), 'id', 'name'), array('prompt'=>'-- wybierz --') )  ?></div>
				</div>        
            </fieldset>
        </div>
    </div>

    
	<?php echo  $form->field($model, 'describe')->textarea(['rows' => 6, 'class'=> 'mce-full'])->label(false) ?>	


 

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Utwórz') : Yii::t('app', 'Zapisz zmiany'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>