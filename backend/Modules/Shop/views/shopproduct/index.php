<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Katalog produktów');
$this->params['breadcrumbs'][] = 'Magazyn';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-products" data-form="#filter-task-products-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel, 'type' => 'advanced']); ?>
        </div>
    </fieldset>
    
    <?= Alert::widget() ?>
	<div id="toolbar-products" class="btn-group toolbar-table-widget">
            <?= ( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/task/matter/create']) , 
					['class' => 'btn btn-success btn-icon ', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/task/matter/exportnew']) , 
					['class' => 'btn btn-info btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-task-products-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]):'' ?>
            <?php if(count(array_intersect(["caseExport", "grantAll"], $grants)) > 0) { ?>
                <div class="btn-group" id="actionsDates-btn">
                    <button type="button" class="btn btn-icon bg-navy" id="group"><i class="fa fa-cogs"></i>Operacje grupowe (<span id="group-counter">0</span>)</button>
                    <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/task/manage/addemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Dołącz pracowników"><i class="fa fa-user-plus text--blue"></i>&nbsp;dołącz pracowników</a>'
                            .'</li>' ?>
                        <?= '<li class="group-actions">'
                               . '<a href="'.Url::to(['/task/manage/delemployee']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Odłącz pracowników"><i class="fa fa-user-times text--red"></i>&nbsp;odłącz pracowników</a>'
                            .'</li>' ?>
                        <?= '<li class="group-actions">'
                           . '<a href="'.Url::to(['/task/manage/caseclose']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Zamknij sprawy"><i class="fa fa-check text--green"></i>&nbsp;zamknij sprawy</a>'
                        .'</li>' ?>
						<?= '<li class="group-actions">'
                           . '<a href="'.Url::to(['/task/manage/casearchive']).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Przenieś do archiwum"><i class="fa fa-archive text--purple"></i>&nbsp;przenieś do archiwum</a>'
                        .'</li>' ?>
                        <!--<li role="separator" class="divider"></li>-->
                    </ul>
                </div>
            <?php } ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-products"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-products"
                data-toolbar="#toolbar-products" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-task-products-search"
                data-url=<?= Url::to(['/shop/shopproduct/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
					<th data-field="state" data-visible="true" data-checkbox="true"></th>
                    <th data-field="sygn" data-sortable="true">Nazwa</th>
					<th data-field="label" data-sortable="true">Kod</th>
                    <th data-field="client" data-sortable="true" >Kategoria</th>
                    <th data-field="side" data-sortable="true" >Jednostka</th>
                    <th data-field="price" data-sortable="true" >Cena</th>
                    <th data-field="marge" data-sortable="true" >Marża</th>
					<th data-field="status"  data-sortable="true" data-align="center" ></th>
                    <th data-field="actions" data-events="actionEvents" data-width="50px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

<?php $this->endContent(); ?>

