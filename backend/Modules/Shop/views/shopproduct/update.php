<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopProduct */

$this->title = Yii::t('app', 'Aktualizacja produktu: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rejestr produktów', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Aktualizacja');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"shop-product-update", 'title'=>Html::encode($this->title))) ?>

    <div class="row">
        <div class="text-right">
            <?= Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Nowy produkt', ['modelClass' => 'Cms Gallery', ]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm btn-responsive']) ?>
            <?= Html::a('<i class="fa fa-image"></i>'.Yii::t('app', 'Galeria', ['modelClass' => 'Cms Gallery',]),['/cms/cmsgallery/images', 'id' => $model->id_gallery_fk], ['class' => 'btn btn-primary btn-icon btn-sm btn-responsive']) ?>
           <!-- <?= Html::a('<i class="fa fa-play-circle"></i>'.Yii::t('app', 'Multimedia', ['modelClass' => 'Blog Post', ]), ['media', 'id' => $model->id], ['class' => 'btn btn-warning btn-icon btn-sm btn-responsive']) ?>
           <?= Html::a('<i class="fa fa-trash-o"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-icon btn-sm btn-responsive',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>-->
            
        </div>
    </div>
    <?= $this->render('_form', [
        'model' => $model, 'dataForm' => $dataForm
    ]) ?>
    

<?= $this->endContent(); ?>