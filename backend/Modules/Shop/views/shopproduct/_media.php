<?php
    use frontend\widgets\Uploaderwidget;
?>                
<div class="row">
    <ul class="tab-bar grey-tab">
        <li class="active">
            <a data-toggle="tab" href="#video">
                <span class="block text-center">
                    <i class="fa fa-file-video-o fa-2x"></i> 
                </span>
                Video
            </a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#audio">
                <span class="block text-center">
                    <i class="fa fa-file-audio-o fa-2x"></i> 
                </span>	
                Audio
            </a>
        </li>
    </ul>
    <div class="tab-content">
       
        <div id="video" class="tab-pane fade active in">
           <?= Uploaderwidget::widget(['typeId' => 4, 'parentId' => $model->id, 'accept' => "video/*"]) ?>
        </div>
        <div id="audio" class="tab-pane">
           <?= Uploaderwidget::widget(['typeId' => 5, 'parentId' => $model->id, 'accept' => "audio/*"]) ?>
        </div>
    </div>
</div>    

        