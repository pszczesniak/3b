<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopProduct */

$this->title = Yii::t('app', 'Nowy produkt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr produktów'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"shop-product-create", 'title'=>Html::encode($this->title))) ?>


    <?= $this->render('_form', [
        'model' => $model, 'dataForm' => $dataForm
    ]) ?>

<?= $this->endContent(); ?>