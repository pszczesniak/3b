<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\Modules\Shop\models\ShopCategory;
use backend\Modules\Shop\models\ShopTax;
use backend\Modules\Shop\models\ShopUnit;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-product-form">

    <?php $form = ActiveForm::begin([ 'id' => 'product-params', 'options' => ['class' => 'ajaxform'] ]); ?>
    
    <div class="row">
        <div class="col-xs-12">
           
            <fieldset><legend>Parametry</legend>
                <?php 
                    if(isset($model->getErrors()['data']) && $errors = $model->getErrors()['data']) {
                        echo '<div class="alert alert-danger"><ul>';
                        foreach($errors as $key => $error) {
                            echo '<li>'.$errors[$key].'</li>';
                        }
                        echo '</ul></div>';
                    }
                ?>
				<?= $dataForm ?>      
            </fieldset>
        </div>
        <div class="col-sm-6">
           
        </div>
    </div>


    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>