<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\Modules\Shop\models\ShopCategory;
use backend\Modules\Shop\models\ShopTax;
use backend\Modules\Shop\models\ShopUnit;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-product-form">

    
    <div id="validation-form" class="alert"></div>
    <?php 
    
		$basicForm = yii\base\View::render('_formBasic',array('model'=>$model));
		$fieldsForm = yii\base\View::render('_formParams',array('model' => $model, 'dataForm'=>$dataForm));
        $docsForm = yii\base\View::render('_formDocs',array('model' => $model,));
        $seoForm = yii\base\View::render('_tabSeo',array('model'=>$model));
        $translateForm = (!$model->isNewRecord) ? yii\base\View::render('_formTranslate',array('model'=>$model->getTranslate($model->id, 'de'))) : '';
		
		echo yii\bootstrap\Tabs::widget([
            'options'=> [
                    //'collapsible'=>true,
                ],

            'items' => [
                [
                    'label' => 'Podstawowe',
                    'content' => $basicForm, 
                    //'url' => ['ajax/content'],
                ],
                [
                    'label' => 'Parametry',
                    'content' => $fieldsForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                [
                    'label' => 'Dokumenty',
                    'content' => $docsForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                [
                    'label' => 'SEO',
                    'content' => $seoForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                [
                    'label' => 'Tłumaczenia',
                    'content' => $translateForm,
                    'visible' =>  !$model->isNewRecord,		
                    //'active' => true	
                ],
                
            ],
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div'],
            'headerOptions' => ['class' => 'my-class'],
            'clientOptions' => ['collapsible' => false],
        ]);
    ?>
    
    
</div>
<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>