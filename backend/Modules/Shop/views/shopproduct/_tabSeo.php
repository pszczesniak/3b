<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

use backend\Modules\cms\models\CmsPageStatus;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([ 'id' => 'page-seo', 'options' => ['class' => 'ajaxform'] ]); ?>
    <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_seo'])->label(false) ?>
	<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-seo' : 'btn btn-primary main-form-btn btn-form-seo']) ?>
    </div>

    <?php ActiveForm::end(); ?>

	
	<?php
		/*$this->registerJs("
						  $('.btn-form-seo').click(function(){
								$.ajax({
										type: 'POST',
										data: $('form#page-seo').serializeArray(),
										success: function (data) {
											if(data['success']) {
												$('#alert-success').show(0).delay(2000).hide(0);
												
											} else {
												$('#alert-error').html(data['errors']['title']);
												$('#alert-error').show(0).delay(2000).hide(0);
											}
										},
										error: function (XMLHttpRequest, textStatus, errorThrown) {
										}
								});
							   return false;
						  });
						");*/
	?>



