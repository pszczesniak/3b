<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopUnit */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="shop-unit-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>
        <div class="row">
            <div class="col-xs-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-6"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?></div>
        </div>

        <?= $form->field($model, 'describe')->textarea(['rows' => 2]) ?>

        <?php $translate = $model->translate; if( count($translate) > 0) { 
                $items = [];
                foreach($translate as $key=>$value) { 
                    $content = '<div class="form-group field-shopunit-name-'.$key.' ">'
                        .'<label for="cmsmenuitem-name" class="control-label">Wersja '.$key.' </label>'
                        .'<input type="text" maxlength="255" name="Lang['. $key .']" class="form-control" id="titem-'. $model->id .'" value="'. $value .'">'
                    .'</div>';
                    array_push($items, ['label' => $key, 'content' => $content]);
                } 
                
                echo /*yii\jui\*/yii\bootstrap\Tabs::widget([
                    'options'=> [
                            //'collapsible'=>true,
                        ],

                    'items' => $items
                ]);
            } 
        ?>
        <div class="clear"></div>
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
