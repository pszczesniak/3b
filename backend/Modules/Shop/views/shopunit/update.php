<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopUnit */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Shop Unit',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="shop-unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
