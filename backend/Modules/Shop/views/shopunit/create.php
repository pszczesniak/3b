<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopUnit */

$this->title = Yii::t('app', 'Create Shop Unit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
