<?php

namespace app\Modules\Shop\controllers;

use Yii;
use frontend\Modules\Shop\models\ShopProduct;
use frontend\Modules\Shop\models\ShopProductSearch;
use frontend\Modules\Shop\models\ShopProductFactor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Response;
use yii\filters\AccessControl;

use frontend\Modules\Cms\CmsGallery;

/**
 * ShopproductController implements the CRUD actions for ShopProduct model.
 */
class ShopproductController extends Controller
{
    /**
     * @inheritdoc
     */
   /* public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['admin', 'author'],
                    ],
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all ShopProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopProductSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["ShopProductSearch"]["status"] = 1;
        $dataProvider = $searchModel->search($queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['ShopProduct'])) {
            $params = $_GET['ShopProduct'];
            \Yii::$app->session->set('search.products', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
                //array_push($where, "json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk != 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
            
            if(isset($params['id_dict_case_type_fk']) && !empty($params['id_dict_case_type_fk']) ) {
				array_push($where, "m.id_dict_case_type_fk = ".$params['id_dict_case_type_fk']);
            }
            
			if(isset($params['customer_role']) && !empty($params['customer_role']) ) {
				array_push($where, "m.customer_role = ".$params['customer_role']);
            }
            
			if(isset($params['id_opposite_side_fk']) && !empty($params['id_opposite_side_fk']) ) {
				array_push($where, "m.id_opposite_side_fk = ".$params['id_opposite_side_fk']);
            }
            if(isset($params['a_instance_sygn']) && !empty($params['a_instance_sygn']) ) {
				//array_push($where, "m.id in (select id_case_fk from {{%cal_case_instance}} where instance_sygn like '%".$params['a_instance_sygn']."%')");
				array_push($where, "lower(m.authority_carrying) like '%".strtolower( addslashes($params['a_instance_sygn']) )."%'");
            }
        } else {
            array_push($where, "id_dict_case_status_fk == 1");
        }
		
		if(isset($_GET['type']) && $_GET['type'] == 'merge') {
			array_push($where, "id_dict_case_status_fk != 6");
		}
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%shop_product}} as p')
            ->where( ['p.status' => 1, 'p.type_fk' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['p.id as id', 'p.name as name', 'p.price', 'no_label', 'm.subname as subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk', 'id_dict_case_result_fk',
					  'id_parent_fk', 'm.status as status', 'c.name as cname', 'c.symbol as csymbol', 'c.id as cid', 'authority_carrying as sygns', "concat_ws(' ', e.lastname, e.firstname) as lemployee",
					  //"(select concat_ws(',', instance_sygn) from {{%cal_case_instance}} where id_case_fk = m.id and status = 1) as sygns"
					  'os.name as osname', 'os.symbol as ossymbol', 'os.id as osid'/*"GROUP_CONCAT(ci.instance_sygn) as sygns"*/
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
		//$query->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk');
        ;
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		$status = \backend\Modules\Task\models\CalCase::listStatus('advanced');
        $roles = \backend\Modules\Task\models\CalCase::listRoles(true);
		$types = \backend\Modules\Task\models\CalCase::listTypes(5);
        $states = \backend\Modules\Task\models\CalCase::listTypes(18);
        $results = \backend\Modules\Task\models\CalCase::listTypes(14);
        
		$presentationAll = CalCase::Viewer();
		foreach($rows as $key=>$value) {
			//$authority_carrying = \yii\helpers\Json::decode($value['authority_carrying']);
			//$model->authority_carrying_name = isset($authority_carrying['name']) ? $authority_carrying['name'] : 'brak danych'; 
			//$model->authority_carrying_contact = isset($authority_carrying['contact']) ? $authority_carrying['contact'] : 'brak danych'; 
			//$model->authority_carrying_sygn = isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : 'brak danych'; 
			
			if($value['id_dict_case_status_fk'] == 6) $value['id'] = $value['id_parent_fk'];
			if($value['id_dict_case_status_fk'] < 4) $value['id_dict_case_status_fk'] = 1;
			$presentation = $presentationAll[$value['id_dict_case_status_fk']];
			
			$tmp['client'] = '<a href="'.Url::to(['/crm/customer/view', 'id'=>$value['cid']]).'" >'.(($value['csymbol']) ? $value['csymbol'] : $value['cname']).'</a>'; 
			$tmp['role'] = '<span class="label">'. ((isset($roles[$value['customer_role']])) ? $roles[$value['customer_role']] : '') .'</span>';
            $tmp['employee'] = $value['lemployee'];
			$tmp['side'] = '<a href="'.Url::to(['/crm/side/view', 'id'=>$value['osid']]).'" >'.(($value['ossymbol']) ? $value['ossymbol'] : $value['osname']).'</a>';           
            $tmp['sname'] = $value['name'];
			$tmp['name'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
            $tmp['label'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['no_label'].'</a>';
			$tmp['label'] .= ($value['subname']) ? '<br /><small class="text--grey">'.$value['subname'].'</small>' : '';
            $tmp['sygn'] = '<a href="'.Url::to(['/task/matter/view', 'id'=>$value['id']]).'" >'.$value['sygns'].'</a>'; //isset($authority_carrying['sygn']) ? $authority_carrying['sygn'] : '';
            $value['id_dict_case_status_fk'] = ($value['id_dict_case_status_fk'] == 2 || $value['id_dict_case_status_fk'] == 3) ? 1 : $value['id_dict_case_status_fk'];
			$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'"><i class="fa fa-'.$presentation['icon'].'"></i></label>';
            if((count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 )) {
                $tmp['show'] = '<a class="inlineConfirm" href="'.Url::to(['/task/matter/sclient', 'id' => $value['id']]).'" data-table="#table-matters" title="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><i class="fa fa-eye'.((!$value['show_client'])?'-slash':'').' text--'.((!$value['show_client'])?'yellow':'blue').'"></i></a>';
            } else {
                $tmp['show'] = ($value['show_client']) ? '<i class="fa fa-eye text--blue" data-toggle="tooltip" data-title="Widoczne dla klienta"></i>' : '<i class="fa fa-eye-slash text--yellow" data-toggle="tooltip" data-title="Ukryte przed klientem"></i>';
            }
            if($value['id_dict_case_status_fk'] == 1) {
                $stage = ($value['id_dict_case_category_fk']) ? '<small class="text--purple"><b>'.(($value['id_dict_case_category_fk']) ? $states[$value['id_dict_case_category_fk']] : ' ').'</b></small>' : '<small class="text--edit">ustaw</span>';
                $tmp['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/state', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw bieżący stan sprawy">'.$stage.'</a>' : '';
            } else if($value['id_dict_case_status_fk'] != 6) {
                $stage = ($value['id_dict_case_result_fk']) ? '<small class="text--green"><b>'.(($value['id_dict_case_result_fk']) ? $results[$value['id_dict_case_result_fk']] : ' ').'</b></small>' : '<small class="text--edit">ustaw rezultat</span>';
                $tmp['stage'] = ($stage) ? '<a href="'.Url::to(['/task/matter/close', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw rezultat sprawy">'.$stage.'</a>' : '';            
            }
            
            if($value['id_dict_case_type_fk']) {
				$type = '<small class="text--purple"><b>'.(($value['id_dict_case_type_fk']) ? $types[$value['id_dict_case_type_fk']] : ' ').'</b></small>';
			} else {
				$type = '<small class="text--edit">ustaw</span>';
			}
            $tmp['type'] = '<a href="'.Url::to(['/task/matter/type', 'id' => $value['id']]).'" class="inlineEdit" data-target="#modal-grid-item" data-toggle="tooltip" data-title="Ustaw typ sprawy">'.$type.'</a>';
			/*$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" class="btn btn-sm bg-teal" title="Pobierz wszystkie dokumenty"><i class="fa fa-download"></i></a>';
                $tmp['actions'] .= ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>': '';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/task/matter/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-matters"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
            if($value['id_dict_case_status_fk'] == 6) {
                $tmp['actions'] = ' ';
                $tmp['stage'] = ' ';
                $tmp['type'] = ' ';
            } else {
                $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/task/matter/update', 'id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ? '<li class="deleteConfirm" data-table="#table-matters" href="'.Url::to(['/task/matter/delete', 'id' => $value['id']]).'" data-label="Usuń" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 && $value['id_dict_case_status_fk'] == 1) ? '<li class="update" data-table="#table-matters" href="'.Url::to(['/task/matter/close', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check text--green\'></i>'.Yii::t('app', 'Zamknij sprawę').'" title="'.Yii::t('app', 'Zamknij sprawę').'"><a href="#"><i class="fa fa-check text--green"></i>&nbsp;Zamknij</a></li>' : '')
                                        . '<li><a href="'.Url::to(['/task/matter/download', 'id' => $value['id']]).'" title="Pobierz wszystkie dokumenty"><i class="fa fa-download text--teal"></i>&nbsp;Pobierz dokumenty</a></li>'
                                        //. ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li class="inline" href="'.Url::to(['/task/matter/sclient', 'id' => $value['id']]).'" data-table="#table-matters" data-label="'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'"><a href="#" ><i class="fa fa-eye'.(($value['show_client'])?'-slash':'').' text--'.(($value['show_client'])?'yellow':'blue').'"></i>&nbsp;'.(($value['show_client'])?'Ukryj przed klientem':'Pokaż klientowi').'</a></li>' : '')
           
                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
            }
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single ShopProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model, 'dataForm' => ''
            ]);
        }
    }

    /**
     * Updates an existing ShopProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!$model->id_gallery_fk) {
            $newGallery = new \backend\Modules\Cms\models\CmsGallery();
            $newGallery->title = 'product#'.$id;
            $newGallery->type_fk = 3;
            if( $newGallery->save() ) $model->id_gallery_fk = $newGallery->id; $model->save();
        }
        $data = [];
        foreach($model->factors as $i => $factor) {
            $data[$factor->id_category_factor_fk] = json_decode($factor->name);
            $data['de'][$factor->id_category_factor_fk] = json_decode($factor->name_lang);
        }
        $model->data = $data;
        
        if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
            
            if( Yii::$app->request->post('Data')  ) {
                $model->user_action = 'Aktualizacja paramterów'; 
                $factors = Yii::$app->request->post('Data');
                $model->data = $factors;
            } else {
                $model->user_action = 'Aktualizacja danych podstawowych';
                $model->load(Yii::$app->request->post());
            }
            
            if ( $model->save()) {
                $res = array(
                    'success' => true,
                );
            } else {
                
                $res = array(
                    'errors'    =>  $model->getErrors() ,
                    'success' => false,
                );
            }
            
            return $res;
        } else {
        
            if( Yii::$app->request->post('Data')  ) {
                $factors = Yii::$app->request->post('Data');
                //ShopProductFactor::deleteAll('id_product_fk = :product', [':product' => $model->id]);
                $model->data = $factors;
                
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
         
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model, 'dataForm' => $this->generateForm($model->category['fields'], $model->data)
                ]);
            }
        }
        
        
    }
    
    public function actionMedia($id) {
        
        $model = $this->findModel($id);
        
        return $this->render('media', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShopProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Dane zostały przeniesione do kosza', 'id' => $id];
            
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ShopProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function generateForm($fields, $data = null)
    {
        $result = '';
        foreach($fields as $field)
        {
            $value = !empty($data[$field->id]) ? $data[$field->id] : null;
            $value_lang = !empty($data['de'][$field->id]) ? $data['de'][$field->id] : $value;
            $required = '';//($field->is_require == 1) ? 'required' : '';
            $field->options = ($field->options) ? explode(',',$field->options) : [];
            if ($field->value_type === 1 /*'string'*/) {
                $result .= '<div class="form-group '.$required.'">'
                                .'<div class="row">' 
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .'</label>'.Html::input('text', "Data[{$field->id}]", $value, ['class' => 'form-control']).'</div>'
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .' [DE]</label>'.Html::input('text', "Data[de][{$field->id}]", $value_lang, ['class' => 'form-control']).'</div>'
                                .'</div>'
                            .'</div>';
            }
            elseif ($field->value_type === 5 /*'text'*/) {
                //$result .= '<div class="form-group '.$required.'"><label class="control-label">'. $field->name .'</label>'. Html::textarea("Data[{$field->id}]", $value, ['class' => 'form-control mce-basic']) .'</div>';
                $result .= '<div class="form-group '.$required.'">'
                                .'<div class="row">' 
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .'</label>'.Html::textarea("Data[{$field->id}]", $value, ['class' => 'form-control mce-basic']).'</div>'
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .' [DE]</label>'.Html::textarea("Data[de][{$field->id}]", $value_lang, ['class' => 'form-control mce-basic']).'</div>'
                                .'</div>'
                            .'</div>';
            }
            elseif ($field->value_type === 2 /*'boolean'*/) {
                $result .= '<div class="checkbox '.$required.'"><label class="control-label">'. Html::checkbox("Data[{$field->id}]", $value, ['uncheck' => 0]) .' '. $field->name .'</label></div>';
            }
            elseif ($field->value_type === 3 /*'select'*/) {
                $options = ['' => 'wybierz'];
                foreach($field->options as $option){
                    $options[$option] = $option;
                }
                $result .= '<div class="form-group '.$required.'">'
                                .'<div class="row">' 
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .'</label><select name="Data['.$field->id.']" class="form-control">'. Html::renderSelectOptions($value, $options) .'</select></div>'
                                    .'<div class="col-sm-6"><label class="control-label">'. $field->name .' [DE]</label>'.Html::input('text', "Data[de][{$field->id}]", $value, ['class' => 'form-control']).'</div>'
                                .'</div>'
                            .'</div>';
            }
            elseif ($field->value_type === 4 /*'checkbox'*/) {
                $options = '';
                foreach($field->options as $option){
                    $checked = $value && in_array($option, $value);
                    $options .= '<br><label>'. Html::checkbox("Data[{$field->id}][]", $checked, ['value' => $option]) .' '. $option .'</label>';
                }
                $result .= '<div class="checkbox well well-sm '.$required.'"><b class="control-label">'. $field->name .'</b>'. $options .'</div>';
            }
 
        }
        return $result;
    }
}
