<?php

namespace app\Modules\Shop\controllers;

use Yii;
use backend\Modules\Shop\models\ShopCategory;
use backend\Modules\Shop\models\ShopCategorySearch;
use backend\Modules\Shop\models\ShopCategoryFactor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * ShopcategoryController implements the CRUD actions for ShopCategory model.
 */
class ShopcategoryController extends Controller
{
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'ShopCategory' => ShopCategory::className()
				]
			],
		];
	}
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['admin', 'author'],
                    ],
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopCategory models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ShopCategorySearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["ShopCategorySearch"]["status"] = 1;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ShopCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
        if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
            
            if( Yii::$app->request->post('Data')  ) {
                $model->user_action = 'Aktualizacja paramterów'; 
                $factors = Yii::$app->request->post('Data');
                $model->data = $factors;
            } else {
                $model->user_action = 'Aktualizacja danych';
                $model->load(Yii::$app->request->post());
            }
            
            if ( $model->save()) {
                $res = array(
                    'success' => true,
                );
            } else {
                
                $res = array(
                    'errors'    =>  $model->getErrors() ,
                    'success' => false,
                );
            }
            
            return $res;
        } else {
        
            if( Yii::$app->request->post('Data')  ) {
                $factors = Yii::$app->request->post('Data');
                //ShopProductFactor::deleteAll('id_product_fk = :product', [':product' => $model->id]);
                $model->data = $factors;
                
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
         
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model, 
                ]);
            }
        }
    }

    /**
     * Deletes an existing ShopCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Dane zostały przeniesione do kosza', 'id' => $id];
            
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ShopCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionFields($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$fields = [];
		$tmp = [];
		
		$fieldsData = ShopCategoryFactor::find()->where(['id_category_fk' => $id, 'status' => 1])->all();
        $valueTypes = \backend\Modules\Shop\models\ShopCategoryFactor::getTypes();

		
		/*$actionColumn = '<div class="edit-btn-group">';
		$actionColumn .= '<a href="javascript:void(0)" data-href="/panel/pl/shop/shopcategoryfactor/updateajax/%d" class="btn btn-xs btn-success btn-icon gridViewModalField" data-table="#table-fields" data-form="recipe-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Edit').'</a>';
		$actionColumn .= '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-icon gridViewModalFieldDelete" data-table="#table-fields"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('lsdd', 'Delete').'</a>';
		$actionColumn .= '</div>';*/
        
        $actionColumn = '<div class="edit-btn-group">';
		//$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
		$actionColumn .= '<a href="/panel/pl/shop/shopcategoryfactor/update/%d" class="btn btn-xs btn-success btn-icon gridViewModal" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

		$actionColumn .= '<button data-href="/panel/pl/shop/shopcategoryfactor/delete/%d" class="btn btn-xs btn-danger gridViewModalDelete"  data-table="#table-items">'.Yii::t('lsdd', 'Delete').'</button>';
		$actionColumn .= '</div>';
		
		foreach($fieldsData as $key=>$value) {
			
			$tmp['name'] = $value->name;
			$tmp['symbol'] = $value->symbol;
			$tmp['value_type'] = $valueTypes[$value->value_type];
			$tmp['options'] = $value->options;
            $tmp['action'] = sprintf($actionColumn, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;//time();
            $tmp['index'] = $value->rank;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
}
