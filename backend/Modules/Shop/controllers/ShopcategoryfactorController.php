<?php

namespace app\Modules\Shop\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\Modules\Shop\models\ShopCategoryFactor;
use backend\Modules\Shop\models\ShopCategoryFactorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;

/**
 * ShopcategoryfactorController implements the CRUD actions for ShopCategoryFactor model.
 */
class ShopcategoryfactorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopCategoryFactor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCategoryFactorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopCategoryFactor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopCategoryFactor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ShopCategoryFactor();
        $model->id_category_fk = $id;

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }*/
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('result' => true);	
		 	} else {
				return array('result' => false, 'html' => $this->renderPartial('_form', [  'model' => $model, ]) );	
		    }
			
			return $this->renderPartial('_form', [
                'model' => $model, 'zone' => $modelPackageSystem->id_zone_fk
            ]);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model, 'zone' => $modelPackageSystem->id_zone_fk
            ]);
        }
    }

    /**
     * Updates an existing ShopCategoryFactor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }*/
        if ($model->load(Yii::$app->request->post()) ) {

			Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('result' => true);	
		 	} else {
				return array('result' => false, 'html' => $this->renderPartial('_form', [  'model' => $model,  ]) );	
		    }
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ShopCategoryFactor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($this->findModel($id)->delete()) {
            return ['result' => true];
        } else {
            return ['result' => false, 'error' => ''];
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopCategoryFactor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopCategoryFactor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopCategoryFactor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreateajax($id) {
		
		$model = new ShopCategoryFactor();
		$model->id_category_fk = $id;
		$model->id_factor_fk = 0;
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				
				/*$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="javascript:void(0)" data-href="/panel/pl/lsdd/lsddrecipematerial/updateajax/%d" class="btn btn-xs btn-success btn-icon gridViewModalMaterial" data-table="#table-materials" data-form="recipe-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Edit').'</a>';
				//$actionColumn = '<a class="edit ml10" href="javascript:void(0)" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>';
				$actionColumn .= '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-icon gridViewModalMaterialDelete" data-table="#table-recipies"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('lsdd', 'Delete').'</a>';
				//$actionColumn .= '<a class="gridViewModalMaterialDelete ml10" href="javascript:void(0)" title="Remove"> <i class="glyphicon glyphicon-remove"></i> </a>';
				//$actionColumn .= '<p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p>';
				//$actionColumn .= '<p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>';
				$actionColumn .= '</div>';*/
				
				return array('result' => true, 'data' =>[]/* [  'name' => $model->material['material_name'], 
															'amount' => $model->amount, 
															'cost' => $model->amount * $model->material['price_native'],
															'cost_alternative' => $model->amount * $model->material['price_native']*2,
															'action' => sprintf($actionColumn, $id),
															//'move' => '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>',
															'move' => '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-down"></i></a>',
															'id' => time(),
															'material' => $model->id_material_fk] */);	
			} else {
				return array('result' => false, 'html' => $this->renderPartial('_form', [  'model' => $model]) );	
			}		
		} else {
			return  $this->renderPartial('_form', [  'model' => $model, ]) ;	
		}
	}
    
    public function actionUpdateajax($id)
    {
        //$model = $this->findModel($id);
		$model = new ShopCategoryFactor();
		$model->id_category_fk = $id;

        if ($model->load(Yii::$app->request->post()) ) {

			Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->validate() && $model->isUpdate == 0) {
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="javascript:void(0)" data-href="/panel/pl/shop/lsddrecipematerial/updateajax/%d" class="btn btn-xs btn-success btn-icon gridViewModalField" data-table="#table-materials" data-form="recipe-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Edit').'</a>';
				$actionColumn .= '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-icon gridViewModalFieldDelete" data-table="#table-recipies"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('lsdd', 'Delete').'</a>';
			    $actionColumn .= '</div>';
				
				return array('result' => true, 'index' => $model->index, 'data' => [  'name' => $model->name, 
																						'symbol' => $model->symbol, 
																						'value_type' => $model->value_type,
																						'action' => sprintf($actionColumn, $id),
																						//'move' => '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>',
																						'move' => '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-down"></i></a>',																						
																						'id' => $model->id,
																						'category' => $model->id_category_fk] );		
		 	} else {
				return array('result' => false, 'index' => $model->index, 'html' => $this->renderPartial('_form', [  'model' => $model ]) );	
		    }
        } else {
            //return  $this->renderPartial('_form', [  'model' => $model, 'zone' => $zoneId ]) ;
			return array('result' => false, 'index' => $model->index, 'html' => $this->renderPartial('_form', [  'model' => $model ]) );		
        }
    }
	
	public function actionSave($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = \backend\Modules\Shop\models\ShopCategoryFactor::findOne($item['id']);
                $model->rank = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany nie zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
}
