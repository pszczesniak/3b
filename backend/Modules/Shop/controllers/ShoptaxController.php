<?php

namespace app\Modules\Shop\controllers;

use Yii;
use backend\Modules\Shop\models\ShopTax;
use backend\Modules\Shop\models\ShopTaxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * ShoptaxController implements the CRUD actions for ShopTax model.
 */
class ShoptaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['admin', 'author'],
                    ],
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopTax models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ShopTax();
 
        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON; 
                $model = new ShopTax(); //reset model
                return ['success' => true];
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON; 
                return ['success' => false, 'html' => $this->renderPartial('_form', ['model' => $model])];
            }
        }
		
		$searchModel = new ShopTaxSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["ShopTaxSearch"]["status"] = 1;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single ShopTax model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopTax model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopTax();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ShopTax model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if(isset(Yii::$app->request->post()['Lang']))
                $model->name_lang = \yii\helpers\Json::encode(Yii::$app->request->post()['Lang']);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON; 
                return ['success' => true];
            } else {
                 Yii::$app->response->format = Response::FORMAT_JSON; 
                return ['success' => false, 'html' => $this->renderPartial('_form', ['model' => $model])];
            }
        } else {
            return $this->renderPartial('_form', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing ShopTax model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopTax model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopTax the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopTax::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
