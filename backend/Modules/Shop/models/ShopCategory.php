<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use common\models\Translations;


/**
 * This is the model class for table "{{%shop_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property string $config
 * @property integer $click
 * @property string $slug
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ShopCategory extends \yii\db\ActiveRecord
{
    public $user_action = '';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name_lang', 'describe','brief'], 'string'],
            [['click', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'config', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'brief' => 'Skr�cony opis',
            'describe' => Yii::t('app', 'Opis'),
            'config' => Yii::t('app', 'Config'),
            'click' => Yii::t('app', 'Click'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				if($this->user_action != 'hit')
                    $this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'name',
				'slugAttribute' => 'slug',
			],
            "timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'ShopCategory',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['frontenddir'] . '/web/uploads/shop/category/cover',
				'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['frontendurl'] . '/uploads/shop/category/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			],
			'coverBehaviorDe' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'ShopCategory',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['frontenddir'] . '/web/uploads/shop/category/de/cover',
				'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['frontendurl'] . '/uploads/shop/category/de/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function getFields()
    {
        //return $this->hasOne(\app\Modules\Lsdd\models\LsddCompanyPerson::className(), ['id' => 'id_company_fk']);
		//$items = $this->hasMany(\backend\Modules\Shop\models\ShopCategoryFactor::className(), ['id_category_fk' => 'id']); 
        $items = \backend\Modules\Shop\models\ShopCategoryFactor::find()->where(['id_category_fk' => $this->id, 'status' => 1])->orderby('rank')->all();
		
		return $items;
    }
    
    public function getProducts()
    {
        //return $this->hasOne(\app\Modules\Lsdd\models\LsddCompanyPerson::className(), ['id' => 'id_company_fk']);
		//$items = $this->hasMany(\backend\Modules\Shop\models\ShopCategoryFactor::className(), ['id_category_fk' => 'id']); 
        $items = \backend\Modules\Shop\models\ShopProduct::find()->where(['id_category_fk' => $this->id, 'status' => 1])->orderby('id desc')->all();
		
		return $items;
    }
	
	public function getImage() {
		return true;
	}
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 4])->one();
        
        if(!$model) {
            $category = ShopCategory::findOne($id);
            
            $attributes['name'] = $category->name;
            $attributes['describe'] = $category->describe;
            $attributes['meta_title'] = $category->meta_title;
            $attributes['meta_keywords'] = $category->meta_keywords;
            $attributes['meta_description'] = $category->meta_description;
                        
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 4;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
            
            if(!isset($model->attributes['meta_title'])) $model->attributes['meta_title'] = '';
            if(!isset($model->attributes['meta_keywords'])) $model->attributes['meta_keywords'] = '';
            if(!isset($model->attributes['meta_description'])) $model->attributes['meta_description'] = '';
        }
        
        return $model;
    }
}
