<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Shop\models\ShopProduct;

/**
 * ShopProductSearch represents the model behind the search form about `backend\Modules\Shop\models\ShopProduct`.
 */
class ShopProductSearch extends ShopProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_unit_fk', 'id_tax_fk', 'is_discount', 'is_sales', 'is_new', 'is_available', 'click', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'name_lang', 'describe', 'config', 'params', 'slug', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['price', 'discount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'discount' => $this->discount,
            'id_unit_fk' => $this->id_unit_fk,
            'id_tax_fk' => $this->id_tax_fk,
            'is_discount' => $this->is_discount,
            'is_sales' => $this->is_sales,
            'is_new' => $this->is_new,
            'is_available' => $this->is_available,
            'click' => $this->click,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_lang', $this->name_lang])
            ->andFilterWhere(['like', 'describe', $this->describe])
            ->andFilterWhere(['like', 'config', $this->config])
            ->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
