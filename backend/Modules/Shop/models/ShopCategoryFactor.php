<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%shop_category_factor}}".
 *
 * @property integer $id
 * @property integer $id_category_fk
 * @property integer $id_factor_fk
 * @property string $name
 * @property string $name_lang
 * @property string $symbol
 * @property string $symbol_lang
 * @property string $config
 * @property integer $is_require
 * @property integer $value_type
 * @property string $options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ShopCategoryFactor extends \yii\db\ActiveRecord
{
    //public $index;
	public $isUpdate;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_category_factor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category_fk', 'value_type', 'name','symbol'], 'required'],
            [['id_category_fk', 'id_factor_fk', 'is_require', 'value_type', 'status', 'created_by', 'updated_by', 'deleted_by', 'index','rank'], 'integer'],
            [['name_lang', 'symbol_lang', 'config', 'options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at','rank'], 'safe'],
            [['name', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_category_fk' => Yii::t('app', 'Id Category Fk'),
            'id_factor_fk' => Yii::t('app', 'Id Factor Fk'),
            'name' => Yii::t('app', 'Nazwa'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'symbol' => Yii::t('app', 'Symbol'),
            'symbol_lang' => Yii::t('app', 'Symbol Lang'),
            'config' => Yii::t('app', 'Config'),
            'is_require' => Yii::t('app', 'wymagany'),
            'value_type' => Yii::t('app', 'Typ pola'),
            'options' => Yii::t('app', 'Options'),
            'rank' => Yii::t('app', 'rank'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
				$this->rank = $this->getCountByCategoryID($this->id_category_fk);
			} else { 
				$this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
            "timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function getTypes() {
        return [
                "1" => "Tekst",
                "2" => "Pole typy 'tak/nie'", 
                "3" => "Lista rozwiajalna",
               // "4" => "Checkbox",
                "5" => "Opis własny"
            ];
    }
	
	public function getCountByCategoryID($id) {
		$items = $this->find()->where('id_category_fk = :id and status = :status', [':id'=>$id, ':status'=>1 ] )->all();
		return count($items)+1;
	}
}
