<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Shop\models\ShopCategoryFactor;

/**
 * ShopCategoryFactorSearch represents the model behind the search form about `backend\Modules\Shop\models\ShopCategoryFactor`.
 */
class ShopCategoryFactorSearch extends ShopCategoryFactor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_category_fk', 'id_factor_fk', 'is_require', 'value_type', 'status', 'created_by', 'updated_by', 'deleted_by','rank'], 'integer'],
            [['name', 'name_lang', 'symbol', 'symbol_lang', 'config', 'options', 'created_at', 'updated_at', 'deleted_at','rank'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopCategoryFactor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_category_fk' => $this->id_category_fk,
            'id_factor_fk' => $this->id_factor_fk,
            'is_require' => $this->is_require,
            'value_type' => $this->value_type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_lang', $this->name_lang])
            ->andFilterWhere(['like', 'symbol', $this->symbol])
            ->andFilterWhere(['like', 'symbol_lang', $this->symbol_lang])
            ->andFilterWhere(['like', 'config', $this->config])
            ->andFilterWhere(['like', 'options', $this->options]);

        return $dataProvider;
    }
}
