<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%shop_factor}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $symbol
 * @property string $symbol_lang
 * @property string $describe
 * @property string $config
 * @property integer $is_require
 * @property integer $value_type
 * @property string $form_type
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ShopFactor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_factor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_lang', 'symbol_lang', 'describe'], 'string'],
            [['is_require', 'value_type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'symbol', 'config', 'form_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'symbol' => Yii::t('app', 'Symbol'),
            'symbol_lang' => Yii::t('app', 'Symbol Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'config' => Yii::t('app', 'Config'),
            'is_require' => Yii::t('app', 'Is Require'),
            'value_type' => Yii::t('app', 'Value Type'),
            'form_type' => Yii::t('app', 'Form Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->fk_status_id = 4;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
