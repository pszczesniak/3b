<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Cms\models\CmsGallery;
use backend\Modules\Shop\models\ShopProductFactor;
use backend\Modules\Shop\models\ShopCategoryFactor;

use common\models\Translations;


/**
 * This is the model class for table "{{%shop_product}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property double $price
 * @property double $discount
 * @property integer $id_unit_fk
 * @property integer $id_tax_fk
 * @property string $config
 * @property string $params
 * @property integer $is_discount
 * @property integer $is_sales
 * @property integer $is_new
 * @property integer $is_available
 * @property integer $click
 * @property string $slug
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ShopProduct extends \yii\db\ActiveRecord
{
    public $data = [];
    public $user_action = '';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'subname', 'id_category_fk'], 'required'],
            [['name_lang', 'describe', 'config', 'params', 'brief'], 'string'],
            [['price', 'discount'], 'number'],
            [['id_category_fk', 'id_unit_fk', 'id_tax_fk', 'is_discount', 'is_sales', 'is_new', 'is_available', 'is_featured', 'click', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_gallery_fk'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'subname', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 1000],
            //['data', 'validateData'],
            /*['discount', 'required', 'when' => function($model) {
				return ($model->is_discount == 1) ? true : false ;
			}]*/
        ];
    }
    
    public function validateData($attribute, $params)
    {
        /*if (!in_array($this->$attribute, ['USA', 'Web'])) {
            $this->addError($attribute, 'The country must be either "USA" or "Web".');
        }*/
        foreach($this->$attribute as $key => $value) {
            $categoryFactor = ShopCategoryFactor::findOne($key);
            if($categoryFactor->is_require == 1 && empty($value)) {
                $this->addError($attribute, 'Parametr "'.$categoryFactor->name.'" jest wymagany.');
            }
        }
    }
   

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa [część I]'),
            'subname' => Yii::t('app', 'Nazwa [część II]'),
            'name_lang' => Yii::t('app', 'Rozszerzenie nazwy'),
            'describe' => Yii::t('app', 'Szczegółowy opis'),
            'brief' => 'Opis skrócony',
            'price' => Yii::t('app', 'Cena'),
            'discount' => Yii::t('app', 'Cena promocyjna'),
            'id_unit_fk' => Yii::t('app', 'Jednostka'),
            'id_tax_fk' => Yii::t('app', 'Podatek'),
            'id_category_fk' => Yii::t('app', 'Kategoria'),
            'config' => Yii::t('app', 'Config'),
            'params' => Yii::t('app', 'Params'),
            'is_discount' => Yii::t('app', 'oznacz w serwisie jako promocja'),
            'is_sales' => Yii::t('app', 'oznacz w serwisie jako wyprzedaż'),
            'is_new' => Yii::t('app', 'oznacz w serwisie jako nowość'),
            'is_available' => Yii::t('app', 'Is Available'),
			'is_featured' => Yii::t('app', 'oznacz w serwisie jako polecany'),
            'click' => Yii::t('app', 'Click'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
                if(!$this->brief)
                    $this->brief = implode(' ', array_slice(explode(' ', strip_tags($this->describe)), 0, 30)).' [...]';
				$this->created_by = \Yii::$app->user->id;
			} else { 
				if($this->user_action != 'hit')
                    $this->updated_by = \Yii::$app->user->id;
			}
            
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $attributes){
        parent::afterSave($insert, $attributes);
   
        if( $this->data ) {
            ShopProductFactor::deleteAll('id_product_fk = :product', [':product' => $this->id]);
            foreach($this->data as $key => $value) {
                $newFactor = new ShopProductFactor();
                $newFactor->id_product_fk = $this->id;
                $newFactor->id_category_factor_fk = $key;
                $newFactor->name =  json_encode($value) ;
               // echo $this->data['de'][$key];exit;
                if(isset($this->data['de'][$key])) { //echo $this->data['de'][$key];exit;
                    $newFactor->name_lang =  json_encode($this->data['de'][$key]);
                }  
                $newFactor->save();
                
                // echo $key. ' => ';var_dump($value); echo '<br />';
            }//exit;
        }
            
    }
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'subname',
				'slugAttribute' => 'slug',
			],
            "timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCategory()
    {
        return $this->hasOne(\backend\Modules\Shop\models\ShopCategory::className(), ['id' => 'id_category_fk']);
    }
    
    public function getFactors()
    {
        //return $this->hasOne(\app\Modules\Lsdd\models\LsddCompanyPerson::className(), ['id' => 'id_company_fk']);
		//$items = $this->hasMany(\backend\Modules\Shop\models\ShopCategoryFactor::className(), ['id_category_fk' => 'id']); 
        $items = \backend\Modules\Shop\models\ShopProductFactor::find()->where(['id_product_fk' => $this->id])->all();
		
		return $items;
    }
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 3])->one();
        
        if(!$model) {
            $product = ShopProduct::findOne($id);
            
            $attributes['name'] = $product->name;
            $attributes['subname'] = $product->subname;
            $attributes['brief'] = $product->brief;
            $attributes['describe'] = $product->describe;
            $attributes['meta_title'] = $product->meta_title;
            $attributes['meta_keywords'] = $product->meta_keywords;
            $attributes['meta_description'] = $product->meta_description;
                        
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 3;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
            
            if(!isset($model->attributes['name'])) $model->attributes['name'] = '';
            if(!isset($model->attributes['subname'])) $model->attributes['subname'] = '';
            if(!isset($model->attributes['brief'])) $model->attributes['brief'] = '';
            if(!isset($model->attributes['describe'])) $model->attributes['describe'] = '';
            if(!isset($model->attributes['meta_title'])) $model->attributes['meta_title'] = '';
            if(!isset($model->attributes['meta_keywords'])) $model->attributes['meta_keywords'] = '';
            if(!isset($model->attributes['meta_description'])) $model->attributes['meta_description'] = '';
        }
        
        return $model;
    }
    
    public function getImages() {
        $gallery = \backend\Modules\Cms\models\CmsGallery::findOne($this->id_gallery_fk);
        
        if(isset($_GET['language']) && $_GET['language'] != 'pl' && $gallery) {
            $gallery_lang = \backend\Modules\Cms\models\CmsGallery::find()->where(['versions_data' => 'product#'.$gallery->id.'#'.$_GET['language']])->one();
            if(isset($gallery_lang) && $gallery_lang->images)
                $gallery = $gallery_lang;
        }
        
        return ($gallery) ? $gallery->items : false;
    }
    
    public function getUnit() {
        return \backend\Modules\Shop\models\ShopUnit::findOne($this->id_unit_fk);
    }
    
    public function getFiles() {
        return \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 14])->orderby('created_at desc')->all();
    }
}
