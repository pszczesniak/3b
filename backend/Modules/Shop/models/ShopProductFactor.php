<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%shop_product_factor}}".
 *
 * @property integer $id
 * @property integer $id_product_fk
 * @property integer $id_category_factor_fk
 * @property string $name
 * @property string $name_lang
 * @property string $symbol
 * @property string $symbol_lang
 * @property string $config
 * @property string $options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class ShopProductFactor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_product_factor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product_fk', 'id_category_factor_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name_lang', 'symbol_lang', 'options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'symbol', 'config'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_product_fk' => Yii::t('app', 'Id Product Fk'),
            'id_category_factor_fk' => Yii::t('app', 'Id Category Factor Fk'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'symbol' => Yii::t('app', 'Symbol'),
            'symbol_lang' => Yii::t('app', 'Symbol Lang'),
            'config' => Yii::t('app', 'Config'),
            'options' => Yii::t('app', 'Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [

            "timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getFactor()
    {
        return $this->hasOne(\backend\Modules\Shop\models\ShopCategoryFactor::className(), ['id' => 'id_category_factor_fk']);
    }
}
