<?php

namespace backend\Modules\Shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Shop\models\ShopTax;

/**
 * ShopTaxSearch represents the model behind the search form about `backend\Modules\Shop\models\ShopTax`.
 */
class ShopTaxSearch extends ShopTax
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'name_lang', 'describe', 'config', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['rate_tax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopTax::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rate_tax' => $this->rate_tax,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_lang', $this->name_lang])
            ->andFilterWhere(['like', 'describe', $this->describe])
            ->andFilterWhere(['like', 'config', $this->config]);

        return $dataProvider;
    }
}
