<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxFormWithAttach', 'data-target' => "#modal-grid-item", 'data-table' => "#table-employments", 'enctype' => 'multipart/form-data', 'data-model' => 'Employment'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <?php if($model->attachs) echo '<div class="alert alert-warning">UWAGA! Proszę przed zapisem ponownie załadować pliki!</div>' ?>
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li <?= (($activeTab == 'info') ? 'class="active"' : '') ?> ><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                    <li <?= (($activeTab == 'basic') ? 'class="active"' : '') ?> ><a data-toggle="tab" href="#tab2b"><i class="fa fa-percent"></i><span class="panel-tabs--text">Ryczałt</span> </a></li>
                    <li <?= (($activeTab == 'over') ? 'class="active"' : '') ?> ><a data-toggle="tab" href="#tab2c"><i class="fa fa-moon-o"></i><span class="panel-tabs--text">Nadgodziny</span> </a></li>
					<li <?= (($activeTab == 'bonus') ? 'class="active"' : '') ?> ><a data-toggle="tab" href="#tab2d"><i class="fa fa-gift"></i><span class="panel-tabs--text">Premie</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2e"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane <?= (($activeTab == 'info') ? 'active' : '') ?>" id="tab2a">
                        <div class="grid">
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="employment-date_from" class="control-label">Data rozpoczęcia</label>
                                    <div class='input-group date' id='datetimepicker_start'>
                                        <input type='text' class="form-control" id="employment-date_from" name="Employment[date_from]" value="<?= $model->date_from ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
                                });
                            </script>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="employment-date_to" class="control-label">Data zakończenia</label>
                                    <div class='input-group date' id='datetimepicker_end'>
                                        <input type='text' class="form-control" id="employment-date_to" name="Employment[date_to]" value="<?= $model->date_to ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
                                   
                                });
                            </script>
                            <div class="col-md-6 col-sm-12  col-xs-12"> <?= $form->field($model, 'id_employee_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAll(), 'id', 'fullname'), ['prompt' => '- wybierz -', 'class' => 'form-control select2', 'required' => true] ) ?> </div>
                        </div>
                        <div class="grid">
                            <div class="col-sm-2 col-xs-12"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [ 'class' => 'form-control'] ) ?> </div>                
                            <div class="col-sm-10 col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                        </div>
                    </div>
                    <div class="tab-pane <?= (($activeTab == 'basic') ? 'active' : '') ?>" id="tab2b">
                        <fieldset><legend>Podstawa&nbsp;&nbsp;<a href="#" id="add-basic" class="btn btn-xs bg-green"><i class="fa fa-plus"></i>kolejny</a></legend>
                            <div id="rates-basic">
                            <?php 
                                foreach($basictime as $i => $modelB) {
                                    echo '<div><div class="input-group input-group-sm">'
                                            . (($i != 0) ? '<span class="input-group-addon removeForm" data-id="basic-'.$modelB->id.'" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>' : '')
                                            .'<span class="input-group-addon"><i class="text--red">*</i>Ryczałt</span>'
                                            .'<input type="hidden" name="Basic['.$i.'][id]" value="'.$modelB->id.'">'
                                            .'<input type="text" name="Basic['.$i.'][rate]" class="form-control number '.((isset($modelB->getErrors()['rate'])) ? ' required' : '').'" placeholder="Wpisz kwotę" value="'.$modelB->rate.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            .'<span class="input-group-addon">'.$model->currency.' <i class="text--red">*</i>za</span>'
                                            .'<input type="text" name="Basic['.$i.'][hours]" class="form-control number '.((isset($modelB->getErrors()['hours_to'])) ? ' required' : '').'" placeholder="Wpisz liczbę godzin" value="'.$modelB->hours_to.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            .'<span class="input-group-addon">h</span>'
                                        .'</div>'
                                        .'<div class="input-group">'
                                            . (($i != 0) ? 
											    '<input type="text" name="Basic['.$i.'][between]"  class="form-control number '.((isset($modelB->getErrors()['rate_between'])) ? ' required' : '').'" placeholder="Stawka pomiędzy" value="'.$modelB->rate_between.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                                .'<span class="input-group-addon">'.$model->currency.'/h <i class="fa fa-info-circle text--blue"></i></span>'
												: '' )
											.'<span class="input-group-addon">Redukcja</span>'
                                            .'<span class="input-group-addon">'
                                                .'<input type="checkbox" name="Basic['.$i.'][reduction]" '.(($modelB->is_reduction) ? 'checked' : '').' >'
                                            .'</span>'
                                            .'<input type="text" name="Basic['.$i.'][min_value]" class="form-control number '.((isset($modelB->getErrors()['no_less_than'])) ? ' required' : '').'" value="'.$modelB->no_less_than.'" pattern="[0-9]+(\.[0-9][0-9]?)?">'
											.'<span class="input-group-addon">nie mniej niż</span>'
											.'<span class="input-group-addon">'.$model->currency.'</span>'
                                            .'</div><br /></div>';
                                }
                            ?>
                            </div>
                        </fieldset>   
                        <i class="fa fa-info-circle text--blue"></i> - stawka za godzinę pomiędzy definiowanym a poprzednim ryczałtem                     
                    </div>
                    <div class="tab-pane <?= (($activeTab == 'over') ? 'active' : '') ?>" id="tab2c">
                        <fieldset><legend>Nadgodziny&nbsp;&nbsp;<a href="#" id="add-over" class="btn btn-xs bg-green"><i class="fa fa-plus"></i>zakres</a></legend>
                            <div id="rates-over">
                            <?php
                                foreach($overtime as $j => $modelO) {
                                    echo '<div><div class="input-group input-group-sm">'
                                            .'<span class="input-group-addon removeForm" data-id="over-'.$modelO->id.'" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>'
                                            .'<input type="hidden" name="Over['.$j.'][id]" value="'.$modelO->id.'">'
                                            .'<span class="input-group-addon"><i class="text--red">*</i>Od </span>'
                                            .'<input type="text" name="Over['.$j.'][from_h]" class="form-control number '.((isset($modelO->getErrors()['hours_from'])) ? ' required' : '').'" placeholder="Wpisz liczbę godzin" value="'.$modelO->hours_from.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            .'<span class="input-group-addon">h do</span>'
                                            .'<input type="text" name="Over['.$j.'][to_h]" class="form-control number '.((isset($modelO->getErrors()['hours_to'])) ? ' required' : '').'" placeholder="Wpisz liczbę godzin" value="'.$modelO->hours_to.'">'
                                            .'<span class="input-group-addon"> h <i class="text--red">*</i>stawka</span>'
                                            .'<input type="text" name="Over['.$j.'][rate]" class="form-control number '.((isset($modelO->getErrors()['rate'])) ? ' required' : '').'" placeholder="Wpisz stawkę za godzinę" value="'.$modelO->rate.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
											.'<span class="input-group-addon"> '.$model->currency.'/h</span>'
                                        .'</div></div>';
                                }
                            ?>
                            </div>
                        </fieldset>
                    </div>
					<div class="tab-pane <?= (($activeTab == 'bonus') ? 'active' : '') ?>" id="tab2d">
                        <fieldset><legend>Premie&nbsp;&nbsp;<a href="#" id="add-bonus" class="btn btn-xs bg-green"><i class="fa fa-plus"></i>warunek</a></legend>
                            <div id="rates-bonus">
                            <?php
                                foreach($bonustime as $k => $modelG) {
                                    echo '<div><div class="input-group input-group-sm">'
                                            .'<span class="input-group-addon removeForm" data-id="bonus-'.$modelG->id.'" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>'
                                            .'<input type="hidden" name="Bonus['.$k.'][id]" value="'.$modelG->id.'">'
                                            .'<span class="input-group-addon"><i class="text--red">*</i>Prermia </span>'
                                            .'<input type="text" name="Bonus['.$k.'][rate]" class="form-control number '.((isset($modelG->getErrors()['rate_bonus'])) ? ' required' : '').'" placeholder="Wpisz wartość premii" value="'.$modelG->rate_bonus.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            .'<span class="input-group-addon">'.$model->currency.' <i class="text--red">*</i>za</span>'
                                            .'<input type="text" name="Bonus['.$k.'][to_h]" class="form-control number '.((isset($modelG->getErrors()['rate_bonus'])) ? ' required' : '').'" placeholder="Wpisz liczbę godzin" value="'.$modelG->hours_to.'" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            .'<span class="input-group-addon"> h </span>'
                                        .'</div></div>';
                                }
                            ?>
                            </div>
                        </fieldset>
                    </div>
                    <div class="tab-pane" id="tab2e"><?php /*var_dump($model->attachs);*/ ?>
                        <?php if(!$model->isNewRecord) { ?>
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 12, 'parentId' => $model->id, 'onlyShow' => false]) ?> 
                        <?php } else { ?>
                            <div class="files-wr" data-count-files="<?= ($model->attachs) ? /*count($model->attachs)*/'1' : '1' ?>" data-title="<?= \Yii::t('app', 'Attach file') ?>">
                                <div class="one-file">
                                    <label for="file-1"><?= \Yii::t('app', 'Attach file') ?></label>
                                    <input class="attachs" name="file-1" id="file-1" type="file">
                                    <div class="file-item hide-btn">
                                        <span class="file-name"></span>
                                        <span class="btn btn-del-file">x</span>
                                    </div>
                                </div>
                                <?php if($model->attachs) {
                                    foreach($model->attachs as $key => $attach) {
                                       /* echo '<div class="one-file"><label for="file-' . ($key+2) . '">'.\Yii::t('app', 'Attach file').'</label>' 
                                              .'<input type="file" name="file-' .($key+2). '" id="file-' .($key+2). '"><div class="file-item hide-btn">' 
                                              .'<span class="file-name">'.$attach->name.'</span><span class="btn btn-del-file">x</span></div></div>';*/
                                        /*echo '<div class="one-file error">'
                                                .'<label for="file-' . ($key+2) . '">Dołącz plik</label>'
                                                .'<input class="attachs" name="file-' . ($key+2) . '" id="file-' . ($key+2) . '" type="file" value="'.$attach->tempName.'">'
                                                .'<div class="file-item">'
                                                    .'<span class="file-name">'.$attach->name.'</span>'
                                                    .'<span class="btn btn-del-file">x</span>'
                                                .'</div>'
                                            .'</div>';*/
                                    }
                                } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'submitButton']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>


<script type="text/javascript">
	
    $('#submitButton').click(function () {
        $('select:invalid, input:invalid').each(function () {
            // Find the tab-pane that this element is inside, and get the id
            var $closest = $(this).closest('.tab-pane');
            var id = $closest.attr('id');

            // Find the link that corresponds to the pane and have it show
            $('.nav a[href="#' + id + '"]').tab('show');

            // Only want to do it once
            return false;
        });
    });

    var iBasic = <?= count($basictime) ?>, iOver = <?= count($overtime) ?>, iBonus = <?= count($bonustime) ?>;
    document.getElementById('add-basic').onclick = function() {
		document.getElementById('rates-basic').insertAdjacentHTML('beforeend','<div><div class="input-group input-group-sm" id="basic-'+iBasic+'">'
                                            +'<input type="hidden" name="Basic['+iBasic+'][id]" >'
                                            +'<span class="input-group-addon removeForm" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>'
                                            +'<span class="input-group-addon"><i class="text--red">*</i>Ryczałt</span>'
                                            +'<input type="text" name="Basic['+iBasic+'][rate]" class="form-control number" placeholder="Wpisz kwotę" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon"><i class="text--red">*</i>za</span>'
                                            +'<input type="text" name="Basic['+iBasic+'][hours]" class="form-control number" placeholder="Wpisz liczbę godzin" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon">h</span>'
                                        +'</div>'
                                        +'<div class="input-group">'
                                            +'<input type="text" name="Basic['+iBasic+'][between]" class="form-control number" placeholder="Stawka pomiędzy" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon"><?= $model->currency ?>/h <i class="text--red">*</i><i class="fa fa-info-circle text--blue"></i></span>'
                                            +'<span class="input-group-addon">Redukcja</span>'
                                            +'<span class="input-group-addon">'
                                                +'<input type="checkbox" name="Basic['+iBasic+'][reduction]"  aria-label="...">'
                                            +'</span>'
											+'<span class="input-group-addon">nie mniej niż</span>'
                                            +'<input type="text" name="Basic['+iBasic+'][min_value]" class="form-control number" pattern="[0-9]+(\.[0-9][0-9]?)?">'
											+'<span class="input-group-addon"><?= $model->currency ?></span>'
                                        +'</div><br /></div>');
		++iBasic;								
	}
    
    document.getElementById('add-over').onclick = function() {
		document.getElementById('rates-over').insertAdjacentHTML('beforeend','<div><div class="input-group input-group-sm">'
                                            +'<span class="input-group-addon removeForm" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>'
                                            +'<input type="hidden" name="Over['+iOver+'][id]" >'
                                            +'<span class="input-group-addon"><i class="text--red">*</i>Od </span>'
                                            +'<input type="text" name="Over['+iOver+'][from_h]" class="form-control number" placeholder="Wpisz liczbę godzin" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon">h do</span>'
                                            +'<input type="text" name="Over['+iOver+'][to_h]" class="form-control number" placeholder="Wpisz liczbę godzin" pattern="[0-9]+(\.[0-9][0-9]?)?">'
                                            +'<span class="input-group-addon"> h <i class="text--red">*</i>stawka</span>'
                                            +'<input type="text" name="Over['+iOver+'][rate]" class="form-control number" placeholder="Wpisz stawkę za godzinę" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
											+'<span class="input-group-addon"> <?= $model->currency ?>/h</span>'
                                        +'</div></div>');
		++iOver;
	}
    
    document.getElementById('add-bonus').onclick = function() {
		document.getElementById('rates-bonus').insertAdjacentHTML('beforeend','<div><div class="input-group input-group-sm">'
                                            +'<span class="input-group-addon removeForm" style="cursor: pointer;"><i class="fa fa-close text--red"></i></span>'
                                            +'<input type="hidden" name="Bonus['+iBonus+'][id]" value="0">'
                                            +'<span class="input-group-addon"><i class="text--red">*</i>Prermia </span>'
                                            +'<input type="text" name="Bonus['+iBonus+'][rate]" class="form-controlnumber " placeholder="Wpisz wartość premii" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon"><?= $model->currency ?> <i class="text--red">*</i>za</span>'
                                            +'<input type="text" name="Bonus['+iBonus+'][to_h]" class="form-control number" placeholder="Wpisz liczbę godzin" pattern="[0-9]+(\.[0-9][0-9]?)?" required>'
                                            +'<span class="input-group-addon"> h </span>'
                                        +'</div></div>');
		++iBonus;
	}
    
    
</script>