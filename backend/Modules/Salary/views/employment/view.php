<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\accounting\ActionsTable;
use frontend\widgets\accounting\InvoicesTable;
use frontend\widgets\accounting\SalaryTable;

use frontend\widgets\files\FilesBlock;

$listTypes = \backend\Modules\Salary\models\Employment::listTypes();
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = $model->employee['fullname']. ' - '. $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr wynagrodzeń'), 'url' => Url::to(['/salary/employment/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = 'Umowa z `'.$model->employee['fullname'].'`';
?>

<script src="/js/chart/Chart.bundle.js"></script>
<script src="/js/chart/utils.js"></script>
<div class="grid">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-purple rounded">
            <span class="mini-stat-icon"><i class="fa fa-calculator text--purple"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['salary']) ? number_format ($stats['salary'], 2, "." ,  " ") : 0 ?> </span> Wypłaty [<?= $model->currency ?>]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer"><?= ($stats['salary']) ? number_format ($stats['salary'], 2, "." ,  " ") : 0 ?> PLN</div> <?php } ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-blue rounded">
            <span class="mini-stat-icon"><i class="fa fa-hourglass text--blue"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['confirm_time']) ? $stats['confirm_time'] : 0 ?> </span> Czas [h]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer">&nbsp;</div> <?php } ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-yellow rounded">
            <span class="mini-stat-icon"><i class="fa fa-sun-o text--yellow"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['free_time']) ? $stats['free_time'] : 0 ?> </span> Urlop [h]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer">&nbsp;</div> <?php } ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-teal rounded">
            <span class="mini-stat-icon"><i class="fa fa-line-chart text--teal"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['confirm_all']==0) ? '0' : round($stats['salary']/$stats['confirm_all'],2) ?> </span> Śr. stawka [<?= $model->currency ?>]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer"><?= ($stats['confirm_all']==0) ? '0' : round($stats['salary']/$stats['confirm_all'],2) ?> PLN</div> <?php } ?>
        </div>
    </div> 
</div>

    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                <li><a data-toggle="tab" href="#tab2"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Dokumenty</span></a></li>
                <li><a data-toggle="tab" href="#tab3"><i class="fa fa-list-ul"></i><span class="panel-tabs--text">Wypłaty</span> </a></li>
                <!--<li><a data-toggle="tab" href="#tab3"><i class="fa fa-history"></i><span class="panel-tabs--text">Czynności</span> </a></li>
                <li><a data-toggle="tab" href="#tab4"><i class="fa fa-gift"></i><span class="panel-tabs--text">Premie</span> </a></li>-->
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <div class="grid">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="grid profile">
                                <div class="col-md-4 col-xs-12">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->id_employee_fk."/preview.jpg"))?"/uploads/employees/cover/".$model->id_employee_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="profile-name">
                                        <h3><a href="<?= Url::to(['/company/employee/view', 'id' => $model->id_employee_fk]) ?>" title="Przejdź do karty klienta"><?= $model->employee['fullname'] ?></a></h3>
                                        <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->employee['phone']) ? $model->employee['phone'] : 'brak danych' ?></p>
                                        <p class="job-title mb0"><i class="fa fa-envelope"></i> <?=  ($model->employee['email']) ?  Html::mailto($model->employee['email'], $model->employee['email']) : 'brak danych'  ?></p>
                                    </div>
                                </div>
                            </div>
                            <canvas id="canvas"></canvas>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div class="alert bg-<?= $listTypes['colors'][$model->type_fk] ?>2 text--<?= $listTypes['colors'][$model->type_fk] ?>"><b><?= $listTypes['names'][$model->type_fk] ?></b></div>
                            <div class="grid">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <fieldset><legend>Podstawa</legend>
                                        <ul class="list-group">
                                        <?php
                                            if($model->type_fk == 1) { 
                                                foreach($model->basictime as $key => $item) {
                                                    echo '<li class="list-group-item">' /*<span class="badge">14</span>*/
                                                            .'Ryczałt '.$item->rate_level.' <b class="text--teal">'.number_format($item->rate,2, '.', ' ').'</b> '.$item->employment['currency'].' za <b class="text--navy">'.$item->hours_to.'</b> h'
                                                            .'<br /><small>Redukcja: <b class="text--purple">'.(($item->is_reduction) ? 'TAK' : 'NIE').'</b>'
                                                            .(($item->is_reduction && $item->no_less_than) ? ' [w kwocie nie mniejszej niż <b class="text--purple">'.number_format($item->no_less_than,2, '.', ' ').'</b> '.$item->employment['currency'].']</small>' : '</small>')
                                                        .'</li>';
                                                }
                                                if(!$model->basictime) echo '<div class="alert alert-info">brak danych</div>';
                                            } else {
                                                 echo '<li class="list-group-item">'
                                                            .(($model->type_fk == 2) ? 'Stawka stała' : 'Stawka godzinowa')
                                                            .' <b class="text--teal">'.number_format($model->rate_basic,2, '.', ' ').'</b> '.(($model->type_fk == 2) ? ' PLN' : ' PLN/h');
                                            }
                                        ?>
                                        </ul>
                                    </fieldset>
                                    <?php if($model->type_fk == 1) { ?>
									<fieldset><legend>Premia</legend>
                                        <ul class="list-group">
                                        <?php
                                            foreach($model->bonustime as $key => $item) {
                                                echo '<li class="list-group-item">'
                                                        .'W przypadku osiągnięcia <b class="text--teal">'.$item->hours_to.'</b> h premia <b class="text--pink">'.number_format($item->rate_bonus,2, '.', ' ') .'</b> '. $item->employment['currency']
                                                    .'</li>';
                                            }
											if(!$model->bonustime) echo '<div class="alert alert-info">brak danych</div>';
                                        ?>
                                        </ul>
                                    </fieldset>
                                    <?php } ?>
                                </div>
                                
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <?php if($model->type_fk == 1) { ?>
                                    <fieldset><legend>Nadgodziny</legend>
                                        <ul class="list-group">
                                        <?php
                                            foreach($model->overtime as $key => $item) {
                                                echo '<li class="list-group-item">Stawka <b class="text--orange">'.number_format($item->rate,2, '.', ' ').'</b> '.$item->employment['currency']
                                                    . (($item->hours_to) ? ' między <b class="text--navy">'.$item->hours_from.'</b> a <b class="text--navy">'.$item->hours_to.'</b> h</li>' : ' powyżej <b class="text--navy">'.$item->hours_from.'</b> h');
                                            }
											if(!$model->overtime) echo '<div class="alert alert-info">brak danych</div>';
                                        ?>
                                        </ul>
                                    </fieldset>
                                    <?php } else { ?>   
                                    <fieldset><legend>Premia</legend>
                                        <ul class="list-group">
                                        <?php
                                            foreach($model->bonustime as $key => $item) {
                                                echo '<li class="list-group-item">'
                                                        .'W przypadku osiągnięcia <b class="text--teal">'.$item->hours_to.'</b> h premia <b class="text--pink">'.number_format($item->rate_bonus,2, '.', ' ') .'</b> '. $item->employment['currency']
                                                    .'</li>';
                                            }
											if(!$model->bonustime) echo '<div class="alert alert-info">brak danych</div>';
                                        ?>
                                        </ul>
                                    </fieldset>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
 
                </div>
                <div class="tab-pane" id="tab2">
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 12, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
                <div class="tab-pane" id="tab3">
                    <?= SalaryTable::widget(['dataUrl' => Url::to(['/salary/default/employee', 'id' => $model->id]) ]) ?>
                </div>
                <!--
                <div class="tab-pane" id="tab4">
                    <?php /*$this->render('_discounts', ['model' => $model])*/ ?>
                </div>-->
            </div>
        </div>
    </div>
<script>
    var chartData =  {
        labels: [<?= $chart['labels'] ?>],
        datasets: [
            {
                data: [<?= implode(',', $chart['data']) ?>],
                backgroundColor: [<?= $chart['colors'] ?>],
                hoverBackgroundColor: [<?= $chart['colors'] ?>]
            }]
    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myMixedChart = new Chart(ctx, {
            type: 'doughnut',
            data: chartData,
            options: {
                legend: { display: false },
               /* tooltips: { enabled: false }*/
            }
        });
    };


</script>