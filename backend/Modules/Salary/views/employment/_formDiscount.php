<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => "#table-discounts"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="grid">

            <div class="col-xs-6">
                <div class="form-group field-accdiscount-acc_period">
                    <label for="accdiscount-acc_period" class="control-label">Okres rozliczeniowy</label>
                    <div class='input-group date' id='datetimepicker_discount_period'>
                        <input type='text' class="form-control" id="accdiscount-acc_period" name="AccDiscount[acc_period]" value="<?= $model->acc_period ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <script type="text/javascript">
					$(function () {
						$('#datetimepicker_discount_period').datetimepicker({ format: 'YYYY-MM',  });
					});
				</script>
            </div>
            <div class="col-xs-6"><?= $form->field($model, 'discount_percent', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">%</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number'] ) ?></div>
            <div class="col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>       
        </div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

