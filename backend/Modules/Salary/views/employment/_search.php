<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-employments-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-employments']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-12 col-xs-12"><?= $form->field($model, 'id_employee_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList($model->id_employee_fk), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-employments', 'data-form' => '#filter-acc-employments-search' ] ) ?></div>        
        <div class="col-md-2 col-sm-6 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-employments', 'data-form' => '#filter-acc-employments-search' ] ) ?> </div>
        <div class="col-md-2 col-sm-6 col-xs-6"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-employments', 'data-form' => '#filter-acc-employments-search' ] ) ?></div>        
        <div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Salary\models\Employment::listTypes()['names'], ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-employments', 'data-form' => '#filter-acc-employments-search' ] ) ?></div>        

        <div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-accorder-date_from">
				<label for="accorder-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="accorder-date_from" name="Employment[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-accorder-date_to">
				<label for="accorder-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="accorder-date_to" name="Accemploymentsearch[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <!--<div class="col-md-5 col-sm-12"><?= $form->field($model, 'symbol')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-employments', 'data-form' => '#filter-acc-employments-search' ]) ?></div>-->
    </div> 

    <?php ActiveForm::end(); ?>

</div>
