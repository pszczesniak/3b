<?php
    use yii\helpers\Url;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-filter', 'title'=>'Filtr')) ?>
    <form action="<?= Url::to(['/accounting/report/'.$report, 'period' => '0-'.$month]) ?>" method="post">
        <div class="input-group">
            <select class="form-control" name="year">
                <option <?= ($year == 2013) ? 'selected' : '' ?> value="2013">2013</option>
                <option <?= ($year == 2014) ? 'selected' : '' ?> value="2014">2014</option>
                <option <?= ($year == 2015) ? 'selected' : '' ?> value="2015">2015</option>
                <option <?= ($year == 2016) ? 'selected' : '' ?> value="2016">2016</option>
            </select>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit" data-original-title="Search"><i class="fa fa-search"></i></button>
            </span>
        </div><!-- /input-group -->
    </form>
    <ul class="nav nav-pills nav-stacked navbar-custom-nav">
        <li class=" <?= ($month == '01') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-01']) ?>"> <i class="fa fa-fw fa-calendar"></i> Styczeń  </a>
        </li>
        <li class=" <?= ($month == '02') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-02']) ?>"> <i class="fa fa-fw fa-calendar"></i> Luty </a>
        </li>
        <li class=" <?= ($month == '03') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-03']) ?>"> <i class="fa fa-fw fa-calendar"></i> Marzec </a>
        </li>
        <li class=" <?= ($month == '04') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-04']) ?>"> <i class="fa fa-fw fa-calendar"></i> Kwiecień </a>
        </li>
        <li class=" <?= ($month == '05') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-05']) ?>"> <i class="fa fa-fw fa-calendar"></i> Maj </a>
        </li>
        <li class=" <?= ($month == '06') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-06']) ?>"> <i class="fa fa-fw fa-calendar"></i> Czerwiec </a>
        </li>
        <li class=" <?= ($month == '07') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-07']) ?>"> <i class="fa fa-fw fa-calendar"></i> Lipiec </a>
        </li>
        <li class=" <?= ($month == '08') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-08']) ?>"> <i class="fa fa-fw fa-calendar"></i> Sierpień </a>
        </li>
        <li class=" <?= ($month == '09') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-09']) ?>"> <i class="fa fa-fw fa-calendar"></i> Wrzesień </a>
        </li>
        <li class=" <?= ($month == '10') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-10']) ?>"> <i class="fa fa-fw fa-calendar"></i> Październik </a>
        </li>
        <li class=" <?= ($month == '11') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-11']) ?>"> <i class="fa fa-fw fa-calendar"></i> Listopad </a>
        </li>
        <li class=" <?= ($month == '12') ? 'active' : '' ?>">
            <a href="<?= Url::to(['/accounting/report/'.$report, 'period' => $year.'-12']) ?>"> <i class="fa fa-fw fa-calendar"></i> Grudzień </a>
        </li>
    </ul>
<?php $this->endContent(); ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-filter', 'title'=>'Inne zestawienia')) ?>
    <ul class="list-group">
        <?php 
            if($report != 'employees')
                echo '<li class="list-group-item"><a href="'. Url::to(['/accounting/report/employees', 'period' => $year.'-'.$month]) .'">Pracownicy</a></li>';
            if($report != 'index')
                echo '<li class="list-group-item"><a href="'. Url::to(['/accounting/report/index', 'period' => $year.'-'.$month]) .'">Pracownicy - klienci</a></li>';
        ?>
    </ul>
<?php $this->endContent(); ?>