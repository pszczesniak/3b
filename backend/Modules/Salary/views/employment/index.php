<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wynagrodzenia');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="toolbar-employments" class="btn-group toolbar-table-widget">
        <?= (Yii::$app->params['env'] == 'test') ?   Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/salary/employment/createajax', 'id' => 0]) , 
                ['class' => 'btn btn-success btn-icon gridViewModal', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "item-form", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) : '' ?>
        <div class="btn-group">
            <button type="button" class="btn btn-icon btn-success"><i class="fa fa-plus"></i>umowę</button>
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" id="postingDates-list">
                <?= '<li>'
                       . '<a href="'.Url::to(['/salary/employment/createajax', 'id' => 0]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-employments" data-title="Ryczałt + nadgodziny">Ryczałt + nadgodziny</a>'
                    .'</li>' ?>
                <?= '<li>'
                       . '<a href="'.Url::to(['/salary/employment/createajax', 'id' => 0]).'?type=2" class="gridViewModal" data-target="#modal-grid-item" data-table="table-employments" data-title="Stawka stała">Stawka stała</a>'
                    .'</li>' ?>
                <?= '<li>'
                       . '<a href="'.Url::to(['/salary/employment/createajax', 'id' => 0]).'?type=3" class="gridViewModal" data-target="#modal-grid-item" data-table="table-employments" data-title="Stawka godzinowa">Stawka godzinowa</a>'
                    .'</li>' ?>
                <!--<li role="separator" class="divider"></li>-->
            </ul>
        </div>
        <?php  /*Html::a('<i class="fa fa-print"></i>Export', Url::to(['/salary/employment/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-salary-employments-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ])*/ ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-employments"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-employments"
                data-toolbar="#toolbar-employments" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number="<?= $setting['page'] ?>"
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="employee"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-acc-employments-search"
                data-url=<?= Url::to(['/salary/employment/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>  
					<th data-field="employee"  data-sortable="true" >Pracownik</th>
					<th data-field="name"  data-sortable="true" >Nazwa</th>
					<th data-field="currency"  data-sortable="false" data-width="30px" data-align="center">Waluta</th>
                    <th data-field="date_from"  data-sortable="false" data-width="40px">Obowiązuje</th>
                    <th data-field="type" data-sortable="false" data-align="center">Typ</th>
                    <th data-field="files"  data-sortable="false" data-align="center"><i class="fa fa-paperclip"></i></th>

                    <th data-field="actions" data-events="actionEvents" data-sortable="false" data-align="center" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>  
        </table>  
    </div>

<?php $this->endContent(); ?>
    
