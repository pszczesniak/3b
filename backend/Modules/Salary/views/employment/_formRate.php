<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-event", 'data-table' => "#table-rates"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="grid">
            <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'unit_price', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">'.$model->order['currency'].'/h</span> </div>  {error}{hint} '])->textInput( [] ) ?></div>

            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="form-group field-accrate-date_from">
                    <label for="accrate-date_from" class="control-label">Obowiązuje od</label>
                    <div class='input-group date' id='datetimepicker_start'>
                        <input type='text' class="form-control" id="accrate-date_from" name="AccRate[date_from]" value="<?= $model->date_from ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <script type="text/javascript">
					$(function () {
						$('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
					});
				</script>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="form-group field-accrate-date_to">
                    <label for="accrate-date_to" class="control-label">Obowiązuje do</label>
                    <div class='input-group date' id='datetimepicker_end'>
                        <input type='text' class="form-control" id="accrate-date_to" name="AccRate[date_to]" value="<?= $model->date_to ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <script type="text/javascript">
					$(function () {
						$('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD',  });
					});
				</script>
            </div>
            <div class="col-xs-6"><?= $form->field($model, 'id_dict_employee_type_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listTypes(), ['prompt' => ' - wybierz -'] ) ?> </div>
            <div class="col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListByType($model->id_dict_employee_type_fk), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>       
        </div>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

    document.getElementById('accrate-id_dict_employee_type_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/employee/employeesbytype']) ?>/"+((this.value)?this.value:0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accrate-id_employee_fk').innerHTML = result.employees; 
            }
        }
       xhr.send();
       
       return false;
    }
</script>
