<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxFormWithAttach', 'data-target' => "#modal-grid-item", 'data-table' => "#table-salary"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <fieldset><legend>Czas do rozliczenia wynagrodzenia</legend>
            <div class="grid">    
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'hours_customer_c', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} <small class="text--purple">W systemie: '.$model->hours_customer.' h</small>'])->textInput( ['class' => 'form-control number'] ) ?>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'hours_own_c', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} <small class="text--purple">W systemie: '.$model->hours_own.' h</small>'])->textInput( ['class' => 'form-control number'] ) ?>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'hours_free_c', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} <small class="text--purple">W systemie: '.$model->hours_free.' h</small>'])->textInput( ['class' => 'form-control number'] ) ?>
                </div>
            </div>
        </fieldset>
        <div class="form-group field-is_customize">
            <label> przeliczaj wg wprowadzonych ręcznie godzin</label>
            <input id="is_customize" class="chk" name="EmploymentSalary[is_customize]" value="1" type="checkbox" <?= ($model->is_customize) ? ' checked' : '' ?>>
            <div class="help-block"></div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'submitButton']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>


<script type="text/javascript">
	
   
</script>