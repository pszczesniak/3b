<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
    //'action' => Url::to(['/salary/period/send', 'id' => $model->id]),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-salary", 'data-type' => 'send'],
]); ?>
    <div class="modal-body">
        <div class="grid">
            <div class="col-sm-6">
                <table class="table table-hover">
                    <thead><tr><th>Pracownik</th><th>Specyfikacje</th></tr></thead>
                    <tbody>
                    <?php
                        foreach($data as $key => $item) {
                            echo '<tr><td>'.$item['lastname'].' '.$item['firstname'].'</td><td align="center">'.$item['total_actions'].'</td></tr>';
                        }
                    ?>
                    </tbody>
               </table>
            </div>
            <div class="col-sm-6">
                <fieldset><legend>Treść wiadomości</legend>
                    <div class="form-group">
                        <textarea rows="6" name="user-comment" class="form-control" id="user-comment" placeholder="Wpisz notatkę..."><?= str_replace("{%period}", $model->period_year."-".( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month), Yii::$app->params['periodSendInfo'])  ?></textarea>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
            
		
    <div class="modal-footer"> 
        <?= Html::submitButton('Wyślij informację', ['class' =>'btn btn-info periodSendBtn'])  ?>

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

