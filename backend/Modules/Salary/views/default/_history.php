<?php
    if(!$history) {
        echo '<div class="alert alert-info">Żadna informacja w kwestii tej wypłaty nie została jeszcze wysłana</div>';
    } else {
        echo '<table class="table table-hover">';
            echo '<thead><tr><td>Pracownik</td><td>Akcja</td><td>Data</td><td>Wiadomość</td></thead>';
            echo '<tbody>';
                foreach($history as $key => $value) {
                    echo '<tr><td>'.$value['fullname'].'</td>'
                            .'<td>'.$value['action'].'</td>'
                            .'<td>'.$value['date'].'</td>'
                            .'<td>'.$value['message'].'</td>'
                        .'</tr>';
                }
            echo '</tbody>';
        echo '</table>';
    }
?>