<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-salary-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-salary']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-3 col-xs-6">
			<div class="form-group field-salary-period">
				<label for="salary-period" class="control-label">Okres</label>
				<div class='input-group date' id='datetimepicker_period' data-table="#table-salary">
					<input type='text' class="form-control" id="salary-period" name="EmploymentSalary[period]" value="<?= $model->period ?>" data-default="<?= $model->period ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-3 col-xs-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Salary\models\Employment::listTypes()['names'], ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-salary', 'data-form' => '#filter-acc-salary-search' ] ) ?></div>        
        <div class="col-md-4 col-sm-3 col-xs-12"><?= $form->field($model, 'id_employment_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Salary\models\Employment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-salary', 'data-form' => '#filter-acc-salary-search'  ] ) ?></div>        
        <div class="col-md-4 col-sm-3 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-salary', 'data-form' => '#filter-acc-salary-search'  ] ) ?></div>                

        <!--<div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'status')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getOrderStates(), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-salary', 'data-form' => '#filter-acc-salary-search' ] ) ?></div>                       
        <div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'verify')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getOrderVerify(), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-salary', 'data-form' => '#filter-acc-salary-search' ] ) ?></div>  -->                             
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    
    
    
</script>