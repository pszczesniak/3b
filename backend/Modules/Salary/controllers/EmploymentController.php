<?php

namespace app\Modules\Salary\controllers;

use Yii;
use backend\Modules\Salary\models\Employment; 
use backend\Modules\Salary\models\EmploymentConstant; 
use backend\Modules\Salary\models\EmploymentRate;
use backend\Modules\Salary\models\EmploymentSalary;
use backend\Modules\Salary\models\EmploymentBonus;   

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;
use yii\web\UploadedFile;

/**
 * EmploymentController implements the CRUD actions for AccActions model.
 */
class EmploymentController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new Employment();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.employment'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }  
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

		//$fieldsDataQuery = $fieldsData = CalCase::find()->where(['status' => 1, 'type_fk' => 1]);
        $post = $_GET;
        if(isset($_GET['Employment'])) { 
            $params = $_GET['Employment']; 
            \Yii::$app->session->set('search.employment', ['params' => $params, 'post' => $post]);
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "id_employee_fk in (select id_employee_fk from {{%employee_department}} where status >= 0 and id_department_fk =  ".$params['id_department_fk'] .")");
            }
            if(isset($params['id_currency_fk']) && !empty($params['id_currency_fk']) ) {
                array_push($where, "id_currency_fk = ".$params['id_currency_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "e.type_fk = ".$params['type_fk']);
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "date_from >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) { 
                array_push($where, "IFNULL(date_to,'".date('Y-m-d')."') <= '".$params['date_to']."'");
            }
        }  	
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "id_employee_fk in ( select id_employee_fk from {{%employee_department}} where id_department_fk in (".implode(',', $this->module->params['managers']).") )");
        }
			
		$fields = [];
		$tmp = [];
		
		$sortColumn = 'ce.lastname collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'e.name collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['e.id as id', 'e.type_fk as type_fk', 'e.name as name', 'date_from', 'date_to', 'ce.id as eid',
                      "concat_ws(' ', lastname, firstname) as employee_name", 'currency_symbol',
                     // "concat_ws(' ', u.lastname, u.firstname) as uname",
                      "(select count(*) from {{%employment_salary}} where id_employment_fk = e.id and status = 1) as salary",
                      "(select min(id) from {{%employment}} where id_parent_fk = e.id and status = 1) as annex",
                      "(select count(*) from {{%files}} where id_fk = e.id and status = 1 and id_type_file_fk = 12) as files"])
            ->from('{{%employment}} e')
            ->join('JOIN', '{{%company_employee}} ce', 'ce.id = e.id_employee_fk')
            ->join('JOIN', '{{%acc_currency}} cur', 'cur.id = e.id_currency_fk')
			//->join('JOIN', '{{%user}} u', 'u.id = o.created_by')
            ->where( ['e.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%employment}} e')
            ->join('JOIN', '{{%company_employee}} ce', 'ce.id = e.id_employee_fk')
            ->where( ['e.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; 
 
		$tmp = [];
		$invoices = 0;
        $listTypes = Employment::listTypes();
        
		foreach($rows as $key=>$value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['name'] = '<a href="'.Url::to(['/salary/employment/view', 'id' => $value['id']]).'">'.$value['name'].'</a>';
            if($value['annex']) {
                $tmp['name'] .= '<br /><small><a href="'.Url::to(['/salary/employment/updateajax', 'id' => $value['annex']]).'" data-toggle="modal" data-target="#modal-grid-item" class="text--purple gridViewModal" tile="zobacz aneks">zobacz aneks</a></small>';
            }
			$tmp['employee'] = '<a href="'.Url::to(['/company/employee/view', 'id' => $value['eid']]).'">'.$value['employee_name'].'</a>';
			$tmp['start'] = $value['date_from'];
            $tmp['type'] = '<small class="btn btn-xs btn-break bg-'.$listTypes['colors'][$value['type_fk']].'">'.$listTypes['names'][$value['type_fk']].'</small>';
            $tmp['currency'] = $value['currency_symbol'];
            $tmp['date_from'] = $value['date_from'];
            $tmp['date_from'] .= ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? '<br /><small class="text--red"> do '.$value['date_to'].'</small>' : (($value['date_to'])? ' <br ><small>do '.$value['date_to'].'</small>':'');
			//$tmp['creator'] = $value['uname'];
            $tmp['files'] =  '<span class="'.(($value['files'] == 0) ? 'text--grey' : 'bg-blue label').'">'.$value['files'] .'</span>';
           
            $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . '<li class="update" data-target="#modal-grid-item" data-table="#table-employments" href="'.Url::to(['/salary/employment/updateajax', 'id' => $value['id']]).'"><a href="'.Url::to(['/salary/employment/updateajax', 'id' => $value['id']]).'"><i class="fa fa-pencil text--blue"></i>&nbsp;Edycja</a></li>'
                                       // . ( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && !$value['annex']) ? '<li class="update" data-target="#modal-grid-item" data-table="#table-employments" href="'.Url::to(['/salary/employment/annex', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-level-up\'></i>'.Yii::t('app', 'Aneks').'"><a href="#"><i class="fa fa-level-up text--purple"></i>&nbsp;Aneks</a></li>' : '')                                        
                                       // . '<li class="update" data-table="#table-employments" href="'.Url::to(['/salary/emplyment/bonuses', 'id' => $value['id']]).'" data-target="#modal-grid-item" data-title="<i class=\'fa fa-gift\'></i>'.Yii::t('app', 'Bonusy').'" data-label="Bonusy"><a href="#"><i class="fa fa-gift text--pink"></i>&nbsp;Premie</a></li>' 
                                        . ( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<li class="deleteConfirm" data-table="#table-employments" href="'.Url::to(['/salary/employment/deleteajax', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>Usuń" data-target="#modal-grid-item"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')

                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
           
			$tmp['className'] = ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionCreateajax($id) {
        $id = CustomHelpers::decode($id);
        $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($id);
        
        $activeTab = 'info';
        
        $basictime = [ 0 => new EmploymentConstant() ]; 
        $overtime = [ 0 => new EmploymentRate() ];
		$bonustime = []; //[ 0 => new EmploymentBonus() ];
        
		$model = new Employment; 
		$model->status = 1;
        $model->name = 'Wynagrodzenie podstawowe';
        $model->id_employee_fk = ($employee) ? $employee->id : 0;
        $model->type_fk = (isset($_GET['type']) && !empty($_GET['type'])) ? $_GET['type'] : 1;
        $model->date_from = '2017-09-01';
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs'); 
			$model->load(Yii::$app->request->post());
			$basictime = [];
            if(isset($_POST['Basic'])) {
                foreach($_POST['Basic'] as $key => $value) {
                    $tempBasic = new EmploymentConstant();
                    $tempBasic->rate = $value['rate'];
                    $tempBasic->hours_from = 0;
                    $tempBasic->hours_to = $value['hours'];
                    $tempBasic->is_reduction = (isset($value['reduction']) && $value['reduction'] == 'on') ? 1 : 0;
                    $tempBasic->rate_between = isset($value['between']) ? $value['between'] : 0;
                    $tempBasic->no_less_than = isset($value['min_value']) ? $value['min_value'] : 0;
                    array_push($basictime, $tempBasic);
                }
			}
			$overtime = [];
            if(isset($_POST['Over'])) {
                foreach($_POST['Over'] as $key => $value) {
                    $tempOver = new EmploymentRate();
                    $tempOver->rate = $value['rate'];
                    $tempOver->hours_from = $value['from_h'];
                    $tempOver->hours_to = $value['to_h'];
                    array_push($overtime, $tempOver);
                }
            }
            $bonustime = [];
            if(isset($_POST['Bonus'])) {
                foreach($_POST['Bonus'] as $key => $value) {
                    $tempBonus = new EmploymentBonus();
                    $tempBonus->rate_bonus = $value['rate'];
                    $tempBonus->hours_from = 0;
                    $tempBonus->hours_to = $value['to_h'];
                    array_push($bonustime, $tempBonus);
                }
            }
            
			$transaction = \Yii::$app->db->beginTransaction();
			try {
				if( $model->validate() && $model->save()) {
					foreach($basictime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->rate_level = $key + 1;
						$item->status = 1;
                        $item->name = 'Ryczałt: '.$item->hours_to.' h';
						if(!$item->save()) {
                            $transaction->rollBack();
                            $activeTab = 'basic';
                            $model->isNewRecord = true;
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						}
					}
					foreach($overtime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->status = 1;
						if(!$item->save()) {
							$transaction->rollBack();
                            $activeTab = 'over';
                            $model->isNewRecord = true;
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						}
					}
                    foreach($bonustime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->status = 1;
						if(!$item->save()) {
							$transaction->rollBack();
                            $activeTab = 'bonus';
                            $model->isNewRecord = true;
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						}
					}
					$transaction->commit();
					return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Umowa została zapisana'  );	
					
				} else {
					return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $model->getErrors() );	
				}
			} catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $model->getErrors() );	
            }
		} else {
            return  $this->renderAjax('_formAjax'.($model->type_fk), [ 'model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]);	
        }
	}
    
    public function actionUpdateajax($id) {
        $id = CustomHelpers::decode($id);
        
        $activeTab = 'info';
        
        $basictime = []; 
        $overtime = [];
		$bonustime = [];
        
        $basicIds = [0 => 0]; $overIds = [0 => 0]; $bonusIds = [0 => 0];
        
		$model = Employment::findOne($id); 
        
        foreach($model->basictime as $key => $modelBasictime) {
            array_push($basictime, $modelBasictime);
        }
        
        foreach($model->overtime as $key => $modelOvertime) {
            array_push($overtime, $modelOvertime);
        }
        
        foreach($model->bonustime as $key => $modelBonustime) {
            array_push($bonustime, $modelBonustime);
        }
		/*$model->status = 1;
        $model->name = 'Wynagrodzenie podstawowe';
        $model->id_employee_fk = ($employee) ? $employee->id : 0;
        $model->type_fk = 1;
        $model->date_from = '2017-09-01';*/
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());
			$basictime = [];
			if(isset($_POST['Basic'])) {
                foreach($_POST['Basic'] as $key => $value) {
                    if($value['id']) {
                        $tempBasic = EmploymentConstant::findOne($value['id']);
                        array_push($basicIds, $value['id']);
                    } else {
                        $tempBasic = new EmploymentConstant();
                    }
                    $tempBasic->rate = $value['rate'];
                    $tempBasic->hours_from = 0;
                    $tempBasic->hours_to = $value['hours'];
                    $tempBasic->is_reduction = (isset($value['reduction']) && $value['reduction'] == 'on') ? 1 : 0;
                    $tempBasic->rate_between = isset($value['between']) ? $value['between'] : 0;
                    $tempBasic->no_less_than = isset($value['min_value']) ? $value['min_value'] : 0;
                    array_push($basictime, $tempBasic);
                }
			}
            
			$overtime = [];
            if(isset($_POST['Over'])) {
                foreach($_POST['Over'] as $key => $value) {
                    if($value['id']) {
                        $tempOver = EmploymentRate::findOne($value['id']);
                        array_push($overIds, $value['id']);
                    } else {
                        $tempOver = new EmploymentRate();
                    }
                    $tempOver->rate = $value['rate'];
                    $tempOver->hours_from = $value['from_h'];
                    $tempOver->hours_to = $value['to_h'];
                    array_push($overtime, $tempOver);
                }
            }
            
            $bonustime = [];
            if(isset($_POST['Bonus'])) {
                foreach($_POST['Bonus'] as $key => $value) {
                    if($value['id']) {
                        $tempBonus = EmploymentBonus::findOne($value['id']);
                        array_push($bonusIds, $value['id']);
                    } else {
                        $tempBonus = new EmploymentBonus();
                    }
                    $tempBonus->rate_bonus = $value['rate'];
                    $tempBonus->hours_from = 0;
                    $tempBonus->hours_to = $value['to_h'];
                    array_push($bonustime, $tempBonus);
                }
            }
            
			$transaction = \Yii::$app->db->beginTransaction();
			/* delete */
            $connection = Yii::$app->getDb();
            $sqlDeleteBasic = "update {{%employment_constant}} set status = -1, deleted_at = now(), deleted_by = ".Yii::$app->user->id." where id_employment_fk = ".$model->id." and id not in (".implode(',', $basicIds).")";
            $connection->createCommand($sqlDeleteBasic)->execute();
            
            $sqlDeleteOver = "update {{%employment_rate}} set status = -1, deleted_at = now(), deleted_by = ".Yii::$app->user->id." where id_employment_fk = ".$model->id." and id not in (".implode(',', $overIds).")";
            $connection->createCommand($sqlDeleteOver)->execute();
            
            $sqlDeleteBonus = "update {{%employment_bonus}} set status = -1, deleted_at = now(), deleted_by = ".Yii::$app->user->id." where id_employment_fk = ".$model->id." and id not in (".implode(',', $bonusIds).")";
            $connection->createCommand($sqlDeleteBonus)->execute();
                    
            try {
				if( $model->validate() && $model->save()) {
					foreach($basictime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->rate_level = $key + 1;
						$item->status = 1;
                        $item->name = 'Ryczałt: '.$item->hours_to.' h';
						if(!$item->save()) {
                            $transaction->rollBack();
                            $activeTab = 'basic';
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						} else {
                            array_push($basicIds, $item->id);
                        }
					}
					foreach($overtime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->status = 1;
						if(!$item->save()) {
							$transaction->rollBack();
                            $activeTab = 'over';
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						} else {
                            array_push($overIds, $item->id);
                        }
					}
                    foreach($bonustime as $key => $item) {
						$item->id_employment_fk = $model->id;
						$item->status = 1;
						if(!$item->save()) {
							$transaction->rollBack();
                            $activeTab = 'bonus';
                            return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $item->getErrors() );	
						} else {
                            array_push($bonusIds, $item->id);
                        }
					}
					$transaction->commit();
                    
                    return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Umowa została zapisana'  );	
					
				} else {
					return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $model->getErrors() );	
				}
			} catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax'.($model->type_fk), ['model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]), 'errors' => $model->getErrors() );	
            }
		} else {
            return  $this->renderAjax('_formAjax'.($model->type_fk), [ 'model' => $model, 'basictime' => $basictime, 'overtime' => $overtime, 'bonustime' => $bonustime, 'activeTab' => $activeTab]);	
        }
	}
       
    public function actionAnnex($id) {
		$order = $this->findModel($id);

        $model = new AccOrder();
        $model->id_parent_fk = $order->id;
        $model->symbol = $order->symbol;
        $model->name = $order->name;
        $model->date_from = date('Y-m-d');
        $model->id_customer_fk = $order->id_customer_fk;
        $model->id_dict_order_type_fk = $order->id_dict_order_type_fk;
        $model->id_settlement_type_fk = $order->id_settlement_type_fk;
        $model->id_currency_fk = $order->id_currency_fk;
        $model->customers = [];
        $customersTemp = [];
       
        foreach($order->items as $key => $item) {
            array_push($model->customers, $item->id_customer_fk);
            array_push($customersTemp, $item->id_customer_fk);
        }
                   
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            $oldOrderInUse = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_order_fk' => $order->id])->andWhere("action_date > '".(date('Y-m-d', strtotime('-1 day', strtotime($model->date_from))))."'")->max('action_date');
            if($oldOrderInUse) {
                return array('success' => false, 'html' => $this->renderAjax('_formAnnex', ['model' => $model]), 'errors' => ['order' => 'W systemie są czynności z dnia '.$oldOrderInUse.' powiązane ze zleceniem i nie można go zamknąć z datą '.(date('Y-m-d', strtotime('-1 day', strtotime($model->date_from))))] );	
            }
            
            if(!$model->customers) $model->customers = [];
			
			foreach($model->customers as $key => $customer) {
				if($customer == $model->id_customer_fk)
					unset($model->customers[$key]);
			}
			if(!$model->customers) $model->customers = [];
            $model->type_fk = ( count($model->customers) == 0 ) ? 1 : 2;
            if( $model->validate() && $model->save()) {
                
                foreach($customersTemp as $key => $value) {
                    if(!in_array($value, $model->customers)) {                      
						//return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						$exist = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->count();
						if($exist > 0) {
							$customer = \backend\Modules\Crm\models\Customer::findOne($value);
							return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						} else {						
							$orderItem = AccOrderItem::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->one();
							$orderItem->status = 0;
							$orderItem->user_closed = Yii::$app->user->id;
							$orderItem->date_closed = date('Y-m-d H:i:s');
							$orderItem->save();
						}
                    }
                }
                
                foreach($model->customers as $key => $value) {
                    if(!in_array($value, $customersTemp)) {
                        $modelScale = new AccOrderItem;     
                        $modelScale->id_order_fk = $model->id;
                        $modelScale->id_customer_fk = $value;
                        $modelScale->save();
                    }
                }
                
                $order->date_to = date('Y-m-d', strtotime('-1 day', strtotime($model->date_from)));
                $order->save();
                
				return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Zmiany zostały zapisane', 
                            'rates' => $this->renderPartial('_rates', ['model' => $model]), 'ratesTable' => (($model->id_dict_order_type_fk == 2 || $model->id_dict_order_type_fk == 4) ? 1 : 0  ));	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAnnex', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAnnex', [ 'model' => $model]);	
        }
	}
    
    public function actionView($id) {

        $model = $this->findModel($id);
        $chart = ['labels' => '', 'colors' => '', 'data' => []];
        $colors = Yii::$app->params['colors'];
        $sql = "select sum(hours_admin) as hours_admin, "
              ."sum(hours_own) as hours_own, "
			  ."sum(hours_free) as hours_free, "
              ."sum(hours_customer) as hours_customer, "
			  ."sum(net_value) as salary "
              ." from {{%employment_salary}} a " 
              ." where status = 1 and id_employment_fk = ".$model->id;
        $statsChart = Yii::$app->db->createCommand($sql)->queryOne(); 
        //var_dump($statsChart);exit;
        $iColor = 0;
        foreach($statsChart as $key => $value) {
            /*if($key == 'hours_admin' && $value) {
                $chart['labels'] .= "'Godziny administracyjne',";
                $chart['colors'] .= "'#DE1771',";
                array_push($chart['data'], $value);
            }*/
            if($key == 'hours_free' && $value) {
                $chart['labels'] .= "'Dni wolne',";
                $chart['colors'] .= "'#EFC109',";
                array_push($chart['data'], $value);
            }
            if($key == 'hours_customer' && $value) {
                $chart['labels'] .= "'Godziny na rzecz klienta',";
                $chart['colors'] .= "'#A287D4',";
                array_push($chart['data'], $value);
            }
            if($key == 'hours_siw' && $value) {
                $chart['labels'] .= "'Godziny na rzecz SIW',";
                $chart['colors'] .= "'#2DC5C7',";
                array_push($chart['data'], $value);
            }
            ++$iColor;
        }
        $chart['labels'] = substr($chart['labels'], 0, -1);
        $chart['colors'] = substr($chart['colors'], 0, -1);
        return $this->render('view', ['model' => $model, 'stats' => Employment::getStats($model->id), 'chart' => $chart] );
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }
    
	public function actionConvert($eid, $period) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $listTypes = Employment::listTypes();
        
        $salary = self::calculate($eid, $period);
		$model = Employment::findOne($eid);
		$row['details'] = '<small class="text--green">przeliczone</small>';
        $row['net_value'] = number_format($salary->net_value, 2, '.', ' ');
        $row['employment'] = '<a href="'.Url::to(['/salary/employment/view', 'id' => $eid]).'"  data-placement="left" data-toggle="tooltip" data-title="Karta wynagrodzenia">'.$model->name
                            .'<br /><small class="btn btn-xs btn-break bg-'.$listTypes['colors'][$salary->employment['type_fk']].'">'.$listTypes['names'][$salary->employment['type_fk']].'</small>&nbsp;'
                            .'<a href="'. Url::to(['/task/personal/actions']).'?employeeId='.$model->id_employee_fk.'&period='.$period.'" class="btn btn-xs bg-blue btn-flat" data-toggle="tooltip" "data-title="Przejdź do czynności"><i class="fa fa-tasks"></i></a>'
                            .(($salary->id) ? '&nbsp;<a href="'. Url::to(['/salary/employment/export', 'id' => $salary->id, 'save' => false]) .'" class="btn btn-xs btn-default btn-flat export-file" data-toggle="tooltip" "data-title="Eksportuj do pliku Excel"><i class="fa fa-file-excel-o text--green"></i></a>' : '');
            
        if($salary->custom_options) {
            $details = \yii\helpers\Json::decode($salary->custom_options);
            $row['id'] = $salary->id;
            $row['net_value'] .= ((isset($details['basictime'])) ? '<br /><small class="text--purple no-break"><i class="fa fa-cog"></i>'.(($salary['constant']['is_reduction'] && $salary->net_value < $salary->constant['rate']) ? '<i class="fa fa-cut text--red"></i>' : '').'&nbsp;'.number_format($details['basictime'], 2, '.', ' ').'</small>' : '')
                                    . ((isset($details['overtime'])) ? '<br /><small class="text--blue no-break"><i class="fa fa-moon-o"></i>&nbsp;'.number_format($details['overtime'], 2, '.', ' ').'</small>' : '')
                                    . ((isset($details['bonustime'])) ? '<br /><small class="text--pink no-break"><i class="fa fa-gift"></i>&nbsp;'.number_format($details['bonustime'], 2, '.', ' ').'</small>' : '');
        }
		return ['success' => true, 'refresh' => 'inline', 'alert' => 'Wynagrodzenie za okres '.$period.' dla pracownika '.$model->employee['fullname'].' zostało przeliczone', 'row' => $row, 'table' => '#table-salary', 'index' => ((isset($_GET['index'])) ? $_GET['index'] : -1)];
	}
	
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        
        $existSalary = EmploymentSalary::find()->where(['status' => 1, 'id_employment_fk' => $model->id])->count();
        if($existSalary) {
            return array('success' => false, 'alert' => 'Nie można usunąć umowy wynagrodzenia, ponieważ są do niej przypisane wypłąty', 'table' => '#table-employments' );
        }
        $existEmployment = Employment::find()->where(['status' => 1, 'id_parent_fk' => $model->id])->count();
        if($existEmployment) {
            return array('success' => false, 'alert' => 'Nie można usunąć umowy wynagrodzenia, ponieważ jest do niej przypisany aneks', 'table' => '#table-employments' );
        }
        
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -2;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'table' => '#table-employments', 'refresh' => 'yes');	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Dane nie zostały usunięte', 'table' => '#table-employments', 'refresh' => 'yes' );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the Employment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = Employment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Wyngarodzenie o wskazanym identyfikatorze nie zostało odnalezione w systemie.');
        }
    }
    
    public static function calculate($id, $period) {
        $model = Employment::findOne($id);
        $salary = EmploymentSalary::find()->where(['status' => 1, 'id_employment_fk' => $model->id, 'period' => $period])->one();
        if(!$salary) { $salary = new EmploymentSalary(); $salary->net_value = 0;}
        $salary->id_employment_fk = $model->id;
        $salary->period = $period;
        $salary->status = 1;
        
        $details = [];
        
        /*$adminHours = \Yii::$app->db->createCommand("select sum(execution_time) as hoursCount from {{%cal_todo}} where status = 1 and id_employee_fk = ".$model->id_employee_fk." and id_dict_todo_type_fk in (1,2) and date_format(todo_date, '%Y-%m') = '".$period."'")
                    ->queryOne()['hoursCount'];*/
        $adminHours = 0;
        $freeHours = \Yii::$app->db->createCommand("select sum(execution_time) as hoursCount from {{%cal_todo}} where status = 1 and id_employee_fk = ".$model->id_employee_fk." and id_dict_todo_type_fk in (3,7) and date_format(todo_date, '%Y-%m') = '".$period."'")
                    ->queryOne()['hoursCount'];
        $customerHours = \Yii::$app->db->createCommand("select sum(unit_time) as hoursCount from {{%acc_actions}} where status = 1 and id_employee_fk = ".$model->id_employee_fk." and id_customer_fk not in (395, 704) and date_format(action_date, '%Y-%m') = '".$period."'") 
                    ->queryOne()['hoursCount'];
        $ownHours = \Yii::$app->db->createCommand("select sum(unit_time) as hoursCount from {{%acc_actions}} where status = 1 and id_employee_fk = ".$model->id_employee_fk." and id_customer_fk in (395, 704) and date_format(action_date, '%Y-%m') = '".$period."'") 
                    ->queryOne()['hoursCount'];
        
        $salary->hours_own = ($ownHours) ? round($ownHours/60, 2) : 0;
        $salary->hours_customer = ($customerHours) ? round($customerHours/60, 2) : 0;
        $salary->hours_free = ($freeHours) ? round($freeHours/60, 2) : 0;
        $salary->hours_admin = ($adminHours) ? round($adminHours/60, 2) : 0;
        
        if($salary->is_customize) {
            $customerHours = $salary->hours_customer_c*60;
            $ownHours = $salary->hours_own_c*60;
            $freeHours = $salary->hours_free_c*60;
        }
        
        $basicTimesArr = $model->basictime; $overTimeArr = $model->overtime; $bonusTimeArr = $model->bonustime;
        $constantModel = false; $rateOverConstant = false; $overtimeModel = false; $bonusModel = false; $netValue = 0;  $overValue = 0;    
        
        if($model->type_fk == 1) {
            foreach($basicTimesArr as $key => $basictime) {
                if( ( ($basictime->hours_to*60 >= ($adminHours + $freeHours + $customerHours + $ownHours)) || !isset($basicTimesArr[$key+1]) ) && !$constantModel) {
                    $constantModel = $basictime;
                    
                    if(isset($basicTimesArr[$key+1])) {
                        $rateOverConstant = $basicTimesArr[$key+1]->rate_between;
                    }
                    
                    break;
                } 
            }
            if($constantModel) {
                $details['basictime'] = $constantModel->rate;
                //echo $constantModel->hours_to*60 .' '. ($adminHours + $customerHours + $ownHours) .' '. ($adminHours + $customerHours + $freeHours + $ownHours);exit;
                if( ($constantModel->hours_to*60 == ($adminHours + $freeHours + $customerHours + $ownHours)) || 
                    ($constantModel->hours_to*60 > ($adminHours + $customerHours + $ownHours) && $constantModel->hours_to*60 < ($adminHours + $customerHours + $freeHours + $ownHours)) ) {
                    $netValue = $constantModel->rate;
                } else if($constantModel->hours_to*60 > ($adminHours + $freeHours + $customerHours + $ownHours) && $constantModel->is_reduction) {
                    $tempRate = $constantModel->rate / $constantModel->hours_to;
                    $netValue = round($tempRate * (($adminHours + $freeHours + $customerHours + $ownHours) / 60), 2);
                    if($netValue < $constantModel->no_less_than) $netValue = $constantModel->no_less_than;
                    $details['basictime'] = $netValue;
                    $details['reduction'] = true;
                } else if ($rateOverConstant) {
                    $netValue = $constantModel->rate + (( $constantModel->hours_to*60 - $adminHours - $freeHours - $customerHours - $ownHours )/60 * $rateOverConstant * -1);
                } else {
                    $netValue = $constantModel->rate;
                    $details['overtime'] = 0; $details['overtime_rates'] = [];
                    $alltime = $adminHours +  $customerHours + $ownHours;
                    $overtime = -1 * ($constantModel->hours_to*60 - $adminHours - $customerHours - $ownHours); 
                    $overtimeTemp = $overtime;
                    foreach($overTimeArr as $key => $item) {
                        if($item->hours_to && ($alltime >= $item->hours_from*60 && $alltime <= $item->hours_to*60)) {
                            $overValue += round($overtimeTemp/60 * $item->rate, 2);
                            $details['overtime'] += round($overtimeTemp/60 * $item->rate, 2);
                            $details['overtime_rates'][$item->id] = $details['overtime'];
                            $overtimeTemp = 0;
                        } else if($item->hours_to && ($alltime >= $item->hours_from*60 && $alltime >= $item->hours_to*60)) { 
                            $hoursInLimit = $item->hours_to - $item->hours_from + 1;
                            $overValue += round($hoursInLimit * $item->rate, 2);
                            $details['overtime'] += round($hoursInLimit * $item->rate, 2);
                            $details['overtime_rates'][$item->id] = round($hoursInLimit * $item->rate, 2);
                            $overtimeTemp -= $hoursInLimit;
                        } else if(!$item->hours_to && $alltime > $item->hours_from*60) { 
                            $hoursInLimit = $alltime - $item->hours_from*60;
                            //echo $hoursInLimit; exit;
                            $overValue += round($hoursInLimit/60 * $item->rate, 2);
                            $details['overtime'] += round($hoursInLimit/60 * $item->rate, 2);
                            $details['overtime_rates'][$item->id] = round($hoursInLimit/60 * $item->rate, 2);
                            $overtimeTemp -= $hoursInLimit;
                        }
                    }
                    //$netValue += $overValue;
                    //$salary->id_overtime_fk = $overtimeModel->id;
                    //$netValue = $constantModel->rate + round($overtime/60 * $overtimeModel->rate, 2);
                }
                $salary->id_constant_fk = $constantModel->id;
            } 
        } else if($model->type_fk == 2) {
            $netValue = $model->rate_basic;
        } else if($model->type_fk == 3) {
            $netValue = $model->rate_basic * ($customerHours + $ownHours);
        }
            
            
		$bonus = false;
		foreach($bonusTimeArr as $key => $item) {
			if($item->hours_to * 60 <= ($adminHours + $customerHours + $ownHours) && !$bonus) {
				$netValue = $netValue + $item->rate_bonus;
				$salary->id_bonus_fk = $item->id;
                $details['bonustime'] = $item->rate_bonus;
                $bonus = true;
			}
		}
        
		$salary->net_value = $netValue + $overValue;
		$salary->custom_options = \yii\helpers\Json::encode($details);
        
        if(!$salary->save()) { var_dump($salary->getErrors()); exit; }
        
        return $salary;
    }
    
    public function actionExport($id, $save) {
        $objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        $id = CustomHelpers::decode($id);
        $model = EmploymentSalary::findOne($id);
        if($model->custom_options) {
            $details = \yii\helpers\Json::decode($model->custom_options);
        } else {
            $details = [];
        }
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        
        /*$data = ['PRAWNIK', 'PODSTAWA (za ……… h)', 'SiW', 'liczba godzin na klienta  (oprócz SiW)','urlop/wolne obniżające ryczałt godzin','kwota za nadgodziny w przedziale ………-………… h', 'kwota za nadgodziny w przedziale …….- ……..',	
                 'kwota za nadgodziny w przedziale >…..', 'REDUKCJA', 'do wypłaty netto'];*/
        $data = [];
        $employmentType = 'PODSTAWA (za '.$model->constant['hours_to'].' h)';
        if($model->employment['type_fk'] == 2) $employmentType = 'Stawka stała';
        if($model->employment['type_fk'] == 3) $employmentType = 'Stawka godzinowa';
        
        $data[0] = [0 => 'PRACOWNIK', 1 => $employmentType, 2 => 'SIW', 3 => 'liczba godzin na klienta (oprócz SiW)'];
        if($model->is_customize)
            $data[1] = [0 => $model->employment['employee']['fullname'], 1 => (($model->employment['type_fk'] == 1)  ? $model->constant['rate'] : $model->employment['rate_basic']), 2 => $model->hours_own_c, 3 => $model->hours_customer_c];
        else
            $data[1] = [0 => $model->employment['employee']['fullname'], 1 => (($model->employment['type_fk'] == 1)  ? $model->constant['rate'] : $model->employment['rate_basic']), 2 => $model->hours_own, 3 => $model->hours_customer];
        
        if($model->employment['type_fk'] == 1) {
            array_push($data[0], 'urlop/wolne obniżające ryczałt godzin');
            array_push($data[1], (($model->is_customize) ? $model->hours_free_c : $model->hours_free));
        }
        
        $iter = ($model->employment['type_fk'] == 1) ? 5 : 3;
           
        foreach($model->employment['overtime'] as $key => $item) {
            array_push($data[0], 'kwota za nadgodziny w przedziale '.(($item->hours_to) ? ($item->hours_from.' - '.$item->hours_to) : ' > '.$item->hours_from).'');
            if(isset($details['overtime_rates']) && isset($details['overtime_rates'][$item->id]))
                array_push($data[1], $details['overtime_rates'][$item->id]);
            else
                array_push($data[1], 0);
            $objWorksheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($iter))->setWidth(25);
            ++$iter;
        }
        
        if($model->employment['type_fk'] == 1) {
            array_push($data[0], 'REDUKCJA'); array_push($data[1], (isset($details['reduction']) ? 'tak' : 'nie')); 
            $objWorksheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($iter))->setWidth(15);
        }
        
        if(isset($details['bonustime'])) {
             ++$iter;
            array_push($data[0], 'PREMIA'); array_push($data[1], $details['bonustime']); 
            $objWorksheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($iter))->setWidth(15);
        }
              
         ++$iter;
        array_push($data[0], 'do wypłaty netto'); array_push($data[1], $model->net_value); 
        $objWorksheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($iter))->setWidth(20);
        
        $objWorksheet->fromArray($data, NULL, 'A1', true);
        for($i = 0; $i <= $iter; ++$i) {
            $objWorksheet->getStyleByColumnAndRow($i, 1)->getFont()->setBold(true);
            $objWorksheet->getStyleByColumnAndRow($i, 1)->getAlignment()->setWrapText(true);
            $objWorksheet->getStyleByColumnAndRow($i, 1)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            if($i != $iter) {
                $objWorksheet->getStyleByColumnAndRow($i, 1)->getFill()->getStartColor()->setARGB('D9D9D9');
            } else {
                $objWorksheet->getStyleByColumnAndRow($i, 1)->getFill()->getStartColor()->setARGB('FFFFFACD');
                $objWorksheet->getStyleByColumnAndRow($i, 2)->getFont()->setBold(true);
            }
        }
        
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
        /*foreach($objPHPExcel->getActiveSheet()->getColumnDimension(1) as $col) {
            $col->setAutoSize(true);
            $col->getFont()->setBold(true);
        }*/
        //$objPHPExcel->getActiveSheet()->calculateColumnWidths();
        //$objPHPExcel->getActiveSheet()->getRowDimension(1)->getFont()->setBold(true);
		//$objPHPExcel->getActiveSheet()->getRowDimension(1)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		//$objPHPExcel->getActiveSheet()->getRowDimension(1)->getFill()->getStartColor()->setARGB('D9D9D9');	
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.\PHPExcel_Cell::stringFromColumnIndex($iter).'2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.\PHPExcel_Cell::stringFromColumnIndex($iter).'2')->getAlignment()->setWrapText(true);
        
        $filename = "wynagrodzenie_dla_".$model->employment['employee']['fullname'].".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //$objWriter->setIncludeCharts(TRUE);

        /*header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
        header("Content-disposition: attachment; filename=\"".$filename."\""); 
        header('Cache-Control: max-age=0');			
        $objWriter->save('php://output');*/
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save("uploads/temp/".$filename);
            
            return $filename;
        } else {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }
    }

}
