<?php

namespace backend\Modules\Salary\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employment_constant}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $order_level
 * @property integer $id_employment_fk
 * @property string $name
 * @property string $symbol
 * @property string $hours_from
 * @property string $hours_to
 * @property string $rate
 * @property string $rate_level
 * @property string $no_less_than
 * @property integer $is_bonus
 * @property integer $is_reduction
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class EmploymentConstant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employment_constant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'rate_level', 'id_employment_fk', 'is_bonus', 'is_reduction', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['hours_to', 'rate'], 'required'],
            [['hours_from', 'hours_to', 'rate', 'no_less_than'], 'number'],
            [['describe', 'custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 2000],
            [['symbol'], 'string', 'max' => 100],
            ['rate', 'checkRange'],
            ['no_less_than', 'checkNoLassThan', 'when' => function($model) { return ($model->no_less_than && $model->rate); } ],
        ];
    }
    
    public function checkNoLassThan(){
        if( $this->no_less_than > $this->rate ) {
            $this->addError('no_less_than', 'Redukacja wartości nie może być większa od kwoty ryczałtu');
        }
	}
    
    public function checkRange(){
        if( $this->rate && $this->hours_to ) {
            $isExistSql = "select count(*) isExist from {{%employment_constant}} where status = 1 and hours_to = ".$this->hours_to." and id_employment_fk = ".$this->id_employment_fk." and id != ".((!$this->isNewRecord) ? $this->id : 0);
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist)
                $this->addError('rate', 'Ryczałt dla '.$this->hours_to.' h jest już wprowadzony');
        }
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'rate_level' => Yii::t('app', 'Order Level'),
            'id_employment_fk' => Yii::t('app', 'Id Employment Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'hours_from' => Yii::t('app', 'Hours From'),
            'hours_to' => Yii::t('app', 'Liczba godzin w ryczałcie'),
            'rate' => Yii::t('app', 'Stawka ryczałtu'),
            'rate_level' => Yii::t('app', 'Rate Level'),
            'no_less_than' => Yii::t('app', 'Redukcja do wartości nie mniejszej niż'),
            'is_bonus' => Yii::t('app', 'Is Bonus'),
            'is_reduction' => Yii::t('app', 'Is Reduction'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if(empty($this->name)) $this->name = 'Ryczałt '.$this->hours_to;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    /*public function afterSave($insert, $changedAttributes) {  

    }*/
    
    public function getEmployment() {
		return $this->hasOne(\backend\Modules\Salary\models\Employment::className(), ['id' => 'id_employment_fk']);
    }
}
