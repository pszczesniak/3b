<?php

namespace backend\Modules\Salary\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employment_bonus}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_employment_fk
 * @property string $name
 * @property string $symbol
 * @property string $hours_from
 * @property string $hours_to
 * @property string $rate_bonus
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class EmploymentBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employment_bonus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_employment_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['hours_to', 'rate_bonus'], 'required'],
            [['hours_to', 'rate_bonus'], 'number'],
            [['describe', 'custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 2000],
            [['symbol'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_employment_fk' => Yii::t('app', 'Id Employment Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'hours_from' => Yii::t('app', 'Hours From'),
            'hours_to' => Yii::t('app', 'Limit godzin'),
            'rate_bonus' => Yii::t('app', 'Wartość premii'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		//if(empty($this->date_to)) $this->date_to = null;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    /*public function afterSave($insert, $changedAttributes) {  
        
    }*/
	
	public function getEmployment() {
		return $this->hasOne(\backend\Modules\Salary\models\Employment::className(), ['id' => 'id_employment_fk']);
    }
}
