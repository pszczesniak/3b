<?php

namespace backend\Modules\Salary\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employment}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_employee_fk
 * @property integer $id_currency_fk
 * @property string $name
 * @property string $symbol
 * @property string $date_from
 * @property string $date_to
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */  
class Employment extends \yii\db\ActiveRecord
{
    public $id_department_fk;
	public $attachs;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_parent_fk', 'id_employee_fk', 'id_currency_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_employee_fk', 'id_currency_fk', 'date_from', 'name'], 'required'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at', 'attachs'], 'safe'],
            [['describe', 'custom_options'], 'string'],
            [['rate_basic'], 'number'],
            [['name'], 'string', 'max' => 2000],
            [['symbol'], 'string', 'max' => 100],
            ['date_from', 'checkDates'/*, 'when' => function($model) { return ($model->no_less_than && $model->rate); }*/ ],
            ['rate_basic', 'required', 'when' => function($model) { return ($model->type_fk == 2); }, 'message' => 'Proszę podać wartość stałej stawki wynagrodzenia' ],
            ['rate_basic', 'required', 'when' => function($model) { return ($model->type_fk == 3); }, 'message' => 'Proszę podać wartość godzinowej stawki wynagrodzenia' ],
        ];
    }
    
    public function checkDates(){

        if( $this->date_to ) {
            if($this->date_to <= $this->date_from) {
                $this->addError('date_to', '`Data zakończenia` nie może być mniejsza od `daty rozpoczęcia`');
                return true;
            }
        } else {
            $isExistSql = "select count(*) isExist from {{%employment}} where status = 1 and date_to is null and id_employee_fk = ".$this->id_employee_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0);
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('date_from', 'Błędny zakres. Dla tego pracownika już istnieje jedna umowa wynagrodzenia z okresem otwarty.');
                return true;
            }
        }
        if ( $this->date_from && $this->date_to ) {
            $isExistSql = "select count(*) isExist from {{%employment}} where status = 1 and date_to is not null and id_employee_fk = ".$this->id_employee_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0)
                            ." and (date_from between ".$this->date_from." and ".$this->date_to." or date_to between ".$this->date_from." and ".$this->date_to.")";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('date_from', 'Błędny zakres. W tym okresie pracownik ma już przypisaną umowę.');
                return true;
            }
            
            $isExistSql = "select count(*) isExist from {{%employment}} where status = 1 and date_to is null and id_employee_fk = ".$this->id_employee_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0)
                            ." and (date_from > ".$this->date_to." )";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('date_from', 'Błędny zakres. W tym okresie pracownik ma już przypisaną umowę.');
                return true;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Typ'),
            'id_employee_fk' => Yii::t('app', 'Employee'),
            'id_currency_fk' => Yii::t('app', 'Currency'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_department_fk' => 'Dział'
        ];
    }
    
    public function beforeSave($insert) {
        //var_dump($this->authority_carrying);exit;
		if(empty($this->date_to)) $this->date_to = null;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {  
        if ($insert && $this->attachs) {
            foreach($this->attachs as $key => $file) {
                if($file) {
                    $modelFile = new \common\models\Files(['scenario' => 'save']);
                    $modelFile->extension_file = $file->extension;
                    $modelFile->size_file = $file->size;
                    $modelFile->mime_file = $file->type;
                    $modelFile->name_file = $file->baseName;
                    $modelFile->systemname_file = Yii::$app->getSecurity()->generateRandomString(10).'_'.date('U');
                    $modelFile->title_file = $file->baseName.'.'.$file->extension;
                    $modelFile->id_fk = $this->id;
                    $modelFile->id_type_file_fk = 12;
                    $modelFile->id_dict_type_file_fk = 0;
                    $modelFile->type_fk = 0;
                    
                    if($modelFile->save()) {
                        /*$modelFile->saveAs('uploads/' . $file->baseName . '.' . $file->extension);*/
                        try {
                            $file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                        } catch (\Exception $e) {
                            var_dump($e); exit;
                        }
                    }
                }
            }
        } 
    }
    
    public static function getTypes() {
        return [1 => 'Umowa o pracę', 2 => 'Stawka godzinowa'];
    }
    
    public static function getStats($id) {
        $sql = "select round(sum(hours_own+hours_customer), 2) as confirm_time, "
              ."round(sum(hours_own+hours_customer+hours_free), 2) as confirm_all, "
			  ."round(sum(hours_free),2) as free_time, "
			  ."round(sum(net_value),2) as salary "
              ." from {{%employment_salary}} a " 
              ." where status = 1 and id_employment_fk = ".$id;
        $stats = Yii::$app->db->createCommand($sql)->queryOne(); 
        
        return $stats;
    }
    
    public function getEmployee() {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public function getCurrency() {
		$currencies = [1 => 'PLN', 2 => 'EUR', 3 => 'USD'];
		
		return (isset($currencies[$this->id_currency_fk])) ? $currencies[$this->id_currency_fk] : 'PLN';
	}
    
    public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 12])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getBasictime() {
        return \backend\Modules\Salary\models\EmploymentConstant::find()->where(['status' => 1, 'id_employment_fk' => $this->id])->orderby('hours_to')->all();
    }
    
    public function getOvertime() {
        return \backend\Modules\Salary\models\EmploymentRate::find()->where(['status' => 1, 'id_employment_fk' => $this->id])->orderby('hours_from')->all();
    }
	
	public function getBonustime() {
        return \backend\Modules\Salary\models\EmploymentBonus::find()->where(['status' => 1, 'id_employment_fk' => $this->id])->orderby('hours_to desc')->all();
    }
    
    public static function getList() {
        $employments = [];
        $sqlQuery = "select e.id as id, concat(concat_ws(' ', lastname, firstname), ' [',e.name,']') as name from {{%employment}} e join {{%company_employee}} ce on ce.id = e.id_employee_fk where e.status = 1 order by ce.lastname";
        $data = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
        if($data) $employments = $data;
        
        return $employments;
    }
    
    public static function listTypes() {
        return ['names' => [1 => 'Ryczałt + nadgodziny', 2 => 'Stawka stała', 3 => 'Stawka godzinowa'], 'colors' => [1 => 'purple', 2 => 'blue', 3 => 'teal']];
    }
}
