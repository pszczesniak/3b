<?php

namespace backend\Modules\Salary\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employment_rate}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_employment_fk
 * @property string $name
 * @property string $symbol
 * @property string $hours_from
 * @property string $hours_to
 * @property string $rate
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class EmploymentRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employment_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_employment_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['hours_from', 'rate'], 'required'],
            [['hours_from', 'hours_to', 'rate'], 'number'],
            [['describe', 'custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 2000],
            [['symbol'], 'string', 'max' => 100],
            ['hours_from', 'checkRange'/*, 'when' => function($model) { return ($model->no_less_than && $model->rate); }*/ ],
        ];
    }
    
    public function checkRange(){

        if( $this->hours_to ) {
            if($this->hours_to <= $this->hours_from) {
                $this->addError('hours_to', 'Ilość `godzin do` nie może być mniejsza od ilości `godzin od`');
                return true;
            }
        } else {
            $isExistSql = "select min(hours_to) isExist from {{%employment_constant}} where status = 1 and id_employment_fk = ".$this->id_employment_fk
                            ." and hours_to > ".$this->hours_from;
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Dla '.$isExist.' dla tego wynagordzenia przypisany jest ryczałt');
                return true;
            }
            
            
            $isExistSql = "select count(*) isExist from {{%employment_rate}} where status = 1 and hours_to is null and id_employment_fk = ".$this->id_employment_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0);
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Dla tego wyngarodzenia już istnieje jeden okres otwarty.');
                return true;
            }
            
            $isExistSql = "select min(hours_from) isExist from {{%employment_rate}} where status = 1 and id_employment_fk = ".$this->id_employment_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0)
                            ." and (".$this->hours_from." < hours_from and ".$this->hours_from." > hours_to)";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Istnieje dla tego wyngarodzenia zakres nadgodzin pokrywający się.');
                return true;
            }
        }
        if ( $this->hours_from && $this->hours_to ) {
            /*$isExistSql = "select min(hours_to) isExist from {{%employment_constant}} where status = 1 and id_employment_fk = ".$this->id_employment_fk
                            ." and (hours_to between ".$this->hours_from." and ".$this->hours_to." or hours_to between ".$this->hours_from." and ".$this->hours_to.")";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Dla '.$isExist.' dla tego wynagordzenia przypisany jest ryczałt');
                return true;
            }*/
            
            $isExistSql = "select min(hours_to) isExist from {{%employment_constant}} where status = 1 and id_employment_fk = ".$this->id_employment_fk
                            ." and hours_to > ".$this->hours_from;
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Dla '.$isExist.' dla tego wynagordzenia przypisany jest ryczałt');
                return true;
            }
            
            $isExistSql = "select min(hours_from) isExist from {{%employment_rate}} where status = 1 and hours_to is not null and id_employment_fk = ".$this->id_employment_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0)
                            ." and (hours_from between ".$this->hours_from." and ".$this->hours_to." or hours_to between ".$this->hours_from." and ".$this->hours_to.")";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_from', 'Błędny zakres. Istnieje dla tego wyngarodzenia zakres nadgodzin pokrywający się.');
                return true;
            }
            
            $isExistSql = "select count(*) isExist from {{%employment_rate}} where status = 1 and hours_to is null and id_employment_fk = ".$this->id_employment_fk. " and id != ". ((!$this->isNewRecord) ? $this->id : 0)
                            ." and (hours_from < ".$this->hours_to." )";
            $isExist = Yii::$app->db->createCommand($isExistSql)->queryOne()['isExist']; 
            if($isExist) {
                $this->addError('hours_to', 'Błędny zakres. Istnieje dla tego wyngarodzenia zakres nadgodzin pokrywający się.');
                return true;
            }
        }
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_employment_fk' => Yii::t('app', 'Id Employment Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'hours_from' => Yii::t('app', 'Od godziny'),
            'hours_to' => Yii::t('app', 'Do godziny'),
            'rate' => Yii::t('app', 'Stawka godzinowa'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		//if(empty($this->date_to)) $this->date_to = null;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    /*public function afterSave($insert, $changedAttributes) {  
        
    }*/
    
    public function getEmployment() {
		return $this->hasOne(\backend\Modules\Salary\models\Employment::className(), ['id' => 'id_employment_fk']);
    }
}
