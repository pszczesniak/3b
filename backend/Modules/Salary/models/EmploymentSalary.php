<?php

namespace backend\Modules\Salary\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%employment_salary}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_employment_fk
 * @property string $period
 * @property string $net_value
 * @property string $hours_customer
 * @property string $hours_own
 * @property string $rate_bonus
 * @property string $describe
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class EmploymentSalary extends \yii\db\ActiveRecord
{
    public $verify;
    public $id_department_fk;
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%employment_salary}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_employment_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_constant_fk', 'id_overtime_fk', 'id_bonus_fk', 'send_user', 'is_customize'], 'integer'],
            [['net_value'], 'required'],
            [['net_value', 'hours_customer', 'hours_own', 'hours_free', 'hours_admin', 'hours_customer_c', 'hours_own_c', 'hours_free_c', 'hours_admin_c', 'rate_bonus'], 'number'],
            [['describe', 'custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'send_message', 'feedback_message', 'send_date', 'feedback_date'], 'safe'],
            [['period'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Typ'),
            'id_employment_fk' => Yii::t('app', 'Employment'),
            'id_department_fk' => Yii::t('app', 'Department'),
            'period' => Yii::t('app', 'Period'),
            'net_value' => Yii::t('app', 'Net Value'),
            'hours_customer' => Yii::t('app', 'Hours Customer'),
            'hours_own' => Yii::t('app', 'Hours Own'),
            'rate_bonus' => Yii::t('app', 'Rate Bonus'),
            'describe' => Yii::t('app', 'Describe'),
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'hours_customer_c' => 'na klienta', 
            'hours_own_c' => 'na SIW', 
            'hours_free_c' => 'wolne/urlop', 
            'hours_admin_c' => ''
        ];
    }
    
    public function beforeSave($insert) {
		//if(empty($this->date_to)) $this->date_to = null;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
   /* public function afterSave($insert, $changedAttributes) {  
        
    }*/
    
    public function getEmployment() {
		return $this->hasOne(\backend\Modules\Salary\models\Employment::className(), ['id' => 'id_employment_fk']);
    }
    
    public function getConstant() {
		return $this->hasOne(\backend\Modules\Salary\models\EmploymentConstant::className(), ['id' => 'id_constant_fk']);
    }
    
    public function getHistory() {
        $history = [];
        if($this->send_date) {
            $temp = [];
            $temp['fullname'] = \common\models\User::findOne($this->send_user)['fullname'];
            $temp['action'] = 'Wysłanie do weryfikacji';
            $temp['date'] = $this->send_date;
            $temp['message'] = $this->send_message;
            array_push($history, $temp);
        }
        
        if($this->feedback_date) {
            $temp = [];
            $temp['fullname'] = \common\models\User::findOne($this->feedback_user)['fullname'];
            $temp['action'] = 'Odpowiedź';
            $temp['date'] = $this->feedback_date;
            $temp['message'] = $this->feedback_message;
            array_push($history, $temp);
        }
        
        return $history;
    }
}
