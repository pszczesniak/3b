<?php

namespace app\Modules\Newsletter\controllers;

use Yii;
use backend\Modules\Newsletter\models\NewsletterGroup;
use app\Modules\Newsletter\models\NewsletterGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * NewslettergroupController implements the CRUD actions for NewsletterGroup model.
 */
class NewslettergroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterGroupSearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['NewsletterGroupSearch'])) {
            $params = $_GET['NewsletterGroupSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(g.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "g.status = ".$params['status']);
            }
        } else {
            array_push($where, "g.status = 1");
        }
        
        $sortColumn = 'g.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'g.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'g.status';
		
		$query = (new \yii\db\Query())
            ->select(['g.id as id', 'g.name as name', 'g.status as status'])
            ->from('{{%newsletter_group}} as g');
           // ->join('LEFT JOIN', '{{%cms_category}} as c', 'c.id = p.fk_category_id')
           // ->where( ['g.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', -1 => 'danger'];
        $statusLits = \Yii::$app->params['defaultListStatus'];
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/pl/newsletter/newslettergroup/updateajax/'.$value['id'].'" class="btn btn-sm btn-default gridViewModal" data-table="#table-groups" data-target="#modal-grid-item"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/newsletter/newslettergroup/deleteajax/'.$value['id'].'" class="btn btn-sm btn-default remove" data-table="#table-groups"><i class="fa fa-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['name'] = $value['name'];
            $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$statusLits[$value['status']].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single NewsletterGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionCreateajax() {
		
        $model = new NewsletterGroup();
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Grupa <b>'.$model->name.'</b> został dodana', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing NewsletterGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('_formAjax', [
                'model' => $model
            ]); 
        } else {
            return $this->renderPartial('_formAjax', [
                'model' => $model
            ]);
        }
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'alert' => 'Wpis <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing NewsletterGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-groups');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-groups');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the NewsletterGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
