<?php

namespace app\Modules\Newsletter\controllers;

use Yii;
use backend\Modules\Newsletter\models\NewsletterSubscribe;
use app\Modules\Newsletter\models\NewsletterSubscribeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * NewslettersubscribeController implements the CRUD actions for NewsletterSubscribe model.
 */
class NewslettersubscribeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterSubscribe models.
     * @return mixed
     */
    
    public function actionIndex()
    {
        $searchModel = new NewsletterSubscribeSearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = []; $groupFilter = false;
        if(isset($_GET['NewsletterSubscribeSearch'])) {
            $params = $_GET['NewsletterSubscribeSearch'];
            if(isset($params['email']) && !empty($params['email']) ) {
				array_push($where, "lower(s.email) like '%".strtolower( addslashes($params['email']) )."%'");
            }
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(s.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "s.status = ".$params['status']);
            }
            if(isset($params['id_group_fk']) && !empty($params['id_group_fk']) ) {
				array_push($where, "gs.id_group_fk = ".$params['id_group_fk']);
                $groupFilter = true;
            }
        } else {
            array_push($where, "s.status = 1");
        } 
        
        $sortColumn = 's.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 's.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 's.status';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 's.name';
		
		$query = (new \yii\db\Query())
            ->select(['s.id as id', 's.email as email', 's.name as name' , 's.status as status'])
            ->from('{{%newsletter_subscribe}} as s');
        if($groupFilter)
           $query->join('JOIN', '{{%newsletter_group_subscribe}} as gs', 'gs.id_subscribe_fk = s.id and gs.status = 1');
          // ->where( ['s.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', -1 => 'danger'];
        $statusLits = \Yii::$app->params['defaultListStatus'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/pl/newsletter/newslettersubscribe/view/'.$value['id'].'" class="btn btn-sm btn-default gridViewModal" data-title="<i class=\'fa fa-eye\'></i>Podgląd" data-table="#table-subscribers" data-target="#modal-grid-item"><i class="fa fa-eye"></i></a>';            
            $actionColumn .= '<a href="/panel/pl/newsletter/newslettersubscribe/updateajax/'.$value['id'].'" class="btn btn-sm btn-default gridViewModal" data-table="#table-subscribers" data-target="#modal-grid-item"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/newsletter/newslettersubscribe/deleteajax/'.$value['id'].'" class="btn btn-sm btn-default remove" data-table="#table-subscribers"><i class="fa fa-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['email'] = $value['email'];
            $tmp['name'] = $value['name'];
            $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$statusLits[$value['status']].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}


    /**
     * Displays a single NewsletterSubscribe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->groups = '';
        foreach($model->subscribegroups as $key => $group) {
            $model->groups .= '<li>'.$group->group['name'].'</li>';
        }
        $model->groups = (!$model->groups) ? 'brak przypisanych grup' : '<ul>'.$model->groups.'</ul>';
        return $this->renderAjax('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new NewsletterSubscribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterSubscribe();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
        $model = new NewsletterSubscribe();
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Subskrybent <b>'.$model->name.'</b> został zarejestrowany', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing NewsletterSubscribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]); 
        } else {
            return $this->renderPartial('_form', [
                'model' => $model
            ]);
        }
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing NewsletterSubscribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-subscribers');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-subscribers');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the NewsletterSubscribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterSubscribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterSubscribe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
