<?php

namespace app\Modules\Newsletter\controllers;

use Yii;
use backend\Modules\Newsletter\models\NewsletterTemplate;
use backend\Modules\Newsletter\models\NewsletterTemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * NewslettertemplatepController implements the CRUD actions for NewsletterTemplate model.
 */
class NewslettertemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterTemplateSearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['NewsletterTemplateSearch'])) {
            $params = $_GET['NewsletterTemplateSearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(t.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['id_group_fk']) && strlen($params['id_group_fk']) ) {
				if($params['id_group_fk'] == 0)
                    array_push($where, "(id_group_fk = 0 or id_group_fk is null)");
                else
                    array_push($where, "id_group_fk = ".$params['id_group_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "t.status = ".$params['status']);
            }
        } else {
            array_push($where, "t.status = 1");
        }
        
        $sortColumn = 't.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 't.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 't.status';
        if( isset($post['sort']) && $post['sort'] == 'group' ) $sortColumn = 't.id_group_fk';
		
		$query = (new \yii\db\Query())
            ->select(['t.id as id', 't.title as title', 't.status as status'])
            ->from('{{%newsletter_template}} as t');
           // ->join('LEFT JOIN', '{{%cms_category}} as c', 'c.id = p.fk_category_id')
           // ->where( ['t.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', -1 => 'danger'];
        $statusLits = \Yii::$app->params['defaultListStatus'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.\Yii::$app->urlManagerFrontEnd->createUrl('biuletyn/'.$value['id']).'" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/newsletter/newslettertemplate/update/'.$value['id'].'" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>' : '');
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/newsletter/newslettertemplate/deleteajax/'.$value['id'].'" class="btn btn-sm btn-default remove" data-table="#table-templates"><i class="fa fa-trash"></i></a>' : '');
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/newsletter/newslettersend/printpdf/'.$value['id'].'" class="btn btn-sm btn-default"  data-table="#table-templates"><i class="fa fa-send"></i></a>' : '');         
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$statusLits[$value['status']].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single NewsletterTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterTemplate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NewsletterTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NewsletterTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-templates');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-groups');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the NewsletterTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
