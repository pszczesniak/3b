<?php

namespace app\Modules\Newsletter\controllers;

use Yii;
use app\Modules\Newsletter\models\NewsletterSend;
use app\Modules\Newsletter\models\NewsletterSendSearch;
use backend\Modules\Newsletter\models\NewsletterTemplate;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * NewslettersendController implements the CRUD actions for NewsletterSend model.
 */
class NewslettersendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterSend models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterSendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsletterSend model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterSend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterSend();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NewsletterSend model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NewsletterSend model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NewsletterSend model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterSend the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterSend::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPrintpdf($id) {
        $model = NewsletterTemplate::findOne($id);
		
        $mpdf = new \Mpdf\Mpdf();
		$mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('NEWSLETTER');
        $mpdf->SetAuthor("Twojeprzyjecia.pl");
        //$mPDF1->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        
        //$mPDF1->SetHeader('|KARTA PRODUKTU SYSTEMU LSDD|');
		$mpdf->SetTextColor(149, 41, 68); 
        $mpdf->SetHeader('|'.$model->title.'|');
        //$mpdf->SetFooter('{PAGENO}|'.date("y-m-d H:i:s"));
		//Yii::$app->homeUrl
		$stylesheet = file_get_contents(\Yii::getAlias('@webroot').'/css/newsletter.css');
		$mpdf->WriteHTML($stylesheet, 1);
		
	    
		$offers = \backend\Modules\Svc\models\SvcOffer::find()->all();
		
	
		$mpdf->WriteHTML($this->renderPartial('printpdf', array('offers' => $offers), true));
        
		$filename = 'newsletter.pdf';
		$send = ( isset($_GET['send']) ) ? 1 : 0;
        
		if($send) {
            $mpdf->Output(/*realpath(Yii::$app->basePath) . '/temp/'*/'temp/'.$filename, 'F');
			return $filename;
		} else {
            $mpdf->Output($filename, 'D');
			exit;
        }		
        //$mpdf->Output('test', 'D');
        
    }
}
