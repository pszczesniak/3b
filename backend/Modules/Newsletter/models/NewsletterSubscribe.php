<?php

namespace backend\Modules\Newsletter\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Newsletter\models\NewsletterGroupSubscribe;

/**
 * This is the model class for table "{{%newsletter_subscribe}}".
 *
 * @property integer $id
 * @property integer $email
 * @property integer $status
 * @property string $created_at
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class NewsletterSubscribe extends \yii\db\ActiveRecord
{
    public $groups;
    public $id_group_fk;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_subscribe}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            //[['email'], 'unique'],
            [['status', 'created_by', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Nazwa'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'groups' => 'Grupy',
            'id_group_fk' => 'Grupa',
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
                if(!$this->name) $this->name = $this->email;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getSubscribegroups() {
        $items = [];
        return \backend\Modules\Newsletter\models\NewsletterGroupSubscribe::find()->where(['id_subscribe_fk' => $this->id, 'status' => 1])->all(); 
		/*foreach($data as $key => $item) {
            $items[$key] = $item;
        }
		return $items;*/
    }
}
