<?php

namespace app\Modules\Newsletter\models;

use Yii;

/**
 * This is the model class for table "{{%newsletter_send}}".
 *
 * @property integer $id
 * @property string $id_template_fk
 * @property integer $status
 * @property string $send_at
 * @property integer $send_by
 */
class NewsletterSend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_send}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_template_fk'], 'required'],
            [['status', 'send_by'], 'integer'],
            [['send_at'], 'safe'],
            [['id_template_fk'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_template_fk' => Yii::t('app', 'Id Template Fk'),
            'status' => Yii::t('app', 'Status'),
            'send_at' => Yii::t('app', 'Send At'),
            'send_by' => Yii::t('app', 'Send By'),
        ];
    }
}
