<?php

namespace app\Modules\Newsletter\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\Modules\Newsletter\models\NewsletterSend;

/**
 * NewsletterSendSearch represents the model behind the search form about `app\Modules\Newsletter\models\NewsletterSend`.
 */
class NewsletterSendSearch extends NewsletterSend
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'send_by'], 'integer'],
            [['id_template_fk', 'send_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterSend::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'send_at' => $this->send_at,
            'send_by' => $this->send_by,
        ]);

        $query->andFilterWhere(['like', 'id_template_fk', $this->id_template_fk]);

        return $dataProvider;
    }
}
