<?php

namespace backend\Modules\Newsletter\models;

use Yii;

/**
 * This is the model class for table "{{%newsletter_group}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $describe
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class NewsletterGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'describe'], 'required'],
            [['describe'], 'string'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'describe' => Yii::t('app', 'Opis'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public static function getList() {

        $data = \backend\Modules\Newsletter\models\NewsletterGroup::find()->where(['status' => 1])->all();
        $groups = [];
                
        foreach($data as $key => $item) {
            $groups[$item->id] = $item->name;
        }    
        
        return $groups;
    }
}
