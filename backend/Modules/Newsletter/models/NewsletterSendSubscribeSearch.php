<?php

namespace app\Modules\Newsletter\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\Modules\Newsletter\models\NewsletterSendSubscribe;

/**
 * NewsletterSendSubscribeSearch represents the model behind the search form about `app\Modules\Newsletter\models\NewsletterSendSubscribe`.
 */
class NewsletterSendSubscribeSearch extends NewsletterSendSubscribe
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_send_fk', 'id_subscribe_fk', 'status', 'send_by'], 'integer'],
            [['send_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterSendSubscribe::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_send_fk' => $this->id_send_fk,
            'id_subscribe_fk' => $this->id_subscribe_fk,
            'status' => $this->status,
            'send_at' => $this->send_at,
            'send_by' => $this->send_by,
        ]);

        return $dataProvider;
    }
}
