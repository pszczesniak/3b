<?php

namespace backend\Modules\Newsletter\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%newsletter_group_subscribe}}".
 *
 * @property integer $id
 * @property integer $id_group_fk
 * @property integer $id_subscribe_fk
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class NewsletterGroupSubscribe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_group_subscribe}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_group_fk', 'id_subscribe_fk', 'email'], 'required'],
            [['id_group_fk', 'id_subscribe_fk', 'status', 'deleted_by'], 'integer'],
            [['created_at', 'created_by', 'deleted_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_group_fk' => Yii::t('app', 'Id Group Fk'),
            'id_subscribe_fk' => Yii::t('app', 'Id Subscribe Fk'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	/*public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}*/
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getGroup() {
		return  \backend\Modules\Newsletter\models\NewsletterGroup::findOne($this->id_group_fk);
	}
}
