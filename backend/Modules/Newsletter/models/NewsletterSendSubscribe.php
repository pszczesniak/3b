<?php

namespace app\Modules\Newsletter\models;

use Yii;

/**
 * This is the model class for table "{{%newsletter_send_subscribe}}".
 *
 * @property integer $id
 * @property integer $id_send_fk
 * @property integer $id_subscribe_fk
 * @property integer $status
 * @property string $send_at
 * @property integer $send_by
 */
class NewsletterSendSubscribe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_send_subscribe}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_send_fk', 'id_subscribe_fk'], 'required'],
            [['id_send_fk', 'id_subscribe_fk', 'status', 'send_by'], 'integer'],
            [['send_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_send_fk' => Yii::t('app', 'Id Send Fk'),
            'id_subscribe_fk' => Yii::t('app', 'Id Subscribe Fk'),
            'status' => Yii::t('app', 'Status'),
            'send_at' => Yii::t('app', 'Send At'),
            'send_by' => Yii::t('app', 'Send By'),
        ];
    }
}
