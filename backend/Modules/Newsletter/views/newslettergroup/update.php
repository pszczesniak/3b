<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterGroup */

$this->title = Yii::t('app', 'Aktualizacja grupy: ', [
    'modelClass' => 'Newsletter Group',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="newsletter-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
