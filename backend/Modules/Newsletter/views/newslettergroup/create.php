<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterGroup */

$this->title = Yii::t('app', 'Nowa grupa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Grupy użytkoników'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
