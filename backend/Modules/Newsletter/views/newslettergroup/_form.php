<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterGroup */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="blog-tag-form">
    <?php $form = ActiveForm::begin([ 'options' => ['data-pjax' => false, 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'describe')->textarea(['rows' => 6]) ?>
    <!--
        <?= $form->field($model, 'status')->textInput() ?>

        <?= $form->field($model, 'created_at')->textInput() ?>

        <?= $form->field($model, 'created_by')->textInput() ?>

        <?= $form->field($model, 'updated_at')->textInput() ?>

        <?= $form->field($model, 'updated_by')->textInput() ?>

        <?= $form->field($model, 'deleted_at')->textInput() ?>

        <?= $form->field($model, 'deleted_by')->textInput() ?>
    -->
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
