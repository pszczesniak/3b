<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-newsletter-groups-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-groups']
    ]); ?>
    
    <div class="row"> 
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-groups', 'data-form' => '#filter-newsletter-groups-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'status', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \Yii::$app->params['defaultListStatus'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-groups', 'data-form' => '#filter-newsletter-groups-search' ] ) ?></div>        
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
