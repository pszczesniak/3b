<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSubscribe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-tamplate-form">
    <?php $form = ActiveForm::begin([ 'options' => ['data-pjax' => false, 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>

        <div class="row">
            <div class="col-sm-6"><?= $form->field($model, 'email')->textInput() ?></div><div class="col-sm-6"><?= $form->field($model, 'name')->textInput() ?></div>
        </div>
<!--
        <?= $form->field($model, 'status')->textInput() ?>

        <?= $form->field($model, 'created_at')->textInput() ?>

        <?= $form->field($model, 'deleted_at')->textInput() ?>

        <?= $form->field($model, 'deleted_by')->textInput() ?>
-->
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
