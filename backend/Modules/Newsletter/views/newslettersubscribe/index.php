<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Newsletter\Newslettersubscribe */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meneżdżer subskrybentów';
$this->params['breadcrumbs'][] = 'Newsletter';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"newsletter-subscribe-index", 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-subscribers" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/newsletter/newslettersubscribe/createajax']) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-subscribers",
                         'data-title' => "Dodaj"
                        ])  ?>    
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-subscribers"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-subscribers"

                data-toolbar="#toolbar-subscribers" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="600"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="id"
                data-sort-order="desc"
                
                data-method="get"
                data-search-form="#filter-newsletter-subscribers-search"
                data-url=<?= Url::to(['/newsletter/newslettersubscribe/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="email"  data-sortable="true" >Email</th>
                    <th data-field="name"  data-sortable="true" >Podpis</th>
                    <th data-field="status" data-align="center" data-sortable="true" >Status</th>
                    <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>
<?php $this->endContent(); ?>
