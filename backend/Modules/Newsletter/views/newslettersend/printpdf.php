<?php

use yii\helpers\Html;
use yii\helpers\Url;


?>

<table cellspacing="0" cellpadding="0">
<?php
    foreach($offers as $key => $offer) {
        $img = (file_exists(\Yii::getAlias('@webroot').'/../../frontend/web/uploads/avatars/avatar-'.$offer->id.'-small.jpg'))?'/uploads/avatars/avatar-'.$offer->id.'-small.jpg':'/uploads/avatars/no-image-small.jpg';
        echo '<tr>'
                .'<td valign="top" class="col-img"><img src="'. $img .'" ></td>'
                .'<td valign="top" class="col-info">'
					.'<h2>'.$offer->companyname.'</h2>'
					.'<h3>'.$offer->address.', '.$offer->postal_code.' '.$offer->city.'</h3>'
					.'<h4><br /><a href="'.yii::$app->params['frontend'].'/oferta/'.$offer->id.'">&nbsp;&nbsp;zobacz szczegóły&nbsp;&nbsp;</a></h4>'
				.'</td>'
            .'</tr>'
			/*.'<tr class="row-content">'
				.'<td colspan="2" class="col-content"><p class="block-content"><br />'.$offer->note.'</p><br /><br /><br /></td>'
			.'</tr>'*/;
    }
    
?>
</table>