<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSend */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Newsletter Send',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Sends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="newsletter-send-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
