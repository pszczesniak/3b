<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterTemplate */

$this->title = 'Aktualizacja szablonu';
$this->params['breadcrumbs'][] = 'Newsletter';
$this->params['breadcrumbs'][] = ['label' => 'Szablony', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"newsletter-template-update", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
