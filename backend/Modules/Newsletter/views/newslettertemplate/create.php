<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterTemplate */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => 'Szablony', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"newsletter-template-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
