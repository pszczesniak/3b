<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSendSubscribe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-send-subscribe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_send_fk')->textInput() ?>

    <?= $form->field($model, 'id_subscribe_fk')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'send_at')->textInput() ?>

    <?= $form->field($model, 'send_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
