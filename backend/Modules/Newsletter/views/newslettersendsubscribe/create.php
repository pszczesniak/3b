<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSendSubscribe */

$this->title = Yii::t('app', 'Create Newsletter Send Subscribe');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter Send Subscribes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-send-subscribe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
