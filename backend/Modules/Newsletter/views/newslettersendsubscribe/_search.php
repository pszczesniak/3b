<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Newsletter\models\NewsletterSendSubscribeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-send-subscribe-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_send_fk') ?>

    <?= $form->field($model, 'id_subscribe_fk') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'send_at') ?>

    <?php // echo $form->field($model, 'send_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
