<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Newsletter\models\NewsletterSendSubscribeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Newsletter Send Subscribes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-send-subscribe-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Newsletter Send Subscribe'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_send_fk',
            'id_subscribe_fk',
            'status',
            'send_at',
            // 'send_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
