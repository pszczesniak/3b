<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccInvoiceItem;
use backend\Modules\Accounting\models\AccInvoice;
use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccOrderItem;
use backend\Modules\Accounting\models\AccDiscount;
use backend\Modules\Accounting\models\AccCorrection;
use backend\Modules\Crm\models\Customer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * InvoiceController implements the CRUD actions for AccActions model.
 */
class InvoiceController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()  {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccInvoice();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.invoices');  
            if($params['params']) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        } else {
			$searchModel->date_issue = date('Y-m');
		}
          
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = AccInvoice::find();
        
        $where = [];
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            \Yii::$app->session->set('search.invoices', ['params' => $params, 'post' => $post]);
            if( isset($params['status']) && strlen($params['status']) ) {
               // echo 'status: '.$params['status'];exit;
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status'] );
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['no']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$params['id_company_branch_fk'].")");
            }
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               // $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk = ".$params['id_resource_fk']);
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
			if(isset($params['date_issue']) && !empty($params['date_issue']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_issue']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') = '".$params['date_issue']."'");
            }
            if( isset($params['id_customer_fk']) && strlen($params['id_customer_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "o.id_customer_fk = ".$params['id_customer_fk'] );
            }
            if( isset($params['id_currency_fk']) && strlen($params['id_currency_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "i.id_currency_fk = ".$params['id_currency_fk'] );
            }
        }  else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_issue, '%Y-%m') >= '".date('Y-m')."'");

        }     
       // $fieldsData = $fieldsDataQuery->all();
			
		$sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'order' ) $sortColumn = 'o.name';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no', "date_format(date_issue, '%Y-%m') as date_issue", 'i.created_at as created_at', 
                      '(select sum(discount_amount) from {{%acc_discount}} where status = 1 and id_invoice_fk = i.id) as discount_amount', 
                      '(select sum(amount_net) from {{%acc_invoice_item}} ii where ii.id_invoice_fk = i.id) as amount_net', 
                      'amount_gross', 'ifnull(c.symbol,c.name) as cname', 'c.id as cid', 'o.id as oid', 'o.name as oname', 'type_name', 'type_color', 'type_symbol', 'currency_symbol'])
            ->from('{{%acc_invoice}} as i')
            ->join('JOIN', '{{%acc_order}} as o', 'o.id = i.id_order_fk')
            ->join('JOIN', '{{%acc_type}} at', 'at.id = o.id_dict_order_type_fk')
            ->join('JOIN', '{{%acc_currency}} curr', 'curr.id = i.id_currency_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = o.id_customer_fk')
            ->where( ['i.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/accounting/invoice/view/', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd faktury').'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
                $actionColumn .= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<a href="'.Url::to(['/accounting/invoice/deleteajax', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-invoices"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>' : '';
            $actionColumn .= '</div>';
            
            $tmp['order'] = '<a href="'.Url::to(['/accounting/order/view/', 'id' => $value['oid']]).'" class="label" style="color:#fff; background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['oname'].'</a>';
            $tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view/', 'id' => $value['cid']]).'">'.mb_strimwidth((($value['cname']) ? $value['cname'] : $value['cname']), 0, 50, "...").'</a>';
            $tmp['no'] = '<span class="no-wrap">'.$value['system_no'].'</span>';
			$tmp['issue'] = $value['created_at'];
            $tmp['period'] = $value['date_issue'];
            $tmp['net'] = '<span class="no-wrap">'.number_format ($value['amount_net'], 2, "," ,  " ").'</span>';
            $tmp['discount'] = number_format ($value['discount_amount'], 2, "," ,  " ");
            $tmp['currency'] = $value['currency_symbol'];
           // $tmp['gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}

    public function actionView($id) {
        $model = $this->findModel($id);

        return $this->render(((Yii::$app->params['env'] == 'dev') ? 'viewnew' : 'view'), ['model' => $model] );
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/     
    }
	
	
	public function actionUitem($id) {
        $id = CustomHelpers::decode($id);
		$model = \backend\Modules\Accounting\models\AccInvoiceItem::findOne($id); 
                   
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
	
            if( $model->validate() && $model->save()) {
                $row = ['name' => $model->item_name, 
                        'net' => number_format ($model->amount_net, 2, "," ,  " "),
                        'vat' => (($model->vat)?$model->vat['value'].'%':''),
                        'gross' => number_format ($model->amount_net, 2, "," ,  " ")
                    ];
                $summary = ['vat' => number_format(AccInvoice::getSummary($model->id)['s_vat'], 2, ',', ' ')];
				return array('success' => true,  'action' => 'invItemEdit', 'id' => $model->id, 'name' => $model->item_name, 'alert' => 'Zmiany zostały zapisane',  'row' => $row, 'summary' => $summary );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formItem', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formItem', [ 'model' => $model]);	
        }
	}
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $model = $this->findModel($id); //->delete();
        $model->status = 0;
        $model->opened_by = Yii::$app->user->id;
        $model->opened_at = date('Y-m-d H:i:s');
        $periodArr = explode('-', $model->date_issue); $period = $periodArr[0].'-'.$periodArr[1];
        
        $periodModel = \backend\Modules\Accounting\models\AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie może zostać usunięta ponieważ okres rozliczeniowy został zamknięty')  );
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        $transaction = \Yii::$app->db->beginTransaction(); 
        try {
            if($model->save()) {
                AccActions::updateAll(['id_invoice_fk' => 0],'status = 1 and id_invoice_fk = :invoice and (is_confirm = 1 or is_gratis = 1)', [':invoice' => $model->id]);
                $values = \backend\Modules\Accounting\models\AccPeriod::getValues($period);
                $sql = "update {{%acc_period}} "
                . " set all_time = ". ( ($values['inv_time']) ? $values['inv_time'] : 0) 
                .", all_profit = ". ( ($values['inv_profit']) ? $values['inv_profit'] : 0)
                .", all_cost = ". ( ($values['inv_costs']) ? $values['inv_costs'] : 0)
                .", all_discount = ". ( ($values['inv_discount']) ? $values['inv_discount'] : 0)
                . " where period_year = ".$periodArr[0]." and period_month = ".($periodArr[1]*1);
                Yii::$app->db->createCommand($sql)->execute();
                
                $sqlUpdate = "update {{%acc_discount}} set id_invoice_fk = 0, discount_amount = 0"
                            ." where id_invoice_fk = ".$model->id; 
				Yii::$app->db->createCommand($sqlUpdate)->execute(); 
			    $sqlUpdate = "update {{%acc_correction}} set id_invoice_fk = 0"
                            ." where id_invoice_fk = ".$model->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                
                $transaction->commit();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie może zostać usunięta')  );
        }
        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->opened_by = \Yii::$app->user->id;
        $model->opened_at = date('Y-m-d H:i:s');
        $model->status = 0;
        
        $periodArr = explode('-', $model->date_issue); $period = $periodArr[0].'-'.$periodArr[1];
        
        $periodModel = \backend\Modules\Accounting\models\AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            //Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie może zostać usunięta ponieważ okres rozliczeniowy został zamknięty')  );
            //return $this->redirect(['view', 'id' => $model->id]);
            return array('success' => false, 'alert' => 'Faktura nie może zostać usunięta ponieważ okres rozliczeniowy został zamknięty', 'table' => '#table-invoices', 'errors' => ['Faktura nie może zostać usunięta ponieważ okres rozliczeniowy został zamknięty'] );
        }
        
        $transaction = \Yii::$app->db->beginTransaction(); 
        try {
            if($model->save()) {
                AccActions::updateAll(['id_invoice_fk' => 0],'status = 1 and id_invoice_fk = :invoice and is_confirm = 1', [':invoice' => $model->id]);
                $values = \backend\Modules\Accounting\models\AccPeriod::getValues($period);
                $sql = "update {{%acc_period}} "
                . " set all_time = ". ( ($values['inv_time']) ? $values['inv_time'] : 0) 
                .", all_profit = ". ( ($values['inv_profit']) ? $values['inv_profit'] : 0)
                .", all_cost = ". ( ($values['inv_costs']) ? $values['inv_costs'] : 0)
                .", all_discount = ". ( ($values['inv_discount']) ? $values['inv_discount'] : 0)
                . " where period_year = ".$periodArr[0]." and period_month = ".($periodArr[1]*1);
                Yii::$app->db->createCommand($sql)->execute();
                
                $sqlUpdate = "update {{%acc_discount}} set id_invoice_fk = 0, discount_amount = 0"
                            ." where id_invoice_fk = ".$model->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                
                $transaction->commit();
            }
        } catch (Exception $e) {
            $transaction->rollBack();    
            return array('success' => false, 'alert' => 'Skontaktuj się z administratorem', 'table' => '#table-invoices', 'errors' => $model->getErrors() );	      
        }
        return array('success' => true, 'alert' => 'Faktura została usunięta', 'table' => '#table-invoices');	
    }


    /**
     * Finds the AccInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = AccInvoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Rozliczenie o wskazanym identyfikatorze nie zostało odnalezione w systemie.');
        }
    }
    
    public function actionCreate($id) {
        $id = CustomHelpers::decode($id);
        $model = Customer::findOne($id); 
        
        $sql = "select acc_period as period, count(*) actions from {{%acc_actions}} "
                ." where status=1 and is_confirm=1 and (id_invoice_fk=0 or id_invoice_fk is null) and id_customer_fk = ".$model->id
                ." group by acc_period order by 1";
        $periods = Yii::$app->db->createCommand($sql)->queryAll();
        return  $this->renderAjax('_formSettle', [ 'model' => $model, 'periods' => $periods, 'id' => $id ]) ;
    }
    
    public function actionGenerate($id, $period) {
        
        //if(AccInvoice::isExist($id, $period)) return false;
        if(AccInvoice::isExist($id, $period)) {
            if( isset($_GET['gen']) && $_GET['gen'] == 1 ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => false, 'alert' => 'Faktura do tego zlecenia została już wygenerowana', 'table' => '#table-orders'];
            } else {
                return false;
            }
        }   
        $order = AccOrder::findOne($id);
        $discount = AccDiscount::find()->where(['status' => 1, 'id_order_fk' => $id, 'acc_period' => $period])->one();
		$correction = AccCorrection::find()->where(['status' => 1, 'id_order_fk' => $id, 'acc_period' => $period])->one(); 
        
        $periodArr = explode('-', $period);
        
        $periodRecord = \backend\Modules\Accounting\models\AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => ($periodArr[1]*1) ])->one();
        if(!$periodRecord) {
            $periodRecord = new \backend\Modules\Accounting\models\AccPeriod();
            $periodRecord->period_year = $periodArr[0];
            $periodRecord->period_month = $periodArr[1];
            $periodRecord->save();
        }
        
        $invoiceNumberSql = "select count(*) as fv_number from {{%acc_invoice}} where date_issue = '".($period.'-01')."'";
        $invoiceNumberResult = Yii::$app->db->createCommand($invoiceNumberSql)->queryOne();
        $invoiceNumber = $invoiceNumberResult['fv_number']+1;
		
		$exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false;
        
        $transaction = \Yii::$app->db->beginTransaction(); 
        try {
            $new = new AccInvoice(); $items = [];
            $new->id_order_fk = $id;
			$new->id_currency_fk = $order->id_currency_fk;
			$new->id_exchange_fk = ($exchange) ? $exchange->id : 0;
			$new->rate_exchange = ($exchange) ? $exchange->rate_value : 1;
            $new->system_no = 'FV/'.$periodArr[0].'/'.$periodArr[1].'/'.$invoiceNumber;
            $new->real_no = 'FV/'.$periodArr[0].'/'.$periodArr[1].'/'.$invoiceNumber;
            $new->date_issue = $period.'-01';
            $time = 0; $profit = 0; $costNet = 0;
            if(!$new->save()) { return false; }
                           
            $rate_constant = 0; $rate_hours_1 = 0; $rate_hours_2 = 0; $rate_way = 0;  $costs = []; $rate_special = 0;
            $actions = AccActions::find()->where(['status' => 1, 'is_confirm' => 1, 'id_order_fk' => $order->id])->andWhere(" (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."'")->orderby('action_date')->all();
            $byHours = 0; $byHoursAll = 0; $advancedInfo = ''; $confirmTime = 0;
            foreach($actions as $key => $action) {
                $time += $action->confirm_time; 
                if($action->id_service_fk == 3) $rate_way += $action->confirm_time;
                if($action->id_service_fk == 1) $rate_hours_1 += $action->confirm_time;
                if($action->id_service_fk == 2) $rate_hours_2 += $action->confirm_time;
                if($action->id_service_fk == 5) $rate_special += $action->unit_price_invoice;
                if($action->id_service_fk == 6) $rate_special -= $action->unit_price;

                if($order->id_dict_order_type_fk == 2) {
                    $byHours += ( ($action->confirm_time-$action->acc_limit) * $action->unit_price );
                }
                if($action->acc_cost_invoice > 0) {
                    array_push($costs, ['order' => $order->id, 'value' => $action->acc_cost_invoice, 'description' => $action->acc_cost_description, 'id' => $action->id]);
                }
                if($order->id_dict_order_type_fk == 2 && $action->acc_limit_price) {
                    $byHoursAll += ( $action->confirm_time * $action->acc_limit_price );
                    $advancedInfo = ' - ryczałt proporcjonalny';
                } else {
                    if($action->id_service_fk == 5)
                        $byHoursAll += $action->unit_price;
                    else if($action->id_service_fk == 6)
                        $byHoursAll -= $action->unit_price;
                    else 
                        $byHoursAll += ( ($action->confirm_time-$action->acc_limit) * $action->unit_price );
                }
				$confirmTime += $action->confirm_time;
            }
            
            if($order->id_dict_order_type_fk == 1) {
                $rate_constant = $order->rate_constatnt;
                array_push($items, ['order' => $order->id, 'rate' => 0, 'value' => $order->rate_constatnt, 'description' => 'Stała opłata']);
            }
            if($order->id_dict_order_type_fk == 2 && (($order->id_settlement_type_fk == 2 && $order->limit_hours <= $confirmTime) || $order->id_settlement_type_fk != 2)) {
                $rate_constant = $order->rate_constatnt;
                array_push($items, ['order' => $order->id, 'rate' => 0, 'value' => $order->rate_constatnt, 'description' => 'Stała opłata']);
            }
            
            if($order->id_dict_order_type_fk >= 2 && $byHoursAll > 0) {
                array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($byHoursAll, 2), 'description' => 'Wg godzin'.$advancedInfo]);
            }
            /*if($order->id_dict_order_type_fk == 2 && $order->limit_hours < ($rate_hours_1 + $rate_hours_2 + $rate_way) ) {
                //array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round(($rate_hours_1-$order->limit_hours)*$order->rate_hourly_1, 2), 'description' => 'Wg godzin']);
                array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($byHours, 2), 'description' => 'Wg godzin']);
            }
            if($order->id_dict_order_type_fk == 3 && $rate_hours_1 > 0 ) {
                array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($rate_hours_1*$order->rate_hourly_1,2), 'description' => 'Wg godzin - czynności merytoryczne']);
            }
            if($order->id_dict_order_type_fk == 4 && $rate_hours_1 > 0 ) {
                $sql = "select id_employee_fk, id_dict_employee_type_fk, sum(confirm_time) as limit_time, max(action_date) as action_date from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk "
                        ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_service_fk = 1 and id_order_fk = ".$order->id." and acc_period = '".$period."'"
                        ." group by id_employee_fk, id_dict_employee_type_fk"; 
                $settleData = Yii::$app->db->createCommand($sql)->query();
                foreach($settleData as $key => $employee) {
                    $hourly_1 = $order->rate_hourly_1;
                    if($order->id_dict_order_type_fk == 4) {
                        $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_dict_employee_type_fk'], $employee['id_employee_fk'], $order->id_customer_fk, $order->id, $employee['action_date']);
                        if($rateEmployee) $hourly_1 = $rateEmployee->unit_price;
                    }
                    $employeeData = \backend\Modules\Company\models\CompanyEmployee::findOne($employee['id_employee_fk']);
                    array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($hourly_1*$employee['limit_time'],2), 'description' => 'Wg godzin - czynności merytoryczne pracownika '.$employeeData->fullname]);
                } 
            }
            if($order->id_dict_order_type_fk >= 3 && $rate_hours_2 > 0) {
                array_push($items, ['order' => $order->id, 'rate' => 2, 'value' => round($rate_hours_2*$order->rate_hourly_2,2), 'description' => 'Wg godzin - czynności administracyjne']);
            }
            if($order->id_dict_order_type_fk >= 3 && $rate_way > 0) {
                array_push($items, ['order' => $order->id, 'rate' => 2, 'value' => round($rate_way*$order->rate_hourly_way,2), 'description' => 'Przejazdy']);
            }*/
            /*if($rate_special > 0) {
                array_push($items, ['order' => $order->id, 'rate' => 2, 'value' => round($rate_special,2), 'description' => 'Czynności specjalne']);
            }*/
            
            AccActions::updateAll(['id_invoice_fk' => $new->id], "(id_invoice_fk = 0 or id_invoice_fk is null) and status = 1 and id_order_fk = :order  and acc_period = '".$period."'", [':order' => $order->id]);

            foreach($items as $key => $item) {
                $newItem = new AccInvoiceItem();
                $newItem->item_no = $key + 1;
                $newItem->id_invoice_fk = $new->id;
                $newItem->id_order_fk = $item['order'];
                $newItem->item_name = $item['description'];
                $newItem->amount_net = $item['value'];
                
                $profit += $item['value'];

                $newItem->save();
            }
            
            foreach($costs as $key => $cost) {
                $newItem = new AccInvoiceItem();
                $newItem->item_no = $key + 1;
                $newItem->id_invoice_fk = $new->id;
                $newItem->id_order_fk = $cost['order'];
                $newItem->item_name = $cost['description'];
                $newItem->amount_net = $cost['value'];
                $newItem->is_cost = 1;
                $newItem->id_acc_action_id = $cost['id'];
                
                $costNet += $cost['value'];

                $newItem->save();
            }
            
            $new->amount_net = $profit;
            $new->save();
            
            $periodRecord->all_time = $time + ((!empty($periodRecord->all_time)) ? $periodRecord->all_time : 0);
            $periodRecord->all_profit = round($profit*$new->rate_exchange,2) + ((!empty($periodRecord->all_profit)) ? $periodRecord->all_profit : 0);
            $periodRecord->all_cost = round($costNet*$new->rate_exchange,2) + ((!empty($periodRecord->all_cost)) ? $periodRecord->all_cost : 0);
            
            $periodRecord->save();
            
            if( $discount ) {
                $sqlUpdate = "update {{%acc_discount}} set "
							."id_invoice_fk = ".$new->id
							.", discount_amount = ".(round($profit*$discount->discount_percent/100, 2)) 
							.", discount_amount_native = ".(round($profit*$discount->discount_percent*$new->rate_exchange/100, 2)) 
                            ." where id = ".$discount->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
            }
			
			/*if( $correction ) {
                $sqlUpdate = "update {{%acc_correction}} set "
							."id_invoice_fk = ".$new->id
                            ." where id = ".$discount->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
            }*/
            
            $transaction->commit();
            
            /*if( !isset($_GET['ajax']) ) {
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Faktura została wygenerowana')  );
                return $this->redirect(['view', 'id' => $new->id]);
            } else {*/
                //Yii::$app->response->format = Response::FORMAT_JSON;
                //return array('success' => true,  'action' => 'genInvoice', 'alert' => 'Faktura została wygenerowana', 'table' => '#table-settlements');
                if( isset($_GET['gen']) && $_GET['gen'] == 1  ) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true, 'alert' => 'Faktura została wygenerowana', 'table' => '#table-actions'];
                } else {
                    return $new->id;
                }
           // }
        } catch (Exception $e) {
            $transaction->rollBack();
            if( isset($_GET['gen']) && $_GET['gen'] == 1 ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => false, 'alert' => 'Faktura nie została wygenerowana', 'table' => '#table-actions'];
            } else {
                return false;
            }
            /*if( !isset($_GET['ajax']) ) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie została wygenerowana')  );
                return $this->redirect(['create', 'id' => $id]);	
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false,  'action' => 'genInvoice', 'alert' => 'Faktura nie została wygenerowana', 'table' => '#table-settlements'  );
            }*/
        } 
    }
    
    public function actionGen() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $itemsJson = $_POST['items'];
        $items = ($itemsJson) ? json_decode($itemsJson) : []; 
	
        $invoices = [];
        foreach($items as $key => $item) {
            if(isset($item->state) && $item->state) {
                $result = $this->actionGenerate($item->id, $item->period);
                if($result) array_push($invoices, $result);
            }
        }
        
        $stats = [];
        
		$sql = "select period_month, period_year, all_time, all_profit, all_cost from {{%acc_period}}"
			." where concat(period_year,'-',lpad(period_month,2,'0'),'-01') >= Date_add(Now(),interval - 12 month)"
			." order by period_year, period_month";
        $periodsData = Yii::$app->db->createCommand($sql)->query(); 
        $temp = [];
        foreach($periodsData as $key => $item) {
            $temp[$item['period_month']] = ['profit' => round($item['all_profit']/1000, 2), 
                                            'rate' => (($item['all_time']!=0) ? round((($item['all_time']) ? ($item['all_profit']/$item['all_time']) : 0), 2) : 0),
                                            'label' => strval($item['period_month']).'.'.strval($item['period_year'])];
        }
        $stats['profits'] = []; $stats['rates'] = []; $stats['labels'] = [];
        foreach($temp as $j => $value) {
            array_push($stats['profits'], $value['profit']);
            array_push($stats['rates'], $value['rate']);
            array_push($stats['labels'], $value['label']);
        }
        $chartRefresh = $this->renderAjax('_chart', ['stats' => $stats]);
		
		if(count($invoices) > 0) {
			return array('success' => true,  'action' => 'genInvoice', 'alert' => 'Faktury została wygenerowane. Pobierz wygenerowane dokumenty w formacie <a href="'.Url::to('/accounting/invoice/package').'?type=xls&ids='.implode(',', $invoices).'">xls</a> lub <a href="'.Url::to('/accounting/invoice/package').'?type=pdf&ids='.implode(',', $invoices).'">pdf</a>', 'table' => '#table-settled', 'stats' => $stats  );
	    } else {
			return array('false' => true,  'action' => 'genInvoice', 'alert' => 'Faktury nie została wygenerowane', 'errors' => [0 => 'Żadna faktura nie została wygenerowana'], 'table' => '#table-settled', 'stats' => $stats  );
		}
    }
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1, 'id_order_fk' => $id] ); 
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActions'])) {
            $params = $post['AccActions']; 
            if(isset($params['description']) && !empty($params['description']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_employee_fk = ".$params['id_employee_fk']);
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("action_date >= '".$params['date_from']."'");
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("action_date <= '".$params['date_to']."'");
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
        } 
 
        //$count = $fieldsDataQuery->count();
        
        $sortColumn = 'is_priority desc, rank_priority, action_date, id';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname'.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'is_confirm', "action_date", "unit_time", "unit_price", "unit_price_invoice", "acc_price",  "confirm_price", "confirm_time", "is_gratis", "is_priority",
                     "acc_limit", "lastname", "firstname", "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_invoice", "acc_cost_description"])
            ->from('{{%acc_actions}} a')
            ->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'id_invoice_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
        $count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn. ' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->orderby('is_priority desc, rank_priority, action_date')->all();
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        $tmp = [];

        foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
          
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['name'] = $value['name'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = ($value['type_fk'] == 5 || $value['type_fk'] == 6) ? $value['name'] : ($value['lastname'] .' '.$value['firstname']);
            $tmp['unit_time'] = number_format ($value['confirm_time'], 2, "," ,  " "); //round($value['unit_time']/60,2);
            $tmp['price'] = ($value['acc_limit'] == $value['confirm_time']) ? 0 : round($value['unit_price'], 2);
            $tmp['price'] = ($value['type_fk'] == 5 || $value['type_fk'] == 6) ? 0 : $tmp['price'];
            $tmp['amount'] = ($value['type_fk'] == 5 || $value['type_fk'] == 6) ? $value['unit_price_invoice']: (($value['is_gratis']) ? 'gratis' : round( ($value['unit_price']*($value['confirm_time']-$value['acc_limit'])),2));
            $tmp['amount'] = ($value['type_fk'] == 6) ? '<span class="text--red">'.(-1*$value['unit_price']).'</span>': $tmp['amount'];
            $tmp['in_limit'] = number_format ($value['acc_limit'], 2, "," ,  " ");
            $tmp['service'] = $type; //.(($value['is_gratis']) ? '<br /><i class="fa fa-gift text--pink"></i>' : '');
            if($value['is_priority']) {
                $tmp['service'] .= '<br /><i class="fa fa-flag text--red" title="Priorytetowa"></i>'; 
            }
            $tmp['description'] = $value['description'];
            $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
            $tmp['cost'] = ($value['acc_cost_invoice']) ? $value['acc_cost_invoice'] : 0;
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            $tmp['className'] = ($value['is_gratis']) ? 'text--pink' : '';
            
            array_push($fields, $tmp); $tmp = [];
        }
		
		return ['total' => $count,'rows' => $fields];
	}
       
    public function actionExportinv($id, $save) {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $employees = ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false );
        
        $model = $this->findModel($id);
        
        $objPHPExcel->setActiveSheetIndex(0);
        if($employees)
            $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        else
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozliczenie zlecenia "'.$model->order['name'].'" za okres '.(date('Y-m', strtotime($model->date_issue))));
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF0000CD');
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(38);
        //$objPHPExcel->getActiveSheet()->setCellValue('D1', \PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
        //$objPHPExcel->getActiveSheet()->getStyle('D1')->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
        //$objPHPExcel->getActiveSheet()->setCellValue('E1', '#12566');
        if($employees)
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Pracownik');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A').'2', 'Data');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B').'2', 'Opis');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C').'2', 'Czas');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D').'2', 'Stawka');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E').'2', 'Kwota');
        
        $i = 3;
        foreach($model->actions as $key => $record){ 
            if($record->type_fk != 4 && $record->type_fk != 5 && $record->type_fk != 6) {
                //if(!$record->acc_limit) $record->acc_limit = 0;
                /*if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                    $record->unit_price = 0;
                } */
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->employee['fullname'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $record->confirm_time);
				/* price */
                $price = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] == 2 && $record->acc_limit_price) ? $record->acc_limit_price : $record->unit_price;
                $price = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] != 2 && $record->acc_limit == $record->confirm_time) ? 0 : $price;
                $inLimit = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] == 2 && $record->acc_limit_price) ? 0 : $record->acc_limit; 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, $price)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, round($price*($record->confirm_time-$inLimit), 2))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                
                $i++;
            }
            if($record->type_fk == 5 && $record->unit_price && $record->unit_price != 0) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, (($record->type_fk == 5) ? $record->name : $record->employee['fullname']) ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->unit_price_invoice)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                
                $i++;
            }   
            if($record->type_fk == 6) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, (($record->type_fk == 6) ? $record->name : $record->employee['fullname']) ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, -1*$record->unit_price)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'F':'E').$i)->getFont()->getColor()->setARGB('FF1E90FF');
                $i++;
            }   
            if($record->is_shift) {
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'F':'E').($i-1))->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'F':'E').($i-1))->getFill()->getStartColor()->setARGB('FFFFFFE0');
            } 
        }  
        $iData = $i-1;
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.($i-1))->getAlignment()->setWrapText(true);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:E'.($i-1))->getAlignment()->setWrapText(true);
       
        $profit = 0; $discount = 0;
        foreach($model->profit as $key => $record){             
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->amount_net)->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            
            if($record->is_cost == 1)
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'F':'E').$i)->getFont()->getColor()->setARGB('FF0000');	
            $i++; 
            $profit += $record->amount_net; 
        } 
        
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
        $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'Razem'); 
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, '=SUM('.(($employees)?'F':'E').($iData+1).':'.(($employees)?'F':'E').($i-1).')')->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $i++;
        
        if($model->discount) {
            $discount = round($profit*$model->discount/100, 2);
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'Rabat '.$model->discount.'%'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $discount)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            
            ++$i;
        }
        $costs = 0;
        foreach($model->costs as $key => $record){             
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->amount_net)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            
            if($record->is_cost == 1)
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'F':'E').$i)->getFont()->getColor()->setARGB('FF0000');	  
            $i++; 
            
            $costs += $record->amount_net;
        } 
        
        if($costs > 0 || $discount > 0) {
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'Do zapłaty'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, ($profit+$costs-$discount))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            
            ++$i;
        }
        if($model->gratis) {
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':E'.$i);
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'GRATIS'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFont()->getColor()->setARGB('FFFFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FF0000CD');
            ++$i;
            foreach($model->gratis as $key => $record){ 
                if($record->type_fk != 4) {
                    //if(!$record->acc_limit) $record->acc_limit = 0;
                    /*if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
                    if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->employee['fullname'] ); 
                    //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                    //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $record->confirm_time); 
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                        
                    $i++; 
                }
            }  
        }
        // Set cell number formats
        //$objPHPExcel->getActiveSheet()->getStyle('E4:E'.($i-3))->getNumberFormat()->setFormatCode('#,##0.00');

        // Set column widths
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        if($employees) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        } else {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        }
        // Set fonts
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setName('Candara');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setUnderline(\PHPExcel_Style_Font::UNDERLINE_SINGLE);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        //$objPHPExcel->getActiveSheet()->getStyle('D13')->getFont()->setBold(true);
        //$objPHPExcel->getActiveSheet()->getStyle('E13')->getFont()->setBold(true);

        // Set alignments
        /*$objPHPExcel->getActiveSheet()->getStyle('D11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('D12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('D13')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);*/

        $objPHPExcel->getActiveSheet()->getStyle('B5')->getAlignment()->setShrinkToFit(true);

        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
                'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
            ),
        );
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$iData)->applyFromArray($styleThinBlackBorderOutline);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:E'.$iData)->applyFromArray($styleThinBlackBorderOutline);
        // Set thick brown border outline around "Total"
        $styleThickBrownBorderOutline = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => 'FF993300'),
                )
            ),
        );
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A'.($iData+1).':F'.($i-1))->applyFromArray($styleThinBlackBorderOutline);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A'.($iData+1).':E'.($i-1))->applyFromArray($styleThinBlackBorderOutline);
        /*$objPHPExcel->getActiveSheet()->getStyle('D13:E13')->applyFromArray($styleThickBrownBorderOutline);*/

        // Set fills
       ///// $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
       ///// $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FF808080');

        // Set style for header row using alternative method
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.( ($employees) ? 'F' : 'E').'2')->applyFromArray(
                array(
                    'font'    => array(
                        'bold'      => true
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                        'top'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'fill' => array(
                        'type'       => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                        'rotation'   => 90,
                        'startcolor' => array(
                            'argb' => 'FFF5F5DC'
                        ),
                        'endcolor'   => array(
                            'argb' => 'FFFFFFFF'
                        )
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                        'left'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray(
                array(
                    'borders' => array(
                        'right'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
        );

        // Unprotect a cell
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
       
        // Add a drawing to the worksheet
        $objDrawing = new \PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        if(\Yii::$app->params['env'] == 'prod') {
            $objDrawing->setPath('/var/www/html/ultima/frontend/web/images/logo_dark.jpg');
        } else {
            $objDrawing->setPath(\Yii::getAlias('@webroot').'/images/logo_dark.jpg');
        }
        //$objDrawing->setPath(Url::base(true).'/images/logo_dark.jpg');
        $objDrawing->setHeight(50);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        // Play around with inserting and removing rows and columns
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(6, 10);
        $objPHPExcel->getActiveSheet()->removeRow(6, 10);
        $objPHPExcel->getActiveSheet()->insertNewColumnBefore('E', 5);
        $objPHPExcel->getActiveSheet()->removeColumn('E', 5);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BInvoice&RPrinted on &D');
        $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename first worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Rozliczenie');

        // Create a new worksheet, after the default sheet
        /*$objPHPExcel->createSheet();

        // Llorem ipsum...
        $sLloremIpsum = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus eget ante. Sed cursus nunc semper tortor. Aliquam luctus purus non elit. Fusce vel elit commodo sapien dignissim dignissim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur accumsan magna sed massa. Nullam bibendum quam ac ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin augue. Praesent malesuada justo sed orci. Pellentesque lacus ligula, sodales quis, ultricies a, ultricies vitae, elit. Sed luctus consectetuer dolor. Vivamus vel sem ut nisi sodales accumsan. Nunc et felis. Suspendisse semper viverra odio. Morbi at odio. Integer a orci a purus venenatis molestie. Nam mattis. Praesent rhoncus, nisi vel mattis auctor, neque nisi faucibus sem, non dapibus elit pede ac nisl. Cras turpis.';

        // Add some data to the second sheet, resembling some different data types
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Terms and conditions');
        $objPHPExcel->getActiveSheet()->setCellValue('A3', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A4', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A5', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A6', $sLloremIpsum);

        // Set the worksheet tab color
        $objPHPExcel->getActiveSheet()->getTabColor()->setARGB('FF0094FF');;

        // Set alignments
        $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getAlignment()->setWrapText(true);

        // Set column widths
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(80);

        // Set fonts
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setUnderline(\PHPExcel_Style_Font::UNDERLINE_SINGLE);

        $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getFont()->setSize(8);

      
        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename second worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Terms and conditions');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);*/
		
        $dateTmp = date('m.Y', strtotime($model->date_issue));
        
        /*if( isset($_GET['type']) && $_GET['type'] == 'pdf') {
            $filename = $model->order['customer']['name'].'_'.$model->order['name'].'_'.$dateTmp.".pdf";
            
            $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
            //$rendererLibraryPath = dirname(__FILE__) . '/../../../../vendor/mpdf/mpdf';
            $rendererLibraryPath = \Yii::getAlias('@webroot').'/../../vendor/mpdf/mpdf';
            //echo $rendererLibraryPath;exit;
            
            if (\PHPExcel_Settings::setPdfRenderer( $rendererName,  $rendererLibraryPath  )) {

                $objWriterPDF = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
                $objWriterPDF->writeAllSheets();
                //$objWriterPDF->save("export/invoices/".$filename);
                
                header('Content-Type: application/pdf');
                //header('Content-Disposition: attachment;filename='.$filename .' ');
                header("Content-disposition: attachment; filename=\"".$filename."\""); 
                header('Cache-Control: max-age=0');			
                $objWriterPDF->save('php://output');
            }*/
        if($save) {
            $filename = trim( ($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.trim($model->order['name']).'_'.$dateTmp.".xlsx";
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
            $objWriter->save(\Yii::getAlias('@webroot')."/uploads/temp/".$filename);
            
            return $filename;
        } else {
            $filename = ( ($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.$model->order['name'].'_'.$dateTmp.".xlsx";
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }
	}
    
    public function actionExportbasic($id, $save) {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $employees = ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false );
        
        $model = $this->findModel($id);
        
        $objPHPExcel->setActiveSheetIndex(0);
        if($employees)
            $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
        else
            $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozliczenie zlecenia "'.$model->order['name'].'" za okres '.(date('Y-m', strtotime($model->date_issue))));
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF0000CD');
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(38);
        //$objPHPExcel->getActiveSheet()->setCellValue('D1', \PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
        //$objPHPExcel->getActiveSheet()->getStyle('D1')->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
        //$objPHPExcel->getActiveSheet()->setCellValue('E1', '#12566');
        if($employees)
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Pracownik');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A').'2', 'Data');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B').'2', 'Opis');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C').'2', 'Czas');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D').'2', 'Stawka');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E').'2', 'Kwota');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F').'2', 'Koszt');
        
        $i = 3;$gratis = [];
        foreach($model->all as $key => $record){ 
            if($record->type_fk != 5 && $record->type_fk != 6) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->employee['fullname'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                
                $objRichText = new \PHPExcel_RichText();
                $objRichText->createTextRun($record->description)->getFont()->setColor(new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLACK) );
                if($record->acc_cost_description) {
                    $break = ($record->type_fk == 4) ? 'Koszt: ' : PHP_EOL.'Koszt: ';
                    $objRichText->createTextRun($break.$record->acc_cost_description)->getFont()->setColor( new \PHPExcel_Style_Color(  \PHPExcel_Style_Color::COLOR_RED ) );
                }
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, $objRichText);
                //$objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $record->confirm_time); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, $record->unit_price)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                if($record->is_confirm)
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, round($record->unit_price*($record->confirm_time-$record->acc_limit), 2))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                if($record->is_gratis) {
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, 'gratis')->getStyle((($employees)?'F':'E').$i); 
                    array_push($gratis, $i);
                }
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                
                if($record->type_fk == 4)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FF0000');
                if($record->is_gratis)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FFFF00FF');
                
                $i++;
            }
            if($record->type_fk == 5 && $record->unit_price && $record->unit_price != 0) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, (($record->type_fk == 5) ? $record->name : $record->employee['fullname']) ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->unit_price_invoice)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $i++;
            }   
            if($record->type_fk == 6) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->name ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                //$objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, $record->description); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, -1*$record->unit_price)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'H':'G'). $i, $record->case['name']); 
                
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FF1E90FF');
                $i++;
            }   
            if($record->is_shift) {
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'G':'F').($i-1))->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'G':'F').($i-1))->getFill()->getStartColor()->setARGB('FFFFFFE0');
            } 
            
        }
        $iData = $i-1;
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:G'.($i-1))->getAlignment()->setWrapText(true);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.($i-1))->getAlignment()->setWrapText(true);
       
        /*$profit = 0; $discount = 0;
        foreach($model->profit as $key => $record){             
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->amount_net)->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            
                   $i++; 
            $profit += $record->amount_net; 
        } */
        
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Wg godzin'); 
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        if(count($gratis) > 0) { $minusTime = ('-'.(($employees)?'D':'C')).implode(('-'.(($employees)?'D':'C')), $gratis); } else {$minusTime = '';}
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, '=SUM('.(($employees)?'D':'C').'3'.':'.(($employees)?'D':'C').($i-1).')'.$minusTime)->getStyle((($employees)?'D':'C').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, '=SUM('.(($employees)?'F':'E').'3'.':'.(($employees)?'F':'E').($i-1).')')->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, '=SUM('.(($employees)?'G':'F').'3'.':'.(($employees)?'G':'F').($i-1).')')->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $i++;
        
        if($model->order['id_dict_order_type_fk'] == 1 || $model->order['id_dict_order_type_fk'] == 2) {
            //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Stała opłata'); 
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $model->order['rate_constatnt'])->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            $i++;
        }
        
        /* podsumowanie */
        //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Razem'); 
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        
        if($model->order['id_dict_order_type_fk'] == 1 || $model->order['id_dict_order_type_fk'] == 2) {
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, "=".(($employees)?'F':'E').($i-1)."+".(($employees)?'F':'E').($i-2)."+".(($employees)?'G':'F').($i-2))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        } else {
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, "=".(($employees)?'F':'E').($i-1)."+".(($employees)?'G':'F').($i-1))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');        
        }
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        $i++;
        
        if(count($gratis) > 0) {
            $minusGratis = ('='.(($employees)?'D':'C')).implode(('+'.(($employees)?'D':'C')), $gratis); 
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'GRATIS'); 
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFF00FF');
            
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $minusGratis)->getStyle((($employees)?'D':'C').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->getStartColor()->setARGB('FFFF00FF');
        }
        
        /*if($model->discount) {
            $discount = round($profit*$model->discount/100, 2);
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'Rabat '.$model->discount.'%'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $discount)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            
            ++$i;
        }*/
       /* $costs = 0;
                
        if($costs > 0 || $discount > 0) {
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'Do zapłaty'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, ($profit+$costs-$discount))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            
            ++$i;
        }*/
        /*if($model->gratis) {
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':E'.$i);
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, 'GRATIS'); 
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFont()->getColor()->setARGB('FFFFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'. $i)->getFill()->getStartColor()->setARGB('FF0000CD');
            ++$i;
            foreach($model->gratis as $key => $record){ 
                if($record->type_fk != 4) {
                    //if(!$record->acc_limit) $record->acc_limit = 0;
                
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->action_date ); 
                    $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, str_replace(array("\n", "\r"), '', $record->description)); 
                    //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                    $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->confirm_time); 
                    $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, 0)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, 'gratis')->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                    if($employees)
                        $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->employee['fullname'] ); 
                    $i++; 
                }
            }  
        }*/
        // Set cell number formats
        //$objPHPExcel->getActiveSheet()->getStyle('E4:E'.($i-3))->getNumberFormat()->setFormatCode('#,##0.00');

        // Set column widths
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        if($employees) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        } else {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        }
        $objPHPExcel->getActiveSheet()->setAutoFilter('A2:'.(($employees)?'G':'F').'2');	
        --$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.(($employees)?'G':'F').$i)->applyFromArray($styleArray);

          // Unprotect a cell
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
      
        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename first worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Rozliczenie');
		
        $dateTmp = date('m.Y', strtotime($model->date_issue));
        
        if($save) {
            $filename = ( ($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.$model->order['name'].'_'.$dateTmp.".xlsx";
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save("uploads/temp/".$filename);
            
            return $filename;
        } else {
            $filename = ( ($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.$model->order['name'].'_'.$dateTmp.".xlsx";
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }
	}
    
    public function actionPrintpdf($id, $save) {
        error_reporting(0); 
        $model = $this->findModel($id);
        
        $dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = (($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.$model->order['name'].'_'.$dateTmp.".pdf";
        
        /*$mpdf = new \Mpdf\Mpdf();
       // $mpdf->AddPage('L');
		$mpdf->useOnlyCoreFonts = true;    // false is default
        
		//$mpdf->autoPageBreak = false;
		//$mpdf->setAutoTopMargin = false;
		//$mpdf->setAutoBottomMargin = false; 
		
		//$mpdf->autoMarginPadding = 2;  
		//$mpdf->setAutoBottomMargin = 'stretch';
		$mpdf->list_indent_first_level = 0;
        //$mpdf->autoPageBreak = false;
       // $mpdf->use_kwt = true; 
       // $mpdf->useSubstitutions=false;
        //$mpdf->simpleTables = true;
		$mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Faktura');
        //$mpdf->SetDisplayMode('fullpage');
        //$mpdf->list_indent_first_level = 0;
		//$mpdf->shrink_tables_to_fit = 1.2;
        //$mpdf->_setPageSize('A4', 'L');
        $mpdf->SetAuthor("Ultima Ratio");
        //$mPDF1->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        
        //$mPDF1->SetHeader('|KARTA PRODUKTU SYSTEMU LSDD|');
		$mpdf->SetTextColor(149, 41, 68); 
        $mpdf->SetHeader('|Rozliczenie za okres '.$dateTmp.'|');
        $mpdf->SetFooter('{PAGENO}|'.date("y-m-d H:i:s"));
		//Yii::$app->homeUrl
		$stylesheet = file_get_contents(\Yii::getAlias('@webroot').'/css/invoice.css');
		$mpdf->WriteHTML($stylesheet, 1);		
	
		$mpdf->WriteHTML($this->renderPartial('printpdf', array('model' => $model, 'employees' => ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false )), true));
        
		$send = ( isset($_GET['send']) ) ? 1 : 0;
        
		if($save) {
            $mpdf->Output('uploads/temp/'.$filename, 'F');
			return $filename;
		} else {
            $mpdf->Output($filename, 'D');
			exit;
        }		
        //$mpdf->Output('test', 'D');*/

        // instantiate and use the dompdf class
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->renderPartial('printpdf', array('model' => $model, 'employees' => ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false )), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        //$dompdf->set_paper("A4", "landscape");

        //$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
        //$dompdf->getCanvas()->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
        // Render the HTML as PDF
        $dompdf->render();
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	
        // Output the generated PDF to Browser
    }
	
	public function actionInvpdf($id, $save) {
        error_reporting(0); 
        $model = $this->findModel($id);
        
        $dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = (($model->order['customer']['symbol']) ? $model->order['customer']['symbol'] : $model->order['customer']['name']).'_'.$model->order['name'].'_'.$dateTmp.".pdf";

        // instantiate and use the dompdf class
        /*$dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->renderPartial('invpdf', array('model' => $model), true), 'UTF-8');

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        
        if($save) {
            $filename = \frontend\controllers\CommonController::normalizeChars($filename); 
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $pdf = $dompdf->output();
            //file_put_contents(\Yii::getAlias('@webroot').'uploads/temp/'.$filename, $pdf); 
            $f = fopen(\Yii::getAlias('@webroot').'/uploads/temp/'.$filename, 'wb');

            fwrite($f, $pdf, strlen($pdf));
            fclose($f);
			return $filename;
		} else {
            $dompdf->stream($filename, array("Attachment" => true));
        }	*/
		
		$mpdf = new \Mpdf\Mpdf();
       // $mpdf->AddPage('L');
		$mpdf->useOnlyCoreFonts = true;    // false is default
        
		//$mpdf->autoPageBreak = false;
		//$mpdf->setAutoTopMargin = false;
		//$mpdf->setAutoBottomMargin = false; 
		
		//$mpdf->autoMarginPadding = 2;  
		//$mpdf->setAutoBottomMargin = 'stretch';
		$mpdf->list_indent_first_level = 0;
        //$mpdf->autoPageBreak = false;
       // $mpdf->use_kwt = true; 
       // $mpdf->useSubstitutions=false;
        //$mpdf->simpleTables = true;
		$mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Faktura');
        //$mpdf->SetDisplayMode('fullpage');
        //$mpdf->list_indent_first_level = 0;
		//$mpdf->shrink_tables_to_fit = 1.2;
        //$mpdf->_setPageSize('A4', 'L');
        $mpdf->SetAuthor("Ultima Ratio");
        //$mPDF1->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        
        //$mPDF1->SetHeader('|KARTA PRODUKTU SYSTEMU LSDD|');
		$mpdf->SetTextColor(149, 41, 68); 
        //$mpdf->SetHeader('|Rozliczenie za okres '.$dateTmp.'|');
        //$mpdf->SetFooter('{PAGENO}|'.date("y-m-d H:i:s"));
		//Yii::$app->homeUrl
		$stylesheet = file_get_contents(\Yii::getAlias('@webroot').'/css/_invoice.css');
		$mpdf->WriteHTML($stylesheet, 1);		
	
		$mpdf->WriteHTML($this->renderPartial('invpdf', array('model' => $model), true));
        
		$send = ( isset($_GET['send']) ) ? 1 : 0;
        
		if($save) {
            $mpdf->Output('uploads/temp/'.$filename, 'F');
			return $filename;
		} else {
            $mpdf->Output($filename, 'D');
			exit;
        }		
        //$mpdf->Output('test', 'D');
        // Output the generated PDF to Browser
    }
    
    public function actionExport() {
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                //'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Wykaz faktur');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
            ->setCellValue('B3', 'Zlecenie') 
            ->setCellValue('C3', 'Waluta') 
            ->setCellValue('D3', 'Numer')
            ->setCellValue('E3', 'Okres')
            ->setCellValue('F3', 'Data rozliczenia')
            ->setCellValue('G3', 'Kwota')
            ->setCellValue('H3', 'Rabat');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->getStartColor()->setARGB('D9D9D9');	

		$i=4; 
		$data = []; $where = [];
        
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            \Yii::$app->session->set('search.invoices', ['params' => $params, 'post' => $post]);


            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
			if(isset($params['date_issue']) && !empty($params['date_issue']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') = '".$params['date_issue']."'");
            }
            if( isset($params['id_customer_fk']) && strlen($params['id_customer_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "o.id_customer_fk = ".$params['id_customer_fk'] );
            }
            if( isset($params['id_currency_fk']) && strlen($params['id_currency_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "i.id_currency_fk = ".$params['id_currency_fk'] );
            }
        }  else {
            array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') = '".date('Y-m')."'");

        }     
       // $fieldsData = $fieldsDataQuery->all();
			
		$sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no', "date_format(date_issue, '%Y-%m') as date_issue", 'i.created_at as created_at', '(select sum(amount_net) from {{%acc_invoice_item}} ii where ii.id_invoice_fk = i.id) as amount_net', 'amount_gross', 'c.name as cname', 'c.symbol as csymbol', 'c.id as cid', 'o.name as oname', 'discount_amount', 'currency_symbol'])
            ->from('{{%acc_invoice}} as i')
            ->join('JOIN', '{{%acc_order}} as o', 'o.id=i.id_order_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = o.id_customer_fk')
            ->join('JOIN', '{{%acc_currency}} curr', 'curr.id = i.id_currency_fk')
            ->join('LEFT JOIN', '{{%acc_discount}} as d', '(d.id_invoice_fk = i.id and d.status=1)')
            ->where( ['i.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		/*$count = $query->count();*/
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
        
        foreach($rows as $record){ 
           
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, ( ($record['csymbol']) ? $record['cname'] : $record['cname']) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['oname'] );
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['currency_symbol'] );
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['system_no']); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, date('Y-m', strtotime($record['date_issue']))); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['created_at']); 
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, (($record['amount_net']) ? $record['amount_net'] : 0))->getStyle('F'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, ($record['discount_amount']) ? $record['discount_amount'] : 0)->getStyle('G'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                    
            $i++; 
        }  
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Faktury');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Faktury'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
	}
    
    public function actionSettlement($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        $model = Customer::findOne($id); 
        
        $sql = "select acc_period as acc_period, o.id as oid, o.name as oname, count(*) actions, i.id as iid, i.system_no as  system_no, o.id_customer_fk as id_customer_fk, "
				."(select discount_percent from {{%acc_discount}} where id_order_fk = a.id_order_fk and acc_period = a.acc_period and status = 1) as discount "
                ." from {{%acc_actions}} a join {{%acc_order}} o on (o.id=a.id_order_fk and o.id_customer_fk=".$id.") left join {{%acc_invoice}} i on (i.status = 1 and date_format(i.date_issue, '%Y-%m')=a.acc_period and i.id_order_fk=a.id_order_fk)"
                //." from {{%acc_actions}} a join {{%acc_order}} o on o.id=a.id_order_fk "
                ." where a.status=1 and a.is_confirm=1 and (id_invoice_fk=0 or id_invoice_fk is null) and (a.id_customer_fk = ".$id." or  a.id_customer_fk in (select id_customer_fk from {{%acc_order_item}} where id_order_fk = o.id))"
                //." where a.status=1 and a.is_confirm=1 and (id_invoice_fk=0 or id_invoice_fk is null) and a.id_customer_fk = ".$id
                ." group by acc_period, o.id, o.id_customer_fk, o.name, i.id, i.system_no order by 1";  // and (id_invoice_fk=0 or id_invoice_fk is null) 
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        
        $fields = [];
        
        foreach($data as $key=>$value) {            
            $tmp['confirm'] = 0;
			$tmp['disabled'] = ($value['iid']) ? 1 : 0;
            $tmp['order'] = $value['oname'].( ($value['iid']) ? '<p class="text--red">Na ten okres została już wystawiona faktura <b><a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['iid']]).'" title="Przejdź do faktury">'.$value['system_no'].'</a></b></p>' : '');
                $tmp['order'] .= (($value['id_customer_fk'] != $model->id) ? '<p class="text--pink">Rozliczenie w ramach zlecenia <a href="'.Url::to(['/crm/customer/view', 'id' => $value['id_customer_fk']]).'?tab=actions&period='.$value['acc_period'].'" title="Przejdź na kartę klienta"><b>'.$value['oname'].' [<i>'.(Customer::findOne($value['id_customer_fk'])['name']).'</i>]</b></a></p>' : '');
			$tmp['period'] = $value['acc_period'];
            $tmp['discount'] = ($value['discount']) ? $value['discount'] : 0;
			$tmp['id'] = $value['oid'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		//return ['total' => count($fields),'rows' => $fields];
        return $fields;
    }
    
    public function actionPackage($type, $ids) {
        $type = $_GET['type'];
        $ids = $_GET['ids'];
        $idsArr = explode(',', $ids);
        
		$zip = new \ZipArchive();
		$zipname = 'uploads/invoices/gen_'.Yii::$app->user->id.'_'.$type.'_'.date('Y_m_d_h_i_s').'.zip';
        $zip->open($zipname, \ZipArchive::CREATE);
        foreach($idsArr as $i => $inv) {
            if($type == 'pdf')
                $filename = $this->actionPrintpdf(CustomHelpers::encode($inv), true);
            else
                $filename = $this->actionExportinv(CustomHelpers::encode($inv), true);
            $zip->addFile('uploads/temp/'.$filename, $filename);
        }
		//$zip->addFile('uploads/CmVFduuO3J_1493999315.pdf', 'invoice2.pdf');
		 
		$zip->close();
				
		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename='.$zipname);
		header('Content-Length: ' . filesize($zipname));
		readfile($zipname);
		
		exit;
    }
    
    public function actionDiscount($id) {
        $invoice = $this->findModel($id);
        $profit = 0;
        foreach($invoice->profit as $key => $item) {
            $profit += $item->amount_net;
        }

        $discount = AccDiscount::find()->where(['status' => 1, 'id_invoice_fk' => $invoice->id])->one();
        $isNew = false; $percent = 0;
        
        if(!$discount) {
            $discount = new AccDiscount();
            $discount->id_invoice_fk = $invoice->id;
            $discount->id_order_fk = $invoice->id_order_fk;
            $discount->id_customer_fk = $invoice->order['id_customer_fk'];
            $discount->name = 'Rabat za okres '.date('Y-m', strtotime($invoice->date_issue));
            $discount->acc_period = date('Y-m', strtotime($invoice->date_issue));
            $isNew = true;
        } else {
            $percent = $discount->discount_percent;
        }
        
        $discount->user_action = 'modify';
        
        if (Yii::$app->request->isPost ) {
			$discount->load(Yii::$app->request->post());
            
            if($percent == $discount->discount_percent) {
                Yii::$app->getSession()->setFlash( 'warning',  'Wartość rabatu nie uległa zmianie' );   
                return $this->redirect(['view', 'id' => $invoice->id]); 
            }
            
            $discount->discount_amount = (round($profit * $discount->discount_percent/100, 2)); 
            $discount->discount_amount_native = (round($profit * $invoice->rate_exchange * $discount->discount_percent/100, 2)); 
            if(!$isNew && $discount->discount_percent == 0) {
                $discount->deleted_by = \Yii::$app->user->id;
                $discount->deleted_at = date('Y-m-d H:i:s');
                $discount->status = -2;
            }
            $transaction = \Yii::$app->db->beginTransaction(); 
            try {
                if($discount->save()) {
                    //\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $invoice->id_order_fk, 'period' => $discount->acc_period]);                
                    $sqlUpdate = "update {{%acc_actions}} set "
                        ." acc_price = round( confirm_price - (confirm_price*".$discount->discount_percent."/100), 2), acc_price_native = round( confirm_price_native - (confirm_price_native*".$discount->discount_percent."/100), 2)"
                        .", id_tax_fk = ".( ($discount->status == -2) ? '0' : $discount->id)
                        ." where is_confirm = 1 and status = 1 and id_invoice_fk = ".$invoice->id; 
                    \Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                    $transaction->commit();
                    if($discount->status == -2){
                       /* $sqlUpdateActions = "update {{%acc_actions}} set id_tax_fk = 0 where id_invoice_fk = ".$invoice->id;
                        Yii::$app->db->createCommand($sqlUpdateActions)->execute(); */
                        
                        Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Rabat został usunięty')  );   
                    } else {
                        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Rabat został ustawiony')  );   
                    }
                    //\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $invoice->id_order_fk, 'period' => date('Y-m', strtotime($invoice->date_issue))]); 
                    return $this->redirect(['view', 'id' => $invoice->id]);  
                } else {
                    $error = 'Rabat nie może zostać zmodyfikowany';
                    foreach($discount->getErrors() as $key => $item)  {
                        $error = $item;
                    }
                    Yii::$app->getSession()->setFlash( 'error',  $error );   
                    return $this->redirect(['view', 'id' => $invoice->id]); 
                }
            } catch (Exception $e) {
                $transaction->rollBack(); 
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Rabat nie może zostać zmodyfikowany')  );   
                return $this->redirect(['view', 'id' => $invoice->id]);      
            }
        }
        
        return $this->renderAjax('_formDiscount', ['model' => $discount]);
    }
	
	public function actionCorrection($id) {
        $invoice = $this->findModel($id);
		$amount = 0;

        $correction = AccCorrection::find()->where(['status' => 1, 'id_invoice_fk' => $invoice->id])->one();
        $isNew = false; 
        
        if(!$correction) {
            $correction = new AccCorrection();
            $correction->id_invoice_fk = $invoice->id;
            $correction->id_order_fk = $invoice->id_order_fk;
            $correction->id_customer_fk = $invoice->order['id_customer_fk'];
            $correction->name = 'Korekta za okres '.date('Y-m', strtotime($invoice->date_issue . "-1 months"));
            $correction->acc_period = date('Y-m', strtotime($invoice->date_issue));
            $isNew = true;
        } else {
            $amount = $correction->correction_amount;
        }
        
        $correction->user_action = 'modify';
        
        if (Yii::$app->request->isPost ) {
			$correction->load(Yii::$app->request->post());
            
            if($amount == $correction->correction_amount) {
                Yii::$app->getSession()->setFlash( 'warning',  'Wartość korekty nie uległa zmianie' );   
                return $this->redirect(['view', 'id' => $invoice->id]); 
            }
            
            //$correction->correction_amount = (round($profit * $discount->discount_percent/100, 2)); 
            //$correction->discount_amount_native = (round($profit * $invoice->rate_exchange * $discount->discount_percent/100, 2)); 
            if(!$isNew && $correction->correction_amount == 0) {
                $correction->deleted_by = \Yii::$app->user->id;
                $correction->deleted_at = date('Y-m-d H:i:s');
                $correction->status = -2;
            }
            $transaction = \Yii::$app->db->beginTransaction(); 
            try {
                if($correction->save()) {
                    $transaction->commit();
                    if($correction->status == -2){
                        Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Korekta została usunięta')  );   
                    } else {
                        Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Korekta została ustawiona')  );   
                    }
                    //\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $invoice->id_order_fk, 'period' => date('Y-m', strtotime($invoice->date_issue))]); 
                    return $this->redirect(['view', 'id' => $invoice->id]);  
                } else {
                    $error = 'Korekta nie może zostać zmodyfikowana';
                    foreach($correction->getErrors() as $key => $item)  {
                        $error = $item;
                    }
                    Yii::$app->getSession()->setFlash( 'error',  $error );   
                    return $this->redirect(['view', 'id' => $invoice->id]); 
                }
            } catch (Exception $e) {
                $transaction->rollBack(); 
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Korekta nie może zostać zmodyfikowana')  );   
                return $this->redirect(['view', 'id' => $invoice->id]);      
            }
        }
        
        return $this->renderAjax('_formCorrection', ['model' => $correction, 'currency' => $invoice->order['currency']]);
    }
}
