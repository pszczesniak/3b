<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccOrderSearch;
use backend\Modules\Accounting\models\AccOrderItem;
use backend\Modules\Accounting\models\AccInvoice;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccService;
use backend\Modules\Accounting\models\AccRate;
use backend\Modules\Accounting\models\AccDiscount;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * OrderController implements the CRUD actions for AccActions model.
 */
class OrderController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccOrderSearch();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.orders'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }  
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

		//$fieldsDataQuery = $fieldsData = CalCase::find()->where(['status' => 1, 'type_fk' => 1]);
        $post = $_GET;
        if(isset($_GET['AccOrderSearch'])) { 
            $params = $_GET['AccOrderSearch']; 
            \Yii::$app->session->set('search.orders', ['params' => $params, 'post' => $post]);
            if( isset($params['id_dict_order_type_fk']) && strlen($params['id_dict_order_type_fk']) ) {
               array_push($where, "id_dict_order_type_fk = ".$params['id_dict_order_type_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(id_customer_fk = ".$params['id_customer_fk'].") or (o.id in (select id_order_fk from {{%acc_order_item}} where status = 1 and id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['id_currency_fk']) && !empty($params['id_currency_fk']) ) {
                array_push($where, "id_currency_fk = ".$params['id_currency_fk']);
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "date_from >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) { 
                array_push($where, "IFNULL(date_to,'".date('Y-m-d')."') <= '".$params['date_to']."'");
            }
        }  	
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "id_customer_fk in ( select id_customer_fk from {{%company_department}} where id_department_fk in (".implode(',', $this->module->params['managers']).") )");
        }
			
		$fields = [];
		$tmp = [];
		
		$sortColumn = 'ifnull(`c`.`symbol`, `c`.`name`) collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'o.name collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'ifnull(`c`.`symbol`, `c`.`name`) collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.id_dict_order_type_fk as id_dict_order_type_fk', 'o.name as name', 'date_from', 'date_to', 
                      'ifnull(c.symbol, c.name) as cname', 'c.id as cid', 'type_name', 'type_color', 'type_symbol','o.type_fk as type_fk', 'currency_symbol',
                     // "concat_ws(' ', u.lastname, u.firstname) as uname",
                      "(select count(*) from {{%acc_invoice}} where id_order_fk = o.id and status = 1) as invoices",
                      "(select min(id) from {{%acc_order}} where id_parent_fk = o.id and status = 1) as annex",
                      "(select count(*) from {{%files}} where id_fk = o.id and status = 1 and id_type_file_fk = 7) as files"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_type}} at', 'at.id = o.id_dict_order_type_fk')
            ->join('JOIN', '{{%acc_currency}} cur', 'cur.id = o.id_currency_fk')
			//->join('JOIN', '{{%user}} u', 'u.id = o.created_by')
            ->where( ['o.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			//->join('JOIN', '{{%acc_type}} at', 'at.id = o.id_dict_order_type_fk')
			//->join('JOIN', '{{%user}} u', 'u.id = o.created_by')
            ->where( ['o.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; 
 
		$tmp = [];
		$invoices = 0;
		foreach($rows as $key=>$value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['name'] = $value['name'];
            if($value['annex']) {
                $tmp['name'] .= '<br /><small><a href="'.Url::to(['/accounting/order/updateajax', 'id' => $value['annex']]).'" data-toggle="modal" data-target="#modal-grid-item" class="text--purple gridViewModal" tile="zobacz aneks">zobacz aneks</a></small>';
            }
			$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['cname'].'</a>';
			$tmp['start'] = $value['date_from'];
            $tmp['currency'] = $value['currency_symbol'];
            $tmp['date_from'] = $value['date_from'];
            $tmp['date_from'] .= ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? '<br /><small class="text--red"> do '.$value['date_to'].'</small>' : (($value['date_to'])? ' <br ><small>do '.$value['date_to'].'</small>':'');
			//$tmp['creator'] = $value['uname'];
			$tmp['method'] = '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>';
			$tmp['merging'] = ($value['type_fk'] == 1) ? '<i class="fa fa-user text--purple"></i>' : '<i class="fa fa-users text--teal"></i>' ;
			$tmp['invoice'] = ($value['invoices'] == 0) ? '0' : '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'" title="Pokaż szczegóły rozliczenia">'.$value['invoices'].'</a>';
            $tmp['files'] =  '<span class="'.(($value['files'] == 0) ? 'text--grey' : 'bg-blue label').'">'.$value['files'] .'</span>';
            /*$tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default"  title="'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
                $tmp['actions'] .= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<a href="'.Url::to(['/accounting/order/updateajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('lsdd', 'Update').'" ><i class="fa fa-pencil"></i></a>' : '';
                $tmp['actions'] .= ( ($value['id_dict_order_type_fk'] == 2 || $value['id_dict_order_type_fk'] == 4) ? '<a href="'.Url::to(['/accounting/order/rates', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-cog\'></i>'.Yii::t('lsdd', 'Stawki').'" title="'.Yii::t('lsdd', 'Stawki').'" ><i class="fa fa-cog"></i></a>' : '');
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/discounts', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-percent\'></i>'.Yii::t('lsdd', 'Rabaty').'" title="'.Yii::t('lsdd', 'Rabaty').'" ><i class="fa fa-percent"></i></a>';                    
                $tmp['actions'] .= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ?  '<a href="'.Url::to(['/accounting/order/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-orders"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>' : '';
            $tmp['actions'] .= '</div>';*/
            $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . '<li data-table="#table-orders" ><a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'"><i class="fa fa-eye text--blue"></i>&nbsp;Karta zlecenia</a></li>'
                                        . ( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<li class="update" data-target="#modal-grid-item" data-table="#table-orders" href="'.Url::to(['/accounting/order/updateajax', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('app', 'Update').'"><a href="#"><i class="fa fa-pencil text--purple"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && !$value['annex']) ? '<li class="update" data-target="#modal-grid-item" data-table="#table-orders" href="'.Url::to(['/accounting/order/annex', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-level-up\'></i>'.Yii::t('app', 'Aneks').'"><a href="#"><i class="fa fa-level-up text--purple"></i>&nbsp;Aneks</a></li>' : '')                                        
                                        . ( ($value['id_dict_order_type_fk'] == 2 || $value['id_dict_order_type_fk'] == 4) ? '<li class="update" data-target="#modal-grid-item" data-table="#table-orders" href="'.Url::to(['/accounting/order/rates', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-cog\'></i>'.Yii::t('app', 'Stawki').'" data-label="Stawki"><a href="#"><i class="fa fa-cog text--pink"></i>&nbsp;Stawki</a></li>' : '')
                                        . '<li class="update" data-table="#table-orders" href="'.Url::to(['/accounting/order/discounts', 'id' => $value['id']]).'" data-target="#modal-grid-item" data-title="<i class=\'fa fa-percent\'></i>'.Yii::t('app', 'Rabaty').'" data-label="Rabaty"><a href="#"><i class="fa fa-percent text--orange"></i>&nbsp;Rabaty</a></li>' 
                                        . ( (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<li class="deleteConfirm" data-table="#table-orders" href="'.Url::to(['/accounting/order/deleteajax', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>Usuń" data-target="#modal-grid-item"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')

                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
           
			$tmp['className'] = ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionCreateajax($id) {
        $id = CustomHelpers::decode($id);
        $customer = \backend\Modules\Crm\models\Customer::findOne($id);
        
		$model = new AccOrder; 
        $model->customers = [];
		$model->status = 1;
        $model->name = 'Stała obsługa';
        $model->symbol = 'SO';
        $model->id_customer_fk = ($customer) ? $customer->id : 0;
        $model->id_dict_order_type_fk = 1;
        $model->date_from = '2017-01-01';
        
        $services = \backend\Modules\Accounting\models\AccService::find()->where(['status' => 1])->all();
        foreach($services as $key => $value) {
            if($value->type_fk == 1) $model->rate_hourly_1 = $value->unit_price;
            if($value->type_fk == 2) $model->rate_hourly_2 = $value->unit_price;
            if($value->type_fk == 3) $model->rate_hourly_way = $value->unit_price;
        }
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            if(!$model->customers) $model->customers = [];
			
			foreach($model->customers as $key => $customer) {
				if($customer == $model->id_customer_fk)
					unset($model->customers[$key]);
			}
			if(!$model->customers) $model->customers = [];
            $model->type_fk = ( count($model->customers) == 0 ) ? 1 : 2;
            if( $model->validate() && $model->save()) {

				foreach($model->customers as $key => $customer) {
                    if($customer != $model->id_customer_fk) {
						$modelScale = new AccOrderItem;     
						$modelScale->id_order_fk = $model->id;
						$modelScale->id_customer_fk = $customer;
						$modelScale->save();
				    }
				}
  
				return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Zlecenie zostało zapisane'  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionBind($id) {
        $id = CustomHelpers::decode($id);
        $customer = \backend\Modules\Crm\models\Customer::findOne($id);
        
		$model = new AccOrderItem; 
		$model->status = 1;
		
        $model->id_customer_fk = ($customer) ? $customer->id : 0;
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            $exist = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['status' => 1, 'id_customer_fk' => (($customer) ? $customer->id : 0), 'id_order_fk' => $model->id_order_fk])->count();
            if($exist) {
                return array('success' => false, 'html' => $this->renderAjax('_formBind', [ 'model' => $model]), 'errors' => [0 => 'Klient jest już powiązany z tym zleceniem'] );	
            }

            if( $model->validate() && $model->save()) {
				$order = AccOrder::findOne($model->id_order_fk);
                $order->type_fk = 2;
                $order->save();
  
				return array('success' => true,  'index' =>0, 'id' => $model->id, 'alert' => 'Powiązanie zostało zapisane', 
                             'action' => 'suborderAdd', 'item' => '<li id="oitem-'.$model->id.'">'.$model->order['customer']['name'].' [<a href="'.Url::to(['/accounting/order/view', 'id' => $model->id_order_fk]).'">'.$order['name'].'</a>]</li>'  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formBind', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formBind', [ 'model' => $model]);	
        }
	}
    
    public function actionUnbind($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $customer = \backend\Modules\Crm\models\Customer::findOne($id);
        if(!isset($_GET['order'])) {
            return array('success' => false, 'alert' => 'Problem z usunięciem', 'table' => '#table-orders' );	
        } else {
            $exist = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_customer_fk' => $customer->id, 'id_order_fk' => $_GET['order']])->count();
            if($exist > 0) {
                return array('success' => false, 'alert' => 'Nie można usunąć tego powiązania, ponieważ klient ma z tym zleceniem powiązane czynności', 'table' => '#table-orders' );
            }
        }
		
        $model = AccOrderItem::find()->where(['status' => 1, 'id_customer_fk' => $customer->id, 'id_order_fk' => $_GET['order']])->one(); 
        if(!$model) {
            return array('success' => true, 'alert' => 'Powiązanie zostało usunięte', 'table' => '#table-orders', 'action' => 'suborderDelete', 'id' => $model->id );	
        }
		$model->status = -1;
        $model->date_closed = date('Y-m-d H:i:s');
        $model->user_closed = \Yii::$app->user->id;
        
        if($model->save()) {
            $orderState = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['status' => 1, 'id_order_fk' => $_GET['order']])->count();
            if($orderState == 0) {
                $order = AccOrder::findOne($_GET['order']);
                $order->type_fk = 1;
                $order->save();
            }
            return array('success' => true, 'alert' => 'Powiązanie zostało usunięte', 'table' => '#table-orders', 'action' => 'suborderDelete', 'id' => $model->id );
        } else {
            return array('success' => false, 'alert' => 'Problem z usunięciem', 'table' => '#table-orders' );
        }
	}
    
    public function actionUpdateajax($id) {
        
		$model = $this->findModel($id); 
        $model->customers = [];
        $customersTemp = [];
       
        foreach($model->items as $key => $item) {
            array_push($model->customers, $item->id_customer_fk);
            array_push($customersTemp, $item->id_customer_fk);
        }
                   
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$model->customers) $model->customers = [];
			
			foreach($model->customers as $key => $customer) {
				if($customer == $model->id_customer_fk)
					unset($model->customers[$key]);
			}
			if(!$model->customers) $model->customers = [];
            $model->type_fk = ( count($model->customers) == 0 ) ? 1 : 2;
            if( $model->validate() && $model->save()) {
                
                foreach($customersTemp as $key => $value) {
                    if(!in_array($value, $model->customers)) {                      
						//return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						$exist = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->count();
						if($exist > 0) {
							$customer = \backend\Modules\Crm\models\Customer::findOne($value);
							return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						} else {						
							$orderItem = AccOrderItem::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->one();
							$orderItem->status = 0;
							$orderItem->user_closed = Yii::$app->user->id;
							$orderItem->date_closed = date('Y-m-d H:i:s');
							$orderItem->save();
						}
                    }
                }
                
                foreach($model->customers as $key => $value) {
                    if(!in_array($value, $customersTemp)) {
                        $modelScale = new AccOrderItem;     
                        $modelScale->id_order_fk = $model->id;
                        $modelScale->id_customer_fk = $value;
                        $modelScale->save();
                    }
                }
				return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Zmiany zostały zapisane', 
                            'rates' => $this->renderPartial('_rates', ['model' => $model]), 'ratesTable' => (($model->id_dict_order_type_fk == 2 || $model->id_dict_order_type_fk == 4) ? 1 : 0  ));	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionAnnex($id) {
        
		$order = $this->findModel($id);

        $model = new AccOrder();
        $model->id_parent_fk = $order->id;
        $model->symbol = $order->symbol;
        $model->name = $order->name;
        $model->date_from = date('Y-m-d');
        $model->id_customer_fk = $order->id_customer_fk;
        $model->id_dict_order_type_fk = $order->id_dict_order_type_fk;
        $model->id_settlement_type_fk = $order->id_settlement_type_fk;
        $model->id_currency_fk = $order->id_currency_fk;
        $model->customers = [];
        $customersTemp = [];
       
        foreach($order->items as $key => $item) {
            array_push($model->customers, $item->id_customer_fk);
            array_push($customersTemp, $item->id_customer_fk);
        }
                   
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            $oldOrderInUse = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_order_fk' => $order->id])->andWhere("action_date > '".(date('Y-m-d', strtotime('-1 day', strtotime($model->date_from))))."'")->max('action_date');
            if($oldOrderInUse) {
                return array('success' => false, 'html' => $this->renderAjax('_formAnnex', ['model' => $model]), 'errors' => ['order' => 'W systemie są czynności z dnia '.$oldOrderInUse.' powiązane ze zleceniem i nie można go zamknąć z datą '.(date('Y-m-d', strtotime('-1 day', strtotime($model->date_from))))] );	
            }
            
            if(!$model->customers) $model->customers = [];
			
			foreach($model->customers as $key => $customer) {
				if($customer == $model->id_customer_fk)
					unset($model->customers[$key]);
			}
			if(!$model->customers) $model->customers = [];
            $model->type_fk = ( count($model->customers) == 0 ) ? 1 : 2;
            if( $model->validate() && $model->save()) {
                
                foreach($customersTemp as $key => $value) {
                    if(!in_array($value, $model->customers)) {                      
						//return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						$exist = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->count();
						if($exist > 0) {
							$customer = \backend\Modules\Crm\models\Customer::findOne($value);
							return array('success' => false, 'errors' => [0 => 'Nie można usunąć powiązania z klientem '.$customer['name'].', ponieważ klient ma z tym zleceniem powiązane czynności'], 'table' => '#table-orders' );
						} else {						
							$orderItem = AccOrderItem::find()->where(['status' => 1, 'id_customer_fk' => $value, 'id_order_fk' => $model->id])->one();
							$orderItem->status = 0;
							$orderItem->user_closed = Yii::$app->user->id;
							$orderItem->date_closed = date('Y-m-d H:i:s');
							$orderItem->save();
						}
                    }
                }
                
                foreach($model->customers as $key => $value) {
                    if(!in_array($value, $customersTemp)) {
                        $modelScale = new AccOrderItem;     
                        $modelScale->id_order_fk = $model->id;
                        $modelScale->id_customer_fk = $value;
                        $modelScale->save();
                    }
                }
                
                $order->date_to = date('Y-m-d', strtotime('-1 day', strtotime($model->date_from)));
                $order->save();
                
				return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Zmiany zostały zapisane', 
                            'rates' => $this->renderPartial('_rates', ['model' => $model]), 'ratesTable' => (($model->id_dict_order_type_fk == 2 || $model->id_dict_order_type_fk == 4) ? 1 : 0  ));	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAnnex', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAnnex', [ 'model' => $model]);	
        }
	}
    
    public function actionView($id) {

        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model, 'stats' => AccOrder::getStats($model->id)] );
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        
        $existActions = AccActions::find()->where(['status' => 1, 'id_order_fk' => $model->id])->count();
        if($existActions) {
            return array('success' => false, 'alert' => 'Nie można usunąć umowy, ponieważ są do niej przypisane czynności', 'table' => '#table-orders' );
        }
        $existOrder = AccOrder::find()->where(['status' => 1, 'id_parent_fk' => $model->id])->count();
        if($existActions) {
            return array('success' => false, 'alert' => 'Nie można usunąć umowy, ponieważ jest do niej przypisany aneks', 'table' => '#table-orders' );
        }
        
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -2;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'table' => '#table-orders');	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Dane nie zostały usunięte', 'table' => '#table-orders' );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the AccOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = AccOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Zlecenie o wskazanym identyfikatorze nie zostało odnalezione w systemie.');
        }
    }
   
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1, 'id_order_fk' => $id] ); 
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActionsSearch'])) {
            $params = $post['AccActionsSearch']; 
            if(isset($params['description']) && !empty($params['description']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_employee_fk = ".$params['id_employee_fk']);
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("acc_period = '".$params['acc_period']."'");
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }

        } 
 
        //$count = $fieldsDataQuery->count();
        
        $sortColumn = 'acc_limit';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname'.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'id_service_fk',
                      'is_confirm', "action_date", "unit_time", "unit_price", "acc_price",  "confirm_price", "confirm_time",  "acc_limit", "acc_period",
                      "lastname", "firstname", "concat_ws(' ', e.lastname, e.firstname) as ename", 
                      "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "acc_cost_description", "id_invoice_fk",
                      "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])
            ->from('{{%acc_actions}} a')
            ->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'id_order_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
        //$count = $query->count();
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", "sum(acc_cost_net) as costs", "sum(confirm_time) as confirm_time", "sum(confirm_time*confirm_price) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.id_order_fk' => $id] );
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time'];
        
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        

        $tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].' cursor-pointer" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
               
            $periodAction = 'forward';
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            
            $tmp['name'] = $value['name'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['lastname'] .' '.$value['firstname'];
            $tmp['unit_time'] = number_format ($value['confirm_time'], 2, "," ,  " "); //round($value['unit_time']/60,2);
            $tmp['price'] = number_format ($value['unit_price'], 2, "," ,  " ");
            $tmp['amount'] = round( ($value['confirm_price']*$value['confirm_time']),2);
            $tmp['in_limit'] = number_format ($value['acc_limit'], 2, "," ,  " ");
            //$tmp['description'] = $value['description'].( ($value['acc_cost_description']) ? 'Koszt: '.$value['acc_cost_description'] : '' );
            $tmp['description'] = htmlentities(($value['id_service_fk'] == 4) ? ($value['acc_cost_description']) : $value['description']);
            $tmp['service'] = $type;
            $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';                
            //$tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
            if(!$value['id_invoice_fk']) {
                $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                if($periodAction == 'forward')
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                else
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
            } else {
                $tmp['order'] = $orderSymbol;
                $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" title="Czynność rozliczona i powiązana z fakturą"></i>';
                $tmp['period'] = '';
            }
            $tmp['confirm'] = $value['is_confirm'];
            $tmp['cost'] = $value['acc_cost_net'];
            //$tmp['className'] = ($value['id_invoice_fk'] == 0) ? 'default' : 'success';
             $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
    		
		return ['total' => $count,'rows' => $fields, 'cost' => number_format($totalRow['costs'], 2, "," ,  " "), 'time1' => number_format($summary, 2, "," ,  " "), 'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 'amount' => number_format($totalRow['amount'], 2, "," ,  " ")];
	}
    
    public function actionInvoices($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  /*else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_issue, '%Y-%m-%d') >= '2016-10-01'");

        }  */   
       // $fieldsData = $fieldsDataQuery->all();
			
		$sortColumn = 'date_issue';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no'," min(date_format(date_issue,'%Y-%m')) as period", "sum(ii.amount_net) as amount_net"])
            ->from('{{%acc_invoice}} as i')
            ->join('JOIN', '{{%acc_invoice_item}} as ii', 'i.id=ii.id_invoice_fk')
            ->where( ['i.status' => 1, 'ii.id_order_fk' => $id] )
            ->groupby(["id", "system_no"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/invoice/view/', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd faktury').'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['symbol'] = $value['system_no'];
			$tmp['issue'] = $value['period'];
            $tmp['amount_net'] = number_format ($value['amount_net'], 2, "," ,  " ");
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionServices($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = [];
        
		$fieldsDataQuery = AccOrderItem::find()->where( [ 'id_order_fk' => $id] );
 
        $count = $fieldsDataQuery->count();
        if( $count <= 2000 ) {
        
            $fieldsData = $fieldsDataQuery->orderby('name')->all();
            $tmp = [];

            foreach($fieldsData as $key=>$value) {
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                $tmp['name'] = $value->name;
                $tmp['actions'] = '<div class="edit-btn-group">';
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                    //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
                $tmp['actions'] .= '</div>';
                //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
                $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $tmp['id'] = $value->id;
                
                array_push($fields, $tmp); $tmp = [];
            }
        } else {
            echo $count;
        }
		
		return $fields;
	}
    
    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Wykaz zleceń');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
            ->setCellValue('B3', 'Metoda rozliczenia')
			->setCellValue('C3', 'Waluta')
            ->setCellValue('D3', 'Obowiązuje od')
            ->setCellValue('E3', 'Obowiązuje do')
            ->setCellValue('F3', 'Szczegóły')
			->setCellValue('G3', 'Klienci powiązani');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:G3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->getStartColor()->setARGB('D9D9D9');	

		$i=4; 
		$data = []; $where = [];
        
        $post = $_GET;
        if(isset($_GET['AccOrderSearch'])) { 
            $params = $_GET['AccOrderSearch']; 
            if( isset($params['id_dict_order_type_fk']) && strlen($params['id_dict_order_type_fk']) ) {
               array_push($where, "id_dict_order_type_fk = ".$params['id_dict_order_type_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(id_customer_fk = ".$params['id_customer_fk'].") or (o.id in (select id_order_fk from {{%acc_order_item}} where status = 1 and id_customer_fk = ".$params['id_customer_fk']."))");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                 array_push($where, "date_from >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) { 
                 array_push($where, "IFNULL(date_to,'".date('Y-m-d')."') <= '".$params['date_to']."'");
            }
        }  
        if(isset($_GET['cid'])) {
            //var_dump($_GET['cid']);exit;
            array_push($where, "id_customer_fk = ".$_GET['cid']);
        }
		
		$sortColumn = 'ifnull(`c`.`symbol`, `c`.`name`) collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'ifnull(`c`.`symbol`, `c`.`name`) collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.name as name', 'date_from', 'date_to', 'c.symbol as csymbol', 'c.name as cname', 'c.id as cid', 'id_dict_order_type_fk', 'type_name', 'type_color', 'type_symbol','o.type_fk as type_fk', "currency_symbol", 'rate_hourly_1', 'rate_hourly_2', 'rate_hourly_way', 'rate_constatnt', 'limit_hours'])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_type}} at', 'at.id = o.id_dict_order_type_fk')
			->join('JOIN', '{{%acc_currency}} cur', 'cur.id = o.id_currency_fk')
            ->where( ['o.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_type}} at', 'at.id = o.id_dict_order_type_fk')
            ->where( ['o.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows'];
		$separator = "; "; //"\n";
		if($count > 0) {
			foreach($rows as $record){ 
				$details = ''; $items = '';
                if($record['id_dict_order_type_fk'] == 1 || $record['id_dict_order_type_fk'] == 2) {
                    $details .= "Opłata za gotowość: ".$record['rate_constatnt'].$separator;
                }
                if($record['id_dict_order_type_fk'] == 2) {
                    $details .= "Liczba godzin w ryczaułcie: ".$record['limit_hours'].$separator;
                }
                if($record['id_dict_order_type_fk'] >= 2) {
                    $details .= "Stawka za czynności merytoryczne: ".$record['rate_hourly_1'].$separator;
                }
                if($record['id_dict_order_type_fk'] >= 2) {
                    $details .= "Stawka za czynności pozamerytoryczne: ".$record['rate_hourly_2'].$separator;
                }
                $details .= "Stawka za przejazd: ".$record['rate_hourly_way'].$separator;
                if($record['type_fk'] == 2 || $record['type_fk'] == 4) {
                    $itemsData = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['status' => 1, 'id_order_fk' => $record['id']])->all();
                    foreach($itemsData as $key => $item) {
                        $items .= ($key+1).'. '.$item->customer['name'].$separator;
                    }
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, (($record['csymbol'])?$record['csymbol']:$record['cname']) ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['type_name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['currency_symbol']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['date_from']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['date_to']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $details); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $items); 
						
				$i++; 
			}  
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:G'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Zlecenia');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Zlecenia'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionExportbasic($id, $period, $save) {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $employees = ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false );
        if(!isset($_GET['employees']) && $save) $employees = true;
        //if($save == 2) $employees = false;
        
        $model = $this->findModel($id);
        
        $objPHPExcel->setActiveSheetIndex(0);
        if($employees)
            $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
        else
            $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
            
        $objRichTextHeader = new \PHPExcel_RichText();
        $objRichTextHeader->createTextRun($model->customer['name'])->getFont()->setColor(new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLACK) );
        $objRichTextHeader->createTextRun(PHP_EOL.'Rozliczenie zlecenia "'.$model->name.'" za okres '.$period)->getFont()->setColor( new \PHPExcel_Style_Color(  \PHPExcel_Style_Color::COLOR_BLUE ) );
    
        $objPHPExcel->getActiveSheet()->setCellValue('A1', $objRichTextHeader);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF0000CD');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(38);
        //$objPHPExcel->getActiveSheet()->setCellValue('D1', \PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
        //$objPHPExcel->getActiveSheet()->getStyle('D1')->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
        //$objPHPExcel->getActiveSheet()->setCellValue('E1', '#12566');
        if($employees)
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Pracownik');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A').'2', 'Data');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B').'2', 'Opis');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C').'2', 'Czas');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D').'2', 'Stawka');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E').'2', 'Kwota');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F').'2', 'Koszt');
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'H':'G').'2', 'Postępowanie');
        
        $i = 3; $gratis = []; $confirmTime = 0; $fromPrevPeriod = 0;
        foreach(AccOrder::getAll($model->id, $period) as $key => $record){ 
            if($record->type_fk != 5 && $record->type_fk != 6) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->employee['fullname'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                
                $objRichText = new \PHPExcel_RichText();
                $objRichText->createTextRun(str_replace(array("\n", "\r"), '', $record->description))->getFont()->setColor(new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLACK) );
                if($record->acc_cost_description) {
                    $break = ($record->type_fk == 4) ? 'Koszt: ' : PHP_EOL.'Koszt: ';
                    $objRichText->createTextRun($break.str_replace(array("\n", "\r"), '',$record->acc_cost_description))->getFont()->setColor( new \PHPExcel_Style_Color(  \PHPExcel_Style_Color::COLOR_RED ) );
                }
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, $objRichText);
                //$objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $record->confirm_time);
                $confirmTime += $record->confirm_time; $fromPrevPeriod += (($record->acc_limit_prev) ? $record->acc_limit_prev : 0);
                /* price */
                $price = ($model->id_dict_order_type_fk == 2 && $model->id_settlement_type_fk == 2 && $record->acc_limit_price) ? $record->acc_limit_price : $record->unit_price;
                $price = ($model->id_dict_order_type_fk == 2 && $model->id_settlement_type_fk != 2 && $record->acc_limit==$record->confirm_time) ? 0 : $price;
                $inLimit = ($model->id_dict_order_type_fk == 2 && $model->id_settlement_type_fk && $record->acc_limit_price) ? 0 : $record->acc_limit;
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, $price)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00');

                if($record->is_confirm)
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, round($price*($record->confirm_time-$inLimit), 2))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                if($record->is_gratis) {
                    $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, 'gratis')->getStyle((($employees)?'F':'E').$i); 
                    array_push($gratis, $i);
                }
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                
                if($record->type_fk == 4)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FF0000');
                if($record->is_gratis)
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FFFF00FF');
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'H':'G'). $i, $record->case['name']);
                $i++;
            }
            if($record->type_fk == 5) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->name ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                //$objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                $objRichText = new \PHPExcel_RichText();
                //$objRichText->createTextRun($record->description)->getFont()->setColor(new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLACK) );
                if($record->acc_cost_description) {
                    $break = 'Koszt: ';
                    $objRichText->createTextRun($break.$record->acc_cost_description)->getFont()->setColor( new \PHPExcel_Style_Color(  \PHPExcel_Style_Color::COLOR_RED ) );
                } else {
                    $objRichText->createTextRun($record->description)->getFont()->setColor(new \PHPExcel_Style_Color( \PHPExcel_Style_Color::COLOR_BLACK) );
                }
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, $objRichText);
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->unit_price_invoice)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'H':'G'). $i, $record->case['name']); 
                $i++;
            }   
            if($record->type_fk == 6) {
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->name ); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'B':'A'). $i, $record->action_date ); 
                //$objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, str_replace(array("\n", "\r"), '', $record->description)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, $record->description); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, 0); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'E':'D'). $i, 0)->getStyle((($employees)?'E':'D').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, -1*$record->unit_price)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, $record->acc_cost_invoice)->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'H':'G'). $i, $record->case['name']); 
                
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.(($employees)?'G':'F').$i)->getFont()->getColor()->setARGB('FF1E90FF');
                $i++;
            }   
            if($record->is_shift) {
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'H':'G').($i-1))->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A'.($i-1).':'.(($employees)?'H':'G').($i-1))->getFill()->getStartColor()->setARGB('FFFFFFE0');
            } 
            
        }  
        $iData = $i-1;
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:H'.($i-1))->getAlignment()->setWrapText(true);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:G'.($i-1))->getAlignment()->setWrapText(true);
       
        /*$profit = 0; $discount = 0;
        foreach($model->profit as $key => $record){             
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $record->amount_net)->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            
                   $i++; 
            $profit += $record->amount_net; 
        } */
        
        //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Wg godzin'); 
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        if(count($gratis) > 0) { $minusTime = ('-'.(($employees)?'D':'C')).implode(('-'.(($employees)?'D':'C')), $gratis); } else {$minusTime = '';}
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, '=SUM('.(($employees)?'D':'C').'3'.':'.(($employees)?'D':'C').($i-1).')'.$minusTime)->getStyle((($employees)?'D':'C').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, '=SUM('.(($employees)?'F':'E').'3'.':'.(($employees)?'F':'E').($i-1).')')->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'G':'F'). $i, '=SUM('.(($employees)?'G':'F').'3'.':'.(($employees)?'G':'F').($i-1).')')->getStyle((($employees)?'G':'F').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        $i++;
        
        if($model->id_dict_order_type_fk == 1 || $model->id_dict_order_type_fk == 2) {
            //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
            if($model->id_dict_order_type_fk == 2 && $model->id_settlement_type_fk == 2 && $confirmTime < $model->limit_hours) {
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Stała opłata - przejście na ryczałt proporcjonalny'); 
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFDDA0DD');
            } else {
                if($model->id_dict_order_type_fk == 2 && $model->id_settlement_type_fk == 1 && $confirmTime < $model->limit_hours) {
                    $constantAdv = ' [liczba godzin, które przechodzą na następny okres: '.($model->limit_hours-$confirmTime+$fromPrevPeriod).']';
                } else {
                    $constantAdv = '';
                }
                //$constantAdv = '';
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Stała opłata'.$constantAdv); 
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
                
                $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, $model->rate_constatnt)->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
                
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
            }
            $i++;
        }
        
        /* podsumowanie */
        //$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':'.(($employees)?'E':'D').$i);  
        $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'Razem'); 
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        
        if($model->id_dict_order_type_fk == 1 || $model->id_dict_order_type_fk == 2) {
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, "=".(($employees)?'F':'E').($i-1)."+".(($employees)?'F':'E').($i-2)."+".(($employees)?'G':'F').($i-2))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');
        } else {
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'F':'E'). $i, "=".(($employees)?'F':'E').($i-1)."+".(($employees)?'G':'F').($i-1))->getStyle((($employees)?'F':'E').$i)->getNumberFormat()->setFormatCode('# ##0.00');        
        }
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'F':'E'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle((($employees)?'G':'F'). $i)->getFill()->getStartColor()->setARGB('FFC0C0C0');
        $i++;
        
        if(count($gratis) > 0) {
            $minusGratis = ('='.(($employees)?'D':'C')).implode(('+'.(($employees)?'D':'C')), $gratis); 
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'C':'B'). $i, 'GRATIS'); 
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B').$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'C':'B'). $i)->getFill()->getStartColor()->setARGB('FFFF00FF');
            
            $objPHPExcel->getActiveSheet()->setCellValue((($employees)?'D':'C'). $i, $minusGratis)->getStyle((($employees)?'D':'C').$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle((($employees)?'D':'C'). $i)->getFill()->getStartColor()->setARGB('FFFF00FF');
        }
        ++$i;
        
        // Set cell number formats
        //$objPHPExcel->getActiveSheet()->getStyle('E4:E'.($i-3))->getNumberFormat()->setFormatCode('#,##0.00');

        // Set column widths
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        if($employees) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        } else {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        }
        $objPHPExcel->getActiveSheet()->setAutoFilter('A2:'.(($employees)?'H':'G').'2');	
        --$i;
        if($model->id_dict_order_type_fk == 1 || $model->id_dict_order_type_fk == 2) {
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.(($employees)?'H':'G').($i-3))->applyFromArray($styleArray);
        } else {
            $objPHPExcel->getActiveSheet()->getStyle('A1:'.(($employees)?'H':'G').($i-2))->applyFromArray($styleArray);
        }
          // Unprotect a cell
        //$objPHPExcel->getActiveSheet()->getStyle('B1')->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
      
        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename first worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Rozliczenie');
		
        $dateTmp = date('m.Y', strtotime($period));
        
        if($save) {
            $filename = ( ($model->customer['symbol']) ? $model->customer['symbol'] : $model->customer['name']).'_'.$model->name.'_'.$dateTmp.".xlsx";
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save("uploads/temp/".$filename);
            
            return $filename;
        } else {
            $filename = ( ($model->customer['symbol']) ? $model->customer['symbol'] : $model->customer['name']).'_'.$model->name.'_'.$dateTmp.".xlsx";
            $filename = \frontend\controllers\CommonController::normalizeChars($filename);
            $filename = iconv('utf-8','ASCII//TRANSLIT',$filename);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }
	}
    
    public function actionRates($id) {
        $id = CustomHelpers::decode($id);
        $model = AccOrder::findOne($id);
        
        return  $this->renderAjax('_rates', [ 'model' => $model]);
    }
    
    public function actionRatedata($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
		$rows = AccRate::find()->where(['id_order_fk' => $id, 'status' => 1])->all();
		$fields = []; $tmp = [];
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/order/rateedit/', 'id' => $value->id]).'" class="btn btn-xs btn-default update" data-table="#table-rates" data-form="item-form" data-target="#modal-grid-event"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/ratedelete/', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-rates"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['type'] = $value->type['name'];
            $tmp['employee'] = $value->employee['fullname'];
            $tmp['rate'] = $value->unit_price;
            $tmp['date_from'] = $value->date_from;
            $tmp['date_to'] = $value->date_to;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionRateadd($id) {
        $id = CustomHelpers::decode($id);
        $order = AccOrder::findOne($id);
		$model = new AccRate();
        $model->id_order_fk = $order->id;
        $model->id_customer_fk = $order->id_customer_fk;
        $model->date_from = $order->date_from;
        $model->date_to = $order->date_to;
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {	
					$sql = "select acc_period "
                    ." from {{%acc_actions}} a "
                    ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = ".$order->id
                    ." group by acc_period"; 
					$actionData = Yii::$app->db->createCommand($sql)->query();
					
					foreach($actionData as $key => $value) {
						\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $order->id, 'period' => $value['acc_period']]); 
					}
                    
                    return array('success' => true,  'action' => 'addRate', 'index' => 0, 'id' => $model->id, 'alert' => 'Stawka została zarejestrowana'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formRate', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formRate', [ 'model' => $model]);	
        }
	}
    
    public function actionRateedit($id) {
       
		$id = CustomHelpers::decode($id);
		$model = AccRate::findOne($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    $sql = "select acc_period "
                    ." from {{%acc_actions}} a "
                    ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = ".$model->id_order_fk
                    ." group by acc_period"; 
					$actionData = Yii::$app->db->createCommand($sql)->query();
					
					foreach($actionData as $key => $value) {
						\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $value['acc_period']]); 
					}
					
					return array('success' => true,  'action' => 'editRate', 'index' =>0, 'id' => $model->id,  'alert' => 'Stawka została zmodyfikowana' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formRate', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formRate', [ 'model' => $model]);	
        }
	}
    
    public function actionDiscounts($id) {
        $id = CustomHelpers::decode($id);
        $model = AccOrder::findOne($id);
        
        return  $this->renderAjax('_discounts', [ 'model' => $model]);
    }
    
    public function actionDiscountdata($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
		$rows = AccDiscount::find()->where(['id_order_fk' => $id, 'status' => 1])->all();
		$fields = []; $tmp = [];
		foreach($rows as $key=>$value) {
			if(!$value->id_invoice_fk) {
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="'.Url::to(['/accounting/order/discountedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-discounts" data-form="item-form" data-target="#modal-grid-event"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
				$actionColumn .= '<a href="'.Url::to(['/accounting/order/discountdelete/', 'id' => $value->id]).'" class="btn btn-sm btn-default deleteConfirm" data-table="#table-discounts"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
				$actionColumn .= '</div>';
            } else {
				$actionColumn = '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value->id_invoice_fk]).'" title="Przejdź do faktury"><i class="fa fa-calculator text--purple"></i></a>';
			}
			
            $tmp['name'] = $value->name;
            $tmp['amount'] = $value->discount_percent;
            $tmp['period'] = $value->acc_period;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionDiscountadd($id) {
        $id = CustomHelpers::decode($id);
        $order = AccOrder::findOne($id);
		$model = new AccDiscount();
        $model->id_order_fk = $order->id;
        $model->id_customer_fk = $order->id_customer_fk;
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                if($model->save()) {				
                    \Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]);
                    return array('success' => true,  'action' => 'addDiscount', 'index' => 0, 'id' => $model->id, 'alert' => 'Rabat została dodany'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formDiscount', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formDiscount', [ 'model' => $model]);	
        }
	}
    
    public function actionDiscountedit($id) {
       
		$id = CustomHelpers::decode($id);
		$model = AccDiscount::findOne($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                if($model->save()) {				
                    \Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]);
                    return array('success' => true,  'action' => 'editDiscount', 'index' =>0, 'id' => $model->id,  'alert' => 'Rabat została zmodyfikowany' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formDiscount', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formDiscount', [ 'model' => $model]);	
        }
	}
    
    public function actionDiscountdelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$model = AccDiscount::findOne($id);
        $model->user_action = 'delete';
        $model->status = -1;
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        if($model->save()) { 
            \Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]); 
            return array('success' => true,  'action' => 'deleteDiscount', 'index' =>0, 'id' => $model->id,  'alert' => 'Rabat został usunięty', 'table' => '#table-discounts' );	
        } else {
            return array('success' => false,  'action' => 'deleteDiscount', 'index' =>0, 'id' => $model->id,  'alert' => 'Rabat nie został usunięty', 'table' => '#table-discounts', 'errors' => [ 0 => 'Rabat nie może zostac usunięty' ] );	
        }  
	}
	
	public function actionInfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = AccOrder::findOne($id);
        
        return ['model' => $model, 'currency' => $model->currency];
    }
}
