<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccTax;
use backend\Modules\Accounting\models\AccService;
use backend\Modules\Accounting\models\AccDictionary;
use backend\Modules\Accounting\models\AccType;
use backend\Modules\Accounting\models\AccRate;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * SettingController implements actions for ComGroup models.
 */
class SettingController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccService();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    /* SERVICE */
    public function actionService()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccService();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'module' => 'service'
        ]);
    }
    
    public function actionServicedata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = AccService::find()->where(['status' => 1]);
        
        if(isset($_GET['AccService'])) {
            $params = $_GET['AccService'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
        } 
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/serviceedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
           // $actionColumn .= '<a href="'.Url::to(['/accounting/setting/servicedelete/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = $value->name;
            $tmp['unit_time'] = $value->unit_time;
            $tmp['unit_price'] = $value->unit_price;

            $tmpType = $dict[$value->type_fk];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            /*$type ='<i class="fa fa-briefcase text--teal"></i>';
            if($value->type_fk == 2) $type ='<i class="fa fa-institution text--grey"></i>';
            if($value->type_fk == 3) $type ='<i class="fa fa-car text--purple"></i>';
            if($value->type_fk == 4) $type ='<i class="fa fa-money text--red"></i>';*/

            $tmp['type'] = $type;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionServiceadd() {
       
		$model = new AccService();
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    
                    return array('success' => true,  'action' => 'addService', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Usługa została dodana'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/service/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/service/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionServiceedit($id) {
       
		$model = $this->findModel($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    return array('success' => true,  'action' => 'editService', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Usługa została zmodyfikowana' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/service/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/service/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionServices($type) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $services = '';
        $data = AccService::find()->where(['status' => 1, 'type_fk' => $type])->all();
        foreach($data as $key => $value) {
            $services .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        
        return ['list' => $services];
    }
    
    /* TAX */
    
    public function actionTax()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccTax();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'module' => 'tax'
        ]);
    }
    
    public function actionTaxdata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = AccTax::find()->where(['status' => 1]);
        
        if(isset($_GET['AccTax'])) {
            $params = $_GET['AccTax'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
        } 
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];

		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/taxedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/taxdelete/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = $value->name;
            $tmp['symbol'] = $value->symbol;
            $tmp['value'] = $value->value;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionTaxadd() {
       
		$model = new AccTax();
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    
                    return array('success' => true,  'action' => 'addService', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Stawka VAT została dodane'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/tax/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/tax/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionTaxedit($id) {
        $id = CustomHelpers::decode($id);
		$model = AccTax::findOne($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    return array('success' => true,  'action' => 'editService', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Usługa została zmodyfikowana' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/tax/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/tax/_formAjax', [ 'model' => $model]);	
        }
	}
	
	public function actionTaxdelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$model = AccTax::findOne($id);
        
        //$model->user_action = 'delete';
        $model->status = -1;
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        if(!$model->save()) { var_dump($model->getErrors()); exit; }
			
        return array('success' => true,  'action' => 'deleteTax', 'index' =>0, 'id' => $model->id,  'alert' => 'Stawka VAT została usunięta', 'table' => '#table-data' );	
	}
    
    /* RATE EMPLOYEES */
    
    public function actionRate()  {
        $searchModel = new AccRate();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'module' => 'rate'
        ]);
    }
    
    public function actionRatedata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = AccRate::find()->where(['status' => 1]);
        
        if(isset($_GET['AccRate'])) {
            $params = $_GET['AccRate'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
        } 
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];

		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/rateedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/ratedelete/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['type'] = $value->type['name'];
            $tmp['employee'] = $value->employee['fullname'];
            $tmp['customer'] = ($value->customer['symbol']) ? $value->customer['symbol'] : $value->customer['name'];
            $tmp['order'] = $value->order['name'];
            $tmp['rate'] = $value->unit_price;
            $tmp['date_from'] = $value->date_from;
            $tmp['date_to'] = $value->date_to;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionRateadd() {
       
		$model = new AccRate();
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if(!$model->id_employee_fk) $model->id_employee_fk = 0;
            if( $model->validate() ) {
                
                if($model->save()) {				
                    
                    return array('success' => true,  'action' => 'addRate', 'index' =>0, 'id' => $model->id, 'alert' => 'Stawka została zarejestrowana'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/rate/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/rate/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionRateedit($id) {
       
		$id = CustomHelpers::decode($id);
		$model = AccRate::findOne($id);
     
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
            
            $existInvSql = "select count(*) as cinv"
			." from {{%acc_actions}} a "
			." where is_confirm = 1 and a.status = 1 and (id_invoice_fk != 0 and id_invoice_fk is not null) and id_unit_rate_fk = ".$model->id; 
            $existInv = Yii::$app->db->createCommand($existInvSql)->queryOne();
            
            if($existInv['cinv'] > 0) {
                return array('success' => false,  'action' => 'deleteRate', 'index' => 0, 'id' => $model->id,  'errors' => [0 => 'Stawka została przypisana do zafakturowanej czynności i nie może zostać zmieniona'], 'table' => '#table-rates' );	
            }
        
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    return array('success' => true,  'action' => 'editRate', 'index' =>0, 'id' => $model->id,  'alert' => 'Stawka została zmodyfikowana', 'table' => '#table-rates' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/rate/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors(), 'table' => '#table-rates' );	
			}		
		} else {
            return  $this->renderAjax('tabs/rate/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionRatedelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
		$model = AccRate::findOne($id);
        
        $existInvSql = "select count(*) as cinv"
			." from {{%acc_actions}} a "
			." where is_confirm = 1 and a.status = 1 and (id_invoice_fk != 0 and id_invoice_fk is not null) and id_unit_rate_fk = ".$model->id; 
        $existInv = Yii::$app->db->createCommand($existInvSql)->queryOne();
        
        if($existInv['cinv'] > 0) {
            return array('success' => false,  'action' => 'deleteRate', 'index' => 0, 'id' => $model->id,  'alert' => 'Stawka została przypisana do zafakturowanej czynności i nie może zostać usunięta', 'errors' => [0 => 'Stawka została przypisana do zafakturowanej czynności i nie może zostać zmieniona'], 'table' => '#table-rates' );	
        }
        
        $model->user_action = 'delete';
        $model->status = -1;
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        if(!$model->save()) { var_dump($model->getErrors()); exit; }
		
		$sql = "select acc_period "
			." from {{%acc_actions}} a "
			." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = ".$model->id_order_fk
			." group by acc_period"; 
		$actionData = Yii::$app->db->createCommand($sql)->query();
		
		foreach($actionData as $key => $value) {
			\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $value['acc_period']]); 
		}
					
        return array('success' => true,  'action' => 'deleteRate', 'index' =>0, 'id' => $model->id,  'alert' => 'Stawka została usunięta', 'table' => '#table-rates' );	
	}
    
    /* TYPES - ADMINISTRATIVE ACTION */
    public function actionType()
    {
        $searchModel = new AccDictionary();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'module' => 'type'
        ]);
    }
    
    public function actionTypedata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = $fieldsData = AccDictionary::find()->where(['status' => 1, 'type_fk' => 1]);
        
        if(isset($_GET['AccDictionary'])) {
            $params = $_GET['AccDictionary'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
        } 
        
        $fieldsData = $fieldsDataQuery->all();
			
		$fields = [];
		$tmp = [];
        $personalTypes = \backend\Modules\Task\models\CalTodo::dictTypes();
		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/typeedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/typedelete/', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = $value->name;
            
            $type ='<i class="fa fa-'.$personalTypes[$value->id]['icon'].' text--'.$personalTypes[$value->id]['color'].'"></i>';
            $tmp['type'] = $type;
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionTypeadd() {
       
		$model = new AccDictionary();
               
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    
                    return array('success' => true,  'action' => 'addType', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Słownik został dodany'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/type/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/type/_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionTypeedit($id) {
        $id = CustomHelpers::decode($id);
		$model = AccDictionary::findOne($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    return array('success' => true,  'action' => 'editType', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Słownik został zmodyfikowany' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/type/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/type/_formAjax', [ 'model' => $model]);	
        }
	}
    
    /* METHOD */
    
    public function actionMethod()
    {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccType();
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'module' => 'method'
        ]);
    }
    
    public function actionMethoddata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsData = AccType::find()->all();
        		
		$fields = [];
		$tmp = [];

		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/setting/methodedit/', 'id' => $value->id]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = $value->type_name;
            $tmp['symbol'] = $value->type_symbol;
            $tmp['color'] = '<div style="background-color: '.$value->type_color.'; width: 100%; height:100%; display:block;">&nbsp;</div>';
           
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionMethodedit($id) {
       
		$id = CustomHelpers::decode($id);
		$model = AccType::findOne($id);
          
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {				
                    return array('success' => true,  'action' => 'editMethod', 'index' =>0, 'id' => $model->id, 'name' => $model->type_name, 'alert' => 'Metoda rozliczenia została zmodyfikowana' );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/method/_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('tabs/method/_formAjax', [ 'model' => $model]);	
        }
	}

    
    
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
		if (($model = AccService::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Brak danych.');
        }
    }

}
