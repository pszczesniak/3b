<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccActionsSearch;
use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccPeriod;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * ActionController implements the CRUD actions for AccActions model.
 */
class ActionController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActionsSearch();
       // $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
       
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
           // $searchModel->date_from = $dateYear.'-'.$month.'-01';
           // $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
           // $searchModel->date_from = ($dateYear-1).'-12-01';
           // $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           // $searchModel->date_from = $dateYear.'-'.date('m').'-01';
           // $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
        
        if(isset($_GET['orderId'])) $searchModel->id_order_fk = $_GET['orderId'];
        if(isset($_GET['period'])) $searchModel->acc_period = $_GET['period'];
      /*  $searchModel->date_from = '2016-10-01';
        $searchModel->date_to = '2016-10-31';*/
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                if($params['type_fk'] == 4)
                    array_push($where, "(a.type_fk = ".$params['type_fk']." or (acc_cost_net > 0))");
                else 
                    array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "(is_confirm = 1 or is_gratis = 1)");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0 and (is_gratis = 0 or is_gratis is null)");
                if($params['invoiced'] == 3) array_push($where, "is_confirm = 1 and (id_invoice_fk is not null and id_invoice_fk != 0)");
                if($params['invoiced'] == 4) array_push($where, "is_gratis = 1");
                if($params['invoiced'] == 5) array_push($where, "(a.id_order_fk is null or a.id_order_fk=0)");
            }
            if(isset($params['is_note']) && !empty($params['is_note']) ) {
                if($params['is_note'] == 1) array_push($where, "a.id in (select id_fk from {{%acc_note}} where type_fk=1)");
				if($params['is_note'] == 2) array_push($where, "a.id not in (select id_fk from {{%acc_note}} where type_fk=1)");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        } 
        
        $sortColumn = 'is_priority desc, rank_priority, action_date, id';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'ename collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'csname';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description", "a.is_gratis as is_gratis", "is_priority",
					  "a.acc_cost_description as cdescription", "a.acc_cost_currency_id as acc_cost_currency_id", "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "unit_price_native", "confirm_price", "acc_limit",
                      "confirm_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "customer_linked", "case when e.id is null then a.name else concat_ws(' ', e.lastname, e.firstname) end as ename", "cur.currency_symbol as currency_symbol",
					  "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "acc_cost_native", "cc.name as sname", "o.name as oname",
					  "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes", "d.name as dname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')  
			->join('LEFT JOIN', '{{%company_department}}  as d', 'd.id=a.id_department_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_currency}} as cur', 'cur.id = o.id_currency_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'a.id_set_fk = cc.id')
            ->where( ['a.status' => 1] );
            
	    $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_native else 0 end) as costs",
                      "sum(case when (is_confirm=1 or is_gratis=1) then confirm_time else 0 end) as confirm_time", 
                      "sum(case when a.type_fk=5 then unit_price_native when acc_limit=0 then (confirm_time*unit_price_native) else ((confirm_time-acc_limit)*unit_price_native) end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
            
        $queryOrders = (new \yii\db\Query())
            ->select(["a.id_order_fk", "min(rate_constatnt) as rate_constatnt"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1] )
            ->groupby(['a.id_order_fk']);
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
            $queryOrders = $queryOrders->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs']; 
        
        $totalRow['lump'] = 0; $totalRow['all'] = $costs+$totalRow['amount']; 
        $totalOrders = $queryOrders->all(); 
        foreach($totalOrders as $io => $order) {
            $totalRow['lump'] += $order['rate_constatnt'];
        }
        $totalRow['all'] += $totalRow['lump'];
        
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes(); 
		foreach($rows as $key=>$value) {
            if($value['type_fk'] == 5 && $value['cdescription']) $value['type_fk'] = 4;
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" data-placement="left" data-toggle="tooltip" data-title="'.$tmpType['name'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<div class="btn btn-xs btn-break" style="background-color:'.$value['type_color'].'" data-placement="left" data-toggle="tooltip" data-title="Kliknij aby oznaczyć czynność lub zmienić rozliczenie" title="Kliknij aby oznaczyć czynność lub zmienić rozliczenie">'.$value['oname'].'</div>' : '<span class="label bg-green" data-placement="left" data-toggle="tooltip" data-title="Przypisz rozliczenie" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
            
            $periodAction = 'forward';
            $timeCounted = $value['confirm_time'];
            
            $value['acc_limit'] = (!$value['acc_limit']) ? 0 : $value['acc_limit'];
            
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            if( $value['id_service_fk'] == 4 || $value['id_service_fk'] == 5 ) {
                $tmp['price'] = 0;
            } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                $value['unit_price_native'] = 0;
                $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price_native'], 2).'</span>';
            } else {
                $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                $value['acc_limit'] = $value['acc_limit'] * 60;
                $timeH_l = intval($value['acc_limit']/60);
                $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price_native'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price_native'], 2);
            }
            $tmp['type_action'] = 1;
            $tmp['amount'] = ($value['type_fk'] == 5) ? $value['unit_price_native'] : round( ($value['unit_price_native']*$timeCounted),2);
            $tmp['name'] = $value['description'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['ename'];
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
            
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['unit_time_min'] = $value['unit_time'];
            
            $tmp['service'] = $type;
            $tmp['description'] = ($value['id_service_fk'] == 4 || $value['id_service_fk'] == 5 && !$value['description']) ? htmlentities($value['cdescription']) : htmlentities($value['description']);
            $tmp['customer'] = (($value['customer_linked']) ? '<i class="fa fa-info-circle text--blue" data-toggle="tooltip" data-title="'.explode('-',$value['customer_linked'])[1].'"></i>&nbsp;' : '')
                              .((!$value['customer_linked']) ? '<a href="'.Url::to(['/accounting/order/bind', 'id' => $value['cid']]).'"  data-toggle="tooltip" data-title="Powiąż ze zleceniem" class="gridViewModal" data-target="#modal-grid-item"><i class="fa fa-sitemap text--navy"></i>&nbsp;</a>' : '')                             
                              .'<a href="'.Url::to(['/accounting/order/createajax', 'id' => $value['cid']]).'"  data-toggle="tooltip" data-title="Dodaj umowę" class="gridViewModal" data-target="#modal-grid-item"><i class="fa fa-plus-circle text--green"></i>&nbsp;</a>'                              
                              .'<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" title="Przejdź do karty klienta">'.( ($value['csymbol'])?$value['csymbol']:$value['cname'] ). '</a>'
                              .(($value['sname']) ? '<br /><small class="text--purple"><i class="fa fa-balance-scale"></i>'.$value['sname'].'</small>' : ''); 
            $tmp['case'] = $value['sname'];             
            $tmp['className'] = ( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) ? 'warning' : 'default';
            if(!$value['id_invoice_fk']) {
                if( in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) 
                    $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                else
                    $tmp['order'] = $orderSymbol;
                   
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia"></i>' : (($value['is_gratis'] == 1) ? '<i class="fa fa-gift text--pink" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia jako gratis"></i>' : '<i class="fa fa-close text--orange" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność nie oznaczona do rozliczenia"></i>');
                if($periodAction == 'forward')
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-placement="left" data-toggle="tooltip" data-title="Przenieś czynność na nastepny okres rozliczeniowy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                else
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-placement="left" data-toggle="tooltip" data-title="Przywróć czynność na właściwy okres rozliczeniowy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
            } else {
                if($value['is_gratis']) $value['is_confirm'] = 1;
                $tmp['order'] = $orderSymbol;
                $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność rozliczona i powiązana z fakturą"></i>'.(($value['is_gratis']) ? '<br /><i class="fa fa-gift text--pink cursor-pointer" title="Gratis"></i>' : '');
                $tmp['period'] = '';
            }
            
            if($value['is_priority']) {
                $tmp['invoice'] .= '<br /><i class="fa fa-flag text--red" title="Priorytetowa"></i>'; 
            }
            
			$tmp['cost'] =  ($value['is_confirm'] == 1) ? $value['acc_cost_native'] : ( (!$value['acc_cost_net'] ? '0' : $value['acc_cost_net'].' <small class="text--grey">'.(\backend\Modules\Accounting\models\AccCurrency::getList()[$value['acc_cost_currency_id']]).'</small>'));
            $tmp['confirm'] = ($value['is_confirm'] || $value['is_gratis']); //(!$value['id_invoice_fk']) ? 0 : 1;
            /*$tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';*/
            $tmp['note'] = '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
            if($value['is_confirm'] == 0 || (!$value['id_invoice_fk'] && $value['is_confirm'] == 1)) {
                $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        . ( (!$value['id_invoice_fk']) ? '<li class="update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item" href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('app', 'Edit').'"><a href="#"><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( ($value['is_confirm'] == 0) ? '<li class="deleteConfirm" data-table="#table-actions" href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (!$value['is_confirm'] && !$value['is_priority'] && $value['id_dict_order_type_fk']) ? '<li class="deleteConfirm" data-table="#table-actions" href="'.Url::to(['/accounting/action/gratis', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-gift\'></i>'.Yii::t('app', 'Gratis').'" data-label="'.((!$value['is_gratis']) ? 'Oznacz jako gratis' : 'Odznacz gratis').'"><a href="#"><i class="fa fa-gift text--pink"></i>&nbsp;'.((!$value['is_gratis']) ? 'Oznacz' : 'Odznacz').' gratis</a></li>' : '')
                                        . ( (!$value['is_confirm'] && !$value['is_gratis'] && $value['id_dict_order_type_fk']) ? '<li class="deleteConfirm" data-table="#table-actions" href="'.Url::to(['/accounting/action/priority', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-flag\'></i>'.Yii::t('app', 'Priorytet').'" data-label="'.((!$value['is_priority']) ? 'Oznacz jako priorytet' : 'Odznacz priorytet').'"><a href="#"><i class="fa fa-flag text--orange"></i>&nbsp;'.((!$value['is_priority']) ? 'Oznacz' : 'Odznacz').' priorytet</a></li>' : '')

                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
            } else {
                $tmp['actions'] = '';
            }
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields, 
                'cost' => number_format($costs, 2, "," ,  " "), 
                'time1' => number_format($summary, 2, "," ,  " "), 
                'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 
                'amount' => number_format($totalRow['amount'], 2, "," ,  " "),
                'lump' => number_format($totalRow['lump'], 2, "," ,  " "),
                'all' => number_format($totalRow['all'], 2, "," ,  " ") ];
	}
    
    public function actionEmployees() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActionsSearch();
        //$searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
        
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->date_from = $dateYear.'-'.$month.'-01';
            $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
            $searchModel->date_from = ($dateYear-1).'-12-01';
            $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
        } else {
            $searchModel->date_from = $dateYear.'-'.date('m').'-01';
            $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        }
   
        return $this->render('employees', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionEdata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where_1 = []; $where_2 = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET; $type = 0;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
               // array_push($where, "a.type_fk = ".$params['type_fk']);
               $type = $params['type_fk'];
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where_1, "a.id_customer_fk = ".$params['id_customer_fk']);
                array_push($where_2, "a.id = 0");
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where_1, "a.id_set_fk = ".$params['id_set_fk']);
                array_push($where_2, "a.id = 0");
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where_1, "lower(a.description) like '%".strtolower($params['description'])."%'");
                array_push($where_2, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where_1, "id_employee_fk = ".$params['id_employee_fk']);
                array_push($where_2, "id_employee_fk = ".$params['id_employee_fk']);
            } else {
                if(!$this->module->params['isAdmin'] && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && !$this->module->params['managers']) {
                    array_push($where_1, "(id_employee_fk = ".$this->module->params['employeeId'].' or id_employee_fk in (select id_to_fk from {{%employee_share}} where id_from_fk = '.$this->module->params['employeeId'].'))');
                    array_push($where_2, "(id_employee_fk = ".$this->module->params['employeeId'].' or id_employee_fk in (select id_to_fk from {{%employee_share}} where id_from_fk = '.$this->module->params['employeeId'].'))');
                } else if ($this->module->params['managers'] && !$this->module->params['isAdmin'] && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                    array_push($where_1, "a.id_department_fk in (select id from {{%company_department}} where id_employee_manager_fk = ".$this->module->params['employeeId'].") and id_employee_fk in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} d on d.id=ed.id_department_fk where ed.status = 1 and id_employee_manager_fk = ".$this->module->params['employeeId'].")");
                    array_push($where_2, "id_employee_fk in (select id_employee_fk from {{%employee_department}} ed join {{%company_department}} d on d.id=ed.id_department_fk where ed.status = 1 and id_employee_manager_fk = ".$this->module->params['employeeId'].")");
                } 
            }
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where_1, "a.id_department_fk = ".$params['id_department_fk']);
                array_push($where_2, "id_employee_fk in (select id_employee_fk from {{%employee_department}} where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where_1, "action_date >= '".$params['date_from']."'");
                array_push($where_2, "todo_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where_1, "action_date <= '".$params['date_to']."'");
                array_push($where_2, "todo_date <= '".$params['date_to']."'");
            }
        } else {
            //$fieldsDataQuery = $fieldsDataQuery->andWhere("date_format(action_date,'%Y-%m') = '2016-10'");
            $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
            if($dateDay <= 10 && $dateMonth > 1) {
                if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
                $date_from = $dateYear.'-'.$month.'-01';
                $date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            } else if($dateDay <= 10 && $dateMonth == 1) {
                $date_from = ($dateYear-1).'-12-01';
                $date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
            } else {
                $date_from = $dateYear.'-'.date('m').'-01';
                $date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            }
            array_push($where_1, "action_date >= '".$date_from."'");
            array_push($where_1, "action_date <= '".$date_to."'");
            
            array_push($where_2, "todo_date >= '".$date_from."'");
            array_push($where_2, "todo_date <= '".$date_to."'");
            
            array_push($where_1, "id_employee_fk = ".$this->module->params['employeeId']);
            array_push($where_2, "id_employee_fk = ".$this->module->params['employeeId']);
        }
        
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'ename collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'ifnull(csymbol,cname) collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'csname';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		$count = 0; $summary = 0; $summary1 = 0; $summary2 = 0; $cost = 0;
        /* action */
		$query1 = (new \yii\db\Query())
            ->select(["concat_ws('1', '1') as type_action", "a.id as id", "a.id_service_fk as type_fk", "a.name as name", "a.description as description", "acc_cost_description", "action_date", "unit_time", "concat_ws(' ', e.lastname, e.firstname) as ename", 
                      "acc_cost_net", "c.id as cid", "c.name as cname", "c.symbol as csymbol", "is_confirm", "cc.id as ccid", "cc.type_fk as cctype", "cc.name as ccname", "d.name as dname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
			->join('JOIN', '{{%company_department}}  as d', 'd.id=a.id_department_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = a.id_set_fk')
            ->where( ['a.status' => 1] );
    
	    $queryTotal1 = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", "sum(acc_cost_net) as costs", "sum(confirm_time) as confirm_time", "sum(confirm_time*confirm_price) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1] );
            
         if( count($where_1) > 0 ) {
            $query1 = $query1->andWhere(implode(' and ',$where_1));
            $queryTotal1 = $queryTotal1->andWhere(implode(' and ',$where_1));
        }
		$totalRow1 = $queryTotal1->one(); $count1 = $totalRow1['total_rows']; $summary1 = ($totalRow1['unit_time']) ? $totalRow1['unit_time'] : 0; $cost = ($totalRow1['amount'])?$totalRow1['amount']:0;
        /* actions */ 
        
        /* personal */
        $query2 = (new \yii\db\Query())
            ->select(["concat_ws('2', '2') as type_action", "a.id as id", "a.id_dict_todo_type_fk as type_fk", "a.name as name", "a.description as description", "concat_ws('','') as acc_cost_description", "todo_date as action_date", 
                      "execution_time as unit_time", "concat_ws(' ', e.lastname, e.firstname) as ename", "concat_ws('0', '0') as acc_cost_net", "concat_ws('', '') as cid", "concat_ws('', '') as cname", "concat_ws('', '') as csymbol","concat_ws('', '0') as is_confirm",
                      "concat_ws('', '') as ccid", "concat_ws('', '') as cctype", "concat_ws('', '') as ccname", "concat_ws('', '') as dname"])
            ->from('{{%cal_todo}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1, 'a.type_fk' => 1] );
            
	    $queryTotal2 = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(execution_time/60,2)) as unit_time"])
            ->from('{{%cal_todo}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->where( ['a.status' => 1] )->andWhere('type_fk = 1');
       
       if( count($where_2) > 0 ) {
            $query2 = $query2->andWhere(implode(' and ',$where_2));
            $queryTotal2 = $queryTotal2->andWhere(implode(' and ',$where_2));
        }
		$totalRow2 = $queryTotal2->one(); $count2 = $totalRow2['total_rows']; $summary2 = ($totalRow2['unit_time']) ? $totalRow2['unit_time'] : 0;
        
        /* personal */
        if($type == 0) {
            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query1->union($query2)])
                ->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 + $count2; $summary = $summary1 + $summary2;
        } else if($type == 1) {
            $unionQuery = $query1->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count = $count1 ; $summary = $summary1 ; $summary2 = 0;
        } else {
            $unionQuery = $query2->limit( isset($post['limit']) ? $post['limit'] : 10 )
                ->offset( isset($post['offset']) ? $post['offset'] : 0 )
                ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            $count =  $count2; $summary =  $summary2; $summary1 = 0; $cost = 0;
        }

		$rows = $unionQuery->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        $dictPersonal = \backend\Modules\Task\models\CalTodo::dictTypes();
		foreach($rows as $key=>$value) {
            $tmp['type_action'] = $value['type_action'];
            if($value['type_action'] == 1) {
                $symbol = '<i class="fa fa-briefcase text--purple cursor-pointer" title="czynności na rzecz klienta"></i>';
                $tmpType = $dict[$value['type_fk']];
                $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
                //$orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
            } else {
                $symbol = '<i class="fa fa-tasks text--grey cursor-pointer" title="czynności administracyjne"></i>';
                $tmpType = $dictPersonal[$value['type_fk']];
                $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            }
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['name'] = (!$value['description']) ? $value['name'] : $value['description'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = (($value['dname'])?'<i class="fa fa-building text--teal" data-placement="right" data-toggle="tooltip" data-title="'.$value['dname'].'"></i>&nbsp;':'').$value['ename'];
            //$tmp['unit_time'] = round($value['unit_time']/60,2);
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
            
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['unit_time_min'] = $value['unit_time'];
            $tmp['service'] = $type;
            $tmp['symbol'] = $symbol;
            $tmp['description'] = ($value['type_action'] == 1 && $value['type_fk'] == 4) ? ('<span class="text--red">Koszt: </span>'.$value['acc_cost_description']) : ((!$value['description']) ? $value['name'] : $value['description']);
           // $tmp['className'] = ($value['type_fk'] == 4) ? 'danger' : 'default';
            $tmp['cost'] = number_format($value['acc_cost_net'], 2, "," ,  " ");
            $tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.(($value['csymbol'])? $value['csymbol']:$value['cname']).'</a>';
            if($value['type_action'] == 2) {
                $tmp['case'] = $value['ccname'];
            } else {
                $tmp['case'] = '<a href="'.Url::to(['/task/'.(($value['cctype'] == 1) ? 'matter' : 'project').'/view', 'id' => $value['ccid']]).'">'.$value['ccname'].'</a>';
            }
            $tmp['actions'] = '<div class="edit-btn-group">';
                if($value['type_action'] == 1) {
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                    $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                } else {
                    $tmp['actions'] .= '<a href="'.Url::to(['/task/personal/updateajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                    $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/task/personal/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                }
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields, 'time1' => $summary, 'time2' => $summary1, 'cost' => $cost, 'amount' => $summary2];
	}

    /**
     * Displays a single CompanyEmployee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $model = $this->findModel($id); 
        $member = ComMeetingMember::find()->where(['id_meeting_fk' => $model->id, 'id_user_fk' => \Yii::$app->user->id])->one();
        
        if(!$member) {
            throw new \yii\web\HttpException(405, 'Nie jesteś przypisany do tego spotakania.');
        }
        
        $statusTmp = $member->status;
        
        if($model->date_from) {
            $model->fromDate = date('Y-m-d', strtotime($model->date_from));
            $model->fromTime = date('H:i', strtotime($model->date_from));
        }
        
        if($model->date_to) {
            $model->toDate = date('Y-m-d', strtotime($model->date_to));
            $model->toTime = date('H:i', strtotime($model->date_to));
        }
        
        if ($member->load(Yii::$app->request->post())) {
            $member->status = -1;
            $member->discard_at = date('Y-m-d H:i:s');
            if($member->save()) {
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Zaproszenie zostało odrzucone')  );
            } else {
                $member->status = $statusTmp;
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Proszę wpisać powód odrzucenia!')  );
            }
        }
				
		return $this->render('view', [
            'model' => $model, 'member' => $member
        ]);
    }
    
    public function actionChange($id) {  
		$model = $this->findModel($id); 
        $customerId = $model->id_customer_fk;
        $orderId = $model->id_order_fk;
        $accPeriod = $model->acc_period;
        $actionDate = $model->action_date;
        $unitTime = $model->unit_time;
        $actionType = $model->id_service_fk;
              
        if($model->unit_time) {
            $model->timeH = intval($model->unit_time/60);
            $model->timeM = $model->unit_time - ($model->timeH*60);
        }
      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->type_fk = $model->id_service_fk;
            if($customerId != $model->id_customer_fk) {
                $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $model->id_customer_fk, 'status' => '1'])->all();
                if( count($orders) == 1 ) {
                    $model->id_order_fk = $orders[0]->id;
                } else {
                    $model->id_order_fk = 0;
                }
            }
           
            if( $model->validate() ) {
                $model->type_fk = $model->id_service_fk;
                
                if($model->is_confirm) {
                    $H = ($model->timeH) ? $model->timeH*60 : 0;
                    $m = ($model->timeM) ? $model->timeM : 0;
                    $model->unit_time = $H + $m;
                    
                    $model->confirm_time = round(($model->unit_time/60), 2);  
                    $model->acc_time = round(($model->unit_time/60), 2); 
                    /*if($model->id_order_fk) {
                        $orderTmp = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
                        if($model->id_service_fk == 3 && $orderTmp->id_dict_order_type_fk >= 2) {
                            $model->confirm_price = $orderTmp->rate_hourly_way;  
                        } else {
                            if($orderTmp->id_dict_order_type_fk == 2) {
                                if($model->id_service_fk == 1) {
                                    $model->confirm_price = $orderTmp->rate_hourly_1;
                                } else if($model->id_service_fk == 2) {
                                    $model->confirm_price = $orderTmp->rate_hourly_2;
                                }
                            } else if($orderTmp->id_dict_order_type_fk >= 3) {
                                if($model->id_service_fk == 1) {
                                    $model->confirm_price = $orderTmp->rate_hourly_1;
                                }
                                if($model->id_service_fk == 2) {
                                    $model->confirm_price = $orderTmp->rate_hourly_2;
                                }
                            } 
                        }
                       // $model->unit_price = $model->confirm_price;
                    } else {
                        $model->is_confirm = 0;
                    }*/
                } 
                if($model->save()) {	
                    if($model->is_confirm) {
                        if($orderId != $model->id_order_fk) {
                            if($orderId) {
                                $order = \backend\Modules\Accounting\models\AccOrder::findOne($orderId);
                                $this->calculation($order, $accPeriod);
                            }
                            if($model->id_order_fk) {
                                $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
                                $this->calculation($order, $model->acc_period);
                            }
                        } else {
                            $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
                            if($model->acc_period != $accPeriod) {
                                $this->calculation($order, $accPeriod);
                                $this->calculation($order, $model->acc_period);
                            } else {
                                if($model->action_date != $actionDate && $order->id_dict_order_type_fk == 2) {
                                    $this->calculation($order, $model->acc_period);
                                }
                                if($model->id_service_fk != $actionType) {
                                    $this->calculation($order, $model->acc_period);
                                }
                            }
                        }
                    }
                    return array('success' => true,   'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Zmiany zostały zapisane'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_changeAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            if($model->type_fk == 5)
                return  $this->renderAjax('_changeSpecialAjax', ['model' => $model]);	
            else
                return  $this->renderAjax('_changeAjax', ['model' => $model]);	
        }
	}
    
    public function actionSettle($id) {      
		$model = $this->findModel($id); 
        $tempOrder = $model->id_order_fk;
        if($model->unit_time) {
            $model->timeH = intval($model->unit_time/60);
            $model->timeM = $model->unit_time - ($model->timeH*60);
        }
      
        if (Yii::$app->request->isPost) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->user_action = 'settled';
            $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
            if(!$order && $model->is_confirm) {
                $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
                return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => [0 => 'Musisz wskazać zlecenie jesli chcesz zafakturować czynność'] );	
            }
            
            if(!$order && !$model->is_confirm) {
                $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
                $model->unit_price = 0;
                $model->unit_price_native = 0;
                $model->confirm_price = 0;
                $model->confirm_price_native = 0;
                $model->confirm_time = 0;
                $model->acc_limit = 0;
                $model->acc_time = 0;
                $model->acc_price = 0;
                $model->acc_price_native = 0;
				$model->id_unit_rate_fk = 0;
				$model->id_tax_fk = 0;
                
                if( $model->validate() ) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if($model->save()) {
                            if($tempOrder) { $modelOrder = \backend\Modules\Accounting\models\AccOrder::findOne($tempOrder); if($modelOrder) $this->calculation($modelOrder, $model->acc_period);  }
                            $transaction->commit();
                            return array('success' => true,  'index' => 0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Czynność została rozliczona'  );	
                        } else {
                            $transaction->rollBack();
                            return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
                        }
                        
                    } catch (Exception $e) {
                        $transaction->rollBack();
                        return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
                    }
                } else {
                    $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
                    return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => $model->getErrors() );	
                }		
            }
            
            $rateId = 0;
            if($model->is_confirm) {
                
                $invoiceExist = \backend\Modules\Accounting\models\AccInvoice::find()->where(['status' => 1, 'id_order_fk' => $model->id_order_fk, 'date_issue' => $model->acc_period.'-01'])->all();
                if($invoiceExist) {
                    $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
                    return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['id_order_fk' => 'To zlecenie na ten okres ma już wystawioną fakturę'] );	
                }
                
                $exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false;
                $exchangeRate = ($exchange) ? $exchange->rate_value : 1;
                $exchangeId = ($exchange) ? $exchange->id : 0;
             
                //$model->user_action = 'convert';
                $model->confirm_time = round(($model->unit_time/60), 2);  
                $model->acc_time = round(($model->unit_time/60), 2);  
				if($model->id_service_fk == 3 && $order->id_dict_order_type_fk >= 2) {
                    $model->unit_price = $order->rate_hourly_way;
                    $model->unit_price_native = round($model->unit_price*$exchangeRate,2);
					$model->confirm_price = $order->rate_hourly_way;
				} else {
					if($order->id_dict_order_type_fk == 2) {
						if($model->id_service_fk == 1) {
                            $model->confirm_price = $order->rate_hourly_1;
                            $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($model->employee['id_dict_employee_type_fk'], $model->id_employee_fk, $model->id_customer_fk, $model->id_order_fk, $model->action_date);
                            if($rateEmployee) {
                                $model->confirm_price = $rateEmployee->unit_price; $rateId = $rateEmployee->id;
                            }
                        } else if($model->id_service_fk == 2) {
							$model->confirm_price = $order->rate_hourly_2;
						}
                        $model->unit_price = $model->confirm_price;
                        $model->unit_price_native = round($model->unit_price*$exchangeRate,2);
                        $model->confirm_price_native = round($model->unit_price*$exchangeRate,2);
					} else if($order->id_dict_order_type_fk >= 3) {
						if($model->id_service_fk == 1) {
							$model->confirm_price = $order->rate_hourly_1;
                            if($order->id_dict_order_type_fk == 4) {
                                $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($model->employee['id_dict_employee_type_fk'], $model->id_employee_fk, $model->id_customer_fk, $model->id_order_fk, $model->action_date);
                                if($rateEmployee) {
                                    $model->confirm_price = $rateEmployee->unit_price;
                                }
                            }
						}
						if($model->id_service_fk == 2) {
							$model->confirm_price = $order->rate_hourly_2;
						}
                        $model->unit_price = $model->confirm_price;
                        $model->unit_price_native = round($model->unit_price*$exchangeRate,2);
                        $model->confirm_price_native = round($model->unit_price*$exchangeRate,2);
                    } 
                }
            } else {          
                //$model->user_action = 'reversal';
                $model->unit_price = 0;
                $model->unit_price_native = 0;
                $model->confirm_price = 0;
                $model->confirm_price_native = 0;
                $model->confirm_time = 0;
                $model->acc_limit = 0;
                $model->acc_time = 0;
                $model->acc_price = 0;
                $model->acc_price_native = 0;
            }
            $model->id_unit_rate_fk = $rateId;
            if( $model->validate() ) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if($model->save()) {
                        if($tempOrder) { $modelOrder = \backend\Modules\Accounting\models\AccOrder::findOne($tempOrder); if($modelOrder) $this->calculation($modelOrder, $model->acc_period);  }
                        if($model->id_order_fk) {  $modelOrder = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);  if($modelOrder) $this->calculation($modelOrder, $model->acc_period);  }
                        $transaction->commit();
                        return array('success' => true, 'index' => 0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Czynność została rozliczona'  );	
                    } else {
                        $transaction->rollBack();
                        return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
                    }
                    
                } catch (Exception $e) {
                    $transaction->rollBack();
                    return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
                }
            } else {
                $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
                return array('success' => false, 'html' => $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]), 'errors' => $model->getErrors() );	
			}		
		} else {
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($model->id_customer_fk);
           // $model->id_order_fk = $orders[0]['id'];
            return  $this->renderAjax('_settleAjax', [ 'model' => $model, 'orders' => $orders]);	
        }
	}
    
    public function actionRecalculation($order, $period) {
        $orderModel = AccOrder::findOne($order);
        $this->calculation($orderModel, $period);
        return true;
    }
       
    private function calculation($order, $period) {
        $discountData = AccOrder::getDiscount($order->id, $period);
        $discount = (!$discountData) ? 0 : $discountData->discount_percent;
        $discountId = (!$discountData) ? 0 : $discountData->id;
        $exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false;
        $exchangeRate = ($exchange) ? $exchange->rate_value : 1;
        
        $sqlUpdateCost = "update {{%acc_actions}} set "
                            ."acc_cost_native = case when (acc_cost_currency_id = 1) then acc_cost_net else round(acc_cost_net*".$exchangeRate.",2) end, "
                            ."acc_cost_invoice = case when (acc_cost_currency_id = 1 and ".$order->id_currency_fk." != 1) then round(acc_cost_net/".$exchangeRate.",2) "
                                                   ." when (acc_cost_currency_id != 1 and ".$order->id_currency_fk." = 1) then round(acc_cost_net*".$exchangeRate.",2) else acc_cost_net end "
                        ." where is_confirm = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and status = 1 and acc_period = '".$period."' and id_order_fk = ".$order->id; 
        Yii::$app->db->createCommand($sqlUpdateCost)->execute(); 
        
        $sqlUpdateSpecial = "update {{%acc_actions}} set "
                            ."unit_price_native = case when (id_currency_fk = 1) then unit_price else round(unit_price*".$exchangeRate.",2) end, "
                            ."unit_price_invoice = case when (id_currency_fk = 1 and ".$order->id_currency_fk." != 1) then round(unit_price/".$exchangeRate.",2) "
                                                   ." when (id_currency_fk != 1 and ".$order->id_currency_fk." = 1) then round(unit_price*".$exchangeRate.",2) else unit_price end, "
                            ."acc_cost_native = case when (acc_cost_currency_id = 1) then acc_cost_net else round(acc_cost_net*".$exchangeRate.",2) end, "
                            ."acc_cost_invoice = case when (acc_cost_currency_id = 1 and ".$order->id_currency_fk." != 1) then round(acc_cost_net/".$exchangeRate.",2) "
                                                   ." when (acc_cost_currency_id != 1 and ".$order->id_currency_fk." = 1) then round(acc_cost_net*".$exchangeRate.",2) else acc_cost_net end "
                        ." where type_fk = 5 and is_confirm = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and status = 1 and acc_period = '".$period."' and id_order_fk = ".$order->id; 
        Yii::$app->db->createCommand($sqlUpdateSpecial)->execute(); 
        
        $rateId = 0;
        
        if($order->id_dict_order_type_fk == 1) {
            $sql = "select sum(confirm_time) as limit_time from {{%acc_actions}} "
                    ." where is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."' and id_order_fk = ".$order->id;
                    //." where is_confirm = 1 and status = 1 and date_format(action_date, '%Y-%m') = '".$period."' and id_order_fk = ".$order->id;
           $settleData = Yii::$app->db->createCommand($sql)->queryOne(); 
           $base = $order->rate_constatnt;// + ($order->rate_hourly_way*$settleData['way_time']);
           $time = $settleData['limit_time'];//+$settleData['way_time'];
           $price = ($time > 0) ? round($base / $time, 2) : 0;
           $priceNative = ($order->id_currency_fk != 1) ? round(($price/$exchange->rate_value),2) : $price;
           $discountPrice = ($time > 0) ? round((($order->rate_constatnt - ($order->rate_constatnt*$discount/100)) / $time), 2) : 0;
           $discountPriceNative = ($order->id_currency_fk != 1) ? round(($discountPrice/$exchange->rate_value),2) : $discountPrice;
           AccActions::updateAll(['unit_price' => 0, 'confirm_price' => $price, 'confirm_price_native' => $priceNative, 'id_tax_fk' => $discountId, 'id_exchange_fk' => (($exchange) ? $exchange->id : 0), 'rate_exchange' => (($exchange) ? $exchange->rate_value : 0), 
                                  'acc_price' => $discountPrice, 'acc_price_native' => $discountPriceNative,  'acc_cost_gross' => $price], 
                                "is_confirm = 1 and status = 1 and id_order_fk = :order and acc_period = '".$period."' and (id_invoice_fk = 0 or id_invoice_fk is null)", [':order' => $order->id]);
           //AccActions::updateAll(['confirm_price' => $price], "is_confirm = 1 and status = 1 and id_order_fk = :order and date_format(action_date, '%Y-%m') = '".$period."'", [':order' => $order->id]);
        } else if($order->id_dict_order_type_fk == 2) {
            /*$sql = "select sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time from {{%acc_actions}} "
                    ." where is_confirm = 1 and status = 1 and date_format(action_date, '%Y-%m') = '".$period."' and id_order_fk = ".$order->id; */
            $sql = "select sum(confirm_time) as limit_time, sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time from {{%acc_actions}} "
                    ." where is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."' and id_order_fk = ".$order->id; 
            $settleData = Yii::$app->db->createCommand($sql)->queryOne(); 
            $over_the_limit = ( $settleData['limit_time'] <= $order->limit_hours ) ? 0 : ($settleData['limit_time'] - $order->limit_hours);
            //$base = $order->rate_constatnt + ($over_the_limit*$order->rate_hourly_1) + ($order->rate_hourly_2*$settleData['admin_time']) + ($order->rate_hourly_way*$settleData['way_time']);
            //$time = $settleData['limit_time'];//+$settleData['admin_time']+$settleData['way_time'];
            //$price = ($time > 0) ? round($base / $time, 2) : 0;
            $base = $order->rate_constatnt;
            $time = $settleData['limit_time'];
            
            if($over_the_limit == 0) {
                $price = ($time > 0) ? round($base / $time, 2) : 0;
                $priceNative = ($order->id_currency_fk != 1) ? round(($price*$exchange->rate_value),2) : $price;
                $discountPrice = ($time > 0) ? round((($base - ($base*$discount/100)) / $time), 2) : 0;
                $discountPriceNative = ($order->id_currency_fk != 1) ? round(($discountPrice*$exchange->rate_value),2) : $discountPrice;
                $sqlUpdate = "update {{%acc_actions}} set acc_limit = confirm_time "
                                .", unit_price = ".$price.", unit_price_native = ".$priceNative.", confirm_price = ".$price.", confirm_price_native = ".$priceNative.", acc_price = ".$discountPrice.", acc_price_native = ".$discountPriceNative
                                .", acc_cost_gross = ".$price.", id_tax_fk = ".$discountId
                            ." where is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."' and id_order_fk = ".$order->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                //AccActions::updateAll(['confirm_price' => $price], "is_confirm = 1 and status = 1 and id_order_fk = :order and date_format(action_date, '%Y-%m') = '".$period."'", [':order' => $order->id]);
            } else {
                /*$sqlUpdate = "update {{%acc_actions}} set confirm_price = ".$price
                    ." where is_confirm = 1 and status = 1 and date_format(action_date, '%Y-%m') = '".$period."' and id_order_fk = ".$order->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); */
                $priceInLimit = ($time > 0) ? round($base / $order->limit_hours, 2) : 0;
                $sqlActions = "select a.id as id, action_date, confirm_time, id_employee_fk, id_service_fk, id_dict_employee_type_fk from {{%acc_actions}} a join {{%company_employee}} ce on ce.id = a.id_employee_fk "
                    ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."' and id_order_fk = ".$order->id
                    ." order by is_priority desc, rank_priority, action_date, id"; 
                $actions = Yii::$app->db->createCommand($sqlActions)->query(); 
                $limitTemp = $order->limit_hours;
                $actionsInLimit = [];
                foreach($actions as $key => $action) {
                    $rateId = 0;
                    if($limitTemp >= $action['confirm_time']) {
                        array_push($actionsInLimit, $action['id']);
                        $limitTemp = $limitTemp - $action['confirm_time'];
                    } else if ($limitTemp > 0 && $limitTemp < $action['confirm_time']) {
                        if($action['id_service_fk'] == 1) {
                            $hourly_price = $order->rate_hourly_1; 
                            $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($action['id_dict_employee_type_fk'], $action['id_employee_fk'], $order->id_customer_fk, $order->id, $action['action_date']);
                            if($rateEmployee) { $hourly_price = $rateEmployee->unit_price; $rateId = $rateEmployee->id; }  
                        }
                        if($action['id_service_fk'] == 2) {
                            $hourly_price = $order->rate_hourly_2; 
                        }
                        if($action['id_service_fk'] == 3) {
                            $hourly_price = $order->rate_hourly_way; 
                        }
                        $priceTemp = round((($limitTemp*$priceInLimit + ($action['confirm_time']-$limitTemp)*$hourly_price)/$action['confirm_time']), 2);
                        $sqlUpdate = "update {{%acc_actions}} set acc_limit = ".$limitTemp.", unit_price = ".$priceTemp.", id_unit_rate_fk = ".$rateId.", acc_cost_gross = ".$priceInLimit.", id_tax_fk = ".$discountId
                            ." where id = ".$action['id']; 
                        Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                        $base += (($action['confirm_time']-$limitTemp)*$hourly_price);
                        $limitTemp = 0;
                    } else {
                        if($action['id_service_fk'] == 1) {
                            $hourly_price = $order->rate_hourly_1; 
                            $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($action['id_dict_employee_type_fk'], $action['id_employee_fk'], $order->id_customer_fk, $order->id, $action['action_date']);
                            if($rateEmployee) { $hourly_price = $rateEmployee->unit_price; $rateId = $rateEmployee->id; }  
                        }
                        if($action['id_service_fk'] == 2) {
                            $hourly_price = $order->rate_hourly_2; 
                        }
                        if($action['id_service_fk'] == 3) {
                            $hourly_price = $order->rate_hourly_way; 
                        }
                        $sqlUpdate = "update {{%acc_actions}} set acc_limit = 0, unit_price = ".$hourly_price.", id_unit_rate_fk = ".$rateId.", acc_cost_gross = ".$priceInLimit.", id_tax_fk = ".$discountId
                            ." where id = ".$action['id']; 
                        Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                        $base += ($action['confirm_time']*$hourly_price); 
                    }
                }
                $sqlUpdate = "update {{%acc_actions}} set acc_limit = confirm_time, unit_price = ".$priceInLimit.", acc_cost_gross = ".$priceInLimit.", id_tax_fk = ".$discountId
                            ." where id in (".implode(',', $actionsInLimit).")"; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                //echo $base.' - '.$time;exit;
                $price = ($time > 0) ? round(($base) / $time, 2) : 0;
                $priceNative = ($order->id_currency_fk != 1) ? round(($price*$exchange->rate_value),2) : $price;
                $discountPrice = ($time > 0) ? round(($base-($base*$discount/100)) / $time, 2) : 0;
                $sqlUpdate = "update {{%acc_actions}} set confirm_price = ".$price.", acc_price = ".$discountPrice.", id_tax_fk = ".$discountId
                            ." where is_confirm = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and status = 1 and acc_period = '".$period."' and id_order_fk = ".$order->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
            }
        } else if($order->id_dict_order_type_fk == 3 || $order->id_dict_order_type_fk == 4) {
            $sql = "select id_employee_fk, id_dict_employee_type_fk, sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time "
                    ." from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk"
                    ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$period."' and id_order_fk = ".$order->id
                    ." group by id_employee_fk, id_dict_employee_type_fk"; 
            $settleData = Yii::$app->db->createCommand($sql)->query();
            $baseAll = 0; $timeAll = 0;
            foreach($settleData as $key => $employee) {
                $hourly_1 = $order->rate_hourly_1;
                $rateId = 0;
                if($order->id_dict_order_type_fk == 4) {
                    $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_dict_employee_type_fk'], $employee['id_employee_fk'], $order->id_customer_fk, $order->id, $period.'-01');
                    if($rateEmployee) { $hourly_1 = $rateEmployee->unit_price; $rateId = $rateEmployee->id; }
                }
                //$hourlyNative = ($order->id_currency_fk != 1) ? round(($hourly_1/$exchange->rate_value),2) : $hourly_1;
                $base = ($hourly_1*$employee['limit_time']) + ($order->rate_hourly_2*$employee['admin_time']) + ($order->rate_hourly_way*$employee['way_time']);
                $baseAll += $base;
                $time = $employee['limit_time']+$employee['admin_time']+$employee['way_time'];
                $timeAll += $time;
                $price = ($time == 0) ? 0 : round($base / $time, 2);
                $priceNative = ($order->id_currency_fk != 1) ? round(($price*$exchange->rate_value),2) : $price;
                $discountPrice = round($price-($price*$discount/100), 2);
                $discountPriceNative = ($order->id_currency_fk != 1) ? round(($discountPrice*$exchange->rate_value),2) : $discountPrice;
				
                AccActions::updateAll(['confirm_price' => $price, 'confirm_price_native' => $priceNative, 'acc_price' => $discountPrice, 'acc_price_native' => $discountPriceNative,
                                       'acc_limit' => 0, 'id_tax_fk' => $discountId], "is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = :order and id_employee_fk = :employee and acc_period = '".$period."'", [':order' => $order->id, ':employee' => $employee['id_employee_fk']]);
				AccActions::updateAll(['unit_price' => $hourly_1, 'id_unit_rate_fk' => $rateId], " id_service_fk = 1 and is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = :order and id_employee_fk = :employee and acc_period = '".$period."'", [':order' => $order->id, ':employee' => $employee['id_employee_fk']]);

			} 
            //$discountPrice = ($timeAll > 0) ? round((($baseAll - $discount) / $timeAll), 2) : 0;
            //AccActions::updateAll(['acc_price' => $discountPrice, 'id_tax_fk' => $discountId], "is_confirm = 1 and status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_order_fk = :order and acc_period = '".$period."'", [':order' => $order->id]);
        }
        return true;
    }
    
    public function actionMsettle($id) {
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $id = CustomHelpers::decode($id);
        if($id == 0) $id = $_GET['customer'];
        
        if(!$id || $id == 0) {
            return '<div class="alert alert-info">Aby skorzytsać z tej funkcjonalności musisz w filtrze wybrać klienta</div>';
        } else {
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            if($orders) $model->id_order_fk = $orders[0]['id'];
            $model->actions = [];
            if( isset($_GET['ids']) && !empty($_GET['ids']) ) $model->actions = explode(',', $_GET['ids']);
            return  $this->renderAjax('_formSettle', [ 'model' => $model, 'orders' => $orders, 'id' => $id ]) ;	
        }
    }
    
    public function actionCmsettle($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $model->load(Yii::$app->request->post());
        if(!$model->validate()) {
            $id = CustomHelpers::decode($id);
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            return array('success' => false, 'html' => $this->renderAjax('_formSettle', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
        }
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
        
        $invoiceExist = \backend\Modules\Accounting\models\AccInvoice::find()->where(['status' => 1, 'id_order_fk' => $model->id_order_fk])
                        ->andWhere('date_issue in (select concat(acc_period,"-01") from  {{%acc_actions}} where id in ('.implode(',', $model->actions).'))')->one();
        if($invoiceExist) {
            $id = CustomHelpers::decode($id);
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            return array('success' => false, 'html' => $this->renderAjax('_formSettle', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['id_order_fk' => 'To zlecenie na okres '.(date('Y-m', strtotime($invoiceExist->date_issue))).' ma już wystawioną fakturę'] );	
        }
                
        $exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false;
        $exchangeRate = ($exchange) ? $exchange->rate_value : 1;
        $exchangeId = ($exchange) ? $exchange->id : 0;
        //AccActions::updateAll(['is_confirm' => $model->is_confirm, 'id_order_fk' => $model->id_order_fk],'id in ('.implode(',', $model->actions).')');
        if($order) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                 $sql = "update {{%acc_actions}} "
                    . " set is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk 
                    . " where id_service_fk >= 4 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute();
                if($order->id_dict_order_type_fk == 1) {
                    $sql = "update {{%acc_actions}} "
                    . " set is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), unit_price = 0, unit_price_native = 0"
                    . " where (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    //. " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute();
                } else if($order->id_dict_order_type_fk == 2 || $order->id_dict_order_type_fk == 3) {
                    if($order->id_dict_order_type_fk == 2) {
                        $sql = "select id_employee_fk, id_dict_employee_type_fk, sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time "
                                ." from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk"
                                ." where a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and a.id in (".implode(',', $model->actions).")"
                                ." group by id_employee_fk, id_dict_employee_type_fk"; 
                        $actionsData = Yii::$app->db->createCommand($sql)->query(); 
                        foreach($actionsData as $key => $employee) {
                            $hourly_1 = $order->rate_hourly_1;
                            $rateId = 0;
                            /*$rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_dict_employee_type_fk'], $employee['id_employee_fk'], $order->id_customer_fk, $order->id);
                            if($rateEmployee) { $hourly_1 = $rateEmployee->unit_price; $rateId = $rateEmployee->id; }*/
                            
                            $sql = "update {{%acc_actions}} set "
                                . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                                . " unit_price = ".$hourly_1.", id_confirm_rate_fk = ".$rateId. ", unit_price_native = ".(round($hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                                . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_employee_fk = ".$employee['id_employee_fk']." and id in (".implode(',', $model->actions).")";
                            
                            Yii::$app->db->createCommand($sql)->execute(); 
                        } 
                    } else {
                        $sql = "update {{%acc_actions}} set "
                        . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                        . " unit_price = ".$order->rate_hourly_1. ", unit_price_native = ".(round($order->rate_hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                        . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                        Yii::$app->db->createCommand($sql)->execute(); 
                    }
                    $sql = "update {{%acc_actions}} set"
                    . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                    . " unit_price = ".$order->rate_hourly_2. ", unit_price_native = ".(round($order->rate_hourly_2*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                    . " where id_service_fk = 2 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute(); 
                } else if($order->id_dict_order_type_fk == 4) {
                   /* $sql = "update {{%acc_actions}} "
                    . " set is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), unit_price = ".$order->rate_hourly_1
                    . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute(); */
                    $sql = "select id_employee_fk, id_dict_employee_type_fk, max(action_date) as action_date, sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, "
                                ." sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time "
                            ." from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk"
                            ." where a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and a.id in (".implode(',', $model->actions).")"
                            ." group by id_employee_fk, id_dict_employee_type_fk"; 
                    $actionsData = Yii::$app->db->createCommand($sql)->query();
                    foreach($actionsData as $key => $employee) {
                        $hourly_1 = $order->rate_hourly_1;
                        $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_dict_employee_type_fk'], $employee['id_employee_fk'], $order->id_customer_fk, $order->id, $employee['action_date']);
                        if($rateEmployee) $hourly_1 = $rateEmployee->unit_price;
                        
                        $sql = "update {{%acc_actions}} set"
                            . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                            . " unit_price = ".$hourly_1.", unit_price_native = ".(round($hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                            . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and id_employee_fk = ".$employee['id_employee_fk']." and id in (".implode(',', $model->actions).")";
                        Yii::$app->db->createCommand($sql)->execute(); 
                    } 
                    $sql = "update {{%acc_actions}} set"
                    . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                    . " unit_price = ".$order->rate_hourly_2. ", unit_price_native = ".(round($order->rate_hourly_2*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                    . " where id_service_fk = 2 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute(); 
                }
                if($order->id_dict_order_type_fk != 1) {
                    $sql = "update {{%acc_actions}} set "
                        . " is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                        . " unit_price = ".$order->rate_hourly_way. ", unit_price_native = ".(round($order->rate_hourly_way*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                        . " where id_service_fk = 3 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                }
                //echo $sql;exit;
                Yii::$app->db->createCommand($sql)->execute(); 
                
                $sql = "select distinct acc_period as period from {{%acc_actions}} where id in (".implode(',', $model->actions).")";
                $periods = Yii::$app->db->createCommand($sql)->query();
                foreach($periods as $key => $period) {
                    $this->calculation($order, $period['period']);
                }
                $transaction->commit();
                return array('success' => true,  'action' => 'msettle', 'alert' => 'Czynności zostały oznaczone'  );		
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formSettle', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
            }
        } else {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $sql = "update {{%acc_actions}} "
                        . " set is_confirm = 0, id_order_fk = 0, unit_price = 0, confirm_price = 0, acc_price = 0 "
                        . " where (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
                Yii::$app->db->createCommand($sql)->execute();
                
                /*$sql = "select distinct acc_period as period from {{%acc_actions}} where id in (".implode(',', $model->actions).")";
                $periods = Yii::$app->db->createCommand($sql)->query();
                foreach($periods as $key => $period) {
                    $this->calculation($order, $period['period']);
                }*/
                $transaction->commit();
                return array('success' => true,  'action' => 'msettle', 'alert' => 'Czynności zostały odznaczone'  );
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formSettle', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z rozliczeniem'] );	
            }
        }
		//$actions = AccActions::find()->where('id in ('.implode(',', $_POST['ids']).')')->all();
			
	}
    
    public function actionAllsettle($id, $period) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($id);
        
        $invoiceExist = \backend\Modules\Accounting\models\AccInvoice::find()->where(['status' => 1, 'id_order_fk' => $id])
                        ->andWhere("date_format(date_issue, '%Y-%m') = '".$period."'")->one();
        if($invoiceExist) {
            $id = CustomHelpers::decode($id);
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            return array('success' => false, 'errors' => [0 => 'Umowa jest już rozliczona na okres '.$period], 'table' => '#table-orders' );	
        }
                
        $exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false;
        $exchangeRate = ($exchange) ? $exchange->rate_value : 1;
        $exchangeId = ($exchange) ? $exchange->id : 0;
        if($order) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                 $sql = "update {{%acc_actions}} "
                    . " set is_confirm = 1 "
                    . " where id_service_fk >= 4 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                    Yii::$app->db->createCommand($sql)->execute();
                if($order->id_dict_order_type_fk == 1) {
                    $sql = "update {{%acc_actions}} "
                    . " set is_confirm = ". $model->is_confirm .", id_order_fk = ". $model->id_order_fk .", confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), unit_price = 0, unit_price_native = 0"
                    . " where (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                    Yii::$app->db->createCommand($sql)->execute();
                } else if($order->id_dict_order_type_fk == 2 || $order->id_dict_order_type_fk == 3) {
                    if($order->id_dict_order_type_fk == 2) {
                        $sql = "select id_employee_fk, id_dict_employee_type_fk, sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time "
                                ." from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk"
                                ." where a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'"
                                ." group by id_employee_fk, id_dict_employee_type_fk"; 
                        $actionsData = Yii::$app->db->createCommand($sql)->query(); 
                        foreach($actionsData as $key => $employee) {
                            $hourly_1 = $order->rate_hourly_1;
                            $rateId = 0;
                            
                            $sql = "update {{%acc_actions}} set "
                                . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                                . " unit_price = ".$hourly_1.", id_confirm_rate_fk = ".$rateId. ", unit_price_native = ".(round($hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                                . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_employee_fk = ".$employee['id_employee_fk']." and id_order_fk = ".$id." and acc_period = '".$period."'";
                            
                            Yii::$app->db->createCommand($sql)->execute(); 
                        } 
                    } else {
                        $sql = "update {{%acc_actions}} set "
                        . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                        . " unit_price = ".$order->rate_hourly_1. ", unit_price_native = ".(round($order->rate_hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                        . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                        Yii::$app->db->createCommand($sql)->execute(); 
                    }
                    $sql = "update {{%acc_actions}} set"
                    . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                    . " unit_price = ".$order->rate_hourly_2. ", unit_price_native = ".(round($order->rate_hourly_2*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                    . " where id_service_fk = 2 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                    Yii::$app->db->createCommand($sql)->execute(); 
                } else if($order->id_dict_order_type_fk == 4) {
                    $sql = "select id_employee_fk, id_dict_employee_type_fk, max(action_date) as action_date, sum(case when id_service_fk=1 then confirm_time else 0 end) as limit_time, "
                                ." sum(case when id_service_fk=2 then confirm_time else 0 end) as admin_time, sum(case when id_service_fk=3 then confirm_time else 0 end) as way_time "
                            ." from {{%acc_actions}} a join {{%company_employee}} e on e.id=a.id_employee_fk"
                            ." where a.status = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and a.id_order_fk = ".$id." and a.acc_period = '".$period."'"
                            ." group by id_employee_fk, id_dict_employee_type_fk"; 
                    $actionsData = Yii::$app->db->createCommand($sql)->query();
                    foreach($actionsData as $key => $employee) {
                        $hourly_1 = $order->rate_hourly_1;
                        $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_dict_employee_type_fk'], $employee['id_employee_fk'], $order->id_customer_fk, $order->id, $employee['action_date']);
                        if($rateEmployee) $hourly_1 = $rateEmployee->unit_price;
                        
                        $sql = "update {{%acc_actions}} set"
                            . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                            . " unit_price = ".$hourly_1.", unit_price_native = ".(round($hourly_1*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                            . " where id_service_fk = 1 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_employee_fk = ".$employee['id_employee_fk']." and id_order_fk = ".$id." and acc_period = '".$period."'";
                        Yii::$app->db->createCommand($sql)->execute(); 
                    } 
                    $sql = "update {{%acc_actions}} set"
                    . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                    . " unit_price = ".$order->rate_hourly_2. ", unit_price_native = ".(round($order->rate_hourly_2*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                    . " where id_service_fk = 2 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                    Yii::$app->db->createCommand($sql)->execute(); 
                }
                if($order->id_dict_order_type_fk != 1) {
                    $sql = "update {{%acc_actions}} set "
                        . " is_confirm = 1, confirm_time = round(unit_time/60,2), acc_time = round(unit_time/60,2), "
                        . " unit_price = ".$order->rate_hourly_way. ", unit_price_native = ".(round($order->rate_hourly_way*$exchangeRate, 2)).", id_exchange_fk = ".$exchangeId.", rate_exchange = ".$exchangeRate
                        . " where id_service_fk = 3 and (id_invoice_fk = 0 or id_invoice_fk is null) and (is_gratis=0 or is_gratis is null) and id_order_fk = ".$id." and acc_period = '".$period."'";
                }
                //echo $sql;exit;
                Yii::$app->db->createCommand($sql)->execute(); 

                $this->calculation($order, $period);
                
                $transaction->commit();
                return array('success' => true, 'alert' => 'Czynności zostały oznaczone do zafakturowania', 'table' => '#table-actions'  );		
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'errors' => ['settle' => 'Problem z oznaczeniem czynności'], 'table' => '#table-actions' );	
            }
        } 	
	}
    
    public function actionAllunsettle($id, $period) {
		Yii::$app->response->format = Response::FORMAT_JSON;
       
        $id = CustomHelpers::decode($id);
        $order = AccOrder::findOne($id);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            /* invoice */
            $invoiceExist = \backend\Modules\Accounting\models\AccInvoice::find()->where(['status' => 1, 'id_order_fk' => $id])
                        ->andWhere("date_format(date_issue, '%Y-%m') = '".$period."'")->one();
            if($invoiceExist) {
                $invoiceExist->status = 0;
                $invoiceExist->opened_by = Yii::$app->user->id;
                $invoiceExist->opened_at = date('Y-m-d H:i:s');
                $invoiceExist->save();
                
                $values = \backend\Modules\Accounting\models\AccPeriod::getValues($period);
                $periodArr = explode('-', $period);
                $sql = "update {{%acc_period}} "
                . " set all_time = ". ( ($values['inv_time']) ? $values['inv_time'] : 0) 
                .", all_profit = ". ( ($values['inv_profit']) ? $values['inv_profit'] : 0)
                .", all_cost = ". ( ($values['inv_costs']) ? $values['inv_costs'] : 0)
                .", all_discount = ". ( ($values['inv_discount']) ? $values['inv_discount'] : 0)
                . " where period_year = ".$periodArr[0]." and period_month = ".($periodArr[1]*1);
                Yii::$app->db->createCommand($sql)->execute();
                
                $sqlUpdate = "update {{%acc_discount}} set id_invoice_fk = 0, discount_amount = 0"
                            ." where id_invoice_fk = ".$invoiceExist->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
            }    
            /* actions */
            $sql = "update {{%acc_actions}} "
                    . " set is_confirm = 0, unit_price = 0, unit_price_native = 0, confirm_price = 0, confirm_price_native = 0, acc_price = 0, acc_price_native = 0,id_unit_rate_fk = 0, id_tax_fk = 0, acc_limit = 0, id_invoice_fk=0 "
                    . " where type_fk != 5 and id_order_fk = ".$id." and acc_period = '".$period."'";
            //echo $sql; exit;
            $result = Yii::$app->db->createCommand($sql)->execute();
            
            $sqlSpecial = "update {{%acc_actions}} "
                    . " set is_confirm = 0, unit_price_native = 0, confirm_price = 0, confirm_price_native = 0, acc_price = 0, acc_price_native = 0,id_unit_rate_fk = 0, id_tax_fk = 0, id_invoice_fk = 0 "
                    . " where type_fk = 5 and id_order_fk = ".$id." and acc_period = '".$period."'";
            //echo $sql; exit;
            $result = Yii::$app->db->createCommand($sqlSpecial)->execute();
 
            $transaction->commit();
            return array('success' => true, 'alert' => 'Czynności zostały wycofane', 'table' => '#table-actions', 'result' => $result );
        } catch (Exception $e) {
            $transaction->rollBack();
            return array('success' => false,  'errors' => ['settle' => 'Problem z wycofaniem'],  'table' => '#table-actions' );	
        }
    }
	
	public function actionForward($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $model->user_action = 'period';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $oldPeriod = $model->acc_period;        
		$model->acc_period = date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );
        $model->is_shift = 1;
        $model->user_shift = Yii::$app->user->id;
        $model->date_shift = date('Y-m-d H:i:s');
		
		if($model->unit_time) {
            $model->timeH = intval($model->unit_time/60);
            $model->timeM = $model->unit_time - ($model->timeH*60);
        }
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			//$transaction->commit();
			if($model->is_confirm == 1) {
				$order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
				$this->calculation($order, $oldPeriod);
				$this->calculation($order, $model->acc_period);
			}
			return array('success' => true,  'action' => 'forward', 'alert' => 'Czynność została przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions'  );		
        } else {
            return array('success' => false,  'action' => 'forward', 'alert' => 'Czynność nie może zostać przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions', 'errors' => $model->getErrors()  );	
        }
		/*} catch (Exception $e) {
			$transaction->rollBack();
			return array('success' => false,  'action' => 'forward', 'alert' => 'Czynność nie może zostać przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions'  );		
		}*/		
	}
    
    public function actionBackward($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $model->user_action = 'period';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $oldPeriod = $model->acc_period; 
		$model->acc_period = $model->acc_period; //date('Y-m', strtotime($model->action_date) );
        $model->is_shift = 0;
        $model->user_shift = Yii::$app->user->id;
        $model->date_shift = date('Y-m-d H:i:s');
		
		if($model->unit_time) {
            $model->timeH = intval($model->unit_time/60);
            $model->timeM = $model->unit_time - ($model->timeH*60);
        }
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			//$transaction->commit();
			if($model->is_confirm == 1) {
				$order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
				$this->calculation($order, $oldPeriod);
				$this->calculation($order, $model->acc_period);
			}
			return array('success' => true,  'action' => 'backend', 'alert' => 'Czynność została przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions'  );		
        } else {
            return array('success' => false,  'action' => 'backend', 'alert' => 'Czynność nie może zostać przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions', 'errors' => $model->getErrors()  );	
        }
		/*} catch (Exception $e) {
			$transaction->rollBack();
			return array('success' => false,  'action' => 'forward', 'alert' => 'Czynność nie może zostać przeniesiona na następny okres rozliczeniowy', 'table' => '#table-actions'  );		
		}*/		
	}
    
    public function actionCreate() {    
		$model = new ComMeeting; 
        $model->meeting_employees = [];
        array_push($model->meeting_employees, Yii::$app->user->id);
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() ) {
                
                if($model->save()) {	
                    $model->date_from = (!$model->fromDate || !$model->fromTime) ? false : ( $model->fromDate . ' ' . $model->fromTime . ':00' );
                    $model->date_to = (!$model->toDate || !$model->toTime) ? false : ( $model->toDate . ' ' . $model->toTime . ':00' );
                    /*if(!$model->meeting_resource) $model->meeting_resource = [];
                    foreach($model->meeting_resource as $key => $resource) {
                        $newSchedule = new \backend\Modules\Company\models\CompanyResourcesSchedule();
                        $newSchedule->status = 0;
                        $newSchedule->id_resource_fk = $resource;
                        $newSchedule->id_employee_fk = $this->module->params['employeeId'];
                        $newSchedule->date_from = $model->date_from;
                        $newSchedule->date_to = $model->date_to;
                        $newSchedule->describe = $model->meeting_purpose;
                        $newSchedule->type_event = 2;
                        $newSchedule->id_event_fk = $model->id;
                        $newSchedule->save();
                        
                        $newMeetingResource = new \backend\Modules\Community\models\ComMeetingResource();
                        $newMeetingResource->id_meeting_fk = $newMeeting->id;
                        $newMeetingResource->id_resource_fk = $resource;
                        $newMeetingResource->id_resource_schedule_fk = $newSchedule->id;
                        $newMeetingResource->save();
                    }*/
                       
                    return array('success' => true,   'index' =>0, 'id' => $model->id, 'name' => $model->title, 'alert' => 'Termin spotkania został zmieniony'  );	
                } 
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)   {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if(!$model->id_invoice_fk) {
            $model->status = -1;
            $model->user_action = 'delete';
            $model->deleted_at = date('Y-m-d H:i:s');
            $model->deleted_by = \Yii::$app->user->id;
            if($model->save()) {
                if($model->is_confirm == 1) {
                    $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
                    $this->calculation($order, $model->acc_period);
                }
                return array('success' => true, 'alert' => 'Dane zostały usunięte', 'table' => '#table-actions');	
            } else {
                //var_dump($model->getErrors());
                return array('success' => false, 'alert' => 'Dane nie zostały usunięte', 'errors' => $model->getErrors());	
            }
        } else {
            return array('success' => false, 'alert' => 'Czynność jest już podpięta do faktury i nie może zostać usunięta', );	
        }
       // return $this->redirect(['index']);
    }

    /**
     * Finds the ComMeeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccActions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = AccActions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Czynność o wskazanym identyfikatorze nie została odnaleziona w systemie.');
        }
    }
    
    public function actionUpdate($id) {
       
		$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one();
		$model = $this->findModel($id);
		$listItem = '';
        
        $member = false;
        if($model->id_meeting_fk) {
            $member = \backend\Modules\Community\models\ComMeetingMember::findOne($model->id_meeting_fk);
        }
        
        if($model->unit_time) {
            $model->timeH = intval($model->unit_time/60);
            $model->timeM = $model->unit_time - ($model->timeH*60);
        }
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
	        $model->type_fk = $model->id_service_fk;
            if($model->validate() && $model->save()) {				
                return array('success' => true, 'id' => $model->id, 'name' => $model->name, 'listItem' => $listItem, 'alert' => 'Zadanie zostało dodane', /*'todoWidget' => TodoWidget::widget(['refresh' => true]) */ );	
			
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAction', [ 'model' => $model, 'member' => $member]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAction', [ 'model' => $model, 'member' => $member]);	
        }
    }
    
    public function actionExport() {	
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
		$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności na rzecz klienta');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Pracownik')
            ->setCellValue('B3', 'Data')
            ->setCellValue('C3', 'Czas trwania')
            ->setCellValue('D3', 'W limicie')
            ->setCellValue('E3', 'Stawka')
			->setCellValue('F3', 'Kwota')
			->setCellValue('G3', 'Opis czynności')
            ->setCellValue('H3', 'Typ czynności')
            ->setCellValue('I3', 'Sprawa')
            ->setCellValue('J3', 'Koszty')
            ->setCellValue('K3', 'Klient')
            ->setCellValue('L3', 'Sposób rozliczenia')
            ->setCellValue('M3', 'Rozliczono do faktury');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:M3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = []; $where = [];
		
		if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                if($params['type_fk'] == 4)
                    array_push($where, "(a.type_fk = ".$params['type_fk']." or (acc_cost_net > 0))");
                else 
                    array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "(is_confirm = 1 or is_gratis = 1)");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0 and (is_gratis = 0 or is_gratis is null)");
                if($params['invoiced'] == 3) array_push($where, "is_confirm = 1 and (id_invoice_fk is not null and id_invoice_fk != 0)");
                if($params['invoiced'] == 4) array_push($where, "is_gratis = 1");
                if($params['invoiced'] == 5) array_push($where, "(a.id_order_fk is null or a.id_order_fk=0)");
            }
        } else {
            $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
            if($dateDay <= 10 && $dateMonth > 1) {
                if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
                $date_from = $dateYear.'-'.$month.'-01';
                $date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            } else if($dateDay <= 10 && $dateMonth == 1) {
                $date_from = ($dateYear-1).'-12-01';
                $date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
            } else {
                $date_from = $dateYear.'-'.date('m').'-01';
                $date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            }
            array_push($where, "action_date >= '".$date_from."'");
            array_push($where, "action_date <= '".$date_to."'");
        }
        
        $sortColumn = 'is_priority desc, rank_priority, action_date, id';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "is_gratis",  "a.name as name", "a.description as description", 'acc_cost_net', 'acc_cost_description', 'is_confirm', "confirm_time", "acc_limit", "a.unit_price as unit_price", "confirm_price", "action_date", "a.unit_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "cc.name as ccname", "s.name as sname", "o.name as oname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('JOIN', '{{%acc_service}} as s', 's.id = a.id_service_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = a.id_set_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
            
	    $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_native else 0 end) as costs",
                      "sum(case when is_confirm=1 then confirm_time else 0 end) as confirm_time", 
                      "sum(case when a.type_fk=5 then unit_price_native when acc_limit=0 then (confirm_time*unit_price_native) else ((confirm_time-acc_limit)*unit_price_native) end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
            
        $queryOrders = (new \yii\db\Query())
            ->select(["a.id_order_fk", "min(rate_constatnt) as rate_constatnt"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1] )
            ->groupby(['a.id_order_fk']);
              
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
            $queryOrders = $queryOrders->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time'];
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
        
        $totalOrders = $queryOrders->all(); 
        $totalLump = 0;
        foreach($totalOrders as $io => $order) {
            $totalLump += $order['rate_constatnt'];
        }
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
			
		if( $count > 0 ) {
			foreach($rows as $record){ 
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, ($record['type_fk'] == 5) ? $record['name'] : $record['ename'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['action_date'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record['is_confirm'] == 1) ? $record['confirm_time'] : round($record['unit_time']/60,2)); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['acc_limit']); 
                
                $timeCounted = $record['confirm_time'];
                if( $record['id_service_fk'] == 4 || $record['id_service_fk'] == 5 || $record['is_gratis']) {
                    $tmp['price'] = 0;
                } else if($record['confirm_time'] == $record['acc_limit'] && $record['is_confirm'] == 1) { 
                    $record['unit_price'] = 0;
                    $tmp['price'] = '<span class="label bg-pink">'.round($record['unit_price'], 2).'</span>';
                } else {
                    $timeCounted = $record['confirm_time'] - $record['acc_limit'];
                    $record['acc_limit'] = $record['acc_limit'] * 60;
                    $timeH_l = intval($record['acc_limit']/60);
                    $timeM_l = $record['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                    $tmp['price'] = ($record['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($record['unit_price'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($record['unit_price'], 2);
                }
                
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['unit_price']); 
                if($record['is_gratis'])
                    $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, 'gratis'); 
                else
                    $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, ($record['type_fk'] == 5) ? $record['unit_price'] : round($record['unit_price']*$timeCounted, 2)); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, str_replace(array("\n", "\r"), '', trim($record['description']))); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['sname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['ccname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, $record['acc_cost_net']); 
                $objPHPExcel->getActiveSheet()->setCellValue('K'. $i, $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('L'. $i, $record['type_name']); 
                $objPHPExcel->getActiveSheet()->setCellValue('M'. $i, (($record['is_confirm']) ? 'TAK' : 'NIE')); 
						
				$i++; 
			} 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i,'=SUM(C4:C'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i,'poza ryczałtem'); 
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i,'=SUM(F4:F'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            
            $objPHPExcel->getActiveSheet()->setCellValue('J'. $i,'=SUM(J4:J'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('J'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('J'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            $objPHPExcel->getActiveSheet()->getStyle('J'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
		}
        ++$i;
        $objPHPExcel->getActiveSheet()->setCellValue('E'. $i,'ryczał');
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
        $objPHPExcel->getActiveSheet()->setCellValue('F'. $i,$totalLump);
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getNumberFormat()->setFormatCode('#,##0.00'); 
        ++$i;
        $objPHPExcel->getActiveSheet()->setCellValue('E'. $i,'razem'); 
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        $objPHPExcel->getActiveSheet()->setCellValue('F'. $i,'=F'.($i-2).'+F'.($i-1));
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        $objPHPExcel->getActiveSheet()->getStyle('F'. $i)->getNumberFormat()->setFormatCode('#,##0.00'); 
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
       // $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:M'.($i-1))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:M'.($i-1))->getAlignment()->setWrapText(true);
        
        $styleArraySum = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        
        $objPHPExcel->getActiveSheet()->getStyle('E'.($i-1).':F'.($i+1))->applyFromArray($styleArraySum);
		$objPHPExcel->getActiveSheet()->getStyle('E'.($i-1).':F'.($i+1))->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Czynności na rzecz klienta');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Czynnosci'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionChat($id) {
        $model = $this->findModel($id); 
        $member = false;
      
        return  $this->renderAjax('_chatAjax', [ 'model' => $model]);	

    }
    
    public function actionNote($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $note = new \backend\Modules\Accounting\models\AccNote();
        
        if (Yii::$app->request->isPost ) {		
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = CustomHelpers::decode($id);
            $note->type_fk = 1;
            $note->id_fk = $model->id;
            $note->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
                $modelArch = new \backend\Modules\Accounting\models\AccActionsArch();
                $modelArch->id_action_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                if(!$modelArch->save()) var_dump($modelArch->getErrors());
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else { 
                return array('success' => false, 'alert' => 'Komentarz nie może zostać dodany' );
            }
        }
        return array('success' => true );
    }
    
    public function actionEditable() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $table = false;
        if($_POST['type'] == 1) {
            $model = AccActions::findOne($_POST['id']);
            if($model->unit_time) {
                $model->timeH = intval($model->unit_time/60);
                $model->timeM = $model->unit_time - ($model->timeH*60);
            }
            if($_POST['field'] == 'unit_time') {
                $timeArr = explode(':', $_POST['value']);
                //$model->unit_time = $timeArr[0]*60+$timeArr[1]; 
                if( count($timeArr) == 2 ) {
                    $model->timeH = $timeArr[0];
                    $model->timeM = $timeArr[1];
                    
                    $H = ($model->timeH) ? $model->timeH*60 : 0;
                    $m = ($model->timeM) ? $model->timeM : 0;
                    $model->unit_time = $H + $m;
                }
                
                //$timeValidate = preg_match("/(2[0-3]|[01][0-9]):[0-5][0-9]/", $_POST['value']);
                $timeValidate = preg_match("/^\d+:[0-5][0-9]/", $_POST['value']);
                
                if(!$timeValidate) {
                    return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'errors' => ['unit_time' => 'Czas wykonania nie może być mniejszy od zera. Prosze wpisać czas wykonania w formacie h:min']];
                }
                
                if($model->unit_time <= 0 && $model->id_service_fk != 4) {
                    return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'errors' => ['unit_time' => 'Czas wykonania nie może być mniejszy od zera. Prosze wpisać czas wykonania w formacie h:min']];
                }
                
                $model->confirm_time = round(($model->unit_time/60), 2);  
                $model->acc_time = round(($model->unit_time/60), 2); 
                
                //if($model->is_confirm == 1)
                    $table = "#table-actions";
            }
        
            if($_POST['field'] == 'description') {
                if($model->id_service_fk == 4)
                    $model->acc_cost_description = $_POST['value'];
                else if($model->id_service_fk == 5 && $model->unit_price)
                    $model->description = $_POST['value'];
                else if($model->id_service_fk == 5 && !$model->unit_price)
                    $model->acc_cost_description = $_POST['value'];
                else
                    $model->description = $_POST['value'];
            }
        
            if($model->save()) {
                if($model->is_confirm == 1 && $_POST['field'] == 'unit_time') {
                    $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
                    $this->calculation($order, $model->acc_period);
                }
                return ['success' => true, 'alert' => 'Zmiany zostały zapisane', 'table' => $table];
            } else {
                return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'table' => $table, 'errors' => $model->getErrors()];
            }
        } else {
            $model = \backend\Modules\Task\models\CalTodo::findOne($_POST['id']);
            
            if($_POST['field'] == 'unit_time') {
                $timeArr = explode(':', $_POST['value']);
                if( count($timeArr) == 2 ) {
                    $model->timeH = $timeArr[0];
                    $model->timeM = $timeArr[1];
                    
                    $H = ($model->timeH) ? $model->timeH*60 : 0;
                    $m = ($model->timeM) ? $model->timeM : 0;
                    $model->execution_time = $H + $m;
                }
                $timeValidate = preg_match("/^\d+:[0-5][0-9]/", $_POST['value']);
                
                if(!$timeValidate) {
                    return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'errors' => ['unit_time' => 'Czas wykonania nie może być mniejszy od zera. Prosze wpisać czas wykonania w formacie h:min']];
                }
                
                if($model->execution_time <= 0) {
                    return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'errors' => ['unit_time' => 'Czas wykonania nie może być mniejszy od zera. Prosze wpisać czas wykonania w formacie h:min']];
                }
                //if($model->is_confirm == 1)
                    $table = "#table-actions";
            }
        
            if($_POST['field'] == 'description') {
                $model->description = $_POST['value'];
            }
            
            if($model->save()) {
                return ['success' => true, 'alert' => 'Zmiany zostały zapisane', 'table' => $table];
            } else {
                return ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'table' => $table, 'errors' => $model->getErrors()];
            }
        }
    }
    
    public function actionUncheck() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActionsSearch();
       // $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
       
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
           // $searchModel->date_from = $dateYear.'-'.$month.'-01';
           // $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
           // $searchModel->date_from = ($dateYear-1).'-12-01';
           // $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           // $searchModel->date_from = $dateYear.'-'.date('m').'-01';
           // $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
      /*  $searchModel->date_from = '2016-10-01';
        $searchModel->date_to = '2016-10-31';*/
        return $this->render('uncheck', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionDatauncheck() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		//$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        } 
        
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'a.unit_time';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'cc.name';
        if( isset($post['sort']) && $post['sort'] == 'cost' ) $sortColumn = 'a.acc_cost_net';
		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description",
					  "a.acc_cost_description as cdescription", "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "confirm_price", "acc_limit",
                      "confirm_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "concat_ws(' ', e.lastname, e.firstname) as ename", 
					  "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "cc.name as sname", "o.name as oname",
					  "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'a.id_set_fk = cc.id')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1] )->andWhere(' (id_invoice_fk = 0 or id_invoice_fk is null) ');
 
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_net else 0 end) as costs",
                      "sum(case when is_confirm=1 then confirm_time else 0 end) as confirm_time", 
                      "sum(case when acc_limit=0 then (confirm_time*unit_price) else ((confirm_time-acc_limit)*unit_price) end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1] )->andWhere(' (id_invoice_fk = 0 or id_invoice_fk is null) ');
    
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs'];

        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
		foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<div class="btn btn-xs btn-break" style="background-color:'.$value['type_color'].'">'.$value['oname'].'</div>' : '<span class="label bg-green"><i class="fa fa-plus"></i></span>';
            
            $periodAction = 'forward';
            $timeCounted = $value['confirm_time'];
            
            $value['acc_limit'] = (!$value['acc_limit']) ? 0 : $value['acc_limit'];
            
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            if( $value['id_service_fk'] == 4 ) {
                $tmp['price'] = 0;
            } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                $value['unit_price'] = 0;
                $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price'], 2).'</span>';
            } else {
                $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                $value['acc_limit'] = $value['acc_limit'] * 60;
                $timeH_l = intval($value['acc_limit']/60);
                $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price'], 2);
            }
            $tmp['amount'] = round( ($value['unit_price']*$timeCounted),2);
            $tmp['name'] = $value['description'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['ename'];
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
            
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['unit_time_min'] = $value['unit_time'];
            
            $tmp['service'] = $type;
            $tmp['description'] = htmlentities(($value['id_service_fk'] == 4) ? $value['cdescription'] : $value['description']);
            $tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" title="Przejdź do karty klienta">'.( ($value['csymbol'])?$value['csymbol']:$value['cname'] ). '</a>'; 
            $tmp['case'] = $value['sname'];             
            $tmp['className'] = ( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) ? 'warning' : 'default';
            if(!$value['id_invoice_fk']) {
                $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green" data-placement="left" data-toggle="tooltip" data-title="Czynność oznaczona do zafakturowania"></i>' : '<i class="fa fa-close text--orange" data-placement="left" data-toggle="tooltip" data-title=""Czynność nie oznaczona do zafakturowania></i>';
                if($periodAction == 'forward')
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                else
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
            } else {
                $tmp['order'] = $orderSymbol;
                $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" data-placement="left" data-toggle="tooltip" data-title="Czynność rozliczona i powiązana z fakturą" title="Czynność rozliczona i powiązana z fakturą"></i>';
                $tmp['period'] = '';
            }
			$tmp['cost'] = $value['acc_cost_net'];
            $tmp['confirm'] = (!$value['id_invoice_fk']) ? 0 : 1;
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields ];
	}
    
    public function actionUnsettle($id) {
        $model = new \frontend\Modules\Accounting\models\ActionsUnsettle();
        $id = CustomHelpers::decode($id);
        if($id == 0) $id = (isset($_GET['order'])) ? $_GET['order'] : 0;
		
		$model->id_order_fk = $id;
        
        if(!$id || $id == 0) {
            return '<div class="alert alert-info">Aby skorzystać z tej funkcjonalności musisz w filtrze wybrać zlecenie</div>';
        } else {
            $order = AccOrder::findOne($id);
			$model->actions = [];
            if( isset($_GET['ids']) && !empty($_GET['ids']) ) $model->actions = explode(',', $_GET['ids']);
            return  $this->renderAjax('_formUnsettle', [ 'model' => $model,  'id' => $id, 'order' => $order ]) ;	
        }
    }
    
    public function actionCunsettle() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
       // $id = CustomHelpers::decode($id);
       // $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
        
        
        $model = new \frontend\Modules\Accounting\models\ActionsUnsettle();
        $model->load(Yii::$app->request->post());
        
        $actionsData = AccActions::find()->where("id in (".implode(',', $model->actions).")")->one();
        $order = AccOrder::findOne($actionsData->id_order_fk);
        
        if(!$model->validate()) {
            return array('success' => false, 'html' => $this->renderAjax('_formUnsettle', [ 'model' => $model, 'order' => $order]), 'errors' => ['settle' => 'Problem z wycofaniem'] );	
        } else {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $sql = "update {{%acc_actions}} "
                        . " set is_confirm = 0, unit_price = 0, unit_price_native = 0, confirm_price = 0, confirm_price_native = 0, acc_price = 0, acc_price_native = 0,id_unit_rate_fk = 0, id_tax_fk = 0, acc_limit = 0 "
                        . " where type_fk != 5 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
				//echo $sql; exit;
                $result = Yii::$app->db->createCommand($sql)->execute();
                
                $sqlSpecial = "update {{%acc_actions}} "
                        . " set is_confirm = 0, unit_price_native = 0, confirm_price = 0, confirm_price_native = 0, acc_price = 0, acc_price_native = 0,id_unit_rate_fk = 0, id_tax_fk = 0 "
                        . " where type_fk = 5 and (id_invoice_fk = 0 or id_invoice_fk is null) and id in (".implode(',', $model->actions).")";
				//echo $sql; exit;
                $result = Yii::$app->db->createCommand($sqlSpecial)->execute();
                
                /*$sql = "select distinct acc_period as period from {{%acc_actions}} where id in (".implode(',', $model->actions).")";
                $periods = Yii::$app->db->createCommand($sql)->query();
                foreach($periods as $key => $period) {
                    $this->calculation($order, $period['period']);
                }*/
                $transaction->commit();
                return array('success' => true,  'action' => 'msettle', 'alert' => 'Czynności zostały wycofane', 'table' => '#table-actions', 'result' => $result );
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formUnsettle', [ 'model' => $model, 'order' => $order]), 'errors' => ['settle' => 'Problem z wycofaniem'] );	
            }
        }
    }
    
    public function actionGratis($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $model->user_action = 'gratis';
        $model->confirm_time = round($model->unit_time/60,2);
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_gratis = ($model->is_gratis) ? 0  : 1;
        $model->user_gratis = Yii::$app->user->id;
        $model->date_gratis = date('Y-m-d H:i:s');
		
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			
			return array('success' => true,  'action' => 'forward', 'alert' => 'Czynność została '.(($model->is_gratis) ? 'oznaczona' : 'odznaczona').' jako gratis', 'table' => '#table-actions'  );		
        } else {
            return array('success' => false,  'action' => 'forward', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-actions', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionMgratis($id) {
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $id = CustomHelpers::decode($id);
        if($id == 0) $id = $_GET['customer'];
        
        if(!$id || $id == 0) {
            return '<div class="alert alert-info">Aby skorzytsać z tej funkcjonalności musisz w filtrze wybrać klienta</div>';
        } else {
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            if($orders) $model->id_order_fk = $orders[0]['id'];
            $model->actions = [];
            if( isset($_GET['ids']) && !empty($_GET['ids']) ) $model->actions = explode(',', $_GET['ids']);
            return  $this->renderAjax('_formGratis', [ 'model' => $model, 'orders' => $orders, 'id' => $id ]) ;	
        }
    }
    
    public function actionCmgratis($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
        
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $model->load(Yii::$app->request->post());
        if(!$model->validate()) {
            return array('success' => false, 'html' => $this->renderAjax('_formGratis', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['settle' => 'Problem z oznaczeniem'] );	
        }
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
       
        if($order) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                 $sql = "update {{%acc_actions}} "
                    . " set is_gratis = 1, id_order_fk = ". $model->id_order_fk .", user_gratis = ".Yii::$app->user->id.", date_gratis = '".date('Y-m-d H:i:s')."', confirm_time = round(unit_time/60,2)" 
                    . " where id in (".implode(',', $model->actions).")";
                    Yii::$app->db->createCommand($sql)->execute();
                               
                $transaction->commit();
                return array('success' => true,  'action' => 'mgratis', 'alert' => 'Czynności zostały oznaczone jako gratis'  );		
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formGratis', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z oznaczeniem'] );	
            }
        } else {
            return array('success' => false, 'html' => $this->renderAjax('_formGratis', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['settle' => 'Proszę wybrać zlecenie'] );	
        }		
	}
    
    public function actionPriority($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        
        if(!$model->id_order_fk) {
            return array('success' => false,  'action' => 'priority', 'alert' => 'Operacja nie może zostać zrealizowana, ponieważ czynność nie jest przypisana do zlecenia', 'table' => '#table-actions', 'errors' => $model->getErrors()  );	
        }
        
        $model->user_action = 'priority';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_priority = ($model->is_priority) ? 0  : 1;
        $model->user_priority = Yii::$app->user->id;
        $model->date_priority = date('Y-m-d H:i:s');
        
        if($model->is_priority) {
            $model->rank_priority = AccActions::find()->where(['id_order_fk' => $model->id_order_fk, 'is_priority' => 1, 'status' => 1])->count()+1;
        } else {
            $sql = " UPDATE {{%acc_actions}} "
                ." SET rank_priority = rank_priority-1"
                ." WHERE status=1 and is_priority=1 and rank_priority > ".$model->rank_priority." and acc_period = '".$model->acc_period."' and id_order_fk=".$model->id_order_fk;
            \Yii::$app->db->createCommand($sql)->execute();
            $model->rank_priority = 0;
        }
		
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'action' => 'forward', 'alert' => 'Czynność została '.(($model->is_priority) ? 'oznaczona' : 'odznaczona').' jako priorytetowa', 'table' => '#table-actions'  );		
        } else {
            return array('success' => false,  'action' => 'forward', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-actions', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionMpriority($id) {
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $id = CustomHelpers::decode($id);
        if($id == 0) $id = $_GET['customer'];
        
        if(!$id || $id == 0) {
            return '<div class="alert alert-info">Aby skorzytsać z tej funkcjonalności musisz w filtrze wybrać klienta</div>';
        } else {
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            if($orders) $model->id_order_fk = $orders[0]['id'];
            $model->actions = [];
            if( isset($_GET['ids']) && !empty($_GET['ids']) ) $model->actions = explode(',', $_GET['ids']);
            return  $this->renderAjax('_formPriority', [ 'model' => $model, 'orders' => $orders, 'id' => $id ]) ;	
        }
    }
    
    public function actionCmpriority($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = CustomHelpers::decode($id);
        $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
        
        $model = new \frontend\Modules\Accounting\models\ActionsSettle();
        $model->load(Yii::$app->request->post());
        if(!$model->validate()) {
            return array('success' => false, 'html' => $this->renderAjax('_formPriority', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['settle' => 'Problem z oznaczeniem'] );	
        }
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($model->id_order_fk);
       
        if($order) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                //echo implode(',', $model->actions); exit;
                $sql = "SET @rank = 0; "
                        ." UPDATE {{%acc_actions}} as a JOIN (SELECT @rank := @rank + 1 AS rank, id FROM {{%acc_actions}} WHERE id in (".implode(',', $model->actions).") ORDER BY action_date) AS priority on priority.id=a.id "
                        ." SET rank_priority = priority.rank, is_priority = 1, id_order_fk = ". $model->id_order_fk .", user_priority = ".Yii::$app->user->id.", date_priority = '".date('Y-m-d H:i:s')."'"
                        ." WHERE a.id in (".implode(',', $model->actions).")";
                    \Yii::$app->db->createCommand($sql)->execute();
                
                //echo $sql; exit;
               
                $transaction->commit();
                return array('success' => true,  'action' => 'mpriority', 'alert' => 'Czynności zostały oznaczone jako priorytetowe'  );		
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_formPriority', [ 'model' => $model, 'orders' => $orders]), 'errors' => ['settle' => 'Problem z oznaczeniem'] );	
            }
        } else {
            return array('success' => false, 'html' => $this->renderAjax('_formPriority', [ 'model' => $model, 'orders' => $orders, 'id' => $id]), 'errors' => ['settle' => 'Proszę wybrać zlecenie'] );	
        }		
	}
    
    public function actionSpecial() {
       
		$model = new AccActions();
        $model->action_date = date('Y-m-d');
        $model->type_fk = 5;
        $model->id_service_fk = 5;
        $model->is_confirm = 1;
                             
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->unit_price = ($model->unit_price <= 0) ? '' : $model->unit_price;
            $model->acc_cost_net = ($model->acc_cost_net <= 0) ? '' : $model->acc_cost_net;
	        //$model->executionTime = $model->timeH*60 + $model->timeM;
                      
            if($model->validate() && $model->save()) {				
                $model->timeH = '';
				$model->timeM = '';
                $model->unit_price = '';
				$model->description = null;
				$model->id_customer_fk = null;
                $model->id_set_fk = null;
				$model->acc_cost_net = null;
				$model->acc_cost_description = null;
                //$stats = $this->personalStats($model->id_employee_fk, $model->timeDate);
                $stats = $this->specialStats($model->id_department_fk, $model->action_date);
                
                return array('success' => true, 'action' => 'create', 'close' => false, 
                             'alert' => 'Czynność specjalna została zarejestrowana', 'table' => '#table-actions', 'html' => $this->renderAjax('_formSpecial', [ 'model' => $model, 'stats' => $stats]) );	
			
            } else {
				$stats = $this->specialStats($model->id_department_fk, $model->action_date);
                return array('success' => false, 'table' => '#table-actions', 'html' => $this->renderAjax('_formSpecial', [ 'model' => $model, 'stats' => $stats]), 'errors' => $model->getErrors() );	
			}		
		} else {
			$stats = $this->specialStats($model->id_department_fk, $model->action_date);
            return  $this->renderAjax('_formSpecial', [ 'model' => $model, 'stats' => $stats]);	
        }
    }
    
    public static function specialStats($departmentId, $date)  {
		$stats['hours_per_day'] = 0;
		$stats['hours_per_month'] = 0;
		$stats['tasks'] = '';
        
        $hours_per_day_action = 0;
        $hours_per_month_action = 0;
        $hours_per_day_cost = 0;
        $hours_per_month_cost = 0;
        
        if(!$departmentId) $departmentId = 0;
		
		$sql = " select id_currency_fk, sum(case when action_date = '".$date."' then unit_price else 0 end) per_day, sum(unit_price) per_month, sum(case when action_date = '".$date."' then acc_cost_net else 0 end) per_day_cost, sum(acc_cost_net) per_month_cost "
                ." from law_acc_actions"
                ." where type_fk = 5 and  status = 1  ".(($departmentId) ? " and id_department_fk = ".$departmentId : '')." and date_format(action_date, '%Y-%m') = '".date('Y-m', strtotime($date))."' group by id_currency_fk";
        $dataStats = Yii::$app->db->createCommand($sql)->queryAll();//var_dump($dataStats);exit;
        foreach($dataStats as $key => $stat) {
            if($stat['id_currency_fk'] == 1) { $hours_per_day_action = $stat['per_day']; $hours_per_month_action = $stat['per_month']; $hours_per_day_cost = $stat['per_day_cost']; $hours_per_month_cost = $stat['per_month_cost'];} 
        }
		$stats['hours_per_day'] = '<small><i class="fa fa-tasks" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.number_format(($hours_per_day_action) ? $hours_per_day_action : 0, 2, ",", " ")
                                    .' / <i class="fa fa-money" data-toggle="tooltip" data-title="Koszty"></i>'.number_format(($hours_per_day_cost) ? $hours_per_day_cost : 0, 2, ",", " ").'</small>';
        $stats['hours_per_month'] = '<small><i class="fa fa-tasks" data-toggle="tooltip" data-title="Czynności na rzecz klienta"></i>'.number_format(($hours_per_month_action) ? $hours_per_month_action : 0, 2, ",", " ")
                                    .' / <i class="fa fa-money" data-toggle="tooltip" data-title="Koszty"></i>'.number_format(($hours_per_month_cost) ? $hours_per_month_cost : 0, 2, ",", " ").'</small>';
		/*if(count($tasks) == 0) {
			$stats['tasks'] = '<small class"alert alert-warning">brak czynności na wybrany dzień</small>';
		}*/
        
       /* if(!$departmentId) {
            $stats['tasks'] = 'Proszę wybrać dział';
        } else {*/
            $sqlTask = "select 1 as type_event, a.id, unit_time, a.description, c.symbol as symbol, c.name as cname, acc_cost_description, id_service_fk, is_confirm "
                        ." from {{%acc_actions}} a join {{%customer}} c on c.id=a.id_customer_fk"
                        ." where a.type_fk = 5 and a.status = 1  ".(($departmentId) ? " and id_department_fk = ".$departmentId : '')." and action_date = '".date('Y-m-d', strtotime($date))."' order by ifnull(symbol,cname)";
            $tasks = Yii::$app->db->createCommand($sqlTask)->queryAll();
            //var_dump($sqlTask);exit;
            foreach($tasks as $key => $task) {
                $cname = ($task['symbol']) ? $task['symbol'] : $task['cname'];
                if($cname) $cname = '<b>'.$cname.'</b> - ';
                if($task['id_service_fk'] == 4) $task['description'] = '<i class="text--yellow">'.$task['dcost'].'</i>';
                $actions = '';
                if(!$task['is_confirm']) {
                    $actions = '<a href="'.Url::to(['/task/personal/efast', 'id' => $task['id'], 'type' => 3]).'" class="icon-edit gridViewModal" data-target="#modal-grid-event" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil text--navy"></i></a>'
                            .'&nbsp;<a href="'.Url::to(['/task/personal/edelete', 'id' => $task['id'], 'type' => 3]).'" class="icon-delete deleteConfirm"  title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash text--red"></i></a>' ;
                }
                $stats['tasks'] .= '<li>'.$cname.$task['description'].' [<small class="text--red">'.(($task['acc_cost_description']?'Koszt: '.$task['acc_cost_description']:'brak kosztu')).'</small>] '
                                         .$actions                              
                                    .'</li>';
            }
            
            if(count($tasks) == 0) {
                $stats['tasks'] = '<small class"alert alert-warning">brak czynności na wybrany dzień</small>';
            }
       // }
		return $stats;
	}
    
    public function actionAccspecial($id) {
       // $id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        //$employeeId = ( isset($_GET['e']) && !empty($_GET['e']) ) ? $_GET['e'] : $this->module->params['employeeId'];        
        $timeStamp = date('Y-m-d');
        $period = date('Y-m');
        
        $stats['hours_per_day'] = 0;
        $stats['hours_per_month'] = 0;
		
		$tasksOl = '';
        
        if( isset($_GET['time']) && !empty($_GET['time']) ) {
            $timeStamp = date('Y-m-d', $_GET['time']);
            $period = date('Y-m', $_GET['time']);
        }
                  
        $stats = $this->specialStats($id, $timeStamp);
        return ['stats' => $stats];
    }
    
    public function actionSorting() {        
        $searchModel = new AccActionsSearch();
       // $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
       
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
           // $searchModel->date_from = $dateYear.'-'.$month.'-01';
           // $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
           // $searchModel->date_from = ($dateYear-1).'-12-01';
           // $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           // $searchModel->date_from = $dateYear.'-'.date('m').'-01';
           // $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
      /*  $searchModel->date_from = '2016-10-01';
        $searchModel->date_to = '2016-10-31';*/
        return $this->render('sorting', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
        ]);
    }
    
    public function actionDatasorting() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		//$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "a.id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        } 
        
        $sortColumn = 'rank_priority';
       		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description",
					  "a.acc_cost_description as cdescription", "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "confirm_price", "acc_limit",
                      "confirm_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "concat_ws(' ', e.lastname, e.firstname) as ename", 
					  "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "cc.name as sname", "o.name as oname",
					  "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'a.id_set_fk = cc.id')
            ->where( ['a.status' => 1, 'a.is_priority' => 1] )->andWhere(' (id_invoice_fk = 0 or id_invoice_fk is null) ');
 
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_net else 0 end) as costs",
                      "sum(case when is_confirm=1 then confirm_time else 0 end) as confirm_time", 
                      "sum(case when acc_limit=0 then (confirm_time*unit_price) else ((confirm_time-acc_limit)*unit_price) end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'is_priority' => 1] )->andWhere(' (id_invoice_fk = 0 or id_invoice_fk is null) ');
    
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs'];

        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
		foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<div class="btn btn-xs btn-break" style="background-color:'.$value['type_color'].'">'.$value['oname'].'</div>' : '<span class="label bg-green"><i class="fa fa-plus"></i></span>';
            
            $periodAction = 'forward';
            $timeCounted = $value['confirm_time'];
            
            $value['acc_limit'] = (!$value['acc_limit']) ? 0 : $value['acc_limit'];
            
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            if( $value['id_service_fk'] == 4 ) {
                $tmp['price'] = 0;
            } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                $value['unit_price'] = 0;
                $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price'], 2).'</span>';
            } else {
                $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                $value['acc_limit'] = $value['acc_limit'] * 60;
                $timeH_l = intval($value['acc_limit']/60);
                $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price'], 2);
            }
            $tmp['amount'] = round( ($value['unit_price']*$timeCounted),2);
            $tmp['name'] = $value['description'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['ename'];
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
            
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['unit_time_min'] = $value['unit_time'];
            
            $tmp['service'] = $type;
            $tmp['description'] = htmlentities(($value['id_service_fk'] == 4) ? $value['cdescription'] : $value['description']);
            $tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" title="Przejdź do karty klienta">'.( ($value['csymbol'])?$value['csymbol']:$value['cname'] ). '</a>'; 
            $tmp['case'] = $value['sname'];             
            $tmp['className'] = ( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) ? 'warning' : 'default';
            if(!$value['id_invoice_fk']) {
                $tmp['order'] = $orderSymbol;
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green" data-placement="left" data-toggle="tooltip" data-title="Czynność oznaczona do zafakturowania"></i>' : '<i class="fa fa-close text--orange" data-placement="left" data-toggle="tooltip" data-title=""Czynność nie oznaczona do zafakturowania></i>';
                if($periodAction == 'forward')
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                else
                    $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
            } else {
                $tmp['order'] = $orderSymbol;
                $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" data-placement="left" data-toggle="tooltip" data-title="Czynność rozliczona i powiązana z fakturą" title="Czynność rozliczona i powiązana z fakturą"></i>';
                $tmp['period'] = '';
            }
			$tmp['cost'] = $value['acc_cost_net'];
            $tmp['confirm'] = (!$value['id_invoice_fk']) ? 0 : 1;
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                $tmp['actions'] .= ( ($value['is_confirm'] == 0) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields ];
	}
       
    public function actionMsorting() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        if( isset($_POST['items']) && !empty($_POST['items'])) {

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach($_POST['items'] as $key => $item) {
                    $sql = "update {{%acc_actions}} "
                        . " set rank_priority = ".($key+1)
                        . " where id = ".$item['id'];
                    $result = Yii::$app->db->createCommand($sql)->execute();
                }           
                
                $transaction->commit();
                return array('success' => true,  'action' => 'sorting', 'alert' => 'Zmiany zostały zapisane', 'table' => '#table-actions'/*, 'result' => $result */);
            } catch (Exception $e) {
                $transaction->rollBack();
                return array('success' => false, 'alert' => 'Problem z realizacją zadania', 'errors' => ['sorting' => 'Problem z wycofaniem'] );	
            }
        } else {
            return array('success' => false, 'alert' => 'Brak czynności do zmian', 'errors' => ['sorting' => 'Brak czynności do zmian'] );
        }
    }
}
