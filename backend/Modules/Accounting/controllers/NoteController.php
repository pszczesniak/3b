<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccNote;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * NoteController implements the CRUD actions for AccActions model.
 */
class NoteController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccNote();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.notes'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }  
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

		//$fieldsDataQuery = $fieldsData = CalCase::find()->where(['status' => 1, 'type_fk' => 1]);
        $post = $_GET;
        if(isset($_GET['AccNote'])) { 
            $params = $_GET['AccNote']; 
            //\Yii::$app->session->set('search.discounts', ['params' => $params, 'post' => $post]);
            if( isset($params['id_order_fk']) && strlen($params['id_order_fk']) ) {
               array_push($where, "id_order_fk = ".$params['id_order_fk']);
            }
			if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "n.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_employee_from_fk']) && !empty($params['id_employee_from_fk']) ) {
                array_push($where, "n.id_employee_from_fk = ".$params['id_employee_from_fk']);
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "date_format(n.created_at, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
			if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "date_format(n.created_at, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
            if(isset($params['note']) && !empty($params['note']) ) {
                array_push($where, "lower(n.note) like '%".strtolower($params['note'])."%'");
            }
        }  	
        
        /*if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "id_customer_fk in ( select id_customer_fk from {{%company_department}} where id_department_fk in (".implode(',', $this->module->params['managers']).") )");
        }*/
			
		$fields = [];
		$tmp = [];
		
		$sortColumn = 'n.created_at';
        if( isset($post['sort']) && $post['sort'] == 'note' ) $sortColumn = 'n.note';
        if( isset($post['sort']) && $post['sort'] == 'date' ) $sortColumn = 'n.created_at';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'd.name';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'order' ) $sortColumn = 'o.name';
        if( isset($post['sort']) && $post['sort'] == 'period' ) $sortColumn = 'acc_period';
		
		$query = (new \yii\db\Query())
            ->select(['n.id as id',  'n.type_fk as type_fk', 'n.note as note', 'id_fk', 'acc_period', 'n.created_at as created_at', "concat_ws(' ', u.lastname, u.firstname) as uname"])
            ->from('{{%acc_note}} n')
			->join('JOIN', '{{%user}} u', 'u.id = n.created_by')
            ->where( ['n.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%acc_note}} n')
            ->where( ['n.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; 
 
		$tmp = [];
		$invoices = 0;
		foreach($rows as $key=>$value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['type'] = ($value['type_fk'] == 1) ? '<small class="text--purple">czynności</small>' : '<small class="text--pink">specyfikacji</small>' ;
            if($value['type_fk'] == 3) $tmp['type'] = '<small class="text--orange">zmiany w czynnościach oznaczonych</small>';
			$tmp['note'] = $value['note'];
			//$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['cname'].'</a>';
            //$tmp['order'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a>';
			$tmp['date'] = $value['created_at'];
			$tmp['creator'] = $value['uname'];
			//$tmp['invoice'] = ($value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['id_invoice_fk']]).'" title="Pokaż szczegóły rozliczenia"><i class="fa fa-calculator"></i></a>' : '';
            $tmp['actions'] = ($value['type_fk'] == 1 || $value['type_fk'] == 3) ? 
								'<a class="btn btn-sm bg-blue update" data-table="#table-notes" data-form="item-form" data-target="#modal-grid-item" href="'.Url::to(['/accounting/action/change', 'id' => $value['id_fk']]).'" data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>'
								: '<a class="btn btn-sm bg-teal" href="'.Url::to(['/accounting/action/index']).'?orderId='.$value['id_fk'].'&period='.$value['acc_period'].'" target="_blank" title="Przejdź do specyfikacji"><i class="fa fa-link"></i></a>';
			//$tmp['className'] = ($value['date_to']) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
}
