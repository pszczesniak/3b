<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccActions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * ReportController implements the CRUD actions for AccActions model.
 */
class ReportController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex($period)  {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActions();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
        if($period == '0-0') {
            $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t'); $month = $dateMonth; $year = $dateYear;
            
            if($dateDay <= 10 && $dateMonth > 1) {
                if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
                $searchModel->date_from = $dateYear.'-'.$month.'-01';
                $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            } else if($dateDay <= 10 && $dateMonth == 1) {
                $searchModel->date_from = ($dateYear-1).'-12-01';
                $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
            } else {
                $searchModel->date_from = $dateYear.'-'.date('m').'-01';
                $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            }
        } else {
            $periodArr = explode('-',$period);
            if($periodArr[0] == 0)
                $year = ( isset($_POST['year']) && !empty($_POST['year']) ) ? $_POST['year'] : date('Y');
            else
                $year = $periodArr[0];
           /* if($periodArr[1] < 10)
                $month = '0'.$periodArr[1];
            else*/
                $month = $periodArr[1];           
        }
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $month, 'year' => $year
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = []; $whereD = [];
        
        if( isset($_GET['year']) && !empty($_GET['year']) ) $year = $_GET['year']; else $year = 0;
        if( isset($_GET['month']) && !empty($_GET['month']) ) $month = $_GET['month']; else $month = 0;
        //if($month < 10) $month = '0'.$month;
        
        $period = $year.'-'.$month; 
		
		if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "laa.id_employee_fk = ".$params['id_employee_fk']);
            }
			if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "laa.id_order_fk = ".$params['id_order_fk']);
                array_push($whereD, "id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(laa.id_customer_fk = ".$params['id_customer_fk']." or laa.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");      
                array_push($whereD, "(id_customer_fk = ".$params['id_customer_fk']." or id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");                      
                //array_push($whereD, "id_customer_fk = ".$params['id_customer_fk']);
            }
		}
        $post = $_GET;
        $sortColumn = '3,1,2';
    
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = '2';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lce.lastname collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'hours' ) $sortColumn = '3';
        if( isset($post['sort']) && $post['sort'] == 'actions' ) $sortColumn = '4';
        if( isset($post['sort']) && $post['sort'] == 'costs' ) $sortColumn = '5';
		$sqlQuery = "SELECT 1 as type_fk, concat(lce.lastname, ' ', lce.firstname) as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, "
                        ." avg(id_tax_fk) as discount_id, sum(confirm_time) as time_hours, avg(confirm_price_native) as confirm_price, avg(acc_price_native) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_employee}} lce on lce.id=laa.id_employee_fk join {{%acc_order}} o on o.id=laa.id_order_fk join {{%customer}} lc on lc.id=o.id_customer_fk"
                        ." where laa.status = 1 and (is_confirm = 1 or is_gratis = 1) and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, concat(lce.lastname, ' ', lce.firstname) ";
        $sqlQuery .= " UNION ";
        $sqlQuery .= "SELECT 2 as type_fk, lce.symbol as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, "
                        ." avg(id_tax_fk) as discount_id, 0 as time_hours, sum(case when laa.type_fk=6 then (-1*unit_price_native) else unit_price_native end) as confirm_price, "
                        ." sum(case when laa.type_fk=6 then (-1*acc_price_native) else acc_price_native end) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_department}} lce on lce.id=laa.id_department_fk join {{%acc_order}} o on o.id=laa.id_order_fk join {{%customer}} lc on lc.id=o.id_customer_fk"
                        ." where laa.status = 1 and laa.type_fk in (5,6) and is_confirm = 1 and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, lce.symbol ";
        
		$sqlQuery .=" order by ".$sortColumn. ' '. (isset($post['order']) ? $post['order'] : 'asc');
       // echo $sqlQuery;exit;
        $data = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
        //$totalRow = $queryTotal->one();
        $sqlQueryTotal = "SELECT sum(confirm_time) as confirm_time, "
                            ."sum(case when (id_tax_fk = 0 or id_tax_fk is null) then confirm_price_native else acc_price_native end * confirm_time) as amount, "
                            ."sum(acc_cost_native) as costs  " 
                        ." FROM law_acc_actions laa join law_company_employee lce on lce.id=laa.id_employee_fk join law_customer lc on lc.id=laa.id_customer_fk "
                        ." WHERE laa.status = 1 and (is_confirm = 1 or is_gratis = 1) and acc_period = '".$period."' ";
	    $sqlQueryTotal .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
        $totalRow = Yii::$app->db->createCommand($sqlQueryTotal)->queryOne();
        
        $sqlQueryTotalSpecial = "SELECT sum(confirm_time) as confirm_time, "
                            ."sum(case when laa.type_fk=6 then (-1*unit_price_native) else unit_price_native end) as amount, "
                            ."sum(acc_cost_native) as costs  " 
                        ." FROM law_acc_actions laa left join law_company_employee lce on lce.id=laa.id_employee_fk join law_customer lc on lc.id=laa.id_customer_fk "
                        ." WHERE laa.status = 1 and  laa.type_fk in (5,6) and is_confirm = 1 and acc_period = '".$period."' ";
	    $sqlQueryTotalSpecial .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
        $totalRowSpecial = Yii::$app->db->createCommand($sqlQueryTotalSpecial)->queryOne();
        
        $sqlQueryDiscounts = "SELECT sum(discount_amount_native) as discount_amount FROM " ." {{%acc_discount}} "
                        ." where status = 1 and acc_period = '".$period."' ";
	    $sqlQueryDiscounts .= ( (count($whereD) > 0) ? ' and '.implode(' and ', $whereD) : '' );
        $discountRow = Yii::$app->db->createCommand($sqlQueryDiscounts)->queryOne();
        
        foreach($data as $key => $value) {
            $tmp['employee'] = $value['employee'];
            $tmp['hours'] = round($value['time_hours'],2);
            $tmp['customer'] = '<span class="cursor-pointer" title="'.$value['oname'].'">'. (($value['customer']) ? $value['customer'] : $value['customer'] ).'</span>';
            $tmp['order'] = '<span class="cursor-pointer" title="'.$value['oname'].'">'. $value['oname'] .'</span>';  
            $tmp['avg_rate'] = ($value['confirm_price'] != 0 && $value['time_hours'] == 0) ? 0 : number_format(round($value['confirm_price'],2), 2, "," ,  " "); 
            $tmp['avg_rate'] = ($value['type_fk'] == 1) ? $tmp['avg_rate'] : 0;
            $tmp['avg_rate_discount'] = ($value['confirm_price'] > 0 && $value['time_hours'] == 0) ? 0 : (number_format(round((($value['acc_price'] == 0) ? $value['confirm_price'] : $value['acc_price']),2), 2, "," ,  " ")); 
            $tmp['avg_rate_discount'] = ($value['type_fk'] == 1) ? $tmp['avg_rate_discount'] : 0;
            $tmp['actions'] = ($value['type_fk'] == 2) ? number_format($value['confirm_price'], 2, "," ,  " ") : ( number_format(round( ( ($value['acc_price'] == 0) ? $value['confirm_price'] : $value['acc_price'] ) * $value['time_hours'],2), 2, "," ,  " ")); 
            $tmp['costs'] = number_format(round($value['costs_net'], 2), 2, "," ,  " "); 
            $tmp['className'] = ($value['discount_id']>0) ? 'text--purple' : '';
            
            array_push($fields, $tmp); $tmp = [];
        }
       
		return ['total' => count($data), 'rows' => $fields, 
                'cost' => number_format(($totalRow['costs']+$totalRowSpecial['costs']), 2, "," ,  " "), 
                'time1' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 
                'amount' => number_format(($totalRow['amount']+$totalRowSpecial['amount']), 2, "," ,  " "), 
                'discounts' => number_format($discountRow['discount_amount'], 2, "," ,  " ")];
	}
    
    public function actionEmployees($period)   {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActions();
        $searchModel->status = 1;
        
        if($period == '0-0') {
            $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t'); $month = $dateMonth; $year = $dateYear;
            
            if($dateDay <= 10 && $dateMonth > 1) {
                if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
                $searchModel->date_from = $dateYear.'-'.$month.'-01';
                $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            } else if($dateDay <= 10 && $dateMonth == 1) {
                $searchModel->date_from = ($dateYear-1).'-12-01';
                $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
            } else {
                $searchModel->date_from = $dateYear.'-'.date('m').'-01';
                $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
            }
        } else {
            $periodArr = explode('-',$period);
            if($periodArr[0] == 0)
                $year = ( isset($_POST['year']) && !empty($_POST['year']) ) ? $_POST['year'] : date('Y');
            else
                $year = $periodArr[0];
           /* if($periodArr[1] < 10)
                $month = '0'.$periodArr[1];
            else*/
                $month = $periodArr[1];
            
        }
   
        return $this->render('employees', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $month, 'year' => $year
        ]);
    }
    
    public function actionEdata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
        if( isset($_GET['year']) && !empty($_GET['year']) ) $year = $_GET['year']; else $year = date('Y');
        if( isset($_GET['month']) && !empty($_GET['month']) ) $month = $_GET['month']; else $month = date('m');
        //if($month < 10) $month = '0'.$month;
        
        $period = $year.'-'.$month;
		
		if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                //array_push($where, "lce.id in (select id_employee_fk from {{%employee_department}} where id_department_fk = ".$params['id_department_fk'].")");
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "lce.id = ".$params['id_employee_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                array_push($where, "lce.status = ".$params['status']);
            }
		} else {    
            array_push($where, "lce.status = 1");
        }
        
		$sqlQuery = "SELECT lce.id as id,concat(lce.lastname, ' ', lce.firstname) as employee, avg(lce.status) as status, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) ) then unit_time else 0 end) as value_1, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) and id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_1_siw, "
                        ." sum(case when (is_confirm = 1 or is_gratis=1) then unit_time else 0 end) as value_2, "
                        ." sum(case when ((is_confirm = 1 or is_gratis=1) and id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_2_siw, "
                        //."select sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end) as personal_admin, " 
                        ."(select concat_ws('|', sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=7 then execution_time else 0 end)) "
                            ." from law_cal_todo where status = 1 and type_fk = 1 and date_format(todo_date,'%Y-%m') = '".$period."' and id_employee_fk = lce.id) as todo_info"
                        //." sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end) as personal_marketing, " 
                        //." sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end) as personal_holiday, " 
                        //." sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end) as personal_l4 " 
                        ." from {{%company_employee}} lce left join {{%acc_actions}} laa on ( lce.id=laa.id_employee_fk and laa.status = 1 and acc_period = '".$period."') "
                        ." where lce.status != -2  ".( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' )
                        ." group by lce.id, concat(lce.lastname, ' ', lce.firstname) "
                        ." order by lce.lastname collate `utf8_polish_ci`";
        //echo $sqlQuery; exit;
        $data = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
        foreach($data as $key => $value) {
            /*$todos = \backend\Modules\Task\models\CalTodo::find()->where(['id_employee_fk' => $value['id']])->andWhere("date_format(todo_date,'%Y-%m') = '".$period."'")->all();
            foreach($todos as $i => $todo) {
                if($todo->type_fk == 5) {
                    $tmp['hours_healt'] += round($todo->execution_time/60, 2);
                } else if($todo->type_fk == 4) {
                    $tmp['hours_holiday'] += round($todo->execution_time/60, 2); 
                } else if($todo->id_dict_todo_type_fk ==  15) {
                    $tmp['hours_marketing'] += round($todo->execution_time/60, 2); 
                } else {
                    $tmp['hours_admin'] += round($todo->execution_time/60, 2); 
                }
            }*/
            $tmp['employee'] = '<a href="'.Url::to(['/task/personal/chart', 'employee' => $value['id'], 'year' => $year]).'" title="Zestawienie czynności w ujęciu rocznym">'.$value['employee'].'</a>';
            //$tmp['hours'] = round($value['time_hours']/60,2);
            $todoInfo = explode('|', $value['todo_info']);
            $tmp['hours_admin'] = (isset($todoInfo[0])) ? round($todoInfo[0]/60,2) : 0;
            $tmp['hours_healt'] = (isset($todoInfo[3])) ? round($todoInfo[3]/60,2) : 0;
            $tmp['hours_marketing'] = (isset($todoInfo[1])) ? round($todoInfo[1]/60,2) : 0;
            $tmp['hours_holiday'] = (isset($todoInfo[2])) ? round($todoInfo[2]/60,2) : 0;
            $tmp['hours_day_off'] = (isset($todoInfo[4])) ? round($todoInfo[4]/60,2) : 0;
            $tmp['hours_1'] = round($value['value_1']/60,2).'<br /><small class="text--pink">w tym SIW: '.round($value['value_1_siw']/60,2).'</small>';
            $tmp['hours_2'] = round($value['value_2']/60,2).'<br /><small class="text--pink">w tym SIW: '.round($value['value_2_siw']/60,2).'</small>';
            $tmp['hours_all'] = round(($value['value_1']+$value['value_2'])/60,2).'<br /><small class="text--pink">w tym SIW: '.round(($value['value_1_siw']+$value['value_2_siw'])/60,2).'</small>';
            
            $tmp['className'] = ($value['status'] == -1) ? 'danger' : '';
            array_push($fields, $tmp); $tmp = [];
        }
       
		//return ['total' => count($data), 'rows' => $fields];
        return $fields;
	}

    public function actionExport($year, $month) {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                //'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
		/*if($month < 10) 
            $period = $_GET['year'].'-0'.$_GET['month'];
        else*/
            $period = $_GET['year'].'-'.$_GET['month'];
        
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Zestawienie wg średniej stawki za okres '.$period);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
            ->setCellValue('B3', 'Zlecenie')
            ->setCellValue('C3', 'Pracownik')
            ->setCellValue('D3', 'Ilość godzin')
            ->setCellValue('E3', 'Stawka średnia')
            ->setCellValue('F3', 'Stawka realna')
			->setCellValue('G3', 'Czynności merytoryczne')
            ->setCellValue('H3', 'Pozostały przychód');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = []; $where = [];
       
        if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "laa.id_employee_fk = ".$params['id_employee_fk']);
            }
			if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "laa.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(laa.id_customer_fk = ".$params['id_customer_fk']." or laa.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");      
            }
		}
		
		$sqlQuery = "SELECT 1 as type_fk, concat(lce.lastname, ' ', lce.firstname) as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, "
                        ." avg(id_tax_fk) as discount_id, sum(confirm_time) as time_hours, avg(confirm_price_native) as confirm_price, avg(acc_price_native) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_employee}} lce on lce.id=laa.id_employee_fk join {{%acc_order}} o on o.id=laa.id_order_fk join {{%customer}} lc on lc.id=o.id_customer_fk"
                        ." where laa.status = 1 and (is_confirm = 1 or is_gratis = 1) and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, concat(lce.lastname, ' ', lce.firstname) ";
        $sqlQuery .= " UNION ";
        $sqlQuery .= "SELECT 2 as type_fk, lce.symbol as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, "
                        ." avg(id_tax_fk) as discount_id, 0 as time_hours, sum(case when laa.type_fk=6 then (-1*unit_price_native) else unit_price_native end) as confirm_price, "
                        ." sum(case when laa.type_fk=6 then (-1*acc_price_native) else acc_price_native end) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_department}} lce on lce.id=laa.id_department_fk join {{%acc_order}} o on o.id=laa.id_order_fk join {{%customer}} lc on lc.id=o.id_customer_fk"
                        ." where laa.status = 1 and laa.type_fk in (5,6) and is_confirm = 1 and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, lce.symbol ";
        $sqlQuery .= " order by 3,1,2";
        $rows = Yii::$app->db->createCommand($sqlQuery)->query();

        foreach($rows as $record){ 
            $avg_rate = ($record['confirm_price'] != 0 && $record['time_hours'] == 0) ? 0 : round($record['confirm_price'],2); 
            $avg_rate = ($record['type_fk'] == 1) ? $avg_rate : 0;
            $avg_rate_discount = ($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? 0 : (round((($record['acc_price'] == 0) ? $record['confirm_price'] : $record['acc_price']),2)); 
            $avg_rate_discount = ($record['type_fk'] == 1) ? $avg_rate_discount  : 0;
            $actions = ($record['type_fk'] == 2) ? $record['confirm_price'] : ( round( ( ($record['acc_price'] == 0) ? $record['confirm_price'] : $record['acc_price'] ) * $record['time_hours'],2)); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['customer'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['oname'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['employee'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, ($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? 0 : round($record['time_hours'], 2)); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $avg_rate /*($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? 0 : round($record['confirm_price'],2)*/); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $avg_rate_discount /*($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? 0 : round($record['acc_price'],2)*/); 
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, "=F".$i."*D".$i /*$actions /*($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? $record['confirm_price'] : (round( ( ($record['discount_id']) ? $record['acc_price'] : $record['confirm_price']) * $record['time_hours'], 2))*/); 
            $objPHPExcel->getActiveSheet()->getStyle('G'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, round($record['costs_net'], 2)); 
                    
            $i++; 
        }  
	    if( count($rows) > 0 ) {
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i,'=SUM(D4:D'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
         
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i,'=SUM(G4:G'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('G'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('G'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('G'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i,'=SUM(H4:H'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('H'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('H'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('H'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
        }
        
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        //$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);

        $objPHPExcel->getActiveSheet()->setAutoFilter('A3:H3');
        
		$objPHPExcel->getActiveSheet()->setTitle('Zestawienie wg średniej stawki');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Zestawienie_wg_sredniej'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
	
	public function actionExportcsv($year, $month) {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                //'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
		/*if($month < 10) 
            $period = $_GET['year'].'-0'.$_GET['month'];
        else*/
            $period = $_GET['year'].'-'.$_GET['month'];
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A1', 'Klient')
            ->setCellValue('B1', 'Pracownik')
            ->setCellValue('C1', 'Ilość godzin')
			->setCellValue('D1', 'Czynności merytoryczne')
            ->setCellValue('E1', 'Pozostały przychód')
			->setCellValue('F1', 'Dział');
		//$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		/*$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A1:H1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setARGB('D9D9D9');	*/	
			
		$i=2; 
		$data = []; $where = [];
       
        if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "laa.id_employee_fk = ".$params['id_employee_fk']);
            }
			if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "laa.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "laa.id_customer_fk = ".$params['id_customer_fk']);
            }
		}
		
		$sqlQuery = "SELECT 1 as type_fk, concat(lce.firstname, ' ', lce.lastname) as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, cd.symbol as dsymbol, "
                        ." avg(id_tax_fk) as discount_id, sum(confirm_time) as time_hours, avg(confirm_price_native) as confirm_price, avg(acc_price_native) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_employee}} lce on lce.id=laa.id_employee_fk "
						." join {{%acc_order}} o on o.id=laa.id_order_fk join {{%customer}} lc on lc.id=o.id_customer_fk "
						." join {{%company_department}} cd on cd.id = laa.id_department_fk "
                        ." where laa.status = 1 and is_confirm = 1 and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, concat(lce.firstname, ' ', lce.lastname), cd.symbol ";
        $sqlQuery .= " UNION ";
		$sqlQuery .= "SELECT 2 as type_fk, lce.symbol as employee, ifnull(lc.symbol, lc.name) as customer, id_order_fk, min(o.name) as oname, cd.symbol as dsymbol, "
                        ." avg(id_tax_fk) as discount_id, 0 as time_hours, sum(case when laa.type_fk=6 then (-1*unit_price_native) else unit_price_native end) as confirm_price, "
                        ." sum(case when laa.type_fk=6 then (-1*acc_price_native) else acc_price_native end) as acc_price, sum(acc_cost_native) as costs_net "
                        ." FROM  {{%acc_actions}} laa join {{%company_department}} lce on lce.id=laa.id_department_fk join {{%acc_order}} o on o.id=laa.id_order_fk "
						." join {{%customer}} lc on lc.id=o.id_customer_fk "
						." join {{%company_department}} cd on cd.id = laa.id_department_fk "
                        ." where laa.status = 1 and laa.type_fk in (5,6) and is_confirm = 1 and acc_period = '".$period."' ";
        $sqlQuery .= ( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' );
		$sqlQuery .= " group by ifnull(lc.symbol, lc.name), id_order_fk, lce.symbol , cd.symbol";
        $sqlQuery .= " order by 3,1,2";
        $rows = Yii::$app->db->createCommand($sqlQuery)->query();
//var_dump($rows); echo '<br />';exit;
        foreach($rows as $record){ 
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, str_replace(',', ' ', $record['customer']) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['employee'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? 0 : round($record['time_hours'], 2)); 
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, ($record['confirm_price'] > 0 && $record['time_hours'] == 0) ? $record['confirm_price'] : (round( ( ($record['discount_id']) ? $record['acc_price'] : $record['confirm_price']) * $record['time_hours'], 2))); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, round($record['costs_net'], 2)); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['dsymbol']); 
                    
            $i++; 
        }  
	    /*if( count($rows) > 0 ) {
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i,'=SUM(D4:D'.($i-1).')'); 
         
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i,'=SUM(F4:F'.($i-1).')'); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i,'=SUM(G4:G'.($i-1).')'); 
        }*/
        

		/////--$i;
		/*$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setWrapText(true);

        $objPHPExcel->getActiveSheet()->setAutoFilter('A3:H3');*/
        
		$objPHPExcel->getActiveSheet()->setTitle('Wsad');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'wsad'.'_'.date("y_m_d__H_i_s").".csv";
		//$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = new \PHPExcel_Writer_CSV($objPHPExcel);
		$objWriter->setDelimiter(',');
		$objWriter->setEnclosure('');
		$objWriter->setLineEnding("\r\n");
		$objWriter->setSheetIndex(0);
        
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8' );
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionEexport($year, $month) {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                //'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
		
       /* if($month < 10) 
            $period = $_GET['year'].'-0'.$_GET['month'];
        else*/
            $period = $_GET['year'].'-'.$_GET['month'];
        
		$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:J2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozliczenie pracowników za okres '.$period);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Pracownik')
            ->setCellValue('B3', 'Godziny administracyjne')
            ->setCellValue('C3', 'L4')
            ->setCellValue('D3', 'Marketing')
			->setCellValue('E3', 'Urlop')
            ->setCellValue('F3', 'Dzień wolny')
            ->setCellValue('G3', 'Godziny niezafakturowane')
            ->setCellValue('H3', 'w tym SIW')
            ->setCellValue('I3', 'Godziny zafakturowane')
            ->setCellValue('J3', 'w tym SIW');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:J3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = []; $where = [];
        
        if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                //array_push($where, "lce.id in (select id_employee_fk from {{%employee_department}} where id_department_fk = ".$params['id_department_fk'].")");
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "lce.id = ".$params['id_employee_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                array_push($where, "lce.status = ".$params['status']);
            }
		}
		
		$sqlQuery = "SELECT lce.id as id,concat(lce.lastname, ' ', lce.firstname) as employee, avg(lce.status) as status, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) ) then unit_time else 0 end) as value_1, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) and id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_1_siw, "
                        ." sum(case when (is_confirm = 1 or is_gratis=1) then unit_time else 0 end) as value_2, "
                        ." sum(case when ((is_confirm = 1 or is_gratis=1) and  id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_2_siw, "
                        //."select sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end) as personal_admin, " 
                        ."(select concat_ws('|', sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=7 then execution_time else 0 end)) "
                            ." from law_cal_todo where status = 1 and type_fk = 1 and date_format(todo_date,'%Y-%m') = '".$period."' and id_employee_fk = lce.id) as todo_info"
                        //." sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end) as personal_marketing, " 
                        //." sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end) as personal_holiday, " 
                        //." sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end) as personal_l4 " 
                        ." from {{%company_employee}} lce left join {{%acc_actions}} laa on ( lce.id=laa.id_employee_fk and laa.status = 1 and acc_period = '".$period."') "
                        ." where lce.status >= -1  ".( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' )
                        ." group by lce.id, concat(lce.lastname, ' ', lce.firstname) "
                        ." order by lce.lastname collate `utf8_polish_ci`";
        
        $rows = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
        foreach($rows as $record){ 
            $todoInfo = explode('|', $record['todo_info']);
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['employee'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, ((isset($todoInfo[0])) ? round($todoInfo[0]/60,2) : 0) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ((isset($todoInfo[3])) ? round($todoInfo[3]/60,2) : 0) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, ((isset($todoInfo[1])) ? round($todoInfo[1]/60,2) : 0) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, ((isset($todoInfo[2])) ? round($todoInfo[2]/60,2) : 0) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, ((isset($todoInfo[4])) ? round($todoInfo[4]/60,2) : 0) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, round($record['value_1']/60,2));
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, round($record['value_1_siw']/60,2));
            $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, round($record['value_2']/60,2)); 
            $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, round($record['value_2_siw']/60,2));
                    
            $i++; 
        }  
	

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        //$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->getAlignment()->setWrapText(true);

        $objPHPExcel->getActiveSheet()->setAutoFilter('A3:J3');
        
		$objPHPExcel->getActiveSheet()->setTitle('Rozliczenie pracowników');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Rozliczenie pracowników'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
	
	public function actionEexportcsv($year, $month) {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                //'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
		
       /* if($month < 10) 
            $period = $_GET['year'].'-0'.$_GET['month'];
        else*/
            $period = $_GET['year'].'-'.$_GET['month'];
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A1', 'Pracownik')
			->setCellValue('B1', 'Wszystkie')
            ->setCellValue('C1', 'Godziny zafakturowane')
            ->setCellValue('D1', 'Godziny niezafakturowane')            
            ->setCellValue('E1', 'Marketing')
            ->setCellValue('F1', 'Godziny administracyjne')
            ->setCellValue('G1', 'Urlop')       
            ->setCellValue('H1', 'Dział');
	
		$i=2; 
		$data = []; $where = [];
        
        if(isset($_GET['AccActions'])) {
			$params = $_GET['AccActions'];
			if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                //array_push($where, "lce.id in (select id_employee_fk from {{%employee_department}} where id_department_fk = ".$params['id_department_fk'].")");
                array_push($where, "laa.id_department_fk = ".$params['id_department_fk']);
            }
			if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "lce.id = ".$params['id_employee_fk']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                array_push($where, "lce.status = ".$params['status']);
            }
		}
		
		$sqlQuery = "SELECT lce.id as id,concat(lce.firstname, ' ', lce.lastname) as employee, d.symbol as dname, avg(lce.status) as status, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) ) then unit_time else 0 end) as value_1, "
                        ." sum(case when ((is_confirm = 0 or is_confirm is null) and (is_gratis = 0 or is_gratis is null) and id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_1_siw, "
                        ." sum(case when (is_confirm = 1 or is_gratis=1) then unit_time else 0 end) as value_2, "
                        ." sum(case when ((is_confirm = 1 or is_gratis=1) and  id_customer_fk in  (395, 704)) then unit_time else 0 end) as value_2_siw, "
                        //."select sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end) as personal_admin, " 
                        ."(select concat_ws('|', sum(case when id_dict_todo_type_fk=1 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end), "
                                                ."sum(case when id_dict_todo_type_fk=7 then execution_time else 0 end)) "
                            ." from law_cal_todo where status = 1 and type_fk = 1 and date_format(todo_date,'%Y-%m') = '".$period."' and id_employee_fk = lce.id) as todo_info"
                        //." sum(case when id_dict_todo_type_fk=2 then execution_time else 0 end) as personal_marketing, " 
                        //." sum(case when id_dict_todo_type_fk=3 then execution_time else 0 end) as personal_holiday, " 
                        //." sum(case when id_dict_todo_type_fk=4 then execution_time else 0 end) as personal_l4 " 
                        ." from {{%company_employee}} lce " //join {{%employee_department}} ed on ed.id_employee_fk = lce.id join {{%company_department}} d on d.id = ed.id_department_fk "
						." left join {{%acc_actions}} laa on ( lce.id=laa.id_employee_fk and laa.status = 1 and acc_period = '".$period."') "
						." left join {{%company_department}} d on d.id = laa.id_department_fk "
                        ." where lce.status >= -1  ".( (count($where) > 0) ? ' and '.implode(' and ', $where) : '' )
                        ." group by lce.id, concat(lce.firstname, ' ', lce.lastname), dname "
                        ." order by lce.lastname collate `utf8_polish_ci`";
        
        $rows = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        $employee = '';
        foreach($rows as $key => $record){ 
            $todoInfo = explode('|', $record['todo_info']);
            //if($record['value_1'] > 0 || $record['value_2'] > 0 || (isset($todoInfo[0]) && $todoInfo[0] > 0) || (isset($todoInfo[1]) && $todoInfo[1] > 0) ) {
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['employee'] );
                $all = round($record['value_1']/60,2) + round($record['value_2']/60,2) 
					  + ( (isset($todoInfo[0]) ? round($todoInfo[0]/60, 2) : 0 ) ) + ( (isset($todoInfo[1]) ? round($todoInfo[1]/60, 2) : 0 ) ) + ( (isset($todoInfo[2]) ? round($todoInfo[2]/60, 2) : 0 ) );
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $all ); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, round($record['value_1']/60,2));
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, round($record['value_2']/60,2));
                if($employee != $record['employee']) {
					$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, ((isset($todoInfo[1])) ? round($todoInfo[1]/60,2) : 0) ); 
                    $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, ((isset($todoInfo[0])) ? round($todoInfo[0]/60,2) : 0) ); 
                    $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, ((isset($todoInfo[2])) ? round($todoInfo[2]/60,2) : 0) ); 
				} else {
					$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, 0 ); 
					$objPHPExcel->getActiveSheet()->setCellValue('F'. $i, 0 ); 
                    $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, 0 ); 
				}
				 
                if(!$record['dname']) {
					$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($record['id']);
					$record['dname'] = $employee->departments[0]['department']['symbol'];
				}
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['dname'] ); 
				
				$employee = $record['employee'];
						
				$i++; 
			//}
        }  

		--$i;
		
		$objPHPExcel->getActiveSheet()->setTitle('Wsad');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'wsad'.'_'.date("y_m_d__H_i_s").".csv";
		//$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = new \PHPExcel_Writer_CSV($objPHPExcel);
		$objWriter->setDelimiter(',');
		$objWriter->setEnclosure('');
		$objWriter->setLineEnding("\r\n");
		$objWriter->setSheetIndex(0);
        
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=utf-8' );
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
}
