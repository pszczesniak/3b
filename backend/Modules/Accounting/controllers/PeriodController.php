<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccPeriod;
use backend\Modules\Accounting\models\AccPeriodItem;
use backend\Modules\Accounting\models\AccPeriodArch;
use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccInvoice;
use backend\Modules\Accounting\models\AccOrderPeriod;
use backend\Modules\Accounting\models\AccOrderEmployee;
use backend\Modules\Crm\models\Customer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * PeriodController implements the CRUD actions for AccActions model.
 */
class PeriodController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()  {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccPeriod();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
          
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'stats' => $this->actionStats()
        ]);
    }
    
    public function actionStats() {
        $stats = [];
        
        //$sql = "select period_month, period_year, all_time, all_profit, all_cost from {{%acc_period}}  where period_year = 2017 order by period_month";
                //." where is_confirm = 1 and status = 1 and date_format(action_date, '%Y-%m') = '".$period."' and id_order_fk = ".$order->id;
		$sql = "select period_month, period_year, all_time, all_profit, all_cost from {{%acc_period}}"
			." where concat(period_year,'-',lpad(period_month,2,'0'),'-01') >= Date_add(Now(),interval - 12 month)"
			." order by period_year, period_month";
        $periodsData = Yii::$app->db->createCommand($sql)->query(); 
        $temp = [];
        foreach($periodsData as $key => $item) {
            $temp[$item['period_month']] = ['profit' => round($item['all_profit']/1000, 2), 
                                            'rate' => (($item['all_time']!=0) ? round((($item['all_time']) ? ($item['all_profit']/$item['all_time']) : 0), 2) : 0),
                                            'label' => strval($item['period_month']).'.'.strval($item['period_year'])];
        }
        
        /*for($i = 1; $i <= 12; ++$i) {
            if( !isset($temp[$i]) ) {
                $temp[$i] = ['profit' => 0, 'rate' => 0, 'label' => $i.'.2017'];
            }
        }*/
        $stats['profits'] = []; $stats['rates'] = []; $stats['labels'] = [];
        foreach($temp as $j => $value) {
            array_push($stats['profits'], $value['profit']);
            array_push($stats['rates'], $value['rate']);
            array_push($stats['labels'], $value['label']);
        }
        
        //$stats['rates'] = [250, 230, 284, 371.25, 180, 90, 236.25, 260, 270, 198, 264, 271.26];
        //$stats['profits'] = [1500, 1600,1500, 1600, 1500, 1600, 1500, 1600,1500, 1600, 1500, 1600];
        if( isset($_GET['ajax']) ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return $stats;
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = AccPeriod::find();
        
        if(isset($_GET['AccPeriod'])) { 
            $params = $_GET['AccPeriod']; 
            if( isset($params['status']) && strlen($params['status']) ) {
               // echo 'status: '.$params['status'];exit;
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status'] );
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$params['id_company_branch_fk'].")");
            }
            if(isset($params['id_resource_fk']) && !empty($params['id_resource_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk = ".$params['id_resource_fk']);
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
        }       
        $fieldsData = $fieldsDataQuery->orderby('period_year desc, period_month desc')->all();
			
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($fieldsData as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
                if($value->status == 1) 
                    $actionColumn .= '<a href="'.Url::to(['/accounting/period/close', 'id' => $value->id]).'" class="btn btn-xs bg-green deleteConfirm" data-label="Zamknij okres rozliczeniowy" data-table="#table-periods" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check\'></i>'.Yii::t('app', 'Potwierdź zamknięcie okresu').'" title="'.Yii::t('app', 'Zamknięcie okresu').'"><i class="fa fa-unlock"></i></a>';
                else if($value->status == 2)
                    $actionColumn .= '<a href="'.Url::to(['/accounting/period/open', 'id' => $value->id]).'" class="btn btn-xs bg-red deleteConfirm" data-label="Otwórz okres rozliczeniowy" data-table="#table-periods" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check\'></i>'.Yii::t('app', 'Potwierdź otwarcie okresu').'" title="'.Yii::t('app', 'Otwarcie okresu').'"><i class="fa fa-lock"></i></a>';
            }
            
            if($value->status == 1) {
                if($value->is_settled == 0 && in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) 
                    $actionColumn .= '<a href="'.Url::to(['/accounting/period/sendform', 'id' => $value->id]).'" class="btn btn-xs bg-blue gridViewModal" data-label="Roześlij kierownikom do weryfikacji" data-table="#table-periods" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-send\'></i>'.Yii::t('app', 'Rozesłanie informacji do kierowników').'" title="'.Yii::t('app', 'Rozesłanie informacji do kierowników').'"><i class="fa fa-send"></i></a>';

                $actionColumn .= '<a href="'.Url::to(['/accounting/period/genform', 'id' => $value->id]).'" class="btn btn-xs bg-pink gridViewModal" data-label="Wyślij żądanie wygenerowania faktur" data-table="#table-periods" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-cloud-download\'></i>'.Yii::t('app', 'Wyślij żądanie wygenerowania faktur').'" title="'.Yii::t('app', 'Wyślij żądanie wygenerowania faktur').'"><i class="fa fa-cloud-download"></i></a>';

                $actionColumn .= '<a href="'.Url::to(['/accounting/period/customers', 'id' => $value->id]).'" class="btn btn-xs bg-purple gridViewModal" data-label="Stan generowania rozliczeń" data-table="#table-periods" data-form="item-form" data-target="#modal-grid-item" data-tbuild="#table-settled" data-title="<i class=\'fa fa-calculator\'></i>'.Yii::t('app', 'Stan generowania rozliczeń').'" title="'.Yii::t('app', 'Stan generowania rozliczeń').'"><i class="fa fa-calculator"></i></a>';
            }        
            $actionColumn .= '</div>';
            
            $tmp['year'] = $value->period_year;
			$tmp['month'] = $value->period_month;
            $tmp['lump_sum'] = number_format($value->all_lump_sum, 2, "," ,  " ");
            $tmp['profit'] = number_format($value->all_profit, 2, "," ,  " ").'<br /><span class="text--pink">po rabacie: '.(number_format(($value->all_profit-$value->discount), 2, "," ,  " ")).'</span>';
            $tmp['time'] = $value->all_time;
            $tmp['discount'] = number_format($value->discount, 2, "," ,  " ");
            if($value->all_time != 0) {
                $tmp['avg'] = round((($value->all_time) ? ($value->all_profit/$value->all_time) : 0), 2).'<br /><span class="text--pink">'.round((($value->all_time) ? (($value->all_profit-$value->discount)/$value->all_time) : 0), 2).'</span>';
            } else {
                $tmp['avg'] = 0;
            }
            $tmp['cost'] = number_format($value->all_cost, 2, "," ,  " ");
            $tmp['invoices'] = count($value->invoices);
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
            $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
            $tmp['typeFormatter'] = 'period';
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

   
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
   
    /**
     * Finds the AccPeriod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccPeriod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = AccPeriod::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Okres rozliczeniowy o wskazanym identyfikatorze nie został odnaleziony w systemie.');
        }
    }
    
    public function actionClose($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = 2;
        //$model->user_action = 'lock';
        
       if($model->save()) {
           // $result = \backend\Modules\Company\models\EmployeeDepartment::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id); 
            //\backend\Modules\Task\models\CaseEmployee::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id);

            return array('success' => true, 'table' => '#table-periods', 'alert' => 'Okres rozliczeniowy <b>'.$model->period_year.'-'.$model->period_month.'</b> został zamknięty',  'index' => (isset($_GET['index'])) ? $_GET['index'] : 0);	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Okres rozliczeniowy <b>'.$model->period_year.'-'.$model->period_month.'</b> nie został zamknięty', );	
        }
    }
    
    public function actionOpen($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = 1;
        //$model->user_action = 'lock';
        
       if($model->save()) {
            //$result = \backend\Modules\Company\models\EmployeeDepartment::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id); 
            //\backend\Modules\Task\models\CaseEmployee::updateAll(['status' => 0], 'status = 1 and id_employee_fk = '.$model->id);

            return array('success' => true, 'table' => '#table-periods', 'alert' => 'Okres rozliczeniowy <b>'.$model->period_year.'-'.$model->period_month.'</b> został ponownie otwarty', 'index' => (isset($_GET['index'])) ? $_GET['index'] : 0);	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Okres rozliczeniowy <b>'.$model->period_year.'-'.$model->period_month.'</b> nie został ponownie otwarty', );	
        }
    }
    
    public function actionSendform($id) {
        $model = $this->findModel($id);
        
        $connection = Yii::$app->getDb();
        /*$command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_manager_fk, lastname, firstname '
                                               .' FROM {{%acc_actions}} a join {{%company_department}} cd on cd.id=a.id_department_fk join {{%company_employee}} ce on ce.id=id_employee_manager_fk'
                                               .' WHERE a.status = 1 and acc_period = :period '
                                               .' group by id_employee_manager_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);*/
        $command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_fk, lastname, firstname '
                                               .' FROM {{%acc_order_period}} ap join {{%company_employee}} ce on ce.id=ap.id_employee_fk'
                                               .' WHERE ap.status = 1 and ap.acc_period = :period '
                                               .' group by id_employee_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);
                                               
        $data = $command->queryAll();
        
        return  $this->renderAjax('_formSend', [ 'model' => $model, 'data' => $data ]) ;	
    }
    
    public function actionSend($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_fk, lastname, firstname '
                                               .' FROM {{%acc_order_period}} ap join {{%company_employee}} ce on ce.id=ap.id_employee_fk'
                                               .' WHERE ap.status = 1 and ap.acc_period = :period '
                                               .' group by id_employee_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);

        $data = $command->queryAll();
      
        if( count($data) == 0) {
            return ['success' => false, 'info' => 'Nie było żadnych informacji do wysłania'];
        } else {
            $periodArch = new AccPeriodArch();
            $periodArch->id_period_fk = $model->id;
            $periodArch->user_action = 'send';
            
            $managers = [];
            foreach($data as $key => $value) {
                array_push($managers, $value['id_employee_fk']);
                $periodItem = new AccPeriodItem();
                $periodItem->type_fk = 1;
                $periodItem->id_period_fk = $model->id;
                $periodItem->status = 0;
                $periodItem->id_manager_fk = $value['id_employee_fk'];
                if(!$periodItem->save()) { var_dump($periodItem->getErrors()); exit;}
            }
            
            $dataArch = ['text' => (isset($_POST['user-comment']) ? $_POST['user-comment'] : ''), 'managers' => $managers];
            $periodArch->data_arch = \yii\helpers\Json::encode($dataArch);
            $periodArch->save();
            
            $model->is_settled = 1;
            $model->settled_at = date('Y-m-d H:i:s');
            $model->settled_by = \Yii::$app->user->id;
            $model->save();
            
            foreach($model->items as $key => $item) {
                $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($item->id_manager_fk);
                if($item->status == 0) {
                    try { 
                        if(Yii::$app->params['env'] == 'prod') {
                            \Yii::$app->mailer->compose(['html' => 'accPeriodInfo-html', 'text' => 'accPeriodInfo-text'], ['model' => $periodArch])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                //->setTo(\Yii::$app->params['testEmail'])
                                ->setTo($employee->email) 
                                //->setCc(\Yii::$app->params['testEmail'])
                                ->setBcc('kamila_bajdowska@onet.eu')
                                ->setSubject('Prośba o weryfikację specyfikacji w systemie ' . \Yii::$app->name )
                                ->send();
                        } else {
                            \Yii::$app->mailer->compose(['html' => 'accPeriodInfo-html', 'text' => 'accPeriodInfo-text'], ['model' => $periodArch])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                ->setTo(['kamila_bajdowska@onet.eu'])
                                ->setSubject('DEV: Prośba o weryfikację specyfikacji w systemie ' . \Yii::$app->name . ' - '. $employee->email)
                                ->send();
                        }
                        $item->status = 1; $item->save();
                    } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                        $request = Yii::$app->request;
                        $log = new \common\models\Logs();
                        $log->id_user_fk = Yii::$app->user->id;
                        $log->action_date = date('Y-m-d H:i:s');
                        $log->action_name = 'period-send';
                        $log->action_describe = $e->getMessage();
                        $log->request_remote_addr = $request->getUserIP();
                        $log->request_user_agent = $request->getUserAgent(); 
                        $log->request_content_type = $request->getContentType(); 
                        $log->request_url = $request->getUrl();
                        $log->save();
                    }
                }
            }
            
            return ['success' => true, 'info' => 'Informacje zostały rozesłane', 'table' => '#table-periods'];
        }
    }
	
		public function actionGenform($id) {
        $model = $this->findModel($id);
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_manager_fk, lastname, firstname '
                                               .' FROM {{%acc_actions}} a join {{%company_department}} cd on cd.id=a.id_department_fk join {{%company_employee}} ce on ce.id=id_employee_manager_fk'
                                               .' WHERE a.status = 1 and acc_period = :period '
                                               .' group by id_employee_manager_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);

        $data = $command->queryAll();
        
        return  $this->renderAjax('_formGen', [ 'model' => $model, 'data' => $data ]) ;	
    }
	
	public function actionGenall($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        
        /*$connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_manager_fk, lastname, firstname '
                                               .' FROM {{%acc_actions}} a join {{%company_department}} cd on cd.id=a.id_department_fk join {{%company_employee}} ce on ce.id=id_employee_manager_fk'
                                               .' WHERE a.status = 1 and acc_period = :period '
                                               .' group by id_employee_manager_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);

        $data = $command->queryAll();*/
      
 
		$periodArch = new AccPeriodArch();
		$periodArch->id_period_fk = $model->id;
		$periodArch->user_action = 'generate';   
            
		$periodArch->save();
    
		return ['success' => true, 'info' => 'Twoje zgłoszenie zostało przyjęte. Po 22 zostaną wygenerowane brakujące i wysłane na Twój e-mail wszystkie faktury za okres '.($model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)), 'table' => '#table-periods'];
    }
    
    public function actionItems($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $html = '<table class="table-logs">';
        $html .= '<thead><tr><th>#</th><th>Status</th><th>Kierownik</th><th>Wysłano</th><th>Zaakceptowano</th><th>Komentarz</th></tr><thead><tbody>';
        
        $sendLog = AccPeriod::findOne($id);
        $items = $sendLog->items;
        if( count($items) == 0 ) {
            $html = '<div class="alert alert-info">Jeszcze nie wysłano żadnych informacji do kierowników</div>';
        } else {
			foreach($items as $key => $item) {
				$docs = [];
				
				$html .= '<tr><td>'.($key+1).'<td><i class="cursor-pointer fa '. ( ($item->status == 1) ? 'fa-send text--purple' : ( ($item->status == 0) ? 'fa-ban text--red' : 'fa-check text--green') ) .'" title="'.( ($item->status == 1) ? 'wysłano informacje i oczekiwanie na akceptację' : ( ($item->status == 0) ? 'błąd wysłania informacji' : 'zaakceptowane przez managera') ).'"></i></td>'
						.'<td>'.str_replace(',', '<br>', $item->manager['fullname']).'</td><td>'.$item->opened_at.'</td><td>'.$item->closed_at.'</td><td>'.$item->details.'</td></tr>';
			}
			
			$html .= '</tbody></table>';
		}		   
        return [ 'html' => $html ];
    }
    
    public function actionCustomers($id) {
        $id = CustomHelpers::decode($id);
        $model = AccPeriod::findOne($id);
        
        return  $this->renderAjax('_customers', [ 'model' => $model]);
    }
    
    public function actionCustomersdata($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $period = $model->period_year.'-'.( ($model->period_month < 10 ) ? '0'.$model->period_month : $model->period_month );
        
        $fields = []; $tmp = [];
        $connection = Yii::$app->getDb();
        /*$command = $connection->createCommand('SELECT id_customer_fk, c.name as name, c.symbol as symbol, count(*) AS total_actions '
                                               .' FROM {{%acc_actions}} a join {{%customer}} c on c.id=a.id_customer_fk'
                                               .' WHERE a.status = 1 and (id_invoice_fk is null or id_invoice_fk = 0) and is_confirm = 1 and acc_period = :period '
                                               .' group by id_customer_fk, c.name, c.symbol order by c.name', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);*/
        $sql = "select acc_period as acc_period, o.id as oid, o.name as oname, c.name as cname, c.symbol as csymbol, count(*) actions, max(a.id_invoice_fk) as iid, "
                ." (select discount_percent from {{%acc_discount}} where id_order_fk = a.id_order_fk and acc_period = a.acc_period and status = 1) as discount "
                ." from {{%acc_actions}} a join {{%acc_order}} o on o.id=a.id_order_fk join {{%customer}} c on c.id= o.id_customer_fk"
                ." where a.status=1 and a.is_confirm=1 and (id_invoice_fk=0 or id_invoice_fk is null) and a.acc_period = '".($model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month))."'"
                ." group by acc_period, o.id, o.name, c.name, c.symbol order by 1"; //and (id_invoice_fk=0 or id_invoice_fk is null)
        $command = $connection->createCommand($sql);
        $data = $command->queryAll();
        
        foreach($data as $key => $value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['confirm'] = 0;
			$tmp['disabled'] = ($value['iid']) ? 1 : 0;
            $tmp['customer'] = ($value['csymbol']) ? $value['csymbol'] : $value['cname'];
            $tmp['order'] = $value['oname'].( ($value['iid']) ? '<p class="text--red">Na ten okres została już wystawiona <a class="text--red" href="'.Url::to(['/accounting/invoice/view', 'id' => $value['iid']]).'" title="Przejdź do faktury">faktura </a></b></p>' : '');
            $tmp['discount'] = $value['discount'];
            $tmp['period'] = $period;
           // $invoices = AccInvoice::find()->where(['status' => 1, 'date_issue' => $period.'-01'])->all();
           // if($invoices == 0) {
              //  $tmp['invoice'] = '<a href="'.Url::to(['/accounting/invoice/generate', 'id' => $value['id_customer_fk']]).'?period='.$period.'&ajax=true" class="btn btn-xs bg-green deleteConfirm" data-table="#table-settlements" data-info="Czy na pewno chcesz wygenerować fakturę?" data-label="Generuj fakturę" title="'.Yii::t('lsdd', 'Generuj fakturę').'" >Generuj fakturę</a>';                    
            /*} else {
                $value = 0;
                $docs = '';
                foreach($invoices as $key => $invoice) {
                    $docs .= '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $invoice->id]).'" title="Otwórz w nowej karcie przeglądarki" target="_blank">'.$invoice->system_no.'</a>';
                }
                $tmp['invoice'] = $docs;
            } */
            $tmp['id'] = $value['oid'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		//return ['total' => $count,'rows' => $fields];
        return $fields;
    }
	
	public function actionSorders() {
		Yii::$app->response->format = Response::FORMAT_JSON;
			
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand('SELECT c.name as name, c.symbol as symbol, sum(round(ii.amount_net*i.rate_exchange,2)) AS amount_net '
                                               .' FROM {{%acc_invoice}} i join {{%acc_order}} o on o.id=i.id_order_fk join {{%customer}} c on c.id=o.id_customer_fk join {{%acc_invoice_item}} ii on ii.id_invoice_fk=i.id'
                                               .' WHERE i.status = 1 and ii.is_cost = 0 and i.date_issue >=  Date_add(Now(),interval - 12 month)'
                                               .' group by c.name, c.symbol order by 3 desc'
                                               .' limit 10' );

        $data = $command->queryAll();
		foreach($data as $key=>$value) {
            $tmp['customer'] = ($value['symbol']) ? $value['symbol'] : $value['name'];
            $tmp['amount'] = number_format($value['amount_net'], 2, "," ,  " ");
            $tmp['chart'] = '<span class="inlinesparkline">1,4,4,7,5,9,10</span>';
			
			array_push($fields, $tmp); $tmp = [];
		}
		/*$tmp['customer'] = 'MAHA';
		$tmp['all_time'] = '56';
		$tmp['amount'] = '17 098,00';
		$tmp['price'] = '256';
        $tmp['chart'] = "<span class=\"inlinesparkline\"  sparkType=\"line\" sparkBarColor=\"green\">1000.5,742.5,40.6,7,5,9,10</span>";
		array_push($fields, $tmp); $tmp = [];*/
		
		return $fields;
	}
    
	public function actionOrders() {
		$searchModel = new AccPeriod();
        
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
        
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
        } else if($dateDay <= 10 && $dateMonth == 1) {
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
        
        //$searchModel->acc_period = date('Y-m'); 
          
        return $this->render('orders', [
            'searchModel' => $searchModel,
        ]);
	}
    
    public function actionOrdersdata() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $where = []; $whereActions = ''; $joinActions = ''; $joinVerify = 'left join'; $whereVerify = "1=1";
       if(isset($_GET['AccPeriod'])) { 
            $params = $_GET['AccPeriod']; 
            if( isset($params['acc_period']) && strlen($params['acc_period']) ) {
                $accPeriod = $params['acc_period'];
            } else {
                $accPeriod = date('Y-m');
            }
            
            if( isset($params['status']) && strlen($params['status']) ) {
                if($params['status'] == 1) {
                    array_push($where, "i.id is not null" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                } else if($params['status'] == 2) {
                    array_push($where, "i.id is null" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                } else if($params['status'] == 3) {
                    //array_push($where, " EXISTS (select id from {{%acc_actions}} where status = 1 and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'join'; $whereActions = "1=1";
                } else if($params['status'] == 4) {
                    //array_push($where, " NOT EXISTS (select id from {{%acc_actions}} where status = 1 and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                    array_push($where, "a.id is null" );
                } else if($params['status'] == 5) {
                    array_push($where, "  EXISTS (select id from {{%acc_actions}} where status = 1 and (is_confirm = 1 or is_gratis=1) and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'join'; $whereActions = "1=1";//"is_confirm = 1 or is_gratis=1";
                }
            } else {
                $joinActions = 'left join'; $whereActions = "1=1";
            }
            
            if( isset($params['verify']) && strlen($params['verify']) ) {
                if($params['verify'] == 1) {
                    array_push($where, "v.id is null" );
                    $joinVerify = 'left join'; $whereVerify = "1=1";
                } else if($params['verify'] == 2) {
                    array_push($where, "v.id is not null" );
                    $joinVerify = ' join'; $whereVerify = "v.send_date is null";
                } else if($params['verify'] == 3) {
                    $joinVerify = 'join'; $whereVerify = "v.send_date is not null";
                } else if($params['verify'] == 4) {
                    $joinVerify = 'join'; $whereVerify = "v.is_accept = 1";
                    //array_push($where, "v.id is null" );
                } 
            } else {
                $joinVerify = 'left join'; $whereVerify = "1=1";
            }
            
            if( isset($params['type_order_fk']) && strlen($params['type_order_fk']) ) {
                array_push($where, "id_dict_order_type_fk = ".$params['type_order_fk'] );
            }
            if( isset($params['id_customer_fk']) && strlen($params['id_customer_fk']) ) {
                array_push($where, "o.id_customer_fk = ".$params['id_customer_fk'] );
            }
            
        } else {
            $accPeriod = date('Y-m');
        }      
        
        $fields = []; $tmp = [];
        $connection = Yii::$app->getDb();
        $post = $_GET;
        $sql = "select c.id as cid, c.symbol as csymbol, c.name as cname, o.id as oid, o.name as oname, type_symbol, type_color, type_name, min(i.id) as invoice, "
                ."(select count(*) from {{%acc_note}} where type_fk = 2 and id_fk=o.id and acc_period='".$accPeriod."') as notes, "
                ."(select count(*) from {{%acc_order_period}} where status = 1 and id_order_fk=o.id and id_employee_fk > 0 and acc_period='".$accPeriod."') as managers, "
                ."(select count(*) from {{%acc_order_period}} where is_accept=1 and status = 1 and id_order_fk=o.id and id_employee_fk > 0 and acc_period='".$accPeriod."') as managers_accept, "
				."(select count(*) from {{%acc_order_period}} where status = 1 and id_order_fk=o.id and id_employee_fk = 0 and acc_period='".$accPeriod."') as client, "
                ."(select count(*) from {{%acc_order_period}} where is_accept=1 and status = 1 and id_order_fk=o.id and id_employee_fk = 0 and acc_period='".$accPeriod."') as client_accept, "
                ."concat_ws('|', sum(case when(id_service_fk < 4 and is_confirm = 1) then unit_time else 0 end), sum(case when(id_service_fk < 4 and is_gratis = 1) then unit_time else 0 end), "
                                       ."sum(case when(id_service_fk < 4 and (is_gratis = 0 or is_gratis is null) and (is_confirm = 0 or is_confirm is null)) then unit_time else 0 end), "
                                       ."sum(case when(is_confirm = 1) then ifnull(acc_cost_net,0) else 0 end), "
                                       ."sum(case when((is_confirm = 0 or is_confirm is null)) then ifnull(acc_cost_net,0) else 0 end), "
                                       ."sum(case when(id_service_fk = 5 and is_confirm = 1) then unit_price when(id_service_fk = 6 and is_confirm = 1) then -1*unit_price else 0 end), "
                                       ."sum(case when(id_service_fk = 5 and (is_confirm = 0 or is_confirm is null)) then unit_price when(id_service_fk = 6 and (is_confirm = 0 or is_confirm is null)) then -1*unit_price else 0 end)) as actions"
                ." from {{%acc_order}} o join {{%acc_type}} at on at.id = o.id_dict_order_type_fk "
                    ." join {{%customer}} c on c.id=o.id_customer_fk "
                    .$joinActions." {{%acc_actions}} a on a.status = 1 and a.id_order_fk = o.id and acc_period = '".$accPeriod."' and (".$whereActions.") "
                    ." left join {{%acc_invoice}} i on (i.status = 1 and i.id_order_fk = o.id and date_format(date_issue, '%Y-%m') = '".$accPeriod."')"
                    .$joinVerify." {{%acc_order_period}} v on v.status = 1 and v.id_order_fk = o.id and v.acc_period = '".$accPeriod."' and (".$whereVerify.") "
                ." where o.status=1 ".(($where) ? " and ".implode(' and ', $where) : '')
                ." group by o.id, o.name, c.id, c.name, type_symbol, type_color, type_name";
                //." order by c.name";
//echo $sql; exit;

		$sqlCount = "select count(distinct o.id) as allRows "
                ." from {{%acc_order}} o "
                .$joinActions." {{%acc_actions}} a on a.status = 1 and id_order_fk = o.id and acc_period = '".$accPeriod."' and (".$whereActions.") "
                ." left join {{%acc_invoice}} i on (i.status = 1 and i.id_order_fk = o.id and date_format(date_issue, '%Y-%m') = '".$accPeriod."')"
                .$joinVerify." {{%acc_order_period}} v on v.status = 1 and v.id_order_fk = o.id and v.acc_period = '".$accPeriod."' and (".$whereVerify.") "
                ." where o.status=1 ".(($where) ? " and ".implode(' and ', $where) : '');
        $commandCount = $connection->createCommand($sqlCount);
        $count = $commandCount->queryOne()['allRows'];
		
		$sql .= " order by ifnull(c.symbol,c.name)"." limit ".(isset($post['offset']) ? $post['offset'] : 0 ).", ".(isset($post['limit']) ? $post['limit'] : 10); 
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		
        
        foreach($data as $key => $value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $actions = explode('|', $value['actions']); 
            $tmp['period'] = $accPeriod; 
            $tmp['customer'] = ($value['csymbol']) ? $value['csymbol'] : $value['cname'];
            $tmp['order'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'"  data-placement="left" data-toggle="tooltip" data-title="'.$value['type_name'].'">'.$value['oname']
                            .'<br /><span class="btn btn-xs btn-break" style="background-color:'.$value['type_color'].'">'.$value['type_symbol'].'</span></a>'
                            .'&nbsp;<a href="'. Url::to(['/accounting/action/index']).'?orderId='.$value['oid'].'&period='.$accPeriod.'" class="btn btn-xs bg-blue btn-flat" data-toggle="tooltip" "data-title="Przejdź do czynności"><i class="fa fa-tasks"></i></a>'
                            .(((isset($actions[0]) && !empty($actions[0])) || (isset($actions[1]) && !empty($actions[1])) || (isset($actions[3]) && !empty($actions[3] )) || (isset($actions[5]) && !empty($actions[5]))) ? '&nbsp;<a href="'. Url::to(['/accounting/order/exportbasic', 'id' => $value['oid'], 'period' => $accPeriod, 'save' => false]) .'&employees=1" class="btn btn-xs btn-default btn-flat export-file" data-toggle="tooltip" "data-title="Eksportuj do pliku Excel"><i class="fa fa-file-excel-o text--green"></i></a>' : '');
            $tmp['invoice'] = ($value['invoice']) ? 
                                    '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['invoice']]).'" data-placement="left" data-toggle="tooltip" data-title="Przejdź do faktury" class="btn btn-xs btn-icon bg-green"><i class="fa fa-calculator"></i>zobacz</a>' 
                                                 :
                                    '<a href="/accounting/invoice/generate/'.$value['oid'].'?period='.$accPeriod.'&gen=1" data-placement="left" data-toggle="tooltip" data-title="Wygeneruj fakturę" class="btn btn-xs btn-icon bg-blue deleteConfirm" data-table="#table-actions" data-label="Wygeneruj fakturę"><i class="fa fa-plus-square"></i>wystaw</a>';
           
            
            $tmp['actions_s'] = ((isset($actions[0]) && !empty($actions[0])) || (isset($actions[1]) && !empty($actions[1])) || (isset($actions[3]) && !empty($actions[3] )) || (isset($actions[5]) && !empty($actions[5]))) ? 
                                    '<small><i class="fa fa-clock-o text--blue"></i>&nbsp;'.round($actions[0]/60,2).'&nbsp;<i class="fa fa-gift text--pink"></i>'.round($actions[1]/60,2).'&nbsp;<i class="fa fa-money text--red"></i>&nbsp;'.round($actions[3],2).'&nbsp;<i class="fa fa-money text--orange"></i>&nbsp;'.round($actions[5],2).'</small>'
                                    .(((isset($actions[0]) && !empty($actions[0])) || (isset($actions[3]) && !empty($actions[3])) || (isset($actions[5]) && !empty($actions[5])) ) ?'<br /><a href="'.Url::to(['/accounting/action/allunsettle', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs btn-icon bg-red deleteConfirm" data-label="Wycofaj z rozliczenia" data-placement="left" data-toggle="tooltip" data-title="Kliknij aby'.(($value['invoice'])?' usunąć fakturę i ' : ' ').'wycofać czynności z rozliczenia"><i class="fa fa-rotate-left"></i>wycofaj </a>' : '') : 
                                    'brak czynności';
            $tmp['actions_nos'] = ((isset($actions[2]) && !empty($actions[2])) || (isset($actions[4]) && !empty($actions[4])) || (isset($actions[6]) && !empty($actions[6]))) ? 
                                    '<small><i class="fa fa-clock-o text--blue"></i>&nbsp;'.round($actions[2]/60,2).'&nbsp;<i class="fa fa-money text--red"></i>&nbsp;'.(isset($actions[4]) ? round($actions[4],2): 0).'&nbsp;<i class="fa fa-money text--orange"></i>&nbsp;'.(isset($actions[6]) ? round($actions[6],2): 0).'</small>'
                                    .'<br /><a href="'.Url::to(['/accounting/action/allsettle', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs btn-icon bg-purple deleteConfirm" data-label="Oznacz do rozliczenia" data-placement="left" data-toggle="tooltip" data-title="Kliknij aby oznaczyć czynności do rozliczenia"><i class="fa fa-square"></i>rozlicz </a>' : 
                                    'brak czynności';
            $tmp['note'] = '<a href="'.Url::to(['/accounting/period/chat', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
            $colorVerify = 'teal';
            if($value['managers'] > 0 && $value['managers'] == $value['managers_accept']) $colorVerify = 'green';
            if($value['managers'] > 0 && $value['managers'] > $value['managers_accept']) $colorVerify = 'yellow';
            $tmp['verify'] = '<a href="'.Url::to(['/accounting/period/bind', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-'.( ($value['managers']==0) ? 'lightgrey' : $colorVerify).' gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check-square\'></i>'.Yii::t('app', 'Weryfikacja przez pracowników').'" title="'.Yii::t('app', 'Weryfikacja przez pracowników').'"><i class="fa fa-check-square"></i></a>';
            
			$colorCVerify = 'teal';
            if($value['client'] > 0 && $value['client'] == $value['client_accept']) $colorCVerify = 'blue';
            if($value['client'] > 0 && $value['client'] > $value['client_accept']) $colorCVerify = 'orange';
            $tmp['cverify'] = '<a href="'.Url::to(['/accounting/period/cbind', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-'.( ($value['client']==0) ? 'lightgrey' : $colorCVerify).' gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check-square\'></i>'.Yii::t('app', 'Weryfikacja przez klienta').'" title="'.Yii::t('app', 'Weryfikacja przez klienta').'"><i class="fa fa-check-square"></i></a>';
            
            $tmp['id'] = $value['oid'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
        //return $fields;
    }
    
    public function actionChat($id, $period) {
        $id = CustomHelpers::decode($id);
        $model = \backend\Modules\Accounting\models\AccOrder::findOne($id); 
        $member = false;
        $notesData = \backend\Modules\Accounting\models\AccNote::find()->where(['status' => 1, 'id_fk' => $model->id, 'type_fk' => 2, 'acc_period' => $period])->orderby('id desc')->all();
        $notes = ($notesData) ? $notesData : [];
      
        return  $this->renderAjax('_chatAjax', [ 'model' => $model, 'notes' => $notes, 'period' => $period]);	

    }
    
    public function actionNote($id,$period) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = CustomHelpers::decode($id);
        $model = \backend\Modules\Accounting\models\AccOrder::findOne($id); 
        $note = new \backend\Modules\Accounting\models\AccNote();
        
        if (Yii::$app->request->isPost ) {		
			$note->load(Yii::$app->request->post());
            $note->id_parent_fk = $id;
            $note->type_fk = 2;
            $note->acc_period = $period;
            $note->id_fk = $model->id;
            $note->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
            if($note->save()) {
                $note->created_at = 'dodany teraz';
                
               /* $modelArch = new \backend\Modules\Accounting\models\AccActionsArch();
                $modelArch->id_action_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                if(!$modelArch->save()) var_dump($modelArch->getErrors());*/
                
                return array('success' => true, 'html' => $this->renderAjax('_note', [  'model' =>$note ]), 'alert' => 'Komentarz został dodany' );
            } else { 
                //var_dump($note->getErrors()); exit();
                return array('success' => false, 'alert' => 'Komentarz nie może zostać dodany' );
            }
        }
        return array('success' => true );
    }
    
    public function actionBind($id, $period) {
        $id = CustomHelpers::decode($id);
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($id);
        
        $changes = false;
        
		$model = new AccOrderPeriod(); 
		$model->status = 1;
		
        $model->id_customer_fk = ($order) ? $order->id_customer_fk : 0;
        $model->id_order_fk = ($order) ? $order->id : 0;
        $model->acc_period = $period;
		
		$replace = ["{%order}" => $order->name, "{%customer}" => $order->customer['name'], "{%period}" => $model->acc_period];
		$verifyText = strtr(Yii::$app->params['periodSendVerifyInfo'], $replace);
		$model->send_message = trim($verifyText);
        
        $managersData = AccOrderPeriod::find()->where(['status' => 1, 'id_order_fk' => $order->id])->andWhere("acc_period = '".$period."'")->all();
        $managers = []; $sendEmails = [];
        if($managersData) {
            $model->managers = [];
            foreach($managersData as $key => $value) {
                array_push($managers, $value->id_employee_fk);
                array_push($model->managers, $value->id_employee_fk);
            }
        }
        
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->id_employee_fk = ($model->managers) ? count($model->managers) : '';
            if($model->validate()) {
                if($model->managers) {
                    foreach($model->managers as $key => $value) {
                        $exist = \backend\Modules\Accounting\models\AccOrderPeriod::find()->where(['status' => 1, 'id_order_fk' => $order->id, 'id_employee_fk' => $value, 'acc_period' => $period])->one();
                        if(!$exist) {
                            $new = new AccOrderPeriod(); 
                            $new->status = 1;
                            $new->id_employee_fk = $value;
                            $new->id_customer_fk = ($order) ? $order->id_customer_fk : 0;
                            $new->id_order_fk = ($order) ? $order->id : 0;
                            $new->acc_period = $period;
							if($model->send) {
								$new->send_message = $model->send_message;
								$new->send_date = date('Y-m-d H:i:s');
							}
                            $new->save();
							$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($value);
							if($employee) array_push($sendEmails, $employee->email);
                            $changes = true;
                        } else{
							if(!$exist->send_date) {
								$employee = \backend\Modules\Company\models\CompanyEmployee::findOne($value);
								if($employee) array_push($sendEmails, $employee->email);
								$exist->send_message = $model->send_message;
								$exist->send_date = date('Y-m-d H:i:s');
								$exist->save();
							}
						}
                    }
                } else {
                    $model->managers = [];
                }
                foreach($managers as $key => $value) {
                    if(!in_array($value, $model->managers)) {
                        $periodItem = \backend\Modules\Accounting\models\AccOrderPeriod::find()->where(['status' => 1, 'id_employee_fk' => $value, 'id_order_fk' => $order->id, 'acc_period' => $period])->one();
                        $periodItem->status = -1;
                        $periodItem->deleted_by = Yii::$app->user->id;
                        $periodItem->deleted_at = date('Y-m-d H:i:s');
                        $periodItem->save();
                        $changes = true;
                    }
                }
				if($sendEmails && $model->send) {
					$filename = \Yii::$app->runAction('accounting/order/exportbasic', ['id' => CustomHelpers::encode($order->id), 'period' => $period, 'save' => true]);
					try { 
						if(Yii::$app->params['env'] == 'prod') {
						    $mail= \Yii::$app->mailer->compose(['html' => 'accVerifyInfo-html', 'text' => 'accVerifyInfo-text'], ['info' => $model->send_message])
								->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
								->setTo( $sendEmails)
								->setCc('m.gajewska@siw.pl')
                                ->setBcc(['kamila_bajdowska@onet.eu'])
								->setSubject('Prośba o weryfikację specyfikacji w systemie '. \Yii::$app->name );
							if(file_exists("/var/www/html/ultima/frontend/web/uploads/temp/".$filename)) { 
								$mail->attach('/var/www/html/ultima/frontend/web/uploads/temp/'.$filename);
							}
							$mail->send();
						} else {
							$mail = \Yii::$app->mailer->compose(['html' => 'accVerifyInfo-html', 'text' => 'accVerifyInfo-text'], ['info' => $model->send_message])
								->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
								->setTo(['kamila_bajdowska@onet.eu'])
								->setSubject('DEV: Prośba o weryfikację specyfikacji w systemie '. \Yii::$app->name.' '.implode(',', $sendEmails) );
							if(file_exists("F:/WebDir/projekty/lawfirm/frontend/web/uploads/temp/".$filename)) { 
								$mail->attach('F:/WebDir/projekty/lawfirm/frontend/web/uploads/temp/'.$filename);
							}
							$mail->send();
						}
					} catch (\Swift_TransportException $e) {
					//echo 'Caught exception: ',  $e->getMessage(), "\n";
						$request = Yii::$app->request;
						$log = new \common\models\Logs();
						$log->id_user_fk = Yii::$app->user->id;
						$log->action_date = date('Y-m-d H:i:s');
						$log->action_name = 'verify-send';
						$log->action_describe = $e->getMessage();
						$log->request_remote_addr = $request->getUserIP();
						$log->request_user_agent = $request->getUserAgent(); 
						$log->request_content_type = $request->getContentType(); 
						$log->request_url = $request->getUrl();
						$log->save();
					}
				}
				return array('success' => true,  'index' =>0, 'id' => $model->id, 'alert' => ($changes)?'Zmiany zostały zapisane'.(($model->send) ? ' a informacje rozesłane' : ''):'Nie wykonano żadnych modyfikacji', 'action' => 'periodManagerBind');	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formBind', [ 'model' => $model, 'peiod' => $period, 'order' => $order, 'history' => $managersData]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formBind', [ 'model' => $model, 'peiod' => $period, 'order' => $order, 'history' => $managersData]);	
        }
	}
	
	public function actionCbind($id, $period) {
        $id = CustomHelpers::decode($id);
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($id);
        
        $changes = false;
        
		$model = new AccOrderPeriod(); 
		$model->status = 1;
		
        $model->id_customer_fk = ($order) ? $order->id_customer_fk : 0;
        $model->id_order_fk = ($order) ? $order->id : 0;
        $model->acc_period = $period;
        $model->email = $order->customer['email'];
		
		$replace = ["{%order}" => $order->name, "{%period}" => $model->acc_period];
		$verifyText = strtr(Yii::$app->params['periodSendCustomerVerifyInfo'], $replace);
		$model->send_message = trim($verifyText);
        
        $customerData = AccOrderPeriod::find()->where(['status' => 1, 'id_order_fk' => $order->id, 'id_employee_fk' => 0])->andWhere("acc_period = '".$period."'")->one();
      
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->id_employee_fk = ($model->managers) ? count($model->managers) : '';
            if($model->validate()) {
                if(!$customerData) {
					$new = new AccOrderPeriod(); 
					$new->status = 1;
					$new->id_employee_fk = 0;
					$new->id_customer_fk = ($order) ? $order->id_customer_fk : 0;
					$new->id_order_fk = ($order) ? $order->id : 0;
					$new->acc_period = $period;
					$new->email = $model->email;

					$new->send_message = $model->send_message;
					$new->send_date = date('Y-m-d H:i:s');

					$new->save();
                    $changes = true;
					
					$filename = \Yii::$app->runAction('accounting/order/exportbasic', ['id' => CustomHelpers::encode($order->id), 'period' => $period, 'save' => 2]);
					try { 
						if(Yii::$app->params['env'] == 'prod') {
							$mail= \Yii::$app->mailer->compose(['html' => 'accVerifyInfoCustomer-html', 'text' => 'accVerifyInfoCustomer-text'], ['info' => $model->send_message])
								->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
								->setTo($model->email)
								//->setCc('m.gajewska@siw.pl')
                                ->setBcc('kamila_bajdowska@onet.eu')
								->setSubject('Prośba o weryfikację specyfikacji w systemie ' );
							if(file_exists("/var/www/html/ultima/frontend/web/uploads/temp/".$filename)) { 
								$mail->attach('/var/www/html/ultima/frontend/web/uploads/temp/'.$filename/*,  ['fileName' => $file['title']]*/);
							}
							$mail->send();
						} else {
							$mail = \Yii::$app->mailer->compose(['html' => 'accVerifyInfoCustomer-html', 'text' => 'accVerifyInfoCustomer-text'], ['info' => $model->send_message])
								->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
								->setTo(['kamila_bajdowska@onet.eu'])
								->setSubject('DEV: Prośba o weryfikację specyfikacji ' . \Yii::$app->name . ' - '. \Yii::$app->name.' '.$model->email );
							if(file_exists("C:/Dev/WebDir/projekty/lawfirm_prod/frontend/web/uploads/temp/".$filename)) { 
								$mail->attach('C:/Dev/WebDir/projekty/lawfirm_prod/frontend/web/uploads/temp/'.$filename/*,  ['fileName' => $file['title']]*/);
							}
							$mail->send();
						}
					} catch (\Swift_TransportException $e) {
					//echo 'Caught exception: ',  $e->getMessage(), "\n";
						$request = Yii::$app->request;
						$log = new \common\models\Logs();
						$log->id_user_fk = Yii::$app->user->id;
						$log->action_date = date('Y-m-d H:i:s');
						$log->action_name = 'verify-send';
						$log->action_describe = $e->getMessage();
						$log->request_remote_addr = $request->getUserIP();
						$log->request_user_agent = $request->getUserAgent(); 
						$log->request_content_type = $request->getContentType(); 
						$log->request_url = $request->getUrl();
						$log->save();
					}
                } else {
                    $changes = false;
                }

				return array('success' => true, 'id' => $model->id, 'alert' => ($changes)?'Zmiany zostały zapisane'.(($model->send) ? ' a informacje rozesłane' : ''):'Nie wykonano żadnych modyfikacji', 'action' => 'periodManagerBind');	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formCbind', [ 'model' => $model, 'peiod' => $period, 'order' => $order, 'history' => $customerData]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formCbind', [ 'model' => $model, 'peiod' => $period, 'order' => $order, 'history' => $customerData]);	
        }
	}
}
