<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccInvoiceItem;
use backend\Modules\Accounting\models\AccInvoice;
use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccOrderItem;
use backend\Modules\Crm\models\Customer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * InvoiceController implements the CRUD actions for AccActions model.
 */
class InvoiceController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex()  {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccInvoice();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.invoices');  
            if(isset($params['params'])) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
            }
            $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
        }
          
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = AccInvoice::find();
        
        $where = [];
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            \Yii::$app->session->set('search.invoices', ['params' => $params, 'post' => $post]);
            if( isset($params['status']) && strlen($params['status']) ) {
               // echo 'status: '.$params['status'];exit;
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status'] );
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['no']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$params['id_company_branch_fk'].")");
            }
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               // $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk = ".$params['id_resource_fk']);
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
            if( isset($params['id_customer_fk']) && strlen($params['id_customer_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "c.id = ".$params['id_customer_fk'] );
            }
        }  else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_issue, '%Y-%m-%d') >= '2016-10-01'");

        }     
       // $fieldsData = $fieldsDataQuery->all();
			
		$sortColumn = 'date_issue';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no', "date_format(date_issue, '%Y-%m') as date_issue", 'i.created_at as created_at', '(select sum(discount_amount) from {{%acc_discount}} where status = 1 and id_invoice_fk = i.id) as discount_amount', '(select sum(amount_net) from {{%acc_invoice_item}} ii where ii.id_invoice_fk = i.id) as amount_net', 'amount_gross', 'c.name as cname', 'c.id as cid'])
            ->from('{{%acc_invoice}} as i')
            //->join('JOIN', '{{%acc_order}} as o', 'o.id=i.id_order_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = i.id_order_fk')
            ->where( ['i.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/invoice/view/', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd faktury').'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['customer'] = $value['cname'];
            $tmp['no'] = $value['system_no'];
			$tmp['issue'] = $value['created_at'];
            $tmp['period'] = $value['date_issue'];
            $tmp['net'] = number_format ($value['amount_net'], 2, "," ,  " ");
            $tmp['discount'] = number_format ($value['discount_amount'], 2, "," ,  " ");
           // $tmp['gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}

    public function actionView($id) {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model] );
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/     
    }
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $model = $this->findModel($id); //->delete();
        $model->status = 0;
        $periodArr = explode('-', $model->date_issue); $period = $periodArr[0].'-'.$periodArr[1];
        
        $periodModel = \backend\Modules\Accounting\models\AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie może zostać usunięta ponieważ okres rozliczeniowy został zamknięty')  );
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        $transaction = \Yii::$app->db->beginTransaction(); 
        try {
            if($model->save()) {
                AccActions::updateAll(['id_invoice_fk' => 0],'status = 1 and id_invoice_fk = :invoice and is_confirm = 1', [':invoice' => $model->id]);
                $values = \backend\Modules\Accounting\models\AccPeriod::getValues($period);
                $sql = "update {{%acc_period}} "
                . " set all_time = ". ( ($values['inv_time']) ? $values['inv_time'] : 0) .", all_profit = ". ( ($values['inv_profit']) ? $values['inv_profit'] : 0) .", all_cost = ". ( ($values['inv_costs']) ? $values['inv_costs'] : 0)
                . " where period_year = ".$periodArr[0]." and period_month = ".($periodArr[1]*1);
                Yii::$app->db->createCommand($sql)->execute();
                
                $sqlUpdate = "update {{%acc_discount}} set id_invoice_fk = 0"
                            ." where id_invoice_fk = ".$model->id; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
                
                $transaction->commit();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            
            Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie może zostać usunięta')  );
        }
        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'delete';
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', );	
        } else {
            //var_dump($model->getErrors());
            return array('success' => false, 'alert' => 'Dane nie zostały usunięte', );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the AccInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        $id = CustomHelpers::decode($id);
		if (($model = AccInvoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Rozliczenie o wskazanym identyfikatorze nie zostało odnalezione w systemie.');
        }
    }
    
    public function actionCreate($id) {
        $id = CustomHelpers::decode($id);
        $model = Customer::findOne($id); 
        
        $sql = "select acc_period as period, count(*) actions from {{%acc_actions}} "
                ." where status=1 and is_confirm=1 and (id_invoice_fk=0 or id_invoice_fk is null) and id_customer_fk = ".$model->id
                ." group by acc_period order by 1";
        $periods = Yii::$app->db->createCommand($sql)->queryAll();
        return  $this->renderAjax('_formSettle', [ 'model' => $model, 'periods' => $periods, 'id' => $id ]) ;
    }
    
    public function actionGenerate($id) {
        $id = CustomHelpers::decode($id);
        $customer = Customer::findOne($id);
        
        $orders = AccOrder::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
        
        $periodArr = explode('-', $_GET['period']);
        
        $discounts = [];
        
        $periodRecord = \backend\Modules\Accounting\models\AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => ($periodArr[1]*1) ])->one();
        if(!$periodRecord) {
            $periodRecord = new \backend\Modules\Accounting\models\AccPeriod();
            $periodRecord->period_year = $periodArr[0];
            $periodRecord->period_month = $periodArr[1];
            $periodRecord->save();
        }
        
        $invoiceNumberSql = "select count(*) as fv_number from {{%acc_invoice}} where date_issue = '".($_GET['period'].'-01')."'";
        $invoiceNumberResult = Yii::$app->db->createCommand($invoiceNumberSql)->queryOne();
        $invoiceNumber = $invoiceNumberResult['fv_number']+1;
        
        $transaction = \Yii::$app->db->beginTransaction(); 
        try {
            $new = new AccInvoice(); $items = [];
            $new->id_order_fk = $id;
            $new->system_no = 'FV/'.$periodArr[0].'/'.$periodArr[1].'/'.$invoiceNumber;
            $new->real_no = 'FV/'.$periodArr[0].'/'.$periodArr[1].'/'.$invoiceNumber;
            $new->date_issue = $_GET['period'].'-01';
            $time = 0; $profit = 0; $costNet = 0;
            if(!$new->save()) {var_dump($new->getErrors()); exit;}
            foreach($orders as $key => $order) {
                
                $discountData = AccOrder::getDiscount($order->id, $_GET['period']);
                if($discountData) array_push($discounts, $discountData->id);
                
                $rate_constant = 0; $rate_hours_1 = 0; $rate_hours_2 = 0; $rate_way = 0;  $costs = [];
                $actions = AccActions::find()->where(['status' => 1, 'is_confirm' => 1, 'id_order_fk' => $order->id])->andWhere(" (id_invoice_fk = 0 or id_invoice_fk is null) and acc_period = '".$_GET['period']."'")->orderby('action_date')->all();
                $byHours = 0;
                foreach($actions as $key => $action) {
                    $time += $action->confirm_time; 
                    if($action->id_service_fk == 3) $rate_way += $action->confirm_time;
                    if($action->id_service_fk == 1) $rate_hours_1 += $action->confirm_time;
                    if($action->id_service_fk == 2) $rate_hours_2 += $action->confirm_time;
                    if($order->id_dict_order_type_fk == 2) {
                        $byHours += ( ($action->confirm_time-$action->acc_limit) * $action->unit_price );
                    }
                    if($action->acc_cost_net > 0) {
                        array_push($costs, ['order' => $order->id, 'value' => $action->acc_cost_net, 'description' => $action->acc_cost_description]);
                    }
                }
                
                if($order->id_dict_order_type_fk == 1 || $order->id_dict_order_type_fk == 2) {
                    $rate_constant = $order->rate_constatnt;
                    array_push($items, ['order' => $order->id, 'rate' => 0, 'value' => $order->rate_constatnt, 'description' => 'Stała opłata']);
                }
                if($order->id_dict_order_type_fk == 2 && $order->limit_hours < ($rate_hours_1 + $rate_hours_2 + $rate_way) ) {
                    //array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round(($rate_hours_1-$order->limit_hours)*$order->rate_hourly_1, 2), 'description' => 'Wg godzin']);
                    array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($byHours, 2), 'description' => 'Wg godzin']);
                }
                if($order->id_dict_order_type_fk == 3 && $rate_hours_1 > 0 ) {
                    array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($rate_hours_1*$order->rate_hourly_1,2), 'description' => 'Wg godzin - czynności merytoryczne']);
                }
                if($order->id_dict_order_type_fk == 4 && $rate_hours_1 > 0 ) {
                    $sql = "select id_employee_fk, sum(confirm_time) as limit_time from {{%acc_actions}} "
                            ." where is_confirm = 1 and status = 1 and id_service_fk = 1 and id_order_fk = ".$order->id
                            ." group by id_employee_fk"; 
                    $settleData = Yii::$app->db->createCommand($sql)->query();
                    foreach($settleData as $key => $employee) {
                        $hourly_1 = $order->rate_hourly_1;
                        if($order->id_dict_order_type_fk == 4) {
                            $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee['id_employee_fk'], $order->id_customer_fk, $order->id);
                            if($rateEmployee) $hourly_1 = $rateEmployee;
                        }
                        $employeeData = \backend\Modules\Company\models\CompanyEmployee::findOne($employee['id_employee_fk']);
                        array_push($items, ['order' => $order->id, 'rate' => 1, 'value' => round($hourly_1*$employee['limit_time'],2), 'description' => 'Wg godzin - czynności merytoryczne pracownika '.$employeeData->fullname]);
                    } 
                }
                if($order->id_dict_order_type_fk >= 3 && $rate_hours_2 > 0) {
                    array_push($items, ['order' => $order->id, 'rate' => 2, 'value' => round($rate_hours_2*$order->rate_hourly_2,2), 'description' => 'Wg godzin - czynności administracyjne']);
                }
                if($order->id_dict_order_type_fk >= 3 && $rate_way > 0) {
                    array_push($items, ['order' => $order->id, 'rate' => 2, 'value' => round($rate_way*$order->rate_hourly_way,2), 'description' => 'Przejazdy']);
                }
                
                AccActions::updateAll(['id_invoice_fk' => $new->id], "(id_invoice_fk = 0 or id_invoice_fk is null) and status = 1 and id_order_fk = :order and is_confirm = 1 and acc_period = '".$_GET['period']."'", [':order' => $order->id]);
            }    
            
            foreach($items as $key => $item) {
                $newItem = new AccInvoiceItem();
                $newItem->item_no = $key + 1;
                $newItem->id_invoice_fk = $new->id;
                $newItem->id_order_fk = $item['order'];
                $newItem->item_name = $item['description'];
                $newItem->amount_net = $item['value'];
                
                $profit += $item['value'];

                $newItem->save();
            }
            
            foreach($costs as $key => $cost) {
                $newItem = new AccInvoiceItem();
                $newItem->item_no = $key + 1;
                $newItem->id_invoice_fk = $new->id;
                $newItem->id_order_fk = $cost['order'];
                $newItem->item_name = $cost['description'];
                $newItem->amount_net = $cost['value'];
                $newItem->is_cost = 1;
                
                $costNet += $cost['value'];

                $newItem->save();
            }
            
            $periodRecord->all_time = $time + ((!empty($periodRecord->all_time)) ? $periodRecord->all_time : 0);
            $periodRecord->all_profit = $profit + ((!empty($periodRecord->all_profit)) ? $periodRecord->all_profit : 0);
            $periodRecord->all_cost = $costNet + ((!empty($periodRecord->all_cost)) ? $periodRecord->all_cost : 0);
            
            $periodRecord->save();
            
            if( count($discounts) > 0 ) {
                $sqlUpdate = "update {{%acc_discount}} set id_invoice_fk = ".$new->id
                            ." where id in (".implode(',', $discounts).")"; 
                Yii::$app->db->createCommand($sqlUpdate)->execute(); 
            }
            
            $transaction->commit();
            
            if( !isset($_GET['ajax']) ) {
                Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Faktura została wygenerowana')  );
                return $this->redirect(['view', 'id' => $new->id]);
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => true,  'action' => 'genInvoice', 'alert' => 'Faktura została wygenerowana', 'table' => '#table-settlements'  );
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            
            if( !isset($_GET['ajax']) ) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('app', 'Faktura nie została wygenerowana')  );
                return $this->redirect(['create', 'id' => $id]);	
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array('success' => false,  'action' => 'genInvoice', 'alert' => 'Faktura nie została wygenerowana', 'table' => '#table-settlements'  );
            }
        }
        
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1, 'id_order_fk' => $id] ); 
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActions'])) {
            $params = $post['AccActions']; 
            if(isset($params['description']) && !empty($params['description']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_employee_fk = ".$params['id_employee_fk']);
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("action_date >= '".$params['date_from']."'");
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("action_date <= '".$params['date_to']."'");
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
        } 
 
        //$count = $fieldsDataQuery->count();
        
        $sortColumn = 'action_date,id';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname'.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'is_confirm', "action_date", "unit_time", "unit_price", "acc_price",  "confirm_price", "confirm_time",  "acc_limit", "lastname", "firstname", "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "acc_cost_description"])
            ->from('{{%acc_actions}} a')
            ->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'id_invoice_fk' => $id] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
        $count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn. ' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
        $tmp = [];

        foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
          
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['name'] = $value['name'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['lastname'] .' '.$value['firstname'];
            $tmp['unit_time'] = number_format ($value['confirm_time'], 2, "," ,  " "); //round($value['unit_time']/60,2);
            $tmp['price'] = ($value['acc_limit'] == $value['confirm_time']) ? 0 : round($value['unit_price'], 2);
            $tmp['amount'] = round( ($value['unit_price']*($value['confirm_time']-$value['acc_limit'])),2);
            $tmp['in_limit'] = number_format ($value['acc_limit'], 2, "," ,  " ");
            $tmp['service'] = $type;
            $tmp['description'] = $value['description'];
            $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
            $tmp['cost'] = $value['acc_cost_net'];
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
		
		return ['total' => $count,'rows' => $fields];
	}
       
    public function actionExportinv($id) {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $employees = ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false );
        
        $model = $this->findModel($id);
        
        $objPHPExcel->setActiveSheetIndex(0);
        if($employees)
            $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        else
            $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozliczenie za okres '.(date('Y-m', strtotime($model->date_issue))));
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF0000CD');
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(38);
        //$objPHPExcel->getActiveSheet()->setCellValue('D1', \PHPExcel_Shared_Date::PHPToExcel( gmmktime(0,0,0,date('m'),date('d'),date('Y')) ));
        //$objPHPExcel->getActiveSheet()->getStyle('D1')->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
        //$objPHPExcel->getActiveSheet()->setCellValue('E1', '#12566');

        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Data');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Opis');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Czas');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Stawka');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Kwota');
        if($employees)
            $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Pracownik');
        $i = 3;
        foreach($model->actions as $key => $record){ 
            if($record->type_fk != 4) {
                //if(!$record->acc_limit) $record->acc_limit = 0;
                /*if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                    $record->unit_price = 0;
                } */
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->action_date ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, str_replace(array("\n", "\r"), '', $record->description)); 
                //$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->confirm_time); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->unit_price)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, round($record->unit_price*($record->confirm_time-$record->acc_limit), 2))->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
                if($employees)
                    $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->employee['fullname'] ); 
                $i++; 
            }
        }  
        $iData = $i-1;
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.($i-1))->getAlignment()->setWrapText(true);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:E'.($i-1))->getAlignment()->setWrapText(true);
       
        foreach($model->items as $key => $record){             
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record->amount_net)->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            
            if($record->is_cost == 1)
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->getFont()->getColor()->setARGB('FF0000');	  
            $i++; 
        } 
        
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);  
        $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, 'Razem'); 
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, '=SUM(E'.($iData+1).':E'.($i-1).')')->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFFFDAB9');
        
        ++$i;
        
        if($model->discount) {
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, 'Rabat'); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $model->discount)->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFE6E6FA');
            
            ++$i;
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);  
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, 'Do zapłaty'); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('B'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, '=E'.($i-2).'-E'.($i-1))->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('FFF5F5DC');
            
            ++$i;
        }

        // Set cell number formats
        //$objPHPExcel->getActiveSheet()->getStyle('E4:E'.($i-3))->getNumberFormat()->setFormatCode('#,##0.00');

        // Set column widths
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        
        // Set fonts
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setName('Candara');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setUnderline(\PHPExcel_Style_Font::UNDERLINE_SINGLE);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        //$objPHPExcel->getActiveSheet()->getStyle('D13')->getFont()->setBold(true);
        //$objPHPExcel->getActiveSheet()->getStyle('E13')->getFont()->setBold(true);

        // Set alignments
        /*$objPHPExcel->getActiveSheet()->getStyle('D11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('D12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('D13')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);*/

        $objPHPExcel->getActiveSheet()->getStyle('B5')->getAlignment()->setShrinkToFit(true);

        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
                'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
            ),
        );
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$iData)->applyFromArray($styleThinBlackBorderOutline);
        else
            $objPHPExcel->getActiveSheet()->getStyle('A3:E'.$iData)->applyFromArray($styleThinBlackBorderOutline);
        // Set thick brown border outline around "Total"
        $styleThickBrownBorderOutline = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => 'FF993300'),
                )
            ),
        );
        
        if($employees)
            $objPHPExcel->getActiveSheet()->getStyle('B'.($iData+1).':F'.($i-1))->applyFromArray($styleThinBlackBorderOutline);
        else
            $objPHPExcel->getActiveSheet()->getStyle('B'.($iData+1).':E'.($i-1))->applyFromArray($styleThinBlackBorderOutline);
        /*$objPHPExcel->getActiveSheet()->getStyle('D13:E13')->applyFromArray($styleThickBrownBorderOutline);*/

        // Set fills
       ///// $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
       ///// $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FF808080');

        // Set style for header row using alternative method
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.( ($employees) ? 'F' : 'E').'2')->applyFromArray(
                array(
                    'font'    => array(
                        'bold'      => true
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                        'top'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'fill' => array(
                        'type'       => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                        'rotation'   => 90,
                        'startcolor' => array(
                            'argb' => 'FFF5F5DC'
                        ),
                        'endcolor'   => array(
                            'argb' => 'FFFFFFFF'
                        )
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                        'left'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    )
                )
        );

        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray(
                array(
                    'borders' => array(
                        'right'     => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
        );

        // Unprotect a cell
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
       
        // Add a drawing to the worksheet
        $objDrawing = new \PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        if(\Yii::$app->params['env'] == 'prod') {
            $objDrawing->setPath('/var/www/html/ultima/frontend/web/images/logo_dark.jpg');
        } else {
            $objDrawing->setPath(\Yii::getAlias('@webroot').'/images/logo_dark.jpg');
        }
        //$objDrawing->setPath(Url::base(true).'/images/logo_dark.jpg');
        $objDrawing->setHeight(50);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


        // Play around with inserting and removing rows and columns
        $objPHPExcel->getActiveSheet()->insertNewRowBefore(6, 10);
        $objPHPExcel->getActiveSheet()->removeRow(6, 10);
        $objPHPExcel->getActiveSheet()->insertNewColumnBefore('E', 5);
        $objPHPExcel->getActiveSheet()->removeColumn('E', 5);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BInvoice&RPrinted on &D');
        $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename first worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Rozliczenie');

        // Create a new worksheet, after the default sheet
        /*$objPHPExcel->createSheet();

        // Llorem ipsum...
        $sLloremIpsum = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus eget ante. Sed cursus nunc semper tortor. Aliquam luctus purus non elit. Fusce vel elit commodo sapien dignissim dignissim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur accumsan magna sed massa. Nullam bibendum quam ac ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin augue. Praesent malesuada justo sed orci. Pellentesque lacus ligula, sodales quis, ultricies a, ultricies vitae, elit. Sed luctus consectetuer dolor. Vivamus vel sem ut nisi sodales accumsan. Nunc et felis. Suspendisse semper viverra odio. Morbi at odio. Integer a orci a purus venenatis molestie. Nam mattis. Praesent rhoncus, nisi vel mattis auctor, neque nisi faucibus sem, non dapibus elit pede ac nisl. Cras turpis.';

        // Add some data to the second sheet, resembling some different data types
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Terms and conditions');
        $objPHPExcel->getActiveSheet()->setCellValue('A3', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A4', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A5', $sLloremIpsum);
        $objPHPExcel->getActiveSheet()->setCellValue('A6', $sLloremIpsum);

        // Set the worksheet tab color
        $objPHPExcel->getActiveSheet()->getTabColor()->setARGB('FF0094FF');;

        // Set alignments
        $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getAlignment()->setWrapText(true);

        // Set column widths
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(80);

        // Set fonts
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('Candara');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setUnderline(\PHPExcel_Style_Font::UNDERLINE_SINGLE);

        $objPHPExcel->getActiveSheet()->getStyle('A3:A6')->getFont()->setSize(8);

      
        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        // Rename second worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Terms and conditions');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);*/
		
        $dateTmp = date('m.Y', strtotime($model->date_issue));
        
        if( isset($_GET['type']) && $_GET['type'] == 'pdf') {
            $filename = $model->customer['name'].'_'.$dateTmp.".pdf";
            
            $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
            //$rendererLibraryPath = dirname(__FILE__) . '/../../../../vendor/mpdf/mpdf';
            $rendererLibraryPath = \Yii::getAlias('@webroot').'/../../vendor/mpdf/mpdf';
            //echo $rendererLibraryPath;exit;
            
            if (\PHPExcel_Settings::setPdfRenderer( $rendererName,  $rendererLibraryPath  )) {

                $objWriterPDF = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
                $objWriterPDF->writeAllSheets();
                //$objWriterPDF->save("export/invoices/".$filename);
                
                header('Content-Type: application/pdf');
                //header('Content-Disposition: attachment;filename='.$filename .' ');
                header("Content-disposition: attachment; filename=\"".$filename."\""); 
                header('Cache-Control: max-age=0');			
                $objWriterPDF->save('php://output');
            }
        } else {
            $filename = $model->customer['name'].'_'.$dateTmp.".xlsx";
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }
	}
    
    public function actionExportsingle($id) {
		$objPHPExcel = new \PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $model = $this->findModel($id);
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rozliczenie za miesiąc '.date('Y-m', strtotime($model->date_issue)));
        $objPHPExcel->getActiveSheet()->setCellValue('A2', $model->customer['name']);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
       // $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'LP')
			->setCellValue('B3', 'Nazwa')
			->setCellValue('C3', 'Zlecenie')
			->setCellValue('D3', 'Kwota');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;	
        foreach($model->items as $key => $record){ 
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, ($key+1) ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record->item_name); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->order['type']['type_name']);   
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->amount_net)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00');
                    
            $i++; 
        }  
		
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Pozycje');
	    
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1); $sheet = 1;
        $objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		/*$objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);*/
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności na rzecz klienta');
       // $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
       // $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
       /* $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/
        
        $objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A2', 'Pracownik')
            ->setCellValue('B2', 'Data')
            ->setCellValue('C2', 'Czas trwania')
            ->setCellValue('D2', 'Stawka')
			->setCellValue('E2', 'Kwota')
			->setCellValue('F2', 'Opis czynności')
            ->setCellValue('G2', 'Typ czynności')
            ->setCellValue('H2', 'Sprawa')
            ->setCellValue('I2', 'Koszty')
            ->setCellValue('J2', 'Sposób rozliczenia');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A2:J2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=3; 
        
        foreach($model->actions as $key => $record){ 
            if(!$record->acc_limit) $record->acc_limit = 0;
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->employee['fullname'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record->action_date ); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, ($record->confirm_time-$record->acc_limit)); 
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->confirm_price)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, round($record->confirm_price*($record->confirm_time-$record->acc_limit), 2))->getStyle('E'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record->description); 
            $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $record->service['name']); 
            $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record->case['name']); 
            $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record->acc_cost_net)->getStyle('D'.$i)->getNumberFormat()->setFormatCode('# ##0.00'); 
            $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, $record->order['type']['type_symbol']); 
            //$objPHPExcel->getActiveSheet()->setCellValue('K'. $i, $record['type_name']); 
                    
            $i++; 
        }  
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Czynności na rzecz klienta');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
        //[nazwa klienta]spacja[miesiąc]kropka[rok]
        //$dataArr = explode('-', $model->date_issue);
        $dateTmp = date('m.Y', strtotime($model->date_issue));
        if( isset($_GET['type']) && $_GET['type'] == 'pdf') {
            $filename = $model->customer['name'].'_'.$dateTmp.".pdf";
            
            $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
            $rendererLibraryPath = dirname(__FILE__) . '/../../../../vendor/mpdf/mpdf';
            //echo $rendererLibraryPath;exit;
            
            if (\PHPExcel_Settings::setPdfRenderer( $rendererName,  $rendererLibraryPath  )) {

                $objWriterPDF = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
                $objWriterPDF->writeAllSheets();
                //$objWriterPDF->save("export/invoices/".$filename);
                
                header('Content-Type: application/pdf');
                //header('Content-Disposition: attachment;filename='.$filename .' ');
                header("Content-disposition: attachment; filename=\"".$filename."\""); 
                header('Cache-Control: max-age=0');			
                $objWriterPDF->save('php://output');
            }
        } else {
            $filename = $model->customer['name'].'_'.$dateTmp.".xlsx";
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //header('Content-Disposition: attachment;filename='.$filename .' ');
            header("Content-disposition: attachment; filename=\"".$filename."\""); 
            header('Cache-Control: max-age=0');			
            $objWriter->save('php://output');
        }

	}
    
    public function actionPrintpdf($id) {
        error_reporting(0); 
        $model = $this->findModel($id);
        
        $dateTmp = date('m.Y', strtotime($model->date_issue));
		$filename = $model->customer['name'].'_'.$dateTmp.".pdf";
        
        $mpdf = new \Mpdf\Mpdf();
		$mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Faktura');
        $mpdf->SetAuthor("Ultima Ratio");
        //$mPDF1->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        
        //$mPDF1->SetHeader('|KARTA PRODUKTU SYSTEMU LSDD|');
		$mpdf->SetTextColor(149, 41, 68); 
        $mpdf->SetHeader('|Rozliczenie za okres '.$dateTmp.'|');
        $mpdf->SetFooter('{PAGENO}|'.date("y-m-d H:i:s"));
		//Yii::$app->homeUrl
		$stylesheet = file_get_contents(\Yii::getAlias('@webroot').'/css/invoice.css');
		$mpdf->WriteHTML($stylesheet, 1);		
	
		$mpdf->WriteHTML($this->renderPartial('printpdf', array('model' => $model, 'employees' => ( (isset($_GET['employees']) && $_GET['employees'] == 1) ? true : false )), true));
        
		$send = ( isset($_GET['send']) ) ? 1 : 0;
        
		if($send) {
            $mpdf->Output(/*realpath(Yii::$app->basePath) . '/temp/'*/'temp/'.$filename, 'F');
			return $filename;
		} else {
            $mpdf->Output($filename, 'D');
			exit;
        }		
        //$mpdf->Output('test', 'D');
        
    }
    
    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Wykaz faktur');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
            ->setCellValue('B3', 'Numer')
            ->setCellValue('C3', 'Okres')
            ->setCellValue('D3', 'Data rozliczenia')
			->setCellValue('E3', 'Utworzył(a)')
            ->setCellValue('F3', 'Kwota');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->getStartColor()->setARGB('D9D9D9');	

		$i=4; 
		$data = []; $where = [];
        
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            if( isset($params['status']) && strlen($params['status']) ) {
               // echo 'status: '.$params['status'];exit;
                $fieldsDataQuery = $fieldsDataQuery->andWhere("status = ".$params['status'] );
            }
            if(isset($params['id_company_branch_fk']) && !empty($params['no']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk in (select id from {{%company_resources}} where id_company_branch_fk = ".$params['id_company_branch_fk'].")");
            }
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               // $fieldsDataQuery = $fieldsDataQuery->andWhere("id_resource_fk = ".$params['id_resource_fk']);
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') >= '".$params['date_from']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_from, '%Y-%m-%d') <= '".$params['date_to']."'");
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
            if( isset($params['id_customer_fk']) && strlen($params['id_customer_fk']) ) {
               // echo 'status: '.$params['status'];exit;
                array_push($where, "c.id = ".$params['id_customer_fk'] );
            }
        }  else {
            $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(date_issue, '%Y-%m-%d') >= '2016-10-01'");

        }     
       // $fieldsData = $fieldsDataQuery->all();
			
		$sortColumn = 'date_issue';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no', "date_format(date_issue, '%Y-%m') as date_issue", 'i.created_at as created_at', 'amount_net', 'amount_gross', 'c.name as cname', 'c.id as cid'])
            ->from('{{%acc_invoice}} as i')
            //->join('JOIN', '{{%acc_order}} as o', 'o.id=i.id_order_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = i.id_order_fk')
            ->where( ['i.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
        
        foreach($rows as $record){ 
           
            $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
            $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['system_no']); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['date_issue']); 
            $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['created_at']); 
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, ''); 
            $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['amount_net']); 
                    
            $i++; 
        }  
		

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Faktury');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Faktury'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}

}
