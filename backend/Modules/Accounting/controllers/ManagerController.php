<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccActions;
use backend\Modules\Accounting\models\AccActionsSearch;
use backend\Modules\Accounting\models\AccPeriod;
use backend\Modules\Accounting\models\AccPeriodItem;
use backend\Modules\Accounting\models\AccPeriodArch;
use backend\Modules\Accounting\models\AccOrderPeriod;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * ManagerController implements the CRUD actions for AccActions model.
 */
class ManagerController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccActionsSearch();
       // $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
        
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
        } else if($dateDay <= 10 && $dateMonth == 1) {
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
        
        if( isset($_GET['period']) && !empty($_GET['period']) ) {
            
            $period = AccPeriodArch::findOne($_GET['period']);
            if($period)
                $searchModel->acc_period = $period->period['period_year'].'-'.( (($period->period['period_month'] < 10) ? '0' : '').$period->period['period_month']);
        }
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
        $periods = \backend\Modules\Accounting\models\AccPeriodItem::find()->where(['id_manager_fk' => $this->module->params['employeeId']])->andWhere('status <= 1')->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'periods' => $periods
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "id_dict_order_type_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "(is_confirm = 1 or is_gratis = 1)");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0 and (is_gratis = 0 or is_gratis is null)");
                if($params['invoiced'] == 3) array_push($where, "is_confirm = 1 and (id_invoice_fk is not null and id_invoice_fk != 0)");
                if($params['invoiced'] == 4) array_push($where, "is_gratis = 1");
                if($params['invoiced'] == 5) array_push($where, "(a.id_order_fk is null or a.id_order_fk=0)");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        }
        
        //array_push($where, "a.id_department_fk in (".implode(',', $this->module->params['managers']).")");
        
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'ename collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'csname';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
			->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description", "is_gratis", "is_priority",
					  "a.acc_cost_description as cdescription", "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "confirm_price", "acc_limit",
                      "confirm_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "concat_ws(' ', e.lastname, e.firstname) as ename", 
					  "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_native", "cc.name as sname", "o.name as oname",
					  "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])      
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
			->join('LEFT JOIN', '{{%cal_case}} as cc', 'a.id_set_fk = cc.id')
            ->where( ['a.status' => 1] );
            
	    $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", "sum(acc_cost_native) as costs", "sum(confirm_time) as confirm_time", "sum(confirm_time*confirm_price) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs'];
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
		foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" data-placement="left" data-toggle="tooltip" data-title="'.$tmpType['name'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" data-placement="left" data-toggle="tooltip" data-title="'.$value['type_name'].'" title="'.$value['type_name'].'">'.$value['type_symbol'].'</span>' : '<span class="label bg-lightgrey" data-placement="left" data-toggle="tooltip" data-title="Brak przypisanego rozliczenia" title="Brak przypisanego rozliczenia"><i class="fa fa-ban text--red"></i></span>';
            
            $periodAction = 'forward';
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['type_action'] = 1;
            $tmp['price'] = round($value['confirm_price'], 2);
            $tmp['amount'] = round( ($value['confirm_price']*$value['confirm_time']),2);
            $tmp['name'] = ($value['type_fk'] == 4) ? $value['cdescription'] : $value['description'];
            $tmp['case'] = $value['sname'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['ename'];
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60);
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['service'] = $type;
            $tmp['description'] = $value['description'];
            $tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" title="Przejdź do karty klienta">'.( ($value['csymbol'])?$value['csymbol']:$value['cname'] ). '</a>'
                              .(($value['sname']) ? '<br /><small class="text--purple"><i class="fa fa-balance-scale"></i>'.$value['sname'].'</small>' : ''); 
            $tmp['className'] = ( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) ? 'warning' : 'default';
            $tmp['order'] = $orderSymbol;
            $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia" title="oznaczony do fakturowania"></i>' : (($value['is_gratis'] == 1) ? '<i class="fa fa-gift text--pink"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia jako gratis" title="gratis"></i>' : '<i class="fa fa-close text--orange"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność nie oznaczona do rozliczenia" title="nie podlega fakturowaniu"></i>');
            if($periodAction == 'forward')
				$tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
            else
				$tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';

			$tmp['cost'] = $value['acc_cost_native'];
            $tmp['confirm'] = $value['is_confirm'];
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                $tmp['actions'] .= ( ($value['is_confirm'] == 0 && !$value['is_gratis']) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');

                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields, 'cost' => number_format($costs, 2, "," ,  " "), 'time1' => number_format($summary, 2, "," ,  " "), 'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 'amount' => number_format($totalRow['amount'], 2, "," ,  " ")];
	}
    
    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności na rzecz klienta');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Pracownik')
            ->setCellValue('B3', 'Data')
            ->setCellValue('C3', 'Czas trwania')
            ->setCellValue('D3', 'Stawka')
			->setCellValue('E3', 'Kwota')
			->setCellValue('F3', 'Opis czynności')
            ->setCellValue('G3', 'Typ czynności')
            ->setCellValue('H3', 'Sprawa')
            ->setCellValue('I3', 'Koszty')
            ->setCellValue('J3', 'Klient')
            ->setCellValue('K3', 'Sposób rozliczenia')
            ->setCellValue('L3', 'Rozliczono do faktury');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:L3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->getStartColor()->setARGB('D9D9D9');		
			
		$i=4; 
		$data = []; $where = [];
		
		if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "id_dict_order_type_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "is_confirm = 1");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        }
        
        array_push($where, "a.id_department_fk in (".implode(',', $this->module->params['managers']).")");
        
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'ename collate `utf8_polish_ci`';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'cname';
        if( isset($post['sort']) && $post['sort'] == 'case' ) $sortColumn = 'csname';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'acc_cost_native', 'acc_cost_description', 'is_confirm', "confirm_time", "confirm_price", "action_date", "unit_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "cc.name as ccname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = a.id_set_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
            
	    $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time'];
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
			
		if( $count > 0 ) {
			foreach($rows as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['ename'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['action_date'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, round($record['unit_time']/60, 2)); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['confirm_price']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, round($record['confirm_price']*$record['confirm_time'], 2)); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['description']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $record['type_name']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['ccname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['acc_cost_native']); 
                $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('K'. $i, $record['type_name']); 
                $objPHPExcel->getActiveSheet()->setCellValue('L'. $i, (($record['is_confirm']) ? 'TAK' : 'NIE')); 
						
				$i++; 
			} 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i,'=SUM(C4:C'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i,'=SUM(E4:E'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('I'. $i,'=SUM(I4:I'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Czynności na rzecz klienta');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Czynnosci'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionAcceptform($id) {
        $id = CustomHelpers::decode($id);
        $model = \backend\Modules\Accounting\models\AccPeriodItem::findOne($id);
        
        /*$connection = Yii::$app->getDb();
        $command = $connection->createCommand('SELECT count(*) AS total_actions, id_employee_manager_fk, lastname, firstname '
                                               .' FROM {{%acc_actions}} a join {{%company_department}} cd on cd.id=a.id_department_fk join {{%company_employee}} ce on ce.id=id_employee_manager_fk'
                                               .' WHERE a.status = 1 and acc_period = :period '
                                               .' group by id_employee_manager_fk, lastname, firstname', [':period' => $model->period_year.'-'.( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month)]);

        $data = $command->queryAll();*/
        
        return  $this->renderAjax('_formAccept', [ 'model' => $model/*, 'data' => $data*/ ]) ;	
    }
    
    public function actionAccept($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$id = CustomHelpers::decode($id);
        $model = \backend\Modules\Accounting\models\AccPeriodItem::findOne($id);
        $model->status = 2;
		$model->closed_at = date('Y-m-d H:i:s');
		$model->details = isset($_POST['user-comment']) ? $_POST['user-comment'] : 'Brak dodatkowych informacji od managera';
        $model->save();
		
		$periodArch = new \backend\Modules\Accounting\models\AccPeriodArch();
		$periodArch->id_period_fk = $model->id;
		$periodArch->user_action = 'accept';
		$dataArch = ['text' => (isset($_POST['user-comment']) ? $_POST['user-comment'] : 'Brak dodatkowych informacji od managera')];
		$periodArch->data_arch = \yii\helpers\Json::encode($dataArch);
		$periodArch->save();
        
        if(Yii::$app->params['env'] == 'prod') {
            \Yii::$app->mailer->compose(['html' => 'accPeriodInfo-html', 'text' => 'accPeriodInfo-text'], ['model' => $periodArch])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                ->setTo(['m.gajewska@siw.pl'])
                //->setTo($employee->email) 
                ->setBcc('kamila_bajdowska@onet.eu')
                ->setSubject('Weryfikacja specyfikacji za okres '.($model->period['period_year'].'-'.$model->period['period_month']).' przez pracownika ' . $model->manager['fullname'])
                ->send();
        } else {
            \Yii::$app->mailer->compose(['html' => 'accPeriodInfo-html', 'text' => 'accPeriodInfo-text'], ['model' => $periodArch])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                ->setTo(['kamila_bajdowska@onet.eu'])
                ->setSubject('DEV: Weryfikacja czynności za okres '.($model->period['period_year'].'-'.$model->period['period_month']).' przez kierownika ' . $model->manager['fullname'])
                ->send();
        }
		
		return ['success' => true, 'info' => 'Informacja do działu księgowości została wysłana', 'table' => '#table-actions'];
    }
    
    public function actionSpecification() {
        if( \Yii::$app->session->get('user.isManagerD') != 1 ) {
            
			if(isset($_GET['orderId']) && isset($_GET['period']) && !empty($_GET['orderId']) && !empty($_GET['period'])) {
				//$existSql = "select count(*) from {{%acc_order_period}} ap where "
				//		." ap.acc_period='".$_GET['period']."' and ap.id_employee_fk = ".$this->module->params['employeeId']." and ap.id_order_fk = ".$_GET['orderId'];
				$exist = AccOrderPeriod::find()->where(['acc_period' => $_GET['period'], 'id_employee_fk' => $this->module->params['employeeId'], 'id_order_fk' => $_GET['orderId']])
				                               ->andWhere('status >= 0')->one();
			    if(!$exist) {
					throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
				}
			} else {			
				throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
			}
        }
        
        $searchModel = new AccActionsSearch();
        $searchModel->id_customer_fk = -1;
       // $searchModel->id_employee_fk = $this->module->params['employeeId'];
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
        
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
        } else if($dateDay <= 10 && $dateMonth == 1) {
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
		
		/*if($dateDay <= 10 && $dateMonth > 1) { 
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->date_from = $dateYear.'-'.$month.'-01';
            $searchModel->date_to = $dateYear.'-'.$month.'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        } else if($dateDay <= 10 && $dateMonth == 1) {
            $searchModel->date_from = ($dateYear-1).'-12-01';
            $searchModel->date_to = ($dateYear-1).'-12-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, ($dateYear-1)));
        } else {
            $searchModel->date_from = $dateYear.'-'.date('m').'-01';
            $searchModel->date_to = $dateYear.'-'.date('m').'-'.date('t', mktime(0, 0, 0, ($dateMonth-1), 1, $dateYear));
        }*/
        
        if( isset($_GET['period']) && !empty($_GET['period']) ) {
            
            $period = AccPeriodArch::findOne($_GET['period']);
            if($period)
                $searchModel->acc_period = $period->period['period_year'].'-'.( (($period->period['period_month'] < 10) ? '0' : '').$period->period['period_month']);
        }
        
        if( isset($_GET['set']) && !empty($_GET['set']) ) {
            $searchModel->acc_period = '';
            $searchModel->date_from = '';
            $searchModel->date_to = '';
            $set = \backend\Modules\Task\models\CalCase::findOne($_GET['set']);
            $searchModel->id_set_fk = $_GET['set'];
            $searchModel->id_customer_fk = $set->id_customer_fk;
        }
		
		if(isset($_GET['orderId'])) {
			$searchModel->id_order_fk = $_GET['orderId'];
			$order = \backend\Modules\Accounting\models\AccOrder::findOne($searchModel->id_order_fk);
			if($order)
				$searchModel->id_customer_fk = $order->id_customer_fk;
		}
        if(isset($_GET['period'])) $searchModel->acc_period = $_GET['period'];
        
        return $this->render('specification', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'manager' => $this->module->params['employeeId']
        ]);
    }
    
    public function actionSdata() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
		$fieldsDataQuery = AccActions::find()->where( ['status' => 1] );
        $post = $_GET;
        if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "a.id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
            } else /*{
                array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
            }*/
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                if($params['type_fk'] == 4)
                    array_push($where, "(a.type_fk = ".$params['type_fk']." or (acc_cost_net > 0))");
                else 
                    array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "(is_confirm = 1 or is_gratis = 1)");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0 and (is_gratis = 0 or is_gratis is null)");
                if($params['invoiced'] == 3) array_push($where, "is_confirm = 1 and (id_invoice_fk is not null and id_invoice_fk != 0)");
                if($params['invoiced'] == 4) array_push($where, "is_gratis = 1");
                if($params['invoiced'] == 5) array_push($where, "(a.id_order_fk is null or a.id_order_fk=0)");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
        } /*else {
            array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
        }*/
        //array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
        if(\Yii::$app->session->get('user.isManagerD') != 1 && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) {
			array_push($where, " (a.acc_period, a.id_order_fk) in (select acc_period, id_order_fk from {{%acc_order_period}} where status >= 0 and id_employee_fk = ".$this->module->params['employeeId'].")");
		}
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
			->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description", "is_gratis", "is_priority", "a.status as status",
					  "a.acc_cost_description as cdescription", "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "confirm_price", "acc_limit",
                      "confirm_time", "c.name as cname", "c.symbol as csymbol", "c.id as cid", "case when e.id is null then a.name else concat_ws(' ', e.lastname, e.firstname) end  as ename", 
					  "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_native", "cc.name as sname", "o.name as oname", "o.symbol as osymbol",
					  "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes", "a.id_department_fk as id_department_fk"])      
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
			->join('LEFT JOIN', '{{%cal_case}} as cc', 'a.id_set_fk = cc.id')
            ->where( '(a.status = 1 or (a.status = -1 and a.is_confirm = 1))'/*['a.status' => 1]*/ );
            
	    $queryTotal = (new \yii\db\Query())
             ->select(["count(*) as total_rows", "sum(case when a.status = 1 then round(unit_time/60,2) else 0 end) as unit_time", 
                      "sum(case when (a.status = 1) then acc_cost_native else 0 end) as costs",
                      "sum(case when (a.status = 1 and (is_confirm=1 or is_gratis=1)) then confirm_time else 0 end) as confirm_time", 
                      "sum(case when (a.status = 1 and a.type_fk=5) then ifnull(unit_price_native,unit_price) "
                             ." when (a.status = 1 and a.type_fk=6) then (-1*ifnull(unit_price_native, unit_price)) "
                             ." when (a.status = 1 and (acc_limit=0 or acc_limit is null)) then (confirm_time*ifnull(estimated_price,unit_price_native)) "
                             ." when (a.status = 1 and acc_limit>0 && acc_limit=confirm_time && o.id_dict_order_type_fk=2) then (confirm_time*estimated_price) "
                             ." when (a.status = 1 ) then ((confirm_time)*ifnull(estimated_price,unit_price_native)) else 0 end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( '(a.status = 1 )'/*['a.status' => 1]*/ );
            
        /*$queryOrders = (new \yii\db\Query())
            ->select(["a.id_order_fk", "min(case when o.id_currency_fk = 1 then rate_constatnt "
                                              ." when (o.id_currency_fk != 1 and a.id_invoice_fk is not null and a.id_invoice_fk != 0) then round(rate_constatnt*i.rate_exchange,2) "
                                              ." else round(rate_constatnt*(select rate_value from {{%acc_exchange_rates}} er where er.id_currency_fk=o.id_currency_fk and is_active=1), 2)  end) as rate_constatnt"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
             ->join('LEFT JOIN', '{{%acc_invoice}} as i', 'i.id = a.id_invoice_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1 ] )->andWhere('acc_limit_price=0 or acc_limit_price is null')
            ->groupby(['a.id_order_fk']);*/
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
            //$queryOrders = $queryOrders->andWhere(implode(' and ',$where));
        }
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
            
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs']; 
        
        $totalRow['lump'] = 0; $totalRow['all'] = $costs+$totalRow['amount']; 
        /*$totalOrders = $queryOrders->all(); 
        foreach($totalOrders as $io => $order) {
            $totalRow['lump'] += $order['rate_constatnt'];
        }
        $totalRow['amount'] += $totalRow['lump'];*/
        
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
		foreach($rows as $key=>$value) {
            $tmpType = $dict[$value['type_fk']];
            $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].'" data-placement="left" data-toggle="tooltip" data-title="'.$tmpType['name'].'" title="'.$tmpType['name'].'"></i>';
            $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" data-placement="left" data-toggle="tooltip" data-title="'.$value['type_name'].'" title="'.$value['type_name'].'">'.$value['oname'].'</span>' : '<span class="label bg-lightgrey" data-placement="left" data-toggle="tooltip" data-title="Brak przypisanego rozliczenia" title="Brak przypisanego rozliczenia"><i class="fa fa-ban text--red"></i></span>';
            
            $periodAction = 'forward';
            if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
            //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['type_action'] = 1;
            $tmp['price'] = round($value['confirm_price'], 2);
            $tmp['amount'] = round( ($value['confirm_price']*$value['confirm_time']),2);
            $tmp['name'] = ($value['type_fk'] == 4) ? $value['cdescription'] : $value['description'];
            $tmp['case'] = $value['sname'];
            $tmp['action_date'] = $value['action_date'];
            $tmp['employee'] = $value['ename'];
            $timeH = intval($value['unit_time']/60);
            $timeM = $value['unit_time'] - ($timeH*60);
            $tmp['unit_time'] = $timeH.':'.$timeM;
            $tmp['unit_time_h'] = round($value['unit_time']/60,2);
            $tmp['service'] = $type;
            $tmp['description'] = ($value['id_service_fk'] == 4 || !$value['description']) ? htmlentities($value['cdescription']) : htmlentities($value['description']);
            //$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" title="Przejdź do karty klienta">'.( ($value['csymbol'])?$value['csymbol']:$value['cname'] ). '</a>'
            $tmp['customer'] = ( ($value['csymbol'])?$value['csymbol']:$value['cname'] )
                              .(($value['sname']) ? '<br /><small class="text--purple"><i class="fa fa-balance-scale"></i>'.$value['sname'].'</small>' : ''); 
            $textColor = (!in_array($value['id_department_fk'], $this->module->params['managers'])) ? ' text--teal' : '';
            $tmp['className'] = (( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) ? 'warning' : 'default').$textColor;
            $tmp['className'] = ( $value['status'] == -1 )  ? 'danger' : $tmp['className'];
            $tmp['order'] = $orderSymbol;
            //$tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia" title="oznaczony do fakturowania"></i>' : (($value['is_gratis'] == 1) ? '<i class="fa fa-gift text--pink"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia jako gratis" title="gratis"></i>' : '<i class="fa fa-close text--orange"  data-placement="left" data-toggle="tooltip" data-title="" title="Czynność nie oznaczona do rozliczenia" title="nie podlega fakturowaniu"></i>');
            if(!$value['id_invoice_fk']) {
                $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia"></i>' : (($value['is_gratis'] == 1) ? '<i class="fa fa-gift text--pink" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność oznaczona do rozliczenia jako gratis"></i>' : '<i class="fa fa-close text--orange" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność nie oznaczona do rozliczenia"></i>');
             } else {
                if($value['is_gratis']) $value['is_confirm'] = 1;
                $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" data-placement="left" data-toggle="tooltip" data-title="" title="Czynność rozliczona i powiązana z fakturą"></i>'.(($value['is_gratis']) ? '<br /><i class="fa fa-gift text--pink cursor-pointer" title="Gratis"></i>' : '');
            }
			if($periodAction == 'forward')
				$tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
            else
				$tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';

			$tmp['cost'] = ($value['acc_cost_native'])?$value['acc_cost_native']:0;
            $tmp['confirm'] = $value['is_confirm'];
            $tmp['actions'] = '<div class="edit-btn-group">';
                if($value['id_service_fk'] <= 4) 
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-xs btn-default update" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>';
                $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                if($value['id_service_fk'] <= 4) 
                    $tmp['actions'] .= ( (!$value['id_invoice_fk'] && $value['status'] == 1) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
                    
                    //$tmp['actions'] .= ( ($value['is_confirm'] == 0 && !$value['is_gratis']) ? '<a href="'.Url::to(['/accounting/action/deleteajax', 'id' => $value['id']]).'" class="btn btn-xs bg-red deleteConfirm" data-table="#table-actions" data-form="item-form"  data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');

                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
            $tmp['actions'] .= '</div>';
            //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['id'] = $value['id'];
            
            array_push($fields, $tmp); $tmp = [];
        }
      
		return ['total' => $count,'rows' => $fields, 'cost' => number_format($costs, 2, "," ,  " "), 'time1' => number_format($summary, 2, "," ,  " "), 'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 'amount' => number_format($totalRow['amount'], 2, "," ,  " ")];
	}
    
    public function actionSexport() {
    
        $i=4; 
		$data = []; $where = []; $employee = false; $department = false; $customer = false; $dateFrom = false;  $dateTo = false;  $accPeriod = false;
		
		if(isset($_GET['AccActionsSearch'])) {
            $params = $_GET['AccActionsSearch'];
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['description']) && !empty($params['description']) ) {
                array_push($where, "lower(a.description) like '%".strtolower($params['description'])."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "a.id_employee_fk = ".$params['id_employee_fk']);
                $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($params['id_employee_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                array_push($where, "a.id_department_fk = ".$params['id_department_fk']);
                $department = \backend\Modules\Company\models\CompanyDepartment::findOne($params['id_department_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "(a.id_customer_fk = ".$params['id_customer_fk']." or a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where coi.status=1 and co.status = 1 and co.id_customer_fk = ".$params['id_customer_fk']."))");
                $customer = \backend\Modules\Crm\models\Customer::findOne($params['id_customer_fk']);
            } /*else {
                array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
            }*/
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "a.id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
                $dateFrom = $params['date_from'];
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
                $dateTo = $params['date_to'];
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                if($params['type_fk'] == 4)
                    array_push($where, "(a.type_fk = ".$params['type_fk']." or (acc_cost_net > 0))");
                else 
                    array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                array_push($where, "a.id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "(is_confirm = 1 or is_gratis = 1)");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0 and (is_gratis = 0 or is_gratis is null)");
                if($params['invoiced'] == 3) array_push($where, "is_confirm = 1 and (id_invoice_fk is not null and id_invoice_fk != 0)");
                if($params['invoiced'] == 4) array_push($where, "is_gratis = 1");
                if($params['invoiced'] == 5) array_push($where, "(a.id_order_fk is null or a.id_order_fk=0)");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
                $accPeriod = $params['acc_period'];
            }
        } /*else {
            array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
        }*/
        //array_push($where, "a.id_customer_fk in (select id_customer_fk from {{%customer_department}} where id_department_fk in (".implode(',', $this->module->params['managers'])."))");
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
        
        $periodLabel = ($accPeriod) ? " na okres ".$accPeriod : '';
        if($employee)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności pracownika '.$employee->fullname.$periodLabel);
        else if($department)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności działu '.$department->name.$periodLabel);
        else if($customer)
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności na rzecz klienta '.(($customer->symbol) ? $customer->symbol : $customer->symbol).$periodLabel);
        else 
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności'.$periodLabel);
       
       // $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Czynności na rzecz klienta');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Pracownik')
            ->setCellValue('B3', 'Data')
            ->setCellValue('C3', 'Czas trwania')
            ->setCellValue('D3', 'Stawka')
			->setCellValue('E3', 'Kwota')
			->setCellValue('F3', 'Opis czynności')
            ->setCellValue('G3', 'Typ czynności')
            ->setCellValue('H3', 'Sprawa')
            ->setCellValue('I3', 'Koszty')
            ->setCellValue('J3', 'Klient')
            ->setCellValue('K3', 'Zlecenie')
            ->setCellValue('L3', 'Rozliczono do faktury');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:L3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFill()->getStartColor()->setARGB('D9D9D9');		
		
        $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.name as name", "a.description as description", 'acc_cost_native', 'acc_cost_description', "a.id_department_fk",
                      'is_confirm', "confirm_time", "estimated_price", "confirm_price", "action_date", "unit_time", "ifnull(c.symbol,c.name) as cname", "c.symbol as csymbol", "c.id as cid", "o.name as oname",
                      "case when e.id is null then a.name else concat_ws(' ', e.lastname, e.firstname) end  as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "cc.name as ccname"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%cal_case}} as cc', 'cc.id = a.id_set_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
            
	    $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1] );
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time'];
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
			
		if( $count > 0 ) {
			foreach($rows as $record){ 
                $tmpType = $dict[$record['type_fk']]; 
                //$objRichText = new \PHPExcel_RichText();
                //$objRichText->createTextRun($record['ename'])->getFont()->setColor( new \PHPExcel_Style_Color((!in_array($record['id_department_fk'], $this->module->params['managers'])) ? \PHPExcel_Style_Color::COLOR_BLUE : \PHPExcel_Style_Color::COLOR_BLACK ) );
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['ename']); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['action_date'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, round($record['unit_time']/60, 2)); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['estimated_price']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, round($record['estimated_price']*$record['confirm_time'], 2)); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['description']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $tmpType['name']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['ccname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['acc_cost_native']); 
                $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, $record['cname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('K'. $i, $record['oname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('L'. $i, (($record['is_confirm']) ? 'TAK' : 'NIE')); 
                
                if(!in_array($record['id_department_fk'], $this->module->params['managers'])) {
                    $objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A'.$i.':L'.$i)->getFont()->setBold(false);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->getFill()->getStartColor()->setARGB('FFF0FFFF');		
				}		
				$i++; 
			} 
            $objPHPExcel->getActiveSheet()->setCellValue('C'. $i,'=SUM(C4:C'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('C'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('E'. $i,'=SUM(E4:E'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('E'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
            
            $objPHPExcel->getActiveSheet()->setCellValue('I'. $i,'=SUM(I4:I'.($i-1).')'); 
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getFill()->getStartColor()->setARGB('D9D9D9');
            $objPHPExcel->getActiveSheet()->getStyle('I'. $i)->getNumberFormat()->setFormatCode('#,##0.00');
		}
		//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWrapText(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:L'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Czynności na rzecz klienta');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Czynnosci'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
	}
    
    public function actionOrders() {
		$searchModel = new AccPeriod();
        //$searchModel->acc_period = date('Y-m'); 
       // $searchModel->status = 5;
        
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
        
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $searchModel->acc_period = $dateYear.'-'.$month;
        } else if($dateDay <= 10 && $dateMonth == 1) {
           $searchModel->acc_period = ($dateYear-1).'-12';
        } else {
           $searchModel->acc_period = $dateYear.'-'.date('m');
        }
        
        if(isset($_GET['period']) && !empty($_GET['period'])) {
            $periodItem = AccPeriodItem::findOne($_GET['period']); 
            if($periodItem)
                $searchModel->acc_period = $periodItem->period['period_year'].((($periodItem->period['period_month']<10)?'0':'').$periodItem->period['period_month']);
                
            $searchModel->status = 0;
        }
		
        $periods = \backend\Modules\Accounting\models\AccPeriodItem::find()->where(['id_manager_fk' => $this->module->params['employeeId']])->andWhere('status <= 1')->all();  
        return $this->render('orders', [
            'searchModel' => $searchModel, 'periods' => $periods
        ]);
	}
    
    public function actionOrdersdata() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $where = []; $whereActions = ''; $joinActions = '';
        if(isset($_GET['AccPeriod'])) { 
            $params = $_GET['AccPeriod']; 
            if( isset($params['acc_period']) && strlen($params['acc_period']) ) {
                $accPeriod = $params['acc_period'];
            } else {
                $accPeriod = date('Y-m');
            }
            
            if( isset($params['status']) && strlen($params['status']) ) {
                if($params['status'] == 1) {
                    array_push($where, "i.id is not null" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                } else if($params['status'] == 2) {
                    array_push($where, "i.id is null" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                } else if($params['status'] == 3) {
                    //array_push($where, " EXISTS (select id from {{%acc_actions}} where status = 1 and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'join'; $whereActions = "1=1";
                } else if($params['status'] == 4) {
                    //array_push($where, " NOT EXISTS (select id from {{%acc_actions}} where status = 1 and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'left join'; $whereActions = "1=1";
                    array_push($where, "a.id is null" );
                } else if($params['status'] == 5) {
                    //array_push($where, "  EXISTS (select id from {{%acc_actions}} where status = 1 and (is_confirm = 1 or is_gratis=1) and id_order_fk = o.id and acc_period = '".$accPeriod."')" );
                    $joinActions = 'join'; $whereActions = "is_confirm = 1 or is_gratis=1";
                }
            } else {
                $joinActions = 'left join'; $whereActions = "1=1";
            }
			
			if( isset($params['verify']) && strlen($params['verify']) ) {
				if($params['verify'] == 1) {
					array_push($where, "is_accept = 1" );
				}
				if($params['verify'] == 2) {
					array_push($where, "is_accept = 0" );
				}
			}
            
            if( isset($params['type_order_fk']) && strlen($params['type_order_fk']) ) {
                array_push($where, "id_dict_order_type_fk = ".$params['type_order_fk'] );
            }
            
        } else {
            $accPeriod = date('Y-m');
        }      
        
        $fields = []; $tmp = [];
        $connection = Yii::$app->getDb();
        $post = $_GET; 
        $sql = "select c.id as cid, c.symbol as csymbol, c.name as cname, o.id as oid, o.name as oname, type_symbol, type_color, type_name, is_accept, accept_date, min(i.id) as invoice, "
                ."(select count(*) from {{%acc_note}} n where type_fk = 2 and id_fk=o.id and n.acc_period='".$accPeriod."') as notes, "
                ."concat_ws('|', sum(case when(is_confirm = 1) then unit_time else 0 end), sum(case when(is_gratis = 1) then unit_time else 0 end), "
                                       ."sum(case when((is_gratis = 0 or is_gratis is null) and (is_confirm = 0 or is_confirm is null)) then unit_time else 0 end), "
                                       ."sum(case when(is_confirm = 1) then acc_cost_net else 0 end), "
                                       ."sum(case when((is_confirm = 0 or is_confirm is null)) then acc_cost_net else 0 end)) as actions"
                ." from {{%acc_order}} o join {{%acc_type}} at on at.id = o.id_dict_order_type_fk join {{%acc_order_period}} ap on ap.id_order_fk=o.id "
                    ." join {{%customer}} c on c.id=o.id_customer_fk "
                    .$joinActions." {{%acc_actions}} a on a.status = 1 and a.id_order_fk = o.id and a.acc_period = '".$accPeriod."' and (".$whereActions.") "
                    ." left join {{%acc_invoice}} i on (i.status = 1 and i.id_order_fk = o.id and date_format(date_issue, '%Y-%m') = '".$accPeriod."')"
                ." where o.status=1 and ap.status=1 and ap.acc_period='".$accPeriod."' and ap.id_employee_fk = ".$this->module->params['employeeId'].(($where) ? " and ".implode(' and ', $where) : '')
                ." group by o.id, o.name, c.id, c.name, type_symbol, type_color, type_name, is_accept, accept_date";
		//echo $sql; exit;		
	    
		$sqlCount = "select count(distinct o.id) as allRows "
                ." from {{%acc_order}} o join {{%acc_order_period}} ap on ap.id=o.id "
                .$joinActions." {{%acc_actions}} a on a.status = 1 and a.id_order_fk = o.id and a.acc_period = '".$accPeriod."' and (".$whereActions.") "
                ." left join {{%acc_invoice}} i on (i.status = 1 and i.id_order_fk = o.id and date_format(date_issue, '%Y-%m') = '".$accPeriod."')"
                ." where o.status=1 and ap.status=1 and ap.acc_period='".$accPeriod."' and ap.id_employee_fk = ".$this->module->params['employeeId'].(($where) ? " and ".implode(' and ', $where) : '');
                
        $commandCount = $connection->createCommand($sqlCount);
        $count = $commandCount->queryOne()['allRows'];
		
		$sql .= " order by c.name"." limit ".(isset($post['offset']) ? $post['offset'] : 0 ).", ".(isset($post['limit']) ? $post['limit'] : 10); 
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		
        
        foreach($data as $key => $value) {
            $actions = explode('|', $value['actions']);
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
            $tmp['period'] = $accPeriod;
            $tmp['customer'] = ($value['cname']) ? $value['cname'] : $value['cname'];
            $tmp['order'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'"  data-placement="left" data-toggle="tooltip" data-title="'.$value['type_name'].'">'.$value['oname']
                            .'<br /><span class="btn btn-xs btn-break" style="background-color:'.$value['type_color'].'">'.$value['type_symbol'].'</span></a>' 
							//.((\Yii::$app->session->get('user.isManagerD') == 1) ? '&nbsp;<a href="'. Url::to(['/accounting/manager/specification']).'?orderId='.$value['oid'].'&period='.$accPeriod.'" class="btn btn-xs bg-blue btn-flat" data-toggle="tooltip" "data-title="Przejdź do czynności"><i class="fa fa-tasks"></i></a>' : '')
                            .'&nbsp;<a href="'. Url::to(['/accounting/manager/specification']).'?orderId='.$value['oid'].'&period='.$accPeriod.'" class="btn btn-xs bg-blue btn-flat" data-toggle="tooltip" "data-title="Przejdź do specyfikacji"><i class="fa fa-tasks"></i></a>'
							.((isset($actions[0]) && $actions[0] > 0) ? '&nbsp;<a href="'. Url::to(['/accounting/order/exportbasic', 'id' => $value['oid'], 'period' => $accPeriod, 'save' => false]) .'&employees=1" class="btn btn-xs btn-default btn-flat export-file" data-toggle="tooltip" "data-title="Eksportuj do pliku Excel"><i class="fa fa-file-excel-o text--green"></i></a>' : '');
            $tmp['invoice'] = ($value['invoice']) ? 
                                    '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['invoice']]).'" data-placement="left" data-toggle="tooltip" data-title="Przejdź do faktury" class="btn btn-xs btn-icon bg-green"><i class="fa fa-calculator"></i>zobacz</a>' 
                                                 :
                                    '<a href="/accounting/invoice/generate/'.$value['oid'].'?period='.$accPeriod.'&gen=1" data-placement="left" data-toggle="tooltip" data-title="Wygeneruj fakturę" class="btn btn-xs btn-icon bg-blue deleteConfirm" data-table="#table-orders" data-label="Wygeneruj fakturę"><i class="fa fa-plus-square"></i>wystaw</a>';
           
             
            $tmp['actions_s'] = ((isset($actions[0]) && !empty($actions[0])) || isset($actions[1]) && !empty($actions[1]) || isset($actions[3]) && !empty($actions[3])) ? 
                                    '<small><i class="fa fa-clock-o text--blue"></i>&nbsp;'.round($actions[0]/60,2).'&nbsp;<i class="fa fa-gift text--pink"></i>'.round($actions[1]/60,2).'<br /><i class="fa fa-money text--red"></i>&nbsp;'.round($actions[3],2).'</small>'
                                   // .'<br /><a href="'.Url::to(['/accounting/action/allunsettle', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs btn-icon bg-red deleteConfirm" data-label="Wycofaj z fakturowania" data-placement="left" data-toggle="tooltip" data-title="Kliknij aby'.(($value['invoice'])?' usunąć fakturę i ' : ' ').'wycofać czynności z rozliczenia"><i class="fa fa-rotate-left"></i>wycofaj </a>' 
                                    :  'brak czynności';
            $tmp['actions_nos'] = ((isset($actions[2]) && !empty($actions[2])) || (isset($actions[4]) && !empty($actions[4]))) ? 
                                    '<small><i class="fa fa-clock-o text--blue"></i>&nbsp;'.round($actions[2]/60,2).'<br /><i class="fa fa-money text--red"></i>&nbsp;'.(isset($actions[4]) ? round($actions[4],2): 0).'</small>'
                                    //.'<br /><a href="'.Url::to(['/accounting/action/allsettle', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs btn-icon bg-purple deleteConfirm" data-label="Oznacz do zafakturowania" data-placement="left" data-toggle="tooltip" data-title="Kliknij aby oznaczyć czynności do rozliczenia"><i class="fa fa-square"></i>oznacz </a>' 
                                    : 'brak czynności';
            $tmp['note'] = '<a href="'.Url::to(['/accounting/period/chat', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
            $tmp['accept'] = (!$value['is_accept'])?'<a href="'.Url::to(['/accounting/manager/verify', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-lightgrey gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check-square\'></i>'.Yii::t('app', 'Weryfikacja').'" title="'.Yii::t('app', 'Weryfikacja').'"><i class="fa fa-check-square"></i></a>' 
                              : '<a href="'.Url::to(['/accounting/manager/verify', 'id' => $value['oid'], 'period' => $accPeriod]).'" class="btn btn-xs bg-green gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-check-square\'></i>'.Yii::t('app', 'Weryfikacja').'" title="'.Yii::t('app', 'Weryfikacja').'"><i class="fa fa-check-square"></i></a>';
           
            $tmp['id'] = $value['oid'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
        //return $fields;
    }
    
    public function actionVerify($id, $period) {
        $id = CustomHelpers::decode($id);
        $order = \backend\Modules\Accounting\models\AccOrder::findOne($id);
                
		$model = AccOrderPeriod::find()->where(['status' => 1, 'id_order_fk' => $order->id, 'acc_period' => $period, 'id_employee_fk' => $this->module->params['employeeId']])->one(); 
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->is_accept = 1;
            $model->accept_date = date('Y-m-d H:i:s');
            if($model->validate() && $model->save()) {
            try { 
                    if(Yii::$app->params['env'] == 'prod') {
                        $mail= \Yii::$app->mailer->compose(['html' => 'accVerifyAccept-html', 'text' => 'accVerifyAccept-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($model->creator['email'])
                            ->setBcc(['kamila_bajdowska@onet.eu'])
                            ->setSubject('Weryfikacja specyfikacji przez '.$model->employee['fullname'].' w systmie '.\Yii::$app->name );

                        $mail->send();
                    } else {
                        $mail = \Yii::$app->mailer->compose(['html' => 'accVerifyAccept-html', 'text' => 'accVerifyAccept-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo(['kamila_bajdowska@onet.eu'])
                            ->setSubject('DEV: Weryfikacja specyfikacji przez '.$model->employee['fullname'].' w systmie '.\Yii::$app->name.' - '.$model->creator['email']);
    
                        $mail->send();
                    }
                } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'verify-send';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }
                return array('success' => true, 'id' => $model->id, 'alert' =>'Weryfikacja została zapisana'/*, 'action' => 'periodManagerBind'*/);	
            } else {
                $model->is_accept = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formVerify', [ 'model' => $model, 'peiod' => $period, 'order' => $order]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formVerify', [ 'model' => $model, 'peiod' => $period, 'order' => $order]);	
        }
	}
}
