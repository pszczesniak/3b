<?php

namespace app\Modules\Accounting\controllers;

use Yii;
use backend\Modules\Accounting\models\AccDiscount;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * DiscountController implements the CRUD actions for AccActions model.
 */
class DiscountController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyEmployee models.
     * @return mixed
     */
    public function actionIndex() {
        /*if( count(array_intersect(["employeePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }*/
        
        $searchModel = new AccDiscount();
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('t');
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.orders'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }  
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'month' => $dateMonth, 'year' => $dateYear,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];

		//$fieldsDataQuery = $fieldsData = CalCase::find()->where(['status' => 1, 'type_fk' => 1]);
        $post = $_GET;
        if(isset($_GET['AccDiscount'])) { 
            $params = $_GET['AccDiscount']; 
            //\Yii::$app->session->set('search.discounts', ['params' => $params, 'post' => $post]);
            if( isset($params['id_order_fk']) && strlen($params['id_order_fk']) ) {
               array_push($where, "id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "o.id_customer_fk = ".$params['id_customer_fk']);
            }
			if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(d.name) like '%".strtolower($params['name'])."%'");
            }
        }  	
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "id_customer_fk in ( select id_customer_fk from {{%company_department}} where id_department_fk in (".implode(',', $this->module->params['managers']).") )");
        }
			
		$fields = [];
		$tmp = [];
		
		$sortColumn = 'd.id';
        if( isset($post['sort']) && $post['sort'] == 'percent' ) $sortColumn = 'd.discount_percent';
        if( isset($post['sort']) && $post['sort'] == 'percent' ) $sortColumn = 'd.discount_amount';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'd.name';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'order' ) $sortColumn = 'o.name';
        if( isset($post['sort']) && $post['sort'] == 'period' ) $sortColumn = 'acc_period';
		
		$query = (new \yii\db\Query())
            ->select(['d.id as id',  'd.name as name', 'discount_percent', 'discount_amount', 'd.id_invoice_fk as id_invoice_fk', 'acc_period', 'c.name as cname', 'c.id as cid', "o.id as oid", "o.name as oname", "concat_ws(' ', u.lastname, u.firstname) as uname"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_discount}} d', 'd.id_order_fk = o.id')
			->join('JOIN', '{{%user}} u', 'u.id = d.created_by')
            ->where( ['d.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_discount}} d', 'd.id_order_fk = o.id')
            ->where( ['o.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; 
 
		$tmp = [];
		$invoices = 0;
		foreach($rows as $key=>$value) {
			//$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
			$tmp['name'] = $value['name'];
			$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'">'.$value['cname'].'</a>';
            $tmp['order'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a>';
			$tmp['period'] = $value['acc_period'];
			$tmp['creator'] = $value['uname'];
            $tmp['percent'] = $value['discount_percent'];
            $tmp['amount'] = ($value['discount_amount'] && $value['id_invoice_fk']) ? number_format($value['discount_amount'], 2, "," ,  " ") : '';
			//$tmp['invoice'] = ($value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['id_invoice_fk']]).'" title="Pokaż szczegóły rozliczenia"><i class="fa fa-calculator"></i></a>' : '';
            if(!$value['id_invoice_fk']) {
				$tmp['actions'] = '<div class="edit-btn-group">';
					$tmp['actions'] .= '<a href="'.Url::to(['/accounting/discount/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default gridViewModal" data-table="#table-discounts" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('lsdd', 'Update').'" ><i class="fa fa-pencil"></i></a>';
					$tmp['actions'] .= '<a href="'.Url::to(['/accounting/discount/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-discounts"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
				$tmp['actions'] .= '</div>';
			} else  {
				$tmp['actions'] = '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['id_invoice_fk']]).'" title="Przejdź do faktury"><i class="fa fa-calculator text--purple"></i></a>';
            }
			//$tmp['className'] = ($value['date_to']) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionCreate() {
        
        $dateYear = date('Y'); $dateMonth = date('n'); $dateDay = date('j');
       
        if($dateDay <= 10 && $dateMonth > 1) {
            if( ($dateMonth-1) < 10 ) $month = '0'.($dateMonth-1); else $month = $dateMonth-1;
            $acc_period = $dateYear.'-'.$month;
        } else if($dateDay <= 10 && $dateMonth == 1) {
           $acc_period = ($dateYear-1).'-12';
        } else {
           $acc_period = $dateYear.'-'.date('m');
        }
        
		$model = new AccDiscount; 
		$model->status = 1;
        $model->name = 'Rabat za okres '.$acc_period;
        $model->acc_period = $acc_period;
            
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
          
            if( $model->validate() && $model->save()) {
				\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]); 
                return array('success' => true,  'action' => 'insertRow', 'index' =>0, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Rabat został zapisany'  );	               
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionUpdate($id) {
        
		$model = $this->findModel($id); 
         
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
            if( $model->validate() && $model->save()) {
				\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]); 
                return array('success' => true,  'id' => $model->id, 'name' => $model->name, 'alert' => 'Zmiany zostały zapisane'  );	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [ 'model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formAjax', [ 'model' => $model]);	
        }
	}
    
    public function actionView($id) {

        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model, 'stats' => AccOrder::getStats($model->id)] );
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }
    
    /**
     * Deletes an existing CompanyEmployee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->deleted_by = \Yii::$app->user->id;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->status = -2;
        if($model->save()) {
            \Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]); 
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'table' => '#table-discounts');	
        } else {
            //var_dump($model->getErrors());
            $errors = $model->getErrors();
            $alert = "Dane nie zostały usunięte";
            foreach($errors as $key => $error) {
                $alert = $error;
            }
            return array('success' => false, 'alert' => $alert, 'table' => '#table-discounts', 'errors' => $model->getErrors() );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the AccDiscount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccDiscount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        $id = CustomHelpers::decode($id);
		if (($model = AccDiscount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pracownik o wskazanym identyfikatorze nie został odnaleziony w systemie.');
        }
    }
   
    public function actionExport() {
		
        $grants = $this->module->params['grants'];
				
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		
 
		$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
		$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(-1);
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Wykaz rabatów');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
            ->setCellValue('B3', 'Zlecenie')
            ->setCellValue('C3', 'Okres rozliczeniowy')
            ->setCellValue('D3', 'Opis')
			->setCellValue('E3', 'Procent')
            ->setCellValue('F3', 'Utworzył(a)');
			
		$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(-1);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getFill()->getStartColor()->setARGB('D9D9D9');	

		$i=4; 
		$data = []; $where = [];
        
        $post = $_GET;
        if(isset($_GET['AccDiscount'])) { 
            $params = $_GET['AccDiscount']; 
            //\Yii::$app->session->set('search.discounts', ['params' => $params, 'post' => $post]);
            if( isset($params['id_order_fk']) && strlen($params['id_order_fk']) ) {
               array_push($where, "id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                array_push($where, "o.id_customer_fk = ".$params['id_customer_fk']);
            }
			if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
            if(isset($params['name']) && !empty($params['name']) ) {
                array_push($where, "lower(d.name) like '%".strtolower($params['name'])."%'");
            }
        }  	
        if(isset($_GET['cid'])) {
            //var_dump($_GET['cid']);exit;
            array_push($where, "id_customer_fk = ".$_GET['cid']);
        }
		
		$sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'percent' ) $sortColumn = 'd.discount_percent';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'd.name';
        if( isset($post['sort']) && $post['sort'] == 'customer' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'period' ) $sortColumn = 'acc_period';
		
		$query = (new \yii\db\Query())
            ->select(['d.id as id',  'd.name as name', 'discount_percent', 'acc_period', 'c.name as cname', 'o.name as oname', 'c.id as cid', "concat_ws(' ', u.lastname, u.firstname) as uname"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_discount}} d', 'd.id_order_fk = o.id')
			->join('JOIN', '{{%user}} u', 'u.id = d.created_by')
            ->where( ['d.status' => 1] );
        
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows"])
            ->from('{{%acc_order}} o')
            ->join('JOIN', '{{%customer}} c', 'c.id = o.id_customer_fk')
			->join('JOIN', '{{%acc_discount}} d', 'd.id_order_fk = o.id')
            ->where( ['o.status' => 1] );
            
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
        }
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        
        $totalRow = $queryTotal->one(); $count = $totalRow['total_rows'];
			
		if($count > 0) {
			foreach($rows as $record){ 
				
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['oname']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['acc_period']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['discount_percent']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['uname']); 
						
				$i++; 
			}  
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:F'.$i)->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setTitle('Rabaty');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Rabaty'.'_'.date("y_m_d__H_i_s").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
	}
    
    public function actionSave() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $acc_period = $_POST['period'];
        
        $model = AccDiscount::find()->where(['status' => 1, 'id_order_fk' => $_POST['id'], 'acc_period' => $acc_period])->one();
        
        if(!$model) {
            $model = new AccDiscount; 
            $model->status = 1;
            $model->name = 'Rabat za okres '.$acc_period;
            $model->id_order_fk = $_POST['id'];
            $order = \backend\Modules\Accounting\models\AccOrder::findOne($_POST['id']);
            $model->id_customer_fk = $order->id_customer_fk;
            $model->acc_period = $acc_period;
        } else {
            if($_POST['value'] == 0) {
                $model->status = -2;
                $model->deleted_by = \Yii::$app->user->id;
                $model->deleted_at = date('Y-m-d H:i:s');
                
                $model->save();
                \Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]);
                return array('success' => true, 'id' => $model->id, 'alert' => 'Rabat został usunięty');	
            }
        }
        
        $model->discount_percent = $_POST['value'];
            
        if (Yii::$app->request->isPost ) {
            if( $model->validate() && $model->save()) {
				\Yii::$app->runAction('/accounting/action/recalculation', ['order' => $model->id_order_fk, 'period' => $model->acc_period]); 
                return array('success' => true, 'id' => $model->id, 'name' => $model->name, 'alert' => 'Rabat został zapisany'  );	               
            } else {
                return array('success' => false, 'alert' => 'Rabat nie został zapisany', 'errors' => $model->getErrors() );	
			}	
		} 
	}
}
