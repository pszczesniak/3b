<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-task-form form calendar-task">
     
        <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'action' => Url::to(['/accounting/order/updateajax', 'id' => $model->id]),
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform'  ],
            'fieldConfig' => [
                //'template' => '<div class="grid"><div class="col-sm-10 col-xs-8">{label}</div><div class="col-sm-2 col-xs-4">{input}</div><div class="col-xs-12">{error}</div></div>',
                /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
            ],
        ]); ?>
        <fieldset><legend class="bg-teal2">Warunki rozliczenia</legend>    
            <div class="grid">
                <div class="col-sm-2 col-xs-12"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [ 'class' => 'form-control select2'] ) ?> </div> 
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'id_dict_order_type_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccType::find()->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?= $form->field($model, 'name')->textInput() ?>
                </div>
            </div>
            <div class="grid">
                <div class="col-sm-4 col-xs-6">
                    <?= $form->field($model, 'rate_constatnt', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span></span> </div>  {error}{hint} '])->textInput( ['disabled' => ($model->id_dict_order_type_fk<=2) ? false : true] ) ?>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <?= $form->field($model, 'limit_hours', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} '])->textInput( ['disabled' => ($model->id_dict_order_type_fk==2) ? false : true] ) ?>
                </div>
                <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_settlement_type_fk')->dropDownList( \backend\Modules\Accounting\models\AccOrder::listSettlements(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>
            </div>
        
            <div class="grid">    
                <div class="col-xs-4">
                    <?= $form->field($model, 'rate_hourly_1', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( ['disabled' => ($model->id_dict_order_type_fk>1) ? false : true] ) ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'rate_hourly_2', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( ['disabled' => ($model->id_dict_order_type_fk>1) ? false : true] ) ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'rate_hourly_way', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( [] ) ?>
                </div>
            </div>
            
            <?php if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) { ?>
                <div class="form-group align-right">
                    <!--<a href="<?= Url::to(['/accounting/order/rates', 'id' => $model->id]) ?>" class="btn btn-sm bg-purple gridViewModal"  data-target="#modal-grid-item"  data-title="<i class=\'fa fa-cog\'></i>Stawki" title="Stawki" ><i class="fa fa-cog"></i></a>-->
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
        </fieldset>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
	
	document.getElementById('accorder-id_dict_order_type_fk').onchange = function() {
        if(this.value == 1) {
            document.getElementById('accorder-rate_constatnt').disabled = false;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = true;
            document.getElementById('accorder-rate_hourly_2').disabled = true;
        } 
        if(this.value == 2) {
            document.getElementById('accorder-rate_constatnt').disabled = false;
            document.getElementById('accorder-limit_hours').disabled = false;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
        } 
        if(this.value == 3) {
            document.getElementById('accorder-rate_constatnt').disabled = true;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
        } 
        if(this.value == 4) {
            document.getElementById('accorder-rate_constatnt').disabled = true;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
        } 
    }
	
	document.getElementById('accorder-id_currency_fk').onchange = function() {
		$curr = this.options[this.selectedIndex].text;
		$elements = document.getElementsByClassName("currencyName");
		for (i1 = 0; i1 < $elements.length; i1++) {  $elements[i1].innerHTML = $curr;  }
	}
</script>
