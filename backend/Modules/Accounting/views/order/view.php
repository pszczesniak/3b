<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\accounting\ActionsTable;
use frontend\widgets\accounting\InvoicesTable;

use frontend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = ($model->customer['symbol'] ? $model->customer['symbol'] : $model->customer['name']). ' - '. $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr zleceń'), 'url' => Url::to(['/accounting/order/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = 'Zlecenie `'.$model->name.'`';
?>
<div class="grid">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-blue rounded">
            <span class="mini-stat-icon"><i class="fa fa-hourglass text--blue"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['confirm_time']) ? $stats['confirm_time'] : 0 ?> </span> Czas [h]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer">&nbsp;</div> <?php } ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-green rounded">
            <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['confirm_amount']) ? number_format ($stats['confirm_amount'], 2, "." ,  " ") : 0 ?> </span> Kwota [<?= $model->currency ?>]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer"><?= ($stats['confirm_amount_native']) ? number_format ($stats['confirm_amount_native'], 2, "." ,  " ") : 0 ?> PLN</div> <?php } ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-pink rounded">
            <span class="mini-stat-icon"><i class="fa fa-line-chart text--pink"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['confirm_time']==0) ? '0' : round($stats['confirm_amount']/$stats['confirm_time'],2) ?> </span> Śr. stawka [<?= $model->currency ?>]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer"><?= ($stats['confirm_time']==0) ? '0' : round($stats['confirm_amount_native']/$stats['confirm_time'],2) ?> PLN</div> <?php } ?>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="mini-stat clearfix bg-red rounded">
            <span class="mini-stat-icon"><i class="fa fa-money text--red"></i></span>
            <div class="mini-stat-info">  <span class="todo-hours_per_day"> <?= ($stats['costs']) ? $stats['costs'] : 0 ?> </span> Koszty [<?= $model->currency ?>]</div>
            <?php if($model->currency != 'PLN') { ?><div class="mini-stat-footer"><?= ($stats['costs_native']) ? $stats['costs_native'] : 0 ?> PLN</div> <?php } ?>
        </div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'order-view', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="grid profile">
                <div class="col-md-4 col-xs-12">
                    <div class="profile-avatar">
                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= $avatar ?>" alt="Avatar">
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="profile-name">
                        <h3><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>" title="Przejdź do karty klienta"><?= $model->customer['name'] ?></a></h3>
                        <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->customer['phone']) ? $model->customer['phone'] : 'brak danych' ?></p>
                        <p class="job-title mb0"><i class="fa fa-envelope"></i> <?=  ($model->customer['email']) ?  Html::mailto($model->customer['email'], $model->customer['email']) : 'brak danych'  ?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <?php $items = $model->items; ?>
                    
                    <div class="profile-info bg-purple2 text--purple padding-10">
                        <?php if( count($items) == 0 ) { echo '<b>brak powiązanych klientów</b>'; } else { ?>
							<h5>Powiązani klienci</h5>
                            <?php echo '<ul>'; ?>
							<?php foreach($items as $key => $item) {
								echo '<p>'.$item['customer']['name'].'</p>';
							} ?>
							<?php echo '</ul>'; ?>
						<?php } ?>
                        <!--<h5 class="text-muted">Utworzono</h5>
                        <p><?= $model->created_at ?></p>
                        <h5 class="text-muted">Utworzył(a)</h5>
                        <p><?= $model->creator['fullname'] ?></p>-->
                    </div>
                    <br />
                    <fieldset><legend>Dokumenty</legend><?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 7, 'parentId' => $model->id, 'onlyShow' => false]) ?></fieldset>
                    
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="text--navy">
                <?= $this->render('_form_settled', ['model' => $model]) ?>
            </div>
            <fieldset>
                <legend class="bg-orange">Stawki zróżnicowane
                    <a aria-controls="rates-container" aria-expanded="true" href="#rates-container" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </legend>
                <div id="rates-container" class="collapse in">
                    <?= $this->render('_rates', ['model' => $model]) ?>
                </div>
            </fieldset>
        </div>
    </div>
 
    <!--<div class="text-center invoice-btn">
        <a class="btn btn-danger btn-lg btn-xs"><i class="fa fa-check"></i> Submit Invoice </a>
        <a class="btn btn-info btn-lg btn-xs" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </a>
    </div>-->
    
<?php $this->endContent(); ?>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-file"></i><span class="panel-tabs--text">Faktury</span></a></li>
                <!--<li><a data-toggle="tab" href="#tab2"><i class="fa fa-list-ul"></i><span class="panel-tabs--text">Usługi</span> </a></li>-->
                <li><a data-toggle="tab" href="#tab3"><i class="fa fa-history"></i><span class="panel-tabs--text">Czynności</span> </a></li>
                <li><a data-toggle="tab" href="#tab4"><i class="fa fa-percent"></i><span class="panel-tabs--text">Rabaty</span> </a></li>
                <li><a data-toggle="tab" href="#tab5"><i class="fa fa-exclamation-triangle text--red"></i><span class="panel-tabs--text">Windykacja</span> </a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <?= InvoicesTable::widget(['dataUrl' => Url::to(['/accounting/order/invoices', 'id' => $model->id]) ]) ?>
                </div>
               
                <div class="tab-pane" id="tab3">
                    <?= ActionsTable::widget(['dataUrl' => Url::to(['/accounting/order/actions', 'id' => $model->id]), 'invoice' => false, 'customerId' => $model->id_customer_fk ]) ?>
                </div>
                
                <div class="tab-pane" id="tab4">
                    <?= $this->render('_discounts', ['model' => $model]) ?>
                </div>
                <div class="tab-pane" id="tab5">
                    windykacja
                </div>
            </div>
        </div>
    </div>
