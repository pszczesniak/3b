<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-orders-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-orders']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-3 col-sm-12 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ] ) ?></div>        
        <div class="col-md-3 col-sm-6 col-xs-12"><?= $form->field($model, 'id_dict_order_type_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccType::find()->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ] ) ?> </div>
        <div class="col-md-2 col-sm-6 col-xs-12"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ] ) ?></div>        

        <div class="col-md-2 col-sm-5 col-xs-6">
			<div class="form-group field-accorder-date_from">
				<label for="accorder-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="accorder-date_from" name="AccOrderSearch[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-5 col-xs-6">
			<div class="form-group field-accorder-date_to">
				<label for="accorder-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="accorder-date_to" name="AccOrderSearch[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <!--<div class="col-md-5 col-sm-12"><?= $form->field($model, 'symbol')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ]) ?></div>-->
    </div> 

    <?php ActiveForm::end(); ?>

</div>
