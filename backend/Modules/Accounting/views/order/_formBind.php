<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-orders"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::find()->where(['type_fk' => 1, 'status' => 1])->all(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
		<fieldset><legend>Dołącz do zlecenia</legend>
			<div class="grid">
				<div class="col-md-6 col-sm-4 col-xs-12"><?= $form->field($model, 'id_parent_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search'  ] ) ?></div>        
				<div class="col-md-6 col-sm-4 col-xs-12"><?= $form->field($model, 'id_order_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getList(($model->id_parent_fk) ? $model->id_parent_fk : 0), 'id', 'name'), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>                               
			</div> 
		</fieldset>   
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>


<script type="text/javascript">
	
	document.getElementById('accorderitem-id_parent_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0), true)
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accorderitem-id_order_fk').innerHTML = result.listOrder;     
            }
        }
       xhr.send();
        return false;
    }
</script>