<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-task-form form calendar-task">
     
    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => (!$model->isNewRecord && $model->status == 1) ? 'ajaxform' : '', ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <div class="grid">
        <div class="col-sm-4"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-4">
            <?= $form->field($model, 'id_dict_order_type_fk')->dropDownList( \backend\Modules\Accounting\models\AccOrder::listTypes(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="accorder-task_date">Data</label>
                <input type='text' class="form-control" id="task_date" name="AccOrder[date_from]" value="<?= $model->date_from ?>"/>
            </div>
        </div>
    </div>
    <div class="grid">
        <div class="col-sm-6">
            <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'id_set_fk')->dropDownList(  \backend\Modules\Task\models\CalCase::getGroups($model->id_customer_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
        </div>
    </div>
    

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">

	document.getElementById('accorder-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/cases']) ?>/"+( (this.value) ? this.value : 0 ), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accorder-id_case_fk').innerHTML = result.list;               
            }
        }
       xhr.send();
        return false;
    }
    
</script>
