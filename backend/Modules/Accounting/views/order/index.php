<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Umowy');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="toolbar-orders" class="btn-group toolbar-table-widget">
        <?=  Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/order/createajax', 'id' => 0]) , 
                ['class' => 'btn btn-success btn-icon gridViewModal', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "item-form", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) ?>
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/order/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-orders-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-orders"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-orders"
                data-toolbar="#toolbar-orders" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number="<?= $setting['page'] ?>"
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="customer"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-acc-orders-search"
                data-url=<?= Url::to(['/accounting/order/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>  
					<th data-field="customer"  data-sortable="true" >Klient</th>
					<th data-field="name"  data-sortable="true" >Nazwa</th>
					<th data-field="method"  data-sortable="true" data-align="center">Metoda</th>
					<th data-field="currency"  data-sortable="true"  data-align="center">Waluta</th>
					<!--<th data-field="creator"  data-sortable="true" >Utworzył(a)</th>-->
                    <th data-field="date_from"  data-sortable="true" >Obowiązuje</th>
					<th data-field="invoice"  data-sortable="false" data-align="center">Faktury</th>
					<th data-field="merging"  data-sortable="false" data-align="center"></th>
                    <th data-field="files"  data-sortable="false" data-align="center"><i class="fa fa-paperclip"></i></th>

                    <th data-field="actions" data-events="actionEvents" data-sortable="false" data-align="center" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>  
        </table>  
    </div>

<?php $this->endContent(); ?>
    
