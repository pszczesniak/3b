<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'Stawki pracowników');
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php if($model->id_dict_order_type_fk == 2 || $model->id_dict_order_type_fk == 4) { ?>
<div class="modal-body">

    <div id="toolbar-rates" class="btn-group">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/order/rateadd', 'id' => $model->id]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-event", 
                         'data-form' => "item-form", 
                         'data-table' => "table-rates",
                         'data-title' => "Dodaj"
                        ])  ?>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-rates"
                data-toolbar="#toolbar-rates" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-search="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="300"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-method="get"
                data-url=<?= Url::to(['/accounting/order/ratedata', 'id' => $model->id]) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="type" data-sortable="true">Typ pracownika</th>
                    <th data-field="employee" data-sortable="true">Pracownik</th>
                    <th data-field="rate" data-sortable="true">Stawka</th>                    
                    <th data-field="date_from"  data-sortable="true">Obowiązuje od</th>
                    <th data-field="date_to"  data-sortable="true">Obowiązuje do</th>
                    <th data-field="actions" data-events="actionEvents"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>          
        </table>      
    </div>
</div>
<?php } else { ?>
    <div class="modal-body">
        <div class="alert alert-warning">Ten typ zlecenia nie obsługuje stawek zróżnicowanych</div>
        <p><small class="text--red">* Jeśli jesteś na karcie zlecenia to być może nie zostały zapisane zmiany związane z typem zlecenia. Po zapisaniu zmian funkcja zostanie odblokowana.</small></p>
    </div>
<?php }