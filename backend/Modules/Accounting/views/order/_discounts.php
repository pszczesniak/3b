<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="modal-body">

    <div id="toolbar-data" class="btn-group">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/order/discountadd', 'id' => $model->id]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-event", 
                         'data-form' => "item-form", 
                         'data-table' => "table-discounts",
                         'data-title' => "Dodaj"
                        ])  ?>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-discounts"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-search="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                
                data-method="get"
                data-url=<?= Url::to(['/accounting/order/discountdata', 'id' => $model->id]) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="period" data-sortable="true" data-align="center">Okres</th>
                    <th data-field="amount" data-sortable="true" data-align="right">Zniżka [%]</th>
                    <th data-field="name" data-sortable="true">Nazwa</th>                    
                    <th data-field="actions" data-events="actionEvents"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>          
        </table>      
    </div>
</div>
