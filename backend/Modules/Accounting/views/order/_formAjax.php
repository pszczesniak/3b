<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-orders"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Ogólne</span></a></li>
                    <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-users"></i><span class="panel-tabs--text">Powiązani klienci</span> </a></li>
                    <li><a data-toggle="tab" href="#tab2c"><i class="fa fa-exclamation-triangle"></i><span class="panel-tabs--text">Windykacja</span> </a></li>
                    <?php if(!$model->isNewRecord) { ?>
                        <li><a data-toggle="tab" href="#tab2d"><i class="fa fa-files-o"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
                        <div class="grid">
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="accorder-date_from" class="control-label">Data rozpoczęcia</label>
                                    <div class='input-group date' id='datetimepicker_start'>
                                        <input type='text' class="form-control" id="accorder-date_from" name="AccOrder[date_from]" value="<?= $model->date_from ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_start').datetimepicker({ format: 'YYYY-MM-DD',  });
                                });
                            </script>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="accorder-date_to" class="control-label">Data zakończenia</label>
                                    <div class='input-group date' id='datetimepicker_end'>
                                        <input type='text' class="form-control" id="accorder-date_to" name="AccOrder[date_to]" value="<?= $model->date_to ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_end').datetimepicker({ format: 'YYYY-MM-DD' });
                                   
                                });
                            </script>
                            <div class="col-md-6 col-sm-12  col-xs-12"> <?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::find()->where(['type_fk' => 1, 'status' => 1])->all(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>
                        </div>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        <div class="grid">
                            <div class="col-sm-2 col-xs-12"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), [ 'class' => 'form-control select2'] ) ?> </div>                
                            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'id_dict_order_type_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccType::find()->all(), 'id', 'type_name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>
                        </div>
                        
                        <div class="grid">
                            
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <?= $form->field($model, 'rate_constatnt', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span></span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', 'disabled' => ($model->id_dict_order_type_fk<=2) ? false : true] ) ?>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <?= $form->field($model, 'limit_hours', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', 'disabled' => ($model->id_dict_order_type_fk==2) ? false : true] ) ?>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'id_settlement_type_fk')->dropDownList( \backend\Modules\Accounting\models\AccOrder::listSettlements(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                            </div>                            
                        </div>
                        <fieldset><legend>Stawka za czynności</legend>
                            <div class="grid">    
                                <div class="col-xs-4">
                                    <?= $form->field($model, 'rate_hourly_1', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', 'disabled' => ($model->id_dict_order_type_fk>1) ? false : true] ) ?>
                                </div>
                                <div class="col-xs-4">
                                    <?= $form->field($model, 'rate_hourly_2', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', 'disabled' => ($model->id_dict_order_type_fk>1) ? false : true] ) ?>
                                </div>
                                <div class="col-xs-4">
                                    <?= $form->field($model, 'rate_hourly_way', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->currency.'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', 'disabled' => ($model->id_dict_order_type_fk>1) ? false : true ] ) ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="tab-pane" id="tab2b">
                        <fieldset><legend>Scal czynności wybranych klientów</legend>
                            <?= $form->field($model, 'customers')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true, 'data-placeholder' => 'wybierz klientów'] )->label(false) ?> 
                        </fieldset>
                    </div>
                    <div class="tab-pane" id="tab2c">
                        <?= $form->field($model, 'debt_p_capital_percent', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">%</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', ] ) ?>
                        <?= $form->field($model, 'debt_p_interest_percent', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon">%</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number', ] ) ?>                    
                    </div>
                    <?php if(!$model->isNewRecord) { ?>
                    <div class="tab-pane" id="tab2d">
                        <fieldset><legend>Dokumenty</legend><?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 7, 'parentId' => $model->id, 'onlyShow' => false]) ?></fieldset>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>


<script type="text/javascript">
	
	document.getElementById('accorder-id_dict_order_type_fk').onchange = function() {
        if(this.value == 1) {
            document.getElementById('accorder-rate_constatnt').disabled = false;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = true;
            document.getElementById('accorder-rate_hourly_2').disabled = true;
            document.getElementById('accorder-rate_hourly_way').disabled = true;
        } 
        if(this.value == 2) {
            document.getElementById('accorder-rate_constatnt').disabled = false;
            document.getElementById('accorder-limit_hours').disabled = false;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
            document.getElementById('accorder-rate_hourly_way').disabled = false;
        } 
        if(this.value == 3) {
            document.getElementById('accorder-rate_constatnt').disabled = true;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
            document.getElementById('accorder-rate_hourly_way').disabled = false;
        } 
        if(this.value == 4) {
            document.getElementById('accorder-rate_constatnt').disabled = true;
            document.getElementById('accorder-limit_hours').disabled = true;
            document.getElementById('accorder-rate_hourly_1').disabled = false;
            document.getElementById('accorder-rate_hourly_2').disabled = false;
            document.getElementById('accorder-rate_hourly_way').disabled = false;
        } 
    }
	
	document.getElementById('accorder-id_currency_fk').onchange = function() {
		$curr = this.options[this.selectedIndex].text;
		$elements = document.getElementsByClassName("currencyName");
		for (i1 = 0; i1 < $elements.length; i1++) {  $elements[i1].innerHTML = $curr;  }
	}
</script>