<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-corrections-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-corrections']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-3 col-sm-4 col-xs-8"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-corrections', 'data-form' => '#filter-acc-corrections-search' ] ) ?></div>        
        <div class="col-md-3 col-sm-4 col-xs-8"><?= $form->field($model, 'id_department_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-corrections', 'data-form' => '#filter-acc-corrections-search' ] ) ?></div>               
        <div class="col-md-2 col-sm-4 col-xs-4">
			<div class="form-group field-acccorrection-acc_period">
				<label for="acccorrection-acc_period" class="control-label">Okres</label>
				<div class='input-group date' id='datetimepicker_period'>
					<input type='text' class="form-control" id="acccorrection-acc_period" name="AccCorrection[acc_period]" value="<?= $model->acc_period ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
       <div class="col-md-4 col-sm-12 col-xs-12"><?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-corrections', 'data-form' => '#filter-acc-corrections-search' ]) ?></div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
