<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Korekty');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faktury'), 'url' => Url::to(['/accounting/invoice/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="toolbar-corrections" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/correction/create']) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-corrections",
						 'data-title' => "<i class='fa fa-plus'></i>Dodaj korektę"
                        ])  ?>
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/correction/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-corrections-search", 
                 'data-table' => "table-items"
                ]) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-corrections"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-corrections"
                data-toolbar="#toolbar-corrections" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-search="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                
                data-method="get"
				data-search-form="#filter-acc-corrections-search"
                data-url=<?= Url::to(['/accounting/correction/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="department" data-sortable="true">Dział</th> 
                    <th data-field="customer" data-sortable="true">Klient</th> 
                    <th data-field="order" data-sortable="true">Zlecenie</th> 
                    <th data-field="name" data-sortable="true">Nazwa</th>
                    <th data-field="period" data-sortable="true">Okres</th>
                    <th data-field="correction_period" data-sortable="true">Za okres</th>
                    <th data-field="amount" data-sortable="true" data-align="right">Kwota</th>                
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="100px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>          
        </table>      
    </div>
</div>

<?php $this->endContent(); ?>
    
