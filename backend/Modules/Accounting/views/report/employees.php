<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Raport: Rozliczenie pracowników');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-2 col-sm-3">
        <?= $this->render('_filter', ['report' => 'employees', 'year' => $year, 'month' => $month]) ?>
    </div>
    <div class="col-md-10 col-sm-9">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
			<?php  echo $this->render('_esearch', ['model' => $searchModel]); ?>
        
            <div id="toolbar-report" class="btn-group toolbar-table-widget">
                <?=  Html::a('<i class="fa fa-table"></i>Rejestr czynności', Url::to(['/task/personal/actions', 'year' => $year, 'month' => $month]) , 
                            ['class' => 'btn bg-pink btn-icon', 
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                            ]) ?>
                <?=  Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/report/eexport', 'year' => $year, 'month' => $month]) , 
                            ['class' => 'btn btn-info btn-icon btn-export', 
                             'id' => 'action-export',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "#filter-acc-report-search", 
                             'data-table' => "table-items",
                             'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                            ]) ?>
				<?=  Html::a('<i class="fa fa-file-excel-o"></i>Wsad [CSV]', Url::to(['/accounting/report/eexportcsv', 'year' => $year, 'month' => $month]) , 
                            ['class' => 'btn btn-success btn-icon btn-export', 
                             'id' => 'action-export-cvs',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "#filter-acc-report-search", 
                             'data-table' => "table-items",
                             'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                            ]) ?>
                <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-report"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
                
            </div>
            <div class="div-table">
                <table  class="table-widget table-striped table-items header-fixed"  id="table-report"
                        data-toolbar="#toolbar-report" 
                        data-toggle="table-widget"  
                        data-show-refresh="false" 
                        data-show-toggle="true"  
                        data-show-columns="false" 
                        data-show-export="false"  
                        table-page-size="40"
                        data-show-pagination-switch="false"
                        data-pagination="true"
                        data-id-field="id"
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-page-size="30"
                        data-height="700"
                        data-show-footer="false"
                        data-side-pagination="client"
						data-row-style="rowStyle"
						data-sort-name="name"
						data-sort-action="asc"
						data-method="get"
						data-search-form="#filter-acc-report-search"

                        data-url=<?= Url::to(['/accounting/report/edata']).'?year='.$year.'&month='.$month ?>>
                    <thead>
                        <tr>
                            <th data-field="employee" data-sortable="true" rowspan="2">Pracownik</th>  
                            <th colspan="5" data-align="center">Godziny administracyjne</th>
                            <th colspan="3" data-align="center">Godziny na rzecz klienta</th>
                        </tr>
                        <tr>
                            <th data-field="hours_admin"  data-sortable="false" data-align="right">administracyjne</th>
                            <th data-field="hours_healt"  data-sortable="false" data-align="right">L4</th>
                            <th data-field="hours_marketing"  data-sortable="false" data-align="right">marketing</th>
                            <th data-field="hours_holiday"  data-sortable="false" data-align="right">urlop</th>
                            <th data-field="hours_day_off"  data-sortable="false" data-align="right">dzień wolny</th>
                            <th data-field="hours_all"  data-sortable="false" data-align="right">wszystkie</th>
                            <th data-field="hours_1"  data-sortable="false" data-align="right">niezafakturowane</th>
                            <th data-field="hours_2"  data-sortable="false" data-align="right">zafakturowane</th>
                        </tr>
         
                    </thead>
                    <tbody class="ui-sortable">

                    </tbody>  
                </table>  
                <fieldset><legend>Objaśnienia</legend> 
                    <table class="calendar-legend">
                        <tbody>
                            <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Pracownicy nieaktywni</td></tr>
                        </tbody>
                    </table>
                </fieldset> 
            </div>

        <?php $this->endContent(); ?>
    </div>
</div>
	     				



