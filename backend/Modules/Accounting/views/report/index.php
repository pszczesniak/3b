<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Raport: Zestawienie wg średniej stawki');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-2 col-sm-3 col-xs-12">
        <?= $this->render('_filter', ['report' => 'index', 'year' => $year, 'month' => $month]) ?>
    </div>
    <div class="col-md-10 col-sm-9 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="grid">    
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="mini-stat clearfix bg-green rounded">
                        <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
                        <div class="mini-stat-info">  <span class="actions_amount">0 </span> Czynności [PLN] </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="mini-stat clearfix bg-red rounded">
                        <span class="mini-stat-icon"><i class="fa fa-calculator text--red"></i></span>
                        <div class="mini-stat-info">  <span class="actions_cost">0 </span> Pozostały przychód [PLN] </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="mini-stat clearfix bg-teal rounded">
                        <span class="mini-stat-icon"><i class="fa fa-hourglass text--teal"></i></span>
                        <div class="mini-stat-info">  <span class="actions_time1">0 </span> Czas [h] </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="mini-stat clearfix bg-purple rounded">
                        <span class="mini-stat-icon"><i class="fa fa-percent text--purple"></i></span>
                        <div class="mini-stat-info">  <span class="discounts">0 </span> Rabaty [PLN] </div>
                    </div>
                </div>
            </div>
            <div id="toolbar-report" class="btn-group toolbar-table-widget">
                
                <?=  Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/report/export', 'year' => $year, 'month' => $month]) , 
                            ['class' => 'btn btn-info btn-icon btn-export', 
                             'id' => 'action-export',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "#filter-acc-report-search", 
                             'data-table' => "table-items",
                             'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                            ]) ?>
				<?=  Html::a('<i class="fa fa-file-excel-o"></i>Wsad [CSV]', Url::to(['/accounting/report/exportcsv', 'year' => $year, 'month' => $month]) , 
                            ['class' => 'btn btn-success btn-icon btn-export', 
                             'id' => 'action-export-cvs',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "#filter-acc-report-search", 
                             'data-table' => "table-items",
                             'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                            ]) ?>
                <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-report"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
                
            </div>
            <div class="div-table">
                <table  class="table-widget table-striped table-items header-fixed"  id="table-report"
                        data-toolbar="#toolbar-report" 
                        data-toggle="table-widget" 
                        data-show-refresh="false" 
                        data-show-toggle="true"  
                        data-show-columns="false" 
                        data-show-export="false"  
                        table-page-size="40"
                        data-show-pagination-switch="false"
                        data-pagination="false"
                        data-id-field="id"
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-height="700"
                        data-show-footer="false"
                        data-side-pagination="server"
						data-row-style="rowStyle"
						data-sort-action="asc"
						data-method="get"
						data-search-form="#filter-acc-report-search"

                        data-url=<?= Url::to(['/accounting/report/data']).'?year='.$year.'&month='.$month ?>>
                    <thead>
                        <tr>
                            <th data-field="id" data-visible="false">ID</th>  
                            <th data-field="customer"  data-sortable="false" >Klient</th>  
                            <th data-field="order"  data-sortable="false" >Zlecenie</th>                     
                            <th data-field="employee"  data-sortable="false" >Pracownik</th>                            
                            <th data-field="hours"  data-sortable="false" data-align="right" data-width="70px">Ilość<br /> godzin</th>
                            <th data-field="avg_rate"  data-sortable="false" data-align="right" data-width="80px">Stawka<br /> średnia<sup class="text--blue">1</sup></th>
                            <th data-field="avg_rate_discount"  data-sortable="false" data-align="right" data-width="80px">Stawka<br />realna<sup class="text--blue">2</sup></th>
                            <th data-field="actions"  data-sortable="false" data-align="right" data-width="120px">Czynności <br />merytoryczne</th>
                            <th data-field="costs"  data-sortable="false" data-align="right">Pozostały <br />przychód</th>
                            <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>-->
                        </tr>
                    </thead>
                    <tbody class="ui-sortable">

                    </tbody>  
                </table> 
                <fieldset><legend>Objaśnienia</legend> 
                    <table class="calendar-legend">
                        <tbody>
                            <tr><td class="calendar-legend-icon"><span class="text--purple">kolor</span></td><td>Zlecenie objęte rabatem</td></tr>
                            <tr><td class="calendar-legend-icon"><span class="text--purple">*</span></td><td>rabat w sumie zbiorczej uzupełnia się dopiero po wygenerowaniu faktury</td></tr>
                            <tr><td class="calendar-legend-icon"><sup class="text--blue">1</sup></td><td>Stawka średnia liczona wg typu rozliczenia</td></tr>
                            <tr><td class="calendar-legend-icon"><sup class="text--blue">2</sup></td><td>Stawka realna liczona dla całego rozliczenia z uwzględnieniem rabatu</td></tr>
                        </tbody>
                    </table>
                </fieldset> 
            </div>

        <?php $this->endContent(); ?>
    </div>
</div>



