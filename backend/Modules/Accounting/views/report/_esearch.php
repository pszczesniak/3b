<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-report-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-report']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-report', 'data-form' => '#filter-acc-actions-search' ] ) ?></div> 
        <div class="col-md-4 col-sm-4 col-xs-6"><?= $form->field($model, 'status')->dropDownList(  \backend\Modules\Company\models\CompanyEmployee::listStatus(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-report', 'data-form' => '#filter-acc-actions-search' ] )->label('Status pracownika') ?></div>
        <div class="col-md-4 col-sm-4 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAcc($model->status), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-report', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>        
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
	document.getElementById('accactions-id_department_fk').onchange = function() {
        selectedEmployee = document.getElementById('accactions-id_employee_fk').value;
		selectedStatus = document.getElementById('accactions-status').value;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/department/employees']) ?>?did="+( (this.value) ? this.value : 0)+"&estatus="+selectedStatus+"&list=acc", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_employee_fk').innerHTML = result.dropdown; 
				document.getElementById('accactions-id_employee_fk').value = selectedEmployee; 
            }
        }
       xhr.send();
        return false;
    }
	
	document.getElementById('accactions-id_employee_fk').onchange = function() {
        selectedDepartment = document.getElementById('accactions-id_department_fk').value;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/employee/info']) ?>/"+( (this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_department_fk').innerHTML = result.departments;  
				document.getElementById('accactions-id_department_fk').value = selectedDepartment;     
            }
        }
        xhr.send();
        return false;
    }
	
	document.getElementById('accactions-status').onchange = function() {
		selectedDepartment = document.getElementById('accactions-id_department_fk').value;
		var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/company/department/employees']) ?>?did="+((selectedDepartment) ? selectedDepartment : 0)+"&estatus="+( (this.value) ? this.value : 0)+"&list=acc", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_employee_fk').innerHTML = result.dropdown;  
				document.getElementById('accactions-id_department_fk').innerHTML = result.departments;    
            }
        }
        xhr.send();
        return false;
    }

</script>