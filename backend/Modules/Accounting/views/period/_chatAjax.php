<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-body calendar-task">
    <div class="content">
        <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/accounting/period/note', 'id' => $model->id, 'period' => $period])]) ?>
    </div>
</div>
          
