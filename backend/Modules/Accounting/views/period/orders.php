<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Podsumowanie');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-orders-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-actions" data-form="#filter-acc-actions-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_osearch', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
    
    <div id="toolbar-actions" class="btn-group toolbar-table-widget">
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-actions"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-orders"> 
        <table  class="table table-striped table-items table-widget"  id="table-actions"
                data-toolbar="#toolbar-actions" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-method="get"
				data-search-form="#filter-acc-actions-search"
                data-url=<?= Url::to(['/accounting/period/ordersdata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="period"  data-sortable="false" data-width="70px">Okres</th>      
                    <th data-field="customer"  data-sortable="false" >Klient</th>
                    <th data-field="order" data-sortable="false" data-align="center">Zlecenie</th>
                    <th data-field="actions_s"  data-sortable="false" data-align="right" data-width="250px">Rozliczone</th>
                    <th data-field="actions_nos"  data-sortable="false" data-align="right" data-width="250px">Nierozliczone</th>
                    <!--<th data-field="case"  data-sortable="true" >Sprawa</th>-->
                    <th data-field="invoice"  data-sortable="false" data-align="center" data-width="70px">Faktura</th>
                    <th data-field="note"  data-sortable="false" data-align="center" data-width="40px"><i class="fa fa-comments" data-toggle="tooltip" data-title="Notatki"></i></th>
                    <th data-field="verify"  data-sortable="false" data-align="center" data-width="40px"><i class="fa fa-check-square" data-toggle="tooltip" data-title="Weryfikacja przez pracowników"></i></th>
					<?php if(\Yii::$app->params['env'] == 'dev') { ?>
					<th data-field="cverify"  data-sortable="false" data-align="center" data-width="40px"><i class="fa fa-briefcase"data-toggle="tooltip" data-title="Weryfikacja przez klienta"></i></th>
					<?php } ?>
				</tr>
            </thead>
            <tbody class="no-scroll">

            </tbody>            
        </table>
        <!--<fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nierozliczona umowa z ryczałtem</td></tr>
                </tbody>
            </table>
        </fieldset>-->
    </div>
</div>
<?php $this->endContent(); ?>

	