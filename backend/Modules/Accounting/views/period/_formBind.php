<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
		<div class="panel with-nav-tabs panel-default">
            <div class="panel-heading panel-heading-tab">
                <ul class="nav panel-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Konfiguracja</span></a></li>
                    <li><a data-toggle="tab" href="#tab2b"><i class="fa fa-rotate-left"></i><span class="panel-tabs--text">Historia</span> </a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2a">
                        <fieldset><legend>Wybierz pracowników</legend>
                            <?= $form->field($model, 'managers')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])/*->andWhere('id in (select id_employee_manager_fk from {{%company_department}})')->orderby('lastname collate `utf8_polish_ci`')*/->all(), 'id', 'fullname'), ['prompt' => '-wybierz-', 'class' => 'form-control chosen-select', 'multiple' => true, 'data-placeholder' => 'wybierz pracowników'] )->label(false) ?> 
                        </fieldset>   
                        <fieldset><legend>Wyślij informację</legend>
                            <?= $form->field($model, 'send', [/*'template' => '{input} {label}'*/])->checkbox([/*'id' => 'calendar-all-day'*/]) ?>
                            <?= $form->field($model, 'send_message')->textarea(['rows' => 2, 'placeholder' => 'Wpisz treść wiadomości ...'])->label(false) ?>
                        </fieldset>
                    </div>
                    <div class="tab-pane" id="tab2b">
                        <?= $this->render('_history', ['model' => $history]) ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Save').' [i wyślij]', ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>
