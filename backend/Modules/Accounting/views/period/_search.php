<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-periods-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-periods']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'year_from')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getYears(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-periods', 'data-form' => '#filter-acc-periods-search' ] ) ?>      
		</div>
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'year_to')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getYears(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-periods', 'data-form' => '#filter-acc-periods-search' ] ) ?>       
		</div>
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'month_from')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getMonths(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-periods', 'data-form' => '#filter-acc-periods-search' ] ) ?>      
		</div>
        <div class="col-md-2 col-sm-4">
            <?= $form->field($model, 'month_to')->dropDownList( \backend\Modules\Accounting\models\AccPeriod::getMonths(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-periods', 'data-form' => '#filter-acc-periods-search' ] ) ?>       
		</div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
