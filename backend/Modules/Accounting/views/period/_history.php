<?php
    if(!$model) {
        echo '<div class="alert alert-info">Ta specyfikacja nie została wysłana do akceptacji</div>';
    } else {
        echo '<table class="table table-hover">';
            echo '<thead><tr><td>Pracownik</td><td>Wysłano</td><td>Zwrot</td></thead>';
            echo '<tbody>';
                foreach($model as $key => $value) {
                    echo '<tr><td>'.$value->employee['fullname'].'</td>'
                            .'<td>'.(($value->send_date) ? $value->send_date.'&nbsp;<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value->send_message.'"></i>' : 'nie wysłano').'</td>'
                            .'<td>'.(($value->accept_date) ? $value->accept_date.'&nbsp;<i class="fa fa-comment text--green" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$value->notes.'"></i>' : 'brak zwrotu').'</td></tr>';
                }
            echo '</tbody>';
        echo '</table>';
    }
?>