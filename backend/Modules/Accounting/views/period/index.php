<?php

use yii\helpers\Html;
use yii\helpers\Url;

use frontend\widgets\crm\CrmStatsTable;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Okresy rozliczeniowe');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-period-index', 'title'=>Html::encode($this->title))) ?>
   <?php  /*echo $this->render('_search', ['model' => $searchModel]);*/ ?>

    <div id="toolbar-periods" class="btn-group toolbar-table-widget">
        
        <?php /*Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/action/export']) , 
                    ['class' => 'btn btn-info btn-icon btn-export', 
                     'id' => 'action-export',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                    ])*/ ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-periods"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
        
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-periods"
                data-toolbar="#toolbar-periods" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                table-page-size="40"
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-detail-view="true"
                data-detail-formatter="periodFormatter"

                data-url=<?= Url::to(['/accounting/period/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>  
                    <th data-field="typeFormatter" data-visible="false">formatter</th>
                    <th data-field="year" data-sortable="false" data-width="30px">Rok</th>                 
                    <th data-field="month" data-sortable="false" data-width="40px" data-align="center">Miesiąc</th>                            
                    <th data-field="invoices" data-sortable="false" data-width="40px" data-align="center">Ilość <br />faktur</th>
                    <th data-field="profit" data-sortable="false" data-align="right">Zysk<br /><small> [PLN]</small></th>
                    <th data-field="avg" data-sortable="false" data-width="70px" data-align="right">Śr. stawka<br /><small> [PLN]</small></th>
                    <th data-field="discount" data-sortable="false" data-align="right">Rabaty<br /><small> [PLN]</small></th>
                    <th data-field="cost" data-sortable="false" data-align="right">Koszty <br /><small>[PLN]</small></th> 
                    <th data-field="time" data-sortable="false" data-align="right">Czas [h]</th>
                    <?php if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) { ?>
                    <th data-field="actions" data-sortable="false" data-align="center" data-width="70px" ></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>  
        </table>  
    </div>

<?php $this->endContent(); ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-period-chart', 'title'=>'Raport za ostatnie 12 miesięcy')) ?>
    <div class="grid">
        <div class="col-sm-6">
			<!--<span class="inlinesparkline">1,4,4,7,5,9,10</span>-->
			<h4>Najbardziej dochodowi klienci</h4>
			<?= CrmStatsTable::widget(['dataUrl' => Url::to(['/accounting/period/sorders']) ]) ?>
		</div>
		<div class="col-sm-6" id="chart-main"><?= $this->render('_chart', ['stats' => $stats]) ?></div>
    </div>
    
<?php $this->endContent(); ?>
<?php /*var_dump( $stats['labels']); echo '<br />'.implode(',', $stats['labels']); exit;*/ ?>
