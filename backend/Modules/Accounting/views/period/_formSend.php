<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'action' => Url::to(['/accounting/period/send', 'id' => $model->id]),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'periodSend', 'data-target' => "#modal-grid-item", 'data-table' => "#table-periods", 'data-type' => 'send'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
        <div class="grid">
            <div class="col-sm-6">
                <table class="table table-hover">
                    <thead><tr><th>Pracownik</th><th>Specyfikacje</th></tr></thead>
                    <tbody>
                    <?php
                        foreach($data as $key => $item) {
                            echo '<tr><td>'.$item['lastname'].' '.$item['firstname'].'</td><td align="center">'.$item['total_actions'].'</td></tr>';
                        }
                    ?>
                    </tbody>
               </table>
            </div>
            <div class="col-sm-6">
                <fieldset><legend>Treść wiadomości</legend>
                    <div class="form-group">
                        <textarea rows="6" name="user-comment" class="form-control" id="user-comment" placeholder="Wpisz notatkę..."><?= str_replace("{%period}", $model->period_year."-".( ($model->period_month < 10) ? '0'.$model->period_month : $model->period_month), Yii::$app->params['periodSendInfo'])  ?></textarea>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
            
		
    <div class="modal-footer"> 
        <?= Html::submitButton('Roześlij informacje', ['class' =>'btn btn-info periodSendBtn'])  ?>

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

