<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="modal-body">
    <div class="div-table-settled"></div>
    <table  class="table table-striped table-items header-fixed"  id="table-settled"
            data-toolbar="#toolbar-data" 
            data-toggle="table" 
            data-show-refresh="true" 
            data-search="true" 
            data-show-toggle="false"  
            data-show-columns="false" 
            data-show-export="false"  
            
            data-checkbox-header="true"
            data-response-handler="responseHandlerChecked"

            data-pagination="false"
            data-id-field="id"
            data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
            data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
            data-height="<?= \Yii::$app->params['table-page-height'] ?>"
            data-show-footer="false"
            data-side-pagination="client"
            data-row-style="rowStyle"
            
            data-method="get"
            data-url=<?= Url::to(['/accounting/period/customersdata', 'id' => $model->id]) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="state" data-visible="true" data-checkbox="true" data-formatter="stateFormatter" data-width="20px"></th>
                <th data-field="period" data-visible="false">Okres</th>
                <th data-field="customer" data-sortable="true">Klient</th>
                <th data-field="order" data-sortable="true">Zlecenie</th>
                <th data-field="discount" data-sortable="false" data-align="right" data-editable="true" data-width="70px">Rabat [%]</th>
                <!--<th data-field="actions" data-events="actionEvents"></th>-->
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>          
    </table>      
</div>
<div class="modal-footer"> 
    <?= Html::submitButton('<i class="fa fa-calculator"></i>Generuj faktury', ['class' => 'btn btn-icon btn-sm bg-purple', 'id' => 'invoice-generate']) ?>
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
</div>
