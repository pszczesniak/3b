<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Raport');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-2 col-sm-3">
        <?= $this->render('_filter', ['report' => 'employees', 'year' => $year, 'month' => $month]) ?>
    </div>
    <div class="col-md-10 col-sm-9">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>

        
            <div id="toolbar-data" class="btn-group">
                
                <?=  Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/action/export']) , 
                            ['class' => 'btn btn-info btn-icon btn-export', 
                             'id' => 'action-export',
                             //'data-toggle' => ($gridViewModal)?"modal":"none", 
                             'data-target' => "#modal-grid-item", 
                             'data-form' => "item-form", 
                             'data-table' => "table-items",
                             'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                            ]) ?>
                <button class="btn btn-default" type="button" id="btn-refresh" title="Odśwież"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
                
            </div>
            <div class="div-table">
                <table  class="table table-striped table-items header-fixed"  id="table-data"
                        data-toolbar="#toolbar-data" 
                        data-toggle="table" 
                        data-show-refresh="false" 
                        data-show-toggle="true"  
                        data-show-columns="false" 
                        data-show-export="false"  
                        table-page-size="40"
                        data-show-pagination-switch="false"
                        data-pagination="true"
                        data-id-field="id"
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-height="700"
                        data-show-footer="false"
                        data-side-pagination="client"
                        data-row-style="rowStyle"

                        data-url=<?= Url::to(['/accounting/report/edata']).'?year='.$year.'&month='.$month ?>>
                    <thead>
                        <tr>
                            <th data-field="employee"  data-sortable="true" rowspan="2">Pracownik</th>  
                            <th colspan="6">Godziny</th>
                        </tr>
                        <tr>
                            <th data-field="hours_admin"  data-sortable="true" >administracyjne</th>
                            <th data-field="hours_healt"  data-sortable="true" >L4</th>
                            <th data-field="hours_marketing"  data-sortable="true" >marketingowe</th>
                            <th data-field="hours_holiday"  data-sortable="true" >Urlop, święto</th>
                            <th data-field="hours_1"  data-sortable="true" >niezafakturowane</th>
                            <th data-field="hours_2"  data-sortable="true" >zafakturowane</th>
                        </tr>
         
                    </thead>
                    <tbody class="ui-sortable">

                    </tbody>  
                </table>  
            </div>

        <?php $this->endContent(); ?>
    </div>
</div>
	     				



