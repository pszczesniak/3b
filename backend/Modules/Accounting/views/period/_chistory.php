<?php
    if(!$model) {
        echo '<div class="alert alert-info">Ta specyfikacja nie została wysłana do akceptacji</div>';
    } else {
        echo '<table class="table table-hover">';
            echo '<thead><tr><td>Email</td><td>Wysłano</td><td>Zwrot</td></thead>';
            echo '<tbody>';
				echo '<tr><td>'.$model->email.'</td>'
						.'<td>'.(($model->send_date) ? $model->send_date.'&nbsp;<i class="fa fa-comment text--blue" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$model->send_message.'"></i>' : 'nie wysłano').'</td>'
						.'<td>'.(($model->accept_date) ? $model->accept_date.'&nbsp;<i class="fa fa-comment text--green" data-toggle="popover" data-placement="left" data-original-title="Notatka" data-content="'.$model->notes.'"></i>' : 'brak zwrotu').'</td></tr>';
            echo '</tbody>';
        echo '</table>';
    }
?>