<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="modal-body">

    <h5></h5>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-settlements"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-search="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                
                data-method="get"
                data-url=<?= Url::to(['/accounting/period/customersdata', 'id' => $model->id]) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="customer" data-sortable="true">Klient</th>
                    <th data-field="amount" data-sortable="true">Kwota</th>
                    <th data-field="invoice" data-sortable="false" data-align="center"></th>                    
                    <!--<th data-field="actions" data-events="actionEvents"></th>-->
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>          
        </table>      
    </div>
</div>
