<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'action' => Url::to(['/accounting/period/genall', 'id' => $model->id]),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'periodGen', 'data-target' => "#modal-grid-item", 'data-table' => "#table-periods", 'data-type' => 'gen'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
        <div class="alert alert-info">
			Zgłoś żądanie wygenerowania brakujących faktur i wsyłania ich w jednej paczce na e-mail.<br />
			Realizacja zgłoszenia zostanie zrealizowana po godzinie 22.
		</div>
    </div>
            
		
    <div class="modal-footer"> 
        <?= Html::submitButton('Zgłaszam żądanie', ['class' =>'btn btn-info periodGenBtn'])  ?>

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

