<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-discounts-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-discounts']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-5 col-sm-8 col-xs-8"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-discounts', 'data-form' => '#filter-acc-discounts-search' ] ) ?></div>        
        <div class="col-md-2 col-sm-4 col-xs-4">
			<div class="form-group field-accdiscount-acc_period">
				<label for="accdiscount-acc_period" class="control-label">Okres</label>
				<div class='input-group date' id='datetimepicker_period'>
					<input type='text' class="form-control" id="accdiscount-acc_period" name="AccDiscount[acc_period]" value="<?= $model->acc_period ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
       <div class="col-md-5 col-sm-12 col-xs-12"><?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-discounts', 'data-form' => '#filter-acc-discounts-search' ]) ?></div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
