<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-notes-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-notes']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-3 col-xs-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Accounting\models\AccNote::getTypes(), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-notes', 'data-form' => '#filter-acc-notes-search' ] ) ?></div>          
		<div class="col-md-2 col-sm-3 col-xs-6"><?= $form->field($model, 'id_employee_from_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getListAcc(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-notes', 'data-form' => '#filter-acc-notes-search' ] ) ?></div>        
        <div class="col-md-2 col-sm-3 col-xs-6">
			<div class="form-group field-accnote-acc_period">
				<label for="accnote-acc_period" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="accnote-acc_period" name="AccNote[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-3 col-xs-6">
			<div class="form-group field-accnote-acc_period">
				<label for="accnote-acc_period" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="accnote-acc_period" name="AccNote[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
       <div class="col-md-4 col-sm-12 col-xs-12"><?= $form->field($model, 'note')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-notes', 'data-form' => '#filter-acc-notes-search' ]) ?></div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
