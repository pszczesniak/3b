<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Komentarze i notatki');
$this->params['breadcrumbs'][] = 'Rozliczenia';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="toolbar-notes" class="btn-group toolbar-table-widget">
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-notes"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-notes"
                data-toolbar="#toolbar-notes" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-search="false" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  

                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-order="desc"
                data-method="get"
				data-search-form="#filter-acc-notes-search"
                data-url=<?= Url::to(['/accounting/note/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
					<th data-field="type" data-sortable="false">Dotyczy</th>
                    <!--<th data-field="customer" data-sortable="true">Klient</th> 
                    <th data-field="order" data-sortable="true">Zlecenie</th> -->
                    <th data-field="note" data-sortable="true">Treść</th>
                    <th data-field="date" data-sortable="true">Data</th>
                    <th data-field="creator" data-sortable="false" data-align="right">Pracownik</th>               
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="60px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>          
        </table>      
    </div>
</div>

<?php $this->endContent(); ?>
    
