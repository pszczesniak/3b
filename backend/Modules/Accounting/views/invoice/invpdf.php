<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <link href="<?= \Yii::getAlias('@webroot') ?>/css/_invoice.css" rel="stylesheet">
  </head>
  <body>
    <header class="clearfix">
		<table style="background-color: #fff; padding: 0; margin: 0;">
			<tr>
				<td align="left"><img src="<?= \Yii::getAlias('@webroot') ?>/images/logo_dark.jpg"></td>
				<td align="right">
					<h1 id="invoice-h1"><?= $model->system_no ?></h1>
					<div class="date">Date wystawienia: <?= date('Y-m-d', strtotime($model->created_at)) ?></div>
					<div class="date">Data płatności: <?= date('Y-m-d', strtotime($model->created_at, '+7 day')) ?></div>
				</td>
			</tr>
		</table>
      
    </header>
    <main>
		<table style="background-color: #fff; padding: 0; margin: 0;">
			<tr>
				<td align="left" valign="top" width="50%" style="font-size:14px; border-left: 6px solid #1f398e; padding-left: 6px;">
					  <div class="to">NABYWCA</div>
					  <h2 class="name"><?= $model->order['customer']['name'] ?></h2>
					  <div class="address"><?= $model->order['customer']['address'] ?></div>
					  <div class="address"><?= $model->order['customer']['postal_code'] .' '. $model->order['customer']['city'] ?></div>
					  <!--<div class="email"><a href="mailto:john@example.com">john@example.com</a></div>-->
				</td>
				<td align="right" valign="top" width="50%" style="font-size:14px; border-right: 6px solid #1f398e; padding-right: 6px;">
					<div class="to">SPRZEDAJĄCY</div>
					<h2 class="name">Sadkowski i Wspólnicy</h2>
					<div class="address">ul. Grzybowska 4, lok U-7A</div>
					<div class="address">00-131 Warszawa</div>
					<!--<div><a href="mailto:kontakt@siw.pl">kontakt@siw.pl</a></div> --> 					
				</td>
			</tr>
		</table>
		<br /><br />
		<table class="items" border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th>#</th>
					<th>Nazwa</th>
					<!--<th>PKWiU</th>-->
					<th>Ilość</th>
					<th>Jm</th>
					<th>Cena</th>					
					<th>Wartość<br />netto</th>
					<th>VAT <br />[%]</th>
					<th>Kwota<br />VAT</th>
					<th>Wartość<br />brutto</th>
				</tr>
			</thead>
			<tbody>
				<!--<tr>
					<td class="no">01</td>
					<td class="desc"><h3>Website Design</h3>Creating a recognizable design solution based on the company's existing visual identity</td>
					<td class="unit">$40.00</td>
					<td class="qty">30</td>
					<td class="total">$1,200.00</td>
				</tr>-->
				<?php $i = 1; $amountNet = 0; $amountGross = 0; $amountVat = 0; ?>
				<?php foreach($model->profit as $key => $item) {
					echo '<tr>'
							.'<td class="no">'.($i).'</td>'
							.'<td class="desc">'.$item->item_name.'</td>'
							//.'<td class="desc"></td>'
							.'<td class="center">1</td>'
							.'<td class="center">szt.</td>'
							.'<td class="right">'.number_format($item->amount_net, 2, "," ,  " ").'</td>'
							.'<td class="total">'.number_format($item->amount_net, 2, "," ,  " ").'</td>'
							.'<td class="center">0</td>'
							.'<td class="total">'.number_format($item->amount_vat, 2, "," ,  " ").'</td>'
							.'<td class="total">'.number_format($item->amount_gross, 2, "," ,  " ").'</td>'
						.'</tr>';
						++$i;
						$amountNet += $item->amount_net; $amountGross += $item->amount_gross; $amountVat += $item->amount_vat;
					} 	       
				?>
				<?php foreach($model->costs as $key => $item) {
					echo '<tr>'
							.'<td class="no-red">'.($i).'</td>'
							.'<td class="desc">'.$item->item_name.'</td>'
							//.'<td class="desc"></td>'
							.'<td class="center">1</td>'
							.'<td class="center">szt.</td>'
							.'<td class="right">'.number_format($item->amount_net, 2, "," ,  " ").'</td>'
							.'<td class="total">'.number_format($item->amount_net, 2, "," ,  " ").'</td>'
							.'<td class="center">0</td>'
							.'<td class="total">'.number_format($item->amount_vat, 2, "," ,  " ").'</td>'
							.'<td class="total">'.number_format($item->amount_gross, 2, "," ,  " ").'</td>'
						.'</tr>';
						++$i;
						$amountNet += $item->amount_net; $amountGross += $item->amount_gross; $amountVat += $item->amount_vat;
					} 
				?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4"></td>
					<td colspan="3" class="right">RAZEM NETTO</td>
					<td colspan="2" class="right"><?= number_format($amountNet, 2, "," ,  " ") ?></td>
				</tr>
				<tr>
					<td colspan="4"></td>
					<td colspan="3" class="right">RAZEM VAT</td>
					<td colspan="2" class="right"><?= number_format($amountVat, 2, "," ,  " ") ?></td>
				</tr>
				<tr>
					<td colspan="4"></td>
					<td colspan="3" class="right">RAZEM BRUTTO</td>
					<td colspan="2" class="right"><?= number_format($amountGross, 2, "," ,  " ") ?></td>
				</tr>
			</tfoot>
		</table>
		<!--<div id="thanks">Thank you!</div>
		<div id="notices">
			<div>NOTICE:</div>
			<div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
		</div>-->
    </main>
    <footer>
      Faktura została wygenerowana w systemie Ultima Ratio.
    </footer>
  </body>
</html>