<?php

use yii\helpers\Html;
use yii\helpers\Url;

use frontend\widgets\crm\CrmStatsTable;

?>

    
<script src="/js/chart/Chart.bundle.js"></script>
<script src="/js/chart/utils.js"></script>

<canvas id="canvas"></canvas>
    
<?php /*var_dump( $stats['labels']); echo '<br />'.implode(',', $stats['labels']); exit;*/ ?>
<script>
    var chartData = {
        labels: [<?= implode(',', $stats['labels']) ?>],
        datasets: [{
            type: 'line',
            label: 'Średnia stawka',
            borderColor: window.chartColors.blue,
            borderWidth: 2,
            fill: false,
            data: [<?= implode(',', $stats['rates']) ?>]
        }, {
            type: 'bar',
            label: 'Wartość z faktur [w tys]',
            backgroundColor: window.chartColors.red,
            data: [<?= implode(',', $stats['profits']) ?>],
            borderColor: 'white',
            borderWidth: 2
        }]

    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myMixedChart = new Chart(ctx, {
            type: 'bar',
            data: chartData,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Statystyki zysku i średniej stawki'
                },
                tooltips: {
                    mode: 'index',
                    intersect: true
                }
            }
        });
    };

    /*document.getElementById('randomizeData').addEventListener('click', function() {
        chartData.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });
        });
        window.myMixedChart.update();
    });*/
</script>