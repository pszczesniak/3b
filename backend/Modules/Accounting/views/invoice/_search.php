<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-invoices-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-invoices']
    ]); ?>
    
    <div class="grid">
        <!--<div class="col-md-2 col-sm-4">
			<div class="form-group field-accinvoice-date_from">
				<label for="accinvoice-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="accinvoice-date_from" name="AccInvoice[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-4">
			<div class="form-group field-accinvoice-date_to">
				<label for="accinvoice-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="accinvoice-date_to" name="AccInvoice[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>-->
		<div class="col-md-3 col-sm-4 col-xs-12">
            <div class="form-group field-accinvoice-acc_period">
                <label for="accinvoice-acc_period" class="control-label">Okres</label>
                <div class='input-group date' id='datetimepicker_period'>
                    <input type='text' class="form-control" id="accinvoice-date_issue" name="AccInvoice[date_issue]" value="<?= $model->date_issue ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12"><?= $form->field($model, 'id_currency_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccCurrency::find()->all(), 'id', 'currency_symbol'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-invoices', 'data-form' => '#filter-acc-invoices-search' ] ) ?></div>                
        <div class="col-md-6 col-sm-4 col-xs-12"><?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-invoices', 'data-form' => '#filter-acc-invoices-search' ] ) ?></div>        
    </div> 

    <?php ActiveForm::end(); ?>

</div>
