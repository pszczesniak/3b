<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/accounting/action/cmsettle']),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
       <?php
            if(count($periods) > 0) {
                //echo '<div class="alert alert-info">Liczba okresów do rozliczenia <b>'.count($periods).'</b> </div>';
                foreach($periods as $key => $period) {
                    echo '<a class="btn btn-sm btn-icon bg-purple" href="'.Url::to(['/accounting/invoice/generate', 'id' => $id]).'?period='.$period['period'].'"><i class="fa fa-calculator"></i>'.$period['period'].'</a>&nbsp;&nbsp;';
                }
            } else {
                echo '<div class="alert alert-danger">Brak czynności do rozliczenia.</div>';
            }
        ?>
    </div>
            
		
    <div class="modal-footer"> 
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

