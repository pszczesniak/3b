<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
    <div class="div-table-settled"></div>
    <table  class="table header-fixed table-widget table-settled"  id="table-settled"
        data-toolbar="#toolbar-data" 
        data-toggle="table" 
        data-show-refresh="true" 
        data-search="true" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"  
        
        data-checkbox-header="true"
        data-response-handler="responseHandlerChecked"

        data-pagination="false"
        data-id-field="id"
        data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
        data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
        data-height="<?= \Yii::$app->params['table-page-height'] ?>"
        data-show-footer="false"
        data-side-pagination="client"
        data-row-style="rowStyle"
        
        data-method="get"
        data-url=<?= Url::to(['/accounting/invoice/settlement', 'id' => $id]) ?>>
        <thead>
            <tr>
                <th data-field="state" data-visible="true" data-checkbox="true" data-formatter="stateFormatter" data-width="20px"></th>
                <th data-field="id" data-visible="false">ID</th>      
                <th data-field="confirm" data-visible="false"></th>                    
                <th data-field="order"  data-sortable="true" >Zlecenie</th>
                <th data-field="period"  data-sortable="true" data-widt="85px">Okres</th>
                <th data-field="discount"  data-sortable="true" data-align="right" data-editable="true" data-widt="70px">Rabat [%]</th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>            
    </table>
</div>
<div class="modal-footer"> 
    <?= Html::submitButton('<i class="fa fa-calculator"></i>Generuj faktury', ['class' => 'btn btn-icon btn-sm bg-purple', 'id' => 'invoice-generate']) ?>
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
</div>

