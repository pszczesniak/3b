<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\accounting\ActionsTable;

use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\Company */

$this->title = $model->system_no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr faktur'), 'url' => Url::to(['/accounting/invoice/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'company-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="<?= Url::to(['/accounting/invoice/exportinv', 'id' => $model->id]) ?>?employees=0" class="btn btn-icon btn-default btn-flat export-file" title="Eksportuj do pliku Excel"><i class="fa fa-file-excel-o text--green"></i> Eksport</a>
                <a href="<?= Url::to(['/accounting/invoice/printpdf', 'id' => $model->id]) ?>?employees=0" class="btn btn-icon btn-default btn-flat export-file" title="Eksportuj do pliku PDF"><i class="fa fa-file-pdf-o text--red"></i> Eksport</a>
                <a href="<?= Url::to(['/accounting/invoice/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm"><i class="fa fa-trash"></i> Usuń</a>
                <br />
                <div class="form-group"> <label><input id="withEmployees" name="withEmployees" value="1" type="checkbox" onchange='handleChange(this);'> eksportuj dane z informacją o pracownikach</label> </div>
            </div>
        </div>
    </div>
    <div class="grid invoice-list">
        <div class="col-sm-4 col-xs-12">
            <img src="/images/logo.png" alt="">
        </div>
        <div class="col-sm-4 col-xs-12">
            <h4 class="text--grey">KLIENT</h4>
            <p><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->customer['id']]) ?>"><?= $model->customer['name'] ?></a></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <h4 class="text--purple">FAKTURA</h4>
            <ul class="unstyled">
                <li>Numer		: <strong><?= $model->system_no ?></strong></li>
                <li>Data		: <?= $model->date_issue ?></li>
            </ul>
        </div>
    </div>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Pozycje</span></a></li>
                <li><a data-toggle="tab" href="#tab2"><i class="fa fa-history"></i><span class="panel-tabs--text">Czynności</span> </a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>nazwa</th>    
                                    <th>zlecenie</th>                         
                                    <th class="align-right">kwota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $totalNet = 0; $totalVat = 0; $totalGross = 0;
                                    foreach($model->items as $key => $value) {
                                        $totalNet   += $value->amount_net; 
                                        $totalVat   += ($value->amount_gross-$value->amount_net); 
                                        $totalGross += $value->amount_gross;
                                        //echo    '<tr class="'. ( ($value->is_cost) ? "bg-red2" : '') .'">'
                                        echo    '<tr '.( ($value->is_cost) ? 'class="text--red"' : '').'>'
                                                    .'<td>'.($key+1).'</td>'
                                                    .'<td>'.$value->item_name.'</td>'
                                                    .'<td><a href="'.Url::to(['/accounting/order/view', 'id' => $value->id_order_fk]).'" title="Przejdź do karty zlecenia">'.$value->order['type']['type_symbol'].'</a></td>'
                                                    .'<td class="align-right">'.number_format ($value->amount_net, 2, "," ,  " ").'</td>'
                                                .'</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="grid">
                        <div class="col-lg-8"> </div>
                        <div class="col-lg-4 invoice-block">
                            <ul class="unstyled amounts">
                                <!--<li><strong>Czynności merytoryczne: </strong> <?= number_format ($totalNet, 2, "," ,  " ") ?></li>
                                <li><strong>Czynności pozamerytoryczne: </strong><?= number_format ($totalVat, 2, "," ,  " ") ?></li>-->
                                <li><strong>Razem: </strong> <?= number_format ($totalNet, 2, "," ,  " ") ?></li>
                                <?php /*if($model->discount == 0) {*/ ?>
                                <li class="text--purple"><strong>Rabat: </strong><?= number_format ($model->discount, 2, "," ,  " ") ?></li>
                                <li class="text--teal"><strong>Do zapłaty: </strong><?= number_format ( ($totalNet-$model->discount), 2, "," ,  " ") ?></li>
                                <?php /*}*/ ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <?= ActionsTable::widget(['dataUrl' => Url::to(['/accounting/invoice/actions', 'id' => $model->id]), 'invoice' => true ]) ?>
                </div>
            </div>
        </div>
    </div>
    
    <!--<div class="text-center invoice-btn">
        <a class="btn btn-danger btn-lg btn-xs"><i class="fa fa-check"></i> Submit Invoice </a>
        <a class="btn btn-info btn-lg btn-xs" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </a>
    </div>-->
    
<?php $this->endContent(); ?>

<script>
    function handleChange(checkbox) {
        var buttons = document.getElementsByClassName("export-file"),
        len = buttons !== null ? buttons.length : 0,
        i = 0;
        
        if(checkbox.checked == true){
            for(i; i < len; i++) {
                href = buttons[i].getAttribute('href'); 
                buttons[i].setAttribute("href", href.replace("employees=0", "employees=1"));
            }
        } else{
            for(i; i < len; i++) {
                href = buttons[i].getAttribute('href'); 
                buttons[i].setAttribute("href", href.replace("employees=1", "employees=0"));
            }
        }
    }
</script>