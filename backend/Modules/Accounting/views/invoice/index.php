<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Faktury');
$this->params['breadcrumbs'][] = 'Rozliczenia';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-report-index', 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="toolbar-invoices" class="btn-group toolbar-table-widget">

        <?= ( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/invoice/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-invoices-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]):'' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-invoices"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-invoices"
                data-toolbar="#toolbar-invoices" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-acc-invoices-search"
                data-url=<?= Url::to(['/accounting/invoice/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>  
                    <th data-field="customer" data-sortable="true" >Klient</th> 
                    <th data-field="order" data-sortable="true" data-align="center">Zlecenie</th> 
                    <th data-field="no" data-sortable="false" data-width="60px">Numer</th>       
                    <th data-field="period" data-sortable="false" data-width="40px" data-align="center">Okres</th>     
                    <th data-field="currency" data-sortable="false" data-width="30px" data-align="center">Waluta</th>       
                    <!--<th data-field="issue" data-sortable="true">Wygenerowano</th>-->
                    <th data-field="net" data-sortable="false" data-align="right">Kwota</th>
                    <th data-field="discount" data-sortable="false" data-align="right">Rabat</th>
                    <th data-field="actions" data-sortable="false" data-align="center" data-events="actionEvents"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>  
        </table>  
    </div>

<?php $this->endContent(); ?>
    
