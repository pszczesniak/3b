<!DOCTYPE html>
<html lang="pl">
  <body>
    <header class="clearfix"> <div id="logo">  <img src="/images/logo_dark.jpg"> </div> </header>
    <div id="details" class="clearfix">
       
      <table repeat_header="1"  autosize="1" style="overflow: auto hidden visible wrap">
        <thead repeat_header="1">
          <tr>
            <th>Data</th>
            <th>Opis</th>
            <th>Czas</th>
            <th>Stawka</th>
            <th>Kwota</th>
            <?php if($employees) echo '<th>Pracownik</th>'; ?>
          </tr>
        </thead>
        <tbody>
            <?php
            foreach($model->actions as $key => $record){ 
                if($record->type_fk != 4) {
                   /* if(!$record->acc_limit) $record->acc_limit = 0;
                    if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
                    echo '<tr>'
                            .'<td>'.$record->action_date.'</td>'
                            .'<td style="text-align: justify;">'.$record->description.'</td>'
                            .'<td class="total">'.$record->confirm_time.'</td>'
                            .'<td class="total">'.number_format($record->unit_price, 2, "," ,  " ").'</td>'
                            .'<td class="total">'.number_format(round($record->unit_price*($record->confirm_time-$record->acc_limit), 2), 2, "," ,  " ").'</td>'
                            .( ($employees) ? '<td>'.$record->employee['fullname'].'</td>' : '' )
                          .'</tr>';
                }
            } 
            ?> 
     
        </tbody>
        <tfoot>
          <?php
            $total = 0; $profit = 0; $discount = 0; $costs = 0;
            foreach($model->profit as $key => $record){  
                $total += $record->amount_net; $profit += $record->amount_net;
                echo '<tr>'
                        .'<td style="background-color: #fff;"></td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.$record->item_name.'</td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($record->amount_net, 2, "," ,  " ").'</td>'
                        .( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                      .'</tr>';
            } 
          ?>
          <tr><td style="background-color: #fff;"></td><td colspan="2" class="text-green">Razem</td><td colspan="2" class="text-green"><?= number_format($total, 2, "," ,  " ") ?></td></tr>
          <?php
                if($model->discount) {
                    $discount = round(($profit*$model->discount/100), 2);
                    echo '<tr>'
                        .'<td style="background-color: #fff;"></td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>Rabat '.$model->discount.'%</td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($discount, 2, "," ,  " ").'</td>'
                        .( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                      .'</tr>';
  
                    //echo '<tr><td style="background-color: #fff;"></td><td colspan="2" class="text-green">Do zapłaty</td><td colspan="2" class="text-green">'.number_format(($total-$model->discount), 2, "," ,  " ") .'</td></tr>';
                }
                
                foreach($model->costs as $key => $record){  
                    $costs += $record->amount_net;
                    echo '<tr>'
                            .'<td style="background-color: #fff;"></td>'
                            .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.$record->item_name.'</td>'
                            .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($record->amount_net, 2, "," ,  " ").'</td>'
                            .( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                          .'</tr>';
                } 
                if($costs != 0 || $discount != 0)
                echo '<tr><td style="background-color: #fff;"></td><td colspan="2" class="text-green">Do zapłaty</td><td colspan="2" class="text-green">'.number_format(($profit-$discount+$costs), 2, "," ,  " ") .'</td></tr>';
          ?>
        </tfoot>
      </table>
     
  </body>
</html>