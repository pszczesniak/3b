<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-items"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        
		<div class="grid">
			<div class="col-sm-8 col-xs-8"><?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?></div>
			<div class="col-sm-4 col-xs-4"><?= $form->field($model, 'id_vat_fk')->dropDownList(\backend\Modules\Accounting\models\AccTax::listTaxes(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>                
		</div>
        <!--                
		<fieldset><legend>Stawka za czynności</legend>
			<div class="grid">    
				<div class="col-xs-4">
					<?= $form->field($model, 'amount_net', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->invoice['currency'].'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number'] ) ?>
				</div>
				<div class="col-xs-4">
					<?= $form->field($model, 'amount_net', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->invoice['currency'].'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number'] ) ?>
				</div>
				<div class="col-xs-4">
					<?= $form->field($model, 'amount_net', ['template' => '{label} <div class="input-group "> {input} <span class="input-group-addon"><span class="currencyName">'.$model->invoice['currency'].'</span>/h</span> </div>  {error}{hint} '])->textInput( ['class' => 'form-control number'] ) ?>
				</div>
			</div>
		</fieldset>
		-->
	</div>
                    

    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

