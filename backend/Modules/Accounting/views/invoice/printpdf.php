<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
  <div class="wrapper wrapper-content p-xl">
    <div class="ibox-content p-xl">
        <table class="table table-bordered">
        <tbody>
          <tr>
            <?php if($employees) echo '<th>Pracownik</th>'; ?>
            <th>Data</th>
            <th>Opis</th>
            <th>Czas</th>
            <th>Stawka</th>
            <th>Kwota</th>
          </tr>

            <?php
            foreach($model->actions as $key => $record){ 
                if($record->type_fk != 4 && $record->type_fk != 5 && $record->type_fk != 6) {
                   /* if(!$record->acc_limit) $record->acc_limit = 0;
                    if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
					$price = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] == 2 && $record->acc_limit_price) ? $record->acc_limit_price : $record->unit_price;
                    $price = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] != 2 &&  $record->acc_limit == $record->confirm_time) ? 0 : $price;
                    $inLimit = ($model->order['id_dict_order_type_fk'] == 2 && $model->order['id_settlement_type_fk'] == 2 && $record->acc_limit_price) ? 0 : $record->acc_limit; 
                    echo '<tr>'
                            .( ($employees) ? '<td class="desc">'.$record->employee['fullname'].'</td>' : '' )
                            .'<td class="total" class="date">'.$record->action_date.'</td>'
                            .'<td class="desc" >'.$record->description.'</td>'
                            .'<td class="total">'.$record->confirm_time.'</td>'
                            .'<td class="total">'.number_format($price, 2, "," ,  " ").'</td>'
                            .'<td class="total">'.number_format(round($price*($record->confirm_time-$inLimit), 2), 2, "," ,  " ").'</td>'
                          .'</tr>';
                }
                if($record->type_fk == 5 && $record->unit_price_invoice) {
                   /* if(!$record->acc_limit) $record->acc_limit = 0;
                    if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
                    echo '<tr>'
                            .( ($employees) ? '<td class="desc">'.$record->name.'</td>' : '' )
                            .'<td class="total" class="date">'.$record->action_date.'</td>'
                            .'<td class="desc" >'.$record->description.'</td>'
                            .'<td class="total">'.$record->confirm_time.'</td>'
                            .'<td class="total">0,00</td>'
                            .'<td class="total">'.number_format(round($record->unit_price_invoice, 2), 2, "," ,  " ").'</td>'                   
                          .'</tr>';
                }
                if($record->type_fk == 6) {
                   /* if(!$record->acc_limit) $record->acc_limit = 0;
                    if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
                    echo '<tr style="color: blue;">'
                            .( ($employees) ? '<td class="desc">'.$record->name.'</td>' : '' )
                            .'<td class="total" class="date">'.$record->action_date.'</td>'
                            .'<td class="desc" >'.$record->description.'</td>'
                            .'<td class="total">'.$record->confirm_time.'</td>'
                            .'<td class="total">0,00</td>'
                            .'<td class="total">'.number_format(round(-1*$record->unit_price, 2), 2, "," ,  " ").'</td>'                   
                          .'</tr>';
                }
            } 
            ?> 
     
        </tbody>
        <tfoot>
          <?php
            $total = 0; $profit = 0; $discount = 0; $costs = 0;
            foreach($model->profit as $key => $record){  
                $total += $record->amount_net; $profit += $record->amount_net;
                echo '<tr>'
                        //.'<td style="background-color: #fff;"></td>'
                        .'<td colspan="'.(($employees) ? 4 : 3).'" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.$record->item_name.'</td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($record->amount_net, 2, "," ,  " ").'</td>'
                        //.( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                      .'</tr>';
            } 
          ?>
          <tr><td colspan="<?= (($employees) ? 4 : 3) ?>" class="text-green">Razem</td><td colspan="2" class="text-green"><?= number_format($total, 2, "," ,  " ") ?></td></tr>
          <?php
                if($model->discount) {
                    $discount = round(($profit*$model->discount/100), 2);
                    echo '<tr>'
                        //.'<td style="background-color: #fff;"></td>'
                        .'<td colspan="'.(($employees) ? 4 : 3).'" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>Rabat '.$model->discount.'%</td>'
                        .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($discount, 2, "," ,  " ").'</td>'
                        //.( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                      .'</tr>';
  
                    //echo '<tr><td style="background-color: #fff;"></td><td colspan="2" class="text-green">Do zapłaty</td><td colspan="2" class="text-green">'.number_format(($total-$model->discount), 2, "," ,  " ") .'</td></tr>';
                }
                
                foreach($model->costs as $key => $record){  
                    $costs += $record->amount_net;
                    echo '<tr>'
                            //.'<td style="background-color: #fff;"></td>'
                            .'<td colspan="'.(($employees) ? 4 : 3).'" class="total" '.(($record->is_cost) ? ' style="color: red;"' : '').'>'.$record->item_name.'</td>'
                            .'<td colspan="2" class="total" '.(($record->is_cost) ? 'style="color: red;"' : '').'>'.number_format($record->amount_net, 2, "," ,  " ").'</td>'
                            //.( ($employees) ? '<td style="background-color: #fff;"></td>' : '' )
                          .'</tr>';
                } 
                if($costs != 0 || $discount != 0)
                echo '<tr><td colspan="'.(($employees) ? 4 : 3).'" class="text-green">Do zapłaty</td><td colspan="2" class="text-green">'.number_format(($profit-$discount+$costs), 2, "," ,  " ") .'</td></tr>';
          ?>
          <?php if($model->gratis) { ?>
            <tr><td colspan="<?= ($employees) ? 6 : 5 ?>" class="gratis">GRATIS</td></tr>
            <?php
            foreach($model->gratis as $key => $record){ 
                if($record->type_fk != 4) {
                   /* if(!$record->acc_limit) $record->acc_limit = 0;
                    if($record->confirm_time == $record->acc_limit && $record->is_confirm == 1) { 
                        $record->unit_price = 0;
                    } */
                    echo '<tr class="gratis-items">'
                            .( ($employees) ? '<td class="desc" style="color: #c4c2c2;">'.$record->employee['fullname'].'</td>' : '' )
                            .'<td class="total" class="date" style="color: #c4c2c2;">'.$record->action_date.'</td>'
                            .'<td class="desc" style="color: #c4c2c2;">'.$record->description.'</td>'
                            .'<td class="total" style="color: #c4c2c2;">'.$record->confirm_time.'</td>'
                            .'<td class="total" style="color: #c4c2c2;">0</td>'
                            .'<td class="total" style="color: #c4c2c2;">gratis</td>'
                          .'</tr>';
                }
            } 
            ?> 
          <?php } ?>
        </tfoot>
      </table>
    </div>
</div>
</body>
</html>