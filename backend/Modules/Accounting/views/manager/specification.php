<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Specyfikacja czynności');
$this->params['breadcrumbs'][] = 'Moduł rozliczeń';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'com-meeting-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-actions" data-form="#filter-acc-actions-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_ssearch', ['model' => $searchModel, 'manager' => $manager]); ?>
        </div>
    </fieldset>
    
    <div class="grid">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-teal rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--teal"></i></span>
                <div class="mini-stat-info">  <span class="actions_time1">0 </span> Całkowity czas [h] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-blue rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--blue"></i></span>
                <div class="mini-stat-info">  <span class="actions_time2">0 </span> Czas rozliczony [h] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-green rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
                <div class="mini-stat-info">  <span class="actions_amount">0 </span> Szacowany przychód [PLN] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-red rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--red"></i></span>
                <div class="mini-stat-info">  <span class="actions_cost">0 </span> Całkowity koszt [PLN] </div>
            </div>
        </div>
        <!--<div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-purple rounded">
                <span class="mini-stat-icon"><i class="fa fa-comments text--purple"></i></span>
                <div class="mini-stat-info">  
                    <span class="actions_send"> 
                       0
                    </span> 
                    Notatki <i class="fa fa-info-circle text--navy" data-toggle="tooltip" data-title="Dostępne po wybraniu okresu i zlecenia"></i></div>
            </div>
        </div>-->
    </div>
    <div id="toolbar-actions" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/manager/sexport']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-actions-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='fa fa-print'></i>Eksportuj"
                ]) ?>
      
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-actions"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-actions">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-actions"
                data-toolbar="#toolbar-actions" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                
                data-checkbox-header="true"
                data-maintain-selected="true"
                data-response-handler="responseHandlerChecked"
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-action="asc"
                data-method="get"
				data-search-form="#filter-acc-actions-search"
                data-url=<?= Url::to(['/accounting/manager/sdata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>  
                    <th data-field="type_action" data-visible="false">TYPE</th>    
                    <th data-field="confirm" data-visible="false"></th>                    
                    <th data-field="employee"  data-sortable="true" >Pracownik</th>
                    <th data-field="action_date"  data-sortable="true" >Data</th>
                    <th data-field="unit_time"  data-sortable="false" data-footer-formatter="sumFormatter" data-align="right" data-editable="true"  data-editable-display-formatter="timeFormatter">Czas</th>
                    <!--<th data-field="price"  data-sortable="true" data-align="right" >Stawka </th>
                    <th data-field="amount"  data-sortable="true" data-footer-formatter="sumFormatter" data-align="right" >Kwota </th>-->
                    <th data-field="description"  data-sortable="false" data-editable-type="textarea" data-editable="true">Opis</th>
                    <th data-field="service"  data-sortable="false" data-align="center">Typ</th>
                    <!--<th data-field="case"  data-sortable="true" >Sprawa</th>-->
                    <th data-field="cost"  data-sortable="false" data-footer-formatter="sumFormatter" data-align="right">Koszt</th>
                    <th data-field="customer"  data-sortable="false" >Klient</th>
                    <th data-field="order"  data-sortable="false" data-align="center"></th>
                    <th data-field="invoice"  data-sortable="false" data-align="center"></th>
					<!--<th data-field="period"  data-sortable="false" data-align="center"></th>-->
                    <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>-->
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="70px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <!--<tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>-->
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Czynności przesunięcie na następny okres rozliczeniowy</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
<?php $this->endContent(); ?>

	