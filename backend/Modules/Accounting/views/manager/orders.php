<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Podsumowanie');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-orders-index', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
        <div class="col-sm-8 col-xs-12">
            <fieldset>
                <legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-orders" data-form="#filter-acc-orders-search"><i class="fa fa-eraser text--teal"></i></button>
                    <a aria-controls="orders-filter" aria-expanded="true" href="#orders-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </legend>
                <div id="orders-filter" class="collapse in">
                    <?php  echo $this->render('_osearch', ['model' => $searchModel]); ?>
                </div>
            </fieldset>
        </div>
        <div class="col-sm-4 col-xs-12">
            <fieldset>
                <legend>Akceptacja
                    <a aria-controls="orders-accept" aria-expanded="true" href="#orders-accept" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                        <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                    </a>
                </legend>
                <div id="orders-accept" class="collapse in">
                    <div class="mini-stat clearfix bg-purple rounded">
                        <span class="mini-stat-icon"><i class="fa fa-send text--purple"></i></span>
                        <div class="mini-stat-info">  
                            <span class="actions_send"> 
                                <?php if(count($periods) == 0) echo 'brak oczeukjących'; ?>
                                <?php 
                                    foreach($periods as $period) {
                                        $periodStr = $period->period['period_year'].'-'.( ($period->period['period_month'] < 10) ? '0'.$period->period['period_month'] : $period->period['period_month']);
                                        echo '<a href="'.Url::to(['/accounting/manager/acceptform', 'id' => $period->id]).'" class="btn bg-green gridViewModal" id="period-accept-'.$period->id.'" data-label="Wyślij akceptację czynności do księgowości" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-send\'></i>'.Yii::t('app', 'Wyślij akceptację czynności do księgowości').'" title="Wyślij akceptację czynności do księgowości"><i class="fa fa-check"></i> '.$periodStr.'</a>&nbsp';
                                    }
                                ?>
                            </span> 
                            Akceptacja czynności</div>
                    </div>
                </div>
            </fieldset> 
        </div>
    </div>
    
    <div id="toolbar-orders" class="btn-group toolbar-table-widget">
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-orders"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-orders"> 
        <table  class="table table-striped table-items table-widget"  id="table-orders"
                data-toolbar="#toolbar-orders" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-method="get"
				data-search-form="#filter-acc-orders-search"
                data-url=<?= Url::to(['/accounting/manager/ordersdata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="period"  data-sortable="false" >Okres</th>      
                    <th data-field="customer"  data-sortable="false" >Klient</th>
                    <th data-field="order" data-sortable="false" data-align="center">Zlecenie</th>
                    <th data-field="actions_s"  data-sortable="false" data-align="right">Rozliczone</th>
                    <th data-field="actions_nos"  data-sortable="false" data-align="right">Nierozliczone</th>
                    <!--<th data-field="case"  data-sortable="true" >Sprawa</th>-->
                    <th data-field="note"  data-sortable="false" data-align="center" data-width="50px"><i class="fa fa-comments"></i></th>
                    <th data-field="accept"  data-sortable="false" data-align="center" data-width="50px"><i class="fa fa-check-square"></i></th>
                </tr>
            </thead>
            <tbody class="no-scroll">

            </tbody>            
        </table>
        <!--<fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nierozliczona umowa z ryczałtem</td></tr>
                </tbody>
            </table>
        </fieldset>-->
    </div>
</div>
<?php $this->endContent(); ?>

	