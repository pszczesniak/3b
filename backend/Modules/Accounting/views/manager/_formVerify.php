<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-orders"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">  
        <fieldset><legend>Informacja z działu rozliczeń</legend>
            <?php if($model->send_date) { ?>
                <div class="alert alert-info"><i class="fa fa-send text--blue"></i>&nbsp;<?= $model->send_date.' "'.$model->send_message.'"'?></div>
            <?php } else { ?>
                <div class="alert bg-purple2 text--purple">Do weryfikacji specyfikacji zostałeś przypisany przez <?= $model->creator['fullname']. ' ['.$model->created_at.']' ?> </div>
            <?php } ?>
            <?php if($model->is_accept) { ?>
                <div class="alert alert-success"><i class="fa fa-check"></i>&nbsp;Zweryfikowano <?= $model->accept_date ?>: <?= $model->notes ?></div>
            <?php } ?>
        </fieldset>
        <?php if(!$model->is_accept) { ?>
        <fieldset><legend>Wyślij weryfikację</legend>
            <?= $form->field($model, 'notes')->textarea(['rows' => 2, 'placeholder' => 'Wpisz odpowiedź'])->label(false) ?>
        </fieldset> 
        <?php } ?>                 
	</div>
    <div class="modal-footer"> 
        <?= (!$model->is_accept) ? Html::submitButton('Wyślij', ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) : '' ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>
