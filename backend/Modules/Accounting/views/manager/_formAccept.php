<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'action' => Url::to(['/accounting/manager/accept', 'id' => $model->id]),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'periodSend', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions", 'data-type' => 'accept', 'data-id' => $model->id],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
        <fieldset><legend>Informacja do księgowości</legend>
            <div class="form-group">
                <textarea rows="6" name="user-comment" class="form-control" id="user-comment" placeholder="Wpisz notatkę..."></textarea>
            </div>
        </fieldset>  
    </div>
   
    <div class="modal-footer"> 
        <?= Html::submitButton('Akceptuj i wyślij', ['class' =>'btn btn-success periodAcceptBtn'])  ?>

        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

