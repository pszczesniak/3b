<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-orders-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-orders']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-4 col-sm-4 col-xs-5">
			<div class="form-group field-accorders-acc_period">
				<label for="accorders-acc_period" class="control-label">Okres</label>
				<div class='input-group date' id='datetimepicker_period'>
					<input type='text' class="form-control" id="accorders-acc_period" name="AccPeriod[acc_period]" value="<?= $model->acc_period ?>" data-default="<?= $model->acc_period ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-4 col-sm-4 col-xs-7"><?= $form->field($model, 'type_order_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccType::find()->all(), 'id', 'type_name'), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ] ) ?></div>                               
        <div class="col-md-4 col-sm-4 col-xs-12"><?= $form->field($model, 'verify')->dropDownList( [1 => 'wysłane', 2 => 'niezweryfikowane'], [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-orders', 'data-form' => '#filter-acc-orders-search' ] ) ?></div>                       
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    
    
    
</script>