<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'action' => Url::to(['/accounting/action/cunsettle']),
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
			<?= $form->field($model, 'id_order_fk')->dropDownList( [$order->id => $order->name], ['prompt' => '- wybierz zlecenie -', 'class' => 'form-control select2'] ) ?> 

			<?php
                if(count($model->actions) >= 1) {
                    echo '<div class="alert alert-info">Liczba czynności zaznaczonych do wycofania <b>'.count($model->actions).'</b> </div>';
                    foreach($model->actions as $key => $action) {
                        echo '<input name="ActionsUnsettle[actions][]" class="none" value="'.$action.'" type="checkbox" checked>';
                    }
                } else {
                    echo '<div class="alert alert-danger">Prosze wybrać przynajmniej 1 czynność do wycofania.</div>';
                }
            ?>
        </div>
    </div>
            
		
    <div class="modal-footer"> 
        <?= (count($model->actions) >= 1) ? Html::submitButton('Wycofaj', ['class' =>'btn btn-danger']) : '' ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>

