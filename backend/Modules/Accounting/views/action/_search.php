<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-acc-actions-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-actions']
    ]); ?>
    
    <div class="grid">
        <div class="col-md-2 col-sm-4 col-xs-5">
			<div class="form-group field-accactions-acc_period">
				<label for="accactions-acc_period" class="control-label">Okres</label>
				<div class='input-group date' id='datetimepicker_period'>
					<input type='text' class="form-control" id="accactions-acc_period" name="AccActionsSearch[acc_period]" value="<?= $model->acc_period ?>" data-default="<?= $model->acc_period ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-3 col-sm-4 col-xs-7"><?= $form->field($model, 'id_customer_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search'  ] ) ?></div>        
        <div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'id_order_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getListAll(), 'id', 'name'), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>                               
        <div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search'  ] ) ?></div> 
		<div class="col-md-3 col-sm-4 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList($model->id_employee_fk), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control select2 widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>        
		<!--<div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-accactions-date_from">
				<label for="accactions-date_from" class="control-label">Data od</label>
				<div class='input-group date' id='datetimepicker_date_from'>
					<input type='text' class="form-control" id="accactions-date_from" name="AccActionsSearch[date_from]" value="<?= $model->date_from ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <div class="col-md-2 col-sm-4 col-xs-6">
			<div class="form-group field-accactions-date_to">
				<label for="accactions-date_to" class="control-label">Data do</label>
				<div class='input-group date' id='datetimepicker_date_to'>
					<input type='text' class="form-control" id="accactions-date_to" name="AccActionsSearch[date_to]" value="<?= $model->date_to ?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>-->
        
        <div class="col-md-2 col-sm-4 col-xs-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('advanced'), [ 'prompt' => ' - wszystkie - ', 'class' => 'form-control  widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>               
        <div class="col-md-2 col-sm-2 col-xs-6"><?= $form->field($model, 'invoiced')->dropDownList(\backend\Modules\Accounting\models\AccActions::listStates(), [ 'class' => 'form-control  widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] )->label('Status') ?></div>                       
        <div class="col-md-2 col-sm-2 col-xs-6"><?= $form->field($model, 'is_note')->dropDownList( [ 0 => '', 1 => 'z notatkami', 2 => 'bez notatek'], [ 'class' => 'form-control  widget-search-list', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ] ) ?></div>                       

        <div class="col-md-6 col-sm-8 col-xs-12"><?= $form->field($model, 'description')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-actions', 'data-form' => '#filter-acc-actions-search' ]) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    
    document.getElementById('accactionssearch-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0), true)
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactionssearch-id_order_fk').innerHTML = result.listOrder;     
            }
        }
       xhr.send();
        return false;
    }
    
</script>