<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Czynności');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'acc-actions-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-actions" data-form="#filter-acc-actions-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
    <div class="grid">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-green rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
                <div class="mini-stat-info">  <span class="actions_all">0 </span> Całość [PLN] </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-purple rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--purple"></i></span>
                <div class="mini-stat-info">  <span class="actions_amount">0 </span> Poza ryczałtem [PLN] </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-pink rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--pink"></i></span>
                <div class="mini-stat-info">  <span class="actions_lump">0 </span> Ryczałt [PLN] </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-yellow rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--yellow"></i></span>
                <div class="mini-stat-info">  <span class="actions_time1">0 </span> Całkowity czas [h] </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-blue rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--blue"></i></span>
                <div class="mini-stat-info">  <span class="actions_time2">0 </span> Czas rozliczony [h] </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-red rounded">
                <span class="mini-stat-icon"><i class="fa fa-money text--red"></i></span>
                <div class="mini-stat-info">  <span class="actions_cost">0 </span> Koszty [PLN] </div>
            </div>
        </div>
    </div>
    <div id="toolbar-actions" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/action/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-actions-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]) ?>
        <!--<form method="POST" action="'.Url::to(['/accounting/action/merging']).'" id="settle-actions-form" > <input type="hidden" name="settle" value="1"></input><button type="submit" id="settle" class="btn bg-orange" disabled><i class="fa fa-calculator"></i> Rozlicz (<span id="settle-counter">0</span>)</button></form>-->
		<!--<a class="btn btn-icon btn-success gridViewModal" title="Rozlicz zaznaczone czynności" href="<?= Url::to(['/accounting/action/msettle']) ?>" id="settle" ><i class="fa fa-calculator"></i>Rozlicz</a>-->
        <?php /*(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? Html::a('<i class="fa fa-calculator"></i>Oznacz do rozliczenia (<span id="settle-counter">0</span>)', Url::to(['/accounting/action/msettle', 'id' => 0]) , 
                        ['class' => 'btn btn-success btn-icon', 
                         "id" => "settle",
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-actions",
                         'data-title' => "Uwzględnij czynności w rozliczeniu"
                        ]) : ''*/ ?>
         <?php if((in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']))) { ?>
            <div class="btn-group" id="actionsDates-btn">
                <button type="button" class="btn btn-icon bg-pink"><i class="fa fa-cogs"></i>Oznacz czynności (<span id="settle-counter">0</span>)</button>
                <button type="button" class="btn bg-pink dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" id="postingDates-list">
                    <?= '<li class="settle-actions">'
                           . '<a href="'.Url::to(['/accounting/action/msettle', 'id' => 0]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Oznacz do rozliczenia">do rozliczenia</a>'
                        .'</li>' ?>
                    <?= '<li class="settle-actions">'
                           . '<a href="'.Url::to(['/accounting/action/mgratis', 'id' => 0]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Oznacz jako gratis">jako gratis</a>'
                        .'</li>' ?>
                    <?= '<li class="settle-actions">'
                           . '<a href="'.Url::to(['/accounting/action/mpriority', 'id' => 0]).'" class="gridViewModal" data-target="#modal-grid-item" data-table="table-actions" data-title="Oznacz jako priorytet">jako priorytet</a>'
                        .'</li>' ?>
                    <!--<li role="separator" class="divider"></li>-->
                </ul>
            </div>
        <?php } ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-actions"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-actions"> 
        <table  class="table table-striped table-items table-widget"  id="table-actions"
                data-toolbar="#toolbar-actions" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                
                data-checkbox-header="true"
                data-maintain-selected="true"
                data-response-handler="responseHandlerChecked"
                
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-method="get"
				data-search-form="#filter-acc-actions-search"
                data-url=<?= Url::to(['/accounting/action/data']) ?>>
            <thead>
                <tr>
                    <?= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? '<th data-field="state" data-visible="true" data-checkbox="true" data-formatter="stateFormatter"></th>' : '' ?>
                    <th data-field="id" data-visible="false">ID</th>   
                    <th data-field="type_action" data-visible="false">TYPE</th>
                    <th data-field="confirm" data-visible="false"></th>                    
                    <th data-field="employee"  data-sortable="true" >Pracownik</th>
                    <th data-field="action_date"  data-sortable="true" >Data</th>
                    <th data-field="unit_time"  data-sortable="false" data-footer-formatter="sumFormatter" data-align="right" data-editable="<?= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? 'true' : 'false'  ?>"  data-editable-display-formatter="timeFormatter">Czas</th>
                    <th data-field="price"  data-sortable="false" data-align="right">Stawka </th>
                    <th data-field="custom"  data-sortable="false" data-align="right" data-editable="<?= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? 'true' : 'false'  ?>">Stawka </th>
                    <th data-field="amount"  data-sortable="false"  data-align="right" data-width="100px">Kwota </th>
                    <th data-field="description"  data-sortable="false" data-editable-type="textarea" data-editable="<?= (in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees'])) ? 'true' : 'false'  ?>">Opis</th>
                    <th data-field="service"  data-sortable="false" data-align="center">Typ</th>
                    <!--<th data-field="case"  data-sortable="true" >Sprawa</th>-->
                    <th data-field="cost"  data-sortable="false" data-align="right">Koszt</th>
                    <th data-field="customer"  data-sortable="false" >Klient</th>
                    <th data-field="order"  data-sortable="false" data-align="center"></th>
                    <th data-field="invoice"  data-sortable="false" data-align="center"></th>
					<th data-field="period"  data-sortable="false" data-align="center"></th>
                    <th data-field="note" data-events="actionEvents" data-sortable="false" data-align="center"></th>
                    <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>-->
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="no-scroll">

            </tbody>            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <!--<tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>-->
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Czynności przesunięcie na następny okres rozliczeniowy</td></tr>
                    <tr><td class="calendar-legend-icon" style="background-color:#f2dede"></td><td>Czynności usunięte</td></tr>
                    <tr><td class="calendar-legend-icon"><span class="label bg-pink">stawka</span></td><td>Czynności rozliczone w ramach ryczałtu</td></tr>
					<tr><td class="calendar-legend-icon"><span class="label bg-navy">stawka</span></td><td>Czynności rozliczone w ramach ryczałtu proporcjonalnego</td></tr>
                    <tr><td class="calendar-legend-icon"><span class="label bg-purple">stawka [x]</span></td><td>Czynności rozliczone częściowo w ramach ryczałtu [x - h:min w ramach ryczałtu]</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
<?php $this->endContent(); ?>

	