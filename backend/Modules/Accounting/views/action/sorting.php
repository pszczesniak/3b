<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sortowanie czynności priorytetowych');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'com-meeting-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_ssearch', ['model' => $searchModel]); ?>
    <div id="table-actions-info-message"></div>
    <div id="toolbar-actions" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="fa fa-sort-numeric-asc"></i>Zapisz zmiany', Url::to(['/accounting/action/msorting', 'id' => 0]) , 
                        ['class' => 'btn bg-yellow btn-icon', 
                         "id" => "sorting",
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-actions",
                         'data-title' => "Sortowanie czynności priorytetowych"
                        ])  ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-actions"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-actions">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-actions"
                data-toolbar="#toolbar-actions" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
                
                data-maintain-selected="true"
                data-response-handler="responseHandlerChecked"
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-action="asc"
                data-method="get"
				data-search-form="#filter-acc-actions-search"
                data-url=<?= Url::to(['/accounting/action/datasorting']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>      
                    <th data-field="confirm" data-visible="false"></th>                    
                    <th data-field="employee"  data-sortable="true" >Pracownik</th>
                    <th data-field="action_date"  data-sortable="false" >Data</th>
                    <th data-field="unit_time"  data-sortable="false" data-footer-formatter="sumFormatter" data-align="right" >Czas</th>
                    <th data-field="price"  data-sortable="false" data-align="right" >Stawka </th>
                    <th data-field="amount"  data-sortable="false"  data-align="right" data-width="100px">Kwota </th>
                    <th data-field="description"  data-sortable="false"  data-editable="true">Opis</th>
                    <th data-field="service"  data-sortable="false" data-align="center">Typ</th>
                    <th data-field="case"  data-sortable="false" >Sprawa</th>
                    <th data-field="cost"  data-sortable="false" data-footer-formatter="sumFormatter" data-align="right">Koszt</th>
                    <th data-field="customer"  data-sortable="false" >Klient</th>
                    <th data-field="order"  data-sortable="false" data-align="center"></th>
                    <th data-field="move" data-events="actionEvents" data-sortable="false" data-align="center"></th>
                    <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>-->
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>            
        </table>
        
    </div>
</div>
<?php $this->endContent(); ?>

	