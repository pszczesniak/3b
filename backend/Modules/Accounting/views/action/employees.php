<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Czynności');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'com-meeting-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_esearch', ['model' => $searchModel]); ?>
    <div class="grid">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-teal rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--teal"></i></span>
                <div class="mini-stat-info">  <span class="actions_time1">0 </span> Całkowity czas [h] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-blue rounded">
                <span class="mini-stat-icon"><i class="fa fa-hourglass text--blue"></i></span>
                <div class="mini-stat-info">  <span class="actions_time2">0 </span> Czynności na rzecz klienta [h] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-green rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--green"></i></span>
                <div class="mini-stat-info">  <span class="actions_amount">0 </span> Czynności administracyjne [h] </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-red rounded">
                <span class="mini-stat-icon"><i class="fa fa-calculator text--red"></i></span>
                <div class="mini-stat-info">  <span class="actions_cost">0 </span> Całkowity koszt [PLN] </div>
            </div>
        </div>
    </div>
    <div id="toolbar-actions" class="btn-group toolbar-table-widget">
        <?= ( 0 == 1 ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/action/create']) , 
                ['class' => 'btn btn-success btn-icon ', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "item-form", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]):'' ?>
        <?= ''/*( ( count(array_intersect(["caseExport", "grantAll"], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/accounting/action/export']) , 
                ['class' => 'btn btn-info btn-icon btn-export', 
                 'id' => 'case-create',
                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "#filter-acc-actions-search", 
                 'data-table' => "table-items",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
                ]):''*/ ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-actions"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-actions"
                data-toolbar="#toolbar-actions" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                data-show-footer="false"
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
                data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
                data-height="<?= \Yii::$app->params['table-page-height'] ?>"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-action="asc"
                data-method="get"
				data-search-form="#filter-acc-actions-search"
                data-url=<?= Url::to(['/accounting/action/edata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>                   
                    <th data-field="employee"  data-sortable="true" >Pracownik</th>
                    <th data-field="action_date"  data-sortable="true" >Data</th>
                    <th data-field="unit_time"  data-sortable="true" data-footer-formatter="sumFormatter" data-align="right" >Czas</th>
                    <th data-field="description"  data-sortable="true" >Opis</th>
                    <th data-field="symbol"  data-sortable="false" data-align="center">Typ</th>
                    <th data-field="service"  data-sortable="false" data-align="center"></th>
                    <!--<th data-field="avatar"  data-sortable="false" data-align="center" data-width="60px" ></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="130px"></th>-->
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        
    </div>

	<!-- Render modal form -->
	<?php
		/*yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
          'footer' => Html::submitButton('Zapisz zmiany', ['class' => 'btn btn-primary']).' <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'size' => 'modal-lg',
          'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
		]);
	 
		echo '<div class="modalContent"><div style="text-align:center"><img src="/images/ajax-loader-img.gif"></div></div>';
        

		yii\bootstrap\Modal::end();*/
	?> 
    <div tabindex="-1" role="dialog" class="fade modal out" id="modal-grid-item" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-pencil"></i>Edytuj</h4>
                </div>
                <div class="modalContent">  </div>
                
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>

	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Update').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-file', // <-- insert this modal's ID
		  'class' => 'modalAjaxForm'
		]);
	 
		echo '<div class="modalContent"></div>';

		yii\bootstrap\Modal::end();
	?>
