<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\components\CustomHelpers;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body action-form">
        <div class="content ">
            <div class="grid">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="alert alert-info"><?= $form->field($model, 'is_confirm', [/*'template' => '{input} {label}'*/])->checkbox([/*'id' => 'calendar-all-day'*/]) ?></div>
                    <div class="grid grid--0">
                        <div class=" col-md-3 col-xs-5">
                            <label class="control-label">Termin </label>
                            <input type='text' class="form-control" id="task_date" name="AccActions[action_date]" value="<?= $model->action_date ?>"/>
                        </div>
                        <div class="col-md-4 col-xs-7">
                            <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-actions']) ?>
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                    <div class="grid">
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2', /*'disabled' => (  ($model->is_confirm) ? true : false ) */] ) ?> 
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'id_order_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Accounting\models\AccOrder::getList(($model->id_customer_fk)?$model->id_customer_fk:0), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'id_set_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                        </div>                        
                    </div>
                    <fieldset><legend class="text--red">Czynność specjalna</legend>
                        <div class="grid">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <?= $form->field($model, 'unit_price')->textInput(['placeholder' => '0,00','maxlength' => true])->label('Kwota') ?> 
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <?= $form->field($model, 'id_currency_fk')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] )->label('Waluta') ?> 
                            </div>
                        </div>
                        <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
                    </fieldset>
           
                    <fieldset><legend class="text--red">Koszt</legend>
                        <div class="grid">
                            <div class="col-md-3 col-sm-8 col-xs-8">
                                <?= $form->field($model, 'acc_cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4">
                                <?= $form->field($model, 'acc_cost_currency_id')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] ) ?> 
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'acc_cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 panel-stats">
                    <div class="mini-stat clearfix bg-teal rounded">
                        <span class="mini-stat-icon"><small class="currency text--teal">PLN</small></span>
                        <div class="mini-stat-info">  <span class="todo-hours_per_day"><?= $stats['hours_per_day'] ?> </span> w wybranym dniu </div>
                    </div>
                    <div class="mini-stat clearfix bg-orange rounded">
                        <span class="mini-stat-icon"><small class="currency text--orange">PLN</small></span>
                        <div class="mini-stat-info">  <span class="todo-hours_per_month"><?= $stats['hours_per_month'] ?></span> w wybranym miesiącu </div>
                    </div>
                    <div class="mini-stat clearfix bg-lightgrey text--navy rounded">
                        <div class="mini-stat-info">
                            <ol class="panel-stats-tasks">
                                <?php
									echo $stats['tasks'];
								?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-sm btn-success']) ?>
        <a class="btn btn-sm bg-teal" href="<?= Url::to(['/task/personal/actions']) ?>" title="Przejdź do rejestru czynności"><i class="fa fa-table"></i></a>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
    })
    document.getElementById('accactions-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0)+"?actionPanel=true&eid=0", true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_set_fk').innerHTML = result.listCase; 
                document.getElementById('accactions-id_order_fk').innerHTML = result.listOrder; 
            }
        }
       xhr.send();
        return false;
    }

</script>
<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
		$('#task_date').on("dp.change", function (e) {
			$(".panel-stats").parent().addClass('overlay');
			var changeDate = new Date(e.date).getTime()/1000; console.log(changeDate);
            var departmentId = $("#accactions-id_department_fk").val(); departmentId = (departmentId) ? departmentId : 0;
			$.ajax({
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $(this).serialize(),
				url: "/accounting/action/accspecial/"+departmentId+"?time="+changeDate,
		
				success: function(data) {
					$(".panel-stats").parent().removeClass('overlay');

					$(".todo-hours_per_day").html(data.stats.hours_per_day);
					$(".todo-hours_per_month").html(data.stats.hours_per_month);
					$(".panel-stats-tasks").html(data.stats.tasks);
				},
				error: function(xhr, textStatus, thrownError) {
					console.log(xhr);
					//alert('Something went to wrong.Please Try again later...');
					$(".panel-stats").parent().removeClass('overlay');
				}
			});
			return false;
		});
       
    });
    
    document.getElementById('accactions-id_department_fk').onchange = function() {
        changeDate = new Date($("#task_date").val()).getTime()/1000; 
        $(".panel-stats").parent().addClass('overlay');
        $.ajax({
            type: 'post',
            cache: false,
            dataType: 'json',
            data: $(this).serialize(),
            url: "/accounting/action/accspecial/"+((this.value) ? this.value : 0)+"?time="+changeDate,
    
            success: function(data) {
                $(".panel-stats").parent().removeClass('overlay');

                $(".todo-hours_per_day").html(data.stats.hours_per_day);
                $(".todo-hours_per_month").html(data.stats.hours_per_month);
                $(".panel-stats-tasks").html(data.stats.tasks);
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                //alert('Something went to wrong.Please Try again later...');
                $(".panel-stats").parent().removeClass('overlay');
            }
        });
        return false;
    }
</script>

