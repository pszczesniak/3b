<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content ">
            <?php /*$form->field($model, 'name')->textInput(['maxlength' => true])*/ ?>

            <div class="grid grid--0">
                <div class=" col-md-3 col-xs-5">
                    <label class="control-label">Termin </label>
                    <input type='text' class="form-control" id="task_date" name="AccAction[action_date]" value="<?= $model->action_date ?>"/>
                </div>
                <div class="col-md-4 col-xs-7">
                    <div class="form-group">
                        <label class="control-label" for="caltask-execution_time"> Czas[H:min] <i class="fa fa-info-circle text--blue" title="Określenie czasu realizacji zadania w formacie H:min"></i></label>
                        <div class="time-element">
                            <div class="time-element_H"><?= $form->field($model, 'timeH')->textInput(['placeholder' => 'H','maxlength' => true])->label(false) ?></div>
                            <div class="time-element_divider">:</div>
                            <div class="time-element_mm"><?= $form->field($model, 'timeM')->textInput(['placeholder' => 'mm','maxlength' => true])->label(false) ?></div>
                        </div> 
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <label class="control-label">Czynność </label>
                    <?= $form->field($model, 'id_type_fk')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('normal'), [ 'class' => 'form-control'] )->label(false) ?> 
                </div>
            </div>
            
            <div class="grid">
                <div class="col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['class' => 'form-control select2' ] ) ?> </div>
                <div class="col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getListByEmployee($model->id_employee_fk), 'id', 'name'), ['class' => 'form-control select2' ] ) ?> </div>
            </div>
            
            <div class="grid">
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                </div>
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'id_set_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                </div>
                <div class="col-sm-4 col-xs-12">
                    <?= $form->field($model, 'id_event_fk')->dropDownList( \backend\Modules\Task\models\CalTask::getGroups($model->id_set_fk), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                 </div>
            </div>
   
			<?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label(false) ?>
            
            <fieldset><legend class="text--red">Koszt</legend>
                <div class="grid">
                     <div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'acc_cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <?= $form->field($model, 'acc_cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                    </div>
                    <!--<div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'acc_cost_vat_id')->dropDownList(\backend\Modules\Accounting\models\AccTax::listTaxes(), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?= $form->field($model, 'acc_cost_gross')->textInput(['placeholder' => 'mm','maxlength' => true]) ?> 
                    </div>-->
                </div>
            </fieldset>
		</div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-primary']) ?>
        <?php if($member) { ?> <a href="<?= Url::to(['/community/meeting/view', 'id' => $member->id_meeting_fk]) ?>" title="Przejdź do karty spotkania" class="btn btn-sm bg-orange"><i class="fa fa-handshake-o"></i></a> <?php } ?>        
        <a href="<?= Url::to(['/accounting/action/deleteajax', 'id' => $model->id]) ?>" title="Usuń zadanie" class="btn btn-sm btn-danger deleteConfirm"><i class="fa fa-trash"></i></a>     
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
       
    });
</script>
<script type="text/javascript">

    document.getElementById('accactions-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('accactions-id_order_fk').innerHTML = result.listOrder; 
                document.getElementById('accactions-id_set_fk').innerHTML = result.listCase;               
            }
        }
       xhr.send();
        return false;
    }
 
</script>

