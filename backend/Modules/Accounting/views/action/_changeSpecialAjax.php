<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\chat\Comments;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    //'options' => ['class' => 'modal-grid',  'enableAjaxValidation'=>true, 'enableClientValidation'=>true,  'data-table' => '#table-events','data-target' => "#modal-grid-event"],
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-actions"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="grid"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
       // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
    <div class="modal-body calendar-task">
        <div class="content">
            <div class="grid">
                <div class="col-sm-8 col-xs-12">
                    <div class="grid grid--0">
                        <div class=" col-md-3 col-xs-5">
                            <label class="control-label">Termin </label>
                            <input type='text' class="form-control" id="task_date" name="AccActions[action_date]" value="<?= $model->action_date ?>"/>
                        </div>
                        <div class="col-md-9 col-xs-7">
                            <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-actions']) ?>
                        </div>
                    </div>
                    <fieldset><legend class="text--red">Kwota</legend>
                        <div class="grid">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <?= $form->field($model, 'unit_price')->textInput(['placeholder' => '0,00','maxlength' => true])->label('Kwota') ?> 
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <?= $form->field($model, 'id_currency_fk')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] )->label('Waluta') ?> 
                            </div>
                        </div>
                    </fieldset>
                    
                    <div class="grid">
                        <div class="col-sm-6 col-xs-6"><?= $form->field($model, 'id_department_fk')->dropDownList(   ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(), 'id', 'name'), ['class' => 'form-control' ] ) ?> </div>
                        <div class="col-sm-6 col-xs-6"><?= $form->field($model, 'id_customer_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Crm\models\Customer::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2', /*'disabled' => (  ($model->is_confirm) ? true : false ) */] ) ?> </div>
                        <div class="col-sm-12 col-xs-12"><?= $form->field($model, 'id_set_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Task\models\CalCase::getList($model->id_customer_fk), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> </div>                        
                    </div>

                    <?= $form->field($model, 'description')->textarea(['rows' => 2, 'placeholder' => 'Wprowadź opis czynności ...'])->label('Opis') ?>
                    
                    <fieldset><legend class="text--red">Koszt</legend>
                        <div class="grid">
                            <div class="col-md-3 col-sm-8 col-xs-8">
                                <?= $form->field($model, 'acc_cost_net')->textInput(['placeholder' => '0,00','maxlength' => true])?> 
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4">
                                <?= $form->field($model, 'acc_cost_currency_id')->dropDownList( \backend\Modules\Accounting\models\AccCurrency::getList(), ['class' => 'form-control select2'] ) ?> 
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'acc_cost_description')->textInput(['placeholder' => 'wpisz opis kosztu ... ','maxlength' => true])?> 
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <?= Comments::widget(['notes' => $model->notes, 'actionUrl' => Url::to(['/accounting/action/note', 'id' => $model->id])]) ?>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: true, showClose: true }); 
       
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#task_date').datetimepicker({ format: 'YYYY-MM-DD', showClear: false, showClose: true }); 
    })
    document.getElementById('accactions-id_customer_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+( (this.value) ? this.value : 0)+"?eid=0", true)
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
               // document.getElementById('accactions-id_order_fk').innerHTML = result.listOrder;     
                document.getElementById('accactions-id_set_fk').innerHTML = result.listCase;               
            }
        }
       xhr.send();
        return false;
    }

    
</script>

