<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Konfiguracja modułu rozliczeń');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--<div class="tabs-left">-->
<div class="grid grid--0">
    <div class="col-xs-3"> 
        <ul class="nav nav-tabs tabs-left">
            <li class="<?= ($module == 'service') ? 'active' : '' ?>">
                <a data-target="#service" href="<?= Url::to(['/accounting/setting/service']) ?>" aria-expanded="true"> <i class="fa fa-wrench"></i><span class="nav-tabs--text">Czynności na rzecz klienta </span></a>
            </li>
            <li class="<?= ($module == 'type') ? 'active' : '' ?>">
                <a data-target="#type" href="<?= Url::to(['/accounting/setting/type']) ?>" aria-expanded="false"><i class="fa fa-tags"></i><span class="nav-tabs--text">Czynności administracyjne</span></a>
            </li>
            <li class="<?= ($module == 'method') ? 'active' : '' ?>">
                <a data-target="#method" href="<?= Url::to(['/accounting/setting/method']) ?>" aria-expanded="false"><i class="fa fa-calculator"></i><span class="nav-tabs--text">Metody rozliczeń</span></span></a>
            </li>
            <li class="<?= ($module == 'rate') ? 'active' : '' ?>">
                <a data-target="#rate" href="<?= Url::to(['/accounting/setting/rate']) ?>" aria-expanded="false"><i class="fa fa-user"></i><span class="nav-tabs--text">Stawki zróżnicowane </span></a>
            </li>
            <li class="<?= ($module == 'tax') ? 'active' : '' ?>">
                <a data-target="#vat" href="<?= Url::to(['/accounting/setting/tax']) ?>" aria-expanded="false"><i class="fa fa-percent"></i><span class="nav-tabs--text">Stawki VAT </span></a>
            </li>
        </ul>
    </div>
    <div class="col-xs-9"> 
        <div class="tab-content ">
            <div id="service" class="bg-white tab-pane <?= ($module == 'service') ? 'active' : '' ?>">
                <div class="panel-body">   
                    <?= ($module == 'service') ? $this->render('tabs/service/index', ['searchModel' => $searchModel]) : ''; ?>
                </div> 
            </div> 
            <div id="type" class="bg-white tab-pane <?= ($module == 'type') ? 'active' : '' ?>">
                <div class="panel-body">     
                    <?= ($module == 'type') ? $this->render('tabs/type/index', ['searchModel' => $searchModel]) : ''; ?>
                </div> 
            </div>
            <div id="method" class="bg-white tab-pane <?= ($module == 'method') ? 'active' : '' ?>">
                <div class="panel-body">     
                    <?= ($module == 'method') ? $this->render('tabs/method/index', ['searchModel' => $searchModel]) : ''; ?>
                </div> 
            </div>
            <div id="rate" class="bg-white tab-pane <?= ($module == 'rate') ? 'active' : '' ?>">
                <div class="panel-body"> 
                    <?= ($module == 'rate') ? $this->render('tabs/rate/index', ['searchModel' => $searchModel]) : ''; ?>
                </div> 
            </div>
            <div id="vat" class="bg-white tab-pane <?= ($module == 'tax') ? 'active' : '' ?>">
                <div class="panel-body"> 
                    <?= ($module == 'tax') ? $this->render('tabs/tax/index', ['searchModel' => $searchModel]) : ''; ?>
                </div> 
            </div>
        </div>
    </div>
</div>
