<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Stawki pracowników');
$this->params['breadcrumbs'][] = $this->title;
?>


<div id="toolbar-data" class="btn-group">
    <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/setting/rateadd']) , 
                    ['class' => 'btn btn-success btn-icon gridViewModal', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ])  ?>
</div>
<div class="div-table">
    <table  class="table table-striped table-items header-fixed"  id="table-rates"
            data-toolbar="#toolbar-data" 
            data-toggle="table" 
            data-show-refresh="true" 
            data-search="true" 
            data-show-toggle="false"  
            data-show-columns="false" 
            data-show-export="false"  

            data-pagination="true"
            data-id-field="id"
            data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
            data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
            data-height="<?= \Yii::$app->params['table-page-height'] ?>"
            data-show-footer="false"
            data-side-pagination="client"
            data-row-style="rowStyle"
            
            data-method="get"
            data-url=<?= Url::to(['/accounting/setting/ratedata']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="type" data-sortable="true">Typ pracownika</th>
                <th data-field="employee" data-sortable="true">Pracownik</th>
                <th data-field="rate" data-sortable="true">Stawka</th>                    
                <th data-field="date_from"  data-sortable="true">Obowiązuje od</th>
                <th data-field="date_to"  data-sortable="true">Obowiązuje do</th>
                <th data-field="customer" data-sortable="true">Klient</th>
                <th data-field="order" data-sortable="true">Zlecenie</th>
                <th data-field="actions" data-events="actionEvents"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>          
    </table>      
</div>




