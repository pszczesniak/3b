<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Metody rozliczeń');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="div-table">
    <table  class="table table-striped table-items header-fixed"  id="table-data"
            data-toolbar="#toolbar-data" 
            data-toggle="table" 
            data-show-refresh="true" 
            data-search="true" 
            data-show-toggle="false"  
            data-show-columns="false" 
            data-show-export="false"  

            data-pagination="true"
            data-id-field="id"
            data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
            data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
            data-height="<?= \Yii::$app->params['table-page-height'] ?>"
            data-show-footer="false"
            data-side-pagination="client"
            data-row-style="rowStyle"
            
            data-method="get"
            data-url=<?= Url::to(['/accounting/setting/methoddata']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="name"  data-sortable="true" data-events="actionEvents" data-sort-name="sname" data-width="40%">Nazwa</th>
                <th data-field="symbol"  data-sortable="true" data-width="20%" >Symbl</th>                    
                <th data-field="color"  data-sortable="true" data-width="30%" >Kolor</th>
                <th data-field="actions" data-events="actionEvents" data-width="10%"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>          
    </table>      
</div>




