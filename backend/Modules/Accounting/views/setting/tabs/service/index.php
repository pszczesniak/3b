<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rejestr usług');
$this->params['breadcrumbs'][] = $this->title;
?>


<div id="toolbar-data" class="btn-group">
    <?php /* Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/accounting/setting/serviceadd']) , 
                    ['class' => 'btn btn-success btn-icon gridViewModal', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) */ ?>
</div>
<table  class="table table-striped table-items header-fixed"  id="table-data"
        data-toolbar="#toolbar-data" 
        data-toggle="table" 
        data-show-refresh="true" 
        data-search="true" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"  

        data-pagination="true"
        data-id-field="id"
        data-page-list="<?= \Yii::$app->params['table-page-list'] ?>"
        data-page-size="<?= \Yii::$app->params['table-page-size'] ?>"
        data-height="<?= \Yii::$app->params['table-page-height'] ?>"
        data-show-footer="false"
        data-side-pagination="client"
        data-row-style="rowStyle"
        
        data-method="get"
        data-url=<?= Url::to(['/accounting/setting/servicedata']) ?>>
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="name"  data-sortable="true" data-events="actionEvents" data-sort-name="sname" >Nazwa</th>
            <th data-field="unit_time"  data-sortable="true"  >Czas [min]</th>                    
            <th data-field="unit_price"  data-sortable="true" >Koszt [PLN]</th>
            <th data-field="type"  data-sortable="true" data-width="16px" data-align="center">Grupa</th>  
            <th data-field="actions" data-events="actionEvents" data-align="center"></th>
        </tr>
    </thead>
    <tbody class="ui-sortable">

    </tbody>          
</table>      




