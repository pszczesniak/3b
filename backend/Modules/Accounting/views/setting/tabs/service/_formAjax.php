<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\FilesWidget;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data", 'data-input' => '.correspondence-address'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
    <div class="modal-body">
		<div class="grid">
            <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'name')->textInput(  ) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'symbol')->textInput(  ) ?></div>
           <!-- <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'type_fk')->dropDownList( \backend\Modules\Accounting\models\AccService::listTypes('normal'), [ 'class' => 'form-control'] ) ?></div> -->
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'id_tax_fk')->dropDownList(  \backend\Modules\Accounting\models\AccTax::listTaxes(), ['class' => 'form-control select2', 'class' => 'form-control select2'] ) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'unit_time')->textInput( ['placeholder' => 'min'] ) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'unit_price')->textInput(  ) ?></div>
        </div>
        <fieldset><legend>Opis</legend>   
            <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(false) ?>
        </fieldset>
	</div>
    <div class="modal-footer"> 
        <!--<a href="#" class="btn btn-primary" data-dismiss="modal">Zapisz</a> -->
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>

