<?php

namespace backend\Modules\Accounting\models;

use Yii;

/**
 * This is the model class for table "{{%acc_type}}".
 *
 * @property integer $id
 * @property string $type_name
 * @property string $type_symbol
 * @property string $type_color
 * @property string $type_icon
 * @property string $custom_options
 */
class AccType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custom_options'], 'string'],
            [['type_name'], 'string', 'max' => 255],
            [['type_symbol'], 'string', 'max' => 5],
            [['type_color', 'type_icon'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_name' => Yii::t('app', 'Type Name'),
            'type_symbol' => Yii::t('app', 'Type Symbol'),
            'type_color' => Yii::t('app', 'Type Color'),
            'type_icon' => Yii::t('app', 'Type Icon'),
            'custom_options' => Yii::t('app', 'Custom Options'),
        ];
    }
}
