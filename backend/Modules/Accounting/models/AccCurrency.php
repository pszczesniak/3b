<?php

namespace backend\Modules\Accounting\models;

use Yii;

/**
 * This is the model class for table "{{%acc_currency}}".
 *
 * @property integer $id
 * @property string $currency_name
 * @property string $currency_short_name
 * @property string $currency_symbol
 */
class AccCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_name'], 'string', 'max' => 255],
            [['currency_short_name', 'currency_symbol'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'currency_name' => Yii::t('app', 'Currency Name'),
            'currency_short_name' => Yii::t('app', 'Currency Short Name'),
            'currency_symbol' => Yii::t('app', 'Currency Symbol'),
        ];
    }
    
    public static function getList() {
        $currencies = [];
        $data = self::find()->all();
        foreach($data as $key => $value) {
            $currencies[$value->id] = $value->currency_symbol;
        }
        
        return $currencies;
    }
}
