<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_discount}}".
 *
 * @property integer $id
 * @property string $acc_period
 * @property string $name
 * @property double $discount_amount
 * @property double $discount_percent
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AccDiscount extends \yii\db\ActiveRecord
{
    
    public $user_action;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_discount}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_period', 'discount_percent', 'id_order_fk', 'name'], 'required'],
            [['discount_amount', 'discount_amount_native', 'discount_percent'], 'number', 'min' => 1],
            [['id_order_fk', 'id_customer_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['acc_period'], 'string', 'max' => 7],
            [['name'], 'string', 'max' => 255],
            ['acc_period', 'checkPeriod' ],
            ['acc_period', 'checkClose' ],
            ['acc_period', 'checkInvoice',  'when' => function($model) { return ( $model->user_action != 'modify'); }],
        ];
    }
    
    public function checkPeriod($attribute, $params)  {                
        if($this->id_order_fk) {
            $query = AccDiscount::find()->where(['status' => 1, 'acc_period' => $this->acc_period, 'id_order_fk' => $this->id_order_fk]);
            if(!$this->isNewRecord)
                $query->andWhere('id != '.$this->id);
            $exist = $query->all();
            if(count($exist) > 0) {
                $this->addError($attribute, 'Wybrane zlecenie na wskazany okres rozliczeniowy ma już przypisany rabat.');
            }
        }
    }
    
    public function checkClose($attribute, $params)  {                
        $periodArr = explode('-', $this->acc_period);
        $periodModel = AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            $this->addError($attribute, 'Okres rozliczeniowy '.$this->acc_period.' został zamknięty i nie można już modyfikować rabatów na ten okres');
        }
    }
    
    public function checkInvoice($attribute, $params)  {   
        if($this->id_order_fk) {
            if($this->id_invoice_fk && $this->id_invoice_fk > 0) {
                $this->addError($attribute, 'Rabat został jest już przypisany do faktury i nie może zostać zmodyfikowany.');
            } else {
                $sql = "select count(*)  as exist_inv from {{%acc_invoice_item}} ii join {{%acc_invoice}} i on i.id = ii.id_invoice_fk where i.status = 1 and ii.id_order_fk = ".$this->id_order_fk." and i.date_issue = '".($this->acc_period.'-01')."'";
                $dataStats = Yii::$app->db->createCommand($sql)->queryOne();
                
                $exist = $dataStats['exist_inv'];
                if($exist > 0) {
                    $this->addError($attribute, 'Do wybranego zlecenia na ten okres rozliczeniowy już została wystawiona faktura.');
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'acc_period' => Yii::t('app', 'Okres rozliczeniowy'),
            'name' => Yii::t('app', 'Nazwa'),
            'discount_amount' => Yii::t('app', 'Kwota'),
            'discount_percent' => Yii::t('app', 'Procent zniżki'),
            'id_customer_fk' => 'Klient',
            'id_order_fk' => 'Zlecenie',
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
}
