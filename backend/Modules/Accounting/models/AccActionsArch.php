<?php

namespace backend\Modules\Accounting\models;

use Yii;

/**
 * This is the model class for table "{{%acc_actions_arch}}".
 *
 * @property integer $id
 * @property integer $id_action_fk
 * @property string $user_action
 * @property string $user_type
 * @property string $data_arch
 * @property string $data_change
 * @property string $created_at
 * @property integer $created_by
 */
class AccActionsArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_actions_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_action_fk'], 'required'],
            [['id_action_fk', 'created_by'], 'integer'],
            //[['data_arch', 'data_change'], 'string'],
            [['created_at', 'data_arch', 'data_change'], 'safe'],
            [['user_action', 'user_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_action_fk' => Yii::t('app', 'Id Action Fk'),
            'user_action' => Yii::t('app', 'User Action'),
            'user_type' => Yii::t('app', 'User Type'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'data_change' => Yii::t('app', 'Data Change'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
