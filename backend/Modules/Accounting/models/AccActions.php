<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use \backend\Modules\Accounting\models\AccActionsArch;
use \backend\Modules\Accounting\models\AccPeriod;

/**
 * This is the model class for table "{{%acc_actions}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_service_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property integer $id_employee_fk
 * @property integer $id_department_fk
 * @property string $action_date
 * @property integer $id_unit_rate_fk
 * @property double $unit_price
 * @property integer $unit_time
 * @property integer $id_confirm_rate_fk
 * @property double $confirm_price
 * @property integer $confirm_time
 * @property string $description
 * @property integer $is_confirm
 * @property integer $id_order_fk
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AccActions extends \yii\db\ActiveRecord
{
    public $date_from;
    public $date_to;
    
    public $timeH;
    public $timeM;
    public $executionTime;
    
    public $id_type_fk;
    public $invoiced;
    public $is_note;
    
    public $user_action;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_actions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_todo_fk', 'type_fk', 'id_service_fk', 'id_customer_fk', 'id_set_fk', 'id_event_fk', 'id_task_fk', 'id_employee_fk', 'id_department_fk', 'id_invoice_fk', 'id_unit_rate_fk', 'id_exchange_fk', 'acc_cost_currency_id', 'acc_cost_vat_id', 'id_currency_fk',
              'unit_time', 'id_confirm_rate_fk', 'is_confirm', 'id_order_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_meeting_fk', 'is_shift', 'user_shift', 'is_gratis', 'user_gratis', 'is_priority', 'user_priority', 'rank_priority'], 'integer'],
            [['id_service_fk', 'id_employee_fk', 'id_department_fk', 'id_customer_fk'], 'required', 'when' => function($model) { return ($model->type_fk != 5 && $model->type_fk != 6); }],
            [['name', 'action_date', 'id_customer_fk', 'id_department_fk'], 'required', 'when' => function($model) { return ($model->type_fk == 5 || $model->type_fk == 6); }],
            [['unit_price'], 'required', 'when' => function($model) { return ($model->type_fk == 5 && (!$model->acc_cost_net && !$model->unit_price) ); }, 'message' => 'Proszę podać kwotę za wykonanie czynności lub wartość kosztu'],
            [['action_date', 'created_at', 'updated_at', 'deleted_at', 'timeH', 'timeM', 'acc_period', 'acc_period_correction', 'date_shift', 'date_gratis', 'date_priority'], 'safe'],
            [['confirm_time', 'unit_price', 'unit_price_native', 'acc_cost_net', 'acc_cost_gross', 'acc_cost_native', 'acc_cost_invoice', 
              'confirm_price', 'confirm_price_native', 'acc_price', 'acc_price_native', 'rate_exchange', 'acc_limit', 'acc_limit_prev', 'acc_limit_price', 'acc_limit_price_native', 'estimated_price'], 'number'],
            [['description', 'name', 'acc_cost_description'], 'string'],
            ['is_confirm', 'checkSettled', 'when' => function($model) { return ($model->is_confirm == 1); } ],
            ['acc_period', 'checkPeriod' ],
            [['acc_cost_net'], 'required', 'message' => 'Proszę podać wartość netto kosztu', 'when' => function($model) { return ($model->id_service_fk == 4); }],
            [['acc_cost_description'], 'required', 'message' => 'Proszę podać  opis kosztu', 'when' => function($model) { return ($model->id_service_fk == 4 || $model->acc_cost_net); }],
            [['acc_cost_description'], 'required', 'message' => 'Proszę podać  opis kosztu', 'when' => function($model) { return ($model->id_service_fk == 5 && $model->acc_cost_net); }],
            [['description'], 'required', 'message' => 'Proszę podać opis czynności specjalnej', 'when' => function($model) { return ($model->id_service_fk == 5 && $model->unit_price); }],
            [['description'], 'required',  'when' => function($model) { return ( $model->id_service_fk != 4 && $model->id_service_fk != 5 ); }],
            //[['timeM'], 'required',  'when' => function($model) { return ( $model->id_service_fk != 4 && $model->user_action != 'delete'); }, 'message' => 'Proszę podać liczbę minut'],
            //[['timeH'], 'required',  'when' => function($model) { return ( $model->id_service_fk != 4 && $model->user_action != 'delete'); }, 'message' => 'Proszę podać liczbę godzin'],
            [['timeM'], 'validateExecutiontime',  'when' => function($model) { return ( $model->id_service_fk != 4 && $model->id_service_fk != 5 && $model->user_action != 'delete'); }],
            [['acc_period'], 'validateInvoice',  'when' => function($model) { return ( $model->id_invoice_fk ); }],
            [['id_employee_fk'], 'validateLimittime',  'when' => function($model) { return ( !empty($model->id_set_fk) ); }],
            [['id_customer_fk'], 'validatePermission',  'when' => function($model) { return ( $model->is_confirm ); }],
			[['id_order_fk'], 'validateOrderactive',  'when' => function($model) { return ( !empty($model->id_order_fk) ); }],
        ];
    }

    public function validateLimittime(){
        if($this->status != -1){
			$set = \backend\Modules\Task\models\CalCase::findOne($this->id_set_fk);
            if($set && $set->details['is_limited']) {
                $modelCe = \backend\Modules\Task\models\CaseEmployee::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_case_fk' => $this->id_set_fk])->one();
                if($modelCe) {
                    $sql = "select sum(unit_time) as unit_time from {{%acc_actions}} where status = 1 and id_employee_fk = ".$this->id_employee_fk." and id_set_fk = ".$this->id_set_fk;
                    if(!$this->isNewRecord) $sql .= " and id != ".$this->id;
                    
                    $dataStats = Yii::$app->db->createCommand($sql)->queryOne();
                    
                    $toUsed = $modelCe->time_limit*60 - $dataStats['unit_time'];
                    
                    $H = ($this->timeH) ? $this->timeH*60 : 0;
                    $m = ($this->timeM) ? $this->timeM : 0;
                    $execution_time = $H + $m;
                    
                    if($toUsed < $execution_time) {
                        $timeH = intval($toUsed/60);
                        $timeM = $toUsed - ($timeH*60);
                        $this->addError('id_employee_fk', 'Liczba godzin możliwa do wykorzystania w ramach limitu dla tego projektu wynosi: '.round($toUsed/60,2).' ['.$timeH.' h '.$timeM.' min]');
                    }
                }
            }
		} 
	}
	
	public function validateOrderactive(){
        if($this->id_order_fk) {
			$order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
            if($order) {
				if($order->date_from > $this->action_date) {
					$this->addError('id_order_fk', 'Zlecenie jest aktywne od '.$order->date_from);
				}
				if($order->date_to) {
					if($order->date_to < $this->action_date) {
						$this->addError('id_order_fk', 'Zlecenie jest aktywne od '.$order->date_to);
					}
				}
			}
		} 
	}
    
    public function checkSettled($attribute, $params)  {                
        if($this->is_confirm == 1 && !$this->id_order_fk) {
            $this->addError($attribute, 'Jeśli chcesz zafakturować czynność to musisz określić sposób rozliczenia');
        }
    }
    
    public function validatePermission($attribute, $params)  {                
        $employeeId = \Yii::$app->session->get('user.idEmployee');
        if($this->is_confirm == 1 && ($this->user_action == 'update' || $this->user_action == 'delete')) {
            $periodSpecificationGrant = "select count(*) isGrant from {{%acc_order_period}} where status >= 0 and acc_period = '".$this->acc_period."' and id_order_fk = ".$this->id_order_fk." and id_employee_fk = ".(($employeeId) ? $employeeId : 0)."";
            $connection = Yii::$app->getDb();
            $commandCount = $connection->createCommand($periodSpecificationGrant);
            $isGrant = $commandCount->queryOne()['isGrant'];
            if(!in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) ) {
                if(!$isGrant)
                    $this->addError($attribute, 'Czynność została oznaczona do rozliczenia i nie masz uprawnień do jej edycji');
            }
        }
    }
    
    public function validateInvoice($attribute, $params)  {                
        $this->addError('acc_period', 'Czynność została zafakturowana i nie może zostać zmieniona');  
    }
      
    public function checkPeriod($attribute, $params)  {                
        $periodArr = explode('-', $this->acc_period);
        $periodModel = AccPeriod::find()->where(['period_year' => $periodArr[0], 'period_month' => $periodArr[1]*1])->one();
        if($periodModel && $periodModel['status'] == 2 /*$periodModel['is_closed'] == 1*/) {
            $this->addError($attribute, 'Okres rozliczeniowy '.$this->acc_period.' został zamknięty i nie można już modyfikować czynności');
        }
    }
    
    public function validateExecutiontime(){
		if(!$this->timeH && !$this->timeM){
			$this->addError('timeM','Proszę podać czas wykonania czynności');
		} 
	}
    
    public function validateSpecialValue() {
        if(!$this->unit_price && !$this->acc_cost_net){
			$this->addError('unit_price', 'Proszę podać kwotę za wykonanie czynności lub koszt');
		} 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()  {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Typ'),
            'id_service_fk' => 'Rodzaj usługi',
            'id_customer_fk' => 'Klient',
            'id_set_fk' => 'Postępowanie',
            'id_event_fk' => 'Zdarzenie kancelaryjne',
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_department_fk' => Yii::t('app', 'Dział'),
            'action_date' => Yii::t('app', 'Action Date'),
            'id_unit_rate_fk' => Yii::t('app', 'Id Unit Rate Fk'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'unit_time' => Yii::t('app', 'Unit Time'),
            'id_confirm_rate_fk' => Yii::t('app', 'Id Confirm Rate Fk'),
            'confirm_price' => Yii::t('app', 'Confirm Price'),
            'confirm_time' => Yii::t('app', 'Confirm Time'),
            'description' => Yii::t('app', 'Opis'),
            'is_confirm' => Yii::t('app', 'Rozlicz'),
            'id_order_fk' => Yii::t('app', 'Zlecenie'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'acc_cost_net' => 'netto',
            'acc_cost_vat_id' => 'stawka VAT',
            'acc_cost_currency_id' => 'stawka VAT',
            'acc_cost_gross' => 'brutto',
            'acc_cost_description' => 'Opis kosztu',
            'invoiced' => 'Status czynności',
            'is_note' => 'Notatki',
            'acc_period' => 'Okres rozliczeniowy',
            'name' => 'Nazwa wykonawcy',
            'acc_period_correction' => 'Korekta za okres'
        ];
    }
    
    public function beforeSave($insert) {
		if($this->user_action != 'delete' && $this->user_action != 'gratis' && $this->user_action != 'period' && $this->user_action != 'priority') {
            $H = ($this->timeH) ? $this->timeH*60 : 0;
            $m = ($this->timeM) ? $this->timeM : 0;
            $this->unit_time = $H + $m;
            
            if($this->is_shift == 0)
                $this->acc_period = date('Y-m', strtotime($this->action_date));
        }
        
        $this->confirm_time = round($this->unit_time/60,2);
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
                $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $this->id_customer_fk, 'status' => '1'])->all();
                if( count($orders) == 1 ) {
                    $this->id_order_fk = $orders[0]->id;
                } else {
					$orders = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['id_customer_fk' => $this->id_customer_fk, 'status' => '1'])->all();
					if( count($orders) == 1 ) {
						$this->id_order_fk = $orders[0]->id_order_fk;
					}
				}
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			//return true;
		} /*else { 
						
			return false;
		}*/
        
        /* cost */
        $order = false;
        if($this->id_order_fk) $order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
        if($this->acc_cost_net) {
            $order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
            if($order) {
                $exchange = ($order->id_currency_fk != 1) ? \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk) : false; 
                if(!$exchange) $exchange = \backend\Modules\Accounting\models\AccExchangeRates::activeRate($this->acc_cost_currency_id);
                $exchangeRate = ($exchange) ? $exchange->rate_value : 1;
                $this->acc_cost_native = ($this->acc_cost_currency_id == 1) ? $this->acc_cost_net : round($this->acc_cost_net * $exchangeRate,2);
                if($this->acc_cost_currency_id == 1 && $order->id_currency_fk != 1)
                    $this->acc_cost_invoice = round($this->acc_cost_net * $exchangeRate,2);
                else if($this->acc_cost_currency_id == 1 && $order->id_currency_fk != 1)
                    $this->acc_cost_invoice = round($this->acc_cost_net / $exchangeRate,2);      
                else         
                    $this->acc_cost_invoice = $this->acc_cost_net;
            }
            /*$sqlUpdateCost = "update {{%acc_actions}} set "
                                ."acc_cost_native = case when (acc_cost_currency_id = 1) then acc_cost_net else round(acc_cost_net*".$exchangeRate.",2) end, "
                                ."acc_cost_invoice = case when (acc_cost_currency_id = 1 and ".$order['id_currency_fk']." != 1) then round(acc_cost_net/".$exchangeRate.",2) "
                                                       ." when (acc_cost_currency_id != 1 and ".$order['id_currency_fk']." = 1) then round(acc_cost_net*".$exchangeRate.",2) else acc_cost_net end "
                            ." where id = ".$this->id; 
            Yii::$app->db->createCommand($sqlUpdateCost)->execute(); */
        }
        /* cost */
        /* init price */
        if($this->id_service_fk <= 3 && $this->id_order_fk && $this->user_action != "custom_price") {
            if(strlen($this->custom_price) && $this->custom_price >= 0) {
                $this->unit_price = $this->custom_price;
            } else {
                if(!$order)
                    $order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
                if($order) {
                    if($order->id_dict_order_type_fk != 1) {
                        if($this->id_service_fk == 1)
                            $this->unit_price = $order->rate_hourly_1;
                        else if($this->id_service_fk == 2)
                            $this->unit_price = $order->rate_hourly_2;
                        else if($this->id_service_fk == 3)
                            $this->unit_price = $order->rate_hourly_way;
                        
                        if(($order->id_dict_order_type_fk == 4 || $order->id_dict_order_type_fk == 2) && $this->id_service_fk == 1) {     
                            $employee = \backend\Modules\Company\models\CompanyEmployee::findOne($this->id_employee_fk);
                            if($employee) {
                                $rateEmployee = \backend\Modules\Accounting\models\AccRate::getRate($employee->id_dict_employee_type_fk, $employee->id, $order->id_customer_fk, $order->id, $this->action_date);
                                if($rateEmployee) 
                                    $this->unit_price = $rateEmployee->unit_price;
                            }
                        }
                    } else {
                        $this->unit_price = 0;
                    }
                }
            }
        }
		if($order) {
			if($order->id_currency_fk != 1) {
				$exchange =  \backend\Modules\Accounting\models\AccExchangeRates::activeRate($order->id_currency_fk);
				$exchangeRate = ($exchange) ? $exchange->rate_value : 1;
				$exchangeId = ($exchange) ? $exchange->id : 0;
				if($exchange)
					$this->unit_price_native = (round($this->unit_price*$exchangeRate, 2));
			} else {
				$this->unit_price_native = $this->unit_price;
			}
            $this->estimated_price = $this->unit_price_native;
		}
        /* init price */
        
		return true;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {
        $note = false;
        $changesStr = '';
        
        if (!$insert && $this->status == 1) {
            $modelArch = new AccActionsArch();
            $modelArch->id_action_fk = $this->id;
            if($this->user_action) {
                $modelArch->user_action = ($this->user_action == 'convert') ? 'settled' : $this->user_action;
            } else {
                $modelArch->user_action = ($this->status == -1) ? 'delete' : 'update';
            }
            
            if($modelArch->user_action == 'delete') 
                $note = 'usunięcie';
            else if($modelArch->user_action == 'settled') 
                $note = 'oznaczenie';
            else if($modelArch->user_action == 'update') 
                $note = 'edycja';
            else
                $note = false;
                
            if(isset($changedAttributes['unit_time']) && $changedAttributes['unit_time'] != $this->unit_time) {
                $changesStr .= '<li>zmiana czasu z '.$changedAttributes['unit_time'].' na '. $this->unit_time.' minut</li>';
            }
            
            if(isset($changedAttributes['action_date']) && $changedAttributes['action_date'] != $this->action_date) {
                $changesStr .= '<li>zmiana daty z '.$changedAttributes['action_date'].' na '. $this->action_date.' </li>';
            }
            
            if(isset($changedAttributes['acc_cost_net']) && $changedAttributes['acc_cost_net'] != $this->acc_cost_net) {
                $changesStr .= '<li>zmiana wartości kosztu z '.$changedAttributes['acc_cost_net'].' na '. $this->acc_cost_net.' minut</li>';
            }
            
            if(isset($changedAttributes['id_customer_fk']) && $changedAttributes['id_customer_fk'] != $this->id_customer_fk) {
                $oldCustomer = \backend\Modules\Crm\models\Customer::findOne($changedAttributes['id_customer_fk']);
                if($oldCustomer)
                    $changesStr .= '<li>zmiana zlecenia z '.$oldCustomer->name.'</li>';
            }
            
            if(isset($changedAttributes['id_order_fk']) && $changedAttributes['id_order_fk'] != $this->id_order_fk) {
                $oldOrder = \backend\Modules\Accounting\models\AccOrder::findOne($changedAttributes['id_order_fk']);
                if($oldOrder)
                    $changesStr .= '<li>zmiana zlecenia z '.$oldOrder->name.'</li>';
            }
            
            if(isset($changedAttributes['description']) && $changedAttributes['description'] != $this->description) {
                $changesStr .= '<li>zmiana wartości opisu</li>';
            }
            
            if(isset($changedAttributes['acc_cost_description']) && $changedAttributes['acc_cost_description'] != $this->description) {
                $changesStr .= '<li>zmiana wartości opisu kosztu</li>';
            }
            
            if(isset($changedAttributes['id_service_fk']) && $changedAttributes['id_service_fk'] != $this->id_service_fk) {
                $changesStr .= '<li>zmiana wartości typu czynności</li>';
            }
                
            $modelArch->data_change = \yii\helpers\Json::encode($this);
            $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
            
            if($this->is_confirm == 1 && !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && $note && $changesStr) {
                $newNote = new \backend\Modules\Accounting\models\AccNote();
                $newNote->id_parent_fk = $this->id;
                $newNote->type_fk = 3;
                $newNote->id_fk = $this->id;
                $newNote->id_employee_from_fk = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => Yii::$app->user->id])->one()->id;
                $newNote->note = 'Notatka wygenerowana automatycznie: '.$note.' czynności <ol>'.$changesStr.'</ol>';
                $newNote->save();
            }
        } 
        
        if ($this->status == -1) {
            $modelArch = new AccActionsArch();
            $modelArch->id_action_fk = $this->id;

            $modelArch->user_action = 'delete';
            
            $modelArch->data_change = \yii\helpers\Json::encode($this);
            $modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
            $modelArch->created_by = \Yii::$app->user->id;
            $modelArch->created_at = new Expression('NOW()');
            $modelArch->save();
        } 
        
        $order = false;
        if($this->id_order_fk) $order = \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
        if($order) {
            if($order->id_dict_order_type_fk == 1) {
                $sqlSelectAllTime = "select sum((unit_time/60)) as allTime from {{%acc_actions}} where status = 1 and id_service_fk <= 3 and id_order_fk = ".$this->id_order_fk." and acc_period = '".$this->acc_period."'";
                $allTime = \Yii::$app->getDb()->createCommand($sqlSelectAllTime)->queryOne();
                $estimatePrice = ($allTime['allTime'] > 0) ? round($order->rate_constatnt/$allTime['allTime'], 2) : 0;
                $sqlEstimatedPrice = "update {{%acc_actions}} set "
                                ." estimated_price = ".$estimatePrice
                                ." where status = 1 and id_service_fk <= 3 and id_order_fk = ".$this->id_order_fk." and acc_period = '".$this->acc_period."'";
                \Yii::$app->getDb()->createCommand($sqlEstimatedPrice)->execute();
            }
        }
      
		parent::afterSave($insert, $changedAttributes);
	}
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public function getService()
    {
		return $this->hasOne(\backend\Modules\Accounting\models\AccService::className(), ['id' => 'id_service_fk']);
    }
    
    public function getCustomer()
    {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getOrder()
    {
		return $this->hasOne(\backend\Modules\Accounting\models\AccOrder::className(), ['id' => 'id_order_fk']);
    }
    
    public function getNotes() {
        $notesData = \backend\Modules\Accounting\models\AccNote::find()->where(['status' => 1, 'id_fk' => $this->id])->andWhere('type_fk in (1,3)')->orderby('id desc')->all();
        return ($notesData) ? $notesData : [];
    }
    
    public function getCase()  {
		return $this->hasOne(\backend\Modules\Task\models\CalCase::className(), ['id' => 'id_set_fk']);
    }
    
    public static function listStates() {
        return [0 => '-wszystkie-', 1 => 'oznaczone', 2 => 'nieoznaczone', 3 => 'rozliczone', 4 => 'gratisy', 5 => 'bez zlecenia', '6' => 'usunięte'];
    }
    
    public function getDeleted() {
        $user = \common\models\User::findOne($this->deleted_by);
        return $this->deleted_at.': '.( ($user) ? $user->fullname : 'admin');
    }
}
