<?php

namespace frontend\Modules\Accounting\models;

use Yii;
use yii\base\Model;

use backend\Modules\Accounting\models\AccActions;

/**
 * PeriodReport is the model behind the contact form.
 */
class PeriodReport extends Model
{
    public $id_department_fk; 
    public $id_employee_fk;
    public $actions = [];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_department_fk', 'id_employee_fk'], 'integer'],
            //[['actions'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_department_fk' => Yii::t('app', 'Dział'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'actions' => 'Czynności'
        ];
    }

    public function saveSettle() {            
    
        return true;
    }
}
