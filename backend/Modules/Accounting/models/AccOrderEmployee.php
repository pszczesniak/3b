<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "{{%acc_order_employee}}".
 *
 * @property integer $id
 * @property integer $id_order_fk
 * @property string $name
 * @property string $symbol
 * @property string $created_at
 * @property integer $created_by
 */
class AccOrderEmployee extends \yii\db\ActiveRecord
{
    public $managers;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_order_employee}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order_fk', 'id_customer_fk', 'id_employee_fk'], 'required'],
            [['id_order_fk', 'id_customer_fk', 'id_set_fk', 'created_by', 'id_employee_fk'], 'integer'],
            [['created_at', 'managers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => Yii::t('app', 'Klient do dołączenia'),
            'id_parent_fk' => Yii::t('app', 'Klient'),
            'id_order_fk' => Yii::t('app', 'Zlecenie'),
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'managers' => 'Kierownicy'
        ];
    }
    
        public function beforeSave($insert) {
        //var_dump($this->authority_carrying);exit;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
            }
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}

    /*public function afterSave($insert, $changedAttributes) {  
            if ($insert) {
                //$customer = \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk);
                $sqlCustomerUpdate = "update {{%customer}} set customer_linked = '".$this->order['id_customer_fk']."-".$this->order['customer']['name']."' where id = ".$this->id_customer_fk;
                \Yii::$app->db->createCommand($sqlCustomerUpdate)->execute(); 
            } else {
                if($this->status == 1) {
                    $sqlCustomerUpdate = "update {{%customer}} set customer_linked = '".$this->order['id_customer_fk']."-".$this->order['customer']['name']."' where id = ".$this->id_customer_fk;
                    \Yii::$app->db->createCommand($sqlCustomerUpdate)->execute(); 
                } else {
                    $sqlCustomerUpdate = "update {{%customer}} set customer_linked = '' where id = ".$this->id_customer_fk;
                    \Yii::$app->db->createCommand($sqlCustomerUpdate)->execute(); 
                }
            }
    }*/
    
    public function getOrder()
    {
		return $this->hasOne(\backend\Modules\Accounting\models\AccOrder::className(), ['id' => 'id_order_fk']);
    }
    
    public function getCustomer()
    {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getEmployee()
    {
		return $this->hasOne(\backend\Modules\Company\models\Companyemployee::className(), ['id' => 'id_employee_fk']);
    }
}
