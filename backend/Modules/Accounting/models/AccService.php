<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_service}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $name
 * @property string $symbol
 * @property double $unit_price
 * @property integer $unit_time
 * @property integer $id_tax_fk
 * @property integer $id_currency_fk
 * @property string $description
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AccService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'unit_time', 'id_tax_fk', 'id_currency_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['unit_price'], 'number'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 300],
            [['symbol'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'unit_price' => Yii::t('app', 'Cena'),
            'unit_time' => Yii::t('app', 'Czas trwania [min]'),
            'id_tax_fk' => Yii::t('app', 'Stawka VAT'),
            'id_currency_fk' => Yii::t('app', 'Id Currency Fk'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function listTypes($type) {
        $types = [];
        /*$items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => $type])->all();
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;*/
        $types[1] = 'merytoryczne';
        $types[2] = 'pozamerytoryczne';
        $types[3] = 'przejazd';
        $types[4] = 'koszty';
        
        if($type == 'advanced') {
            $types[5] = 'specjalne';
            $types[6] = 'korekty';
        }
        
        return $types;
    }
    
    public static function dictTypes() {
        $types = [];

        $types[1] = ['name' => 'merytoryczne', 'icon' => 'briefcase', 'color' => 'purple'];
        $types[2] = ['name' => 'pozamerytoryczne', 'icon' => 'folder-open', 'color' => 'teal'];
        $types[3] = ['name' => 'przejazd', 'icon' => 'car', 'color' => 'grey'];
        $types[4] = ['name' => 'koszty', 'icon' => 'money', 'color' => 'red'];
        $types[5] = ['name' => 'specjalne', 'icon' => 'flag-checkered', 'color' => 'pink'];
        $types[6] = ['name' => 'korekty', 'icon' => 'calendar-minus-o', 'color' => 'red'];
        $types[7] = ['name' => 'prowizja', 'icon' => 'percent', 'color' => 'yellow'];
        
        return $types;
    }
}
