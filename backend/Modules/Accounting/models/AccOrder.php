<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "law_acc_order".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_dict_order_type_fk
 * @property integer $id_currency_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property integer $id_department_fk
 * @property double $rate_hourly
 * @property double $rate_constatnt
 * @property integer $limit_hours
 * @property string $symbol
 * @property string $description
 * @property string $date_from
 * @property string $date_to
 * @property integer $is_close
 * @property string $notes
 * @property integer $payment_type
 * @property integer $payment_term
 * @property string $payment_account
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AccOrder extends \yii\db\ActiveRecord
{
    
    public $customers;
	public $curr_symbol;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'law_acc_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_dict_order_type_fk', 'id_settlement_type_fk', 'id_currency_fk', 'id_customer_fk', 'id_set_fk', 'id_department_fk', 'is_close', 'payment_type', 'payment_term', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_parent_fk'], 'integer'],
            [['limit_hours', 'rate_hourly_1', 'rate_hourly_2', 'rate_hourly_way', 'rate_constatnt', 'debt_p_capital_percent', 'debt_p_interest_percent'], 'number'],
            [['description', 'notes'], 'string'],
            [['date_from', 'created_at', 'updated_at', 'deleted_at', 'customers'], 'safe'],
            [['id_customer_fk', 'name', 'date_from'], 'required'],
            [['symbol'], 'string', 'max' => 255],
            [['date_to', 'payment_account'], 'string', 'max' => 45],
            ['rate_constatnt', 'required', 'when' => function($model) { return ($model->id_dict_order_type_fk == 1 || $model->id_dict_order_type_fk == 2); } ],
            ['limit_hours', 'required', 'when' => function($model) { return ($model->id_dict_order_type_fk == 2); } ],
            ['rate_hourly_1', 'required', 'when' => function($model) { return ($model->id_dict_order_type_fk >= 2); }, 'message' =>  'Proszę podać stawkę za czynności merytoryczne'],
            ['rate_hourly_2', 'required', 'when' => function($model) { return ($model->id_dict_order_type_fk >= 2); }, 'message' =>  'Proszę podać stawkę za czynności pozamerytoryczne'],
            ['rate_hourly_way', 'required', 'when' => function($model) { return ($model->id_dict_order_type_fk >= 2); }, 'message' =>  'Proszę podać stawkę za przejazd'],            
            ['id_dict_order_type_fk', 'checkUse', 'when' => function($model) { return (!$model->isNewRecord); } ],
        ];
    }
    
    public function checkUse(){
        $inUse = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_order_fk' => $this->id, 'is_confirm' => 1])->count();
        $temp = \backend\Modules\Accounting\models\AccOrder::findOne($this->id);
        if( $inUse > 0 && ( $temp->id_dict_order_type_fk != $this->id_dict_order_type_fk ||
                            $temp->rate_constatnt != $this->rate_constatnt ||
                            $temp->rate_hourly_1 != $this->rate_hourly_1 ||
                            $temp->rate_hourly_2 != $this->rate_hourly_2 ||
                            $temp->rate_hourly_way != $this->rate_hourly_way ||
                            $temp->limit_hours != $this->limit_hours) ){
            $this->addError('id_dict_order_type_fk', 'W ramach tego zlecenia są juz rozliczone czynności i nie można zmienić danych związanych z rozliczeniem');
        } 
        if( $inUse > 0 && ( strtotime($temp->date_from) < strtotime($this->date_from) ) ) {
            $inUseDateFrom = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_order_fk' => $this->id])->andWhere("action_date < '".$this->date_from."'")->min('action_date');
            if($inUseDateFrom)
                $this->addError('date_from', 'W systemie są czynności z dnia '.$inUseDateFrom.' powiązane z tym zleceniem');
        }
        if( $inUse > 0 && $this->date_to ) {
            $inUseDateTo = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'id_order_fk' => $this->id])->andWhere("action_date > '".$this->date_to."'")->max('action_date');
            if($inUseDateTo)
                $this->addError('date_to', 'W systemie są czynności z dnia '.$inUseDateTo.' powiązane z tym zleceniem');
        }
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Sposób rozliczenia'),
            'name' => 'Nazwa',
            'id_dict_order_type_fk' => Yii::t('app', 'Typ zlecenia'),
            'id_currency_fk' => Yii::t('app', 'waluta'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_set_fk' => Yii::t('app', 'w sprawie'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'rate_hourly_way' => Yii::t('app', 'przejazd'),
            'rate_hourly_1' => Yii::t('app', 'merytoryczne'),
            'rate_hourly_2' => Yii::t('app', 'pozamerytoryczne'),            
            'rate_constatnt' => Yii::t('app', 'Opłata za gotowość'),
            'limit_hours' => Yii::t('app', 'Limit godzin w ryczałcie'),
            'symbol' => Yii::t('app', 'Symbol'),
            'description' => Yii::t('app', 'Description'),
            'date_from' => Yii::t('app', 'Data rozpoczęcia'),
            'date_to' => Yii::t('app', 'Data zamknięcia'),
            'is_close' => Yii::t('app', 'Is Close'),
            'by_actions' => Yii::t('app', 'Rozliczanie wg godzin?'),
            'notes' => Yii::t('app', 'Notes'),
            'payment_type' => Yii::t('app', 'Payment Type'),
            'payment_term' => Yii::t('app', 'Payment Term'),
            'payment_account' => Yii::t('app', 'Payment Account'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'customers' => 'Dołącz do rozliczenia klientów',
            'debt_p_capital_percent' => 'Prowizja od wpłat na kapitał', 
            'debt_p_interest_percent' => 'Proziwzja od wpłat na odsetki',
            'id_settlement_type_fk' => 'Niewykorzystane godziny',
        ];
    }
    
    public function beforeSave($insert) {
        //var_dump($this->authority_carrying);exit;
		if(empty($this->date_to)) $this->date_to = null;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;          
			} else { 
				$this->updated_by = \Yii::$app->user->id;               
			}
			return true;
		} else { 					
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function afterSave($insert, $changedAttributes) {  
        if (!$insert && $this->status <= 0 && $this->type_fk == 2) {
            $sqlCustomerUpdate = "update {{%customer}} set customer_linked = '' where id in (select id_customer_fk from {{%acc_order_item}} where id_order_fk = ".$this->id.")";
            \Yii::$app->db->createCommand($sqlCustomerUpdate)->execute(); 
        }
    }
    
    public function getCustomer() {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }

    public function getType() {
        return $this->hasOne(\backend\Modules\Accounting\models\AccType::className(), ['id' => 'id_dict_order_type_fk']);
    }
    
    public function getCreator() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
    
    public static function listTypes() {
        $types = [];
        
        $types[1] = 'Stała obsługa';
        $types[2] = 'Zadanie';
        $types[3] = 'Projekt';
        
        return $types;
    }
    
    public static function listMethods() {
        $methods = [];
        
        /*$methods[1] = 'za gotowość + wg godzin';
        $methods[2] = 'wg godzin, ale mnie mniej niż';
        $methods[3] = 'wg godzin';
        $methods[4] = 'stała kwota';
        $methods[5] = 'ryczałt';*/
        $methods[1] = 'RYCZAŁT';
        $methods[2] = 'ROZLICZENIE RYCZAŁTOWO-KWOTOWE';
        $methods[3] = 'ROZLICZENIE WG STAWKI GODZINOWEJ';
        $methods[4] = 'ROZLICZENIE WG STAWKI GODZINOWEJ ZRÓŻNICOWANEJ';
        
        return $methods;
    }
    
    public static function listSettlements() {
        $types = [];
        
        $types[1] = 'przenieś na następny okres';
        $types[2] = 'przelicz proporcjonalnie';
        
        return $types;
    }
    
    public static function getList($id) {
        $orders = [];
        $data = \backend\Modules\Accounting\models\AccOrder::find()
				->where(['status' => 1])
				->andWhere('(id_customer_fk = '.$id.' or id in (select id_order_fk from {{%acc_order_item}} where id_customer_fk = '.$id.'))')
				->all();
        
        if($data) {
            foreach($data as $key => $order) {
                //$orders[$order->id] = $order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : '');
                array_push($orders, ['id' => $order->id, 'name' => ($order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : ''))]);
            }
        }
        //$orders = $data;
        
        return $orders;
    }
    
    public static function getListAll() {
        $orders = [];
        $sqlQuery = "select o.id as id, concat(o.name, ' [', c.name, ']') as name from {{%acc_order}} o join {{%customer}} c on c.id = o.id_customer_fk where o.status = 1 order by c.name";
        $data = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
        if($data) $orders = $data;
        
        return $orders;
    }
    
    public function getInvoices() {
		$items = $this->hasMany(\backend\Modules\Accounting\models\AccInvoice::className(), ['id_order_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
    
    public function getItems()  {
		$items = $this->hasMany(\backend\Modules\Accounting\models\AccOrderItem::className(), ['id_order_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
    
    public static function getStats($id) {
        $sql = "select round(sum(confirm_time), 2) as confirm_time, "
			  ."round(sum(confirm_price*confirm_time),2) as confirm_amount, "
			  ."round(sum(confirm_price_native*confirm_time),2) as confirm_amount_native, "
			  ."round(sum(acc_cost_invoice),2) as costs, round(sum(acc_cost_native),2) as costs_native"
              ." from {{%acc_actions}} a " 
              ." where status = 1 and is_confirm = 1 and id_order_fk = ".$id;
        $stats = Yii::$app->db->createCommand($sql)->queryOne(); 
        
        return $stats;
    }
    
    public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 7])->orderby('id desc')->all();
        return $filesData;
    }
    
    public static function getDiscount($orderId, $period) {
        return \backend\Modules\Accounting\models\AccDiscount::find()->where(['status' => 1, 'id_order_fk' => $orderId, 'acc_period' => $period])->one();
    }
	
	public function getCurrency() {
		$currencies = [1 => 'PLN', 2 => 'EUR', 3 => 'USD'];
		
		return (isset($currencies[$this->id_currency_fk])) ? $currencies[$this->id_currency_fk] : 'PLN';
	}
    
    public static function getAll($id, $period) {
        return \backend\Modules\Accounting\models\AccActions::find()->where(['id_order_fk' => $id, 'status' => 1, 'acc_period' => $period])->andWhere('(is_confirm = 1 or is_gratis=1)')->orderby('is_priority desc, rank_priority, action_date, id')->all();
    }
	
	public static function calculation($id, $period) {
	
		return true;
	}
    
    public static function getListForClient() {
        $cid = (\Yii::$app->session->get('user.cid')) ? \Yii::$app->session->get('user.cid') : 0;
        
        return AccOrder::find()->where(['status' => 1, 'id_customer_fk' => $cid])->all();
    }
}
