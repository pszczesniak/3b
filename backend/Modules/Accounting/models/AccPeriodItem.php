<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_period_item}}".
 *
 * @property integer $id
 * @property integer $id_period_fk
 * @property integer $id_employee_fk
 * @property integer $id_customer_fk
 * @property integer $id_order_fk
 * @property string $financial_data
 * @property string $details
 * @property string $description
 * @property integer $is_closed
 * @property integer $is_settled
 * @property integer $status
 * @property string $closed_at
 * @property integer $closed_by
 * @property string $settled_at
 * @property integer $settled_by
 * @property string $opened_at
 * @property integer $opened_by
 */
class AccPeriodItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_period_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_period_fk', 'type_fk'], 'required'],
            [['id', 'id_period_fk', 'id_employee_fk', 'id_customer_fk', 'id_order_fk', 'id_manager_fk', 'id_department_fk', 'is_closed', 'is_settled', 'status', 'closed_by', 'settled_by', 'opened_by'], 'integer'],
            [['financial_data', 'details', 'description'], 'string'],
            [['closed_at', 'settled_at', 'opened_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_period_fk' => Yii::t('app', 'Id Period Fk'),
            'id_employee_fk' => Yii::t('app', 'Id Employee Fk'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'id_order_fk' => Yii::t('app', 'Id Order Fk'),
            'all_lump_sum' => Yii::t('app', 'All Lump Sum'),
            'all_profit' => Yii::t('app', 'All Profit'),
            'all_time' => Yii::t('app', 'All Time'),
            'all_time_personal' => Yii::t('app', 'All Time Personal'),
            'all_cost' => Yii::t('app', 'All Cost'),
            'details' => Yii::t('app', 'Details'),
            'description' => Yii::t('app', 'Description'),
            'is_closed' => Yii::t('app', 'Is Closed'),
            'is_settled' => Yii::t('app', 'Is Settled'),
            'status' => Yii::t('app', 'Status'),
            'closed_at' => Yii::t('app', 'Closed At'),
            'closed_by' => Yii::t('app', 'Closed By'),
            'settled_at' => Yii::t('app', 'Settled At'),
            'settled_by' => Yii::t('app', 'Settled By'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'opened_by' => Yii::t('app', 'Opened By'),
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->settled_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'settled_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getPeriod()  {
		return $this->hasOne(\backend\Modules\Accounting\models\AccPeriod::className(), ['id' => 'id_period_fk']);
    }
    
    public function getManager()  {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_manager_fk']);
    }
}
