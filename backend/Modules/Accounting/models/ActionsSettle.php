<?php

namespace frontend\Modules\Accounting\models;

use Yii;
use yii\base\Model;

use backend\Modules\Accounting\models\AccOrder;
use backend\Modules\Accounting\models\AccActions;

/**
 * ActionForm is the model behind the contact form.
 */
class ActionsSettle extends Model
{
    public $id_order_fk; 
    public $is_confirm;
    public $actions = [];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['actions'], 'required'],
            [['is_confirm'], 'integer'],
            [['actions'], 'safe'],
            ['id_order_fk', 'required', 'when' => function($model) { return ( $model->is_confirm == 1 ); }, 'message' => 'Musisz wskazać zlecenie jesli chcesz zafakturować czynności' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'is_confirm' => Yii::t('app', 'Rozlicz'),
            'id_order_fk' => Yii::t('app', 'Rozliczenie'),
            'actions' => 'Czynności'
        ];
    }

    public function saveSettle() {            
    
        return true;
    }
}
