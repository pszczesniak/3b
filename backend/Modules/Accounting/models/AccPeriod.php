<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_period}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $period_year
 * @property integer $period_month
 * @property double $all_lump_sum
 * @property double $all_profit
 * @property double $all_time
 * @property double $all_time_personal
 * @property string $description
 * @property integer $is_closed
 * @property integer $is_settled
 * @property integer $status
 * @property string $closed_at
 * @property integer $closed_by
 * @property string $settled_at
 * @property integer $settled_by
 * @property string $opened_at
 * @property integer $opened_by
 */
class AccPeriod extends \yii\db\ActiveRecord
{
    public $year_from;
    public $year_to;
    public $month_from;
    public $month_to;
    public $acc_period;
    public $type_order_fk;
    public $id_customer_fk;
    public $verify;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_period}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'period_year', 'period_month', 'is_closed', 'is_settled', 'status', 'closed_by', 'settled_by', 'opened_by'], 'integer'],
            [['period_year', 'period_month'], 'required'],
            [['all_lump_sum', 'all_profit', 'all_time', 'all_time_personal'], 'number'],
            [['description'], 'string'],
            [['closed_at', 'settled_at', 'opened_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'period_year' => Yii::t('app', 'Period Year'),
            'period_month' => Yii::t('app', 'Period Month'),
            'all_lump_sum' => Yii::t('app', 'All Lump Sum'),
            'all_profit' => Yii::t('app', 'All Profit'),
            'all_time' => Yii::t('app', 'All Time'),
            'all_time_personal' => Yii::t('app', 'All Time Personal'),
            'description' => Yii::t('app', 'Description'),
            'is_closed' => Yii::t('app', 'Is Closed'),
            'is_settled' => Yii::t('app', 'Is Settled'),
            'status' => Yii::t('app', 'Status'),
            'closed_at' => Yii::t('app', 'Closed At'),
            'closed_by' => Yii::t('app', 'Closed By'),
            'settled_at' => Yii::t('app', 'Settled At'),
            'settled_by' => Yii::t('app', 'Settled By'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'opened_by' => Yii::t('app', 'Opened By'),
            'year_from' => 'Rok od',
            'year_to' => 'Rok do',
            'month_from' => 'Miesiąc od',
            'month_to' => 'Miesiąc do',
            'type_order_fk' => 'Typ zlecenia',
            'id_customer_fk' => 'Klient',
            'verify' => 'Weryfikacja'
        ];
    }
    
   /* public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}*/
    
    public function getInvoices() {
        return \backend\Modules\Accounting\models\AccInvoice::find()->where(["status" => 1, "date_format(date_issue, '%Y-%c')" => $this->period_year.'-'.$this->period_month])->all();
    }
    
    public function getItems() {
        return \backend\Modules\Accounting\models\AccPeriodItem::find()->where(["id_period_fk" => $this->id])->all();
    }
    
    public function getSend() {
        return \backend\Modules\Accounting\models\AccPeriodArch::find()->where(["id_period_fk" => $this->id, 'user_action' => 'send'])->all();
    }
    
    public function getDiscount() {
        $sql = "select sum(discount_amount_native) as discount from {{%acc_discount}} where status = 1 and acc_period = '".($this->period_year.'-'.(($this->period_month < 10) ? '0'.$this->period_month : $this->period_month))."'";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        
        return ($data['discount']) ? $data['discount'] : 0;
    }
    
    public static function getValues($period) {
        $sql = "select sum(confirm_time) as inv_time, sum(acc_cost_native) as inv_costs, "
                ." (select sum(round(ii.amount_net*i.rate_exchange,2)) from {{%acc_invoice_item}} ii join {{%acc_invoice}} i on i.id=ii.id_invoice_fk where ii.is_cost = 0 and i.status = 1 and date_format(date_issue, '%Y-%m') = '".$period."') as inv_profit, "
                ." (select sum(d.discount_amount_native) from {{%acc_discount}} d  where d.status = 1 and acc_period = '".$period."' and d.id_invoice_fk != 0 and d.id_invoice_fk is not null) as inv_discount "
                ." from {{%acc_actions}} a "
                ." where is_confirm = 1 and a.status = 1 and (id_invoice_fk !=0 and id_invoice_fk is not null) and acc_period = '".$period."'"; 
	     
        $actionsData = Yii::$app->db->createCommand($sql)->queryOne();
        
        return $actionsData;
    }
    
    public static function getYears() {
        $years = [ 2017 => 2017 ];
        
        return $years;
    }
    
    public static function getMonths() {
        $years = [ 1 => 'styczeń', 2 => 'luty', 3 => 'marzec', 4 => 'kwiecień', 5 => 'maj', 6 => 'czerwiec', 7 => 'lipiec', 8 => 'sierpień', 9 => 'wrzesień', 10 => 'październik', 11 => 'listopad', 12 => 'grudzień' ];
        
        return $years;
    }
    
    public static function getOrderStates() {
        return [ 1 => 'rozliczone', 2 => 'nierozliczone', 3 => 'z czynnościami', 4 => 'brak czynności', 5 => 'do zafakturowania'];
    }
    
    public static function getOrderVerify() {
        return [ 1 => 'nieprzypisane', 2 => 'niewysłane [przypisane]', 3 => 'wysłane', 4 => 'potwierdzone [w tym częściowo]'];
    }
}
