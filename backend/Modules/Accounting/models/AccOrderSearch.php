<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Accounting\models\AccOrder;

/**
 * AccOrderSearch represents the model behind the search form about `backend\Modules\Task\models\AccOrder`.
 */
class AccOrderSearch extends AccOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_dict_order_type_fk', 'id_currency_fk', 'id_customer_fk', 'id_set_fk', 'id_department_fk', 'is_close', 'payment_type', 'payment_term', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_from', 'created_at', 'updated_at', 'deleted_at', 'customers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
