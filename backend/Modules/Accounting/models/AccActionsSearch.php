<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Accounting\models\AccActions;

/**
 * AccActionsSearch represents the model behind the search form about `backend\Modules\Task\models\AccActions`.
 */
class AccActionsSearch extends AccActions
{
    public $date_from;
    public $date_to;
    public $acc_period;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_service_fk', 'id_customer_fk', 'id_set_fk', 'id_employee_fk', 'id_department_fk', 'id_unit_rate_fk', 'unit_time', 'id_confirm_rate_fk', 'confirm_time', 'is_confirm', 'id_order_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['action_date', 'created_at', 'updated_at', 'deleted_at', 'timeH', 'timeM'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccActions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_fk' => $this->update_for_all,
            'id_type_fk' => $this->id_dict_case_type_fk,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'acc_period' => $this->acc_period,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
