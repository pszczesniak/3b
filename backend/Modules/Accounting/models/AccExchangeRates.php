<?php

namespace backend\Modules\Accounting\models;

use Yii;

/**
 * This is the model class for table "{{%acc_exchange_rates}}".
 *
 * @property integer $id
 * @property integer $id_currency_fk
 * @property string $rate_date
 * @property string $rate_value
 * @property string $rate_converter
 * @property string $currency
 * @property string $add_date
 * @property integer $is_active
 */
class AccExchangeRates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_exchange_rates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_currency_fk', 'is_active'], 'integer'],
            [['rate_date', 'rate_value', 'currency'], 'required'],
            [['rate_date', 'add_date'], 'safe'],
            [['rate_value', 'rate_converter'], 'number'],
            [['currency'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_currency_fk' => Yii::t('app', 'Id Currency Fk'),
            'rate_date' => Yii::t('app', 'Rate Date'),
            'rate_value' => Yii::t('app', 'Rate Value'),
            'rate_converter' => Yii::t('app', 'Rate Converter'),
            'currency' => Yii::t('app', 'Currency'),
            'add_date' => Yii::t('app', 'Add Date'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
    
    public static function activeRate($currency) {
        return self::find()->where(['is_active' => 1, 'id_currency_fk' => $currency])->one();
    }
}
