<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_invoice}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_order_fk
 * @property string $system_no
 * @property string $real_no
 * @property string $date_issue
 * @property double $amount_net
 * @property double $amount_gross
 * @property string $amount_of_words
 * @property double $billed_hours
 * @property integer $id_currency_fk
 * @property integer $payment_term
 * @property integer $payment_form
 * @property string $payment_account
 * @property integer $is_closed
 * @property integer $is_settled
 * @property integer $status
 * @property string $closed_at
 * @property integer $closed_by
 * @property string $settled_at
 * @property integer $settled_by
 * @property string $opened_at
 * @property integer $opened_by
 */
class AccInvoice extends \yii\db\ActiveRecord
{
    public $date_from;
    public $date_to;
    public $id_customer_fk;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_invoice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_order_fk', 'id_discount_fk', 'id_correction_fk', 'id_currency_fk', 'payment_term', 'payment_form', 'is_closed', 'is_settled', 'status', 'settled_by', 'opened_by', 'id_exchange_fk'], 'integer'],
            [['id_order_fk', 'system_no', 'real_no', 'date_issue'], 'required'],
            [['date_issue', 'settled_at', 'opened_at'], 'safe'],
            [['amount_net', 'amount_gross', 'billed_hours', 'rate_exchange'], 'number'],
            [['system_no', 'real_no', 'amount_of_words', 'payment_account'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_order_fk' => Yii::t('app', 'Id Order Fk'),
            'system_no' => Yii::t('app', 'System No'),
            'real_no' => Yii::t('app', 'Real No'),
            'date_issue' => Yii::t('app', 'Date Issue'),
            'amount_net' => Yii::t('app', 'Amount Net'),
            'amount_gross' => Yii::t('app', 'Amount Gross'),
            'amount_of_words' => Yii::t('app', 'Amount Of Words'),
            'billed_hours' => Yii::t('app', 'Billed Hours'),
            'id_currency_fk' => Yii::t('app', 'Id Currency Fk'),
            'payment_term' => Yii::t('app', 'Payment Term'),
            'payment_form' => Yii::t('app', 'Payment Form'),
            'payment_account' => Yii::t('app', 'Payment Account'),
            'is_closed' => Yii::t('app', 'Is Closed'),
            'is_settled' => Yii::t('app', 'Is Settled'),
            'status' => Yii::t('app', 'Status'),
            'closed_at' => Yii::t('app', 'Closed At'),
            'closed_by' => Yii::t('app', 'Closed By'),
            'settled_at' => Yii::t('app', 'Settled At'),
            'settled_by' => Yii::t('app', 'Settled By'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'opened_by' => Yii::t('app', 'Opened By'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_discount_fk' => 'Rabat',
			'id_correction_fk' => 'Rabat',
            'id_currency_fk' => 'Waluta'
        ];
    }
    
    public function beforeSave($insert) { 
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getOrder() {
		//return $this->hasOne(\backend\Modules\Accounting\models\AccOrder::className(), ['id' => 'id_order_fk']);
        return \backend\Modules\Accounting\models\AccOrder::findOne($this->id_order_fk);
    }
    
    public function getDiscount() {
		$sql = "select discount_percent"
                ." from {{%acc_discount}} d "
                ." where d.status = 1 and id_invoice_fk = ".$this->id; 
        $discountData = Yii::$app->db->createCommand($sql)->queryOne();
        
        return ($discountData['discount_percent']) ? $discountData['discount_percent'] : 0;
    }
	
	public function getCorrection() {
		$sql = "select correction_amount"
                ." from {{%acc_correction}} c "
                ." where c.status = 1 and id_invoice_fk = ".$this->id; 
        $discountData = Yii::$app->db->createCommand($sql)->queryOne();
        
        return ($discountData['correction_amount']) ? $discountData['correction_amount'] : 0;
    }
    
    public static function getSummary($id) {
        $sql = "select sum(amount_net) as s_net, sum(amount_vat) as s_vat, sum(amount_gross) as s_gross "
                ." from {{%acc_invoice_item}} ii "
                ." where ii.status = 1 and id_invoice_fk = ".$id; 
       return Yii::$app->db->createCommand($sql)->queryOne();
    }
    
    public function getCustomer() {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getItems() {
        return \backend\Modules\Accounting\models\AccInvoiceItem::find()->where(['id_invoice_fk' => $this->id, 'status' => 1])->orderby('id')->all();
    }
    
    public function getProfit() {
        return \backend\Modules\Accounting\models\AccInvoiceItem::find()->where(['id_invoice_fk' => $this->id, 'status' => 1, 'is_cost' => 0])->orderby('id')->all();
    }
    
    public function getCosts() {
        return \backend\Modules\Accounting\models\AccInvoiceItem::find()->where(['id_invoice_fk' => $this->id, 'status' => 1, 'is_cost' => 1])->orderby('id')->all();
    }
    
    public function getActions() {
        return \backend\Modules\Accounting\models\AccActions::find()->where(['id_invoice_fk' => $this->id, 'status' => 1, 'is_confirm' => 1])->orderby('is_priority desc, rank_priority, action_date, id')->all();
    }
    
    public function getGratis() {
        return \backend\Modules\Accounting\models\AccActions::find()->where(['id_invoice_fk' => $this->id, 'status' => 1, 'is_gratis' => 1])->orderby('action_date')->all();
    }
    
    public function getAll() {
        return \backend\Modules\Accounting\models\AccActions::find()->where(['id_invoice_fk' => $this->id, 'status' => 1])->andWhere('(is_confirm = 1 or is_gratis=1)')->orderby('is_priority desc, rank_priority, action_date, id')->all();
    }
    
    public function getValues() {
        $sql = "select sum(confirm_time) as inv_time, sum(acc_cost_net) as inv_costs, (select sum(amount_net) from {{%acc_invoice_item}} where id_invoice_fk = ".$this->id.") as inv_profit"
                ." from {{%acc_actions}} a "
                ." where is_confirm = 1 and a.status = 1 and id_invoice_fk = ".$this->id; 
        $actionsData = Yii::$app->db->createCommand($sql)->queryOne();
        
        return $actionsData;
    }
    
    public static function isExist($order, $period) {
        return AccInvoice::find()->where(['status' => 1, 'id_order_fk' => $order, 'date_issue' => $period.'-01'])->one();
    }
    
    public function getCurrency() {
		$currencies = [1 => 'PLN', 2 => 'EUR', 3 => 'USD'];
		
		return (isset($currencies[$this->id_currency_fk])) ? $currencies[$this->id_currency_fk] : 'PLN';
	}
}
