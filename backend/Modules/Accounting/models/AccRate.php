<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_rate}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property integer $id_service_fk
 * @property integer $id_customer_fk
 * @property integer $id_set_fk
 * @property integer $id_employee_fk
 * @property integer $id_department_fk
 * @property integer $id_currency_fk
 * @property string $date_from
 * @property string $date_to
 * @property double $unit_price
 * @property integer $unit_time
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AccRate extends \yii\db\ActiveRecord
{
    public $user_action = 'manage';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_fk', 'id_service_fk', 'id_customer_fk', 'id_set_fk', 'id_dict_employee_type_fk', 'id_employee_fk', 'id_department_fk', 'id_currency_fk', 'id_order_fk', 'unit_time', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_customer_fk', 'id_order_fk', 'id_dict_employee_type_fk', 'unit_price', 'date_from'], 'required'],
            [['date_from', 'date_to', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['unit_price'], 'number'],
            ['date_to', 'validateDates'],
            ['id_dict_employee_type_fk', 'validateExist', 'when' => function($model) { return ( $model->user_action != 'delete' ); } ],
        ];
    }
    
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Wprowadź poprawną datę końca');
		}
	}
    
    public function validateExist(){
		$exist = false;
        
        $exist = \backend\Modules\Accounting\models\AccActions::find()->where(['status' => 1, 'is_confirm' => 1, 'id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk])->andWhere(" (id_invoice_fk != 0 and id_invoice_fk is not null) ")->all();
        if($exist) {
            $this->addError('id_employee_fk', 'Pracownik ma już we wskazanym okresie czynności oznaczone do zafakturowania i nie można '.( ($this->isNewRecord) ? 'dodać' : 'edytować').' tej stawki');
            return;
        }
        /*if(!$this->id_customer_fk){
			if($this->date_to)
                $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'status' => 1])->andWhere("( ('".$this->date_from."' between date_from and date_to) or (".$this->date_to." between date_from and date_to) or (date_from <= ".$this->date_to." and date_to is null) )")->one();
            else
                $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'status' => 1])->andWhere("date_to is null")->one();
		} else {
            if(!$this->id_order_fk){
                if($this->date_to)
                    $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_customer_fk' => $this->id_customer_fk, 'status' => 1])->andWhere("( ('".$this->date_from."' between date_from and date_to) or (".$this->date_to." between date_from and date_to) or (date_from <= ".$this->date_to." and date_to is null) )")->one();
                else
                    $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_customer_fk' => $this->id_customer_fk, 'status' => 1])->andWhere("date_to is null")->one();
            } else {
                if($this->date_to)
                    $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])->andWhere("( ('".$this->date_from."' between date_from and date_to) or (".$this->date_to." between date_from and date_to) or (date_from <= ".$this->date_to." and date_to is null) )")->one();
                else
                    $exist = AccRate::find()->where(['id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])->andWhere("date_to is null")->one();
            }
        }*/
        if($this->isNewRecord) {
			if($this->date_to)
				$exist = AccRate::find()->where(['id_dict_employee_type_fk' => $this->id_dict_employee_type_fk, 'id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])
                                        ->andWhere("( ('".$this->date_from."' between date_from and date_to) or ('".$this->date_to."' between date_from and date_to) or (date_from between '".$this->date_from."' and '".$this->date_to."') or (date_to between '".$this->date_from."' and '".$this->date_to."') or ('".$this->date_from."' <= date_from and date_to is null) )")->one();
            else
                $exist = AccRate::find()->where(['id_dict_employee_type_fk' => $this->id_dict_employee_type_fk, 'id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])
                                        ->andWhere("( ('".$this->date_from."' <= date_to) or (date_to is null) )")->one();
        } else {
			if($this->date_to)
				$exist = AccRate::find()->where(['id_dict_employee_type_fk' => $this->id_dict_employee_type_fk, 'id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])
                                        ->andWhere('id != '.$this->id)->andWhere("( ('".$this->date_from."' between date_from and date_to) or ('".$this->date_to."' between date_from and date_to) or (date_from between '".$this->date_from."' and '".$this->date_to."') or (date_to between '".$this->date_from."' and '".$this->date_to."') or ('".$this->date_from."' <= date_from and date_to is null) )")->one();
			else
				$exist = AccRate::find()->where(['id_dict_employee_type_fk' => $this->id_dict_employee_type_fk, 'id_employee_fk' => $this->id_employee_fk, 'id_order_fk' => $this->id_order_fk, 'status' => 1])
                                        ->andWhere('id != '.$this->id)->andWhere("( ('".$this->date_from."' <= date_to) or (date_to is null) )")->one();
		}			
        if($exist) {
            if($this->id_employee_fk)
                $this->addError('id_employee_fk', 'Pracownik ma już we wskazanym okresie przypisaną stawkę <b>['.$exist->unit_price.' PLN - od '.$exist->date_from.' do '.(($exist->date_to) ? $exist->date_to : 'bez ograniczeń').']</b>');
            else
                $this->addError('id_dict_employee_type_fk', 'Ten typ pracownika ma już we wskazanym okresie przypisaną stawkę ['.$exist->unit_price.' PLN - od '.$exist->date_from.' do '.(($exist->date_to) ? $exist->date_to : 'bez ograniczeń').']');
        }   
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'id_service_fk' => Yii::t('app', 'Id Service Fk'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'id_set_fk' => Yii::t('app', 'Postępowanie'),
            'id_dict_employee_type_fk' => 'Typ pracownika',
            'id_employee_fk' => Yii::t('app', 'Pracownik'),
            'id_department_fk' => Yii::t('app', 'Dział'),
            'id_currency_fk' => Yii::t('app', 'Id Currency Fk'),
            'id_order_fk' => 'Zlecenie',
            'date_from' => Yii::t('app', 'Obowiązuje od'),
            'date_to' => Yii::t('app', 'Obowiązuje do'),
            'unit_price' => Yii::t('app', 'Stawka [PLN]'),
            'unit_time' => Yii::t('app', 'Unit Time'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;   
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCustomer()  {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
     public function getOrder() {
		return $this->hasOne(\backend\Modules\Accounting\models\AccOrder::className(), ['id' => 'id_order_fk']);
    }
    
    public function getEmployee()  {
		return $this->hasOne(\backend\Modules\Company\models\CompanyEmployee::className(), ['id' => 'id_employee_fk']);
    }
    
    public function getType()  {
		return $this->hasOne(\backend\Modules\Dict\models\DictionaryValue::className(), ['id' => 'id_dict_employee_type_fk']);
    }
    
    public static function getRate($employeeType, $employeeId, $customerId, $orderId, $accDate) {
        $rate = false;
        
        if($orderId) {
            $rate = AccRate::find()->where(['status' => 1, 'id_employee_fk' => $employeeId, 'id_order_fk' => $orderId])->one();
            if($rate) {
                return $rate;
            } else {
                $rate = AccRate::find()->where(['status' => 1, 'id_dict_employee_type_fk' => $employeeType, 'id_order_fk' => $orderId])->one();
                if($rate) 
                    return $rate;
            }
        }
        
        if($customerId) {
            $rate = AccRate::find()->where(['status' => 1, 'id_employee_fk' => $employeeId, 'id_customer_fk' => $customerId])->andWhere('(id_order_fk is null or id_order_fk = 0)')->one();
            if($rate) return $rate;
        }
        
        $rate = AccRate::find()->where(['status' => 1, 'id_employee_fk' => $employeeId])->andWhere('(id_order_fk is null or id_order_fk = 0) and (id_customer_fk is null or id_customer_fk = 0)')->one();
        if($rate) return $rate;
        
        return $rate;
    }
}
