<?php

namespace frontend\Modules\Accounting\models;

use Yii;
use yii\base\Model;


/**
 * ActionForm is the model behind the contact form.
 */
class ActionsUnsettle extends Model
{
    public $id_order_fk;
	public $actions = [];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order_fk','actions'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'actions' => 'Czynności',
			'id_order_fk' => 'Zlecenie'
        ];
    }

    public function saveSettle() {            
    
        return true;
    }
}
