<?php

namespace backend\Modules\Accounting\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%acc_invoice_item}}".
 *
 * @property integer $id
 * @property integer $id_invoice_fk
 * @property integer $item_no
 * @property string $item_name
 * @property double $amount_net
 * @property double $amount_gross
 * @property string $amount_of_words
 * @property double $billed_hours
 * @property string $billed_unit
 * @property double $billed_amount
 * @property double $billed_price
 * @property integer $id_vat_fk
 * @property double $currency_rate
 * @property integer $is_cost
 * @property integer $id_acc_action_id
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $settled_at
 * @property integer $settled_by
 * @property string $opened_at
 * @property integer $opened_by
 */
class AccInvoiceItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%acc_invoice_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_invoice_fk', 'item_no'], 'required'],
            [['type_fk', 'id_invoice_fk', 'item_no', 'id_vat_fk', 'is_cost', 'id_acc_action_id', 'status', 'created_by', 'settled_by', 'opened_by', 'id_discount_fk'], 'integer'],
            [['amount_net', 'amount_gross', 'billed_hours', 'billed_amount', 'billed_price', 'currency_rate'], 'number'],
            [['created_at', 'settled_at', 'opened_at'], 'safe'],
            [['item_name'], 'string', 'max' => 1000],
            [['amount_of_words', 'billed_unit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_invoice_fk' => Yii::t('app', 'Id Invoice Fk'),
            'item_no' => Yii::t('app', 'Item No'),
            'item_name' => Yii::t('app', 'Name'),
            'amount_net' => Yii::t('app', 'Amount Net'),
            'amount_gross' => Yii::t('app', 'Amount Gross'),
            'amount_of_words' => Yii::t('app', 'Amount Of Words'),
            'billed_hours' => Yii::t('app', 'Billed Hours'),
            'billed_unit' => Yii::t('app', 'Billed Unit'),
            'billed_amount' => Yii::t('app', 'Billed Ammount'),
            'billed_price' => Yii::t('app', 'Billed Price'),
            'id_vat_fk' => Yii::t('app', 'Stawka VAT'),
            'currency_rate' => Yii::t('app', 'Currency Rate'),
            'is_cost' => Yii::t('app', 'Is Cost'),
            'id_acc_action_id' => Yii::t('app', 'Id Acc Action ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'settled_at' => Yii::t('app', 'Settled At'),
            'settled_by' => Yii::t('app', 'Settled By'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'opened_by' => Yii::t('app', 'Opened By'),
        ];
    }
	
	public function beforeSave($insert) {
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getVat() {
		return $this->hasOne(\backend\Modules\Accounting\models\AccTax::className(), ['id' => 'id_vat_fk']);
    }
    
    public function getOrder()  {
		return $this->hasOne(\backend\Modules\Accounting\models\AccOrder::className(), ['id' => 'id_order_fk']);
    }
	
	public function getInvoice()  {
		return $this->hasOne(\backend\Modules\Accounting\models\AccInvoice::className(), ['id' => 'id_invoice_fk']);
    }
    
    public function getDiscount() {
		return $this->hasOne(\backend\Modules\Accounting\models\AccDiscount::className(), ['id' => 'id_discount_fk']);
    }
}
