<?php

namespace app\Modules\Mix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Mix\models\MixComment;

/**
 * MixCommentSearch represents the model behind the search form about `backend\Modules\Mix\models\MixComment`.
 */
class MixCommentSearch extends MixComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_comment_fk', 'id_root_fk', 'id_fk', 'id_client_fk', 'id_user_fk', 'is_owner', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'email', 'comment', 'comment_arch', 'created_at', 'updated_at', 'deleted_at', 'deleted_comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MixComment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_comment_fk' => $this->id_comment_fk,
            'id_root_fk' => $this->id_root_fk,
            'id_fk' => $this->id_fk,
            'id_client_fk' => $this->id_client_fk,
            'id_user_fk' => $this->id_user_fk,
            'is_owner' => $this->is_owner,
            'rank' => $this->rank,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'comment_arch', $this->comment_arch])
            ->andFilterWhere(['like', 'deleted_comment', $this->deleted_comment]);

        return $dataProvider;
    }
}
