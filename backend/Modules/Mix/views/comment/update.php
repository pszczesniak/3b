<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcComment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Svc Comment',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-comment-update", 'title'=>Html::encode($this->title))) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
