<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcComment */

$this->title = 'Komentarz: #'.$model->id;
if($model->id_fk == 0)
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opinie o portalu'), 'url' => ['manage']];
else
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Komentarze do ofert'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-comment-view", 'title'=>Html::encode($this->title))) ?>

    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->
    <?= Alert::widget() ?>
    <div id="message">
        <?php $status = [-1 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'];  $colors = [-1 => 'danger', 1 => 'info', 2 => 'success']; ?>
        <ul class="message-container">
            <li class="received">
                <div class="details">
                    <div class="left"><?= $model->name ?> <div class="arrow brand"></div> o portalu
                    </div>
                    <div class="right"><span class="label label-<?= $colors[$model->status] ?>"><?= $status[$model->status] ?></span><span class="details-date"><?= $model->created_at ?></span></div>
                </div>
                <div class="message">
                    
                    <?php for($i=1; $i<=$model->rank;++$i) echo '<i class="fa fa-star"></i>'; ?><br />
                    <?= $model->comment ?>
                    <?php if($model->status == -1) echo '<hr /><b>Powód odrzucenia:</b><span class="text-danger"> '.$model->deleted_comment .'  [ '.$model->deleted_at.' ]</span>'; ?>
                </div>
                
                <form id="comment-form" method="POST"> 
                    <div class="form-group field-mixcomment-deleted-comment">
                        <textarea rows="3" name="MixComment[deleted_comment]" placeholder="Powód odrzucenia" class="form-control"></textarea>
                        <div class="help-block"></div>
                        
                    </div>
                    <div class="clear"></div>
                
                    <div class="align-right">
                        <?php if($model->status != 2) { ?>
                            <a class="btn btn-sm btn-success btn-icon" href="<?= Url::to(['/mix/comment/commentaccept', 'id' => $model->id]) ?>" alt="Zaakceptuj" title="Zaakceptuj"><i class="fa fa-check"></i>Akceptuj</a>
                        <?php } ?>
                        <button class="btn btn-sm btn-danger btn-icon" type="submit"  alt="Odrzuć" title="Odrzuć"><i class="fa fa-ban"></i>Odrzuć</a>
                    </div>
                </form>
            </li>
        </ul>

    </div>
<?= $this->endContent(); ?>
