<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-svc-comments-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-comments']
    ]); ?>
    
    <div class="row">
        <?php if($type == 1) { ?>
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'id_offer_fk')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-comments', 'data-form' => '#filter-svc-comments-search' ])->label('Oferta <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'id_client_fk')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-comments', 'data-form' => '#filter-svc-comments-search' ])->label('Klient <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'status')->dropDownList( [0 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-comments', 'data-form' => '#filter-svc-comments-search' ] ) ?></div>
        <?php } else { ?>
        <div class="col-sm-6 col-md-6 col-xs-12"> <?= $form->field($model, 'id_client_fk')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-comments', 'data-form' => '#filter-svc-comments-search' ])->label('Klient <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-6 col-md-6 col-xs-12"><?= $form->field($model, 'status')->dropDownList( [0 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-comments', 'data-form' => '#filter-svc-comments-search' ] ) ?></div>
        <?php } ?>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
