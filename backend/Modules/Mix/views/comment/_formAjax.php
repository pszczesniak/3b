<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcComment */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-comments"],
        ]); ?>
    <div class="modal-body">

        <?= $form->field($model, 'id_user_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Apl\models\AplCustomer::getUsers(), 'id_user_fk', 'fullname'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] ) ?> 
       
        <div class="row">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        </div>

        <?= $form->field($model, 'user_info')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
    </div>
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>
<?php ActiveForm::end(); ?>
<script type="text/javascript">
    document.getElementById('mixcomment-id_user_fk').onchange = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/apl/aplcustomer/info']) ?>/"+this.value, true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('mixcomment-name').value = result.info.lastname + ' ' + result.info.firstname; 
                document.getElementById('mixcomment-email').value = result.info.email;              
            }
        }
       xhr.send();
        return false;
    }
</script>