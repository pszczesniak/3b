<?php

namespace app\Modules\Document\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

use common\models\Structure;
use common\models\Files;
use backend\Modules\Task\models\CalCase;
use common\components\CustomHelpers;

/**
 * Types controller for the `Document` module
 */
class TypesController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()  {
        $grants = $this->module->params['grants'];
        $model = new Files();
        return $this->render('index', ['model' => $model, 'grantNew' => ($this->module->params['isAdmin'] == 1 || $this->module->params['isSpecial']) ? 'yes' : 'no', 
                                                          'grantEdit' => ($this->module->params['isAdmin'] == 1 || $this->module->params['isSpecial']) ? 'yes' : 'no', 
                                                          'grantDelete' => ($this->module->params['isAdmin'] == 1 || $this->module->params['isSpecial']) ? 'yes' : 'no', ]);
    }
    
    public function actionFolders() {
        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int)$_GET['id'] : 0;
        Yii::$app->response->format = Response::FORMAT_JSON;
        
       // $sql = " SELECT *  FROM {{%pattern}} s  WHERE  s.id_parent_fk = ". $node ." ORDER BY s.pos";
       // $temp = Yii::$app->db->createCommand($sql)->queryAll();
        $temp = Structure::find()->where(['status' => 1, 'id_parent_fk' => $node])->all();
        $rslt = [];
        foreach($temp as $key => $model) {
            //if($model->item_type == 'file')
            //    $model->item_name = '<a href="'.Url::to(['/files/upload', 'id' => $model->id]).'" title="Pobierz plik">'.$model->item_name.'</a>';
            $rslt[] = array('id' => $model->id, 'type' => $model->item_type, 'text' =>$model->item_name, 'children' => (count($model->children) > 0) ? true : false, 
                            'a_attr' => [/*'class' => 'file-download',*/ 'href' => '/files/getfile/'.$model->id_file_fk] /*Url::to(['/files/getfile', 'id' => $model->id_file_fk])*/,
                            'li_attr' => ['data-new' => (isset($grants['documentPatternAdd'])) ? 'yes' : 'no', 'data-edit' => (isset($grants['documentPatternEdit'])) ? 'yes' : 'no', 'data-remove' => (isset($grants['documentPatternDelete'])) ? 'yes' : 'no', ]
                        );
        }
        
        return $rslt;
    }
    
    public function actionNodes() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int)$_GET['id'] : 0;
        $nodes = [];
        
        $model = Structure::findOne($node);
        if($model->item_type == 'file')
            $model = Structure::findOne($model->id_parent_fk);
        $nodes['content'] = $this->renderAjax('_form', ['model' => $model]);
        
        return $nodes;
    }
    
    public function actionCreate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new Structure();
        $model->type_fk = 1;
        $model->id_parent_fk = $_GET['id'];
        $model->item_name = $_GET['text'];
		$model->item_type = 'category';
        $model->status = 1;
        $model->save();
        
        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int)$_GET['id'] : 0;
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name];
        
        return $rslt;
    }
    
    public function actionRename() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = Structure::findOne($_GET['id']);
        $model->item_name = $_GET['text'];
        $model->save();
        
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name];
        
        return $rslt;
    }
    
    public function actionDelete() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = Structure::findOne($_GET['id']);
        if($model->item_type == 'root') {
            $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name, 'success' => false, 
                 'alert' => 'Ten element nie może zostać usunięty.', 'html' =>  $this->renderAjax('_form', ['model' => $model->parent]), 
                 'node' => ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name] 
                ];
        } else {
            $model->delete();
        }
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name, 'success' => true, 
                 'alert' => 'Wzór dokumentu został usunięty.', 'html' =>  $this->renderAjax('_form', ['model' => $model->parent]), 
                 'node' => ['id' => $model->id_parent_fk, 'parent' => $model->parent['id_parent_fk'], 'text' => $model->parent['item_name']] 
                ];
        
        return $rslt;
    }
    
        
    public function actionUpload()  {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new Files(['scenario' => Files::SCENARIO_UPLOAD]);
		$model->load(Yii::$app->request->post());
       // $postData = Yii::$app->request->post('Files');
       // if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstances($model, 'files');
			//$model->name_file = $postData['name_file'];
            if ($model->upload()) {
                // file is uploaded successfully
                //return ['result' => true, 'success' => 'Pliki zostały dodane', 'new' => $model->files_list];
                
                $node = new Structure();
                $node->id_parent_fk = $model->id_fk;
                $node->item_name = $model->title_file;
                $node->item_type = 'file';
                $node->id_file_fk = $model->id;
                $node->status = 0;
                $node->save();
                
               // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
                $parent = Structure::findOne($model->id_fk);
                
                $row = '<tr class="no-active">'
                       // .'<td><a href="'.Url::to(['/files/getfile', 'id' => $node->id_file_fk]).'">'.$node->item_name.'</a></td>'
                        .'<td><a href="/files/getfile/'.$node->id_file_fk.'">'.$node->item_name.'</a></td>'
                        .'<td>'.$node->user.'</td>'
                        .'<td>teraz</td>'
                        .'<td>'
                            .'<a href="/document/default/delete?id='. $node->id .'" class="btn btn-xs btn-danger pattern-action"><i class="fa fa-trash"></i></a>'
                            .'<a href="/document/default/accept?id='. $node->id .'" class="btn btn-xs btn-success pattern-action"><i class="fa fa-check"></i></td>'
                    .'</tr>';
                
                $rslt = ['result' => true,'node' => ['id' => $node->id_parent_fk, 'parent' => $node->parent['id_parent_fk'], 'text' => $node->parent['item_name']], 'id' => $node->id_parent_fk, 'row' => $row ];
                return $rslt;
            } else {
				$errors = $model->getErrors();
				foreach($errors as $key=>$value) {
					$error = $value;
				}
				return ['result' => false, 'error' => $value];
			}
       // }

        /*$this->render('upload', ['model' => $model])*/;
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = Files::find()->where(['status' => 1]);
        
        if(isset($_GET['Files'])) {
            $params = $_GET['Files'];
            if(isset($params['name_file']) && !empty($params['name_file']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name_file) like '%".strtolower($params['name_file'])."%'");
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_case_fk from law_case_employee where id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_type_file_fk']) && !empty($params['id_dict_type_file_fk']) ) { 
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_type_file_fk = ".$params['id_dict_type_file_fk']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(created_at, '%Y-%m-%d') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("DATE_FORMAT(created_at, '%Y-%m-%d') <= '".$params['date_to']."'");
            }
        } else {
            $fieldsDataQuery = $fieldsDataQuery->limit(100);
            //if(isset($_GET['id'])) $fieldsDataQuery = $fieldsDataQuery->andWhere("id = ".$_GET['id']);
        }
        
        if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin']) {
           /* if($this->module->params['employeeKind'] == 70)
                $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
            else */
                $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_case_fk from {{%case_employee}} where id_employee_fk = '.$this->module->params['employeeId'].' )');
        }     
        $fieldsData = $fieldsDataQuery->orderby('created_at desc')->all();
			
			
		$fields = [];
		$tmp = [];
		      
        /*$actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/new/task/project/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/new/task/project/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/new/task/project/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';*/
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
		foreach($fieldsData as $key=>$value) {
			
			$tmp['client'] = 'test';//$value->customer['name'];
            $tmp['name'] = $value->name_file;
			//$tmp['name'] = '<a href="'.Url::to(['/task/project/view', 'id' => $value->id]).'" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value->name.'</a>';
            $tmp['size'] = round($value->size_file/1024, 2);
            $tmp['ext'] = round($value->extension_file, 2);
            $tmp['created'] = Yii::$app->runAction('common/ago', ['date' => $value->created_at]);
            //$tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            /*$tmp['actions'] = '<div class="edit-btn-group">'
									.'<a href="'.Url::to(['/task/project/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>'
									.'<a href="'.Url::to(['/task/project/update', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>'
									.'<a href="'.Url::to(['/task/project/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>'
								.'</div>';*/
			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/project/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-upload"></i></a>';
		    $tmp['actions'] .= '</div>';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value->id);
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

}
