<?php

namespace app\Modules\Document\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

use backend\Modules\Document\models\Pattern;
use common\models\Files;


/**
 * Default controller for the `Document` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $grants = $this->module->params['grants'];
        return $this->render('index', ['grantNew' => (isset($grants['documentPatternAdd'])) ? 'yes' : 'no', 'grantEdit' => (isset($grants['documentPatternEdit'])) ? 'yes' : 'no', 'grantDelete' => (isset($grants['documentPatternDelete'])) ? 'yes' : 'no', ]);
    }
    
    public function actionFolders() {
        $node = (  isset($_GET['id']) && $_GET['id'] !== '#' ) ? (int)$_GET['id'] : 0;
        Yii::$app->response->format = Response::FORMAT_JSON;
        
       // $sql = " SELECT *  FROM {{%pattern}} s  WHERE  s.id_parent_fk = ". $node ." ORDER BY s.pos";
       // $temp = Yii::$app->db->createCommand($sql)->queryAll();
       $temp = Pattern::find()->where(['status' => 1, 'id_parent_fk' => $node])->all();
        $rslt = [];
        foreach($temp as $key => $model) {
            //if($model->item_type == 'file')
            //    $model->item_name = '<a href="'.Url::to(['/files/upload', 'id' => $model->id]).'" title="Pobierz plik">'.$model->item_name.'</a>';
            $rslt[] = array('id' => $model->id, 'type' => $model->item_type, 'text' =>$model->item_name, 'children' => (count($model->children) > 0) ? true : false, 
                            'a_attr' => [/*'class' => 'file-download',*/ 'href' => Url::to(['/files/getfile', 'id' => $model->id_file_fk])],
                            'li_attr' => ['data-new' => (isset($grants['documentPatternAdd'])) ? 'yes' : 'no', 'data-edit' => (isset($grants['documentPatternEdit'])) ? 'yes' : 'no', 'data-remove' => (isset($grants['documentPatternDelete'])) ? 'yes' : 'no', ]
                        );
        }
        
        //$temp = $fs->get_children($node);
        
       /* array_push($rslt, ['id' => 'ajson1', 'parent' => '#', 'text' => 'Simple root node']);
        array_push($rslt, ['id' => 'ajson2', 'parent' => '#', 'text' => 'Root node 2']);
        array_push($rslt, ['id' => 'ajson3', 'parent' => 'ajson2', 'text' => 'Child 1']);
        array_push($rslt, ['id' => 'ajson4', 'parent' => 'ajson2', 'text' => 'Child 2']);*/
        
        /*foreach($temp as $v) {
            $rslt[] = array('id' => $v['id'], 'text' => $v['nm'], 'children' => ($v['rgt'] - $v['lft'] > 1));
        }*/
        
        return $rslt;
    }
    
    public function actionNodes() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int)$_GET['id'] : 0;
        $nodes = [];
        
        $model = Pattern::findOne($node);
        if($model->item_type == 'file')
            $model = Pattern::findOne($model->id_parent_fk);
        $nodes['content'] = $this->renderAjax('_form', ['model' => $model]);
        
        return $nodes;
    }
    
    public function actionCreate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new Pattern();
        $model->id_parent_fk = $_GET['id'];
        $model->item_name = $_GET['text'];
		$model->item_type = 'folder';
        $model->status = 1;
        $model->save();
        
        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int)$_GET['id'] : 0;
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name];
        
        return $rslt;
    }
    
    public function actionRename() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = Pattern::findOne($_GET['id']);
        $model->item_name = $_GET['text'];
        $model->save();
        
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name];
        
        return $rslt;
    }
    
    public function actionDelete() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = Pattern::findOne($_GET['id']);
        if($model->item_type == 'root') {
            $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name, 'success' => false, 
                 'alert' => 'Ten element nie może zostać usunięty.', 'html' =>  $this->renderAjax('_form', ['model' => $model->parent]), 
                 'node' => ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name] 
                ];
        } else {
            $model->delete();
        }
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name, 'success' => true, 
                 'alert' => 'Wzór dokumentu został usunięty.', 'html' =>  $this->renderAjax('_form', ['model' => $model->parent]), 
                 'node' => ['id' => $model->id_parent_fk, 'parent' => $model->parent['id_parent_fk'], 'text' => $model->parent['item_name']] 
                ];
        
        return $rslt;
    }
    
    public function actionAccept() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = Pattern::findOne($_GET['id']);
        Pattern::updateAll(['status' => 0], 'status = 1 and id_parent_fk = '.$model->id_parent_fk);
        $model->status = 1;
        $model->save();
        
       // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
        $rslt = ['id' => $model->id, 'parent' => $model->id_parent_fk, 'text' => $model->item_name, 'success' => true, 
                 'alert' => 'Wzór dokumentu został zaakceptowany.', 'html' =>  $this->renderAjax('_form', ['model' => $model->parent]),
                 'node' => ['id' => $model->id_parent_fk, 'parent' => $model->parent['id_parent_fk'], 'text' => $model->parent['item_name']]
                ];
        
        return $rslt;
    }
    
    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new Files(['scenario' => Files::SCENARIO_UPLOAD]);
		$model->load(Yii::$app->request->post());
       // $postData = Yii::$app->request->post('Files');
       // if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstances($model, 'files');
			//$model->name_file = $postData['name_file'];
            if ($model->upload()) {
                // file is uploaded successfully
                //return ['result' => true, 'success' => 'Pliki zostały dodane', 'new' => $model->files_list];
                
                $node = new Pattern();
                $node->id_parent_fk = $model->id_fk;
                $node->item_name = $model->title_file;
                $node->item_type = 'file';
                $node->id_file_fk = $model->id;
                $node->status = 0;
                $node->save();
                
               // $temp = $this->mk($node, isset($_GET['position']) ? (int)$_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
                $parent = Pattern::findOne($model->id_fk);
                
                $row = '<tr class="no-active">'
                        .'<td><a href="'.Url::to(['/files/getfile', 'id' => $node->id_file_fk]).'">'.$node->item_name.'</a></td>'
                        .'<td>'.$node->user.'</td>'
                        .'<td>teraz</td>'
                        .'<td>'
                            .'<a href="'.Url::to(['/document/default/delete', 'id' => $node->id]).'" class="btn btn-xs btn-danger pattern-action"><i class="fa fa-trash"></i></a>'
                            .'<a href="'.Url::to(['/document/default/accept', 'id' => $node->id]).'" class="btn btn-xs btn-success pattern-action"><i class="fa fa-check"></i></td>'
                    .'</tr>';
                
                $rslt = ['result' => true,'node' => ['id' => $node->id_parent_fk, 'parent' => $node->parent['id_parent_fk'], 'text' => $node->parent['item_name']], 'id' => $node->id_parent_fk, 'row' => $row ];
                return $rslt;
            } else {
				$errors = $model->getErrors();
				foreach($errors as $key=>$value) {
					$error = $value;
				}
				return ['result' => false, 'error' => $value];
			}
       // }

        /*$this->render('upload', ['model' => $model])*/;
    }

}
