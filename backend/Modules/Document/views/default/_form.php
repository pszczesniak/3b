<?php
    use yii\helpers\Url;
?>
<div class="patterns">
    <table class="pattern-details">
        <thead>
            <tr><td class="details-label">Folder</td><td colspan="3"><?= $model->item_name ?></td></tr>
            <tr><td class="details-label">Autor</td><td><?= $model->user ?></td><td class="details-label">Utworzono</td><td><?= $model->created_at ?></td></tr>
        </thead>
        <tbody>
            <tr><td class="details-label" colspan="4">Wzory dokumentów</td></tr>
            <tr><td class="details-label">Plik</td><td class="details-label">Autor</td><td class="details-label">Utworzono</td><td class="details-label">Operacje</td></tr>
            <!--<tr><td>document.docx</td><td>Paweł ciura</td><td>2016-07-15 13:23:48</td><td class="details-label"><a href="3"><i class="fa fa-trash"></i></a></td></tr>-->
            <?php 
                foreach($model->files as $key => $file) {
                    echo '<tr id="row-'.$file->id.'" class="'. ( ($file->status == 1) ? 'active' : 'no-active' ) .'">'
                            .'<td><a href="'.Url::to(['/files/getfile', 'id' => $file->id_file_fk]).'">'.$file->item_name.'</a></td>'
                            .'<td>'.$file->user.'</td>'
                            .'<td>'.$file->created_at.'</td>'
                            .'<td>'
                                .'<a href="'.Url::to(['/document/default/delete', 'id' => $file->id]).'" class="btn btn-xs btn-danger pattern-action"><i class="fa fa-trash"></i></a>'
                                . ( ($file->status == 0) ? '<a href="'.Url::to(['/document/default/accept', 'id' => $file->id]).'" class="btn btn-xs btn-success pattern-action"><i class="fa fa-check"></i></td>' : '' )
                        .'</tr>';
                }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Wybierz nowy wzorzec <input type="file" style="display: none;" class="ufile">
                                    </span>
                                </label>
                                <input type="text"  class="form-control ufile-name">
                            </div>
                        </div>
                        <div class="col-sm-4"><button class="btn btn-success btn-file-tree"  data-action="<?= Url::to(['/document/default/upload']) ?>" data-type="5" data-id="<?= $model->id ?>">Zapisz nowy wzorzec</button></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
