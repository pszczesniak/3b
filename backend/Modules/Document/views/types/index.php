<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rejestr dokumentów');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'document-types-index', 'title'=>Html::encode($this->title))) ?>
<div class="grid">
    <div class="col-sm-4 col-md-3">  
        <div id="structure" data-new="<?= $grantNew ?>" data-edit="<?= $grantEdit ?>" data-remove="<?= $grantDelete ?>"></div>
    </div>  
    <div class="col-sm-8 col-md-9"> 
        <?php  echo $this->render('_search', ['model' => $model]); ?>
        <table  class="table table-striped table-items header-fixed"  id="table-files"  data-url=<?= Url::to(['/document/types/data']) ?> >
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true" data-width="60%">Nazwa</th>
                    <th data-field="created"  data-sortable="true"  data-width="10%">Date</th>
                    <th data-field="size"  data-sortable="true" data-align="right" data-width="10%">Rozmiar [kB]</th>
                    <th data-field="ext"  data-sortable="true" data-align="center" data-width="10%"></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
           
    </div>
</div>

<?php $this->endContent(); ?>

