<?php

namespace app\Modules\Document;

use Yii;
/**
 * Document module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\Modules\Document\controllers';

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        if(!Yii::$app->user->isGuest) {
			if(!in_array(Yii::$app->controller->id, [ 'site' ]) ){
				/*if(!$this->params['departments'] ) {
					throw new \yii\web\HttpException(405, 'Parametry sesji nie zosta�y ustawione lub wygas�y. Aby rozpocz�� prac� z systemem nale�y wybra� opcj� konfiguracji sesji i ustawi� wymagane parametry..');
				}*/
                if(Yii::$app->runAction('/site/mustresetpassword', []) >= 30 && \Yii::$app->user->identity->username != 'superadmin')
                    return Yii::$app->getResponse()->redirect(Url::to(['/site/changepassword']));
			}
        } else {
			//return $this->redirect(Url::to(['/site/login']));
			return Yii::$app->getResponse()->redirect(Url::to(['/site/login']));
		}

        return true; // or false to not run the action
    }
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        $grants = [];
        $departments = [];
        if(!Yii::$app->user->isGuest) {
            if(\Yii::$app->user->identity->username != 'superadmin') {
            
                $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
                
                if(!$employee->is_admin) {
                    if($temp = $employee->departments) {
                        foreach($temp as $key => $department) {
                            array_push($departments, $department->id_department_fk);
                        }
                    }
                    
                    if(!Yii::$app->user->isGuest /*&& $this->params['structureId']*/) {
                        $auth = Yii::$app->authManager;
                        $roles = $auth->getRolesByUser(Yii::$app->user->identity->id);//var_dump($roles); exit;
                       // $departments = \backend\Modules\Company\models\EmployeeDepartment::find()->where(['id_zone_structure_fk' => $this->params['structureId'], 'id_user_fk' => Yii::$app->user->identity->id])->one();
                        foreach($roles as $key=>$value) {

                            $grantsRole = $auth->getPermissionsByRole($value->name); 
                            foreach($grantsRole as $key1=>$value1) {
                                array_push($grants, $key1);
                            }
                           
                            array_push($grants, $key);
                        }
                        $grantsUser = $auth->getPermissionsByUser(Yii::$app->user->identity->id); 
                    }   

                    $this->params['employeeId'] = $employee->id;
                    $this->params['employeeKind'] = $employee->id_dict_employee_kind_fk;
                } else {
                    array_push($grants, 'grantAll');
                }
            } else {
                array_push($grants, 'grantAll');
            }
        }
        $this->params['grants'] = $grants;
        $this->params['departments'] = $departments;
        //var_dump($grants); exit;
	    $app = Yii::$app;
	    $app->session->set('user.module','company');
	   
       
		parent::init();
    }
}
