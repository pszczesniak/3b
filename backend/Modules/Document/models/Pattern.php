<?php

namespace backend\Modules\Document\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%pattern}}".
 *
 * @property string $id
 * @property integer $id_parent_fk
 * @property string $name
 * @property integer $lvl
 * @property integer $lft
 * @property integer $rgt
 * @property integer $pos
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Pattern extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pattern}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_fk', 'id_file_fk', 'lvl', 'lft', 'rgt', 'pos', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['item_name', 'item_type'], 'required'],
            [['created_at', 'updated_at', 'deleted_at', 'id_file_fk'], 'safe'],
            [['item_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent_fk' => 'Id Parent Fk',
            'item_name' => 'Name',
            'item_type' => 'Type',
            'lvl' => 'Lvl',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'pos' => 'Pos',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if(!$this->id_parent_fk) $this->id_parent_fk = 0;
            //if(!$this->status) $this->status = 1;
            if(!$this->item_type) $this->item_type = 'folder';
			
			if($this->isNewRecord ) {
				if(!$this->created_by) $this->created_by = Yii::$app->user->id;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getChildren()  {
		$items = $this->hasMany(self::className(), ['id_parent_fk' => 'id'])->where(['status' => '1'])->all(); 
		/*if(count($items) > 0)
			return $items;
		else
			return false;*/
        return $items;
    }
    
    public function getFiles()  {
		$items = Pattern::find()->where(['id_parent_fk' => $this->id, 'item_type' => 'file'])->andWhere('status >= 0')->orderby('status desc, created_at desc')->all(); 
		/*if(count($items) > 0)
			return $items;
		else
			return false;*/
        return $items;
    }
    
    public function getParent()
    {
		return $this->hasOne(\backend\Modules\Document\models\Pattern::className(), ['id' => 'id_parent_fk']);
    }
    
    public function getUser() {
        return \common\models\User::findOne($this->created_by)->fullname;
    }
}
