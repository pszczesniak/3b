<?php

namespace backend\Modules\Document\models;

use Yii;

/**
 * This is the model class for table "{{%pattern_struct}}".
 *
 * @property string $id
 * @property string $lft
 * @property string $rgt
 * @property string $lvl
 * @property string $pid
 * @property string $pos
 */
class PatternStruct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pattern_struct}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lft', 'rgt', 'lvl', 'pid', 'pos'], 'required'],
            [['lft', 'rgt', 'lvl', 'pid', 'pos'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'lvl' => 'Lvl',
            'pid' => 'Pid',
            'pos' => 'Pos',
        ];
    }
}
