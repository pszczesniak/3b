<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Customer Person',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="customer-person-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
