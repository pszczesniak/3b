<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */

$this->title = Yii::t('app', 'Create Customer Person');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-person-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
