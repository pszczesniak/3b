<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'person-form', 
									'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
									/*'action' => Url::to(['/lsdd/lsddcompanyperson/create']),*/ 
									'options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>false, 'enableClientValidation'=>false,  'data-table' => '#table-persons','data-target' => "#modal-grid-person"],
								 ]); ?>
    <div class="modal-body calendar-task">
        <div class="grid">
            <div class="col-xs-12 col-md-6 col-sm-6">
                <div class="grid">
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="grid">
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textArea(['row' => 2, 'maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6">
                <fieldset><legend>Oddział</legend>
                    <?= $form->field($model, 'id_customer_branch_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CustomerBranch::find()->where(['status' => 1, 'id_customer_fk' => $model->id_customer_fk])->orderby('name')->all(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] )->label(false) ?>
                </fieldset>  
                <?php if($model->isNewRecord && \Yii::$app->params['env'] == 'dev' && $model->customer['type_fk'] == 1) { ?>
					<fieldset><legend>Konto w systemie</legend>
						<?= $form->field($model, 'account', ['template' => '{input}{label}{hint}{error}'])
													->checkbox(['maxlength' => true, 'class' => 'form__checkbox', 'label' => null])->label('Czy utworzyć konto w systemie dla tej osoby?') ?>
						<div class="grid">
							<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?></div>
							<div class="col-sm-6 col-xs-12"><?= $form->field($model, 'user_role')->dropDownList(\backend\Modules\Crm\models\CustomerPerson::listRoles(), ['prompt' => ' - wybierz -'] ) ?></div>
						</div>                   
					</fieldset>
				<?php } ?>
                <div class="align-center">
                    <?php 
                        if($model->isNewRecord) {
                            echo '<div class="alert alert-warning">Aby dodać zdjęcie musisz utworzyć kontakt.</div>';
                        } else {
                            echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                [
                                    'id' => 'imgAttachmentPerson',
                                    'model' => $model,
                                    'behaviorName' => 'coverBehavior',
                                    'apiRoute' => '/crm/person/imgAttachApi',
                                ]
                            
                            );
                        }
                    ?>
                </div>
            </div>
        </div>

        
    </div>
    <div class="modal-footer">   
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>