<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-crm-customers-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-customers']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-3 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_department_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_dict_customer_status_id', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Crm\models\Customer::listStatus(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
        <div class="col-sm-4 col-md-3 col-xs-12"><?= $form->field($model, 'id_dict_customer_debt_collection_id', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Crm\models\Customer::listDebts(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
