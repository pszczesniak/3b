<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Klienci - nowy');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_nsearch', ['model' => $searchModel]); ?>
    <?= Alert::widget() ?>
 
	<div id="toolbar-customers" class="btn-group">
		 <?= ( ( count(array_intersect(["customerAdd", 'grantAll'], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/crm/customer/create'] ) , 
					['class' => 'btn btn-success btn-icon', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) : '' ?>
        <?= ( ( count(array_intersect(["customerExport", "grantAll"], $grants)) > 0 )) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/crm/customer/export'] ) , 
                    ['class' => 'btn btn-info btn-icon btn-export', 
                     'id' => 'case-create',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
                     'data-form' => "item-form", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) : '' ?>
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-customers"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-customers"
                data-toolbar="#toolbar-customers" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-crm-customers-search"
                data-url=<?= Url::to(['/crm/customer/ndata']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true" data-width="90%">Nazwa</th>
                    <th data-field="actions" data-events="actionEvents" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';
        

		yii\bootstrap\Modal::end();
	?> 

<?php $this->endContent(); ?>
