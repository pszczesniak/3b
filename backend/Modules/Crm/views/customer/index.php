<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kontrahenci');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-index', 'title'=>Html::encode($this->title))) ?>
    <fieldset>
		<legend>Filtrowanie danych <button class="btn-reset-filter" type="button" title="Zresetuj filtr" data-table="#table-customers" data-form="#filter-crm-customers-search"><i class="fa fa-eraser text--teal"></i></button>
			<a aria-controls="actions-filter" aria-expanded="true" href="#actions-filter" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
			</a>
		</legend>
		<div id="actions-filter" class="collapse in">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </fieldset>
    
    <?= Alert::widget() ?>
 
	<div id="toolbar-customers" class="btn-group toolbar-table-widget">
		 <?= ( ( count(array_intersect(["customerAdd", 'grantAll'], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/crm/customer/create'] ) , 
					['class' => 'btn btn-success btn-icon', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) : '' ?>
        <?= ( ( count(array_intersect(["customerExport", "grantAll"], $grants)) > 0 )) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/crm/customer/export'] ) , 
                    ['class' => 'btn btn-info btn-icon btn-export', 
                     'id' => 'customer-export',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-crm-customers-search", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) : '' ?>
            
		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-customers"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget table-multi-action"  id="table-customers"
                data-toolbar="#toolbar-customers" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-checkbox-header="false"
				data-search-form="#filter-crm-customers-search"
                data-url=<?= Url::to(['/crm/customer/data']) ?>>
            <thead>
                <tr>
                    <!--<th data-field="id" data-visible="false">ID</th><th data-field="state" data-visible="true" data-checkbox="true"></th>-->
                    <th data-field="symbol" data-sortable="true">Symbol</th>
                    <th data-field="name"  data-sortable="true">Nazwa</th>
                    <th data-field="nip"  data-sortable="true">NIP</th>
                    <th data-field="group"  data-sortable="true">Segment</th>
                    <th data-field="type"  data-sortable="false" data-width="20px" data-align="center"></th>
                    <th data-field="status"  data-sortable="false">Status</th>
                    <th data-field="archive"  data-sortable="false" data-align="center" data-width="20px"><i class="fa fa-archive" data-toggle="tooltip" data-title="Archiwalny"></i></th>       
                    <?php if(Yii::$app->params['env'] == 'dev' && Yii::$app->params['showClient']) { ?>
                    <th data-field="account"  data-sortable="true" data-align="center" data-width="20px"><i class="fa fa-key" data-toggle="tooltip" data-title="Konto w systemie"></i></th>
                    <?php } ?>
                    <th data-field="actions" data-events="actionEvents" data-width="70px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Blokada windykacyjna</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>

	<!-- Render modal form -->
	

<?php $this->endContent(); ?>
