<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-crm-customers-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-customers']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'id_crm_group_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CrmGroup::getList(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>        
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'id_dict_customer_status_id', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  \backend\Modules\Crm\models\Customer::listStatus(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'type_fk', [/*'template' => '{label}<div class="form-select">{input}</div>{error}'*/])->dropDownList(\backend\Modules\Crm\models\Customer::listRoles(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'is_archive', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList([0 => 'aktywny', 1 => 'archiwalny'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'nip')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ])->label('NIP <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>                
        <div class="col-sm-<?= (Yii::$app->params['departments']) ? '3' : '6' ?> col-md-<?= (Yii::$app->params['departments']) ? '3' : '6' ?> col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'symbol')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ])->label('Symbol <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>        
        <?php if(Yii::$app->params['departments']) { ?>
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'id_department_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ] ) ?></div>    
        <?php } ?>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
