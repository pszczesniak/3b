<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-employee-form form">

    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
               // 'template' => '<div class="grid"><div class="col-xs-3">{label}</div><div class="col-xs-9">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    
    
        <fieldset><legend>Podstawowe</legend>
            <div class="grid">
                <div class="col-sm-6"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="grid">
                <div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
            </div>
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </fieldset>
        <fieldset><legend>Dodatkowe</legend>
            <div class="grid">
                <div class="col-sm-6">
                    <?= $form->field($model, 'id_company_branch_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(), 'id', 'name'), [] ) ?> 
                    <?= $form->field($model, 'id_dict_employee_type_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listTypes(), [] ) ?>
                    <?= $form->field($model, 'id_dict_employee_kind_fk')->dropDownList(\backend\Modules\Company\models\CompanyEmployee::listKinds(), [] ) ?> 
                </div>
                <div class="col-sm-6">
                    <label>Działy</label>
                    <?= $form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyDepartment::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'multiple' => 'multiple', ] ); ?>
                </div>
            </div>

            
        </fieldset>
        <div class="form-group align-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


    <?php ActiveForm::end(); ?>

</div>
