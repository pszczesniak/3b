<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>


<table  class="table table-striped table-items header-fixed"  id="table-cases"
        data-toolbar="#toolbar-data" 
        data-toggle="table" 
        data-show-refresh="false" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"
        data-url=<?= Url::to(['/crm/customer/tcases', 'id'=>$id]) ?>>	 
        
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="name"  data-sortable="true" >Tytuł</th>
            <th data-field="type"  data-sortable="true" >Typ</th>
            <th data-field="status"  data-sortable="true" >Status</th>
            <th data-field="actions" data-events="actionEvents" data-width="30px"></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

    

    

    



