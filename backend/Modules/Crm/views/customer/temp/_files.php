<?php
    use frontend\widgets\FilesWidget;
?>

<?php if(!$model->isNewRecord) { ?>
    <?php if(!$onlyShow) echo FilesWidget::widget(['typeId' => $type, 'parentId' => $model->id, 'view' => false]) ?>
    <div class="attachement-list">
        <ul class="files-container">
            <?php
                if(count($model->files) == 0 && $onlyShow) {
                    echo '<li class="documents-empty"><div class="alert alert-info">nie dodano jeszcze żadnych dokumentów</div></li>';
                } else {
                    foreach($model->files as $key => $file) {
                        $actions = (!$onlyShow) ? '<span class="list-file-edit"><a class="file-update gridViewModal" data-target="#modal-grid-file" href="/files/update/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-pencil"></i>Edytuj</a></span>'
                                                    .'<span class="list-file-del"><a class="file-delete" href="/files/delete/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-trash"></i>Usuń</a></span>' : '';
                            echo '<li id="file-'.$file->id.'">'
                                    .'<div class="attachment">'
                                        .'<div class="attachment-thumb">'
                                            .'<a href="#"><i class="fa fa-paperclip"></i></a>'
                                        .'</div>'
                                        .'<div class="attachment-info">'
                                            .'<div class="attachment-file-name">'
                                                 .'<a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a>'
                                            .'</div>'
                                            .'<div class="attachment-file-creator">'
                                                 .'<p>'.$file->user.' <i class="fa fa-clock"></i>'.$file->created_at.'</p>'
                                            .'</div>'
                                            .'<div class="attachment-action-bar">'
                                               .'<span class="list-file-download"><a href="/files/getfile/'.$file->id.'"><i class="fa fa-download"></i> Pobierz</a></span>'
                                               .$actions
                                            .'</div>'
                                        .'</div>'
                                    .'</div>'
                            .'</li>';
                    }
                }
            ?>
        </ul>
    </div>
<?php } else {
        echo '<div class="alert alert-warning">Aby dodać pliki musisz utworzyć sprawę.</div>';
    } 
?>