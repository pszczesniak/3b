<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
?>
<div class="modal-body">
    <div class="grid">
        <div class="col-sm-4">
            <div class="avatar-container align-center">
                <div class="avatar-container--title"><h4><?= $model->name ?></h4></div>
                <?php
                    $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id."/preview.jpg"))?"/uploads/customers/cover/".$model->id."/preview.jpg":"/images/default-user.png";
                    echo '<img alt="avatar" src="'.$avatar.'" title="avatar"></img><br /><br />';
                ?>
            </div>
        </div>
        <div class="col-sm-8">
            <?php 
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [                      
                            'attribute' => 'phone',
                            'format'=>'raw',
                            'value' => (($model->phone) ?  $model->phone : 'brak danych'),
                        ],
                        [                      
                            'attribute' => 'email',
                            'format'=>'raw',
                            'value' => (($model->email) ?  Html::mailto($model->email, $model->email) : 'brak danych'),
                        ],
                        'address',
                       
                       /* [                      
                            'attribute' => 'departments_list:html',
                            'format'=>'raw',
                            'label' => 'Działy',
                            'value' => $model->departments_list,
                        ],     */     
                    ],
                ]) 
            ?>
        </div>
    </div>
</div>   
        
<div class="modal-footer"> 
    <a href=<?= Url::to(['/crm/customer/view', 'id' => $model->id]) ?>" title="Przejdź do karty klienta" class="btn btn-sm bg-teal"><i class="fa fa-link"></i></a>
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Zamknij</button>
</div>