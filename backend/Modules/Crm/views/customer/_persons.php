<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ajax-table-items">	 
	<div id="toolbar-person" class="btn-group">
		<?= /*(!$showOnly) ?*/ Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('app', 'New')), Url::to(['/crm/person/create', 'id' => $model->id]) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "person-form", 
					 'data-table' => "table-persons",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
					]) /*: ''*/ ?>
		<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
	</div>
	<table class="table table-striped table-items"  id="table-persons"
		    data-toolbar="#toolbar-person" 
			data-toggle="table" 
			data-show-refresh="true"
			data-show-toggle="false" 
			data-show-columns="false" 
			data-show-export="false" 
			data-filter-control="false"
			data-pagination="true"
			data-url=<?= Url::to(['/crm/customer/persons', 'id'=>$model->id]) ?>>	
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="firstname"  data-sortable="true" >Imię</th>
                <th data-field="lastname"  data-sortable="true" >Nazwisko</th>
                <th data-field="position"  data-sortable="true" >Stanowisko</th>
                <th data-field="contact"  data-sortable="true" >Kontakt</th>
                <th data-field="info"  data-sortable="true" >Oddział <br /> Opiekun</th>
                <th data-field="image"  data-sortable="false" data-width="45px"></th>
                <?php if(\Yii::$app->params['env'] == 'dev' && \Yii::$app->params['showClient']) { ?><th data-field="user"  data-sortable="false" data-align="center" data-events="actionEvents"></th> <?php } ?>
               <?= /*(!$showOnly) ? */'<th data-field="actions" data-events="actionEvents" data-width="30px"></th>' /*: ''*/ ?>
            </tr>
        </thead>
        <tbody></tbody>
	</table>
    
</div>

    

    

    



