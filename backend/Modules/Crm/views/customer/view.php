<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\CasesTable;
use frontend\widgets\accounting\OrderTable;
use frontend\widgets\sale\OffersTable;
use frontend\widgets\accounting\InvoicesTable;
use frontend\widgets\debt\TimelineTable;
use frontend\widgets\crm\BranchesTable;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Profil kontrahenta';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kontrahenci'), 'url' => Url::to(['/crm/customer/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/crm/customer/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowy kontrahent</a>
            <!--<a href="" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>-->
        </div>
    </div>
</div>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-view-add', 'title'=>'Karta kontrahenta')) ?>
    <h3><?= $model->name ?></h3>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="<?= (!isset($_GET['tab'])) ? "active" : "" ?>"><a data-toggle="tab" href="#tab1"><i class="fa fa-cog"></i><span class="panel-tabs--text">Info</span></a></li>
                <li><a data-toggle="tab" href="#tab2"><i class="fa fa-map-marker-alt"></i><span class="panel-tabs--text">Oddziały</span></a></li>
                <li><a data-toggle="tab" href="#tab3"><i class="fa fa-address-book"></i><span class="panel-tabs--text">Kontakty</span></a></li>
                <li><a data-toggle="tab" href="#tab4"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty </span></a></li>
                <!--<li><a data-toggle="tab" href="#tab5"><i class="fa fa-shopping-cart"></i><span class="panel-tabs--text">Oferty</span></a></li>-->
                <li><a data-toggle="tab" href="#tab6"><i class="fa fa-folder-open"></i><span class="panel-tabs--text">Sprawy</span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane <?= (!isset($_GET['tab'])) ? "active" : "" ?>" id="tab1">
                    <div class="grid">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="grid profile">
                                <div class="col-md-4 col-xs-12">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id."/preview.jpg"))?"/uploads/customers/cover/".$model->id."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="profile-name">
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->role ?></p>
                                        <p class="job-title mb0"><i class="fa fa-tag"></i> <?= ($model->symbol) ? $model->symbol : 'brak symbolu' ?></p>
                                        <!-- <p class="balance"> Pensja: <span>146 PLN</span> </p> -->
                                        <!--<a href="#" class="btn bg-teal btn-large mr10"> <i class="fa fa-envelope"></i> Wyślij wiadomość</a>-->
                                        <a href="<?= Url::to(['/crm/customer/update', 'id' => $model->id]) ?>" class="btn bg-blue"> <i class="fa fa-pencil-alt"></i> Edytuj</a>
                                        <a href="<?= Url::to(['/timeline/customer', 'id' => $model->id]) ?>" class="btn bg-pink"> <i class="fa fa-history"></i> Historia</a>
                                    </div>
                                </div>
                                <?php if(count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0  && \Yii::$app->params['debtClient']) { ?>
                                <div class="col-xs-12">
                                    <fieldset id="debt-block"> <legend class="<?= ($model->debt_lock == 0) ? 'text--navy' : 'text--red' ?>"><i class="fa fa-exclamation-triangle"></i>&nbsp;<?= ($model->debt_lock == 0) ? 'Ustawienia windykacji' : 'Blokada windykacyjna' ?></legend>
                                        <p>Limit kredytu kupieckiego: <b id="crm-debt-limit_credit" class="text--red"><?= number_format($model->limits['credit'],2, '.', ' ') ?></b>&nbsp;<a href="<?= Url::to(['/crm/customer/limits', 'id' => $model->id]) ?>" class="btn btn-xs bg-navy gridViewModal" data-target="#modal-grid-item" data-title="<i class='text--red fa fa-exclamation-triangle'></i> Konfiguracja limitów"> <i class="fa fa-pencil-square"></i> </a></p>
                                        <div id="crm-debt-info">
                                            <span class="btn btn-sm btn-icon bg-<?= ( ($model->debt_lock == 0) ? 'green' : 'red' ) ?>"><i class="fa fa-<?= ( ($model->debt_lock == 0) ? 'check' : 'exclamation' ) ?>"></i><?= ( ($model->debt_lock == 0) ? 'konto aktywne' : 'konto zablokowane' ) ?></span>
                                                <a href="<?= Url::to(['/crm/customer/'.(($model->debt_lock == 0) ? 'dlock' : 'dunlock'), 'id' => $model->id]) ?>" class="btn btn-sm btn-<?= ( ($model->debt_lock == 0) ? 'danger' : 'success' )?> deleteConfirm" data-label="<?= ( ($model->debt_lock == 0) ? 'Zablokuj konto' : 'Odblokuj konto' ) ?>" data-toggle="tooltip" data-title="<?= ( ($model->debt_lock == 0) ? 'zablokuj' : 'odblokuj' ) ?>"> <?= ($model->debt_lock == 0) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ?></a>
                                        </div><br />
                                    </fieldset> 
                                </div>
                                <?php } ?>
                                
                                <div class="col-xs-12">
                                    <fieldset> <legend>Opiekun klienta</legend> 
                                        <?php if(!$model->guardian) { echo '<div class="alert alert-warning">nie ustawiono</div>'; } else { ?>
                                            <div class="grid profile">
                                                <div class="col-md-4">
                                                    <div class="profile-avatar">
                                                        <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->guardian['id']."/preview.jpg"))?"/uploads/employees/cover/".$model->guardian['id']."/preview.jpg":"/images/default-user.png"; ?>
                                                        <img src="<?= $avatarContact ?>" alt="Avatar">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="profile-name">
                                                        <h3><?= $model->guardian['fullname'] ?></h3>
                                                        <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->guardian['kindname']) ? $model->guardian['typename'] : 'brak danych' ?></p>
                                                        <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->guardian['phone']) ? $model->guardian['phone'] : 'brak danych' ?></p>
                                                        <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->guardian['email']) ? Html::mailto($model->guardian['email'], $model->guardian['email']) : 'brak danych'  ?></p>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </fieldset>
                                </div>             
                                <?php if(\Yii::$app->params['env'] == 'dev' && \Yii::$app->params['showClient']) { ?>
                                <div class="col-md-12  col-xs-12">
                                    <fieldset> <legend>Konto firmowe</legend>
                                        <?php if(!$model->user) { ?>
                                            <div class="align-center crm-account-create">
                                                <a href="<?= Url::to(['/crm/customer/caccount', 'id' => $model->id]) ?>" class="btn bg-green gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-user-plus'></i>Kreator konta"> <i class="fa fa-user-plus"></i> Utwórz konto firmowe</a>
                                            </div><br />
                                        <?php } ?>
                                        <div class="meta-data">
                                            <dl class="dl-horizontal crm-accounts-info">
                                                <?php if($model->user) { ?>
                                                <dt>Status:</dt>
                                                <dd class="crm-accounts-info-status">
                                                    <span class="label label-<?= ($model->user['status'] == 10) ? 'success' : 'danger' ?>"><?= ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ?></span>
                                                    <a href="<?= Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]) ?>" class="btn btn-xs btn-<?= ($model->user['status'] == 10) ? 'danger' : 'success' ?> deleteConfirm" data-label="<?= ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ?>" title="<?= ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ?>"><?= ($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ?></a>
                                                </dd>
                                                <dt>Login:</dt> <dd><?= $model->user['username'] ?></dd>
                                                <dt>Utworzono:</dt> <dd><?= date('Y-m-d H:i:s', $model->user['created_at']) ?></dd>
                                                <!-- <dt>Utworzył:</dt> <dd>Joanna Pietras</dd> -->
                                                <dt>Ostatnie logowanie:</dt> <dd><?= $model->user['lastlogin'] ?></dd>
                                                <?php } ?>
                                                <dt>Konta osobiste:</dt>
                                                <dd class="crm-accounts-info-peronals">
                                                    <span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b><?= $model->accounts['accounts_active'] ?></b></span>
                                                    &nbsp;/&nbsp;
                                                    <span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b><?= $model->accounts['accounts_inactive'] ?></b></span></dd>
                                            </dl>
                                        </div>
                                    </fieldset>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <fieldset> <legend>Klasyfikacja</legend>
                                <div class="grid">
                                    <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Segment</dt> <dd><?= ($model->group) ? $model->group['name'] : '<i class="text--grey">brak danych</i>' ?></dd> </dl> </div>
                                    <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Status</dt> <dd><?= $model->statusname ?></dd> </dl> </div>
                                    <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Kategoria</dt> <dd><?= ($model->category) ? $model->category['name'] : '<i class="text--grey">brak danych</i>' ?></dd> </dl> </div>
                                </div>
                            </fieldset>
							<fieldset> <legend>Dodatkowe informacje</legend>
                                <div class="grid">
                                    <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Telefon</dt> <dd><?= ($model->phone) ? $model->phone : '<i class="text--grey">brak danych</i>' ?></dd> </dl> </div>
                                    <div class="col-sm-4 col-xs-12"> <dl class="mt20"> <dt class="text-muted">Email</dt> <dd><?=  ($model->email) ?  Html::mailto($model->email, $model->email) : '<i class="text--grey">brak danych</i>'  ?></dd> </dl> </div>
                                </div>
                                <div class="table-responsive about-table">
                                    <table class="table">
                                        <tbody>
                                            <tr> <th>Adres</th> <td><?= $model->fulladdress ?></td> </tr>
                                            <tr> <th>NIP</th> <td><?= $model->nip ?></td>  </tr>
                                            <!--<tr> <th>REGON</th> <td><?= $model->regon ?></td> </tr>
                                            <tr> <th>Konto</th> <td><?= $model->account_number ?></td> </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset> <legend>Opis</legend>
                                <p><?= ($model->description) ? $model->description : '<div class="alert alert-info">brak dodatkowego opisu</div>' ?></p>
                            </fieldset>
                            <fieldset> <legend>Regiony</legend>
                                <p><?= $model->departments_list ?></p>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <?= BranchesTable::widget([ 'dataUrl' => Url::to(['/crm/customer/branches', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/crm/branch/create', 'id' => $model->id]) ]) ?>
                </div>
                <div class="tab-pane" id="tab3">
                    <?=  $this->render('_persons', ['model' => $model, 'showOnly' => true]) ?>
                </div>
                <div class="tab-pane" id="tab4">
                    <?php /*$this->render('_files', ['model' => $model, 'type' => 2, 'onlyShow' => true])*/ ?>
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => true]) ?>
                </div>
                <!--<div class="tab-pane" id="tab5">
                    <?php /*OffersTable::widget([ 'dataUrl' => Url::to(['/crm/customer/offers', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/sale/offer/create', 'id' => $model->id]) ])*/ ?>
                </div> -->
                <div class="tab-pane" id="tab6">
                    <?= CasesTable::widget([ 'dataUrl' => Url::to(['/crm/customer/tcases', 'id'=>$model->id]), 'customerVisible' => false, 'insert' => true, 'insertUrl' => Url::to(['/task/project/createajax', 'id' => $model->id]) ]) ?>
                </div> 
            </div>
        </div>
    </div>  

<?php $this->endContent(); ?>
