<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-data"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'template' => '<div class="row"><div class="col-xs-4">{label}</div><div class="col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
	<div class="modal-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="avatar-container">
                    <div class="avatar-container--title"><?= $model->name ?></div>
                    <?php
                        $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id."/preview.jpg"))?"/uploads/customers/cover/".$model->id."/preview.jpg":"/images/default-user.png";
                        echo '<img alt="avatar" src="'.$avatar.'" title="avatar"></img><br /><br />';
                    ?>
                    <div class="avatar-container--contact">
                        <ul>
                            <li><i class="fa fa-envelope"></i><?= ($model->email) ? $model->email : 'brak danych'; ?></li>
                            <li><i class="fa fa-phone"></i><?= ($model->phone) ? $model->phone : 'brak danych'; ?></li>
                        </ul>
                    </div>
                    <?php if($edit) { ?>
                        <a href="/old/crm/customer/updateajax/<?= $model->id ?>?index=<?= $index ?>" class="btn btn-sm btn-primary viewEditModal" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class='glyphicon glyphicon-pencil'></i>Edycja">Edytuj</a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-8">
                <?php 
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'address',
                            'nip',
                            'regon',
                            [                      
                                'attribute' => 'id_dict_customer_status_id',
                                'format'=>'raw',
                                'value' => $model->status['name'],
                            ],
                            [                      
                                'attribute' => 'id_dict_customer_debt_collection_id',
                                'format'=>'raw',
                                'value' => '',
                            ],    
                            [                      
                                'attribute' => 'departments_list:html',
                                'format'=>'raw',
                                'label' => 'Działy',
                                'value' => $model->departments_list,
                            ], 
							[                      
                                'attribute' => 'files_list:html',
                                'format'=>'raw',
                                'label' => 'Dokumenty',
                                'value' => $this->render('_files', ['model' => $model, 'type' => 2, 'onlyShow' => true]),
                            ],      
                           
                        ],
                    ]) 
                ?>
            </div>
            <div class="col-xs-12">
                <h4>Osoby kontaktowe</h4>
                <?= $model->persons_list; ?>
            </div>
        </div>
    </div>    
            
    <div class="modal-footer"> 
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>