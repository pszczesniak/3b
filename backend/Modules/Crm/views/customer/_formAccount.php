<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'person-form', 
									'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
									/*'action' => Url::to(['/lsdd/lsddcompanyperson/create']),*/ 
									'options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'data-target' => "#modal-grid-item"],
								 ]); ?>
    <div class="modal-body calendar-task">
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        </div>   
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
        </div>                        
    </div>
    <div class="modal-footer">   
        <p class="align-right text--pink">* Imię i Nazwisko jest opcjonalnie ustawione na użytkownika 'Konto Firmowe', ale można wprowadzić tam dane konktretnej osoby</p>
        <?= Html::submitButton('Utwórz konto i wyślij informację do klienta', ['class' => 'btn btn-sm bg-purple']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>