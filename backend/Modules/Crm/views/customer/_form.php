<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord) ? 'modalAjaxForm' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
               // 'template' => '<div class="grid"><div class="col-xs-3">{label}</div><div class="col-xs-9">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
            <?= ($model->isNewRecord && count($model->getErrors()) > 0 ) ?  '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
			<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading panel-heading-tab">
                    <ul class="nav panel-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab2a"><i class="fa fa-cog"></i><span class="panel-tabs--text">Profil</span></a></li>
						<?php if(!$model->isNewRecord) { ?><li><a data-toggle="tab" href="#tab2c"><i class="fa fa-users"></i><span class="panel-tabs--text">Kontakty</span></a></li><?php } ?>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2a">
                            <div class="grid">
                                <div class="<?= (!$model->isNewRecord) ? 'col-md-6' : 'col-md-4' ?>">
                                    <fieldset><legend>Podstawowe</legend>
                                        <?= $form->field($model, 'role_fk')->dropDownList(\backend\Modules\Crm\models\Customer::listRoles(), [] ) ?>
                                        <div class="grid <?= ($model->role_fk == 1) ? 'none' : '' ?>" id="role-2">
                                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?></div>
                                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?></div>
                                        </div>
                                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control ']) ?>
                                        <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
                                    </fieldset>
                                    <fieldset><legend>Kontakt</legend>
                                        <div class="grid">
                                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                                        </div>
                                        <div class="grid">
                                            <div class="col-xs-4"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
                                            <div class="col-xs-8"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                                            <div class="col-xs-12"><?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
                                        </div>
                                    </fieldset>
                                    <?php if(Yii::$app->params['branches']) { ?>
                                    <fieldset><legend>Regiony</legend>
                                        <?= $form->field($model, 'departments_list', ['template' => '{input}{error}',  'options' => ['class' => '']])->dropdownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(\Yii::$app->user->id), 'id', 'name'), ['class' => 'ms-select', 'multiple' => 'multiple', ] )->label(); ?>
                                    </fieldset>
                                    <?php } ?>
                                </div>
                                <div class="<?= (!$model->isNewRecord) ? 'col-md-6' : 'col-md-4' ?>">
                                    <fieldset><legend>Segmentacja</legend>
                                        <?= $form->field($model, 'id_crm_group_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CrmGroup::getList(), 'id', 'name'), ['prompt' => '- wybierz -', 'class' => 'form-control'] )->label(false) ?>
                                    </fieldset>
                                    <!--<fieldset><legend>Opiekun klienta</legend>
                                        <?= $form->field($model, 'id_employee_guardian_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => '- wybierz -', 'class' => 'form-control select2'] )->label(false) ?>
                                    </fieldset>-->
									
                                    <fieldset><legend>Dodatkowe</legend>
                                        <div class="grid">
                                            <div class="col-xs-12"><?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?></div>
                                            <!--<div class="col-sm-6"><?= $form->field($model, 'regon')->textInput(['maxlength' => true]) ?></div>
                                            <div class="col-xs-12"><?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?></div>-->
                                            <div class="col-xs-12">
                                                <?= $form->field($model, 'id_dict_customer_status_id', ['template' => '
                                                          {label}
                                                           <div class="input-group ">
                                                                {input}
                                                                <span class="input-group-addon bg-green">'.
                                                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/2' , 
                                                                        ['class' => 'insertInline text--white', 
                                                                         'data-target' => "#customer-status-insert", 
                                                                         'data-input' => ".customer-status"
                                                                        ])
                                                                .'</span>
                                                           </div>
                                                           {error}{hint}
                                                       '])->dropDownList( \backend\Modules\Crm\models\Customer::listStatus(), ['prompt' => '- wybierz -', 'class' => 'form-control customer-status'] ) ?>
                                                <div id="customer-status-insert" class="insert-inline bg-purple2 none"> </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <?= $form->field($model, 'id_dict_customer_debt_collection_id', ['template' => '
                                                          {label}
                                                           <div class="input-group ">
                                                                {input}
                                                                <span class="input-group-addon bg-green">'.
                                                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/dict/dictionary/createinline']).'/3' , 
                                                                        ['class' => 'insertInline text--white', 
                                                                         'data-target' => "#customer-debt-insert", 
                                                                         'data-input' => ".customer-debt"
                                                                        ])
                                                                .'</span>
                                                           </div>
                                                           {error}{hint}
                                                       '])->dropDownList( \backend\Modules\Crm\models\Customer::listDebts(), ['prompt' => '- wybierz -', 'class' => 'form-control customer-debt'] ) ?>
                                                <div id="customer-debt-insert" class="insert-inline bg-purple2 none"> </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset><legend>Notatka</legend>
                                        <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
                                    </fieldset>
                                </div>
                                <?php if($model->isNewRecord) { 
                                    echo '<div class="col-md-4 well">'.$this->render('_firstPerson', ['model' => $person, 'form' => $form]).'</div>' ;
                                } ?>
                            </div>
                        </div>
                        <?php if(!$model->isNewRecord) { ?>
						<div class="tab-pane" id="tab2c">
							<?=  $this->render('_persons', ['model' => $model, 'showOnly' => false]) ?>
						</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="form-group align-right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">

	document.getElementById('customer-role_fk').onchange = function(event) {
        //if(event.target.value == 1) {
            document.getElementById('role-2').classList.toggle("none");
        //}
    }
    
    document.getElementById('customer-firstname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
    
    document.getElementById('customer-lastname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
</script>
<?php if( $model->isNewRecord && in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) ) { ?>
    <script type="text/javascript">
        document.getElementById('accorder-id_dict_order_type_fk').onchange = function() {
            if(this.value == 1) {
                document.getElementById('accorder-rate_constatnt').disabled = false;
                document.getElementById('accorder-limit_hours').disabled = true;
                document.getElementById('accorder-rate_hourly_1').disabled = true;
                document.getElementById('accorder-rate_hourly_2').disabled = true;
                document.getElementById('accorder-rate_hourly_way').disabled = true;
            } 
            if(this.value == 2) {
                document.getElementById('accorder-rate_constatnt').disabled = false;
                document.getElementById('accorder-limit_hours').disabled = false;
                document.getElementById('accorder-rate_hourly_1').disabled = false;
                document.getElementById('accorder-rate_hourly_2').disabled = false;
                document.getElementById('accorder-rate_hourly_way').disabled = false;
            } 
            if(this.value == 3) {
                document.getElementById('accorder-rate_constatnt').disabled = true;
                document.getElementById('accorder-limit_hours').disabled = true;
                document.getElementById('accorder-rate_hourly_1').disabled = false;
                document.getElementById('accorder-rate_hourly_2').disabled = false;
                document.getElementById('accorder-rate_hourly_way').disabled = false;
            } 
            if(this.value == 4) {
                document.getElementById('accorder-rate_constatnt').disabled = true;
                document.getElementById('accorder-limit_hours').disabled = true;
                document.getElementById('accorder-rate_hourly_1').disabled = false;
                document.getElementById('accorder-rate_hourly_2').disabled = false;
                document.getElementById('accorder-rate_hourly_way').disabled = false;
            } 
        }
        
        document.getElementById('accorder-id_currency_fk').onchange = function() {
            $curr = this.options[this.selectedIndex].text;
            $elements = document.getElementsByClassName("currencyName");
            for (i1 = 0; i1 < $elements.length; i1++) {  $elements[i1].innerHTML = $curr;  }
        }
    </script>
<?php } ?>