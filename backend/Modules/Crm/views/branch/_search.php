<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-crm-branches-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-branches']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-6"><?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList(($model->id_customer_fk) ? $model->id_customer_fk : 0), 
                                                                [ 'prompt' => '-- wybierz klienta --', 'class' => 'widget-search-list  selectAutoComplete', 'data-url' => Url::to('/crm/default/autocomplete'), 'data-table' => '#table-offers', 'data-form' => '#filter-sale-offers-search'] )
                                                            ->label('Klient') ?></div>
        <div class="col-sm-6 col-md-6 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-customers', 'data-form' => '#filter-crm-customers-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
