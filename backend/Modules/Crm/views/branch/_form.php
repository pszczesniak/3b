<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'branch-form', 
									'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
									/*'action' => Url::to(['/lsdd/lsddcompanyperson/create']),*/ 
									'options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>false, 'enableClientValidation'=>false,  'data-table' => '#table-branches', 'data-target' => "#modal-grid-item"],
								 ]); ?>
    <div class="modal-body calendar-task">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <fieldset><legend>Opiekun oddziału</legend>
            <?= $form->field($model, 'id_employee_guardian_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => '- wybierz -', 'class' => 'form-control select2Modal'] )->label(false) ?>
        </fieldset>   
        <fieldset><legend>Kontakt</legend>
            <div class="grid">
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="grid">
                <div class="col-xs-4"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-8"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-12"><?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
            </div>
        </fieldset>
    </div>
    <div class="modal-footer">   
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>