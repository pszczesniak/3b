<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */


?>
<div class="modal-body">
    <?php 
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [                      
                    'attribute' => 'name',
                    'format'=>'raw',
                    'label' => 'Nazwa',
                    'value' => $model->name
                ], 
                [                      
                    'attribute' => 'debt_limit',
                    'format'=>'raw',
                    'value' => number_format($model->debt_limit, 2, '.', ' '),
                ],
                'describe', 
 
            ],
        ]) 
    ?>
</div>
    
        
<div class="modal-footer"> 
    <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" title="Zamknij okno">Odrzuć</button>
</div>