<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-groups"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="grid">
        <div class="col-sm-8 col-xs-8"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
        <div class="col-sm-4 col-xs-4"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>        
        <!--<div class="col-sm-4 col-xs-6"><?= $form->field($model, 'debt_day_limit')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'debt_limit')->textInput(['maxlength' => true]) ?></div>-->
    </div>  
    <fieldset><legend>Opis</legend>
        <?= $form->field($model, 'describe')->textarea(['rows' => 3])->label(false) ?>
    </fieldset>
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
