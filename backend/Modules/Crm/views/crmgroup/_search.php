<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-crm-groups-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-groups']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-groups', 'data-form' => '#filter-crm-groups-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
        <div class="col-sm-2 col-md-2 col-xs-12"> <?= $form->field($model, 'symbol')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-groups', 'data-form' => '#filter-crm-groups-search' ])->label('Symbol <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>        
    </div> 

    <?php ActiveForm::end(); ?>

</div>
