<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\files\FilesBlock;
use common\widgets\Alert;


/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = Yii::t('app', 'Update') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Strony przeciwne'), 'url' => Url::to(['/crm/side/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edycja' ;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/crm/side/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa strona</a>
            <a href="<?= Url::to(['/crm/side/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
            <!--<a href="" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>-->
        </div>
    </div>
</div>
<?= Alert::widget() ?>
    <div class="grid">
        <div class="col-sm-6 col-md-8 col-xs-12">
            <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-update', 'title'=>Html::encode($this->title))) ?>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            <?php $this->endContent(); ?>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-teal"> 
                    <span class="panel-title"> <span class="fa fa-image"></span> Zdjęcie </span> 
                    <div class="panel-heading-menu pull-right">
                        <a aria-controls="crm-customer-update-image" aria-expanded="false" href="#crm-customer-update-image" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body" id="crm-customer-update-image">
                    <div class="align-center">
                        <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                    [
                                        'id' => 'imgAttachment',
                                        'model' => $model,
                                        'behaviorName' => 'coverBehavior',
                                        'apiRoute' => '/crm/customer/imgAttachApi',
                                    ]
                                
                                )
                        ?>
                    </div>
                </div>
            </div>
			
			<!-- -->
			
            <div class="panel panel-default">
                <div class="panel-heading bg-purple"> 
					<span class="panel-title"> <span class="fa fa-paperclip"></span> Dokumenty </span> 
					<div class="panel-heading-menu pull-right">
                        <a aria-controls="crm-customer-update-docs" aria-expanded="true" href="#crm-customer-update-docs" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
				</div>
                <div class="panel-body" id="crm-customer-update-docs">
                    <?php /* $this->render('_files', ['model' => $model, 'type' => 2, 'onlyShow' => false])*/ ?>
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => false]) ?>
                </div>
            </div>
			
			<!-- -->
			
			<div class="panel panel-default">
                <div class="panel-heading bg-orange"> 
					<span class="panel-title"> <span class="fa fa-users"></span> Osoby kontaktowe </span> 
					<div class="panel-heading-menu pull-right">
                        <a aria-controls="crm-customer-update-person" aria-expanded="true" href="#crm-customer-update-person" data-toggle="collapse" class="collapse-link collapse-window">
                            <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
                        </a>
                    </div>
				</div>
                <div class="panel-body" id="crm-customer-update-person">
                    <?=  $this->render('_persons', ['model' => $model, 'showOnly' => false]) ?>
                </div>
            </div>

        </div>
    </div>

