<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */

$this->title = Yii::t('app', 'Nowa strona przeciwna');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Strony przeciwne'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-update', 'title'=>Html::encode($this->title))) ?>
    <?= $this->render('_form', [
        'model' => $model, 'person' => $person
    ]) ?>

<?php $this->endContent(); ?>
    