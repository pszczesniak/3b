<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'saveInlineFormSide',
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'saveInlineForm', 'data-target' => "#modal-grid-item", 'data-input' => '.matter-side'],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
	<div class="grid">
		<div class="col-sm-3 col-xs-12"><?= $form->field($model, 'role_fk')->dropDownList(\backend\Modules\Crm\models\Customer::listRoles(), [] ) ?></div>
		<div class="col-sm-9 col-xs-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?></div>
	</div>
    <div class="grid <?= ($model->role_fk == 1) ? 'none' : '' ?>" id="role-2">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?></div>
    </div>
    <div class="grid">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="grid">
        <div class="col-sm-2 col-xs-5"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-5 col-xs-7"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-5 col-xs-12"><?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
    </div>   
	<div class="text--red" id="errorValueHint"></div>
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<button class="btn btn-sm btn-default closeInlineForm" type="button" >Odrzuć</button>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    
	document.getElementById('customer-role_fk').onchange = function(event) {
        //if(event.target.value == 1) {
            document.getElementById('role-2').classList.toggle("none");
        //}
    }
	
    document.getElementById('customer-firstname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
    
    document.getElementById('customer-lastname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
	
	document.getElementById("saveInlineFormSide").onsubmit = function() {
		var data = new FormData(this);
		var http = new XMLHttpRequest();
		http.open('POST', '<?= Url::to(['/crm/side/createinline']) ?>',true);
 
		http.onreadystatechange=function()  {
			if (http.readyState==4 && http.status == 200) {
				var result = JSON.parse(http.responseText);
				if(result.success) { 
					document.getElementById('side-insert').classList.add('none');
					sideSelect = document.getElementById('caseside-id_side_fk');
					sideSelect.innerHTML += "<option value='"+result.id+"'>"+result.name+"</option>";
					sideSelect.value = result.id;
					//alert("Form Submitted");
                } else {
					document.getElementById("errorValueHint").innerHTML = result.alert;
				}
			} else {
				document.getElementById("errorValueHint").innerHTML = 'Wystąpił błąd. '+http.status;
			}
		};
 
		http.send(data); 
		return false;
	}    

</script>


