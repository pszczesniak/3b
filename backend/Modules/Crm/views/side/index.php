<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Strony i Uczestnicy');
$this->params['breadcrumbs'][] = 'Sprawy';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-side-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Alert::widget() ?>
 
	<div id="toolbar-sides" class="btn-group toolbar-table-widget">
		 <?= ( ( count(array_intersect(["sideAdd", 'grantAll'], $grants)) > 0 ) ) ? Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/crm/side/create'] ) , 
					['class' => 'btn btn-success btn-icon', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) : '' ?>
        <?= ( ( count(array_intersect(["sideExport", "grantAll"], $grants)) > 0 )) ? Html::a('<i class="fa fa-print"></i>Export', Url::to(['/crm/side/export'] ) , 
                    ['class' => 'btn btn-info btn-icon btn-export', 
                     'id' => 'side-export',
                     //'data-toggle' => ($gridViewModal)?"modal":"none", 
                     'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-crm-sides-search", 
                     'data-table' => "table-items",
                     'data-title' => "Dodaj"
                    ]) : '' ?>

		<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-sides"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-sides"
                data-toolbar="#toolbar-sides" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-page-number=<?= $setting['page'] ?>
                data-page-size="<?= $setting['limit'] ?>"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
				data-search-form="#filter-crm-sides-search"
                data-url=<?= Url::to(['/crm/side/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="symbol" data-sort-name="sname" data-sortable="true">Symbol</th>
                    <th data-field="name" data-sort-name="sname" data-sortable="true">Nazwa</th>
                    <th data-field="contact" data-sort-name="sname" data-sortable="true">Kontakt</th>
                    <th data-field="character"  data-sortable="false">W charakterze</th>
                    <th data-field="actions" data-events="actionEvents" data-width="70px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

	<!-- Render modal form -->
	

<?php $this->endContent(); ?>
