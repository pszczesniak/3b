<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form form">

    
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
               // 'template' => '<div class="grid"><div class="col-xs-3">{label}</div><div class="col-xs-9">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <?= ($model->isNewRecord && count($model->getErrors()) > 0 ) ?  '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <div class="grid">
        <div class="<?= (!$model->isNewRecord) ? 'col-md-12 col-sm-6 col-xs-12' : 'col-md-8 col-sm-6 col-xs-12' ?>">
            <div class="grid">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <fieldset><legend>Podstawowe</legend>
                        <?= $form->field($model, 'role_fk')->dropDownList(\backend\Modules\Crm\models\Customer::listRoles(), [] ) ?>
                        <div class="grid <?= ($model->role_fk == 1) ? 'none' : '' ?>" id="role-2">
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?></div>
                        </div>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control uppercase']) ?>
                        <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
                    </fieldset>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <fieldset><legend>Kontakt</legend>
                        <div class="grid">
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                        </div>
                        <div class="grid">
                            <div class="col-xs-4"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-xs-8"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-xs-12"><?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?></div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12">
                    <fieldset><legend>Notatka</legend>
                        <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
                    </fieldset>
                </div>
            </div>         
        </div>
            
        <?php if($model->isNewRecord) { 
            echo '<div class="col-md-4 col-sm-6 col-xs-12 well">'.$this->render('_firstPerson', ['model' => $person, 'form' => $form]).'</div>' ;
        } ?>
    </div>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">

	document.getElementById('customer-role_fk').onchange = function(event) {
        //if(event.target.value == 1) {
            document.getElementById('role-2').classList.toggle("none");
        //}
    }
    
    document.getElementById('customer-firstname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
    
    document.getElementById('customer-lastname').onfocusout = function() {
	   document.getElementById('customer-name').value = document.getElementById('customer-lastname').value + ' ' + document.getElementById('customer-firstname').value;   
       return false;
    }
</script>
