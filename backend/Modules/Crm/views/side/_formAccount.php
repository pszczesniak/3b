<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Lsdd\models\LsddCompanyPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'person-form', 
									'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
									/*'action' => Url::to(['/lsdd/lsddcompanyperson/create']),*/ 
									'options' => ['class' => 'modalAjaxForm', 'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'data-target' => "#modal-grid-item"],
								 ]); ?>
    <div class="modal-body calendar-task">
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        </div>   
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
        </div>                        
    </div>
    <div class="modal-footer">   
        <p class="align-left text--pink">* Imię i Nazwisko jest opcjonalnie ustawione na użytkownika 'Konto Firmowe', ale oczywiście z każdym momencie można wprowadzić tam dane konktretnego pracownika firmy</p>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
<?php ActiveForm::end(); ?>