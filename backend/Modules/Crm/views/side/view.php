<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\tasks\CasesTable;
use frontend\widgets\accounting\OrderTable;
use frontend\widgets\accounting\ActionsTable;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model app\Modules\Company\models\CompanyEmployee */

$this->title = 'Profil strony';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Strony przeciwne'), 'url' => Url::to(['/crm/side/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?= Url::to(['/crm/side/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa strona</a>
            <!--<a href="" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>-->
        </div>
    </div>
</div>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-customer-view-add', 'title'=>'')) ?>
    <h3><?= $model->name ?></h3>
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading panel-heading-tab">
            <ul class="nav panel-tabs">
                <li class="<?= (!isset($_GET['tab'])) ? "active" : "" ?>"><a data-toggle="tab" href="#tab1"><i class="fa fa-cog"></i><span class="panel-tabs--text">Info</span></a></li>
                <li><a data-toggle="tab" href="#tab2"><i class="fa fa-address-book"></i><span class="panel-tabs--text">Kontakty</span></a></li>
                <li><a data-toggle="tab" href="#tab3"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty </span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane <?= (!isset($_GET['tab'])) ? "active" : "" ?>" id="tab1">
                    <div class="grid">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="grid profile">
                                <div class="col-md-4">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id."/preview.jpg"))?"/uploads/customers/cover/".$model->id."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="profile-name">
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->role ?></p>
                                        <p class="job-title mb0"><i class="fa fa-tag"></i> <?= ($model->symbol) ? $model->symbol : 'brak symbolu' ?></p>
                                        <!-- <p class="balance"> Pensja: <span>146 PLN</span> </p> -->
                                        <!--<a href="#" class="btn bg-teal btn-large mr10"> <i class="fa fa-envelope"></i> Wyślij wiadomość</a>-->
                                        <a href="<?= Url::to(['/crm/side/update', 'id' => $model->id]) ?>" class="btn bg-blue"> <i class="fa fa-pencil"></i> Edytuj</a>
                                        <a href="<?= Url::to(['/timeline/customer', 'id' => $model->id]) ?>" class="btn bg-pink"> <i class="fa fa-history"></i> Historia</a>
                                    </div>
                                </div>              
                                <div class="col-md-12">
                                    <div class="profile-info bt">
                                        <h5 class="text-muted">Działy</h5>
                                        <p><?= $model->departments_list ?></p>
                                    </div>
                                </div>
                                <?php if(\Yii::$app->params['env'] == 'dev' && $model->type_fk == 1) { ?>
                                <div class="col-md-12">
                                    <div class="profile-info bt">
                                        <h5 class="text-muted">Konto firmowe</h5>
                                        <?php if(!$model->user) { ?>
                                            <div class="align-center crm-account-create">
                                                <a href="<?= Url::to(['/crm/customer/caccount', 'id' => $model->id]) ?>" class="btn bg-green gridViewModal" data-target="#modal-grid-item" data-title="<i class='fa fa-user-plus'></i>Kreator konta"> <i class="fa fa-user-plus"></i> Utwórz konto firmowe</a>
                                            </div><br />
                                        <?php } ?>
                                        <div class="meta-data">
                                            <dl class="dl-horizontal crm-accounts-info">
                                                <?php if($model->user) { ?>
                                                <dt>Status:</dt>
                                                <dd class="crm-accounts-info-status">
                                                    <span class="label label-<?= ($model->user['status'] == 10) ? 'success' : 'danger' ?>"><?= ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ?></span>
                                                    <a href="<?= Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]) ?>" class="btn btn-xs btn-<?= ($model->user['status'] == 10) ? 'danger' : 'success' ?> deleteConfirm" data-label="<?= ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ?>" title="<?= ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ?>"><?= ($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ?></a>
                                                </dd>
                                                <dt>Login:</dt>
                                                <dd><?= $model->user['username'] ?></dd>
                                                <dt>Utworzono:</dt>
                                                <dd><?= date('Y-m-d H:i:s', $model->user['created_at']) ?></dd>
                                                <dt>Utworzył:</dt>
                                                <dd>Joanna Pietras</dd>
                                                <dt>Ostatnie logowanie:</dt>
                                                <dd><?= $model->user['lastlogin'] ?></dd>
                                                <?php } ?>
                                                <dt>Konta osobiste:</dt>
                                                <dd class="crm-accounts-info-peronals">
                                                    <span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b><?= $model->accounts['accounts_active'] ?></b></span>
                                                    &nbsp;/&nbsp;
                                                    <span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b><?= $model->accounts['accounts_inactive'] ?></b></span></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="profile-about">
                                <h4>Dodatkowe informacje</h4>
                                <div class="contact-info bt">
                                    <div class="grid">
                                        <div class="col-md-4"> <dl class="mt20"> <dt class="text-muted">Telefon</dt> <dd><?= ($model->phone) ? $model->phone : 'brak danych' ?></dd> </dl> </div>
                                        <div class="col-md-8"> <dl class="mt20"> <dt class="text-muted">Email</dt> <dd><?=  ($model->email) ?  Html::mailto($model->email, $model->email) : 'brak danych'  ?></dd> </dl> </div>
                                    </div>
                                </div>
                                <div class="table-responsive about-table">
                                    <table class="table">
                                        <tbody>
                                            <tr> <th>Adres</th> <td><?= $model->address ?></td> </tr>
                                            <tr> <th>NIP</th> <td><?= $model->nip ?></td>  </tr>
                                            <!--<tr> <th>REGON</th> <td><?= $model->regon ?></td> </tr>
                                            <tr> <th>Konto</th> <td><?= $model->account_number ?></td> </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <?=  $this->render('_persons', ['model' => $model, 'showOnly' => true]) ?>
                </div>
                <div class="tab-pane" id="tab3">
                    <?php /*$this->render('_files', ['model' => $model, 'type' => 2, 'onlyShow' => true])*/ ?>
                    <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => true]) ?>
                </div>
            </div>
        </div>
    </div>  

<?php $this->endContent(); ?>
