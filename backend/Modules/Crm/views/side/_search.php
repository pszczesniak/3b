<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-crm-sides-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-sides']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-6 col-md-8 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-sides', 'data-form' => '#filter-crm-sides-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>
        <div class="col-sm-3 col-md-2 col-xs-6"> <?= $form->field($model, 'symbol')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-sides', 'data-form' => '#filter-crm-sides-search' ])->label('Symbol <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie si� po wpisniau przynajmniej 3 znak�w"></i>') ?></div>        
        <div class="col-sm-3 col-md-2 col-xs-6"><?= $form->field($model, 'side_character', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Crm\models\Customer::inCharacter(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-sides', 'data-form' => '#filter-crm-sides-search' ] ) ?></div>
    </div> 


    <?php ActiveForm::end(); ?>

</div>
