<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Crm\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `backend\Modules\Crm\models\Customer`.
 */
class CustomerSearch extends Customer
{
    public $id_department_fk;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_fk', 'id_dict_customer_status_id', 'id_dict_customer_debt_collection_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'address', 'phone', 'email', 'nip', 'regon', 'description', 'account_number', 'custom_data', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user_fk' => $this->id_user_fk,
            'id_dict_customer_status_id' => $this->id_dict_customer_status_id,
            'id_dict_customer_debt_collection_id' => $this->id_dict_customer_debt_collection_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'regon', $this->regon])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data]);

        return $dataProvider;
    }
}
