<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%customer_person}}".
 *
 * @property integer $id
 * @property integer $id_customer_fk
 * @property string $lastname
 * @property string $firstname
 * @property string $phone
 * @property string $email
 * @property string $position
 * @property string $description
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CustomerPerson extends \yii\db\ActiveRecord
{
    public $user_action;
    public $login;
    public $account;
    
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'id_customer_fk', 'id_customer_branch_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'account'], 'integer'],
            [['firstname', 'lastname'], 'required'],
            [['description', 'custom_data', 'user_role', 'login'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'login'], 'safe'],
            [['lastname', 'firstname'], 'string', 'max' => 300],
            [['phone'], 'string', 'max' => 20],
            [['email', 'position'], 'string', 'max' => 100],
            ['email', 'email'],
            [['login', 'email'], 'required', 'when' => function($model) { return $model->account == 1; } ],
            [['login'], 'validateLogin', 'when' => function($model) { return ($model->account); }],
            [['email'], 'validateEmail', 'when' => function($model) { return ($model->account); }],
        ];
    }
    
    public function validateLogin(){

        $exist = \common\models\crm\CUser::find()->where("lower(username) = '". strtolower($this->login) ."'")->count();
       
		if( $exist > 0 ){
			$this->addError('login','Użytkownik o wskazanym loginie już występuje w systemie');
		} 
	}
    
    public function validateEmail(){

        $exist = \common\models\crm\CUser::find()->where("email = '". $this->email ."'")->count();
       
		if( $exist > 0 ){
			$this->addError('login','Podany e-mail został już wykorzystany');
		} 
	}


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'id_user_fk' => 'Użytkownik',
            'user_role' => 'Uprawnienia systemowe',
            'lastname' => Yii::t('app', 'Nazwisko'),
            'firstname' => Yii::t('app', 'Firstname'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'position' => Yii::t('app', 'Stanowisko'),
            'description' => Yii::t('app', 'Description'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
				
				if($this->account) {
					$user = new \common\models\crm\CUser();
					$user->generateAuthKey();
					$user->status = 10;
					$user->type_fk = 2;
					$user->username = $this->login;
					$user->firstname = $this->firstname;
					$user->lastname = $this->lastname;
					$pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
					$user->setPassword($pass);
					$user->email = $this->email;
					if($user->save()) { 
						$this->id_user_fk = $user->id;
						$this->created_by = \Yii::$app->user->id;
						//$this->custom_data = \yii\helpers\Json::encode( ['copyUser' => $this->copyUser] );
						
						\Yii::$app->mailer->compose(['html' => 'customerAuthorization-html', 'text' => 'customerAuthorization-text'], ['login' => $user->username, 'password' => $pass])
						->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
						//->setTo($this->email)
						->setTo('kamila_bajdowska@onet.eu')
						->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
						->send();
					} else {
						return false;
					}
				}
			} else { 
				if($this->account && !$this->id_user_fk) {
					$user = new \common\models\crm\CUser();
					$user->generateAuthKey();
					$user->status = 10;
					$user->type_fk = 2;
					$user->username = $this->login;
					$user->firstname = $this->firstname;
					$user->lastname = $this->lastname;
					$pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
					$user->setPassword($pass);
					$user->email = $this->email;
					if($user->save()) { 
						$this->id_user_fk = $user->id;
						$this->created_by = \Yii::$app->user->id;
						//$this->custom_data = \yii\helpers\Json::encode( ['copyUser' => $this->copyUser] );
						
						\Yii::$app->mailer->compose(['html' => 'customerAuthorization-html', 'text' => 'customerAuthorization-text'], ['login' => $user->username, 'password' => $pass])
						->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
						//->setTo($this->email)
						->setTo('kamila_bajdowska@onet.eu')
						->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
						->send();
					} else {
						return false;
					}
				} else if($this->account && $this->id_user_fk) {
                    $user = \common\models\crm\CUser::findOne($this->id_user_fk);
                    $user->username = $this->login;
					$user->email = $this->email;
                } 
                /*$modelArch = new CustomerArch();
                $modelArch->table_fk = 1;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
            'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'CustomerPerson',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
                'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/customers/contacts/cover',
                'url' => Yii::$app->params['frontend'] . Yii::$app->params['basicurl'] . '/uploads//customers/contacts/cover',
				//'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['basicurl'] . '/uploads//customers/contacts/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
      
		//var_dump($this); exit;
		return $result;
	}
    
    public function getFullname() {
        return $this->lastname . ' ' . $this->firstname;
    }
    
    public function getCustomer() {
        return \backend\Modules\Crm\models\Customer::findOne($this->id_customer_fk);
    }
    
    public function getBranch() {
        return \backend\Modules\Crm\models\CustomerBranch::findOne($this->id_customer_branch_fk);
    }
    
    public function getGuardian() {
        return \backend\Modules\Company\models\CompanyEmployee::find()->where( ['id' => $this->id_employee_guardian_fk])->one();
    }
    
    public function getUser() {
        return ($this->id_user_fk) ? \common\models\crm\CUser::findOne($this->id_user_fk) : false;
    }
	
	public static function listRoles() {
		$roles = ['admin' => 'pełne uprawnienia', 'user' => 'wgląd do spraw'];
		
		return $roles;
	}
    
    public static function getList($id) {
        return CustomerPerson::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
    }
}
