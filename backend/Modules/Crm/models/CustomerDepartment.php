<?php

namespace backend\Modules\Crm\models;

use Yii;

/**
 * This is the model class for table "{{%customer_department}}".
 *
 * @property integer $id
 * @property integer $id_customer_fk
 * @property integer $id_department_fk
 * @property string $created_at
 * @property integer $created_by
 */
class CustomerDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_customer_fk', 'id_department_fk'], 'required'],
            [['id_customer_fk', 'id_department_fk', 'created_by', 'deleted_by', 'status', 'id_arch_fk'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'id_department_fk' => Yii::t('app', 'Id Department Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function getCustomer()
    {
		return $this->hasOne(\backend\Modules\Crm\models\Customer::className(), ['id' => 'id_customer_fk']);
    }
    
    public function getDepartment()
    {
		return $this->hasOne(\backend\Modules\Company\models\CompanyDepartment::className(), ['id' => 'id_department_fk']);
    }
}
