<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Crm\models\CustomerArch;
use backend\Modules\Crm\models\CustomerPerson;
use backend\Modules\Crm\models\CustomerDepartment;
use backend\Modules\Debt\models\DebtReport;

/**
 * This is the model class for table "{{%customer_branch}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $nip
 * @property string $regon
 * @property string $description
 * @property string $account_number
 * @property string $custom_data
 * @property integer $id_dict_customer_status_id
 * @property integer $id_dict_customer_debt_collection_id
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CustomerBranch extends \yii\db\ActiveRecord
{
    
    public $user_action;
    public $id_department_fk;
    public $departments_list = [];
    public $departments_rel = [];
    public $files_list;
    public $persons_list;
    public $is_ignore = 0;
	public $createOrder;
	public $bindOrder;
	public $id_order_fk;
    
    public $login;
    public $account;
    public $firstnamePerson;
    public $lastnamePerson;
    
    public $side_character;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_branch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [ 
            [['id_customer_fk', 'id_user_fk', 'id_dict_crm_branch_type_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'is_ignore', 'type_fk', 'id_employee_guardian_fk'], 'integer'],
            //[['debt_limit'], 'number'],
            [['name'], 'required'/*, 'when' => function($model) { return $model->role_fk == 1; }*/],
            [['description', 'custom_data','symbol'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'departments_list', 'login', 'account', 'customer_linked'], 'safe'],
            [['symbol'], 'string', 'max' => 100],
            [['name', 'address'], 'string', 'max' => 300],
            [['phone', 'nip', 'regon'], 'string', 'max' => 20],
            [['email', 'city', 'postal_code'], 'string', 'max' => 100],
            [['account_number'], 'string', 'max' => 500],
            ['email', 'email'],
            //['nip', 'unique', 'filter' => ['status' => 1, 'type_fk' => 1], 'message' => 'Klient o podanym numerze NIP już występuje w systemie.','when' => function($model) { return ($model->is_ignore == 0 && $model->type_fk == 1); }],
            //['name', 'unique', 'filter' => ['status' => 1, 'type_fk' => 1], 'message' => 'Klient o podanej nazwie już występuje w systemie.','when' => function($model) { return ($model->is_ignore == 0 && $model->type_fk == 1); }],
            ['name', 'compareName', 'when' => function($model) { return $model->is_ignore == 0; } ],
            [['login'], 'validateLogin', 'when' => function($model) { return ($model->account); }],
            [['email'], 'validateEmail', 'when' => function($model) { return ($model->account); }],
            //[['id_crm_group_fk'], 'validateManagment',],
        ];
    }
    
    public function compareName($attribute, $params)  {
        if($this->isNewRecord)
			$names = \backend\Modules\Crm\models\CustomerBranch::find()->where(['status' => 1, 'type_fk' => $this->type_fk, 'id_customer_fk' => $this->id_customer_fk])->andWhere("lower(replace(name, ' ', '')) = '".str_replace(' ', '', htmlspecialchars($this->name, ENT_QUOTES, 'UTF-8'))."'")->all();
        else
			$names = \backend\Modules\Crm\models\CustomerBranch::find()->where(['status' => 1, 'type_fk' => $this->type_fk, 'id_customer_fk' => $this->id_customer_fk])->andWhere("id != ".$this->id." and lower(replace(name, ' ', '')) = '".str_replace(' ', '', htmlspecialchars($this->name, ENT_QUOTES, 'UTF-8'))."'")->all();

		$ok = true;
        
        if(count($names) > 0) {
            $this->addError($attribute, 'Dla tego klienta oddział o podanej nazwie już istnieje');
        }
        /*if(count($names) > 0) {
            $this->addError($attribute, 'W systemie istnieje klient o podobnej nazwie. Jeśli <b>'.$names[0]->name.'</b>'
                                        .' to zupełnie inny klient i chcesz wprowadzić nowego klienta o podanej nazwie zaznacz tutaj <input type="checkbox" value="1" name="Customer[is_ignore]" id="customer-is_ignore"> i ponownie zapisz zmiany.');
        }*/
    }
    
    public function validateLogin() {

        $exist = \common\models\crm\CUser::find()->where("lower(username) = '". strtolower($this->login) ."'")->count();
       
		if( $exist > 0 ){
			$this->addError('login','Użytkownik o wskazanym loginie już występuje w systemie');
		} 
	}
    
    public function validateEmail() {

        $exist = \common\models\crm\CUser::find()->where("email = '". $this->email ."'")->count();
       
		if( $exist > 0 ){
			$this->addError('login','Podany e-mail został już wykorzystany');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()  {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => 'Klient',
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nip' => Yii::t('app', 'Nip'),
            'regon' => Yii::t('app', 'Regon'),
            'description' => Yii::t('app', 'Description'),
            'account_number' => Yii::t('app', 'Account Number'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'id_dict_customer_status_id' => Yii::t('app', 'Status'),
            'id_dict_customer_debt_collection_id' => Yii::t('app', 'Kategoria'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'id_department_fk' => 'Dział',
            'departments_list' => 'Działy',
            'type_fk' => 'Rodzaj',
            'symbol' => 'Symbol',
            'firstname' => 'Imię',
            'lastname' => 'Nazwisko',
            'postal_code' => 'Kod',
            'city' => 'Miasto',
            'id_crm_group_fk' => 'Segment',
            'debt_limit' => 'Limit kredytu kupieckiego',
            'side_character' => 'W charakterze',
            'acc_order_required' => 'Wymagane wybranie zlecenia przy rejestracji czynności',
			'createOrder' => 'Utwórz umowę',
			'bindOrder' => 'Powiąż z istniejącym zleceniem',
            'id_employee_guardian_fk' => 'Opiekun'
        ];
    }
    
    public function beforeSave($insert) {
        /*$this->firstname = strtoupper($this->firstname);
        $this->lastname = strtoupper($this->lastname);*/
        $this->name = strtoupper($this->name);
		
		if(empty($this->symbol)) $this->symbol = null;
        
        //if(!$this->name && $this->role_fk == 2) $this->name = $this->lastname . ' ' . $this->firstname;
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				/*$modelArch = new CustomerArch();
                $modelArch->table_fk = 1;
				$modelArch->id_root_fk = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->data_arch = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();*/
                if($this->account) {
					$user = new \common\models\crm\CUser();
					$user->generateAuthKey();
					$user->status = 10;
					$user->type_fk = 1;
					$user->username = $this->login;
					$user->firstname = $this->firstname;
					$user->lastname = $this->lastname;
					$pass = Yii::$app->getSecurity()->generateRandomString(6);
					$user->setPassword($pass);
					$user->email = $this->email;
					if($user->save()) { 
						$this->id_user_fk = $user->id;
						$this->created_by = \Yii::$app->user->id;
						//$this->custom_data = \yii\helpers\Json::encode( ['copyUser' => $this->copyUser] );
						try {
                            \Yii::$app->mailer->compose(['html' => 'customerAuthorization-html', 'text' => 'customerAuthorization-text'], ['login' => $user->username, 'password' => $pass])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            //->setTo($this->email)
                            ->setTo('kamila_bajdowska@onet.eu')
                            ->setSubject('Nowy użytkownik w systmie ' . \Yii::$app->name)
                            ->send();
                        } catch (\Swift_TransportException $e) {
							//echo 'Caught exception: ',  $e->getMessage(), "\n";
							$request = Yii::$app->request;
							$log = new \common\models\Logs();
							$log->id_user_fk = Yii::$app->user->id;
							$log->action_date = date('Y-m-d H:i:s');
							$log->action_name = 'customrAccount';
							$log->action_describe = $e->getMessage();
							$log->request_remote_addr = $request->getUserIP();
							$log->request_user_agent = $request->getUserAgent(); 
							$log->request_content_type = $request->getContentType(); 
							$log->request_url = $request->getUrl();
							$log->save();
						}
					} else {
						return false;
					}
				}
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
     public function afterSave($insert, $changedAttributes) {
		parent::afterSave($insert, $changedAttributes);
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'Customer',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/customers/cover',
				'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['basicurl'] . '/uploads/customers/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
    
    public function delete() {
		$this->status = -1;
        $this->is_ignore = 1;
        $this->user_action = 'delete';
		$this->deleted_at = new Expression('NOW()');
        $this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public function getDepartments() {
		$items = $this->hasMany(\backend\Modules\Crm\models\CustomerDepartment::className(), ['id_customer_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
	
	public function getBranches() {
		$items = $this->hasMany(\backend\Modules\Crm\models\CustomerDepartment::className(), ['id_customer_fk' => 'id'])->where(['status' => 1]); 
		return $items;
    }
    
    public function getFiles() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 2])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getFilesacc() {
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 6])->orderby('id desc')->all();
        return $filesData;
    }
    
    public static function getList($id) {
        return CustomerBranch::find()->where(['status' => 1, 'id_customer_fk' => $id])->all();
    }
    
    public static function getListForManager($id) {
        if(!Yii::$app->user->isGuest) {
            return Customer::find()->select(["id", "case when (instr(upper(name),upper(symbol)) = 0 ) then concat(name,' [',symbol,']') else name end as name"])
                                   ->where(['status' => 1, 'type_fk' => 1])
                                   ->andWhere("id in (select id_customer_fk from {{%customer_department}} cd join {{%company_department}} d on d.id=id_department_fk where id_employee_manager_fk = ".$id.")")
                                   ->orderby('name collate `utf8_polish_ci`')->all();
        } else {
            return [];
        }
    }
    
    public static function getShortList($id) {
        $data = [];
        if($id == 0) {
            $sql = "select id, name from {{%customer}} order by name limit 20 ";
        } else {
           // $customer = self::findOne($id);
            $sql = "select id, name from {{%customer}} where id = ".$id;
        }
        
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        foreach($result as $key => $item) {
            $data[$item['id']] = $item['name'];
        }
        
        return $data;
    }
    
    public static function getOppositeSides() {
        if(!Yii::$app->user->isGuest) {
            return Customer::find()->where(['status' => 1, 'type_fk' => 2])->orderby('name collate `utf8_polish_ci`')->all();
        } else {
            return [];
        }
    }
    
    public static function getPersonsList($id) {
        return \backend\Modules\Crm\models\CustomerPerson::find()->where(['status' => 1, 'id_customer_fk' => $id])->orderby('lastname collate `utf8_polish_ci`')->all();
    }
    
    public static function listStatus() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 2])->orderby('name')->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public static function listDebts() {
        
        $items = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 3])->all();
        $types = [];
        foreach($items as $key => $item) { 
            $types[$item->id] = $item->name;
        }
        return $types;
    }
    
    public function getStatusname()
    {
		$item = \backend\Modules\Dict\models\DictionaryValue::findOne($this->id_dict_customer_status_id);
        return ($item) ? $item->name : 'brak informacji';
    }
    
    public function getPersons()
    {
		//return $this->hasOne(\backend\Modules\Crm\models\CustomerPerson::className(), ['id' => 'id_customer_fk']);
        $persons = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $this->id, 'status' => '1'])->all();
        return $persons;
    }
	
	public function getCreator() {
		return  \common\models\User::findOne($this->created_by);
	}
    
    public function getOrders() {
        $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $this->id, 'status' => '1'])->all();
        return $orders;
    }
    
    public function getSuborders() {
        $orders = \backend\Modules\Accounting\models\AccOrderItem::find()->where(['id_customer_fk' => $this->id, 'status' => '1'])->all();
        return $orders;
    }
    
    public function getUser() {
        return ($this->id_user_fk) ? \common\models\crm\CUser::findOne($this->id_user_fk) : false;
    }
    
    public function getAccounts() {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('
            SELECT sum(case when status=10 then 1 else 0 end) as accounts_active, sum(case when status=10 then 0 else 1 end) as accounts_inactive  FROM {{%c_user}}
            WHERE id in (select id_user_fk from {{%customer_person}} where status = 1 and id_customer_fk = :customerId) ',
            [':customerId' => $this->id]);

        $accounts = $command->queryOne();
        if(!$accounts['accounts_active']) $accounts['accounts_active'] = 0;
        if(!$accounts['accounts_inactive']) $accounts['accounts_inactive'] = 0;
        
        return $accounts;
    }
    
    public static function listRoles() {
        return [1 => 'Firma', 2 => 'Osoba fizyczna'];
    }
    
    public function getRole() {
        return $this->listRoles()[$this->role_fk];
    }
	
	public function getCategory() {
        $name = 'brak';
        return \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id' => $this->id_dict_customer_debt_collection_id])->one();
        //if($category)  $name = $category->name;
        
        //return $name;
    }
    public function getGroup() {
        return \backend\Modules\Crm\models\CrmGroup::find()->where( ['id' => $this->id_crm_group_fk])->one();
    }
    
    public function getGuardian() {
        return \backend\Modules\Company\models\CompanyEmployee::find()->where( ['id' => $this->id_employee_guardian_fk])->one();
    }
    
    public function getLimits() {
        $limits = [];
        $limits['credit'] = 0;
        if($this->debt_limit)  $limits['credit'] = $this->debt_limit;
        
        if($limits['credit'] == 0 && $this->group)
            $limits['credit'] = $this->group['debt_limit'];

        return $limits;
    }
    
    public function getFulladdress() {
        $fulladdress = $this->address;
        
        if($fulladdress) {
            if($this->postal_code) {
                $fulladdress .= ', '.$this->postal_code;
                
                if($this->city)
                    $fulladdress .= ' '.$this->city;
            } else {
                if($this->city)
                    $fulladdress .= ', '.$this->city;
            }
        }
        
        return $fulladdress;
    }
    
    public static function inCharacter() {
        $list = [];
        
        $list[0] = 'Strona przeciwna';
        $roles = \backend\Modules\Dict\models\DictionaryValue::find()->where( ['id_dictionary_fk' => 11])->all();
        foreach($roles as $key => $role) {
            $list[$role->id] = $role->name;
        }
        
        return $list;
    }
    
    public static function getSettlement($id, $option) {
        $stats = []; $stats['amount'] = 0; $stats['costs'] = 0; $stats['time'] = 0; $stats['constant'] = 0;
        $sqlStats = false;
        if($option == 1) {
            $lastInvSql = "select max(date_issue) as last_period from {{%acc_invoice}} i join {{%acc_order}} o on o.id=i.id_order_fk where i.status=1 and o.id_customer_fk = ".$id;
            $lastInv = \Yii::$app->db->createCommand($lastInvSql)->queryOne(); 
            
            if($lastInv && $lastInv['last_period']) {
                $sqlStats = "select sum(unit_price*(confirm_time-acc_limit)) as s_amount, sum(unit_time) as s_time, sum(acc_cost_invoice) as s_cost "
                            ." from {{%acc_actions}} a join {{%acc_order}} o on o.id=a.id_order_fk and o.id_customer_fk = ".$id
                            ." where a.status = 1 and a.is_confirm = 1 and acc_period = '".date('Y-m', strtotime($lastInv['last_period']))."'";
            }
        } else if($option == 2) {
            $sqlStats = "select sum(unit_price*confirm_time) as s_amount, sum(unit_time) as s_time, sum(acc_cost_invoice) as s_cost "
                            ." from {{%acc_actions}} a join {{%acc_order}} o on o.id=a.id_order_fk and o.id_customer_fk = ".$id
                            ." where a.status = 1 and a.is_confirm = 1 and acc_period like '".date('Y')."-%'";
        } else {
            $sqlStats = "select sum(unit_price*confirm_time) as s_amount, sum(unit_time) as s_time, sum(acc_cost_invoice) as s_cost "
                            ." from {{%acc_actions}} a join {{%acc_order}} o on o.id=a.id_order_fk and o.id_customer_fk = ".$id
                            ." where a.status = 1 and a.is_confirm = 1";
        }
        if($sqlStats) {
            $statsData = \Yii::$app->db->createCommand($sqlStats)->queryOne();
            if($statsData) { 
                $stats['amount'] = $statsData['s_amount']; 
                $stats['costs'] = $statsData['s_cost']; 
                $stats['rate'] = $statsData['s_amount']; 
                $timeH = intval($statsData['s_time']/60);
                $timeM = $statsData['s_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
                $stats['time'] = $timeH.':'.$timeM;
            }
        }
        return $stats;
    }
    
    public static function getAccountsList($id) {
        return \common\models\crm\CUser::find()->where(['status' => 10])->andWhere('id != '.Yii::$app->user->id)
                                               ->andWhere('(id in (select id_user_fk from {{%customer}} where status = 1 and id = '.$id.') or id in (select id_user_fk from {{%customer_person}} where status = 1 and id_customer_fk = '.$id.'))')
                                               ->all();
    }
    
    public static function getListWithAccount() {
        if(!Yii::$app->user->isGuest) {
            //$employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();
            //$departments = [];
            
           /* if(!$employee->is_admin) {
                array_push($departments, 0);
                foreach($employee->departments as $key => $department) {
                    array_push($departments, $department->id_department_fk);
                }
                
                return Customer::find()->where(['status' => 1])->andWhere('id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $departments).') )')->orderby('name')->all();
            } else {*/
                return Customer::find()->select(["id", "case when (instr(upper(name),upper(symbol)) = 0 ) then concat(name,' [',symbol,']') else name end as name"])
									   ->where(['status' => 1, 'type_fk' => 1])->andWhere('id_user_fk != 0 and id_user_fk is not null')->orderby('name collate `utf8_polish_ci`')->all();
           // }
        } else {
            return [];
        }
    }
    
    public function getNopaid() {
        return DebtReport::find()->where(['id_customer_fk' => $this->id])->all();
    }
    
    public static function setEstimateOf($id, $nip) {
        $sql = "update {{%debt_report}} set id_customer_fk = ".$id." where nip = '".$nip."'";
        $result = \Yii::$app->db->createCommand($sql)->execute();  

        return 1;    
    }
}
