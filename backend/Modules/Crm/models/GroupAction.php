<?php

namespace frontend\Modules\Crm\models;

use Yii;
use yii\base\Model;

use backend\Modules\Crm\models\Customer;

/**
 * GroupAction is the model behind the contact form.
 */
class GroupAction extends Model
{
    public $ids; 
    public $employees_list;
	public $user_action;
    public $group_id;
    public $status_id;
    public $debt_lock;
    public $departments_list = [];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ids'], 'required'],
			//['employees_list', 'required', 'when' => function($model) { return ($model->user_action == 'addEmployees' || $model->user_action == 'delEmployees'); }, 'message' => 'Proszę wybrać pracowników' ],
            [['group_id', 'status_id', 'debt_lock'], 'integer'],
            [['ids', 'employees_list', 'departments_list'], 'safe'],
            //['date_to','validateDates'],
        ];
    }
    public function validateDates(){
		if(strtotime($this->date_to) < strtotime($this->date_from)){
			//$this->addError('date_from','Please give correct Start and End dates');
			$this->addError('date_to','Proszę podać poprawną datę końca');
		} 
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids' => 'Klienci',
            'employees_list' => 'Pracownicy',
            'departments_list' => 'Działy',
            'group_id' => 'Segment',
            'status_id' => 'Status'
        ];
    }

    public function setGroup() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer}} set id_crm_group_fk = ".(($this->group_id)?$this->group_id:0)." where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
        
		
		/*foreach($cases as $key => $value) {
			$modelArch = new CalCaseArch();
			$modelArch->id_case_fk = $key;
			$modelArch->user_action = 'addEmployee';
			$modelArch->data_change = \yii\helpers\Json::encode($this);
			$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
			$modelArch->created_by = \Yii::$app->user->id;
			$modelArch->created_at = new Expression('NOW()');
		}*/
		
        return true;
    }
    
    public function setStatus() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer}} set id_dict_customer_status_id = ".(($this->status_id)?$this->status_id:0)." where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
		
        return true;
    }
    
    public function setLock() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer}} set debt_lock = 1 where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
		
        return true;
    }
    
    public function setUnlock() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer}} set debt_lock = 0 where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
		
        return true;
    }
    
    public function addDepartments() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer}} set id_dict_customer_status_id = ".(($this->status_id)?$this->status_id:0)." where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
		
        return true;
    }
    
    public function delDepartments() {            
        $customers = [];
		
		$sqlUpdate = "update {{%customer_department}} set status = -1, deleted_by = ".(($this->status_id)?$this->status_id:0)." where id in (".$this->ids.")";
		$relData = \Yii::$app->db->createCommand($sqlUpdate)->execute();
		
        return true;
    }
}
