<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Crm\models\CustomerPerson;

/**
 * CustomerPersonSearch represents the model behind the search form about `backend\Modules\Crm\models\CustomerPerson`.
 */
class CustomerPersonSearch extends CustomerPerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_customer_fk', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['lastname', 'firstname', 'phone', 'email', 'position', 'description', 'custom_data', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerPerson::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_customer_fk' => $this->id_customer_fk,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data]);

        return $dataProvider;
    }
}
