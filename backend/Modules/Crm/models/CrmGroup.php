<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%crm_group}}".
 *
 * @property integer $id
 * @property integer $type_fk
 * @property string $name
 * @property string $symbol
 * @property integer $debt_day_limit
 * @property double $debt_limit
 * @property string $custom_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CrmGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crm_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'symbol'], 'required'],
            [['type_fk', 'debt_day_limit', 'status', 'created_by', 'updated_by', 'deleted_by', 'only_management'], 'integer'],
            [['debt_limit'], 'number'],
            [['custom_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'describe'], 'safe'],
            [['name'], 'string', 'max' => 300],
            [['symbol'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'name' => Yii::t('app', 'Name'),
            'symbol' => Yii::t('app', 'Symbol'),
            'debt_day_limit' => Yii::t('app', 'Limit dni zwłoki'),
            'debt_limit' => Yii::t('app', 'Limit kredytu kupieckiego'),
            'describe' => 'Opis',
            'custom_options' => Yii::t('app', 'Custom Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'only_management' => 'ustawiane tyko przez zarząd'
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			} else { 
				$this->updated_by = \Yii::$app->user->id;  
			}
			return true;
		} else { 				
			return false;
		}
		return false;
	}
 
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function getList() {
        if(!Yii::$app->user->isGuest) {
            return CrmGroup::find()->where(['status' => 1, 'type_fk' => 1])->orderby('name collate `utf8_polish_ci`')->all();//->select(["id", "case when (instr(upper(name),upper(symbol)) = 0 ) then concat(name,' [',symbol,']') else name end as name"])
                                   
        } else {
            return [];
        }
    }
}
