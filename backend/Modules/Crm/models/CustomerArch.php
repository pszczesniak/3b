<?php

namespace backend\Modules\Crm\models;

use Yii;
use yii\helpers\Url;
use yii\db\Expression;

use backend\Modules\Crm\models\Customer;
/**
 * This is the model class for table "{{%customer_arch}}".
 *
 * @property integer $id
 * @property integer $table_fk
 * @property integer $id_root_fk
 * @property string $data_arch
 * @property string $created_at
 * @property integer $created_by
 */
class CustomerArch extends \yii\db\ActiveRecord
{
   public $id_fk;
   public $type_fk;
   /* public $style="bg-teal"; 
	public $icon="pencil"; 
	public $text = 'Aktualizacja'; 
	public $content = '';*/
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_fk', 'id_root_fk'], 'required'],
            [['table_fk', 'id_root_fk', 'created_by'], 'integer'],
            [['user_action'], 'string'],
            [['data_arch', 'data_change', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_fk' => Yii::t('app', 'Table Fk'),
            'id_root_fk' => Yii::t('app', 'Id Root Fk'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
                $this->created_at = new Expression('NOW()');
            }
        }
        
        return true;
    }
	
	public function getCase() {
        return ($this->user_action == 'matter' || $this->user_action == 'project') ? \backend\Modules\Task\models\CalCase::findOne($this->data_change) : false;
    }
	
	public function getTask() {
        return ($this->user_action == 'case' || $this->user_action == 'event') ? \backend\Modules\Task\models\CalTask::findOne($this->data_change) : false;
    }
	
	public function getCreator() {
		return $user = \common\models\User::findOne($this->created_by);
	}
	
	public static function setStyle($type) {

		$style="bg-teal";
		if($type == 'docs') {
			$style="bg-blue";
		}
		if($type == 'matter') {
			$style="bg-purple"; 
		}
		if($type == 'project') {
			$style="bg-orange"; 
		}
		if($type == 'create') {
			$style = 'bg-green';
		}
		if($type == 'delete') {
			$style = 'bg-red';
		}
        if($type == 'debt_lock') {
			$style = 'bg-red';
		}
        if($type == 'debt_unlock') {
			$style = 'bg-green';
		}
		
		return $style;
	}
	
	public function getStyle() {
		return self::setStyle($this->user_action);
	}
	
	public static function setIcon($type) {
		$icon="pencil"; 
		if($type == 'docs') {
			$icon="file"; 
		}
		if($type == 'matter') {
			$icon="balance-scale"; 
		}
		if($type == 'project') {
			$icon="folder-open"; 
		}
		if($type == 'create') {
			$icon = 'briefcase';
		}
		if($type == 'delete') {
			$icon = 'trash';
		}
        if($type == 'debt_lock') {
			$icon = 'lock';
		}
        if($type == 'debt_unlock') {
			$icon = 'unlock';
		}
		
		return $icon;
	}
	
	public function getIcon() {
		return self::setIcon($this->user_action);
	}
	
	public static function setTitle($type, $id, $short, $avatar,$user) {
		$avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$user."/preview.jpg"))?"/uploads/employees/cover/".$user."/preview.jpg":"/images/default-user.png";			
		$title = 'Aktualizacja '.$type;
		if($type == 'docs') {
			$customer = Customer::findOne($id);
			$title = 'Nowe dokumenty'; 
			if(!$short)
				$title .= ' do klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$customer->name.'</a>';
		}
		if($type == 'matter') {
			$model = \backend\Modules\Task\models\CalCase::findOne($id);
			$title = 'Dodanie sprawy: '.'<a href="'.Url::to(['/task/matter/view','id'=>$id]).'" title="Przypisanie do klienta '.$model->customer['name'].'">'.$model->name.'</a>';
		}
		if($type == 'project') {
			$model = \backend\Modules\Task\models\CalCase::findOne($id);
			$title = 'Dodanie projektu: '.'<a href="'.Url::to(['/task/project/view','id'=>$id]).'" title="Przypisanie do klienta '.$model->customer['name'].'">'.$model->name.'</a>';
		}
		if($type == 'create') {
			$model = \backend\Modules\Crm\models\Customer::findOne($id);
			$title = 'Utworzenie klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$model->name.'</a>';
		}
		if($type == 'delete') {
			$model = \backend\Modules\Crm\models\Customer::findOne($id);
			$title = 'Usunięcie klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$model->name.'</a>';
		}
		if($type == 'update' || $type == 'edit') {
			$model = \backend\Modules\Crm\models\Customer::findOne($id);
			if($short == true)
				$title = 'Aktualizacja klienta';
		    else	
				$title = 'Aktualizacja klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$model->name.'</a>';
		}
        if($type == 'debt_lock') {
			$model = \backend\Modules\Crm\models\Customer::findOne($id);
			if($short == true)
				$title = 'Włączenie blokady';
		    else	
				$title = 'Włączenie blokady na klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$model->name.'</a>';
		}
        if($type == 'debt_unlock') {
			$model = \backend\Modules\Crm\models\Customer::findOne($id);
			if($short == true)
				$title = 'Wyłączenie blokady';
		    else	
				$title = 'Wyłączenie blokady na klienta <a href="'.Url::to(['/crm/customer/view', 'id' => $id]).'">'.$model->name.'</a>';
		}
		
		return ($avatar == true) ? '<figure><img src="'. $avatar .'" alt="Avatar"></figure>'.$title : $title;
	}
	
	public function getTitle() {
		return self::setTitle($this->user_action, $this->data_change, true, true, $this->created_by);
	}
	
	public static function setContent($type, $short, $avatar, $data_change, $data_arch) {
		$content = '';
		if($type == 'docs') {
			$content = '<p><ol>';
			foreach(\yii\helpers\Json::decode($data_change) as $key => $value) {
				$file = \common\models\Files::findOne($value);
				$content .= '<li><a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a></li>';
			}
			$content .= '</ol></p>';
		}
        
        if($type == 'update' || $type == 'edit') {
			$changeData = \yii\helpers\Json::decode($data_change);
			$archData = \yii\helpers\Json::decode($data_arch);
	
			$content = '<ul>';
			
			if( isset($archData['name']) &&  $archData['name'] != $changeData['name'] ) {
				$content .= '<li> zmiana nazwy z "<span class="old">'.$archData['name'].'</span>" na <span class="new">"'.$changeData['name'].'"</span></li>';
			}
            
            if( isset($archData['id_dict_customer_status_id']) &&  $archData['id_dict_customer_status_id'] != $changeData['id_dict_customer_status_id'] ) {
				$status = Customer::listStatus();
                $content .= '<li> zmiana statusu z <span class="old">'.  ( isset($status[$archData['id_dict_customer_status_id']]) ? $status[$archData['id_dict_customer_status_id']] : 'brak' ) .'</span> na <span class="new">'.  ( isset($status[$changeData['id_dict_customer_status_id']]) ? $status[$changeData['id_dict_customer_status_id']] : 'brak' ) .'</span> </li>';
			}
	     
			if( isset($archData['address']) &&  $archData['address'] != $changeData['address'] ) {
				if(empty($archData['address']))
					$content .= '<li> dodanie adresu <span class="new">'.$changeData['address'].'</span></li>';
				else
					$content .= '<li> zmiana adresu z "<span class="old">'.$archData['address'].'</span>" na <span class="new">"'.$changeData['address'].'"</span></li>';
			}
            
            if( isset($archData['phone']) &&  $archData['phone'] != $changeData['phone'] ) {
				if(empty($archData['address']))
					$content .= '<li> dodanie telefonu <span class="new">'.$changeData['phone'].'</span></li>';
				else
					$content .= '<li> zmiana telefonu z "<span class="old">'.$archData['phone'].'</span>" na <span class="new">"'.$changeData['phone'].'"</span></li>';
			}
            
            if( isset($archData['email']) &&  $archData['email'] != $changeData['email'] ) {
				if(empty($archData['address']))
					$content .= '<li> dodanie adresu email <span class="new">'.$changeData['email'].'</span></li>';
				else
					$content .= '<li> zmiana adresu email z "<span class="old">'.$archData['email'].'</span>" na <span class="new">"'.$changeData['email'].'"</span></li>';
			}
            
            if( isset($archData['nip']) &&  $archData['nip'] != $changeData['nip'] ) {
				if(empty($archData['nip']))
					$content .= '<li> dodanie NIP <span class="new">'.$changeData['nip'].'</span></li>';
				else
					$content .= '<li> zmiana NIP z "<span class="old">'.$archData['nip'].'</span>" na <span class="new">"'.$changeData['nip'].'"</span></li>';
			}
            
            if( isset($archData['regon']) &&  $archData['regon'] != $changeData['regon'] ) {
				if(empty($archData['regon']))
					$content .= '<li> dodanie REGON <span class="new">'.$changeData['regon'].'</span></li>';
				else
					$content .= '<li> zmiana REGON z "<span class="old">'.$archData['regon'].'</span>" na <span class="new">"'.$changeData['regon'].'"</span></li>';
			}
            
            if( isset($changeData['description']) && isset($archData['description']) &&  $archData['description'] != $changeData['description'] ) {
				if(empty($archData['description']))
					$content .= '<li> dodanie opisu <span class="new">'.$changeData['description'].'</span></li>';
				else
					$content .= '<li> zmiana opisu z "<span class="old">'.$archData['description'].'</span>" na <span class="new">"'.$changeData['description'].'"</span></li>';
			}
            
			$content .= '</ul>';
		}
				
		return $content;
	}
	
	public function getContent() {
		return self::setContent($this->user_action,true,true,$this->data_change, $this->data_arch);
	}
	
	public static function getRender($type, $id, $short, $avatar,$user, $data_change, $data_arch) {
		$item['style'] = self::setStyle($type);
		$item['icon'] = self::setIcon($type);
        if($type == 'matter' || $type == 'project') $id = $data_change;
		$item['title'] = self::setTitle($type, $id, $short, $avatar,$user);
		$item['content'] = self::setContent($type, $short, $avatar, $data_change, $data_arch);
		
		return $item;
	}
}
