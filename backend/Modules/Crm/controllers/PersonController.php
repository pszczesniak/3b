<?php

namespace app\Modules\Crm\controllers;

use Yii;
use backend\Modules\Crm\models\CustomerPerson;
use backend\Modules\Crm\models\CustomerPersonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;
/**
 * PersonController implements the CRUD actions for CustomerPerson model.
 */
class PersonController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'CustomerPerson' => CustomerPerson::className()
				]
			],
		];
	}
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerPerson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerPersonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerPerson model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerPerson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   
	public function actionCreate($id)  {
        
		$model = new CustomerPerson();
		$model->id_customer_fk = CustomHelpers::decode($id);
		
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$customer = $model->customer;
                $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                            .'&nbsp;/&nbsp;'
                            .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
                
                return array('success' => true, 'action' => 'cpaccount', 'info' => $infoHtml, 'refresh' => 'yes', 'table' => '#table-persons');	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formPerson', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formPerson', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formPerson', [
                'model' => $model, 
            ]);
        }
    }


    /**
     * Updates an existing CustomerPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

			Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('success' => true, 'refresh' => 'yes', 'table' => '#table-persons');	
		 	} else {
				return array('success' => false, 'html' => $this->renderPartial('_formPerson', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
        } else {
            return $this->renderPartial('_formPerson', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerPerson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->delete()) {
            if($model->user) {
               $user = $model->user;
               $user->status = 0; 
               $user->save();
            }
            $customer = $model->customer;
            $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                        .'&nbsp;/&nbsp;'
                        .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
                
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => "#table-persons", 'action' => 'dpaccount', 'info' => $infoHtml);	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => "#table-persons" );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerPerson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerPerson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
        if (($model = CustomerPerson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCaccount($id)
    {
        
		$model = $this->findModel($id);
		$model->account = 1;
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$customer = $model->customer;
                $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                            .'&nbsp;/&nbsp;'
                            .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
                
                return array('success' => true, 'action' => 'cpaccount', 'info' => $infoHtml);	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
	
	public function actionUaccount($id)
    {
        
		$model = $this->findModel($id);
		$model->login = $model->user['username'];
		
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('success' => true);	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionLock($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 0; 
           $user->save();
        }
        $customer = $model->customer;
        $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                    .'&nbsp;/&nbsp;'
                    .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
            
        return array('success' => true, 'alert' => 'Konto zostało zablokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'dpaccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 10; 
           $user->save();
        }
        $customer = $model->customer;
        $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                    .'&nbsp;/&nbsp;'
                    .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
            
        return array('success' => true, 'alert' => 'Konto zostało odblokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'dpaccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
}
