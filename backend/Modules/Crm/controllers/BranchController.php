<?php

namespace app\Modules\Crm\controllers;

use Yii;
use backend\Modules\Crm\models\CustomerPerson;
use backend\Modules\Crm\models\CustomerPersonSearch;
use backend\Modules\Crm\models\CustomerBranch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;
use yii\helpers\Url;
/**
 * BranchController implements the CRUD actions for CustomerBranch model.
 */
class BranchController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
   
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerBranch models.
     * @return mixed
     */
    public function actionIndex()  {
        if( count(array_intersect(["customerPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
       $searchModel = new CustomerBranch();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.branches');  
            if($params['params']) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = [];
        $post = $_GET;
        if(isset($_GET['CustomerBranch'])) {
            $params = $_GET['CustomerBranch'];
            \Yii::$app->session->set('search.branches', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(b.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "c.id in (select id_customer_fk from law_customer_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_crm_group_fk']) && !empty($params['id_crm_group_fk']) ) {
				array_push($where, "id_crm_group_fk = ".$params['id_crm_group_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "c.role_fk = ".$params['type_fk']);
            }
            if(isset($params['nip']) && !empty($params['nip']) ) {
				array_push($where, "nip = ".$params['nip']);
            }
        }
			
		$fields = [];
		$tmp = [];
        
        $grants = $this->module->params['grants'];
		
		$sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'c.symbol';
        if( isset($post['sort']) && $post['sort'] == 'group' ) $sortColumn = 'ifnull(g.symbol, g.name)';
        if( isset($post['sort']) && $post['sort'] == 'nip' ) $sortColumn = 'c.nip';
        /*if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'unit_time';*/
        
        $query = (new \yii\db\Query())
            ->select(['c.id as cid', 'c.name as cname', 'c.symbol as symbol', 'c.nip', 'b.id', 'b.name', 'b.address', 'b.city', 'b.postal_code', 'b.email', 'b.phone'])
            ->from('{{%customer}} c')
            ->join('JOIN', '{{%customer_branch}} b', 'c.id = b.id_customer_fk')
            //->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->where( ['c.status' => 1, 'c.type_fk' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
		//$statusList = \backend\Modules\Crm\models\Customer::listStatus();     
		foreach($rows as $key=>$value) {
			
			$tmp['customer'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['cid']]).'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" >'.$value['cname'].'</a>';
            $tmp['name'] = $value['name'];
            $tmp['email'] = $value['email'];
            $tmp['phone'] = $value['phone'];
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/crm/branch/update', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Edycja oddziału').'" class="btn btn-xs btn-default update" data-target="#modal-grid-item" data-label="Aktualizacja"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/crm/branch/delete', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Usunięcie oddziału').'" class="btn btn-xs btn-default remove"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			/*if($value['id_user_fk']) {
                $tmp['account'] = ($value['ustatus'] == 10) ? '<i class="fa fa-key text--green"></i>' : '<i class="fa fa-key text--red" data-toggle="tooltip" data-title="Konto zablokowane"></i>';
            } else {
                $tmp['account'] = '';
            }*/
            $tmp['actions'] = $actionColumn; 
            //$tmp['className'] = ($value['debt_lock'] == 1) ? 'danger' : 'default';
            $tmp['id'] = CustomHelpers::encode($value['id']);
			$tmp['nid'] = $value['id'];
            
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];

	}

    /**
     * Displays a single CustomerBranch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerBranch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   
	public function actionCreate($id)  {
        
		$model = new CustomerBranch();
		$model->id_customer_fk = CustomHelpers::decode($id);
		
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				//$customer = $model->customer;
               /* $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                            .'&nbsp;/&nbsp;'
                            .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';*/
                
                return array('success' => true, 'refresh' => 'yes', 'table' => '#table-branches');	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_form', [ 'model' => $model, ]), 'table' => '#table-branches', 'errors' => $model->getErrors() );	
		    }
        } else {
            return $this->renderAjax('_form', [
                'model' => $model, 
            ]);
        }
    }


    /**
     * Updates an existing CustomerPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)  {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

			Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
                $row['name'] = $model->name;
                $row['email'] = $model->email;
                $row['phone'] = $model->phone;
                $row['contact'] = $model->fulladdress;
                $row['employee'] = $model->guardian['fullname'];
				return array('success' => true, 'row' => $row, 'table' => '#table-branches', 'refresh' => 'inline', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1));	
		 	} else {
				return array('success' => false, 'html' => $this->renderPartial('_form', [ 'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerPerson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->delete()) {
            /*if($model->user) {
               $user = $model->user;
               $user->status = 0; 
               $user->save();
            }*/
            $customer = $model->customer;
         
            return array('success' => true, 'alert' => 'Oddział został usunięty', 'id' => $id, 'table' => "#table-branches", 'refresh' => 'yes');	
        } else {
            return array('success' => false, 'row' => 'Oddział nie został usunięty', 'id' => $id, 'table' => "#table-branches" );	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerPerson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerPerson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $id = CustomHelpers::decode($id);
        if (($model = CustomerBranch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCaccount($id) {
        
		$model = $this->findModel($id);
		$model->account = 1;
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$customer = $model->customer;
                $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                            .'&nbsp;/&nbsp;'
                            .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
                
                return array('success' => true, 'action' => 'cpaccount', 'info' => $infoHtml);	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
	
	public function actionUaccount($id)
    {
        
		$model = $this->findModel($id);
		$model->login = $model->user['username'];
		
		/*if (Yii::$app->request->isAjax && $model->load($_POST)) {
			Yii::$app->response->format = 'json';
			return \yii\widgets\ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('success' => true);	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionLock($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 0; 
           $user->save();
        }
        $customer = $model->customer;
        $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                    .'&nbsp;/&nbsp;'
                    .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
            
        return array('success' => true, 'alert' => 'Konto zostało zablokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'dpaccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 10; 
           $user->save();
        }
        $customer = $model->customer;
        $infoHtml = '<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$customer['accounts']['accounts_active'].'</b></span>'
                    .'&nbsp;/&nbsp;'
                    .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$customer['accounts']['accounts_inactive'].'</b></span>';
            
        return array('success' => true, 'alert' => 'Konto zostało odblokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'dpaccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
}
