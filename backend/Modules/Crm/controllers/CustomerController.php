<?php

namespace app\Modules\Crm\controllers;

use Yii;
use backend\Modules\Crm\models\Customer;
use backend\Modules\Crm\models\CustomerArch;
use backend\Modules\Crm\models\CustomerPerson;
use backend\Modules\Crm\models\CustomerSearch;
use backend\Modules\Crm\models\CustomerDepartment;
use backend\Modules\Task\models\CalCase;
use backend\Modules\Accounting\models\AccOrder;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'Customer' => Customer::className()
				]
			],
		];
	}
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["customerPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
       $searchModel = new CustomerSearch();
        $searchModel->id_dict_customer_status_id = \backend\Modules\Dict\models\Dictionary::setDefault(2);
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.customers');  
            if($params['params']) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = Customer::find()->where(['status' => 1]);
        
        $grants = $this->module->params['grants']; $where = [];
        $post = $_GET;
        if(isset($_GET['CustomerSearch'])) {
            $params = $_GET['CustomerSearch'];
            \Yii::$app->session->set('search.customers', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(c.name) like '%".strtolower( addslashes($params['name']) )."%'");
				array_push($where, "lower(c.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['symbol']) && !empty($params['symbol']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(c.symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
				array_push($where, "lower(c.symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_customer_fk from law_customer_department where id_department_fk = ".$params['id_department_fk'].")");
				array_push($where, "c.id in (select id_customer_fk from law_customer_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_customer_status_id']) && !empty($params['id_dict_customer_status_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
				array_push($where, "id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
            }
            if(isset($params['id_dict_customer_debt_collection_id']) && !empty($params['id_dict_customer_debt_collection_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
				array_push($where, "id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
            }
            if(isset($params['id_crm_group_fk']) && !empty($params['id_crm_group_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_crm_group_fk = ".$params['id_crm_group_fk']);
				array_push($where, "id_crm_group_fk = ".$params['id_crm_group_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("c.role_fk = ".$params['type_fk']);
				array_push($where, "c.role_fk = ".$params['type_fk']);
            }
            if(isset($params['is_archive']) && strlen($params['is_archive']) ) {
				array_push($where, "c.is_archive = ".$params['is_archive']);
            }
            if(isset($params['nip']) && !empty($params['nip']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("nip = ".$params['nip']);
				array_push($where, "nip = ".$params['nip']);
            }
        }
        
        if(count(array_intersect(["debtSpecial", "grantAll"], $grants)) == 0 && !$this->module->params['isSpecial']) {
            $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
            array_push($where, 'c.id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        }
		//$count = $fieldsDataQuery->count();
        
       // $fieldsData = $fieldsDataQuery->orderby('name')->all();
			
		$fields = [];
		$tmp = [];
        
        $grants = $this->module->params['grants'];
		
		$sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'c.symbol';
        if( isset($post['sort']) && $post['sort'] == 'group' ) $sortColumn = 'ifnull(g.symbol, g.name)';
        if( isset($post['sort']) && $post['sort'] == 'nip' ) $sortColumn = 'c.nip';
        /*if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'unit_time';*/
        
        $query = (new \yii\db\Query())
            ->select(['c.role_fk', 'c.id as id', 'c.name as name', 'c.symbol as symbol', 'c.nip', 'c.debt_limit', 'c.debt_lock', 'id_dict_customer_status_id',  'c.is_archive', "ifnull(g.name,' ') as gname", 'g.symbol as gsymbol', 'c.id_user_fk', 'u.status as ustatus'])
            ->from('{{%customer}} c')
            ->join('LEFT JOIN', '{{%crm_group}} g', 'g.id = c.id_crm_group_fk')
            ->join('LEFT JOIN', '{{%user}} u', 'u.id = c.id_user_fk')
            //->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->where( ['c.status' => 1, 'c.type_fk' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
		$statusList = \backend\Modules\Crm\models\Customer::listStatus();     
		foreach($rows as $key=>$value) {
			
			$tmp['name'] = '<a href="'.Url::to(['/crm/customer/view','id' => $value['id']]).'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
            $tmp['sname'] = $value['name'];
            $tmp['symbol'] = $value['symbol'];
            $tmp['nip'] = $value['nip'];
            $tmp['type'] = ($value['role_fk'] == 1) ? '<i class="fa fa-building text--teal" data-toggle="tooltip" data-title="Firma"></i>' : '<i class="fa fa-user text--blue" data-toggle="tooltip" data-title="Osoba fizyczna"></i>';
            $tmp['group'] = (($value['gsymbol']) ? $value['gsymbol'] : $value['gname'])
                            .(( count(array_intersect(["debtSpecial", 'grantAll'], $grants)) > 0 && $value['debt_limit']) ? '<br /><small class="text--red btn-icon"><i class="fa fa-credit-card"></i>'.number_format($value['debt_limit'],2,'.',' ').'</small>': '') ;
            $tmp['status'] = ($value['id_dict_customer_status_id']) ? $statusList[$value['id_dict_customer_status_id']] : '';
            //$tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            /*$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/crm/customer/view','id' => $value['id']]).'" class="btn btn-sm btn-default " title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["customerEdit", "grantAll"], $grants)) > 0 ) ? '<a href="'.Url::to(['/crm/customer/update','id' => $value['id']]).'" class="btn btn-sm btn-default" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil"></i></a>' : '';
				$tmp['actions'] .= ( count(array_intersect(["customerDelete", "grantAll"], $grants)) > 0 ) ? '<a href="'.Url::to(['/crm/customer/delete','id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-customers" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>':'';
			$tmp['actions'] .= '</div>';*/
            $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        .'<li><a href="'.Url::to(['/crm/customer/view','id' => $value['id']]).'" ><i class="fa fa-eye text--blue"></i>&nbsp;Szczegóły</a></li>'
                                        . ( (count(array_intersect(["customerEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/crm/customer/update','id' => $value['id']]).'" ><i class="fa fa-pencil-alt text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["customerDelete", "grantAll"], $grants)) > 0 ) ? '<li class="remove" data-table="#table-customers" href="'.Url::to(['/crm/customer/delete', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                        . ( (count(array_intersect(["customerDelete", "grantAll"], $grants)) > 0 && $value['is_archive'] == 0) ? '<li class="modalConfirm" data-table="#table-customers" href="'.Url::to(['/crm/customer/archive', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-label="Przenieś klienta do archiwum" title="'.Yii::t('app', 'Przenieś do archiwum').'"><a href="#"><i class="fa fa-archive text--purple"></i>&nbsp;<span class="text--purple">Przenieś do archiwum</span></a></li>' : '')            
                                        . ( (count(array_intersect(["customerDelete", "grantAll"], $grants)) > 0 && $value['is_archive'] == 1) ? '<li class="modalConfirm" data-table="#table-customers" href="'.Url::to(['/crm/customer/unarchive', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-label="Przywróć klienta z archiwum" title="'.Yii::t('app', 'Przywróć z archiwum').'"><a href="#"><i class="fa fa-backward text--teal"></i>&nbsp;<span class="text--teal">Przywróć z archiwum</span></a></li>' : '')            
                                        
                                        //. ( (count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0 ) ? '<li class="update" data-table="#table-customers" href="'.Url::to(['/crm/customer/limits', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-credit-card text--purple\'></i>'.Yii::t('app', 'Ustaw limit').'" title="'.Yii::t('app', 'Ustaw limit').'"><a href="#"><i class="fa fa-credit-card text--purple"></i>&nbsp;Ustaw limity</a></li>' : '')            
                                        //. ( (count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0 && $value['debt_lock'] == 0) ? '<li class="deleteConfirm" data-table="#table-customers" href="'.Url::to(['/crm/customer/dlock', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-label="Zablokuj klienta" title="'.Yii::t('app', 'Zablokuj klienta').'"><a href="#"><i class="fa fa-lock text--red"></i>&nbsp;<span class="text--red">Zablokuj klienta</span></a></li>' : '')            
                                        //. ( (count(array_intersect(["debtSpecial", "grantAll"], $grants)) > 0 && $value['debt_lock'] == 1) ? '<li class="deleteConfirm" data-table="#table-customers" href="'.Url::to(['/crm/customer/dunlock', 'id' => $value['id']]).'" data-target="#modal-grid-item"  data-label="Odblokuj klienta" title="'.Yii::t('app', 'Odblokuj klienta').'"><a href="#"><i class="fa fa-unlock text--green"></i>&nbsp;<span class="text--green">Odblokuj klienta</span></a></li>' : '')            
                                        
                                        //.'<li role="separator" class="divider"></li>'
                                        //.'<li><a href="#">Separated link</a></li>'
                                      .'</ul>';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			if($value['id_user_fk']) {
                $tmp['account'] = ($value['ustatus'] == 10) ? '<i class="fa fa-key text--green"></i>' : '<i class="fa fa-key text--red" data-toggle="tooltip" data-title="Konto zablokowane"></i>';
            } else {
                $tmp['account'] = '';
            }
            $tmp['archive'] = ($value['is_archive']) ? '<i class="fa fa-archive text--purple"></i>' : '';
            $tmp['className'] = ($value['debt_lock'] == 1) ? 'danger' : 'default';
            $tmp['id'] = CustomHelpers::encode($value['id']);
			$tmp['nid'] = $value['id'];
            
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];

	}

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)   {
        if( count(array_intersect(["customerPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        } 
        
        $model = $this->findModel($id);
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        $departmentsList = ''; $departments = [];
        $filesList = '';
        $personsList = '';
        $grants = $this->module->params['grants'];
        $accPermission = false;
        if( in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) ) $accPermission = true;
        if( isset($this->module->params['employeeKind']) && $this->module->params['employeeKind'] == 100 ) $accPermission = true;
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
            array_push($departments, $item->id_department_fk);
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        $managerData = \backend\Modules\Company\models\CompanyDepartment::find()->where(['status' => 1, 'id_employee_manager_fk' => $this->module->params['employeeId']])->all();
        foreach($managerData as $key => $value) {
            if( in_array($value->id, $departments) ) {
                $accPermission = true;
            }
        }
        
        return $this->render('view', [
            'model' => $model, 'accPermission' => $accPermission, 'grants' => $grants
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
		
		if( count(array_intersect(["customerAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = new Customer();        
        $model->type_fk = 1;
        $model->role_fk = 1;
        $model->acc_order_required = 1;
		$model->createOrder = 1;
        $person = new CustomerPerson();
		$departments = [];
        $grants = $this->module->params['grants'];
	
		$save = false; $send = false;
        
        $specialDepartments = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartments as $key => $value) {
            array_push($model->departments_list, $value->id);
        }
		if (Yii::$app->request->isPost ) {
			
			$model->load(Yii::$app->request->post());
            $person->load(Yii::$app->request->post());
			if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']))
            
			if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
                foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                    array_push($model->departments_list, $value);
                }
            }
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");
			$transaction = \Yii::$app->db->beginTransaction();
			try {
				if($model->validate()) {
                    $save = true;
			    }
					
				if($save) {
					if($model->save()) {
						if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
							//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
							foreach($_POST['Customer']['departments_list'] as $key=>$value) {
								$model_cd = new CustomerDepartment();
								$model_cd->id_customer_fk = $model->id;
								$model_cd->id_department_fk = $value;
								$model_cd->save();
							}
						}
					
						if(in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && $model->createOrder) {
							$order->id_customer_fk = $model->id;
							if(!$order->save()) {
								$model->id = null;
								$model->isNewRecord = true;
								$transaction->rollBack();
								return  $this->render('create', ['model' => $model, 'departments' => $departments, 'person' => $person, 'order' => $order]) ;
							} 
						} else {
							$send = true;
						}
					
						$person->id_customer_fk = $model->id;
						$person->save();
					
						$transaction->commit();
					}

					return $this->redirect(['view', 'id' => $model->id]);	
				} else {
					//return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'departments' => $departments, 'person' => $person]), 'errors' => $model->getErrors() );	
					$transaction->rollBack();
					return  $this->render('create', [  'model' => $model, 'departments' => $departments, 'person' => $person, 'order' => $order]) ;
				}
			} catch (Exception $e) {
				$transaction->rollBack();
				return  $this->render('create', ['model' => $model, 'departments' => $departments, 'person' => $person]) ;
			}		
			//return array('success' => true, 'alert' => 'Klient <b>'.$model->name.'</b> został zapisany' );						
		} else {
			return  $this->render('create', ['model' => $model, 'departments' => $departments, 'person' => $person]) ;	
		}
	}

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        if( count(array_intersect(["customerEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        /*if(!$model->person) {
            $person = new CustomerPerson();
            $person->id_customer_fk = $id;
            $person->save();
        }*/
        $departments = [];
        $grants = $this->module->params['grants'];
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
            array_push($model->departments_list, $item->id_department_fk);
        }
        $filesList = '';
       /* foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul class="files-set">'.$filesList.'</ul>' :  '<ul class="files-set"></ul>';*/

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");
           /* if( isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
                foreach($_POST['Customer']['departments_list'] as $key => $value) {
                   // array_push($model->departments_rel, $value);
                    array_push($model->departments_list, $value);
                }
            } */
			if($model->validate() && $model->save()) {
				
				/*CustomerDepartment::deleteAll('id_customer_fk = :customer', [':customer' => $id]);
				if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
					
                    foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                        $model_cd = new CustomerDepartment();
                        $model_cd->id_customer_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} */
				
				return array('success' => true,  'alert' => 'Dane klienta <b>'.$model->name.'</b> zostały zaktualizowane.' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model, 'departments' => $departments]) ;	
		}
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTask::SCENARIO_DELETE;
        
        $cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $model->id])->count();
        if($cases > 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Klient <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane aktywne postępowania')  );
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Klient <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane aktywne postępowania', 'id' => $id, 'table' => '#table-customers');	
			}
        }
        
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Klient <b>'.$model->name.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Klient <b>'.$model->name.'</b> został usunięty', 'id' => $id, 'table' => '#table-customers');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Klient <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Klient <b>'.$model->name.'</b> nie został usunięty', 'id' => $id, 'table' => '#table-customers');	
			}
        }
        
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
		//$id = CustomHelpers::decode($id);

		if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Klient o wskazanym identyfikatorze nie został znaleziony w systemie.');
        }
    }
    public function actionPersons($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$persons = [];
		$tmp = [];
		
		$model = $this->findModel($id);
		$personsData = $model->persons;
		
		foreach($personsData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success viewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/crm/person/update', 'id' => $value->id]).'" class="btn btn-xs btn-success viewModal" data-table="#table-persons" data-target="#modal-grid-item" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil-alt"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/crm/person/delete', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-persons"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value->id;
			$tmp['firstname'] = $value->firstname;
			$tmp['lastname'] = $value->lastname;
			$tmp['position'] = $value->position;
			$value->phone = $value->phone?$value->phone:'brak';
			$value->email = $value->email?$value->email:'brak';
            $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            $tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			$tmp['contact'] = '<div class="btn-icon"><i class="glyphicon glyphicon-phone"></i>'.$value->phone.'</div><div class="btn-icon"><i class="glyphicon glyphicon-envelope"></i>'.$value->email.'</div>';
            $tmp['info'] = '<div class="btn-icon text--purple"><i class="fa fa-map-marker-alt"></i>'.(($value->branch) ? $value->branch['name'] : 'brak oddziału').'</div><div class="btn-icon text--purple""><i class="fa fa-user"></i>'.(($value->guardian) ? $value->guardian['fullname'] : 'brak opiekuna').'</div>';

            $tmp['actions'] = $actionColumn;
            if($value->user) {
                $userActive = ($value->user['status'] == 10) ? '<a href="'.Url::to(['/crm/person/lock', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-label="Zablokuj konto" data-table="#table-persons">Zablokuj</a>' 
                                                             : '<a href="'.Url::to(['/crm/person/unlock', 'id' => $value->id]).'" class="btn btn-xs bg-teal remove" data-label="Odblokuj konto" data-table="#table-persons">Odblokuj</a>';
            }
            $tmp['user'] = (!$value->id_user_fk) ? 
							'<a href="'.Url::to(['/crm/person/caccount', 'id' => $value->id]).'" class="viewModal" title="Utwórz konto" data-table="#table-persons" data-form="person-form" data-target="#modal-grid-person"><i class="fa fa-user-plus text--green"></i></a>' :
							'<a href="'.Url::to(['/crm/person/uaccount', 'id' => $value->id]).'" class="viewModal" title="Edytuj konto" data-table="#table-persons" data-form="person-form" data-target="#modal-grid-person"><i class="fa fa-user text--blue"></i></a><br />'.$userActive;
			//array_push($tmp, '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>');
			
			array_push($persons, $tmp); $tmp = [];
		}
		
		return $persons;
	}
    
    public function actionExport() {		
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:J2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Klienci');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Nazwa')
            ->setCellValue('B3', 'Symbol')
            ->setCellValue('C3', 'Segment')
            ->setCellValue('D3', 'NIP')
			->setCellValue('E3', 'REGON')
            ->setCellValue('F3', 'Kod pocztowy')
            ->setCellValue('G3', 'Miasto')
			->setCellValue('H3', 'Adres')
            ->setCellValue('I3', 'Opiekun klienta')
			->setCellValue('J3', 'Działy');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('B3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->getStartColor()->setARGB('D9D9D9');
 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('C3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('E3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('E3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('F3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('G3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('G3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('G3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('H3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('H3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('I3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('I3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('I3')->getFill()->getStartColor()->setARGB('D9D9D9');
        
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('J')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('J3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('J3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('J3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4; 
		$data = [];

        $grants = $this->module->params['grants']; $where = [];
        $post = $_GET;
        if(isset($_GET['CustomerSearch'])) {
            $params = $_GET['CustomerSearch'];
            \Yii::$app->session->set('search.customers', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(c.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['symbol']) && !empty($params['symbol']) ) {
				array_push($where, "lower(c.symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "c.id in (select id_customer_fk from law_customer_department where id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_customer_status_id']) && !empty($params['id_dict_customer_status_id']) ) {
				array_push($where, "id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
            }
            if(isset($params['id_dict_customer_debt_collection_id']) && !empty($params['id_dict_customer_debt_collection_id']) ) {
				array_push($where, "id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
            }
            if(isset($params['id_crm_group_fk']) && !empty($params['id_crm_group_fk']) ) {
				array_push($where, "id_crm_group_fk = ".$params['id_crm_group_fk']);
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "c.role_fk = ".$params['type_fk']);
            }
            if(isset($params['is_archive']) && strlen($params['is_archive']) ) {
				array_push($where, "c.is_archive = ".$params['is_archive']);
            }
            if(isset($params['nip']) && !empty($params['nip']) ) {
				array_push($where, "nip = ".$params['nip']);
            }
        }
        
        if(count(array_intersect(["grantAll"], $grants)) == 0) {
            array_push($where, 'c.id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        }
		
        $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'c.symbol';
        if( isset($post['sort']) && $post['sort'] == 'group' ) $sortColumn = 'ifnull(g.symbol, g.name)';
        if( isset($post['sort']) && $post['sort'] == 'nip' ) $sortColumn = 'c.nip';
        /*if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'unit_time';*/
        
        $query = (new \yii\db\Query())
            ->select(['c.role_fk', 'c.id as id', 'c.name as name', 'c.symbol as symbol', 'c.nip', 'c.regon', 'c.debt_limit', 'c.debt_lock', 'c.is_archive', 'id_dict_customer_status_id', 'city', 'c.postal_code', 'c.address',
                       "ifnull(g.name,' ') as gname", 'g.symbol as gsymbol', "concat_ws(' ', e.lastname, e.firstname) as guardian",
                       "(select group_concat(symbol) from {{%company_department}} d join {{%customer_department}} cd on cd.id_department_fk=d.id where id_customer_fk=c.id) as departments"])
            ->from('{{%customer}} c')
            ->join('LEFT JOIN', '{{%crm_group}} g', 'g.id = c.id_crm_group_fk')
            ->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = c.id_employee_guardian_fk')
            ->where( ['c.status' => 1, 'c.type_fk' => 1] );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
        $query->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $data = $query->all();
		$statusList = \backend\Modules\Crm\models\Customer::listStatus();    
	
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['name'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['symbol']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record['gname']); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record['nip']); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $i, $record['regon']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $i, $record['postal_code']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $i, $record['city']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'. $i, $record['address']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'. $i, $record['guardian']); 
         
                $objPHPExcel->getActiveSheet()->setCellValue('J'. $i, $record['departments']); 	
				$i++; 
			}  
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        
		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Klienci');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Klienci'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionCases($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = '';
		
        
        //$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $id = (!$id || empty($id)) ? 0 : $id;
        $cases = \backend\Modules\Task\models\CalCase::getList($id);
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        $list .= '<option value=""> - wybierz - </option>';
        if( count($matters) > 0 ) {
            $list .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        return ['list' => $list];
    }
    
    public function actionRelationship($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $listCase = '';
		$listOrder = '';
        
        //$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $id = (!$id || empty($id)) ? 0 : $id;
        //$cases = \backend\Modules\Task\models\CalCase::getList($id);
        $employeeId = $this->module->params['employeeId'];
        if( isset($_GET['eid']) && !empty($_GET['eid']) ) $employeeId = $_GET['eid'];  
        //$cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$employeeId.')')->orderby('name')->all();
        
        $cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->orderby('name')->all();
        
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        $listCase .= '<option value=""> - wybierz - </option>';
        if( count($matters) > 0 ) {
            $listCase .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $listCase .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            //$listCase .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $listCase .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        if($id == 0) {
            if(isset($_GET['actionPanel'])) {
                 $listOrder .= '<option>- brak zleceń -</option>';
            } else {
                //$orders = \backend\Modules\Accounting\models\AccOrder::getListAll();
				/*$sqlQuery = "select o.id as id, concat(o.name, ' [', c.name, ']') as name from {{%acc_order}} o join {{%customer}} c on c.id = o.id_customer_fk where o.status = 1 ";
				$sqlQuery .= (isset($_GET['date']) && !empty($_GET['date'])) ? " and '".date('Y-m-d', $_GET['date'])."' between date_from and ifnull(date_to, '".date('Y-m-d', $_GET['date'])."') " : '';
				$sqlQuery .= " order by c.name";
				$orders = Yii::$app->db->createCommand($sqlQuery)->queryAll();
        
                $listOrder .= '<option value=""> - wybierz - </option>';
                if($orders) { 
					foreach($orders as $key => $value) {
						$listOrder .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
					}
				}*/
            }
        } else {
            //$orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
			$orders = [];
			/*$dataOrderSql = \backend\Modules\Accounting\models\AccOrder::find()
					->where(['status' => 1])
					->andWhere('(id_customer_fk = '.$id.' or id in (select id_order_fk from {{%acc_order_item}} where status = 1 and id_customer_fk = '.$id.'))');
					if((isset($_GET['date']) && !empty($_GET['date']))) { 
						$dataOrderSql->andWhere(" '".date('Y-m-d', $_GET['date'])."' between date_from and ifnull(date_to, '".date('Y-m-d', $_GET['date'])."')");
					}
			$dataOrder = $dataOrderSql->all();
			//var_dump($dataOrder); exit;
			if(!isset($_GET['actionPanel'])) 
                $listOrder .= '<option value=""> - wybierz - </option>';
			
			if($dataOrder) {
				foreach($dataOrder as $key => $order) {
					//$orders[$order->id] = $order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : '');
					//array_push($orders, ['id' => $order->id, 'name' => ($order['name'].( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : ''))]);
					$listOrder .= '<option value="'.$order->id.'">'.($order->name.( ($order->id_customer_fk != $id) ? ' ['.$order->customer['name'].']' : '')).'</option>';
				}
			}*/
            
            /*foreach($orders as $key => $value) {
                $listOrder .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
            }*/
        }
        $listContacts = '';
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('lastname collate `utf8_polish_ci`, firstname')->all();
        $listContacts .= '<option value=""> - wybierz - </option>';
        foreach($contacts as $key => $model) {
            $listContacts .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
        
        $listBranches = '';
        $branches = \backend\Modules\Crm\models\CustomerBranch::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('name collate `utf8_polish_ci`')->all();
        $listBranches .= '<option value=""> - wybierz - </option>';
        foreach($branches as $key => $model) {
            $listBranches .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }
        
        return ['listContacts' => $listContacts, 'listBranches' => $listBranches, 'listCase' => $listCase, 'listOrder' => $listOrder];
    }
    
    public function actionLists($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $specialEmployees = [];
        if(isset($_GET['type']) && $_GET['type'] == 'matter' ) {
            $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
            foreach($specialEmployeesSql as $key => $value) {
                array_push($specialEmployees, $value->id_employee_fk);
            }
        }
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();       
        $model = Customer::findOne($id);
        $departments = ''; $ids = []; array_push($ids, 0); $employees_list = ''; $list_cases = '';
        $checked = 'checked';
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        foreach($model->departments as $key => $value) {
            if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                    
                array_push($ids, $value->id_department_fk);
            } else {
                if(in_array($value->id_department_fk, $this->module->params['departments'])) {                
                    //$departments .= '<li><input class="checkbox_department" id="d-'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.'><label for="d'.$value->id_department_fk.'">'.$value->department['name'].'</label></li>';
                    $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                    
                    array_push($ids, $value->id_department_fk);
                }
            }
        }
        
        if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin" || $admin > 0) {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
        } else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        }
        foreach($employees as $key => $model) {
            if($model->id == $this->module->params['employeeId'] || in_array($model->id, $specialEmployees) )  $checked = 'checked'; else $checked = '';
            $employees_list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'><label for="d'.$model->id.'">'.$model->fullname.'</label></li>';
        }
        
        $list = '';
        
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($contacts as $key => $model) {
            $list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
		
		//$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $cases = \backend\Modules\Task\models\CalCase::getList($id);
        $list_cases .= '<option value=""> - wybierz - </option>';
        /*foreach($cases as $key => $model) {
            $list_cases .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }*/
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        if( count($matters) > 0 ) {
            $list_cases .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list_cases .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        $ordersList = '';
        $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('name')->all();
        $ordersList .= '<option value=""> - wybierz - </option>';
        foreach($orders as $key => $model) {
            $ordersList .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }
        
        return ['contacts' => $list, 'departments' => $departments, 'employees' => $employees_list, 'cases' => $list_cases, 'orders' => $ordersList];
    }
    
    public function actionMlists($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $specialEmployees = [];
        if(isset($_GET['type']) && $_GET['type'] == 'matter' ) {
            $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
            foreach($specialEmployeesSql as $key => $value) {
                array_push($specialEmployees, $value->id_employee_fk);
            }
        }
        if( empty($id) ) $id = 0;
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();       
        $model = Customer::findOne($id);
        $departments = ''; $ids = []; array_push($ids, 0); $employees_list = ''; $list_cases = '';
        $checked = 'checked';
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        if($model) {
            foreach($model->departments as $key => $value) {
                if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                    $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                        
                    array_push($ids, $value->id_department_fk);
                } else {
                    if(in_array($value->id_department_fk, $this->module->params['departments'])) {                
                        //$departments .= '<li><input class="checkbox_department" id="d-'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.'><label for="d'.$value->id_department_fk.'">'.$value->department['name'].'</label></li>';
                        $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                        
                        array_push($ids, $value->id_department_fk);
                    }
                }
            }
        }
        if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin" || $admin > 0) {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
        } else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        }
        foreach($employees as $key => $emodel) {
            if($emodel->id == $this->module->params['employeeId'] || in_array($emodel->id, $specialEmployees) )  $checked = 'checked'; else $checked = '';
            $employees_list .= '<li><input class="checkbox_employee" id="d'.$emodel->id.'" type="checkbox" name="employees[]" value="'.$emodel->id.'" '.$checked.'><label for="d'.$emodel->id.'">'.$emodel->fullname.'</label></li>';
        }
        
        $list = ''; $listAccounts = '';
        
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($contacts as $key => $model) {
            $list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
        
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->andWhere('id_user_fk != 0 and id_user_fk is not null')->orderby('lastname')->all();
        foreach($contacts as $key => $model) {
            $listAccounts .= '<option value="'.$model->id_user_fk.'">'.$model->fullname.'</option>';
        }
		
		//$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        if(Yii::$app->params['env'] == 'dev') {
            $cases = \backend\Modules\Task\models\CalCase::getListInstances($id);
        } else {
            $cases = \backend\Modules\Task\models\CalCase::getList($id);
        }
        if(isset($_GET['prompt'])) {
            $list_cases .= '<option value=""> - '.$_GET['prompt'].' - </option>';
        }
        /*foreach($cases as $key => $model) {
            $list_cases .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }*/
        $matters = [];
        $projects = [];
        if(Yii::$app->params['env'] == 'dev') {
            foreach($cases as $key => $value) {
                if($value['type_fk'] == 1) 
                    $matters[$value['id']] = $value['name'];
                else
                    $projects[$value['id']] = $value['name'];
            }
        } else {
            foreach($cases as $key => $value) {
                if($value->type_fk == 1) 
                    $matters[$value->id] = $value->name;
                else
                    $projects[$value->id] = $value->name;
            }
        }
        
        if( count($matters) > 0 ) {
            $list_cases .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list_cases .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
        $list_orders = '<option> - wybierz - </option>';
        foreach($orders as $key => $value) { 
            $list_orders .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
        }
        
        return ['contacts' => $list, 'departments' => $departments, 'employees' => $employees_list, 'cases' => $list_cases, 'orders' => $list_orders, 'accounts' => $listAccounts];
    }
    
    public function actionTcases($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = $fieldsData = \backend\Modules\Task\models\CalCase::find()->where(['status' => 1, 'id_customer_fk' => CustomHelpers::decode($id)])->andWhere('id_dict_case_status_fk != 6');
        //$fieldsDataQuery = $fieldsDataQuery->where(['id_customer_fk' => CustomHelpers::decode($id)]);
           
        $fieldsData = $fieldsDataQuery->orderby('name')->all();
			
			
		$fields = [];
		$tmp = [];
        $status = \backend\Modules\Task\models\CalCase::listStatus('advanced');
		      
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($fieldsData as $key=>$value) {
			$type = ( $value->type_fk == 1) ? 'matter' : 'project';
			$tmp['customer'] = $value->customer['name'];
            $tmp['type'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Sprawa</span>' : '<span class="label bg-teal">Projekt</span>' ;
			$tmp['name'] = '<a href="'.Url::to(['/task/'.(($value->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $value->id]).'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value->name.'</a>';
            $tmp['status'] = '<label class="label '.( ($value['status'] == 1) ? $colors[$value['id_dict_case_status_fk']] : 'bg-red').'">'.( ($value['status'] == 1) ? $status[$value['id_dict_case_status_fk']] : 'Usunięta').'</label>';
			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/'.(($value->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
			$tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionTimeline($id) {
        $model = Customer::findOne($id);
        $arch = CustomerArch::find()->where(['id_root_fk' => $id, 'table_fk' => 1])->orderby('id desc')->all();
        
        return $this->render('_timeline', ['model' => $model, 'arch' => $arch]);
    }
    
    public function actionCreateajax() {
		
        $model = new Customer();
        $model->type_fk = 1;
        $model->role_fk = 1;
        $grants = $this->module->params['grants'];
        $departments = [];
        
        $specialDepartments = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartments as $key => $value) {
            array_push($model->departments_list, $value->id);
        }
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
                foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                    array_push($model->departments_list, $value);
                }
            }
            
            $model->status = 1;
            $model->type_fk = 1;
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");  
            if($model->validate() && $model->save()) {
                if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                        $model_cd = new CustomerDepartment();
                        $model_cd->id_customer_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} 
                
               /* try {
                    \Yii::$app->mailer->compose(['html' => 'newCustomer-html', 'text' => 'newCustomer-text'], ['model' => $model, 'order' => true])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                ->setTo(Yii::$app->params['newCustomerOrder'])
                                ->setBcc('kamila_bajdowska@onet.eu')
                                ->setSubject('Nowy klient w systemie: `'.$model->name.'`' )
                                ->send();
                } catch (\Swift_TransportException $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    $log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'newcusomer';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();
                }*/
                
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}
    
    public function actionOrders($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];
        
        $id = CustomHelpers::decode($id);
        
		$fieldsDataQuery = $fieldsData = \backend\Modules\Accounting\models\AccOrder::find()->where(['status' => 1])->andWhere( '(id_customer_fk = '.$id.' or id in (select id_order_fk from {{%acc_order_item}} where status = 1 and id_customer_fk = '.$id.'))');       
        $fieldsData = $fieldsDataQuery->orderby('id desc')->all();
		
		$fields = [];
		$tmp = [];
        $status = \backend\Modules\Task\models\CalCase::listStatus('normal');
		      
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
		foreach($fieldsData as $key=>$value) {
			$invoices = $value->invoices;
            $label = ( (count($invoices) == 1) ? 'faktura' : ( ( count($invoices) >= 2 && count($invoices) <= 4) ? 'faktury' : 'faktur') );
            
            $type = ( $value->type_fk == 1) ? 'matter' : 'project';
			$tmp['name'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'">'.$value->name.'</a>'. (($id != $value->id_customer_fk) ? ' <small class="text--purple">['.$value->customer['name'].']</small>' : '');
            $tmp['name'] .= ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? '<br /><small class="text--red">od '.$value['date_from'].' do '.$value['date_to'].'</small>' : '';
			$tmp['start'] = $value->date_from;
            $tmp['creator'] = $value->creator['fullname'];
            $tmp['method'] = '<span class="label" style="background-color:'.$value->type['type_color'].'" data-toggle="tooltip" data-title="'.$value->type['type_name'].'">'.$value->type['type_symbol'].'</span>&nbsp;&nbsp;'
                             .(($value->type_fk == 1) ? '<i class="fa fa-user text--purple" data-toggle="tooltip" data-title=""></i>' : '<i class="fa fa-users text--teal" data-toggle="tooltip" data-title="Scaleni klienci"></i>');
            $tmp['merging'] = ($value->type_fk == 1) ? '<i class="fa fa-user text--purple"></i>' : '<i class="fa fa-users text--teal"></i>' ;
            $tmp['invoice'] = (count($invoices) == 0) ? count($invoices) : '<a href="'.Url::to(['/accounting/order/view', 'id' => $value->id]).'" title="Pokaż szczegóły rozliczenia">'.count($invoices).'</a>';
			$tmp['actions'] = '<div class="edit-btn-group">';
				if($id != $value->id_customer_fk) {
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/view', 'id' => $value->id]).'" class="btn btn-xs btn-default"  title="'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';  
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/unbind', 'id' => $id]).'?order='.$value->id.'" class="btn btn-xs btn-danger remove" data-table="#table-orders" data-placement="left" data-title="Odłącz klienta od zlecenia" data-toggle="tooltip" title="Odłącz klienta od zlecenia" ><i class="fa fa-user-times"></i></a>';  
                } else {
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/updateajax', 'id' => $value['id']]).'" class="btn btn-xs btn-default viewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('lsdd', 'Update').'" ><i class="fa fa-pencil-alt"></i></a>';
                    $tmp['actions'] .= ( ($value->id_dict_order_type_fk == 2 || $value->id_dict_order_type_fk == 4) ? '<a href="'.Url::to(['/accounting/order/rates', 'id' => $value['id']]).'" class="btn btn-xs btn-default viewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-cog\'></i>'.Yii::t('lsdd', 'Stawki').'" title="'.Yii::t('lsdd', 'Stawki').'" ><i class="fa fa-cog"></i></a>' : '');
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/discounts', 'id' => $value->id]).'" class="btn btn-xs btn-default viewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-percent\'></i>'.Yii::t('lsdd', 'Rabaty').'" title="'.Yii::t('lsdd', 'Rabaty').'" ><i class="fa fa-percent"></i></a>';                    
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/deleteajax', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-orders"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
                }
            $tmp['actions'] .= '</div>';
            $tmp['className'] = ($value['date_to'] && $value['date_to'] < date('Y-m-d')) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
            $tmp['currency'] = $value->currency;
            $tmp['files'] =  '<span class="'.((!$value->files) ? 'text--grey' : 'bg-blue label').'">'.count($value->files) .'</span>';
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActionsSearch'])) {
            $params = $post['AccActionsSearch']; 
            if(isset($params['description']) && !empty($params['description']) ) {
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                //array_push($where, "id_dict_order_type_fk = ".$params['id_order_fk']);
                array_push($where, "id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "is_confirm = 1");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0");
            }
        } 
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "a.id_department_fk in (".implode(',', $this->module->params['managers']).")");
        }
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) == 0 ) {
            if(\Yii::$app->session->get('user.isManager') == 0)
                array_push($where, "a.id_employee_fk = ".$this->module->params['employeeId']."");
        }
        
        $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time ';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price ';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit ';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description", "a.acc_cost_description as cdescription",
                      "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "unit_price_native", "unit_price_invoice", "confirm_price", "acc_limit", "confirm_time", 
                      "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "acc_cost_native",  "a.acc_cost_currency_id as acc_cost_currency_id",
                      "o.name as oname", "cur.currency_symbol as currency_symbol",
                      "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])
            ->from('{{%acc_actions}} a')
            ->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_currency}} as cur', 'cur.id = o.id_currency_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1])->andWhere( '(a.id_customer_fk = '.$id.' or  a.id_customer_fk in (select aoi.id_customer_fk from {{%acc_order_item}} aoi join {{%acc_order}} ao on aoi.id_order_fk=ao.id where aoi.status=1 and ao.status = 1 and  ao.id_customer_fk = '.$id.'))');
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ', $where));
        }

        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        //echo $query->createCommand()->sql; exit;
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_native else 0 end) as costs",
                      "sum(case when is_confirm=1 then confirm_time else 0 end) as confirm_time", 
                      "sum(case when a.type_fk=5 then unit_price_native when acc_limit=0 then (confirm_time*unit_price_native) else ((confirm_time-acc_limit)*unit_price_native) end) as amount"])
            ->from('{{%acc_actions}} as a')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1])->andWhere( '(a.id_customer_fk = '.$id.' or  a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where co.id_customer_fk = '.$id.'))');
            
        $queryOrders = (new \yii\db\Query())
            ->select(["a.id_order_fk", "min(rate_constatnt) as rate_constatnt"])
            ->from('{{%acc_actions}} as a')
            ->join('LEFT JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1] )
            ->andWhere( '(a.id_customer_fk = '.$id.' or  a.id_customer_fk in (select coi.id_customer_fk from {{%acc_order_item}} coi join {{%acc_order}} co on co.id=coi.id_order_fk where co.id_customer_fk = '.$id.'))')
            ->groupby(['a.id_order_fk']);
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
            $queryOrders = $queryOrders->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs'];
        
        $totalRow['lump'] = 0; $totalRow['all'] = $costs+$totalRow['amount']; 
        $totalOrders = $queryOrders->all(); 
        foreach($totalOrders as $io => $order) {
            $totalRow['lump'] += $order['rate_constatnt'];
        }
        $totalRow['all'] += $totalRow['lump'];
        
        /*$query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );*/
        
       // if( $count <= 4000 ) {
           // $fieldsData = $fieldsDataQuery->orderby('action_date desc')->all();
            $tmp = [];
            $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
            foreach($rows as $key=>$value) {
                $tmpType = $dict[$value['type_fk']];
                $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].' cursor-pointer" title="'.$tmpType['name'].'"></i>';
                $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="Kliknij aby oznaczyć czynność">'.$value['oname'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
                
                $periodAction = 'forward';
                $timeCounted = $value['confirm_time'];
                
                $value['acc_limit'] = (!$value['acc_limit']) ? 0 : $value['acc_limit'];
                
                if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                if( $value['id_service_fk'] == 4 || $value['id_service_fk'] == 5) {
                    $tmp['price'] = 0;
                } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                    $value['unit_price_native'] = 0;
                    $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price_native'], 2).'</span>';
                } else {
                    $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                   // $value['acc_limit'] = $value['acc_limit'] * 60;
                    $timeH_l = intval($value['acc_limit']);
                    $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                    $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price_native'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price_native'], 2);
                }
                $tmp['confirm'] = $value['is_confirm'];
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                $tmp['name'] = $value['description'];
                $tmp['action_date'] = $value['action_date'];
                $tmp['employee'] = ($value['type_fk'] == 5) ? $value['name'] : $value['ename'];
                //$tmp['unit_time'] = number_format ($value['unit_time']/60, 2, "," ,  " "); //round($value['unit_time']/60,2);
                $timeH = intval($value['unit_time']/60);
                $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
                
                $tmp['unit_time'] = $timeH.':'.$timeM;
                $tmp['unit_time_h'] = round($value['unit_time']/60,2);
                $tmp['unit_time_min'] = $value['unit_time'];
                if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
                    //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                    if( $value['id_service_fk'] == 4 || $value['id_service_fk'] == 5 ) {
                        $tmp['price'] = 0;
                    } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                        $value['unit_price_native'] = 0;
                        $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price_native'], 2).'</span>';
                    } else {
                        $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                        $value['acc_limit'] = $value['acc_limit'] * 60;
                        $timeH_l = intval($value['acc_limit']/60);
                        $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                        $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price_native'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price_native'], 2);
                    }
                $tmp['amount'] = ($value['type_fk'] == 5) ? $value['unit_price_native'] : round( ($value['unit_price_native']*$timeCounted),2);
                $tmp['service'] = $type;
                //$tmp['description'] = $value['description'].( ($value['cdescription']) ? 'Koszt: '.$value['cdescription'] : '' );
                $tmp['description'] = htmlentities(($value['id_service_fk'] == 4) ? ($value['cdescription']) : ( ($value['description']) ? $value['description'] : $value['cdescription']));
                //$tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="viewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                //$tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                //$tmp['confirm'] = $value['is_confirm'];
                $periodAction = 'forward';
                if(!$value['id_invoice_fk']) {
                    $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="viewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                    $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                    if($periodAction == 'forward')
                        $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                    else
                        $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
                } else {
                    $tmp['order'] = $orderSymbol;
                    $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" title="Czynność rozliczona i powiązana z fakturą"></i>';
                    $tmp['period'] = '';
                }
                
		    	$tmp['cost'] =  ($value['is_confirm'] == 1) ? $value['acc_cost_native'] : ( (!$value['acc_cost_net'] ? '0' : $value['acc_cost_net'].' <small class="text--grey">'.(\backend\Modules\Accounting\models\AccCurrency::getList()[$value['acc_cost_currency_id']]).'</small>'));
                $tmp['actions'] = '<div class="edit-btn-group">';
                    $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                    
                    //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
                $tmp['actions'] .= '</div>';
                //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
                $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $tmp['id'] = $value['id'];
                
                array_push($fields, $tmp); $tmp = [];
            }
       /* } else {
            echo $count;
        }*/
		
		return ['total' => $count,'rows' => $fields,
                'cost' => number_format($costs, 2, "," ,  " "), 
                'time1' => number_format($summary, 2, "," ,  " "), 
                'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 
                'amount' => number_format($totalRow['amount'], 2, "," ,  " "),
                'lump' => number_format($totalRow['lump'], 2, "," ,  " "),
                'all' => number_format($totalRow['all'], 2, "," ,  " ")];
	}
    
    public function actionCaccount($id) {
        $model = $this->findModel($id);
		$model->account = 1;
        $model->firstname = 'Firmowe';
        $model->lastname = 'Konto';
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$infoHtml = '<dt>Status:</dt>'
                             .'<dd><span class="label label-success">aktywne</span></dd>'
                             .'<dt>Login:</dt>'
                             .'<dd>'.$model->login.'</dd>'
                             .'<dt>Utworzono:</dt>'
                             .'<dd>teraz</dd>'
                             .'<dt>Utworzył:</dt>'
                             .'<dd>'.((Yii::$app->user->identity->fullname) ? Yii::$app->user->identity->fullname : Yii::$app->user->identity->username).'</dd>'
                             .'<dd class="crm-accounts-info-peronals">'
                                .'<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$model->accounts['accounts_active'].'</b></span>'
                                .'&nbsp;/&nbsp;'
                                .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$model->accounts['accounts_inactive'].'</b></span></dd>'
                             .'</dl>';

                return ['success' => true, 'action' => 'caccount', 'info' => $infoHtml];	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [ 'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionLock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 0; 
           $user->save();
        }
        $infoHtml = '<span class="label label-'.( ($model->user['status'] == 10) ? 'success' : 'danger' ).'">'.( ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ).'</span>'
                    .' <a href="'.Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]).'" class="btn btn-xs btn-'.( ($model->user['status'] == 10) ? 'danger' : 'success' ).' deleteConfirm" data-label="'.( ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ).'" title="'.( ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ).'">'
                        .(($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ).'</a>';
            
        return array('success' => true, 'alert' => 'Konto zostało zablokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'daccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 10; 
           $user->save();
        }
        $infoHtml = '<span class="label label-'.( ($model->user['status'] == 10) ? 'success' : 'danger' ).'">'.( ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ).'</span>'
                    .' <a href="'.Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]).'" class="btn btn-xs btn-'.( ($model->user['status'] == 10) ? 'danger' : 'success' ).' deleteConfirm" data-label="'.( ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ).'" title="'.( ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ).'">'
                        .(($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ).'</a>';
            
        return array('success' => true, 'alert' => 'Konto zostało odblokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'daccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionCard($id) {
        $model = $this->findModel($id);
        
        return $this->renderPartial('card', ['model' => $model]);
    }
    
    public function actionInvoices($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['AccInvoice'])) { 
            $params = $_GET['AccInvoice']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  
			
		$sortColumn = 'date_issue';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['i.id as id', 'i.system_no as system_no','o.id as oid', 'o.name as oname','o.id_customer_fk as cid', "c.name as cname", " min(date_format(date_issue,'%Y-%m')) as period", "sum(ii.amount_net) as amount_net"])
            ->from('{{%acc_invoice}} as i')
            ->join('JOIN', '{{%acc_invoice_item}} as ii', 'i.id=ii.id_invoice_fk')
            ->join('JOIN', '{{%acc_order}} as o', 'o.id=i.id_order_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id=o.id_customer_fk')
            ->where( ['i.status' => 1] )->andWhere('(o.id_customer_fk = '.$id.' or o.id in (select id_order_fk from {{%acc_order_item}} oi where oi.id_customer_fk = '.$id.'))')
            ->groupby(["i.id", "system_no", "o.id", "o.name", "o.id_customer_fk", "c.name"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/accounting/invoice/view', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd faktury').'" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
            $tmp['symbol'] = $value['system_no'].'<br /><small><a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a></small>'
                            .(($value['cid'] != $id) ? '<br /><small><a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" class="text--pink">'.$value['cname'].'<a></small>': '');
			$tmp['issue'] = $value['period'];
            $tmp['amount_net'] = number_format ($value['amount_net'], 2, "," ,  " ");
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionLimits($id) {
        $model = $this->findModel($id);
		$model->user_action = "limits";
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$infoHtml = '';
                return ['success' => true, 'action' => 'climits', 'limits' => $model->limits];	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formLimits', [ 'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formLimits', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formLimits', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionDlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'debt_lock';
        $model->debt_lock = 1;
 
        $model->save();

        $infoHtml = '<span class="btn btn-sm btn-icon bg-'. ( ($model->debt_lock == 0) ? 'green' : 'red' ) .'">'
                        .'<i class="fa fa-'. ( ($model->debt_lock == 0) ? 'check' : 'exclamation' ) .'"></i>'. ( ($model->debt_lock == 0) ? 'konto aktywne' : 'konto zablokowane' ) 
                    .'</span>&nbsp;'
                    .'<a href="'. Url::to(['/crm/customer/'.(($model->debt_lock == 0) ? 'dlock' : 'dunlock'), 'id' => $model->id]) .'" '
                        .' class="btn btn-sm btn-'.( ($model->debt_lock == 0) ? 'danger' : 'success' ).' deleteConfirm" '
                        .' data-label="'. ( ($model->debt_lock == 0) ? 'Zablokuj konto' : 'Odblokuj konto' ) .'" '
                        .' data-toggle="tooltip" data-title="'. ( ($model->debt_lock == 0) ? 'zablokuj' : 'odblokuj' ) .'"> '
                            . (($model->debt_lock == 0) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>') 
                    .'</a>';
        $legend = '<legend class="'. (($model->debt_lock == 0) ? 'text--navy' : 'text--red' ).'"><i class="fa fa-exclamation-triangle"></i>&nbsp;'. (($model->debt_lock == 0) ? 'Ustawienia windykacji' : 'Blokada windykacyjna' ).'</legend>';   
        return array('success' => true, 'alert' => 'Konto zostało zablokowane', 'id' => $id, 'table' => "#table-customers", 'action' => 'debtLock', 'info' => $infoHtml, 'legend' => $legend);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionDunlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'debt_unlock';
        $model->debt_lock = 0;
        
        $model->save();
        
        $infoHtml = '<span class="btn btn-sm btn-icon bg-'. ( ($model->debt_lock == 0) ? 'green' : 'red' ) .'">'
                        .'<i class="fa fa-'. ( ($model->debt_lock == 0) ? 'check' : 'exclamation' ) .'"></i>'. ( ($model->debt_lock == 0) ? 'konto aktywne' : 'konto zablokowane' ) 
                    .'</span>&nbsp;'
                    .'<a href="'. Url::to(['/crm/customer/'.(($model->debt_lock == 0) ? 'dlock' : 'dunlock'), 'id' => $model->id]) .'" '
                        .' class="btn btn-sm btn-'.( ($model->debt_lock == 0) ? 'danger' : 'success' ).' deleteConfirm" '
                        .' data-label="'. ( ($model->debt_lock == 0) ? 'Zablokuj konto' : 'Odblokuj konto' ) .'" '
                        .' data-toggle="tooltip" data-title="'. ( ($model->debt_lock == 0) ? 'zablokuj' : 'odblokuj' ) .'"> '
                            . (($model->debt_lock == 0) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>') 
                    .'</a>';
        $legend = '<legend class="'. (($model->debt_lock == 0) ? 'text--navy' : 'text--red' ).'"><i class="fa fa-exclamation-triangle"></i>&nbsp;'. (($model->debt_lock == 0) ? 'Ustawienia windykacji' : 'Blokada windykacyjna' ).'</legend>';       
        return array('success' => true, 'alert' => 'Konto zostało odblokowane', 'id' => $id, 'table' => "#table-customers", 'action' => 'debtLock', 'info' => $infoHtml, 'legend' => $legend);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionMultigroup() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setGroup";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->setGroup()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiGroup', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiGroup', ['model' => $model]) ;	
		}
	}
    
    public function actionMultistatus() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setStatus";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->setStatus()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiStatus', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiStatus', ['model' => $model]) ;	
		}
	}
    
    public function actionMultilock() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setLock";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            if($model->validate() && $model->setLock()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiDebt', ['model' => $model, 'lock' => true]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiDebt', ['model' => $model, 'lock' => true]) ;	
		}
	}
    
    public function actionMultiunlock() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setUnock";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            
            if($model->validate() && $model->setUnlock()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers' );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiDebt', ['model' => $model, 'lock' => false]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiDebt', ['model' => $model, 'lock' => false]) ;	
		}
	}
    
    public function actionMultidepartmentsadd() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setDepartments";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->addDepartments()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers', 'add' => true );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiDepartments', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiDepartments', ['model' => $model, 'add' => true]) ;	
		}
	}
    
    public function actionMultidepartmentsdel() {        
		$model = new \frontend\Modules\Crm\models\GroupAction();
		$model->user_action = "setDepartments";
		
		if(empty($_GET['ids'])) {
			return '<div class="alert alert-danger">Proszę wybrać klientów!</div>';
		} else {
			$model->ids = $_GET['ids'];
		}
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

            if($model->validate() && $model->delDepartments()) {                      
                return array('success' => true, 'alert' => 'Zaminy zostały zapisane', 'table' => '#table-customers', 'add' => false );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formMultiDepartments', ['model' => $model]), 'errors' => $model->getErrors() );	
            }     	
		} else {
            return  $this->renderAjax('_formMultiDepartments', ['model' => $model, 'add' => false]) ;	
		}
	}
    
    public function actionOffers($id) {
		//$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['Offer'])) { 
            $params = $_GET['Offer']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  
			
		$sortColumn = 'o.created_at';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['o.id as id', 'o.name as name','o.created_at', 'o.state', "concat_ws(' ', ce.lastname, ce.firstname) as empname", "concat_ws(' ', u.lastname, u.firstname) as creator",
                      "concat_ws(' ', cu.lastname, cu.firstname) as contact", 'o.id as amount_net', 'p.name as pname', 'p.id as pid'])
            ->from('{{%offer}} as o')
            ->join('JOIN', '{{%user}} as u', 'o.created_by=u.id')
            ->join('LEFT JOIN', '{{%company_employee}} as ce', 'o.id_employee_fk=ce.id')
            ->join('LEFT JOIN', '{{%customer_person}} as cu', 'o.id_contact_fk=cu.id')
            ->join('LEFT JOIN', '{{%cal_case}} as p', 'o.id_project_fk=p.id')
            ->where( ['o.status' => 1, 'o.id_customer_fk' => $id] );
            //->groupby(["i.id", "system_no", "o.id", "o.name", "o.id_customer_fk", "c.name"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
        $presentationAll = \backend\Modules\Offer\models\Offer::Viewer();
		foreach($rows as $key=>$value) {
			$presentation = $presentationAll[$value['state']];
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/sale/offer/view', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Podgląd oferty').'" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '</div>';
            
           /* $tmp['symbol'] = $value['system_no'].'<br /><small><a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a></small>'
                            .(($value['cid'] != $id) ? '<br /><small><a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" class="text--pink">'.$value['cname'].'<a></small>': '');*/
			$tmp['name'] = $value['name'];
            if($value['pid'])
                $tmp['name'] .= '<br /><small class="text--purple"><a href="'.Url::to(['/task/project/view', 'id' => $value['pid']]).'">'.$value['pname'].'</a></small>';
            $tmp['employee'] = $value['empname'];
            $tmp['contact'] = $value['contact'];
            $tmp['created'] = $value['created_at'].'<br /><small>'.$value['creator'].'</small>';
            $tmp['amount_net'] = number_format ($value['amount_net'], 2, "," ,  " ");
            $tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'">'.$presentation['label'].'</label>';
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionBranches($id) {
		//$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
      
        $where = [];
        $post = $_GET;
        if(isset($_GET['CustomerBranch'])) { 
            $params = $_GET['CustomerBranch']; 
            if(isset($params['system_no']) && !empty($params['system_no']) ) {
               array_push($where, "lower(system_no) like '%".strtolower($params['system_no'])."%'");
            }
			if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "DATE_FORMAT(date_issue, '%Y-%m') <= '".$params['date_to']."'");
            }
        }  
			
		$sortColumn = 'b.name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'o.symbol';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'system_no' ) $sortColumn = 'i.system_no';
        if( isset($post['sort']) && $post['sort'] == 'issue' ) $sortColumn = 'date_issue';
		
		$query = (new \yii\db\Query())
            ->select(['b.id as id', 'b.name as name','b.created_at', "concat_ws(' ', ce.lastname, ce.firstname) as empname", "b.address", "b.city", "b.postal_code", "b.email", "b.phone"])
            ->from('{{%customer_branch}} as b')
            ->join('LEFT JOIN', '{{%company_employee}} as ce', 'b.id_employee_guardian_fk=ce.id')
            ->where( ['b.status' => 1, 'b.id_customer_fk' => $id] );
            //->groupby(["i.id", "system_no", "o.id", "o.name", "o.id_customer_fk", "c.name"]);
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
		
		$rows = $query->all();
		$fields = [];
		$tmp = [];
        $colors = [-2 => 'red', -1 => 'orange', 0 => 'blue', 1 => 'green'];
		foreach($rows as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/crm/branch/update', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Edycja oddziału').'" class="btn btn-xs btn-default update" data-target="#modal-grid-item" data-label="Aktualizacja"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/crm/branch/delete', 'id' => $value['id']]).'" title="'.Yii::t('app', 'Usunięcie oddziału').'" class="btn btn-xs btn-default remove"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            
           /* $tmp['symbol'] = $value['system_no'].'<br /><small><a href="'.Url::to(['/accounting/order/view', 'id' => $value['oid']]).'">'.$value['oname'].'</a></small>'
                            .(($value['cid'] != $id) ? '<br /><small><a href="'.Url::to(['/crm/customer/view', 'id' => $value['cid']]).'" class="text--pink">'.$value['cname'].'<a></small>': '');*/
			$tmp['name'] = $value['name'];
            $tmp['employee'] = $value['empname'];
            $tmp['email'] = $value['email'];
            $tmp['phone'] = $value['phone'];
            $tmp['contact'] = $value['address'].(($value['city']) ? (', '.(($value['postal_code']) ? ($value['postal_code'].' ') : '').$value['city']) : (($value['postal_code']) ? (', '.$value['postal_code']) : ''));
            //$tmp['created'] = $value['created_at'].'<br /><small>'.$value['creator'].'</small>';
           // $tmp['amount_gross'] = number_format ($value['amount_gross'], 2, "," ,  " ");
           // $tmp['state'] = '<span class="label bg-'.$colors[$value->status].'">'.$states[$value->status].'</span>';
          //  $tmp['className'] = ($value->is_settled == 1) ? 'green' : ( ($value->is_closed == -1) ? 'warning' : 'default' );
            $tmp['actions'] = $actionColumn; //sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionArchive($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'archive';
        $model->is_archive = 1;
        $model->date_archive = date('Y-m-d H:i:s');
        $model->user_archive = Yii::$app->user->id;
 
        $model->save();

        return array('success' => true, 'alert' => 'Klient został przeniesieony do archiwum', 'id' => $id, 'table' => "#table-customers", 'action' => 'crmArchive');	
   
       // return $this->redirect(['index']);
    }
    
    public function actionUnarchive($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->user_action = 'unarchive';
        $model->is_archive = 0;
        //$model->date_archive = date('Y-m-d H:i:s');
        //$model->user_archive = Yii::$app->user->id;
 
        $model->save();

        return array('success' => true, 'alert' => 'Klient został przywrócony do rejestru głównego', 'id' => $id, 'table' => "#table-customers", 'action' => 'crmArchive');	
   
       // return $this->redirect(['index']);
    }
}

