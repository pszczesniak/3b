<?php

namespace app\Modules\Crm\controllers;

use yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `Crm` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
	
	public function actionAutocomplete() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $result = false;
        if(isset($_GET['q'])) {
            $sql = "SELECT id, name FROM {{%customer}} WHERE status = 1 and name LIKE '%".$_GET['q']."%' order by name LIMIT 20"; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
        if(isset($_GET['id'])) {
            $sql = "SELECT id, name FROM {{%customer}} WHERE id = ".$_GET['id']; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
        if(!$result) {
            $sql = "SELECT id, name FROM {{%customer}} WHERE status = 1 and name LIKE '%".$_GET['q']."%' order by name LIMIT 20"; 
            $result = Yii::$app->db->createCommand($sql)->queryAll();
        }
		$data = [];
        if($result) {
            foreach($result as $key => $row){
                $data[] = ['id' => $row['id'], 'text' => $row['name']];
            }
        }
		return $data;
	}
}
