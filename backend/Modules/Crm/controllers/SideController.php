<?php

namespace app\Modules\Crm\controllers;

use Yii;
use backend\Modules\Crm\models\Customer;
use backend\Modules\Crm\models\CustomerArch;
use backend\Modules\Crm\models\CustomerPerson;
use backend\Modules\Crm\models\CustomerSearch;
use backend\Modules\Crm\models\CustomerDepartment;
use backend\Modules\Task\models\CalCase;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * SideController implements the CRUD actions for Customer model.
 */
class SideController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'Customer' => Customer::className()
				]
			],
		];
	}
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( count(array_intersect(["customerPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new CustomerSearch();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.customers');  
            if($params['params']) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = Customer::find()->where(['status' => 1]);
        
        $grants = $this->module->params['grants']; $where = []; $oppositeSide = false; $sideRole = false;
        $post = $_GET;
        if(isset($_GET['CustomerSearch'])) {
            $params = $_GET['CustomerSearch'];
            \Yii::$app->session->set('search.customers', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(c.name) like '%".strtolower( addslashes($params['name']) )."%'");
				array_push($where, "lower(c.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['symbol']) && !empty($params['symbol']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(c.symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
				array_push($where, "lower(c.symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
            }
            if(isset($params['side_character']) && strlen($params['side_character']) ) {
                if($params['side_character'] == 0) {
                    $oppositeSide = true;
                    array_push($where, "cc.id is not null");
                } else {
                    $sideRole = true;
                    array_push($where, "id_dict_side_role_fk = ".$params['side_character']);
                }
            }
            if(isset($params['id_dict_customer_status_id']) && !empty($params['id_dict_customer_status_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
				array_push($where, "id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
            }
            if(isset($params['id_dict_customer_debt_collection_id']) && !empty($params['id_dict_customer_debt_collection_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
				array_push($where, "id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
            }
        }
        
        /*if(count(array_intersect(["grantAll"], $grants)) == 0) {
            $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        }*/
		//$count = $fieldsDataQuery->count();
        
       // $fieldsData = $fieldsDataQuery->orderby('name')->all();
			
		$fields = [];
		$tmp = [];
        
        $grants = $this->module->params['grants'];
		
		$sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'symbol';
        /*if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'unit_time';*/
        
        $query = (new \yii\db\Query())
            ->select(['c.id as id', 'c.name as name', 'c.symbol as symbol', 'phone', 'email', 
                      'min(cc.id) as is_opposite', ' group_concat(distinct dv.name) as role_list'])
            ->from('{{%customer}} c')
            ->join(( ($oppositeSide) ? 'JOIN' : 'LEFT JOIN'), '{{%cal_case}} cc', 'cc.id_opposite_side_fk = c.id')
            ->join('LEFT JOIN', '{{%case_side}} cs', 'cs.id_side_fk = c.id')
            ->join('LEFT JOIN', '{{%dictionary_value}} dv', 'dv.id = cs.id_dict_side_role_fk')
            ->where( ['c.status' => 1, 'c.type_fk' => 2] )
            ->groupby('c.id', 'c.name', 'c.symbol', 'phone', 'email');
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
		      
		foreach($rows as $key=>$value) {
			
			$tmp['name'] = '<a href="'.Url::to(['/crm/side/view','id' => $value['id']]).'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" >'.$value['name'].'</a>';
            $tmp['sname'] = $value['name'];
            $tmp['symbol'] = $value['symbol'];
			$tmp['contact'] = '<div class="btn-icon"><i class="fa fa-phone text--teal"></i>'.(($value['phone']) ? $value['phone'] : '<span class="text--teal2">brak danych</span>').'</div><div class="btn-icon"><i class="fa fa-envelope text--teal"></i>'.(($value['email']) ? $value['email'] : '<span class="text--teal2">brak danych</span>').'</div>';
            $tmp['character'] = ($value['is_opposite']) ? '<span class="text--red">strona przeciwna</span>' : '';
            $tmp['character'] .= ($value['is_opposite'] && $value['role_list']) ? ', ' : '';
            $tmp['character'] .= ($value['role_list']) ? str_replace(',', ', ', $value['role_list']) : '';
            //$tmp['actions'] = sprintf($actionColumn, $value->id, $value->id, $value->id);
            $tmp['actions'] = '<div class="btn-group">'
                                      .'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                        .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                      .'</button>'
                                      .'<ul class="dropdown-menu dropdown-menu-right">'
                                        .'<li><a href="'.Url::to(['/crm/side/view','id' => $value['id']]).'" ><i class="fa fa-eye text--blue"></i>&nbsp;Karta uczestnika</a></li>'
                                        . ( (count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ? '<li><a href="'.Url::to(['/crm/side/update','id' => $value['id']]).'" ><i class="fa fa-pencil text--blue"></i>&nbsp;Edytuj</a></li>' : '')
                                        . ( (count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ? '<li class="remove" data-table="#table-sides" href="'.Url::to(['/crm/side/delete', 'id' => $value['id']]).'" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('app', 'Delete').'"><a href="#"><i class="fa fa-trash text--red"></i>&nbsp;Usuń</a></li>' : '')
                                      .'</ul>';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];

	}

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)   {
        if( count(array_intersect(["customerPanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        
        $departmentsList = ''; $departments = [];
        $filesList = '';
        $personsList = '';
        $grants = $this->module->params['grants'];
        $accPermission = false;
        if( in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) ) $accPermission = true;
        if( isset($this->module->params['employeeKind']) && $this->module->params['employeeKind'] == 100 ) $accPermission = true;
        
        foreach($model->departments as $key => $item) {
            $departmentsList .= '<li>'.$item->department['name'] .'</li>';
            array_push($departments, $item->id_department_fk);
        }
        $model->departments_list = ($departmentsList) ? '<ul>'.$departmentsList.'</ul>' :  'brak danych';
        
        $managerData = \backend\Modules\Company\models\CompanyDepartment::find()->where(['status' => 1, 'id_employee_manager_fk' => $this->module->params['employeeId']])->all();
        foreach($managerData as $key => $value) {
            if( in_array($value->id, $departments) ) {
                $accPermission = true;
            }
        }
        
        return $this->render('view', [
            'model' => $model, 'accPermission' => $accPermission
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
		
		if( count(array_intersect(["customerAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = new Customer();
        $model->type_fk = 2;
        $person = new CustomerPerson();
		$departments = [];
        $grants = $this->module->params['grants'];
        
        $specialDepartments = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->all();
        foreach($specialDepartments as $key => $value) {
            array_push($model->departments_list, $value->id);
        }
		if (Yii::$app->request->isPost ) {
			
			$model->load(Yii::$app->request->post());
            $person->load(Yii::$app->request->post());
            if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
                foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                    array_push($model->departments_list, $value);
                }
            }
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");
			if($model->validate() && $model->save()) {
				if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
					//EmployeeDepartment::deleteAll('id_employee_fk = :offer', [':offer' => $id]);
                    foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                        $model_cd = new CustomerDepartment();
                        $model_cd->id_customer_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} 
               
                $person->id_customer_fk = $model->id;
                $person->save();
				return $this->redirect(['view', 'id' => $model->id]);
				//return array('success' => true, 'alert' => 'Klient <b>'.$model->name.'</b> został zapisany' );	
			} else {
                //return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'departments' => $departments, 'person' => $person]), 'errors' => $model->getErrors() );	
                return  $this->render('create', [  'model' => $model, 'departments' => $departments, 'person' => $person]) ;
			}		
		} else {
			return  $this->render('create', [  'model' => $model, 'departments' => $departments, 'person' => $person]) ;	
		}
	}

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        if( count(array_intersect(["customerEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        /*if(!$model->person) {
            $person = new CustomerPerson();
            $person->id_customer_fk = $id;
            $person->save();
        }*/
        $departments = [];
        $grants = $this->module->params['grants'];
        foreach($model->departments as $key => $item) {
            array_push($departments, $item->id_department_fk);
            array_push($model->departments_list, $item->id_department_fk);
        }
        $filesList = '';
       /* foreach($model->files as $key => $item) {
            $filesList .= '<li><a href="/files/getfile/'.$item->id.'">'.$item->title_file .'</a> ['.$item->created_at.'] </li>';
        }
        $model->files_list = ($filesList) ? '<ul class="files-set">'.$filesList.'</ul>' :  '<ul class="files-set"></ul>';*/

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           /* if( isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
                foreach($_POST['Customer']['departments_list'] as $key => $value) {
                   // array_push($model->departments_rel, $value);
                    array_push($model->departments_list, $value);
                }
            } */
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");
			if($model->validate() && $model->save()) {
				
				/*CustomerDepartment::deleteAll('id_customer_fk = :customer', [':customer' => $id]);
				if(isset($_POST['Customer']['departments_list']) && !empty($_POST['Customer']['departments_list']) ) {
					
                    foreach($_POST['Customer']['departments_list'] as $key=>$value) {
                        $model_cd = new CustomerDepartment();
                        $model_cd->id_customer_fk = $model->id;
                        $model_cd->id_department_fk = $value;
                        $model_cd->save();
                    }
				} */
				
				return array('success' => true,  'alert' => 'Dane klienta <b>'.$model->name.'</b> zostały zaktualizowane.' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', [  'model' => $model, 'departments' => $departments]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->render('update', [  'model' => $model, 'departments' => $departments]) ;	
		}
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTask::SCENARIO_DELETE;
        
        $cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $model->id])->count();
        if($cases > 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Klient <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane aktywne postępowania')  );
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Klient <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ ma przypisane aktywne postępowania', 'id' => $id, 'table' => '#table-customers');	
			}
        }
        
        $model->status = -1;
        $model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Klient <b>'.$model->name.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Klient <b>'.$model->name.'</b> został usunięty', 'id' => $id, 'table' => '#table-customers');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Klient <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Klient <b>'.$model->name.'</b> nie został usunięty', 'id' => $id, 'table' => '#table-customers');	
			}
        }
        
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
        //$id = base64_decode($id);
		/*$key = 'szyfr';
		//$id = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($id), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		$data = str_replace(array('-','_'),array('+','/'),$id);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
		$id = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($data), MCRYPT_MODE_CBC, md5(md5($key))), "\0");*/
		$id = CustomHelpers::decode($id);
		////$decrypted_id_raw = base64_decode($id);
		////$decrypted_id = preg_replace(sprintf('/%s/', $key), '', $decrypted_id_raw);
		if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Klient o wskazanym identyfikatorze nie został znaleziony w systemie.');
        }
    }
    public function actionPersons($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$persons = [];
		$tmp = [];
		
		$model = $this->findModel($id);
		$personsData = $model->persons;
		
		foreach($personsData as $key=>$value) {
			
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/crm/person/update', 'id' => $value->id]).'" class="btn btn-xs btn-success gridViewModal" data-table="#table-persons" data-form="person-form" data-target="#modal-grid-person" data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" ><i class="fa fa-pencil"></i></a>';

            $actionColumn .= '<a href="'.Url::to(['/crm/person/delete', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-persons"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
            $tmp = [];
			$tmp['id'] = $value->id;
			$tmp['firstname'] = $value->firstname;
			$tmp['lastname'] = $value->lastname;
			$tmp['position'] = $value->position;
			$value->phone = $value->phone?$value->phone:'brak';
			$value->email = $value->email?$value->email:'brak';
            $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$value->id."/preview.jpg"))?"/uploads/customers/contacts/cover/".$value->id."/preview.jpg":"/images/default-user.png";
            $tmp['image'] = '<img alt="avatar" width="30" height="30" src="'.$avatar.'" title="avatar"></img>';
			$tmp['contact'] = '<div class="btn-icon"><i class="glyphicon glyphicon-phone"></i>'.$value->phone.'</div><div class="btn-icon"><i class="glyphicon glyphicon-envelope"></i>'.$value->email.'</div>';
			$tmp['actions'] = $actionColumn;
            if($value->user) {
                $userActive = ($value->user['status'] == 10) ? '<a href="'.Url::to(['/crm/person/lock', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-label="Zablokuj konto" data-table="#table-persons">Zablokuj</a>' 
                                                             : '<a href="'.Url::to(['/crm/person/unlock', 'id' => $value->id]).'" class="btn btn-xs bg-teal remove" data-label="Odblokuj konto" data-table="#table-persons">Odblokuj</a>';
            }
            $tmp['user'] = (!$value->id_user_fk) ? 
							'<a href="'.Url::to(['/crm/person/caccount', 'id' => $value->id]).'" class="gridViewModal" title="Utwórz konto" data-table="#table-persons" data-form="person-form" data-target="#modal-grid-person"><i class="fa fa-user-plus text--green"></i></a>' :
							'<a href="'.Url::to(['/crm/person/uaccount', 'id' => $value->id]).'" class="gridViewModal" title="Edytuj konto" data-table="#table-persons" data-form="person-form" data-target="#modal-grid-person"><i class="fa fa-user text--blue"></i></a><br />'.$userActive;
			//array_push($tmp, '<span class="drag-handle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>');
			
			array_push($persons, $tmp); $tmp = [];
		}
		
		return $persons;
	}
    
    public function actionExport() {
		
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Klienci');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Nazwa')
			->setCellValue('B3', 'Adres')
			->setCellValue('C3', 'NIP')
			->setCellValue('D3', 'REGON');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('B3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->getStartColor()->setARGB('D9D9D9');
 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('C3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4; 
		$data = [];

        $fieldsDataQuery = Customer::find()->where(['status' => 1, 'type_fk' => 2]);
        
        if(isset($_GET['CustomerSearch'])) {
            $params = $_GET['CustomerSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_customer_fk from law_customer_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_customer_status_id']) && !empty($params['id_dict_customer_status_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
            }
            if(isset($params['id_dict_customer_debt_collection_id']) && !empty($params['id_dict_customer_debt_collection_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
            }
        }

        if(count(array_intersect(["grantAll"], $this->module->params['grants'])) == 0) {
            $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        }
            
        $data = $fieldsDataQuery->all();
			
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->name ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record->address); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->nip); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->regon); 
                $employees = '';
                /*foreach($record->employees as $key => $employee) {
                    $employees .= $employee->employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);*/
						
				$i++; 
			}  
		}
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->setTitle('Klienci');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Klienci'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionCases($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = '';
	
        //$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $id = (!$id || empty($id)) ? 0 : $id;
        $cases = \backend\Modules\Task\models\CalCase::getList($id);
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        $list .= '<option value=""> - wybierz - </option>';
        if( count($matters) > 0 ) {
            $list .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        return ['list' => $list];
    }
    
    public function actionRelationship($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $listCase = '';
		$listOrder = '';
        
        //$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $id = (!$id || empty($id)) ? 0 : $id;
        //$cases = \backend\Modules\Task\models\CalCase::getList($id);
        $employeeId = $this->module->params['employeeId'];
        if( isset($_GET['eid']) && !empty($_GET['eid']) ) $employeeId = $_GET['eid'];  
        $cases = CalCase::find()->where(['status' => 1, 'id_customer_fk' => $id])->andWhere('id_dict_case_status_fk != 6')->andWhere('id in (select id_case_fk from {{%case_employee}} where status = 1 and id_employee_fk = '.$employeeId.')')->orderby('name')->all();
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        $listCase .= '<option value=""> - wybierz - </option>';
        if( count($matters) > 0 ) {
            $listCase .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $listCase .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $listCase .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $listCase .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        if($id == 0) {
            $orders = \backend\Modules\Accounting\models\AccOrder::getListAll();
            $listOrder .= '<option value=""> - wybierz - </option>';
            foreach($orders as $key => $value) {
                $listOrder .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
            }
        } else {
            $orders = \backend\Modules\Accounting\models\AccOrder::getList($id);
            $listOrder .= '<option value=""> - wybierz - </option>';
            foreach($orders as $key => $value) {
                $listOrder .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }
        
        return ['listCase' => $listCase, 'listOrder' => $listOrder];
    }
    
    public function actionLists($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $specialEmployees = [];
        if(isset($_GET['type']) && $_GET['type'] == 'matter' ) {
            $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
            foreach($specialEmployeesSql as $key => $value) {
                array_push($specialEmployees, $value->id_employee_fk);
            }
        }
        
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();       
        $model = Customer::findOne($id);
        $departments = ''; $ids = []; array_push($ids, 0); $employees_list = ''; $list_cases = '';
        $checked = 'checked';
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        foreach($model->departments as $key => $value) {
            if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                    
                array_push($ids, $value->id_department_fk);
            } else {
                if(in_array($value->id_department_fk, $this->module->params['departments'])) {                
                    //$departments .= '<li><input class="checkbox_department" id="d-'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.'><label for="d'.$value->id_department_fk.'">'.$value->department['name'].'</label></li>';
                    $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                    
                    array_push($ids, $value->id_department_fk);
                }
            }
        }
        
        if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin" || $admin > 0) {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
        } else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        }
        foreach($employees as $key => $model) {
            if($model->id == $this->module->params['employeeId'] || in_array($model->id, $specialEmployees) )  $checked = 'checked'; else $checked = '';
            $employees_list .= '<li><input class="checkbox_employee" id="d'.$model->id.'" type="checkbox" name="employees[]" value="'.$model->id.'" '.$checked.'><label for="d'.$model->id.'">'.$model->fullname.'</label></li>';
        }
        
        $list = '';
        
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($contacts as $key => $model) {
            $list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
		
		//$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $cases = \backend\Modules\Task\models\CalCase::getList($id);
        $list_cases .= '<option value=""> - wybierz - </option>';
        /*foreach($cases as $key => $model) {
            $list_cases .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }*/
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        if( count($matters) > 0 ) {
            $list_cases .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list_cases .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        $ordersList = '';
        $orders = \backend\Modules\Accounting\models\AccOrder::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('name')->all();
        $ordersList .= '<option value=""> - wybierz - </option>';
        foreach($orders as $key => $model) {
            $ordersList .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }
        
        return ['contacts' => $list, 'departments' => $departments, 'employees' => $employees_list, 'cases' => $list_cases, 'orders' => $ordersList];
    }
    
    public function actionMlists($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $specialEmployees = [];
        if(isset($_GET['type']) && $_GET['type'] == 'matter' ) {
            $specialEmployeesSql = \backend\Modules\Company\models\EmployeeDepartment::find()->where('id_department_fk in (select id from {{%company_department}} where all_employee = 1)')->all();
            foreach($specialEmployeesSql as $key => $value) {
                array_push($specialEmployees, $value->id_employee_fk);
            }
        }
        if( empty($id) ) $id = 0;
        $employee = \backend\Modules\Company\models\CompanyEmployee::find()->where(['id_user_fk' => \Yii::$app->user->id])->one();       
        $model = Customer::findOne($id);
        $departments = ''; $ids = []; array_push($ids, 0); $employees_list = ''; $list_cases = '';
        $checked = 'checked';
        $admin = 0;
        if($employee)
            $admin = \backend\Modules\Company\models\CompanyDepartment::find()->where(['all_employee' => 1])->andWhere('id in (select id_department_fk from {{%employee_department}} where id_employee_fk = '.$employee->id.')')->count();
        
        if($model) {
            foreach($model->departments as $key => $value) {
                if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin") {
                    $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                        
                    array_push($ids, $value->id_department_fk);
                } else {
                    if(in_array($value->id_department_fk, $this->module->params['departments'])) {                
                        //$departments .= '<li><input class="checkbox_department" id="d-'.$value->id_department_fk.'" type="checkbox" name="departments[]" value="'.$value->id_department_fk.'" '.$checked.'><label for="d'.$value->id_department_fk.'">'.$value->department['name'].'</label></li>';
                        $departments .= '<option value="'.$value->id_department_fk.'" selected>'.$value->department['name'].'</option>';
                        
                        array_push($ids, $value->id_department_fk);
                    }
                }
            }
        }
        if( ($employee && $employee->is_admin) || \Yii::$app->user->identity->username == "superadmin" || $admin > 0) {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();        
        } else {
            $employees = \backend\Modules\Company\models\CompanyEmployee::find()->andWhere('(id_dict_employee_kind_fk < '.$employee->id_dict_employee_kind_fk.' or id = '.$employee->id.')')->andWhere('id in (select id_employee_fk from {{%employee_department}} where id_department_fk in ('.implode(',',$ids).'))')->orderby('lastname')->all();
        }
        foreach($employees as $key => $emodel) {
            if($emodel->id == $this->module->params['employeeId'] || in_array($emodel->id, $specialEmployees) )  $checked = 'checked'; else $checked = '';
            $employees_list .= '<li><input class="checkbox_employee" id="d'.$emodel->id.'" type="checkbox" name="employees[]" value="'.$emodel->id.'" '.$checked.'><label for="d'.$emodel->id.'">'.$emodel->fullname.'</label></li>';
        }
        
        $list = '';
        
        $contacts = \backend\Modules\Crm\models\CustomerPerson::find()->where(['id_customer_fk' => $id, 'status' => 1])->orderby('lastname')->all();
        $list .= '<option value=""> - wybierz - </option>';
        foreach($contacts as $key => $model) {
            $list .= '<option value="'.$model->id.'">'.$model->fullname.'</option>';
        }
		
		//$cases = \backend\Modules\Task\models\CalCase::find()->where(['id_customer_fk' => $id])->orderby('name')->all();
        $cases = \backend\Modules\Task\models\CalCase::getList($id);
        //$list_cases .= '<option value=""> - wybierz - </option>';
        /*foreach($cases as $key => $model) {
            $list_cases .= '<option value="'.$model->id.'">'.$model->name.'</option>';
        }*/
        $matters = [];
        $projects = [];
        foreach($cases as $key => $value) {
            if($value->type_fk == 1) 
                $matters[$value->id] = $value->name;
            else
                $projects[$value->id] = $value->name;
        }
        
        if( count($matters) > 0 ) {
            $list_cases .= '<optgroup label="Sprawy">';
            foreach($matters as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        if( count($projects) > 0 ) {
            $list_cases .= '<optgroup label="Projekty">';
            foreach($projects as $key => $value) {
                $list_cases .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        
        return ['contacts' => $list, 'departments' => $departments, 'employees' => $employees_list, 'cases' => $list_cases];
    }
    
    public function actionTcases($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];

		$fieldsDataQuery = $fieldsData = \backend\Modules\Task\models\CalCase::find()->where(['status' => 1, 'id_customer_fk' => CustomHelpers::decode($id)]);
        //$fieldsDataQuery = $fieldsDataQuery->where(['id_customer_fk' => CustomHelpers::decode($id)]);
           
        $fieldsData = $fieldsDataQuery->orderby('name')->all();
			
			
		$fields = [];
		$tmp = [];
        $status = \backend\Modules\Task\models\CalCase::listStatus('advanced');
		      
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green', 6 => 'bg-pink'];
		foreach($fieldsData as $key=>$value) {
			$type = ( $value->type_fk == 1) ? 'matter' : 'project';
			$tmp['customer'] = $value->customer['name'];
            $tmp['type'] = ($value->type_fk == 1) ? '<span class="label bg-purple">Sprawa</span>' : '<span class="label bg-teal">Projekt</span>' ;
			$tmp['name'] = '<a href="'.Url::to(['/task/'.(($value->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $value->id]).'" class="update" data-table="#table-data" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'"'.Yii::t('lsdd', 'View').'" >'.$value->name.'</a>';
            $tmp['status'] = '<label class="label '.( ($value['status'] == 1) ? $colors[$value['id_dict_case_status_fk']] : 'bg-red').'">'.( ($value['status'] == 1) ? $status[$value['id_dict_case_status_fk']] : 'Usunięta').'</label>';
			$tmp['actions'] = '<div class="edit-btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/task/'.(($value->type_fk == 1) ? 'matter' : 'project').'/view', 'id' => $value->id]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';
			$tmp['actions'] .= '</div>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionTimeline($id) {
        $model = Customer::findOne($id);
        $arch = CustomerArch::find()->where(['id_root_fk' => $id, 'table_fk' => 1])->orderby('id desc')->all();
        
        return $this->render('_timeline', ['model' => $model, 'arch' => $arch]);
    }
    
    public function actionCreateajax() {
		
        $model = new Customer();
        $model->type_fk = 2;
		$model->role_fk = 1;
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->type_fk = 2;
            $model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");       
            if($model->validate() && $model->save()) {
           
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}
    
    public function actionOrders($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants'];
        
        $id = CustomHelpers::decode($id);
        
		$fieldsDataQuery = $fieldsData = \backend\Modules\Accounting\models\AccOrder::find()->where(['status' => 1])->andWhere( '(id_customer_fk = '.$id.' or id in (select id_order_fk from {{%acc_order_item}} where status = 1 and id_customer_fk = '.$id.'))');       
        $fieldsData = $fieldsDataQuery->orderby('id desc')->all();
		
		$fields = [];
		$tmp = [];
        $status = \backend\Modules\Task\models\CalCase::listStatus('normal');
		      
		$colors = [1 => 'bg-blue', 2 => 'bg-orange', 3 => 'bg-purple', 4 => 'bg-green'];
		foreach($fieldsData as $key=>$value) {
			$invoices = $value->invoices;
            $label = ( (count($invoices) == 1) ? 'faktura' : ( ( count($invoices) >= 2 && count($invoices) <= 4) ? 'faktury' : 'faktur') );
            
            $type = ( $value->type_fk == 1) ? 'matter' : 'project';
			$tmp['name'] = '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'">'.$value->name.'</a>'. ' <small class="text--purple">['.$value->customer['name'].']</small>';
            $tmp['start'] = $value->date_from;
            $tmp['creator'] = $value->creator['fullname'];
            $tmp['method'] = '<span class="label" style="background-color:'.$value->type['type_color'].'" title="'.$value->type['type_name'].'">'.$value->type['type_symbol'].'</span>';
            $tmp['merging'] = ($value->type_fk == 1) ? '<i class="fa fa-user text--purple"></i>' : '<i class="fa fa-users text--teal"></i>' ;
            $tmp['invoice'] = (count($invoices) == 0) ? count($invoices) : '<a href="'.Url::to(['/accounting/order/view', 'id' => $value->id]).'" title="Pokaż szczegóły rozliczenia">'.count($invoices).'</a>';
			$tmp['actions'] = '<div class="edit-btn-group">';
				if($id != $value->id_customer_fk) {
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/view', 'id' => $value->id]).'" class="btn btn-xs btn-default"  title="'.Yii::t('lsdd', 'View').'" ><i class="fa fa-eye"></i></a>';                
                } else {
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/view', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-pencil\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('lsdd', 'Update').'" ><i class="fa fa-pencil"></i></a>';
                    $tmp['actions'] .= ( ($value->id_dict_order_type_fk == 2 || $value->id_dict_order_type_fk == 4) ? '<a href="'.Url::to(['/accounting/order/rates', 'id' => $value['id']]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-cog\'></i>'.Yii::t('lsdd', 'Stawki').'" title="'.Yii::t('lsdd', 'Stawki').'" ><i class="fa fa-cog"></i></a>' : '');
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/discounts', 'id' => $value->id]).'" class="btn btn-xs btn-default gridViewModal" data-table="#table-orders" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-percent\'></i>'.Yii::t('lsdd', 'Rabaty').'" title="'.Yii::t('lsdd', 'Rabaty').'" ><i class="fa fa-percent"></i></a>';                    
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/order/deleteajax', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-orders"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
                }
            $tmp['actions'] .= '</div>';
            $tmp['className'] = ($value->date_to) ? 'danger' : 'normal';
			$tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;
            $tmp['currency'] = $value->currency;
            $tmp['files'] =  '<span class="'.((!$value->files) ? 'text--grey' : 'bg-blue label').'">'.count($value->files) .'</span>';
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
    
    public function actionActions($id) {
		$id = CustomHelpers::decode($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $fieldsData = []; $fields = []; $where = [];
        
        //echo $_POST;
        $post = $_GET;//\yii\helpers\Json::encode($_POST);
        if(isset($post['AccActionsSearch'])) {
            $params = $post['AccActionsSearch']; 
            if(isset($params['description']) && !empty($params['description']) ) {
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
                array_push($where, "id_employee_fk = ".$params['id_employee_fk']);
            }
            if(isset($params['id_set_fk']) && !empty($params['id_set_fk']) ) {
                array_push($where, "id_set_fk = ".$params['id_set_fk']);
            }
            if(isset($params['date_from']) && !empty($params['date_from']) ) {
                array_push($where, "action_date >= '".$params['date_from']."'");
            }
            if(isset($params['date_to']) && !empty($params['date_to']) ) {
                array_push($where, "action_date <= '".$params['date_to']."'");
            }
            if(isset($params['acc_period']) && !empty($params['acc_period']) ) {
                array_push($where, "acc_period = '".$params['acc_period']."'");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
                //$fieldsDataQuery = $fieldsDataQuery->andWhere("type_fk = ".$params['type_fk']);
                array_push($where, "a.type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_order_fk']) && !empty($params['id_order_fk']) ) {
                //array_push($where, "id_dict_order_type_fk = ".$params['id_order_fk']);
                array_push($where, "id_order_fk = ".$params['id_order_fk']);
            }
            if(isset($params['invoiced']) && !empty($params['invoiced']) ) {
                if($params['invoiced'] == 1) array_push($where, "is_confirm = 1");
				if($params['invoiced'] == 2) array_push($where, "is_confirm = 0");
            }
        } 
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) > 0 ) {
            array_push($where, "a.id_department_fk in (".implode(',', $this->module->params['managers']).")");
        }
        
        if( !in_array(Yii::$app->user->id, \Yii::$app->params['accEmployees']) && count($this->module->params['managers']) == 0 ) {
            if(\Yii::$app->session->get('user.isManager') == 0)
                array_push($where, "a.id_employee_fk = ".$this->module->params['employeeId']."");
        }
        
        $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'lastname '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date ';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name '.' collate `utf8_polish_ci` ';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'confirm_time ';
        if( isset($post['sort']) && $post['sort'] == 'unit_price' ) $sortColumn = 'confirm_price ';
        if( isset($post['sort']) && $post['sort'] == 'in_limit' ) $sortColumn = 'acc_limit ';
        
        $query = (new \yii\db\Query())
            ->select(["a.id as id", "a.type_fk as type_fk", "a.id_service_fk as id_service_fk", "a.name as name", "a.description as description", "a.acc_cost_description as cdescription",
                      "a.id_invoice_fk as id_invoice_fk", 'is_confirm', "action_date", "acc_period", "unit_time", "unit_price", "confirm_price", "acc_limit", "confirm_time", 
                      "concat_ws(' ', e.lastname, e.firstname) as ename", "id_dict_order_type_fk", "type_symbol", "type_name", "type_color", "acc_cost_net", "o.name as oname",
                      "(select count(*) from {{%acc_note}} where type_fk = 1 and id_fk=a.id) as notes"])
            ->from('{{%acc_actions}} a')
            ->join('JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1])->andWhere( '(a.id_customer_fk = '.$id.' or  a.id_customer_fk in (select aoi.id_customer_fk from {{%acc_order_item}} aoi join {{%acc_order}} ao on aoi.id_order_fk=ao.id where ao.id_customer_fk = '.$id.'))');
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ', $where));
        }

        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
        //echo $query->createCommand()->sql; exit;
        $queryTotal = (new \yii\db\Query())
            ->select(["count(*) as total_rows", "sum(round(unit_time/60,2)) as unit_time", 
                      "sum(case when is_confirm=1 then acc_cost_net else 0 end) as costs",
                      "sum(case when is_confirm=1 then confirm_time else 0 end) as confirm_time", 
                      "sum(case when acc_limit=0 then (confirm_time*unit_price) else ((confirm_time-acc_limit)*unit_price) end) as amount"])->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1])->andWhere( '(a.id_customer_fk = '.$id.' or  a.id_customer_fk in (select id_customer_fk from {{%acc_order_item}} where id_order_fk = o.id))');
            
        $queryOrders = (new \yii\db\Query())
            ->select(["a.id_order_fk", "min(rate_constatnt) as rate_constatnt"])
            ->from('{{%acc_actions}} as a')
            ->join('JOIN', '{{%company_employee}}  as e', 'e.id=a.id_employee_fk')
            ->join('JOIN', '{{%customer}} as c', 'c.id = a.id_customer_fk')
            ->join('LEFT JOIN', '{{%acc_order}} as o', 'o.id = a.id_order_fk')
            ->join('LEFT JOIN', '{{%acc_type}} as t', 't.id = o.id_dict_order_type_fk')
            ->where( ['a.status' => 1, 'a.is_confirm' => 1, 'a.id_customer_fk' => $id] )
            ->groupby(['a.id_order_fk']);
       
       if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
            $queryTotal = $queryTotal->andWhere(implode(' and ',$where));
            $queryOrders = $queryOrders->andWhere(implode(' and ',$where));
        }
		$totalRow = $queryTotal->one(); $count = $totalRow['total_rows']; $summary = $totalRow['unit_time']; $costs = $totalRow['costs'];
        
        $totalRow['lump'] = 0; $totalRow['all'] = $costs+$totalRow['amount']; 
        $totalOrders = $queryOrders->all(); 
        foreach($totalOrders as $io => $order) {
            $totalRow['lump'] += $order['rate_constatnt'];
        }
        $totalRow['all'] += $totalRow['lump'];
        
        /*$query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );*/
        
       // if( $count <= 4000 ) {
           // $fieldsData = $fieldsDataQuery->orderby('action_date desc')->all();
            $tmp = [];
            $dict = \backend\Modules\Accounting\models\AccService::dictTypes();
            foreach($rows as $key=>$value) {
                $tmpType = $dict[$value['type_fk']];
                $type = '<i class="fa fa-'.$tmpType['icon'].' text--'.$tmpType['color'].' cursor-pointer" title="'.$tmpType['name'].'"></i>';
                $orderSymbol = ($value['id_dict_order_type_fk']) ? '<span class="label" style="background-color:'.$value['type_color'].'" title="Kliknij aby oznaczyć czynność">'.$value['oname'].'</span>' : '<span class="label bg-green" title="Przypisz rozliczenie"><i class="fa fa-plus"></i></span>';
                
                $periodAction = 'forward';
                $timeCounted = $value['confirm_time'];
                
                $value['acc_limit'] = (!$value['acc_limit']) ? 0 : $value['acc_limit'];
                
                if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                if( $value['id_service_fk'] == 4 ) {
                    $tmp['price'] = 0;
                } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                    $value['unit_price'] = 0;
                    $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price'], 2).'</span>';
                } else {
                    $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                   // $value['acc_limit'] = $value['acc_limit'] * 60;
                    $timeH_l = intval($value['acc_limit']);
                    $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                    $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price'], 2);
                }
                $tmp['confirm'] = $value['is_confirm'];
                //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                $tmp['name'] = $value['description'];
                $tmp['action_date'] = $value['action_date'];
                $tmp['employee'] = $value['ename'];
                //$tmp['unit_time'] = number_format ($value['unit_time']/60, 2, "," ,  " "); //round($value['unit_time']/60,2);
                $timeH = intval($value['unit_time']/60);
                $timeM = $value['unit_time'] - ($timeH*60); $timeM = ($timeM < 10) ? '0'.$timeM : $timeM;
                
                $tmp['unit_time'] = $timeH.':'.$timeM;
                $tmp['unit_time_h'] = round($value['unit_time']/60,2);
                $tmp['unit_time_min'] = $value['unit_time'];
                if( strtotime($value['acc_period'].'-01') > strtotime($value['action_date']) ) $periodAction = 'backward';
                    //$status = ($value->user['status'] == 10) ? 'lock' : 'unlock';
                    if( $value['id_service_fk'] == 4 ) {
                        $tmp['price'] = 0;
                    } else if($value['confirm_time'] == $value['acc_limit'] && $value['is_confirm'] == 1) { 
                        $value['unit_price'] = 0;
                        $tmp['price'] = '<span class="label bg-pink">'.round($value['unit_price'], 2).'</span>';
                    } else {
                        $timeCounted = $value['confirm_time'] - $value['acc_limit'];
                        $value['acc_limit'] = $value['acc_limit'] * 60;
                        $timeH_l = intval($value['acc_limit']/60);
                        $timeM_l = $value['acc_limit'] - ($timeH_l*60); $timeM_l = ($timeM_l < 10) ? '0'.$timeM_l : $timeM_l;
                        $tmp['price'] = ($value['acc_limit'] > 0) ? '<span class="label bg-purple"><small>'.round($value['unit_price'], 2).' ['.($timeH_l.':'.$timeM_l).']</small></span>'  : round($value['unit_price'], 2);
                    }
                $tmp['amount'] = round( ($value['unit_price']*$timeCounted),2);
                $tmp['service'] = $type;
                //$tmp['description'] = $value['description'].( ($value['cdescription']) ? 'Koszt: '.$value['cdescription'] : '' );
                $tmp['description'] = htmlentities(($value['id_service_fk'] == 4) ? ($value['cdescription']) : $value['description']);
                //$tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                //$tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                //$tmp['confirm'] = $value['is_confirm'];
                $periodAction = 'forward';
                if(!$value['id_invoice_fk']) {
                    $tmp['order'] = '<a href="'.Url::to(['/accounting/action/settle', 'id' => $value['id']]).'" class="gridViewModal" data-table="#table-actions" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Rozliczenie" >'.$orderSymbol.'</a>';
                    $tmp['invoice'] = ($value['is_confirm'] == 1) ? '<i class="fa fa-check text--green"></i>' : '<i class="fa fa-close text--orange"></i>';
                    if($periodAction == 'forward')
                        $tmp['period'] = '<a href="'.Url::to(['/accounting/action/forward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na następny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na następny miesiąc" ><i class="fa fa-step-forward"></i></a>';
                    else
                        $tmp['period'] = '<a href="'.Url::to(['/accounting/action/backward', 'id' => $value['id']]).'" class="deleteConfirm text--navy" data-table="#table-actions" data-label="Potwierdź" data-info="Czy na pewno chcesz przenieść czynność na pierwotny okres rozliczeniowy?" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-calculator\'></i>Przenieś na pierwotny miesiąc" ><i class="fa fa-step-backward"></i></a>';
                } else {
                    $tmp['order'] = $orderSymbol;
                    $tmp['invoice'] = '<i class="fa fa-calculator text--green cursor-pointer" title="Czynność rozliczona i powiązana z fakturą"></i>';
                    $tmp['period'] = '';
                }
                
                $tmp['cost'] = $value['acc_cost_net'];
                $tmp['actions'] = '<div class="edit-btn-group">';
                    $tmp['actions'] .= ( (!$value['id_invoice_fk']) ? '<a href="'.Url::to(['/accounting/action/change', 'id' => $value['id']]).'" class="btn btn-sm btn-default update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('app', 'Update').'" title="'.Yii::t('app', 'Update').'"><i class="fa fa-pencil"></i></a>' : '');
                    $tmp['actions'] .= '<a href="'.Url::to(['/accounting/action/chat', 'id' => $value['id']]).'" class="btn btn-xs bg-'.( ($value['notes']==0) ? 'lightgrey' : 'blue').' update" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-comments\'></i>'.Yii::t('app', 'Notatki').'" title="'.Yii::t('app', 'Dodaj notatkę').'"><i class="fa fa-comment"></i></a>';
                    
                    //$tmp['actions'] .= ( $value->created_by == \Yii::$app->user->id ) ? '<a href="'.Url::to(['/community/meeting/delete', 'id' => $value->id]).'" class="btn btn-sm btn-default " data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-bamn\'></i>'.Yii::t('app', 'Odwołaj').'" title="'.Yii::t('app', 'Odwołaj').'"><i class="fa fa-ban text--red"></i></a>': '';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/deleteajax', 'id' => $value->id]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
                    //$tmp['actions'] .= ( count(array_intersect(["employeeDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/company/employee/'.$status.'', 'id' => $value->id]).'" class="btn btn-sm btn-default '.$status.'" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'fa fa-'.$status.'\'></i>'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'" title="'.( ($status == 'lock') ? 'Zablokuj' : 'Odblokuj' ).'"><i class="fa fa-'.$status.'"></i></a>':'';
                $tmp['actions'] .= '</div>';
                //$tmp['actions'] = ($value->user['status'] == 10) ? sprintf($actionColumn, $value->id, $value->id, $value->id, 'lock', $value->id, 'lock', 'lock', 'Zablokuj', 'Zablokuj', 'Zablokuj', '<i class="fa fa-lock"></i>') : sprintf($actionColumn, $value->id, $value->id, $value->id, 'unlock', $value->id, 'unlock',  'unlock', 'Odblokuj', 'Odblokuj', 'Odblokuj', '<i class="fa fa-unlock"></i>');
                $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
                $tmp['id'] = $value['id'];
                
                array_push($fields, $tmp); $tmp = [];
            }
       /* } else {
            echo $count;
        }*/
		
		return ['total' => $count,'rows' => $fields,
                'cost' => number_format($costs, 2, "," ,  " "), 
                'time1' => number_format($summary, 2, "," ,  " "), 
                'time2' => number_format($totalRow['confirm_time'], 2, "," ,  " "), 
                'amount' => number_format($totalRow['amount'], 2, "," ,  " "),
                'lump' => number_format($totalRow['lump'], 2, "," ,  " "),
                'all' => number_format($totalRow['all'], 2, "," ,  " ")];
	}
    
    public function actionCaccount($id) {
        $model = $this->findModel($id);
		$model->account = 1;
        $model->firstname = 'Firmowe';
        $model->lastname = 'Konto';
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				$infoHtml = '<dt>Status:</dt>'
                             .'<dd><span class="label label-success">aktywne</span></dd>'
                             .'<dt>Login:</dt>'
                             .'<dd>'.$model->login.'</dd>'
                             .'<dt>Utworzono:</dt>'
                             .'<dd>teraz</dd>'
                             .'<dt>Utworzył:</dt>'
                             .'<dd>'.((Yii::$app->user->identity->fullname) ? Yii::$app->user->identity->fullname : Yii::$app->user->identity->username).'</dd>'
                             .'<dd class="crm-accounts-info-peronals">'
                                .'<span class="text--blue cursor-pointer crm-personal-account-active" title="konta aktywne"><b>'.$model->accounts['accounts_active'].'</b></span>'
                                .'&nbsp;/&nbsp;'
                                .'<span class="text--red cursor-pointer crm-personal-account-inactive" title="konta zablokowane"><b>'.$model->accounts['accounts_inactive'].'</b></span></dd>'
                             .'</dl>';

                return ['success' => true, 'action' => 'caccount', 'info' => $infoHtml];	
		 	} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAccount', [ 'model' => $model, ]), 'errors' => $model->getErrors() );	
		    }
			
			return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        } else {
            return $this->renderAjax('_formAccount', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionLock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 0; 
           $user->save();
        }
        $infoHtml = '<span class="label label-'.( ($model->user['status'] == 10) ? 'success' : 'danger' ).'">'.( ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ).'</span>'
                    .' <a href="'.Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]).'" class="btn btn-xs btn-'.( ($model->user['status'] == 10) ? 'danger' : 'success' ).' deleteConfirm" data-label="'.( ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ).'" title="'.( ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ).'">'
                        .(($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ).'</a>';
            
        return array('success' => true, 'alert' => 'Konto zostało zablokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'daccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionUnlock($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if($model->user) {
           $user = $model->user;
           $user->status = 10; 
           $user->save();
        }
        $infoHtml = '<span class="label label-'.( ($model->user['status'] == 10) ? 'success' : 'danger' ).'">'.( ($model->user['status'] == 10) ? 'aktywne' : 'zablokowane' ).'</span>'
                    .' <a href="'.Url::to(['/crm/customer/'.(($model->user['status'] == 10) ? 'lock' : 'unlock'), 'id' => $model->id]).'" class="btn btn-xs btn-'.( ($model->user['status'] == 10) ? 'danger' : 'success' ).' deleteConfirm" data-label="'.( ($model->user['status'] == 10) ? 'Zablokuj konto' : 'Odblokuj konto' ).'" title="'.( ($model->user['status'] == 10) ? 'zablokuj' : 'odblokuj' ).'">'
                        .(($model->user['status'] == 10) ? '<i class="fa fa-lock"></i>' : '<i class="fa fa-unlock"></i>' ).'</a>';
            
        return array('success' => true, 'alert' => 'Konto zostało odblokowane', 'id' => $id, 'table' => "#table-persons", 'action' => 'daccount', 'info' => $infoHtml);	
   
       // return $this->redirect(['index']);
    }
    
    public function actionCard($id) {
        $model = $this->findModel($id);
        
        return $this->renderPartial('card', ['model' => $model]);
    }
	
	public function actionCreateinline() {
		
		$model = new Customer();
        $model->type_fk = 2;
		$model->role_fk = 1;
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            //$model->name = $_POST['value'];
            //$exist = DictionaryValue::find()->where
			if($model->validate() && $model->save()) {
				return array('success' => true, 'alert' => 'Wartość została dodana', 'id' => $model->id, 'name' => $model->name );
			} else {
                $alert = "Ten słownik juz posiada taką wartość";
                foreach($model->getErrors() as $key => $value){
                    $alert = $value;
                }
                return array('success' => false, 'alert' => $alert );	
			}		
		} else {
	
			return  $this->renderAjax('_formInline', [  'model' => $model,]) ;	
		}
	}
}

