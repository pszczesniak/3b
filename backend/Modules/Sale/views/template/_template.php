<fieldset><legend>Elementy szablonu</legend>
    <ul class="list-group">
        <?php
            foreach($model->items as $key => $item) {
                if($item->status == 1)
					$input = '<textarea name="tmpl-'.$item->id.'" class="mce-basic">'.$item->default_value.'</textarea>';
				if($item->status == 2)
					$input = '<input type="hidden" name="hmpl-'.$item->id.'" value="{PRODUCTS}">';
				echo '<li class="list-group-item '.(($item->status == 2) ? 'bg-orange' : '').'">'
                        .'<h4 '.(($item->status == 1) ? 'class="text--purple"' : '').'>'.$item->name.'</h4>'
						.(($create) ? ('<br />'.$input ): '')
                    .'</li>';
            }
        ?>
    </ul>
</fieldset>
<fieldset><legend>O szablonie</legend>
    Utworzony przez <b class="text--navy"><?= $model->creator['fullname'] ?></b>&nbsp;&nbsp;<small class="btn-icon"><i class="fa fa-clock"></i><?= $model->created_at ?></small><br /><br />
    <p><?= ($model->describe) ? $model->describe : '<div class="alert alert-info">brak dodatkowego opisu</div>' ?></p>
</fieldset>