<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;
use frontend\widgets\sale\ProductsTable;
use frontend\widgets\tasks\ClaimsTable;
use frontend\widgets\tasks\InstancesTable;
use frontend\widgets\debt\TimelineTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

$this->title = '#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprawy'), 'url' => Url::to(['/task/matter/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid"> 
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/sale/offer/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa oferta</a>
		    <?php } ?>
			<a href="<?= Url::to(['/timeline/offer', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Historia</a>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) && 1 == 2) { ?>
				<a href="<?= Url::to(['/sale/offer/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
		</div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-matter-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading bg-lightgrey text--navy">
                    <strong>Oferta</strong>
                    <div class="pull-right">
                        <a href="<?= Url::to(['/sale/offer/printpdf', 'id' => $model->id, 'save' => false]) ?>" class="btn btn-icon bg-teal btn-flat" style="margin-top:-8px"><span class="fa fa-print"></span> Eksportuj</a>
                        <a href="<?= Url::to(['/sale/offer/export', 'id' => $model->id]) ?>" class="btn btn-icon bg-blue btn-flat" style="margin-top:-8px"><span class="fab fa-telegram-plane"></span> Wyślij</a>
                        <?php if( ( count(array_intersect(["caseEdit", "grantAll"], $grants)) > 0 ) ) { ?>
                            <a href="<?= Url::to(['/sale/offer/update', 'id' => $model->id]) ?>" class="btn btn-icon btn-primary btn-flat" style="margin-top:-8px"><span class="fa fa-pencil-alt"></span> Edytuj</a>
                        <?php } ?>
                        <div class="clear"></div>
                    </div>
				</div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details">
                        <div class="row-details">
                            <p><span class="bold">Nazwa</span>: <b><?= $model->name ?></b></p>
                        </div>
                        
                        <div class="row-details">
                            <p>
                                <span class="bold">Prowadzący</span>: 
                                <?php if(!$model->leadingEmployee) { echo 'brak informacji'; } else { ?>
                                    <div class="grid profile">
                                        <div class="col-md-4">
                                            <div class="profile-avatar">
                                                <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg"))?"/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg":"/images/default-user.png"; ?>
                                                <img src="<?= $avatarContact ?>" alt="Avatar">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="profile-name">
                                                <h3><?= $model->leadingEmployee['fullname'] ?></h3>
                                                <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingEmployee['kindname']) ? $model->leadingEmployee['typename'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingEmployee['phone']) ? $model->leadingEmployee['phone'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingEmployee['email']) ? Html::mailto($model->leadingEmployee['email'], $model->leadingEmployee['email']) : 'brak danych'  ?></p>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
                           </p>
                        </div>
                        <div class="row-details">
                            <p>
                                <span class="bold">Osoba kontaktowa</span>: 
                                <?php if(!$model->leadingContact) { echo 'brak informacji'; } else { ?>
                                    <div class="grid profile">
                                        <div class="col-md-4">
                                            <div class="profile-avatar">
                                                <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg"))?"/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg":"/images/default-user.png"; ?>
                                                <img src="<?= $avatarContact ?>" alt="Avatar">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="profile-name">
                                                <h3><?= $model->leadingContact['fullname'] ?></h3>
                                                <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingContact['position']) ? $model->leadingContact['position'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingContact['phone']) ? $model->leadingContact['phone'] : 'brak danych' ?></p>
                                                <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingContact['email']) ? Html::mailto($model->leadingContact['email'], $model->leadingContact['email']) : 'brak danych'  ?></p>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
                           </p>
                        </div>  
                        <div class="row-details">
                            <div class="grid profile">
                                <div class="col-md-4">
                                    <div class="profile-avatar">
                                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                                        <img src="<?= $avatar ?>" alt="Avatar">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="profile-name">
                                        <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                                        <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['statusname'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                             
                    </div>
                </div><!--/project-body-->
            </section>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            
            <div class="profile-about">
                <h4>Dodatkowe informacje</h4>

                <div class="table-responsive about-table">
                    <table class="table">
                        <tbody>
   
                            <tr>
                                <th>Opis</th>
                                <td><?= $model->describe ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel with-nav-tabs panel-default">
                <?php $notes = $model->notes; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-shopping-cart"></i><span class="panel-tabs--text">Produkty</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
                       <!-- <li><a data-toggle="tab" href="#tab3"><i class="fa fa-inbox"></i><span class="panel-tabs--text">Korespondencja </span></a></li> -->
						<li><a data-toggle="tab" href="#tab4"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>
						<li><a data-toggle="tab" href="#tab5"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Zadania</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?= ProductsTable::widget(['dataUrl' => Url::to(['/sale/offer/products', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/sale/offer/create', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
						</div>
						<div class="tab-pane" id="tab2">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 3, 'parentId' => $model->id, 'onlyShow' => true, 
                                                'download' => Url::to(['/task/matter/download', 'id' => $model->id]) ]) ?>
						</div>
						<div class="tab-pane" id="tab4">
                            <?php /* $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes])*/ ?>
                            <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/task/matter/note', 'id' => $model->id])]) ?>
						</div>
                        <div class="tab-pane" id="tab5">
                            <?= EventsTable::widget(['dataUrl' => Url::to(['/task/matter/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

<?php $this->endContent(); ?>