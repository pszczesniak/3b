<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
use frontend\widgets\sale\TemplateTable;

$this->title = Yii::t('app', 'Konfiguracja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Szablony'), 'url' => Url::to(['/sale/template/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="grid">
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/sale/template/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowy szablon</a>
		    <?php } ?>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/sale/template/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat modalConfirm" ><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
        </div>
        
    </div>
</div>
<?= Alert::widget() ?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-update', 'title'=>Html::encode($this->title))) ?>
    <fieldset><legend>Elementy</legend>
        <?= TemplateTable::widget(['dataUrl' => Url::to(['/sale/template/items', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/sale/template/icreate', 'id' => $model->id]), 'saveUrl' => Url::to(['/sale/template/save', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
    </fieldset>
    <fieldset><legend>Szablon</legend>
         <?= $this->render('_form', ['model' => $model, ]) ?>
    </fieldset>

<?php $this->endContent(); ?>
