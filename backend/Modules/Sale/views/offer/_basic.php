<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;
?>
<div class="row-details">
    <p><span class="bold">Nazwa</span>: <b><?= $model->name ?></b></p>
</div>

<div class="row-details">
    <p>
        <span class="bold">Prowadzący</span>: 
        <?php if(!$model->leadingEmployee) { echo 'brak informacji'; } else { ?>
            <div class="grid profile">
                <div class="col-md-4">
                    <div class="profile-avatar">
                        <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg"))?"/uploads/employees/cover/".$model->leadingEmployee['id']."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= $avatarContact ?>" alt="Avatar">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="profile-name">
                        <h3><?= $model->leadingEmployee['fullname'] ?></h3>
                        <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingEmployee['kindname']) ? $model->leadingEmployee['typename'] : 'brak danych' ?></p>
                        <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingEmployee['phone']) ? $model->leadingEmployee['phone'] : 'brak danych' ?></p>
                        <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingEmployee['email']) ? Html::mailto($model->leadingEmployee['email'], $model->leadingEmployee['email']) : 'brak danych'  ?></p>

                    </div>
                </div>
            </div>
        <?php } ?>
        <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
   </p>
</div>
<div class="row-details">
    <p>
        <span class="bold">Osoba kontaktowa</span>: 
        <?php if(!$model->leadingContact) { echo 'brak informacji'; } else { ?>
            <div class="grid profile">
                <div class="col-md-4">
                    <div class="profile-avatar">
                        <?php $avatarContact = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg"))?"/uploads/customers/contacts/cover/".$model->leadingContact['id']."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= $avatarContact ?>" alt="Avatar">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="profile-name">
                        <h3><?= $model->leadingContact['fullname'] ?></h3>
                        <p class="job-title mb0"><i class="fa fa-building"></i> <?= ($model->leadingContact['position']) ? $model->leadingContact['position'] : 'brak danych' ?></p>
                        <p class="job-title mb0"><i class="fa fa-phone"></i> <?= ($model->leadingContact['phone']) ? $model->leadingContact['phone'] : 'brak danych' ?></p>
                        <p class="job-title mb0"><i class="fa fa-envelope"></i> <?= ($model->leadingContact['email']) ? Html::mailto($model->leadingContact['email'], $model->leadingContact['email']) : 'brak danych'  ?></p>

                    </div>
                </div>
            </div>
        <?php } ?>
        <!--<span class="icon-plus add-assignee tooltipstered"></span>-->
   </p>
</div>  
<div class="row-details">
    <div class="grid profile">
        <div class="col-md-4">
            <div class="profile-avatar">
                <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg"))?"/uploads/customers/cover/".$model->id_customer_fk."/preview.jpg":"/images/default-user.png"; ?>
                <img src="<?= $avatar ?>" alt="Avatar">
            </div>
        </div>
        <div class="col-md-8">
            <div class="profile-name">
                <h5><a href="<?= Url::to(['/crm/customer/view', 'id' => $model->id_customer_fk]) ?>"><?= $model->customer['name'] ?></a></h5>
                <p class="job-title mb0"><i class="fa fa-cog"></i> <?= $model->customer['statusname'] ?></p>
                <?php if($model->branch) { ?>
                    <p class="job-title mb0"><i class="fa fa-map-marker-alt"></i> <?= $model->branch['name'] ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>