<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

?>

<h4><small>Nazwa: </small><span class="text--pink"><?= $model->name ?></span></h4>
<fieldset><legend>Elementy</legend>
    <ul class="list-group">
        <?php
            foreach($items as $key => $item) {
                echo '<li class="list-group-item '.(($item->status == 2) ? 'bg-orange' : '').'">'
                        .'<h5 '.(($item->status == 1) ? 'class="text--navy"' : '').'>'.$item->name.'&nbsp;'.(($item->status == 1) ? '<a href="'.Url::to(['/sale/offer/eitem', 'id' => $item->id]).'" class="viewModal btn btn-sm btn-default text--purple" data-title="Edycja pozycji" data-target="#modal-grid-item"><i class="fa fa-pencil-alt"></i></a>' : '') .'</h5>'
                        .'<div id="tmpl-'.$item->id.'">'.(($item->status == 1) ? (($item->describe) ? $item->describe : 'nie ustawiono') : '').'</div>'
                    .'</li>';
            }
        ?>
    </ul>
</fieldset>
<fieldset><legend>O szablonie</legend>
    Utworzony przez <b class="text--navy"><?= $model->creator['fullname'] ?></b>&nbsp;&nbsp;<small class="btn-icon"><i class="fa fa-clock"></i><?= $model->created_at ?></small><br /><br />
    <p><?= ($model->describe) ? $model->describe : '<div class="alert alert-info">brak dodatkowego opisu</div>' ?></p>
</fieldset>