<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-factor-form">

    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-items"],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
        <div class="modal-body">
            <h5 class="text--purple"><?= $model->name ?></h5>
            <?= $form->field($model, 'describe')->textarea(['class' => 'mce-basic', 'placeholder' => 'Wpisz opis'])->label(false) ?>
        </div>

        <div class="modal-footer">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
