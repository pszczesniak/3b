<table width="100%">
    <?php foreach($products as $key => $category) { ?>
    <tr>
        <td>
            <table class="table table-bordered" width="100%">
                <thead>
                    <tr> <th colspan="3"><h5><?= $category['name'] ?></h5><p><?= $category['describe'] ?></p></th> </tr>
                    <tr> <th>Produkt</th><th>Kolor</th><th>Cena</th></tr>
                </thead>
                <tbody>
                    <?php foreach($category['products'] as $i => $product) { ?>
                    <tr>
                        <td style="width:80%;">
                            <h6><?= $product['product']['name'] ?></h6>
                            <p><?= $product['product']['describe'] ?></p>
                            <p style="color: #284AB8;"><?= ($product['config']) ? ('Zużycie: '.$product['config']['resistance']) : '' ?></p>
                        </td>
                        <td><?= ($product['config']) ? ($product['config']['color']) : '' ?></td><td class="total"><?= $product['product']['price'] ?> zł/<?= $product['unit'] ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </td>
    </tr>
    <?php } ?>
</table>