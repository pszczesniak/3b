<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\files\FilesBlock;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */

$this->title = Yii::t('app', 'Nowa oferta');
$this->params['breadcrumbs'][] = 'Sprzedaż';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr ofert'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-case-create', 'title'=>Html::encode($this->title))) ?>
    <?= $this->render('_form', ['model' => $model, ]) ?>
<?php $this->endContent(); ?>

<script type="text/javascript">
	document.getElementById('offer-id_template_fk').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/sale/template/info']) ?>/"+((this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('template-container').innerHTML = result.html; 
				KAPI.tinyMceBasic();    
            }
        }
        xhr.send();
        return false;
    }
    
    document.getElementById('offer-id_customer_fk').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/crm/customer/relationship']) ?>/"+(this.value ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('offer-id_contact_fk').innerHTML = result.listContacts;   
                document.getElementById('offer-id_branch_fk').innerHTML = result.listBranches;  
                document.getElementById('offer-id_project_fk').innerHTML = result.listCase;               
            }
        }
        xhr.send();
        return false;
    }
</script>

