<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use frontend\widgets\files\FilesBlock;
use frontend\widgets\company\EmployeesBlock;
use frontend\widgets\tasks\EventsTable;
use frontend\widgets\chat\Comments;
use frontend\widgets\sale\ProductsTable;
use frontend\widgets\sale\OffersTable;
use frontend\widgets\tasks\InstancesTable;
use frontend\widgets\debt\TimelineTable;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\Calmatter */

$this->title = '#'.$model->id;
$this->params['breadcrumbs'][] = 'Sprzedaż';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Oferty'), 'url' => Url::to(['/sale/offer/index', 'back' => 'yes'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid"> 
    <div class="col-md-12">
        <div class="pull-right">
            <?php if( ( count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0 ) ) { ?>
				<a href="<?= Url::to(['/sale/offer/create']) ?>" class="btn btn-success btn-flat btn-add-new-user" ><i class="fa fa-plus"></i> Nowa oferta</a>
		    <?php } ?>
			<a href="<?= Url::to(['/timeline/offer', 'id' => $model->id]) ?>" class="btn bg-pink btn-flat btn-add-new-user" ><i class="fa fa-history"></i> Historia</a>
            <?php if( ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) && 1 == 2) { ?>
				<a href="<?= Url::to(['/sale/offer/delete', 'id' => $model->id]) ?>" class="btn btn-danger btn-flat btn-add-new-user" data-toggle="modal" data-target="#modal-pull-right-add"><i class="fa fa-trash"></i> Usuń</a>
			<?php } ?>
		</div>
    </div>
</div>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cal-matter-view', 'title'=>Html::encode($this->title))) ?>
    <?= Alert::widget() ?>
    <div class="grid view-project">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <section class="panel">
                <div class="project-heading bg-lightgrey text--navy">
                    <strong>Oferta</strong>
                    <div class="pull-right">
                        <a href="<?= Url::to(['/sale/offer/printpdf', 'id' => $model->id, 'save' => false]) ?>" class="btn btn-xs btn-icon bg-teal btn-flat" style="margin-top:-8px"><span class="fa fa-print"></span> Eksportuj</a>
                        <a href="<?= Url::to(['/sale/offer/export', 'id' => $model->id]) ?>" class="btn btn-xs btn-icon bg-blue btn-flat" style="margin-top:-8px"><span class="fab fa-telegram-plane"></span> Wyślij</a>
                        <a href="<?= Url::to(['/sale/offer/update', 'id' => $model->id]) ?>" class="btn btn-xs btn-icon btn-primary btn-flat viewModal" data-target="#modal-grid-item" data-title="Edycja oferty" style="margin-top:-8px"><span class="fa fa-pencil-alt"></span> Edytuj</a>
                        <div class="clear"></div>
                    </div>
				</div>
                <div class="panel-body project-body">
                    
                    <div class="row project-details" id="tmpl-basic">
                        <?= $this->render('_basic', ['model' => $model]) ?>
                    </div>
                </div><!--/project-body-->
            </section>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <!--
            <div class="profile-about">
                <h4>Dodatkowe informacje</h4>

                <div class="table-responsive about-table">
                    <table class="table">
                        <tbody>
   
                            <tr>
                                <th>Opis</th>
                                <td><?= $model->describe ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>-->
            <div class="panel with-nav-tabs panel-default">
                <?php $notes = $model->notes; ?>
                <div class="panel-heading panel-heading-tab">
					<ul class="nav panel-tabs">
						<li class="active"><a data-toggle="tab" href="#tab1"><i class="fa fa-shopping-cart"></i><span class="panel-tabs--text">Produkty</span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><i class="fa fa-file-code"></i><span class="panel-tabs--text">Szablon</span></a></li>
                        <li><a data-toggle="tab" href="#tab3"><i class="fa fa-file"></i><span class="panel-tabs--text">Dokumenty</span> </a></li>
						<li><a data-toggle="tab" href="#tab4"><i class="fa fa-comments"></i><span class="panel-tabs--text">Komentarze </span><?= (count($notes)>0) ? '<span class="badge bg-blue">'.count($notes).'</span>' : ''?></a></li>
						<li><a data-toggle="tab" href="#tab5"><i class="fa fa-tasks"></i><span class="panel-tabs--text">Zadania</span></a></li>
				    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						<div class="tab-pane active" id="tab1">
                            <?= ProductsTable::widget(['dataUrl' => Url::to(['/sale/offer/products', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/sale/offer/citem', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
						</div>
                        <div class="tab-pane" id="tab2">
                            <?= $this->render('_template', ['model' => $model->template, 'items' => $model->items, 'create' => false]) ?>
                        </div>
						<div class="tab-pane" id="tab3">
                            <?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 13, 'parentId' => $model->id, 'onlyShow' => false, 
                                                'download' => false ]) ?>
						</div>
						<div class="tab-pane" id="tab4">
                            <?php /* $this->render('_comments', ['id' => $model->id, 'notes' => $model->notes])*/ ?>
                            <?= Comments::widget(['notes' => $notes, 'actionUrl' => Url::to(['/sale/offer/note', 'id' => $model->id])]) ?>
						</div>
                        <div class="tab-pane" id="tab5">
                            <?= EventsTable::widget(['dataUrl' => Url::to(['/sale/offer/events', 'id'=>$model->id]), 'insert' => true, 'insertUrl' => Url::to(['/task/event/new', 'id' => $model->id]), 'actionWidth' => '90px' ]) ?>
						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--<p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

<?php $this->endContent(); ?>