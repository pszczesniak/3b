<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-factor-form">

    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-items"],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            //'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
        <div class="modal-body">
            <div class="grid">
                <div class="col-sm-5 col-xs-12">
                    <?= $form->field($model, 'id_product_fk')->dropDownList(ArrayHelper::map(\backend\Modules\Shop\models\ShopProduct::find()->where(['status' => 1])->orderby('name')->all(), 'id', 'name'), ['prompt' => '-- wybierz produkt --', 'class' => 'form-control select2' ] ) ?>
                </div>
                <div class="col-sm-3 col-xs-5">
                    <?= $form->field($model, 'custom_price')->textInput(['class' => 'form-control number']) ?>
                </div>
                <div class="col-sm-4 col-xs-7">
                    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($model, 'resistance')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <fieldset><legend>Rabat</legend>
                <div class="grid">
                    <div class="col-md-4 col-sm-6 col-xs-12"><?= $form->field($model, 'discount_fk')->dropDownList([0 => 'brak rabatu', 1 => '% ceny', 2 => 'redukcja ceny'])->label('Opcja') ?></div>
                    <div class="col-md-4 col-sm-3 col-xs-6"><?= $form->field($model, 'discount_percent')->textInput(['class' => 'form-control number', 'disabled' => (($model->discount_fk == 0 || $model->discount_fk == 2) ? true : false)])->label('Wartość procentowa') ?></div>
                    <div class="col-md-4 col-sm-3 col-xs-6"><?= $form->field($model, 'discount_value')->textInput(['class' => 'form-control number', 'disabled' => (($model->discount_fk == 0 || $model->discount_fk == 1) ? true : false)])->label('Redukcja kwotowa') ?></div>
                </div>
            </fieldset>
        </div>

        <div class="modal-footer">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">

	document.getElementById('offerproduct-discount_fk').onchange = function(event) {
        if(event.target.value == 0) {
            document.getElementById('offerproduct-discount_percent').disabled = true;
            document.getElementById('offerproduct-discount_value').disabled = true;
        }
        if(event.target.value == 1) {
            document.getElementById('offerproduct-discount_percent').disabled = false;
            document.getElementById('offerproduct-discount_value').disabled = true;
        }
        if(event.target.value == 2) {
            document.getElementById('offerproduct-discount_percent').disabled = true;
            document.getElementById('offerproduct-discount_value').disabled = false;
        }
    }
    
    document.getElementById('offerproduct-id_product_fk').onchange = function(event) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?= Url::to(['/shop/shopproduct/info']) ?>/"+((this.value) ? this.value : 0), true);
        xhr.onreadystatechange=function()  {
            if (xhr.readyState==4 && xhr.status==200)  {
                var result = JSON.parse(xhr.responseText);
                document.getElementById('offerproduct-custom_price').value = result.price; 
                document.getElementById('offerproduct-resistance').value = result.resistance;                
            }
        }
        xhr.send();
        return false;
    }
</script>