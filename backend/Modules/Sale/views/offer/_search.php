<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\sale\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-sale-offers-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-offers']
    ]); ?>

    <div class="grid">
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList(($model->id_customer_fk) ? $model->id_customer_fk : 0), 
                                                                [ 'prompt' => '-- wybierz klienta --', 'class' => 'widget-search-list  selectAutoComplete', 'data-url' => Url::to('/crm/default/autocomplete'), 'data-table' => '#table-offers', 'data-form' => '#filter-sale-offers-search'] )
                                                            ->label('Klient') ?></div>
		<div class="col-md-3 col-sm-3 col-xs-6"><?= $form->field($model, 'state')->dropDownList( [1 => 'nowa', 2 => 'oczekująca', 3 => 'zaakceptowana', 4 => 'dostarczona', 5 => 'odrzucona'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-offers', 'data-form' => '#filter-sale-offers-search'  ] ) ?></div>        
        <div class="col-md-3 col-sm-3 col-xs-6"><?= $form->field($model, 'id_employee_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-offers', 'data-form' => '#filter-sale-offers-search'  ] ) ?></div>
        <div class="col-sm-3 col-md-3 col-xs-12"> <?= $form->field($model, 'name')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-offers', 'data-form' => '#filter-sale-offers-search' ])->label('Nazwa <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
    </div> 
		
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
