<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
    <!--<header id="header">
        <table>
            <tr>
                <td><span class="page-number"></span></td>
                <td style="text-align: right;">test</td>
            </tr>
        </table>
    </header>-->

    <main>
		<h2>Produkty</h2>
        <table style="background-color: #fff; padding: 0; margin: 0;">
			<tr>
				<td align="left" valign="top" width="50%" style="font-size:14px; border-left: 6px solid #1f398e; padding-left: 6px;">
					  <div class="to">NABYWCA</div>
					  <h2 class="name"><?= $model->customer['name'] ?></h2>
					  <div class="address"><?= $model->customer['address'] ?></div>
					  <div class="address"><?= $model->customer['postal_code'] .' '. $model->customer['city'] ?></div>
					  <!--<div class="email"><a href="mailto:john@example.com">john@example.com</a></div>-->
				</td>
			</tr>
		</table>
		<br /><br />
        <div class="wrapper wrapper-content p-xl">
            <div class="ibox-content p-xl">
                <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Product</th>
                    <th>Cena</th>
                  </tr>

                    <?php
                    foreach($model->products as $key => $record){ 
                        echo '<tr>'
                                .'<td>'.$record->product['name'].'</td>'
                                .'<td class="total">'.number_format($record->custom_price, 2, "," ,  " ").'</td>'
                              .'</tr>';
                    } 
                    ?> 
             
                </tbody>
              </table>
            </div>
        </div>
        <div class="wrapper wrapper-content p-xl">
            <div class="ibox-content p-xl">
                <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Product</th>
                    <th>Cena</th>
                  </tr>

                    <?php
                    foreach($model->products as $key => $record){ 
                        echo '<tr>'
                                .'<td>'.$record->product['name'].'</td>'
                                .'<td class="total">'.number_format($record->custom_price, 2, "," ,  " ").'</td>'
                              .'</tr>';
                    } 
                    ?> 
             
                </tbody>
              </table>
            </div>
        </div>
        <div class="wrapper wrapper-content p-xl">
            <div class="ibox-content p-xl">
                <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Product</th>
                    <th>Cena</th>
                  </tr>

                    <?php
                    foreach($model->products as $key => $record){ 
                        echo '<tr>'
                                .'<td>'.$record->product['name'].'</td>'
                                .'<td class="total">'.number_format($record->custom_price, 2, "," ,  " ").'</td>'
                              .'</tr>';
                    } 
                    ?> 
             
                </tbody>
              </table>
            </div>
        </div>
        <div class="wrapper wrapper-content p-xl">
            <div class="ibox-content p-xl">
                <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Product</th>
                    <th>Cena</th>
                  </tr>

                    <?php
                    foreach($model->products as $key => $record){ 
                        echo '<tr>'
                                .'<td>'.$record->product['name'].'</td>'
                                .'<td class="total">'.number_format($record->custom_price, 2, "," ,  " ").'</td>'
                              .'</tr>';
                    } 
                    ?> 
             
                </tbody>
              </table>
            </div>
        </div>
        <!--<hr>-->
        <h2>Warunki handlowe</h2>
    </main>
    <!--<footer id="footer">
        <p style="text-align: center;">Hufgard Polska sp. z o.o. | ul. Rząsawska 40, 42-209 Częstochowa | tel. +48 34 360 46 94 | fax. +48 34 360 46 98 | NIP 949-204-24-56 | Regon 240650090 | Kapitał Zakładowy 500.000,00 PLN | KRS 0000282225
        Sad Gospodarczy w Częstochowie XII Wydział Gospodarczy | Krajowego Rejestru Sadowego | Zarząd: Alexander Hufgard, Andrzej Grenda
        mBANK S.A. Oddział Korporacyjny Częstochowa | 23 1140 1889 0000 5065 0100 1001 dla płatności w PLN | 93 1140 1889 0000 5065 0100 1002 dla płatności w EUR</p>
    </footer>-->

</body>
</html>