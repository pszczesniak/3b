<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="<?= \Yii::getAlias('@webroot') ?>/css/_print.css" rel="stylesheet">
 
</head>
<body>
    <header id="header"><span class="page-number"></span></header>
    <table style="background-color: #fff; padding: 0; margin: 0; width: 100%;">
        <tr>
            <td align="left" valign="middle" width="50%" style="font-size:14px; border-left: 6px solid #4EAF37; padding-left: 6px;">
                  <h5 class="name"><?= $model->customer['name'] ?></h5>
                  <div class="address"><?= $model->customer['address'] ?></div>
                  <div class="address"><?= $model->customer['postal_code'] .' '. $model->customer['city'] ?></div>
                  <!--<div class="email"><a href="mailto:john@example.com">john@example.com</a></div>-->
            </td>
            <td align="right" valign="middle" width="50%">
                
                <!--<div class="to">SPRZEDAJĄCY</div>
                <h2 class="name">Sadkowski i Wspólnicy</h2>-->
                <div class="address">Częstochowa</div>
                <div class="address"><?= date('Y-m-d') ?></div>
                <img src="<?= \Yii::getAlias('@webroot') ?>/images/logo_dark.jpg" height="100px">
                <!--<div><a href="mailto:kontakt@siw.pl">kontakt@siw.pl</a></div> --> 					
            </td>
        </tr>
    </table>
    
    <div>
        <?php if($model->id_contact_fk) { ?>
            <h4><i><?= $model->leadingContact['user_role'].' '.$model->LeadingContact['fullname'] ?></i></h4>
        <?php } ?>
    </div>
    
    <?php
        foreach($model->items as $key => $item) {
            if(trim($item->describe) == '{PRODUCTS}') {
                echo $this->render('_products', ['products' => $model->grouping]);
            } else {
                echo '<div class="item-container"><h3 class="headline">'.$item->name.'</h3>';
                echo $item->describe.'</div>';
            }
        }
    ?>
    
    
    <div style="text-align: right; margin-top:100px;">
        Z poważaniem<br />
        <?php if($model->leadingEmployee) { ?>
            <?= $model->leadingEmployee['fullname'].'<br />'; ?>
            <?= $model->leadingEmployee['email'].'<br />'; ?>
            <?= $model->leadingEmployee['phone'].'<br />'; ?>
        <?php } ?>
        <!--Emilia Dębska<br />
        e.debska@hufgard.pl--0<br />
        +48 601 655 010<br />-->
    </div>
    <footer id="footer">
        <table>
            <tr>
                <td style="width: 100px;"><img src="<?= \Yii::getAlias('@webroot') ?>/images/logo.jpg" style="width:100%"></td>
                <td style="text-align: center;">
                    <p style="text-align: center;">Hufgard Polska sp. z o.o. | ul. Rząsawska 40, 42-209 Częstochowa | tel. +48 34 360 46 94 | fax. +48 34 360 46 98 | NIP 949-204-24-56 | Regon 240650090 | Kapitał Zakładowy 500.000,00 PLN | KRS 0000282225</p>
                    <p style="text-align: center;">Sad Gospodarczy w Częstochowie XII Wydział Gospodarczy | Krajowego Rejestru Sadowego | Zarząd: Alexander Hufgard, Andrzej Grenda</p>
                    <p style="text-align: center;">mBANK S.A. Oddział Korporacyjny Częstochowa | 23 1140 1889 0000 5065 0100 1001 dla płatności w PLN | 93 1140 1889 0000 5065 0100 1002 dla płatności w EUR</p>
                </td>
            </tr>
        </table>
    </footer>
</body>
</html>