<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Fileswidget;
use frontend\widgets\company\EmployeesCheck;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cal-case-form form">
    <?php $form = ActiveForm::begin([
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => (!$model->isNewRecord) ? 'ajaxform' : '', ],
            'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                //'template' => '<div class="grid"><div class="col-sm-2">{label}</div><div class="col-sm-10">{input}</div><div class="col-xs-12">{error}</div></div>',
               // 'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>
    <div class="grid">
		<div class="col-sm-8 col-xs-7"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
		<div class="col-sm-4 col-xs-5"><?= $form->field($model, 'state')->dropDownList( \backend\Modules\Offer\models\Offer::getStates() ) ?></div>
	</div>
	
	<div class="grid">
		<div class="col-sm-6 col-xs-12">
			<?= $form->field($model, 'id_company_branch_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyBranch::getList(0), 'id', 'name'), [ 'prompt' => '-- wybierz szablon --', 'class' => 'form-control'] ) ?> 
		</div>
		<div class="col-sm-6 col-xs-12">
			<?= $form->field($model, 'id_employee_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Company\models\CompanyEmployee::find()->where(['status' => 1])->all(), 'id', 'fullname'), [ 'prompt' => '-- przypisz do pracownika --', /*'class' => 'selectAutoComplete', 'data-url' => Url::to('/crm/default/autocomplete')*/] ) ?> 
		</div>
	</div>
	<div class="grid">
		<div class="col-sm-3 col-xs-12">
			<?= $form->field($model, 'id_customer_fk')->dropDownList( \backend\Modules\Crm\models\Customer::getShortList(($model->id_customer_fk) ? $model->id_customer_fk : 0), [ 'prompt' => '-- wybierz klienta --', 'class' => 'selectAutoComplete', 'data-url' => Url::to('/crm/default/autocomplete'), 
                                                                           'data-default' => (($model->id_customer_fk) ? substr($model->customer['name'], 0, 10) : 'a'), 'data-id' => $model->id_customer_fk] ) ?> 
		</div>
        <div class="col-sm-3 col-xs-12">
			<?= $form->field($model, 'id_customer_branch_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CustomerBranch::getList($model->id_customer_fk), 'id', 'fullname'), [ 'prompt' => '-- wybierz kontakt --', 'class' => 'form-control select2'] ) ?> 
		</div>
        <div class="col-sm-3 col-xs-12">
			<?= $form->field($model, 'id_contact_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Crm\models\CustomerPerson::getList($model->id_customer_fk), 'id', 'fullname'), [ 'prompt' => '-- wybierz kontakt --', 'class' => 'form-control select2'] ) ?> 
		</div>
		<div class="col-sm-3 col-xs-12">
			<?= $form->field($model, 'id_project_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Task\models\CalCase::getProjects($model->id_customer_fk), 'id', 'name'), [ 'prompt' => '-- wybierz projekt --', 'class' => 'form-control'] ) ?> 
		</div>	
	</div>
	<?= $form->field($model, 'id_template_fk')->dropDownList( \backend\Modules\Offer\models\OfferTemplate::getList( ), [ 'prompt' => '-- wybierz szablon --', 'class' => 'form-control'] ) ?> 

	<div id="template-container">
    </div>
    
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
