<?php

namespace app\Modules\Sale\controllers;

use Yii;
use backend\Modules\Offer\models\Offer;
use backend\Modules\Offer\models\OfferTemplate;
use backend\Modules\Offer\models\OfferTemplateItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\CustomHelpers;

/**
 * TemplateController implements the CRUD actions for CalCase model.
 */
class TemplateController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalCase models.
     * @return mixed
     */
    public function actionIndex() {
        if( count(array_intersect(["casePanel", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $searchModel = new OfferTemplate();
        //$searchModel->id_dict_case_status_fk = (Yii::$app->params['env'] == 'dev') ? 1 : 5;
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.templates'); 
            if($params) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'grants' => $this->module->params['grants'],
            'setting' => $setting
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        
        if(isset($_GET['OfferTemplate'])) {
            $params = $_GET['OfferTemplate'];
            \Yii::$app->session->set('search.templates', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
			if(isset($params['id_company_branch_fk']) && !empty($params['id_company_branch_fk']) ) {
				array_push($where, "m.id_company_branch_fk = ".$params['id_company_branch_fk']);
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
        } 
		
        
        // if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {  }    
			
		$fields = []; $tmp = [];
        $status = [];//CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
        if( isset($post['sort']) && $post['sort'] == 'employee' ) $sortColumn = 'e.lastname';
		
		$query = (new \yii\db\Query())->from('{{%offer_template}} as m')
            ->where( ['m.status' => 1] );
            //->groupBy(['m.id', 'm.name', 'no_label', 'm.subname', 'customer_role', 'id_dict_case_status_fk', 'id_dict_case_type_fk', 'id_dict_case_category_fk',
			//		  'id_parent_fk', 'm.status', 'c.name', 'c.symbol', 'c.id'/*, 'os.name', 'os.symbol', 'os.id'*/]);
	    /*if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}*/
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
		
		$query->select(['m.id as id', 'm.name as name', 'm.status as status', "concat_ws(' ', e.lastname, e.firstname) as lemployee", 'm.created_by', 'm.created_at', 'is_active'
					 /*"(select sum(size_file) from {{%files}} where type_fk = 3 and status = 1 and id_fk = c.id) as file_size"*/]);
        $query->join(' JOIN', '{{%user}} as e', 'e.id = m.created_by');
			  
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
        
		foreach($rows as $key=>$value) {
			$tmp['name'] = '<a href="'.Url::to(['/sale/template/view', 'id'=>$value['id']]).'" >'.$value['name'].'</a>';
			$tmp['created'] = $value['created_at'];
			$tmp['creator'] = $value['lemployee'];
			//$tmp['status'] = '<label class="label bg-'.$presentation['color'].'" data-toggle="tooltip" data-title="'.$presentation['label'].'"><i class="fa fa-'.$presentation['icon'].'"></i></label>';
          
			$tmp['actions'] = '<div class="edit-btn-group btn-group">';
				$tmp['actions'] .= '<a href="'.Url::to(['/sale/template/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('app', 'View').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-eye"></i></a>';
				$tmp['actions'] .= ( count(array_intersect(["caseDelete", "grantAll"], $grants)) > 0 ) ?  '<a href="'.Url::to(['/sale/template/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-templates"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>':'';
			$tmp['actions'] .= '</div>';
            if($value['is_active'])
                $tmp['active'] = '<a class="modalConfirm" data-label="Ustaw szablon jako nieaktywny" href="'.Url::to(['/sale/template/inactive', 'id' => $value['id']]).'" data-title="Ustaw jako nieaktywną" data-toggle="tooltip" data-placement="left"><i class="fa fa-check text--green"></i></a>';
            else
                $tmp['active'] = '<a class="modalConfirm" data-label="Ustaw szablon jako aktywny" href="'.Url::to(['/sale/template/active', 'id' => $value['id']]).'" data-title="Ustaw jako aktywną" data-toggle="tooltip" data-placement="left"><i class="fa fa-square text--teal"></i></a>';
       
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
            $tmp['nid'] = $value['id'];
			
			array_push($fields, $tmp); $tmp = [];
		}
		return ['total' => $count,'rows' => $fields];
	}
   
    /**
     * Creates a new CalCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {	
		if( count(array_intersect(["caseAdd", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        $model = new OfferTemplate();
        //$model->type_fk = 1;
        
		//$model->id_company_branch_fk = $this->module->params['employeeBranch']; 
        $departments = []; $employees = []; array_push($employees, $this->module->params['employeeId']);
       	
        $grants = $this->module->params['grants'];
		
		if (Yii::$app->request->isPost ) {
			//Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            
        	if($model->validate() && $model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
                return  $this->render('create', [ 'model' => $model, ]) ;	
			}		
		} else {
			return  $this->render('create', [ 'model' => $model,]) ;	
		}
	}

    /**
     * Updates an existing CalCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)  {
        if( count(array_intersect(["caseEdit", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }
        
        $model = $this->findModel($id);
        $offer = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        if(!$this->module->params['isAdmin']){
            $checked = \backend\Modules\Task\models\CaseEmployee::find()->where([ 'id_employee_fk' => $this->module->params['employeeId'], 'id_case_fk' => $model->id])->count();
		
            if($checked == 0) {
                if(!$this->module->params['isSpecial']) {
                    throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień do tej sprawy.');
                }
            }
        }
        
        if($model->status == -1) {
            return  $this->render('delete', [  'model' => $model, ]) ;	
        }
        $departments = []; $employees = [];
        $grants = $this->module->params['grants'];
    
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {			

				return array('success' => true,  'action' => false, 'alert' => 'Szablon <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_form', ['model' => $model]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->render( 'update', [
                        'model' => $model, 'grants' => $this->module->params['grants']
                    ]) ;	
		}
    }

    /**
     * Deletes an existing CalCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)  {
        //$this->findModel($id)->delete();
        if( count(array_intersect(["caseDelete", "grantAll"], $this->module->params['grants'])) == 0 ) {
            throw new \yii\web\HttpException(405, 'Brak odpowiednich uprawnień.');
        }

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalCase::SCENARIO_DELETE;
        $model->status = -1;
        //$model->user_action = 'delete';
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        
        $offer = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        //$correspondence = \backend\Modules\Correspondence\models\CorrespondenceTask::find()->where(['status' => 1, 'id_case_fk' => $model->id])->andWhere('id_task_fk = -1')->andWhere('id_correspondence_fk in (select id from {{%correspondence}} where status = 1)')->count();
        
        if($offer != 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty')  );
				return $this->redirect(['index', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Szablon <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ na jego podstawie założone są oferty', 'id' => $id, 'table' => '#table-templates');	
			}
        }
        
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Szablon <b>'.$model->name.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Szablon <b>'.$model->name.'</b> został usunięty.<br/>', 'id' => $id, 'table' => '#table-templates');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Szablon <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Szablon <b>'.$model->name.'</b> nie został usunięty.<br />', 'id' => $id, 'table' => '#table-templates');	
			}
        }
    }
    
    public function actionActive($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_active = 1;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'yes', 'row' => $row, 'alert' => 'Szablon został ustawiony jako aktywny', 'table' => '#table-templates', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)  );		
        } else {
            return array('success' => false,  'refresh' => 'yes', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-templates', 'errors' => $model->getErrors()  );	
        }	
	}
    
    public function actionInactive($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        //$model->user_action = 'active';
        //echo  date('Y-m', strtotime($model->acc_period.'-01' . "+1 months") );exit;
        $model->is_active = 0;
      	$row = [];	
        //$transaction = \Yii::$app->db->beginTransaction();
		if($model->save()) {     
			return array('success' => true,  'refresh' => 'yes', 'row' => $row, 'alert' => 'Szablon został ustawiony jako nieaktywny', 'table' => '#table-templates', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1)  );		
        } else {
            return array('success' => false,  'refresh' => 'yes', 'alert' => 'Operacja nie może zostać zrealizowana', 'table' => '#table-templates', 'errors' => $model->getErrors()  );	
        }	
	}
    
    /**
     * Finds the CalCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)  {
        //$id = CustomHelpers::decode($id);
		if (($model = OfferTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested template does not exist.');
        }
    }
    
    public function actionExport() {	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sprawy');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Klient')
			->setCellValue('B3', 'Nazwa')
			->setCellValue('C3', 'Status')
			->setCellValue('D3', 'Pracownicy');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3:D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4;  $data = []; $grants = $this->module->params['grants']; $where = []; $post = $_GET;
        $status = CalCase::listStatus('normal');
		//$data = Yii::$app->db->createCommand("select * from law_cal_case")->queryAll();
        if(isset($_GET['CalCaseSearch'])) {
            $params = $_GET['CalCaseSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(m.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            /*if(isset($params['authority_carrying']) && !empty($params['authority_carrying']) ) { 
                // $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(JSON_EXTRACT(authority_carrying, '$.name')) like '%".strtolower($params['authority_carrying'])."%'");
                 $fieldsDataQuery = $fieldsDataQuery->andWhere("json_get(authority_carrying,'name') LIKE '%".strtolower($params['authority_carrying'])."%'");
            }*/
            if(isset($params['id_employee_fk']) && !empty($params['id_employee_fk']) ) {
				array_push($where, "m.id in (select id_case_fk from law_case_employee where status = 1 and id_employee_fk = ".$params['id_employee_fk'].")");
            }
            if(isset($params['type_fk']) && !empty($params['type_fk']) ) {
				array_push($where, "type_fk = ".$params['type_fk']);
            }
            if(isset($params['id_dict_case_status_fk']) && !empty($params['id_dict_case_status_fk']) ) {
                if($params['id_dict_case_status_fk'] == 5) {
                    array_push($where, "id_dict_case_status_fk < 4");
                } else {
                    array_push($where, "id_dict_case_status_fk = ".$params['id_dict_case_status_fk']);
                }
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "m.id_customer_fk = ".$params['id_customer_fk']);
            }
        } else {
            array_push($where, "id_dict_case_status_fk != 4 and id_dict_case_status_fk != 6");
        }
        
        $fields = []; $tmp = [];
        $status = CalCase::listStatus('advanced');
		
		$sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'm.name';
        if( isset($post['sort']) && $post['sort'] == 'client' ) $sortColumn = 'c.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'm.id_dict_case_status_fk';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.name as name', 'id_dict_case_status_fk', 'm.status as status', 'c.name as cname', 'c.id as cid'])
            ->from('{{%cal_case}} as m')
            ->join(' JOIN', '{{%customer}} as c', 'c.id = m.id_customer_fk')
            ->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    if(count(array_intersect(["grantAll"], $grants)) == 0 && !$this->module->params['isAdmin'] && !$this->module->params['isSpecial']) {
			$query = $query->join(' JOIN', '{{%case_employee}} as ce', 'm.id = ce.id_case_fk and ce.status=1 and ce.id_employee_fk = '.$this->module->params['employeeId']);
		}
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'asc') );
		      		
		$rows = $query->all();
			
		if(count($rows) > 0) {
			foreach($rows as $record){ 
				$case = CalCase::findOne($record['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record['cname'] ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record['name']); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, isset($status[$record['id_dict_case_status_fk']]) ? $status[$record['id_dict_case_status_fk']] : 'nieznany'); 
				//$objPHPExcel->getActiveSheet()->setCellValue('D'. $i,''); 
                $employees = '';
                foreach($case->employees as $key => $employee) {
                    $employees .= $employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);
						
				$i++; 
			}  
		}
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);*/
		--$i;
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setTitle('Sprawy');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Sprawy'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');		
	}
    
    public function actionItems($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
        $items = []; $fields = []; $tmp = [];
        
        $colors1 = [1 => 'bg-red', 2 => 'bg-orange', 3 => 'bg-blue', 4 => 'bg-purple'];
        $colors2 = [1 => 'bg-teal', 2 => 'bg-green', 3 => 'bg-red'];
		$model = $this->findModel($id);
		$itemsData = $model->items;
	
		foreach($itemsData as $key => $value) {
            
            $actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/sale/template/iupdate', 'id' => $value->id]).'" class="btn btn-xs btn-default update" data-table="#table-items" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-pencil-alt\'></i>'.Yii::t('lsdd', 'Update').'" title="'.Yii::t('app', 'View').'"><i class="fa fa-pencil-alt"></i></a>';
                $actionColumn .= '<a href="'.Url::to(['/sale/template/idelete', 'id' => $value->id]).'" class="btn btn-xs btn-default remove" data-table="#table-items" data-form="event-form" data-target="#modal-grid-item" data-title="<i class=\'fa fa-trash\'></i>'.Yii::t('lsdd', 'Delete').'" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>';
            $actionColumn .= '</div>';
        
			$tmp['name'] = $value->name;
            $tmp['value'] = $value->default_value;

            $tmp['actions'] = ($value->status == 1) ? $actionColumn : '';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
			
			array_push($items, $tmp); $tmp = [];
		}	
		return $items;
	}
    
    public function actionIcreate($id) {
		
		$model = new OfferTemplateItem();
		$model->id_template_fk = $id;
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				return array('success' => true, 'refresh' => 'yes', 'table' => '#table-items', 'index' => 1);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formItem', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formItem', [ 'model' => $model, ]) ;	
		}
	}
   
	public function actionIupdate($id)  {
        $model = OfferTemplateItem::findOne($id);
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->save()) {
				
                $data['name'] = $model->name;
                $data['default_value'] = $model->default_value;
				
				return array('success' => true, 'row' => $data, 'refresh' => 'inline', 'table' => '#table-items', 'index' => $_GET['index']);	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formItem', [  'model' => $model,]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formItem', [ 'model' => $model, ]) ;	
		}
    }

	
	public function actionIdelete($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = OfferTemplateItem::findOne($id);
		if($model->delete()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $model->id, 'table' => '#table-items', 'refresh' => 'inline');	
        } else {
            return array('success' => false, 'alert' => 'Dane nie zostały usunię', );	
        }

       // return $this->redirect(['index']);
    }
    
    public function actionInfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($id == 0) {
            $html = '<div class="alert alert-warning">Proszę wybrać szablon</div>';
        } else {
            $model = $this->findModel($id);
            $html = $this->renderPartial('_template', ['model' => $model, 'create' => true]);
        }
        
        return ['html' => $html];
    }
    
    public function actionSave($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $offers = Offer::find()->where(['status' => 1, 'id_template_fk' => $model->id])->count();
        if($offers != 0) {
            $res = ['success' => false, 'alert' => 'Zmiany nie mogą zostać wprowadzone, ponieważ szablon jest powiązany z ofertami'];
            return $res;
        }
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = \backend\Modules\Offer\models\OfferTemplateItem::findOne($item['id']);
                $model->rank_no = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
}
