<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsMenu */
/*use kartik\sortable\Sortable;*/

use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([ 'id' => 'box-content' ]); ?>
    <div class="grid">	
        <div class="col-md-7">
            
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
            <?php echo  $form->field($model, 'describe')->textarea(['rows' => 6, 'class'=> 'mce-full'])->label(false) ?>	

            <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_content'])->label(false); ?>
             <div class="form-group align-right">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-content' : 'btn btn-primary main-form-btn btn-form-content']) ?>
            </div>

                
        </div>
        <div class="col-md-5">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Opcje dodatkowe</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'is_link')->checkBox([/*'label' => ...,*/ 'uncheck' => null, 'checked' => true]) ?>
                    <?= $form->field($model, 'link_type')->dropDownList( [1=>'Link wewnętrzny', 2=>'Link zewnętrzny'] )  ?>
                    <?= $form->field($model, 'link_href')->textInput(['maxlength' => 255])?>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Zdjęcie wyróżniające</h3>
                </div>
                <div class="panel-body">
                    <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                [
                                    'id' => 'imgAttachment',
                                    'model' => $model,
                                    'behaviorName' => 'coverBehavior',
                                    'apiRoute' => '/cms/cmswidget/imgAttachApi',
                                ]
                            
                            )
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>