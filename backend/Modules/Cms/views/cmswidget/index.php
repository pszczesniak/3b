<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dodatki';
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"cms-widgets-index", 'title'=>Html::encode($this->title))) ?>

	<?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-widgets" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Shop Product'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-widgets"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-widgets"

            data-toolbar="#toolbar-widgets" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-cms-widgets-search"
            data-url=<?= Url::to(['/cms/cmswidget/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="title"  data-sortable="true" >Tytuł</th>
                <th data-field="type_widget" data-align="center" data-sortable="true" >Typ</th>
                <th data-field="actions" data-events="actionEvents" data-align="center" data-width="100px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>
<?php $this->endContent(); ?>

<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modalConfirm', // <-- insert this modal's ID
	]);
 
	echo ' <div class="modal-body"><p>Czy na pewno chcesz usunąć ten element?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

	yii\bootstrap\Modal::end();
?> 