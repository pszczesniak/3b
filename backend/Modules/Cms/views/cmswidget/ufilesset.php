<?php
use yii\helpers\Html;
use backend\widgets\Fileswidget;

$this->title = 'Zbiór plików: '. $model->name;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dodatki'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-widget-update-2', 'title'=>Html::encode($this->title))) ?>

	<?= Fileswidget::widget(['typeId' => 7, 'parentId' => $model->id]) ?>

<?php $this->endContent(); ?>
