<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Aktualizacja zakładki: ' . $model->name);
$this->params['breadcrumbs'][] = ['label' => 'Widgety',  'url' => ['index', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Zarządzanie zakładkami',  'url' => ['uaccordion', 'id' => $model->fk_id]];
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-widget-accordion-item-update-1', 'title'=>Html::encode($this->title))) ?>

    <div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= \backend\widgets\TranslateWidget::widget(['id' => $model->id, 'action' => '/cms/cmswidget/aitranslate', 'tinyMce' => true]) ?>
            <?= Html::a('<i class="fa fa-undo"></i>'.Yii::t('app', 'Powrót do listy zakładek', [
				'modelClass' => 'Cms Widget',
			]), ['uaccordion', 'id' => $model->fk_id], ['class' => 'btn btn-info btn-icon btn-sm']) ?>

		</div>
    </div>
    <?= $this->render('_uaccordion_base',array('model'=>$model)) ?></div>
    
<?php $this->endContent(); ?>

<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>