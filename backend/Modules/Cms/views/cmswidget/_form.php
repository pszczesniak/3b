<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-widget-form">

    <?php /*$form = ActiveForm::begin();*/ ?>

    <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "<div class=\"grid\">
                                            <div class=\"col-xs-6\">{label}</div>\n<div class=\"col-xs-6 text-right\">{hint}</div>
                                        \n<div class=\"col-xs-12\">{input}</div>
                                        </div>",
                    ],
                ]); ?>
	<?= $form->errorSummary($model); ?>
	<?php /* $form->field($model, 'type_widget')->textInput()*/ ?>
    ffa-pencil-alt
    <div id="widget-types">
		<input type="radio" id="1" class="rad" name="CmsWidget[type_widget]" value="1"/>
		<label for="1">Blok tekstowy</label>
		<input type="radio" id="2" class="rad" name="CmsWidget[type_widget]" value="2"/>
		<label for="2">Zakładki</label>
		<input type="radio" id="3" class="rad" name="CmsWidget[type_widget]" value="3"/>
		<label for="3">Zbiór plików</label>
		<?php /*$form->field($model, 'type_widget')-> listBox(
			array('1'=>'1',2=>'2',3=>3,4=>4,5=>5),
			array('prompt'=>'Select')
			);*/ ?>
    </div>
	<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?php /* $form->field($model, 'title_langs')->textarea(['rows' => 6]) */ ?>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
