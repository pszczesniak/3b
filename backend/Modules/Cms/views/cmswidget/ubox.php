<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsMenu */
/*use kartik\sortable\Sortable;*/

use yii\widgets\ActiveForm;

use dosamigos\tinymce\TinyMce;

use common\models\cms\CmsCategory;
use common\models\cms\CmsPage;

$this->title = 'Blok tekstowy: '. $model->name;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dodatki'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-widget-update-1', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
            <?= \backend\widgets\TranslateWidget::widget(['id' => $model->id, 'action' => '/cms/cmswidget/btranslate', 'tinyMce' => true]) ?>
        </div>
    </div>
    <br />
    <?= yii\base\View::render('_ubox_base',array('model'=>$model));  ?>
    
<?php $this->endContent(); ?>
<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>