<?php

use yii\helpers\Html;
use yii\helpers\Url;

use backend\widgets\Relateditemswidget;


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cms Menu',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dodatki'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-index', 'title'=>Html::encode($this->title))) ?>

    <div class="info-message"></div>                    
    <?= Relateditemswidget::widget(['createUrl' => Url::to(['/cms/cmswidget/createaccordionitem', 'id'=>$model->id]),
                            'createLabel' => 'Nowa zakładka',
                            'createIcon' => 'plus',
                            'createStyle' => 'success',
                            //'buttonId' => 'production-status',
                            'dataUrl' =>  Url::to(['/cms/cmswidget/accordionitems', 'id'=>$model->id]),
                            'tableId' => "table-items",
                            'modalId' => "modal-grid-item",
                            'gridViewModal' => 'gridViewModal',
                            'tableOptions' => 'data-toggle="table" data-show-refresh="true" data-show-toggle="false"  data-show-columns="false" data-show-export="false"  data-filter-control="false" ',
                            'otherButtons' => '<a class="btn btn-primary btn-icon none" id="save-table-items-move" href="'.Url::to(['/cms/cmswidget/accordionitemssave', 'id' => $model->id]).'" data-table="#table-items"><i class="glyphicon glyphicon-floppy-disk"></i>Zapisz zmiany</a>',
                            'columns' => [
                                0 => ['name' => Yii::t('lsdd', 'ID'), 'data' => 'data-field="id" data-visible="false"' ],
                                1 => ['name' => Yii::t('app', 'Name'), 'data' => 'data-field="name"' ],
                                2 => ['name' => '', 'data' => 'data-field="action" data-align="center" data-events="actionEvents"' ],
                                3 => ['name' => '', 'data' => 'data-field="move" data-align="center" data-events="actionEvents"' ],
                            ]   
                    ]) ?>
    <small class="text--blue"><i class="fa fa-info-circle"></i>&nbsp; na stronie głównej poratlu pojawią się 3 pierwsze wpisy</small>
<?php $this->endContent(); ?>
