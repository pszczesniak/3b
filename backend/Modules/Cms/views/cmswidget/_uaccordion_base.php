<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsMenu */
/*use kartik\sortable\Sortable;*/

use yii\widgets\ActiveForm;
?>

    <?php /*$form = ActiveForm::begin();*/ ?>

    <?php $form = ActiveForm::begin([ ]); ?>
	<?= $form->errorSummary($model); ?>
	<?php /* $form->field($model, 'type_widget')->textInput()*/ ?>
    <?= $form->field($model, 'status')->checkBox()  ?>
	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?php echo  $form->field($model, 'describe')->textarea(['rows' => 6, 'class'=> 'mce-full'])->label(false) ?>	
    <?php /* $form->field($model, 'title_langs')->textarea(['rows' => 6]) */ ?>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? 'Utw�rz' : 'Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>