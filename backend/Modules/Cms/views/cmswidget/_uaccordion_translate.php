<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([ 'action' => Url::to(['/translations/update', 'id' => $model->id]), 'id' => 'page-translate','options' => ['class' => 'ajaxform'] ]); ?>
    <div class="modal-body">
        <div class="form-group field-lang">
            <label for="lang" class="control-label">Język</label>
            <select name="lang" class="form-control">
                <?php 
                    foreach(\Yii::$app->params['languages'] as $key => $value) {
                        echo ($key == $lang) ? '<option value="'.$key.'">'.$value.'</option>' : '';
                    }
                ?>
            </select>

            <div class="help-block"></div>
        </div>    
         <div class="form-group field-name">
            <label for="name" class="control-label">Tytuł</label>
            <input type="text" maxlength="255" value="<?= $model->attributes['name'] ?>" name="name" class="form-control" id="name">

            <div class="help-block"></div>
        </div>
        <div class="form-group field-describe">
            <label for="describe" class="control-label">Treść</label>
            <textarea  name="describe" class="form-control mce-full" id="describe"><?= $model->attributes['describe'] ?></textarea>

            <div class="help-block"></div>
        </div>
    </div>
    <div class="modal-footer"> 
        <?= Html::submitButton('<i class="fa fa-save"></i>Zapisz zmiany', ['class' =>  'btn btn-icon btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>

<?php ActiveForm::end(); ?>