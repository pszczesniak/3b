<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
    <?php /*$form = ActiveForm::begin();*/ ?>

    <?php $form = ActiveForm::begin([
        //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
        'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-widgets", 'data-input' => '.page-widget'],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
           // 'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
            /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
        ],
    ]); ?>
	<?php /* $form->errorSummary($model);*/ ?>
	<?php /* $form->field($model, 'type_widget')->textInput()*/ ?>
    <div id="widget-types">
		<input type="radio" id="1" class="rad" name="CmsWidget[type_widget]" value="1"/>
		<label for="1">Blok tekstowy</label>
		<input type="radio" id="2" class="rad" name="CmsWidget[type_widget]" value="2"/>
		<label for="2">Zakładki</label>
		<input type="radio" id="3" class="rad" name="CmsWidget[type_widget]" value="3"/>
		<label for="3">Zbiór plików</label>
		<?php /*$form->field($model, 'type_widget')-> listBox(
			array('1'=>'1',2=>'2',3=>3,4=>4,5=>5),
			array('prompt'=>'Select')
			);*/ ?>
    </div>
	<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?php /* $form->field($model, 'title_langs')->textarea(['rows' => 6]) */ ?>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
