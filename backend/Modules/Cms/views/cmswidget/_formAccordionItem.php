<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-widget-form">

    <?php /*$form = ActiveForm::begin();*/ ?>

    <?php $form = ActiveForm::begin([
                    'options' => ['class' => 'modal-grid', 'data-table' => '#table-items', 'data-target' => '#modal-grid-item'],
                    'fieldConfig' => [
                        'template' => "<div class=\"row\">
                                            <div class=\"col-xs-6\">{label}</div>\n<div class=\"col-xs-6 text-right\">{hint}</div>
                                        \n<div class=\"col-xs-12\">{input}</div>
                                        </div>",
                    ],
                ]); ?>
	<?= $form->errorSummary($model); ?>
	<?php /* $form->field($model, 'type_widget')->textInput()*/ ?>
    <?= $form->field($model, 'status')->checkBox()->label('Opublikuj')  ?>
	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'describe')->textarea(['rows' => 6]) ?>
    <?php /* $form->field($model, 'title_langs')->textarea(['rows' => 6]) */ ?>

    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
