<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsWidget */

$this->title = 'Kreator dodatku';
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => 'Dodatki', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-index', 'title'=>Html::encode($this->title))) ?>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
	
<?php $this->endContent(); ?>

