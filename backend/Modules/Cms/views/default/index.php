<?php
	use yii\helpers\Url;
    use yii\helpers\Html;
    
    use backend\widgets\Shortcutwidget;
    use backend\widgets\gtw\Panelstatswidget;
    use backend\widgets\Panelpercentwidget;
    use backend\widgets\Lastofferwidget;
    use backend\widgets\Lastcommentwidget;
    use backend\widgets\LastLoans;
    use common\widgets\Alert;
    
    $this->title = 'Witamy '.Yii::$app->user->identity->username;
    $this->params['breadcrumbs'][] = 'Info';
?>
<div class="cms-default-index  ">
    <?= Alert::widget() ?>
	<?php if (\Yii::$app->user->isGuest) { ?>
		<div class="container text-center">
            <h1>Witamy w panelu admina</h1>
            <a href="<?= Url::to(['/site/login']) ?>" class="btn btn-lg btn-success">Zaloguj się do systemu</a>
        </div>
	<?php } else {  ?>
		<div class="container text-center">
            <h1>Witamy w panelu admina</h1>
            <!--<a href="<?= Url::to(['/apl/aplloan/index']) ?>" class="btn btn-lg btn-warning" title="Wnioski"><i class="fa fa-folder-open fa-3x"></i></a>
            <a href="<?= Url::to(['/cms/cmspage/index']) ?>" class="btn btn-lg btn-primary" title="Strony serwisu"><i class="fa fa-font fa-3x"></i></a>
            <a href="<?= Url::to(['/newsletter/newslettertemplate/index']) ?>" class="btn btn-lg btn-info"><i class="fa fa-envelope fa-3x"></i></a>-->
        </div>
        <div class="main-container">
            <div class="padding-md"
                <?= Panelstatswidget::widget([]) ?>
            </div>
        </div>
		<br /> <hr />
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"main-data", 'title'=>Html::encode('Parametry pożyczki'))) ?>
                    <?= $this->render('_form', array('model'=>$model)) ?>
                <?= $this->endContent(); ?>
            </div>
            <div class="col-sm-6 col-xs-12">
                <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"main-data", 'title'=>Html::encode('Wnioski do rozpatrzenia'))) ?>
                    <?= LastLoans::widget([]) ?>
                <?= $this->endContent(); ?>
            </div>
        </div>
	<?php } ?>
</div>



