<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Shop\models\ShopCategoryFactor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-factor-form">
    <?php $form = ActiveForm::begin(['id' => 'item-form-field', 
									'action' => Url::to(['/cms/default/createajax', 'id' => $type]),
									'options' => [ 'data-pjax' => false, 'enableAjaxValidation'=>false, 'enableClientValidation'=>true, 'data-table' => '#table-steps', 'data-target' => "#modal-grid-item"],
								 ]); ?>

        <?= $form->field($model, 'describe')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'index')->hiddenInput(['maxlength' => true])->label(false) ?>
        <div class="form-group align-right">
            <?= Html::submitButton('Potwiedź zmiany', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
