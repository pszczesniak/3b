<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-site-form">
    <div id="validation-form" class="alert"></div>
	
	<?php $form = ActiveForm::begin(['action' => Url::to(['/cms/default/save']), 'id' => 'page-basic', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'] ]); ?>

        <fieldset><legend>Kwota pożyczki</legend>
            <div class="row">
                <div class="col-sm-6"><?= $form->field($model, 'amountFrom')->textInput(['maxlength' => 255])->label('od') ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'amountTo')->textInput(['maxlength' => 255])->label('do') ?></div>
            </div>
		</fieldset>
        <fieldset><legend>Okres kredytowania</legend>
            <div class="row">
                <div class="col-sm-6"><?= $form->field($model, 'periodFrom')->textInput(['maxlength' => 255])->label('od') ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'periodTo')->textInput(['maxlength' => 255])->label('do') ?></div>
            </div>
		</fieldset>
		<div class="form-group align-right">
			<?= Html::submitButton('Zapisz zmiany', ['class' => 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end(); ?>
   
</div>
