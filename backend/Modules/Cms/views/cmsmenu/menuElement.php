<?php
	use yii\helpers\Url;
	if($model->fk_type_id == 1) { $style = 'teal'; $label = 'strona'; }
	if($model->fk_type_id == 2) { $style = 'orange'; $label = 'kategoria'; }
	if($model->fk_type_id == 3) { $style = 'blue'; $label = 'link'; }
	if($model->fk_type_id == 4) { $style = 'purple'; $label = 'separator'; }
	if($model->fk_type_id == 5) { $style = 'green'; $label = 'galeria'; }
    if($model->fk_type_id == 6) { $style = 'warning'; $label = 'katalog'; }
?>
<li class="dd-item dd3-item" data-id="<?= $model->id ?>">
	<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>
	<div class="dd3-content">
		<span class="dd3-content-title"><?= $model->name ?></span>
		<div class="item-action-edit bg-<?= $style ?>" id="item-<?= $model->id ?>"> 
			<p class="click-text">  <?= $label ?> <span class="arrow"></span> </p>
		</div>
	</div>
	<div class="item-edit" id="item-edit-<?= $model->id ?>">
		<form method="POST" action="<?= Url::to(['/cms/cmsmenu/iupdate', 'id' => $model->id]) ?>" class="menu-item-update" id="u-<?= $model->id ?>">
            <div class="help-block-<?= $model->id ?>"></div>
            <div class="form-group field-cmsmenuitem-name required">
                <label for="cmsmenuitem-name" class="control-label">Opis w menu</label>
                <input type="text" maxlength="255" name="CmsMenuItem[name]" class="form-control" id="uitem-<?= $model->id ?>" value="<?= $model->name ?>">
            </div>
    
            <?php if($model->fk_type_id == 1) { ?>
                <div class="form-group field-cmsmenu-fk_cms_element_id required">
                    <select type="text" maxlength="255" name="CmsMenuItem[fk_cms_element_id]" class="form-control" >
                        <?php foreach($itemsPages as $key=>$value) {
                            $selected = '';
                            if($key == $model->fk_cms_element_id) $selected = ' selected="selected" ';
                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                         } ?>
                    </select>
                </div>
            <?php } ?>
            <?php if($model->fk_type_id == 2) { ?>
                <div class="form-group field-cmsmenu-fk_cms_element_id required">
                    <select type="text" maxlength="255" name="CmsMenuItem[fk_cms_element_id]" class="form-control" >
                        <?php foreach($itemsCategories as $key=>$value) {
                            $selected = '';
                            if($key == $model->fk_cms_element_id) $selected = ' selected="selected" ';
                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                         } ?>
                    </select>
                </div>
            <?php } ?>
            <?php if($model->fk_type_id == 5) { ?>
                <div class="form-group field-cmsmenu-fk_cms_element_id required">
                    <select type="text" maxlength="255" name="CmsMenuItem[fk_cms_element_id]" class="form-control" >
                        <?php foreach($itemsGalleries as $key=>$value) {
                            $selected = '';
                            if($key == $model->fk_cms_element_id) $selected = ' selected="selected" ';
                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                         } ?>
                    </select>
                </div>
            <?php } ?>
            <?php if($model->fk_type_id == 6) { ?>
                <div class="form-group field-cmsmenu-fk_cms_element_id required">
                    <select type="text" maxlength="255" name="CmsMenuItem[fk_cms_element_id]" class="form-control" >
                        <?php foreach($itemsCatalogs as $key=>$value) {
                            $selected = '';
                            if($key == $model->fk_cms_element_id) $selected = ' selected="selected" ';
                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                         } ?>
                    </select>
                </div>
            <?php } ?>
            <?php if($model->fk_type_id == 3) { ?>
                <div class="form-group field-cmsmenu-item_name required">
                    <label for="cmsmenuitem-link_href" class="control-label">Link</label>
                    <input type="text" maxlength="255" name="CmsMenuItem[link_href]" class="form-control" id="uitem-<?= $model->id ?>" value="<?= $model->link_href ?>">
                </div>
            <?php } ?>
            <div class="form-group align-right">
                <!--<a data-confirm="Czy na pewno zapisać zmiany?" data-method="post" data-remote="true" title="Update" href="<?= Url::to(['/cms/cmsmenu/iupdate', 'id' => $model->id]) ?>" id="u-<?= $model->id ?>" class="menu-item-update label label-success">Zapisz</a>-->
                <?= \backend\widgets\TranslateWidget::widget(['id' => $model->id, 'action' => '/cms/cmsmenu/translate']) ?>
                <input type="submit" value="Zapisz" class="btn btn-primary btn-sm"></input>
                <a data-confirm="Czy na pewno usunąć ten element?" data-method="post" data-remote="true" title="Delete" href="<?= Url::to(['/cms/cmsmenu/idelete', 'id' => $model->id]) ?>" id="d-<?= $model->id ?>" class="modalConfirm btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
            </div>
        </form>
	</div>
	<?php 
        if(is_array($model->children)) {
            if(count($model->children) > 0 ) {
                echo  $this->render('menu', [ 'items' => $model->children, 'level' => ($level+1), 'itemsPages' => $itemsPages, 'itemsCategories' => $itemsCategories, 'itemsGalleries' => $itemsGalleries, 'itemsCatalogs' => $itemsCatalogs ]) ;
            }
        }
    ?>
</li>