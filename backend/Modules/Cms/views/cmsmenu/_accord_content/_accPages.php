<div class="content-page-all-select custom-inputs"><input type="checkbox" class="selecctall" id="checkbox_page"/> <label for="checkbox_page">Zaznacz wszystkie strony</label></div>
<div class="content-page-select custom-inputs">
	<ul id="menu-list-page" class="chk-container ">
		<?php
			foreach($items as $key=>$value){
				//$tmpData = []; $tmpData['content'] = $value; $tmpData['itemOptions'] = ['id'=>$key];
				//array_push($itemsPages, $tmpData);
				echo '<li><input class="checkbox_page" id="chp'.$key.'" type="checkbox" name="check[]" value="'.$key.'"><label for="chp'.$key.'">'.$value.'</label></li>';
			}
		?>
	</ul> 
</div>
<div class="align-right">
	<button id="menu-selected-page" class="btn btn-primary chk-selected" 
			data-id=<?= $model->id ?> 
			data-target=".checkbox_page" 
			data-url=<?= yii\helpers\Url::to(['/cms/cmsmenu/icreate']) ?>
			data-type="1" >Dodaj strony do menu
	</button>
</div>