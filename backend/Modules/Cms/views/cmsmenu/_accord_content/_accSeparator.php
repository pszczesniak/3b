<form class="form-horizontal">
    <div class="form-group form-group-sm">
		<label class="col-sm-4 control-label" for="element-name">Nazwa elementu</label>
		<div class="col-sm-8">
			<input class="form-control" type="text" id="element-name-separator" placeholder="Nazwa elementu">
			<div class="help-block" id="separator-error"></div>
		</div>
	</div>
	<div class="align-right">
		<button data-id="<?= $model->id ?>" id="menu-selected-separator" class="btn btn-primary" data-url="<?= yii\helpers\Url::to(['/cms/cmsmenu/icreate']) ?>" data-type="4" >Dodaj element do menu</button>

	</div>
</form>
