<form class="form-horizontal">
    <div class="form-group form-group-sm">
		<label class="col-sm-4 control-label" for="element-name">Nazwa elementu</label>
		<div class="col-sm-8">
			<input class="form-control" type="text" id="element-name" placeholder="Nazwa elementu">
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label class="col-sm-4 control-label" for="element-url">Adres URL</label>
		<div class="col-sm-8">
			<input class="form-control" type="text" id="element-url" placeholder="Adres Url">
			<div class="help-block" id="link-error"></div>
		</div>
	</div>
	<div class="align-right">
		<button data-id="<?= $model->id ?>" id="menu-selected-link" class="btn btn-primary" data-url="<?= yii\helpers\Url::to(['/cms/cmsmenu/icreate']) ?>" data-type="3" >Dodaj element do menu</button>
	</div>
</form>