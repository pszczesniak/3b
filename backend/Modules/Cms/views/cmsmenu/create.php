<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsMenu */

$this->title = Yii::t('app', 'Kreator nawiagacji');
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer nawigacji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"cms-page-index", 'title'=>Html::encode($this->title))) ?>
	<div class="view-window-content">
		<div class="row">
			<div div class="col-md-12">
				<?= $this->render('_form', [
					'model' => $model,
				]) ?>
			</div>
		</div>

	</div>
<?php $this->endContent(); ?>

