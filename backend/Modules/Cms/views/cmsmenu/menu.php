<?php
	use yii\helpers\Url;
?>
<ol <?php if($level == 0) echo 'id="menu-items-list"'; ?> class="dd-list">
	<?php
		foreach($items as $key=>$value) { 

			//echo $this->render('menuElement', [ 'title' => $value->name, 'type' => $value->fk_type_id, 'id' => $value->id, 'items' => $value->children, 'level' => $level ]); 	
            echo $this->render('menuElement', [ 'model' => $value, 'level' => $level, 'itemsPages' => $itemsPages, 'itemsCategories' => $itemsCategories, 'itemsGalleries' => $itemsGalleries, 'itemsCatalogs' => $itemsCatalogs ]);
		}
	?>
</ol>


