<?php
	use yii\helpers\Url;
	if($type == 1) { $style = 'primary'; $label = 'strona'; }
	if($type == 2) { $style = 'warning'; $label = 'kategoria'; }
	if($type == 3) { $style = 'info'; $label = 'link'; }
	if($type == 4) { $style = 'danger'; $label = 'separator'; }
	if($type == 5) { $style = 'success'; $label = 'galeria'; }
?>
<li class="dd-item dd3-item" data-id="<?= $id ?>">
	<div class="dd-handle dd3-handle"><span class="glyphicon glyphicon-move"></span></div>
	<div class="dd3-content">
		<span class="dd3-content-title"><?= $title ?></span>
		<div class="item-action-edit label-<?= $style ?>" id="item-<?= $id ?>"> 
			<p class="click-text">  <?= $label ?> <span class="arrow"></span> </p>
		</div>
	</div>
	<div class="item-edit" id="item-edit-<?= $id ?>">
		<div class="form-group field-cmsmenuitem-name required">
			<label for="cmsmenuitem-name" class="control-label">Tytuł</label>
			<input type="text" maxlength="255" name="CmsMenuItem[name]" class="form-control" id="uitem-<?= $id ?>" value="<?= $title ?>">
			<div class="help-block"></div>
		</div>
		<div class="form-group align-right">
			<a data-confirm="Czy na pewno zapisać zmiany?" data-method="post" data-remote="true" title="Update" href="<?= Url::to(['/cms/cmsmenu/iupdate', 'id' => $id]) ?>" id="u-<?= $id ?>" class="menu-item-update label label-success">Zapisz</a>						
			<a data-confirm="Czy na pewno usunąć ten element?" data-method="post" data-remote="true" title="Delete" href="<?= Url::to(['/cms/cmsmenu/idelete', 'id' => $id]) ?>" id="d-<?= $id ?>" class="menu-item-delete label label-danger">Usuń</a>
		</div>
	</div>
	<?php if(count($items) > 0 ) {
		echo  $this->render('menu', [ 'items' => $items, 'level' => ($level+1) ]) ;
	} ?>
</li>