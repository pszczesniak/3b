<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\Modules\Cms\models\CmsMenuType;

/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-menu-form ">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
	<?= $form->field($model, 'fk_type_id')->dropDownList( ArrayHelper::map(CmsMenuType::find()->where('type_element = :type', ['type'=>2])->all(), 'id', 'name') );  ?>

    <?php /* $form->field($model, 'title_langs')->textarea(['rows' => 6])*/ ?>

	<?= $form->field($model, 'show_title')->checkBox(['uncheck' => null, 'checked' => true]) ?>

	<?php /*$form->field($model, 'showAllPages')->checkBox(['uncheck' => null, 'checked' => true])*/ ?>


    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
