<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsMenuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-menu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fk_type_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'title_langs') ?>

    <?= $form->field($model, 'rank') ?>

    <?php // echo $form->field($model, 'showTitle') ?>

    <?php // echo $form->field($model, 'showAllPages') ?>

    <?php // echo $form->field($model, 'isSystem') ?>

    <?php // echo $form->field($model, 'html_options') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
