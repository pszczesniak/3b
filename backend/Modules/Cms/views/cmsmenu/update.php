<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app',  $model->title);
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer nawigacji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title/*, 'url' => ['view', 'id' => $model->id]*/];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="cms-menu-update">
    <!--<div class="row">
		<div class="col-md-12">
			<?= Html::a('<i class="fa fa-plus-circle"></i>'.Yii::t('app', 'Create Cms Menu', [
				'modelClass' => 'Cms Category',
			]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
			<?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Manager', [
				'modelClass' => 'Cms Category',
			]), ['create'], ['class' => 'btn btn-warning btn-icon']) ?>
		</div>
    </div>
	<hr />-->
    <div class="grid">
		<div class="col-md-6">
			<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-menu-update-1', 'title'=>Html::encode($this->title))) ?>

				<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="true" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Strony</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accPages',array('model'=>$model, 'items' => $itemsPages)) ?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="false" data-toggle="collapse" data-parent="#accordion" href="#collapse2">Kategorie</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse ">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accCategories',array('model'=>$model, 'items' => $itemsCategories)) ?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="false" data-toggle="collapse" data-parent="#accordion" href="#collapse3">Link zewnętrzny</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse ">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accLink',array('model'=>$model)) ?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="false" data-toggle="collapse" data-parent="#accordion" href="#collapse4">Separator</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse ">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accSeparator',array('model'=>$model)) ?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="false" data-toggle="collapse" data-parent="#accordion" href="#collapse5">Galerie</a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse ">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accGaleries',array('model'=>$model, 'items' => $itemsGalleries)) ?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-expanded="false" data-toggle="collapse" data-parent="#accordion" href="#collapse6">Kategorie produktów</a>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse ">
                            <div class="panel-body"><?= yii\base\View::render('_accord_content/_accShop',array('model'=>$model, 'items' => $itemsCatalogs)) ?></div>
                        </div>
                    </div>
            
                </div>
                <?php /*echo yii\jui\Accordion::widget([
					'items' => [
						[
							'header' => 'Strony',
							'content' =>  yii\base\View::render('_accord_content/_accPages',array('model'=>$model, 'items' => $itemsPages)),
						],
						[
							'header' => 'Kategorie',
							'headerOptions' => ['tag' => 'h3'],
							'content' =>  yii\base\View::render('_accord_content/_accCategories',array('model'=>$model, 'items' => $itemsCategories)),
							'options' => ['tag' => 'div'],
						],
						[
							'header' => 'Link zewnętrzny',
							'content' =>  yii\base\View::render('_accord_content/_accLink',array('model'=>$model)),
						],
						[
							'header' => 'Separator',
							'content' =>  yii\base\View::render('_accord_content/_accSeparator',array('model'=>$model)),
						],
						[
							'header' => 'Galerie',
							'content' =>  yii\base\View::render('_accord_content/_accGaleries',array('model'=>$model, 'items' => $itemsGalleries)),
						],
                        [
							'header' => 'Kategorie produktów',
							'content' =>  yii\base\View::render('_accord_content/_accShop',array('model'=>$model, 'items' => $itemsCatalogs)),
						],
					],
					'options' => ['tag' => 'div'],
					'itemOptions' => ['tag' => 'div'],
					'headerOptions' => ['tag' => 'h3'],
					'clientOptions' => ['collapsible' => false],
				]);*/
				?>
			<?php $this->endContent(); ?>
			
		</div>
		<div class="col-md-6 ">
			<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-menu-update-2', 'title'=>Html::encode($this->title))) ?>
					<div class="text-right dd-actions">
						<?= Html::a('<i class="glyphicon glyphicon-floppy-disk"></i>'.Yii::t('app', 'Save'), ['isave'], [
							'class' => 'btn btn-primary btn-icon', 'id' => 'save-items',
							'data' => [
								'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
								'method' => 'post',
							],
						]) ?>
						<?php /* ($model->isSystem==1) ? '' : Html::a('<i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
							'class' => 'btn btn-danger btn-icon',
							'data' => [
								'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
								'method' => 'post',
							],
						]) */ ?>
                        <?php /*Html::a('<i class="fa fa-language"></i>'.Yii::t('app', 'Translate'), Url::to(['translate', 'id' => $model->id]), [
							'class' => 'btn btn-warning btn-icon gridViewModal', 'id' => 'translate-items', 'data-target' => "#modal-translate",
						
						])*/ ?>
					</div><div class="clear"></div>
					<hr />
					<div  class="dd" data-alert="Przed wyjściem z arkusza zapisz zmiany!">
						<?php echo $buildMenu; ?>
					</div>

			<?php $this->endContent(); ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<!-- Render translate modal -->    
<?php
	yii\bootstrap\Modal::begin([
	  'header' => Yii::t('lsdd', 'Tłumaczenia'),
	  'id' => 'modal-translate', // <-- insert this modal's ID
	  'class' =>'modal',
	  'size' => 'modal-md',
	]);

	echo '<div class="modalContent"></div>';

	yii\bootstrap\Modal::end();
?>