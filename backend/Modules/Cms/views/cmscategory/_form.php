<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-category-form tab-block">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Podstawowe</a></li>
        <?php if(!$model->isNewRecord) { ?>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Treść</a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">SEO</a></li>
        <?php } ?>
    </ul>

      <!-- Tab panes -->
   <div class="tab-content tab-content-normal">
        <div role="tabpanel" class="tab-pane active" id="tab1"><?= $this->render('tabs/_tabBasic',array('model'=>$model)) ?></div>
        <?php if(!$model->isNewRecord) { ?>
        <div role="tabpanel" class="tab-pane" id="tab2"><?= $this->render('tabs/_tabContent',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab3"><?= $this->render('tabs/_tabSeo',array('model'=>$model)) ?></div>
        <?php } ?>
    </div>
</div>
