<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;


//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([ 'id' => 'category-content' ]); ?>
    <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_content'])->label(false) ?>
	<?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?>	
	
	<?php echo  $form->field($model, 'content')->textarea(['rows' => 6, 'class'=> 'mce-full'])->label(false) ?>	

    <div class="form-group align-right">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>	

