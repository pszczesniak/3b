<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


use backend\Modules\cms\models\CmsPageStatus;
use common\models\cms\CmsCategory;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="grid">
    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

        <?php $form = ActiveForm::begin([ 'id' => 'category-basic', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'] ]); ?>

        <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_basic'])->label(false) ?>
        <div class="grid">
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'template_view')->dropDownList( \backend\Modules\Cms\models\CmsPage::getTemplateView() )  ?></div>
            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'list_view')->dropDownList( \backend\Modules\Cms\models\CmsCategory::getListView() )  ?></div>            
        </div>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'title_slug')->textInput(['maxlength' => 255]) ?>
        <?php /*$form->field($model, 'isCalendar')->checkBox(['label' => ..., 'uncheck' => null, 'checked' => true])*/ ?>

        <div class="form-group align-right">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <?php if(!$model->isNewRecord) { ?>
    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
        <div class="panel panel-warning">
            <div class="panel-heading">Historia</div>
            <!--<div class="panel-body">-->
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'hits',
                        'created_at',
                        [                      
                            'attribute' => 'created_by',
                            'format'=>'raw',
                            'value' =>  \common\models\User::findOne($model->created_by)->username
                        ], 
                        [                      
                            'attribute' => 'updated_at',
                            'format'=>'raw',
                            'label' => 'Ostanio aktualizowano',
                            'visible' =>  ($model->updated_by)  ? true : false,
                        ],  
                        [                      
                            'attribute' => 'updated_by',
                            'format'=>'raw',
                            'label' => 'Aktualizowane przez',
                            'visible' =>  ($model->updated_by) ? true : false,
                            'value' =>  ($model->updated_by) ? \common\models\User::findOne($model->updated_by)->username : ''
                        ], 
                        [                      
                            'attribute' => 'deleted_at',
                            'format'=>'raw',
                            'label' => 'Usunięto',
                            'visible' =>  ($model->deleted_by)  ? true : false,
                        ],  
                        [                      
                            'attribute' => 'deleted_by',
                            'format'=>'raw',
                            'label' => 'Usunięto przez',
                            'visible' =>  ($model->deleted_by) ? true : false,
                            'value' =>  ($model->deleted_by) ? \common\models\User::findOne($model->deleted_by)->username : ''
                        ], 
                    ],
                ]) ?>
            <!--</div>-->
        </div>
    </div>
    <?php } ?>
</div>
