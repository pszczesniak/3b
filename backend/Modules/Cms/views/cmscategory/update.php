<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsCategory */

$this->title = $model->title;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-category-update', 'title'=>Html::encode($this->title))) ?>
    <div class="grid">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= Html::a('<i class="fa fa-plus-circle"></i>'.Yii::t('app', 'Create Cms Category', [
				'modelClass' => 'Cms Category',
			]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm']) ?>
			<?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Manager', [
				'modelClass' => 'Cms Category',
			]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm']) ?>
            <?= \backend\widgets\TranslateWidget::widget(['id' => $model->id, 'action' => '/cms/cmscategory/translate', 'tinyMce' => true]) ?>
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger btn-icon btn-sm deleteConfirm', ]) ?>
		</div>
		<div div class="col-md-12">
			<?= $this->render('_form', [
				'model' => $model,
			]) ?>
		</div>
    </div>
<?php $this->endContent(); ?>
