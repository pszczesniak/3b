<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsCategory */

$this->title = 'Nowa kategoria';
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kategorie'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-category-create', 'title'=>Html::encode($this->title))) ?>    <div class="row">

    <div class="grid">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Manager', [
				'modelClass' => 'Cms Category',
			]), ['create'], ['class' => 'btn btn-warning btn-icon']) ?>
		</div>
		<div div class="col-md-12">
			<?= $this->render('_form', [
				'model' => $model,
			]) ?>
        </div>
	</div>
<?php $this->endContent(); ?>
