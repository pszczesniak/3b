<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsGallery */
$this->title = $model->title;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer galerii'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-gallery-index', 'title'=>Html::encode($this->title))) ?>
    <div class="text-right">
        <?= Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Nowa galeria', [
            'modelClass' => 'Cms Gallery',
        ]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm btn-responsive']) ?>
        <?= Html::a('<i class="fa fa-image"></i>'.Yii::t('app', 'Zdjęcia', [
            'modelClass' => 'Cms Gallery',
        ]), ['images', 'id' => $model->id], ['class' => 'btn btn-info btn-icon btn-sm btn-responsive']) ?>
       <?php /*Html::a('<i class="fa fa-trash-o"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger btn-icon btn-sm btn-responsive',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		])*/ ?>
        <?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Rejestr galerii', [
            'modelClass' => 'Cms Gallery',
        ]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm btn-responsive']) ?>
    </div>
    <hr />
	<?=  $this->render('_form', [
		'model' => $model,
	]) ?>
<?php $this->endContent(); ?>

