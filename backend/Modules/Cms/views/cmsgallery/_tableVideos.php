<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="ajax-table-items">	
	<div id="toolbar-files">  </div>
	
	<table  class="table table-striped table-items"  id="table-files"
			data-toolbar="#toolbar-files" 
			data-toggle="table" 
            data-refresh="true"
			data-url=<?= Url::to(['/files/index', 'id'=>$id, 'type'=>$type]) ?>>
		<thead>
			<tr>
				<th><?= Yii::t('lsdd', 'Name') ?></th>
				<th <?= ($type != 1)?'data-width="25"':'data-width="100"' ?> data-align="center"><i class="glyphicon glyphicon-download-alt"></i></th>
				<th data-width="150" data-align="center"></th>
			</tr>
		</thead>
		<tbody class="ui-sortable">

		</tbody>
		 <!-- <tfoot>
			<tr>
			  <td colspan="4" class="form-inline col-xs-12" role="form">
				<div class="form-group"><input type="text" id="bed-description" class="form-control"></div>
				<div class="form-group"><button id="add-bed-btn" class="btn btn-sm btn-block btn-primary"><span class="glyphicon glyphicon-plus"></span>Add</button></div>
			  </td>
			</tr>
		  </tfoot>-->
	</table>
</div>