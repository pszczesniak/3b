<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-gallery-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= ($model->is_system == 0) ? $form->field($model, 'view_type')->dropDownList( \backend\Modules\Cms\models\CmsGallery::getTypes() ) : '' ?>
    <?php /*$form->field($model, 'title_slug')->textInput(['maxlength' => 255])*/ ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?php /*$form->field($model, 'short_content')->textarea(['rows' => 6]) */ ?>

    <?php /*$form->field($model, 'meta_title')->textInput(['maxlength' => 255]) */ ?>

    <?php /*$form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) */ ?>

    <?php /*$form->field($model, 'meta_description')->textarea(['rows' => 6]) */ ?>


    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
