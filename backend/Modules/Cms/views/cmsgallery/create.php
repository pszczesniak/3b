<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsGallery */

$this->title = Yii::t('app', 'Kreator galerii');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer galerii'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-gallery-create', 'title'=>Html::encode($this->title))) ?>
	<div class="text-right">
        <?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Menedżer galerii', [
            'modelClass' => 'Cms Gallery',
        ]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm btn-responsive']) ?>
    </div>
    <hr />
    <?= $this->render('_form', [
		'model' => $model,
	]) ?>

<?php $this->endContent(); ?>
