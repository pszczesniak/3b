<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\galleries\ActiveForm */
?>

<div class="cms-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-cms-galleries-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-galleries']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-8 col-md-8 col-xs-12"> <?= $form->field($model, 'title')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-galleries', 'data-form' => '#filter-cms-galleries-search' ])->label('Tytuł <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'view_type')->dropDownList( \backend\Modules\Cms\models\CmsGallery::getTypes(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-galleries', 'data-form' => '#filter-cms-galleries-search' ] ) ?></div>
    </div>     <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
