<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\cms\CmsGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menedżer galerii');
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-gallery-index', 'title'=>Html::encode($this->title))) ?>
    
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-galleries" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Cms Gallery'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-galleries"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-galleries"

            data-toolbar="#toolbar-galleries" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-cms-galleries-search"
            data-url=<?= Url::to(['/cms/cmsgallery/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="title" data-sortable="true" >Tytuł</th>
                <th data-field="type" data-align="center" data-sortable="true" >Typ</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>
    
<?php $this->endContent(); ?>
<?php
	yii\bootstrap\Modal::begin([
	  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Confirm').'</h4>',
	  //'toggleButton' => ['label' => 'click me'],
	  'id' => 'modalConfirm', // <-- insert this modal's ID
	]);
 
	echo ' <div class="modal-body"><p>Czy na pewno chcesz usunąć ten element?</p> </div> <div class="modal-footer"> <a href="#" id="btnYesDelete" data-id=0 class="btn btn-danger">Usuń</a> <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-default">Anuluj</a> </div>';

	yii\bootstrap\Modal::end();
?> 