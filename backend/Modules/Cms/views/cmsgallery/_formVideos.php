<?php

use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="grid">
	<div class="col-sm-6 col-xs-12">
		<fieldset><legend>Dołącz plik [[<small>*.mp4, *.webm, *.ogg</small>]</legend>
			<div class="file-upload-alert file-upload-alert-<?= $type ?>"></div>
			<div class="upload-container upload-<?= $type ?>">
				<div class="progress-wrp none"><!--<div class="progress-bar"></div ><div class="status">0%</div>--></div>
				<div class="input-group">
					<label class="input-group-btn">
						<span class="btn bg-purple">
							<i class="fa fa-hand-point-up" title="Wybierz plik"></i> <input type="file" multiple="" data-type="<?= $type ?>" style="display: none;" class="ufile">
						</span>
					</label>
					<input type="text"  class="form-control ufile-name ufile-name-<?= $type ?>">
					<div class="input-group-btn">
						<button class="btn btn-primary btn-file-send" data-table="#table-files-<?= $type ?>" data-action="<?= Url::to(['/files/upload']) ?>" data-type="<?= $type ?>" data-id="<?= $id ?>" data-original-title="Upload" title="Zapisz"><i class="fa fa-save" title="Zapisz"></i></button>
					</div>
				</div>	
			</div>
		</fieldset>
	</div>
	<div class="col-sm-6 col-xs-12">
		<fieldset><legend>Dołącz plik z Youtube</legend>
			<?php $formYoutube = ActiveForm::begin(['id' => 'form-upload-youtube', 
											 'action' => Url::to(['/files/youtube']), /*'layout' => 'horizontal',*/ 
											 'options' => [	'class' => 'form-upload-file', 
															'data-table' => '#table-files-'.$type, 
															'data-inputfile' => 'upload-file-youtube', 
															'data-label' => '<i class="fa fa-upload"></i>' 
														],
											  'fieldConfig' => [
													'template' => "{input}",
													'options' => [
														'tag'=>'span'
													]
												]
											]) ?>
			<?= $formYoutube->field($model, 'id_fk')->hiddenInput(['id' => 'upload-file-fk-youtube', 'value' => $id ])->label(false); ?>
			<?= $formYoutube->field($model, 'id_type_file_fk')->hiddenInput(['id' => 'upload-file-type-youtube', 'value' => $type ])->label(false); ?>

			<div class="input-group">
				<span class="input-group-addon">
					<i class="fab fa-youtube"></i>
				</span>
				<?= $formYoutube->field($model, 'title_file')->textInput(['id' => 'upload-file-title-youtube', 'maxlength' => true, 'placeholder' => 'Adres youtube']) ?>
				<span class="input-group-btn">
					<?= Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'submit-upload-youtube btn more_btn1']) ?>
				</span>
			</div><!-- /input-group -->
			<?php ActiveForm::end() ?>
		</fieldset>
	</div>
</div>