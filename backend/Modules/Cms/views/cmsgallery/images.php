<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsGallery */
use backend\extensions\kapi\gallery\GalleryManager;

$this->title = Yii::t('app', 'Menedżer galerii');
$this->params['breadcrumbs'][] = 'CMS';

$visit = false;

if(substr($model->title, 0, 8) == 'product#') {
	$product = substr($model->title, 8); $lang = ($model->versions_data) ? 'DE' : 'PL';
	$this->title =  'Galeria produktu #'.$product.'[ '. $lang .' ]';
	$this->params['breadcrumbs'][] = ['label' => 'Produkt #'.$product, 'url' => Url::to(['/shop/shopproduct/update', 'id' => $product])];
	$this->params['breadcrumbs'][] = 'Galeria produktu '.substr($model->title, 7);
} else if(substr($model->title, 0, 5) == 'blog#') {
	$post = substr($model->title, 5); $lang = ($model->versions_data) ? 'DE' : 'PL';
	$this->title =  'Galeria do wpisu bloga #'.$post.'[ '. $lang .' ]';
	$this->params['breadcrumbs'][] = ['label' => 'Wpis #'.$post, 'url' => Url::to(['/blog/blogpost/update', 'id' => $post])];
	$this->params['breadcrumbs'][] = 'Galeria wpisu bloga '.substr($model->title, 7);
} else if(substr($model->title, 0, 9) == 'ugallery#') {
	$id = substr($model->title, 9); $lang = ($model->versions_data) ? 'DE' : 'PL';
    $visit = \backend\Modules\Eg\models\Visit::findOne($id);
	$this->title =  'Galeria: '.$visit->additional_info;
	$this->params['breadcrumbs'][] = ['label' => 'Wpis #'.$id, 'url' => Url::to(['/eg/visit/showajax', 'id' => $id]), 'class' => 'viewModal', 'data-target' => '#modal-grid-item', 'data-title' => 'Edycja terminu'];
	$this->params['breadcrumbs'][] = 'Galeria użytkownika '.substr($model->title, 7);
} else {
	$this->title = $model->title;
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rejestr galerii'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['update', 'id' => $model->id]];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
}
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-gallery-images', 'title'=>Html::encode($this->title))) ?>
    <div class="align-right">
        <?php 
            if(substr($model->title, 0, 8) == 'product#' && !$model->versions_data) {
                $gallery_lang = \backend\Modules\Cms\models\CmsGallery::find()->where(['versions_data' => 'product#'.$model->id.'#de'])->one();
                if(!$gallery_lang) {
                    echo Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Galeria DE', [
                            'modelClass' => 'Cms Gallery',
                        ]), ['createlang', 'id' => $model->id, 'type' => 'product', 'lang' => 'de'], ['class' => 'btn btn-success btn-icon btn-sm btn-responsive']);
                }    else {
                    echo Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Galeria DE', [
                            'modelClass' => 'Cms Gallery',
                        ]), ['images', 'id' => $gallery_lang->id], ['class' => 'btn btn-primary btn-icon btn-sm btn-responsive']);
                }
            } else if (substr($model->title, 0, 8) == 'product#' && $model->versions_data) {
                $arr = explode('#',$model->versions_data);
                echo Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Galeria PL', [
                            'modelClass' => 'Cms Gallery',
                        ]), ['images', 'id' => $arr[1]], ['class' => 'btn btn-primary btn-icon btn-sm btn-responsive']);
            }
        ?>
    </div>
    <div class="text-right">
        <?= (!$visit) ? Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Nowa galeria', [
            'modelClass' => 'Cms Gallery',
        ]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm btn-responsive']) : '' ?>
        <?= (!$visit) ? Html::a('<i class="fa fa-pencil-alt"></i>'.Yii::t('app', 'Edycja', [
            'modelClass' => 'Cms Gallery',
        ]), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-icon btn-sm btn-responsive']) : '' ?>
        <?php /*Html::a('<i class="fa fa-trash-o"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger btn-icon btn-sm btn-responsive',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		])*/ ?>
        <?= (!$visit) ? Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Rejestr galerii', [
            'modelClass' => 'Cms Gallery',
        ]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm btn-responsive']) : '' ?>
        
        <?= ($visit) ? '<a href="'.Url::to(['/cms/cmsgallery/send', 'id' => $model->id]).'" class="btn btn-icon bg-blue viewModal" data-target="#modal-grid-item" data-title="Wyślij informację o galerii"><i class="fab fa-telegram-plane"></i>Wyślij info do klienta</a>' : '' ?>
    </div>
    <hr />
	<div id="gallery-manager-container" data-id="<?= $model->id ?>" data-url="<?= Url::to(['/files/galleryimages', 'id' => $model->id]) ?>" data-behavior="<?= Url::to(['/cms/cmsgallery/galleryApi']) ?>">
    <?php
		echo GalleryManager::widget(
			[
				'model' => $model,
				'behaviorName' => 'galleryBehavior',
				'apiRoute' => 'cmsgallery/galleryApi'
			]
		);
	?>
    </div>
<?php $this->endContent(); ?>
