<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\cms\CmsGallery */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cms Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-gallery-index', 'title'=>Html::encode($this->title))) ?>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'versions_data:ntext',
			'title',
			'title_slug',
			'content:ntext',
			'short_content:ntext',
			'meta_title',
			'meta_keywords',
			'meta_description:ntext',
			'hits',
			'is_system',
		],
	]) ?>
<?php $this->endContent(); ?>

