<?php

use yii\helpers\Html;


use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-page-form tab-block">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Podstawowe</a></li>
        <?php if(!$model->isNewRecord) { ?>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Treść</a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">SEO</a></li>
        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Galerie</a></li>
        <li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Dodatki</a></li>
        <?php if($model->category) { ?> <?= ($model->category['details_config']) ? '<li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Konfiguracja</a></li>' : '' ?><?php } ?>
        <?php } ?>
    </ul>

      <!-- Tab panes -->
   <div class="tab-content tab-content-normal">
        <div role="tabpanel" class="tab-pane active" id="tab1"><?= $this->render('tabs/_tabBasic',array('model'=>$model)) ?></div>
        <?php if(!$model->isNewRecord) { ?>
        <div role="tabpanel" class="tab-pane" id="tab2"><?= $this->render('tabs/_tabContent',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab3"><?= $this->render('tabs/_tabSeo',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab4"><?= $this->render('tabs/_tabGalleries',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab5"><?= $this->render('tabs/_tabWidgets', array('model'=>$model)) ?></div>
        <?php if($model->category) { ?> <?= ($model->category['details_config']) ? '<div role="tabpanel" class="tab-pane" id="tab6">'.($this->render('tabs/_tabAdditional', array('model'=>$model))).'</div>' : '' ?><?php } ?>
        <?php } ?>
    </div>
</div>
