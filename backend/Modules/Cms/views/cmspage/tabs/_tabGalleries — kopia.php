<?php 
	use yii\helpers\ArrayHelper; 
	use yii\helpers\Url;
?>
<div class="row">
	<?php 
		/*$galleryTable = \backend\Modules\Cms\models\CmsGallery::tableName();
		$galleryPageTable = \backend\Modules\Cms\models\CmsPageGallery::tableName();
		$galleries = \backend\Modules\cms\models\CmsGallery::find()
				->where([$galleryTable.'.status' => 1])
				->join('LEFT JOIN', $galleryPageTable , $galleryTable.'.id = '.$galleryPageTable.'.fk_gallery_id and '.$galleryPageTable.'.status = 1')
				->select([$galleryTable.'.id, '.$galleryTable.'.title ,'.$galleryPageTable.'.id as pid'])
				->asArray()->all();*/
		$galleries = \backend\Modules\Cms\models\CmsGallery::find()->asArray()->all();
		$selected = [];
		foreach($model as $key=>$value) { array_push($selected, $value['fk_gallery_id']);}
	?>
	<?php if( count($galleries) > 0) {  ?>
		<div class="col-md-6">
			<select id='callbacks-galleries' multiple='multiple' data-urlselected="<?= Url::to(["cmspage/gcreate"]) ?>" data-urldeselected="<?= Url::to(["cmspage/gdelete"]) ?>">
				<?php 	
					for($i=0; $i<count($galleries); ++$i) {
						if(/*$modal[$i]['pid']*/in_array($galleries[$i]['id'], $selected))
							echo "<option id='galleryList-".$galleries[$i]['id']."' data-id='".$pageId."' value='".$galleries[$i]['id']."' selected>".$galleries[$i]['title']."</option>";
						else
							echo "<option id='galleryList-".$galleries[$i]['id']."' data-id='".$pageId."' value='".$galleries[$i]['id']."'>".$galleries[$i]['title']."</option>";
					}
				?>
			</select>
		</div>
		<div class="col-md-6">
			<table id="page-gallery" class="table table-bordered table-condensed table-hover table-responsive">
				<thead><tr><td>#</td><td>Nazwa</td></tr></thead>
				<tbody>
					<?php if(count($model) > 0 ) { 
						foreach($model as $key=>$value) {
							//$gallery = \backend\Modules\cms\models\CmsGallery::findOne($value);
							echo '<tr id="pg-'.$value['id'].'"><td>'.$value['rank'].'</td><td>'.$value['gallery_label'].'</td></tr>';
						}
					} else { ?>
						<tr id="pg-none-gallery"><td colspan="2"><div  class="alert alert-info" role="alert">Brak powiązań</div> </td></tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	<?php } else { ?>
		<div  class="alert alert-info" role="alert">Brak zdefiniowanych galerii</div> </td>
	<?php } ?> 
	
</div>