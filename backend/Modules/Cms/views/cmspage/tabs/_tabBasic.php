<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use backend\Modules\Cms\models\CmsPageStatus;
use backend\Modules\Cms\models\CmsCategory;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>
   
     <div class="grid">
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-8">
			<?php $form = ActiveForm::begin([ 'id' => 'page-basic', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'] ]); ?>

            <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_basic'])->label(false); ?>
			
            <div class="grid">
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'fk_status_id')->dropDownList( ArrayHelper::map(CmsPageStatus::find()->all(), 'id', 'name') )  ?></div>
                <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'template_view')->dropDownList( \backend\Modules\Cms\models\CmsPage::getTemplateView() )  ?> </div>
            </div>

			<!--<?= $form->field($model, 'fk_part_id')->textInput() ?>-->

			<?= $form->field($model, 'fk_category_id')->dropDownList( ArrayHelper::map(CmsCategory::find()->all(), 'id', 'title'), array('prompt'=>'Brak kategorii') )  ?>

			<?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

			<?= $form->field($model, 'title_slug')->textInput(['maxlength' => 255]) ?>

			
			<?php /*Yii::$app->formatter->dateFormat = 'Y-m-d';*/ ?>
			<div class="form-group">
                <label for="cmspage-event_date" class="control-label">Data wydarzenia</label>
                <div class='input-group date clsDatePicker' id='datetimepicker_event'>
                    <input type='text' class="form-control" id="cmspage-event_date" name="CmsPage[event_date]" value="<?= $model->event_date ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <?php /* $form->field($model, 'is_print')->checkBox([ 'uncheck' => null, 'checked' => true]) */ ?>
 
			<div class="form-group align-right">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
			</div>

			<?php ActiveForm::end(); ?>
            <?php if($model->id == 1) { ?>
            <div class="grid">
                <div class="col-sm-6 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Zdjęcie w wiodące strony</h3>
                        </div>
                        <div class="panel-body">
                            <small class="text--blue">* proszę wgrać plik w formacie JPG [tylko takie zostaną pokazane na stronie]</small>
                            <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                                        [
                                            'id' => 'imgAttachment',
                                            'model' => $model,
                                            'behaviorName' => 'coverBehavior',
                                            'apiRoute' => '/cms/cmspage/imgAttachApi',
                                        ]
                                    
                                    )
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Film w nagłówku</h3>
                        </div>
                        <div class="panel-body">
                            <ul>
                                <li><h4>Maska [napis]&nbsp;&nbsp;<a href="<?= Url::to(['/cms/cmspage/change', 'id' => 1, 'type' => 1]) ?>" class="btn btn-sm btn-info viewModal" data-target="#modal-grid-item" data-title="Zmiana maski na stronie głównej">Zmień</a></h4></li>
                                <li><h4>Plik video [mp4]&nbsp;&nbsp;<a href="<?= Url::to(['/cms/cmspage/change', 'id' => 1, 'type' => 2]) ?>" class="btn btn-sm btn-info viewModal" data-target="#modal-grid-item" data-title="Zmiana pliku video w formacie mp4 na stronie głównej">Zmień</a></h4></li>
                                <li><h4>Plik video [webm]&nbsp;&nbsp;<a href="<?= Url::to(['/cms/cmspage/change', 'id' => 1, 'type' => 3]) ?>" class="btn btn-sm btn-info viewModal" data-target="#modal-grid-item" data-title="Zmiana pliku video w formacie webm na stronie głównej">Zmień</a></h4></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-4">
			
            <?php if(!$model->isNewRecord) { ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">Historia</div>
                    <!--<div class="panel-body">-->
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'hits',
                                'created_at',
                                [                      
                                    'attribute' => 'created_by',
                                    'format'=>'raw',
                                    'value' =>  \common\models\User::findOne($model->created_by)->username
                                ], 
                                [                      
                                    'attribute' => 'updated_at',
                                    'format'=>'raw',
                                    'label' => 'Ostanio aktualizowano',
                                    'visible' =>  ($model->updated_by) ? true : false,
                                ],  
                                [                      
                                    'attribute' => 'updated_by',
                                    'format'=>'raw',
                                    'label' => 'Aktualizowane przez',
                                    'visible' =>  ($model->updated_by) ? true : false,
                                    'value' =>  ($model->updated_by) ? \common\models\User::findOne($model->updated_by)->username : ''
                                ], 
                                [                      
                                    'attribute' => 'deleted_at',
                                    'format'=>'raw',
                                    'label' => 'Usunięto',
                                    'visible' =>  ($model->deleted_by) ? true : false,
                                ],  
                                [                      
                                    'attribute' => 'deleted_by',
                                    'format'=>'raw',
                                    'label' => 'Usunięto przez',
                                    'visible' =>  ($model->deleted_by) ? true : false,
                                    'value' =>  ($model->deleted_by) ? \common\models\User::findOne($model->deleted_by)->username : ''
                                ], 
                            ],
                        ]) ?>

                        <!--<div class="form-group pull-right">
                            <button data-url="/cms/cmspage/config?id=<?= $model->id ?>" class="btn btn-info main-form-btn btn-pacge-config" id="wh-2" type="submit">Zapisz zmiany</button>				
                        </div>-->
                    <!--</div>-->
                </div>
            <?php } ?>
		</div>
	</div>
	
	