<?php 
	use yii\helpers\ArrayHelper; 
	use yii\helpers\Url;
?>
<div class="row">
	<!--<div class="col-md-12 alert alert-info" id="widget-response">Aby dodać widget i go skonfigurować kliknij z listy po lewej</div>-->
	<?php 
		/*$widgetTable = \backend\Modules\Cms\models\CmsWidget::tableName();
		$widgetPageTable = \backend\Modules\Cms\models\CmsPageWidget::tableName();
		$widgets = \backend\Modules\cms\models\CmsWidget::find()
				//->where([$widgetTable.'.status' => 1])
				->join('LEFT JOIN', $widgetPageTable , $widgetTable.'.id = '.$widgetPageTable.'.fk_widget_id')
				->select([$widgetTable.'.id, '.$widgetTable.'.title ,'.$widgetPageTable.'.id as pid'])
				->asArray()->all();*/
		$widgets = \backend\Modules\Cms\models\CmsWidget::find()->asArray()->all();
		$selected = [];
		foreach($model as $key=>$value) { array_push($selected, $value['fk_widget_id']);}
    ?>
	<?php if( count($widgets) > 0) { ?>
		<div class="col-md-6">
			<select id='callbacks-widgets' multiple='multiple' data-urlselected="<?= Url::to(["cmspage/wcreate"]) ?>" data-urldeselected="<?= Url::to(["cmspage/wdelete"]) ?>">
				<?php 	
					for($i=0; $i<count($widgets); ++$i) {
						if(/*$modal[$i]['pid']*/in_array($widgets[$i]['id'], $selected))
							echo "<option id='widgetList-".$widgets[$i]['id']."' data-id='".$pageId."' value='".$widgets[$i]['id']."' selected>".$widgets[$i]['title']."</option>";
						else
							echo "<option id='widgetList-".$widgets[$i]['id']."' data-id='".$pageId."' value='".$widgets[$i]['id']."'>".$widgets[$i]['title']."</option>";
					}
				?>
			</select>
		</div>
		<div class="col-md-6">
			<table id="page-widget" class="table table-bordered table-condensed table-hover table-responsive">
				<thead><tr><td>#</td><td>Nazwa</td></tr></thead>
				<tbody>
					<?php if(count($model) > 0 ) { 
						foreach($model as $key=>$value) {
							//$gallery = \backend\Modules\cms\models\CmsGallery::findOne($value);
							echo '<tr id="pw-'.$value['id'].'"><td>'.$value['rank'].'</td><td>'.$value['widget_label'].'</td></tr>';
						}
					} else { ?>
						<tr id="pw-none-widget"><td colspan="2"><div  class="alert alert-info" role="alert">Brak powiązań</div> </td></tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	<?php } else { ?>
		<div  class="alert alert-info" role="alert">Brak zdefiniowanych widgetów</div> </td>
	<?php } ?> 
	
</div>