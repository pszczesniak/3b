<?php 
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
?>
<div class="form">
    <div id="toolbar-galleries" class="toolbar-table-widget table-tools">
        <?php $form = ActiveForm::begin(['action' => Url::to(['/cms/cmspage/gadd', 'id' => $model->id]),
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform form-pgallery', 'data-table' => "#table-galleries", 'data-form' => 'form-pgallery'],
            'fieldConfig' => [ ],
        ]); ?>
            <div class="grid">
                <div class="col-xs-12">
                    <?= $form->field($model, 'galleries_list', ['template' => 
                          '{label}
                           <div class="input-group ">
                                {input}
                                <div class="input-group-btn">'.
                                    Html::a('<span class="fa fa-plus"></span>', Url::to(['/cms/cmsgallery/createajax']) , 
                                        ['class' => 'btn btn-success viewModal', 
                                         'id' => 'gallery-add',
                                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                         'data-target' => "#modal-grid-item", 
                                         'data-form' => "item-form",
                                         'data-title' => "Nowa galeria"
                                        ]).Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn btn-primary'])
                                .'</div>'
                           .'</div>{error}{hint}'
                       ])->dropDownList( ArrayHelper::map(\backend\Modules\Cms\models\CmsGallery::getList(), 'id', 'title'), ['data-placeholder' => 'wybierz galerie', 'class' => 'form-control page-gallery ms-options', "multiple" => true] )->label(false)
                    ?> 
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <!--<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-galleries"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>-->
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-galleries"

        data-toolbar="#toolbar-galleries" 
        data-toggle="table-widget" 
        data-show-refresh="true" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"  

        data-show-pagination-switch="false"
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-height="300"
        data-show-footer="false"
        data-side-pagination="client"
        data-row-style="rowStyle"
        data-sort-name="id"
        data-sort-order="desc"
        
        data-method="get"
        data-search-form="#filter-cms-galleries-search"
        data-url=<?= Url::to(['/cms/cmspage/galleries', 'id' => $model->id]) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="title"  data-sortable="true" >Tytuł</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>


</div>