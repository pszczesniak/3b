<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;


//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>
  <?php $details = explode('|', $model->category['details_config']); ?>
 <div class="row">
    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Zdjęcie wyróżniające</h3>
            </div>
            <div class="panel-body">
                <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
                            [
                                'id' => 'imgAttachment',
                                'model' => $model,
                                'behaviorName' => 'coverBehavior',
                                'apiRoute' => '/cms/cmspage/imgAttachApi',
                            ]
                        
                        )
                ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
        <?php
            if(isset($details[1]) && $details[1] == 'map') {
         ?>
                <?php $form = ActiveForm::begin([ 'id' => 'page-details', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'] ]); ?>
                    <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_details'])->label(false); ?>
                    <fieldset><legend>Współrzędne geograficzne [do mapy]</legend>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'map_latitude')->textInput()  ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'map_longitude')->textInput() ?> </div>
                            <div class="col-sm-12 col-xs-12"><?= $form->field($model, 'map_note')->textInput() ?> </div> 
                        </div>
                        <small  class="text--blue"><i class="fa fa-info-circle"></i>&nbsp; użycie znaku '|' spowoduje przeniesienie tekstu do nowej linii w kliknięciu na marker na mapie</small>
                    </fieldset>
                    <div class="form-group align-right">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
        <?php  }  ?>
    </div>
</div>
	
	