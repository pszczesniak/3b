<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

//use dosamigos\tinymce\TinyMce;
//use dosamigos\ckeditor\CKEditor;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([ 'id' => 'page-content', 'options' => ['class' => 'ajaxform'] ]); ?>
    <?= $form->field($model, 'user_action')->hiddenInput(['label' => false,'value' => 'update_content'])->label(false) ?>
	<?php 
        if($model->fk_category_id || $model->id == 1) {
            echo  $form->field($model, 'short_content')->textarea(['rows' => 6, 'class'=> 'mce-basic']) ;	
        }
    ?>
	
	<?php /* $form->field($model, 'content')->widget(TinyMce::className(), [
		'options' => ['id' => 'cmspage-content','rows' => 6],
		//'language' => 'es',
		'clientOptions' => [
			
			'plugins' => [
				"advlist autolink lists link charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste filemanager"
			],
			'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | filemanager"
		]
	]);*/ ?>
	<?php echo  $form->field($model, 'content')->textarea(['rows' => 6, 'class'=> 'mce-full', 'data-url' => Yii::$app->params['frontend']]) ?>	
	<?php /*echo $form->field($model, 'content')->widget(TinyMce::className(), [
		'options' => ['rows' => 10],
		'language' => 'pl',
		'clientOptions' => [
			'plugins' => [
				"advlist autolink lists link charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste image"
			],
			'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
			'file_browser_callback' => new yii\web\JsExpression('function myFileBrowser(field, url, type, win) {
						if(type == "image") {
							tinyMCE.activeEditor.windowManager.open({
								file: "/filebrowser/browse.php?opener=tinymce4&field=" + field + "&type=" + type,
								title: "Media ",
								width: 700,
								height: 500,
								inline: true,
								close_previous: false
							}, {
								window: win,
								input: field
							}); 
						}
					return false;			
					}')
			//'file_browser_callback' => 'myFileBrowser'
		],
	]);*/  ?>
	<?php /*$form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ])*/ ?>

<!--    <iframe id="form_target" name="form_target" style="display:none"></iframe>

<form id="my_form" action="/upload/" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden"><input name="image" type="file" onchange="$('#my_form').submit();this.value='';"></form>-->
    <div class="form-group align-right">
        <?php if(!$model->isNewRecord) echo Html::submitButton(('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' =>'btn btn-primary main-form-btn btn-form-content btn-icon']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>	
	