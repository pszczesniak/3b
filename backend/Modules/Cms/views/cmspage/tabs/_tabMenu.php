<?php 
	use yii\helpers\ArrayHelper; 
	use yii\helpers\Url;
?>
<div class="row">
	<!--<div class="col-md-12 alert alert-info" id="menu-response">Aby dodać menu i go skonfigurować kliknij z listy po lewej</div>-->
	<?php 
		/*$menuTable = \backend\Modules\Cms\models\CmsMenu::tableName();
		$menuPageTable = \backend\Modules\Cms\models\CmsPageMenu::tableName();
		$menus = \backend\Modules\cms\models\CmsMenu::find()
				//->where([$menuTable.'.status' => 1])
				->join('LEFT JOIN', $menuPageTable , $menuTable.'.id = '.$menuPageTable.'.fk_cms_element_id')
				->select([$menuTable.'.id, '.$menuTable.'.title ,'.$menuPageTable.'.id as pid'])
				->asArray()->all();*/
		$menus = \backend\Modules\Cms\models\CmsMenu::find()->asArray()->all();
		$selected = [];
		foreach($model as $key=>$value) { array_push($selected, $value['fk_menu_id']);}
	?>
	<?php if( count($menus) > 0) { ?>
		<div class="col-md-6">
			<select id='callbacks-menus' multiple='multiple' data-urlselected="<?= Url::to(["cmspage/mcreate"]) ?>" data-urldeselected="<?= Url::to(["cmspage/mdelete"]) ?>">
				<?php 	
					for($i=0; $i<count($menus); ++$i) {
						if(/*$modal[$i]['pid']*/in_array($menus[$i]['id'], $selected))
							echo "<option id='menuList-".$menus[$i]['id']."' data-id='".$pageId."' value='".$menus[$i]['id']."' selected>".$menus[$i]['title']."</option>";
						else
							echo "<option id='menuList-".$menus[$i]['id']."' data-id='".$pageId."' value='".$menus[$i]['id']."'>".$menus[$i]['title']."</option>";
					}
				?>
			</select>
		</div>
		<div class="col-md-6">
			<table id="page-menu" class="table table-bordered table-condensed table-hover table-responsive">
				<thead><tr><td>#</td><td>Nazwa</td></tr></thead>
				<tbody>
					<?php if(count($model) > 0 ) { 
						foreach($model as $key=>$value) {
							//$gallery = \backend\Modules\cms\models\CmsGallery::findOne($value);
							echo '<tr id="pm-'.$value['id'].'"><td>'.$value['rank'].'</td><td>'.$value['menu_label'].'</td></tr>';
						}
					} else { ?>
						<tr id="pm-none-menu"><td colspan="2"><div  class="alert alert-info" role="alert">Brak powiązań</div> </td></tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	<?php } else { ?>
		<div  class="alert alert-info" role="alert">Brak zdefiniowanych menuów</div> </td>
	<?php } ?> 
	
</div>