<?php 
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
?>
<div class="form">
    <div id="toolbar-widgets" class="btn-group toolbar-table-widget table-tools">
        <?php $form = ActiveForm::begin(['action' => Url::to(['/cms/cmspage/wadd', 'id' => $model->id]),
            //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
            'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
            'options' => ['class' => 'ajaxform form-pwidget', 'data-table' => "#table-widgets", 'data-form' => 'form-pwidget'],
            'fieldConfig' => [ ],
        ]); ?>

            <?= $form->field($model, 'widgets_list', ['template' => '
                  {label}
                   <div class="input-group ">
                        {input}
                        <div class="input-group-btn">'.
                            Html::a('<span class="fa fa-plus"></span>', Url::to(['/cms/cmswidget/createajax']) , 
                                ['class' => 'btn btn-success viewModal text--white', 
                                 'id' => 'widget-add',
                                 //'data-toggle' => ($gridViewModal)?"modal":"none", 
                                 'data-target' => "#modal-grid-item", 
                                 'data-form' => "item-form",
                                 'data-title' => "Nowy widget"
                                ]).Html::submitButton('<i class="fa fa-save"></i>', ['class' => 'btn btn-primary'])
                        .'</div>
                   </div>
                   {error}{hint}
               '])->dropDownList( \backend\Modules\Cms\models\CmsWidget::getList(), ["multiple" => true, 'data-placeholder' => 'wybierz dodatek', 'class' => 'form-control page-widget ms-options'] )->label(false)
            ?> 

        <?php ActiveForm::end(); ?>
        <!--<button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-widgets"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>-->
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-widgets"

        data-toolbar="#toolbar-widgets" 
        data-toggle="table-widget" 
        data-show-refresh="true" 
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"  

        data-show-pagination-switch="false"
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-height="300"
        data-show-footer="false"
        data-side-pagination="client"
        data-row-style="rowStyle"
        data-sort-name="id"
        data-sort-order="desc"
        
        data-method="get"
        data-search-form="#filter-cms-widgets-search"
        data-url=<?= Url::to(['/cms/cmspage/widgets', 'id' => $model->id]) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="title"  data-sortable="true" >Tytuł</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>


</div>