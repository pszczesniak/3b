<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([ 'action' => Url::to(['/translations/update', 'id' => $model->id]), 'id' => 'page-translate','options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item"] ]); ?>
	<div class="modal-body">
		<div class="row">
            <div class="col-sm-6">
                <div class="form-group field-lang">
                    <label for="lang" class="control-label">Język</label>
                    <select name="lang" class="form-control">
                        <?php 
                            foreach(\Yii::$app->params['languages'] as $key => $value) {
                                echo ($key == $lang) ? '<option value="'.$key.'">'.$value.'</option>' : '';
                            }
                        ?>
                    </select>

                    <div class="help-block"></div>
                </div>    
                <div class="form-group field-title">
                    <label for="title" class="control-label">Tytuł</label>
                    <input type="text" maxlength="255" value="<?= $model->attributes['title'] ?>" name="title" class="form-control" id="title">

                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-sm-6">  
                <div class="form-group field-meta_title">
                    <label for="meta_title" class="control-label">Meta title</label>
                    <input type="text" maxlength="255" value="<?= $model->attributes['meta_title'] ?>" name="meta_title" class="form-control" id="meta_title">

                    <div class="help-block"></div>
                </div>
                <div class="form-group field-meta_keywords">
                    <label for="meta_keywords" class="control-label">Meta keywors</label>
                    <input type="text" maxlength="255" value="<?= $model->attributes['meta_keywords'] ?>" name="meta_keywords" class="form-control" id="meta_keywords">

                    <div class="help-block"></div>
                </div>
                <div class="form-group field-meta_description">
                    <label for="meta_description" class="control-label">Meta description</label>
                    <textarea  name="meta_description" class="form-control" id="content"><?= $model->attributes['meta_description'] ?></textarea>

                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group field-content">
                    <label for="content" class="control-label">Treść</label>
                    <textarea  name="content" class="form-control mce-full" id="content"><?= $model->attributes['content'] ?></textarea>

                    <div class="help-block"></div>
                </div>
            </div>
        </div>
	</div>
    <div class="modal-footer"> 
        <?= Html::submitButton('<i class="fa fa-save"></i>Zapisz zmiany', ['class' =>  'btn btn-icon btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button" >Odrzuć </button>
    </div>

<?php ActiveForm::end(); ?>