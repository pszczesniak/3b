<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */

$this->title = Yii::t('app', 'Kreator strony');
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer stron'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-create', 'title'=>Html::encode($this->title))) ?>
    <div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Menedżer stron', [
				'modelClass' => 'Cms Category',
			]), ['index'], ['class' => 'btn btn-warning btn-icon']) ?>
		</div>
		<div div class="col-md-12">

			<?= $this->render('_form', [
				'model' => $model,
			]) ?>
		</div>
    </div>
<?php $this->endContent(); ?>
