<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */

$this->title = Yii::t('app', 'Aktualizacja strony: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = 'CMS';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menedżer stron'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
/*$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');*/
?>
	
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-update-1', 'title'=>Html::encode($this->title))) ?>
	<div class="grid">
		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= Html::a('<i class="fa fa-plus-circle"></i>'.Yii::t('app', 'Create Cms Page', [
				'modelClass' => 'Cms Page',
			]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm']) ?>
			<?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Manager', [
				'modelClass' => 'Cms Page',
			]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm']) ?>
            <?= \backend\widgets\TranslateWidget::widget(['id' => $model->id, 'action' => '/cms/cmspage/translate', 'tinyMce' => true]) ?>
            <a href="<?= \Yii::$app->urlManagerFrontEnd->createUrl('/'.\Yii::$app->params['frontendNativeLang'].$model->id.'-'.htmlspecialchars($model->title_slug)) ?>" class="btn btn-icon btn-info btn-sm btn-default" target="_blank"><i class="fa fa-eye"></i>Podgląd</a>
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger btn-icon btn-sm deleteConfirm', ]) ?>
		</div>
		<div div class="col-md-12">
			<?= $this->render('_form', [
				'model' => $model,
			]) ?>
		</div>
    </div>
<?php $this->endContent(); ?>

<script type="text/javascript" src="/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("./js/all.min.js"));?>"></script>