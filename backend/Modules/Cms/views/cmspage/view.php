<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cms Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'cms-page-view', 'title'=>Html::encode($this->title))) ?>
    <div class="row">

		<div class="col-md-6"></div>
		<div class="col-md-6 pull-right text-right">
			<?= Html::a('<i class="glyphicon glyphicon-pencil"></i>'.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-icon']) ?>
			<?= Html::a('<i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger btn-icon',
				'data' => [
					'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
					'method' => 'post',
				],
			]) ?>
		
		</div>
		<div div class="col-md-12">

			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'id',
					'fk_status_id',
					'fk_part_id',
					'fk_category_id',
					'title',
					'title_slug',
					'content:ntext',
					'short_content:ntext',
					'meta_title',
					'meta_keywords',
					'meta_description:ntext',
					'hits',
					'isActive',
					'editable',
					'featured',
					'event_date',
					'isPrint',
					'status',
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
					'deleted_at',
					'deleted_by',
				],
			]) ?>
        </div>
    </div>
<?php $this->endContent(); ?>
