<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-cms-pages-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-pages']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'title')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-pages', 'data-form' => '#filter-cms-pages-search' ])->label('Tytuł <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'fk_category_id')->dropDownList( ArrayHelper::map(\backend\Modules\Cms\models\CmsCategory::find()->where(['status' => 1])->orderby('title')->all(), 'id', 'title'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-pages', 'data-form' => '#filter-cms-pages-search' ] ) ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'status')->dropDownList(  ArrayHelper::map(\backend\Modules\Cms\models\CmsPageStatus::find()->all(), 'id', 'name'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-pages', 'data-form' => '#filter-cms-pages-search' ] ) ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
