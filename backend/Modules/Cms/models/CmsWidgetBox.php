<?php

namespace backend\Modules\Cms\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\modules\cms\models\CmsWidgetBox;
use common\models\Translations;

/**
 * This is the model class for table "{{%cms_widget_box}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $show_title
 * @property integer $is_system
 * @property integer $is_link
 * @property integer $link_type
 * @property integer $fk_cms_widget_id
 * @property string $link_href
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsWidgetBox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_box}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name_langs', 'describe_langs', 'html_options'], 'string'],
            [['show_title', 'is_system', 'is_link', 'link_type', 'fk_cms_widget_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['describe', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'link_href'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa widgetu'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'show_title' => Yii::t('app', 'Show Title'),
            'is_system' => Yii::t('app', 'Is System'),
            'is_link' => Yii::t('app', 'Czy blok jest odnośnikiem?'),
            'link_type' => Yii::t('app', 'Typ odnosnika'),
            'fk_cms_widget_id' => Yii::t('app', 'Fk Cms Element ID'),
            'link_href' => Yii::t('app', 'Adres odnosnika'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			}/* else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'CmsWidgetBox',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/widgets/box/cover',
				'url' =>  Yii::$app->params['frontend'] . '/uploads/widgets/box/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
			
		];
	}
    
    /*public function getTranslate() {
        $langs = \common\models\Lang::find()->where(['is_active' => 1, 'is_main' => 0])->all();
        $translate = [];
        if(!$this->name_langs) {
            foreach($langs as $key => $lang) {
                $translate[$lang->lang]['name'] = '';
            }
        } else {
            $translate = \yii\helpers\Json::decode($this->name_langs);
        }
        if(!$this->describe_langs) {
            foreach($langs as $key => $lang) {
                $translate[$lang->lang]['name'] = '';
            }
        } else {
            $translate = \yii\helpers\Json::decode($this->describe_langs);
        }
        
        return $translate;
    }*/
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 5])->one();
        
        if(!$model) {
            $box = CmsWidgetBox::findOne($id);
            
            $attributes['name'] = $box->name;
            $attributes['describe'] = $box->describe;
            
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 5;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
        }
        
        return $model;
    }
}
