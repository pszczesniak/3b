<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Cms\models\CmsPage;

/**
 * CmsPageSearch represents the model behind the search form about `common\models\Cms\CmsPage`.
 */
class CmsPageSearch extends CmsPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fk_status_id', 'fk_part_id', 'fk_category_id', 'hits', 'is_active', 'editable', 'featured', 'is_print', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['title', 'title_slug', 'content', 'short_content', 'meta_title', 'meta_keywords', 'meta_description', 'event_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsPage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
			'fk_status_id' => $this->fk_status_id,
            'fk_part_id' => $this->fk_part_id,
            'fk_category_id' => $this->fk_category_id,
            'hits' => $this->hits,
            'is_active' => $this->is_active,
            'editable' => $this->editable,
            'featured' => $this->featured,
            'event_date' => $this->event_date,
            'is_print' => $this->is_print,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query/*->andFilterWhere(['=', 'status', 1])*/
		    ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'title_slug', $this->title_slug])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'short_content', $this->short_content])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        return $dataProvider;
    }
}
