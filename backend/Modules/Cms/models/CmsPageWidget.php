<?php

namespace backend\Modules\Cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_page_widget}}".
 *
 * @property integer $id
 * @property integer $fk_widget_id
 * @property integer $fk_cms_widget_id
 * @property integer $fk_cms_element_id
 * @property integer $widget_position
 * @property integer $rank
 * @property string $rank_url
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsPageWidget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_widget_id', 'fk_id'], 'required'],
            [['fk_widget_id', 'fk_cms_widget_id', 'fk_cms_element_id', 'widget_position', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['rank_url', 'widget_label', 'widget_label_langs'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_widget_id' => Yii::t('app', 'Fk Widget ID'),
            'fk_cms_widget_id' => Yii::t('app', 'Fk Cms Widget ID'),
            'fk_cms_element_id' => Yii::t('app', 'Fk Cms Element ID'),
            'widget_position' => Yii::t('app', 'Widget Position'),
            'rank' => Yii::t('app', 'Rank'),
            'rank_url' => Yii::t('app', 'Rank Url'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function getWidget()
    {
        return $this->hasOne(\backend\Modules\Cms\models\CmsWidget::className(), ['id' => 'fk_widget_id']);
    }
}
