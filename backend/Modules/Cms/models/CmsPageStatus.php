<?php

namespace backend\Modules\cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_page_status}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property string $describe_lang
 */
class CmsPageStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name_lang', 'describe', 'describe_lang'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
        ];
    }
	
	public function getPages()
    {
        return $this->hasMany(common\models\cms\CmsPage::className(), ['fk_status_id' => 'id']);
    }
}
