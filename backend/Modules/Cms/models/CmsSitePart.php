<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cms_site_part}}".
 *
 * @property integer $id
 * @property integer $fk_site_id
 * @property integer $rank
 * @property integer $fk_home_page_id
 * @property string $title
 * @property string $title_langs
 * @property string $part_styles
 * @property string $created_at
 * @property integer $created_by
 */
class CmsSitePart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_site_part}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_site_id', 'fk_home_page_id'], 'required'],
            [['fk_site_id', 'rank', 'fk_home_page_id', 'created_by'], 'integer'],
            [['part_styles'], 'string'],
            [['created_at'], 'safe'],
            [['title', 'title_langs'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_site_id' => Yii::t('app', 'Fk Site ID'),
            'rank' => Yii::t('app', 'Rank'),
            'fk_home_page_id' => Yii::t('app', 'Fk Home Page ID'),
            'title' => Yii::t('app', 'Title'),
            'title_langs' => Yii::t('app', 'Title Langs'),
            'part_styles' => Yii::t('app', 'Part Styles'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
				$this->created_at = new Expression('NOW()');
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}

}
