<?php

namespace backend\Modules\Cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_category_lang}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property string $lang
 * @property string $title
 * @property string $title_slug
 * @property string $content
 * @property string $short_content
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class CmsCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_category_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'title_slug', 'created_by', 'updated_by'], 'required'],
            [['content', 'short_content', 'meta_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['lang', 'title', 'title_slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'lang' => Yii::t('app', 'Lang'),
            'title' => Yii::t('app', 'Title'),
            'title_slug' => Yii::t('app', 'Title Slug'),
            'content' => Yii::t('app', 'Content'),
            'short_content' => Yii::t('app', 'Short Content'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
