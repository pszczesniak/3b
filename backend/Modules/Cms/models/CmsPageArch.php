<?php

namespace backend\Modules\Cms\models;

use Yii;


/**
 * This is the model class for table "{{%cms_page_arch}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property string $user_action
 * @property string $version_data
 * @property string $created_at
 * @property integer $created_by
 */
class CmsPageArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'created_by'], 'integer'],
            [['version_data'], 'string'],
            [['created_at'], 'safe'],
            //[['created_by'], 'required'],
            [['user_action'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'user_action' => Yii::t('app', 'User Action'),
            'version_data' => Yii::t('app', 'Version Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
