<?php

namespace backend\Modules\Cms\models;

use Yii;
//use vendor\yiisoft\yii2\behaviors\SluggableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Cms\models\CmsPageArch;
use common\models\Translations;

/**
 * This is the model class for table "{{%cms_page}}".
 *
 * @property integer $id
 * @property integer $fk_status_id
 * @property integer $fk_part_id
 * @property integer $fk_category_id
 * @property string $title
 * @property string $title_slug
 * @property string $content
 * @property string $short_content
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $hits
 * @property integer $isActive
 * @property integer $editable
 * @property integer $featured
 * @property string $event_date
 * @property integer $is_print
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsPage extends \yii\db\ActiveRecord
{
    public $user_action = 'create';
	public $galleries_list = [];
	public $widgets_list = [];
    
    public $map_latitude;
    public $map_longitude;
    public $map_note;
    
    public $attachs;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_status_id', 'fk_category_id'], 'integer'],
            [['title'], 'required'],
            [['title', 'meta_description'], 'string'],
            [['content', 'short_content', 'event_date', 'created_at', 'updated_at', 'deleted_at', 'galleries_list', 'widgets_list', 'details_config', 'map_latitude', 'map_longitude', 'map_note', 'attachs'], 'safe'],
            [['title', 'title_slug', 'meta_title', 'meta_keywords', 'template_view'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_status_id' => Yii::t('app', 'Fk Status ID'),
            'fk_part_id' => Yii::t('app', 'Fk Part ID'),
            'fk_category_id' => Yii::t('app', 'Fk Category ID'),
            'title' => Yii::t('app', 'Title'),
            'title_slug' => Yii::t('app', 'Title Slug'),
            'content' => Yii::t('app', 'Content'),
            'short_content' => Yii::t('app', 'Short Content'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'hits' => Yii::t('app', 'Hits'),
            'isActive' => Yii::t('app', 'Is Active'),
            'editable' => Yii::t('app', 'Editable'),
            'featured' => Yii::t('app', 'Featured'),
            'event_date' => Yii::t('app', 'Event Date'),
            'is_print' => Yii::t('app', 'Is Print'),
            'status' => Yii::t('app', 'Status'),
            'hits' => 'Ilość odsłon',
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'template_view' => 'Szablon strony',
            'map_latitude' => 'Szerokość',
            'map_longitude' => 'Długość',
            'map_note' => 'Opis na mapie'
        ];
    }
	
	public function beforeSave($insert) {
		if($this->user_action == 'update_details') {
            $this->details_config = \yii\helpers\Json::encode(['latitude' => $this->map_latitude, 'longitude' => $this->map_longitude, 'map_note' => $this->map_note]);
        }
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (Yii::$app->id == 'app-console') ? 1 : \Yii::$app->user->id;
			} else { 
				if($this->user_action != 'hit') {
                    $this->updated_by = \Yii::$app->user->id;
                    $this->updated_at = new Expression('NOW()');
                    
                    $modelArch = new CmsPageArch();
                    $modelArch->fk_id = $this->id;
                    $modelArch->user_action = $this->user_action;
                    $modelArch->version_data = \yii\helpers\Json::encode($this);
                    $modelArch->created_by = \Yii::$app->user->id;
                    $modelArch->created_at = new Expression('NOW()');
                    $modelArch->save();
                }
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->fk_status_id = 4;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'title',
				'slugAttribute' => 'title_slug',
			],
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'CmsPage',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/pages/cover',
				//'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['basicdir'] . '/web/uploads/pages/cover',
                'url' => Yii::$app->params['frontend'] . '/uploads/pages/cover',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 800;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
						return $img
							->copy()
							->resize($dstSize);
					},
				]
			]
		];
	}
	
	public function getPagestatus()
    {
        return $this->hasOne(\backend\Modules\cms\models\CmsPageStatus::className(), ['id' => 'fk_status_id']);
    }
	
	public function getCategory()
    {
        return $this->hasOne(\backend\Modules\Cms\models\CmsCategory::className(), ['id' => 'fk_category_id']);
    }
    
    public function getSite()
    {
        return \backend\Modules\Cms\models\CmsSite::findOne(1);
    }
	
	public function getGalleries()
    {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsPageGallery::className(), ['fk_id' => 'id'])->andWhere(['status' => 1])->andWhere('fk_gallery_id not in (select id from {{%cms_gallery}} where view_type = 3)')->orderBy('rank')->asArray(); 
		/*foreach($items as $key=>$value) { 
			$childrenTmp = $this->hasMany(\common\models\cms\CmsMenuItem::className(), ['fk_id' => 'id'])->andWhere(['status' => '1', 'menu_level' => 2, 'fk_id' => $key])->orderBy('rank');
			$value->getChildren();
		}*/
		return $items;
    }
	
	public function getWidgets()
    {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsPageWidget::className(), ['fk_id' => 'id'])->orderBy('rank')->asArray(); 
		/*foreach($items as $key=>$value) { 
			$childrenTmp = $this->hasMany(\common\models\cms\CmsMenuItem::className(), ['fk_id' => 'id'])->andWhere(['status' => '1', 'menu_level' => 2, 'fk_id' => $key])->orderBy('rank');
			$value->getChildren();
		}*/
		return $items;
    }
	
	public function getMenus()
    {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsPageMenu::className(), ['fk_cms_element_id' => 'id'])->orderBy('rank')->asArray(); 
		/*foreach($items as $key=>$value) { 
			$childrenTmp = $this->hasMany(\common\models\cms\CmsMenuItem::className(), ['fk_id' => 'id'])->andWhere(['status' => '1', 'menu_level' => 2, 'fk_id' => $key])->orderBy('rank');
			$value->getChildren();
		}*/
		return $items;
    }
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 1])->one();
        
        if(!$model) {
            $page = CmsPage::findOne($id);
            
            $attributes['title'] = $page->title;
            $attributes['short_content'] = $page->short_content;
            $attributes['content'] = $page->content;
            $attributes['meta_title'] = $page->meta_title;
            $attributes['meta_keywords'] = $page->meta_keywords;
            $attributes['meta_description'] = $page->meta_description;
            
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 1;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
        }
        
        return $model;
    }
    
    public function getUser($action) {
        $model = \common\models\User::findOne($this->$action);
        if(!$model)
            $user = '';
        else
            $user = $model->username;
        
        return $user;
    }
    
    public function getActive() {
        $menuItems = \backend\Modules\Cms\models\CmsMenuItem::find()->where(['status' => 1, 'fk_type_id' => 1, 'fk_cms_element_id' => $this->id])->all();
        
        return $menuItems;
    }
    
    public static function getTemplateView() {
        if(isset(\Yii::$app->params['pageTemplates']))
            $templateViews = \Yii::$app->params['pageTemplates'];
        else 
            $templateViews = [ 0 => 'widok podstawowy' ];
        
        return $templateViews;
    }
    
    public function saveAdvanced($type) {
        $filesIds = [];
        if($type == 1) {
            $fileName = 'mask.png';
            $fileNamePrev = 'mask_'.time().'.png';
        } else if($type == 2) {
            $fileName = 'move.mp4';
            $fileNamePrev = 'move_'.time().'.mp4';
        } else if($type == 3) {
            $fileName = 'move.webm';
            $fileNamePrev = 'move_'.time().'.webm';
        }
        
        try {
            foreach($this->attachs as $key => $file) {
                //echo Yii::getAlias('@webroot').'/frontend/web/uploads/homepage/mask.png'; exit;
                //$file->saveAs(Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/' . $modelFile->systemname_file . '.' . $file->extension);
                rename(Yii::getAlias('@webroot').'/../../frontend/web/uploads/homepage/'.$fileName, Yii::getAlias('@webroot').'/../../frontend/web/uploads/homepage/'.$fileNamePrev);
                $file->saveAs(Yii::getAlias('@webroot').'/../../frontend/web/uploads/homepage/'.$fileName);
            }
        } catch (\Exception $e) {
            var_dump($e); exit;
        }
        return true;       
    }
}
