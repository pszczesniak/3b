<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cms_widget_files_set_item}}".
 *
 * @property integer $id
 * @property integer $fk_set_id
 * @property integer $fk_type_file_id
 * @property string $title
 * @property string $title_langs
 * @property string $describe
 * @property string $describe_langs
 * @property string $file_name
 * @property string $file_extension
 * @property string $file_sie
 * @property integer $mv_type
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsWidgetFilesSetItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_files_set_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_set_id', 'fk_type_file_id', 'mv_type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['title', 'describe', 'file_name', 'file_extension', 'created_by'], 'required'],
            [['title_langs', 'describe_langs', 'html_options'], 'string'],
            [['file_sie'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title', 'describe', 'file_name', 'file_extension'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_set_id' => Yii::t('app', 'Fk Set ID'),
            'fk_type_file_id' => Yii::t('app', 'Fk Type File ID'),
            'title' => Yii::t('app', 'Title'),
            'title_langs' => Yii::t('app', 'Title Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_extension' => Yii::t('app', 'File Extension'),
            'file_sie' => Yii::t('app', 'File Sie'),
            'mv_type' => Yii::t('app', 'Mv Type'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			}/* else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
		];
	}
}


