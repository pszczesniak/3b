<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\Modules\Cms\models\CmsMenu;

/**
 * CmsMenuSearch represents the model behind the search form about `common\models\cms\CmsMenu`.
 */
class CmsMenuSearch extends CmsMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fk_type_id', 'rank', 'show_title', 'show_all_pages', 'is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['title', 'title_langs', 'html_options', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsMenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fk_type_id' => $this->fk_type_id,
            'rank' => $this->rank,
            'show_title' => $this->show_title,
            'show_all_pages' => $this->show_all_pages,
            'is_system' => $this->is_system,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'title_langs', $this->title_langs])
            ->andFilterWhere(['like', 'html_options', $this->html_options]);

        return $dataProvider;
    }
}
