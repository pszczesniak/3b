<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cms_widget_accordion}}".
 *
 * @property integer $id
 * @property integer $fk_cms_widget_id
 * @property string $title
 * @property string $title_langs
 * @property integer $show_title
 * @property integer $is_system
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsWidgetAccordion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_accordion}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['fk_cms_widget_id', 'show_title', 'is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name_langs', 'html_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_cms_widget_id' => Yii::t('app', 'Fk Widget ID'),
            'name' => Yii::t('app', 'Name'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'show_title' => Yii::t('app', 'Show Title'),
            'is_system' => Yii::t('app', 'Is System'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
		
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			}/* else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
		];
	}
    
    public function getItems() {
        $result = \backend\Modules\Cms\models\CmsWidgetAccordionItem::find()->where(['fk_id' => $this->id, 'status' => 1])->orderby('status desc, rank')->all();
        
        return ($result) ? $result : [];
    }
}

