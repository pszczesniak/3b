<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%cms_widget_files_set}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $show_title
 * @property integer $is_system
 * @property integer $is_player
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsWidgetFilesSet extends \yii\db\ActiveRecord
{
    
	public $imageFiles;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_files_set}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name_langs', 'describe_langs', 'html_options'], 'string'],
            [['show_title', 'is_system', 'is_player', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'describe'], 'string', 'max' => 255],
			//[['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf', 'maxFiles' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'show_title' => Yii::t('app', 'Show Title'),
            'is_system' => Yii::t('app', 'Is System'),
            'is_player' => Yii::t('app', 'Is Player'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			}/* else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
		];
	}
	
	 public function upload()
    {
        if ($this->validate()) { 
            foreach ($this->imageFiles as $file) {
                $file->saveAs('./upload/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function getItems() {
        //$result = \backend\Modules\Cms\models\CmsWidgetFilesSetItem::find()->where(['fk_set_id' => $this->id, 'status' => 1])->all();
        $result = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 7])->all();
        return ($result) ? $result : [];
    }
}


