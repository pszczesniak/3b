<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cms_site}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $native_lang
 * @property string $created_at
 * @property integer $created_by
 */
class CmsSite extends \yii\db\ActiveRecord
{
    //const SCENARIO_CONFIG = 'config';
    //const SCENARIO_STEPS = 'steps';
    
    public $user_action;
    
    public $amountFrom;
    public $amountTo;
    public $periodFrom;
    public $periodTo;
    public $loanNote;
    public $loanProvision;
    public $loanInterest;
    public $defaultAmount;
    public $defaultPeriod;
    
    public $step;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_site}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amountFrom', 'amountTo', 'periodFrom', 'periodTo', 'loanProvision', 'loanInterest', 'defaultAmount', 'defaultPeriod'], 'required'],
            [['loanProvision', 'loanInterest', 'defaultAmount'], 'number'], 
            [['amountFrom', 'amountTo', 'periodFrom', 'periodTo', 'defaultPeriod'], 'integer'],
            [['created_at', 'custom_data', 'loanNote', 'step'], 'safe'],
            [['created_by'], 'integer'],
            [['title', 'native_lang','email', 'phone'], 'string', 'max' => 255]
        ];
    }
    
    /*public function scenarios()
    {
        return [
            self::SCENARIO_CONFIG => ['amountFrom', 'amountTo', 'periodFrom', 'periodTo'],
			self::SCENARIO_STEPS => ['step'],
			'default' => array('id'),
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
			'email' => Yii::t('app', 'E-mail'),
			'phone' => Yii::t('app', 'Telefon'),
            'native_lang' => Yii::t('app', 'Native Lang'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'amountFrom' => 'Minimalna kwota pożyczki',
            'amountTo' => 'Maksymalna kwota pożyczki',
            'periodFrom' => 'Minimalna liczba rat',
            'periodTo' => 'Maksymalna liczba rat', 
            'loanNote' => 'Dodatkowy opis',
            'loanProvision' => 'Prowizja',
            'loanInterest' => 'Oprocenotwanie',
            'defaultAmount' => 'Opcjonalna kwota',
            'defaultPeriod' => 'Opcjonalny okres spłaty'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
				$this->created_at = new Expression('NOW()');
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
    
    public function getSteps() {
        $customData = \yii\helpers\Json::decode($this->custom_data);
        
        return $customData['steps'];
    }

}
