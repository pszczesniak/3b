<?php

namespace backend\Modules\Cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_widget}}".
 *
 * @property integer $id
 * @property integer $type_widget
 * @property string $title
 * @property string $title_langs
 */
class CmsWidget extends \yii\db\ActiveRecord
{
    public $user_action = 'create';
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_widget', 'title'], 'required'],
            [['type_widget'], 'integer'],
            [['title_langs'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_widget' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'title_langs' => Yii::t('app', 'Title Langs'),
        ];
    }
	
	public function afterSave($insert, $changedAttributes) {
		//if (parent::afterSave($insert, $changedAttributes)) {
			if( $insert ) {
				if($this->type_widget == 1) {
					$modelBox = new CmsWidgetBox();
					$modelBox->fk_cms_widget_id = $this->id;
					$modelBox->name = $this->title;
					//$modelArch->version_data = \yii\helpers\Json::encode($this);
					//$modelArch->created_by = \Yii::$app->user->id;
					//$modelArch->created_at = new Expression('NOW()');
					if($modelBox->save()) {
						$this->fk_element_id = $modelBox->id; $this->save();
					} else {
						var_dump($modelBox->getErrors()); exit;
						return false;
					}
				} else if($this->type_widget == 2) {
					$modelAccordion = new CmsWidgetAccordion();
					$modelAccordion->fk_cms_widget_id = $this->id;
					$modelAccordion->name = $this->title;

					if($modelAccordion->save()) {
						$this->fk_element_id = $modelAccordion->id; $this->save();
					} else {echo 'id: '.$this->id;exit;
						var_dump($modelAccordion->getErrors()); exit;
						return false;
					}
				} else if($this->type_widget == 3) {
					$modelFilesSet = new CmsWidgetFilesSet();
					$modelFilesSet->fk_cms_widget_id = $this->id;
					$modelFilesSet->name = $this->title;

					if($modelFilesSet->save()) {
						$this->fk_element_id = $modelFilesSet->id; $this->save();
					} else {
						var_dump($modelFilesSet->getErrors()); exit;
						return false;
					}
				}
			} 
			return true;
		/*} else { 
						
			return false;
		}*/
		return false;
	}
    
    public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->is_deleted = 1;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
    
    public static function getList() {
		$widgets = CmsWidget::find()->where(['is_deleted' => 0])->andWhere('title is not null')->orderby('title')->all();
		
        $options = []; $options['Bloki tekstowe'] = []; $options['Zakładki'] = []; $options['Dokumenty'] = []; $options['Systemowe'] = [];
        foreach($widgets as $key => $item) {
            if($item->type_widget == 1) $type = 'Bloki tekstowe';
            else if($item->type_widget == 2) $type = 'Zakładki';
            else if($item->type_widget == 3) $type = 'Dokumenty';
            else $type = 'Systemowe';
            $options[$type][$item->id] = $item->title;
        }
        
        if(count($options['Bloki tekstowe']) == 0) unset($options['Bloki tekstowe']);
        if(count($options['Zakładki']) == 0) unset($options['Zakładki']);
        if(count($options['Dokumenty']) == 0) unset($options['Dokumenty']);
        if(count($options['Systemowe']) == 0) unset($options['Systemowe']);
        
        return ($options && $options != NULL) ? ($options) : [];
	}
    
    public static function getTypes() {
        return [1 => 'Blok tekstowy', 2 => 'Zakładki', 3 => 'Zbiór plików', 10 => 'Systemowy'];
    }
}


