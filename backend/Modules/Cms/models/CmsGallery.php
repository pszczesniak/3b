<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use backend\extensions\kapi\gallery\GalleryBehavior;
use yii\imagine\Image;

/**
 * This is the model class for table "{{%cms_gallery}}".
 *
 * @property integer $id
 * @property integer $type_fk - 0: zwykła, 1: blog, 2: product
 * @property string $versions_data
 * @property string $title
 * @property string $title_slug
 * @property string $content
 * @property string $short_content
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $hits
 * @property integer $view_type
 * @property integer $is_system
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsGallery extends \yii\db\ActiveRecord
{
    
    public $user_action;
    public $send_message;
    public $send_email;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['versions_data', 'content', 'short_content', 'meta_description', 'send_email'], 'string'],
            [['title'], 'required'],
            [['type_fk', 'hits', 'view_type', 'is_system', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'send_message', 'user_action'], 'safe'],
            [['title', 'title_slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['send_message', 'send_email'], 'required', 'when' => function($model) { return ($model->user_action == 'send'); }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'versions_data' => Yii::t('app', 'Versions Data'),
            'title' => Yii::t('app', 'Title'),
            'title_slug' => Yii::t('app', 'Title Slug'),
            'content' => Yii::t('app', 'Content'),
            'short_content' => Yii::t('app', 'Short Content'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'hits' => Yii::t('app', 'Hits'),
            'view_type' => Yii::t('cms', 'View photo type'),
            'is_system' => Yii::t('app', 'Is System'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'send_message' => 'Treść',
            'send_email' => 'Adres e-mail'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
                if(!$this->type_fk) $this->type_fk = 0;
			} else { 
				$this->updated_by = 1;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function behaviors()
	{
		return [
			"slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'title',
				'slugAttribute' => 'title_slug',
			],
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			'galleryBehavior' => [
				 'class' => GalleryBehavior::className(),
				 'type' => 'cmsgallery',
				 'tableName' => Yii::$app->getDb()->tablePrefix.'cms_gallery_photo',
				 'extension' => 'jpg',
				 //'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['frontenddir'].'/web/uploads/gallery',
                 'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['basicdir'] . '/web/uploads/gallery',
				 /////'directory' => 'F:/WebDir/projekty/yii-website/images/gallery',
				 /////'directory' => Yii::getAlias('@webroot') . Yii::$app->params['uploadPath'],
                 //'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['frontendurl'] . '/uploads/gallery',
                 'url' => Yii::$app->params['frontend'] . '/uploads/gallery',
				 'versions' => [
				    'thumb' => function ($img) {
						 return $img
							->copy()
                            ->thumbnail(new \Imagine\Image\Box(200, 200));
					},
					'small' => function ($img) {
						 $dstSize = $img->getSize();
						 $maxWidth = 600;
						 if ($dstSize->getWidth() > $maxWidth) {
							 $dstSize = $dstSize->widen($maxWidth);
						 }
						 return $img
							 ->copy()
							 ->resize($dstSize);
					},
                    'medium' => function ($img) {
						 $dstSize = $img->getSize();
						 $maxWidth = 1024;
						 if ($dstSize->getWidth() > $maxWidth) {
							 $dstSize = $dstSize->widen($maxWidth);
						 }
						 return $img
							 ->copy()
							 ->resize($dstSize);
					},
                    'large' => function ($img) {
						 $dstSize = $img->getSize();
						 $maxWidth = 1920;
						 if ($dstSize->getWidth() > $maxWidth) {
							 $dstSize = $dstSize->widen($maxWidth);
						 }
						 return $img
							 ->copy()
							 ->resize($dstSize);
					},
				 ]
			 ]
		];
	}
	
	public function getItems() {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsGalleryPhoto::className(), ['owner_id' => 'id'])->andWhere(['status' => '1', 'is_show' => 1])->orderBy('rank'); 
		
		return $items;
    }
    
    public function getItemsall() {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsGalleryPhoto::className(), ['owner_id' => 'id'])->andWhere(['status' => '1'])->orderBy('rank'); 
		
		return $items;
    }
	
	public function getVideos() {
		$items = \common\models\Files::find()->where(['status' => 1, 'id_type_file_fk' => 1, 'id_fk' => $this->id])->orderby('id desc')->all(); 
		
		return $items;
	}
	
	public static function getList() {
		$galleries = CmsGallery::find()->where(['status' => 1])->andWhere('title is not null')->orderby('title')->all();
		
		return ($galleries) ? $galleries : [];
	}
	
	public function getGallery() {
		return \backend\Modules\Cms\models\CmsGallery::findOne($this->fk_gallery_id);
    }
    
    public static function getTypes() {
        return [0 => 'Standardowa galeria', 1 => 'Slider', 2 => 'Książka'];
    }
    
    public function getTerm() {
		return \backend\Modules\Eg\models\Visit::find()->where(['id_gallery_fk' => $this->id])->one();
    }
	
	public function getBooks() {
		return self::find()->where(['status' => 1, 'view_type' => 2])->all();
	}
	
	public function getFirstPhoto() {
		return \backend\Modules\Cms\models\CmsGalleryPhoto::find()->where(['status' => 1, 'owner_id' => $this->id])->orderby('rank')->one();
	}
}
