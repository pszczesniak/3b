<?php

namespace backend\Modules\Cms\models;

use Yii;

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use backend\Modules\Cms\models\CmsCategoryArch;
use backend\Modules\Cms\models\CmsPage;
use common\models\Translations;

/**
 * This is the model class for table "{{%cms_category}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property string $title
 * @property string $title_slug
 * @property string $content
 * @property string $short_content
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $hits
 * @property integer $is_calendar
 * @property integer $editable
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsCategory extends \yii\db\ActiveRecord
{
     public $user_action = 'create';
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'hits', 'is_calendar', 'editable', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['title'], 'required'],
            [['content', 'short_content', 'meta_description', 'details_config', 'template_view', 'list_view'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title', 'title_slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'title' => Yii::t('app', 'Title'),
            'title_slug' => Yii::t('app', 'Title Slug'),
            'content' => Yii::t('app', 'Content'),
            'short_content' => Yii::t('app', 'Short Content'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'hits' => Yii::t('app', 'Hits'),
            'is_calendar' => Yii::t('app', 'Is Calendar'),
            'editable' => Yii::t('app', 'Editable'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'template_view' => 'Szablon strony',
            'list_view' => 'Szablon listy wpisów'
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			} else { 
				$modelArch = new CmsCategoryArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"slug" => [
				'class' => SluggableBehavior::className(),
				'attribute' => 'title',
				'slugAttribute' => 'title_slug',
			],
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getItems()
    {
        $items = $this->hasMany(\backend\Modules\Cms\models\CmsPage::className(), ['fk_category_id' => 'id']); 
		
		return $items;
    }
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 2])->one();
        
        if(!$model) {
            $category = CmsCategory::findOne($id);
            
            $attributes['title'] = $category->title;
            $attributes['short_content'] = $category->short_content;
            $attributes['content'] = $category->content;
            $attributes['meta_title'] = $category->meta_title;
            $attributes['meta_keywords'] = $category->meta_keywords;
            $attributes['meta_description'] = $category->meta_description;
            
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 2;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
        }
        
        return $model;
    }
    
    public static function getListView() {
        if(isset(\Yii::$app->params['categoryListViews']))
            $listViews = \Yii::$app->params['categoryListViews'];
        else 
            $listViews = [ 0 => 'widok podstawowy' ];
        
        return $listViews;
    }

}
