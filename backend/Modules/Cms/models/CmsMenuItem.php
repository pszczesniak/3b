<?php

namespace backend\Modules\Cms\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

use common\models\Translations;

/**
 * This is the model class for table "{{%cms_menu_item}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property integer $fk_type_id
 * @property integer $fk_menu_id
 * @property integer $fk_cms_element_id
 * @property string $link_href
 * @property integer $menu_level
 * @property string $name
 * @property string $name_langs
 * @property integer $rank
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsMenuItem extends \yii\db\ActiveRecord
{

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_menu_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'fk_type_id', 'fk_menu_id', 'fk_cms_element_id', 'menu_level', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['fk_type_id', 'fk_menu_id', 'name'], 'required'],
            [['name_langs', 'html_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['link_href', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'fk_type_id' => Yii::t('app', 'Fk Type ID'),
            'fk_menu_id' => Yii::t('app', 'Fk Menu ID'),
            'fk_cms_element_id' => Yii::t('app', 'Fk Cms Element ID'),
            'link_href' => Yii::t('app', 'Link Href'),
            'menu_level' => Yii::t('app', 'Menu Level'),
            'name' => Yii::t('app', 'Tytuł'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'rank' => Yii::t('app', 'Rank'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
				$this->menu_level = 1;
				$this->rank = $this->getCountByMenuID($this->fk_menu_id);
			} /*else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getCountByMenuID($mid) {
		$items = $this->find()->where('fk_menu_id = :mid and menu_level = :level', [':mid'=>$mid, ':level'=>1 ] )->count();
		return $items + 1;
	}
	
	public function getChildren()  {
        //return $this->hasMany(CmsMenuItem::className(), ['fk_id' => 'id']);
		$items = $this->hasMany(self::className(), ['fk_id' => 'id'])->andWhere(['status' => '1'])->orderBy('rank'); 
		if(is_array($items)) {
            if(count($items) > 0)
                return $items;
            else
                return false;
        } else {
            return false;
        }
    }
	
	public function getUrl() {
		$url = '/';
		if($this->fk_type_id == 1) {
			$page = \backend\Modules\Cms\models\CmsPage::findOne($this->fk_cms_element_id);
			if($page->fk_category_id != 0) {
				//$url = \backend\modules\cms\models\CmsCategory::findOne($page->fk_category_id)->title_slug.'/'.$page->title_slug;
				$url = Url::to(['/site/cpage', 'pid' => $page->id, 'pslug' => $page->title_slug, 'cslug' => \backend\Modules\Cms\models\CmsCategory::findOne($page->fk_category_id)->title_slug]);
			} else {
				if($page->id != 1)
					$url = Url::to(['/site/page', 'pid' => $page->id, 'slug' => $page->title_slug ]);
				else
					$url = Url::to(['/site/index']);
			}
		} else if($this->fk_type_id == 2) {
			$category = \backend\Modules\Cms\models\CmsCategory::findOne($this->fk_cms_element_id);
			$url = Url::to(['/site/category', 'cslug' => $category->title_slug, 'cid' => $category->id]);
		} else if($this->fk_type_id == 3) {
            if( substr($this->link_href, 0, 4) == 'http' ) {
                $url = $this->link_href;
            } else {
                $url = \Yii::$app->urlManager->createAbsoluteUrl(['site/index']).$this->link_href;
            }
        } else if($this->fk_type_id == 5) {
			//$category = \backend\Modules\Cms\models\CmsGallery::findOne($this->fk_cms_element_id);
			$url = Url::to(['/site/gallery', 'gid' => $this->fk_cms_element_id]);
		}  else if($this->fk_type_id == 6) {
			$category = \backend\Modules\Shop\models\ShopCategory::findOne($this->fk_cms_element_id);
			$url = Url::to(['/shop/category', 'cid' => $this->fk_cms_element_id, 'cslug' => $category->slug]);
		} else {
			$url = "#";
		}
		return $url;
	}
    
   /* public function getTranslate() {
        $langs = \common\models\Lang::find()->where(['is_active' => 1, 'is_main' => 0])->all();
        $translate = [];
        if(!$this->name_langs) {
            foreach($langs as $key => $lang) {
                $translate[$lang->lang] = '';
            }
        } else {
            $translate = \yii\helpers\Json::decode($this->name_langs);
        }
        
        return $translate;
    }*/
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 7])->one();
        
        if(!$model) {
            $item = CmsMenuItem::findOne($id);
            
            $attributes['name'] = $item->name;
            
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 7;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
        }
        
        return $model;
    }

}
