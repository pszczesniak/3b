<?php

namespace backend\Modules\Cms\models;
use Yii;

/**
 * This is the model class for table "{{%cms_gallery_photo}}".
 *
 * @property integer $id
 * @property integer $fk_gallery_id
 * @property string $name
 * @property string $description
 * @property integer $rank
 * @property string $file_name
 * @property string $file_ext
 * @property string $file_type
 * @property integer $isShort
 * @property integer $isLink
 * @property integer $link_type
 * @property integer $link_fk_id
 * @property string $link_href
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsGalleryPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_gallery_photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_gallery_id', 'rank', 'is_short', 'is_link', 'is_show', 'link_type', 'link_fk_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['type'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            ['link_href', 'url', 'defaultScheme' => 'http'], 
            [['name', 'file_name', 'file_ext', 'file_type', 'link_href'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_gallery_id' => Yii::t('app', 'Fk Gallery ID'),
            'name' => Yii::t('app', 'Podpis'),
            'description' => Yii::t('app', 'Description'),
            'rank' => Yii::t('app', 'Rank'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_ext' => Yii::t('app', 'File Ext'),
            'file_type' => Yii::t('app', 'File Type'),
            'isShort' => Yii::t('app', 'Is Short'),
            'isLink' => Yii::t('app', 'Is Link'),
            'link_type' => Yii::t('app', 'Link Type'),
            'link_fk_id' => Yii::t('app', 'Link Fk ID'),
            'link_href' => Yii::t('app', 'Link Href'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'is_show' => 'Prezentuj gościom'
        ];
    }
    
    public function beforeSave($insert) {
		/*if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
			} 
			return true;
		} else { 
						
			return false;
		}*/
        
        if ($this->link_href && preg_match("#http?://#", $this->link_href) === 0){
            $this->link_href = 'http://'.$this->link_href;
        }
		return true;
	}
    
    
}
