<?php

namespace app\Modules\Cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_widget_arch}}".
 *
 * @property integer $id
 * @property integer $fk_widget_id
 * @property integer $fk_cms_widget_id
 * @property string $user_action
 * @property string $version_data
 * @property string $created_at
 * @property integer $created_by
 */
class CmsWidgetArch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_arch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_widget_id', 'fk_cms_widget_id', 'created_by'], 'required'],
            [['fk_widget_id', 'fk_cms_widget_id', 'created_by'], 'integer'],
            [['version_data'], 'string'],
            [['created_at'], 'safe'],
            [['user_action'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_widget_id' => Yii::t('app', 'Fk Widget ID'),
            'fk_cms_widget_id' => Yii::t('app', 'Fk Cms Widget ID'),
            'user_action' => Yii::t('app', 'User Action'),
            'version_data' => Yii::t('app', 'Version Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
