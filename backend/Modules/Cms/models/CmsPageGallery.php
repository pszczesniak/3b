<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%cms_page_gallery}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property integer $type
 * @property integer $fk_element_id
 * @property integer $fk_gallery_id
 * @property integer $gallery_mode
 * @property string $gallery_label
 * @property string $gallery_label_langs
 * @property integer $rank
 * @property string $rank_url
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsPageGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page_gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'type', 'fk_element_id', 'fk_gallery_id', 'gallery_mode', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['fk_gallery_id', 'fk_id'], 'required'],
            [['gallery_label_langs'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['gallery_label', 'rank_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'type' => Yii::t('app', 'Type'),
            'fk_element_id' => Yii::t('app', 'Fk Element ID'),
            'fk_gallery_id' => Yii::t('app', 'Fk Gallery ID'),
            'gallery_mode' => Yii::t('app', 'Gallery Mode'),
            'gallery_label' => Yii::t('app', 'Gallery Label'),
            'gallery_label_langs' => Yii::t('app', 'Gallery Label Langs'),
            'rank' => Yii::t('app', 'Rank'),
            'rank_url' => Yii::t('app', 'Rank Url'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
	
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) 
				$this->created_by = 1;
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
	
	public function getGallery()
    {
        return $this->hasOne(\backend\Modules\Cms\models\CmsGallery::className(), ['id' => 'fk_gallery_id']);
    }
}
