<?php

namespace backend\Modules\cms\models;

use Yii;

/**
 * This is the model class for table "{{%cms_menu_type}}".
 *
 * @property integer $id
 * @property integer $type_element
 * @property string $name
 * @property string $name_lang
 * @property string $describe
 * @property string $describe_lang
 */
class CmsMenuType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_menu_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_element', 'name'], 'required'],
            [['type_element'], 'integer'],
            [['name_lang', 'describe', 'describe_lang'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_element' => Yii::t('app', 'Type Element'),
            'name' => Yii::t('app', 'Name'),
            'name_lang' => Yii::t('app', 'Name Lang'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_lang' => Yii::t('app', 'Describe Lang'),
        ];
    }
	
	public static function getList($type)
    {
        $items = \backend\Modules\Cms\models\CmsMenuType::find()->where(['type_element' => $type])->all(); 
		/*foreach($items as $key=>$value) { 
			$childrenTmp = $this->hasMany(\common\models\cms\CmsMenuItem::className(), ['fk_id' => 'id'])->andWhere(['status' => '1', 'menu_level' => 2, 'fk_id' => $key])->orderBy('rank');
			$value->getChildren();
		}*/
		return $items;
    }
}
