<?php

namespace backend\Modules\Cms\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\Translations;


/**
 * This is the model class for table "{{%cms_widget_accordion_item}}".
 *
 * @property integer $id
 * @property integer $fk_id
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $rank
 * @property string $html_options
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class CmsWidgetAccordionItem extends \yii\db\ActiveRecord
{
    public $user_action;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_widget_accordion_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_id', 'rank', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['describe'], 'required', 'when' => function($model) { return ($model->user_action != 'question-faq'); }],
            [['name_langs', 'describe_langs', 'html_options'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'user_action'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fk_id' => Yii::t('app', 'Fk ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Opis'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'rank' => Yii::t('app', 'Rank'),
            'html_options' => Yii::t('app', 'Html Options'),
            'status' => Yii::t('app', 'Opublikuj'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = 1;
				$this->rank = $this->getCountByWidgetID($this->fk_id);
			} /*else { 
				$modelArch = new CmsPageArch();
				$modelArch->fk_id = $this->id;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCountByWidgetID($mid) {
		$items = $this->find()->where('fk_id = :mid and status = :status', [':mid'=>$mid, ':status'=>1 ] );
		return count($items)+1;
	}
    
    public static function getTranslate($id, $lang) {
        $model = Translations::find()->where(['fk_id' => $id, 'lang' => $lang, 'type_id' => 6])->one();
        
        if(!$model) {
            $item = CmsWidgetAccordionItem::findOne($id);
            
            $attributes['name'] = $item->name;
            $attributes['describe'] = $item->describe;
            
            $model = new Translations();
            $model->fk_id = $id;
            $model->type_id = 6;
            $model->lang = $lang;
            $model->attr_data = \yii\helpers\Json::encode($attributes);
            $model->attributes = $attributes;
            $model->save();
        } else {
            $model->attributes = \yii\helpers\Json::decode($model->attr_data);
        }
        
        return $model;
    }
}
