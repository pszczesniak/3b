<?php

namespace app\Modules\Cms;

class Modules extends \yii\base\Module
{
    public $controllerNamespace = 'app\Modules\Cms\controllers';

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // your custom code here
        /*if(!\Yii::$app->user->isGuest) {
            $rule = (isset(\Yii::$app->user->identity->role) && \Yii::$app->user->identity->role->name == 'admin');
        } else $rule = true;
        
        if( $rule ) 
            return true;
        else
            return \Yii::$app->getResponse()->redirect('/site/noaccess');*/
		
		return true;
       
    }
    
    public function init()
    {
        \Yii::$app->session->set('user.module','cms');
        $grants = [];
        
        $auth = \Yii::$app->authManager;
        $roles = $auth->getRolesByUser(\Yii::$app->user->identity->id);//var_dump($roles); exit;
        foreach($roles as $key=>$value) {
            $grantsRole = $auth->getPermissionsByRole($value->name); 
            foreach($grantsRole as $key1=>$value1) {
                array_push($grants, $key1);
            }                      
            array_push($grants, $key);
        }
		//\Yii::$app->user->identity->module = 'cms';
		//\Yii::$app->user->setAttribute('module', 'cms');
		//var_dump(\Yii::$app->user->identity);
		parent::init();

        // custom initialization code goes here
    }
}
