<?php

namespace app\Modules\Cms\controllers;

use Yii;
use backend\Modules\Cms\models\CmsMenu;
use backend\Modules\Cms\models\CmsMenuItem;
use backend\Modules\Cms\models\CmsMenuSearch;
use backend\Modules\Cms\models\CmsCategory;
use backend\Modules\Cms\models\CmsPage;
use backend\Modules\Cms\models\CmsGallery;

use backend\Modules\Shop\models\ShopCategory;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * CmsMenuController implements the CRUD actions for CmsMenu model.
 */
class CmsmenuController extends Controller
{
    public $enableCsrfValidation = false;
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
					'icreate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsMenu models.
     * @return mixed
     */

	public function actionIndex()
    {
        $searchModel = new CmsMenuSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["CmsMenuSearchs"]["status"] = 1;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['CmsMenuSearch'])) {
            $params = $_GET['CmsMenuSearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(p.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }

            if(isset($params['type']) && !empty($params['type']) ) {
				array_push($where, "fk_type_id = ".$params['type']);
            }
        } 
        
        $sortColumn = 'm.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'p.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'p.status';
		
		$query = (new \yii\db\Query())
            ->select(['m.id as id', 'm.title as title', 'fk_type_id', 'mt.name as tname', 'is_system'])
            ->from('{{%cms_menu}} as m')
            ->join('JOIN', '{{%cms_menu_type}} as mt', 'mt.id = m.fk_type_id')
            ->where( ['m.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', 2 => 'info', '3' => 'purple', 4 => 'danger'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmsmenu/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= ( ($value['is_system'] == 1) ? '<a href="'.Url::to(['/cms/cmsmenu/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove"  data-table="#table-pages"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
			$tmp['type'] = $value['tname'];
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}


    /**
     * Displays a single CmsMenu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsMenu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionIcreate() {
		//$model = CmsWidgetAccordion::findOne($id);->findModel($id);
		//Yii::$app->formatter->dateFormat = 'yyyy-mm-dd';
		$model = new CmsMenuItem();
        
        $itemsPages = ArrayHelper::map(CmsPage::find()->where(['fk_category_id' => null])->orderby('title')->all(), 'id', 'title');
		$itemsCategories = ArrayHelper::map(CmsCategory::find()->orderby('title')->all(), 'id', 'title');
		$itemsGalleries = ArrayHelper::map(CmsGallery::find()->orderby('title')->all(), 'id', 'title');
        $itemsCatalogs = [];//ArrayHelper::map(ShopCategory::find()->orderby('name')->all(), 'id', 'name');
        
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			$model->fk_menu_id = $_POST['menu_id']; 
			$model->fk_type_id = $_POST['item_type']; 
			if(isset($_POST['element_id']))
				$model->fk_cms_element_id = $_POST['element_id']; 
			else $model->fk_cms_element_id = 0;
			if(isset($_POST['link_href']))
				$model->link_href = $_POST['link_href'];
			$model->name = $_POST['element_name']; 
	        if (/*$model->load(Yii::$app->request->post()) &&*/ $model->save()) {
				$res = array(
					//'success' => true,'id' => $model->id, 'text' => $model->name, 'html' => $this->renderPartial('menuElement', [ 'title' => $model->name, 'type' => $_POST['item_type'], 'id' => $model->id, 'items' => [], 'level' => 0 ]) 
                    'success' => true,'id' => $model->id, 'text' => $model->name, 'html' => $this->renderPartial('menuElement', [ 'model' => $model, 'level' => 0, 'itemsPages' => $itemsPages, 'itemsCategories' => $itemsCategories, 'itemsGalleries' => $itemsGalleries,'itemsCatalogs' => $itemsCatalogs ])
				);
			} else {
				$errors = $model->getErrors();
					foreach($errors as $key=>$value) {
						$error = $value;
					}
				$res = array(
					'error'    => $error ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$errors = $model->getErrors();
				foreach($errors as $key=>$value) {
					$error = $value;
				}
				$res = array(
					'error'    => $error ,
					'success' => false,
				);
			return $res;
		}

	}
	
	public function actionIupdate($id) {
		$model = CmsMenuItem::findOne($id);
		//$model->name = $_POST['element_name']; 
        
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		    if(isset(Yii::$app->request->post()['Lang']))
                $model->name_langs = \yii\helpers\Json::encode(Yii::$app->request->post()['Lang']);
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true
				);
			} else {
				$errors = $model->getErrors();
                foreach($errors as $key=>$value) {
                    $error = $value;
                }
                
                $res = array(
					'error'    =>  $error ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}

	}
	
	public function actionIdelete($id) {
		$model = CmsMenuItem::findOne($id);
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		
	        if ($model->delete()) {
				$res = array(
					'success' => true, 'structure' => true, 'id' => $model->id
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}

	}

    public function actionIsave( ) {
	    $items = \yii\helpers\Json::decode( Yii::$app->request->post('items') );
		
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		    $rank = 1; $rankChild = 1; $rankChild2 = 1; 
			//print_r($items[0]);
			for($i = 0; $i < count($items); ++$i) {
				$model = CmsMenuItem::findOne($items[$i]['id']);
				$model->menu_level = 1;
				$model->rank = $rank;
				$model->save(); ++$rank;
				if(isset($items[$i]['children'])) {
					$childrenTmp = $items[$i]['children'];
					for($j = 0; $j < count($childrenTmp); ++$j) {
						$model = CmsMenuItem::findOne($childrenTmp[$j]['id']);
						$model->fk_id = $items[$i]['id'];
						$model->menu_level = 2;
						$model->rank = $rankChild;
						$model->save(); ++$rankChild;
						
						if(isset($childrenTmp[$j]['children'])) {
							$childrenTmp2 = $childrenTmp[$j]['children'];
							for($k = 0; $k < count($childrenTmp2); ++$k) {
								$model = CmsMenuItem::findOne($childrenTmp2[$k]['id']);
								$model->fk_id = $childrenTmp[$j]['id'];
								$model->menu_level = 3;
								$model->rank = $rankChild2;
								$model->save(); ++$rankChild2;
							}
							$rankChild2 = 1;
						}
					}
					$rankChild = 1;
				}
			}
			
			$res = array(
				'success' => true, 'alert' => 'Zmiany zostały zapisane'
			);
			
	 
			return $res;
		} else {
			$res = array(
					/*'errors'    =>  $model->getErrors() ,*/
					'success' => false, 'alert' => 'Zmiany nie zostały zapisane'
				);
			return $res;
		}
	}
    
    /*public function actionTranslate($id) {
		$model = CmsMenu::findOne($id);
		//$model->name = $_POST['element_name']; 
        
		return $this->renderPartial('translate', ['model' => $model]);

	}*/
    
    public function actionSavetranslate() {
        if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true
				);
			} else {
				$errors = $model->getErrors();
                foreach($errors as $key=>$value) {
                    $error = $value;
                }
                
                $res = array(
					'error'    =>  $error ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}
    }

    public function actionUpdate($id)  {
        $model = $this->findModel($id);
	
		$buildMenu = $this->buildAdminMenu($model->items, 0);
		
		$itemsPages = ArrayHelper::map(CmsPage::find()->where(['status' => 1])->andWhere('fk_category_id is null')->orderby('title')->all(), 'id', 'title');
		$itemsCategories = ArrayHelper::map(CmsCategory::find()->orderby('title')->all(), 'id', 'title');
		$itemsGalleries = ArrayHelper::map(CmsGallery::find()->orderby('title')->all(), 'id', 'title');
        $itemsCatalogs = []; //ArrayHelper::map(ShopCategory::find()->orderby('name')->all(), 'id', 'name');
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'buildMenu' => $buildMenu, 'itemsPages' => $itemsPages, 'itemsCategories' => $itemsCategories, 'itemsGalleries' => $itemsGalleries,'itemsCatalogs' => $itemsCatalogs
            ]);
        }
    }

    public function actionDelete($id)  {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	private function buildAdminMenu($items,$level) {
	
        $itemsPages = ArrayHelper::map(CmsPage::find()->where(['fk_category_id' => null])->orderby('title')->all(), 'id', 'title');
		$itemsCategories = ArrayHelper::map(CmsCategory::find()->orderby('title')->all(), 'id', 'title');
		$itemsGalleries = ArrayHelper::map(CmsGallery::find()->orderby('title')->all(), 'id', 'title');
        $itemsCatalogs = []; //ArrayHelper::map(ShopCategory::find()->orderby('name')->all(), 'id', 'name');
		return $this->renderPartial('menu', ['items' => $items, 'level' => $level, 'itemsPages' => $itemsPages, 'itemsCategories' => $itemsCategories, 'itemsGalleries' => $itemsGalleries, 'itemsCatalogs' => $itemsCatalogs]);
	}
    
    public function actionTranslate($id) {
        $model = CmsMenuItem::findOne($id);
        
        return $this->renderAjax('_tabTranslate', ['model'=>$model->getTranslate($model->id, $_GET['lang']), 'lang' => $_GET['lang']]);
    }
	
	/*public function actionMenuelement($id, $type, $title) {
		return $this->renderPartial('menuElement', ['title' => $title, 'type' => $type, 'id' => $id ]);
	}*/
}
