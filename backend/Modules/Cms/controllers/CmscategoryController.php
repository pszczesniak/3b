<?php

namespace app\Modules\Cms\controllers;

use Yii;
use backend\Modules\Cms\models\CmsCategory;
use backend\Modules\Cms\models\CmsCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * CmsCategoryController implements the CRUD actions for CmsCategory model.
 */
class CmscategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = true; //($action->id !== "save-order"); // <-- here
        return parent::beforeAction($action);
    }

    /**
     * Lists all CmsCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmsCategorySearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["CmsCategorySearch"]["status"] = 1;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['CmsCategorySearch'])) {
            $params = $_GET['CmsCategorySearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(c.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
           /* if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "p.fk_status_id = ".$params['status']);
            }*/
        } 
        
        $sortColumn = 'c.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'c.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'c.status';
		
		$query = (new \yii\db\Query())
            ->select(['c.id as id', 'c.title as title', 'c.title_slug as title_slug', 'c.status as status'])
            ->from('{{%cms_category}} as c')
            ->where( ['c.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', 2 => 'info', '3' => 'warning', 4 => 'danger'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.\Yii::$app->urlManagerFrontEnd->createUrl('/'.\Yii::$app->params['frontendNativeLang'].'c-'.$value['id'].'-'.$value['title_slug']).'" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmscategory/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="'.Url::to(['/cms/cmscategory/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove"  data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            //$tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$value['sname'].'</span>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single CmsCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CmsCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		//Yii::$app->formatter->dateFormat = 'yyyy-mm-dd';
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			$model->user_action = $_POST['CmsCategory']['user_action']; 
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true,
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		}
    }

    /**
     * Deletes an existing CmsCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Dane zostały przeniesione do kosza', 'id' => $id];
            
        } else {
            return $this->redirect(['index']);
        }
    }
    /**
     * Finds the CmsCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionTranslate($id) {
        $model = $this->findModel($id);
        
        return $this->renderAjax('tabs/_tabTranslate', ['model'=>$model->getTranslate($model->id, $_GET['lang']), 'lang' => $_GET['lang']]);
    }
}
