<?php

namespace app\Modules\Cms\controllers;

use Yii;
use backend\Modules\Cms\models\CmsWidget;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use backend\Modules\Cms\models\CmsWidgetBox;
use backend\Modules\Cms\models\CmsWidgetAccordion;
use backend\Modules\Cms\models\CmsWidgetAccordionItem;
use backend\Modules\Cms\models\CmsWidgetFilesSet;

use yii\web\UploadedFile;

/**
 * CmsWidgetController implements the CRUD actions for CmsWidget model.
 */
class CmswidgetController extends Controller
{
    
	public function actions()
	{
		return [
            'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'CmsWidgetBox' => CmsWidgetBox::className()
				]
			],
		];
	}
	
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsWidget models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CmsWidget::find()->where('is_deleted <= 0'),
        ]);

        return $this->render('index', [
            'searchModel' => new CmsWidget(),
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['CmsWidget'])) {
            $params = $_GET['CmsWidget'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(w.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['type_widget']) && !empty($params['type_widget']) ) {
				array_push($where, "type_widget = ".$params['type_widget']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "w.status = ".$params['status']);
            }
        } 
        
        $sortColumn = 'w.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'w.title';
        if( isset($post['sort']) && $post['sort'] == 'type_widget' ) $sortColumn = 'w.type_widget';
		
		$query = (new \yii\db\Query())
            ->select(['w.id as id', 'w.title as title', 'type_widget', 'fk_element_id'])
            ->from('{{%cms_widget}} as w');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = CmsWidget::getTypes();
        $color = [1 => 'primary', 2 => 'pink', 3 => 'orange', 10 => 'purple'];
        
		foreach($rows as $key=>$value) {
            if($value['type_widget'] == 1)
                $url = Url::to(['cmswidget/ubox', 'id' => $value['fk_element_id']]);
            else if($value['type_widget'] == 2)
                $url = Url::to(['cmswidget/uaccordion', 'id' => $value['fk_element_id']]);
            else if($value['type_widget'] == 3)
                $url = Url::to(['cmswidget/ufilesset', 'id' => $value['fk_element_id']]);
            else $url = false;
            
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= ( ($value['type_widget'] != 10) ? '<a href="'.$url.'" class="btn btn-sm btn-default" data-table="#table-items"><i class="fa fa-pencil"></i></a>' : '');
            $actionColumn .= ( ($value['type_widget'] != 10) ? '<a href="'.Url::to(['/cms/cmspage/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            $tmp['type_widget'] = '<span class="label label-'.$color[$value['type_widget']].'">'.$label[$value['type_widget']].'</span>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" data-table="#table-items" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}


    /**
     * Displays a single CmsWidget model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsWidget model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsWidget();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    if($model->type_widget == 1)
				return $this->redirect(['ubox', 'id' => $model->fk_element_id]);
			else if($model->type_widget == 2)
				return $this->redirect(['uaccordion', 'id' => $model->fk_element_id]);
			else if($model->type_widget == 3)
				return $this->redirect(['ufilesset', 'id' => $model->fk_element_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax()
    {
        $model = new CmsWidget();
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return array('success' => true, 'action' => 'createByInput', 'alert' => 'Dodatek <b>'.$model->title.'</b> został utworzony', 'id' => $model->id, 'name' => $model->title, 'input' => 'cmspage-widgets_list' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model]), 'errors' => $model->getErrors() );	
			}     	
		} else {
            return  $this->renderAjax('_formAjax', [  'model' => $model ]) ;	
		}
    }

    /**
     * Updates an existing CmsWidget model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionUbox($id)
    {
        
		//$model = $this->findModel($id);
		$model = CmsWidgetBox::findOne($id);
        $widget = CmsWidget::find()->where(['type_widget' => 1, 'fk_element_id' => $id])->one();
        $widget->title = $model->name;
        $widget->save();
		//!== null

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['ubox', 'id' => $model->id]);
        } else {
            return $this->render('ubox', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionUaccordion($id)
    {
        
		//$model = $this->findModel($id);
		$model = CmsWidgetAccordion::findOne($id);
        $widget = CmsWidget::find()->where(['type_widget' => 2, 'fk_element_id' => $id])->one();
        $widget->title = $model->name;
        $widget->save();
		//!== null

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['uaccordion', 'id' => $model->id]);
        } else {
            return $this->render('uaccordion', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionUfilesset($id)
    {
        
		//$model = $this->findModel($id);
		$model = CmsWidgetFilesSet::findOne($id);
        $widget = CmsWidget::find()->where(['type_widget' => 3, 'fk_element_id' => $id])->one();
        $widget->title = $model->name;
        $widget->save();
		//!== null

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('ufilesset', [
                'model' => $model,
            ]);
        }*/
		if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            if ($model->upload()) {
                // file is uploaded successfully
                return $this->render('ufilesset', ['model' => $model]);
            }
        }
		
		return $this->render('ufilesset', ['model' => $model]);
    }
	
	

    /**
     * Deletes an existing CmsWidget model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsWidget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsWidget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsWidget::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionCreateaccordionitem($id)
    {
        
		$model = new CmsWidgetAccordionItem();
        $model->fk_id = $id;
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
		    if($model->save()) {
				return array('result' => true);	
		 	} else {
				return array('result' => false, 'html' => $this->renderPartial('_formAccordionItem', [  'model' => $model, 'id' => $id, ]) );	
		    }
			
			return $this->renderPartial('_formAccordionItem', [
                'model' => $model, 'id' => $id,
            ]);
        } else {
            return $this->renderPartial('_formAccordionItem', [
                'model' => $model, 'id' => $id, 
            ]);
        }
    }
	
	public function actionAccordionitems($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$items = [];
		$tmp = [];
		
		$itemsData = \backend\Modules\Cms\models\CmsWidgetAccordionItem::find()->where('status >= 0 and fk_id = '.$id)->orderby('status desc, rank')->all();
		
		foreach($itemsData as $key=>$value) {
			$actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmswidget/updateaccordionitem', 'id' => $value->id]).'" class="btn btn-xs btn-success btn-icon"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

            $actionColumn .= '<a href="'.Url::to(['/cms/cmswidget/deleteaccordionitem', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-items" data-confirm="Czy na pewno usunąć ten wpis?" >'.Yii::t('lsdd', 'Delete').'</a>';
            $actionColumn .= '</div>';
            
            $tmp['name'] = (($value->status == 0) ? '<span class="text--blue">Nowe pytanie z portalu</span> ' : '' ).$value->name;
            $tmp['action'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" data-table="#table-items" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" data-table="#table-items" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
            $tmp['index'] = $value->rank;
            $tmp['id'] = $value->id;
			
			array_push($items, $tmp); $tmp = [];
		}
		
		return $items;
	}
    
    public function actionUpdateaccordionitem($id)
    {
        
		$model = CmsWidgetAccordionItem::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['uaccordion', 'id' => $model->fk_id]);
        } else {
            return $this->render('updateAccordionItem', [
                'model' => $model, 
            ]);
        }
    }
    
    public function actionDeleteaccordionitem($id)
    {
        \backend\Modules\Cms\models\CmsWidgetAccordionItem::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return array('result' => true, 'table' => '#table-items');	
        //return $this->redirect(['index']);
    }
    
    public function actionAccordionitemssave($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = \backend\Modules\Cms\models\CmsWidgetAccordionItem::findOne($item['id']);
                $model->rank = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany nie zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
    
    public function actionBtranslate($id) {
        $model = \backend\Modules\Cms\models\CmsWidgetBox::findOne($id);
        
        return $this->renderAjax('_ubox_translate', ['model'=>$model->getTranslate($model->id, $_GET['lang']), 'lang' => $_GET['lang']]);
    }
    
    public function actionAitranslate($id) {
        $model = \backend\Modules\Cms\models\CmsWidgetAccordionItem::findOne($id);
        
        return $this->renderAjax('_uaccordion_translate', ['model'=>$model->getTranslate($model->id, $_GET['lang']), 'lang' => $_GET['lang']]);
    }
    
    public function actionUnverified($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $rows = CmsWidgetAccordionItem::find()->where(['status' => 0, 'fk_id' => $id])->all();
        $fields = [];
        
        foreach($rows as $key=>$value) {           
            $actionColumn = '<div class="edit-btn-group">';
            //$actionColumn .= '<button id="edit-bed" class="btn btn-xs btn-success gridViewModal" data-toggle="modal" data-target="#modal-grid" data-id=%d data-title="'.Yii::t('lsdd', 'Edit').'">'.Yii::t('lsdd', 'Edit').'</button>';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmswidget/updateaccordionitem', 'id' => $value->id]).'" class="btn btn-xs btn-success btn-icon"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

            $actionColumn .= '<a href="'.Url::to(['/cms/cmswidget/deleteaccordionitem', 'id' => $value->id]).'" class="btn btn-xs btn-danger remove" data-table="#table-items" data-confirm="Czy na pewno usunąć ten wpis?" >'.Yii::t('lsdd', 'Delete').'</a>';
            $actionColumn .= '</div>';
			$tmp['title'] = $value->name;
            $tmp['created'] = $value->created_at;
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" data-table="#table-items" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}
        return $fields;
    }
    
}
