<?php

namespace app\Modules\Cms\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;
use backend\Modules\Cms\models\CmsSite;
use backend\models\SiteStepsForm;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        }
		
		$model = CmsSite::findOne(1);
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true,
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else
			return $this->render('index', ['model' => $model]);
    }
	
	public function actionFiles()
    {
        //FilemanagerTinyAssets::register($this->view);
		return $this->render('files');
    }
	
	public function actionSteps() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = CmsSite::findOne(1);
		if($model->custom_data)	{	
			$fieldsDataAll = \yii\helpers\Json::decode($model->custom_data);
            $fieldsData = (isset($fieldsDataAll['steps'])) ? $fieldsDataAll['steps'] : [];
		} else {
			$fieldsData = [];
        }
			
		$fields = [];
		$tmp = [];
		
        
        $actionColumn = '<div class="edit-btn-group">';
		$actionColumn .= '<a href="'.Url::to(['/cms/default/createajax', 'id' =>'2']).'" class="btn btn-xs btn-success btn-icon edit" data-table="#table-steps" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

		$actionColumn .= '<button class="btn btn-xs btn-danger item_remove" data-table="#table-steps">'.Yii::t('lsdd', 'Delete').'</button>';
		$actionColumn .= '</div>';
		foreach($fieldsData as $key=>$value) {
			
			$tmp['describe'] = $value['describe'];
            $tmp['action'] = sprintf($actionColumn, $key, $key);
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" data-table="#table-steps" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" data-table="#table-steps" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $key;//time();
            $tmp['index'] = $key;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}
	
	public function actionCreateajax($id) {
		
		$model = new SiteStepsForm();
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
			if($model->validate() ) {
				$actionColumn = '<div class="edit-btn-group">';
				$actionColumn .= '<a href="'.Url::to(['/cms/default/createajax', 'id' =>'2']).'" class="btn btn-xs btn-success btn-icon edit" data-table="#table-steps" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

				$actionColumn .= '<button class="btn btn-xs btn-danger item_remove" data-table="#table-steps">'.Yii::t('lsdd', 'Delete').'</button>';
				$actionColumn .= '</div>';
				
				$data['describe'] = $model->describe;
				$data['action'] = sprintf($actionColumn, 0, 0);
				$data['move'] = '<a class="btn btn-default btn-sm action up" data-table="#table-steps" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" data-table="#table-steps" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
				
				return array('result' => true, 'row' => $data, 'type' => $id, 'index' => $model->index);	
			} else {
				return array('result' => false, 'html' => $this->renderPartial('_formItem', [ 'model' => $model, 'type' => $id]) );	
			}		
		} else {
			$model->describe = '';
            return  $this->renderPartial('_formItem', [ 'model' => $model, 'type' => $id]) ;	
		}
	}

	public function actionSave() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $res = [];
        $items = Yii::$app->request->post('items');
        $tmp = []; $data = [];
        
		$model = CmsSite::findOne(1);
        $customData = \yii\helpers\Json::decode($model->custom_data);
        
        $model->load(Yii::$app->request->post());
		
        $data['amountFrom'] = $model->amountFrom;
        $data['amountTo'] = $model->amountTo;
        $data['periodFrom'] = $model->periodFrom;
        $data['periodTo'] = $model->periodTo;
        $data['loanNote'] = $model->loanNote;
        $data['loanProvision'] = $model->loanProvision;
        $data['loanInterest'] = $model->loanInterest;
        $data['defaultAmount'] = $model->defaultAmount;
        $data['defaultPeriod'] = $model->defaultPeriod;
        
        $data['steps'] = $customData['steps'];
		
		$model->custom_data = \yii\helpers\Json::encode($data);
		if($model->save()) {
			$res = ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
		} else {
			//var_dump($model->getErrors());
            $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane. Proszę uzupełnić wszystkie dane.', 'errors' => $model->getErrors()];
		}
	
        return $res;
    }
    
    public function actionSavesteps() {
		Yii::$app->response->format = Response::FORMAT_JSON;
        $res = [];
        $items = Yii::$app->request->post('items');
        $tmp = []; $data = []; $steps = [];
        
		$model = CmsSite::findOne(1);
        $customData = \yii\helpers\Json::decode($model->custom_data);
        $data['amountFrom'] = $customData['amountFrom'];
        $data['amountTo'] = $customData['amountTo'];
        $data['periodFrom'] = $customData['periodFrom'];
        $data['periodTo'] = $customData['periodTo'];
        $data['loanNote'] = $customData['loanNote'];
        $data['loanProvision'] = $customData['loanProvision'];
        $data['loanInterest'] = $customData['loanInterest'];
        $data['defaultAmount'] = $customData['defaultAmount'];
        $data['defaultPeriod'] = $customData['defaultPeriod'];
        
        $model->amountFrom = $customData['amountFrom'];
        $model->amountTo = $customData['amountTo'];
        $model->periodFrom = $customData['periodFrom'];
        $model->periodTo = $customData['periodTo'];
        $model->loanNote = $customData['loanNote'];
        $model->loanProvision = $customData['loanProvision'];
        $model->loanInterest = $customData['loanInterest'];
        $model->defaultAmount = $customData['defaultAmount'];
        $model->defaultPeriod = $customData['defaultPeriod'];
		
		foreach($items as $key => $item) {
			$tmp['describe'] = $item['describe'];
			array_push($steps, $tmp);
		}
        $data['steps'] = $steps;
		$model->custom_data = \yii\helpers\Json::encode($data);
		if($model->save()) {
			$res = ['success' => true, 'alert' => 'Zmiany zostały zapisane', 'table' => '#table-steps'];
		} else {
			$res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane', 'table' => '#table-steps'];
		}
	
        return $res;
    }
}
