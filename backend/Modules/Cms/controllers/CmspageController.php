<?php

namespace app\Modules\Cms\controllers;

use Yii;
use backend\Modules\Cms\models\CmsPage;
use backend\Modules\Cms\models\CmsPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;

use yii\web\UploadedFile;

//use linchpinstudios\filemanager\assets\FilemanagerTinyAssets;


/**
 * CmsPageController implements the CRUD actions for CmsPage model.
 */
class CmspageController extends Controller
{
    
	public function actions() {
		//FilemanagerTinyAssets::register($this->view);

		return [
            'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'CmsPage' => CmsPage::className()
				]
			],
		];
	}
	
	public function behaviors()
    {
        return [
           
            'verbs' => [
                'class' => VerbFilter::className(),
                /*'actions' => [
                    'delete' => ['post'],
                ],*/
            ],
        ];
    }

    /**
     * Lists all CmsPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmsPageSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["CmsPageSearch"]["status"] = 1;
        $queryParams["CmsPageSearch"]["fk_category_id"] = 0;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['CmsPageSearch'])) {
            $params = $_GET['CmsPageSearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(p.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['fk_category_id']) && /*!empty($params['fk_category_id'])  &&*/ strlen($params['fk_category_id']) ) {
				if($params['fk_category_id'] == 0)
                    array_push($where, "(fk_category_id = 0 or fk_category_id is null)");
                else
                    array_push($where, "fk_category_id = ".$params['fk_category_id']);
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "p.fk_status_id = ".$params['status']);
            }
        } 
        
        $sortColumn = 'p.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'p.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'p.status';
        if( isset($post['sort']) && $post['sort'] == 'category' ) $sortColumn = 'c.title';
		
		$query = (new \yii\db\Query())
            ->select(['p.id as id', 'p.title as title', 'p.title_slug as title_slug', 'p.fk_status_id as fk_status_id', 'p.status as status', 'c.title as ctitle', 'ps.name as sname'])
            ->from('{{%cms_page}} as p')
            ->join('JOIN', '{{%cms_page_status}} as ps', 'ps.id = p.fk_status_id')
            ->join('LEFT JOIN', '{{%cms_category}} as c', 'c.id = p.fk_category_id')
            ->where( ['p.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', 2 => 'info', '3' => 'purple', 4 => 'danger'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.\Yii::$app->urlManagerFrontEnd->createUrl('/'.\Yii::$app->params['frontendNativeLang'].$value['id'].'-'.$value['title_slug']).'" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmspage/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="'.Url::to(['/cms/cmspage/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove"  data-table="#table-pages"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            $tmp['category'] = $value['ctitle'];
            $tmp['status'] = '<span class="label label-'.$label[$value['fk_status_id']].'">'.$value['sname'].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single CmsPage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)  {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new CmsPage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CmsPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
		/*$options = [
           'tinymce'             => \Yii::$app->urlManager->createUrl('/filemanager/files/tinymce'),
           'properties'          => \Yii::$app->urlManager->createUrl('/filemanager/files/properties'),
        ];
        $this->getView()->registerJs("filemanager.init(".json_encode($options).");", \yii\web\View::POS_END, 'my-options');*/
		
		$model = $this->findModel($id);
        
        if($model->details_config) {
            $details = \yii\helpers\Json::decode($model->details_config);
            $model->map_latitude = (isset($details['latitude'])) ? $details['latitude'] : '';
            $model->map_longitude = (isset($details['longitude'])) ? $details['longitude'] : '';
            $model->map_note = (isset($details['map_note'])) ? $details['map_note'] : '';
        }
		//Yii::$app->formatter->dateFormat = 'yyyy-mm-dd';
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			$model->user_action = $_POST['CmsPage']['user_action']; 
			//echo $_POST['CmsPage']['content'];
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true,
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
			    //echo $_POST['CmsPage']['content'];
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		}
    }
    
    public function actionChange($id)   {
		
		$model = $this->findModel($id);
        if(isset($_GET['type']) && $_GET['type'] == 1) {
            $mimeType = 'image/png';
        } else if(isset($_GET['type']) && $_GET['type'] == 2) {
            $mimeType = 'video/mp4';
        } else if(isset($_GET['type']) && $_GET['type'] == 3) {
            $mimeType = 'video/webm';
        }
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->attachs = UploadedFile::getInstances($model, 'attachs');
			$model->load(Yii::$app->request->post());
			if($model->validate() && $model->saveAdvanced($_GET['type'])) {
				return array('success' => true, 'alert' => 'Plik został zamieniony', 'refresh' => 'yes');	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('change', ['model' => $model, 'mimeType' => $mimeTyp]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('change', ['model' => $model, 'mimeType' => $mimeType]) ;	
		}
    }
	

    /**
     * Deletes an existing CmsPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        $model = $this->findModel($id); //->delete();
        
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->active) {
                return ['success' => false, 'alert' => 'Strona jest aktywna w menu i nie może zostać usunięta', 'id' => $id, 'table' => '#table-pages'];
            } else {
                $model->fk_status_id = 4;
                $model->deleted_at = date('Y-m-d H:i:s');
                $model->deleted_by = \Yii::$app->user->id;
                $model->save();
                return ['success' => true, 'alert' => 'Dane zostały przeniesione do kosza', 'id' => $id, 'table' => '#table-pages'];
            }
        } /*else {
            if($model->active) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else 
                return $this->redirect(['index']);
            }
        }*/
    }
	
	/****** galleries ******/
	public function actionGalleries($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $data = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $id, 'status' => 1])->orderby('rank')->all();
        $rows = [];
        
        foreach($data as $key => $record) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmsgallery/update', 'id' => $record->fk_gallery_id]).'" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>';
			$actionColumn .= '<a href="'.Url::to(['/cms/cmsgallery/images', 'id' => $record->fk_gallery_id]).'" class="btn btn-sm btn-default"><i class="fa fa-image"></i></a>';
			$actionColumn .= '<a href="'.Url::to(['/cms/cmspage/gdelete', 'id' => $record->id]).'" class="btn btn-sm btn-default deleteConfirm" data-table="#table-galleries"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
			
			$tmp = [];
			$tmp['title'] = $record->gallery['title'];
            $tmp['actions'] = $actionColumn ;
			
			array_push($rows, $tmp);
        }
        
        return $rows;
    }
	
	public function actionGadd($id) {
		$model = CmsPage::findOne($id);
		
		//if (Yii::$app->request->isPost) {
		$model->load(Yii::$app->request->post());
		Yii::$app->response->format = Response::FORMAT_JSON; 
        if(!$model->galleries_list) {
            return ['success' => false, 'alert' => 'Proszę wybrać dodatki', 'errors' => [0 => 'Proszę wybrać galerie']];
        }
		foreach($model->galleries_list as $key => $value) {
			$new = new \backend\Modules\Cms\models\CmsPageGallery();
			$new->fk_id = $id; 
			$new->fk_gallery_id = $value; 
			$gallery = \backend\Modules\Cms\models\CmsGallery::findOne($value);
			$new->gallery_label = $gallery->title;
			$new->rank = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $id])->count()+1;
			if(!$new->save()) {
				$res = array(
						'errors'    =>  $new->getErrors() ,
						'success' => false,
					);
				return $res;
			}
		}
		/*$model = new \backend\Modules\Cms\models\CmsPageGallery();
		$model->fk_id = $_POST['page_id']; 
		$model->fk_gallery_id = $_POST['element_id']; 
		$gallery = \backend\Modules\Cms\models\CmsGallery::findOne($_POST['element_id']);
		$model->gallery_label = $gallery->title;
		$model->rank = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $_POST['page_id']])->count()+1;*/

		return ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
		//} 
    }
	
	public function actionGdelete($id) {	
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = \backend\Modules\Cms\models\CmsPageGallery::findOne($id);
		$model->status = -1;
		$model->save();
		
		return ['success' => true, 'alert' => 'Galeria zostałą odłączona od strony', 'table' => '#table-galleries'];
	}
	
	/**** widgety *****/
	public function actionWidgets($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $data = \backend\Modules\Cms\models\CmsPageWidget::find()->where(['fk_id' => $id, 'status' => 1])->orderby('rank')->all();
        $rows = [];
        
        foreach($data as $key => $record) {
            if($record->widget['type_widget'] == 1)
                $url = Url::to(['cmswidget/ubox', 'id' => $record->widget['fk_element_id']]);
            else if($record->widget['type_widget'] == 2)
                $url = Url::to(['cmswidget/uaccordion', 'id' => $record->widget['fk_element_id']]);
            else if($record->widget['type_widget'] == 3)
                $url = Url::to(['cmswidget/ufilesset', 'id' => $record->widget['fk_element_id']]);
            else $url = false;
            
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= ($url) ? '<a href="'.$url.'" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>' : '';
			$actionColumn .= '<a href="'.Url::to(['/cms/cmspage/wdelete', 'id' => $record->id]).'" class="btn btn-sm btn-default deleteConfirm" data-table="#table-widgets"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
            $actionColumn .= '</div>';
			
			$tmp = [];
			$tmp['title'] = $record->widget['title'];
            $tmp['actions'] = $actionColumn ;
			
			array_push($rows, $tmp);
        }
        
        return $rows;
    }
    
    public function actionWadd($id) {
		$model = CmsPage::findOne($id);
		
		//if (Yii::$app->request->isPost) {
		$model->load(Yii::$app->request->post());
		Yii::$app->response->format = Response::FORMAT_JSON; 
        if(!$model->widgets_list) {
            return ['success' => false, 'alert' => 'Proszę wybrać dodatki', 'errors' => [0 => 'Proszę wybrać dodatki']];
        }
		foreach($model->widgets_list as $key => $value) {
			$new = new \backend\Modules\Cms\models\CmsPageWidget();
			$new->fk_id = $id; 
			$new->fk_widget_id = $value; 
			$widget = \backend\Modules\Cms\models\CmsWidget::findOne($value);
			$new->widget_label = $widget->title;
            $new->fk_cms_widget_id = $widget->fk_element_id;
			$new->rank = \backend\Modules\Cms\models\CmsPageWidget::find()->where(['fk_id' => $id])->count()+1;
			if(!$new->save()) {
				$res = array(
						'errors'    =>  $new->getErrors() ,
						'success' => false,
					);
				return $res;
			}
		}
		/*$model = new \backend\Modules\Cms\models\CmsPageGallery();
		$model->fk_id = $_POST['page_id']; 
		$model->fk_gallery_id = $_POST['element_id']; 
		$gallery = \backend\Modules\Cms\models\CmsGallery::findOne($_POST['element_id']);
		$model->gallery_label = $gallery->title;
		$model->rank = \backend\Modules\Cms\models\CmsPageGallery::find()->where(['fk_id' => $_POST['page_id']])->count()+1;*/

		return ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
		//} 
    }
	public function actionWcreate() {
		$model = new \backend\Modules\Cms\models\CmsPageWidget();
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			$model->fk_id = $_POST['page_id']; 
			$model->fk_widget_id = $_POST['element_id']; 
			$widget = \backend\Modules\Cms\models\CmsWidget::findOne($_POST['element_id']);
			$model->widget_label = $widget->title;
			$model->rank = \backend\Modules\Cms\models\CmsPageWidget::find()->where(['fk_id' => $_POST['page_id']])->count()+1;
	
	        if (/*$model->load(Yii::$app->request->post()) &&*/ $model->save()) {
				$res = array(
					'success' => true,'model' => $model, 'element_label' => $model->widget_label
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}

	}
	
	public function actionWdelete($id) {	
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = \backend\Modules\cms\models\CmsPageWidget::findOne($id);
		$model->status = -1;
		$model->save();
		
		return ['success' => true, 'alert' => 'Dodatek został odłączony od strony', 'table' => '#table-widgets'];
	}
	
	public function actionMcreate() {
		$model = new \backend\Modules\Cms\models\CmsPageMenu();
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			$model->fk_type_id = 1;
			$model->fk_cms_element_id = $_POST['page_id']; 
			$model->fk_menu_id = $_POST['element_id']; 
			$menu = \backend\Modules\Cms\models\CmsMenu::findOne($_POST['element_id']);
			$model->menu_label = $menu->title;
			$model->rank = \backend\Modules\Cms\models\CmsPageMenu::find()->where(['fk_cms_element_id' => $_POST['page_id']])->count()+1;
	
	        if (/*$model->load(Yii::$app->request->post()) &&*/ $model->save()) {
				$res = array(
					'success' => true,'model' => $model, 'element_label' => $menu->title
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}

	}
	
	public function actionMdelete() {
		$model = new \backend\Modules\cms\models\CmsPageMenu();
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
		
			$model = \backend\Modules\Cms\models\CmsPageMenu::find()->where(['fk_cms_element_id' => $_POST['page_id'], 'fk_menu_id' => $_POST['element_id'], 'status' => 1])->one();

	        if (/*$model->load(Yii::$app->request->post()) &&*/ $model->delete()) {
				$res = array(
					'success' => true,'model' => $model
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {
			$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			return $res;
		}

	}

    /**
     * Finds the CmsPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionTranslate($id) {
        $model = $this->findModel($id);
        
        return $this->renderAjax('tabs/_tabTranslate', ['model'=>$model->getTranslate($model->id, $_GET['lang']), 'lang' => $_GET['lang']]);
    }
}
