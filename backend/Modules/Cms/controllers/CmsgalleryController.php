<?php

namespace app\Modules\Cms\controllers;

use Yii;
use backend\Modules\Cms\models\CmsGallery;
use backend\Modules\Cms\models\CmsGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;

use backend\extensions\kapi\gallery\GalleryManagerAction;

	/**
	 * GalleryController implements the CRUD actions for CmsGallery model.
	 */
	class CmsgalleryController extends Controller
	{
		public function actions() {
			return [
			   'galleryApi' => [
				   'class' => GalleryManagerAction::className(),
				   // mappings between type names and model classes (should be the same as in behaviour)
				   'types' => [
					   'cmsgallery' => cmsgallery::className()
				   ]
			   ],
			];
		}
	
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsGallery models.
     * @return mixed
     */
    public function actionIndex() {
        
        $searchModel = new CmsGallerySearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		$queryParams["CmsGallerySearch"]["status"] = 1;
        $queryParams["CmsGallerySearch"]["type_fk"] = 0;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['CmsGallerySearch'])) {
            $params = $_GET['CmsGallerySearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(g.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "g.status = ".$params['status']);
            }
            if(isset($params['view_type']) && strlen($params['view_type']) ) {
				array_push($where, "g.view_type = ".$params['view_type']);
            }
        } 
        
        $sortColumn = 'g.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'g.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'g.status';
		
		$query = (new \yii\db\Query())
            ->select(['g.id as id', 'g.title as title', 'view_type', 'g.status as status', 'is_system'])
            ->from('{{%cms_gallery}} as g')
            ->where( ['g.status' => 1, 'g.type_fk' => 0] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = CmsGallery::getTypes();
        $color = [0 => 'purple', 1 => 'pink', 2 => 'orange', 3 => 'teal'];
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmsgallery/images', 'id' => $value['id']]).'" class="btn btn-xs btn-default"><i class="fa fa-image"></i></a>';
			$actionColumn .= '<a href="'.Url::to(['/cms/cmsgallery/videos', 'id' => $value['id']]).'" class="btn btn-xs btn-default"><i class="fa fa-video"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/cms/cmsgallery/update', 'id' => $value['id']]).'" class="btn btn-xs btn-default"><i class="fa fa-pencil-alt"></i></a>';
            $actionColumn .= ( ($value['is_system'] == 0) ? '<a href="'.Url::to(['/cms/cmsgallery/delete', 'id' => $value['id']]).'" class="btn btn-xs btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            $tmp['type'] = '<span class="label bg-'.$color[$value['view_type']].'">'.$label[$value['view_type']].'</span>';            
           // $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$value['sname'].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single CmsGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsGallery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionCreateajax()
    {
        $model = new CmsGallery();
		
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return array('success' => true, 'action' => 'createByInput', 'alert' => 'Galeria <b>'.$model->title.'</b> została utworzona', 'id' => $model->id, 'name' => $model->title, 'input' => 'cmspage-galleries_list' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model]), 'errors' => $model->getErrors() );	
			}     	
		} else {
            return  $this->renderAjax('_formAjax', [  'model' => $model ]) ;	
		}
    }
    
    public function actionCreatelang($id, $type, $lang)
    {
        $gallery = $this->findModel($id);
        
        $model = new CmsGallery();
        $model->versions_data = $type.'#'.$id.'#'.$lang;
        $model->title = $gallery->title;
        $model->title_slug = $gallery->title_slug;
        
        $model->type_fk = $gallery->type_fk;

        if ($model->save()) {
            return $this->redirect(['images', 'id' => $model->id]);
        } else {
            return $this->redirect(['images', 'id' => $id]);
        }
    }


    /**
     * Updates an existing CmsGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSlider()
    {
        $model = $this->findModel(1);

        return $this->render('images', [
			'model' => $model,
		]);
    }
    
    public function actionPartner()
    {
        $model = $this->findModel(2);

        return $this->render('images', [
			'model' => $model,
		]);
    }
    
    public function actionSpecial()
    {
        $model = $this->findModel(3);

        return $this->render('images', [
			'model' => $model,
		]);
    }
	
	
	public function actionImages($id)
    {
        $model = $this->findModel($id);

		return $this->render('images', [
			'model' => $model,
		]);
       
    }
	
	public function actionVideos($id) {
        $model = $this->findModel($id);
		$file = new \common\models\Files();
		return $this->render('videos', [
			'model' => $model,  'file' => $file
		]);
       
    }

    /**
     * Deletes an existing CmsGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true, 'alert' => 'Dane zostały przeniesione do kosza', 'id' => $id];
            
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the CmsGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSend($id) {
        $model = $this->findModel($id);
		$model->user_action = 'send';
        $model->send_email = $model->term['patient']['email'];
        $model->send_message = '<p>Galeria zdjęć do imprezy <b>'.$model->term['additional_info'].'</b> została umieszczona </p>'
                            .'<p style="text-align:center;"><a href="'.\Yii::$app->urlManagerFrontEnd->createUrl('/'.\Yii::$app->params['frontendNativeLang'].'strefa-klienta/galerie-uzytkownikow/'.$model->term['id'].'/'.$model->term['additional_info']).'" class="button">pod tym adresem</a></p>'
                            .'<p>Kod autoryzacyjny do galerii to: <b class="textPurple">'.$model->term['access_code'].'</b></p>';
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->validate()) {
                try { 
                    $mail= \Yii::$app->mailer->compose(['html' => 'sendInfo-html', 'text' => 'sendInfo-text'], ['info' => $model->send_message, 'id' => $model->id, 'access_code' => $model->term['access_code']])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo( $model->send_email )
                        ->setBcc(['kamila_bajdowska@onet.eu'])
                        ->setSubject('W serwisie '. \Yii::$app->name .' została dodana galeria do wydarzenia '.$model->term['additional_info'] );
                    /*if(file_exists("/var/www/html/ultima/frontend/web/uploads/temp/".$filename)) { 
                        $mail->attach('/var/www/html/ultima/frontend/web/uploads/temp/'.$filename);
                    }*/
                    $mail->send();
                } catch (\Swift_TransportException $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $request = Yii::$app->request;
                    /*$log = new \common\models\Logs();
                    $log->id_user_fk = Yii::$app->user->id;
                    $log->action_date = date('Y-m-d H:i:s');
                    $log->action_name = 'verify-send';
                    $log->action_describe = $e->getMessage();
                    $log->request_remote_addr = $request->getUserIP();
                    $log->request_user_agent = $request->getUserAgent(); 
                    $log->request_content_type = $request->getContentType(); 
                    $log->request_url = $request->getUrl();
                    $log->save();*/
                }
			
				return array('success' => true, 'action' => 'sendMsg', 'id' => $model->id, 'alert' => 'Wiadomość do klienta została wysłana', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1));	
                
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formSend', ['model' => $model,]), 'errors' => $model->getErrors() );	
			}		
		} else {
            return  $this->renderAjax('_formSend', ['model' => $model, ]);	
        }
	}
}
