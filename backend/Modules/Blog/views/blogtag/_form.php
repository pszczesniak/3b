<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogTag */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
    $this->registerJs(
   '$("document").ready(function(){ 
        $("#pjax-grid-view-form-dicts").on("pjax:end", function() {
            $.pjax.reload({container:"#grid-view-dicts", timeout: 1000});  //Reload GridView
			/*$("table").bootstrapTable({locale:"en-US"});*/
        });
    });'
);
?>

<div class="blog-tag-form">
    <?php yii\widgets\Pjax::begin(['id' => 'pjax-grid-view-form-dicts']) ?>
		<?php $form = ActiveForm::begin(['id' => $model->isNewRecord?'modal-grid-view-form-dict-create':'modal-grid-view-form-dict', 'options' => ['data-pjax' => false, 'enableAjaxValidation'=>true, 'data-target' => "#modal-grid-view-dict"]]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 128]) ?>

            <!--<?= $form->field($model, 'frequency')->textInput(['maxlength' => 10]) ?>-->

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Module::t('blog', 'Create') : Module::t('blog', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>
