<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\Modules\Blog\Module;
use yii\widgets\DetailView;
use common\widgets\Alert;


/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogComment */

$this->title = 'Komentarz: #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog', 'Blog - Komentarze'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-comment-view", 'title'=>Html::encode($this->title))) ?>

    <!--<p>
        <?= Html::a(Module::t('blog', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Module::t('blog', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Module::t('blog', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>-->
    
    <?= Alert::widget() ?>
    <div id="message">
        <?php $status = [0 => 'odrzucony', 1 => 'nieopublikowany', 2 => 'opublikowany'];  $colors = [-1 => 'danger', 0 => 'info', 1 => 'success']; ?>
        <ul class="message-container">
            <li class="received">
                <div class="details">
                    <div class="left"><?= $model->author ?>
                        <div class="arrow orange"></div><?= '<span class="label label-'.$colors[$model->status].'">'.$model->getStatus()->label.'</span>' ?>
                    </div>
                    <div class="right"><?= $model->created_at ?></div>
                </div>
                <div class="message">
                    
                    <?php for($i=1; $i<=$model->rank;++$i) echo '<i class="fa fa-star"></i>'; ?><br />
                    <?= $model->content ?>
                </div>
                
                <form id="comment-form" method="POST"> 
                    <div class="form-group field-blogcomment-deleted-comment pull-right">
                        <textarea rows="3" name="BlogComment[deleted_comment]" placeholder="Powód odrzucenia"></textarea>
                        <div class="help-block"></div>
                        
                    </div>
                    <div class="clear"></div>
                
                    <div class="tool-box">
                        <?php if($model->status != 1) { ?>
                            <a class="circle-icon small fa fa-check" href="<?= Url::to(['svccomment/commentaccept', 'id' => $model->id]) ?>" alt="Zaakceptuj" title="Zaakceptuj"></a>
                        <?php } ?>
                        <button class="circle-icon small red-hover glyphicon glyphicon-remove" type="submit"  alt="Odrzuć" title="Odrzuć"></a>
                        <!--<a class="circle-icon small red-hover glyphicon glyphicon-flag" href="#"></a>-->
                    </div>
                </form>
            </li>
        </ul>

    </div>

    <?php /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'post_id',
                'value' => $model->post->title,
            ],
            'content:ntext',
            'author',
            'email:email',
            'url:url',
            [
                'attribute' => 'status',
                'value' => $model->getStatus()->label,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])*/ ?>

<?= $this->endContent(); ?>
