<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogComment */

$this->title = 'Komentarz #' . $model->id;
$this->params['breadcrumbs'][] = 'Blog';
$this->params['breadcrumbs'][] = ['label' => 'Meneżdżer komentarzy', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edycja';
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-index", 'title' => Html::encode($this->title))) ?>
<div class="blog-comment-update">
    <div class="text-right">
        <?= Html::a('<i class="fa fa-table"></i>'.Yii::t('app', 'Rejestr komentarzy', [
            'modelClass' => 'Cms Comments',
        ]), ['index'], ['class' => 'btn btn-warning btn-icon btn-sm btn-responsive']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger btn-icon btn-sm deleteConfirm', ]) ?>
    </div>
    <hr />
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?= $this->endContent(); ?>