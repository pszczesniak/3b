<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;
use yii\widgets\ActiveForm;
use backend\Modules\Blog\models\BlogPost;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_post_fk')->dropDownList(ArrayHelper::map(BlogPost::find()->all(), 'id', 'title')) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'status')->dropDownList(\backend\Modules\Blog\models\Status::labels()) ?>

    <div class="form-group align-right">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
