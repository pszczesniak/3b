<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-catalog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-blog-comments-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-comments']
    ]); ?>
    
    <div class="row"> 
        <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'content')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-comments', 'data-form' => '#filter-blog-comments-search' ])->label('Komentarz <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'id_post_fk', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( ArrayHelper::map(\backend\Modules\Blog\models\BlogPost::getList(), 'id', 'title'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-comments', 'data-form' => '#filter-blog-comments-search'  ] ) ?></div>
        <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'status', ['template' => '{label}<div class="form-select">{input}</div>{error}'])->dropDownList( \backend\Modules\Blog\models\Status::labels(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list', 'data-table' => '#table-comments', 'data-form' => '#filter-blog-comments-search' ] ) ?></div>        
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
