<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\Modules\Blog\Module;
use backend\Modules\Blog\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blog\models\BlogPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('blog', 'Wpisy bloga');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-index", 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-posts" class="btn-group toolbar-table-widget">
        <?= Html::a(Yii::t('app', 'New') , ['create'], ['class' => 'btn btn-success']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-posts"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-posts"

            data-toolbar="#toolbar-posts" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-blog-posts-search"
            data-url=<?= Url::to(['/blog/blogpost/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="title"  data-sortable="true">Tytuł</th>
                <th data-field="catalog"  data-sortable="true">Kategoria</th>
                <th data-field="status"  data-sortable="true">Status</th>
                <th data-field="created"  data-sortable="true">Utworzono</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>

<?= $this->endContent(); ?>
