<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-blog-posts-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-posts']
    ]); ?>
    
    <div class="grid">
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'id_catalog_fk')->dropDownList(  ArrayHelper::map(\backend\Modules\Blog\models\BlogCatalog::find()->where(['status' => 1])->all(), 'id', 'title'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-posts', 'data-form' => '#filter-blog-posts-search' ] ) ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"><?= $form->field($model, 'status')->dropDownList(  ['1' => 'opublikowany', '0' => 'nieopublikowany', '-1' => 'w koszu'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-posts', 'data-form' => '#filter-blog-posts-search' ] ) ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'title')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-posts', 'data-form' => '#filter-blog-posts-search' ])->label('Tytuł <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
