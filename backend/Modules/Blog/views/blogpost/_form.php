<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\Modules\Blog\models\BlogCatalog;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-post-form-">

    <?php $form = ActiveForm::begin([
        'options'=>[/*'class' => 'form-horizontal', 'enctype'=>'multipart/form-data'*/],
        /*'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],*/
    ]); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>
        <?= $form->field($model, 'surname')->textInput(['maxlength' => 128]) ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'id_catalog_fk')->dropDownList(ArrayHelper::map(BlogCatalog::get(0, BlogCatalog::find()->all()), 'id', 'title')) ?>
                <?php /* $form->field($model, 'status')->dropDownList(\backend\Modules\Blog\models\Status::labels())*/ ?>
                <?= $form->field($model, 'tags')->dropDownList( ArrayHelper::map(\backend\Modules\Blog\models\BlogTag::find()->orderby('name')->all(), 'name', 'name'), array('class' => 'ms-select', 'multiple' => 'multiple') )  ?>
            </div>
            <div class="col-sm-6">
                <fieldset><label>Skrót</label>
                    <?= $form->field($model, 'brief')->textarea(['rows' => 4])->label(false) ?>
                </fieldset>
            </div>
        </div>
        
        <?php /* $form->field($model, 'banner')->fileInput()*/ ?>
        <?php /*$form->field($model, 'click')->textInput()*/ ?> 
        <?=  $form->field($model, 'content')->textarea(['rows' => 6, 'class'=> 'mce-full'])->label(false) ?>	
       
        <div class="form-group align-right">
            <label class="col-lg-2 control-label" for="">&nbsp;</label>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript" src="/panel/js/tinymce.min.js?v=<?php echo date ("ymdHis", filemtime("js/tinymce.min.js")); ?>"></script>