<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;


/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogPost */

$this->title = Module::t('blog', 'Nowy wpis na bloga') ;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog', 'Wpisy na bloga'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-create", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
