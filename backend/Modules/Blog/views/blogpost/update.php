<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\Modules\Blog\Module;
use common\widgets\Alert;


use backend\Modules\Blog\models\Status;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogPost */

$this->title = Module::t('blog', 'Aktualizacja wpisu') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog', 'Wpisy na bloga'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Module::t('blog', 'Aktualizacja');
?>
 <?= Alert::widget() ?>
<div class="row">
    <div class="col-sm-8 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-update", 'title'=>Html::encode($this->title))) ?>
            <div class="fb-publish-result"></div>
            <div class="text-right">
                <?= Html::a('<i class="fa fa-plus"></i>'.Yii::t('app', 'Nowy wpis', [
                    'modelClass' => 'Cms Gallery',
                ]), ['create'], ['class' => 'btn btn-success btn-icon btn-sm btn-responsive']) ?>
                <?= ($model->id_gallery_fk) ? Html::a('<i class="fa fa-image"></i>'.Yii::t('app', 'Galeria', ['modelClass' => 'Cms Gallery',]), Url::to(['/cms/cmsgallery/images', 'id' => $model->id_gallery_fk]), ['class' => 'btn btn-primary btn-icon btn-sm btn-responsive']) : '' ?>
                <?= Html::a('<i class="fa fa-play-circle"></i>'.Yii::t('app', 'Multimedia', ['modelClass' => 'Blog Post', ]), ['media', 'id' => $model->id], ['class' => 'btn btn-pink btn-icon btn-sm btn-responsive']) ?>
                
                <?php 
                    $accessToken = \Yii::$app->session->get('fb.token');
            
                    if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
                        echo '<button class="btn btn-sm btn-info fb-publish" data-action="'.Url::to(['/blog/blogpost/publish', 'id' => $model->id]).'">Opublikuj na FB</button>';
                    }
                ?>
                <!--<?= Html::a('<i class="fa fa-trash-o"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-icon btn-sm btn-responsive',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>-->
                
            </div>
            <hr />
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        <?= $this->endContent(); ?>
    </div>
    <div class="col-sm-4 col-xs-12">
        <?php
            if ($model->status === Status::STATUS_ACTIVE) {
                $class = 'label-success';
            } elseif ($model->status === Status::STATUS_INACTIVE) {
                $class = 'label-info';
            } else {
                $class = 'label-danger';
            }
            $span = ' <span class="label ' . $class . '">' . $model->getStatus()->label . '</span>';
        ?>
        
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class' => "blog-post-status", 'title' => 'Status '.$span )) ?>
            <form id="post-form" method="POST" action="<?= Url::to(['blogpost/postthrow', 'id' => $model->id]) ?>"> 
                <div class="form-group field-blogpost-deleted-comment">
                    <?php if($model->status == -1) { ?>
                    <div class="deleted-comment"><b>Powód odrzucenia: </b><p><?= (!$model->deleted_comment) ? "nie podano powodu" : $model->deleted_comment ?></p></div>
                    <?php } else { ?>
                    <textarea rows="3" class="form-control" name="BlogPost[deleted_comment]" placeholder="Powód odrzucenia" ></textarea>
                    <?php } ?>
                    <div class="help-block"></div>
                    
                </div>
                <div class="clear"></div>
            
                <div class="form-group align-right">
                    <?php if($model->status != 1) { ?>
                        <a class="btn btn-success btn-icon" href="<?= Url::to(['blogpost/postaccept', 'id' => $model->id]) ?>" alt="Zaakceptuj" title="Zaakceptuj"><i class="fa fa-check"></i>Opublikuj</a>
                    <?php } ?>
                    <?php if($model->status != -1) { ?>
                        <button class="btn btn-danger btn-icon" type="submit"  alt="Odrzuć" title="Odrzuć"><i class="glyphicon glyphicon-remove"></i>Odrzuć</a>
                    <?php } ?>
                    <!--<a class="circle-icon small red-hover glyphicon glyphicon-flag" href="#"></a>-->
                </div>
            </form>
  
        <?= $this->endContent(); ?>
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-post-image", 'title'=>'Zdjęcie wiodące wpisu')) ?>
            <div class="text-center">
                <?php echo \backend\extensions\kapi\imgattachment\ImageAttachmentWidget::widget(
						[
                                'id' => 'imgAttachment',
                                'model' => $model,
                                'behaviorName' => 'coverBehavior',
                                'apiRoute' => '/blog/blogpost/imgAttachApi',
                            ]
                        
                        )
                ?>

            </div>
        <?= $this->endContent(); ?>
    </div>
</div>

    


