<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \backend\Modules\Blog\models\BlogCatalog;
use app\Modules\Blog\Module;

/* @var $this yii\web\View */
/* @var $model funson86\blog\models\BlogCatalog */
/* @var $form yii\widgets\ActiveForm */

//fix the issue that it can assign itself as parent
$parentCatalog = ArrayHelper::merge([0 => Module::t('blog', 'Root Catalog')], ArrayHelper::map(BlogCatalog::get(0, BlogCatalog::find()->all()), 'id', 'str_label'));
unset($parentCatalog[$model->id]);

?>

<div class="blog-catalog-form">

    <?php $form = ActiveForm::begin([
        'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
        /*'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],*/
    ]); ?>
    <!--<?= $form->field($model, 'id_parent_fk')->dropDownList($parentCatalog) ?>-->

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => 128]) ?>
    
    <div class="row">
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'is_nav')->dropDownList(BlogCatalog::getArrayIsNav()) ?></div>
        <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'status')->dropDownList(\backend\Modules\Blog\models\Status::labels()) ?></div>
    </div>

    <!--<?= $form->field($model, 'banner')->fileInput() ?>-->

    <!--<?= $form->field($model, 'sort_order')->textInput() ?>

    <?= $form->field($model, 'page_size')->textInput() ?>

    <?= $form->field($model, 'template')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'redirect_url')->textInput(['maxlength' => 255]) ?>-->

    

    <div class="form-group align-right">
        <label class="col-lg-2 control-label" for="">&nbsp;</label>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-icon btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
