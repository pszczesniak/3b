<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Newsletter\NewsletterTemplate */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meneżdżer katalogów';
$this->params['breadcrumbs'][] = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-catalog-index", 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-catalogs" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Shop Product'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-catalogs"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-catalogs"

                data-toolbar="#toolbar-catalogs" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="600"
                data-show-footer="false"
                data-side-pagination="server"
                data-row-style="rowStyle"
                data-sort-name="id"
                data-sort-order="desc"
                
                data-method="get"
                data-search-form="#filter-blog-catalogs-search"
                data-url=<?= Url::to(['/blog/blogcatalog/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="title"  data-sortable="true" >Tytuł</th>
                    <th data-field="status" data-align="center" data-sortable="true" >Status</th>
                    <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>
<?php $this->endContent(); ?>
