<?php

use yii\helpers\Html;
use app\Modules\Blog\Module;

/* @var $this yii\web\View */
/* @var $model app\Modules\Blog\models\BlogCatalog */

$this->title = Yii::t('blog', 'Edycja: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Blog Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('blog', 'Edycja');
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"blog-catalog-", 'title'=>Html::encode($this->title))) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
