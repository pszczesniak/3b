<?php

namespace backend\Modules\Blog\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\Modules\Blog\Module;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\Modules\Blog\models\BlogComment;
use backend\Modules\Blog\models\BlogCatalog;
use backend\Modules\Blog\models\BlogTag;

/**
 * This is the model class for table "{{%blog_post}}".
 *
 * @property integer $id
 * @property integer $id_catalog_fk
 * @property string $title
 * @property string $surname
 * @property string $brief
 * @property string $content
 * @property string $tags
 * @property string $banner
 * @property integer $click
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class BlogPost extends \yii\db\ActiveRecord
{
    private $_oldTags;

    private $_status;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [ 
            [['id_catalog_fk', 'title', 'surname', 'content'], 'required'],
            [['id_catalog_fk', 'click', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_gallery_fk'], 'integer'],
            [['brief', 'content', 'author_name', 'author_email', 'deleted_comment'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'tags'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['banner','brief'], 'string', 'max' => 255],
            [['surname'], 'string', 'max' => 150],
            [['id_catalog_fk'], 'exist', 'skipOnError' => true, 'targetClass' => BlogCatalog::className(), 'targetAttribute' => ['id_catalog_fk' => 'id']],
            [['deleted_comment'], 'required', 'on' => 'delete'],
            [['author_name', 'author_email'], 'required', 'when' => function($model) {
				return ($model->created_by == 0);
			}],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_catalog_fk' => Yii::t('blog', 'Kategoria'),
            'title' => Yii::t('blog', 'Tytuł'),
            'surname' => Yii::t('blog', 'Podtytuł'),
            'brief' => Yii::t('blog', 'Skrót'),
            'content' => Yii::t('blog', 'Treść'),
            'tags' => Yii::t('blog', 'Tagi'),
            'banner' => Yii::t('blog', 'Banner'),
            'click' => Yii::t('blog', 'Click'),
            'status' => Yii::t('blog', 'Status'),
            'commentsCount' => Yii::t('blog', 'Comments'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'author_name' => Yii::t('app', 'Podpis'),
            'author_email' => Yii::t('app', 'Email'),
        ];
    }
    
    public function beforeSave($insert) {
		
        if(!$this->brief) $this->brief = implode(' ', array_slice(explode(' ', strip_tags($this->content)), 0, 30)).' [...]';
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
            
            'coverBehavior' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentBehavior::className(),
				// type name for model
				'type' => 'BlogPost',
				// image dimmentions for preview in widget 
				'previewHeight' => 200,
				'previewWidth' => 300,
				// extension for images saving
				'extension' => 'jpg',
				// path to location where to save images
				'directory' => Yii::getAlias('@webroot') . '/../..' . Yii::$app->params['frontenddir'] . '/web/uploads/blog',
				'url' => Yii::getAlias('@web') . '/..'. Yii::$app->params['frontendurl'] . '/uploads/blog',
				// additional image versions
				'versions' => [
					'small' => function ($img) {
						/** @var ImageInterface $img */
						return $img
							->copy()
							->resize($img->getSize()->widen(200));
					},
					'medium' => function ($img) {
						/** @var ImageInterface $img */
						$dstSize = $img->getSize();
						$maxWidth = 750;
						if ($dstSize->getWidth() > $maxWidth) {
							$dstSize = $dstSize->widen($maxWidth);
						}
                        /*$maxHeight = 300;
                        if ($dstSize->getHeight() > $maxHeight) {
							$dstSize = $dstSize->heighten(300);
						}*/
						return $img
							->copy()
							->resize($dstSize->widen(750));
                        /*return $img
							->copy()
                            ->thumbnail(new \Imagine\Image\Box(750, 300));*/
					},
				]
			]
			
		];
	}
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogComments()
    {
        return $this->hasMany(BlogComment::className(), ['id_post_fk' => 'id']);
    }

    public function getCommentsCount()
    {
        //return $this->hasMany(BlogComment::className(), ['id_post_fk' => 'id', 'status' => Status::STATUS_ACTIVE])->count('id_post_fk');
        return BlogComment::find()->where(['id_post_fk' => $this->id, 'status' => Status::STATUS_ACTIVE])->count('id_post_fk');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(BlogCatalog::className(), ['id' => 'id_catalog_fk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getComments()
    {
        return $this->hasMany(BlogComment::className(), ['id_post_fk' => 'id'])->orderby('created_at desc');
    }

    public function getStatus()
    {
        if ($this->_status === null) {
            $this->_status = new Status($this->status);
        }
        return $this->_status;
    }

    /**
     * Before save.
     * created_at updated_at
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
        //BlogTag::updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * After save.
     *
     */
    public function afterDelete()
    {
        parent::afterDelete();
        // add your code here
        //BlogTag::updateFrequencyOnDelete($this->_oldTags);
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->_oldTags = $this->tags;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public static function getArrayCatalog()
    {
        return ArrayHelper::map(BlogCatalog::find()->all(), 'id', 'title');
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute,$params)
    {
        $this->tags = BlogTag::array2string(array_unique(array_map('trim', BlogTag::string2array($this->tags))));
    }

    /**
     *
     */
    public function getUrl()
    {
        return Url::to(['/blog/view', 'id' => $this->id, 'surname' => $this->surname]);
    }

    /**
     *
     */
    public function getTagLinks()
    {
        $links = [];
        foreach(BlogTag::string2array($this->tags) as $tag)
            $links[] = Html::a($tag, Yii::$app->getUrlManager()->createUrl(['blog/index', 'tag'=>$tag]));

        return $links;
    }

    /**
     * comment need approval
     */
    public function addComment($comment)
    {
        $comment->status = Status::STATUS_INACTIVE;
        $comment->id_post_fk = $this->id;
        return $comment->save();
    }
    
    public static function getList() {
        return BlogPost::find()->where(['status' => 1])->orderby('title')->all();
    }
}
