<?php

namespace backend\Modules\Blog\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\Modules\Blog\Module;

/**
 * This is the model class for table "{{%blog_catalog}}".
 *
 * @property integer $id
 * @property integer $id_parent_fk
 * @property string $title
 * @property string $surname
 * @property string $brief
 * @property string $content
 * @property string $banner
 * @property integer $is_nav
 * @property integer $sort_order
 * @property integer $page_size
 * @property string $template
 * @property string $redirect_url
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class BlogCatalog extends \yii\db\ActiveRecord
{
    const IS_NAV_YES = 1;
    const IS_NAV_NO = 0;
    const PAGE_TYPE_LIST = 'list';
    const PAGE_TYPE_PAGE = 'page';
    private $_isNavLabel;
    private $_status;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_catalog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_fk', 'is_nav', 'sort_order', 'page_size', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['title'], 'required'],
            [['surname', 'brief', 'content'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title', 'banner', 'template', 'redirect_url'], 'string', 'max' => 255],
            [['surname'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent_fk' => Yii::t('blog', 'Id Parent Fk'),
            'title' => Yii::t('blog', 'Tytuł'),
            'surname' => Yii::t('blog', 'Podtytuł'),
            'brief' => Yii::t('blog', 'skrót'),
            'content' => Yii::t('blog', 'Opis'),
            'banner' => Yii::t('blog', 'Zdjęcie przewodnie'),
            'is_nav' => Yii::t('blog', 'Dodać do nawigacji?'),
            'sort_order' => Yii::t('blog', 'Sort Order'),
            'page_size' => Yii::t('blog', 'Page Size'),
            'template' => Yii::t('blog', 'Template'),
            'redirect_url' => Yii::t('blog', 'Redirect Url'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPosts()
    {
        return $this->hasMany(BlogPost::className(), ['catalog_id' => 'id']);
    }

    public function getPostsCount()
    {
        return $this->count(BlogPost::className(), ['catalog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(BlogCatalog::className(), ['id' => 'id_parent_fk']);
    }

    public function getStatus()
    {
        if ($this->_status === null) {
            $this->_status = new Status($this->status);
        }
        return $this->_status;
    }

    /**
     * Before save.
     * created_at update_time
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/


    /**
     * @inheritdoc
     */
    public static function getArrayIsNav()
    {
        return [
            self::IS_NAV_YES => Module::t('blog', 'tak'),
            self::IS_NAV_NO => Module::t('blog', 'nie'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getOneIsNavLabel($isNav = null)
    {
        if($isNav)
        {
            $arrayIsNav = self::getArrayIsNav();
            return $arrayIsNav[$isNav];
        }
        else
            return;
    }

    public function getIsNavLabel()
    {
        if ($this->_isNavLabel === null)
        {
            $arrayIsNav = self::getArrayIsNav();
            $this->_isNavLabel = $arrayIsNav[$this->is_nav];
        }
        return $this->_isNavLabel;
    }

    /**
     * @param int $parentId  parent catalog id
     * @param array $array  catalog array list
     * @param int $level  catalog level, will affect $repeat
     * @param int $add  times of $repeat
     * @param string $repeat  symbols or spaces to be added for sub catalog
     * @return array  catalog collections
     */

    static public function get($parentId = 0, $array = array(), $level = 0, $add = 2, $repeat = ' ')
    {
        $strRepeat = '';
        // add some spaces or symbols for non top level categories
        if ($level>1) {
            for($j = 0; $j < $level; $j ++)
            {
                $strRepeat .= $repeat;
            }
        }

        // i feel this is useless
        if($level>0)
            $strRepeat .= '';

        $newArray = array ();
        $tempArray = array ();

        //performance is not very good here
        foreach ( ( array ) $array as $v )
        {
            if ($v['id_parent_fk'] == $parentId)
            {
                $newArray [] = array ('id' => $v['id'], 'title' => $v['title'], 'id_parent_fk' => $v['id_parent_fk'],  'sort_order' => $v['sort_order'],
                    'banner' => $v['banner'], //'postsCount'=>$v['postsCount'],
                    'is_nav' => $v['is_nav'], 'template' => $v['template'],
                    'status' => $v['status'], 'created_at' => $v['created_at'], 'updated_at' => $v['updated_at'], 'redirect_url' => $v['redirect_url'], 'str_repeat' => $strRepeat, 'str_label' => $strRepeat.$v['title'],);

                $tempArray = self::get ( $v['id'], $array, ($level + $add), $add, $repeat);
                if ($tempArray)
                {
                    $newArray = array_merge ( $newArray, $tempArray );
                }
            }
        }
        return $newArray;
    }

    /**
     * return all sub catalogs of a parent catalog
     * @param int $parentId
     * @param array $array
     * @return array
     */

    static public function getCatalog($parentId=0,$array = array())
    {
        $newArray=array();
        foreach ((array)$array as $v)
        {
            if ($v['id_parent_fk']==$parentId)
            {
                $newArray[$v['id']]=array(
                    'text'=>$v['title'].' ??['.($v['is_nav'] ? Module::t('common', 'CONSTANT_YES') : Module::t('common', 'CONSTANT_NO')).'] ??['.$v['sort_order'].
                        '] ??['.($v['page_type'] == 'list' ? Module::t('common', 'PAGE_TYPE_LIST') : Module::t('common', 'PAGE_TYPE_PAGE')).'] ??['.
                        F::getStatus2($v['status']).'] [<a href="'.Yii::app()->createUrl('/catalog/update',array('id'=>$v['id'])).'">??</a>][<a href="'
                        .Yii::app()->createUrl('/catalog/create',array('id'=>$v['id'])).'">?????</a>]&nbsp;&nbsp[<a href="'.
                        Yii::app()->createUrl('/catalog/delete',array('id'=>$v['id'])).'">??</a>]',
                    //'children'=>array(),
                );

                $tempArray = self::getCatalog($v['id'],$array);
                if($tempArray)
                {
                    $newArray[$v['id']]['children']=$tempArray;
                }
            }
        }
        return $newArray;
    }

    static public function getCatalogIdStr($parentId=0,$array = array())
    {
        $str = $parentId;
        foreach ((array)$array as $v)
        {
            if ($v['id_parent_fk']==$parentId)
            {

                $tempStr = self::getCatalogIdStr($v['id'],$array);
                if($tempStr)
                {
                    $str .= ','.$tempStr;
                }
            }
        }
        return $str;
    }

    static public function getRootCatalogId($id = 0, $array = [])
    {
        if(0 == $id)
        {
            return 0;
        }

        foreach ((array)$array as $v)
        {
            if ($v['id'] == $id) {
                $parentId = $v['id_parent_fk'];
                if(0 == $parentId)
                    return $id;
                else
                    return self::getRootCatalogId($parentId, $array);
            }
        }
    }

    static public function getCatalogSub2($id=0,$array = array())
    {
        if(0 == $id)
        {
            return 0;
        }

        $arrayResult = array();
        $rootId = self::getRootCatalogId($id, $array);
        foreach ((array)$array as $v)
        {
            if ($v['id_parent_fk']==$rootId)
            {
                array_push($arrayResult, $v);
            }
        }

        return $arrayResult;
    }

    static public function getBreadcrumbs($id=0,$array = array())
    {
        if(0 == $id)
        {
            return;
        }

        $arrayResult = self::getPathToRoot($id, $array);

        return array_reverse($arrayResult);
    }

    static public function getPathToRoot($id=0,$array = array())
    {
        if (0 == $id) {
            return array();
        }

        $arrayResult = array();
        $id_parent_fk = 0;
        foreach ((array)$array as $v) {
            if ($v['id'] == $id) {
                $id_parent_fk = $v['id_parent_fk'];
                if (self::PAGE_TYPE_LIST == $v['page_type'])
                    $arrayResult = array($v['title'] => array('list', id => $v['id']));
                elseif (self::PAGE_TYPE_PAGE == $v['page_type'])
                    $arrayResult = array($v['title'] => array('page', id => $v['id']));
            }
        }

        if (0 < $id_parent_fk) {
            $arrayTemp = self::getPathToRoot($id_parent_fk, $array);

            if (!empty($arrayTemp))
                $arrayResult += $arrayTemp;
        }

        if (!empty($arrayResult))
            return $arrayResult;
        else
            return;
    }
}
