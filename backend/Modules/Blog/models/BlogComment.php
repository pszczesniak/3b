<?php

namespace backend\Modules\Blog\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\Modules\Blog\Module;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%blog_comment}}".
 *
 * @property integer $id
 * @property integer $id_post_fk
 * @property string $content
 * @property string $email
 * @property string $url
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class BlogComment extends \yii\db\ActiveRecord
{
    private $_status;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_post_fk', 'content', 'email'], 'required'],
            [['id_post_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'id_parent_fk', 'discussion_label'], 'integer'],
            [['content', 'deleted_comment'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['email', 'url', 'author'], 'string', 'max' => 128],
            [['id_post_fk'], 'exist', 'skipOnError' => true, 'targetClass' => BlogPost::className(), 'targetAttribute' => ['id_post_fk' => 'id']],
            [['deleted_comment'], 'required', 'on' => 'delete'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_post_fk' => Yii::t('app', 'Wpis'),
            'author' => 'Autor',
            'content' => 'Komentarz',
            'email' =>  'Email',
            'url' => 'Url',
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (!Yii::$app->user->isGuest)?\Yii::$app->user->id:0;
			} 
			return true;
		} else { 
						
			return false;
		}
		return false;
	}
	
	public function delete() {
		/*$result = $this->update(['status' => 2, 'deleted_at' => new Expression('NOW()')]);*/
		$this->status = -1;
		$this->deleted_at = new Expression('NOW()');
		$this->deleted_by = \Yii::$app->user->id;
		$result = $this->save();
		//var_dump($this); exit;
		return $result;
	}
	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(BlogPost::className(), ['id' => 'id_post_fk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogPost()
    {
        return $this->hasOne(BlogPost::className(), ['id' => 'id_post_fk']);
    }

    public function getStatus()
    {
        if ($this->_status === null) {
            $this->_status = new Status($this->status);
        }
        return $this->_status;
    }

    /**
     * Before save.
     * created_at updated_at
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

    public function getAuthorLink()
    {
        if(!empty($this->url))
            return Html::a(Html::encode($this->author), $this->url);
        else
            return Html::encode($this->author);
    }

    public function getUrl($post=null)
    {
        if($post === null)
            $post = $this->post;
        return $post->url . '#c' . $this->id;
    }

    public static  function findRecentComments($limit=10)
    {
      /*  return self::find()->joinWith('blogPost')->where([
            'blog_comment.status' => Status::STATUS_ACTIVE,
            'blog_post.status' => Status::STATUS_ACTIVE,
        ])->orderBy([
            'created_at' => SORT_DESC
        ])->limit($limit)->all();*/
        /*return $this->with('post')->findAll(array(
            'condition'=>'t.status='.CONSTANT::STATUS_ACTIVE .' AND post.status='.CONSTANT::STATUS_ACTIVE,
            'order'=>'t.created_at DESC',
            'limit'=>$limit,
        ));*/
        return self::find()->orderBy([ 'created_at' => SORT_DESC ])->limit($limit)->all();
    }
    
    public function getChildren()  {
		$items = $this->hasMany(self::className(), ['id_parent_fk' => 'id'])->where(['status' => '1']); 
		if(count($items) > 0)
			return $items;
		else
			return false;
    }
}
