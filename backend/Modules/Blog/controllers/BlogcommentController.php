<?php

namespace app\Modules\Blog\controllers;

use Yii;
use backend\Modules\Blog\models\BlogComment;
use app\Modules\Blog\models\BlogCommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class BlogcommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(403, 'No Auth');

        $searchModel = new BlogCommentSearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['BlogCommentSearch'])) {
            $params = $_GET['BlogCommentSearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(p.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['content']) && !empty($params['content']) ) {
				array_push($where, "lower(c.title) like '%".strtolower( addslashes($params['content']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "c.status = ".$params['status']);
            }
            if(isset($params['id_post_fk']) && strlen($params['id_post_fk']) ) {
				array_push($where, "c.id_post_fk = ".$params['id_post_fk']);
            }
        } else {
            array_push($where, "c.status = 1");
        }
        
        $sortColumn = 'c.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'p.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'c.status';
        if( isset($post['sort']) && $post['sort'] == 'content' ) $sortColumn = 'c.content';
		
		$query = (new \yii\db\Query())
            ->select(['c.id as id', 'p.title as ptitle', 'c.status as status', 'c.content as content', 'id_post_fk'])
            ->from('{{%blog_post}} as p')
            ->join(' JOIN', '{{%blog_comment}} as c', 'c.id_post_fk = p.id');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );

		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-1' => 'danger', '0' => 'info', '1' => 'success'];
        $status = ['-1' => 'odrzucony', '0' => 'nieopulikowany', '1' => 'opublikowany'];

		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/pl/blog/blogcomment/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="/panel/pl/blog/blogcomment/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = '<a href="'.Url::to(['/blog/blogpost/update', 'id' => $value['id_post_fk']]).'" title="Przejdż do wpisu">'.mb_substr($value['ptitle'], 0, 50, 'utf-8').'</a>';
            $words = array_slice(explode(' ', strip_tags($value['content'])), 0, 30);
            $tmp['comment'] = implode(' ', $words).( (count($words) > 30) ? ' [...]' : '');
            //$tmp['comment'] = mb_substr($value['content'], 0, 80, 'utf-8');
            $tmp['status'] = '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single Comment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(401, 'No Auth');
        
        $model->scenario = 'delete';
        
        $statusTmp = $model->status;
        
        if ($model->load(Yii::$app->request->post())) {
            $model->status = -1;
            if($model->save()) {
                Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Komentarz został odrzucony')  );
            } else {
                $model->status = $statusTmp;
                Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Proszę wpisac powód odrzucenia!')  );
            }
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionCommentaccept($id) {
        
       /* if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        } */
        
        $comment = BlogComment::findOne($id);
        $comment->status = 1;
        $comment->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Komentarz został opublikowany')  );
        return $this->redirect(['view', 'id' => $comment->id]);
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //if(!Yii::$app->user->can('createPost')) throw new HttpException(401, 'No Auth');

        $model = new BlogComment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
        $model = new BlogComment();
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Komentarz <b>'.$model->name.'</b> został dodany', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //if(!Yii::$app->user->can('updatePost')) throw new HttpException(401, 'No Auth');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //if(!Yii::$app->user->can('deletePost')) throw new HttpException(401, 'No Auth');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-comments');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-comments');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
