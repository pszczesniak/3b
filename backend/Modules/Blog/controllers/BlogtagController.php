<?php

namespace app\Modules\Blog\controllers;

use Yii;
use backend\Modules\Blog\models\BlogTag;
use app\Modules\Blog\models\BlogTagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class BlogtagController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(403, 'No Auth');
        $model = new BlogTag;
        
        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model = new BlogTag(); //reset model
        }*/
        
        $searchModel = new BlogTagSearch();
        $searchModel->status = 1;
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['BlogTagSearch'])) {
            $params = $_GET['BlogTagSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(t.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "t.status = ".$params['status']);
            }
        } else {
            array_push($where, "t.status = 1");
        }
        
        $sortColumn = 't.id';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 't.name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 't.status';
		
		$query = (new \yii\db\Query())
            ->select(['t.id as id', 't.name as name', 't.status as status'])
            ->from('{{%blog_tag}} as t');
           // ->join('LEFT JOIN', '{{%cms_category}} as c', 'c.id = p.fk_category_id')
           // ->where( ['g.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $label = [1 => 'success', 0 => 'info', -1 => 'danger'];
        $statusLits = \backend\Modules\Blog\models\Status::labels();
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/pl/blog/blogtag/updateajax/'.$value['id'].'" class="btn btn-sm btn-default gridViewModal" data-table="#table-tags" data-target="#modal-grid-item"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status']==1) ? '<a href="/panel/pl/blog/blogtag/deleteajax/'.$value['id'].'" class="btn btn-sm btn-default remove" data-table="#table-tags"><i class="fa fa-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['name'] = $value['name'];
            $tmp['status'] = '<span class="label label-'.$label[$value['status']].'">'.$statusLits[$value['status']].'</span>';
            $tmp['actions'] = $actionColumn;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single Tag model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(401, 'No Auth');
        
        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //if(!Yii::$app->user->can('createPost')) throw new HttpException(401, 'No Auth');

        $model = new BlogTag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateajax() {
		
        $model = new BlogTag();
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Tag <b>'.$model->name.'</b> został dodany', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}


    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //if(!Yii::$app->user->can('updatePost')) throw new HttpException(401, 'No Auth');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('_form', [
                'model' => $model
            ]); 
        } else {
            return $this->renderPartial('_form', [
                'model' => $model
            ]);
        }
    }
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'alert' => 'Tag <b>'.$model->name.'</b> został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [ 'model' => $model, ]) ;	
		}
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //if(!Yii::$app->user->can('deletePost')) throw new HttpException(401, 'No Auth');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-tags');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-tags');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogTag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
