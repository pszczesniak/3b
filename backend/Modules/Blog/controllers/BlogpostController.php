<?php

namespace app\Modules\Blog\controllers;

use backend\Modules\Blog\models\Status;
use Yii;
use backend\Modules\Blog\models\BlogPost;
use app\Modules\Blog\models\BlogPostSearch;
use backend\Modules\Cms\models\CmsGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\Response;
use common\components\CustomHelpers;

/**
 * BlogPostController implements the CRUD actions for BlogPost model.
 */
class BlogpostController extends Controller
{
    public function actions()
	{
		return [
			'imgAttachApi' => [
				'class' => \backend\extensions\kapi\imgattachment\ImageAttachmentAction::className(),
				// mappings between type names and model classes (should be the same as in behaviour)
				'types' => [
					'BlogPost' => BlogPost::className()
				]
			],
		];
	}
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all BlogPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(403, 'No Auth');

        $searchModel = new BlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'arrayCatalog' => BlogPost::getArrayCatalog(),
        ]);
       /* $searchModel = new BlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $arrayCatalog = BlogPost::getArrayCatalog();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'arrayCatalog' => $arrayCatalog,
        ]);*/
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['BlogPostSearch'])) {
            $params = $_GET['BlogPostSearch'];
            if(isset($params['title']) && !empty($params['title']) ) {
				array_push($where, "lower(p.title) like '%".strtolower( addslashes($params['title']) )."%'");
            }
            if(isset($params['status']) && strlen($params['status']) ) {
				array_push($where, "p.status = ".$params['status']);
            }
            if(isset($params['id_catalog_fk']) && !empty($params['id_catalog_fk']) ) {
				array_push($where, "id_catalog_fk = ".$params['id_catalog_fk']);
            }
        } 
        
        $sortColumn = 'p.id';
        if( isset($post['sort']) && $post['sort'] == 'title' ) $sortColumn = 'p.title';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'p.status';
        if( isset($post['sort']) && $post['sort'] == 'id_catalog_fk' ) $sortColumn = 'c.title';
		
		$query = (new \yii\db\Query())
            ->select(['p.id as id', 'p.title as title', 'p.status as status', 'c.title as ctitle'])
            ->from('{{%blog_post}} as p')
            ->join(' JOIN', '{{%blog_catalog}} as c', 'c.id = p.id_catalog_fk');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );

		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-1' => 'danger', '0' => 'info', '1' => 'success'];
        $status = ['-1' => 'odrzucony', '0' => 'nieopulikowany', '1' => 'opublikowany'];

        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="/panel/pl/blog/blogpost/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="/panel/pl/blog/blogpost/delete/%d" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['title'] = $value['title'];
            $tmp['catalog'] = $value['ctitle'];
            $tmp['status'] = '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single BlogPost model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //if(!Yii::$app->user->can('readPost')) throw new HttpException(401, 'No Auth');
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BlogPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //if(!Yii::$app->user->can('createPost')) throw new HttpException(401, 'No Auth');

        $model = new BlogPost();
        $model->loadDefaultValues();
        $model->created_by = Yii::$app->user->identity->id;
       // $model->tags = 'wybierz tagi do artykułu';

        if ($model->load(Yii::$app->request->post())) {
            $model->banner = UploadedFile::getInstance($model, 'banner');
            $tags_tmp = '';
            if($model->tags) {
                foreach($model->tags as $key => $tag) {
                    $tags_tmp .= $tag .',';
                }
                $model->tags = substr($tags_tmp, 0, -1);
            }
            
            if ($model->validate()) {
                if ($model->banner) {
                    $bannerName = Yii::$app->params['blogUploadPath'] . date('Ymdhis') . rand(1000, 9999) . '.' . $model->banner->extension;
                    $model->banner->saveAs(Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . $bannerName);
                    $model->banner = $bannerName;
                }
                
               /* $newGallery = new \backend\Modules\Cms\models\CmsGallery();
                $newGallery->title = 'blog#'.$id;
                if( $newGallery->save() ) $model->id_gallery_fk = $newGallery->id;*/
                
                $model->save(false);

                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BlogPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //if(!Yii::$app->user->can('updatePost')) throw new HttpException(401, 'No Auth');

        $model = $this->findModel($id);
        
        if(!$model->id_gallery_fk) {
            $newGallery = new \backend\Modules\Cms\models\CmsGallery();
            $newGallery->title = 'blog#'.$id; 
            $newGallery->type_fk = 1;
            $newGallery->view_type = 0;
            if( $newGallery->save() ) { $model->id_gallery_fk = $newGallery->id; $model->save(); }
        }
        $model->tags = explode(',', $model->tags);
        $oldBanner = $model->banner;

        if ($model->load(Yii::$app->request->post())) {
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if ($model->validate()) {
                $tags_tmp = '';
                if($model->tags) {
                    foreach($model->tags as $key => $tag) {
                        $tags_tmp .= $tag .',';
                    }
                    $model->tags = substr($tags_tmp, 0, -1);
                }
                if($model->banner){
                    $bannerName = Yii::$app->params['blogUploadPath'] . date('Ymdhis') . rand(1000, 9999) . '.' . $model->banner->extension;
                    $model->banner->saveAs(Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . $bannerName);
                    $model->banner = $bannerName;
                } else {
                    $model->banner = $oldBanner;
                }
                $model->save(false);
                return $this->redirect(['update', 'id' => $model->id]);
            } /*else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }*/
        } else {
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionMedia($id) {
        
        $model = $this->findModel($id);
        
        return $this->render('media', [
            'model' => $model,
        ]);
    }
    
    public function actionGallery($id) {
        
        $model = $this->findModel($id);
        
        return $this->render('gallery', [
            'model' => $model,
        ]);
    }
    
    public function actionPostaccept($id) {
        
       /* if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        } */
        
        $post = BlogPost::findOne($id);
        $post->status = 1;
        $post->save();
        Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Wpis został opublikowany')  );
        return $this->redirect(['update', 'id' => $post->id]);
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }
    
    public function actionPostthrow($id) {
        
       /* if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        } */
        
        $post = BlogPost::findOne($id);
        $post->scenario = 'delete';
        $post->status = -1;
        if($post->load(Yii::$app->request->post()) && $post->save())
            Yii::$app->getSession()->setFlash( 'success', Yii::t('lsdd', 'Wpis został odrzucony')  );
        else
            Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Prosze podać powód odrzucenia.')  );
        return $this->redirect(['update', 'id' => $post->id]);
        
        /*if(SvcOffer::find()->where(['id_user_fk' => \Yii::$app->user->id])->one()->id != $email->id_offer_fk) {
            return $this->redirect(Url::to(['/site/noaccess']));*/
        
    }

    /**
     * Deletes an existing BlogPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //if(!Yii::$app->user->can('deletePost')) throw new HttpException(401, 'No Auth');

        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = Status::STATUS_DELETED;
        $model->save();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-posts');	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-posts');	
        }

       // return $this->redirect(['index']);
    }

    /**
     * Finds the BlogPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogPost::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPublish($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $response = false;
        $appId = '682422408630872';
        $appSecret = '91a618f4fd16b6a08e511d54f33ff7b4';
        $accessToken = \Yii::$app->session->get('fb.token');
        
        if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
            $fb = new \Facebook\Facebook([
                'app_id' => $appId,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v2.8',
                'default_access_token' => \Yii::$app->session->get('fb.token'),
                'cookie' => true
            ]);
            //$imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id_user_fk.'-big.jpg':'/images/fb_logo.jpg';
            $imgSrc = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/blog/".$model->id."/medium.jpg")) ? "/../uploads/blog/".$model->id."/medium.jpg" : "/../images/fb_logo.jpg";
            $linkData = [
              'link' => \Yii::$app->urlManagerFrontEnd->createUrl('blog/post/'.CustomHelpers::encode($model->id).'-'.urlencode($model->title) ),
              'message' => ($model->brief) ? $model->brief : trim(preg_replace('/\s\s+/', ' ', $model->note)),
              'picture' => 'http://twojeprzyjecia.pl'.$imgSrc,
              'name' => $model->title,
              'description' => $model->surname
              ];

            try {
             
                $permissions = $fb->get('/me/permissions');//var_dump($permissions);
                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                $user = $response->getGraphUser(); //var_dump($user);
                $msg = $fb->post( '/'.$user->getId().'/feed', $linkData, $accessToken  );
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
               // echo 'Graph returned an error: ' . $e->getMessage();
                return ['result' => false, 'error' => $e->getMessage(), 'alert' => '<div class="alert alert-success">Artykuł nie został opublikowan na FB</div>'];
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                return ['result' => false,  'error' => $e->getMessage()];
            }
            return ['result' => true, 'msg' => $msg, 'alert' => '<div class="alert alert-success">Artykuł został opublikowana na FB</div>'];
        } else {
        
            return ['result' => false, 'alert' => '<div class="alert alert-success">Oferta nie została opublikowana na FB</div>'];
        }
    }
}
