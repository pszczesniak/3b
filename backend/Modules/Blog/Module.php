<?php

namespace app\Modules\Blog;

use Yii;

/**
 * Blog module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\Modules\Blog\controllers';
    
   /* public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // your custom code here
        if(!\Yii::$app->user->isGuest) {
            $rule = (isset(\Yii::$app->user->identity->role) && \Yii::$app->user->identity->role->name == 'admin');
        } else $rule = true;
        
        if( $rule ) 
            return true;
        else
            return \Yii::$app->getResponse()->redirect('/site/noaccess');
       
    }*/
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    
     public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('Blog/' . $category, $message, $params, $language);
    }
}
