<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Słowniki');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'dict-dictionary-index', 'title'=>Html::encode($this->title))) ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
 
	<div id="toolbar-data" class="btn-group">
		<?= (1==1)?Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/dict/dictionary/createajax']) , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]):'' ?>
    </div>
    <div class="div-table">
        <table  class="table table-striped table-items header-fixed"  id="table-data"
                data-toolbar="#toolbar-data" 
                data-toggle="table" 
                data-show-refresh="true" 
                data-show-toggle="false"  
                data-show-columns="false" 
                data-show-export="false"  
            
                data-show-pagination-switch="false"
                data-pagination="true"
                data-pagination-h-align="left"
                data-pagination-detail-h-align="right"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-height="500"
                data-show-footer="false"
                data-side-pagination="client"
                data-row-style="rowStyle"
                data-sort-name="name"
                data-sort-order="asc"
                data-method="get"
                data-url=<?= Url::to(['/dict/dictionary/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="name"  data-sortable="true" data-width="90%">Nazwa</th>
                    <th data-field="actions" data-events="actionEvents" data-width="10%"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">
            </tbody>           
        </table>
        <fieldset><legend>Objaśnienia</legend> 
            <table class="calendar-legend">
                <tbody>
                    <tr><td class="calendar-legend-icon" style="background-color: #c4e3f3"></td><td>Opcjonalnie ustawiane w filtrach</td></tr>
                    <tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Archiwalne</td></tr>
                </tbody>
            </table>
        </fieldset>
    </div>	
</div>
<?php $this->endContent(); ?>
