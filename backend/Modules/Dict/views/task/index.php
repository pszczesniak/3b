<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Task\models\CalCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Typy zadań');
$this->params['breadcrumbs'][] = 'Administracja';
$this->params['breadcrumbs'][] = 'Słowniki';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>'crm-group-index', 'title'=>Html::encode($this->title))) ?>

    <?= Alert::widget() ?>
   <div class="info-message"></div>
	<div id="toolbar-items" class="btn-group toolbar-table-widget">
		 <?= Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/dict/task/create'] ) , 
					['class' => 'btn btn-success btn-icon viewModal',  
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-items",
					 'data-title' => "Dodaj"
					]) ?>
		<a class="btn bg-purple btn-icon none" id="save-table-items-move" href="<?= Url::to(['/dict/task/save']) ?>"><i class="fa fa-save"></i>Zapisz zmiany</a>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-items"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	</div>
    <div class="div-table-items">
        <table  class="table table-striped table-items header-fixed table-widget"  id="table-items"
                data-toolbar="#toolbar-items" 
                data-toggle="table-widget" 
                data-show-refresh="false" 
                data-show-toggle="true"  
                data-show-columns="false" 
                data-show-export="false"  
                table-page-size="40"
                data-show-pagination-switch="false"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100]"
                data-height="600"
                data-show-footer="false"
                data-row-style="rowStyle"
                data-filter-control="false"
                data-filter-show-clear="false"
                data-url=<?= Url::to(['/dict/task/data']) ?>>
            <thead>
                <tr>
                    <th data-field="id" data-visible="false">ID</th>
                    <th data-field="no" data-visible="false" data-align="center"></th>
                    <th data-field="symbol" data-sort-name="symbol" data-sortable="true">Symbol</th>
                    <th data-field="name" data-sort-name="name" data-sortable="true">Nazwa</th>
                    <th data-field="color" data-sortable="true" data-width="30px" data-align="center">Kolor</th>
                    <th data-field="icon" data-sortable="true" data-width="30px" data-align="center">Ikonka</th>
                    <th data-field="move" data-events="actionEvents" data-align="center" data-width="100px"></th>
                    <th data-field="actions" data-events="actionEvents" data-align="center" data-width="90px"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">

            </tbody>
            
        </table>
    </div>

	<!-- Render modal form -->
	

<?php $this->endContent(); ?>
