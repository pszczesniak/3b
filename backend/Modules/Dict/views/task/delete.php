<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCase */
use common\widgets\Alert;
$this->title = Yii::t('app', 'Panel klienta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Alert');
?>

<div class="alert alert-danger">
    Klient <b><?= $model->name ?></b> został usunięty <br /><br />
    Data usunięcia: <?= $model->deleted_at ?><br />
    Osoba usuwająca: <?= \common\models\User::findOne($model->deleted_by)->fullname ?>
</div>