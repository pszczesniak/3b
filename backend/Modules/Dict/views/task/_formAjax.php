<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    //'options'=>['class' => 'form-horizontal', 'enctype'=>'multipart/form-data'],
    'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
    'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-items"],
    'fieldConfig' => [
        //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        //'template' => '<div class="row"><div class="col-sm-2 col-xs-4">{label}</div><div class="col-sm-10 col-xs-8">{input}</div><div class="col-xs-12">{error}</div></div>',
        /*'labelOptions' => ['class' => 'col-lg-2 control-label'],*/
    ],
]); ?>
<div class="modal-body">
    <div class="grid">
        <div class="col-sm-8 col-xs-8"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>
        <div class="col-sm-4 col-xs-4"><?= $form->field($model, 'symbol')->textInput(['maxlength' => true, 'class' => 'form-control']) ?></div>        
    </div>  
    <div class="grid grid--0">
        <div class="col-sm-6">
            <div class="example-content">
                <div class="example-content-widget">
                    <div id="cp2" class="input-group colorpicker-component">
                        <input type="text" value="<?= $model->dict_color ?>" class="form-control" name="CalDictionary[dict_color]" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <script>
                        $(function () {
                            $('#cp2').colorpicker();
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-sm-6 icon-picker">
            <div class="ct-iconpicker">
                <div class="ct-ip-holder">
                    <div class="ct-ip-icon"><i class="<?= $model->dict_icon ?>"></i></div>
                    <input type="hidden" value="<?= $model->dict_icon ?>" class="ct-icon-value" name="CalDictionary[dict_icon]">
                </div>
                <div class="ct-ip-popup clearfix">
                    <!--<div class="ct-ip-search">
                        <input type="text" class="ct-ip-search-input" placeholder="Search icon" />
                    </div>-->
                    <ul>  
                        <li><a href="#" data-icon="fa fa-list"><i class="fa fa-list"></i></a></li>
                        <li><a href="#" data-icon="fa fa-phone"><i class="fa fa-phone"></i></a></li>
                        <li><a href="#" data-icon="fa fa-envelope"><i class="fa fa-envelope"></i></a></li>
                        <li><a href="#" data-icon="fa fa-wrench"><i class="fa fa-wrench"></i></a></li>
                        <li><a href="#" data-icon="fa fa-cog"><i class="fa fa-cog"></i></a></li>
                        <li><a href="#" data-icon="fa fa-circle"><i class="fa fa-circle"></i></a></li>
                        <li><a href="#" data-icon="fa fa-book"><i class="fa fa-book"></i></a></li>
                        <li><a href="#" data-icon="fa fa-briefcase"><i class="fa fa-briefcase"></i></a></li>
                        <li><a href="#" data-icon="fa fa-car"><i class="fa fa-car"></i></a></li>
                        <li><a href="#" data-icon="fa fa-bicycle"><i class="fa fa-bicycle"></i></a></li>    
                        <li><a href="#" data-icon="fa fa-building"><i class="fa fa-building"></i></a></li>                        
                        <li><a href="#" data-icon="fa fa-camera"><i class="fa fa-camera"></i></a></li>
                        <li><a href="#" data-icon="fa fa-video"><i class="fa fa-video"></i></a></li>
                        <li><a href="#" data-icon="fa fa-desktop"><i class="fa fa-desktop"></i></a></li>
                        <li><a href="#" data-icon="fa fa-laptop"><i class="fa fa-laptop"></i></a></li>
                        <li><a href="#" data-icon="fa fa-heartbeat"><i class="fa fa-heartbeat"></i></a></li>
                        <li><a href="#" data-icon="fa fa-sun"><i class="fa fa-sun"></i></a></li>
                        <li><a href="#" data-icon="fa fa-bolt"><i class="fa fa-bolt"></i></a></li>
                        <li><a href="#" data-icon="fa fa-bell"><i class="fa fa-bell"></i></a></li>
                        <li><a href="#" data-icon="fa fa-coffee"><i class="fa fa-coffee"></i></a></li>
                        <li><a href="#" data-icon="fa fa-comments"><i class="fa fa-comments"></i></a></li>
                        <li><a href="#" data-icon="fa fa-eye-dropper"><i class="fa fa-eye-dropper"></i></a></li>
                        <li><a href="#" data-icon="fa fa-filter"><i class="fa fa-filter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <fieldset><legend>Opis</legend>
        <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(false) ?>
    </fieldset>
</div>
<div class="modal-footer"> 
    <div class="form-group align-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">
    ;(function($){
          $('.ct-iconpicker .ct-ip-holder .ct-ip-icon').on('click', function(){
            var iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');
            
            iconPickerPopup.slideToggle();
          });
          $('.ct-iconpicker .ct-ip-popup').on('click', 'a', function (e) {
            e.preventDefault();
            var iconClass = $(this).data('icon'),
                inputField = $('.ct-icon-value'),
                iconHolder = $('.ct-ip-icon i');
            iconHolder.attr('class', '');
            iconHolder.addClass(iconClass);
            inputField.val(iconClass);
          });

          $(document).mouseup(function (e){
            var iconPicker = $('.ct-iconpicker'),
                iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');

              if ( ( ! iconPicker.is(e.target) && iconPicker.has(e.target).length === 0 ) ){
                  iconPickerPopup.slideUp();
              }
          });
          

        })(jQuery)
</script>