<?php

namespace app\Modules\Dict\controllers;

use Yii;
use backend\Modules\Task\models\CalDictionary;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\components\CustomHelpers;

/**
 * TaskController implements the CRUD actions for Customer model.
 */
class TaskController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    //'data' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CalDictionary();
        $setting = ['limit' => 20, 'offset' => 0, 'page' => 1];
        if( isset($_GET['back']) && $_GET['back'] == 'yes' ) {
            $params = \Yii::$app->session->get('search.dicts');  
            if($params['params']) {
                foreach($params['params'] as $key => $value) {
                    $searchModel->$key = $value;
                }
                $setting = ['limit' => $params['post']['limit'], 'offset' => $params['post']['offset'], 'page' => ($params['post']['offset']/$params['post']['limit']+1)];
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'setting' => $setting
        ]);
    }
    
    public function actionData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsDataQuery = CalDictionary::find()->where(['status' => 1]);
        
        $where = [];
        $post = $_GET;
        if(isset($_GET['CalDictionary'])) {
            $params = $_GET['CalDictionary'];
            \Yii::$app->session->set('search.crmgroup', ['params' => $params, 'post' => $post]);
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower( addslashes($params['name']) )."%'");
				array_push($where, "lower(name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['symbol']) && !empty($params['symbol']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
				array_push($where, "lower(symbol) like '%".strtolower( addslashes($params['symbol']) )."%'");
            }
        }
			
		$fields = [];
		$tmp = [];
        
		$sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'symbol' ) $sortColumn = 'symbol';
        /*if( isset($post['sort']) && $post['sort'] == 'action_date' ) $sortColumn = 'action_date';
        if( isset($post['sort']) && $post['sort'] == 'name' ) $sortColumn = 'name';
        if( isset($post['sort']) && $post['sort'] == 'unit_time' ) $sortColumn = 'unit_time';*/
        
        $query = (new \yii\db\Query())
            ->select(['c.id as id', 'c.name as name', 'c.symbol as symbol', 'c.status', 'c.description', 'c.dict_color', 'c.dict_icon'])
            ->from('{{%cal_dictionary}} c')
            //->join('LEFT JOIN', '{{%company_employee}} e', 'e.id = a.id_employee_fk')
            ->where( 'status >= 1' );
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' collate `utf8_polish_ci` ' . (isset($post['order']) ? $post['order'] : 'asc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand());
        $rows = $query->all();
		      
		foreach($rows as $key=>$value) {
			
            $tmp['name'] = $value['name'];
            $tmp['symbol'] = $value['symbol'];
            $tmp['describe'] = $value['description'];
            $tmp['color'] = '<span style="width:15px; height: 15px; display:block; margin-left:20px; background-color: '.$value['dict_color'].'">&nbsp;</span>';
            $tmp['icon'] = '<i class="'.$value['dict_icon'].'"></i>';
           
            $tmp['actions'] = '<div class="edit-btn-group">';
                $tmp['actions'] .= '<a href="'.Url::to(['/dict/task/update','id' => $value['id']]).'" class="btn btn-sm btn-default update" data-target="#modal-grid-item" title="'.Yii::t('app', 'Edit').'"><i class="fa fa-pencil-alt"></i></a>';
                $tmp['actions'] .= (($value['status'] == 1) ? '<a href="'.Url::to(['/dict/task/delete','id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-items" title="'.Yii::t('app', 'Delete').'"><i class="fa fa-trash"></i></a>' : '');
            $tmp['actions'] .= '</div>';
       
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = CustomHelpers::encode($value['id']);
			
			array_push($fields, $tmp); $tmp = [];
		}
		//return ['total' => $count, 'rows' => $fields];
        return $fields;
	}

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)   {        
        $model = $this->findModel($id);

        return $this->renderPartial('view', [
            'model' => $model, 
        ]);
    }
    
    public function actionCreate() {	
        $model = new CalDictionary();
        $model->type_fk = 1;
        $model->dict_icon = 'fa fa-list';
        $model->dict_color = "#3538ca";
        
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
            $model->type_fk = 1;
            //$model->name = mb_strtoupper($model->name, "UTF-8"); $model->firstname = mb_strtoupper($model->firstname, "UTF-8"); $model->lastname = mb_strtoupper($model->lastname, "UTF-8");  
            if($model->validate() && $model->save()) {
           
                return array('success' => true, 'refresh' => 'yes', 'table' => "#table-items", 'alert' => 'Wpis <b>'.$model->name.'</b> został utworzony', 'id'=>$model->id,'name' => $model->name, 'input' => isset($_GET['input']) ? '#'.$_GET['input'] : false );	
            } else {
                $model->status = 0;
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model, 'grants' => $this->module->params['grants']]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model, ]) ;	
		}
	}
    
    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)   {
        
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());

			if($model->validate() && $model->save()) {
				return array('success' => true,  'alert' => 'Dane zostały zaktualizowane.', 'table' => '#table-items', 'refresh' => 'inline', 'index' => (isset($_GET['index']) ? $_GET['index'] : -1) );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', ['model' => $model,]) ;	
		}
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)  {
        //$this->findModel($id)->delete();

        //Yii::$app->response->format = Response::FORMAT_JSON;
        $model= $this->findModel($id);
        //$model->scenario = CalTask::SCENARIO_DELETE;
        
        /*$tasks = CalTask::find()->where(['status' => 1, 'id_crm_group_fk' => $model->id])->count();
        if($customers > 0) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Wpis <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ jest w użyciu')  );
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Wpis <b>'.$model->name.'</b> nie może zostać usunięty, ponieważ jest w użyciu', 'id' => $id, 'table' => '#table-items');	
			}
        }*/
        
        $model->status = -1;
		$model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'success', Yii::t('app', 'Słownik <b>'.$model->name.'</b> został usunięty')  );
				return $this->redirect(['index']);
		    } else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => true, 'alert' => 'Słownik <b>'.$model->name.'</b> został usunięty', 'id' => $id, 'table' => '#table-items');	
			}
        } else {
            if(!Yii::$app->request->isAjax) {
				Yii::$app->getSession()->setFlash( 'danger', Yii::t('app', 'Słownik <b>'.$model->name.'</b> nie został usunięty')  );
				return $this->redirect(['view', 'id' => $id]);
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array('success' => false, 'alert' => 'Słownik <b>'.$model->name.'</b> nie został usunięty', 'id' => $id, 'table' => '#table-items');	
			}
        }
        
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CrmGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)   {
		$id = CustomHelpers::decode($id);

		if (($model = CalDictionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Słownik o wskazanym identyfikatorze nie został znaleziony w systemie.');
        }
    }
   
    public function actionExport() {
	
		$objPHPExcel = new \PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Lawfirm")
                    ->setLastModifiedBy("Lawfirm")
                    ->setTitle("Excel Export Document")
                    ->setSubject("Excel Export Document")
                    ->setDescription("Exporting documents to Excel using php classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Excel export file");
        $styleArray = array(
            'alignment' => array(
              //  'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
		
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
       
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Klienci');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Autor: '.\Yii::$app->user->identity->fullname.', Utworzony: '.date("Y-m-d H:i:s").'');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('FF808080');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB('FFFFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
		$sheet = 0;
		$objPHPExcel->setActiveSheetIndex($sheet);
		
		$objPHPExcel->setActiveSheetIndex($sheet)
			->setCellValue('A3', 'Nazwa')
			->setCellValue('B3', 'Adres')
			->setCellValue('C3', 'NIP')
			->setCellValue('D3', 'REGON');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(true); 
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('A3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('B3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFill()->getStartColor()->setARGB('D9D9D9');
 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('C3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->getStartColor()->setARGB('D9D9D9');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex($sheet)->getStyle('D3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('D3')->getFill()->getStartColor()->setARGB('D9D9D9');
			
		$i=4; 
		$data = [];

        $fieldsDataQuery = Customer::find()->where(['status' => 1, 'type_fk' => 1]);
        
        if(isset($_GET['CustomerSearch'])) {
            $params = $_GET['CustomerSearch'];
            if(isset($params['name']) && !empty($params['name']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("lower(name) like '%".strtolower($params['name'])."%'");
            }
            if(isset($params['id_department_fk']) && !empty($params['id_department_fk']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id in (select id_customer_fk from law_customer_department where status = 1 and id_department_fk = ".$params['id_department_fk'].")");
            }
            if(isset($params['id_dict_customer_status_id']) && !empty($params['id_dict_customer_status_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_status_id = ".$params['id_dict_customer_status_id']);
            }
            if(isset($params['id_dict_customer_debt_collection_id']) && !empty($params['id_dict_customer_debt_collection_id']) ) {
                $fieldsDataQuery = $fieldsDataQuery->andWhere("id_dict_customer_debt_collection_id = ".$params['id_dict_customer_debt_collection_id']);
            }
        }

        if(count(array_intersect(["grantAll"], $this->module->params['grants'])) == 0) {
            $fieldsDataQuery = $fieldsDataQuery->andWhere('id in (select id_customer_fk from {{%customer_department}} where id_department_fk in ('.implode(',', $this->module->params['departments']).') )');
        }
            
        $data = $fieldsDataQuery->all();
			
		if(count($data) > 0) {
			foreach($data as $record){ 
				$objPHPExcel->getActiveSheet()->setCellValue('A'. $i, $record->name ); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'. $i, $record->address); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'. $i, $record->nip); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $record->regon); 
                $employees = '';
                /*foreach($record->employees as $key => $employee) {
                    $employees .= $employee->employee['fullname'].PHP_EOL;
                } //"Hello".PHP_EOL." World"
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $i, $employees); $objPHPExcel->getActiveSheet()->getStyle('D'. $i)->getAlignment()->setWrapText(true);*/
						
				$i++; 
			}  
		}
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
		--$i;
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->applyFromArray($styleArray);

		$objPHPExcel->getActiveSheet()->setTitle('Klienci');
	    
        $objPHPExcel->setActiveSheetIndex(0); 
		
		$filename = 'Klienci'.'_'.date("Y_m_d__His").".xlsx";
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename='.$filename .' ');
		header('Cache-Control: max-age=0');			
		$objWriter->save('php://output');
		
	}
    
    public function actionSave() {
		Yii::$app->response->format = Response::FORMAT_JSON;
              
        $res = [];
        $i = 1;
        $items = Yii::$app->request->post('items');
        $transaction = \Yii::$app->db->beginTransaction();
		try {
            foreach($items as $key => $item) {
                $model = CalDictionary::findOne($item['id']);
                $model->rank_no = $i; ++$i;
                if(!$model->save()) {
                    $res = ['success' => false, 'alert' => 'Zmiany nie zostały zapisane']; 
                } 
            }
            $transaction->commit();
            $res = ['success' => true, 'alert' => 'Zmiany zostały zapisane'];
        } catch (Exception $e) {echo $e;
			$transaction->rollBack();
            $res = ['success' => false, 'alert' => 'Zmiany zostały zapisane'];
		}
        
        return $res;
    }
}

