<?php

namespace app\Modules\Dict;

use Yii;
use yii\helpers\Url;

/**
 * Dict module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\Modules\Dict\controllers';

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        if(!Yii::$app->user->isGuest) {
			if(!in_array(Yii::$app->controller->id, [ 'site' ]) ){
				/*if(!$this->params['departments'] ) {
					throw new \yii\web\HttpException(405, 'Parametry sesji nie zosta�y ustawione lub wygas�y. Aby rozpocz�� prac� z systemem nale�y wybra� opcj� konfiguracji sesji i ustawi� wymagane parametry..');
				}*/
                if(Yii::$app->runAction('/site/mustresetpassword', []) >= 30 && \Yii::$app->user->identity->username != 'superadmin')
                    return Yii::$app->getResponse()->redirect(Url::to(['/site/changepassword']));
			}
        } else {
			//return $this->redirect(Url::to(['/site/login']));
			return Yii::$app->getResponse()->redirect(Url::to(['/site/login']));
		}

        return true; // or false to not run the action
    }
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
