<?php

namespace backend\Modules\Dict\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%dictionary_value}}".
 *
 * @property integer $id
 * @property integer $id_dictionary_fk
 * @property string $name
 * @property string $name_langs
 * @property string $describe
 * @property string $describe_langs
 * @property integer $is_system
 * @property integer $in_use
 * @property string $custom_data
 * @property string $data_arch
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class DictionaryValue extends \yii\db\ActiveRecord
{
    public $color = '#4b87ae';
    public $icon = 'fa fa-circle';
    public $sideOrder = 1;
	public $required_meetingWith;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dictionary_fk', 'is_system', 'in_use', 'is_archive', 'is_default', 'status', 'created_by', 'updated_by', 'deleted_by', 'sideOrder', 'required_meetingWith'], 'integer'],
            [['name', 'id_dictionary_fk'], 'required'],
            [['name_langs', 'symbol_langs', 'symbol', 'describe', 'describe_langs', 'custom_data', 'data_arch', 'color', 'icon'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'color', 'icon'], 'safe'],
            [['name'], 'string', 'max' => 300],
            ['name', 'uniqueValue'/*, 'when' => function($model) { return $model->isNewRecord; }*/ ],
        ];
    }
    
    public function uniqueValue($attribute, $params)  {
        if($this->isNewRecord)
			$values = \backend\Modules\Dict\models\DictionaryValue::find()->where(['status' => 1, 'id_dictionary_fk' => $this->id_dictionary_fk])->andWhere("lower(replace(name, ' ', '')) = '".str_replace(' ', '', htmlspecialchars($this->name, ENT_QUOTES, 'UTF-8'))."'")->all();
        else
			$values = \backend\Modules\Dict\models\DictionaryValue::find()->where(['status' => 1, 'id_dictionary_fk' => $this->id_dictionary_fk])->andWhere("id != ".$this->id." and lower(replace(name, ' ', '')) = '".str_replace(' ', '', htmlspecialchars($this->name, ENT_QUOTES, 'UTF-8'))."'")->all();

		$ok = true;
        
        if(count($values) > 0) {
            $this->addError($attribute, 'Ten słownik posiada już taką wartość.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_dictionary_fk' => Yii::t('app', 'Id Dictionary Fk'),
            'name' => Yii::t('app', 'Name'),
            'name_langs' => Yii::t('app', 'Name Langs'),
            'describe' => Yii::t('app', 'Describe'),
            'describe_langs' => Yii::t('app', 'Describe Langs'),
            'is_system' => Yii::t('app', 'Is System'),
            'in_use' => Yii::t('app', 'In Use'),
            'is_archive' => 'Wpisy archiwalne',
            'is_default' => 'Wartość opcjonalnie ustawiana w filtrach',
			'required_meetingWith' => 'Wymagane wypełnienie parametru `Spotkanie z` przy rezerwacji zasobu',
            'custom_data' => Yii::t('app', 'Custom Data'),
            'data_arch' => Yii::t('app', 'Data Arch'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		
        $this->custom_data = \yii\helpers\Json::encode(['color' => $this->color, 'icon' => $this->icon, 'sideOrder' => $this->sideOrder, 'required_meetingWith' => $this->required_meetingWith]);
        
        if (parent::beforeSave($insert)) {
			if($this->isNewRecord ) {
				$this->created_by = (Yii::$app->id == 'app-console') ? 1 : \Yii::$app->user->id;
            
			} else { 
				$this->updated_by = \Yii::$app->user->id;
                
			}
			return true;
		} else { 		
			return false;
		}
		return false;
	}
    
    public function afterSave($insert, $changedAttributes) {
        if($this->is_default) {
            $sqlInstances = "update {{%dictionary_value}} set is_default = 0 where status = 1 and id != ".$this->id." and id_dictionary_fk = ".$this->id_dictionary_fk;
            $result = Yii::$app->db->createCommand($sqlInstances)->execute();
        }
        parent::afterSave($insert, $changedAttributes);
	}
    
    public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCustom() {
        $custom = [];
        $custom['icon'] = $this->icon;
        $custom['color'] = $this->color;
        if(($this->id_dictionary_fk == 4 || $this->id_dictionary_fk == 12) && $this->custom_data) {
            $customData = \yii\helpers\Json::decode($this->custom_data);
            if($customData) {
                $custom['icon'] = $customData['icon'];
                $custom['color'] = $customData['color'];
                $custom['sideOrder'] = (isset($customData['sideOrder'])) ? $customData['sideOrder'] : 1; 
				$custom['required_meetingWith'] = (isset($customData['required_meetingWith'])) ? $customData['required_meetingWith'] : 0; 
            }
        }
        
        return $custom;
    }
}
