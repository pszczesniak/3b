<?php

namespace backend\Modules\Apl\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%apl_info}}".
 *
 * @property integer $id
 * @property integer $id_parent_fk
 * @property integer $type_fk: 1-widoczna dla klienta; 2-notatka wewnętrzna
 * @property integer $id_customer_fk
 * @property integer $id_loan_fk
 * @property string $created_at
 * @property integer $created_by
 * @property integer $id_arch_fk
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AplInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apl_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_customer_fk', 'id_loan_fk'], 'required'],
            [['type_fk', 'id_parent_fk', 'id_customer_fk', 'id_loan_fk', 'created_by', 'status', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at', 'info_content', 'data_arch'], 'safe'],
            [['info_content'], 'required', 'when' => function($model) { return ($model->status == 1); }], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_customer_fk' => Yii::t('app', 'Id Customer Fk'),
            'id_loan_fk' => Yii::t('app', 'Id Loan Fk'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'type_fk' => Yii::t('app', 'Id Arch Fk'),
            'status' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'info_content' => 'Wiadomość',
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {			
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}

	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getParent() {
        return \backend\Modules\Apl\models\AplInfo::findOne($this->id_parent_fk);
    }
    
    public function getChildren() {
        return \backend\Modules\Apl\models\AplInfo::find()->where(['id_parent_fk' => $this->id, 'status' => 1])->orderby('id desc')->all();
    }
    
    public function getCustomer() {
        return \backend\Modules\Apl\models\AplCustomer::findOne($this->id_customer_fk);
    }
    
    public function getLoan() {
        return \backend\Modules\Apl\models\AplLoan::findOne($this->id_loan_fk);
    }
}