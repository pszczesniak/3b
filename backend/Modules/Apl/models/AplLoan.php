<?php

namespace backend\Modules\Apl\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%apl_loan}}".
 *
 * @property integer $id
 * @property integer $id_request_fk
 * @property integer $id_customer_fk
 * @property double $l_amount
 * @property integer $l_period
 * @property double $l_monthly_rate
 * @property double $l_amount_total
 * @property double $l_rrso
 * @property integer $id_purpose_fk
 * @property string $l_purpose
 * @property string $shift_term
 * @property integer $l_day_of_payment
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AplLoan extends \yii\db\ActiveRecord
{
    const SCENARIO_ACCEPT = 'accept';
    
    public $accepts;
    
    public $amountFrom;
    public $amountTo;
    public $periodFrom;
    public $periodTo;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apl_loan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_request_fk', 'id_customer_fk', 'l_period', 'id_purpose_fk', 'l_day_of_payment', 'status', 'created_by', 'updated_by', 'deleted_by', 'shift_months'], 'integer'],
            [['l_amount', 'l_period', 'id_purpose_fk', 'l_day_of_payment'], 'required'],
            [['l_amount', 'l_monthly_rate', 'l_amount_total', 'l_rrso', 'l_provision', 'l_provision_amount', 'l_interest', 'l_interest_amount'], 'number'],
            [['l_purpose', 'custom_data'], 'string'],
            [['shift_term', 'created_at', 'updated_at', 'deleted_at', 'accepts', 'repayment_term'], 'safe'],
            ['accepts', 'validateAccept', 'on' => self::SCENARIO_ACCEPT, 'skipOnEmpty' => false],
        ];
    }
              
    public function scenarios() {
        $scenarios = parent::scenarios();
		$scenarios['default'] = ['id_request_fk', 'id_customer_fk', 'l_amount', 'l_period', 'id_purpose_fk', 'l_day_of_payment', 'repayment_term',
                                 'l_monthly_rate', 'l_amount_total', 'l_rrso', 'l_provision', 'l_provision_amount', 'l_interest', 'l_interest_amount'];
        $scenarios[self::SCENARIO_ACCEPT] = ['accepts'];
        return $scenarios;
    }
    
    public function validateAccept($attribute, $params)  {

        if(!$this->accepts) {
            $this->addError($attribute, 'Prosimy o akceptację warunków');
        } else {   
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 4])->all();
            foreach($accepts as $key => $value) {
                if(!in_array($value->id, $this->accepts))
                    $this->addError($attribute, 'Prosimy o akceptację warunków');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_request_fk' => Yii::t('app', 'Id Request Fk'),
            'id_customer_fk' => Yii::t('app', 'Klient'),
            'l_amount' => Yii::t('app', 'Kwota'),
            'l_period' => Yii::t('app', 'Okres'),
            'l_monthly_rate' => Yii::t('app', 'Miesięczna rata'),
            'l_amount_total' => Yii::t('app', 'Całkowity koszt'),
            'l_rrso' => Yii::t('app', 'RRSO'),
            'id_purpose_fk' => Yii::t('app', 'Cel'),
            'l_purpose' => Yii::t('app', 'Cel'),
            'shift_term' => Yii::t('app', 'Przesunięcie spłaty'),
            'l_day_of_payment' => Yii::t('app', 'Dzień spłaty'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'l_provision' => 'Prowizja',
            'l_provision_amount' => 'Wartość prowizji',
            'l_interest' => 'Odsetki',
            'l_interest_amount' => 'Wartość odsetek',
            'repayment_term' => 'Okres kredytowania',
            'shift_months' => 'Przesunięcie spłaty [mc]'
        ];
    }
    
    public function beforeSave($insert) {
		$this->l_amount_total = $this->l_monthly_rate * $this->l_period + $this->l_provision_amount;
        
        if (parent::beforeSave($insert)) {
			
			if($this->isNewRecord ) {
				$this->created_by = \Yii::$app->user->id;
			}/* else { 
				$modelArch = new LsddArchives();
				$modelArch->fk_id = $this->id;
				$modelArch->table_fk = $this->idArchive;
				$modelArch->user_action = $this->user_action;
				$modelArch->version_data = \yii\helpers\Json::encode($this);
				$modelArch->created_by = \Yii::$app->user->id;
				$modelArch->created_at = new Expression('NOW()');
			    $modelArch->save();
			}*/
			return true;
		} else { 
						
			return false;
		}
		return false;
	}

	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function listPurposes(){
        $array = [];
        
        $array[1] = 'szkoła';
        $array[2] = 'niespodziewane wydatki';
        $array[3] = 'rodzina';
        $array[4] = 'zakup sprzętu AGD/RTV';
        $array[5] = 'wakacje / podróż';
        $array[6] = 'konsolidacja';
        $array[7] = 'bieżące wydatki';
        $array[8] = 'zakup samochodu';
        $array[9] = 'zakup mieszkania / domu';
        $array[10] = 'pozyczka dla innej osoby';
        $array[11] = 'działalność gospodarcza';
        $array[12] = 'inny';

        return $array;
    }
    
    public static function listDayPayments() {
        $array = [];
        for($i=1; $i<=28;++$i) {
            $array[$i] = $i;
        }
        
        return $array;
    }
    
    public function getPurpose() {
        if($this->id_purpose_fk)
            return self::listPurposes()[$this->id_purpose_fk];
        else
            return 'nie ustawiono';
    }
    
    public function getCustomer() {
        return \backend\Modules\Apl\models\AplCustomer::findOne($this->id_customer_fk);
    }
    
    public function getRequest() {
        return \backend\Modules\Apl\models\AplRequest::findOne($this->id_request_fk);
    }
    
    public static function statusList() {
        $result = [];
        
        $result['names'] = ['-3' => 'usunięty', '-2' => 'odrzucony', '0' => 'niepotwierdzony', '1' => 'oczekujący', '2' => 'przyjęty'];
        $result['colors'] = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'warning', '1' => 'info', '2' => 'success'];
        $result['colors_f'] = ['-3' => 'red', '-2' => 'red', '-1' => 'yellow', '0' => 'yellow', '1' => 'blue', '2' => 'green'];
        $result['icons'] = ['-3' => 'danger', '-2' => 'thumbs-down', '-1' => 'warning', '0' => 'warning', '1' => 'hourglass', '2' => 'thumbs-up'];
        
        return $result;
    }
    
    public function getFiles() {
        $filesData = [];
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 2])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getCfiles() {
        $filesData = [];
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 2, 'show_client' => 1])->orderby('id desc')->all();
        return $filesData;
    }
    
    public function getNotes() {
        //$notesData = [];
        $notesData = \backend\Modules\Apl\models\AplInfo::find()->where(['status' => 1, 'id_loan_fk' => $this->id, 'id_parent_fk' => 0])->orderby('id desc')->all();
        
        return $notesData;
    }
}
