<?php

namespace backend\Modules\Apl\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%apl_request}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $type_fk
 * @property string $params
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AplRequest extends \yii\db\ActiveRecord
{
    
    public $l_amount;
    public $l_period;
    
    public $request_remote_addr;
    public $request_user_agent;
    public $request_content_type;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apl_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['l_amount', 'l_period'], 'required'],
            [['id_user_fk', 'type_fk', 'status', 'created_by', 'updated_by', 'deleted_by', 'l_amount', 'l_period'], 'integer'],
            [['params'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'l_amount', 'l_period', 'request_remote_addr', 'request_user_agent', 'request_content_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'type_fk' => Yii::t('app', 'Type Fk'),
            'params' => Yii::t('app', 'Params'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$this->params = \yii\helpers\Json::encode(['request_remote_addr' => $this->request_remote_addr, 'request_user_agent' => $this->request_user_agent, 'request_content_type' => $this->request_content_type]);
			//'l_amount' => $this->l_amount, 'l_period' => $this->l_period,
            if($this->isNewRecord ) {
                $this->created_by = \Yii::$app->user->id;
			}
			return true;
		} else { 
						
			return false;
		}
		return false;
	}

	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public function getCalculation() {
        $params = \yii\helpers\Json::decode($this->params);
        $paramsArr = [];
        $paramsArr['l_amount'] = $params['l_amount'];
        $paramsArr['l_period'] = $params['l_period'];
        $paramsArr['request_remote_addr'] = $params['request_remote_addr'];
        $paramsArr['request_user_agent'] = $params['request_user_agent'];
        $paramsArr['request_content_type'] = $params['request_content_type'];
        
        $model = \backend\Modules\Cms\models\CmsSite::findOne(1); 
        $custom_data = \yii\helpers\Json::decode($model->custom_data);
        
        $provision = (isset($custom_data['loanProvision'])) ? $custom_data['loanProvision'] : 0;
        $interest = (isset($custom_data['loanInterest'])) ? $custom_data['loanInterest'] : 0;
        
        $rate                  = \frontend\controllers\AplController::pmt($interest, $paramsArr['l_period'], $paramsArr['l_amount']);
        $paramsArr['rate']     = number_format($rate, 2);
        $paramsArr['deadline'] = date('d.m.Y', strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+".$paramsArr['l_period']." months"));
        $charges = $paramsArr['l_amount']*$provision/100;
        $paramsArr['charges']  = number_format($charges, 2, '.', ' ');

        $cashflow = [];
		array_push($cashflow, ['period' => 0, 'amount' => ($charges-$paramsArr['l_amount'])]);
		for($i = 1; $i <= $paramsArr['l_period']; ++$i) {
			array_push($cashflow, ['period' => $i*(365/12)/365, 'amount' => $paramsArr['rate']]);
		}

        $paramsArr['l_rrso'] = \frontend\controllers\AplController::rrso($cashflow);
        
        return $paramsArr;
    }
}
