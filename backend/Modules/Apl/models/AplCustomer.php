<?php

namespace backend\Modules\Apl\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%apl_customer}}".
 *
 * @property integer $id
 * @property integer $id_user_fk
 * @property integer $id_request_fk
 * @property string $lastname
 * @property string $firstname
 * @property string $pesel
 * @property string $evidence_number
 * @property string $address
 * @property string $address_for_correspondence
 * @property string $phone
 * @property string $phone_mobile
 * @property string $email
 * @property integer $id_marital_status_fk
 * @property integer $number_of_members
 * @property integer $own_car
 * @property integer $employment_status
 * @property integer $employment_period
 * @property string $employer_name
 * @property string $employer_custom
 * @property string $bank_name
 * @property string $bank_account_number
 * @property integer $bank_how_long_use
 * @property integer $frequency_payment
 * @property integer $use_credit_card
 * @property double $monthly_income
 * @property double $monthly_expenses
 * @property double $monthly_other_loans
 * @property string $position
 * @property string $description
 * @property string $custom_data
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class AplCustomer extends \yii\db\ActiveRecord
{
    
    public $repeat_email;
    public $password;
    public $repeat_password;
    
    /*public $address_postalcode;
    public $address_city;*/
    public $address_street;
    public $address_street_number;
    public $address_local_number;
    public $address_status;
    
    public $c_address;
    public $c_address_postalcode;
    public $c_address_city;
    public $c_address_street;
    public $c_address_street_number;
    public $c_address_local_number;
    
    public $loan_purpose;
    public $loan_day_of_payment;
    
    public $car_type;
    public $car_year;
    public $car_insurance;
    
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_BASIC = 'basic';
	const SCENARIO_CONTACT = 'contact';
    const SCENARIO_FINANCE = 'finance';
    const SCENARIO_ACCEPT = 'accept';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_ACCOUNT = 'account';
    
    public $user_action;
    
    public $rules;
    public $accepts_1;
    public $accepts_2;
    public $accepts_3;
    
    public $c_account;
    public $c_send;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apl_customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_fk', 'id_request_fk', 'id_marital_status_fk', 'number_of_members', 'own_car', 'employment_status', 'bank_how_long_use', 'frequency_payment', 'use_credit_card', 'status', 'created_by', 'updated_by', 'deleted_by', 'loan_day_of_payment'], 'integer'],
            [['lastname', 'firstname', 'email', 'pesel', 'evidence_number', 'id_marital_status_fk', 'number_of_members', 'own_car', 'employment_status', 'employer_name', 'employment_period', 'password', 'repeat_password', 'repeat_email'], 'required', 'on' => self::SCENARIO_REGISTER],
            [['lastname', 'firstname'], 'required', 'on' => self::SCENARIO_CREATE], 
            [['email'], 'required', 'when' => function($model) { return ($model->c_account); }, 'on' => self::SCENARIO_CREATE],   
            [['email'], 'required', 'on' => self::SCENARIO_ACCOUNT],          
            [['car_type', 'car_year', 'car_insurance'], 'required', 'when' => function($model) { return ($model->own_car); }, 'on' => [self::SCENARIO_REGISTER, self::SCENARIO_BASIC, self::SCENARIO_UPDATE]], 
            [['address_postalcode', 'address_city', 'address_street', 'address_street_number', 'address_status'], 'required', 'on' => [self::SCENARIO_CONTACT, self::SCENARIO_UPDATE]], 
            [['c_address_postalcode', 'c_address_city', 'c_address_street', 'c_address_street_number'], 'required', 'when' => function($model) { return ($model->c_address); }, 'on' => [self::SCENARIO_CONTACT, self::SCENARIO_UPDATE]], 
            [['monthly_income', 'monthly_expenses', 'monthly_other_loans', 'bank_name', 'bank_account_number', 'bank_how_long_use', 'frequency_payment', 'use_credit_card'], 'required', 'on' => [self::SCENARIO_FINANCE, self::SCENARIO_UPDATE]],  
            [['loan_purpose', 'loan_day_of_payment'], 'required', 'when' => function($model) { return ($model->user_action != 'finance'); }, 'on' => self::SCENARIO_FINANCE],          
           // [['repeat_email', 'password', 'repeat_password'], 'required'],
            [['address', 'address_for_correspondence', 'employer_custom', 'description', 'custom_data', 'car_type', 'car_year', 'car_insurance', 'loan_purpose'], 'string'],
            [['monthly_income', 'monthly_expenses', 'monthly_other_loans'], 'number'],
            [['created_at', 'updated_at', 'deleted_at', 'password', 'info_car', 'accepts_1', 'accepts_2', 'accepts_3', 'c_send', 'c_address', 'c_account'], 'safe'],
            [['lastname', 'firstname'], 'string', 'max' => 300],
            [[/*'pesel', 'evidence_number', */'phone', 'phone_mobile'], 'string', 'max' => 20],
			/////['username', 'match', 'pattern' => '/[a-zA-Z0-9_-]+/', 'message' => 'Your username can only contain alphanumeric characters, underscores and dashes.'],
			['pesel', 'match', 'pattern' => '/^[0-9]{11}+$/', 'message' => 'Proszę podać odpowiednią wartość pesel. Jedenastocyfrowy numer identyfikacyjny. Wpisz go bez spacji.'],
			['evidence_number', 'match', 'pattern' => '/^[a-zA-Z]{3}[0-9]{6}+$/', 'message' => 'Proszę podać odpowiednią wartość numeru dowodu. Dziewięciocyfrowy numer Twojego dowodu osobistego. Wprowadź go bez spacji.'],
			[['email', 'position'], 'string', 'max' => 100],
            [['employer_name', 'bank_name', 'bank_account_number'], 'string', 'max' => 1000],
            [['repeat_password'], 'compare', 'compareAttribute' => 'password', 'message' => 'Błąd w powtórzeniu hasła', 'on' => self::SCENARIO_REGISTER],
            [['repeat_email'], 'compare', 'compareAttribute' => 'email', 'message' => 'Błąd w powtórzeniu adresu e-mail', 'on' => self::SCENARIO_REGISTER],
            ['email','email'],
            [['pesel', 'email'],'unique'],
            ['email', 'compareEmail', 'on' => self::SCENARIO_REGISTER ],
            ['accepts_1', 'validateRegister', 'on' => self::SCENARIO_REGISTER, 'skipOnEmpty' => false],
            ['accepts_2', 'validateFinance', 'when' => function($model) { return ($model->user_action != 'finance'); }, 'on' => self::SCENARIO_FINANCE, 'skipOnEmpty' => false],
            ['accepts_3', 'validateAccept', 'on' => self::SCENARIO_ACCEPT, 'skipOnEmpty' => false],
        ];
    }
    
    public function compareEmail($attribute, $params)  {
		$exist = \common\models\User::find()->where(['email' => $this->email])->all();
               
        if(count($exist) > 0) {
            $this->addError($attribute, 'Podany adres e-mail został już wykorzystany.');
        }
    }

    public function validateRegister($attribute, $params)  {

        if(!$this->accepts_1) {
            $this->addError($attribute, 'Prosimy o akceptację warunków');
        } else {   
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 1])->all();
            foreach($accepts as $key => $value) {
                if(!in_array($value->id, $this->accepts_1))
                    $this->addError($attribute, 'Prosimy o akceptację warunków');
            }
        }
    }
    
    public function validateFinance($attribute, $params)  {

        if(!$this->accepts_2) {
            $this->addError($attribute, 'Prosimy o akceptację warunków');
        } else {   
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 3])->all();
            foreach($accepts as $key => $value) {
                if(!in_array($value->id, $this->accepts_2))
                    $this->addError($attribute, 'Prosimy o akceptację warunków');
            }
        }
    }
    
    public function validateAccept($attribute, $params)  {

        if(!$this->accepts_3) {
            $this->addError($attribute, 'Prosimy o akceptację warunków');
        } else {   
            $accepts = \common\models\Custom::find()->where(['status' => 1, 'type_fk' => 4])->all();
            foreach($accepts as $key => $value) {
                if(!in_array($value->id, $this->accepts_3))
                    $this->addError($attribute, 'Prosimy o akceptację warunków');
            }
        }
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
		$scenarios['default'] = ['firstname', 'lastname', 'email'];
        $scenarios[self::SCENARIO_BASIC] = ['user_action', 'firstname', 'lastname', 'pesel', 'evidence_number', 'id_marital_status_fk', 'number_of_members', 'employment_status', 'employment_period', 'employer_name', 'own_car', 'info_car', 'car_type', 'car_year', 'car_insurance'];        
        $scenarios[self::SCENARIO_REGISTER] = ['email', 'firstname', 'lastname', 'pesel', 'evidence_number', 'id_marital_status_fk', 'number_of_members', 'employment_status', 'employment_period', 'employer_name', 
                                               'own_car', 'info_car', 'car_type', 'car_year', 'car_insurance', 'password', 'repeat_password', 'repeat_email', 'accepts_1'];
        $scenarios[self::SCENARIO_CONTACT] = ['c_address', 'phone', 'phone_mobile', 'address_city', 'address_postalcode', 'address_street', 'address_street_number', 'address_local_number', 'address_status',
                                              'c_address_city', 'c_address_postalcode', 'c_address_street', 'c_address_street_number', 'c_address_local_number'];
        $scenarios[self::SCENARIO_FINANCE] = ['monthly_income', 'monthly_expenses', 'monthly_other_loans', 'loan_purpose', 'loan_day_of_payment', 'bank_name', 'bank_account_number', 'bank_how_long_use', 'frequency_payment', 'use_credit_card', 'accepts_2'];
        $scenarios[self::SCENARIO_ACCEPT] = ['accepts_3'];
        $scenarios[self::SCENARIO_UPDATE] = ['user_action', 'firstname', 'lastname', 'pesel', 'evidence_number', 'id_marital_status_fk', 'number_of_members', 'employment_status', 'employment_period', 'employer_name', 
                                             'own_car', 'info_car', 'car_type', 'car_year', 'car_insurance', 'address_city', 'address_postalcode', 'address_street', 'address_street_number', 'address_local_number', 'address_status',
                                              'c_address', 'c_address_city', 'c_address_postalcode', 'c_address_street', 'c_address_street_number', 'c_address_local_number',
                                              'monthly_income', 'monthly_expenses', 'monthly_other_loans', 'loan_purpose', 'loan_day_of_payment', 'bank_name', 'bank_account_number', 'bank_how_long_use', 'frequency_payment', 'use_credit_card'];        
        $scenarios[self::SCENARIO_CREATE] = ['c_account', 'c_send', 'email', 'firstname', 'lastname', 'pesel', 'evidence_number', 'id_marital_status_fk', 'number_of_members', 'employment_status', 'employment_period', 'employer_name', 
                                             'own_car', 'info_car', 'car_type', 'car_year', 'car_insurance', 'address_city', 'address_postalcode', 'address_street', 'address_street_number', 'address_local_number', 'address_status',
                                              'c_address_city', 'c_address_postalcode', 'c_address_street', 'c_address_street_number', 'c_address_local_number',
                                              'monthly_income', 'monthly_expenses', 'monthly_other_loans', 'loan_purpose', 'loan_day_of_payment', 'bank_name', 'bank_account_number', 'bank_how_long_use', 'frequency_payment', 'use_credit_card'];    
        $scenarios[self::SCENARIO_ACCOUNT] = ['email'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user_fk' => Yii::t('app', 'Id User Fk'),
            'id_request_fk' => Yii::t('app', 'Id Request Fk'),
            'lastname' => Yii::t('app', 'Nazwisko'),
            'firstname' => Yii::t('app', 'Imię'),
            'pesel' => Yii::t('app', 'Pesel'),
            'evidence_number' => Yii::t('app', 'Numer dowodu'),
            'address' => Yii::t('app', 'Address'),
            'address_for_correspondence' => Yii::t('app', 'Address For Correspondence'),
            'phone' => Yii::t('app', 'Telefon stacjonarny'),
            'phone_mobile' => Yii::t('app', 'Telefon komórkowy'),
            'email' => Yii::t('app', 'Email'),
            'id_marital_status_fk' => Yii::t('app', 'Stan cywilny'),
            'number_of_members' => Yii::t('app', 'Liczba osób na utrzymaniu'),
            'own_car' => Yii::t('app', 'Własny samochód'),
            'car_type' => 'Marka',
            'car_year' => 'Rocznik',
            'car_insurance' => 'Data końca ubezpieczenia',
            'employment_status' => Yii::t('app', 'Status zatrudnienia'),
            'employment_period' => Yii::t('app', 'Okres zatrudnienia'),
            'employer_name' => Yii::t('app', 'Nazwa pracodawcy'),
            'employer_custom' => Yii::t('app', 'Employer Custom'),
            'bank_name' => Yii::t('app', 'Nazwa banku'),
            'bank_account_number' => Yii::t('app', 'Numer rachunku bankowego'),
            'bank_how_long_use' => Yii::t('app', 'Od jak dawna jesteś klientem banku'),
            'frequency_payment' => Yii::t('app', 'Częstotliwość wynagrodzenia'),
            'use_credit_card' => Yii::t('app', 'Czy posiadasz kartę kredytową'),
            'monthly_income' => Yii::t('app', 'Całkowity miesięczny dochód netto'),
            'monthly_expenses' => Yii::t('app', 'Calkowite miesięczne wydatki'),
            'monthly_other_loans' => Yii::t('app', 'Miesięczne obciążenia z tytułu innych pożyczek'),
            'position' => Yii::t('app', 'Position'),
            'description' => Yii::t('app', 'Description'),
            'custom_data' => Yii::t('app', 'Custom Data'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'password' => 'Hasło',
            'repeat_password' => 'Powtórz hasło',
            'repeat_email' => 'Powtórz email',
            'address_postalcode' => 'Kod pocztowy',
            'address_city' => 'Miasto',
            'address_street' => 'Ulica',
            'address_street_number' => 'Numer',
            'address_local_number' => 'Lokal',
            'address_status' => 'Status nieruchomości',
            'c_address' => 'Osobny adres korespondencyjny',
            'c_address_postalcode' => 'Kod pocztowy',
            'c_address_city' => 'Miasto',
            'c_address_street' => 'Ulica',
            'c_address_street_number' => 'Numer',
            'c_address_local_number' => 'Lokal',
            'loan_purpose' => 'Cel pożyczki',
            'loan_day_of_payment' => 'Dzień płatnści raty',
            'c_account' => 'Utwórz konto klienta',
            'c_send' => 'Wyślij informację do klienta'
        ];
    }
    
    public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {			
			if($this->isNewRecord ) {
            
                if($this->own_car) {
                    $this->info_car = \yii\helpers\Json::encode(['type' => $this->car_type, 'year' => $this->car_year, 'insurance' => $this->car_insurance]);
                }
				if($this->scenario == 'register') {
                    $user = new \common\models\User();
                    $user->generateAuthKey();
                    $user->status = 10;
                    $user->email = $this->email;
                    $user->username = $this->email;
                    $user->roleType = 'client';
                    $user->firstname = $this->firstname;
                    $user->lastname = $this->lastname;
                    //$this->password = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
                    $user->setPassword($this->password);
                    
                    if($user->save()) { 
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('client');
                        $auth->assign($role, $user->id);
                        
                        $this->id_user_fk = $user->id;
                        $this->created_by = \Yii::$app->user->id;
                        try {
                            \Yii::$app->mailer->compose(['html' => 'clientRegistration-html', 'text' => 'clientRegistration-text'], ['login' => $user->username, 'password' => $this->password, 'username' => ($this->firstname.' '.$this->lastname) ])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                            ->setTo($this->email)
                            ->setBcc(\Yii::$app->params['bccEmail'])
                            ->setSubject('Potwierdzenie rejestracji w serwisie GTW Finanse')
                            ->send();
                        } catch (\Swift_TransportException $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                            $request = Yii::$app->request;
                            /*$log = new \common\models\Logs();
                            $log->id_user_fk = Yii::$app->user->id;
                            $log->action_date = date('Y-m-d H:i:s');
                            $log->action_name = 'resource-booking';
                            $log->action_describe = $e->getMessage();
                            $log->request_remote_addr = $request->getUserIP();
                            $log->request_user_agent = $request->getUserAgent(); 
                            $log->request_content_type = $request->getContentType(); 
                            $log->request_url = $request->getUrl();
                            $log->save();*/
                        }
                    } else {
                        //var_dump($user->getErrors()); exit;
                        return false;
                    }
                    
                    $this->created_by = \Yii::$app->user->id;
                } else {
                    $this->created_by = \Yii::$app->user->id;
                }
			} else { 
				if($this->user_action == 'contact' || $this->user_action == 'register_contact') {
                    $this->address = \yii\helpers\Json::encode(['postalcode' => $this->address_postalcode, 'city' => $this->address_city, 'street' => $this->address_street, 's_number' => $this->address_street_number, 'l_number' => $this->address_local_number, 'status' => $this->address_status, 'correspondence' => $this->c_address]);
                    if($this->c_address) {
                        $this->address_for_correspondence = \yii\helpers\Json::encode(['postalcode' => $this->c_address_postalcode, 'city' => $this->c_address_city, 'street' => $this->c_address_street, 's_number' => $this->c_address_street_number, 'l_number' => $this->c_address_local_number, 'status' => $this->address_status, 'correspondence' => 'yes']);                
                    }
                }
                
                if($this->user_action == 'basic') {
                    if($this->own_car) {
                        $this->info_car = \yii\helpers\Json::encode(['type' => $this->car_type, 'year' => $this->car_year, 'insurance' => $this->car_insurance]);
                    }
                }
                if($this->user_action == 'basic' || $this->user_action == 'contact' || $this->user_action == 'finance') {
                    $modelArch = new \backend\Modules\Apl\models\AplCustomerArch();
                    $modelArch->table_fk = 1;
                    $modelArch->id_root_fk = $this->id;
                    $modelArch->user_action = $this->user_action;
                    $modelArch->data_arch = \yii\helpers\Json::encode($this);
                    $modelArch->created_by = \Yii::$app->user->id;
                    $modelArch->created_at = new Expression('NOW()');
                    $modelArch->save();
                }
			}
			return true;
		} else { 
			
			return false;
		}
		return false;
	}

	
	public function behaviors()	{
		return [
			"timestamp" =>  [
                'class' => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => 'updated_at',
				'value' => new Expression('NOW()'),
            ],
			
		];
	}
    
    public static function statusMarital() {
        $array = [];
        $array[1] = 'żonaty / mężatka';
        $array[2] = 'rozwiedziony / rozwiedziona';
        $array[3] = 'kawaler / panna';
        $array[4] = 'wdowiec / wdowa';
        $array[5] = 'rozwodnik / rozwódka';
        $array[6] = 'związek nieformalny';
        
        return $array;
    }
    
    public static function statusEmployment() {
        $array = [];
        $array[1] = 'pełny etat';
        $array[2] = 'niepełny etat';
        $array[3] = 'rencista';
        $array[4] = 'emeryt';
        $array[5] = 'służby mundurowe';
        $array[6] = 'działalność gospodarcza';
        $array[7] = 'rolnik inne';
        
        return $array;
    }
    
    public static function statusAddress(){
        $array = [];
        
        $array[1] = 'właściciel';
        $array[2] = 'współwłaściciel';
        $array[3] = 'mieszka z rodzicami';
        $array[4] = 'najemca';
        $array[5] = 'stancja studencka';
        $array[6] = 'inne (kwaterunkowe, spółdzielcze)';
        
        return $array;
    }
    
    public static function periodBank(){
        $array = [];
        
        $array[1] = 'mniej niż rok';
        $array[2] = 'od roku do 3 lat';
        $array[3] = 'powyżej 3 lat';
        
        return $array;
    }
    
    public function getFullname() {
        return $this->lastname.' '.$this->firstname;
    }
    
    public function getAccount() {
        return ($this->id_user_fk) ? \common\models\User::findOne($this->id_user_fk) : false;
    }
    
    public function getAddresses() {
        $addresses = [];
        
        $address = \yii\helpers\Json::decode($this->address);
        $addresses['normal']['address_postalcode'] = $address['postalcode'];
        $addresses['normal']['address_city'] = $address['city'];
        $addresses['normal']['address_street'] = $address['street'];
        $addresses['normal']['address_street_number'] = $address['s_number'];
        $addresses['normal']['address_local_number'] = $address['l_number'];
        $addresses['normal']['address_status'] = $address['status'];
        $addresses['normal']['correspondence'] = $address['correspondence'];
        
        if($this->address_for_correspondence) {
            $address_for_correspondence = \yii\helpers\Json::decode($this->address_for_correspondence);
            $addresses['correspondence']['address_postalcode'] = $address_for_correspondence['postalcode'];
            $addresses['correspondence']['address_city'] = $address_for_correspondence['city'];
            $addresses['correspondence']['address_street'] = $address_for_correspondence['street'];
            $addresses['correspondence']['address_street_number'] = $address_for_correspondence['s_number'];
            $addresses['correspondence']['address_local_number'] = $address_for_correspondence['l_number'];
        } else {
            $addresses['correspondence'] = false;
        }
        
        $addresses['full'] = $address['postalcode'].' '.$address['city'].', '.$address['street'].' '.$address['s_number'].' lok. '.$address['l_number'];
        
        return $addresses;
    }
    
    public function getCar() {
        $carInfo = [];
        
        if($this->own_car) {
            $carData = \yii\helpers\Json::decode($this->info_car);
            $carInfo['type'] = $carData['type'];
            $carInfo['year'] = $carData['year'];
            $carInfo['insurance'] = $carData['insurance'];
        }
        
        return $carInfo;
    }
    
    public static function getStats($id) {
        //$customer = \backend\Modules\Apl\AplCustomer::findOne($id);
        $stats = [];
        $query = (new \yii\db\Query())
            ->select([ "(select concat_ws(',',sum(case when status = 3 then 1 else 0 end),sum(case when status = -2 then 1 else 0 end),sum(case when status = 1 then 1 else 0 end)) from gtw.gtw_apl_loan where id_customer_fk=c.id) as loans"])
            ->from('{{%apl_customer}} as c')
            ->where( ['c.id' => $this->id] );           
        $statsData = $query->one();
        
        $loans = explode(',', $statsData['loans']);
        $stats['loan_accept'] = ( (isset($loans[0]) && !empty($loans[0])) ? $loans[0] : 0);
        $stats['loan_unaccept'] = ( (isset($loans[1]) && !empty($loans[1])) ? $loans[1] : 0);
        $stats['loan_wait'] = ( (isset($loans[2]) && !empty($loans[2])) ? $loans[2] : 0);
            
        return $stats;
    }
    
    public static function getUsers() {
        return AplCustomer::find()->where('status >= 1')->orderby('lastname collate `utf8_polish_ci`')->all();
    }
    
    public function getFiles() {
        $filesData = [];
        if(!$this->isNewRecord)
        $filesData = \common\models\Files::find()->where(['status' => 1, 'id_fk' => $this->id, 'id_type_file_fk' => 2])->orderby('id desc')->all();
        return $filesData;
    }
    
    public static function statusList() {
        $result = [];
        
        $result['names'] = ['-3' => 'usunięty', '-2' => 'zablokowany', '0' => 'niepotwierdzony', '1' => '2 krok', '2' => '3 krok', '3' => 'potwierdzony', '4' => 'BOK'];
        $result['colors'] = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'warning', '1' => 'info', '2' => 'info', '3' => 'success', '4' => 'success'];
        $result['colors2'] = ['-3' => 'red', '-2' => 'red', '-1' => 'yellow', '0' => 'yellow', '1' => 'yellow', '2' => 'blue', '3' => 'green', '4' => 'green'];
        
        return $result;
    }
    
    public static function statusShortList() {
        $result = [];
        
        $result['names'] = ['-3' => 'usunięty', '-2' => 'zablokowany', '0' => 'w trakcie rejestracji', '3' => 'aktywny'];
        
        return $result;
    }
    
    public static function getList() {
        return AplCustomer::find()->where('status >= 3')->orderby('lastname', 'firstname')->all();
    }
}
