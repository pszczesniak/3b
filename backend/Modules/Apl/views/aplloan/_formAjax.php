<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Structurewidget;
use backend\widgets\GoogleMapWidget;
use common\components\CustomHelpers;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false,
                                'options' => ['class' => 'modalAjaxForm', 'data-target' => "#modal-grid-item", 'data-table' => "#table-loans"]]); ?>
<div class="modal-body calendar-task">
    <?php /*($model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : '';*/ ?>
    <!--<span class="text--purple">Data złożenia: <b><?= $model->created_at ?></b></span>-->
    <fieldset><legend>Parametry pożyczki</legend>
        <div class="row">
            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'l_amount')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'l_period')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'l_day_of_payment')->dropdownList(\backend\Modules\Apl\models\AplLoan::listDayPayments(), ['prompt' => '-- wybierz --']); ?></div>
        </div>
    </fieldset>
    <div class="grid">
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Klient</legend>
                <?= $form->field($model, 'id_customer_fk')->dropdownList(ArrayHelper::map(\backend\Modules\Apl\models\AplCustomer::getList(), 'id', 'fullname'), ['prompt' => '-- wybierz --'])->label(false); ?>
            </fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Cel pożyczki</legend>
                <?= $form->field($model, 'id_purpose_fk')->dropdownList(\backend\Modules\Apl\models\AplLoan::listPurposes(), ['prompt' => '-- wybierz --'])->label(false); ?>
            </fieldset>
        </div>
    </div>
 
    <div class="modal-footer"> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-sm btn-default" type="button" >Odrzuć</button>
    </div>
</div>
<?php ActiveForm::end(); ?>

