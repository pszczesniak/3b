<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Svc\models\SvcEvent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-apl-loans-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-loans']
    ]); ?>
    
    <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'id_customer_fk')->dropDownList( ArrayHelper::map(\backend\Modules\Apl\models\AplCustomer::getList(), 'id', 'fullname'), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-loans', 'data-form' => '#filter-svc-loans-search' ] ) ?></div>        
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'status')->dropDownList( \backend\Modules\Apl\models\AplLoan::statusList()['names'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-loans', 'data-form' => '#filter-svc-loans-search' ] ) ?></div>        
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'id_purpose_fk')->dropDownList( \backend\Modules\Apl\models\AplLoan::listPurposes(), ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-loans', 'data-form' => '#filter-svc-loans-search' ] ) ?></div>        
    
    </div> 
 
    <!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

</div>
