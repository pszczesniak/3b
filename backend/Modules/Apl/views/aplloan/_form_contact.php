<div class="pd-agent-inq">
    <h4 class="box-title">Wiadomość do klienta</h4>
    <form class="form-horizontal form-agent-inq">
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Temat"> </div>
        </div>
        <!--<div class="form-group">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Phone"> </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input type="email" class="form-control" placeholder="E-Mail"> </div>
        </div>-->
        <div class="form-group">
            <div class="col-md-12">
                <textarea class="form-control" rows="3" placeholder="Wiadomość"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info btn-rounded pull-right">Wyslij wiadomość</button>
            </div>
        </div>
    </form>
</div>