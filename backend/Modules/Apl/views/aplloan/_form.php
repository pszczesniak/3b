<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use backend\widgets\Structurewidget;
use backend\widgets\GoogleMapWidget;
use common\components\CustomHelpers;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= ($model->getErrors()) ? '<div class="alert alert-danger">'.$form->errorSummary($model).'</div>' : ''; ?>
    <?php if(isset($errors) && !empty($errors) ) { ?>
        <div class="alert alert-danger"> <ul>
            <?php foreach($errors as $key => $value) { echo '<li>'.$value[0].'</li>'; } ?>
        </ul></div>
    <?php } ?>
    <span class="text--purple">Data złożenia: <b><?= $model->created_at ?></b></span>
    <fieldset><legend>Parametry pożyczki</legend>
        <div class="row">
            <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'l_amount')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'l_period')->textInput(['maxlength' => true]) ?></div>
            <div class="col-sm-4 col-xs-6"><?= $form->field($model, 'l_day_of_payment')->dropdownList(\backend\Modules\Apl\models\AplLoan::listDayPayments(), ['prompt' => '-- wybierz --']); ?></div>
        </div>
    </fieldset>
    <fieldset><legend>Cel pożyczki</legend>
        <?= $form->field($model, 'id_purpose_fk')->dropdownList(\backend\Modules\Apl\models\AplLoan::listPurposes(), ['prompt' => '-- wybierz --'])->label(false); ?>
    </fieldset>
    

    <div class="form-group align-right">
        <?=  Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])  ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


