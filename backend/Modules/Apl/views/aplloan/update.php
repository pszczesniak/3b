<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Aktualizacja';
$this->params['breadcrumbs'][] = ['label' => 'Rejestr wniosków', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '#'.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Aktualizacja';
?>

    
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>Html::encode($this->title))) ?>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            <?= $this->endContent(); ?>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>'Klient')) ?>    
                <?= $this->render('_customer_info', ['model' => $model->customer]) ?>
            <?= $this->endContent(); ?>
        </div>
    </div>


