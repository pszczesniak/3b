<div class="white-box">
    <!--<h3 class="box-title">Browser Stats</h3>-->
    <ul class="basic-list">
        <li>Całkowity koszt <span class="pull-right label-danger label"><?= number_format($model->l_amount_total, 2,  '.', ' ') ?></span></li>
        <li>Wysokość raty <span class="pull-right label-warning label"><?= number_format($model->l_monthly_rate, 2,  '.', ' ') ?></span></li>
        <li>Okres kredytowania <span class="pull-right label-info label"><?= date('d.m.Y', strtotime($model->repayment_term)) ?></span></li>
        <li>Opłaty [<small>prowizja <b class="text--purple"><?= $model->l_provision ?>%</b></small>] <span class="pull-right label-purple label"><?= number_format($model->l_provision_amount, 2,  '.', ' ') ?></span></li>
        <li>Oprocentowanie <span class="pull-right label-purple label">10%</span></li>
        <li>RRSO <span class="pull-right label-primary label"><?= $model->l_rrso ?>%</span></li>
    </ul>
</div>