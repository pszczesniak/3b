<div class="comment" id="comment-<?= $model->id ?>">
    <img src="/images/default-user.png" alt="" class="comment-avatar">
    <div class="comment-body">
        <div class="comment-text">
            <div class="comment-header">
                <a href="#" title="">Piotr Szczęśniak</a><span><?= $model->created_at ?></span>
            </div>
            <?= $model->info_content ?>
        </div>
        <div class="comment-footer">
            <!--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>
            <a href="#"><i class="fa fa-thumbs-o-down"></i></a>-->
            <a href="<?= \yii\helpers\Url::to(['/apl/aplloan/note', 'parent' => $model->id]) ?>" class="gridViewModal" data-target="#modal-grid-item" data-title="Odpowiedź na wiadomość" data-icon="envelope">Odpowiedz</a> 
        </div>
    </div>
    <?php 
        if($model->children) {
            foreach($model->children as $key => $child) {
                echo $this->render('_note', ['model' => $child]);
                //echo $child->id.'<br />';
            }
        }
    ?>
</div>