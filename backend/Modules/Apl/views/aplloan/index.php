<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\Svcloansearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wnioski');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-index", 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-loans" class="btn-group toolbar-table-widget">
        <?=  Html::a('<i class="fa fa-plus"></i>Dodaj', Url::to(['/apl/aplloan/create', 'id' => 0]) , 
                        ['class' => 'btn btn-success btn-icon gridViewModal', 
                         'id' => 'case-create',
                         //'data-toggle' => ($gridViewModal)?"modal":"none", 
                         'data-target' => "#modal-grid-item", 
                         'data-form' => "item-form", 
                         'data-table' => "table-items",
                         'data-title' => "Dodaj"
                        ])  ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-loans"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-loans"
            data-toolbar="#toolbar-loans" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-apl-loans-search"
            data-url=<?= Url::to(['/apl/aplloan/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="l_amount" data-sortable="true" data-align="right">Kwota</th>
                <th data-field="l_period" data-sortable="true" data-width="80px" data-align="center">Okres</th>
                <th data-field="customer" data-sortable="true">Klient</th>                    
                <th data-field="status" data-sortable="true" data-align="center">Status</th>
                <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
    </table>   
<?= $this->endContent(); ?>