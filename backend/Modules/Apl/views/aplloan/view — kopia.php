<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Wniosek #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rejestr ofert', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*$imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id.'-big.jpg':'/images/no-image-big.jpg';
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->companyname]);
\Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => ($model->brief) ? $model->brief : trim(preg_replace('/\s\s+/', ' ', $model->note)) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:og_url', 'content' => \Yii::$app->urlManagerFrontEnd->createUrl('oferta/'.\common\components\CustomHelpers::encode($model->id).'/'.$model->slug) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://twojeprzyjecia.pl'.$imgSrc ]);*/

?>
<?php if($model->status == -1) { ?>
    <div class="alert alert-danger">Wniosek została usnięta</div>
<?php }  else { ?>
    <br />
    <p class="align-right padding-right-10 panel-actions">
        <?= ($model->status == 1) ? Html::a(Yii::t('app', 'Zaakceptuj wniosek'), ['accept', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
        <?= ($model->status == 1) ? Html::a(Yii::t('app', 'Odrzuć wniosek'), ['discard', 'id' => $model->id], ['class' => 'btn btn-danger deleteConfirmWithComment', 'data-label' => 'Odrzuć wniosek']) : '' ?>
        <?= ($model->status == 0) ? '<span class="btn btn-info">Wniosek niepotwierdzony przez klienta</span>' : '' ?>
        <?= ($model->status == 2) ? '<span class="btn btn-success">Zaakceptowany</span>' : '' ?>
        <?= ($model->status == -2) ? '<span class="btn btn-danger">Odrzucony</span>' : '' ?>
        <!--<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>-->
    </p>
<?php } ?>
<?= Alert::widget() ?>
<div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>Html::encode($this->title))) ?>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        <?= $this->endContent(); ?>
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>'Szczegóły pożyczki')) ?>
            <?= $this->render('_loan_details', [
                'model' => $model,
            ]) ?>
        <?= $this->endContent(); ?>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>'Klient')) ?>    
            <?= $this->render('_customer_info', ['model' => $model->customer]) ?>
        <?= $this->endContent(); ?>
    </div>
</div>
