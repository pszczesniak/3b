<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="padding-md">
    <h4><?= $model->customer['fullname'] ?></h4>
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <div class="row">
                <div class="col-xs-6 col-sm-12 col-md-6 text-center">
                    <a href="#">
                        <img class="img-thumbnail" alt="User Avatar" src="<?= (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->customer['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$model->customer['id_user_fk'].".png" : "/../images/default-user.png" ?>">
                    </a>
                    <div class="seperator"></div>
                    <div class="seperator"></div>
                </div><!-- /.col -->
                <div class="col-xs-6 col-sm-12 col-md-6">
                    <?= Html::a('<i class="fa fa-pencil"></i>'.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-primary']) ?>
                    <div class="seperator"></div>
                    <span class="btn btn-info btn-xs btn-icon m-bottom-sm"><i class="fa fa-phone" aria-hidden="true"></i><?= (($model->customer['phone']) ? $model->customer['phone'] : 'brak numeru') ?></span>
                    <div class="seperator"></div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.col -->
        <div class="col-md-9 col-sm-9">
           
          
        </div><!-- /.col -->
    </div><!-- /.row -->			
</div>