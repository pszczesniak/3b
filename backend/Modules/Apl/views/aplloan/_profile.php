<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<div class="padding-md">
    <div class="card-profile card-profile__green">
        <div class="profile-header">
            <h4>potwierdzony</h4>
            <?php
                if(!$model->id_user_fk) $idUser = 0; else $idUser = $model->id_user_fk;
                $avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$idUser.".png")) ? "/uploads/avatars/thumb/avatar-".$idUser.".png" : "/images/default-user.png";
            ?>
            <span class="profile-header__avatar"><img src="<?= \Yii::$app->params['frontend'].$avatar ?>" alt=""></span>
        </div>
        <div class="profile-body">
            <h4><?= $model->fullname ?></h4>
            <div class="card-block">
                <?= Html::a('<i class="fa fa-user"></i>Edytuj', Url::to(['/apl/aplcustomer/update', 'id' => $model->id]), ['class' => 'btn btn-icon btn-primary']) ?>
                <?= Html::a('<i class="fa fa-lock"></i>'.'Zablokuj', [Url::to(['/apl/aplcustomer/delete', 'id' => $model->id]), 'id' => $model->id], ['class' => 'btn btn-icon btn-danger']) ?>
            </div>
            <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.  </p>-->
            <ul class="profile-contact">
                <li>
                    <div class="media small-teaser">
                        <div class="media-left media-middle">
                            <div class="teaser_icon label-success round">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>
                        <div class="media-body media-middle">
                            <strong>Email: </strong><?= ($model->email) ? $model->email : 'brak danych' ?>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media small-teaser">
                        <div class="media-left media-middle">
                            <div class="teaser_icon label-success round">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div>
                        <div class="media-body media-middle">
                            <strong>Telefon stacjonarny: </strong><?= ($model->phone) ? $model->phone : 'brak danych' ?>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media small-teaser">
                        <div class="media-left media-middle">
                            <div class="teaser_icon label-success round">
                                <i class="fa fa-mobile"></i>
                            </div>
                        </div>
                        <div class="media-body media-middle">
                            <strong>Telefon stacjonarny: </strong><?= ($model->phone_mobile) ? $model->phone_mobile : 'brak danych' ?>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media small-teaser">
                        <div class="media-left media-middle">
                            <div class="teaser_icon label-success round">
                                <i class="fa fa-address-card"></i>
                            </div>
                        </div>
                        <div class="media-body media-middle">
                            <strong>PESEL: </strong><?= ($model->pesel) ? $model->pesel : 'brak danych' ?>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media small-teaser">
                        <div class="media-left media-middle">
                            <div class="teaser_icon label-success round">
                                <i class="fa fa-address-card"></i>
                            </div>
                        </div>
                        <div class="media-body media-middle">
                            <strong>NUMER DOWODU: </strong><?= ($model->evidence_number) ? $model->evidence_number : 'brak danych' ?>
                        </div>
                    </div>
                </li>
            </ul>
            <!--<a href="" class="btn btn-primary btn-sm btn-block m-t-10">Follow</a>-->
        </div>
    </div>
</div>     