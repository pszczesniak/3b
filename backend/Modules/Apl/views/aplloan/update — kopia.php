<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Aktualizacja';
$this->params['breadcrumbs'][] = ['label' => 'Rejestr ofert', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->companyname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Aktualizacja';
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-update", 'title'=>Html::encode($this->title))) ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<?= $this->endContent(); ?>
