<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\widgets\Alert;

use backend\widgets\files\FilesBlock;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcOffer */

$this->title = 'Wniosek #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rejestr wniosków', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*$imgSrc = (file_exists(\Yii::getAlias('@webroot').'/uploads/avatars/avatar-'.$model->id.'-big.jpg'))?'/uploads/avatars/avatar-'.$model->id.'-big.jpg':'/images/no-image-big.jpg';
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->companyname]);
\Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => ($model->brief) ? $model->brief : trim(preg_replace('/\s\s+/', ' ', $model->note)) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:og_url', 'content' => \Yii::$app->urlManagerFrontEnd->createUrl('oferta/'.\common\components\CustomHelpers::encode($model->id).'/'.$model->slug) ]);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://twojeprzyjecia.pl'.$imgSrc ]);*/

?>
<?php if($model->status == -1) { ?>
    <div class="alert alert-danger">Wniosek została usnięta</div>
<?php }  else { ?>
    <br />
    <p class="align-right padding-right-10 panel-actions">
        <?= ($model->status == 1) ? Html::a(Yii::t('app', 'Zaakceptuj wniosek'), ['accept', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
        <?= ($model->status == 1) ? Html::a(Yii::t('app', 'Odrzuć wniosek'), ['discard', 'id' => $model->id], ['class' => 'btn btn-danger deleteConfirmWithComment', 'data-label' => 'Odrzuć wniosek']) : '' ?>
        <?= ($model->status == 0) ? '<span class="btn btn-info">Wniosek niepotwierdzony przez klienta</span>' : '' ?>
        <?= ($model->status == 2) ? '<span class="btn btn-success">Zaakceptowany</span>' : '' ?>
        <?= ($model->status == -2) ? '<span class="btn btn-danger">Odrzucony</span>' : '' ?>
        <!--<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>-->
    </p>
<?php } ?>
<?= Alert::widget() ?>
<div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">
        <?= $this->render('_profile', ['model' => $model->customer]) ?>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12"> 
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"apl-loan-update", 'title'=>Html::encode($this->title))) ?>
            <div class="tab-block">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-calculator"></i></span><span class="hidden-xs">Szczegóły</span></a></li>
                    <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-files-o"></i></span><span class="hidden-xs">Dokumenty</span></a></li>                    
                    <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-comments"></i></span><span class="hidden-xs">Chat</span></a></li>
                </ul>

                  <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="col-sm-6 col-xs-12">
                            <?= $this->render('_form', [ 'model' => $model, ]) ?>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?= $this->render('_loan_details', [ 'model' => $model,]) ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2"><?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => false]) ?></div>
                    <div role="tabpanel" class="tab-pane" id="tab3"> <?= $this->render('_chat', ['model' => $model, 'id' => $model->id, 'type' => 2]) ?></div>
                </div>
            </div>
            
            
        <?= $this->endContent(); ?>  
    </div>

</div>
<?php if(isset($_GET['info'])) { ?>
<script type="text/javascript">
document.onreadystatechange = function(){
    if (document.readyState === 'complete') {
        $("#modal-grid-item").find(".modal-title").html('<i class="fa fa-envelope"></i>Wiadomość');
	    $("#modal-grid-item").modal("show")
				  .find(".modalContent")
				  .load("<?= \yii\helpers\Url::to(['/apl/aplloan/note']) ?>?parent=<?= $_GET['info'] ?>");
    }
};
</script>
<?php } ?>