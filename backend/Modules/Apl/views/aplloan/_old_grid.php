<?= GridView::widget([
        'id' => 'grid-view-dicts',
        'dataProvider' => $dataProvider,
        'tableOptions'=>Yii::$app->params['tableOptions'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
               'class' => 'yii\grid\DataColumn',
               'headerOptions' => ['data-searchable' => 'true', 'data-field' => "user", 'data-searchable' => "true", 'data-filter-control' => "input", /*'data-sortable' => 'true'*/],
               'attribute' => 'companyname',
               /*'value' => function ($data) {
                    return $data->fullname;
                  },*/
            ],
            [
               'class' => 'yii\grid\DataColumn',
               'headerOptions' => ['data-searchable' => 'true', 'data-field' => "package", 'data-searchable' => "true", 'data-filter-control' => "select", /*'data-sortable' => 'true'*/],
               'attribute' => 'id_package_fk',
               'value' => function ($data) {
                    return $data->package['name'];
                  },
            ],
            [
               'class' => 'yii\grid\DataColumn',
               'headerOptions' => ['data-searchable' => 'true', 'data-field' => "package", 'data-searchable' => "true", 'data-filter-control' => "select", /*'data-sortable' => 'true'*/],
               'attribute' => 'confirm',
               'value' => function ($data) {
                    return $data->package['name'];
                  },
            ],
            'city',
            [
                'format' => 'image',
                'contentOptions' => ['class' => 'col-avatar'],
                'value'=>function($model) { 
                    return  (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? "/../uploads/avatars/thumb/avatar-".$model->id_user_fk.".png" : "/../images/default-user.png"; 
                },
            ],
            // 'city',
            // 'postal_code',
            // 'address',
            // 'pos_lat',
            // 'pos_lng',
            // 'companyname',
            // 'firstname',
            // 'lastname',
            // 'phone',
            // 'phone_additional:ntext',
            // 'email:email',
            // 'website_url:url',
            // 'facebook_url:url',
            // 'note:ntext',
            // 'first_logged_date',
            // 'last_logged_date',
            // 'facebook_config:ntext',
            // 'confirm',
            // 'confirm_payment',
            // 'status',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'deleted_at',
            // 'deleted_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'table-actions'],
                'template' => '{view}{update}{delete}',
                'headerOptions' => ['data-switchable'=>'false'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
                                'title' => Yii::t('app', 'View'), 'class' => 'btn btn-default btn-sm '
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
                                'title' => Yii::t('app', 'Update'), 'class' => 'btn btn-default btn-sm ', 'data-title' => '<i class="glyphicon glyphicon-pencil"></i>'.Yii::t('lsdd', 'Update')
                        ]);
                    },
                    'delete' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
								'title' => Yii::t('app', 'Delete'), 'class' => 'btn btn-default btn-sm deleteConfirm', 
								/*'data-confirm' => "Czy na pewno usun�� ten element?",*/ 'data-id' => $model->id
						]);
					}
                ],
            ],
        ],
    ]); ?>