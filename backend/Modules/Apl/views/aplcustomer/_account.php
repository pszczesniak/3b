<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"apl-customer-account", 'title'=>'Konto w systemie')) ?>
    <?php if($model->account) { ?>
        <ul class="list-group">
            <li class="list-group-item"><b>Login: </b><?= $user->username ?> </li>
            <li class="list-group-item"><b>E-mail: </b><?= $user->email ?> </li>
            <li class="list-group-item"><b>Imię: </b><?= $user->username ?> </li>
            <li class="list-group-item"><b>Nazwisko: </b><?= $user->username ?> </li>
        </ul>
    <?php } else { ?>
        <?php
            $user->email = $model->email;
            $user->username = $model->email;
            $user->firstname = $model->firstname;
            $user->lastname = $model->lastname;
            $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::to(['/apl/aplcustomer/account', 'id' => $model->id]), 'id' => 'client-account', 'options' => ['class' => 'ajaxform'],
                                'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false ])
        ?>
            <div class="row">
                <div class="col-xs-12"><?= $form->field($user, 'email')->textInput( ['maxlength' => 255] )  ?></div>
                <div class="col-xs-12"><?= $form->field($user, 'username')->textInput( ['maxlength' => 255] )  ?></div>
                <div class="col-md-6 col-sm-12 col-xs-6"><?= $form->field($user, 'firstname')->textInput( ['maxlength' => 255] )  ?></div>
                <div class="col-md-6 col-sm-12 col-xs-6"><?= $form->field($user, 'lastname')->textInput( ['maxlength' => 255] )  ?></div>
            </div>
            <?= $form->field($user, 'c_send')->checkBox()  ?>
            <div class="form-group align-right">
                <?= Html::submitButton('Utwórz konto', ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
            </div>
        <?php  ActiveForm::end() ?>
    <?php } ?>
<?= $this->endContent(); ?>