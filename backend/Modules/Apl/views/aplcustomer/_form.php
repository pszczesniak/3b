<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-client-form tab-block">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Dane podstawowe</span></a></li>
        <?php if(!$model->isNewRecord) { ?>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-address-card"></i></span> <span class="hidden-xs">Dane kontaktowe</span></a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-dollar"></i></span> <span class="hidden-xs">Dane finansowe</span></a></li>
        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-tasks"></i></span> <span class="hidden-xs">Wnioski</span></a></li>
        <?php } ?>
    </ul>

      <!-- Tab panes -->
   <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1"><?= $this->render('tabs/_tabBasic',array('model'=>$model, 'cform' => false)) ?></div>
        <?php if(!$model->isNewRecord) { ?>
        <div role="tabpanel" class="tab-pane" id="tab2"><?= $this->render('tabs/_tabContact',array('model'=>$model, 'cform' => false)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab3"><?= $this->render('tabs/_tabFinance',array('model'=>$model, 'cform' => false)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab4"><?= $this->render('tabs/_tabLoans',array('model'=>$model, 'cform' => false)) ?></div>
        <?php } ?>
    </div>
</div>