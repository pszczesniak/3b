<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;


//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

 <?php $form = (!$cform) ? ActiveForm::begin([ 'id' => 'customer-finance', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'],
                                               'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false ]) : $cform; ?>  
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Finanse</legend>
                <?= $form->field($model, 'monthly_income')->textInput( ['maxlength' => 255] )  ?>
                <?= $form->field($model, 'monthly_expenses')->textInput( ['maxlength' => 255] )  ?> 
                <?= $form->field($model, 'monthly_other_loans')->textInput( ['maxlength' => 255] )  ?> 
            </fieldset> 
        </div>
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Bank</legend>
                <?= $form->field($model, 'bank_name')->textInput( ['maxlength' => 255] )  ?>
                <?= $form->field($model, 'bank_account_number')->textInput( ['maxlength' => 255] )  ?>
                <?= $form->field($model, 'bank_how_long_use')->dropdownList(\backend\Modules\Apl\models\AplCustomer::periodBank(), ['prompt' => '-- wybierz --']); ?>
                <?= $form->field($model, 'use_credit_card')->radioList([1 => 'TAK', 0 => 'NIE'], 
                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                        },
                                ]) ?>
                <?= $form->field($model, 'frequency_payment')->radioList([1 => 'Tygodniowo', 2 => 'Miesięcznie'], 
                                    ['class' => 'btn-group', 'data-toggle' => "buttons",
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                            return '<label class="btn btn-sm btn-info' . ($checked ? ' active' : '') . '">' .
                                                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
                                        },
                                ]) ?>
            </fieldset>
        </div>
    </div>

    <?php if(!$cform) { ?>
    <div class="form-group align-right">
        <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_finance'])->label(false); ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>
    <?php } ?>
<?php if(!$cform) { ActiveForm::end(); } ?>	

	
	