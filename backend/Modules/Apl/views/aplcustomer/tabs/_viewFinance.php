<fieldset class="info-container"><legend>Finanse [miesięczne PLN]</legend>
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->monthly_income) ? number_format($model->monthly_income, 2, "," ,  " ") : 'brak informacji'; ?></p>
            <span class="label-view">Przychód netto </span>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->monthly_expenses) ? number_format($model->monthly_expenses, 2, "," ,  " ") : 'brak informacji'; ?></p>
            <span class="label-view">Wydatki</span>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->monthly_other_loans) ? number_format($model->monthly_other_loans, 2, "," ,  " ") : 'brak informacji'; ?></p>
            <span class="label-view">Kosz innych pożyczek</span>
        </div>
    </div>
</fieldset>	  
<fieldset class="info-container"><legend>Bank</legend>
    <div class="row">
        <div class="col-xs-12">
            <p><?= ($model->bank_name) ? $model->bank_name : 'brak informacji'; ?></p>
            <span class="label-view">Nazwa </span>
        </div>
        <div class="col-xs-12">
            <p><?= ($model->bank_account_number) ? $model->bank_account_number : 'brak informacji'; ?></p>
            <span class="label-view">Numer konta</span>
        </div>

        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->bank_how_long_use) ? backend\Modules\Apl\models\AplCustomer::periodBank()[$model->bank_how_long_use] : 'brak informacji'; ?></p>
            <span class="label-view">Od kiedy jest klientem banku</span>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->use_credit_card) ? 'TAK' : 'NIE'; ?></p>
            <span class="label-view">Czy używa karty kredytowej</span>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p><?= ($model->frequency_payment) ? ( ($model->frequency_payment == 1) ? 'tygodniowo' : 'miesięcznie') : 'brak informacji'; ?></p>
            <span class="label-view">Wypłaty</span>
        </div>
    </div>
</fieldset>	 