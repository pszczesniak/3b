<fieldset class="info-container"><legend>Telefon</legend>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <p><?= ($model->phone_mobile) ? $model->phone_mobile : 'brak informacji'; ?></p>
            <span class="label-view">Komórkowy </span>
        </div>
        <div class="col-sm-6 col-xs-12">
            <p><?= ($model->phone) ? $model->phone : 'brak informacji'; ?></p>
            <span class="label-view">Stacjonarny</span>
        </div>
    </div>
</fieldset>	  
<fieldset class="info-container"><legend>Adres</legend>
    <p><?= $model->addresses['full']; ?></p>
</fieldset>	 
<fieldset class="info-container"><legend>Adres korespondecyjny</legend>
    <p><?= $model->addresses['full']; ?></p>
</fieldset>	 