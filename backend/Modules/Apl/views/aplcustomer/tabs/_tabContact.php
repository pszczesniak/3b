<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;


//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

 <?php $form = (!$cform) ? ActiveForm::begin([ 'id' => 'customer-contact', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'],
                                               'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false ]) : $cform; ?>  

    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <fieldset><legend>Telefon</legend>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone_mobile')->textInput( ['maxlength' => 255] )  ?></div>
                            <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'phone')->textInput( ['maxlength' => 255] )  ?> </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset><legend>Adres zameldowania</legend>
                <div class="row">
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'address_postalcode')->textInput( ['maxlength' => 255] )  ?></div>
                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'address_city')->textInput( ['maxlength' => 255] )  ?> </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'address_street')->textInput( ['maxlength' => 255] )  ?></div>
                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'address_street_number')->textInput( ['maxlength' => 255] )  ?> </div>
                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'address_local_number')->textInput( ['maxlength' => 255] )  ?> </div>
                </div>
            </fieldset>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'c_address')->checkBox([ 'uncheck' => null, 'checked' => true])  ?>
            <fieldset><legend>Adres korespondencyjny</legend>
                <div class="row">
                    <div class="col-sm-4 col-xs-12"><?= $form->field($model, 'c_address_postalcode')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false ) ] )  ?></div>
                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'c_address_city')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-xs-12"><?= $form->field($model, 'c_address_street')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?></div>
                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'c_address_street_number')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                    <div class="col-sm-2 col-xs-6"><?= $form->field($model, 'c_address_local_number')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->c_address) ? true : false )] )  ?> </div>
                </div>
            </fieldset>
        </div>
    </div>

    <?php if(!$cform) { ?>
    <div class="form-group align-right">
        <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_contact'])->label(false); ?> 
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>
    <?php } ?>
<?php if(!$cform) { ActiveForm::end(); } ?>	

<script type="text/javascript"> 
    document.getElementById('aplcustomer-c_address').onchange = function(event) {
        if(!event.target.checked) {
            document.getElementById('aplcustomer-c_address_city').disabled = true;
            document.getElementById('aplcustomer-c_address_postalcode').disabled = true;
            document.getElementById('aplcustomer-c_address_street').disabled = true;
            document.getElementById('aplcustomer-c_address_street_number').disabled = true;
            document.getElementById('aplcustomer-c_address_local_number').disabled = true;
        } 
        if(event.target.checked) {
            document.getElementById('aplcustomer-c_address_city').disabled = false;
            document.getElementById('aplcustomer-c_address_postalcode').disabled = false;
            document.getElementById('aplcustomer-c_address_street').disabled = false;
            document.getElementById('aplcustomer-c_address_street_number').disabled = false;
            document.getElementById('aplcustomer-c_address_local_number').disabled = false;
        }
    }
 </script>
	