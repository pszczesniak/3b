<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use backend\Modules\Cms\models\CmsPageStatus;
use backend\Modules\Cms\models\CmsCategory;

//use yii\jui\Tabs;
//use yii\bootstrap;

/* @var $this yii\web\View */
/* @var $model common\models\Cms\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = (!$cform) ? ActiveForm::begin([ 'id' => 'customer-basic', 'options' => ['class' => $model->isNewRecord?'createform':'ajaxform'],
                                               'enableAjaxValidation'=>false, 'enableClientValidation'=>false, 'enableClientScript'=>false, 'validateOnSubmit' => false ]) : $cform; ?>  
     <div class="row">
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
            <?php if($model->isNewRecord) { ?>
            <fieldset><legend>Konto klienta</legend>
                <div class="row">
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'email')->textInput( ['maxlength' => 255] )  ?> </div>
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'c_account')->checkBox()  ?><?= $form->field($model, 'c_send')->checkBox()  ?></div>
                </div>
            </fieldset>
            <?php } ?>
			<fieldset><legend>Dane osobowe</legend>
                <div class="row">
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'firstname')->textInput( ['maxlength' => 255] )  ?></div>
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'lastname')->textInput( ['maxlength' => 255] )  ?> </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'pesel')->textInput( ['maxlength' => 255] )  ?></div>
                    <div class="col-sm-6 col-xs-12"><?= $form->field($model, 'evidence_number')->textInput( ['maxlength' => 255] )  ?> </div>
                </div>
            </fieldset>
            <fieldset><legend>Zatrudnienie</legend>
                <?= $form->field($model, 'employer_name')->textInput( ['maxlength' => 255] )  ?> 
                <div class="row">
                    <div class="col-sm-6 col-xs-12">                    
                        <?= $form->field($model, 'employment_status', [])->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusEmployment(), [ 'prompt' => '-- wybierz --']); ?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="aplcustomer-employment_period" class="control-label">Data wydarzenia</label>
                            <div class='input-group date clsDatePicker' id='datetimepicker_period'>
                                <input type='text' class="form-control" id="aplcustomer-employment_period" name="AplCustomer[employment_period]" value="<?= $model->employment_period ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
		<div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
			<fieldset><legend>Dodatkowe</legend>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">                    
                        <?= $form->field($model, 'id_marital_status_fk', [])->dropdownList(\backend\Modules\Apl\models\AplCustomer::statusMarital(), [ 'prompt' => '-- wybierz --']); ?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?= $form->field($model, 'number_of_members')->textInput( ['maxlength' => 255] )  ?>
                    </div>
                </div>
            </fieldset>
            <fieldset><legend>Samochód</legend>
                <?= $form->field($model, 'own_car')->checkBox([ 'uncheck' => 0, 'checked' => 1])  ?>
                <?= $form->field($model, 'car_type')->textInput( ['maxlength' => 255, 'disabled' => ( (!$model->own_car) ? true : false )] )  ?>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">                    
                        <?= $form->field($model, 'car_year')->textInput( ['maxlength' => 255,  'disabled' => ( (!$model->own_car) ? true : false )] )  ?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="aplcustomer-car_insurance" class="control-label">Data wydarzenia</label>
                            <div class='input-group date clsDatePicker' id='datetimepicker_event'>
                                <input type='text' class="form-control" id="aplcustomer-car_insurance" name="AplCustomer[car_insurance]" value="<?= $model->car_insurance ?>" <?=  (!$model->own_car) ? 'disabled' : ''  ?> />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
		</div>
	</div>
    <?php if(!$cform) { ?>
    <div class="form-group align-right">
        <?= $form->field($model, 'user_action')->hiddenInput(['value' => 'update_basic'])->label(false); ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
    </div>
    <?php } ?>
<?php if(!$cform) { ActiveForm::end(); } ?>	

<script type="text/javascript"> 
    document.getElementById('aplcustomer-own_car').onchange = function(event) {
        if(!event.target.checked) {
            //document.getElementById('car-info').classList.add('no-display');
            document.getElementById('aplcustomer-car_type').disabled = true;
            document.getElementById('aplcustomer-car_year').disabled = true;
            document.getElementById('aplcustomer-car_insurance').disabled = true;
        } 
        if(event.target.checked) {
            //document.getElementById('car-info').classList.remove('no-display');
            document.getElementById('aplcustomer-car_type').disabled = false;
            document.getElementById('aplcustomer-car_year').disabled = false;
            document.getElementById('aplcustomer-car_insurance').disabled = false;
        }
    }
 </script>
	