<fieldset class="info-container"><legend>Zatrudnienie</legend>
    <div class="row">
        <div class="col-xs-12">
            <p><?= $model->employer_name ?></p>
            <span class="label-view">Nazwa pracodawcy</span>
        </div>
        <div class="col-sm-6 col-xs-12">
            <p><?= ($model->employment_status) ? \backend\Modules\Apl\models\AplCustomer::statusEmployment()[$model->employment_status] : 'brak danych' ?></p>
            <span class="label-view">Status </span>
        </div>
        <div class="col-sm-6 col-xs-12">
            <p><?= $model->employment_period ?></p>
            <span class="label-view">Okres</span>
        </div>
    </div>
</fieldset>	 
<fieldset class="info-container"><legend>Dodatkowe</legend>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <p><?= ($model->id_marital_status_fk) ? \backend\Modules\Apl\models\AplCustomer::statusMarital()[$model->id_marital_status_fk] : 'brak danych' ?></p>
            <span class="label-view">Stan cywilny </span>
        </div>
        <div class="col-sm-6 col-xs-12">
            <p><?= $model->number_of_members ?></p>
            <span class="label-view">Liczba osób na utrzymaniu</span>
        </div>
    </div>
</fieldset>	  
<fieldset class="info-container"><legend>Samochód</legend>
    <?php if($model->own_car) { ?>
        <div class="row">
            <div class="col-sm-4 col-xs-12"><p><?= $model->car['type'] ?></p><span class="label-view">Typ</span></div>
            <div class="col-sm-4 col-xs-12"><p><?= $model->car['year'] ?></p><span class="label-view">Rocznik</span></div>
            <div class="col-sm-4 col-xs-12"><p><?= $model->car['insurance'] ?></p><span class="label-view">Termin ubezpieczenia</span></div>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">brak własnego samochodu</div>
    <?php } ?>
</fieldset>