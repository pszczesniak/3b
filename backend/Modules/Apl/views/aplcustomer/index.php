<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\Modules\Svc\models\SvcClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Klienci');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"svc-offer-index", 'title'=>Html::encode($this->title))) ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div id="toolbar-clients" class="btn-group toolbar-table-widget">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('lsdd', 'New'), ['modelClass' => Yii::t('lsdd', 'Apl customer'), ]), ['create'], ['class' => 'btn btn-success btn-icon']) ?>
        <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-clients"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
    </div>
    <table  class="table table-striped table-items header-fixed table-big table-widget"  id="table-clients"
            data-toolbar="#toolbar-clients" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
        
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100, ALL]"
            data-height="600"
            data-show-footer="false"
            data-side-pagination="server"
            data-row-style="rowStyle"
            data-sort-name="id"
            data-sort-order="desc"
            
            data-method="get"
            data-search-form="#filter-svc-clients-search"
            data-url=<?= Url::to(['/apl/aplcustomer/data']) ?>>
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="firstname"  data-sortable="true" data-searchable="true" >Imię</th>
                <th data-field="lastname"  data-sortable="true" data-searchable="true" >Nazwisko</th>
                <th data-field="pesel"  data-sortable="true" data-searchable="true" >Pesel</th>    
                <th data-field="postalcode"  data-sortable="address" data-searchable="true" >Kod pocztowy</th> 
                <th data-field="city"  data-sortable="address" data-searchable="true" >Miasto</th>                
                <th data-field="status"  data-sortable="true" data-searchable="true" data-align="center">Status</th>
                <th data-field="avatar"  data-sortable="false" data-width="60px" data-align="center"></th>
                <th data-field="stats"  data-sortable="false" data-width="300px" data-align="center">Wnioski</th>
                <th data-field="actions" data-events="actionEvents" data-width="80px" data-align="center"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>
        
    </table>
    
    
<?= $this->endContent(); ?>
