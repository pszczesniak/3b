<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<fieldset><legend>Wnioski</legend>
    <div class="summary-container">
        <div class="row">
            <div class="item item-unique-visitors col-md-4 col-sm-6 col-xs-12">
                <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-files-o text--blue"></span><span class="title-text">oczekujące</span></h4>
                <p class="item-figure text--blue">2</p>
            </div>
            <div class="item item-sessions col-md-4 col-sm-6 col-xs-12">
                <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-thumbs-up text--green"></span><span class="title-text">przyjęte</span></h4>
                <p class="item-figure text--green">2</p> 
            </div>
            <div class="item item-bounce-rate col-md-4 col-sm-6 col-xs-12">
                <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-thumbs-down text--red"></span><span class="title-text">odrzucone</span></h4>
                <p class="item-figure text--red">2</p>
            </div>    
        </div>
    </div>
</fieldset>