<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */

$this->title = 'Karta klienta';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12">
        <?= $this->render('_profile', [
            'model' => $model,
        ]) ?>
        <?= $this->render('_account', [
            'model' => $model, 'user' => ($model->account) ? $model->account : new \common\models\User()
        ]) ?>
        <?php /*$this->beginContent('@app/views/layouts/view-window.php',array('class'=>"apl-customer-comments-view", 'title'=>'K'))*/ ?>
            
        <?php /*$this->endContent();*/ ?>
    </div>
    <div class="col-xl-8 col-md-8 col-sm-12 col-xs-12">
        <?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>"apl-customer-stats-client-view", 'title'=>'Szczegóły profilu')) ?>
            <?= $this->render('_details', [
                'model' => $model,
            ]) ?>
        <?= $this->endContent(); ?>
        
        <?php 
            /*$this->beginContent('@app/views/layouts/view-window.php',array('class'=>"apl-customer-stats-client-view", 'title'=>'Statystyki')); 
            echo $this->render('_stats', ['model' => $model,  ]) ;
            $this->endContent(); */
        ?>
    </div>
</div>
