<?php
    use yii\helpers\Html;
?>

<div class="padding-md">
    <h4><?= ($model->firstname || $model->lastname) ? $model->firstname .' '. $model->lastname : 'brak imienia i nazwiska' ?></h4>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6 col-sm-12 col-md-6 text-center">
                    <a href="#">
                        <img class="img-thumbnail" alt="User Avatar" src="<?= (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$model->id_user_fk.".png")) ? "/../uploads/avatars/thumb/avatar-".$model->id_user_fk.".png" : "/../images/default-user.png" ?>">
                    </a>
                    <div class="seperator"></div>
                    <div class="seperator"></div>
                </div><!-- /.col -->
                <div class="col-xs-6 col-sm-12 col-md-6">
                    <?= Html::a('<i class="fa fa-pencil"></i>'.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-icon btn-primary']) ?>
                    <?= Html::a('<i class="fa fa-trash"></i>'.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-icon btn-danger']) ?>
                </div><!-- /.col -->
            </div><!-- /.row -->
            
            <div class="separator"></div>
            
            <div class="panel">
                <div class="panel-tab clearfix">
                    <ul class="tab-bar bg-primary">
                        <li class="active"><a data-toggle="tab" href="#config"><i class="fa fa-cog"></i> </a></li>
                        <li><a data-toggle="tab" href="#address"><i class="fa fa-map-marker"></i> </a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="config" class="tab-pane fade in active">
                        <!--<h5 class="headline">  Informacje   <span class="line"></span>   </h5>-->
                        <ul class="list-group">
                            <li class="list-group-item">test</li>
                        </ul> 		
                                                    
                    </div>
                
                    <div id="address" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item">test</li>
                        </ul> 							
                    </div>
                    
                </div><!-- /tab-content -->
            </div>
            
            
        </div><!-- /.col -->
        <div class="col-sm-6">
            <?php $activity = ['comments' => 0, 'queries' => 0] ?>
            <div class="panel panel-overview">
                <?php $colors = [1 => 'info', 2 => 'success', 3 => 'danger'] ?>
                <div class="overview-icon bg-info-brand">
                    <i class="fa fa-comment"></i>
                </div>
                <div class="overview-value">
                    <div class="h2"><?= $activity['comments'] ?> </div>
                    <div class="text-muted">Liczba komenatrzy</div>
                </div>
            </div><!--/ panel -->
            <div class="panel panel-overview">
                <div class="overview-icon bg-warning-brand">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="overview-value">
                    <div class="h2"><?= $activity['queries'] ?></div>
                    <div class="text-muted">Zapytań integratora</div>
                </div>
            </div><!--/ panel -->
            
            <div class="panel panel-white">
                <div class="panel-heading">  Opis  </div>
                <div class="panel-body">  test  </div>
            </div><!-- /panel -->

                

        </div><!-- /.col -->
    </div><!-- /.row -->			
</div>