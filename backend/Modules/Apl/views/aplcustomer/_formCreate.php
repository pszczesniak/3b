<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\Modules\Svc\models\SvcClient */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([ 'id' => 'customer-create' ]); ?>
<div class="svc-client-form tab-block">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Dane podstawowe</a></li>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Dane kontaktowe</a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Dane finansowe</a></li>
    </ul>

      <!-- Tab panes -->
   <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1"><?= $this->render('tabs/_tabBasic',array('model'=>$model, 'cform' => $form)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab2"><?= $this->render('tabs/_tabContact',array('model'=>$model, 'cform' => $form)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab3"><?= $this->render('tabs/_tabFinance',array('model'=>$model, 'cform' => $form)) ?></div>
    </div>
</div>
<br />
<div class="form-group align-right">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : ('<i class="fa fa-save"></i>'.Yii::t('app', 'Save')), ['class' => $model->isNewRecord ? 'btn btn-success main-form-btn btn-form-basic' : 'btn btn-primary main-form-btn btn-form-basic btn-icon']) ?>
</div>
<?php ActiveForm::end(); ?>