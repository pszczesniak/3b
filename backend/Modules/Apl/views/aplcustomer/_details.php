<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use backend\widgets\files\FilesBlock;
?>

<div class="tab-block">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-user"></i></span><span class="hidden-xs">Dane podstawowe</span></a></li>
        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-address-card"></i></span><span class="hidden-xs">Dane kontaktowe</span></a></li>
        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-dollar"></i></span><span class="hidden-xs">Dane finansowe</span></a></li>
        <li role="presentation"><a href="#tab4" aria-controls="tab3" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-files-o"></i></span><span class="hidden-xs">Dokumenty</span></a></li>
        <li role="presentation"><a href="#tab5" aria-controls="tab4" role="tab" data-toggle="tab"><span class="visible-xs"><i class="fa fa-tasks"></i></span><span class="hidden-xs">Wnioski</span></a></li>
    </ul>

      <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1"><?= $this->render('tabs/_viewBasic',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab2"><?= $this->render('tabs/_viewContact',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab3"><?= $this->render('tabs/_viewFinance',array('model'=>$model)) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab4"><?= FilesBlock::widget(['files' => $model->files, 'isNew' => $model->isNewRecord, 'typeId' => 2, 'parentId' => $model->id, 'onlyShow' => false]) ?></div>
        <div role="tabpanel" class="tab-pane" id="tab5"><?= $this->render('tabs/_tabLoans',array('model'=>$model)) ?></div>
    </div>
</div>