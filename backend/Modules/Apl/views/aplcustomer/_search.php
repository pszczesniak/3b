<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\Modules\Task\models\CalCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'id' => 'filter-svc-clients-search',
        'options' => ['class' => 'widget-table-filter', 'data-table' => '#table-clients']
    ]); ?>
    
    <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'status')->dropDownList( \backend\Modules\Apl\models\AplCustomer::statusShortList()['names'], ['prompt' => ' - wybierz -', 'class' => 'form-control widget-search-list select2', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ] ) ?></div>        
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'pesel')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('Pesel <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>                
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'firstname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('Imię <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-6"> <?= $form->field($model, 'lastname')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('Nazwisko <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-4 col-md-4 col-xs-12"> <?= $form->field($model, 'email')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('E-mail <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-3 col-md-3 col-xs-4"> <?= $form->field($model, 'address_postalcode')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('Kod pocztowy <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
        <div class="col-sm-5 col-md-5 col-xs-8"> <?= $form->field($model, 'address_city')->textInput([ 'class' => 'form-control widget-search-text', 'data-table' => '#table-clients', 'data-form' => '#filter-svc-clients-search' ])->label('Miasto <i class="fa fa-info-circle text--blue" title="Wyszukiwanie rozpocznie się po wpisniau przynajmniej 3 znaków"></i>') ?></div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
