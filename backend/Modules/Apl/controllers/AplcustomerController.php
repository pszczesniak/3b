<?php

namespace app\Modules\Apl\controllers;

use Yii;
use backend\Modules\Apl\models\AplCustomer;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Url;

/**
 * AplcustomerController implements the CRUD actions for AplCustomer model.
 */
class AplcustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AplCustomer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AplCustomer();
        $searchModel->status = 3;

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

        $post = $_GET; $where = [];
        if(isset($_GET['AplCustomer'])) {
            $params = $_GET['AplCustomer'];
            if(isset($params['lastname']) && !empty($params['lastname']) ) {
				array_push($where, " (lower(lastname) like '%".strtolower( addslashes($params['lastname']) )."%' or lower(lastname) like '%".strtolower( addslashes($params['lastname']) )."%')");
            }
            if(isset($params['firstname']) && !empty($params['firstname']) ) {
				array_push($where, " (lower(firstname) like '%".strtolower( addslashes($params['firstname']) )."%' or lower(lastname) like '%".strtolower( addslashes($params['firstname']) )."%')");
            }
            if(isset($params['address_postalcode']) && !empty($params['address_postalcode']) ) {
				array_push($where, "address_postalcode like '%".strtolower( addslashes($params['address_postalcode']) )."%'");
            }
            if(isset($params['address_city']) && !empty($params['address_city']) ) {
				array_push($where, "address_city like '%".strtolower( addslashes($params['address_city']) )."%'");
            }
            if(isset($params['email']) && !empty($params['email']) ) {
				array_push($where, "email like '%".strtolower( addslashes($params['email']) )."%'");
            }
            if(isset($params['pesel']) && !empty($params['pesel']) ) {
				array_push($where, "pesel like '%".strtolower( addslashes($params['pesel']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
                if($params['status'] < 0)
                    array_push($where, "status = ".$params['status']);
                else if($params['status'] >= 3)
                    array_push($where, "status in (3,4) ");
                else 
                    array_push($where, "status in (0,1,2) ");
            }
        } 
        
        $sortColumn = 'c.id';
        if( isset($post['sort']) && $post['sort'] == 'firstname' ) $sortColumn = 'firstname';
        if( isset($post['sort']) && $post['sort'] == 'lastname' ) $sortColumn = 'lastname';
        if( isset($post['sort']) && $post['sort'] == 'bank_name' ) $sortColumn = 'bank_name';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'status';
        
		$query = (new \yii\db\Query())
            ->select(['c.id as id', 'c.bank_name as bank_name', 'c.status as status', 'firstname', 'lastname', 'id_user_fk', "pesel", "address_postalcode", "address_city",
                      "(select concat_ws(',',sum(case when status = 3 then 1 else 0 end),sum(case when status = -2 then 1 else 0 end),sum(case when status = 1 then 1 else 0 end)) from {{%apl_loan}} where id_customer_fk=c.id) as loans"])
            ->from('{{%apl_customer}} as c');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            
        $rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = AplCustomer::statusList()['colors'];
        $status = AplCustomer::statusList()['names'];
        
        $statusList = AplCustomer::statusList();
  
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/apl/aplcustomer/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= '<a href="'.Url::to(['/apl/aplcustomer/update', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1 ) ? '<a href="'.Url::to(['/apl/aplcustomer/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['firstname'] = $value['firstname'];
            $tmp['lastname'] = $value['lastname'];
            $tmp['pesel'] = $value['pesel'];
            $tmp['postalcode'] = $value['address_postalcode'];
            $tmp['city'] = $value['address_city'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = $actionColumn;
            $avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/images/default-user.png";
            $tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.\Yii::$app->params['frontend'].$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
            $loans = explode(',', $value['loans']);
            $tmp['stats'] = '<ul class="list-inline text-center">'
                                .'<li class="text--green"><span class="figure">'.( (isset($loans[0]) && !empty($loans[0])) ? $loans[0] : 0).'</span><span>przyznane</span></li>'
                                .'<li class="text--red"><span class="figure">'.(isset($loans[1]) ? $loans[1] : 0).'</span><span>odrzucone</span></li>'
                                .'<li class="text--blue"><span class="figure">'.(isset($loans[2]) ? $loans[2] : 0).'</span><span>oczekujące</span></li>'
                            .'</ul>';

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}

    /**
     * Displays a single AplCustomer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AplCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AplCustomer();
        $model->status = 4;
        $model->scenario = 'create';
        $user = new User();

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/) {
            //return $this->redirect(['view', 'id' => $model->id]);
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if($model->c_account) {
                    $user = new \common\models\User();
                    $user->generateAuthKey();
                    $user->status = 10;
                    $user->roleType = 'client';

                    $user->username = $model->email; 
                    $user->email = $model->email;
                    $user->firstname = $model->firstname;
                    $user->lastname = $model->lastname;
                    $pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
                    $user->setPassword($pass);
                    
                    if($user->save()) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('client');
                        $auth->assign($role, $user->id);

                        $model->id_user_fk = $user->id;
                        //$model->facebook_config = $pass;
                        
                        if($model->c_send) {
                            try {
                                \Yii::$app->mailer->compose(['html' => 'clientAccount-html', 'text' => 'clientAccount-text'], ['login' => $user->username, 'password' => $pass, 'username' => ($model->firstname.' '.$model->lastname) ])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                ->setTo($model->email)
                                ->setBcc(\Yii::$app->params['bccEmail'])
                                ->setSubject('Konto klienta w serwisie GTW Finanse')
                                ->send();
                            } catch (\Swift_TransportException $e) {
                                $model->c_send = 1;
                            }
                        }
                    } else {
                        $transaction->rollBack();
                        return $this->render('create', [
                            'model' => $model, 'user' => $user, 'errors' => $model->getErrors()
                        ]);
                    }
                }
                if($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    $transaction->rollBack();
                    return $this->render('create', [
                        'model' => $model, 'user' => $user, 'errors' => $model->getErrors()
                    ]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                        'model' => $model, 'user' => $user, 'errors' => array_merge($model->getErrors(), $user->getErrors())
                    ]);
            }
            /*if($model->save()) {
               // $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
               // $transaction->rollBack();
                return $this->render('create', [
                    'model' => $model, 'user' => $user, 'errors' => $model->getErrors()
                ]);
            }*/
        } else {
            return $this->render('create', [
                'model' => $model, 'user' => $user
            ]);
        }
    }

    /**
     * Updates an existing AplCustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = "update";
        if(!$model->address_for_correspondence) $model->c_address = false; else $model->c_address = true;
        
        $user = User::findOne($model->id_user_fk);
        /*if($user)
            $model->username = $user->username;*/
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $actionArr = explode('_', $model->user_action);
            if($model->validate() && $model->save()) {			
				return array('success' => true,  'action' => false, 'alert' => 'Dane zostały zaktualizowae' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('tabs/_tab'.ucfirst($actionArr[1]), ['model' => $model, 'cform' => false]), 'errors' => $model->getErrors() );	
			}	
        } else {
            return $this->render('update', [
                'model' => $model, 'user' => $user
            ]);
        }
    }

    /**
     * Deletes an existing AplCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $user = \common\models\User::findOne($model->id_user_fk); 
        $user->status = 0;
        $user->save(); 

        return $this->redirect(['index']);
    }

    /**
     * Finds the AplCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AplCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AplCustomer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Klient o podanym identyfikatorze nie istnieje.');
        }
    }
    
    public function actionInfo($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $customer = \backend\Modules\Apl\models\AplCustomer::find()->where(['id_user_fk' => $id])->one();
        return ['info' => $customer];
    }
    
    public function actionLoans($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['AplLoan'])) {
            $params = $_GET['AplLoan'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(o.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['city']) && !empty($params['city']) ) {
				array_push($where, "lower(o.city) like '%".strtolower( addslashes($params['city']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "o.status = ".$params['status']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        $sortColumn = 'l.id';
        if( isset($post['sort']) && $post['sort'] == 'companyname' ) $sortColumn = 'o.companyname';
        if( isset($post['sort']) && $post['sort'] == 'city' ) $sortColumn = 'o.city';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'o.status';
        if( isset($post['sort']) && $post['sort'] == 'package' ) $sortColumn = 'p.name';
		
		$query = (new \yii\db\Query())
            ->select(['l.id as id', 'l.l_amount as l_amount', 'l.l_period as l_period', 'l.status as status', 'c.id as cid', "concat_ws(' ', c.lastname, c.firstname) as customer", 'c.created_at as created_at', 'l.id_request_fk as l_request', 'c.id_request_fk as c_request'])
            ->from('{{%apl_loan}} as l')
            ->join(' JOIN', '{{%apl_customer}} as c', 'c.id = l.id_customer_fk')
            ->where( ['l.status' => 1, 'id_customer_fk' => $id] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand())
		      
       /* $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/matter/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/task/matter/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/task/matter/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';*/
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'warning', '1' => 'info', '2' => 'success'];
        $status = ['-3' => 'usunięty', '-2' => 'odrzucony', '0' => 'niepotwierdzony', '1' => 'potwierdzony', '2' => 'przyjęty'];//CompanyEmployee::listTypes();
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/apl/aplloan/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            //$actionColumn .= '<a href="/panel/pl/apl/aplloan/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="'.Url::to(['/apl/aplloan/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['customer'] = $value['customer'];
            $tmp['l_amount'] = $value['l_amount'];
            $tmp['l_period'] = $value['l_period'];
            $tmp['created'] = $value['created_at'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn) ;
            //$avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/../images/default-user.png";
            //$tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
            $tmp['className'] = ($value['c_request'] == $value['l_request']) ? 'info' : '';

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    
    public function actionAccount($id)  {
        $model = $this->findModel($id);
        $model->scenario = "account";
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $user = new \common\models\User();
        $user->scenario = 'default';
        $user->load(Yii::$app->request->post());
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $user->generateAuthKey();
            $user->status = 10;
            $user->roleType = 'client';

            $pass = Yii::$app->getSecurity()->generateRandomString(6); //generateRandomString
            $user->setPassword($pass);
            if($user->save()) {
                $auth = \Yii::$app->authManager;
                $role = $auth->getRole('client');
                $auth->assign($role, $user->id);

                $model->id_user_fk = $user->id;
                
                if($user->c_send) {
                    try {
                        \Yii::$app->mailer->compose(['html' => 'clientAccount-html', 'text' => 'clientAccount-text'], ['login' => $user->username, 'password' => $pass, 'username' => ($model->firstname.' '.$model->lastname) ])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($this->email)
                        ->setBcc(\Yii::$app->params['bccEmail'])
                        ->setSubject('Konto klienta w serwisie GTW Finanse')
                        ->send();
                    } catch (\Swift_TransportException $e) {
                        $model->c_send = 1;
                    }
                }
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_account', ['model' => $model, 'user' => $user]), 'errors' => $user->getErrors() );
            }

            if($model->save()) {
                $transaction->commit();
                return array('success' => true, 'alert' => 'Konto zostało utworzone' );
            } else {
                $transaction->rollBack();
                return array('success' => false, 'html' => $this->renderAjax('_account', ['model' => $model, 'user' => $user]), 'errors' => $user->getErrors() );
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return array('success' => false, 'html' => $this->renderAjax('_account', ['model' => $model, 'user' => $user]), 'errors' => $user->getErrors() );
        }
    }
}
