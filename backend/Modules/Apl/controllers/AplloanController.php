<?php

namespace app\Modules\Apl\controllers;

use Yii;
use backend\Modules\Apl\models\AplLoan;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\CustomHelpers;
use yii\helpers\Url;

/**
 * AplLoanController implements the CRUD actions for AplLoan model.
 */
class AplloanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AplLoan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AplLoan();

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }

    public function actionData() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['AplLoan'])) {
            $params = $_GET['AplLoan'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(o.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['city']) && !empty($params['city']) ) {
				array_push($where, "lower(o.city) like '%".strtolower( addslashes($params['city']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "l.status = ".$params['status']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "id_customer_fk = ".$params['id_customer_fk']);
            }
            if(isset($params['id_purpose_fk']) && !empty($params['id_purpose_fk']) ) {
				array_push($where, "id_purpose_fk = ".$params['id_purpose_fk']);
            }
        } 
        
        $sortColumn = 'l.id';
        if( isset($post['sort']) && $post['sort'] == 'companyname' ) $sortColumn = 'o.companyname';
        if( isset($post['sort']) && $post['sort'] == 'city' ) $sortColumn = 'o.city';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'o.status';
        if( isset($post['sort']) && $post['sort'] == 'package' ) $sortColumn = 'p.name';
		
		$query = (new \yii\db\Query())
            ->select(['l.id as id', 'l.l_amount as l_amount', 'l.l_period as l_period', 'l.status as status', 'c.id as cid', "concat_ws(' ', c.lastname, c.firstname) as customer"])
            ->from('{{%apl_loan}} as l')
            ->join(' JOIN', '{{%apl_customer}} as c', 'c.id = l.id_customer_fk');
            //->where( ['m.status' => 1, 'm.type_fk' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand())
		      
       /* $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/matter/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/task/matter/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/task/matter/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';*/
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'warning', '1' => 'info', '2' => 'success'];
        $status = ['-3' => 'usunięty', '-2' => 'odrzucony', '0' => 'niepotwierdzony', '1' => 'oczekujący', '2' => 'przyjęty'];//CompanyEmployee::listTypes();
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/apl/aplloan/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            //$actionColumn .= '<a href="/panel/pl/apl/aplloan/update/%d" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></a>';
            $actionColumn .= ( ($value['status'] == 1) ? '<a href="'.Url::to(['/apl/aplloan/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';
			$tmp['customer'] = $value['customer'];
            $tmp['l_amount'] = $value['l_amount'];
            $tmp['l_period'] = $value['l_period'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = $actionColumn;
            //$avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/../images/default-user.png";
            //$tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    
    
    public function actionUnverified() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$post = $_GET; $where = [];
        if(isset($_GET['AplLoan'])) {
            $params = $_GET['AplLoan'];
            if(isset($params['name']) && !empty($params['name']) ) {
				array_push($where, "lower(o.name) like '%".strtolower( addslashes($params['name']) )."%'");
            }
            if(isset($params['city']) && !empty($params['city']) ) {
				array_push($where, "lower(o.city) like '%".strtolower( addslashes($params['city']) )."%'");
            }
            if(isset($params['status']) && !empty($params['status']) ) {
				array_push($where, "o.status = ".$params['status']);
            }
            if(isset($params['id_customer_fk']) && !empty($params['id_customer_fk']) ) {
				array_push($where, "id_customer_fk = ".$params['id_customer_fk']);
            }
        } 
        
        $sortColumn = 'l.id';
        if( isset($post['sort']) && $post['sort'] == 'companyname' ) $sortColumn = 'o.companyname';
        if( isset($post['sort']) && $post['sort'] == 'city' ) $sortColumn = 'o.city';
        if( isset($post['sort']) && $post['sort'] == 'status' ) $sortColumn = 'o.status';
        if( isset($post['sort']) && $post['sort'] == 'package' ) $sortColumn = 'p.name';
		
		$query = (new \yii\db\Query())
            ->select(['l.id as id', 'l.l_amount as l_amount', 'l.l_period as l_period', 'l.status as status', 'c.id as cid', "concat_ws(' ', c.lastname, c.firstname) as customer", 'c.created_at as created_at', 'l.id_request_fk as l_request', 'c.id_request_fk as c_request'])
            ->from('{{%apl_loan}} as l')
            ->join(' JOIN', '{{%apl_customer}} as c', 'c.id = l.id_customer_fk')
            ->where( ['l.status' => 1] );
	    
        if( count($where) > 0 ) {
            $query = $query->andWhere(implode(' and ',$where));
        }
		$count = $query->count();
        $query->limit( isset($post['limit']) ? $post['limit'] : 10 )
            ->offset( isset($post['offset']) ? $post['offset'] : 0 )
            ->orderBy($sortColumn.' ' . (isset($post['order']) ? $post['order'] : 'desc') );
            //->orderBy([ 'lastname '  => isset($_GET['order']) ? $_GET['order'] : 'asc' ]);
        //var_dump($query->createCommand())
		      
       /* $actionColumn = '<div class="edit-btn-group">';
        $actionColumn .= '<a href="/task/matter/view/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-eye-open\'></i>'.Yii::t('lsdd', 'View').'" title="'.Yii::t('app', 'View').'" ><i class="fa fa-eye"></i></a>';
        $actionColumn .= '<a href="/task/matter/update/%d" class="btn btn-sm btn-default" data-table="#table-items" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'" title="'.Yii::t('app', 'Edit').'" ><i class="fa fa-pencil"></i></a>';
        $actionColumn .= '<a href="/task/matter/deleteajax/%d" class="btn btn-sm btn-default remove" data-table="#table-items"><i class="fa fa-trash" title="'.Yii::t('app', 'Delete').'"></i></a>';
        $actionColumn .= '</div>';*/
		
		$rows = $query->all();

		$fields = [];
		$tmp = [];
        $colors = ['-3' => 'danger', '-2' => 'danger', '-1' => 'warning', '0' => 'warning', '1' => 'info', '2' => 'success'];
        $status = ['-3' => 'usunięty', '-2' => 'odrzucony', '0' => 'niepotwierdzony', '1' => 'potwierdzony', '2' => 'przyjęty'];//CompanyEmployee::listTypes();
        
		foreach($rows as $key=>$value) {
            $actionColumn = '<div class="edit-btn-group">';
            $actionColumn .= '<a href="'.Url::to(['/apl/aplloan/view', 'id' => $value['id']]).'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
            $actionColumn .= ( ($value['status'] == 1 ) ? '<a href="'.Url::to(['/apl/aplloan/delete', 'id' => $value['id']]).'" class="btn btn-sm btn-default remove" data-table="#table-data"><i class="glyphicon glyphicon-trash"></i></a>' : '');
            $actionColumn .= '</div>';

			$tmp['customer'] = $value['customer'];
            $tmp['l_amount'] = $value['l_amount'];
            $tmp['l_period'] = $value['l_period'];
            $tmp['created'] = $value['created_at'];
            $tmp['status'] = isset($status[$value['status']]) ? '<span class="label label-'.$colors[$value['status']].'">'.$status[$value['status']].'</span>' : 'nieznany';
            $tmp['actions'] = sprintf($actionColumn, $value['id'], $value['id'], $value['id']) ;
            //$avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png")) ? "/../uploads/avatars/thumb/avatar-".$value['id_user_fk'].".png" : "/../images/default-user.png";
            //$tmp['avatar'] = '<img alt="avatar" width="40" height="40" src="'.$avatar.'" title="avatar"></img>';
            $tmp['move'] = '<a class="btn btn-default btn-sm action up" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value['id'];
            $tmp['className'] = ($value['c_request'] == $value['l_request']) ? 'info' : '';

			array_push($fields, $tmp); $tmp = [];
		}

		return ['total' => $count,'rows' => $fields];
	}
    

    /**
     * Displays a single AplLoan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AplLoan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new AplLoan();
        $model->id_customer_fk = $id;
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            if($model->validate() && $model->save()) {			
				return array('success' => true,  'action' => false, 'alert' => 'Wniosek został dodany' );	
			} else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', ['model' => $model]), 'errors' => $model->getErrors() );	
			}	
        } else {
            return $this->renderAjax('_formAjax', [
                'model' => $model, 
            ]);
        }
    }

    /**
     * Updates an existing AplLoan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->id_customer_fk = $id;
     
        if ($model->load(Yii::$app->request->post()) ) {
            if ($model->save()) {
               
            } else {
                return $this->render('update', [
                    'model' => $model, 
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 
            ]);
        }
    }

    /**
     * Deletes an existing AplLoan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AplLoan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AplLoan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AplLoan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The loan does not exist.');
        }
    }

    public function actionAccept($id)  {
        $model = $this->findModel($id);
        $model->status = 2;
        
        //$user = User::findOne($model->id_user_fk);
        if($model->save()) {
            try {
                \Yii::$app->mailer->compose(['html' => 'loanAccept-html', 'text' => 'loanAccept-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                            //->setTo($model->customer['email'])
                            ->setTo(\Yii::$app->params['bccEmail'])
                            ->setSubject('Akceptacja wniosku pożyczkowego w serwisie' . \Yii::$app->name)
                            ->send();
                $model->save();
            } catch (\Swift_TransportException $e) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Problem z wyslaniem e-maila'));
            }
        }

        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionDiscard($id)  {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = -2;
        $model->l_purpose = ( isset($_POST['comment']) ) ?  $_POST['comment'] : '';
        
        //$user = User::findOne($model->id_user_fk);
        if($model->save()) {
            try {
                \Yii::$app->mailer->compose(['html' => 'loanDiscard-html', 'text' => 'loanDiscard-text'], ['model' => $model])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                            //->setTo($model->customer['email'])
                            ->setTo(\Yii::$app->params['bccEmail'])
                            ->setSubject('Odrzucenie wniosku pożyczkowego w serwisie ' . \Yii::$app->name)
                            ->send();
                $model->save();
            } catch (\Swift_TransportException $e) {
                Yii::$app->getSession()->setFlash( 'error', Yii::t('lsdd', 'Problem z wyslaniem e-maila'));
            }
        }

        return ['success' => true, 'alert' => 'Wniosek został odrzucony', 'id' => $model->id, 'action' => 'discard', 'table' => false];
    }
    
    public function actionNote() {
        
        //$model = $this->findModel($id);
        $note = new \backend\Modules\Apl\models\AplInfo();
        $note->type_fk = 1;
        
        if( isset($_GET['parent']) ){
            $parent = \backend\Modules\Apl\models\AplInfo::findOne($_GET['parent']);
            $note->id_parent_fk = $parent->id;
            $note->id_loan_fk = $parent->id_loan_fk;
            $note->id_customer_fk = $parent->id_customer_fk;
        } else {
            if($_GET['type'] == 1) {
                $note->id_customer_fk = $_GET['id'];
                $note->id_loan_fk = 0;
            } else {
                $loan = \backend\Modules\Apl\models\AplLoan::findOne($_GET['id']);
                $note->id_loan_fk = $_GET['id'];
                $note->id_customer_fk = $loan->id_customer_fk;
            }
            $note->id_parent_fk = 0;
        }
        $note->status = 0;
        
        if (Yii::$app->request->isPost ) {		
			Yii::$app->response->format = Response::FORMAT_JSON;
            $note->load(Yii::$app->request->post());
            $note->status = 1;
            if($note->save()) {
                $customer = $note->customer;
                if($customer->email) {
                    try {
                        \Yii::$app->mailer->compose(['html' => 'adminAction-html', 'text' => 'adminAction-text'], ['model' => $note->loan, 'info' => 'Otrzymałeś nową wiadomość w systemie GTW', 'id' => $note->id ])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                        ->setTo($customer->email)
                        ->setTo(\Yii::$app->params['bccEmail'])
                        ->setSubject('Nowe zdarzenie w serwisie GTW Finanse')
                        ->send();
                    } catch (\Swift_TransportException $e) {
                        $note->status = 1;
                    }
                }
                //$note->created_at = 'dodany teraz';
                $html = '<div class="comment" id="comment-'.$note->id.'">'
                            .'<img src="/images/default-user.png" alt="" class="comment-avatar">'
                            .'<div class="comment-body">'
                                .'<div class="comment-text">'
                                    .'<div class="comment-header">'
                                        .'<a href="#" title="">Twoja wiadomość</a><span>'.date('Y-m-d H:i:s').'</span>'
                                    .'</div>'
                                    .$note->info_content
                                .'</div>'
                                .'<div class="comment-footer">'
                                    //.'<a href="#"><i class="fa fa-thumbs-o-up"></i></a>'
                                    //.'<a href="#"><i class="fa fa-thumbs-o-down"></i></a>'
                                    .'<a href="'.Url::to(['/apl/aplloan/note', 'parent' => $note->id]).'" class="gridViewModal" data-target="#modal-grid-item" data-title="Odpowiedź na wiadomość" data-icon="envelope">Odpowiedz</a>'
                                .'</div>'
                            .'</div>'
                        .'</div>';
                
                /*$modelArch = new \backend\Modules\Task\models\CalCaseArch();
                $modelArch->id_case_fk = $model->id;
                $modelArch->user_action = 'note';
                $modelArch->data_arch = \yii\helpers\Json::encode(['note' => $note->note, 'id' => $note->id]);
                $modelArch->data_change = $note->id;
                $modelArch->created_by = \Yii::$app->user->id;
                $modelArch->created_at = date('Y-m-d H:i:s');
                //$modelArch->data_arch = \yii\helpers\Json::encode($changedAttributes);
                if(!$modelArch->save()) var_dump($modelArch->getErrors());*/
                
                return array('success' => true,  'action' => 'add_note', 'title' => 'Dziękujemy za kontakt', 'alert' => 'Wiadomność została wysłana', 'parent' => $note->id_parent_fk, 'html' => $html );
            } else {
                return array('success' => false, 'errors' => $note->getErrors(), 'html' => $this->renderPartial('_noteForm', [ 'model' =>$note ]), );
            }
        }
        return $this->renderPartial('_noteForm', ['model' => $note]);
    }
}
