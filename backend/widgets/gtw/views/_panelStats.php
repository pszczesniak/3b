<?php
    use yii\helpers\Url;
?>
<div class="summary-container">
    <div class="row">
        <div class="item item-sessions col-md-3 col-sm-6 col-xs-12">
            <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-users text--purple"></span><span class="title-text">Liczba klientów potwierdzonych</span></h4>
            <p class="item-figure text--purple"><?= $stats['amountClients'] ?></p> 
        </div>
        
        <div class="item item-unique-visitors col-md-3 col-sm-6 col-xs-12">
            <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-files-o text--pink"></span><span class="title-text">Liczba wniosków potwierdzonych</span></h4>
            <p class="item-figure text--pink"><?= $stats['amountLoans'] ?></p>
        </div>
        
        <div class="item item-bounce-rate col-md-3 col-sm-6 col-xs-12">
            <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-comments text--blue"></span><span class="title-text">Opinie opublikowane</span></h4>
            <p class="item-figure text--blue" id="comments-info"><?= $stats['amountComments'] ?></p>
        </div>
        
        <div class="item item-duration  col-md-3 col-sm-6 col-xs-12">
            <h4 class="item-title"><span aria-hidden="true" class="icon fa fa-home text--orange"></span><span class="title-text">Liczba wejść na stronę główną</span></h4>
            <p class="item-figure text--orange"><?= $stats['homePageClicked'] ?></p>
        </div>     
    </div>
</div>