<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcCategory;

class Structurewidget extends Widget{
	
	private $structure;
	public $zone = 1;
	
	public function init(){
		parent::init();
		
		$this->structure = SvcCategory::find()->where(['status' => 1, 'id_parent_fk' => 0])->all();
	}
	
	public function run(){
		return $this->render('_formStructure', [
            'structure' => $this->structure, 
        ]);
	}
}
?>
