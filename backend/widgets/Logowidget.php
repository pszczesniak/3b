<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use app\Modules\Lsdd\models\LsddZone;

class Logowidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
	
	public function init(){
		parent::init();
		
		$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		$this->zone = LsddZone::findOne($zoneId);
	}
	
	public function run(){
		return $this->render('_moduleMenu/_logo', [
            'zone' => $this->zone, 
        ]);
	}
}
?>
