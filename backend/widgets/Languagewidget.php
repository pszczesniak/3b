<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Languagewidget extends Widget{
	
	private $modal;
	public $lang = 'pl';
	public $langName = 'Polish';
	
	public function init(){
		parent::init();
		$langs = ['pl' => 'Polish', 'en' => 'English', 'de' => 'Deutsch', 'ru' => 'Russian'];
		if(\Yii::$app->request->get('language')) $this->lang = \Yii::$app->request->get('language'); else $this->lang = 'pl';
		if(!array_key_exists($this->lang, $langs)) $this->lang = 'pl';
 
		$this->langName = $langs[$this->lang];
	}
	
	public function run(){
		return $this->render('_formLanguage', [
            'lang' => $this->lang, 'langName' => $this->langName, 
        ]);
	}
}
?>
