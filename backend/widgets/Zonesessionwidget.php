<?php
namespace backend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

use app\Modules\Lsdd\models\LsddZone;

class Zonesessionwidget extends Widget{
	
	private $zone = false;
	private $currency;
	private $zonesList = array();
	private $currencyList = array();
	
	public function init(){
		parent::init();
		if(!\Yii::$app->user->isGuest) {
			$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1; 
			
			$this->zone = LsddZone::findOne($zoneId);
			$config = \yii\helpers\Json::decode($this->zone->zone_config); 
			$this->zone->lang = $config['lang'];
			$this->zone->currency = $config['currency'];
			$this->zone->currency_symbol = $config['currency_symbol'];
			$this->zone->alternative_currency = $config['alternative_currency'];
			$this->zone->alternative_currency_symbol = $config['alternative_currency_symbol']; 
			$this->zone->alternative_currency_list = (isset($config['alternative_currency_list']))?$config['alternative_currency_list']:false;
			$this->zone->alternative_currency_symbol_list = (isset($config['alternative_currency_symbol_list']))?$config['alternative_currency_symbol_list']:false; 
			$this->zone->skin = $config['skin'];
			$this->zone->app_title = $config['app_title'];
			
			$this->currency = \Yii::$app->session->get('user.currency')?\Yii::$app->session->get('user.currency'):$this->zone->alternative_currency_symbol; 

			if(Yii::$app->user->identity->role->name == 'padre')
				$zones = LsddZone::find()->all();
			else 
				$zones = LsddZone::find()->where(['id' => $zoneId])->all();
				
			foreach($zones as $key=>$value) {
				$this->zonesList[ $value->id ] = $value->zone_name;
			}
			
			foreach(explode(',', $this->zone->alternative_currency_symbol_list) as $key=>$value) {
				$this->currencyList[ $value ] = $value;
			}
		}
 	}
	
	public function run(){
		return $this->render('_formZoneSession', [
            'zone' => $this->zone, 'zonesList' => $this->zonesList, 'currencyList' => $this->currencyList, 'currency' => $this->currency
        ]);
	}
}
?>
