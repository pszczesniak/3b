<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use common\models\Files;

class Fileswidget extends Widget{
	
	private $modal;
	public $typeId = 0;
	public $parentId = 0;
	public $view = false;
	
	public function init(){
		parent::init();
		
		$this->modal = new Files();
	}
	
	public function run(){
		return $this->render('_formFiles', [
            'model' => $this->modal, 'type' => $this->typeId, 'id' => $this->parentId, 'view' => $this->view
        ]);
	}
}
?>
