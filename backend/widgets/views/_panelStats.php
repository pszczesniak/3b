<?php
    use yii\helpers\Url;
?>
<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="panel-stat3 bg-danger-brand">
            <h2 id="userCount" class="m-top-none"><?= $stats['amountOffers'] ?></h2>
            <h5>Zarejestrowani usługodawcy</h5>
            <i class="fa fa-hand-o-right fa-lg"></i><span class="m-left-xs"><a href="<?= Url::to(['/svc/svcoffer/index']) ?>">Przejdź do rejestru ofert</a></span>
            <div class="stat-icon">
                <i class="fa fa-glass fa-3x"></i>
            </div>
            <!--<div class="refresh-button">
                <i class="fa fa-refresh"></i>
            </div>-->
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>
    </div><!-- /.col -->
    <div class="col-sm-6 col-md-3">
        <div class="panel-stat3 bg-info-brand">
            <h2 class="m-top-none"><?= $stats['amountClients'] ?></h2>
            <h5>Zarejestrowani klienci</h5>
            <i class="fa fa-hand-o-right fa-lg"></i><span class="m-left-xs"><a href="<?= Url::to(['/svc/svcclient/index']) ?>">Przejdź do rejestru klientów</a></span>
            <div class="stat-icon">
                <i class="fa fa-user fa-3x"></i>
            </div>
            <!--<div class="refresh-button">
                <i class="fa fa-refresh"></i>
            </div>-->
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>
    </div><!-- /.col -->
    <div class="col-sm-6 col-md-3">
        <div class="panel-stat3 bg-warning-brand">
            <h2 id="orderCount" class="m-top-none"><?= ($stats['bestOfferRanking']['amount']) ? $stats['bestOfferRanking']['amount'] : 'brak ocen' ?></h2>
            <h5>Najwyżej oceniona oferta</h5>
            <?php if($stats['bestOfferRanking']['id']) { ?>
                <i class="fa fa-hand-o-right fa-lg"></i><span class="m-left-xs"><a href="<?= Url::to(['/svc/svcoffer/view', 'id' => $stats['bestOfferRanking']['id'] ]) ?>">Przejdź do ofery</a></span>
            <?php } ?>
            <div class="stat-icon">
                <i class="fa fa-trophy fa-3x"></i>
            </div>
            <!--<div class="refresh-button">
                <i class="fa fa-refresh"></i>
            </div>-->
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>
    </div><!-- /.col -->
    <div class="col-sm-6 col-md-3">
        <div class="panel-stat3 bg-success-brand">
            <h2 id="visitorCount" class="m-top-none"><?= $stats['bestOfferClicked']['amount'] ?></h2>
            <h5>Najczęściej klikana oferta</h5>
            <i class="fa fa-hand-o-right fa-lg"></i><span class="m-left-xs"><a href="<?= Url::to(['/svc/svcoffer/view', 'id' => $stats['bestOfferClicked']['id'] ]) ?>">Przejdź do ofery</a></span>
            <div class="stat-icon">
                <i class="fa fa-heart fa-3x"></i>
            </div>
            <!--<div class="refresh-button">
                <i class="fa fa-refresh"></i>
            </div>-->
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>
    </div><!-- /.col -->
</div>