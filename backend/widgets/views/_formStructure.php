<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<select id="zone-structure" multiple="multiple">
	<?php
		foreach($structure as $key => $value) {
			//echo '<option value="'.$value->id.'" data-section="'.$structure->zone_name.'" data-index="'.$value->id.'">'.$value->item_name.'</option>';
			foreach($value->children as $cKey => $cValue) {
				echo '<option value="'.$cValue->id.'" data-section="'.$value->name.'/'.$value->name.'" data-index="'.$cValue->id.'">'.$cValue->name.'</option>';
			}
		}
	?>
	<!--<option value="one" data-section="top" selected="selected" data-index="3">C++</option>
	<option value="two" data-section="top" selected="selected" data-index="1">Python</option>
	<option value="three" data-section="top" selected="selected" data-index="2">Ruby</option>
	<option value="four" data-section="top">Swift</option>
	<option value="wow" data-section="JavaScript/Library/Popular">jQuery</option>-->
</select>