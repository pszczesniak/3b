<div class="panel panel-default">	
    <div class="panel-heading clearfix">
        <span class="pull-left">Nowe opinie o portalu</span>
        <ul class="tool-bar">
            <!--<li><a data-original-title="Refresh" title="" data-placement="bottom" data-toggle="tooltip" class="refresh-widget" href="#"><i class="fa fa-refresh"></i></a></li>-->
            <li><a data-toggle="collapse" href="#lastComments" class="collapse-link collapse-window" ><i class="collapse-window-icon glyphicon glyphicon-menu-up"></i></a>
        </ul>
    </div>		
    <!--<div class="chat" id="lastComments">-->
    <div id="lastComments" class="panel-body chat collapse in">
        <ul class="chat-list">
            
            <?php
                if(count($comments) == 0) {
                    echo '<div class="alert alert-info">Brak opinii do akceptacji</div>';
                }
                foreach($comments as $key => $comment) {
                    echo '<li class="left clearfix">'
                            .'<span class="chat-img pull-left">'
                                .'<img src="/../images/default-user.png" alt="User Avatar">'
                            .'</span>'
                            .'<a href="'.\yii\helpers\Url::to(['/svc/svccomment/view', 'id' => $comment->id]).'"><div class="chat-body clearfix">'
                                .'<div class="header">'
                                    .'<strong class="primary-font">Klient</strong>'
                                    .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> '.$comment->created_at.'</small>'
                                .' </div>'
                                .'<p>'
                                    .$comment->comment
                                .'</p>'
                            .'</div></a>'
                        .'</li>'
                       /* .'<li class="right clearfix">'
                            .'<span class="chat-img pull-right">'
                                .'<img src="/../images/default-user.png" alt="User Avatar">'
                            .'</span>'
                            .'<div class="chat-body clearfix">'
                            .'<div class="header">'
                                .'<strong class="primary-font">Usługodawca</strong>'
                                .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>'
                            .'</div>'
                                .'<p>'
                                    .'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. '
                                .'</p>'
                            .'</div>'
                        .'</li>'*/;
                }
            ?>

        </ul>
    </div>
    <div class="loading-overlay">
        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
    </div>
</div><!-- /panel -->