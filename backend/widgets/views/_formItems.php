<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="ajax-table-items">	
	<div id="toolbar-<?= $tableId ?>" class="btn-group">
		<?= (!$view)?Html::a('<i class="glyphicon glyphicon-'.$createIcon.'"></i>'.$createLabel, /*Url::to(['/lsdd/lsddpackagesystemitem/create', 'id' => $model->id])*/$createUrl , 
					['class' => 'btn btn-'.$createStyle.' btn-icon '.$gridViewModal, 
					 'id' => $buttonId,
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => /*"#modal-grid-item"*/'#'.$modalId, 
					 'data-form' => "item-form", 
					 'data-table' => $tableId,//"table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".$createLabel
					]):'' ?>
        <?= $otherButtons ?>
		<!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
	</div>
	<div id="<?= $tableId ?>-info-message"></div>
    <table class="table table-striped table-items"  id=<?= $tableId ?>
		    data-toolbar="#toolbar-<?= $tableId ?>" 
			<?= $tableOptions ?>
			data-url=<?= $dataUrl ?>>
		<thead>
			<tr>
				<?php 
					foreach($columns as $key=>$column) {
						$data = (array_key_exists("data", $column))?$column['data']:'';
						echo '<th '.$data.'>'.$column['name'].'</th>';
					}
				?>
			</tr>
		</thead>
		<tbody class="ui-sortable">

		</tbody>
		 <!-- <tfoot>
			<tr>
			  <td colspan="4" class="form-inline col-xs-12" role="form">
				<div class="form-group"><input type="text" id="bed-description" class="form-control"></div>
				<div class="form-group"><button id="add-bed-btn" class="btn btn-sm btn-block btn-primary"><span class="glyphicon glyphicon-plus"></span>Add</button></div>
			  </td>
			</tr>
		  </tfoot>-->
		</table>


	<!-- Render modal form -->
	<?php
		yii\bootstrap\Modal::begin([
		  'header' => '<h4 class="modal-title btn-icon"><i class="glyphicon glyphicon-plus"></i>'.Yii::t('lsdd', 'New').'</h4>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => $modalId,//'modal-grid-item', // <-- insert this modal's ID
		  'class' => 'modal-grid'
		]);
	 
		echo '<div class="modalContent"></div>';

		yii\bootstrap\Modal::end();
	?> 
</div>