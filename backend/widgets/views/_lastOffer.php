<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <span class="pull-left">
            Najnowsze oferty 
        </span>
        <ul class="tool-bar">
            <!--<li><a data-original-title="Refresh" title="" data-placement="bottom" data-toggle="tooltip" class="refresh-widget" href="#"><i class="fa fa-refresh"></i></a></li>-->
            <li><a data-toggle="collapse" href="#lastOffers" class="collapse-link collapse-window" ><i class="collapse-window-icon glyphicon glyphicon-menu-up"></i></a>
        </ul>
    </div>
    <div id="lastOffers" class="panel-body no-padding collapse in">
        <ul class="list-group task-list no-margin collapse in">
            
            <?php
                $packages = [1 => 'info', 2 => 'warning', 3 => 'success'];
                foreach($offers as $key => $offer) {
                    echo '<li class="list-group-item">'
                            .' <label class="label-checkbox inline">'
                                // .' <input type="checkbox" class="task-finish">'
                                 .' <span class="custom-checkbox"></span>'
                            .' </label>'
                            .$offer->companyname.' <span class="label label-'.$packages[$offer->id_package_fk].' m-left-xs">'.$offer->package['name'].'</span>'
                            .' <span class="pull-right">'
                                .' <a class="task-del" href="'.\yii\helpers\Url::to(['/svc/svcoffer/view', 'id' => $offer->id]).'"><i class="fa fa-eye fa-lg text-info"></i></a>'
                           .' </span>'
                        .' </li>';
                }
            ?>
          
            
        </ul><!-- /list-group -->
    </div>
    <div class="loading-overlay">
        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
    </div>
</div><!-- /panel -->