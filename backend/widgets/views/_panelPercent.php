<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <span class="pull-left">Pakiety</span>
        <ul class="tool-bar">
            <!--<li><a data-original-title="Refresh" title="" data-placement="bottom" data-toggle="tooltip" class="refresh-widget" href="#"><i class="fa fa-refresh"></i></a></li>-->
            <li>
                <a data-toggle="collapse" href="#panelPercent" class="collapse-link collapse-window" ><i class="collapse-window-icon glyphicon glyphicon-menu-up"></i></a>
            </li>
        </ul>
    </div>	
    <div class="panel-body collapse in" id="panelPercent" >
        <?php
            $packages = [1 => 'info', 2 => 'warning', 3 => 'success'];
            foreach($stats as $key => $stat) {
                echo '<div style="height:8px; margin:5px 0 0 0;" class="progress progress-striped active">'
                        .'<div style="width: '.$stat['percent'].'%" class="progress-bar progress-bar-'.$packages[$stat['id']].'">'
                            .'<span class="sr-only">'.$stat['percent'].'% ofert</span>'
                        .'</div>'
                    .'</div>'
                    
                    .'<strong class="pull-left m-top-xs">'.$stat['percent'].'% ofert</strong>'
                    .'<strong class="pull-right m-top-xs">'.$stat['name'].'</strong>'
                    
                    .'<div class="clear"></div>';
            }
        ?>
        <!--<div style="height:8px; margin:5px 0 0 0;" class="progress progress-striped active">
            <div style="width: 65%" class="progress-bar progress-bar-info">
                <span class="sr-only">65% ofert</span>
            </div>
        </div>
        
        <strong class="pull-left m-top-xs">65% ofert</strong>
        <strong class="pull-right m-top-xs">Pakiet podstawowy</strong></strong>
        
        <div class="clear"></div>
       
        <div style="height:8px; margin:5px 0 0 0;" class="progress progress-striped active">
            <div style="width: 25%" class="progress-bar progress-bar-success">
                <span class="sr-only">25% ofert</span>
            </div>
        </div>
        
        <strong class="pull-left m-top-xs">25% ofert</strong>
        <strong class="pull-right m-top-xs">Pakiet standardowy</strong>
        
        <div class="clear"></div>
       
        <div style="height:8px; margin:5px 0 0 0;" class="progress progress-striped active">
            <div style="width: 10%" class="progress-bar progress-bar-danger">
                <span class="sr-only">10% ofert</span>
            </div>
        </div>
        
        <strong class="pull-left m-top-xs">10% ofert</strong>
        <strong class="pull-right m-top-xs">Pakiet rozszerzony</strong>-->
    </div>
</div>     