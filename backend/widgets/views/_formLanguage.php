<?php

use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<span> <span class="flag flag-<?= $lang ?> "></span><?= $langName ?> <i class="caret"></i></span>
</a>

<ul class="dropdown-menu dropdown-menu-right">
	<?php if($lang != 'pl') { ?> <li> <a class="switch-lang" data-lang='pl' href="<?= Url::current([ 'language' => 'pl']) ?>" ><span class="flag flag-pl"></span>Polish  </a> </li>  <?php } ?>
	<?php if($lang != 'en') { ?> <li> <a class="switch-lang" data-lang='en' href="<?= Url::current([ 'language' => 'en']) ?>" ><span class="flag flag-gb"></span>English </a> </li>  <?php } ?>
	<?php if($lang != 'de') { ?> <li> <a class="switch-lang" data-lang='de' href="<?= Url::current([ 'language' => 'de']) ?>" ><span class="flag flag-de"></span>Deutsch </a> </li>  <?php } ?>
	<?php if($lang != 'ru') { ?> <li> <a class="switch-lang" data-lang='ru' href="<?= Url::current([ 'language' => 'ru']) ?>" ><span class="flag flag-ru"></span>Russian </a> </li>  <?php } ?>
</ul>