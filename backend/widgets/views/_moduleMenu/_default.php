<?php
	use yii\helpers\Url;
?>
<nav class="l-navbar js-nav">
    <ul class="l-navbar__menu">
        <?php if(!Yii::$app->user->isGuest) { ?>
            <li class="l-navbar__item">
                <div class="block block--nopad block--col-center block--avatar">
                    <figure class="avatar">
                        <?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".\Yii::$app->session->get('user.employeeId')."/preview.jpg"))?"/uploads/employees/cover/".\Yii::$app->session->get('user.employeeId')."/preview.jpg":"/images/default-user.png"; ?>
                        <img src="<?= \common\models\Avatar::getAvatar(Yii::$app->user->id) ?>" alt="avatar" class="avtr">
                    </figure>
                    <div>
                        <h6 class="avatar__title"><b><?= (Yii::$app->user->identity->fullname) ? Yii::$app->user->identity->fullname : Yii::$app->user->identity->username ?></b></h6>
                        <p class="avatar__desc text-center">
                            <!--<a href="javascript:;" title="TITLE"><i class="fa fa-envelope-o"></i></a>
                            <a href="javascript:;" title="TITLE"><i class="fa fa-comments"></i></a>-->
                            <a href="<?= Url::to(['/task/personal/task', 'id' => 0]) ?>" title="Zarejestruj czynność" data-title="<i class='fa fa-calendar-plus-o'></i>Rejestracja czynności" data-target="#modal-grid-item" class="gridViewModal"><i class="fa fa-calendar-plus-o text--purple"></i></a>
                            <a href="<?= Url::to(['/company/default/dashboard']) ?>" title="Twój panel"><i class="fa fa-dashboard"></i></a>
                            <a href="<?= Url::to(['/task/calendar/personal']) ?>" title="Kalendarz osobisty"><i class="fa fa-calendar-alt"></i></a>
                            <!--<a href="<?= Url::to(['/community/meeting/index']) ?>" title="Spotkania"><i class="fa fa-handshake-o"></i></a>-->
                            <!--<a href="<?= Url::to(['/timeline/manager', 'date' => strtotime(date('Y-m-d H').':00:00')]) ?>" title="Historia zdarzeń"><i class="fa fa-history"></i></a>-->
                            <a href="<?= Url::to(['/site/logout']) ?>" title="Wyloguj się"><i class="fa fa-power-off icon--red"></i></a>
                        </p>
                    </div>
                </div>
            </li>
           
            <?php if($pass) { ?>
				<!--<li class="l-navbar__item">
					<a class="l-navbar__link <?= ((Yii::$app->controller->action->id == 'dashboard' ) ? 'active' : '') ?>" href="<?= Url::to(['/company/default/dashboard']) ?>" title="Homepage">
						<i class="fa fa-home"></i>  Twój panel 
					</a>
				</li>-->
                <?php  if(count(array_intersect(["eventPanel", "grantAll"], $grants)) > 0) { ?> 
				<li class="l-navbar__item">
                    <a class="l-navbar__link l-navbar__link--sub js-submenu <?= ( in_array(Yii::$app->controller->module->id, ['eg', 'svc']) ) ? ' open' : '' ?>" href="#" title="Biuro">
                        <i class="fa fa-archive"></i> Biuro
                        <svg class="sub-icon" height="16px" width="16px"><use xlink:href="/images/svg-defs.svg#icon-arrow-right"/></svg>
                    </a>
                    <ul class="l-navbar__sub-menu <?= (in_array(Yii::$app->controller->module->id, ['eg', 'svc'])  ? 'active' : '') ? ' open' : 'l-navbar__sub-menu--close' ?> ">
                        <li>
                            <a href="<?= Url::to(['/eg/calendar/index']) ?>" <?= (Yii::$app->controller->id == 'calendar') ? 'class="active"' : '' ?> title="">Kalendarz</a>
                        </li>
                        <?php  if(count(array_intersect(["casePanel", "grantAll"], $grants)) > 0) { ?> 
                            <li>
                                <a href="<?= Url::to(['/eg/patient/index']) ?>" <?= (Yii::$app->controller->id == 'patient') ? 'class="active"' : '' ?> title="">Klienci</a>
                                <div class="l-navbar__sub-menu__icons">
								<?php  if(count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0) { ?> 
								<a href="<?= Url::to(['/eg/patient/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                                <?php } ?>
								</div>
                            </li>
                        <?php } ?>
                        <?php  if(count(array_intersect(["eventPanel", "grantAll"], $grants)) > 0) { ?> 
                            <li>
                                <a href="<?= Url::to(['/eg/visit/index']) ?>" <?= (Yii::$app->controller->id == 'visit') ? 'class="active"' : '' ?> title="">Terminy</a>
                                <div class="l-navbar__sub-menu__icons">
									<?php  if(count(array_intersect(["eventAdd", "grantAll"], $grants)) > 0) { ?>
										<a href="<?= Url::to(['/offer/template/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
									<?php } ?>
								</div>
                            </li>
                        <?php } ?>
                        <li>
                            <a href="<?= Url::to(['/eg/service/index']) ?>" <?= (Yii::$app->controller->id == 'service') ? 'class="active"' : '' ?> title="">Usługi</a>
                            <div class="l-navbar__sub-menu__icons">
                            <?php  if(count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0) { ?> 
                            <a href="<?= Url::to(['/eg/service/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                            <?php } ?>
                            </div>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/svc/svcpackage/index']) ?>" <?= (Yii::$app->controller->id == 'svcpackage' || Yii::$app->controller->id == 'svcpackageoption') ? 'class="active"' : '' ?> title="">Pakiety</a>
                            <div class="l-navbar__sub-menu__icons">
                            <?php  if(count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0) { ?> 
                            <a href="<?= Url::to(['/svc/svcpackage/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                            <?php } ?>
                            <a href="<?= Url::to(['/svc/svcpackageoption/index']) ?>" class="btn btn-xs bg-pink pull-right sidebar-add"><i class="fa fa-tasks"></i></a>
                            </div>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/eg/type/index']) ?>" <?= (Yii::$app->controller->id == 'type') ? 'class="active"' : '' ?> title="">Rodzaje imprez</a>
                            <div class="l-navbar__sub-menu__icons">
                            <?php  if(count(array_intersect(["caseAdd", "grantAll"], $grants)) > 0) { ?> 
                            <a href="<?= Url::to(['/eg/type/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                            <?php } ?>
                            </div>
                        </li>
                    </ul>
                </li>
				<?php } ?>
                <li class="l-navbar__item">
                    <a class="l-navbar__link l-navbar__link--sub js-submenu <?= ( in_array(Yii::$app->controller->module->id, ['cms']) ) ? ' open' : '' ?>" href="#" title="Zarządzanie stroną">
                        <i class="fa fa-globe"></i> Zarządzanie stroną
                        <svg class="sub-icon" height="16px" width="16px"><use xlink:href="/images/svg-defs.svg#icon-arrow-right"/></svg>
                    </a>
                    <ul class="l-navbar__sub-menu <?= (in_array(Yii::$app->controller->module->id, ['cms'])  ? 'active' : '') ? ' open' : 'l-navbar__sub-menu--close' ?> ">
                        <li>
                            <a href="<?= Url::to(['/cms/cmspage/update', 'id' => 1]) ?>" <?= (Yii::$app->controller->id == 'calendar') ? 'class="active"' : '' ?> title="">Strona główna</a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/cms/cmspage/index']) ?>" <?= (Yii::$app->controller->id == 'cmspage' || Yii::$app->controller->id == 'cmscategory') ? 'class="active"' : '' ?> title="">Rejestr stron</a>
                            <div class="l-navbar__sub-menu__icons">
                                <a href="<?= Url::to(['/cms/cmspage/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                                <a href="<?= Url::to(['/cms/cmscategory/index']) ?>" class="btn btn-xs bg-pink pull-right sidebar-add"><i class="fa fa-th"></i></a>
                            </div>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/cms/cmsmenu/index']) ?>" <?= (Yii::$app->controller->id == 'cmsmenu') ? 'class="active"' : '' ?> title="">Nawigacja</a>
                            <div class="l-navbar__sub-menu__icons">
                                <?php  if(count(array_intersect(["eventAdd", "grantAll"], $grants)) > 0) { ?>
                                    <a href="<?= Url::to(['/cms/cmsmenu/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                                <?php } ?>
                            </div>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/cms/cmsgallery/index']) ?>" <?= (Yii::$app->controller->id == 'cmsgallery') ? 'class="active"' : '' ?> title="">Galerie</a>
                            <div class="l-navbar__sub-menu__icons">
                                <a href="<?= Url::to(['/cms/cmsgallery/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                            </div>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/cms/cmswidget/index']) ?>" <?= (Yii::$app->controller->id == 'cmswidget') ? 'class="active"' : '' ?> title="">Dodatki</a>
                            <div class="l-navbar__sub-menu__icons">
                                <a href="<?= Url::to(['/cms/cmswidget/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add"><i class="fa fa-plus"></i></a>
                            </div>
                        </li>
                        <!--<li>
                            <a href="<?= Url::to(['/cms/default/files']) ?>" <?= (Yii::$app->controller->id == 'default') ? 'class="active"' : '' ?> title="">Pliki</a>
                        </li>-->
                    </ul>
                </li>
				<li class="l-navbar__item">
					<a class="l-navbar__link <?= ((((Yii::$app->controller->id == 'calendar' && Yii::$app->controller->module->id == 'task') || Yii::$app->controller->id == 'meeting') || Yii::$app->controller->id == 'event') ? 'active' : '') ?>"  href="<?= Url::to(['/task/calendar/personal']) ?>" title="Kalendarz ">
						<i class="fa fa-calendar-alt"></i>  Kalendarz osobisty
					</a>
                    <div class="l-navbar__menu__icons">
                        <a href="<?= Url::to(['/community/meeting/index']) ?>" class="btn btn-xs bg-orange pull-right sidebar-add" title="Kalendarz spotkań"><i class="far fa-handshake"></i></a>
                        <!--<a href="<?= Url::to(['/task/calendar/index']) ?>" class="btn btn-xs bg-grey pull-right sidebar-add" title="Kalendarz firmowy"><i class="fa fa-building"></i></a>-->
                    </div>
				</li>
				<li class="l-navbar__item">
                    <a class="l-navbar__link l-navbar__link--sub js-submenu <?= (Yii::$app->controller->module->id == 'company') ? ' open' : '' ?>" href="#" title="Firma">
                        <i class="fa fa-building"></i>  Firma
                        <svg class="sub-icon" height="16px" width="16px"><use xlink:href="/images/svg-defs.svg#icon-arrow-right"/></svg>
                    </a>
                    <ul class="l-navbar__sub-menu <?= (Yii::$app->controller->module->id == 'company') ? ' open' : 'l-navbar__sub-menu--close' ?> ">
                        <li><a href="<?= Url::to(['/company/company/view', 'id' => 1]) ?>" <?= (Yii::$app->controller->id == 'company') ? 'class="active"' : '' ?> title="">Dane</a></li>
						<li><a href="<?= Url::to(['/company/docs/folder']) ?>" <?= (Yii::$app->controller->id == 'docs') ? 'class="active"' : '' ?> title="">Dokumenty</a></li>
                        <!--<li><a href="<?= Url::to(['/company/employee/contacts']) ?>" <?= (Yii::$app->controller->id == 'employee' &&  Yii::$app->controller->action->id == 'contacts') ? 'class="active"' : '' ?> title="">Książka kontaktów</a></li>-->
                        <?php  if(count(array_intersect(["departmenPanel", "grantAll"], $grants)) == -1) { ?> <li><a href="<?= Url::to(['/company/department/index']) ?>" <?= (Yii::$app->controller->id == 'department') ? 'class="active"' : '' ?> title="">Działy</a></li> <?php } ?>
                        <?php  if(count(array_intersect(["employeePanel", "grantAll"], $grants)) > 0) { ?> <li><a href="<?= Url::to(['/company/employee/index']) ?>" <?= (Yii::$app->controller->id == 'employee' &&  Yii::$app->controller->action->id != 'contacts') ? 'class="active"' : '' ?> title="">Pracownicy</a></li> <?php } ?>
                    </ul>
                </li>
                <!--<li class="l-navbar__item">
					<a class="l-navbar__link <?= ((Yii::$app->controller->module->id == 'crm' ) ? 'active' : '') ?>" href="<?= Url::to(['/crm/customer/index']) ?>" title="Kontrahenci">
						<i class="fa fa-address-card"></i> Kontrahenci
					</a>
                    <div class="l-navbar__menu__icons">
                        <a href="<?= Url::to(['/crm/crmgroup/index']) ?>" class="btn btn-xs bg-purple pull-right sidebar-add" title="Grupy kontrahentów"><i class="fa fa-tags"></i></a>
                        <a href="<?= Url::to(['/crm/customer/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add" title="Nowy kontrahent"><i class="fa fa-plus"></i></a>
                    </div>
				</li>
                <li class="l-navbar__item">
					<a class="l-navbar__link <?= ((Yii::$app->controller->module->id == 'task' && Yii::$app->controller->id == 'project') ? 'active' : '') ?>" href="<?= Url::to(['/task/project/index']) ?>" title="Sprawy">
						<i class="fa fa-folder-open"></i> Sprawy
					</a>
                    <div class="l-navbar__menu__icons">
                        <a href="<?= Url::to(['/task/project/create']) ?>" class="btn btn-xs bg-green pull-right sidebar-add" title="Nowa sprawa"><i class="fa fa-plus"></i></a>
                    </div>
				</li>-->
                <?php  if(count(array_intersect(["permissionPanel", "dictionaryPanel", "grantAll"], $grants)) > 0) { ?>
                <li class="l-navbar__item">
                    <a class="l-navbar__link l-navbar__link--sub js-submenu <?= ( in_array(Yii::$app->controller->id, ['admin', 'dictionary', 'types']) ) ? ' open' : '' ?>" href="#" title="Administracja">
                        <i class="fa fa-cogs"></i>  Administracja
                        <svg class="sub-icon" height="16px" width="16px"><use xlink:href="/images/svg-defs.svg#icon-arrow-right"/></svg>
                    </a>
                    <ul class="l-navbar__sub-menu <?= ( in_array(Yii::$app->controller->id, ['admin', 'dictionary', 'types']) ) ? ' open' : 'l-navbar__sub-menu--close' ?> ">
                        <?php  if(count(array_intersect(["permissionPanel", "grantAll"], $grants)) == -1) { ?> <li><a href="<?= Url::to(['/company/admin/index']) ?>" <?= (Yii::$app->controller->id == 'admin') ? 'class="active"' : '' ?> title="">Uprawnienia</a></li> <?php } ?>
                        <?php  if(count(array_intersect(["dictionaryPanel", "grantAll"], $grants)) > 0) { ?> 
                            <li>
                                <a href="<?= Url::to(['/dict/dictionary/index']) ?>" <?= (Yii::$app->controller->id == 'dictionary') ? 'class="active"' : '' ?> title="">Słowniki</a>
                                <div class="l-navbar__menu__icons">
                                    <a href="<?= Url::to(['/dict/task/index']) ?>" class="btn btn-xs bg-teal pull-right sidebar-add" title="Typy zadań"><i class="fa fa-tasks"></i></a>
                                </div>
                            </li> 
                            
                        <?php } ?>
                        <!--<li><a href="<?= Url::to(['/document/types/index']) ?>" <?= (Yii::$app->controller->id == 'types') ? 'class="active"' : '' ?> title="">Kategorie dokumentów</a></li> -->                      
                        <?php  if(count(array_intersect(["permissionPanel", "grantAll"], $grants)) > 0) { ?> 
                            <li><a href="<?= Url::to(['/logs/index']) ?>" <?= (Yii::$app->controller->id == 'logs') ? 'class="active"' : '' ?> title="">Logi</a></li> 
                            <!--<li><a href="<?= Url::to(['/timeline/manager', 'date' => strtotime(date('Y-m-d H').':00:00')]) ?>" <?= (Yii::$app->controller->id == 'timeline' && Yii::$app->controller->action->id == 'manager') ? 'class="active"' : '' ?> title="">Aktywność</a></li>-->
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
            <li class="l-navbar__item">
                <a class="l-navbar__link" href="<?= Url::to(['/site/login']) ?>" title="Logowanie">
                    <i class="fa fa-lock"></i>  Zaloguj się
                </a>
            </li>
        <?php } ?>
    </ul>
</nav>