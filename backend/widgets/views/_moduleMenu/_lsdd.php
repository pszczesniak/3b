<?php
	use yii\helpers\Url;
	use backend\widgets\SideMenu;
?>
<?php 
	echo SideMenu::widget([
		'type' => SideMenu::TYPE_DEFAULT,
		'heading' => '',
		'options' => ['class'=>'sidebar-menu'],
		'activeCssClass' => 'activeItem',
		'items' => [
			[
				'url' => Url::to(['/lsdd/default/index']),
				'label' => 'Panel startowy',
				'icon' => 'dashboard',
				'active'=>(Yii::$app->controller->id == 'default'),
				'visible' => !Yii::$app->user->isGuest

			],
			[
				'label' => Yii::t('lsdd', 'Recipies'),
				'icon' => 'filter',
				'items' => [
					/*['label'=>Yii::t('lsdd', 'Recipe accepted / produced'), 'url' => Url::to(['/lsdd/lsddrecipe/index']), 'visible'=>( in_array("viewsRecipe", $grants) || in_array("padre", $grants) ) ],
					['label' => '', 'options' => ['class' => 'divider'], 'visible'=>( in_array("viewsRecipe", $grants) || in_array("padre", $grants) ) ],*/
					['label'=>Yii::t('lsdd', 'Manage Recipes'), 'url' => Url::to(['/lsdd/lsddrecipe/index']),  'visible'=>( count(array_intersect(["viewsRecipe", "padre"], $grants)) > 0 ) ],
					['label'=>Yii::t('lsdd', 'Create Recipe'),  'url' => Url::to(['/lsdd/lsddrecipe/create']), 'visible'=>( count(array_intersect(["createRecipe", "padre"], $grants)) > 0 ) ],
					['label'=>Yii::t('lsdd', 'Mixing Recipes'), 'url' => Url::to(['/lsdd/lsddrecipe/mixing']), 'visible'=>( count(array_intersect(["mixingRecipe", "padre"], $grants)) > 0 ) ],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label'=>Yii::t('lsdd', 'Manage Recipe Types'), 'url' => Url::to(['/lsdd/lsddrecipetype/index']), 'visible'=>( count(array_intersect(["viewsRecipeType", "padre"], $grants)) > 0 ) ],
					['label'=>Yii::t('lsdd', 'Manage Recipe Groups'), 'url' => Url::to(['/lsdd/lsddrecipegroup/index']),  'visible'=>( count(array_intersect(["viewsRecipeGroup", "padre"], $grants)) > 0 ) ],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label'=>Yii::t('lsdd', 'Recipies in the construction'), 'url' => Url::to(['/lsdd/lsddrecipe/index']),  'visible'=>( count(array_intersect(["viewsRecipe", "padre"], $grants)) > 0 ) ],
					['label'=>Yii::t('lsdd', 'Mixing in the construction'), 'url' => Url::to(['/lsdd/lsddrecipe/index']),  'visible'=>( count(array_intersect(["mixingRecipe", "padre"], $grants)) > 0 ) ],
				],
				'active' => (Yii::$app->controller->id == 'lsddrecipe' || Yii::$app->controller->id == 'lsddrecipetype' || Yii::$app->controller->id == 'lsddrecipegroup'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Materials'),
				'icon' => 'barcode',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Materials'), 'url' => Url::to(['/lsdd/lsddmaterial/index']),  'visible'=>( count(array_intersect(["viewsMaterial", "padre"], $grants)) > 0 ) ],
					['label' => Yii::t('lsdd', 'Create Material'), 'url' => Url::to(['/lsdd/lsddmaterial/create']),  'visible'=>( count(array_intersect(["createMaterial", "padre"], $grants)) > 0 ) ],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label' => Yii::t('lsdd', 'Manage Material Functions'), 'url' => Url::to(['/lsdd/lsddmaterialfunction/index']),  'visible'=>( count(array_intersect(["viewsMaterialFunction", "padre"], $grants)) > 0 ) ],
					['label' => Yii::t('lsdd', 'Manage Material Actions'), 'url' => Url::to(['/lsdd/lsddmaterialaction/index']),  'visible'=>( count(array_intersect(["viewsMaterialAction", "padre"], $grants)) > 0 ) ],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label' => Yii::t('lsdd', 'Manage Colors System'), 'url' => Url::to(['/lsdd/lsddcolorsystem/index']),  'visible'=>( count(array_intersect(["viewsColorSystem", "padre"], $grants)) > 0 ) ],	
					['label' => Yii::t('lsdd', 'Create Color System'), 'url' => array('/lsdd/lsddcolorsystem/create'),  'visible'=>( count(array_intersect(["createColorSystem", "padre"], $grants)) > 0 ) ],
				],
				'active' => (Yii::$app->controller->id == 'lsddmaterial' || Yii::$app->controller->id == 'lsddmaterialaction' ||Yii::$app->controller->id == 'lsddmaterialfunction' || Yii::$app->controller->id == 'lsddcolorsystem'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Packages'),
				'icon' => 'gift',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Packages'), 'url' => Url::to(['/lsdd/lsddpackage/index']),  'visible'=>( count(array_intersect(["viewsPackage", "padre"], $grants)) > 0 ) ],
					['label' => Yii::t('lsdd', 'Create Package'), 'url' => Url::to(['/lsdd/lsddpackage/index', 'create' => true]),  'visible'=>( count(array_intersect(["createPackage", "padre"], $grants)) > 0 ) ],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label' => Yii::t('lsdd', 'Manage Packages System'), 'url' => Url::to(['/lsdd/lsddpackagesystem/index']), 'visible'=>( count(array_intersect(["viewsPackageSystem", "padre"], $grants)) > 0 ) ],
					['label' => Yii::t('lsdd', 'Create Package System'), 'url' => Url::to(['/lsdd/lsddpackagesystem/create']), 'visible'=>( count(array_intersect(["createColorSystem", "padre"], $grants)) > 0 ) ],
				],
				'active' => (Yii::$app->controller->id == 'lsddpackage' || Yii::$app->controller->id == 'lsddpackagesystem'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Companies'),
				'icon' => 'user',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Companies'),  'url'=> Url::to(['/lsdd/lsddcompany/index']), 'visible'=>( count(array_intersect(["viewsCompany", "padre"], $grants)) > 0 )],
					['label' => Yii::t('lsdd', 'Create Company'), 'url'=> Url::to(['/lsdd/lsddcompany/create']), 'visible'=>( count(array_intersect(["createCompany", "padre"], $grants)) > 0 )],
				],
				'active' => (Yii::$app->controller->id == 'lsddcompany'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Suppliers'),
				'icon' => 'user',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Suppliers'),  'url'=> Url::to(['/lsdd/lsddsupplier/index']),  'visible'=>( count(array_intersect(["viewsSupplier", "padre"], $grants)) > 0 )],
					['label' => Yii::t('lsdd', 'Create Supplier'), 'url'=> Url::to(['/lsdd/lsddsupplier/create']),  'visible'=>( count(array_intersect(["createSupplier", "padre"], $grants)) > 0 )],
				],
				'active' => (Yii::$app->controller->id == 'lsddsupplier'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Projects'),
				'icon' => 'folder-open',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Projects'), 'url'=> Url::to(['/lsdd/lsddproject/index']),  'visible'=>( count(array_intersect(["viewsProject", "padre"], $grants)) > 0 ) ],
					['label' => Yii::t('lsdd', 'Create Project'), 'url'=> Url::to(['/lsdd/lsddproject/create']),  'visible'=>( count(array_intersect(["createProject", "padre"], $grants)) > 0 ) ],
				],
				'active' => (Yii::$app->controller->id == 'lsddproject'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => Yii::t('lsdd', 'Production'),
				'icon' => 'cog',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage production'), 'url'=> Url::to(['/lsdd/lsddproduction/index']),  'visible'=>( count(array_intersect(["viewsProduction", "padre"], $grants)) > 0 )],
					['label' => Yii::t('lsdd', 'Create production'), 'url'=> Url::to(['/lsdd/lsddproduction/create']),  'visible'=>( count(array_intersect(["createProduction", "padre"], $grants)) > 0 )],
				],
				'active' => (Yii::$app->controller->id == 'lsddproduction'),
				'visible' => !Yii::$app->user->isGuest
			],
			[
				'label' => 'Admin',
				'icon' => 'user',
				'items' => [
					['label' => Yii::t('lsdd', 'Manage Zones'),  'url'=> Url::to(['/lsdd/lsddzone/index']), 'active' => (Yii::$app->controller->id == 'lsddzone' ),  'visible'=>( count(array_intersect(["viewsZone", "padre"], $grants)) > 0 )],
					['label' => Yii::t('lsdd', 'Create Zone'),  'url'=> Url::to(['/lsdd/lsddzone/create']), 'active' => (Yii::$app->controller->id == 'lsddzone' ),  'visible'=>( count(array_intersect(["createZone", "padre"], $grants)) > 0 )],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label' => 'Użytkownicy',  'url'=> Url::to(['/lsdd/lsdduser/index']), 'active' => (Yii::$app->controller->id == 'lsdduser' ),],
					['label' => 'Nowy użytkownik',  'url'=> Url::to(['/lsdd/lsdduser/create']), 'active' => (Yii::$app->controller->id == 'lsdduser' ),],
					['label' => '', 'options' => ['class' => 'divider'] ],
					['label' => 'Grupy użytkowników',  'url'=> Url::to(['/lsdd/lsdduserrole/index']), 'active' => (Yii::$app->controller->id == 'lsdduserrole' ),],
					/*['label' => 'Użytkownicy',  'url'=> Url::to(['/admin/assignment/index']), 
					  'active' => (Yii::$app->controller->id == 'assignment' ),],
					['label' => 'Role', 'url'=> Url::to(['/admin/role/index']), 'active' => (Yii::$app->controller->id == 'role' ),],
					['label' => 'Uprawnienia', 'url'=> Url::to(['/admin/permission/index']), 'active' => (Yii::$app->controller->id == 'permission' ),],*/
				],
				'visible' => !Yii::$app->user->isGuest,
				'active' => (Yii::$app->controller->id == 'lsdduser' || Yii::$app->controller->id == 'lsdduserrole' || Yii::$app->controller->id == 'lsddzone' ),
			],
			[
				'label' => 'Help',
				'icon' => 'question-sign',
				'items' => [
					['label' => 'About', 'icon'=>'info-sign', 'url'=>'#'],
					['label' => 'Contact', 'icon'=>'phone', 'url'=>'#'],
				],
				'visible' => !Yii::$app->user->isGuest
			],
		],
	]);
?>