<?php
	use yii\helpers\Url;
	use backend\widgets\SideMenu;
?>
<?php
	echo SideMenu::widget([
		'type' => SideMenu::TYPE_DEFAULT,
		'heading' => '',
		'options' => ['class'=>'sidebar-menu'],
		'activeCssClass' => 'activeItem',
		'items' => [
			[
				'url' => Url::to(['/site/index']),
				'label' => 'Panel główny',
				'icon' => 'dashboard',
				'active' => (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'),
				'visible' => !Yii::$app->user->isGuest

			],
            [
				'url' => Url::to(['/site/configuration']),
				'label' => 'Konfiguracja',
				'icon' => 'cog',
				'active' => (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'configuration'),
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin"], $grants)) > 0)
			],
            [
				'label' => 'Aplikacja',
				//'icon' => 'user',
				'url' =>false,
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["appCustomerPanel", "appLoanPanel"], $grants)) > 0),
                'options' => ['class'=>'item-caption'],
			],
            [
				'label' => 'Klienci i wnioski',
				'icon' => 'user',
				'items' => [
					['label' => 'Rejestr klientów', 'url'=> Url::to(['/apl/aplcustomer/index']), 'active' => (Yii::$app->controller->id == 'aplcustomer' ), 'visible' => (count(array_intersect(["appCustomerPanel"], $grants)) > 0) ],
                    ['label' => 'Rejestr wniosków', 'url'=> Url::to(['/apl/aplloan/index']), 'active' => (Yii::$app->controller->id == 'aplloan' ), 'visible' => (count(array_intersect(["appLoanPanel"], $grants)) > 0)],
                   // ['label' => '', 'options' => ['class' => 'divider'], ],
                   // ['label' => 'Słownik - wykształcenie', 'url'=> Url::to(['/svc/svceducation/index']), 'active' => (Yii::$app->controller->id == 'svceducation' ),],
                   // ['label' => 'Słownik - branża', 'url'=> Url::to(['/svc/svctrade/index']), 'active' => (Yii::$app->controller->id == 'svctrade' ),],
				],
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "appCustomerPanel", "appLoanPanel"], $grants)) > 0),
                //'options' => ['class'=>'bg-green'],
			],
            [
				'url' => Url::to(['/mix/comment/manage']),
				'label' => 'Komentarze',
				'icon' => 'comments',
				'options' => ['class'=>'treeview'],
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin"], $grants)) > 0),
				'active' => (Yii::$app->controller->id == 'comment' && in_array(Yii::$app->controller->action->id, ['manage']) )
			],
            [
				'url' => Url::to(['/dict/dictionary/index']),
				'label' => 'Słowniki',
				'icon' => 'bookmark',
				'options' => ['class'=>'treeview'],
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin"], $grants)) > 0),
				'active' => (Yii::$app->controller->id == 'dictionary' )
			],
            [
				'label' => 'Zarządzanie stroną',
				//'icon' => 'user',
				'url' =>false,
                'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "pagePanel", "pageAdd", "categoryPanel", "widgetManage", "menuManage", "galleryManage"], $grants)) > 0), 
                'options' => ['class'=>'item-caption'],
			],
            [
				'label' => 'Treść serwisu',
				'icon' => 'font',
				'items' => [
					['label' => 'Kreator strony',  'url'=> Url::to(['/cms/cmspage/create']),'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["pageAdd"], $grants)) > 0)],
					['label' => 'Zarządzanie stronami', 'url'=> Url::to(['/cms/cmspage/index']),'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["pagePanel"], $grants)) > 0)],
					['label' => 'Zarządzanie kategoriami', 'url'=> Url::to(['/cms/cmscategory/index']),'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["categoryPanel"], $grants)) > 0)],
				],
				'active' => (Yii::$app->controller->id == 'cmspage' || Yii::$app->controller->id == 'cmscategory'),
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "pagePanel", "categoryPanel", "pageAdd"], $grants)) > 0)
			],
			[
				'label' => 'Menu',
				'icon' => 'list',
				'items' => [
					['label' => 'Kreator menu',  'url'=> Url::to(['/cms/cmsmenu/create'])],
					['label' => 'Zarządzanie nawigacją', 'url'=> Url::to(['/cms/cmsmenu/index'])],
				],
				'active' => (Yii::$app->controller->id == 'cmsmenu'),
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "menuManage"], $grants)) > 0)
			],
			[
				'label' => 'Galerie',
				'icon' => 'image',
				'items' => [
					['label' => 'Kreator galeri',  'url'=> Url::to(['/cms/cmsgallery/create'])],
					['label' => 'Zarządzanie galeriami', 'url'=> Url::to(['/cms/cmsgallery/index'])],
				],
				'active' => (Yii::$app->controller->id == 'cmsgallery'),
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "galleryManage"], $grants)) > 0)
			],
			[
				'label' => 'Dodatki',
				'icon' => 'cubes',
				'items' => [
					['label' => 'Kreator dodatku',  'url'=> Url::to(['/cms/cmswidget/create'])],
					['label' => 'Zarządzanie dodatkami', 'url'=> Url::to(['/cms/cmswidget/index'])],
				],
				'active' => (Yii::$app->controller->id == 'cmswidget'),
				'visible' => (!Yii::$app->user->isGuest && count(array_intersect(["admin", "widgetManage"], $grants)) > 0)
			], 
            /*[
				'url' => '#',//Url::to(['/cms/default/index']),
				'label' => 'Newsletter',
				'icon' => 'envelope',
                'items' => [
					['label' => 'Szablony', 'url'=> Url::to(['/newsletter/newslettertemplate/index']), 'active' => (Yii::$app->controller->id == 'newslettertemplate' ),],
					['label' => 'Subskrybenci',  'url'=> Url::to(['/newsletter/newslettersubscribe/index']),   'active' => (Yii::$app->controller->id == 'newslettersubscribe' ),],
					['label' => 'Grupy', 'url'=> Url::to(['/newsletter/newslettergroup/index']), 'active' => (Yii::$app->controller->id == 'newslettergroup' ),],
				],
				//'active' => (Yii::$app->module->id == 'blog'),
				'visible' => !Yii::$app->user->isGuest

			],*/
			[
				'url' => Url::to(['/cms/default/files']),
				'label' => 'Pliki',
				'icon' => 'file',
				'options' => ['class'=>'treeview'],
				'visible' => !Yii::$app->user->isGuest,
				'active' => (Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'files'),
			],
			[
				'label' => 'Admin',
				'icon' => 'users',
				'items' => [
					['label' => 'Użytkownicy',  'url'=> Url::to(['/admin/user/index']),  'active' => (Yii::$app->controller->id == 'user' ),],
					//['label' => 'Role', 'url'=> Url::to(['/admin/role/index']), 'active' => (Yii::$app->controller->id == 'role' ),],
					//['label' => 'Uprawnienia', 'url'=> Url::to(['/admin/permission/index']), 'active' => (Yii::$app->controller->id == 'permission' ),],
				],
				'visible' =>  (!Yii::$app->user->isGuest && count(array_intersect(["admin", "userPanel"], $grants)) > 0)
			],
            /*[
				'url' => Url::to(['/facebook/index']),
				'label' => 'Socialmedia',
				'icon' => 'globe',
				'options' => ['class'=>'treeview'],
				'visible' => !Yii::$app->user->isGuest,
				'active' => (Yii::$app->controller->id == 'facebook' ),
			],*/
			[
				'label' => 'Pomoc',
				'icon' => 'life-ring',
				'items' => [
					['label' => 'O systemie', 'url'=>'#'],
					['label' => 'Kontakt', 'url'=>'#'],
				],
				'visible' => !Yii::$app->user->isGuest
			],
		],
	]);
	//$options = ['class' => 'sidebar-menu nav nav-pills nav-stacked kv-sidenav'];
	//Html::removeCssClass($options, 'nav nav-pills nav-stacked kv-sidenav');
	//Html::addCssClass($options, 'sidebar-menu');
?>