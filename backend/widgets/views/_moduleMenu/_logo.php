<?php
	use yii\helpers\Html;
?>

<a href="/admin/dashboard" class="logo"> 
<?php
	if ($zone->getBehavior('coverBehavior')->hasImage()) {
		echo Html::img($zone->getBehavior('coverBehavior')->getUrl('medium'));
	}
?>
</a>