<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="ajax-table-items">	
	<div id="toolbar-files-<?= $type ?>">  <span class="lead"><u>Dokumenty</u></span> </div>
	
	<table  class="table table-striped table-items"  id="table-files-<?= $type ?>"
			data-toolbar="#toolbar-files-<?= $type ?>" 
			data-toggle="table" 
			data-show-refresh="true",
			data-filter-control="true"
			data-url=<?= Url::to(['/files/index', 'id'=>$id, 'type'=>$type]) ?>>
		<thead>
			<tr>
				<th><?= Yii::t('lsdd', 'Name') ?></th>
				<th data-width="25"><i class="glyphicon glyphicon-download-alt"></i></th>
				<th data-width="150" data-align="center"></th>
			</tr>
		</thead>
		<tbody class="ui-sortable">

		</tbody>
		 <!-- <tfoot>
			<tr>
			  <td colspan="4" class="form-inline col-xs-12" role="form">
				<div class="form-group"><input type="text" id="bed-description" class="form-control"></div>
				<div class="form-group"><button id="add-bed-btn" class="btn btn-sm btn-block btn-primary"><span class="glyphicon glyphicon-plus"></span>Add</button></div>
			  </td>
			</tr>
		  </tfoot>-->
	</table>
</div>