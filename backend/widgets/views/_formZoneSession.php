<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>
<?php if(!Yii::$app->user->isGuest) { ?>
	<?php
	  yii\bootstrap\Modal::begin([
		  'header' => '<h2>Konfiguracja sesji</h2>',
		  //'toggleButton' => ['label' => 'click me'],
		  'id' => 'modal', // <-- insert this modal's ID
	  ]);
	 
	  ?>
		<div class="row session-config">
			<?php /*Html::beginForm([Url::to(['/site/configzone'])], 'post', ['enctype' => 'multipart/form-data'])*/ ?>
			<form enctype="multipart/form-data" method="post" action="<?= Url::to(['/site/configzone']) ?>">
				<?= Html::dropDownList('zone', $zone->id, $zonesList,['id'=>'config-zone'] ) ?>
				<label for="currency">Waluta alternatywna</label><?= Html::dropDownList('currency', $currency, $currencyList, ['id'=>'config-currency'] ) ?>
				<input type="hidden" name="location" value="<?= Url::current() ?>"></input>
				<fieldset><legend>Kurs waluty<?= Html::input('text', 'rate', 4.25, ['class' => 'rate']) ?></legend>
					<?= Html::radioList('rate', 1, [1=>'Kurs bankowy', 2=>'Kurs strefy', 3=>'Kurs własny'], ['itemOptions' => ['class' =>'radio-block']]) ?>
				</fieldset>
				
				<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i>Zatwierdź ustawienia', ['class' => 'btn btn-icon btn-primary pull-right']) ?>
			</form>
			<?php /*Html::endForm()*/ ?>
		</div>
	  <?php
	 
	  yii\bootstrap\Modal::end();
	?>
	
	<div class="main-s-wrapper-heading white-bg ">
		<h3><!--<?= $this->title; ?>--><span class="grey-label">Strefa:</span> <?= $zone->zone_name ?><a data-toggle="modal" data-target="#modal" class="btn btn-warning btn-sm" title="Konfiguracja sesji"><i class="glyphicon glyphicon-cog"></i></a></h3>
		<ul class="breadcrumb">
			<li><span class="grey-label" title="Waluta bazowa">Waluta I:</span> <?= $zone->currency_symbol ?></li>
			<li><span class="grey-label" title="Waluta alternatywna">Waluta II:</span> <?= $currency ?></li>
			<li><span class="grey-label">Kurs:</span> 4.2</li>
		</ul>
		<?php /* var_dump(Yii::$app->user->identity); */ ?>
		<!--<?= \yii\widgets\Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>-->
	</div>
<?php } ?>

