<?php

use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if(!$view) { ?>
<div class="panel panel-default">
	<div class="panel-heading">Dołącz pliki</div>
	<div class="panel-body">
		<?php $formFile = ActiveForm::begin(['id' => 'form-upload-file-'.$type, //'form-upload-file-1', 
											 'action' => Url::to(['/files/upload']), /*'layout' => 'horizontal',*/ 
											 'options' => [	'enctype' => 'multipart/form-data', 
															'class' => 'form-upload-file', 
															'data-table' => '#table-files-'.$type, 
															'data-inputfile' => 'upload-file-'.$type, 
															'data-label' => 'Pobierz pliki' 
														],
											  'fieldConfig' => [
													'template' => "{input}",
													'options' => [
														'tag'=>'span'
													]
												]
											]) ?>
		<?= $formFile->field($model, 'id_fk')->hiddenInput(['id' => 'upload-file-fk-'.$type, 'value' => $id ])->label(false); ?>
		<?= $formFile->field($model, 'id_type_file_fk')->hiddenInput(['id' => 'upload-file-type-'.$type, 'value' => $type ])->label(false); ?>
		<div class="row">
			<div class="col-xs-6">
				<?= $formFile->field($model, 'title_file')->textInput(['id' => 'upload-file-title-'.$type, 'maxlength' => true, 'placeholder' => 'Nazwa pliku']) ?>
			</div>
			<div class="col-xs-4">
				<?= $formFile->field($model, 'files[]', ['template' => '<div class="btn btn-default fileUpload"><i class="glyphicon glyphicon-paperclip"></i>{input}{label}</div>'])
							 ->fileInput([ 'id' => 'upload-file-'.$type, 'class' => 'upload', 'data-multiple-caption' => "{count} files selected", 'multiple' => true/*, 'accept' => 'image/*'*/])
							 ->label('Pobierz pliki') ?>
			</div>
			<div class="col-xs-2">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'submit-upload-file btn btn-success']) ?>
			</div>
		</div>


	<?php ActiveForm::end() ?>
	</div>
</div>
<?php } ?>	


<?= $this->render('_tableFiles', [
		'model' => $model, 'type' => $type, 'id' => $id
	]) ?>
