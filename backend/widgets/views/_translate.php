<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<?php if(count($languages) > 0 ) { ?>
<div class="btn-group">
    <button type="button" class="btn btn-purple btn-icon btn-sm"><i class="fa fa-language"></i>Tłumaczenia</button>
    <button type="button" class="btn btn-purple btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu">
        <?php
            foreach($languages as $key => $lang) {
                echo '<li><a href="'.Url::to([$action, 'id' => $id]).'?lang='.$key.'" class="gridViewModal" data-title="<i class=\'fa fa-language\'></i>Tłumacznie ['.$key.']" data-target="#modal-grid-item" data-editor="'.$tinyMce.'">'.$lang.'</a></li>';
            }
        ?>
        <!--<li role="separator" class="divider"></li>
        <li><a href="#">Separated link</a></li>-->
    </ul>
</div>
<?php } ?>