<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcOffer;


class Lastofferwidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $offers = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $this->offers = SvcOffer::find()->where(['status' => 1])->orderby('id desc')->limit(7)->all();
	}
	
	public function run(){
		return $this->render('_lastOffer', [ 'offers' => $this->offers ]);
	}
}
?>
