<?php
namespace backend\widgets\cms;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Cms\models\CmsWidgetAccordionItem;


class LastTabItem extends Widget{
	
	public $lang = 'pl';
	public $langName = 'Polish';
    public $tabId = 0;
	
	public function init(){
		parent::init();
		
	}
	
	public function run(){
		return $this->render('_last_tab_item', [ 'id' => $this->tabId]);
	}
}
?>
