<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="toolbar-tabItems" class="btn-group toolbar-table-widget">
    <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-tabItems"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
</div>
<table  class="table table-items header-fixed table-big table-widget"  id="table-tabItems"
        data-toolbar="#toolbar-tabItems" 
        data-toggle="table-widget" 
        data-show-refresh="false" 
        data-show-toggle="true"  
        data-show-columns="false" 
        data-show-export="false"  
    
        data-show-pagination-switch="false"
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-height="300"
        data-show-footer="false"
        data-side-pagination="client"
        data-row-style="rowStyle"
        data-sort-name="id"
        data-sort-order="desc"
        
        data-method="get"
        data-search-form="#filter-svc-tabItems-search"
        data-url=<?= Url::to(['/cms/cmswidget/unverified', 'id' => $id]) ?>>
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="title"  data-sortable="true">Pytanie</th>                 
            <th data-field="created"  data-sortable="true" data-width="60px" data-align="center">Wysłano</th>
            <th data-field="className" data-visible="false">className</th>
            <th data-field="actions" data-events="actionEvents" data-width="130px"></th>
        </tr>
    </thead>
    <tbody class="ui-sortable">

    </tbody>
</table>   
<fieldset><legend>Objaśnienia</legend> 
    <table class="table-legend">
        <tbody>
            <!--<tr><td class="table-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>-->
            <tr><td class="table-legend-icon" style="background-color: #d9edf7"></td><td>Klient serwisu</td></tr>
        </tbody>
    </table>
</fieldset>