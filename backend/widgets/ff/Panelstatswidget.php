<?php
namespace backend\widgets\ff;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Apl\models\AplLoan;
use backend\Modules\Apl\models\AplCustomer;

class Panelstatswidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $stats = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $this->stats['amountLoans'] = AplLoan::find()->count() ;
        $this->stats['amountClients'] = AplCustomer::find()->where(['status' => 3])->count() ;
        $this->stats['homePageClicked'] = \backend\Modules\Cms\models\CmsPage::findOne(1)['hits'];
        $this->stats['amountComments'] = \backend\Modules\Mix\models\MixComment::find()->where(['status' => 2])->count();
	}
	
	public function run(){
		return $this->render('_panelStats', [ 'stats' => $this->stats ]);
	}
}
?>
