<?php
namespace backend\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;


class TranslateWidget extends Widget{
	
	private $languages = false;
	public $action;
    public $id;
    public $tinyMce = false;
	
	public function init(){
		parent::init();
        
        $this->languages = (isset(\Yii::$app->params['languages']) && \Yii::$app->params['languages']) ? \Yii::$app->params['languages'] : [];
	}
	
	public function run(){
		return $this->render('_translate', [
            'languages' => $this->languages, 
            'action' => $this->action,
            'id' => $this->id,
            'tinyMce' => $this->tinyMce
        ]);
	}
}
?>
