<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcComment;


class Lastcommentwidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $comments = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $this->comments = SvcComment::find()->where(['id_offer_fk' =>  0, 'status' => 2])->orderby('id desc')->all();
	}
	
	public function run(){
		return $this->render('_lastComment', [ 'comments' => $this->comments]);
	}
}
?>
