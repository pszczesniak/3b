<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Shortcutwidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
	}
	
	public function run(){
		return $this->render('_shortcut', [
        ]);
	}
}
?>
