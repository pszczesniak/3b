<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
?>
<div class="grid">
	<div class="col-sm-12 col-xs-12">
		<table id="tooths">
			<tr><th colspan="6" class="text--grey">Prawa</th><th colspan="4" class="text--lightgrey">Góra</th><th colspan="6" class="text--grey">Lewa</th></tr>
			<tr>
                <!--<td>18</td><td>17</td><td>16</td><td>15</td><td>14</td><td>13</td><td>12</td><td>11</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td><td>28</td>-->
                <?php for($i = 8; $i >= 1; --$i) { 
                    echo '<td class="tooth-no" data-tooth="1'.$i.'">1'.$i.'</td>';
                } ?>
                <?php for($i = 1; $i <= 8; ++$i) { 
                    echo '<td class="tooth-no" data-tooth="2'.$i.'">2'.$i.'</td>';
                } ?>
            </tr>
            <tr>
                <?php for($i = 8; $i >= 1; --$i) { 
                    $notes = ''; $status = '';
                    if(isset($tooths['1'.$i])) {
                        $notes = $tooths['1'.$i]['notes'];
                        $status = $tooths['1'.$i]['status'];
                    }
                    echo '<td class="tooth-img" data-tooth="1'.$i.'">'
                            .'<img src="/images/tooths/1'.$i.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $patientId, 'tooth' => '1'.$i]).'" class="viewModal" data-title="Ustaw status zęba <b>1'.$i.'</b>" data-target="#modal-grid-item">ustaw status zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $patientId, 'tooth' => '1'.$i]).'" class="viewModal" data-title="Wpis do historii leczenia zęba <b>1'.$i.'</b>" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/view', 'id' => $patientId, 'tooth' => '1'.$i]).'" class="viewModal" data-title="Podgląd zęba <b>1'.$i.'</b>" data-target="#modal-grid-item">podgląd zęba</a></li>'
                                .'</ul>'
                            .'</div>'
                        .'</td>';
                } ?>
                <?php for($i = 1; $i <= 8; ++$i) { 
                    $notes = ''; $status = '';
                    if(isset($tooths['2'.$i])) {
                        $notes = $tooths['2'.$i]['notes'];
                        $status = $tooths['2'.$i]['status'];
                    }
                    echo '<td class="tooth-img" data-tooth="2'.$i.'">'
                            .'<img src="/images/tooths/2'.$i.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $patientId, 'tooth' => '2'.$i]).'" class="viewModal" data-title="Ustaw status zęba <b>2'.$i.'</b>" data-target="#modal-grid-item">ustaw status zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $patientId, 'tooth' => '2'.$i]).'" class="viewModal" data-title="Wpis do historii leczenia zęba <b>2'.$i.'</b>" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/view', 'id' => $patientId, 'tooth' => '2'.$i]).'" class="viewModal" data-title="Podgląd zęba <b>2'.$i.'</b>" data-target="#modal-grid-item">podgląd zęba</a></li>'                                    
                                .'</ul>'
                            .'</div>'
                        .'</td>';
                } ?>
            </tr>
			<tr>
                <?php for($i = 8; $i >= 1; --$i) { 
                    $notes = ''; $status = '';
                    if(isset($tooths['4'.$i])) {
                        $notes = $tooths['4'.$i]['notes'];
                        $status = $tooths['4'.$i]['status'];
                    }
                    echo '<td class="tooth-img" data-tooth="4'.$i.'">'
                            .'<img src="/images/tooths/4'.$i.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $patientId, 'tooth' => '4'.$i]).'" class="viewModal" data-title="Ustaw status zęba <b>4'.$i.'</b>" data-target="#modal-grid-item">ustaw status zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $patientId, 'tooth' => '4'.$i]).'" class="viewModal" data-title="Wpis do historii leczenia zęba <b>4'.$i.'</b>" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/view', 'id' => $patientId, 'tooth' => '4'.$i]).'" class="viewModal" data-title="Podgląd zęba <b>4'.$i.'</b>" data-target="#modal-grid-item">podgląd zęba</a></li>'                                    
                                .'</ul>'
                            .'</div>'
                        .'</td>';
                } ?>
                <?php for($i = 1; $i <= 8; ++$i) { 
                    $notes = ''; $status = '';
                    if(isset($tooths['3'.$i])) {
                        $notes = $tooths['3'.$i]['notes'];
                        $status = $tooths['3'.$i]['status'];
                    }
                    echo '<td class="tooth-img" data-tooth="3'.$i.'">'
                            .'<img src="/images/tooths/3'.$i.'.png">'.$notes.$status
                            .'<div class="dropdown btn-group">'
                                .'<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                                    .'<i class="fa fa-cogs"></i> <span class="caret"></span>'
                                .'</button>'
                                .'<ul class="dropdown-menu">'
                                    .'<li><a href="'.Url::to(['/eg/tooth/state', 'id' => $patientId, 'tooth' => '3'.$i]).'" class="viewModal" data-title="Ustaw status zęba <b>3'.$i.'</b>" data-target="#modal-grid-item">ustaw staus zęba</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/create', 'id' => $patientId, 'tooth' => '3'.$i]).'" class="viewModal" data-title="Wpis do historii leczenia zęba <b>4'.$i.'</b>" data-target="#modal-grid-item">dodaj wpis</a></li>'
                                    .'<li><a href="'.Url::to(['/eg/tooth/view', 'id' => $patientId, 'tooth' => '3'.$i]).'" class="viewModal" data-title="Podgląd zęba <b>3'.$i.'</b>" data-target="#modal-grid-item">podgląd zęba</a></li>'                                    
                                .'</ul>'
                            .'</div>'
                        .'</td>';
                } ?>
            </tr>
            <tr>
                <!--<td>18</td><td>17</td><td>16</td><td>15</td><td>14</td><td>13</td><td>12</td><td>11</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td><td>28</td>-->
                <?php for($i = 8; $i >= 1; --$i) { 
                    echo '<td class="tooth-no" data-tooth="4'.$i.'">4'.$i.'</td>';
                } ?>
                <?php for($i = 1; $i <= 8; ++$i) { 
                    echo '<td class="tooth-no" data-tooth="3'.$i.'">3'.$i.'</td>';
                } ?>
            </tr>
			<tr><th colspan="6" class="text--grey">Prawa</th><th colspan="4" class="text--lightgrey">Dół</th><th colspan="6" class="text--grey">Lewa</th></tr>
		</table>
	</div>
	<div class="col-sm-12 col-xs-12">
		<div id="toolbar-treatment" class="btn-group toolbar-table-widget">
            <?php /* Html::a('<i class="fa fa-plus"></i>Dodaj', $insertUrl , 
					['class' => 'btn btn-success btn-icon viewModal', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "item-form", 
					 'data-table' => "table-treatment",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					])*/ ?>
            <?= Html::a('<i class="fa fa-file-excel"></i>Export', $exportUrlExcel , 
					['class' => 'btn btn-success btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-eg-treatment-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]) ?>
            <?= Html::a('<i class="fa fa-file-pdf"></i>Export', $exportUrlPdf , 
					['class' => 'btn btn-danger btn-icon btn-export', 
					 'id' => 'case-create',
					 //'data-toggle' => ($gridViewModal)?"modal":"none", 
					 'data-target' => "#modal-grid-item", 
					 'data-form' => "#filter-eg-treatment-search", 
					 'data-table' => "table-items",
					 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Dodaj"
					]) ?>
            <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-treatment"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
		</div>
		<table  class="table-widget table-striped table-items header-fixed"  id="table-treatment"
				data-toolbar="#toolbar-treatment" 
				data-toggle="table-widget" 
				data-show-refresh="false" 
				data-show-toggle="true"  
				data-show-columns="false" 
				data-show-export="false"  
				table-page-size="40"
				data-show-pagination-switch="false"
				data-pagination="true"
				data-side-pagination="server"
                data-sort-order="desc"
				data-id-field="id"
				data-page-list="[10, 25, 50, 100]"
				data-height="700"
				data-show-footer="false"
				data-row-style="rowStyle"
				data-filter-control="false"
				data-filter-show-clear="false"
				data-search-form="#filter-eg-treatment-search"
				data-url=<?= $dataUrl ?> >
			<thead>
				<tr>
					<th data-field="id" data-visible="false">ID</th>                   
					<th data-field="no" data-visible="false" data-align="center"></th>
					<th data-field="tooth" data-sortable="false" data-width="50px" data-align="center">Ząb</th>
                    <th data-field="created" data-sortable="false" data-width="100px" data-align="center">Data</th>
					<th data-field="doctor" data-sortable="false">Lekarz</th>
					<th data-field="note" data-sortable="false">Komentarz</th>
					<th data-field="docs" data-visible="false" data-sortable="false" data-width="20px"><i class="fa fa-attachment"></i></th>
					<th data-field="actions" data-events="actionEvents" data-align="center" data-width="90px"></th>
				</tr>
			</thead>
			<tbody class="ui-sortable">

			</tbody>  
		</table>  
	</div>
</div>