<?php
namespace backend\widgets\eg;

use yii\base\Widget;
use yii\helpers\Html;


class VisitsTable extends Widget{
	public $dataUrl;
    public $insert = false;
    public $insertUrl = '';
    public $actionWidth = '30px';
    public $showEmployee = true;
    public $showClient = false;
	
	public function init(){
		parent::init();		
	}
	
	public function run(){
		return $this->render('_table_visits', [
            'dataUrl' => $this->dataUrl, 'insert' => $this->insert, 'insertUrl' => $this->insertUrl, 'actionWidth' => $this->actionWidth, 'showEmployee' => $this->showEmployee, 'showClient' => $this->showEmployee
        ]);
	}
}
?>
