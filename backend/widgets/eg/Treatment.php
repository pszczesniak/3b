<?php
namespace backend\widgets\eg;

use yii\base\Widget;
use yii\helpers\Html;


class Treatment extends Widget{
	public $dataUrl;
    public $insert = false;
    public $insertUrl = '';
    public $exportUrlExcel = '';
    public $exportUrlPdf = '';
    public $actionWidth = '30px';
    public $showEmployee = true;
    public $showClient = false;
    public $patientId = 0;
    protected $tooths = [];
	
	public function init(){
		parent::init();	

        $sql = "select t.id, tooth_no, id_dict_tooth_status_fk, (select count(*) from {{%eg_tooth_history}} where id_tooth_fk = t.id) notes, d.name as dict_name, d.custom_data "
                ." from {{%eg_patient_tooth}} t left join {{%dictionary_value}} d on d.id = t.id_dict_tooth_status_fk "
                ." where id_patient_fk = ".$this->patientId;
        $toothsData = \Yii::$app->db->createCommand($sql)->queryAll();
        foreach($toothsData as $key => $item) {
            if($item['custom_data']) {
                $customData = \yii\helpers\Json::decode($item['custom_data']);
                $color = $customData['color'];
            } else {
                $color = 'gray';
            }
            $this->tooths[$item['tooth_no']] = ['notes' => (($item['notes']) ? '<small class="badge bg-teal">'.$item['notes'].'</small>' : ''),
                                                'status' => (($item['id_dict_tooth_status_fk']) ? '<small class="label" style="background-color: '.$color.'">'.$item['dict_name'].'</small>' : '')];
        }
	}
	
	public function run(){
		return $this->render('_treatment', ['tooths' => $this->tooths, 'patientId' => $this->patientId,
            'dataUrl' => $this->dataUrl, 'insert' => $this->insert, 'insertUrl' => $this->insertUrl, 'exportUrlExcel' => $this->exportUrlExcel, 'exportUrlPdf' => $this->exportUrlPdf,  
            'actionWidth' => $this->actionWidth, 'showEmployee' => $this->showEmployee, 'showClient' => $this->showEmployee
        ]);
	}
}
?>
