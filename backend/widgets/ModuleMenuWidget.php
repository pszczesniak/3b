<?php
namespace backend\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;


class ModuleMenuWidget extends Widget{
	
	private $grants = [];
	private $accVerify = false;
	public $module = 0;
    private $manager = 0;
	
	public function init(){
		parent::init();
		
		if(!Yii::$app->user->isGuest) {
			
            
                array_push($this->grants, 'grantAll');
           
		}
        
	}
	
	public function run(){
		/*$this->module*/
        return $this->render('_moduleMenu/_'. ( isset(Yii::$app->params['version']) ? Yii::$app->params['version'] : 'default'), [
            'grants' => $this->grants, 'manager' => $this->manager, 'accVerify' => $this->accVerify, 'pass' => true
        ]);
	}
}
?>
