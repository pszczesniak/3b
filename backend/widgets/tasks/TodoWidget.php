<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;
use backend\Modules\Task\models\CalTodo;
use backend\Modules\Task\models\CalTask;

class TodoWidget extends Widget{
	public $tasks;
    public $events;
    public $todos;
    public $refresh = false;
    
	public function init(){
		parent::init();	
        $employeeId = (\Yii::$app->session->get('user.idEmployee')) ? \Yii::$app->session->get('user.idEmployee') : 0;
        $this->todos = 	CalTodo::find()->where(['status' => 1, 'id_dict_todo_type_fk' => 6, 'is_close' => 0, 'todo_show' => 1, 'id_employee_fk' => $employeeId /*'created_by' => \Yii::$app->user->id*/])
                                   ->orderby('id, todo_time')->all();
        /*$this->tasks = 	CalTodo::find()->where(['status' => 1, 'todo_show' => 1, 'is_close' => 0, 'todo_date' => date('Y-m-d'), 'created_by' => \Yii::$app->user->id])
                                   ->andWhere('type_fk != 2')
                                   ->orderby('id, todo_time')->all();
        $session = \Yii::$app->session; $employeeId = $session->get('user.idEmployee');
        $this->events = CalTask::find()->where(['status' => 1, 'id_dict_task_status_fk' => 1, 'event_date' => date('Y-m-d')])
                                   ->andWhere('id in (select id_task_fk from {{%task_employee}} where id_employee_fk = '.$employeeId.')')
                                   ->orderby('id, event_time')->all();*/
	}
	
	public function run(){
		return $this->render('_todo_widget', [
            'todos' => $this->todos, 'refresh' => $this->refresh
        ]);
	}
}
?>
