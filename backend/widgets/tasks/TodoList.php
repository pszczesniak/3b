<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;
use backend\Modules\Task\models\CalTodo;

class TodoList extends Widget{
	public $tasks;
	public $show_task = true;
    
	public function init(){
		parent::init();	
        $employeeId = (\Yii::$app->session->get('user.idEmployee')) ? \Yii::$app->session->get('user.idEmployee') : 0;
        if($this->show_task)
            $this->tasks = 	CalTodo::find()->where(['status' => 1, 'todo_show' => 1, 'id_employee_fk' => $employeeId /*, 'created_by' => \Yii::$app->user->id*/])
                                       //->andWhere( "( (id_dict_todo_type_fk = 6) or (id_dict_todo_type_fk != 6 and todo_date = '".date('Y-m-d')."') )" )
                                       ->orderby('type_fk desc,id desc, todo_time')->all();
        else
            $this->tasks = 	CalTodo::find()->where(['status' => 1, 'todo_show' => 1, 'type_fk' => 2, 'id_dict_todo_type_fk' => 6, 'created_by' => \Yii::$app->user->id])->orderby('type_fk desc,id desc, todo_time')->all();
	}
	
	public function run(){

        return $this->render('_todo_list', [
            'tasks' => $this->tasks, 
        ]);
	}
}
?>
