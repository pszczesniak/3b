<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Task\models\CalCaseSearch;

class CasesTable extends Widget{
	public $dataUrl;
	public $customerVisible = true;
    public $insert = false;
    public $insertUrl = '';
	
	public function init(){
		parent::init();		
	}
	
	public function run(){
		$searchModel = new CalCaseSearch();
        $searchModel->id_dict_case_status_fk = 3;
		
		return $this->render('_table_cases', [
            'dataUrl' => $this->dataUrl,
			'customerVisible' => $this->customerVisible,
			'model' => $searchModel,
            'insert' => $this->insert, 'insertUrl' => $this->insertUrl,
        ]);
	}
}
?>
