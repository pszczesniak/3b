<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;


class EventsTable extends Widget{
	public $dataUrl;
    public $insert = false;
    public $insertUrl = '';
	public $insertLabel = 'zdarzenie';
    public $actionWidth = '30px';
	
	public function init(){
		parent::init();		
	}
	
	public function run(){
		return $this->render('_table_events', [
            'dataUrl' => $this->dataUrl, 'insert' => $this->insert, 'insertUrl' => $this->insertUrl, 'insertLabel' => $this->insertLabel, 'actionWidth' => $this->actionWidth
        ]);
	}
}
?>
