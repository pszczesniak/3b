<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;


class InstancesTable extends Widget{
	public $dataUrl;
    public $insert = false;
    public $insertUrl = '';
    public $actionWidth = '30px';
	
	public function init(){
		parent::init();		
	}
	
	public function run(){
		return $this->render('_table_instances', [
            'dataUrl' => $this->dataUrl, 'insert' => $this->insert, 'insertUrl' => $this->insertUrl, 'actionWidth' => $this->actionWidth
        ]);
	}
}
?>
