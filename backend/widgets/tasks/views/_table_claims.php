<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="toolbar-claims" class="btn-group toolbar-table-widget">
    <?= ($insert) ? Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Nowe'), $insertUrl , 
                ['class' => 'btn btn-success btn-icon gridViewModal', 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "claim-form", 
                 'data-table' => "table-claims",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                ]) : '' ?>
    <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-claims"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
</div>
<div class="div-table-claims">
    <table  class="table-widget table-striped table-items header-fixed"  id="table-claims"
            data-toolbar="#toolbar-claims" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
            table-page-size="40"
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100]"
            data-height="400"
            data-show-footer="false"
            data-row-style="rowStyle"
            data-filter-control="false"
            data-filter-show-clear="false"
            data-search-form="#filter-crm-claims-search"
            data-url=<?= $dataUrl ?> >
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>                   
                <th data-field="type" data-align="center">Typ</th>
                <th data-field="name" data-sortable="false">Nazwa</th>
                <th data-field="dates" data-sortable="false" data-align="center" data-width="70px">Data wystawienia</br>Data płatności</th>
                <th data-field="value" data-sortable="false" data-align="right">Kwota</th>
                <th data-field="details" data-sortable="false"><i class="fa fa-calculator"></i></th>
                <th data-field="note" data-sortable="false" data-width="20px"><i class="fa fa-comment"></i></th>
                <th data-field="actions" data-events="actionEvents" data-align="center" data-width="20px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>  
    </table>  
</div>
