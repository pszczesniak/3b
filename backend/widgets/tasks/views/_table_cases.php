<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="toolbar-cases" class="btn-group">
    <?= ($insert) ? Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', Yii::t('app', 'Nowa')), $insertUrl , 
                ['class' => 'btn btn-success btn-icon viewModal', 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "case-form", 
                 'data-table' => "table-cases",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                ]) : '' ?>
    <!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
</div>

<table  class="table table-striped table-items header-fixed"  id="table-cases"
        data-toolbar="#toolbar-cases" 
        data-toggle="table" 
        data-show-refresh="true" 
        data-search="false" 
        data-filter-control="true"
		data-filter-show-clear="false"
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"
        data-url=<?= $dataUrl ?>
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-side-pagination="client"
        data-row-style="rowStyle">	 
        
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="name" data-sort-name="sname"  data-sortable="true" data-filter-control="input">Tytuł</th>
            <?php if($customerVisible) { ?> <th data-field="customer" data-sortable="true" data-filter-control="input">Klient</th> <?php } ?>
            <th data-field="type"  data-sortable="true" data-filter-control="select">Typ</th>
            <th data-field="status"  data-sortable="true" data-filter-control="select">Status</th>
            <th data-field="actions" data-events="actionEvents" data-width="30px"></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

    

    

    



