<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if(!$refresh) { ?>
    <div class="dropdown" id="tasksWidget">
<?php } ?>
    <a href="#" class="dropdown__toggle" data-toggle="dropdown" aria-expanded="false" title="Musisz to zrobić dzisiaj"><i class="fa fa-exclamation-triangle "></i><span class="label label-danger absolute"><?= ( count($tasks)+count($events) )  ?></span></a>
    <ul class="dropdown-menu pull-right" role="menu">
        <li class="dropdown__header">Moje zadania na dziś</li>
        <li>
            <ul class="list-icon">
                <?php
                    $limit = count($tasks); if($limit > 5) $limit = 5;
                    $personalTypes = \backend\Modules\Task\models\CalTodo::dictTypes(); 
                    for($i = 0; $i < $limit; ++$i) {
                        $task = $tasks[$i];
                        $color = $personalTypes[$task->id_dict_todo_type_fk]['color']; $icon = $personalTypes[$task->id_dict_todo_type_fk]['icon'];
                        $url = Url::to(['/task/personal/updateajax', 'id' => $task['id']]);
                        
                        echo '<li class="head"><span class="fa fa-'.$icon.' icon icon--'.$color.'"></span> <span><a href="'.$url.'" data-target="#modal-grid-item" class="gridViewModal" title="Podgląd zadania">'.((strlen($task->name)>80)?(substr($task->name,0,80).'[...]'):$task->name).'</a></span></li> ';
                    }
                    
                    $limit = count($events); if($limit > 5) $limit = 5;
                    for($i = 0; $i < $limit; ++$i) {
                        $task = $events[$i];
                        $color = 'purple'; $icon = 'gavel';
                        if($task->type_fk == 2) { $color = 'teal'; $icon = 'tasks'; }
                        $url = Url::to(['/task/event/showajax', 'id' => $task['id']]);
                        echo '<li class="head"><span class="fa fa-'.$icon.' icon icon--'.$color.'"></span> <span><a href="'.$url.'" data-target="#modal-grid-item" class="gridViewModal" title="Podgląd zadania">'.((strlen($task->name)>80)?(substr($task->name,0,80).'[...]'):$task->name).'</a></span></li> ';
                    }
                    
                    if( (count($tasks)+count($events)) == 0)
                        echo '<li><span class="fa fa-info icon icon--grey"></span> <span>brak zadań</span></li>';
                ?>
                <!--<li><span class="fa fa-bell icon icon--orange"></span> <span><b>Ważne</b> Zadzwonić do Pana XXX Zadzwonić do Pana XXX</span></li>
                <li><span class="fa fa-bar-chart icon icon--teal"></span> <span>Wykonać raport dla firmy 'TEST'</span></li>
                <li><span class="fa fa-birthday-cake icon icon--purple"></span> <span>Dzisiaj <b>Ania</b> ma urodziny</span></li>--> 
            </ul>
        </li>
        <li class="dropdown__footer">
            <a class="btn btn--full btn--lightgrey" href="<?= Url::to(['/task/personal/todo']) ?>" title="Wszystkie zadania"><i class="fa fa-share"></i> Wszystkie zadania</a>
        </li>
    </ul>
<?php if(!$refresh) { ?>
    </div>
<?php } ?>
