<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="toolbar-instances" class="btn-group toolbar-table-widget">
    <?= ($insert) ? Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Nowa instancja'), $insertUrl , 
                ['class' => 'btn btn-success btn-icon gridViewModal', 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "claim-form", 
                 'data-table' => "table-instances",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>Nowa instancja"
                ]) : '' ?>
    <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-instances"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
</div>
<div class="div-table-instances">
    <table  class="table-widget table-striped table-items header-fixed"  id="table-instances"
            data-toolbar="#toolbar-instances" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
            table-page-size="40"
            data-show-pagination-switch="false"
            data-pagination="true"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100]"
            data-height="400"
            data-show-footer="false"
            data-row-style="rowStyle"
            data-filter-control="false"
            data-filter-show-clear="false"
            data-search-form="#filter-crm-instances-search"
            data-url=<?= $dataUrl ?> >
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>                   
                <th data-field="name" data-sortable="false">Poziom</th>
                <th data-field="sygn" data-align="center">Sygnatura</th>
                <th data-field="instance" data-align="center">Organ</th>
                <th data-field="active" data-align="center"><i class="fa fa-map-marker text--purple" data-title="Instancja aktywna" data-toggle="tooltip"></i></th>
                <th data-field="lawsuit" data-align="center"><i class="fa fa-gavel text--purple" data-title="Pozew" data-toggle="tooltip"></i></th>
                <th data-field="actions" data-events="actionEvents" data-align="center" data-width="20px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>  
    </table>  
</div>
