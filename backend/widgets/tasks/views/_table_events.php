<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="toolbar-events" class="btn-group">
    <?= ($insert) ? Html::a('<i class="glyphicon glyphicon-plus"></i>'.$insertLabel, $insertUrl , 
                ['class' => 'btn bg-green btn-icon viewModal', 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "event-form", 
                 'data-table' => "table-events",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                ]) : '' ?>
    <!-- <button type="button" class="btn btn-default"> <i class="glyphicon glyphicon-heart"></i> </button> -->
</div>
<table  class="table table-striped table-items header-fixed"  id="table-events"
        data-toolbar="#toolbar-events" 
        data-toggle="table" 
        data-search="false" 
		data-show-refresh="true" 
        data-filter-control="true"
		data-filter-show-clear="false"
        data-show-toggle="false"  
        data-show-columns="false" 
        data-show-export="false"
        data-url=<?= $dataUrl ?>
        data-pagination="true"
        data-id-field="id"
        data-page-list="[10, 25, 50, 100, ALL]"
        data-side-pagination="client"
        data-row-style="rowStyle">	  
        
    <thead>
        <tr>
            <th data-field="id" data-visible="false">ID</th>
            <th data-field="delay" data-visible="false">DELAY</th>
            <th data-field="className" data-visible="false">className</th>
            <th data-field="ename" data-sort-name="esname"  data-sortable="true"  data-filter-control="input">Tytuł</th>
            <th data-field="term"  data-sortable="true"  data-filter-control="input">Termin</th>
            <th data-field="etype"  data-sortable="true"  data-filter-control="select">Typ</th>
            <th data-field="estatus"  data-sortable="true"  data-filter-control="select">Status</th>
            <!--<th data-field="category"  data-sortable="true" >Waga</th>-->
            <th data-field="actions" data-events="actionEvents" data-align="center" data-width="<?= $actionWidth ?>"></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
<fieldset><legend>Objaśnienia</legend> 
	<table class="calendar-legend">
		<tbody>
			<tr><td class="calendar-legend-icon" style="background-color: #f2dede"></td><td>Minął termin wykonania</td></tr>
			<tr><td class="calendar-legend-icon" style="background-color: #fcf8e3"></td><td>Nie ustawiono czasu</td></tr>
		</tbody>
	</table>
</fieldset>

    

    

    



