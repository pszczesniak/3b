<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if(!$refresh) { ?>
<div class="dropdown" id="todoWidget">
<?php } ?>
    <a href="#" class="dropdown__toggle" data-toggle="dropdown" aria-expanded="false" title="TODO"><i class="fa fa-tasks"></i><span class="label label-warning absolute"><?= count($todos) ?></span></a>
    <ul class="dropdown-menu pull-right" role="menu">
        <li class="dropdown__header">TODO</li>
        <li>
            <ul class="list-icon">
                <?php
                    $limit = count($todos); if($limit > 5) $limit = 5;
                    for($i = 0; $i < $limit; ++$i) {
                        $task = $todos[$i];
                        $icon = 'flag';
                        if($task->id_priority_fk == 1) { $color = 'blue'; }
                        if($task->type_fk == 2) { $color = 'yellow';  }
                        if($task->type_fk == 3) { $color = 'red';  }
                        echo '<li class="head"><span class="fa fa-'.$icon.' icon icon--'.$color.'"></span> <span>'.$task->name.'</span></li> ';
                    }
                    if($limit == 0)
                        echo '<li><span class="fa fa-info icon icon--grey"></span> <span>brak zadań</span></li>';
                ?>
                <!--<li><span class="fa fa-bell icon icon--orange"></span> <span><b>Ważne</b> Zadzwonić do Pana XXX Zadzwonić do Pana XXX</span></li>
                <li><span class="fa fa-bar-chart icon icon--teal"></span> <span>Wykonać raport dla firmy 'TEST'</span></li>
                <li><span class="fa fa-birthday-cake icon icon--purple"></span> <span>Dzisiaj <b>Ania</b> ma urodziny</span></li>--> 
            </ul>
        </li>
        <li class="dropdown__footer">
            <a class="btn btn--full btn--lightgrey" href="<?= Url::to(['/task/personal/todo']) ?>" title="Wszystkie zadania"><i class="fa fa-share"></i> Wszystkie zadania</a>
        </li>
    </ul>
<?php if(!$refresh) { ?>
    </div>
<?php } ?>
