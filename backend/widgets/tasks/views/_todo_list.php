<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>
<div  class="todo-tasks">
    <div class="list">
        <div class="list-title"> <a href="<?= Url::to(['/task/personal/todo']) ?>" title="Przejdź do kalendarza osobistego"><i class="fa fa-tasks"></i></a>do zrobienia</div>
        <div class="list-add">
            <input class="add-text" id="add-text" type="text" placeholder="Nazwa zadania" maxlength="200">
            <button class="add-todo-short text--white" data-action="<?= Url::to(['/task/personal/createshort']) ?>"><i class="fa fa-plus text--white"></i></button>
        </div>
        <div class="list-body">
            <ul class="todo-list" >
            <?php
                $priorityColors = [ 1 => 'blue', 2 => 'yellow', 3 => 'orange', 4 => 'red'];
                
                foreach($tasks as $key => $task) {
                    $url = Url::to(['/task/personal/updateajax', 'id' => $task->id]);
                    if($task->type_fk == 1) {
                        //$task->name = '<i class="fa fa-flag text--'.$priorityColors[$task->id_priority_fk].'"></i>'.$task->name . ' <span class="todo-normal-time">@'.( (!$task->todo_deadline) ? 'brak terminu' : $task->todo_deadline ).'</span>';
                        $task->name = $task->name . ' <span class="todo-normal-time text--red">@'.( (!$task->todo_deadline) ? 'brak terminu' : $task->todo_deadline ).'</span>';                        
                        echo '<li class="todo-'.$task->id.' todo-normal  '.( ($task->is_close == 1) ? "completed" : '' ).'">'
                                .'<div>'
                                    .'<input type="checkbox" id="checkbox-'.$task->id.'" value="'.$task->id.'" name="check" '.( ($task->is_close == 1) ? "checked" : '' ).'>'
                                    .'<label class="checkbox" data-value="'.$task->id.'" for="checkbox-'.$task->id.'" data-action="'.Url::to(['/task/personal/state','id' => $task->id]).'">'
                                .'</div>'
                                .'<p><a href="'.$url.'" data-target="#modal-grid-item" class="gridViewModal" title="Podgląd zadania">'.$task->name.'</a></p><a class="close" href="'.Url::to(['/task/personal/hide','id' => $task->id]).'"><i class="fa fa-close"></i></a>'
                            .'</li>';
                    } else {
                        echo '<li class="todo-'.$task->id.' todo-short  '.( ($task->is_close == 1) ? "completed" : '' ).'">'
                                .'<div>'
                                    .'<input type="checkbox" id="checkbox-'.$task->id.'" value="'.$task->id.'" name="check" '.( ($task->is_close == 1) ? "checked" : '' ).'>'
                                    .'<label class="checkbox" data-value="'.$task->id.'" for="checkbox-'.$task->id.'" data-action="'.Url::to(['/task/personal/state','id' => $task->id]).'">'
                                .'</div>'
                                .'<p><a href="'.$url.'" data-target="#modal-grid-item" class="gridViewModal" title="Podgląd zadania">'.$task->name.'</a></p><a class="close" href="'.Url::to(['/task/personal/hide','id' => $task->id]).'"><i class="fa fa-close"></i></a>'
                            .'</li>';
                    }
                }
            ?>
  
            </ul>
        </div>
    </div>
</div>