<?php
namespace backend\widgets\tasks;

use yii\base\Widget;
use yii\helpers\Html;


class ClaimsTable extends Widget{
	public $dataUrl;
    public $insert = false;
    public $insertUrl = '';
    public $actionWidth = '30px';
	
	public function init(){
		parent::init();		
	}
	
	public function run(){
		return $this->render('_table_claims', [
            'dataUrl' => $this->dataUrl, 'insert' => $this->insert, 'insertUrl' => $this->insertUrl, 'actionWidth' => $this->actionWidth
        ]);
	}
}
?>
