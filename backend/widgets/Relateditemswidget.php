<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Relateditemswidget extends Widget{
	
	private $modal;
	public $createUrl = '';//Url::to(['/lsdd/lsddpackagesystemitem/create', 'id'=>1]);
	public $createLabel =  '';
	public $createIcon =  'plus';
	public $createStyle =  'success';
	public $dataUrl = '';//Url::to(['/lsdd/lsddpackagesystem/items', 'id'=>1]);
	public $tableId = "table-items";
	public $modalId = "modal-grid-item";
	public $columns = [];
	public $view = false;
	public $gridViewModal = 'gridViewModal';
	public $buttonId;
	public $tableOptions = 'data-toggle="table" data-show-refresh="true" data-show-toggle="true"  data-show-columns="true" data-show-export="true"  data-filter-control="true"';
    public $otherButtons = false;
	
	public function init(){
		parent::init();
		
		if(empty($this->buttonId)) $this->buttonId = 'button-'.rand();
		//$this->modal = new Files();
	}
	
	public function run(){
		return $this->render('_formItems', [
            'createUrl' => $this->createUrl, 
			'dataUrl' => $this->dataUrl, 
			'tableId' => $this->tableId, 
			'modalId' => $this->modalId, 
			'columns' => $this->columns, 
			'createLabel' => $this->createLabel, 
			'createIcon' => $this->createIcon, 
			'createStyle' => $this->createStyle, 
			'view' => $this->view, 
			'buttonId' => $this->buttonId, 
			'tableOptions' => $this->tableOptions,
			'gridViewModal' => $this->gridViewModal,
            'otherButtons' => $this->otherButtons
        ]);
	}
}
?>
