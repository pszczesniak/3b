<?php
    use yii\helpers\Url;
    
    use backend\widgets\files\FilesUpload;
?>

<?php if(!$isNew) { ?>
    <?php if(!$onlyShow) echo FilesUpload::widget(['typeId' => $typeId, 'parentId' => $parentId, 'view' => false, 'types' => $types]) ?>
    <?php if(count($files) > 0) { ?>
    <br />
    <fieldset>
        <legend >Operacje
            <a aria-controls="files-container" aria-expanded="true" href="#files-container" data-toggle="collapse" class="collapse-link collapse-window pull-right">
                <i class="collapse-window-icon glyphicon glyphicon-menu-up"></i>
            </a>
        </legend>
        <div id="files-container" class="collapse in">
            <div class="grid grid--0 search-docs">
                <div class="col-sm-10 col-xs-7">
                    <div id="wrap-search">
                        <div class="search-docs">
                            <input id="search-docs" name="search" type="text" placeholder="Czego szukasz?"><input id="search_submit" value="Rechercher" type="submit">
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-5">
                    <div class="input-group my-group"> 
                        <select id="search-docs-category" name="search-docs-category" class="selectpicker form-control" data-live-search="true" title="-kategoria-">
                            <option value="-1">-wszystkie-</option>
                            <?php
                                foreach($types as $key => $item) {
                                    echo '<option value="'.$item->id.'">'.$item->name.'</option>';
                                }
                            ?>
                        </select> 
                        <?php if($download) { ?>
                        <span class="input-group-btn">
                            <button class="btn bg-teal btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-download"></i> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="<?= $download ?>" >wszystkie </a></li>
                                <li><a href="<?= $download ?>" id="search-docs-download">wybrane </a></li>
                            </ul>
                        </span>
                        <?php } ?>
                    </div>
                  
                </div>
            </div>
        </div>
    </fieldset>
   
    <?php } ?>
    <div class="clear"></div>
    <div class="attachement-list"> 
        <ul class="files-container files-container-<?= $typeId ?>">
            <?php
                if(count($files) == 0 && $onlyShow) {
                    echo '<li class="documents-empty"><div class="alert alert-info">nie dodano jeszcze żadnych dokumentów</div></li>';
                } else {
                    foreach($files as $key => $file) {
                        
                        $icon = 'fa fa-paperclip';
                        $title = "Załącznik";
                        $url = '#';
                        if($file->id_type_file_fk == 5) {
                            if($file->type_fk == 1) {
                                $icon = 'fa fa-envelope text--blue';
                                $title = "Korespondencja przychodząca";
                                $url = Url::to(['/correspondence/box/view', 'id' => $file->id_fk]);
                            } else {
                                $icon = 'fa fa-send text--orange';
                                $title = "Korespondencja wychodząca";
                                $url = Url::to(['/correspondence/box/view', 'id' => $file->id_fk]);
                            }
                        }
                        if($file->id_type_file_fk == 4 && $typeId == 3) {
                            if($file->type_fk == 1) {
                                $icon = 'fa fa-gavel text--pink';
                                $title = "Rozprawa";
                                $url = Url::to(['/task/case/view', 'id' => $file->id_fk]);
                            } else {
                                $icon = 'fa fa-tasks text--teal';
                                $title = "Zadanie";
                                $url = Url::to(['/task/event/view', 'id' => $file->id_fk]);
                            }
                        } 
                        $actions = (!$onlyShow || $actionsShow) ? '<span class="list-file-edit"><a class="file-update gridViewModal" data-target="#modal-grid-file" href="/files/update/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-pencil"></i>Edytuj</a></span>'
                                                 .'<span class="list-file-del"><a class="file-delete" href="/files/delete/'.$file->id.'" data-id="'.$file->id.'"><i class="fa fa-trash"></i>Usuń</a></span>' 
                                                 .'<span class="list-file-replace"><a class="file-replace gridViewModal" data-target="#modal-grid-file" href="/files/replace/'.$file->id.'" data-id="'.$file->id.'" data-title="Zamień plik" title="Zamień plik"><i class="fa fa-exchange"></i>Zamień</a></span>'
												: ( (Yii::$app->params['env'] == 'dev') ? '<span class="list-file-print"><a class="file-print" href="/uploads/'.$file->systemname_file.'.'.$file->extension_file.'" data-id="'.$file->id.'"><i class="fa fa-print"></i>Drukuj</a></span>' : '') ;
                            if( (!$onlyShow && ($typeId == 3 || $typeId == 4) && $file->id_type_file_fk == 5) || (!$onlyShow && ($typeId == 3) &&  in_array($file->id_type_file_fk, [4,5]) )  ) {
                                echo '';
                            } else {
                                echo '<li class="document-normal" id="file-'.$file->id.'" data-category="'.( ($file->category) ? $file->category['id'] : '0').'">'
										//.'<embed src="/uploads/WwXrRvZY_W_1480074882.pdf" id="file-'.$file->id.'" name="file-'.$file->id.'" hidden>'
                                        .'<div class="attachment-downloading none">Pobieranie ...</div><div class="attachment">'
                                            .'<div class="attachment-thumb">'
                                                .'<a href="'.$url.'"><i class="'.$icon.'" title="'.$title.'"></i></a>'
                                            .'</div>'
                                            .'<div class="attachment-info">'
                                                .'<div class="attachment-file-name">'
                                                     .'<a class="file-download" href="/files/getfile/'.$file->id.'">'.$file->title_file .'</a>'
                                                .'</div>'
                                                .'<div class="attachment-file-creator">'
                                                     .'<p>'.$file->user.' <i class="fa fa-clock"></i>'.$file->created_at.'</p>'
                                                .'</div>'
                                                .'<div class="attachment-file-category">'
                                                     .'<p> <i class="fa fa-tag"></i><span>'.( ($file->category) ? $file->category['name'] : 'nieprzypisany').'</span></p>'
                                                .'</div>'
                                                .'<div class="attachment-action-bar">'
                                                   .'<span class="list-file-download"><a class="file-download" href="/files/getfile/'.$file->id.'"><i class="fa fa-download"></i> Pobierz</a></span>'
                                                   .$actions
                                                .'</div>'
                                            .'</div>'
                                        .'</div>'
                                .'</li>';
                            }
                    }
                }
            ?>
        </ul>
    </div>
<?php } else {
        echo '<div class="alert alert-warning">Aby dodać pliki musisz utworzyć rekord.</div>';
    } 
?>
