<?php

use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if(!$view) { ?>
<div class="file-upload-alert file-upload-alert-<?= $type ?>"></div>
<div class="upload-container upload-<?= $type ?>">
    <div class="progress-wrp none"><!--<div class="progress-bar"></div ><div class="status">0%</div>--></div>

    <div class="grid grid--0">
        <div class="col-xs-7">
            <div class="input-group">
                <label class="input-group-btn">
                    <span class="btn bg-purple">
                        <i class="fa fa-hand-pointer-o" title="Wybierz plik"></i> <input type="file" multiple="" data-type="<?= $type ?>" style="display: none;" class="ufile">
                    </span>
                </label>
                <input type="text"  class="form-control ufile-name ufile-name-<?= $type ?>">
            </div>
        </div>
        <div class="col-xs-5">
            <div class="input-group">
                <select type="text"  class="form-control ufile-type ufile-type-<?= $type ?>"> 
                    <!--<option value="0">-- wybierz kategorię --</option>-->
                    <?php
                        foreach($types as $key => $item) {
                            echo '<option '. ( ($model->id_dict_type_file_fk == $item->id) ? ' selected ' : '') .' value="'.$item->id.'">'.$item->name.'</option>';
                           /* if($item->children) {
                                echo Yii::$app->runAction('common/structure', ['parent' => $item, 'type' => 1, 'selected' => null]);
                            } else {
                                echo '<option class="level-'.$item->lvl.'" value="'.$item->id.'">'.$item->item_name.'</option>';
                            }*/
                        }
                    ?>
                </select>
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-file-send" data-table="#table-files-<?= $type ?>" data-action="<?= Url::to(['/files/upload']) ?>" data-type="<?= $type ?>" data-id="<?= $id ?>" data-original-title="Upload" title="Pobierz"><i class="fa fa-cloud-upload" title="Pobierz"></i></button>
                </span>
            </div>
        </div>
    </div>
</div>

<?php } ?>	


<?php /*$this->render('_tableFiles', [
		'model' => $model, 'type' => $type, 'id' => $id
	])*/ ?>
