<?php
namespace backend\widgets\files;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\Structure;
use backend\Modules\Dict\models\DictionaryValue;

class FilesBlock extends Widget{
	public $files;
    public $typeId = 0;
    public $parentId = 0;
    public $onlyShow = false;
	public $actionsShow = false;
    public $isNew = true;
    public $types = [];
    public $download = false;
    public $showType = true;
	
	public function init(){
		parent::init();	
        //$typesData = Structure::find()->where(['type_fk' => 1, 'status' => 1, 'id_parent_fk' => 1])->all();
        $typesData = DictionaryValue::find()->where(['status' => 1, 'id_dictionary_fk' => 1])->orderby('name')->all();
	    foreach($typesData as $key => $item) {
            array_push($this->types, $item);
        }
	}
	
	public function run(){
		return $this->render('_block_files', [
            'files' => $this->files, 'isNew' => $this->isNew, 'typeId' => $this->typeId, 'parentId' => $this->parentId, 
            'onlyShow' => $this->onlyShow, 'types' => $this->types, 'actionsShow' => $this->actionsShow, 'download' => $this->download, 'showType' => $this->showType
        ]);
	}
}
?>
