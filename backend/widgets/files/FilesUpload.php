<?php
namespace backend\widgets\files;

use yii\base\Widget;
use yii\helpers\Html;

use common\models\Files;

class FilesUpload extends Widget{
	
	private $modal;
	public $typeId = 0;
	public $parentId = 0;
	public $view = false;
    public $types;
    public $showType = true;
	
	public function init(){
		parent::init();
		
		$this->modal = new Files();
       // $this->modal->id_dict_type_file_fk = 18;
	}
	
	public function run(){
		return $this->render( '_upload_files_dev' , [
            'model' => $this->modal, 'type' => $this->typeId, 'id' => $this->parentId, 'view' => $this->view, 'types' => $this->types, 'showType' => $this->showType
        ]);
	}
}
?>
