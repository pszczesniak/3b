<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcOffer;
use backend\Modules\Svc\models\SvcComment;
use backend\Modules\Svc\models\SvcClient;

class Panelstatswidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $stats = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $this->stats['amountOffers'] = SvcOffer::find()->where(['status' => 1])->count() ;
        $this->stats['amountClients'] = SvcClient::find()->where(['status' => 1])->count() ;
        $bestCommentsOffer = SvcComment::find()->select('id_offer_fk, (sum(rank)/count(*)) as ranking')->where(['status' => 2])->andWhere('id_offer_fk != 0')->groupby('id_offer_fk')->orderby('ranking desc')->limit(1)->one();
        if($bestCommentsOffer)
            $this->stats['bestOfferRanking'] = ['id' => $bestCommentsOffer->id_offer_fk, 'amount' => $bestCommentsOffer->ranking];
        else
            $this->stats['bestOfferRanking'] = false;
        $bestClickOffer = SvcOffer::find()->where(['status' => 1])->andWhere('id != 0')->orderby('stats_click desc')->limit(1)->one();
        if($bestClickOffer)
            $this->stats['bestOfferClicked'] = ['id' => $bestClickOffer->id, 'amount' => $bestClickOffer->stats_click];
        else
            $this->stats['bestOfferClicked'] = false;
	}
	
	public function run(){
		return $this->render('_panelStats', [ 'stats' => $this->stats ]);
	}
}
?>
