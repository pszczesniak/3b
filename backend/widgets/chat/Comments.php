<?php
namespace backend\widgets\chat;

use yii\base\Widget;
use yii\helpers\Html;
use backend\Modules\Community\models\ComPost;

class Comments extends Widget{
	public $notes;
    public $actionUrl; 
    public $view = 'normal';
    
	public function init(){
		parent::init();	
	}
	
	public function run(){
		return $this->render('_comments_'.$this->view, [
            'notes' => $this->notes, 'actionUrl' => $this->actionUrl
        ]);
	}
}
?>
