<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="dropdown">
    <a href="#" class="dropdown__toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-comments"></i><span class="label bg-teal absolute"><?= count($posts) ?></span></a>
    <ul class="dropdown-menu pull-right" role="menu">
        <li class="dropdown__header">Ostatnie wpisy</li>
        <li>
            <div class="notification-items no-overflow">
                <?php
                    $limit = count($posts); if($limit > 5) $limit = 5;
                    for($i = 0; $i < $limit; ++$i) {
                        $post = $posts[$i];
                        $icon = ($post['type_fk'] == 1) ? 'circle' : 'lock';
                        $color = ($post['type_fk'] == 1) ? 'green' : 'red';
                        echo '<div class="item media">'
                                .'<div class="media-head"><span class="member">'.$post['fullname'].'</span><div class="meta"> '.Yii::$app->runAction('common/ago', ['date' => $post['created_at']]).' </div></div>'
                                .'<div class="media-body"> <a href="'.Url::to(['/community/chat/room', 'id' => $post['id_topic_fk']]).'"> '.$post['note'].' </a> </div>'
                            .'</div>';
                    }
                    if($limit == 0)
                        echo '<div class="alert alert-info"><span class="fa fa-info icon icon--grey"></span> <span>brak komentarzy</span></div>';
                ?>
										
										
            <!--<ul class="list-icon">-->
                <?php
                   /* $limit = count($posts); if($limit > 5) $limit = 5;
                    for($i = 0; $i < $limit; ++$i) {
                        $post = $posts[$i];
                        $icon = ($post['type_fk'] == 1) ? 'circle' : 'lock';
                        $color = ($post['type_fk'] == 1) ? 'green' : 'red';
                        echo '<li class="head"><span class="fa fa-'.$icon.' icon icon--'.$color.'"></span> <span>'.$post['note'].'</span></li> ';
                    }
                    if($limit == 0)
                        echo '<li><span class="fa fa-info icon icon--grey"></span> <span>brak komentarzy</span></li>';*/
                ?>
                <!--<li><span class="fa fa-bell icon icon--orange"></span> <span><b>Ważne</b> Zadzwonić do Pana XXX Zadzwonić do Pana XXX</span></li>
                <li><span class="fa fa-bar-chart icon icon--teal"></span> <span>Wykonać raport dla firmy 'TEST'</span></li>
                <li><span class="fa fa-birthday-cake icon icon--purple"></span> <span>Dzisiaj <b>Ania</b> ma urodziny</span></li>--> 
            <!--</ul>-->
        </li>
        <li class="dropdown__footer">
            <a class="btn btn--full btn--lightgrey" href="<?= Url::to(['/community/chat/index']) ?>" title="CHAT"><i class="fa fa-share"></i> przejdź do rozmów</a>
        </li>
    </ul>
</div>
