<?php $avatar = (is_file(Yii::getAlias('@webroot') . "/../.." . Yii::$app->params['basicdir'] . "/web/uploads/employees/cover/".$model->created_by."/preview.jpg"))?"/uploads/employees/cover/".$model->created_by."/preview.jpg":"/images/default-user.png"; ?>

<li class="<?= ($model->created_by == \Yii::$app->user->id) ? 'right' : 'left' ?> clearfix">
    <span class="chat-img pull-<?= ($model->created_by == \Yii::$app->user->id) ? 'right' : 'left' ?>">
        <img src="<?= $avatar ?>" alt="User Avatar" class="img-circle">
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <strong class="primary-font"><?= (($model->created_by == \Yii::$app->user->id) ? 'TY' : $model->user) ?></strong> <small class="pull-right text-muted"> <i class="fa fa-clock-o"></i><?= $model->created_at ?></small>
        </div>
        <p>  <?= $model->note ?>  </p>
    </div>
    <div class="clear"></div>
</li>