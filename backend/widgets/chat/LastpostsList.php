<?php
namespace backend\widgets\chat;

use yii\base\Widget;
use yii\helpers\Html;
use backend\Modules\Community\models\ComPost;

class LastpostsList extends Widget{
	public $posts;
    
	public function init(){
		parent::init();	
       
        $session = \Yii::$app->session; $employeeId = $session->get('user.idEmployee');
        $sql = "select id_topic_fk, g.type_fk, note, concat_ws(' ', u.lastname, u.firstname) as fullname, p.created_at"
                ." from {{%com_post}} p join {{%user}} u on u.id=p.created_by join {{%com_topic}} t on t.id = p.id_topic_fk join {{%com_group}} g on g.id = t.id_group_fk join {{%com_member}} gm on gm.id_group_fk = g.id and gm.id_user_fk = ".\Yii::$app->user->id
                ." where p.created_by != ".\Yii::$app->user->id." and (".\Yii::$app->user->id.",p.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})"
                ." union "
                ." select id_topic_fk, g2.type_fk, note, concat_ws(' ', u2.lastname, u2.firstname) as fullname, p2.created_at"
                ." from {{%com_post}} p2 join {{%user}} u2 on u2.id=p2.created_by join {{%com_topic}} t2 on t2.id = p2.id_topic_fk join {{%com_group}} g2 on g2.id = t2.id_group_fk and g2.type_fk = 1 "
                ." where p2.created_by != ".\Yii::$app->user->id." and (".\Yii::$app->user->id.",p2.id) not in (select id_user_fk,id_post_fk from {{%com_post_member}})"
                ." order by created_at desc";
        $this->posts = \Yii::$app->db->createCommand($sql)->queryAll();
        /*$this->posts = 	ComPost::find()->where('created_by != '.\Yii::$app->user->id)
                                   ->andWhere('id_topic_fk in (select id from {{%com_topic}} where type_fk = 1) ')
                                   ->orderby('id desc')->all();*/

	}
	
	public function run(){
		return $this->render('_posts_list', [
            'posts' => $this->posts, 
        ]);
	}
}
?>
