<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Svc\models\SvcOffer;
use backend\Modules\Svc\models\SvcPackage;

class Panelpercentwidget extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $stats = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $offersAll = SvcOffer::find()->where(['status' => 1])->count();
        $packagesStats = SvcOffer::find()->select('id_package_fk, count(*) as counter')->where(['status' => 1])->groupby('id_package_fk')->orderby('id_package_fk')->all();
        foreach($packagesStats as $key => $values) {
            $this->stats[$values['id_package_fk']]['id'] = $values['id_package_fk'];
            $this->stats[$values['id_package_fk']]['name'] = SvcPackage::findOne($values['id_package_fk'])->name;
            $this->stats[$values['id_package_fk']]['percent'] = $values['counter']/$offersAll*100;
        }
	}
	
	public function run(){
		return $this->render('_panelPercent', [ 'stats' => $this->stats  ]);
	}
}
?>
