<?php
namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Apl\models\AplLoan;


class LastLoans extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $loans = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        //$this->loans = SvcOffer::find()->where(['status' => 1])->orderby('id desc')->limit(7)->all();
	}
	
	public function run(){
		return $this->render('_lastLoans', [ /*'loans' => $this->loans*/ ]);
	}
}
?>
