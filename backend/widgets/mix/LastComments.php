<?php
namespace backend\widgets\mix;

use yii\base\Widget;
use yii\helpers\Html;

use backend\Modules\Mix\models\MixComment;


class LastComments extends Widget{
	
	private $zone;
	public $lang = 'pl';
	public $langName = 'Polish';
    public $comments = [];
	
	public function init(){
		parent::init();
		
		//$zoneId = \Yii::$app->session->get('user.zone')?\Yii::$app->session->get('user.zone'):1;
		//$this->zone = LsddZone::findOne($zoneId);
        $this->comments = MixComment::find()->where(['id_fk' =>  0, 'status' => 0])->orderby('id desc')->all();
	}
	
	public function run(){
		return $this->render('_lastComment', [ 'comments' => $this->comments]);
	}
}
?>
