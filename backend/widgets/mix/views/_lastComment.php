<div class="chat">
    <ul class="chat-list">
        
        <?php
            if(count($comments) == 0) {
                echo '<div class="alert alert-info">Brak opinii do akceptacji</div>';
            }
            foreach($comments as $key => $comment) {
                //if(!$model->id_user_fk) $idUser = 0; else $idUser = $model->id_user_fk;
                $avatar = (file_exists(\Yii::getAlias('@webroot')."/../../frontend/web/uploads/avatars/thumb/avatar-".$comment->id_user_fk.".png")) ? "/uploads/avatars/thumb/avatar-".$comment->id_user_fk.".png" : "/images/default-user.png";
                echo '<li class="left clearfix" id="verify-comment-'.$comment->id.'">'
                        .'<span class="chat-img pull-left">'
                            .'<img src="'.\Yii::$app->params['frontend'].$avatar.'" alt="Avatar"><br />'
                            .'<a href="'.\yii\helpers\Url::to(['/mix/comment/accept', 'id' => $comment->id]).'" title="Opublikuj komentarz" class="deleteConfirm" data-color="success" data-label="Akceptuj"><i class="fa fa-check-circle fa-2x text--green"></i></a>'
                            .'<a href="'.\yii\helpers\Url::to(['/mix/comment/discard', 'id' => $comment->id]).'" title="Odrzuć komentarz" class="deleteConfirmWithComment" data-color="danger" data-label="Odrzuć"><i class="fa fa-times-circle fa-2x text--red"></i></a>'
                        .'</span>'
                        .'<a href="'.\yii\helpers\Url::to(['/mix/comment/view', 'id' => $comment->id]).'"><div class="chat-body clearfix">'
                            .'<div class="header">'
                                .'<strong class="primary-font">'.$comment->name.'</strong>'
                                .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> '.$comment->created_at.'</small>'
                            .' </div>'
                            .'<p>'
                                .$comment->comment
                            .'</p>'
                        .'</div></a>'
                    .'</li>'
                   /* .'<li class="right clearfix">'
                        .'<span class="chat-img pull-right">'
                            .'<img src="/../images/default-user.png" alt="User Avatar">'
                        .'</span>'
                        .'<div class="chat-body clearfix">'
                        .'<div class="header">'
                            .'<strong class="primary-font">Usługodawca</strong>'
                            .'<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>'
                        .'</div>'
                            .'<p>'
                                .'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. '
                            .'</p>'
                        .'</div>'
                    .'</li>'*/;
            }
        ?>

    </ul>
 </div>  