<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Modules\Customer\models\CustomerPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="toolbar-visits" class="btn-group toolbar-table-widget">
    <?= ($insert) ? Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Nowa'), $insertUrl , 
                ['class' => 'btn btn-success btn-icon viewModal', 
                 'data-target' => "#modal-grid-item", 
                 'data-form' => "claim-form", 
                 'data-table' => "table-visits",
                 'data-title' => "<i class='glyphicon glyphicon-plus'></i>".Yii::t('app', 'New') 
                ]) : '' ?>
    <button class="btn btn-default btn-refresh-table" type="button" title="Odśwież" data-table="#table-visits"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
</div>
<div class="div-table-visits">
    <table  class="table-widget table-striped table-items header-fixed"  id="table-visits"
            data-toolbar="#toolbar-visits" 
            data-toggle="table-widget" 
            data-show-refresh="false" 
            data-show-toggle="true"  
            data-show-columns="false" 
            data-show-export="false"  
            table-page-size="40"
            data-show-pagination-switch="false"
            data-pagination="true"
            data-side-pagination="server"
            data-id-field="id"
            data-page-list="[10, 25, 50, 100]"
            data-height="400"
            data-show-footer="false"
            data-row-style="rowStyle"
            data-filter-control="false"
            data-filter-show-clear="false"
            data-search-form="#filter-crm-visits-search"
            data-url=<?= $dataUrl ?> >
        <thead>
            <tr>
                <th data-field="id" data-visible="false">ID</th>
                <th data-field="term" data-sortable="false" data-width="90px" data-align="center">Termin</th>
                <th data-field="time" data-sortable="false" data-width="90px" data-align="center">Czas [h]</th>
                <th data-field="doctor"  data-sortable="true" data-align="center">Pracownik</th>
                <th data-field="city" data-sortable="true">Miejscowość</th>
                <th data-field="address" data-sortable="true">Adres</th>
                <th data-field="info" data-sortable="true">Informacja</th>
                <th data-field="note" data-sortable="false" data-width="40px" data-align="center"><i class="far fa-sticky-note"></i></th>
                <th data-field="calculate" data-visible="false" data-sortable="false" data-align="center"><i class="far fa-calcualtor"></i></th>
                <th data-field="status" data-sortable="true"  data-width="100px" data-align="center">Status</th>
                <th data-field="confirm" data-align="center" data-events="actionEvents"><i class="fa fa-check text--grey" data-title="Potwierdzona" data-toggle="tooltip"></i></th>
                <th data-field="actions" data-events="actionEvents" data-width="90px"></th>
            </tr>
        </thead>
        <tbody class="ui-sortable">

        </tbody>  
    </table>  
</div>
