<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/view-window.php',array('class'=>Inflector::camel2id(StringHelper::basename($generator->modelClass))."-index", 'title'=>Html::encode($this->title))) ?>

<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <p>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
		'summary' => "<div class='summary'>". Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.').Html::a('<i class="glyphicon glyphicon-plus"></i>'.Yii::t('app', 'Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass)), ['modelClass' => Inflector::camel2words(StringHelper::basename($generator->modelClass), ]), ['create'], ['class' => 'btn btn-s btn-success btn-icon'])."</div>" ,		
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            // '" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
				'class' => 'yii\grid\ActionColumn',
				'contentOptions' => ['class' => 'table-actions'],
				'template' => '{view}{update}{delete}',
				'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					},
					'update' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-pencil"></i>', Url::to(['/cms/cmspage/update', 'id'=>$model->id]), [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					},
					'delete' => function ($url, $model) {
						return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
								'title' => Yii::t('app', 'Images'), 'class' => 'btn btn-default btn-sm'
						]);
					}
				],
			],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>

<?php $this->endContent(); ?>
