<?php
/**
 * @var View $this
 */
use yii\web\View;
use backend\extensions\kapi\imgattachment\ImageAttachmentWidget;

/** @var ImageAttachmentWidget $widget */
$widget = $this->context;
?>
<?php
if ($widget->model->isNewRecord): ?>
    <div class="imageAttachment">
        <div class="preview"
             style="width: <?php echo $widget->getAttachmentBehavior()->previewWidth ?>px;
                 height: <?php echo $widget->getAttachmentBehavior()->previewHeight ?>px;">
            <div class="no-image">
                <?php echo 'Zanim pobierz zdjęcie, zapisz zmiany we wpisie';/*echo Yii::t('imageAttachment/main', 'Before image upload<br> save this.');*/ ?>
            </div>
            <img/>
        </div>
    </div>
<?php else: ?>
    <div class="imageAttachment" id="<?php echo $widget->id ?>" data-apiurl="<?= $options['apiUrl'] ?>" data-hasimage="<?= $options['hasImage'] ?>" data-previewurl="<?= $options['previewUrl'] ?>">

        <div class="preview">
            <div class="no-image"><?php echo 'Brak zdjęcia'; ?></div>
            <img/>
        </div>
        <div class="btn-toolbar actions-bar">
            <span class="btn btn-success btn-file">
                <i class="glyphicon glyphicon-upload glyphicon-white"></i>
                <span class="file_label"
                      data-upload-text="<?php echo 'Pobierz'; ?>"
                      data-replace-text="<?php echo 'Zastąp'; ?>">
                      </span>
                <input type="file" name="image" class="afile" accept="image/*" multiple="multiple"/>
            </span>

            <span class="btn btn-default disabled remove_image">
                <i class="glyphicon glyphicon-trash"></i> <?php echo 'Usuń'; ?></span>
        </div>
        <div class="overlay">
            <div class="overlay-bg">&nbsp;</div>
            <div class="drop-hint">
                <span
                    class="drop-hint-info"><?php echo 'Przeciągniaj zdjęcie'; ?></span>
            </div>
        </div>
        <div class="progress-overlay">
            <div class="overlay-bg">&nbsp;</div>
            <div class="progress-modal">
                <div class="info">
                    <h3><?php echo 'Pobieranie...'; ?></h3>

                    <div class="progress ">
                        <div class="progress-bar progress-bar-info progress-bar-striped active upload-progress"
                             role="progressbar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
