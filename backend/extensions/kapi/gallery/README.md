KAPISoft Gallery Manager
========================
KAPISoft Gallery Manager

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kapi/yii2-kp-gallery "*"
```

or add

```
"kapi/yii2-kp-gallery": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \kapi\gallery\AutoloadExample::widget(); ?>```