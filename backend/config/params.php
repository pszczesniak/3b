<?php
return [
    'adminEmail' => 'admin@example.com',
    'tableOptions' => [  
		//'class'=>'table table-big table-striped table-bordered table-columns-drag', 
		'class'=>'table table-big table-striped table-bordered', 
	    'data-toolbar'=>"#toolbar",
		'data-toggle'=>"table",
		'data-show-toggle'=>"true",
	    'data-show-columns'=>"true",
	    'data-show-export'=>"true",
		'data-filter-control'=>"true",
        'data-cache'=>"false",
		/*'data-search'=>"true",*/
	    //'data-height'=>"300",
	    //'data-url'=>"https://api.github.com/users/wenzhixin/repos?type=owner&sort=full_name&direction=asc&per_page=100&page=1",
	    //'data-pagination'=>"true",
	    //'data-search'=>"true",
	    //'data-show-refresh'=>"true",
	    /*'data-export-types'=>"['excel', 'pdf']",
	    'data-export-options'=>'{
		 "fileName": "testo", 
		 "worksheetName": "test1",         
		 "jspdf": {                  
		   "autotable": {
			 "styles": { "rowHeight": 20, "fontSize": 10 },
			 "headerStyles": { "fillColor": 255, "textColor": 0 },
			 "alternateRowStyles": { "fillColor": [60, 69, 79], "textColor": 255 }
		   }
		 }
	   }',*/
	],
    'frontenddir' => '/frontend',
	'frontendurl' => '',
	//'languages' => 'de',
	'frontendNativeLang' => '',
    'defaultListStatus' => [1 => 'aktywny', -1 => 'usunięty'],
];
