<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['gii', 'log'],
    'language' => 'pl_PL',
    'modules' => [
		'gii' => [
            'class' => 'yii\gii\Module',
        ],
		'admin' => [
			'class' => 'app\Modules\Admin\Module',
		],
		'loc' => [
            'class' => 'app\Modules\Loc\Module',
        ],
		'cms' => [
            'class' => 'app\Modules\Cms\Modules',
        ],
        'blog' => [
            'class' => 'app\Modules\Blog\Module',
        ],
        'newsletter' => [
            'class' => 'app\Modules\Newsletter\Module',
        ],
        'apl' => [
            'class' => 'app\Modules\Apl\Module',
        ],
        'mix' => [
            'class' => 'app\Modules\Mix\Module',
        ],
        'dict' => [
            'class' => 'app\Modules\Dict\Module',
        ],
		'filemanager' => [
            'class'         => 'linchpinstudios\filemanager\Module',
            'thumbnails'    => [[100,100]],                                              // Optional: array
            'path'          => 'images/uploads/',                                       // Default relative to your web directory or AWS
            'thumbPath'     => 'images/uploads/thumb/',                                 // Default relative to your web directory or AWS
            'url'           => '/',                                                      // either s3 buket URL or CloudFront (can be changed)
            'aws'           => [
                'enable'        => false,
                'key'           => 'YOURAWS_KEY',
                'secret'        => 'YOURAWS_SECRET',
                'bucket'        => 'your-bucket',
            ],
        ],
        'admin' => [
			'class' => 'app\Modules\Admin\Module',
		],
		'company' => [
            'class' => 'app\Modules\Company\Module',
        ],
		'crm' => [
            'class' => 'app\Modules\Crm\Module',
        ],
		'dict' => [
            'class' => 'app\Modules\Dict\Module',
        ],
        'task' => [
            'class' => 'app\Modules\Task\Module',
        ],
		'document' => [
            'class' => 'app\Modules\Document\Module',
        ],
        'correspondence' => [
            'class' => 'app\Modules\Correspondence\Module',
        ],
        'community' => [
            'class' => 'app\Modules\Community\Module',
        ],
        'accounting' => [
            'class' => 'app\Modules\Accounting\Module',
        ],
        'debt' => [
            'class' => 'app\Modules\Debt\Module',
        ],
		'shop' => [
            'class' => 'app\Modules\Shop\Module',
        ],
        'eg' => [
            'class' => 'app\Modules\Eg\Module',
        ],
        'svc' => [
            'class' => 'app\Modules\Svc\Module',
        ],
	],
    'homeUrl' => '/',
	'components' => [
       'request'=>[
            //'baseUrl'=>'/',
			'enableCsrfValidation' => false
        ],
        'urlManager'=>[
            //'scriptUrl'=>'/backend/index.php',
			//'class' => 'common\components\ZUrlManager',
			//'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
			//'enableDefaultLanguageUrlCode' => true,
			/*'ruleConfig' => [
                'class' => common\components\LanguageUrlRule::className()
            ],*/
			//'languages' => ['pl', 'en', 'ru', 'de'],
			'rules' => [
			    '/' => '/site/index',
				'login' => 'site/login',
				'logout' => 'site/logout',
                'history' => 'site/history',
                'info' => 'site/info',
                'request-password-reset' => 'site/request-password-reset',
                'reset-password/<token:\S+>' => 'site/reset-password', 
                'change-password' => 'site/changepassword',
                'check-session' => 'site/checksession',
                
				'configuration' => 'site/configuration',
                
                'custom/data' => 'custom/data',
                'custom/createajax/<type:\d+>' => 'custom/createajax',
                'custom/updateajax/<id:\d+>' => 'custom/updateajax',
                'custom/deleteajax/<id:\d+>' => 'custom/deleteajax',
                
				'files/upload' => 'files/upload',
                'files/update/<id:\d+>' => 'files/update',
                'files/change/<id:\d+>' => 'files/change',
				'files/<action:(getfile|delete|replace|printfile)>/<id:\d+>' => 'files/<action>',
				'files/<id>/<type>' => 'files/index',
                
                'alerts/create/<fid:\d+>/<type:\d+>' => 'alerts/create',
                'alerts/items/<fid:\d+>/<type:\d+>' => 'alerts/items',
                'alerts/update/<id>' => 'alerts/update',
                'alerts/delete/<id>' => 'alerts/delete',
				
                'task/matter/folders/<mid:\d+>/<type:\d+>' => 'task/matter/folders',
				'task/event/createeventform/<type:\d+>/<term>' => 'task/event/createeventform',
                'dict/dictionary/updateajax/<id:\d+>/<type:\d+>' => 'dict/dictionary/updateajax',
                //'dict/dictionary/deleteajax/<id:\d+>/<type:\d+>' => 'dict/dictionary/updateajax',
                'timeline/manager/<date>' => 'timeline/manager',
				'timeline/<action>/<id>' => 'timeline/<action>',
                
                'accounting/report/<action>/<period>' => 'accounting/report/<action>',
                'accounting/setting/services/<type>' => 'accounting/setting/services',
                'accounting/report/export/<year>/<month>' => 'accounting/report/export',
				'accounting/report/exportcsv/<year>/<month>' => 'accounting/report/exportcsv',
                'accounting/report/eexport/<year>/<month>' => 'accounting/report/eexport',
				'accounting/report/eexportcsv/<year>/<month>' => 'accounting/report/eexportcsv',
                'task/personal/services/<type>' => 'task/personal/services',
                'task/personal/efast/<id>/<type>' => 'task/personal/efast',
                'task/personal/edelete/<id>/<type>' => 'task/personal/edelete',
                'task/personal/statsexport/<eid>/<year>' => 'task/personal/statsexport',
                
                'salary/employment/convert/<eid>/<period>' => 'salary/employment/convert',
                'salary/default/customize/<eid>/<period>' => 'salary/default/customize',
				
                'company/resources' => 'company/resources/index',
                'company/replacement' => 'company/replacement/index',
				'company/departments' => 'company/department/index',
				'company/admin' => 'company/admin/index',
				'company/employees' => 'company/employee/index',
				'company/<id>' => 'company/company/view',
				'company/update/<id>' => 'company/company/update',
				
				'crm/customers' => 'crm/customer/index',
				//'crm/customer/new' => 'crm/customer/create',
				//'crm/customers/<id:[a-zA-Z0-9_\-]+>' => 'crm/customer/view',
				'crm/customers/<id>' => 'crm/customer/view',
				'crm/customers/update/<id>' => 'crm/customer/update',
				'crm/customers/delete/<id>' => 'crm/customer/delete',
				
				'office/calendar' => 'task/calendar/index',
                'office/matters' => 'task/matter/index',
				'office/matter/new' => 'task/matter/create',
				'office/matters/<id>' => 'task/matter/view',
				'office/matters/update/<id>' => 'task/matter/update',
				'office/set/events/<id>' => 'task/matter/events',
				
				'office/projects' => 'task/project/index',
				'office/project/new' => 'task/project/create',
				'office/projects/<id>' => 'task/project/view',
				'office/projects/update/<id>' => 'task/project/update',
				
				'office/events' => 'task/event/index',
				'office/event/new' => 'task/event/create',
				'office/events/<id>' => 'task/event/view',
				'office/events/update/<id:\w+>' => 'task/event/update',
                
                'eg/tooth/create/<id>/<tooth>' => 'eg/tooth/create',
                'eg/tooth/remove/<id>/<tooth>' => 'eg/tooth/remove',
                'eg/tooth/treatment/<id>/<tooth>' => 'eg/tooth/treatment',
                'eg/tooth/view/<id>/<tooth>' => 'eg/tooth/view',
				
				'office/cases' => 'task/case/index',
				'office/case/new' => 'task/case/create',
				'office/cases/<id>' => 'task/case/view',
				'office/cases/update/<id:\w+>' => 'task/case/update',
                
                'community/inbox/index/<option>/<label>' => 'community/inbox/index/',
                'community/inbox/data/<option>/<label>' => 'community/inbox/data',
                'community/inbox/starred/<mid>' => 'community/inbox/starred',
				
				'debt/import/items/<cid>/<nip>' => 'debt/import/items',
				
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'<module>' => '<module>/default/index',
				'<module>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
				'<module>/<controller:\w+>/<action:\w+>/<id>' => '<module>/<controller>/<action>',
				'<module>/<controller:\w+>/create' => '<module>/<controller>/create',
                '<module>/<controller:\w+>' => '<module>/<controller>/index',
				'<module>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				//'<module>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				//'<controller:\w+>/<id:\d+>' => '<controller>/view',
				//'<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
				//'<module>/<controller>/<action>' => '<module>/<controller>/<action>',
				//'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				//Rules with Server Names
				/*'http://admin.domain.com/login' => 'admin/user/login',
				'http://www.domain.com/login' => 'site/login',
				'http://<country:\w+>.domain.com/profile' => 'user/view',
				'<controller:\w+>/<id:\d+>-<slug:[A-Za-z0-9 -_.]+>' => '<controller>/view',*/
			]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
			'loginUrl'=>array('site/login'),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'assetManager' => [
			'bundles' => [
				//Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = false;
                //Yii::$app->assetManager->bundles['yii\web\YiiAsset'] = false;
                'yii\web\AssetBundle' => false,
                'yii\web\YiiAsset' => false,
                'yii\web\JqueryAsset' => false ,
					//['js'=>[], 'css' => []	],
				'yii\bootstrap\BootstrapPluginAsset' => false,
				'yii\bootstrap\BootstrapAsset' => false  ,
                'yii\widgets\ActiveFormAsset' => false,
                'yii\validators\ValidationAsset' => false,  
			]
		],
	   'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages', // if advanced application, set @frontend/messages
					'sourceLanguage' => 'pl',
					'forceTranslation' => true,

					'fileMap' => [
						//'main' => 'main.php',
					],
				],
			],
		],
    ],
    'params' => $params,
];
