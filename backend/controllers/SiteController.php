<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\LoginForm;
use common\models\PasswordForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use yii\web\Response;
use yii\helper\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','history'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()  {
 
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/company/default/dashboard' /*'/task/event/calendar'*/]);
        } else {
            return $this->redirect(['/site/login']);
        }
        //return $this->render('index');
        //exit;
    }
    
    public function actionHelp()  {
 
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        return $this->render('help');
        //exit;
    }


    public function actionLogin()  {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        if( isset($_GET['check']) && $_GET['check'] == 'out' ) {
            Yii::$app->session->setFlash('error', 'Twoja sesja wygasła!');
        }
        $this->layout = "@app/views/layouts/auth";
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->goBack();
            $lastChangePass = $this->mustResetPassword(); 
            if($lastChangePass < 150) {
                //return $this->goBack();
                $url = Yii::$app->getSession()->get('__returnUrl', null); 
                if (is_array($url)) {
                    if (isset($url[0])) {
                        //return Yii::$app->getUrlManager()->createUrl($url);
                        //if( \Yii::$app->session->get('user.isSpecial') == 0 )
                            return $this->goBack();
                        /*else
                            return $this->redirect(['/task/calendar/index']);*/
                    } else {
                        return $this->goBack();
                    }
                } else {
                    //if( \Yii::$app->session->get('user.isSpecial') == 0 )
                        return $this->goBack();
                    /*else
                        return $this->redirect(['/task/calendar/index']);*/
                }

                
                // $this->redirect(['/task/event/calendar'])/*$this->goBack()*/ 
            } else {
                return $this->redirect(['/site/changepassword'])/*$this->goBack()*/;
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionCheck() {
		if(\Yii::$app->session->get('user.isSpecial') == 0) {
			Yii::$app->user->logout();
            return $this->goHome();
		} else {
		    if(\Yii::$app->session->get('user.idBranch')) {
				$sqlBoxNoSend = "SELECT count(*) as no_send FROM {{%correspondence}} "
								." WHERE (id_send_fk is null or id_send_fk=0) and type_fk=1 and status=1 and send_at is null and id_company_branch_fk = ".\Yii::$app->session->get('user.idBranch');
				$countBoxNoSend = Yii::$app->db->createCommand($sqlBoxNoSend)->queryOne()['no_send'];
			} else {
				$countBoxNoSend = 0;
			}
			if($countBoxNoSend != 0) {
				return $this->redirect(['/correspondence/box/index', 'alert' => 'noSend']);
			} else {
				Yii::$app->user->logout();
                return $this->goHome();
			}
	    }
	}

    public function actionLogout() {
		Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionChecksession() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != 'login') {
            return [ 'check' => 0, 'action' => Yii::$app->request->url ];
        } else {
            return [ 'check' => 1, 'action' => Yii::$app->request->url ];
        }
    }
    
    public function actionRequestPasswordReset()
    {
        $this->layout = "@app/views/layouts/auth";
        $model = new PasswordResetRequestForm();
        $send = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                //Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                Yii::$app->session->setFlash('success', 'Sprawdź pocztę i wykonaj dalsze instrukcje');
                $send = true;
               // return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Niestety, nie jesteśmy w stanie zresetować hasła, ponieważ mamy problem z wysłaniem e-maila'/*'Sorry, we are unable to reset password for email provided.'*/);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model, 'send' => $send
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = "@app/views/layouts/auth";
        $send = false;
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
           // throw new BadRequestHttpException($e->getMessage());
		   Yii::$app->session->setFlash('error', $e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success','Nowe hasło zostało zapisane.'/* 'New password was saved.'*/);
            //$send = true;
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model, 'send' => $send
        ]);
    }
    
    public function actionChangepassword(){
        $model = new PasswordForm;
        if(Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::to(['/site/login']));
        } else {
            $modeluser = User::findOne(Yii::$app->user->identity->id);
        }
        $modeluser->scenario = User::SCENARIO_PASSWORD;
        $days = $this->mustResetPassword();     
        if( $model->load(Yii::$app->request->post()) && $model->validate() ) {
            try{
                //$modeluser->password = $_POST['PasswordForm']['newpass'];
                $modeluser->setPassword($model->newpass);
                $modeluser->password_reset_time = date('Y-m-d H:i:s');
                if($modeluser->save()){
                    Yii::$app->getSession()->setFlash(
                        'success', Yii::t('lsdd', 'Password changed')
                    );
                    if($days >= 30) 
                        return $this->redirect(['changepassword']);
                }else{ //var_dump($modeluser->getErrors()); exit;
                    Yii::$app->getSession()->setFlash(
                        'error', Yii::t('lsdd', 'Password not changed')
                    );
                  //  return $this->redirect(['index']);
                }
                return $this->render('changepassword',[
                    'model'=>$model, 'days' => $days
                ]);
            } catch(Exception $e){
                Yii::$app->getSession()->setFlash(
                    'error',"{$e->getMessage()}"
                );
                return $this->render('changepassword',[
                    'model'=>$model, 'days' => $days
                ]);
            }
        } else{
            return $this->render('changepassword',[
                'model'=>$model, 'days' => $days
            ]);
        }
    }
    
    public function mustResetPassword() {
        if(!Yii::$app->user->isGuest) {
            if(Yii::$app->user->identity->password_reset_time) {
                $t1 = StrToTime ( date('Y-m-d', strtotime(Yii::$app->user->identity->password_reset_time)) );
            } else {
                $t1 = Yii::$app->user->identity->created_at;
            } 
            //echo date('Y-m-d H:i:s', Yii::$app->user->identity->created_at);exit;
            $t2 = StrToTime ( date('Y-m-d') );
            $diff = $t2 - $t1;
            $days = round($diff / ( 60 * 60 * 24));
            return $days;
        } else return 0;
    }
    
    public function actionMustresetpassword() {
        if(!Yii::$app->user->isGuest) {
            if(Yii::$app->user->identity->password_reset_time) {
                $t1 = StrToTime ( date('Y-m-d', strtotime(Yii::$app->user->identity->password_reset_time)) );
            } else {
                $t1 = Yii::$app->user->identity->created_at;
            } 
            //echo date('Y-m-d H:i:s', Yii::$app->user->identity->created_at);exit;
            $t2 = StrToTime ( date('Y-m-d') );
            $diff = $t2 - $t1;
            $days = round($diff / ( 60 * 60 * 24));
            return $days;
        } else return 0;
    }
    
    public function actionHistory() {
		//$date = strtotime(date('Y-m-d H').':00:00');
        $date = strtotime('2016-09-22 19:00:00');
        $managersSql = "select distinct id_employee_manager_fk from {{%company_department}} where id_employee_manager_fk is not null";
        $managers = Yii::$app->db->createCommand($managersSql)->queryAll();
        
        if(isset($_POST['start'])) $start = $_POST['start']; else $start = date('Y-m-d H', ($date-3600));
		if(isset($_POST['end'])) $end = $_POST['end']; else $end = date('Y-m-d H', $date);
		
		$where = "  DATE_FORMAT(created_at, '%Y-%m-%d %H') >= '".$start."' ";
		$where .= " and DATE_FORMAT(created_at, '%Y-%m-%d %H') < '".$end."' ";
        
        $whereUpdate = "  DATE_FORMAT(updated_at, '%Y-%m-%d %H') >= '".$start."' ";
		$whereUpdate .= " and DATE_FORMAT(updated_at, '%Y-%m-%d %H') < '".$end."' ";
        
        $whereRowsCase = " (( ".$where." ) or ( ".$whereUpdate." ) or (id in (select id_fk from {{%files}} where id_type_file_fk=3 and ".$where." ) ) )  ";
        $whereRowsTask = " (( ".$where." ) or ( ".$whereUpdate." ) or (id in (select id_fk from {{%files}} where id_type_file_fk=4 and ".$where." ) ) )  ";
        
        foreach($managers as $key => $value) {
            $model = \api\modules\company\models\CompanyEmployee::find()->where(['id' => $value])->one();
            $departmens = [0 => 0];
            $managerOfDepartments = \api\modules\company\models\CompanyDepartment::find()->where(['id_employee_manager_fk' => $model->id])->all();
            foreach($managerOfDepartments as $key => $value) {
                array_push($departmens, $value->id);
            }
            
            $sets = \backend\Modules\Task\models\CalCase::find()->where('id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',',$departmens).'))')->andWhere($whereRowsCase)->orderby('type_fk')->all();
            $events = \backend\Modules\Task\models\CalTask::find()->where('id_case_fk in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',',$departmens).'))')->andWhere($whereRowsTask)->orderby('type_fk')->all();
            $changes = [];
           // echo 'id in (select id_case_fk from {{%case_department}} where id_department_fk in ('.implode(',',$departmens).')) and '.$where.$whereUpdate.'<br />';
            foreach( $sets as $key => $set ) {
                $temp = [];
                $created = strtotime($set->created_at);
                
                $temp['id'] = $set['id'];
                $temp['fk'] = 'set';
                $temp['name'] = $set->name;
                $temp['customer'] = $set->customer['name'];
                $temp['creator'] = \common\models\User::findOne($set->created_by)->fullname;
                $temp['type_fk'] = $set->type_fk;
                $temp['type'] = ($set->type_fk == 1) ? 'sprawy' : 'projektu';
                $temp['status'] = $set->name;
                $temp['changes_list'] = [];
                if( $created >= ($date-3600) && $created < $date) {
                    //array_push($temp['changes_list'], 'Wprowadzenie '. ( ($set->type_fk == 1) ? 'sprawy' : 'projektu' ) .' do systemu');
                    $tmp = [];
                    $tmp['sender'] = $temp['creator'];
                    $tmp['date'] = $set->created_at;
                    $tmp['content'] = '<span style="font-weight:bold;color:green;">Wprowadzenie '. ( ($set->type_fk == 1) ? 'sprawy' : 'projektu' ) .' do systemu</span>';
                    array_push($temp['changes_list'],$tmp);
                }    

                $archs = \backend\Modules\Task\models\CalCaseArch::find()->where(['id_case_fk' => $set->id])->andWhere($where)->orderby('id')->all();
                foreach($archs as $i => $item) {
                   /* if($item->user_action != 'event' && $item->user_action != 'case') {                        
                        array_push($temp['changes_list'], ($item->user_action == 'docs') ? 'Nowe dokumenty '.$item->content : $item->content);
                    }*/
                    if($item->user_action != 'event' && $item->user_action != 'case') { 
                        $tmp = [];
                        $tmp['sender'] = $item->creator->fullname;
                        $tmp['date'] = $item->created_at;
                        if($item->user_action == 'docs') {
                            $tmp['content'] = 'Nowe dokumenty '.str_replace('<a','<span',str_replace('</a>','</span>',$item->content));
                        } else {
                            $tmp['content'] = $item->content;
                        }
                        if( !empty($tmp['content']) )
                            array_push($temp['changes_list'],$tmp);
                    }
                }
                
                array_push($changes, $temp);
            }
            
            foreach( $events as $key => $event ) {
                $temp = [];
                $created = strtotime($event->created_at);
                
                $temp['id'] = $event['id'];
                $temp['fk'] = 'event';
                $temp['name'] = $event->name;
                $temp['customer'] = $event->case['customer']['name'];
                $temp['creator'] = \common\models\User::findOne($event->created_by)->fullname;
                $temp['type_fk'] = $event->type_fk;
                $temp['type'] = ($event->type_fk == 1) ? 'rozprawy' : 'zadania';
                $temp['status'] = $event->name;
                $temp['changes_list'] = [];
                if( $created >= ($date-3600) && $created < $date) {
                    //array_push($temp['changes_list'], 'Wprowadzenie '. ( ($set->type_fk == 1) ? 'sprawy' : 'projektu' ) .' do systemu');
                    $tmp = [];
                    $tmp['sender'] = $temp['creator'];
                    $tmp['date'] = $event->created_at;
                    $tmp['content'] = '<span style="font-weight:bold;color:green;">Wprowadzenie '. ( ($event->type_fk == 1) ? 'rozprawy' : 'zadania' ) .' do systemu</span>';
                    array_push($temp['changes_list'],$tmp);
                }       

                $archs = \backend\Modules\Task\models\CalTaskArch::find()->where(['id_task_fk' => $event->id])->andWhere($where)->orderby('id')->all();
                foreach($archs as $i => $item) {
                   /* if($item->user_action != 'event' && $item->user_action != 'case') {                        
                        array_push($temp['changes_list'], ($item->user_action == 'docs') ? 'Nowe dokumenty '.$item->content : $item->content);
                    }*/
                    if($item->user_action != 'event' && $item->user_action != 'case') { 
                        $tmp = [];
                        $tmp['sender'] = $item->creator->fullname;
                        $tmp['date'] = $item->created_at;
                        if($item->user_action == 'docs') {
                            $tmp['content'] = 'Nowe dokumenty '.str_replace('<a','<span',str_replace('</a>','</span>',$item->content));
                        } else {
                            $tmp['content'] = $item->content;
                        }
                        if( !empty($tmp['content']) )
                            array_push($temp['changes_list'],$tmp);
                    }
                }
               
                
                array_push($changes, $temp);
                
            }
            echo '<br /><br />';var_dump($changes);
            if(count($changes) > 0) {
                \Yii::$app->mailer->compose(['html' => 'history-html', 'text' => 'history-text'], ['changes' => $changes, 'date' => $date ])
                                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name ])
                                    //->setTo(\Yii::$app->params['testEmail'])
                                    ->setTo(['kamila_bajdowska@onet.eu'])
                                    ->setSubject('Informacja systemu nt. zmian w sprawach/projektach/rozprawach/zadaniach dla użytkownika '.$model->fullname) 
                                    ->send();
            }
        }
        
       
        //return $this->render('manager', ['model' => $model, 'arch' => $arch, 'start' => $start, 'end' => $end]);
    }
    
    public function actionInfo() {
        phpinfo(); 
    }
}
