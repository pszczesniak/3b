<?php

namespace backend\controllers;

use Yii;
use common\models\Translations;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Url;


class TranslationsController extends Controller
{
	
	public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "delete"); 
        return parent::beforeAction($action);
    }
    
    public function actionUpdate($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        $model->attr_data = \yii\helpers\Json::encode(Yii::$app->request->post());
        if ($model->save()) {
            $res = ['success' => true, 'alert' => 'Tłumaczenie zostało zapisane'];
        } else {
            $res = [ 'success' => false, 'errors' => $model->getErrors() , ];
        }
        
        return $res;
    }
		
	/**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)   {
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return array('result' => true);	
        //return $this->redirect(['index']);
    }
	
	/**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Translations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
