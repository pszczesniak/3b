<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\Custom;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use common\components\CustomHelpers;
use yii\helpers\Url;

/**
 * Custom controller
 */
class CustomController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "contact" ); 
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'contact'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'update', 'data', 'createajax', 'updateajax', 'deleteajax'],
                        'allow' => true,
                        //'roles' => ['@'],
                    ],
                ],
            ],
           /* 'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
		//$queryParams["CmsPageSearch"]["status"] = 1;
        $query = Custom::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionData($type) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$fieldsData = Custom::find()->where(['type_fk' => $type, 'status' => 1])->orderby('id')->all();

		$fields = [];
		$tmp = [];
	
		foreach($fieldsData as $key => $value) {

            $actionColumn = '<div class="edit-btn-group">';
                $actionColumn .= '<a href="'.Url::to(['/custom/updateajax', 'id' => $value->id]).'" class="btn btn-xs btn-success btn-icon gridViewModal" data-table="#table-custom-'.$value->type_fk.'" data-form="item-form" data-target="#modal-grid-item"  data-title="<i class=\'glyphicon glyphicon-pencil\'></i>'.Yii::t('lsdd', 'Edit').'"'.Yii::t('lsdd', 'Edit').'" >'.Yii::t('lsdd', 'Edit').'</a>';

                $actionColumn .= '<button class="btn btn-xs btn-danger deleteConfirm" data-table="#table-custom-'.$value->type_fk.'">'.Yii::t('lsdd', 'Delete').'</button>';
            $actionColumn .= '</div>';
        
			$tmp['describe'] = $value->content;
            $tmp['name'] = $value->title;
            $tmp['action'] = $actionColumn;
            //$tmp['move'] = '<a class="btn btn-default btn-sm action up" data-table="#table-steps" href="javascript:void(0)" title="Move up"><i class="glyphicon glyphicon-arrow-up"></i></a><a class="btn btn-default btn-sm action down" data-table="#table-steps" href="javascript:void(0)" title="Move down"><i class="glyphicon glyphicon-arrow-down"></i></a>';
			$tmp['id'] = $value->id;//time();
            $tmp['index'] = $key;
			
			array_push($fields, $tmp); $tmp = [];
		}
		
		return $fields;
	}

    public function actionUpdate($id)
    {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/offer/login']));
        } 
 
        //$id = CustomHelpers::decode($id);
        $model = Custom::findOne($id);

        if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON; 
			//echo $_POST['CmsPage']['content'];
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$res = array(
					'success' => true,
				);
			} else {
				$res = array(
					'errors'    =>  $model->getErrors() ,
					'success' => false,
				);
			}
	 
			return $res;
		} else {

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		}
      
    }
    
    public function actionCreateajax($type) {
		
        $model = new Custom();
        $model->type_fk = $type;
        $model->system_name = 'rule';
   	
		if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
            $model->status = 1;
    
            if($model->validate() && $model->save()) {
                return array('success' => true, 'action' => 'insertRow', 'index' =>1, 'alert' => 'Zmiany zostały zapisane', 'id'=>$model->id,'name' => $model->title );	
            } else {
                return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
            }
      	
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
	}
    
    public function actionUpdateajax($id) {
        $model = $this->findModel($id);
       
        if (Yii::$app->request->isPost ) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model->load(Yii::$app->request->post());
           
			if($model->validate() && $model->save()) {
				return array('success' => true, 'action' => 'updateRow', 'alert' => 'Wpis został zaktualizowany' );	
			} else {
				return array('success' => false, 'html' => $this->renderAjax('_formAjax', [  'model' => $model, ]), 'errors' => $model->getErrors() );	
			}		
		} else {
			return  $this->renderAjax('_formAjax', [  'model' => $model, ]) ;	
		}
    }
    
    public function actionDeleteajax($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status = -1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->deleted_by = \Yii::$app->user->id;
        if($model->save()) {
            return array('success' => true, 'alert' => 'Dane zostały usunięte', 'id' => $id, 'table' => '#table-custom-'.$model->type_fk);	
        } else {
            return array('success' => false, 'row' => 'Dane nie zostały usunię', 'id' => $id, 'table' => '#table-custom-'.$model->type_fk);	
        }

       // return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = Custom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
