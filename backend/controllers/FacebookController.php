<?php

namespace backend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\data\ActiveDataProvider;


/**
 * FacebookController implements the CRUD actions for SvcOffer model.
 */
class FacebookController extends Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;//($action->id !== "upload" && $action->id !== "saveavatar"); 
        return parent::beforeAction($action);
    }
 
    
   public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\AccessControl',
               // 'only' => ['create', 'update', 'delete', 'gallery', 'files', 'newsletter', 'socialmedia', 'inbox', 'location', 'requestPasswordReset'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                       /* 'matchCallback' => function(){
                            return (isset(Yii::$app->user->identity->role) && Yii::$app->user->identity->role->name == 'admin');
                        },*/
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                   return $action->controller->redirect('/site/noaccess');

                   // return \Yii::$app->getUser()->loginRequired();
                },
            ],
            [
                'class' => 'yii\filters\AccessControl',
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return \Yii::$app->getUser()->loginRequired();
                },
            ],
            //...

        ];
    }

    
    public function actions()
	{
		return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
            
		];
	}
    
    public function onAuthSuccess($client) {
        $reponse = $client->getUserAttributes();

        $session = \Yii::$app->session;
        $token = $client->accessToken->params['access_token'];

        $session->set('fb.token' ,$token);
        $id = ArrayHelper::getValue($reponse , 'id');
        $session->set('fb.id', $id);
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['index']);
       // }
    
    }
    
    public function actionFbdisconnet() {
        
        $session = \Yii::$app->session;
        
        $session->remove('fb.token');
        $session->remove('fb.id');
        //Facebook Oauth
        //if($client instanceof \yii\authclient\clients\Facebook){

            //Do Facebook Login
            return $this->redirect(['index']);
       // }
    
    }
    
    public function actionIndex()
    {
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/login']));
        } 
        
        
        return $this->render('index', [
            'model' => false,
        ]);
    }
    
    public function actionPublish() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $response = false;
        $appId = '682422408630872';
        $appSecret = '91a618f4fd16b6a08e511d54f33ff7b4';
        $accessToken = \Yii::$app->session->get('fb.token');
        
        if(\Yii::$app->session->get('fb.token') && \Yii::$app->session->get('fb.id')) {
            $fb = new \Facebook\Facebook([
                'app_id' => $appId,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v2.5',
                'default_access_token' => \Yii::$app->session->get('fb.token'),
                'cookie' => true
            ]);
            $linkData = [
              'link' => 'http://twojeprzyjecia.pl',
              'message' => 'User provided message',
              ];

            try {
               /* //$response = $fb->get('/me?fields=id,name,albums.limit(5){name, photos.limit(2){name, picture, tags.limit(2)}},posts.limit(5)');//var_dump($response); echo '<br />';
                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                $user = $response->getGraphUser();
                //  echo 'Name: ' . $user->getName().'<br />';
               // var_dump($user);
                $res = $fb->get($user->getId().'/feed?message=test',  $accessToken);
               //  echo '<br /><br />';
                $msg = $res->getGraphObject();
                var_dump($msg);*/
                $permissions = $fb->get('/me/permissions');//var_dump($permissions);
                $response = $fb->get('/me?fields=id,email,name,link,website,about', $accessToken); //var_dump( $response->getGraphObject()).'<br />';
                $user = $response->getGraphUser(); //var_dump($user);
                $msg = $fb->post( '/'.$user->getId().'/feed', ['message' => 'Playing around with FB Graph..'], $accessToken  );
              

            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
               // echo 'Graph returned an error: ' . $e->getMessage();
                return ['result' => false, 'error' => $e->getMessage()];
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                return ['result' => false,  'error' => $e->getMessage()];
            }
            return ['result' => true, 'msg' => $msg];
     
        
        } else {
        
            return ['result' => false];
        }
        
        /*$facebook = new \Facebook\Facebook(array(
            'appId'  => $appId,
            'secret' => $appSecret,
            'default_graph_version' => 'v2.5',
            'default_access_token' => $accessToken,
            'cookie' => true
        ));
        
        
        $session = $facebook->getSession();

        if (!$session) {
            $url = $facebook->getLoginUrl(array(
                'canvas' => 1,
                'fbconnect' => 0
            ));
            echo "<script type='text/javascript'>top.location.href = '$url';</script>";

        } else {
            try {
                $uid = $facebook->getUser();
                $me = $facebook->api('/me');
                $updated = date("l, F j, Y", strtotime($me['updated_time']));
                echo "Hello " . $me['name'] . "<br />";
                echo "You last updated your profile on " . $updated;

                $connectUrl = $facebook->getUrl(
                  'www',
                  'login.php',
                  array_merge(array(
                    'api_key'         => $facebook->getAppId(),
                    'cancel_url'      => 'http://www.test.com',
                    'req_perms'       => 'publish_stream',
                    'display'         => 'page',
                    'fbconnect'       => 1,
                    'next'            => 'http://www.test.com',
                    'return_session'  => 1,
                    'session_version' => 3,
                    'v'               => '1.0',
                  ), $params)
                );

                $result = $facebook->api(
                    '/me/feed/',
                    'post',
                    array('access_token' => $accessToken, 'message' => 'Playing around with FB Graph..')
                );

            } catch (FacebookApiException $e) {
                echo "Error:" . print_r($e, true);
            }
        }*/
        
        
    }
} 
