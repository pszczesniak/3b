module.exports = function (grunt) {

	var config = {
        website: 'frontend'
    };

	grunt.initConfig({
        config: config,
		// Sass file compilation and compression
        sass: {
            dist: {
                options: {
                    style: 'nasted'
                },
                files: {
                    "<%= config.website %>/web/css/all.css": "assets/scss/<%= config.website %>/main.scss",
                    "<%= config.website %>/web/css/critical.css": "assets/scss/<%= config.website %>/critical.scss",
                    "<%= config.website %>/web/css/admin.css": "assets/scss/<%= config.website %>/admin.scss"
                    /*,
					"<%= config.website %>/web/css/all_admin.css": "assets/scss/<%= config.website %>-admin/main.scss",*/
                }
				/*files: [{
                    expand: true,
                    cwd: 'assets/scss/frontend',
                    src: ['*.scss'],
                    dest: 'frontend/web/css',
                    ext: '.css'
                }]*/
            }/*,
			 dev: {                                  // Another target
				options: {                          // Target options
					style: 'expanded'
				},
				files: {
					"<%= config.website %>/web/css/all.css": "assets/scss/<%= config.website %>/main.scss",
                    "<%= config.website %>/web/css/critical_homepage.css": "assets/scss/<%= config.website %>/critical_homepage.scss",
					"<%= config.website %>/web/css/all_admin.css": "assets/scss/<%= config.website %>-admin/main.scss"
				}
			}*/
        },

        typescript: {
            base: {
                src: ['assets/ts/*.ts'],
                dest: '<%= config.website %>/web/js/all.js',
                options: {
                    module: 'amd',
                    sourceMap: true,
                    target: 'es5'
                }
            }
        },
        concat_sourcemap: {
            options: {
                sourcesContent: true
            },
            all: {
                files: {
                    '<%= config.website %>/web/js/all.js': grunt.file.readJSON('assets/js/'+config.website+'/all.json')
					//'backend/web/js/all.js': grunt.file.readJSON('assets/js/backend/all.json')
                }
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, flatten: true, src: ['vendor/bower/bootstrap/fonts/*'], dest: 'web/fonts/', filter: 'isFile'}
                ]
            }
        },
		 // SCSS linting
        scsslint: {
            allFiles: [
              '/assets/scss/<%= config.website %>/**/*.scss', '/assets/scss/<%= config.website %>/**/**/*.scss',
            ],
            options: {
              bundleExec: false,
              colorizeOutput: false
            }
        },
        /*uglify: {
            options: {
                mangle: false
            },
            all: {
                files: {
                    '<%= config.website %>/web/js/all.min.js': '<%= config.website %>/web/js/all.js',
					'<%= config.website %>/web/js/all_admin.min.js': '<%= config.website %>/web/js/all_admin.js',
					//'backend/web/js/all.min.js': 'backend/web/js/all.js'
                }
            }
        },*/

        // CSS minification
        cssmin: {
            dist: {
              files: [{
                expand: true,
                cwd: '<%= config.website %>/web/css/',
                src: ['*.css', '!*.min.css'],
                dest: '<%= config.website %>/web/css/',
                ext: '.min.css'
              }]
            }
        },

        // Adding autoprefixes to CSS files
        autoprefixer: {
            options: {
              //browsers: ['last 2 versions']
            },
            dist: {
              src: '<%= config.website %>/web/css/*.css'
            }
        },

        watch: {
            typescript: {
                files: ['assets/ts/*.ts'],
                tasks: ['typescript'/*, 'uglify:all'*/],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['assets/js/**/*.js', 'assets/js/**/**/**/*.js', 'assets/js/**/all.json', 'assets/js/**/all_admin.json'],
                tasks: ['concat_sourcemap'/*, 'uglify:all', 'uglify:lib'*/],
                options: {
                    livereload: true
                }
            },
			sass: {
              files: ['assets/scss/<%= config.website %>/*.scss', 'assets/scss/<%= config.website %>/components/*.scss', 'assets/scss/<%= config.website %>/components/*/*.scss', 'assets/scss/<%= config.website %>/components/*/*/*.scss', 'assets/scss/<%= config.website %>/plugins/*/*.scss',
			          'assets/scss/<%= config.website %>-admin/*.scss', 'assets/scss/<%= config.website %>-admin/components/*.scss', 'assets/scss/<%= config.website %>-admin/components/*/*.scss', 'assets/scss/<%= config.website %>-admin/plugins/*/*.scss'],
              tasks: ['sass'],
              options: {
                spawn: false,
                livereload: true
              }
            },
            fonts: {
                files: [
                    'vendor/bower/bootstrap/fonts/*'
                ],
                tasks: ['copy'],
                options: {
                    livereload: true
                }
            },

        }/*,
		criticalcss: {
			custom: {
				options: {
					url: "http://yii-frontend-website.dev/",
					width: 1200,
					height: 1200,
					outputfile: "frontend/web/css/critical.css",
					filename: "frontend/web/css/main.css", // Using path.resolve( path.join( ... ) ) is a good idea here
					buffer: 800*1024,
					ignoreConsole: false
				}
			}
	    },*/
    });

    // Plugin loading
    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-concat-sourcemap');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-scss-lint');
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    //grunt.loadNpmTasks('grunt-criticalcss');
    //grunt.loadNpmTasks('grunt-image-embed');



    // Task definition
    grunt.registerTask('build', ['sass', 'autoprefixer', 'typescript', 'copy', 'concat_sourcemap'/*, 'uglify', 'imageEmbed', 'criticalcss'*/, 'cssmin']);
    grunt.registerTask('default', ['autoprefixer', 'watch']);
    grunt.registerTask('test', ['scsslint', 'jshint']);
};
