module.exports = {
  entry: [/* './src/global.js',*/ './src/app.jsx'],
  output: {
    filename: './src/bundle.js',
  },
  module: {
   /*  preLoaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
    ],*/

    loaders: [
      {
        test: [/\.jsx$/, /\.es6$/],
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015'],
        },
      },
    ],
  },

  resolve: {
    extensions: ['', '.js', '.jsx', '.es6'],
  },
};
